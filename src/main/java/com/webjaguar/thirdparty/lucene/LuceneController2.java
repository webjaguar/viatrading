/* Copyright 2014 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 12.02.2014
 */

package com.webjaguar.thirdparty.lucene;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.facet.params.FacetSearchParams;
import org.apache.lucene.facet.search.CountFacetRequest;
import org.apache.lucene.facet.search.FacetRequest;
import org.apache.lucene.facet.search.FacetResult;
import org.apache.lucene.facet.search.FacetResultNode;
import org.apache.lucene.facet.search.FacetsCollector;
import org.apache.lucene.facet.taxonomy.CategoryPath;
import org.apache.lucene.facet.taxonomy.TaxonomyReader;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyReader;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queries.BooleanFilter;
import org.apache.lucene.queries.ChainedFilter;
import org.apache.lucene.queries.FilterClause;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery.TooManyClauses;
import org.apache.lucene.search.FieldCacheTermsFilter;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.MultiCollector;
import org.apache.lucene.search.NumericRangeFilter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopFieldDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.FilterAttribute;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LuceneProductSearch;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Price;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.RangeValue;
import com.webjaguar.model.Supplier;
import com.webjaguar.web.domain.Constants;



@Controller
public class LuceneController2 extends WebApplicationObjectSupport{

	private IndexSearcher searcher;
	private QueryParser qp;

	@Autowired
	private WebJaguarFacade webJaguar;
	
	@RequestMapping(value="/lsearch.jhtm")
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Date d1 = new Date();
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		Customer customer = this.webJaguar.getCustomerByRequest(request);
		
		LuceneProductSearch search = getProductSearch(request, customer);
		//System.out.println("Lucene Search line 101 "+(new Date().getTime() - d1.getTime()));
		// protected access
		if (customer != null) {
			search.setProtectedAccess(customer.getProtectedAccess());
		} else {
			search.setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue());
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		}
		if ((Boolean) gSiteConfig.get("gMASTER_SKU") && Integer.parseInt(siteConfig.get("PARENT_CHILDERN_DISPLAY").getValue()) == 0) {
			search.setMasterSku(true);
		}
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		String url = request.getContextPath() + request.getServletPath();
		String qry = request.getQueryString();
		if(qry != null) {
			url = url + "?" + qry;
		}
		// continue shopping
		request.getSession().setAttribute("continueShoppingUrl", url);
        // required to set to request directly as frontend jsps are disabled to access session
		// sometimes jsp creates multiple jsessionid
		request.setAttribute("continueShoppingUrl", url);
		// TO BE DELETED
		//search.setConjunction(siteConfig.get("SEARCH_DEFAULT_KEYWORDS_CONJUNCTION").getValue());

		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		List<Product> results = new ArrayList<Product>();

		boolean showPriceColumn = false;
		boolean showQtyColumn = false;
		boolean minCharsMet = false;
		
		// check number of characters
		if ( search.getNoKeywords() == null || (search.getNoKeywords() != null && !search.getNoKeywords()) ) {
			search.setKeywordsMinChars(Integer.parseInt(siteConfig.get("SEARCH_KEYWORDS_MINIMUM_CHARACTERS").getValue()));
			if (search.getKeywords().trim().length() > 0) {
				for (String token : search.getKeywords().split(" ")) {
					if (token.length() >= search.getKeywordsMinChars()) {
						minCharsMet = true;
					}
				}
			} else if (search.getKeywordsMinChars() == 0) {
				minCharsMet = true;
			}
			model.put("minCharsMet", minCharsMet);
		}

		// default proximity set to 50. If it is greater than 100, set it back to 50
		Integer proximity = Integer.parseInt(siteConfig.get("LUCENE_PROXIMITY_VALUE").getValue());
		if (ServletRequestUtils.getIntParameter(request, "pr") != null) {
			proximity = ServletRequestUtils.getIntParameter(request, "pr", proximity) <= 100 ? ServletRequestUtils.getIntParameter(request, "pr", proximity) : proximity;
		} else if (request.getSession().getAttribute("proximity") != null) {
			proximity = (Integer) request.getSession().getAttribute("proximity");
		}
		request.getSession().setAttribute("proximity", proximity);
		// required to set to request directly as frontend jsps are disabled to access session
		// sometimes jsp creates multiple jsessionid
		request.setAttribute("proximity", proximity);
	
        // Either keywords are required or must have noKeywords=true
		if ((search.getKeywords() != null && search.getKeywords().trim().length() > 0 && minCharsMet) || ServletRequestUtils.getBooleanParameter(request, "noKeywords", false)) {
        	
			DirectoryReader reader = DirectoryReader.open(FSDirectory.open(new File(getServletContext().getRealPath("/lucene_index/products"))));
			
			
			searcher = new IndexSearcher(reader);
			    
			SortField sfName = new SortField("nameSortField", SortField.Type.STRING, false);
			SortField sfSearchRank = new SortField("search_rank", SortField.Type.INT, false);
			SortField sfPriceRank = new SortField("min_price", SortField.Type.DOUBLE, false);
			
			Sort sort = new Sort(sfSearchRank, sfName, sfPriceRank);
			
			if(siteConfig.get("LUCENE_RELEVANCE_SEARCH").getValue().equals("true")) {
				SortField sfAbcCode = new SortField("abc_code", SortField.Type.INT, false);;
				SortField sfBrandName = new SortField("brandSortField", SortField.Type.STRING, false);;
				
				sort = new Sort(SortField.FIELD_SCORE, sfAbcCode, sfSearchRank, sfName, sfPriceRank);
				
				if(search.getSort().equalsIgnoreCase("rlv")) {
					sort = new Sort(SortField.FIELD_SCORE, sfAbcCode, sfSearchRank, sfName, sfPriceRank);
				} else if(search.getSort().equalsIgnoreCase("lth")) {
					sort = new Sort(sfPriceRank, sfAbcCode, sfSearchRank, sfName);
				} else if(search.getSort().equalsIgnoreCase("htl")) {
					sfPriceRank = new SortField("min_price", SortField.Type.DOUBLE, true);
					sort = new Sort(sfPriceRank, sfAbcCode, sfSearchRank, sfName);
				} else if(search.getSort().equalsIgnoreCase("name")) {
					sort = new Sort(sfName, sfAbcCode, sfSearchRank, sfPriceRank);
				} else if(search.getSort().equalsIgnoreCase("braz")) {
					sort = new Sort(sfBrandName, sfAbcCode, sfName);
				} else if(search.getSort().equalsIgnoreCase("brza")) {
					sfBrandName = new SortField("brandSortField", SortField.Type.STRING, true);
					sort = new Sort(sfBrandName, sfAbcCode, sfName);
				}
				
			} else {
				if(search.getSort().equalsIgnoreCase("lth")) {
					sort = new Sort(sfPriceRank, sfSearchRank, sfName);
				} else if(search.getSort().equalsIgnoreCase("htl")) {
					sfPriceRank = new SortField("min_price", SortField.Type.DOUBLE, true);
					sort = new Sort(sfPriceRank, sfSearchRank, sfName);
				} else if(search.getSort().equalsIgnoreCase("name")) {
					sort = new Sort(sfName, sfSearchRank, sfPriceRank);
				} 
			}
			
			StringBuffer keywords = new StringBuffer();

			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
			qp = new QueryParser(Version.LUCENE_CURRENT, "keywords", analyzer);
			qp.setAllowLeadingWildcard(true);
			
			// Use || to search multiple search phrases
			// For example, /lsearch.jhtm?keywords=Red+Pen||Red+Pencil will search for "Red Pen" OR "Red Pencil"
			String[] searchPhrases = search.getKeywords().split("\\|\\|");
			
			// search fields
			if (siteConfig.get("LUCENE_GROUP_FIELDS_INTO_ONE").getValue().equals("true")) {
				if(siteConfig.get("LUCENE_RELEVANCE_SEARCH").getValue().equals("true") && search.getKeywords().trim().split(" ").length == 1) {
					// Like Search *
					// Remove - as it does not work with partial search
					String keyword = search.getKeywords().trim().split(" ")[0];
					keyword = keyword.replace("-", "");
					keywords.append("(all_fields_data : *" + keyword +"*");
				
					keywords.append(" OR all_fields_data :\"" + keyword + "\"~" + proximity);
					
				} else {
				
					keywords.append("( \"" + searchPhrases[0].trim() + "\"~" + proximity);
					
					for (String searchPhrase : searchPhrases) {
						keywords.append(" OR all_fields_data :\"" + searchPhrase.trim() + "\"~" + proximity);
					}
				}
				
				if(search.getExcludeKeywords() != null) {
					String[] excludeKeywords = search.getExcludeKeywords().split("\\|\\|");
					/*
					 * EXCLUDE KEYWORDS
					 * 
					 */
					for(String excludeKey: excludeKeywords) {
						keywords.append(" NOT all_fields_data :\"" + excludeKey + "\"~" + proximity);
					}
				}
				//System.out.println("keywords: " + keywords);	
				
			} else {
				keywords.append("( \"" + searchPhrases[0].trim() + "\"~" + proximity);
			
				for (String searchPhrase : searchPhrases) {
					keywords.append(" OR name :\"" + searchPhrase + "\"~" + proximity);
				} 

				String[] searchFieldsList = siteConfig.get("SEARCH_FIELDS").getValue() != null ? siteConfig.get("SEARCH_FIELDS").getValue().split(",") : null;
				if (searchFieldsList != null) {
					for (String field : searchFieldsList) {
						if (!field.isEmpty() && !(field.equalsIgnoreCase("id") || field.equalsIgnoreCase("name") || field.equalsIgnoreCase("search_rank"))) {
							for (String searchPhrase : searchPhrases) {
								keywords.append(" OR " + field + " :\"" + searchPhrase + "\"~" + proximity);
							}
						}
					}
				}
			}
			//System.out.println("Lucene Search line 245 "+(new Date().getTime() - d1.getTime()));
			
			if(ServletRequestUtils.getStringParameter(request, "madeIn") != null) {
				keywords.append(" AND (all_fields_data : \"U.S.A.\" OR all_fields_data : \"USA\")" );
			}
			
			if(ServletRequestUtils.getStringParameter(request, "eco") != null) {
				keywords.append(" AND (all_fields_data : \"eco\")" );
			}
				
			// complete query bracket
			keywords.append(")");

			//System.out.println("keywords: " + keywords);
			BooleanFilter multipleFilterWrapper = null;
			BooleanFilter taxonomyFilterWrapper = null;
			
			
			
			// min & max price
			Double minPrice = ServletRequestUtils.getDoubleParameter(request, "minPrice", -1.0);
			Double maxPrice = ServletRequestUtils.getDoubleParameter(request, "maxPrice", -1.0);
			
			
			if(siteConfig.get("LUCENE_SEARCH_ON_MAX_PRICE_FILTER_ONLY").getValue().equals("true")) {
				
				Filter maxPriceFilter = null;
				if (minPrice != -1.0 || maxPrice != -1.0) {
					maxPriceFilter = NumericRangeFilter.newDoubleRange("max_price", (minPrice != -1.0 ? minPrice : Double.MIN_VALUE), (maxPrice != -1.0 ? maxPrice : Double.MAX_VALUE), true, true);
					if(multipleFilterWrapper == null) {
						multipleFilterWrapper = new BooleanFilter();
						taxonomyFilterWrapper = new BooleanFilter();
					}
					multipleFilterWrapper.add(maxPriceFilter, Occur.MUST);
					taxonomyFilterWrapper.add(maxPriceFilter, Occur.MUST);
				} 
				
			} else {
				Filter minPriceFilter = null;
				Filter maxPriceFilter = null;

				if (minPrice != -1.0 && maxPrice != -1.0) {
					minPriceFilter = NumericRangeFilter.newDoubleRange("min_price", minPrice, maxPrice, true, true);
					maxPriceFilter = NumericRangeFilter.newDoubleRange("max_price", minPrice, maxPrice, true, true);

					Filter minLimitPriceFilter = NumericRangeFilter.newDoubleRange("min_price", Double.MIN_VALUE, maxPrice, true, true);
					Filter maxLimitPriceFilter = NumericRangeFilter.newDoubleRange("max_price", minPrice, Double.MAX_VALUE, true, true);

					// convert to array of filters and 
					Filter[] filterArray = new Filter[4];
					filterArray[0] = minPriceFilter;
					filterArray[1] = maxPriceFilter;
					filterArray[2] = minLimitPriceFilter;
					filterArray[3] = maxLimitPriceFilter;
					
					// 0 for OR operator
					// 1 for AND operator
					int[] logicArray = new int[4];
					logicArray[0] = 1;
					logicArray[1] = 0;
					logicArray[2] = 0;
					logicArray[3] = 1;
						
					org.apache.lucene.queries.ChainedFilter chainFilter = new ChainedFilter(filterArray, logicArray);
					if(multipleFilterWrapper == null) {
						multipleFilterWrapper = new BooleanFilter();
						taxonomyFilterWrapper = new BooleanFilter();
					}
					multipleFilterWrapper.add(chainFilter, Occur.MUST);
					taxonomyFilterWrapper.add(chainFilter, Occur.MUST);
					
				}
				if (minPrice != -1.0 && maxPrice == -1.0) {
					maxPriceFilter = NumericRangeFilter.newDoubleRange("max_price", minPrice, Double.MAX_VALUE, true, true);
					if(multipleFilterWrapper == null) {
						multipleFilterWrapper = new BooleanFilter();
						taxonomyFilterWrapper = new BooleanFilter();
					}
					multipleFilterWrapper.add(maxPriceFilter, Occur.MUST);
					taxonomyFilterWrapper.add(maxPriceFilter, Occur.MUST);
					
				}
				if (minPrice == -1.0 && maxPrice != -1.0) {
					minPriceFilter = NumericRangeFilter.newDoubleRange("min_price", Double.MIN_VALUE, maxPrice, true, true);
					if(multipleFilterWrapper == null) {
						multipleFilterWrapper = new BooleanFilter();
						taxonomyFilterWrapper = new BooleanFilter();
					}
					multipleFilterWrapper.add(minPriceFilter, Occur.MUST);
					taxonomyFilterWrapper.add(minPriceFilter, Occur.MUST);
				}
			}
				
			// Minimum Quantity Filter
			Filter minQtyFilter = null;
			int minQty = 0;
			int maxQty = 0;
			if(siteConfig.get("LUCENE_SEARCH_EXACT_MIN_QTY").getValue() != null && siteConfig.get("LUCENE_SEARCH_EXACT_MIN_QTY").getValue().equals("true")) {
				maxQty = ServletRequestUtils.getIntParameter(request, "minQty", 0);
			} else {
				minQty = ServletRequestUtils.getIntParameter(request, "minQty", 0);
				maxQty = ServletRequestUtils.getIntParameter(request, "maxQty", 0);
				
			}
			if(minQty > 0 || maxQty > 0){
				minQtyFilter = NumericRangeFilter.newIntRange("min_quantity", ((minQty <= 0) ? 1: minQty), ((maxQty <= 0) ? Integer.MAX_VALUE: maxQty), true, true);
				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(minQtyFilter, Occur.MUST);
				taxonomyFilterWrapper.add(minQtyFilter, Occur.MUST);
			}
				
			// search rank Filter
			Filter searchRankFilter = null;
			int searchRank = ServletRequestUtils.getIntParameter(request, "searchRank", -1);
			if(searchRank >= 0){
						
				searchRankFilter = NumericRangeFilter.newIntRange("search_rank", 0, searchRank, true, true);
				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(searchRankFilter, Occur.MUST);
				taxonomyFilterWrapper.add(searchRankFilter, Occur.MUST);
			}
							
			
			// range filters on product fields


			List<ProductField> productFieldsForSearch = new ArrayList<ProductField>(); 
			for(ProductField productField : this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"))) {
				if(productField.isIndexForSearch() || productField.isIndexForFilter() || productField.isRangeFilter()) {
					productFieldsForSearch.add(productField);
				}
			}
			for(ProductField productField : productFieldsForSearch) {
				if(productField.isRangeFilter()) {
					
					Filter fieldRangeFilter = null;
					Double minRange = ServletRequestUtils.getDoubleParameter(request, "field_"+productField.getId()+"_min", -1.0);
					Double maxRange = ServletRequestUtils.getDoubleParameter(request, "field_"+productField.getId()+"_max", -1.0);
					if (minRange == -1.0 && maxRange == -1.0) {
						continue;
					} else if (minRange != -1.0 && maxRange != -1.0) {
						fieldRangeFilter = NumericRangeFilter.newDoubleRange(productField.getName(), minRange, maxRange, true, true);
					} else if (minRange != -1.0 && maxRange == -1.0) {
						fieldRangeFilter = NumericRangeFilter.newDoubleRange(productField.getName(), minRange, Double.MAX_VALUE, true, true);
					} else if (minRange == -1.0 && maxRange != -1.0) {
						fieldRangeFilter = NumericRangeFilter.newDoubleRange(productField.getName(), Double.MIN_VALUE, maxRange, true, true);
					}
					if(multipleFilterWrapper == null) {
						multipleFilterWrapper = new BooleanFilter();
						taxonomyFilterWrapper = new BooleanFilter();
					}
					multipleFilterWrapper.add(fieldRangeFilter, Occur.MUST);
					taxonomyFilterWrapper.add(fieldRangeFilter, Occur.MUST);
				}
			}
		
			
			
			// markup pricing
			if(search.getSpecialPricingAvailable() != null){
				TermQuery markUpQuery= new TermQuery(new Term("mark_up_available", ""+search.getSpecialPricingAvailable())); 
				Filter markUpFilter = new QueryWrapperFilter(markUpQuery); 

				TermQuery salesTagQuery= new TermQuery(new Term("active_sales_tag", ""+search.getSpecialPricingAvailable())); 
				Filter salesTagFilter = new QueryWrapperFilter(salesTagQuery); 

				
				
				// convert to array of filters and 
				Filter[] filterArray = new Filter[2];
				filterArray[0] = markUpFilter;
				filterArray[1] = salesTagFilter;
				
				// 0 for OR operator
				// 1 for AND operator
				int[] logicArray = new int[2];
				logicArray[0] = 1;
				logicArray[1] = 0;
					
				org.apache.lucene.queries.ChainedFilter chainFilter = new ChainedFilter(filterArray, logicArray);
				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(chainFilter, Occur.MUST);
				taxonomyFilterWrapper.add(chainFilter, Occur.MUST);
			}

			// sales tag
			if(search.getSalesTagActive() != null){
				TermQuery salesTagQuery= new TermQuery(new Term("active_sales_tag", ""+search.getSalesTagActive())); 
				Filter salesTagFilter = new QueryWrapperFilter(salesTagQuery); 
				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(salesTagFilter, Occur.MUST);
				taxonomyFilterWrapper.add(salesTagFilter, Occur.MUST);
			}
			
			// supplier Account number 
			/*
			 * This block sees that when a supplier is logged in and search for a product, they can search only products belonging to them.
			 */
			if(search.getSupplierAccountNumber() != null){
				/*
				 * Lucene filters doesn't work if term from request contains CAPS 
				 */
				TermQuery suppAccountNumberQuery= new TermQuery(new Term("account_number", search.getSupplierAccountNumber().toLowerCase())); 
				Filter suppAccountNumberFilter = new QueryWrapperFilter(suppAccountNumberQuery); 

				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(suppAccountNumberFilter, Occur.MUST);
				taxonomyFilterWrapper.add(suppAccountNumberFilter, Occur.MUST);
				
			}
			
			
			
			//faceted search filters
			// Filter does not work with String so it needs to be converted to eqvivalent integer value.
			List<String> selectedFilters = null;
			Map<String, List<FilterAttribute>> nameFilterMap = new HashMap<String, List<FilterAttribute>>();
 			for(FilterAttribute filterAttribute : search.getFilters()) {
 				List<FilterAttribute> filterAttrList = null;
 				if(nameFilterMap.containsKey(filterAttribute.getParent())) {
 					filterAttrList = nameFilterMap.get(filterAttribute.getParent());
 				} else {
 					filterAttrList = new ArrayList<FilterAttribute>();
 	 			}
 				filterAttrList.add(filterAttribute);
 				
 				nameFilterMap.put(filterAttribute.getParent(), filterAttrList);
 			}
 			
 			for(String filterName : nameFilterMap.keySet()) {
 				if(selectedFilters == null) {
					selectedFilters = new ArrayList<String>();
				}	
 				String[] valuesArray = new String[nameFilterMap.get(filterName).size()];
 				boolean isCheckboxFilter = false;
 				BooleanFilter tempFiler = new BooleanFilter();
				for(int i=0; i<nameFilterMap.get(filterName).size(); i++) {
 					FilterAttribute filterAttribute = nameFilterMap.get(filterName).get(i);
 	 				valuesArray[i] = this.getEquivalentIntNumber(filterAttribute.getName().trim()).toString();
 					isCheckboxFilter = filterAttribute.isCheckboxFilter();
 				
 					TermQuery filterTermQuery = new TermQuery(new Term(filterName, this.getEquivalentIntNumber(filterAttribute.getName().trim()).toString())); 
 					Filter filterTermFilter = new QueryWrapperFilter(filterTermQuery); 
 					tempFiler.add(filterTermFilter, Occur.SHOULD);
 				}
 				
				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
				}
 				multipleFilterWrapper.add(tempFiler, Occur.MUST);
 				if(!isCheckboxFilter) {
	 				if(taxonomyFilterWrapper == null) {
	 					taxonomyFilterWrapper = new BooleanFilter();
	 				}
 					taxonomyFilterWrapper.add(tempFiler, Occur.MUST);
 	 	 		}
 				
			}
			
 			
			Query query = null;
			TopFieldDocs docs = null;
			try {
				if(search.getKeywords() != null && !search.getKeywords().toString().trim().isEmpty()) {
					query = qp.parse(keywords.toString());
				} else {
					query = new MatchAllDocsQuery();
				}
				query.setBoost(0.5f);
				
				if(search.getSort().equalsIgnoreCase("rlv")) {
					Query adiSpecialQuery = new ViaSpecialQuery(query, search.getKeywords().toString());
					docs = searcher.search(adiSpecialQuery, multipleFilterWrapper, 10000, sort);
				} else {
					docs = searcher.search(query, multipleFilterWrapper, 10000, sort);
				}	
			} catch (ParseException e) {
				model.put("tooManyClauses", true);
			} catch (TooManyClauses e) {
				model.put("tooManyClauses", true);
			}
				
				
			if (docs != null) {
				ScoreDoc[] hits = docs.scoreDocs;
				int count = hits.length;
				int start = (search.getPage() - 1) * search.getPageSize();
				if (start > count) {
					search.setPage(1);
					start = 0;
				}
				int end = start + search.getPageSize();
				if (end > count) {
					end = count;
				}

				int pageCount = count / search.getPageSize();
				if (count % search.getPageSize() > 0) {
					pageCount++;
				}

				model.put("start", start);
				model.put("pageCount", pageCount);
				model.put("count", count);

				
				
				//faceted search filters
				Map<String, List<String>> selectedFilterNameValuesMap = null;
				for(FilterAttribute filterAttribute : search.getFilters()) {
					if(selectedFilterNameValuesMap == null) {
						selectedFilterNameValuesMap = new HashMap<String, List<String>>();
					} 
					
					String fieldName = filterAttribute.getParent();
					List<String> values = selectedFilterNameValuesMap.get(fieldName);
					if(values == null) {
						values = new ArrayList<String>();
					}
					values.add(filterAttribute.getName().trim());
				
					selectedFilterNameValuesMap.put(fieldName, values);
				}
				
				// Multifaceted Search on Product Fields
			//	this.getMultiFacetedSearchOnRegularFields(searcher, count, reader, query, multipleFilterWrapper, taxonomyFilterWrapper, selectedFilterNameValuesMap, model, gSiteConfig, request, siteConfig);
				this.getMultiFacetedSearchOnRegularFields(searcher, count, reader, query, multipleFilterWrapper, taxonomyFilterWrapper, selectedFilterNameValuesMap, nameFilterMap, model, gSiteConfig, request, siteConfig);
				
				
				for (int i = start; i < end; i++) {
					Document doc = searcher.doc(hits[i].doc);
					Product product = this.webJaguar.getProductById(new Integer(doc.get("id")), request);
					if (product != null) {

						if (search.isMasterSku() && product.getMasterSku() != null && !product.getMasterSku().isEmpty()) {
							// Don't add Child product to result
						} else {
							
							if(siteConfig.get("LUCENE_SEARCH_SHOW_INDEXED_PRICE").getValue().equals("true")) {
								
								List<Price> priceList = new ArrayList<Price>();
								Price price1 = new Price();
								try {
									price1.setAmt(Double.parseDouble(doc.get("max_price")));
									
									priceList.add(price1);
								} catch(Exception e) {
									e.printStackTrace();
								}
								
								try {
									if(Double.parseDouble(doc.get("min_price")) != Double.parseDouble(doc.get("max_price"))) {
										Price price2 = new Price();
										price2.setAmt(Double.parseDouble(doc.get("min_price")));
										priceList.add(price2);
									}
								} catch(Exception e) {
									e.printStackTrace();
								}
								product.setPrice(priceList);
							} 
							
							results.add(product);
						}

						if (!showPriceColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) || product.getMsrp() != null)) {
							showPriceColumn = true;
						}
						if (!showQtyColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) && (!product.isLoginRequire() || (product.isLoginRequire() && customer != null)))) {
							showQtyColumn = true;
						}

					}
				}
				model.put("pageEnd", start + results.size());
			}
		}
        model.put("showPriceColumn", showPriceColumn);
		model.put("showQtyColumn", showQtyColumn);
		
		if (siteConfig.get("SEARCH_DISPLAY_MODE").getValue().equals("quick2")) {
			model.put("productFields", this.webJaguar.getProductFields(request, results, false));
		} else {
			model.put("productFields", this.webJaguar.getProductFields(request, results, true));
		}
		
		// bread crumbs
		if (request.getParameter("cid") != null && !request.getParameter("cid").isEmpty()) {
			List<Category> breadCrumb = this.webJaguar.getCategoryTree(ServletRequestUtils.getIntParameter(request, "cid"), true, request);
			model.put("breadCrumbs", breadCrumb);
			model.put("cid", ServletRequestUtils.getIntParameter(request, "cid"));

			String protectedAccess = "0";
			// check if protected feature is enabled

			if (protectedLevels == 0) {
				protectedAccess = Constants.FULL_PROTECTED_ACCESS;
			} else {
				if (customer != null)  {
					protectedAccess = customer.getProtectedAccess();
				} else {
					protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
				}
			}
			Integer categoryId = ServletRequestUtils.getIntParameter(request, "cid", -1);
			Category thisCategory = this.webJaguar.getCategoryById(categoryId, protectedAccess);
			
			Layout productSearchLayout = this.webJaguar.getSystemLayout("productSearch", request.getHeader("host"), request);

			if (thisCategory.getHeadTag() != null && !thisCategory.getHeadTag().equals("")) {
				thisCategory.setHeadTag(thisCategory.getHeadTag().replace("#name#", thisCategory.getName()));
				thisCategory.setHeadTag(thisCategory.getHeadTag().replace("#field1#", (thisCategory.getField1() == null) ? "" : thisCategory.getField1()));
				thisCategory.setHeadTag(thisCategory.getHeadTag().replace("#field2#", (thisCategory.getField2() == null) ? "" : thisCategory.getField2()));
				thisCategory.setHeadTag(thisCategory.getHeadTag().replace("#field3#", (thisCategory.getField3() == null) ? "" : thisCategory.getField3()));
				thisCategory.setHeadTag(thisCategory.getHeadTag().replace("#field4#", (thisCategory.getField4() == null) ? "" : thisCategory.getField4()));
				thisCategory.setHeadTag(thisCategory.getHeadTag().replace("#field5#", (thisCategory.getField5() == null) ? "" : thisCategory.getField5()));
				productSearchLayout.setHeadTag(thisCategory.getHeadTag());
			}
			
			
			productSearchLayout.setHeaderHtml(thisCategory.getHtmlCode());
			// get protected html
			if (protectedLevels > 0) {
				if (!thisCategory.getProtectedLevel().equals("0") && thisCategory.isProtect()) {
					if (thisCategory.getProtectedHtmlCode() != null && thisCategory.getProtectedHtmlCode().trim().length() > 0) {
						productSearchLayout.setHeaderHtml(thisCategory.getProtectedHtmlCode());
					}
				}
			}
			
			productSearchLayout.setFooterHtml(thisCategory.getFooterHtmlCode());
			model.put("productSearchLayout", productSearchLayout);
			
		} else {
			Layout productSearchLayout = this.webJaguar.getSystemLayout("productSearch", request.getHeader("host"), request);
			productSearchLayout.setHideLeftBar((siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) ? true : false);
			model.put("productSearchLayout", productSearchLayout);
		}
		
		Layout layout = (Layout) request.getAttribute("layout");
		
		if(request.getAttribute("lng")!=null && request.getAttribute("lng").equals("es")){
			layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#","Mostrando resultados de " + search.getKeywords()));
		}else{
			layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#","Showing results for " + search.getKeywords()));
		}
		
		model.put("results", results);
		model.put("hideSearchBox", ServletRequestUtils.getBooleanParameter(request, "hideSearchBox", false));
		
		//Add to COmparioson for Gajoodle layout
		if ((Boolean) gSiteConfig.get("gCOMPARISON")) {
			List<Integer> comparisonList = (List) request.getSession().getAttribute("comparisonList");
			Map<Integer, Object> comparisonMap;
			if (comparisonList != null) {
				comparisonMap = new HashMap<Integer, Object>();
				for (Integer id : comparisonList) {
					comparisonMap.put(id, true);
				}
				model.put("comparisonMap", comparisonMap);
			}

			model.put("maxComparisons", new Integer(((Configuration) siteConfig.get("COMPARISON_MAX")).getValue()));
		}
		
		//save search keywords for predictive search
		if(search.getKeywords() != null && !search.getKeywords().contains("_") && search.getKeywords().trim().length() > 2) {
			try {
		//		this.webJaguar.insertKeyword(search.getKeywords());
			} catch (Exception e) { }
		}
		
		// Show add presentation only when salesrep cookie is there.
		if ((Boolean) gSiteConfig.get("gPRESENTATION")) {
			model.put("showAddPresentation", (request.getAttribute("cookieSalesRep") != null && customer == null) ? true : false);
		}
		//System.out.println("***Total Time to search on Lucene Search line 916 "+(new Date().getTime() - d1.getTime()));
		
		Boolean luceneSearch = false;
		if(search!=null && search.getKeywords()!=null){
			model.put("searchKeyword", search.getKeywords());
			model.put("luceneSearch", true);
		}else{
			model.put("searchKeyword", null);
		}
				
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/lucene/search", "model", model);
		} else {
			return new ModelAndView("frontend/lucene/search", "model", model);
		}
	}

	private LuceneProductSearch getProductSearch(HttpServletRequest request, Customer customer) {
		LuceneProductSearch search = (LuceneProductSearch) WebUtils.getSessionAttribute(request, "lProductSearch");
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		if (search == null) {
			search = new LuceneProductSearch();
			search.setSort(siteConfig.get("SEARCH_SORT").getValue());
			search.setPageSize(new Integer(siteConfig.get("NUMBER_PRODUCT_PER_PAGE_SEARCH").getValue()));
			if(request.getSession().getAttribute("catPageSize") != null) {
				search.setPageSize(new Integer(request.getSession().getAttribute("catPageSize").toString()));
			}
			request.getSession().setAttribute("lProductSearch", search);
		}
		// required to set to request directly as frontend jsps are disabled to access session
		// sometimes jsp creates multiple jsessionid
		request.setAttribute("lProductSearch", search);
		search.setPageSize(new Integer(siteConfig.get("NUMBER_PRODUCT_PER_PAGE_SEARCH").getValue()));
		if(request.getSession().getAttribute("catPageSize") != null) {
			search.setPageSize(new Integer(request.getSession().getAttribute("catPageSize").toString()));
		}
		
		if (request.getParameter("keywords") != null) {
			search.setKeywords(ServletRequestUtils.getStringParameter(request, "keywords", "").replace("*", ""));
		}
		
		if (request.getParameter("excludeKeywords") != null) {
			search.setExcludeKeywords(ServletRequestUtils.getStringParameter(request, "excludeKeywords", null));
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);
			}
		}

		// size (For new responsive design, page sizes are kept at 24,49,100)
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 24);
			if (size < 24) {
				search.setPageSize(24);
			} else {
				search.setPageSize(size);
			}
			request.getSession().setAttribute("catPageSize", request.getParameter("size"));
		}

		// sort by
		search.setSort(ServletRequestUtils.getStringParameter(request, "sort", siteConfig.get("SEARCH_SORT").getValue()));
		
		// product fields
		int gPRODUCT_FIELDS = (Integer) ((Map<String, Object>) request.getAttribute("gSiteConfig")).get("gPRODUCT_FIELDS");
		Map<String, ProductField> productFieldMap = new HashMap<String, ProductField>();
		boolean setNewMapping = false;
		for (int i = 1; i <= gPRODUCT_FIELDS; i++) {
			if (request.getParameter("field_" + i) != null) {
				setNewMapping = true;
				ProductField pField = new ProductField(i, request.getParameter("field_" + i));
				productFieldMap.put("field_" + i, pField);
			}
		}
		
		if (setNewMapping) {
			search.setProductFieldMap(productFieldMap);
		}
		if (search.getProductFieldMap() != null) {
			search.setProductField(new ArrayList<ProductField>());
			search.getProductField().addAll(search.getProductFieldMap().values());
		}
		Map<String, String> extraFieldMap = new HashMap<String, String>();
		if (request.getParameter("madeIn") != null) {
			extraFieldMap.put("madeIn", request.getParameter("madeIn"));
		}
		if (request.getParameter("eco") != null) {
			extraFieldMap.put("eco", request.getParameter("eco"));
		}
		search.setExtraFieldMap(extraFieldMap);
		
		// min price
		if (request.getParameter("minPrice") != null) {
			double minPrice = ServletRequestUtils.getDoubleParameter(request, "minPrice", -100.0);
			if (minPrice > -100) {
				search.setMinPrice(minPrice);
			} else {
				search.setMinPrice(null);
			}
		} else {
			search.setMinPrice(null);
		}
		
		// max price
		if (request.getParameter("maxPrice") != null) {
			double maxPrice = ServletRequestUtils.getDoubleParameter(request, "maxPrice", -100.0);
			if (maxPrice > -100) {
				search.setMaxPrice(maxPrice);
			} else {
				search.setMaxPrice(null);
			}
		} else {
			search.setMaxPrice(null);
		}
		
		// min Qty
		if (request.getParameter("minQty") != null) {
			int minQty = ServletRequestUtils.getIntParameter(request, "minQty", -100);
			if (minQty > -100) {
				search.setMinQty(minQty);
			} else {
				search.setMinQty(null);
			}
		} else {
			search.setMinQty(null);
		}
		
		// max Qty
		if (request.getParameter("maxQty") != null) {
			int maxQty = ServletRequestUtils.getIntParameter(request, "maxQty", -100);
			if (maxQty > -100) {
				search.setMaxQty(maxQty);
			} else {
				search.setMaxQty(null);
			}
		} else {
			search.setMaxQty(null);
		}
		
		// supplier account number
		/*
		 * If a supplier is logged in, supplier should be able to search only their products
		 */
		Supplier supplier = null;
		if(customer != null) {
			/*
			 * Only customers who are converted into supplier will gave a supplier Id in the customer table*/
			supplier = this.webJaguar.getSupplierById(customer.getSupplierId());
		}
		if(supplier != null){
			search.setSupplierAccountNumber(supplier.getAccountNumber());
		} else {
			search.setSupplierAccountNumber(ServletRequestUtils.getStringParameter(request, "s", null));
		}
		// search rank
		if (request.getParameter("searchRank") != null) {
			int searchRank = ServletRequestUtils.getIntParameter(request, "searchRank", -1);
			if (searchRank > -1) {
				search.setSearchRank(searchRank);
			} else {
				search.setSearchRank(null);
			}
		}
		
		
		
		// faceted search
		search.getFilters().clear();
		if (ServletRequestUtils.getStringParameters(request, "facetNameValue") != null && ServletRequestUtils.getStringParameters(request, "facetNameValue").length > 0) {
			
			Map<String, ProductField> fieldIdMap = new HashMap<String, ProductField>();
			for(ProductField productField : this.webJaguar.getProductFieldsRanked(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"), null)) {
				if(productField.isIndexForSearch() || productField.isIndexForFilter() || productField.isRangeFilter()) {
					fieldIdMap.put(productField.getName().toLowerCase(), productField);
				}
			}
			
			for(String facetNameValue : ServletRequestUtils.getStringParameters(request, "facetNameValue")) {
				try {
					FilterAttribute filterAttribute = new FilterAttribute();
				    filterAttribute.setParent(facetNameValue.split("_value_")[0]);
					filterAttribute.setParentDisplayName(facetNameValue.split("_value_")[0]);
				    filterAttribute.setName(facetNameValue.split("_value_")[1]);
				    
				    ProductField pField = fieldIdMap.get(facetNameValue.split("_value_")[0].trim().toLowerCase());
				    if(pField != null) {
				    	filterAttribute.setCheckboxFilter(pField.isCheckboxFilter());
				    }
				    
					filterAttribute.setDisplayName(facetNameValue.split("_value_")[1]);
					search.getFilters().add(filterAttribute);
					
				} catch(Exception e) {
					
				}
			}
		}
		
		
		//Range Filters
		this.setRangeFiltersonRegularFields(request, search, gSiteConfig);
		
		
		//active sales tag
		if (request.getParameter("st") != null) {
			search.setSalesTagActive(ServletRequestUtils.getBooleanParameter(request, "st", true));
		} else {
			search.setSalesTagActive(null);
		}
		
		//active special pricing
		if (request.getParameter("sp") != null) {
			search.setSpecialPricingAvailable(ServletRequestUtils.getBooleanParameter(request, "sp", true));
		} else {
			search.setSpecialPricingAvailable(null);
		}

		//no keywords
		if (request.getParameter("noKeywords") != null) {
			search.setNoKeywords(ServletRequestUtils.getBooleanParameter(request, "noKeywords", false));
			search.setKeywords("");
		} else {
			search.setNoKeywords(null);
		}
		
		return search;
	}
	
	private void getMultiFacetedSearchOnRegularFields(IndexSearcher searcher, int searchResultCount, DirectoryReader reader, Query query, Filter multipleFilterWrapper, Filter taxonomyFilterWrapper, Map<String, List<String>> selectedFilterNameValuesMap, Map<String, List<FilterAttribute>> nameFilterMap, Map<String, Object> model, Map<String, Object> gSiteConfig, HttpServletRequest request, Map<String, Configuration> siteConfig) throws IOException{

		TopScoreDocCollector tdc = TopScoreDocCollector.create(1, true);
		TaxonomyReader taxo = new DirectoryTaxonomyReader(FSDirectory.open( new File(getServletContext().getRealPath("/lucene_index/productsTaxoDir" ))));
		
		List<ProductField> productFieldFiltersList = new ArrayList<ProductField>(); 
		Map<String, ProductField> fieldIdMap = new HashMap<String, ProductField>();
		for(ProductField productField : this.webJaguar.getProductFieldsRanked(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"), null)) {
			if(productField.isIndexForSearch() || productField.isIndexForFilter() || productField.isRangeFilter()) {
				productFieldFiltersList.add(productField);
				fieldIdMap.put(productField.getName(), productField);
			}
		}
		
		
		//Format must be #name#_rank_#value# (Category_rank_5, Price Range_rank_3);
		if(!siteConfig.get("LUCENE_FILTERS_RANK").getValue().trim().isEmpty()) {
			String[] filterRankValueArray = siteConfig.get("LUCENE_FILTERS_RANK").getValue().split(",");
			int id = -1;
			for(String filterRankValue : filterRankValueArray) {
				String[] frv = filterRankValue.split("_rank_");
				
				ProductField pf = new ProductField();
				pf.setId(id);
				id = id - 1;
				
				pf.setName(frv[0].trim());
				pf.setRank(Integer.parseInt(frv[1].trim()));
				pf.setIndexForFilter(true);
				pf.setIndexForSearch(true);
				productFieldFiltersList.add(pf);
				fieldIdMap.put(pf.getName(), pf);
			}
		}
		
		
		List<FacetRequest> facetRequests = new ArrayList<FacetRequest>();
		FacetRequest facetRequest = null;
		
		for(ProductField productField : productFieldFiltersList) {
			if(productField.isIndexForFilter()) {
				facetRequest = new CountFacetRequest(new CategoryPath(productField.getName()),100);
				// avoid duplicate fields/faceted request like multiple categories from appearing on the filters
				if(!facetRequests.contains(facetRequest)) {
					facetRequests.add(facetRequest);
				}
			}
		}
		if(!facetRequests.isEmpty()) {
			
			
			FacetSearchParams facetSearchParams = new FacetSearchParams(facetRequests);
			FacetsCollector facetsCollector = FacetsCollector.create(facetSearchParams, reader, taxo);
			try {
				searcher.search(query, multipleFilterWrapper, MultiCollector.wrap(tdc, facetsCollector));
			} catch(Exception e) {
				
			}
			List<FacetResult> res = facetsCollector.getFacetResults();
			
			Map<String, List<FilterAttribute>> filterMap = new HashMap<String, List<FilterAttribute>>();
			List<FilterAttribute> filterAttributes = new ArrayList<FilterAttribute>();
			FilterAttribute filterAttribute = null;
			for (FacetResult fr:res) {
				
				if(fr.getFacetResultNode().subResults.isEmpty()) {
					continue;
				}
				ProductField  pf = fieldIdMap.get(fr.getFacetResultNode().label.toString());
				//continue for checkbox filter as there is a separate function call to add checkbox filter at the end of this function.
				if(pf.isCheckboxFilter()) {
					continue;
				}
				
				if(!pf.isCheckboxFilter() && selectedFilterNameValuesMap != null && selectedFilterNameValuesMap.containsKey(fr.getFacetResultNode().label.toString())) {
					continue;
				}
				
				filterAttribute = new FilterAttribute();
			    filterAttribute.setName(fr.getFacetResultNode().label.toString());
			    filterAttribute.setCheckboxFilter(pf.isCheckboxFilter());
				
			    FilterAttribute subFilterAttribute = null;
			    List<FilterAttribute> subFilterAttributes = new ArrayList<FilterAttribute>();
			    
			    for ( FacetResultNode sr : fr.getFacetResultNode().subResults) {
			    	
			    	// do not show filter with no name 
			    	if(sr.label.components[1].trim().isEmpty()) {
			    		continue;
			    	}
			    	
			    	// do not show filter if count is same as search result count 
			    	// ADI Requirement: If clicking on a refiner would not change the number of products displaying, the refiner should not be displayed.
			    	if(!filterAttribute.isCheckboxFilter() && (new Double(sr.value).intValue() >= searchResultCount)) {
			    		continue;
			    	}
			    	
			    	//for checkbox filter, do not show selected value 
			    	if(filterAttribute.isCheckboxFilter() && selectedFilterNameValuesMap != null 
							&& selectedFilterNameValuesMap.containsKey(fr.getFacetResultNode().label.toString())) {
						List<String> values = selectedFilterNameValuesMap.get(fr.getFacetResultNode().label.toString());
						if(values.contains(sr.label.components[1].trim())) {
							continue;
						}
			    	}
			    	
			    	subFilterAttribute = new FilterAttribute();
				    subFilterAttribute.setName(sr.label.components[1]);
				    subFilterAttribute.setDisplayName(sr.label.components[1]);
				    subFilterAttribute.setResultCount(new Double(sr.value).intValue());
					
				    subFilterAttributes.add(subFilterAttribute);
			    }
				
			    // Do not display filter, if sub filters are empty
			    if(subFilterAttributes.isEmpty()) {
			    	// do not show filter with no name 
			    	continue;
			    }

			    filterAttribute.setSubFilters(subFilterAttributes);
				filterAttribute.setRangeFilter(false);
				filterAttribute.setFilterId(pf.getId());
					
				filterAttribute.setRank(pf.getRank());
					
		    	filterAttributes.add(filterAttribute);
				filterMap.put(null, filterAttributes);
			}
				
			
			filterMap = this.updateFiltersOrderByRank(filterMap, siteConfig);
			
			model.put("filterMap", filterMap);
			model.put("filterAttributes", filterAttributes);
			
			this.getMultiFacetedSearchOnRegularFieldsWithMultipleSelection(filterAttributes, filterMap, fieldIdMap, productFieldFiltersList, taxo, searcher, reader, query, taxonomyFilterWrapper, selectedFilterNameValuesMap, nameFilterMap, model, siteConfig);
		//	this.getMultiFacetedSearchOnRegularFieldsWithMultipleSelection(filterAttributes, filterMap, fieldIdMap, productFieldFiltersList, taxo, searcher, reader, query, taxonomyFilterWrapper, selectedFilterNameValuesMap, model, siteConfig);
		}
	}

	private void getMultiFacetedSearchOnRegularFieldsWithMultipleSelection(List<FilterAttribute> filterAttributes, Map<String, List<FilterAttribute>> filterMap, Map<String, ProductField> fieldIdMap, List<ProductField> productFieldFiltersList, TaxonomyReader taxo, IndexSearcher searcher, DirectoryReader reader, Query query, Filter taxonomyFilterWrapper, Map<String, List<String>> selectedFilterNameValuesMap, Map<String, List<FilterAttribute>> nameFilterMap2, Map<String, Object> model, Map<String, Configuration> siteConfig) throws IOException{

		TopScoreDocCollector tdc = TopScoreDocCollector.create(1, true);
		//Format must be #name#_rank_#value# (Category_rank_5, Price Range_rank_3);
		if(!siteConfig.get("LUCENE_FILTERS_RANK").getValue().trim().isEmpty()) {
			String[] filterRankValueArray = siteConfig.get("LUCENE_FILTERS_RANK").getValue().split(",");
			int id = -1;
			for(String filterRankValue : filterRankValueArray) {
				String[] frv = filterRankValue.split("_rank_");
				
				ProductField pf = new ProductField();
				pf.setId(id);
				id = id - 1;
				
				pf.setName(frv[0].trim());
				pf.setRank(Integer.parseInt(frv[1].trim()));
				pf.setIndexForFilter(true);
				pf.setIndexForSearch(true);
				productFieldFiltersList.add(pf);
				fieldIdMap.put(pf.getName(), pf);
			}
		}
		
		Set<String> checkBoxFiltersList = new HashSet<String>();
		for(ProductField productField : productFieldFiltersList) {
			if(productField.isIndexForFilter() && productField.isCheckboxFilter()) {
				checkBoxFiltersList.add(productField.getName());
			}
		}
		for(String checkBoxFilter : checkBoxFiltersList) {
			FacetRequest facetRequest = new CountFacetRequest(new CategoryPath(checkBoxFilter),100);
			List<FacetRequest> facetRequests = new ArrayList<FacetRequest>();
			facetRequests.add(facetRequest);
			
			BooleanFilter tempTaxoFilter = null;
			if(taxonomyFilterWrapper != null) {
				for(FilterClause fc : ((BooleanFilter) taxonomyFilterWrapper).clauses()) {
					if(!fc.toString().contains(checkBoxFilter)) {
						if(tempTaxoFilter == null) {
	 						tempTaxoFilter = new BooleanFilter();
	 					}
						tempTaxoFilter.add(fc);
					}
				}
			}
			
			
			
			for(String checkBoxFilter2 : checkBoxFiltersList) {
				if(checkBoxFilter2.equalsIgnoreCase(checkBoxFilter)) {
 					continue;
 				}	
				BooleanFilter tempFiler = new BooleanFilter();
 				boolean addedNewFilter = false;
 				if(selectedFilterNameValuesMap != null) {
 					for(String key : selectedFilterNameValuesMap.keySet()) {
 	 					if(key.equalsIgnoreCase(checkBoxFilter)) {
 	 						continue;
 	 					}
 	 					List<String> selectedValues = selectedFilterNameValuesMap.get(key);
 	 					for(String value : selectedValues) {
 	 						TermQuery filterTermQuery = new TermQuery(new Term(key, this.getEquivalentIntNumber(value).toString())); 
 	 	 					Filter filterTermFilter = new QueryWrapperFilter(filterTermQuery); 
 	 	 					tempFiler.add(filterTermFilter, Occur.SHOULD);
 	 	 					addedNewFilter = true;
 	 	 				}
 	 				} 
 	 			}
 				if(addedNewFilter) {
 					if(tempTaxoFilter == null) {
 						tempTaxoFilter = new BooleanFilter();
 					}
 					tempTaxoFilter.add(tempFiler, Occur.MUST);
 		 	 	}
 			}
			
			FacetSearchParams facetSearchParams = new FacetSearchParams(facetRequests);
			FacetsCollector facetsCollector = FacetsCollector.create(facetSearchParams, reader, taxo);
			try {
				searcher.search(query, tempTaxoFilter, MultiCollector.wrap(tdc, facetsCollector));
			} catch(Exception e) {
				
			}
			List<FacetResult> res = facetsCollector.getFacetResults();
			
			for (FacetResult fr:res) {
				
				if(fr.getFacetResultNode().subResults.isEmpty()) {
					continue;
				}
				ProductField  pf = fieldIdMap.get(fr.getFacetResultNode().label.toString());
				
				if(!pf.isCheckboxFilter()) {
					continue;
				}
				FilterAttribute filterAttribute = new FilterAttribute();
			    filterAttribute.setName(fr.getFacetResultNode().label.toString());
			    filterAttribute.setCheckboxFilter(pf.isCheckboxFilter());
				
			    FilterAttribute subFilterAttribute = null;
			    List<FilterAttribute> subFilterAttributes = new ArrayList<FilterAttribute>();
			    
				filterAttribute.setCheckboxFilter(pf.isCheckboxFilter());
				
			    
			    for ( FacetResultNode sr : fr.getFacetResultNode().subResults) {
			    	
			    	// do not show filter with no name 
			    	if(sr.label.components[1].trim().isEmpty()) {
			    		continue;
			    	}
			    	
			    	subFilterAttribute = new FilterAttribute();
				    if(filterAttribute.isCheckboxFilter() && selectedFilterNameValuesMap != null 
							&& selectedFilterNameValuesMap.containsKey(fr.getFacetResultNode().label.toString())) {
						List<String> values = selectedFilterNameValuesMap.get(fr.getFacetResultNode().label.toString());
						if(values.contains(sr.label.components[1].trim())) {
							subFilterAttribute.setSelected(true);
						}
			    	}
			    	
			    	subFilterAttribute.setName(sr.label.components[1]);
				    subFilterAttribute.setDisplayName(sr.label.components[1]);
				    subFilterAttribute.setResultCount(new Double(sr.value).intValue());
					
				    subFilterAttributes.add(subFilterAttribute);
			    }
				
			    if(!subFilterAttributes.isEmpty()) {
			    	filterAttribute.setSubFilters(subFilterAttributes);
			    	filterAttributes.add(filterAttribute);
			    	filterMap.put(null, filterAttributes);
				}
			}
				
		}
		filterMap = this.updateFiltersOrderByRank(filterMap, siteConfig);
		model.put("filterMap", filterMap);
		model.put("filterAttributes", filterAttributes);
	

	}

	
	private void setRangeFiltersonRegularFields(HttpServletRequest request, LuceneProductSearch search, Map<String, Object> gSiteConfig){
		List<ProductField> productFieldsForSearch = new ArrayList<ProductField>(); 
		for(ProductField productField : this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"))) {
			if(productField.isIndexForSearch() || productField.isIndexForFilter() || productField.isRangeFilter()) {
				productFieldsForSearch.add(productField);
			}
		}
		for(ProductField productField : productFieldsForSearch) {
			if(productField.isRangeFilter()) {
				// min range
				
				if (request.getParameter("field_"+productField.getId()+"_min") != null || request.getParameter("field_"+productField.getId()+"_min") != null) {
					if(search.getRangeFilters() == null) {
						search.setRangeFilters(new HashMap<String, List<FilterAttribute>>());
					}
					
					List<FilterAttribute> filterAttributeList = new ArrayList<FilterAttribute>();
					
					FilterAttribute minRangeAttribute = new FilterAttribute();
					minRangeAttribute.setName("field_"+productField.getId()+"_min");
					minRangeAttribute.setValue(request.getParameter("field_"+productField.getId()+"_min"));
					minRangeAttribute.setFilterId(productField.getId());
					filterAttributeList.add(minRangeAttribute);
					
					FilterAttribute maxRangeAttribute = new FilterAttribute();
					maxRangeAttribute.setName("field_"+productField.getId()+"_max");
					maxRangeAttribute.setValue(request.getParameter("field_"+productField.getId()+"_max"));
					maxRangeAttribute.setFilterId(productField.getId());
					filterAttributeList.add(maxRangeAttribute);
					
					search.getRangeFilters().put("field_"+productField.getId(), filterAttributeList);
				}
			}
		}
		
	}

	public Integer getEquivalentIntNumber(String fieldValue) {
		final int prime = 31;
		Integer result = 1;
		for(char c : fieldValue.toCharArray()) {
			result = prime * result + Character.getNumericValue(c);
		}
		return  Math.abs(result);
	}
	
	public Map<String, List<FilterAttribute>> updateFiltersOrderByRank(Map<String, List<FilterAttribute>> filterMap, Map<String, Configuration> siteConfig) {
		
		List<RangeValue> ranges = this.webJaguar.getRangeValues(null);
		//List<RangeValue> ranges = null;
		Map<String, Integer> rvMap = null;
		if(ranges != null && !ranges.isEmpty()) {
			rvMap = new HashMap<String, Integer>();
			for(RangeValue rv : ranges) {
				rvMap.put(rv.getRangeDisplayValue(), rv.getId());
			}
		}
		
		List<FilterAttribute> tempList = new ArrayList<FilterAttribute>();
		Map<String, String> fieldGroupMap = new HashMap<String, String>();
		for(String key : filterMap.keySet()) {
			
			List<FilterAttribute> faList = filterMap.get(key);
			Collections.sort(faList);
			filterMap.put(key, faList);
			
			for(FilterAttribute faAttr : faList) {
				if(rvMap != null && faAttr.getName().equalsIgnoreCase("Price Range")) {
					if(faAttr.getSubFilters() != null) {
						for(FilterAttribute tempAttr : faAttr.getSubFilters()) {
							tempAttr.setRank(rvMap.get(tempAttr.getDisplayName()));
						}
					}
				}
				
				if(rvMap != null && faAttr.getName().equalsIgnoreCase("Reward Points Range")) {
					if(faAttr.getSubFilters() != null) {
						for(FilterAttribute tempAttr : faAttr.getSubFilters()) {
							tempAttr.setRank(rvMap.get(tempAttr.getDisplayName()));
						}
					}
				}
				
				if(faAttr.getSubFilters() != null) {
					// for domains other than bnoticed, show top 20 filters by result count and sort it alpabatically
					// if bntoiced agree on this behavior, remove this if condition.
					if(!(siteConfig.get("SITE_URL").getValue().contains("bnoticed") || siteConfig.get("SITE_URL").getValue().contains("test300"))) {
						if(faAttr.getSubFilters().size() > 20) {
							for(int i=20; i<faAttr.getSubFilters().size(); i++) {
								faAttr.getSubFilters().remove(i);
							}
						}
						Collections.sort(faAttr.getSubFilters());
					}
				}
			}
			
			tempList.addAll(filterMap.get(key));
			
			for(FilterAttribute attr : filterMap.get(key)) {
				fieldGroupMap.put(attr.getName(), key);
			}
		}
		
		Collections.sort(tempList);
		
		Map<String, List<FilterAttribute>> tempFilterMap = new LinkedHashMap<String, List<FilterAttribute>>();
		for(FilterAttribute attr : tempList) {
			if(!tempFilterMap.containsKey(fieldGroupMap.get(attr.getName()))) {
				tempFilterMap.put(fieldGroupMap.get(attr.getName()), filterMap.get(fieldGroupMap.get(attr.getName())));
			}
		}
		
		return tempFilterMap;
	}
	
}
