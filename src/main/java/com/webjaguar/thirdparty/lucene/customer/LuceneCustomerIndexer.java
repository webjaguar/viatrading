package com.webjaguar.thirdparty.lucene.customer;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CustomerSearch;

public class LuceneCustomerIndexer extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	Map<String, Configuration> siteConfig;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("results", index());
		
		return new ModelAndView("admin/customers/reIndex", map);
	}
		
	public String index() throws IOException {
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		CustomerSearch search = new CustomerSearch();
		search.setEmail("");
		int customerCount = this.webJaguar.getLuceneCustomerListCount(search);
		sbuff.append(customerCount + "\n");
		
		int limit = 20000;
		
		File baseDir = new File(getServletContext().getRealPath("/lucene_index/"));
		if (!baseDir.exists()) {
			baseDir.mkdir();			
		}
		
		baseDir = new File(baseDir, "customerIndexing");
		baseDir.mkdir();			
		
		File tempIndexDir = new File(baseDir, "tempCustomers");
		tempIndexDir.mkdir();			
		
	    Directory fsDir = FSDirectory.open(tempIndexDir);
	    
	    Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_CURRENT, analyzer);
		IndexWriter writer = new IndexWriter(fsDir, config);
		
		for (int offset=0; offset<customerCount;) {
			
			if (offset > 0) sbuff.append(", ");
			sbuff.append(offset);	
			/*
			Map<String, Object> customer = (Map<String, Object>) this.webJaguar.getLuceneCustomer(offset, limit);
			System.out.println("username: " + customer.get("username"));
			*/
			List<Integer> ids = new ArrayList<Integer>();
			List<Integer> updateIds = new ArrayList<Integer>();
			
			for (Map<String, Object> customer: this.webJaguar.getLuceneCustomer(offset, limit, null)) {
				
				Document doc = new Document();
				/*
				// id has to be added on SiteConfig Search Field.
				doc.add(new Field("id", customer.get("id").toString(), TextField.TYPE_STORED));
				
				//username
				doc.add(new Field("username", customer.get("username") != null ? customer.get("username").toString() : "", TextField.TYPE_STORED));
				
				//first_name
				doc.add(new Field("first_name", customer.get("first_name") != null ? customer.get("first_name").toString() : "", TextField.TYPE_STORED));
				
				//last_name
				doc.add(new Field("last_name", customer.get("last_name") != null ? customer.get("last_name").toString() : "", TextField.TYPE_STORED));
				
				//company
				doc.add(new Field("company", customer.get("company") != null ? customer.get("company").toString() : "", TextField.TYPE_STORED));
				*/
				StringBuilder allData = null;

				//Customer
				if (customer.get("username") != null) {
					if(!ids.contains(Integer.parseInt(customer.get("id").toString()))) {
						//System.out.println("CUSTOMER: " +  customer.get("id").toString());
						ids.add(Integer.parseInt(customer.get("id").toString()));
						allData = new StringBuilder();
						//System.out.println("TextField: " + TextField.TYPE_STORED);
						doc.add(new Field("id", customer.get("id").toString(), TextField.TYPE_STORED));
						allData.append(customer.get("id").toString());
						allData.append(" "+ (customer.get("username").toString()));
						allData.append(" "+ (customer.get("first_name") != null ? customer.get("first_name").toString() : ""));
						allData.append(" "+ (customer.get("last_name") != null ? customer.get("last_name").toString() : ""));
						allData.append(" "+ (customer.get("company") != null ? customer.get("company").toString() : ""));
					
						//all fields data
						if(allData != null) {
							doc.add(new Field("all_fields_data", allData.toString(), TextField.TYPE_STORED));
						}
						writer.addDocument(doc);
					} else if(!updateIds.contains(Integer.parseInt(customer.get("id").toString()))) {
						updateIds.add(Integer.parseInt(customer.get("id").toString()));
						//System.out.println("CUSTOMER-UPDATE******: " +  customer.get("id").toString());
						doc.add(new Field("id", customer.get("id").toString(), TextField.TYPE_STORED));
						allData = new StringBuilder();
						allData.append(customer.get("id").toString());
						allData.append(" "+customer.get("username").toString());
						for(Map<String, Object> updateCustomer : this.webJaguar.getLuceneCustomer(0, 0, Integer.parseInt(customer.get("id").toString()))) {
							allData.append(" "+ (updateCustomer.get("first_name") != null ? updateCustomer.get("first_name").toString() : ""));
							allData.append(" "+ (updateCustomer.get("last_name") != null ? updateCustomer.get("last_name").toString() : ""));
							allData.append(" "+ (updateCustomer.get("company") != null ? updateCustomer.get("company").toString() : ""));
						}
						//all fields data
						if(allData != null) {
							doc.add(new Field("all_fields_data", allData.toString(), TextField.TYPE_STORED));
						}
						Term term = new Term("id", customer.get("id").toString());
						writer.updateDocument(term, doc, analyzer);
					}
				}
			}
			offset = offset + limit;
		}
		Date end = new Date();
		sbuff.append("\n");
		sbuff.append("Start on " + start + "\n");
		sbuff.append("End on " + end + "\n");
		sbuff.append("Indexed a total of " + writer.maxDoc() + "\n");
	    sbuff.append(end.getTime() - start.getTime() + " total milliseconds" + "\n");
	    sbuff.append("---------------" + "\n");

	    writer.close();
	    
	    File indexDir = new File(baseDir, "customers");
		if (indexDir.exists()) {
			if(indexDir.isDirectory()) {
				for(String fileName : indexDir.list()) {
					new File(indexDir, fileName).delete();
				}
			}
		}
		indexDir.mkdir();			
		// copy index files from temp to products
		FileUtils.copyDirectory(tempIndexDir, indexDir);
		
		if (tempIndexDir.exists()) {
			if(tempIndexDir.isDirectory()) {
				for(String fileName : tempIndexDir.list()) {
					new File(tempIndexDir, fileName).delete();
				}
			}
		}
		
		return sbuff.toString();
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}

}
