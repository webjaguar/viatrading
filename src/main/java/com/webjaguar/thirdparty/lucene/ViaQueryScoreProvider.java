package com.webjaguar.thirdparty.lucene;

import java.io.IOException;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.index.DocsEnum;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queries.CustomScoreProvider;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.search.Query;
import org.apache.lucene.util.BytesRef;

public class ViaQueryScoreProvider extends CustomScoreProvider {

	Query query;
	String keywords;
	
    public ViaQueryScoreProvider(AtomicReaderContext context, Query query, String keywords) {
        super(context);
        this.query = query;
        this.keywords = keywords;
    }

    public float customScore(int doc, float subQueryScore, float valSrcScores[]) throws IOException {
    	// consult context 
    	IndexReader r = context.reader();
    	Document tempDoc = r.document(doc);
    	
    	BytesRef term = new BytesRef(keywords.getBytes());
	    
    	Terms skuTerms = r.getTermVector(doc, "sku"); //get terms vectors for one document and one field
    	if (skuTerms != null && skuTerms.size() > 0) {
    		
    		TermsEnum termsEnum = skuTerms.iterator(null); // access the terms for this field
    	    boolean isExist = termsEnum.seekExact(term, true);
    	    if(isExist) {
    	    	return 9.0f;
	     	}
    	    if(tempDoc.get("sku") != null) {
    	    	for(String field : tempDoc.get("sku").toString().split(" ")) {
        	    	if(field.equalsIgnoreCase(keywords)) {
        	    		return 9.0f;
        	    	}
        	    }
    	    	String skuField = tempDoc.get("sku").toString().replace("-", "");
    	    	for(String field : skuField.split(" ")) {
        	    	if(field.equalsIgnoreCase(keywords)) {
        	    		return 9.0f;
        	    	}
        	    }
    	    }
    	}
    	
    	if(tempDoc.get("sku").contains(keywords)) {
    		return 8.5f;
     	}
    	
    	
    	Terms nameTerms = r.getTermVector(doc, "name"); //get terms vectors for one document and one field
    	if (nameTerms != null && nameTerms.size() > 0) {
    		TermsEnum termsEnum = nameTerms.iterator(null); // access the terms for this field
    	    boolean isExist = termsEnum.seekExact(term, true);
    	    if(isExist) {
    	    	return 8.0f;
	     	}
    	}
    	
    	Terms allFieldsTerms = r.getTermVector(doc, "all_fields_data"); //get terms vectors for one document and one field
    	if (allFieldsTerms != null && allFieldsTerms.size() > 0) {
    		TermsEnum termsEnum = allFieldsTerms.iterator(null); // access the terms for this field
    	    boolean isExist = termsEnum.seekExact(term, false);
    	    if(isExist) {
    	    	return 7.5f;
	     	}
    	}
    	
    	if(tempDoc.get("name").contains(keywords)) {
    		return 7.0f;
     	}
    	
    	return 6.5f;
 		
       
 	}
}