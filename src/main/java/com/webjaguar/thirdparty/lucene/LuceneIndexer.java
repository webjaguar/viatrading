/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.16.2009
 */

package com.webjaguar.thirdparty.lucene;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.DoubleField;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.IntField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.facet.index.FacetFields;
import org.apache.lucene.facet.taxonomy.CategoryPath;
import org.apache.lucene.facet.taxonomy.TaxonomyWriter;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyWriter;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.search.suggest.analyzing.AnalyzingSuggester;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.apache.poi.util.IntegerField;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CustomerGroup;
import com.webjaguar.model.Price;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.model.RangeValue;
import com.webjaguar.model.Supplier;
import com.webjaguar.web.domain.KeyBean;

public class LuceneIndexer extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	public MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	Map<String, Configuration> siteConfig;
	
	private static boolean indexingInProgress = false;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> map = new HashMap<String, Object>();
		
		if (!siteConfig.get("LUCENE_REINDEX").getValue().equals("true") || siteConfig.get("SEARCH_FIELDS").getValue().isEmpty()) {
			// check if enabled
			return null;
		}
		
		if(indexingInProgress) {
			map.put("results", "Indexing is currently in progress. Please try again later.");
			return new ModelAndView("admin/catalog/product/reIndex", map);
		} 
		
		indexingInProgress = true;
		
		try {
			if (request.getParameter("now") != null) {
				Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
				
				map.put("results", reIndex(gSiteConfig, siteConfig, webJaguar));
				
				//update category(if any) and site config with current time
				
				DateFormat df = new SimpleDateFormat("MM/dd 'at' HH:mm");
			    String date = df.format(new Date());
			    this.webJaguar.updateSiteConfig("LUCENE_LAST_INDEX_DATE", date);
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
		indexingInProgress = false;
		return new ModelAndView("admin/catalog/product/reIndex", map);
	}
	
	public String reIndex(Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig,  WebJaguarFacade webJaguar) throws Exception {
		StringBuffer sbuff = new StringBuffer();
		String searchFields = siteConfig.get("SEARCH_FIELDS").getValue();
		Date start = new Date();
		this.webJaguar = webJaguar;
		
		Date d1 = new Date();
		//System.out.println("Start Indexing");
		
		ProductSearch search = new ProductSearch();
		
		// Don't show/index children product
		boolean onlyParent = false;
		
		// Map of product and category association 
		Map<Integer, String> productCategoryAssociationMap = new HashMap<Integer, String>();
		if(siteConfig.get("INDEX_CATIDS_ON_LUCENE_PRODUCT").getValue().equalsIgnoreCase("true")) {
			Map<Integer, String> productIdActualCategoryIdsMap = this.webJaguar.getProductIdCategoryIdsMap();
			for(Integer productId : productIdActualCategoryIdsMap.keySet()) {
				if(productIdActualCategoryIdsMap.get(productId) != null && !productIdActualCategoryIdsMap.get(productId).trim().isEmpty()) {
					String catIds = productIdActualCategoryIdsMap.get(productId).replace(",", " ");
					productCategoryAssociationMap.put(productId, catIds);
				}
			}
		}
		
		// Map of product and category association 
		int productCount = this.webJaguar.getLuceneProductCount(false);
		//System.out.println("Count "+productCount);
		//System.out.println("Line 175 "+(new Date().getTime() - d1.getTime()));
		sbuff.append(productCount + "\n");
		int limit = 20000;
		search.setLimit(limit);
		
	    File baseDir = new File(getServletContext().getRealPath("/lucene_index/"));
		if (!baseDir.exists()) {
			baseDir.mkdir();			
		}
		
		
		File tempIndexDir = new File(baseDir, "tempProducts");
		tempIndexDir.mkdir();
		if(tempIndexDir != null) {
			Directory fsDir = FSDirectory.open(tempIndexDir);
			
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_CURRENT, analyzer);
			
			if(IndexWriter.isLocked(fsDir)){
				IndexWriter.unlock(fsDir);
			}
			IndexWriter writer = new IndexWriter(fsDir, config);
			
			// for multifaceted search
			File taxoIndexDir = new File(baseDir, "productsTaxoDir");
			taxoIndexDir.mkdir();
			Directory taxoDir = FSDirectory.open(taxoIndexDir);
			TaxonomyWriter taxo = new DirectoryTaxonomyWriter(taxoDir, OpenMode.CREATE);
			FacetFields ff = new FacetFields(taxo);
			
			
			Set<String> searchFieldsSet = new HashSet<String>();
			// put values in set to remove duplicate elements.
			for(String field : searchFields.split(",")) {
				searchFieldsSet.add(field);
			}
			
			
			// get regular fields if it is enabled for indexing or search or range filter
			List<ProductField> productFieldsForSearch = new ArrayList<ProductField>(); 
			for(ProductField productField : this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"))) {
				if(productField.isIndexForSearch() || productField.isIndexForFilter() || productField.isRangeFilter()) {
					productFieldsForSearch.add(productField);
					searchFieldsSet.add("field_"+productField.getId());
				}
			}
			
			String[] searchFieldsList = new String[searchFieldsSet.size()];
			int i = 0;
			for(String field : searchFieldsSet) {
				searchFieldsList[i] = field;
				i++;
			}
			for(String field : searchFieldsList) {
				//System.out.println("Filed "+field);
			}
			
			// just for premier site to append t-shirts, if t-shirt is available.
			Pattern pattern = null;
			Matcher matcher = null;
			
			List<RangeValue> ranges = this.webJaguar.getRangeValues(null);
			
			for (int offset=0; offset<productCount;) {
				//System.out.println("offset "+offset);
				if (offset > 0) sbuff.append(", ");
				sbuff.append(offset);				
				List<Map<String, Object>> products = null;
				try {
					products = this.webJaguar.getLuceneProduct(limit, offset, onlyParent, null, searchFieldsList, false);
					
					if(siteConfig.get("INDEX_CATIDS_ON_LUCENE_PRODUCT").getValue().equalsIgnoreCase("true") && productCategoryAssociationMap != null && !productCategoryAssociationMap.isEmpty()) { 
						for (Map<String, Object> product: products) {
							product.put("catIds", productCategoryAssociationMap.get(Integer.parseInt(product.get("id").toString())));
						}
					}	
				} catch (Exception e){
					offset = offset + limit;
			        search.setOffset(offset);
			        
					e.printStackTrace();
				    continue;
				}
				for (Map<String, Object> product: products) {
					// Get child skus and index it with parent sku
					// sometimes, there are 2 levels of parent-children-children
					// In 2 levels of children, get last level of children and index it with parent products
					List<Map<String, Object>> childProducts = new ArrayList<Map<String,Object>>();
					List<Map<String, Object>> level1ChildProducts = null;
					if ((Boolean) gSiteConfig.get("gMASTER_SKU")) {
						level1ChildProducts = this.webJaguar.getLuceneProduct(100, 0, false, product.get("sku").toString(), searchFieldsList, false);
						childProducts.addAll(level1ChildProducts);
						for(Map<String, Object> level1ChildProduct: level1ChildProducts) {
							List<Map<String, Object>> level2ChildProducts = this.webJaguar.getLuceneProduct(100, 0, false, level1ChildProduct.get("sku").toString(), searchFieldsList, false);
							if(level2ChildProducts != null) {
								childProducts.addAll(level2ChildProducts);
							}
						}
					}
					Document doc = new Document();
					
					FieldType type = new FieldType();
					type.setIndexed(true);
					type.setStored(true);
					type.setStoreTermVectors(true);

					// id has to be added on SiteConfig Search Field.
					doc.add(new Field("id", product.get("id").toString(), TextField.TYPE_STORED));
					
					if(product.get("name") != null) {
						//this field must be anaylzed and stored as vector as higher boost can be given at the time of search
						doc.add(new Field("name", product.get("name").toString(), type));
						//sort by name, need to store as String field.
						doc.add(new StringField("nameSortField", product.get("name").toString(), Field.Store.YES));
					}
					if(product.get("sku") != null) {
						doc.add(new Field("sku", product.get("sku").toString(), type));
					}
					if(product.get("i18n_name") != null) {
						//System.out.println("lucene i18n_name------------------------->" + product.get("i18n_name").toString());
						doc.add(new Field("i18n_name", product.get("i18n_name").toString(), type));
					}
					// search fields
					StringBuilder sbAll = null;
					StringBuilder predictiveSearchData = new StringBuilder();
					if (siteConfig.get("LUCENE_GROUP_FIELDS_INTO_ONE").getValue().equals("true")) {
						sbAll = new StringBuilder();
						
						// Do not index id, search rank, price, minimum qty as it is used for filters and not for search.
						// Fields are indexed separately in other functions so do not add it here to avoid duplicate content on the index.
						for(String field : searchFieldsList) {
							if( !field.isEmpty() 
									&& !field.equalsIgnoreCase("id") 
									&& !field.equalsIgnoreCase("search_rank") 
									&& !field.contains("price")  
									&& !field.equalsIgnoreCase("minimum_qty") 
									&& !field.contains("field_")) {
								
								// for parent product
								if(product.get(field) != null && !sbAll.toString().contains(product.get(field).toString())) {
									sbAll.append(product.get(field) != null ? " "+product.get(field).toString() : " ");
								}
								// for skus with dashes, add string without dashes.
								if(field.equalsIgnoreCase("sku") && product.get(field) != null) {
									String skuWithoutDashes = product.get(field).toString().replace("-", "");
									if(!sbAll.toString().contains(skuWithoutDashes)) {
										sbAll.append(" "+skuWithoutDashes);
									}
								}
								
								// for predictve search
								if( !field.isEmpty() && (field.equalsIgnoreCase("name") || field.equalsIgnoreCase("sku") || field.equalsIgnoreCase("manufacture_name") )){
									predictiveSearchData.append(product.get(field) != null ? " "+product.get(field).toString() : " ");
								}
								
								//append data for child products, excluding search rank and prices
								if(childProducts != null) {
									for(Map<String, Object> childProduct: childProducts) {
										if(childProduct.get(field) != null && !sbAll.toString().contains(childProduct.get(field).toString())) {
											sbAll.append(" "+childProduct.get(field).toString());
										}
									
									
										// for predictve search
										if( !field.isEmpty() && (field.equalsIgnoreCase("name") || field.equalsIgnoreCase("sku") || field.equalsIgnoreCase("manufacture_name") )){
											predictiveSearchData.append(childProduct.get(field) != null ? " "+childProduct.get(field).toString() : " ");
										}
									}
								}
							}
						}
						if (pattern != null && siteConfig.get("PREMIER_DISTRIBUTOR").getValue().equals("true")) {
							matcher = pattern.matcher(sbAll.toString());
							if (matcher.matches()) {
								sbAll.append(" t-shirts");
							}
						}
					} else {
						// Do not index id, search rank, price, minimum qty as it is used for filters and not for search.
						// Fields are indexed separately in other functions so do not add it here to avoid duplicate content on the index.
						for(String field : searchFieldsList) {
							if( !field.isEmpty() 
									&& !field.equalsIgnoreCase("id") 
									&& !field.equalsIgnoreCase("search_rank") 
									&& !field.contains("price")  
									&& !field.equalsIgnoreCase("minimum_qty") 
									&& !field.contains("field_")) {
							
								StringBuilder sb = new StringBuilder();
								
								// for parent product
								sb.append(product.get(field) != null ? product.get(field).toString() : "");
								
								// for predictve search
								if( !field.isEmpty() && (field.equalsIgnoreCase("name") || field.equalsIgnoreCase("sku") || field.equalsIgnoreCase("manufacture_name"))){
									predictiveSearchData.append(product.get(field) != null ? " "+product.get(field).toString() : " ");
								}
								
								//append data for child products, excluding search rank and prices
								if(childProducts != null) {
									for(Map<String, Object> childProduct: childProducts) {
										sb.append(childProduct.get(field) != null ? " "+childProduct.get(field).toString() : "");
									
										// for predictve search
										if( !field.isEmpty() && (field.equalsIgnoreCase("name") || field.equalsIgnoreCase("sku") || field.equalsIgnoreCase("manufacture_name"))){
											predictiveSearchData.append(childProduct.get(field) != null ? " "+childProduct.get(field).toString() : " ");
										}
									}
								}
								doc.add(new Field(field, sb.toString(), TextField.TYPE_NOT_STORED));
							}
						}
					}
					
					//search rank
					doc.add(new IntField("search_rank", Integer.parseInt(product.get("search_rank") != null ? product.get("search_rank").toString() : "100"), IntField.TYPE_STORED));
					
					// child product skus
					
					
					// min and max price
					Double minPrice = Double.parseDouble(product.get("min_price") != null ? product.get("min_price").toString() : "0.0");
					Double maxPrice = Double.parseDouble(product.get("max_price") != null ? product.get("max_price").toString() : "0.0");
					
					// min qty
					Integer minimumQuantity = Integer.parseInt(product.get("minimum_qty") != null ? product.get("minimum_qty").toString() : "-1");
					// supplier number
					String supplierAccountNumber = product.get("account_number") != null ? product.get("account_number").toString() : "";
					if(childProducts != null) {
						
						for(Map<String, Object> childProduct: childProducts) {
							Double cMinPrice = Double.parseDouble(childProduct.get("min_price") != null ? childProduct.get("min_price").toString() : "0.0");
							Double cMaxPrice = Double.parseDouble(childProduct.get("max_price") != null ? childProduct.get("max_price").toString() : "0.0");
							int cMinimumQuantity = Integer.parseInt(childProduct.get("minimum_qty") != null ? childProduct.get("minimum_qty").toString() : "-1");
							
							minPrice = cMinPrice < minPrice  ?  cMinPrice : minPrice;
							maxPrice = cMaxPrice > maxPrice  ?  cMaxPrice : maxPrice;
							
							minimumQuantity = cMinimumQuantity < minimumQuantity  ?  cMinimumQuantity : minimumQuantity;
						}
					}
					
					// indexing all catIds
					if(siteConfig.get("INDEX_CATIDS_ON_LUCENE_PRODUCT").getValue().equalsIgnoreCase("true")) {
						doc.add(new Field("catIds", (product.get("catIds") != null ? product.get("catIds").toString() : ""), TextField.TYPE_STORED));
					}
					
					doc.add(new DoubleField("min_price", minPrice, DoubleField.TYPE_STORED));
					doc.add(new DoubleField("max_price", maxPrice, DoubleField.TYPE_STORED));
					
					doc.add(new IntField("min_quantity", minimumQuantity, IntField.TYPE_STORED));
					
					// Reward Points are always calculated on minprice
					doc.add(new Field("account_number", supplierAccountNumber, TextField.TYPE_STORED));
					
					//active sales tag ( Need if products with special discount needs to be displayed)
					doc.add(new Field("active_sales_tag", product.get("active_sales_tag") != null ? product.get("active_sales_tag").toString() : "false", TextField.TYPE_STORED));
					
					//markup per supplier
					doc.add(new Field("mark_up_available", product.get("mark_up_available") != null ? product.get("mark_up_available").toString() : "false", TextField.TYPE_STORED));
					
					// Multifaceted Search on Product Fields
					this.indexRegularFieldsForMultifacetedSearch(minPrice, maxPrice, sbAll, predictiveSearchData, doc, taxo, product, ff, productFieldsForSearch, childProducts, null, ranges);
					
					if(sbAll != null) {
						doc.add(new Field("all_fields_data", sbAll.toString(), type));
					}
	
					if(predictiveSearchData != null) {
						doc.add(new Field("predictive_search_data", predictiveSearchData.toString(), TextField.TYPE_STORED));
					}
					if(doc.get("abc_code") == null) {
						doc.add(new IntField("abc_code", 100, IntField.TYPE_STORED));
					}
					
					writer.addDocument(doc);
				}
				//System.out.println("Indexing completed "+offset);
				offset = offset + limit;
		        search.setOffset(offset);
		        // DO NOT DELETE THIS LOG
		        //optimize writer after 1 mn products
		    	if((offset%(limit * 50)) == 0) {
		    	    writer.commit();
		    	    taxo.commit();
		    	}
		    }
			Date end = new Date();
			sbuff.append("\n");
			sbuff.append("Start on " + start + "\n");
			sbuff.append("End on " + end + "\n");
			sbuff.append("Indexed a total of " + writer.maxDoc() + "\n");
		    sbuff.append(end.getTime() - start.getTime() + " total milliseconds" + "\n");
		    sbuff.append("---------------" + "\n");
	
		    writer.close();
		    taxo.close();
		    IndexWriter.unlock(fsDir);
		}

		File indexDir = new File(baseDir, "products");
		if (indexDir.exists()) {
			if(indexDir.isDirectory()) {
				for(String fileName : indexDir.list()) {
					new File(indexDir, fileName).delete();
				}
			}
		}
		indexDir.mkdir();			
		// copy index files from temp to products
		FileUtils.copyDirectory(tempIndexDir, indexDir);
		
		if (tempIndexDir.exists()) {
			if(tempIndexDir.isDirectory()) {
				for(String fileName : tempIndexDir.list()) {
					new File(tempIndexDir, fileName).delete();
				}
			}
		}
		productCount = 0;
		return sbuff.toString();
	}
	
	public void autoIndex() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		siteConfig = this.webJaguar.getSiteConfig();
		
			try {
				String message = reIndex(gSiteConfig, siteConfig, webJaguar);
				notifyAdmin("Success: Lucene autoIndex() on " + siteConfig.get("SITE_URL").getValue(), message);
			} catch (Exception e) {
				e.printStackTrace();
				notifyAdmin("Failure : Lucene autoIndex() on " + siteConfig.get("SITE_URL").getValue(), e.getMessage());
			}
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
	
	private void indexRegularFieldsForMultifacetedSearch(double minPrice, double maxPrice, StringBuilder sbAll, StringBuilder predictiveSearchData, Document doc, TaxonomyWriter taxo, Map<String, Object> product, FacetFields ff, List<ProductField> productFieldsForSearch, List<Map<String, Object>> childProducts, Map<Long, String> categoryIdNameMap, List<RangeValue> ranges) throws IOException{

		List<CategoryPath> categories = new ArrayList<CategoryPath>();
		Integer equivalentIntNumberOfString = null;
		
		// Price Ranges
		if(ranges != null && ranges.size() > 0) {
			for(RangeValue rangeValue : ranges) {
				if(rangeValue.getMinValue() <= minPrice && rangeValue.getMaxValue() >= minPrice) {
					equivalentIntNumberOfString = this.getEquivalentIntNumber(rangeValue.getRangeDisplayValue());
					doc.add(new Field("Price Range", equivalentIntNumberOfString.toString(), TextField.TYPE_STORED));
					CategoryPath cp = new CategoryPath("Price Range", rangeValue.getRangeDisplayValue());
					categories.add(cp);
					taxo.addCategory(cp);
					break;		
				} 
			}
		}
		
		// Categories
		if(categoryIdNameMap != null && !categoryIdNameMap.isEmpty()) {
			Set<Object> catgoryIds = this.webJaguar.getCategoryIdsByProductId(Integer.parseInt(product.get("id").toString()));
			if(catgoryIds != null && catgoryIds.size() > 0) {
				for(Object catId : catgoryIds) {
					Long categoryId = Long.parseLong(catId.toString());
					
					if(siteConfig.get("LUCENE_CATEGORY_FILTER_BASED_ON_TREE").getValue().equals("true")) {
						
						List<Category> catTree = this.webJaguar.getCategoryTree(Integer.parseInt(catId.toString()), true, null);
						String[] components = new String[catTree.size() + 1];
						components[0] = "Category";	
						for(int i=0; i<catTree.size(); i++) {
							Category cat = catTree.get(i);
							equivalentIntNumberOfString = this.getEquivalentIntNumber(categoryIdNameMap.get(new Long(cat.getId())));
							doc.add(new Field("Category", equivalentIntNumberOfString.toString(), TextField.TYPE_STORED));
							components[i+1] = categoryIdNameMap.get(new Long(cat.getId()));
						}
								
						CategoryPath cp = new CategoryPath(components);
						categories.add(cp);
								
						taxo.addCategory(cp);
					} else {
						equivalentIntNumberOfString = this.getEquivalentIntNumber(categoryIdNameMap.get(categoryId));
						doc.add(new Field("Category", equivalentIntNumberOfString.toString(), TextField.TYPE_STORED));
						CategoryPath cp = new CategoryPath("Category", categoryIdNameMap.get(categoryId));
						categories.add(cp);
						taxo.addCategory(cp);
						break;		
					}
							
				}
			}
		} 
				
		// Filter does not work with String so it needs to be converted to eqvivalent integer value.
		// limited fields
		String[] commaSeparatedValues = null;
		for(ProductField pField : productFieldsForSearch) {
			
			// index for search
			if(pField.isIndexForSearch()) {
				commaSeparatedValues = null;
				equivalentIntNumberOfString = 0;
				if(product.get("field_"+pField.getId()) != null) {
					commaSeparatedValues = product.get("field_"+pField.getId()).toString().split(",");
					for(String value : commaSeparatedValues) {
						if(!value.trim().isEmpty()) {
							equivalentIntNumberOfString = this.getEquivalentIntNumber(value.trim());
							doc.add(new Field(pField.getName(),  equivalentIntNumberOfString.toString(), TextField.TYPE_STORED));
						}
					}
				}
				
				if(childProducts != null) {
					for(Map<String, Object> childProduct: childProducts) {
						commaSeparatedValues = null;
						equivalentIntNumberOfString = 0;
						if(childProduct.get("field_"+pField.getId()) != null) {
							commaSeparatedValues = childProduct.get("field_"+pField.getId()).toString().split(",");
							for(String value : commaSeparatedValues) {
								if(!value.trim().isEmpty()) {
									equivalentIntNumberOfString = this.getEquivalentIntNumber(value.trim());
									doc.add(new Field(pField.getName(),  equivalentIntNumberOfString.toString(), TextField.TYPE_STORED));
								}
							}
						}
					}
				}
				
				if(sbAll != null) {
					// for parent product
					if(product.get("field_"+pField.getId()) != null && !sbAll.toString().contains(product.get("field_"+pField.getId()).toString())) {
						sbAll.append(" "+product.get("field_"+pField.getId()).toString());
					}
					
					
					//append data for child products, excluding search rank and prices
					if(childProducts != null) {
						for(Map<String, Object> childProduct: childProducts) {
							if(childProduct.get("field_"+pField.getId()) != null && !sbAll.toString().contains(childProduct.get("field_"+pField.getId()).toString())) {
								sbAll.append(" "+childProduct.get("field_"+pField.getId()).toString());
							}
						}
					}
				}
			}
			
			// index for predictive search
			if(pField.isIndexForPredictiveSearch()) {
				
				if(predictiveSearchData != null) {
					// for parent product
					if(product.get("field_"+pField.getId()) != null && !predictiveSearchData.toString().contains(product.get("field_"+pField.getId()).toString())) {
						predictiveSearchData.append(" "+product.get("field_"+pField.getId()).toString());
					}
					
					//append data for child products, excluding search rank and prices
					if(childProducts != null) {
						for(Map<String, Object> childProduct: childProducts) {
							if(childProduct.get("field_"+pField.getId()) != null && !predictiveSearchData.toString().contains(childProduct.get("field_"+pField.getId()).toString())) {
								predictiveSearchData.append(" "+childProduct.get("field_"+pField.getId()).toString());
							}
						}
					}
				}
			}
			
			// index for filter
			if(pField.isIndexForFilter()) {
				commaSeparatedValues = null;
				if(product.get("field_"+pField.getId()) != null) {
					commaSeparatedValues = product.get("field_"+pField.getId()).toString().split(",");
					for(String value : commaSeparatedValues) {
						if(!value.trim().isEmpty()) {
							equivalentIntNumberOfString = this.getEquivalentIntNumber(value.trim());
							doc.add(new Field(pField.getName(),  equivalentIntNumberOfString.toString(), TextField.TYPE_STORED));
						}
					}
				}
				
				CategoryPath cp = null;
				if(product.get("field_"+pField.getId()) != null && !product.get("field_"+pField.getId()).toString().trim().isEmpty()) {
					commaSeparatedValues = product.get("field_"+pField.getId()).toString().split(",");
					for(String value : commaSeparatedValues) {
						if(!value.trim().isEmpty()) {
							cp = new CategoryPath(pField.getName(), value != null ? " "+value.trim() : " ");
							categories.add(cp);
							taxo.addCategory(cp);
						}
					}
				}
				if(childProducts != null) {
					for(Map<String, Object> childProduct: childProducts) {
						if(childProduct.get("field_"+pField.getId()) != null && !childProduct.get("field_"+pField.getId()).toString().trim().isEmpty()) {
							commaSeparatedValues = childProduct.get("field_"+pField.getId()).toString().split(",");
							
							for(String value : commaSeparatedValues) {
								if(!value.trim().isEmpty()) {
									cp = new CategoryPath(pField.getName(), value != null ? " "+value.trim() : " ");
									categories.add(cp);
									taxo.addCategory(cp);
								}
							}
						}
					}
				}
			}
			// index for range filter (Integer or Double value required)
			if(pField.isRangeFilter()) {
				try {
					doc.add(new DoubleField(pField.getName(), Double.parseDouble(product.get("field_"+pField.getId()) != null ? " "+product.get("field_"+pField.getId()).toString() : " "), DoubleField.TYPE_STORED));
				
					if(childProducts != null) {
						for(Map<String, Object> childProduct: childProducts) {
							doc.add(new DoubleField(pField.getName(), Double.parseDouble(childProduct.get("field_"+pField.getId()) != null ? " "+childProduct.get("field_"+pField.getId()).toString() : " "), DoubleField.TYPE_STORED));
						}
					}
				} catch(Exception e){ 
					// may not contain integer or double value 
				}
			}
		}
		
		/*
		 * Special Pricing
		 * Rule for bnoticed: Check if mark_up (Always gets the first markup from supplier) 
		 * OR 
		 * sales tag (for product having sales tag, cheks the filter 'Add to Filer' on sales tag is on) is available
		*/
		
		String specialPricing = "No";
		if((product.get("mark_up_available") != null &&  product.get("mark_up_available").toString().equals("true")) || (product.get("add_tag_to_filter") != null && product.get("add_tag_to_filter").toString().equals("true"))) {
			specialPricing = "Yes";
		}
		doc.add(new Field("Special Pricing", this.getEquivalentIntNumber(specialPricing).toString(), TextField.TYPE_STORED));
		CategoryPath catP = new CategoryPath("Special Pricing", specialPricing);
		categories.add(catP);
		taxo.addCategory(catP);
		
		if(!categories.isEmpty()) {
			ff.addFields(doc, categories);
		}
	}
	
	
	public Integer getEquivalentIntNumber(String fieldValue) {
		int prime = 31;
		int result = 1;
		
		for(char c : fieldValue.toCharArray()) {
			result = prime * result + Character.getNumericValue(c);
		}
		return  Math.abs(result);
	}
	
}
