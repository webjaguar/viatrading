/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.thirdparty.glosoweatherstation;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.web.form.ProductForm;


public class ContactFormController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ContactFormController () {
		setCommandName("contactForm");
		setCommandClass(Contact.class);		
		setPages(new String[] {"frontend/glosoweatherstation/contact"});
	}	
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder( request, binder );
		NumberFormat nf = NumberFormat.getInstance( RequestContextUtils.getLocale( request ) );
		nf.setMaximumFractionDigits( 2 );
		nf.setMinimumFractionDigits( 2 );
	}
	
	protected boolean suppressValidation(HttpServletRequest request, Object command) {
		if (ServletRequestUtils.getStringParameter(request, "_cancel", "").equals("true")) {
			return true;
		}
		return false;
	}
	
	public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		Contact contact = (Contact) command;


		//TODO Insert Contact to CRM
		//this.webJaguar.insertProduct(productForm.getProduct(), null);

		// TODO redirect back to this form with message
		return new ModelAndView(new RedirectView("contact.jhtm"));
	}
	
	protected ModelAndView processCancel(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
     throws Exception {
		return new ModelAndView(new RedirectView("account_products.jhtm"));
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		
		Contact contact = (Contact) command;
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	
       	Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		map.put("model", myModel);    	
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		switch (page) {
			case 0:

				break;
		}
		
		return map;
    } 
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		Contact contact = new Contact();

		return contact;
	}
	
	protected void validatePage(Object command, Errors errors, int page) {
		Contact contact = (Contact) command;
		
		switch (page) {
			case 0:
				// check email
//				if ( ) {
//					// duplicate
//					errors.rejectValue( "email", "SKU_ALREADY_EXISTS", "An item using this SKU already exists." );				
//				}

				break;
		}
	}
	
	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		ProductForm productForm = (ProductForm) command;

		switch (page) {
			case 0:
				
				break;
		}		
	}
}