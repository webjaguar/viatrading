package com.webjaguar.thirdparty.glosoweatherstation;

public class Contact {

	private String firstName;
	private String lastName;
	private boolean subscribeEmail;
	private String email;
	private String addr1;
	private String addr2;
	private String city;
	private String stateProvince;
	private String zip;
	private String phone;
	private String cellPhone;
	private boolean textMessage;
	private String cellPhoneCarrier;
	private String promoCode;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isSubscribeEmail() {
		return subscribeEmail;
	}

	public void setSubscribeEmail(boolean subscribeEmail) {
		this.subscribeEmail = subscribeEmail;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddr1() {
		return addr1;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public String getAddr2() {
		return addr2;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public boolean isTextMessage() {
		return textMessage;
	}

	public void setTextMessage(boolean textMessage) {
		this.textMessage = textMessage;
	}

	public String getCellPhoneCarrier() {
		return cellPhoneCarrier;
	}

	public void setCellPhoneCarrier(String cellPhoneCarrier) {
		this.cellPhoneCarrier = cellPhoneCarrier;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

}
