package com.webjaguar.thirdparty.plowAndHearth;

public class CCEncryptDecrypt {
	private static String ccString ;

	public static String fCreditCardRoutine(String ccNumber,String 	ccExpDate,String orderNumber,String custFname,String custLname,
	String recipientZip,String originalOrder,String 			dateTimeOrder,String emailAddress,String ccEncryptDecrypt)
	{

		int ccNumberLength ;
		String retCCVal ;

		ccString = "TREWQNMGFDSAYUIOPBVCXZHJKL" ;

		ccNumberLength = ccNumber.length() ;
		if (ccNumber.length() == 0)
		{
			System.out.println("\nNo Credit Card Number given.") ;
			System.out.println("\nExiting Out...\n") ;
			return (null) ;
		}

		retCCVal = fComEncrDecr								(ccNumber,dateTimeOrder,ccEncryptDecrypt) ;
		return(retCCVal) ;
	}



	private static String fComEncrDecr(String ccNumber,String 		dateTimeOrder,String ccEncryptDecrypt)
	{
		int i,j,K1 ;
		int T1,T2,T3,T4,T5,T6,T7,T8,T9,T10 ;
		int S1, S2, S3 ;
		int V1, V2 ;

		String ccEncrDcr = null ,ccEncrypt = null ,ccDecrypt = null  ;

		T1  = Integer.parseInt(dateTimeOrder.substring(0,1)) ;
		T2  = Integer.parseInt(dateTimeOrder.substring(1,2)) ;
		T3  = Integer.parseInt(dateTimeOrder.substring(2,3)) ;
		T4  = Integer.parseInt(dateTimeOrder.substring(3,4)) ;
		T5  = Integer.parseInt(dateTimeOrder.substring(4,5)) ;
		T6  = Integer.parseInt(dateTimeOrder.substring(5,6)) ;
		T7  = Integer.parseInt(dateTimeOrder.substring(6,7)) ;
		T8  = Integer.parseInt(dateTimeOrder.substring(7,8)) ;
		T9  = Integer.parseInt(dateTimeOrder.substring(8,9)) ;
		T10 = Integer.parseInt(dateTimeOrder.substring(9,10)) ;

		S1 = 1000 * T1 + 100 * T3 + 10 * T7 + T9 ;
		S2 = 1000 * T2 + 100 * T4 + 10 * T8 + T10 ;
		S3 = 10 * T5 + T6 ;

		V1 = ((S1*S2)+S3) % 26 ;
		V2 = (S1+(S2*S3)) % 19 ;
/*		System.out.println("T1\t : "+T1) ;
		System.out.println("T2\t : "+T2) ;
		System.out.println("T3\t : "+T3) ;
		System.out.println("T4\t : "+T4) ;
		System.out.println("T5\t : "+T5) ;
		System.out.println("T6\t : "+T6) ;
		System.out.println("T7\t : "+T7) ;
		System.out.println("T8\t : "+T8) ;
		System.out.println("T9\t : "+T9) ;
		System.out.println("T10\t : "+T10) ;
		System.out.println("S1\t : "+S1) ;
		System.out.println("S2\t : "+S2) ;
		System.out.println("S3\t : "+S3) ;
		System.out.println("V1\t : "+V1) ;
		System.out.println("V2\t : "+V2) ;*/


		if (ccEncryptDecrypt.equals("ECR") )
		{
			ccEncrDcr =	fEncryptRoutine							(V1,V2,ccNumber,dateTimeOrder) ;
		}
		if (ccEncryptDecrypt.equals("DCR") )
		{
			ccEncrDcr = fDecryptRoutine(							V1,V2,ccNumber,dateTimeOrder) ;
		}
		return(ccEncrDcr) ;
	}

	private static String fEncryptRoutine(int V1,int V2,String 		ccNumber,String dateTimeOrder)
	{

		int shiftPosition, tmpPosition, diffPosition ;
		int ccNumberLength ;
		int j,K1 ;
		int i,basePosition ;
		String ccEncrypt = new String() ;
		basePosition = V1 ;
		shiftPosition = V2 ;
		ccNumberLength = ccNumber.length() ;
		for (i = 0 ; i <= ccNumberLength-1 ; i++)
		{
			if ( i == 0)
			{
				shiftPosition = 0 ;
			}
			else
				shiftPosition = V2 ;
			K1 = Integer.parseInt(ccNumber.substring(i,i+1)) ;

			basePosition = (basePosition+shiftPosition+K1) % 25 ;
			if (basePosition == 0)
				basePosition = 25 ;
			ccEncrypt = ccEncrypt+ccString.substring					(basePosition,basePosition+1) ;
		}

		return(ccEncrypt) ;
	}

	private static String fDecryptRoutine(int V1,int V2,String 		ccNumber,String dateTimeOrder)
	{
		int j,K1 ;
		int i ;

		int basePosition, shiftPosition, tmpPosition, diffPosition ;
		int ccNumberLength ;
		String ccDecrypt = new String() ;
		String ccEncrypt = new String();


		ccEncrypt = ccNumber ;
		basePosition = V1 ;
		shiftPosition = V2 ;

		for ( i = 0 ; i <= ccEncrypt.length()-1 ; i++)
		{
			shiftPosition = V2 ;
			if ( i == 0)
				shiftPosition = 0 ;

			for(j=0 ; j <= 25 ; j++)
			{
				if (ccEncrypt.substring(i,i+1).equals					(ccString.substring(j,j+1)))
				{
					break ;
				}
			}
			tmpPosition = j ;
			diffPosition = (tmpPosition-shiftPosition-				basePosition) %  25 ;

			if (diffPosition < 0)
				diffPosition = diffPosition + 25 ;


			ccDecrypt = ccDecrypt + diffPosition ;
			basePosition = tmpPosition ;
		}
		return(ccDecrypt) ;

	}
}
