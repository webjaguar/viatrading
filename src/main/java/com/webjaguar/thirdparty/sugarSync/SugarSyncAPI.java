package com.webjaguar.thirdparty.sugarSync;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.GetMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.paymentech.eis.tools.http.Header;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.thirdparty.sugarSync.auth.AccessToken;
import com.webjaguar.thirdparty.sugarSync.auth.RefreshToken;
import com.webjaguar.thirdparty.sugarSync.file.CreationFile;
import com.webjaguar.thirdparty.sugarSync.file.FileDownloadAPI;
import com.webjaguar.thirdparty.sugarSync.file.FileUploadAPI;
import com.webjaguar.thirdparty.sugarSync.userInfo.UserInfo;
import com.webjaguar.thirdparty.sugarSync.util.HttpResponse;
import com.webjaguar.thirdparty.sugarSync.util.SugarSyncHTTPGetUtil;
import com.webjaguar.thirdparty.sugarSync.util.XmlUtil;

public class SugarSyncAPI {

	static String userName = null;
	static String password = null;
	static String applicationId = null;
	static String accessKey = null;
	static String privateKey = null;

	public SugarSyncAPI(Map<String, Configuration> siteConfig) {
		String[] credentials = siteConfig.get("SUGAR_SYNC_CREDENTIALS").getValue().split(",");
		String[] authentication = siteConfig.get("SUGAR_SYNC_AUTHENTICATION").getValue().split(",");

		this.userName = credentials[0]; // "baco@igroupny.com";
		this.password = credentials[1]; // "l0ngw00d";
		this.applicationId = authentication[0]; // "/sc/3779901/333_185636073";
		this.accessKey = authentication[1]; // "Mzc3OTkwMTEzNTExMTM4NzYyNzc";
		this.privateKey = authentication[2]; // "ZjIzNWM5MmE5ZTY0NGNkYWFkZjc2YmQ3Y2I0YjliZWI";
	}

	// used for printing user QUOTA
	private static final Double ONE_GB = 1024.0 * 1024 * 1024;

	// tool parameters and commands.
	private static final String userParam = "-user";
	private static final String passParam = "-password";
	private static final String applicationIdParam = "-application";
	private static final String accesskeyParam = "-accesskey";
	private static final String privateaccesskeyParam = "-privatekey";

	private static final String quotaCmd = "quota";
	private static final String listCmd = "list";
	private static final String uploadCmd = "upload";
	private static final String downloadCmd = "download";

	/**
	 * Returns the value of the parameter value specified as argument
	 * 
	 * @param param
	 *        the parameter for which the value is requested
	 * @param argumentList
	 *        the arguments passed to main method
	 * @return the value of the input parameter
	 */
	private static String getParam(String param, List<String> argumentList) {
		int indexOfParam = argumentList.indexOf(param);
		if (indexOfParam == -1) {
			System.out.println("Parameter " + param + " not specified!!!");
			printUsage();
			System.exit(0);
		}
		return argumentList.get(indexOfParam + 1);
	}

	/**
	 * Returns the command for the tool
	 * 
	 * @param argumentList
	 *        the arguments passed to main method
	 * @return the command which will be run by the tool
	 */
	private static String getCommand(List<String> argumentList) {
		String cmd = argumentList.get(argumentList.size() - 1);
		if (Arrays.asList(quotaCmd, listCmd).contains(cmd)) {
			return cmd;
		} else
			return argumentList.get(argumentList.size() - 2);
	}

	// --- SugarSync API calls
	/**
	 * Returns a refresh token for the developer with the credentials specified in the method parameters and the application id
	 * 
	 * @param username
	 *        SugarSync username (email address)
	 * @param password
	 *        SugarSync password
	 * @param applicationId
	 *        The developer application id
	 * @param accessKey
	 *        Developer accessKey
	 * @param privateAccessKey
	 *        Developer privateAccessKey
	 * @return refresh token
	 * @throws IOException
	 *         if any I/O error if thrown
	 */
	private static String getRefreshToken(String username, String password, String applicationId, String accessKey, String privateAccessKey) throws IOException {
		HttpResponse httpResponse = null;
		httpResponse = RefreshToken.getAuthorizationResponse(username, password, applicationId, accessKey, privateAccessKey);

		if (httpResponse.getHttpStatusCode() > 299) {
			System.out.println("Error while getting refresh token!");
			printResponse(httpResponse);
			System.exit(0);
		}

		return httpResponse.getHeader("Location").getValue();
	}

	/**
	 * Return an access token for the developer keys and a refresh token
	 * 
	 * @param accessKey
	 *        Developer accessKey
	 * @param privateAccessKey
	 *        Developer privateAccessKey
	 * @param refreshToken
	 *        Refresh token string returned ass a response from app-authorization request
	 * @return the access token that will be used for all API requests
	 * @throws IOException
	 *         if any I/O error if thrown
	 */
	private static String getAccessToken(String accessKey, String privateAccessKey, String refreshToken) throws IOException {
		HttpResponse httpResponse = AccessToken.getAccessTokenResponse(accessKey, privateAccessKey, refreshToken);

		if (httpResponse.getHttpStatusCode() > 299) {
			System.out.println("Error while getting access token!");
			printResponse(httpResponse);
			System.exit(0);
		}

		return httpResponse.getHeader("Location").getValue();
	}

	/**
	 * Returns the account information
	 * 
	 * @param accessToken
	 *        the access token
	 * @return a HttpResponse containing the server's xml response in the response body
	 * @throws IOException
	 *         if any I/O error occurs
	 */
	private static HttpResponse getUserInfo(String accessToken) throws IOException {
		HttpResponse httpResponse = UserInfo.getUserInfo(accessToken);
		System.out.println("httpResponse: " + httpResponse.getResponseBody());
		validateHttpResponse(httpResponse);
		return httpResponse;
	}

	/**
	 * Returns the "Magic Briefcase" SugarSync default folder contents
	 * 
	 * 1.Get the "Magic Briefcase" folder representation
	 * 
	 * 2.Extract the folder contents link from the folder representation: parse the xml file and retrieve the <contents> node value
	 * 
	 * 3.Make a HTTP GET to the previous extracted link
	 * 
	 * @param accessToken
	 *        the access token
	 * @return a HttpResponse containing the server's xml response in the response body
	 * @throws IOException
	 *         if any I/O error occurs
	 */
	private static HttpResponse getFolderContents(String accessToken, SugarSync sugarSync) throws IOException, XPathExpressionException {

		HttpResponse folderRepresentationResponse = getFolderRepresentation(accessToken, sugarSync);
		validateHttpResponse(folderRepresentationResponse);
		String folderContentsLink = XmlUtil.getNodeValues(folderRepresentationResponse.getResponseBody(), "/collectionContents/collection/contents/text()").get(0);
		HttpResponse folderContentsResponse = SugarSyncHTTPGetUtil.getRequest(folderContentsLink, accessToken);
		validateHttpResponse(folderContentsResponse);
		return folderContentsResponse;
	}

	/**
	 * Returns the "Magic Briefcase" SugarSync default folder representation.
	 * 
	 * 1. Make a HTTP GET call to https://api.sugarsync.com/user for the user information
	 * 
	 * 2. Extract < > node value from the xml response
	 * 
	 * 3. Make a HTTP GET to the previous extracted link
	 * 
	 * @param accessToken
	 *        the access token
	 * @return a HttpResponse containing the server's xml response in the response body
	 * @throws IOException
	 *         if any I/O error occurs
	 */
	private static HttpResponse getFolderRepresentation(String accessToken, SugarSync sugarSync) throws IOException, XPathExpressionException {

		HttpResponse userInfoResponse = getUserInfo(accessToken);
		// get the folder representation link
		String folderLink = XmlUtil.getNodeValues(userInfoResponse.getResponseBody(), "/user/workspaces/text()").get(0);
		// make a HTTP GET to the link extracted from user info
		HttpResponse folderRepresentationResponse = SugarSyncHTTPGetUtil.getRequest(folderLink, accessToken);
		validateHttpResponse(folderRepresentationResponse);
		return folderRepresentationResponse;
	}

	public HttpResponse getSearchFolderRepresentation(String accessToken, String name) throws IOException, XPathExpressionException {

		HttpResponse userInfoResponse = getUserInfo(accessToken);
		// get the folder representation link
		String folderLink = XmlUtil.getNodeValues(userInfoResponse.getResponseBody(), "/user/items/text()").get(0);
		folderLink = folderLink + "?displayName=" + name + "&max=100";
		HttpResponse folderRepresentationResponse = SugarSyncHTTPGetUtil.getRequest(folderLink, accessToken);
		validateHttpResponse(folderRepresentationResponse);
		return folderRepresentationResponse;
	}

	public static void getFileData(String endpoint, String accessToken) throws HttpException, IOException {
		HttpClient client = new HttpClient();
		/* Implement an HTTP GET method on the resource */
		GetMethod get = new GetMethod(endpoint);
		/* Create an access token and set it in the request header */
		// String token = GetAuth.getAuthQuest();
		get.setRequestHeader("Authorization", accessToken);
		/* Execute the HTTP GET request */
		client.executeMethod(get);

		/* Display the response */
		System.out.println("Response status code: " + get.getStatusCode());
		System.out.println("Response header: ");
		Header[] headers = (Header[]) get.getResponseHeaders();

		for (int i = 0; i < headers.length; i++) {
			System.out.println(headers[i]);
		}

		InputStream in = get.getResponseBodyAsStream();
		FileOutputStream out = new FileOutputStream(new File("C:\\down.jpg"));
		byte[] b = new byte[1024];
		int len = 0;
		while ((len = in.read(b)) != -1) {
			out.write(b, 0, len);
		}
		in.close();
		out.close();
		System.out.println("file download success");
	}

	// --- End SugarSync API calls

	// --- tool commands method
	/**
	 * Handles "quota" tool command. Makes a HTTP GET call to https://api.sugarsync.com/user and displays the quota information from the server xml response.
	 * 
	 * @param accessToken
	 *        the access token
	 * @throws IOException
	 *         if any I/O error occurs
	 * @throws XPathExpressionException
	 */
	private static void handleQuotaCommand(String accessToken) throws IOException, XPathExpressionException {
		HttpResponse httpResponse = getUserInfo(accessToken);
		System.out.println("ResponseBody: " + httpResponse.getResponseBody());
		// read the <quota> node values
		String limit = XmlUtil.getNodeValues(httpResponse.getResponseBody(), "/user/quota/limit/text()").get(0);
		String usage = XmlUtil.getNodeValues(httpResponse.getResponseBody(), "/user/quota/usage/text()").get(0);

		DecimalFormat threeDForm = new DecimalFormat("#.###");
		// print quota info
		String storageAvailableInGB = threeDForm.format(Double.valueOf(limit) / ONE_GB);
		String storageUsageInGB = threeDForm.format(Double.valueOf(usage) / ONE_GB);
		String freeStorageInGB = threeDForm.format(((Double.valueOf(limit) - Double.valueOf(usage)) / ONE_GB));
		System.out.println("\n---QUOTA INFO---");
		System.out.println("Total storage available: " + storageAvailableInGB + " GB");
		System.out.println("Storage usage: " + storageUsageInGB + " GB");
		System.out.println("Free storage: " + freeStorageInGB + " GB");
	}

	/**
	 * Handles "list" tool command. Makes a HTTP GET request to the "Magic Briefcase" contents link and displays the file and folder names within the folder
	 * 
	 * @param accessToken
	 *        the access token
	 * @throws IOException
	 * @throws XPathExpressionException
	 * @throws TransformerException
	 */
	public void handleListCommand(SugarSync sugarSync, boolean folderNumber) throws IOException, XPathExpressionException, TransformerException {
		if (folderNumber == true) {
			HttpResponse folderContentsResponse = getFolderContents(sugarSync.getAccessToken(), sugarSync);
			System.out.println("ResponseBody: " + folderContentsResponse.getResponseBody());
			printFolderContents(folderContentsResponse.getResponseBody(), sugarSync, true);
		} else {
			System.out.println("URL: " + sugarSync.getUrl());
			String url = sugarSync.getUrl() + "/contents";
			HttpResponse folderRepresentationResponse = SugarSyncHTTPGetUtil.getRequest(url, sugarSync.getAccessToken());
			printFolderContents(folderRepresentationResponse.getResponseBody(), sugarSync, false);
		}
	}

	/**
	 * Prints the files and folders from the xml response
	 * 
	 * @param responseBody
	 *        the xml server response
	 */
	public void printFolderContents(String responseBody, SugarSync sugarSync, boolean start) {
		sugarSync.setResponseBody(responseBody);
		try {
			List<String> folderNames = new ArrayList<String>();
			if (start) {
				folderNames = XmlUtil.getNodeValues(responseBody, "/collectionContents/collection[@type=\"syncFolder\"]/displayName/text()");
			} else {
				folderNames = XmlUtil.getNodeValues(responseBody, "/collectionContents/collection[@type=\"folder\"]/displayName/text()");
			}
			// sugarSync.setFolderNames(folderNames);
			List<String> fileNames = XmlUtil.getNodeValues(responseBody, "/collectionContents/file/displayName/text()");
			/*
			 * System.out.println("\n-Workspace"); System.out.println("\t-Folders:");
			 * 
			 * int i=0; for (String folder : folderNames) { System.out.println("\t\t" +i+": "+ folder); i++; } System.out.println("\t-Files:"); for (String file : fileNames) { System.out.println("\t\t" + file); }
			 */
		} catch (XPathExpressionException e1) {
			System.out.println("Error while printing the folder contents:");
			System.out.println("responseBody:" + responseBody);
		}
	}

	/**
	 * Handles "download" command.
	 * 
	 * 1. Get the "Magic Briefcase" contents
	 * 
	 * 2. Check if the specified file exists in the "Magic Briefcase" folder and retrieve its file data link
	 * 
	 * 3. Make a HTTP GET request to the previous extracted link and save the response content to a local file
	 * 
	 * @param accessToken
	 *        the access token
	 * @param file
	 *        the remote file within the "Magic Briefcase" folder
	 * @throws IOException
	 * @throws XPathExpressionException
	 */
	public boolean handleDownloadCommand(String accessToken, String file, SugarSync sugarSync) throws IOException, XPathExpressionException {

		HttpResponse fileDownloadResponse = FileDownloadAPI.downloadFileData(sugarSync.getUrl(), file, accessToken);

		return validateHttpResponse(fileDownloadResponse);
	}

	/**
	 * Handles "upload" tool command
	 * 
	 * 1. Get the user information.
	 * 
	 * 2. Extract the "Magic Briefcase" folder link from the user information response.
	 * 
	 * 3. Creates a file representation in "Magic Briefcase" folder.
	 * 
	 * 4. Uploads the file data associated to the previously created file representation.
	 * 
	 * @param accessToken
	 *        the access token
	 * @param file
	 *        the local file that will be uploaded in "Magic Briefcase".
	 * @throws XPathExpressionException
	 * @throws IOException
	 */
	private static void handleUploadCommand(String accessToken, String file) throws XPathExpressionException, IOException {
		if (!(new File(file).exists())) {
			System.out.println("\nFile " + file + "  doesn not exists in the current directory");
			System.exit(0);
		}
		HttpResponse userInfoResponse = getUserInfo(accessToken);

		String folderLink = XmlUtil.getNodeValues(userInfoResponse.getResponseBody(), "/user/magicBriefcase/text()").get(0);

		HttpResponse resp = CreationFile.createFile(folderLink, file, "", accessToken);

		String fileDataUrl = resp.getHeader("Location").getValue() + "/data";
		resp = FileUploadAPI.uploadFile(fileDataUrl, file, accessToken);

		System.out.println("\nUpload completed successfully. Check \"Magic Briefcase\" remote folder");

	}

	// ---Print and validation
	/**
	 * Validates the input arguments
	 * 
	 * @param args
	 *        the arguments passed to main method
	 */
	private static void validateArgs(List<String> args) {
		if (args.size() != 11 && args.size() != 12) {
			printUsage();
			System.exit(0);
		}
	}

	/**
	 * Validates the HTTP response. If HTTP response status code indicates an error the details are printed and the tool exists
	 * 
	 * @param httpResponse
	 *        the HTTP response which will be validated
	 */
	private static boolean validateHttpResponse(HttpResponse httpResponse) {
		boolean success = true;
		if (httpResponse.getHttpStatusCode() > 299) {
			System.out.println("HTTP ERROR!");
			printResponse(httpResponse);
			success = false;
		}
		return success;
	}

	/**
	 * Prints the http response
	 * 
	 * @param response
	 *        the HTTP response
	 */
	private static void printResponse(HttpResponse response) {
		System.out.println("STATUS CODE: " + response.getHttpStatusCode());
		// if the response is in xml format try to pretty format it, otherwise
		// leave it as it is
		String responseBodyString = null;
		try {
			responseBodyString = XmlUtil.formatXml(response.getResponseBody());
		} catch (Exception e) {
			responseBodyString = response.getResponseBody();
		}
		System.out.println("RESPONSE BODY:\n" + responseBodyString);
	}

	/**
	 * Prints the tool usage
	 */
	private static void printUsage() {
		System.out.println("USAGE:");
		System.out.println("java -jar sample-tool.jar " + userParam + " <username> " + passParam + " <password> " + applicationIdParam + " <appId> " + accesskeyParam + " <publicAccessKey> "
				+ privateaccesskeyParam + " <privateAccessKey> " + " ( " + quotaCmd + " | " + listCmd + " | " + downloadCmd + " <fileToDownload> | " + uploadCmd + " <fileToUpload> )");
		System.out.println("\nWHERE:");
		System.out.println("<username> - SugarSync username (email address)");
		System.out.println("<password> - SugarSync password");
		System.out.println("<appId> - The id of the app created from developer site");
		System.out.println("<publicAccessKey> - Developer accessKey");
		System.out.println("<privateAccessKey> - Developer privateAccessKey");
		System.out.println("<fileToDownload> - The file from default \"Magic Briefcase\" folder that you want to download");
		System.out.println("<fileToUpload> - The file from current directory that you want to upload into default \"Magic Briefcase\" folder ");

		System.out.println("\nEXAMPLES:");
		System.out.println("\nDisplaying user quota:");
		System.out.println("java -jar sample-tool.jar " + userParam + " user@email.com " + passParam + " userpassword " + applicationIdParam + " /sc/10016/3_1640259 " + accesskeyParam
				+ " MTUzOTEyNjEzMjM4NzEwNDg0MTc " + privateaccesskeyParam + " ZmNhMWY2MTZlY2M1NDg4OGJmZDY4OTExMjY5OGUxOWY " + quotaCmd);

		System.out.println("\nListing \"Magic Briefcase\" folder contents:");
		System.out.println("java -jar sample-tool.jar " + userParam + " user@email.com " + passParam + " userpassword " + applicationIdParam + " /sc/10016/3_1640259 " + accesskeyParam
				+ " MTUzOTEyNjEzMjM4NzEwNDg0MTc " + privateaccesskeyParam + " ZmNhMWY2MTZlY2M1NDg4OGJmZDY4OTExMjY5OGUxOWY " + listCmd);

		System.out.println("\nDownloading \"file.txt\" file  from \"Magic Briefcase\"");
		System.out.println("java -jar sample-tool.jar " + userParam + " user@email.com " + passParam + " userpassword " + applicationIdParam + " /sc/10016/3_1640259 " + accesskeyParam
				+ " MTUzOTEyNjEzMjM4NzEwNDg0MTc " + privateaccesskeyParam + " ZmNhMWY2MTZlY2M1NDg4OGJmZDY4OTExMjY5OGUxOWY " + downloadCmd + " file.txt");
		System.out.println("Please not that \"file.txt\" must exists in \"Magic Briefcase\" remote folder");

		System.out.println("\nUploading \"uploadFile.txt\" file  to \"Magic Briefcase\"");
		System.out.println("java -jar sample-tool.jar " + userParam + " user@email.com " + passParam + " userpassword " + applicationIdParam + " /sc/10016/3_1640259 " + accesskeyParam
				+ " MTUzOTEyNjEzMjM4NzEwNDg0MTc " + privateaccesskeyParam + " ZmNhMWY2MTZlY2M1NDg4OGJmZDY4OTExMjY5OGUxOWY " + downloadCmd + " file.txt");
		System.out.println("Please not that \"uploadFile.txt\" must exists in the local directory");

	}

	public void method(String cmd, SugarSync sugarSync) {

		String[] stringList = { "-user", userName, "-password", password, "-application", applicationId, "-accesskey", accessKey, "-privatekey", privateKey, "list" };

		List<String> argumentList = Arrays.asList(stringList);

		validateArgs(argumentList);

		String username = getParam(userParam, argumentList);
		String password = getParam(passParam, argumentList);
		String applicationId = getParam(applicationIdParam, argumentList);
		String accessKey = getParam(accesskeyParam, argumentList);
		String privateAccessKey = getParam(privateaccesskeyParam, argumentList);

		try {
			String refreshToken = getRefreshToken(username, password, applicationId, accessKey, privateAccessKey);

			String accessToken = getAccessToken(accessKey, privateAccessKey, refreshToken);
			sugarSync.setAccessToken(accessToken);
			String command = getCommand(argumentList);
			if (command.equals(quotaCmd)) {
				handleQuotaCommand(accessToken);
			} else if (command.equals(listCmd)) {
				handleListCommand(sugarSync, true);
			} else if (command.equals(downloadCmd)) {
				String file = argumentList.get(argumentList.size() - 1);
				handleDownloadCommand(accessToken, file, sugarSync);
			} else if (command.equals(uploadCmd)) {
				String file = argumentList.get(argumentList.size() - 1);
				handleUploadCommand(accessToken, file);
			} else {
				System.out.println("Uknown command: " + command);
				printUsage();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		SugarSync sugarSync = new SugarSync();
		userName = "baco@igroupny.com";
		password = "l0ngw00d";
		applicationId = "/sc/3779901/333_185636073";
		accessKey = "Mzc3OTkwMTEzNTExMTM4NzYyNzc";
		privateKey = "ZjIzNWM5MmE5ZTY0NGNkYWFkZjc2YmQ3Y2I0YjliZWI";

		String[] stringList = { "-user", userName, "-password", password, "-application", applicationId, "-accesskey", accessKey, "-privatekey", privateKey, "list" };

		List<String> argumentList = Arrays.asList(stringList);
		System.out.println("argumentList: " + argumentList);

		validateArgs(argumentList);

		String username = getParam(userParam, argumentList);
		String password = getParam(passParam, argumentList);
		String applicationId = getParam(applicationIdParam, argumentList);
		String accessKey = getParam(accesskeyParam, argumentList);
		String privateAccessKey = getParam(privateaccesskeyParam, argumentList);

		try {
			String refreshToken = getRefreshToken(username, password, applicationId, accessKey, privateAccessKey);

			String accessToken = getAccessToken(accessKey, privateAccessKey, refreshToken);

			String command = getCommand(argumentList);

			if (command.equals(quotaCmd)) {
				handleQuotaCommand(accessToken);
			} else if (command.equals(listCmd)) {
				// handleListCommand(accessToken, sugarSync, null);
			} else if (command.equals(downloadCmd)) {
				String file = argumentList.get(argumentList.size() - 1);
				System.out.println("LEN: " + file.length());
				// handleDownloadCommand(accessToken, file, sugarSync);
			} else if (command.equals(uploadCmd)) {
				String file = argumentList.get(argumentList.size() - 1);
				handleUploadCommand(accessToken, file);
			} else {
				System.out.println("Uknown command: " + command);
				printUsage();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
