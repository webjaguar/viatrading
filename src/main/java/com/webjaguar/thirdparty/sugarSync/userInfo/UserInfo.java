package com.webjaguar.thirdparty.sugarSync.userInfo;

import java.io.IOException;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;

import com.webjaguar.thirdparty.sugarSync.util.HttpResponse;

/**
 * 
 * Sample class used for getting user information
 * 
 */
public class UserInfo {
    /**
     * SugarSync User info API URL
     */
    private static final String USER_INFO_API_URL = "https://api.sugarsync.com/user";

    /**
     * The User-Agent HTTP Request header's value 
     */
    private static final String USER_AGENT = "Webjaguar";
    
    /**
     * Returns a UserInfo java bean containing all user information
     * 
     * @param accessToken
     *            the access token
     * @return a HttpResponse instance representing the response for the get
     *         request
     * @throws IOException
     *             if any I/O errors are thrown
     */
    public static HttpResponse getUserInfo(String accessToken) throws IOException {
        HttpClient client = new HttpClient();
        GetMethod get = new GetMethod(USER_INFO_API_URL);
        get.setRequestHeader("Authorization", accessToken);
        get.setRequestHeader("User-Agent",USER_AGENT);
        get.setRequestHeader("X-SugarSync-API-Version","1.5");
        client.executeMethod(get);

        // get HTTP response
        Integer statusCode = get.getStatusCode();
        String responseBody = get.getResponseBodyAsString();
        Header[] headers = get.getResponseHeaders();
        System.out.println("Headers: " + headers);
        return new HttpResponse(statusCode, responseBody, headers);
    }

}
