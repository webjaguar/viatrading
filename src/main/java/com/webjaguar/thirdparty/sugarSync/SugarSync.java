package com.webjaguar.thirdparty.sugarSync;

import java.util.List;

public class SugarSync {
	
    private String responseBody;	
    private String accessToken;
	private List<String> folderNames;
	private List<String> fileNames;
	private String url;
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<String> getFolderNames() {
		return folderNames;
	}

	public void setFolderNames(List<String> folderNames) {
		this.folderNames = folderNames;
	}

	public List<String> getFileNames() {
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}

	public String getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

}
