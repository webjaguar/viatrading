package com.webjaguar.thirdparty.sugarSync.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.FileUtils;

import com.webjaguar.thirdparty.sugarSync.util.HttpResponse;

/**
 * Sample class for file download
 */
public class FileDownloadAPI {
    
    /**
     * The User-Agent HTTP Request header's value 
     */
    private static final String USER_AGENT = "Webjaguar";
    
    /**
     * Downloads a remote file to the localDownloadPath
     * 
     * @param fileDataURL
     *            the remote file data link
     * @param localDownloadPath
     *            the local path where the file will be downloaded
     * @param accessToken
     *            the SugarSync access token
     * @return the HTTP response
     * @throws IOException
     *             if any I/O error occurs
     */
    public static HttpResponse downloadFileData(String fileDataURL, String localDownloadPath, String accessToken)
            throws IOException {
        // makes the HTTP get request
        HttpClient client = new HttpClient();
        GetMethod get = new GetMethod(fileDataURL);
        get.setRequestHeader("Authorization", accessToken);
        get.setRequestHeader("User-Agent",USER_AGENT);
        client.executeMethod(get);

        // get the input stream of the response and write its content to the
        // local file
        InputStream in = get.getResponseBodyAsStream();
        FileOutputStream out = new FileOutputStream(new File(localDownloadPath));
        byte[] b = new byte[1024];
        int len = 0;
        while ((len = in.read(b)) != -1) {
            out.write(b, 0, len);
        }
        in.close();
        out.close();

        // get HTTP response
        Integer statusCode = get.getStatusCode();
        Header[] headers = get.getResponseHeaders();

        return new HttpResponse(statusCode, null, headers);
    }
}
