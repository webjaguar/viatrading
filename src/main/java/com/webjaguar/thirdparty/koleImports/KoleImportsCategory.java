/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.08.2010
 */

package com.webjaguar.thirdparty.koleImports;

import java.util.ArrayList;
import java.util.List;

public class KoleImportsCategory {

	private String code;
	private String cat;
	private String sub;
	private Integer catId;
	private Double priceChange;
	List<KoleImportsCategory> subs;
	
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
	public String getCat()
	{
		return cat;
	}
	public void setCat(String cat)
	{
		this.cat = cat;
	}
	public String getSub()
	{
		return sub;
	}
	public void setSub(String sub)
	{
		this.sub = sub;
	}
	public Integer getCatId()
	{
		return catId;
	}
	public void setCatId(Integer catId)
	{
		this.catId = catId;
	}
	public Double getPriceChange()
	{
		return priceChange;
	}
	public void setPriceChange(Double priceChange)
	{
		this.priceChange = priceChange;
	}
	public List<KoleImportsCategory> getSubs()
	{
		return subs;
	}
	public void setSubs(List<KoleImportsCategory> subs)
	{
		this.subs = subs;
	}
	public void addSub(KoleImportsCategory sub) 
	{
		if (subs == null) subs = new ArrayList<KoleImportsCategory>();
		subs.add(sub);
	}

}
