/* Copyright 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 08.10.2007
 */

package com.webjaguar.thirdparty.weatherchannel;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.webjaguar.model.Weather;

public class WeatherChannelApi {	
	String hostname = "xoap.weather.com";
	String protocol = "http";
	String prefix = "weather/local";
	String partnerId = "1044739873";
	String licencedKey = "ce3cc449c3c2aa42";
	String prod = "xoap";
	// DTD:http://www.weather.com/documentation/xml/weather.dtd
	
	public Weather ModelGetTemp(String zipcode, String dayf) {
		Weather temperature = new Weather();
		try {
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + zipcode);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("prod=" + prod);
			request.append("&link=xoap");
			request.append("&cc=*");
			request.append("&dayf=" + dayf);
			request.append("&par=" + partnerId);
			request.append("&key=" + licencedKey);

			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
			
			SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(connection.getInputStream());

		    Element root = doc.getRootElement();
		    
		    
		    // get tomorrow high and low
			Element dayElement = root.getChild("dayf");
			Iterator results = dayElement.getChildren("day").iterator();
			while (results.hasNext()) {
				Element day = (Element) results.next();
				if ( day.getAttributeValue( "d" ).trim().toString().equals( "1" ) ) {
					temperature.setHigh( day.getChild( "hi" ).getText());
					temperature.setLow( day.getChild( "low" ).getText());
				}
			}
			
			// get current condition
			Element cc = root.getChild("cc");	
			Element ccTemp = cc.getChild("tmp");
			temperature.setCcTemp( ccTemp.getText() );
			Element ccIcon = cc.getChild("icon");
			temperature.setCcIcon( ccIcon.getText() );
			Element ccTextCondition = cc.getChild("t");
			temperature.setCcTextCondition( ccTextCondition.getText() );
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return temperature;
	}
}
