/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.21.2009
 */

package com.webjaguar.thirdparty.ingrammicro;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class IngramMicroCategoryController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		
		List<IngramMicroCategory> categories = new ArrayList<IngramMicroCategory>();
		
		if (request.getMethod().equals("POST")) {
			for (String code: request.getParameterValues("code")) {
				IngramMicroCategory category = new IngramMicroCategory();
				category.setCode(code);
				try {
					category.setCatId(ServletRequestUtils.getIntParameter(request, code + "_catId"));
				} catch (Exception e) {
					category.setCatId(null);
				}
				try {
					category.setPriceChange(ServletRequestUtils.getDoubleParameter(request, code + "_priceChange"));
				} catch (Exception e) {
					category.setPriceChange(null);
				}
				category.setPriceChangeBase(ServletRequestUtils.getStringParameter(request, code + "_priceChangeBase", ""));
				categories.add(category);
			}
			this.webJaguar.nonTransactionSafeUpdateIngramMicroCategories(categories);
		}
		
		categories = this.webJaguar.getIngramMicroCategories();
		
		Map<String, IngramMicroCategory> categoryMap = new HashMap<String, IngramMicroCategory>();
		Iterator<IngramMicroCategory> iter = categories.iterator();
		while (iter.hasNext()) {
			IngramMicroCategory category = iter.next();
			if (category.getSub().equals("*")) {
				categoryMap.put(category.getCat(), category);				
			} else {
				if (categoryMap.get(category.getCat()) != null) {
					categoryMap.get(category.getCat()).addSub(category);					
				}
				iter.remove();
			}
		}
		
		map.put("categories", categories);		

		return new ModelAndView("admin/vendors/ingramMicro/category", map);
	}
}
