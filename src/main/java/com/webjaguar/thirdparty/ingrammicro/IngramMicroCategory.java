/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.21.2009
 */

package com.webjaguar.thirdparty.ingrammicro;

import java.util.ArrayList;
import java.util.List;

public class IngramMicroCategory {

	private String code;
	private String cat;
	private String sub;
	private String descr;
	private Integer catId;
	private Double priceChange;
	List<IngramMicroCategory> subs;
	private String priceChangeBase;
	
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
	public String getCat()
	{
		return cat;
	}
	public void setCat(String cat)
	{
		this.cat = cat;
	}
	public String getSub()
	{
		return sub;
	}
	public void setSub(String sub)
	{
		this.sub = sub;
	}
	public String getDescr()
	{
		return descr;
	}
	public void setDescr(String descr)
	{
		this.descr = descr;
	}
	public Integer getCatId()
	{
		return catId;
	}
	public void setCatId(Integer catId)
	{
		this.catId = catId;
	}
	public Double getPriceChange()
	{
		return priceChange;
	}
	public void setPriceChange(Double priceChange)
	{
		this.priceChange = priceChange;
	}
	public List<IngramMicroCategory> getSubs()
	{
		return subs;
	}
	public void setSubs(List<IngramMicroCategory> subs)
	{
		this.subs = subs;
	}
	public void addSub(IngramMicroCategory sub) 
	{
		if (subs == null) subs = new ArrayList<IngramMicroCategory>();
		subs.add(sub);
	}
	public String getPriceChangeBase() {
		return priceChangeBase;
	}
	public void setPriceChangeBase(String priceChangeBase) {
		this.priceChangeBase = priceChangeBase;
	}

}
