/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.18.2010
 */

package com.webjaguar.thirdparty.echosign;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;

import echosign.api.dto8.EmbeddedWidgetCreationResult;
import echosign.api.dto8.UrlWidgetCreationResult;

public class WidgetController extends SimpleFormController  {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public WidgetController() {
		setSessionForm(false);
		setCommandName("echoSign");
		setCommandClass(Widget.class);
		setFormView("admin/echoSign/widget");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {	
    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	Widget widget = (Widget) command;
    	
    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile file = multipartRequest.getFile("_file");
		if (file != null && !file.isEmpty()) {
			File tempFolder = new File(getServletContext().getRealPath("temp"));
			if (!tempFolder.exists()) {
				tempFolder.mkdir();
			}				
			
			File document = new File(tempFolder, file.getOriginalFilename()); 
			file.transferTo(document);
			
			widget.setFileName(file.getOriginalFilename());
			
			EchoSignApi echoSignApi = new EchoSignApi();
			try {
				boolean success = false;
				String errorMessage = null;
				if (widget.isEmbedded()) {
					EmbeddedWidgetCreationResult result = echoSignApi.createEmbeddedWidget(widget.getApiKey(), widget.getUserKey(), document, widget.getDocumentName(), widget.getCallbackUrl(), widget.getCompletionUrl());					
					if (result.isSuccess()) {
						widget.setDocumentKey(result.getDocumentKey());
						widget.setJavascript(result.getJavascript());		
						success = true;
					} else {
						errorMessage = result.getErrorMessage();
					}					
				} else {
					UrlWidgetCreationResult result = echoSignApi.createUrlWidget(widget.getApiKey(), widget.getUserKey(), document, widget.getDocumentName(), widget.getCallbackUrl(), widget.getCompletionUrl());					
					if (result.isSuccess()) {
						widget.setDocumentKey(result.getDocumentKey());
						widget.setUrl(result.getUrl());		
						success = true;
					} else {
						errorMessage = result.getErrorMessage();
					}					
				} 
				if (success) {
					this.webJaguar.nonTransactionSafeInsertEchoSignWidget(widget);
					return new ModelAndView(new RedirectView("widgets.jhtm"));					
				} else {
					map.put("errorMessage", errorMessage);
					return showForm(request, response, errors, map);					
				}
			} catch (Exception e) {
				map.put("errorMessage", e.toString());
			}
			document.delete();
		} else {
			map.put("message", "missingUpload");
		}		
    	
		return showForm(request, response, errors, map);
    }
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
    	Widget widget = new Widget();
    	widget.setApiKey(siteConfig.get("ECHOSIGN_API_KEY").getValue().trim());
    	widget.setUserKey(siteConfig.get("ECHOSIGN_USER_KEY").getValue().trim());	
    	    	
    	int currentYear = Calendar.getInstance().get(Calendar.YEAR);
    	request.setAttribute("echoSignSentMap", this.webJaguar.getEchoSignSentMap(currentYear));
    	
		return widget;
	}
}
