/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.19.2010
 */

package com.webjaguar.thirdparty.echosign;

public class EchoSignWidgetSearch {

	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }

	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }

	// sort
	private String sort= "date_created DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }

	// widget document key
	private String widget;
	public void setWidget(String widget) { this.widget = widget; }
	public String getWidget() { return widget; }
}
