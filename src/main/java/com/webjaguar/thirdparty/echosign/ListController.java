/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 04.26.2010
 */

package com.webjaguar.thirdparty.echosign;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class ListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
		EchoSignSearch search = getSearch(request);
    	
		Map<String, Object> model = new HashMap<String, Object>();
		
		PagedListHolder list = new PagedListHolder(this.webJaguar.getEchoSignBySearch(search));		

		list.setPageSize(search.getPageSize());
		list.setPage(search.getPage()-1);
		
		model.put("list", list);
		
    	int currentYear = Calendar.getInstance().get(Calendar.YEAR);
    	request.setAttribute("echoSignSentMap", this.webJaguar.getEchoSignSentMap(currentYear));
    	
		return new ModelAndView("admin/echoSign/list", "model", model);
    }
	
	private EchoSignSearch getSearch(HttpServletRequest request) {
		EchoSignSearch search = (EchoSignSearch) request.getSession().getAttribute("echoSignSearch");
		if (search == null) {
			search = new EchoSignSearch();
			request.getSession().setAttribute("echoSignSearch", search);
		}
		
		// status
		if (request.getParameter("status") != null) {
			search.setAgreementStatus(ServletRequestUtils.getStringParameter(request, "status", ""));
		}
		
		// document name
		if (request.getParameter("documentName") != null) {
			search.setDocumentName(ServletRequestUtils.getStringParameter(request, "documentName", ""));
		}
		
		// username
		if (request.getParameter("username") != null) {
			search.setUsername(ServletRequestUtils.getStringParameter(request, "username", ""));
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);				
			}
		}			
		
		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				search.setPageSize(10);
			} else {
				search.setPageSize(size);				
			}
		}
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				search.setStartDate(df.parse( request.getParameter("startDate")));
			} catch (ParseException e) {
				search.setStartDate(null);
	        }
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				search.setEndDate(df.parse( request.getParameter("endDate")));
			} catch (ParseException e) {
				search.setEndDate(null);
	        }
		}
		
		// widget document key
		if (request.getParameter("widget") != null) {
			search.setWidget(ServletRequestUtils.getStringParameter(request, "widget", ""));
		}
		
		return search;
	}	
}
