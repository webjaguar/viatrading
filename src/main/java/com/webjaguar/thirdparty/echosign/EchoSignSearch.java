/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 04.26.2010
 */

package com.webjaguar.thirdparty.echosign;

import java.util.Date;

public class EchoSignSearch {

	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }

	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }

	// sort
	private String sort= "date_sent DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }
	
	// status
	private String agreementStatus;
	public String getAgreementStatus() { return agreementStatus; }
	public void setAgreementStatus(String agreementStatus) { this.agreementStatus = agreementStatus; }
	
	// document name
	private String documentName;
	public String getDocumentName() { return documentName; }
	public void setDocumentName(String documentName) { this.documentName = documentName; }
	
	// username
	private String username;
	public String getUsername() { return username; }
	public void setUsername(String username) { this.username = username; }
	
	// start date
	private Date startDate; 
	public Date getStartDate() { return startDate; }
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	
	// end date
	private Date endDate;
	public Date getEndDate() { return endDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }

	// widget document key
	private String widget;
	public void setWidget(String widget) { this.widget = widget; }
	public String getWidget() { return widget; }
	
	// used for triggers
	private String extraSQL;
	public String getExtraSQL() { return extraSQL; }
	public void setExtraSQL(String extraSQL) { this.extraSQL = extraSQL; }

}
