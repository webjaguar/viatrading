/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.09.2010
 */

package com.webjaguar.thirdparty.echosign;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.SalesRep;

import echosign.api.dto.SendReminderResult;

public class SendReminderController extends SimpleFormController  {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public SendReminderController() {
		setSessionForm(false);
		setCommandName("echoSign");
		setCommandClass(EchoSign.class);
		setFormView("admin/echoSign/sendReminder");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {	
    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	EchoSign echoSign = (EchoSign) command;
    	
		// replace dynamic elements
		Customer customer = this.webJaguar.getCustomerById(echoSign.getUserId());
		SalesRep salesRep = null;
		if (customer != null) {
			salesRep = this.webJaguar.getSalesRepById(customer.getSalesRepId());
		}
		echoSign.setMessage(this.webJaguar.replaceDynamicElement(echoSign.getMessage(), customer, salesRep));
    	
		EchoSignApi echoSignApi = new EchoSignApi();
		try {
			SendReminderResult result = echoSignApi.sendReminder(echoSign.getApiKey(), echoSign.getDocumentKey(),
						echoSign.getMessage());
			
			map.put("errorMessage", result.getResult());			
		} catch (Exception e) {
			map.put("errorMessage", e.toString());
		}	
    	
		return showForm(request, response, errors, map);
    }
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		
		EchoSign echoSign = this.webJaguar.getEchoSign(ServletRequestUtils.getStringParameter(request, "key", null));
    	if (echoSign == null) {
    		throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("../customers/")));
    	}
		
    	int currentYear = Calendar.getInstance().get(Calendar.YEAR);
    	request.setAttribute("echoSignSentMap", this.webJaguar.getEchoSignSentMap(currentYear));
    	
		return echoSign;
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("messages", this.webJaguar.getSiteMessageList());
		return map;
	}
}
