/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.18.2010
 */

package com.webjaguar.thirdparty.echosign;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class WidgetsListController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
		EchoSignWidgetSearch search = getSearch(request);
    	
		Map<String, Object> model = new HashMap<String, Object>();
		
		PagedListHolder list = new PagedListHolder(this.webJaguar.getEchoSignWidgetBySearch(search));		

		list.setPageSize(search.getPageSize());
		list.setPage(search.getPage()-1);
		
		model.put("list", list);
		
    	int currentYear = Calendar.getInstance().get(Calendar.YEAR);
    	request.setAttribute("echoSignSentMap", this.webJaguar.getEchoSignSentMap(currentYear));
    	
		return new ModelAndView("admin/echoSign/widgetList", "model", model);
    }
	
	private EchoSignWidgetSearch getSearch(HttpServletRequest request) {
		EchoSignWidgetSearch search = (EchoSignWidgetSearch) request.getSession().getAttribute("echoSignWidgetSearch");
		if (search == null) {
			search = new EchoSignWidgetSearch();
			request.getSession().setAttribute("echoSignWidgetSearch", search);
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);				
			}
		}			
		
		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				search.setPageSize(10);
			} else {
				search.setPageSize(size);				
			}
		}
		
		// widget document key
		if (request.getParameter("widget") != null) {
			search.setWidget(ServletRequestUtils.getStringParameter(request, "widget", ""));
		}
		
		return search;
	}	
}
