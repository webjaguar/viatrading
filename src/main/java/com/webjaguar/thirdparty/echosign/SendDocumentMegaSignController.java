/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.05.2010
 */

package com.webjaguar.thirdparty.echosign;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeSet;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.web.domain.Constants;

import echosign.api.dto.DocumentKey;
import echosign.api.dto.SendDocumentMegaSignResult;

public class SendDocumentMegaSignController extends SimpleFormController  {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public SendDocumentMegaSignController() {
		setSessionForm(false);
		setCommandName("echoSign");
		setCommandClass(EchoSign.class);
		setFormView("admin/echoSign/sendDocumentMegaSign");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {	
    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	EchoSign echoSign = (EchoSign) command;
    	
    	MultipartHttpServletRequest multipartRequest =  null;
    	try {
    		multipartRequest = (MultipartHttpServletRequest) request;
    	} catch(ClassCastException e) {
    		map.put("message", "missingUpload");
    		return showForm(request, response, errors, map);
    	}
    	
		MultipartFile file = multipartRequest.getFile("_file");
		if (file != null && !file.isEmpty()) {
			File tempFolder = new File(getServletContext().getRealPath("temp"));
			if (!tempFolder.exists()) {
				tempFolder.mkdir();
			}				
			
			File document = new File(tempFolder, file.getOriginalFilename()); 
			file.transferTo(document);
			
			echoSign.setFileName(file.getOriginalFilename());
			
			EchoSignApi echoSignApi = new EchoSignApi();
			try {
				SendDocumentMegaSignResult result = echoSignApi.sendDocumentMegaSign(echoSign.getApiKey(), echoSign.getUserKey(), 
							document, echoSign.getDocumentName(), echoSign.getMessage(), echoSign.getRecipients(), echoSign.getReminderFrequency());
				if (result.isSuccess()) {
					echoSign.setMegaSignDocumentKey(result.getDocumentKey().getDocumentKey());
					echoSign.setDateSent(new Date());
					DocumentKey[] documentKeys = result.getDocumentKeyArray();
					for (int i = 0; i < documentKeys.length; i++) {
						echoSign.setDocumentKey(documentKeys[i].getDocumentKey());
						echoSign.setRecipient(documentKeys[i].getRecipientEmail());
						this.webJaguar.nonTransactionSafeInsertEchoSign(echoSign, false);
					}
					return new ModelAndView(new RedirectView("../customers/"));
				} else {
					map.put("errorMessage", result.getErrorMessage());					
				}
			} catch (Exception e) {
				map.put("errorMessage", e.toString());
			}
			document.delete();
		} else {
			map.put("message", "missingUpload");
		}		
    	
		return showForm(request, response, errors, map);
    }
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		EchoSign echoSign = new EchoSign();
		echoSign.setApiKey(siteConfig.get("ECHOSIGN_API_KEY").getValue().trim());
		echoSign.setUserKey(siteConfig.get("ECHOSIGN_USER_KEY").getValue().trim());	
		
		String ids[] = ServletRequestUtils.getStringParameters(request, "__selected_id");
		if (ids.length == 0 || echoSign.getApiKey().equals("")) {
    		throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("../customers/")));    					
		} else if (ids.length == 1) {
    		throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("sendDocument.jhtm?cid=" + ids[0])));
		} else {
			echoSign.setRecipients(new TreeSet<String>());
			for (int i=0; i<ids.length; i++) {
				Customer customer = this.webJaguar.getCustomerById(Integer.parseInt(ids[i]));
				if (customer != null && Constants.validateEmail(customer.getUsername(), null)) {
					echoSign.getRecipients().add(customer.getUsername().toLowerCase());
				}
			}
			if (echoSign.getRecipients().isEmpty()) {
	    		throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("../customers/"))); 
			}
		}		
		    	
    	echoSign.setMessage("Please review and sign this document.");
    	
    	int currentYear = Calendar.getInstance().get(Calendar.YEAR);
    	request.setAttribute("echoSignSentMap", this.webJaguar.getEchoSignSentMap(currentYear));
    	
		return echoSign;
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("messages", this.webJaguar.getSiteMessageList());
		return map;
	}
}
