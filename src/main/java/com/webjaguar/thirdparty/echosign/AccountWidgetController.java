/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 04.13.2010
 */

package com.webjaguar.thirdparty.echosign;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;

import echosign.api.dto8.EmbeddedWidgetCreationResult;
import echosign.api.dto8.UrlWidgetCreationResult;

public class AccountWidgetController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {

		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
    	Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		Customer customer = (Customer) request.getAttribute("sessionCustomer");

		Widget widget = this.webJaguar.getEchoSignWidget(ServletRequestUtils.getStringParameter(request, "key", ""));
		if (widget != null) {
			EchoSignApi api = new EchoSignApi();
			
			try {
				if (widget.getJavascript() != null) {
					// embedded
					EmbeddedWidgetCreationResult creationResult = api.personalizeEmbeddedWidget(siteConfig.get("ECHOSIGN_API_KEY").getValue(), widget.getJavascript(), customer.getUsername(), null);
					if (creationResult.isSuccess()) {
						widget.setJavascript(creationResult.getJavascript());
					} else {
						model.put("creationResult", creationResult);
					}
				} else {
					// url
					UrlWidgetCreationResult creationResult = api.personalizeUrlWidget(siteConfig.get("ECHOSIGN_API_KEY").getValue(), widget.getUrl(), customer.getUsername(), null);
					if (creationResult.isSuccess()) {
						return new ModelAndView(new RedirectView(creationResult.getUrl()));
					} else {
						model.put("creationResult", creationResult);
					}
				}
			} catch (Exception e) {
				model.put("echoSignError", e.toString());
			}
			
			model.put("widget", widget);
		}
		
		Layout layout = (Layout) request.getAttribute("layout");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		
		return new ModelAndView("frontend/account/widget", "model", model);    	
    }

	
}
