/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.18.2010
 */

package com.webjaguar.thirdparty.echosign;

import java.util.Date;

public class Widget {
	
	private Date created;
	private String apiKey;
	private String userKey;
	private String documentKey;
	private String documentName;
	private String fileName;
	private String callbackUrl;
	private String completionUrl;
	private String url;
	private String javascript;	
	private int numOfDocuments;
	private boolean embedded;
	
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getUserKey() {
		return userKey;
	}
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}
	public String getDocumentKey() {
		return documentKey;
	}
	public void setDocumentKey(String documentKey) {
		this.documentKey = documentKey;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getCallbackUrl() {
		return callbackUrl;
	}
	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}
	public String getCompletionUrl() {
		return completionUrl;
	}
	public void setCompletionUrl(String completionUrl) {
		this.completionUrl = completionUrl;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getJavascript() {
		return javascript;
	}
	public void setJavascript(String javascript) {
		this.javascript = javascript;
	}
	public void setNumOfDocuments(int numOfDocuments) {
		this.numOfDocuments = numOfDocuments;
	}
	public int getNumOfDocuments() {
		return numOfDocuments;
	}
	public boolean isEmbedded() {
		return embedded;
	}
	public void setEmbedded(boolean embedded) {
		this.embedded = embedded;
	}
}
