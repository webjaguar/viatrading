/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.02.2010
 */

package com.webjaguar.thirdparty.echosign;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.SalesRep;

public class SendDocumentController extends SimpleFormController  {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public SendDocumentController() {
		setSessionForm(false);
		setCommandName("echoSign");
		setCommandClass(EchoSign.class);
		setFormView("admin/echoSign/sendDocument");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {	
    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	EchoSign echoSign = (EchoSign) command;
    	
    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile file = multipartRequest.getFile("_file");
		if (file != null && !file.isEmpty()) {
			File tempFolder = new File(getServletContext().getRealPath("temp"));
			if (!tempFolder.exists()) {
				tempFolder.mkdir();
			}				
			
			File document = new File(tempFolder, file.getOriginalFilename()); 
			file.transferTo(document);
			
			echoSign.setFileName(file.getOriginalFilename());
			
			// replace dynamic elements
			Customer customer = this.webJaguar.getCustomerById(echoSign.getUserId());
			SalesRep salesRep = null;
			if (customer != null) {
				salesRep = this.webJaguar.getSalesRepById(customer.getSalesRepId());
			}
			echoSign.setMessage(this.webJaguar.replaceDynamicElement(echoSign.getMessage(), customer, salesRep));
			
			EchoSignApi echoSignApi = new EchoSignApi();
			try {
				echoSign.setDocumentKey(echoSignApi.sendDocument(echoSign.getApiKey(), echoSign.getUserKey(), 
							document, echoSign.getDocumentName(), echoSign.getMessage(), echoSign.getRecipient(), echoSign.getReminderFrequency()));
				this.webJaguar.nonTransactionSafeInsertEchoSign(echoSign, false);

				return new ModelAndView(new RedirectView("../customers/customer.jhtm?id=" + echoSign.getUserId()));
			} catch (Exception e) {
				map.put("errorMessage", e.toString());
			}
			document.delete();
		} else {
			map.put("message", "missingUpload");
		}		
    	
		return showForm(request, response, errors, map);
    }
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		EchoSign echoSign = new EchoSign();
		echoSign.setApiKey(siteConfig.get("ECHOSIGN_API_KEY").getValue().trim());
		echoSign.setUserKey(siteConfig.get("ECHOSIGN_USER_KEY").getValue().trim());	
		echoSign.setUserId(ServletRequestUtils.getIntParameter(request, "cid", -1));
		
    	Customer customer = this.webJaguar.getCustomerById(echoSign.getUserId());
    	if (customer == null || echoSign.getApiKey().equals("")) {
    		throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("../customers/")));    		
    	}		
    	echoSign.setRecipient(customer.getUsername());
    	
    	echoSign.setMessage("Please review and sign this document.");
    	
    	int currentYear = Calendar.getInstance().get(Calendar.YEAR);
    	request.setAttribute("echoSignSentMap", this.webJaguar.getEchoSignSentMap(currentYear));
    	
		return echoSign;
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("messages", this.webJaguar.getSiteMessageList());
		return map;
	}
}
