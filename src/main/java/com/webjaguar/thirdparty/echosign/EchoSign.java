/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.04.2010
 */

package com.webjaguar.thirdparty.echosign;

import java.util.Date;
import java.util.Set;

import com.webjaguar.model.Customer;

public class EchoSign {
	
	private Date dateSent;
	private Date lastUpdate;
	private String apiKey;
	private String userKey;
	private String documentKey;
	private String megaSignDocumentKey;
	private String widgetDocumentKey;
	private String agreementStatus;
	private String documentUrl;
	private String history;
	private String documentName;
	private String message;
	private String fileName;
	private Integer userId;
	private String recipient;
	private Set<String> recipients;
	private boolean notifySigner;
	private String reminderFrequency;
	
	private Customer customer;
	
	public Date getDateSent() {
		return dateSent;
	}
	public void setDateSent(Date dateSent) {
		this.dateSent = dateSent;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getUserKey() {
		return userKey;
	}
	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}
	public String getDocumentKey() {
		return documentKey;
	}
	public void setDocumentKey(String documentKey) {
		this.documentKey = documentKey;
	}
	public String getMegaSignDocumentKey() {
		return megaSignDocumentKey;
	}
	public void setMegaSignDocumentKey(String megaSignDocumentKey) {
		this.megaSignDocumentKey = megaSignDocumentKey;
	}
	public String getWidgetDocumentKey() {
		return widgetDocumentKey;
	}
	public void setWidgetDocumentKey(String widgetDocumentKey) {
		this.widgetDocumentKey = widgetDocumentKey;
	}
	public String getAgreementStatus() {
		return agreementStatus;
	}
	public void setAgreementStatus(String agreementStatus) {
		this.agreementStatus = agreementStatus;
	}
	public String getDocumentUrl() {
		return documentUrl;
	}
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}
	public String getHistory() {
		return history;
	}
	public void setHistory(String history) {
		this.history = history;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getRecipient() {
		return recipient;
	}
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	public Set<String> getRecipients() {
		return recipients;
	}
	public void setRecipients(Set<String> recipients) {
		this.recipients = recipients;
	}
	public int getNumberOfRecipients() {
		return this.recipients.size();
	}
	public boolean isNotifySigner() {
		return notifySigner;
	}
	public void setNotifySigner(boolean notifySigner) {
		this.notifySigner = notifySigner;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Customer getCustomer() {
		return customer;
	}
	public String getReminderFrequency() {
		return reminderFrequency;
	}
	public void setReminderFrequency(String reminderFrequency) {
		this.reminderFrequency = reminderFrequency;
	}

}
