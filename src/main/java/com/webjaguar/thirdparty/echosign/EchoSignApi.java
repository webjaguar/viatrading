/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 02.18.2010
 */

package com.webjaguar.thirdparty.echosign;

import java.io.File;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.util.Set;

import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.service.binding.ObjectServiceFactory;

import echosign.api.EchoSignDocumentService8;
import echosign.api.dto.CallbackInfo;
import echosign.api.dto.CancelDocumentResult;
import echosign.api.dto.DocumentCreationInfo;
import echosign.api.dto.DocumentInfo;
import echosign.api.dto.DocumentKey;
import echosign.api.dto.DocumentUrlResult;
import echosign.api.dto.FileInfo;
import echosign.api.dto.Pong;
import echosign.api.dto.SendDocumentMegaSignResult;
import echosign.api.dto.SendReminderResult;
import echosign.api.dto.SenderInfo;
import echosign.api.dto.UserCreationInfo;
import echosign.api.dto.DocumentCreationInfo.ReminderFrequency;
import echosign.api.dto8.EmbeddedWidgetCreationResult;
import echosign.api.dto8.GetFormDataResult;
import echosign.api.dto8.UrlWidgetCreationResult;
import echosign.api.dto8.WidgetCompletionInfo;
import echosign.api.dto8.WidgetCreationInfo;
import echosign.api.dto8.WidgetPersonalizationInfo;

public class EchoSignApi {	
	
	String url = "https://secure.echosign.com/services/EchoSignDocumentService8";
	
	private EchoSignDocumentService8 getService(String url) throws MalformedURLException {
	    Service serviceModel = new ObjectServiceFactory().create(EchoSignDocumentService8.class);
	    return (EchoSignDocumentService8) new XFireProxyFactory().create(serviceModel, url);
	}
	
	public Pong testPing(String apiKey) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		return service.testPing(apiKey);
	}
	
	public String sendDocument(String apiKey, String userKey, File file, String documentName, String message, String recipient, String reminderFrequency) throws Exception {		
		return sendDocument(apiKey, userKey, file, documentName, message, new String[] { recipient }, reminderFrequency)[0].getDocumentKey();
	}
	
	public DocumentKey[] sendDocument(String apiKey, String userKey, File file, String documentName, String message, String[] recipients, String reminderFrequency) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
		SenderInfo senderInfo = null;
		if (userKey != null && userKey.trim().length() > 0) {
			senderInfo = new SenderInfo(userKey);
		}
		
	    FileInfo fileInfo = new FileInfo(file.getName(), null, file);
	    
	    if (documentName.trim().equals("")) {
	    	documentName = file.getName();
	    }
	    
		ReminderFrequency remFreq = null;
		if (reminderFrequency != null && reminderFrequency.equals("DAILY_UNTIL_SIGNED")) {
			remFreq = ReminderFrequency.DAILY_UNTIL_SIGNED;
		} else if (reminderFrequency != null && reminderFrequency.equals("WEEKLY_UNTIL_SIGNED")) {
			remFreq = ReminderFrequency.WEEKLY_UNTIL_SIGNED;
		}
		
		DocumentCreationInfo documentInfo =
	        new DocumentCreationInfo(recipients,								// to array
	        null,																// cc array
	        documentName,														// name
	        message,															// message
	        new FileInfo[] {fileInfo},											// file info array
	        DocumentCreationInfo.SignatureType.ESIGN,							// signature type
	        DocumentCreationInfo.SignatureFlow.SENDER_SIGNATURE_NOT_REQUIRED,	// signature info
	        null, 																// securityOptions
	        null, 																// externalID
	        remFreq, 															// reminderFrequency
	        null);																// callbackInfo
		
		return service.sendDocument(apiKey, senderInfo, documentInfo);
	}
		
	public DocumentInfo getDocumentInfo(String apiKey, String documentKey) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
		return service.getDocumentInfo(apiKey, documentKey);
	}
	
	public void getLatestDocument(String apiKey, String documentKey, File file) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
	    byte[] data = service.getLatestDocument(apiKey, documentKey);
	    FileOutputStream stream = new FileOutputStream(file);
	    stream.write(data);
	    stream.close();
	}

	public String getLatestDocumentUrl(String apiKey, String documentKey) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
		DocumentUrlResult result = service.getLatestDocumentUrl(apiKey, documentKey);	
		return result.getUrl();
	}
	
	public GetFormDataResult getFormData(String apiKey, String documentKey) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
		return service.getFormData(apiKey, documentKey);
	}
	
	public String createUser(String apiKey, UserCreationInfo user) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
		return service.createUser(apiKey, user);
	}
	
	public SendDocumentMegaSignResult sendDocumentMegaSign(String apiKey, String userKey, File file, String documentName, String message, Set<String> recipients, String reminderFrequency) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
		SenderInfo senderInfo = null;
		if (userKey != null && userKey.trim().length() > 0) {
			senderInfo = new SenderInfo(userKey);
		}
		
	    FileInfo fileInfo = new FileInfo(file.getName(), null, file);
	    
	    if (documentName.trim().equals("")) {
	    	documentName = file.getName();
	    }
	    
		ReminderFrequency remFreq = null;
		if (reminderFrequency != null && reminderFrequency.equals("DAILY_UNTIL_SIGNED")) {
			remFreq = ReminderFrequency.DAILY_UNTIL_SIGNED;
		} else if (reminderFrequency != null && reminderFrequency.equals("WEEKLY_UNTIL_SIGNED")) {
			remFreq = ReminderFrequency.WEEKLY_UNTIL_SIGNED;
		}
	    
	    if (recipients.size() <= 1) {
	    	throw new Exception("megasign requires more than one recipient");
	    }
	    
	    String[] recipient = new String[recipients.size()];
		
		DocumentCreationInfo documentInfo =
	        new DocumentCreationInfo(recipients.toArray(recipient),										// to array
	        null,																// cc array
	        documentName,														// name
	        message,															// message
	        new FileInfo[] {fileInfo},											// file info array
	        DocumentCreationInfo.SignatureType.ESIGN,							// signature type
	        DocumentCreationInfo.SignatureFlow.SENDER_SIGNATURE_NOT_REQUIRED,	// signature info
	        null, 																// securityOptions
	        null, 																// externalID
	        remFreq, 															// reminderFrequency
	        null);																// callbackInfo

		return service.sendDocumentMegaSign(apiKey, senderInfo, documentInfo);
	}
	
	public CancelDocumentResult cancelDocument(String apiKey, String documentKey, String message, boolean notifySigner) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
		return service.cancelDocument(apiKey, documentKey, message, notifySigner);
	}
	
	public SendReminderResult sendReminder(String apiKey, String documentKey, String message) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
		return service.sendReminder(apiKey, documentKey, message);
	}
	
	public EmbeddedWidgetCreationResult createEmbeddedWidget(String apiKey, String userKey, File file, String documentName, String callbackUrl, String completionUrl) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
		SenderInfo senderInfo = null;
		if (userKey != null && userKey.trim().length() > 0) {
			senderInfo = new SenderInfo(userKey);
		}
		
	    FileInfo fileInfo = new FileInfo(file.getName(), null, file);
	    
	    if (documentName.trim().equals("")) {
	    	documentName = file.getName();
	    }
	    
	    CallbackInfo callbackInfo = null;
	    if (callbackUrl != null) {
	    	callbackInfo = new CallbackInfo();
		    callbackInfo.setSignedDocumentUrl(callbackUrl);	    	
	    }
	    
	    WidgetCompletionInfo widgetCompletionInfo = null;
	    if (completionUrl != null && completionUrl.trim().length() > 0) {
	    	widgetCompletionInfo = new WidgetCompletionInfo(
	    		completionUrl,													// url
	    		false,															// deframe
	    																		// Note that in the case of embedded widgets, browser security restrictions do not permit automatic redirection in the full browser window, so if deframe is true the user will instead just see a link to the success page. 
	    																		// We recommend this scenario be avoided - in other words, setting deframe to false is recommended for embedded widgets.
	    		0);																// delay
	    }
		
		WidgetCreationInfo widgetCreationInfo = new WidgetCreationInfo(
	        documentName,														// name
	        new FileInfo[] {fileInfo},											// fileInfos
	        DocumentCreationInfo.SignatureFlow.SENDER_SIGNATURE_NOT_REQUIRED,	// signatureFlow
	        null,																// securityOptions
	        callbackInfo,														// callbackInfo
	        widgetCompletionInfo);												// widgetCompletionInfo
		
		return service.createEmbeddedWidget(apiKey, senderInfo, widgetCreationInfo);
	}
	
	public EmbeddedWidgetCreationResult personalizeEmbeddedWidget(String apiKey, String widgetJavascript, String email, String comment) throws Exception {
		EchoSignDocumentService8 service = getService(url);
	    
		WidgetPersonalizationInfo widgetPersonalizationInfo = new WidgetPersonalizationInfo(
				email,																// email
		        comment,															// comment
		        null,																// expiration
		    	false,																// reusable
				null);																// allowManualVerification
		
		return service.personalizeEmbeddedWidget(apiKey, widgetJavascript, widgetPersonalizationInfo);
	}
	
	public EmbeddedWidgetCreationResult createPersonalEmbeddedWidget(String apiKey, String userKey, File file, String documentName, String email, String comment) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
		SenderInfo senderInfo = null;
		if (userKey != null && userKey.trim().length() > 0) {
			senderInfo = new SenderInfo(userKey);
		}
		
	    FileInfo fileInfo = new FileInfo(file.getName(), null, file);
	    
	    if (documentName.trim().equals("")) {
	    	documentName = file.getName();
	    }
		
		WidgetCreationInfo widgetCreationInfo = new WidgetCreationInfo(
	        documentName,														// name
	        new FileInfo[] {fileInfo},											// fileInfos
	        DocumentCreationInfo.SignatureFlow.SENDER_SIGNATURE_NOT_REQUIRED,	// signatureFlow
	        null,																// securityOptions
	        null,																// callbackInfo
	        null);																// widgetCompletionInfo
		
		WidgetPersonalizationInfo widgetPersonalizationInfo = new WidgetPersonalizationInfo(
			email,																// email
			comment,															// comment
	        null,																// expiration
	    	false,																// reusable
			null);																// allowManualVerification
		
		return service.createPersonalEmbeddedWidget(apiKey, senderInfo, widgetCreationInfo, widgetPersonalizationInfo);
	}
	
	public UrlWidgetCreationResult createUrlWidget(String apiKey, String userKey, File file, String documentName, String callbackUrl, String completionUrl) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
		SenderInfo senderInfo = null;
		if (userKey != null && userKey.trim().length() > 0) {
			senderInfo = new SenderInfo(userKey);
		}
		
	    FileInfo fileInfo = new FileInfo(file.getName(), null, file);
	    
	    if (documentName.trim().equals("")) {
	    	documentName = file.getName();
	    }
	    
	    CallbackInfo callbackInfo = null;
	    if (callbackUrl != null) {
	    	callbackInfo = new CallbackInfo();
		    callbackInfo.setSignedDocumentUrl(callbackUrl);	    	
	    }
	    
	    WidgetCompletionInfo widgetCompletionInfo = null;
	    if (completionUrl != null && completionUrl.trim().length() > 0) {
	    	widgetCompletionInfo = new WidgetCompletionInfo(
	    		completionUrl,													// url
	    		true,															// deframe
	    		0);																// delay
	    }
		
		WidgetCreationInfo widgetCreationInfo = new WidgetCreationInfo(
	        documentName,														// name
	        new FileInfo[] {fileInfo},											// fileInfos
	        DocumentCreationInfo.SignatureFlow.SENDER_SIGNATURE_NOT_REQUIRED,	// signatureFlow
	        null,																// securityOptions
	        callbackInfo,														// callbackInfo
	        widgetCompletionInfo);												// widgetCompletionInfo
		
		return service.createUrlWidget(apiKey, senderInfo, widgetCreationInfo);
	}
	
	public UrlWidgetCreationResult personalizeUrlWidget(String apiKey, String widgetUrl, String email, String comment) throws Exception {
		EchoSignDocumentService8 service = getService(url);
		
		WidgetPersonalizationInfo widgetPersonalizationInfo = new WidgetPersonalizationInfo(
			email,																// email
			comment,															// comment
			null,																// expiration
			false,																// reusable
			null);																// allowManualVerification
		
		return service.personalizeUrlWidget(apiKey, widgetUrl, widgetPersonalizationInfo);
	}	
}

//You can do an HTTP PUT using Jakarta Commons HTTP Client
//PutMethod put = new PutMethod("http://jakarta.apache.org");
//put.setRequestBody(new FileInputStream("UploadMe.gif"));
//HttpClient client = new HttpClient();
//client.executeMethod(put);

