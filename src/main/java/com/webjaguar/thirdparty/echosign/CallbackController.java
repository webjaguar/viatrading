/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.12.2010
 */

package com.webjaguar.thirdparty.echosign;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class CallbackController extends WebApplicationObjectSupport implements Controller {
	
	WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }
    
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {	
    	
		if (!request.getMethod().equals("PUT")) {
			// response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "HTTP method " + request.getMethod() + " is not supported by this URL");
			// do not disclose anything
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
		
		Widget widget = this.webJaguar.getEchoSignWidgetByPath(request.getServletPath());
		if (widget != null) {
			// valid
			File echosignDir = new File(getServletContext().getRealPath("/assets/echosign"));
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream( getServletContext().getRealPath("WEB-INF/properties/site.properties")));
				if (prop.get("site.root") != null) {
					echosignDir = new File((String) prop.get("site.root") + "/assets/echosign");
				}
			} catch (Exception e) {}
			if (!echosignDir.exists()) echosignDir.mkdir();
			
			// create directory
			String path = request.getServletPath();		// pattern /echosignCallback_XXXX.jhtm
			File folder = new File(echosignDir, path.substring(18, path.length()-5));
			if (!folder.exists()) folder.mkdir(); 

			// save file
	    	String TIMEZONE = "GMT";
			String TIMESTAMP_FORMAT = "yyyy-MM-dd-kkmmss'.'SSS";
			SimpleDateFormat dateFormatter = new SimpleDateFormat(TIMESTAMP_FORMAT);
			dateFormatter.setTimeZone(TimeZone.getTimeZone(TIMEZONE));
			
			InputStream in = request.getInputStream();
			OutputStream outstream = new FileOutputStream(new File(folder, dateFormatter.format(new Date()) + ".pdf"));

			// Transfer bytes from in to out
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				outstream.write(buf, 0, len);
			}
			in.close();
			outstream.close();	

		} else {
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		
		return null;
    }
}
