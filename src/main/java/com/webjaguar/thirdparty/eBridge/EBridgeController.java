/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 08.20.2009
 */

package com.webjaguar.thirdparty.eBridge;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import WebServices.eBridge.DocType;
import WebServices.eBridge.EPortalServiceLocator;
import WebServices.eBridge.EPortalServiceSoap;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;


public class EBridgeController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		siteConfig = this.webJaguar.getSiteConfig();
		
		System.out.println("1");
		boolean sendFile = ServletRequestUtils.getBooleanParameter(request, "sendFile", false);
		if (sendFile) {
			System.out.println("2");
			boolean reply = sendFile();
		}
		

		return new ModelAndView("admin/eBridge/index", map);
	}
	
	private boolean sendFile() throws Exception {
	   
		EPortalServiceLocator eBridgeService = new EPortalServiceLocator();
		EPortalServiceSoap eBridgePort;
		eBridgePort = eBridgeService.getePortalServiceSoap();
		System.out.println("3");
		String content = createContent();
		
		boolean reply = eBridgePort.sendFile("WebJa88871", "85051", content, "myFileName.xml");
		if (reply){
			System.out.println(reply);
			// do more
		} else {
			System.out.println(reply);
			// do more
		}

		//eBridgePort.getDocumentList("WebJa88871", "85051", status, docType, partner, fromDate, toDate)

		//		WebServices.eBridge.DocType[] docTypes = eBridgePort.getDocTypeList("WebJa88871", "85051");
//		for (DocType docType : docTypes) {
//			System.out.println("Id " + docType.getId() + " Name" + docType.getName());
//		}
		return reply;
	}
	
	private String createContent() {
		StringBuffer xmlRequest = new StringBuffer(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<Order xmlns=\"rrn:org.xcbl:schemas/xcbl/v4_0/ordermanagement/v1_0/ordermanagement.xsd\" xmlns:core=\"rrn:org.xcbl:schemas/xcbl/v4_0/core/core.xsd\" xmlns:dgs=\"http://www.w3.org/2000/09/xmldsig#\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"rrn:org.xcbl:schemas/xcbl/v4_0/ordermanagement/v1_0/ordermanagement.xsd C:/Schemas/xcbl40/schema/org/xcbl/path_delim/schemas/xcbl/v4_0/ordermanagement/v1_0/ordermanagement.xsd\">" +
					"<OrderHeader>" +
						"<OrderNumber>" +
							"<BuyerOrderNumber>PO # 123456789</BuyerOrderNumber>" +
						"</OrderNumber>" +
						"<OrderIssueDate>2008-01-25T00:00:01</OrderIssueDate>" +
						"<OrderReferences>" +
							"<AccountCode>" +
								"<core:RefNum xmlns=\"\">End Customer PO Num</core:RefNum>" +
								"<core:RefDate xmlns=\"\">2008-03-01T00:00:33</core:RefDate>" +
							"</AccountCode>" +
							"<ContractReferences>" +
								"<core:Contract xmlns=\"\">" +
									"<core:ContractID>" +
										"<core:Ident>Contract123</core:Ident>" +
									"</core:ContractID>" +
									"<core:TypeOfContract>" +
										"<core:ContractTypeCoded>Other</core:ContractTypeCoded>" +
										"<core:ContractTypeCodedOther>CostPlus</core:ContractTypeCodedOther>" +
									"</core:TypeOfContract>" +
								"</core:Contract>" +
							"</ContractReferences>" +
							"<OtherOrderReferences>" +
								"<core:ReferenceCoded xmlns=\"\">" +
									"<core:ReferenceTypeCoded>Status</core:ReferenceTypeCoded>" +
									"<core:PrimaryReference>" +
										"<core:RefNum>Other</core:RefNum>" +
									"</core:PrimaryReference>" +
								"</core:ReferenceCoded>" +
							"</OtherOrderReferences>" +
						"</OrderReferences>" +
						"<Purpose>" +
							"<core:PurposeCoded xmlns=\"\">Other</core:PurposeCoded>" +
							"<core:PurposeCodedOther xmlns=\"\">Original</core:PurposeCodedOther>" +
						"</Purpose>" +
						"<RequestedResponse>" +
							"<core:RequestedResponseCoded xmlns=\"\">Other</core:RequestedResponseCoded>" +
							"<core:RequestedResponseCodedOther xmlns=\"\">NA</core:RequestedResponseCodedOther>" +
						"</RequestedResponse>" +
						"<OrderType>" +
							"<core:OrderTypeCoded xmlns=\"\">Other</core:OrderTypeCoded>" +
							"<core:OrderTypeCodedOther xmlns=\"\">InboundSalesOrder</core:OrderTypeCodedOther>" +
						"</OrderType>" +
						"<OrderCurrency>" +
							"<core:CurrencyCoded xmlns=\"\">Other</core:CurrencyCoded>" +
							"<core:CurrencyCodedOther xmlns=\"\">USD</core:CurrencyCodedOther>" +
							"</OrderCurrency>" +
						"<OrderLanguage>" +
							"<core:LanguageCoded xmlns=\"\">Other</core:LanguageCoded>" +
							"<core:LanguageCodedOther xmlns=\"\">EN-US</core:LanguageCodedOther>" +
						"</OrderLanguage>" +
						"<OrderTaxReference>" +
							"<core:TaxTypeCoded xmlns=\"\">Other</core:TaxTypeCoded>" +
							"<core:TaxTypeCodedOther xmlns=\"\">" +
								"<core:Ident>GST</core:Ident>" +
							"</core:TaxTypeCodedOther>" +
							"<core:TaxFunctionQualifierCoded xmlns=\"\">Other</core:TaxFunctionQualifierCoded>" +
							"<core:TaxFunctionQualifierCodedOther xmlns=\"\">Other</core:TaxFunctionQualifierCodedOther>" +
							"<core:TaxCategoryCoded xmlns=\"\">Other</core:TaxCategoryCoded>" +
							"<core:TaxCategoryCodedOther xmlns=\"\">Other</core:TaxCategoryCodedOther>" +
							"<core:ReasonTaxExemptCoded xmlns=\"\">Other</core:ReasonTaxExemptCoded>" +
							"<core:ReasonTaxExemptCodedOther xmlns=\"\">Exempt-ForExport</core:ReasonTaxExemptCodedOther>" +
							"<core:TaxPercent xmlns=\"\">5</core:TaxPercent>" +
							"<core:TaxTreatmentCoded xmlns=\"\">Other</core:TaxTreatmentCoded>" +
							"<core:TaxTreatmentCodedOther xmlns=\"\">InvoiceLevel</core:TaxTreatmentCodedOther>" +
							"<core:TaxAmount xmlns=\"\">5</core:TaxAmount>" +
							"<core:TaxLocation xmlns=\"\">" +
								"<core:LocationQualifierCoded>Other</core:LocationQualifierCoded>" +
								"<core:LocationQualifierCodedOther>SP</core:LocationQualifierCodedOther>" +
								"<core:LocationIdentifier>" +
									"<core:LocID>" +
										"<core:Ident>Ontario</core:Ident>" +
									"</core:LocID>" +
								"</core:LocationIdentifier>" +
							"</core:TaxLocation>" +
						"</OrderTaxReference>" +
						"<OrderDates>" +
							"<RequestedShipByDate>2008-01-30T00:00:12</RequestedShipByDate>" +
							"<RequestedDeliverByDate>2008-01-31T00:00:13</RequestedDeliverByDate>" +
							"<PromiseDate>2008-01-03T00:00:14</PromiseDate>" +
							"<ValidityDates>" +
								"<core:StartDate xmlns=\"\">2008-01-01T00:00:15</core:StartDate>" +
								"<core:EndDate xmlns=\"\">2008-02-07T00:00:16</core:EndDate>" +
							"</ValidityDates>" +
							"<CancelByDate>2008-02-07T00:00:17</CancelByDate>" +
						"</OrderDates>" +
						"<PartialShipmentAllowed>true</PartialShipmentAllowed>" +
						"<OrderParty>" +
							"<BuyerParty>" +
								"<core:PartyID xmlns=\"\">" +
									"<core:Ident>9496008868</core:Ident>" +
								"</core:PartyID>" +
								"<core:ListOfIdentifier xmlns=\"\">" +
									"<core:Identifier>" +
										"<core:Ident>5555551212</core:Ident>" +
									"</core:Identifier>" +
								"</core:ListOfIdentifier>" +
								"<core:NameAddress xmlns=\"\">" +
									"<core:Name1>Shahin</core:Name1>" +
									"<core:Street>6B Liberty</core:Street>" +
									"<core:StreetSupplement1>Suite 145</core:StreetSupplement1>" +
									"<core:StreetSupplement2></core:StreetSupplement2>" +
									"<core:PostalCode>92656</core:PostalCode>" +
									"<core:City>ALiso Viejo</core:City>" +
									"<core:County>Orange</core:County>" +
									"<core:Region>" +
										"<core:RegionCoded>Other</core:RegionCoded>" +
										"<core:RegionCodedOther>Ontario</core:RegionCodedOther>" +
									"</core:Region>" +
									"<core:Country>" +
										"<core:CountryCoded>Other</core:CountryCoded>" +
										"<core:CountryCodedOther>Canada</core:CountryCodedOther>" +
									"</core:Country>" +
								"</core:NameAddress>" +
								"<core:PrimaryContact xmlns=\"\">" +
									"<core:ContactID>" +
										"<core:Ident>Main Contact</core:Ident>" +
									"</core:ContactID>" +
									"<core:ContactName>Contact Name</core:ContactName>" +
									"<core:ListOfContactNumber>" +
										"<core:ContactNumber>" +
											"<core:ContactNumberValue>555-555-1212</core:ContactNumberValue>" +
											"<core:ContactNumberTypeCoded>TelephoneNumber</core:ContactNumberTypeCoded>" +
										"</core:ContactNumber>" +
									"</core:ListOfContactNumber>" +
								"</core:PrimaryContact>" +
								"<core:PartyTaxInformation xmlns=\"\">" +
									"<core:TaxIdentifier>" +
										"<core:Ident>TaxID</core:Ident>" +
									"</core:TaxIdentifier>" +
								"</core:PartyTaxInformation>" +
							"</BuyerParty>" +
							"<SellerParty>" +
								"<core:PartyID xmlns=\"\">" +
									"<core:Ident>EBRIDGESOFTWARE</core:Ident>" +
								"</core:PartyID>" +
								"<core:ListOfIdentifier xmlns=\"\">" +
									"<core:Identifier>" +
										"<core:Ident>123456</core:Ident>" +
									"</core:Identifier>" +
								"</core:ListOfIdentifier>" +
								"<core:NameAddress xmlns=\"\">" +
									"<core:Name1>Vendor 1</core:Name1>" +
								"</core:NameAddress>" +
								"<core:PrimaryContact xmlns=\"\">" +
									"<core:ContactID>" +
										"<core:Ident>Salesperson ID</core:Ident>" +
									"</core:ContactID>" +
									"<core:ContactName>Salesperson Name</core:ContactName>" +
								"</core:PrimaryContact>" +
							"</SellerParty>" +
							"<ShipToParty>" +
								"<core:PartyID xmlns=\"\">" +
									"<core:Ident>0551</core:Ident>" +
								"</core:PartyID>" +
								"<core:ListOfIdentifier xmlns=\"\">" +
									"<core:Identifier>" +
										"<core:Ident>0551</core:Ident>" +
									"</core:Identifier>" +
								"</core:ListOfIdentifier>" +
								"<core:NameAddress xmlns=\"\">" +
									"<core:Name1>Main</core:Name1>" +
									"<core:Street>123 Main Street</core:Street>" +
									"<core:StreetSupplement1>Unit D</core:StreetSupplement1>" +
									"<core:StreetSupplement2>2nd Floor</core:StreetSupplement2>" +
									"<core:PostalCode>Z1Z 1Z1</core:PostalCode>" +
									"<core:City>Burlington</core:City>" +
									"<core:County>Halton</core:County>" +
									"<core:Region>" +
										"<core:RegionCoded>Other</core:RegionCoded>" +
										"<core:RegionCodedOther>Ontario</core:RegionCodedOther>" +
									"</core:Region>" +
									"<core:Country>" +
										"<core:CountryCoded>Other</core:CountryCoded>" +
										"<core:CountryCodedOther>Canada</core:CountryCodedOther>" +
									"</core:Country>" +
								"</core:NameAddress>" +
								"<core:PrimaryContact xmlns=\"\">" +
									"<core:ContactName>Receivers Name</core:ContactName>" +
								"</core:PrimaryContact>" +
							"</ShipToParty>" +
							"<BillToParty>" +
								"<core:PartyID xmlns=\"\">" +
									"<core:Ident>5555551212</core:Ident>" +
								"</core:PartyID>" +
								"<core:ListOfIdentifier xmlns=\"\">" +
									"<core:Identifier>" +
										"<core:Ident>5555551212</core:Ident>" +
									"</core:Identifier>" +
								"</core:ListOfIdentifier>" +
								"<core:NameAddress xmlns=\"\">" +
									"<core:Name1>Main</core:Name1>" +
									"<core:Street>123 Main Street</core:Street>" +
									"<core:StreetSupplement1>Unit D</core:StreetSupplement1>" +
									"<core:StreetSupplement2>2nd Floor</core:StreetSupplement2>" +
									"<core:PostalCode>Z1Z 1Z1</core:PostalCode>" +
									"<core:City>Burlington</core:City>" +
									"<core:County>Halton</core:County>" +
									"<core:Region>" +
										"<core:RegionCoded>Other</core:RegionCoded>" +
										"<core:RegionCodedOther>Ontario</core:RegionCodedOther>" +
									"</core:Region>" +
									"<core:Country>" +
										"<core:CountryCoded>Other</core:CountryCoded>" +
										"<core:CountryCodedOther>Canada</core:CountryCodedOther>" +
									"</core:Country>" +
								"</core:NameAddress>" +
							"</BillToParty>" +
							"<WarehouseParty>" +
								"<core:PartyID xmlns=\"\">" +
									"<core:Ident>MAIN</core:Ident>" +
								"</core:PartyID>" +
								"<core:ListOfIdentifier xmlns=\"\">" +
									"<core:Identifier>" +
										"<core:Ident>MAIN</core:Ident>" +
									"</core:Identifier>" +
								"</core:ListOfIdentifier>" +
								"<core:PrimaryContact xmlns=\"\">" +
									"<core:ContactID>" +
										"<core:Ident>Main Contact</core:Ident>" +
									"</core:ContactID>" +
									"<core:ContactName>Contact Name</core:ContactName>" +
									"<core:ListOfContactNumber>" +
										"<core:ContactNumber>" +
											"<core:ContactNumberValue>555-555-1212</core:ContactNumberValue>" +
											"<core:ContactNumberTypeCoded>TelephoneNumber</core:ContactNumberTypeCoded>" +
										"</core:ContactNumber>" +
									"</core:ListOfContactNumber>" +
								"</core:PrimaryContact>" +
							"</WarehouseParty>" +
							"<SoldToParty>" +
								"<core:PartyID xmlns=\"\">" +
									"<core:Ident>55555512121234</core:Ident>" +
								"</core:PartyID>" +
								"<core:ListOfIdentifier xmlns=\"\">" +
									"<core:Identifier>" +
										"<core:Ident>55555512121234</core:Ident>" +
									"</core:Identifier>" +
								"</core:ListOfIdentifier>" +
							"</SoldToParty>" +
							"<ManufacturingParty>" +
								"<core:PartyID xmlns=\"\">" +
									"<core:Ident>123456</core:Ident>" +
								"</core:PartyID>" +
								"<core:ListOfIdentifier xmlns=\"\">" +
									"<core:Identifier>" +
										"<core:Ident>123456</core:Ident>" +
									"</core:Identifier>" +
								"</core:ListOfIdentifier>" +
								"<core:NameAddress xmlns=\"\">" +
									"<core:Name1>Acme Inc</core:Name1>" +
								"</core:NameAddress>" +
								"<core:PrimaryContact xmlns=\"\">" +
									"<core:ContactName>John Smith</core:ContactName>" +
								"</core:PrimaryContact>" +
							"</ManufacturingParty>" +
							"<ListOfPartyCoded>" +
								"<core:PartyCoded xmlns=\"\">" +
									"<core:PartyID>" +
										"<core:Ident>Other</core:Ident>" +
									"</core:PartyID>" +
									"<core:ListOfIdentifier>" +
										"<core:Identifier>" +
											"<core:Ident>Other</core:Ident>" +
										"</core:Identifier>" +
									"</core:ListOfIdentifier>" +
									"<core:NameAddress>" +
										"<core:Name1>Other</core:Name1>" +
									"</core:NameAddress>" +
									"<core:PartyRoleCoded>Other</core:PartyRoleCoded>" +
								"</core:PartyCoded>" +
							"</ListOfPartyCoded>" +
						"</OrderParty>" +
						"<ListOfTransportRouting>" +
							"<core:TransportRouting xmlns=\"\">" +
								"<core:CarrierID>" +
									"<core:Ident>UPSG</core:Ident>" +
								"</core:CarrierID>" +
							"</core:TransportRouting>" +
						"</ListOfTransportRouting>" +
						"<OrderTermsOfDelivery>" +
							"<core:TermsOfDeliveryFunctionCoded xmlns=\"\">Other</core:TermsOfDeliveryFunctionCoded>" +
							"<core:TermsOfDeliveryFunctionCodedOther xmlns=\"\">Not Used</core:TermsOfDeliveryFunctionCodedOther>" +
							"<core:TransportTermsCoded xmlns=\"\">Other</core:TransportTermsCoded>" +
							"<core:TransportTermsCodedOther xmlns=\"\">FreeOnBoard</core:TransportTermsCodedOther>" +
							"<core:ShipmentMethodOfPaymentCoded xmlns=\"\">Other</core:ShipmentMethodOfPaymentCoded>" +
							"<core:ShipmentMethodOfPaymentCodedOther xmlns=\"\">CC</core:ShipmentMethodOfPaymentCodedOther>" +
							"<core:Location xmlns=\"\">" +
								"<core:LocationIdentifier>" +
									"<core:LocID>" +
										"<core:Ident>Other</core:Ident>" +
									"</core:LocID>" +
								"</core:LocationIdentifier>" +
							"</core:Location>" +
							"<core:TermsOfDeliveryDescription xmlns=\"\">Ship Collect</core:TermsOfDeliveryDescription>" +
							"<core:RiskOfLossCoded xmlns=\"\">Other</core:RiskOfLossCoded>" +
							"<core:RiskOfLossCodedOther xmlns=\"\">RiskOfLossInTransitToDestinationIsSellers</core:RiskOfLossCodedOther>" +
							"<core:RiskOfLossDescription xmlns=\"\">Sellers Risk</core:RiskOfLossDescription>" +
						"</OrderTermsOfDelivery>" +
						"<OrderPaymentInstructions>" +
							"<core:PaymentTerms xmlns=\"\">" +
								"<core:NetDueDate>2008-03-01T00:00:33</core:NetDueDate>" +
								"<core:PaymentTerm>" +
									"<core:PaymentTermCoded>Other</core:PaymentTermCoded>" +
									"<core:PaymentTermCodedOther>08</core:PaymentTermCodedOther>" +
									"<core:PaymentTermDescription>2 % 10, Net 30 Days</core:PaymentTermDescription>" +
									"<core:DiscountInformation>" +
										"<core:DiscountPercent>2</core:DiscountPercent>" +
										"<core:DiscountAmount>" +
											"<core:MonetaryAmount>20</core:MonetaryAmount>" +
										"</core:DiscountAmount>" +
										"<core:DiscountDueDate>2008-02-15T00:00:36</core:DiscountDueDate>" +
									"</core:DiscountInformation>" +
								"</core:PaymentTerm>" +
							"</core:PaymentTerms>" +
							"<core:PaymentMethod xmlns=\"\">" +
								"<core:PaymentMeanCoded>Other</core:PaymentMeanCoded>" +
								"<core:PaymentMeanCodedOther>Not Used</core:PaymentMeanCodedOther>" +
							"</core:PaymentMethod>" +
						"</OrderPaymentInstructions>" +
						"<OrderAllowancesOrCharges>" +
							"<core:AllowOrCharge xmlns=\"\">" +
								"<core:IndicatorCoded>Other</core:IndicatorCoded>" +
								"<core:IndicatorCodedOther>C</core:IndicatorCodedOther>" +
								"<core:MethodOfHandlingCoded>Other</core:MethodOfHandlingCoded>" +
								"<core:MethodOfHandlingCodedOther>02</core:MethodOfHandlingCodedOther>" +
								"<core:AllowanceOrChargeDescription>" +
									"<core:ListOfDescription>Freight</core:ListOfDescription>" +
									"<core:ServiceCoded>Other</core:ServiceCoded>" +
									"<core:ServiceCodedOther>D240</core:ServiceCodedOther>" +
								"</core:AllowanceOrChargeDescription>" +
								"<core:TypeOfAllowanceOrCharge>" +
									"<core:PercentageAllowanceOrCharge>" +
										"<core:PercentQualifier>" +
											"<core:PercentQualifierCoded>Other</core:PercentQualifierCoded>" +
											"<core:PercentQualifierCodedOther>Other</core:PercentQualifierCodedOther>" +
										"</core:PercentQualifier>" +
										"<core:Percent>25</core:Percent>" +
									"</core:PercentageAllowanceOrCharge>" +
								"</core:TypeOfAllowanceOrCharge>" +
							"</core:AllowOrCharge>" +
						"</OrderAllowancesOrCharges>" +
						"<OrderHeaderNote>Not Used</OrderHeaderNote>" +
						"<SpecialHandling>" +
							"<core:SpecialHandlingCoded xmlns=\"\">Other</core:SpecialHandlingCoded>" +
							"<core:SpecialHandlingCodedOther xmlns=\"\">ClearedForExport</core:SpecialHandlingCodedOther>" +
							"<core:SpecialHandlingNote xmlns=\"\">Special Handling Notes</core:SpecialHandlingNote>" +
						"</SpecialHandling>" +
						"<ListOfStructuredNote>" +
							"<core:StructuredNote xmlns=\"\">" +
								"<core:GeneralNote>Order Header Notes</core:GeneralNote>" +
								"<core:NoteID>Other</core:NoteID>" +
							"</core:StructuredNote>" +
						"</ListOfStructuredNote>" +
						"<ListOfNameValueSet>" +
							"<core:NameValueSet xmlns=\"\">" +
								"<core:SetName>HeaderReferences</core:SetName>" +
								"<core:SetID>Other</core:SetID>" +
								"<core:ListOfNameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>MerchType</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>MerchTypeDescription</core:Name>" +
										"<core:Value>Other</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Department</core:Name>" +
										"<core:Value>Other</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>DepartmentDescription</core:Name>" +
										"<core:Value>Other</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>PromotionCode</core:Name>" +
										"<core:Value>Other</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>PromotionStartDate</core:Name>" +
										"<core:Value>Other</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>FOBDescription</core:Name>" +
										"<core:Value>Other</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>TermsNetDays</core:Name>" +
										"<core:Value>10</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>TermsDayOfMonth</core:Name>" +
										"<core:Value>15</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>TermsBasisDateCoded</core:Name>" +
										"<core:Value>3</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>TermsAmountSubjectToDiscount</core:Name>" +
										"<core:Value>150</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>TermsDeferredDueDate</core:Name>" +
										"<core:Value>2008-03-01T00:00:33</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>TermsDeferredAmount</core:Name>" +
										"<core:Value>170</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>TermsPercentOfInvoicePayable</core:Name>" +
										"<core:Value>97</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>OrderWeight</core:Name>" +
										"<core:Value>150</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>OrderWeightUofM</core:Name>" +
										"<core:Value>LB</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>SalesTerritory</core:Name>" +
										"<core:Value>NE</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>POType</core:Name>" +
										"<core:Value>SA</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>AdditionalPOType</core:Name>" +
										"<core:Value>SA</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>ProcessHandlingCode</core:Name>" +
										"<core:Value>A</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>CurrencyOwner</core:Name>" +
										"<core:Value>Buyer</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>ExpeditorName</core:Name>" +
										"<core:Value>Other</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>ExpeditorPhone</core:Name>" +
										"<core:Value>Other</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>ClaimTrackingNumber</core:Name>" +
										"<core:Value>Other</core:Value>" +
									" </core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>ContractDescription</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>InvoiceType</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>InvoiceDate</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>InvoiceNumber</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>ReservationNumber</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>RegionCode</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>RegionDescription</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>ArrivalCode</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>SellingChannelID</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Consolidator</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>BatchNumber</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>CustomerReferenceDescription</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>FileIdentifier</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>FileIdentifierDescription</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>InternalOrderNumber</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>InternalOrderNumberDescription</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
								"</core:ListOfNameValuePair>" +
							"</core:NameValueSet>" +
							"<core:NameValueSet xmlns=\"\">" +
								"<core:SetName>TranslationData</core:SetName>" +
								"<core:SetID>Other</core:SetID>" +
								"<core:ListOfNameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_PartnerType</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_PriceSource</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_ASNFormat</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_ASNContainer</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_TranslationType</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_Status</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_ID</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_Name</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_EXTID</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_EXTName</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_Note</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_Level</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_EDIFlag</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_CustomField1</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_CustomField2</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_CustomField3</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_CustomField4</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Partner_CustomField5</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_OwnerID</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_OwnerType</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_TranslationType</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_Status</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_ID</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_Name</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_EXTID</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_EXTName</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_Note</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_Level</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_EDIFlag</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_CustomField1</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_CustomField2</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_CustomField3</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_CustomField4</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
									"<core:NameValuePair>" +
										"<core:Name>Ship_CustomField5</core:Name>" +
										"<core:Value>Other</core:Value>" +
									"</core:NameValuePair>" +
								"</core:ListOfNameValuePair>" +
							"</core:NameValueSet>" +
						"</ListOfNameValueSet>" +
					"</OrderHeader>" +
					"<OrderDetail>" +
						"<ListOfItemDetail>" +
							"<ItemDetail>" +
								"<BaseItemDetail>" +
									"<LineItemNum>" +
										"<core:BuyerLineItemNum xmlns=\"\">1</core:BuyerLineItemNum>" +
									"</LineItemNum>" +
									"<ItemIdentifiers>" +
										"<core:PartNumbers xmlns=\"\">" +
											"<core:SellerPartNumber>" +
												"<core:PartID>112233</core:PartID>" +
											"</core:SellerPartNumber>" +
											"<core:BuyerPartNumber>" +
												"<core:PartID>112233</core:PartID>" +
											"</core:BuyerPartNumber>" +
											"<core:OtherItemIdentifiers>" +
												"<core:ProductIdentifierCoded>" +
													"<core:ProductIdentifierQualifierCoded>UPC</core:ProductIdentifierQualifierCoded>" +
													"<core:ProductIdentifier>123456789012</core:ProductIdentifier>" +
												"</core:ProductIdentifierCoded>" +
											"</core:OtherItemIdentifiers>" +
										"</core:PartNumbers>" +
										"<core:ItemDescription xmlns=\"\">Widget</core:ItemDescription>" +
									"</ItemIdentifiers>" +
									"<TotalQuantity>" +
										"<core:QuantityValue xmlns=\"\">10</core:QuantityValue>" +
										"<core:UnitOfMeasurement xmlns=\"\">" +
											"<core:UOMCoded>Other</core:UOMCoded>" +
											"<core:UOMCodedOther>EA</core:UOMCodedOther>" +
										"</core:UnitOfMeasurement>" +
									"</TotalQuantity>" +
									"<ListOfQuantityCoded>" +
										"<core:QuantityCoded xmlns=\"\">" +
											"<core:QuantityValue>10</core:QuantityValue>" +
											"<core:UnitOfMeasurement>" +
												"<core:UOMCoded>Other</core:UOMCoded>" +
												"<core:UOMCodedOther>EA</core:UOMCodedOther>" +
											"</core:UnitOfMeasurement>" +
											"<core:QuantityQualifierCoded>Original</core:QuantityQualifierCoded>" +
										"</core:QuantityCoded>" +
									"</ListOfQuantityCoded>" +
									"<ListOfPartyCoded>" +
										"<core:PartyCoded xmlns=\"\">" +
											"<core:PartyID>" +
												"<core:Ident>0551</core:Ident>" +
											"</core:PartyID>" +
											"<core:ListOfIdentifier>" +
												"<core:Identifier>" +
													"<core:Ident>0551</core:Ident>" +
												"</core:Identifier>" +
											"</core:ListOfIdentifier>" +
											"<core:PartyRoleCoded>ShipTo</core:PartyRoleCoded>" +
										"</core:PartyCoded>" +
										"<core:PartyCoded xmlns=\"\">" +
											"<core:PartyID>" +
												"<core:Ident>MAIN</core:Ident>" +
											"</core:PartyID>" +
											"<core:PrimaryContact>" +
												"<core:ContactID>" +
													"<core:Ident>MAIN</core:Ident>" +
												"</core:ContactID>" +
												"<core:ContactName>Warehouse</core:ContactName>" +
												"<core:ListOfContactNumber>" +
													"<core:ContactNumber>" +
														"<core:ContactNumberValue>Other</core:ContactNumberValue>" +
														"<core:ContactNumberTypeCoded>Other</core:ContactNumberTypeCoded>" +
													"</core:ContactNumber>" +
												"</core:ListOfContactNumber>" +
											"</core:PrimaryContact>" +
											"<core:PartyRoleCoded>ShipTo</core:PartyRoleCoded>" +
										"</core:PartyCoded>" +
									"</ListOfPartyCoded>" +
									"<ConditionsOfSale>" +
										"<core:SalesRequirement xmlns=\"\">" +
											"<core:SalesRequirementCoded>Other</core:SalesRequirementCoded>" +
											"<core:SalesRequirementCodedOther>BackOrderIfOutOfStock</core:SalesRequirementCodedOther>" +
										"</core:SalesRequirement>" +
										"<core:SalesActionCoded xmlns=\"\">Other</core:SalesActionCoded>" +
										"<core:SalesActionCodedOther xmlns=\"\">CancelBalanceThatExceedsSalesActionValues</core:SalesActionCodedOther>" +
										"<core:SalesActionValue xmlns=\"\">10</core:SalesActionValue>" +
									"</ConditionsOfSale>" +
								"</BaseItemDetail>" +
								"<PricingDetail>" +
									"<core:ListOfPrice xmlns=\"\">" +
										"<core:Price>" +
											"<core:PricingType>" +
												"<core:PriceTypeCoded>Other</core:PriceTypeCoded>" +
												"<core:PriceTypeCodedOther>NetUnitPrice</core:PriceTypeCodedOther>" +
											"</core:PricingType>" +
											"<core:UnitPrice>" +
												"<core:UnitPriceValue>26.55</core:UnitPriceValue>" +
												"<core:Currency>" +
													"<core:CurrencyCoded>Other</core:CurrencyCoded>" +
													"<core:CurrencyCodedOther>USD</core:CurrencyCodedOther>" +
												"</core:Currency>" +
											"</core:UnitPrice>" +
											"<core:PriceBasisQuantity>" +
												"<core:QuantityValue>25</core:QuantityValue>" +
												"<core:UnitOfMeasurement>" +
													"<core:UOMCoded>Other</core:UOMCoded>" +
													"<core:UOMCodedOther>EA</core:UOMCodedOther>" +
												"</core:UnitOfMeasurement>" +
											"</core:PriceBasisQuantity>" +
										"</core:Price>" +
									"</core:ListOfPrice>" +
									"<core:Tax xmlns=\"\">" +
				      			"<core:TaxTypeCoded xmlns=\"\">Other</core:TaxTypeCoded>" +
				       			"<core:TaxTypeCodedOther xmlns=\"\">" +
				      				"<core:Ident>GST</core:Ident>" +
				      			"</core:TaxTypeCodedOther>" +
				      			"<core:TaxFunctionQualifierCoded xmlns=\"\">Other</core:TaxFunctionQualifierCoded>" +
				      			"<core:TaxFunctionQualifierCodedOther xmlns=\"\">Other</core:TaxFunctionQualifierCodedOther>" +
				      			"<core:TaxCategoryCoded xmlns=\"\">Other</core:TaxCategoryCoded>" +
				      			"<core:TaxCategoryCodedOther xmlns=\"\">Other</core:TaxCategoryCodedOther>" +
				      			"<core:ReasonTaxExemptCoded xmlns=\"\">Other</core:ReasonTaxExemptCoded>" +
				      			"<core:ReasonTaxExemptCodedOther xmlns=\"\">Exempt-ForExport</core:ReasonTaxExemptCodedOther>" +
				      			"<core:TaxPercent xmlns=\"\">5</core:TaxPercent>" +
				      			"<core:TaxAmount xmlns=\"\">5</core:TaxAmount>" +
				      			"<core:TaxLocation xmlns=\"\">" +
				      				"<core:LocationQualifierCoded>Other</core:LocationQualifierCoded>" +
				      				"<core:LocationQualifierCodedOther>SP</core:LocationQualifierCodedOther>" +
				      				"<core:LocationIdentifier>" +
				      					"<core:LocID>" +
				      						"<core:Ident>Ontario</core:Ident>" +
				      					"</core:LocID>" +
				      				"</core:LocationIdentifier>" +
				      			"</core:TaxLocation>" +
									"</core:Tax>" +
									"<core:ItemAllowancesOrCharges xmlns=\"\">" +
										"<core:AllowOrCharge>" +
											"<core:IndicatorCoded>Other</core:IndicatorCoded>" +
											"<core:IndicatorCodedOther>C</core:IndicatorCodedOther>" +
											"<core:MethodOfHandlingCoded>Other</core:MethodOfHandlingCoded>" +
											"<core:MethodOfHandlingCodedOther>02</core:MethodOfHandlingCodedOther>" +
											"<core:AllowanceOrChargeDescription>" +
												"<core:ListOfDescription>Freight</core:ListOfDescription>" +
												"<core:ServiceCoded>Other</core:ServiceCoded>" +
												"<core:ServiceCodedOther>D240</core:ServiceCodedOther>" +
											"</core:AllowanceOrChargeDescription>" +
											"<core:TypeOfAllowanceOrCharge>" +
												"<core:MonetaryValue>" +
													"<core:MonetaryAmount>25</core:MonetaryAmount>" +
													"<core:Currency>" +
														"<core:CurrencyCoded>Other</core:CurrencyCoded>" +
														"<core:CurrencyCodedOther>USD</core:CurrencyCodedOther>" +
													"</core:Currency>" +
												"</core:MonetaryValue>" +
											"</core:TypeOfAllowanceOrCharge>" +
										"</core:AllowOrCharge>" +
									"</core:ItemAllowancesOrCharges>" +
									"<core:LineItemTotal xmlns=\"\">" +
										"<core:MonetaryAmount>500</core:MonetaryAmount>" +
									"</core:LineItemTotal>" +
								"</PricingDetail>" +
								"<SpecialHandling>" +
									"<core:SpecialHandlingNote xmlns=\"\">Line Special Handling Notes</core:SpecialHandlingNote>" +
								"</SpecialHandling>" +
								"<ListOfStructuredNote>" +
									"<core:StructuredNote xmlns=\"\">" +
										"<core:GeneralNote>Line General Note</core:GeneralNote>" +
										"<core:NoteID>Other</core:NoteID>" +
									"</core:StructuredNote>" +
								"</ListOfStructuredNote>" +
								"<ListOfNameValueSet>" +
									"<core:NameValueSet xmlns=\"\">" +
										"<core:SetName>DetailReferences</core:SetName>" +
										"<core:ListOfNameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Color</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Size</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>RetailPrice</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>InnerPack</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>PackUofM</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>RequestedShipDate</core:Name>" +
												"<core:Value>2008-03-01T00:00:33</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>AssemblyLineFeedLocation</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>ReserveAssemblyLineFeedLocation</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>MaterialStorageLocation</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>BarCodeInstructions</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>PackingLevelCode</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>PackingType</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>ReturnableDetails</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>MarkingInstructions</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>ShippingMarks</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>ContainerStatus</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>TypeOfMarking</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>DirectShipPartNumber</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>PackageWeight</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>WeightQualifier</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>WeightUofM</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>WeightUofM</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>ProductGroup</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>PromotionCode</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Department</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>AllowanceorChargeCode</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>ProductDescriptionCode</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>SecurityTagNumber</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>BidNumber</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>WeightAgreementNumber</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>TermsTypeCode</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>TermsBasisDateCode</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>TermsDiscountPercent</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>TermsDiscountDueDate</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>TermsDiscountDaysDue</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>TermsNetDueDate</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>TermsDescription</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>ChangeReasonID</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>ChangePriceIdentifier</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>ChangeUnitPrice</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>ChnageQuantity</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>ChangeUofM</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
										"</core:ListOfNameValuePair>" +
									"</core:NameValueSet>" +
									"<core:NameValueSet>" +
										"<core:SetName>PackageXX</core:SetName>" +
										"<core:ListOfNameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>PackageNumber</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>PackageQuantity</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>TrackingNumber</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>PackageComment</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
										"</core:ListOfNameValuePair>" +
									"</core:NameValueSet>" +
									"<core:NameValueSet>" +
										"<core:SetName>LotXX</core:SetName>" +
										"<core:ListOfNameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>LotOrSerialNumber</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>LotOrSerialQuantity</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>LotOrSerialExpiryDate</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>LotOrSerialComment</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
										"</core:ListOfNameValuePair>" +
									"</core:NameValueSet>" +
									"<core:NameValueSet>" +
										"<core:SetName>TranslationData</core:SetName>" +
										"<core:ListOfNameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_UPCCode</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Weight</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Height</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Length</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Width</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_InnerPack</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_StandardPack</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_TranslationType</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Status</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_ID</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Name</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_EXTID</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_EXTName</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Note</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Level</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_EDIFlag</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_EXTName</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Custom1</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Custom2</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Custom3</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Custom4</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
											"<core:NameValuePair>" +
												"<core:Name>Item_Custom5</core:Name>" +
												"<core:Value>Other</core:Value>" +
											"</core:NameValuePair>" +
										"</core:ListOfNameValuePair>" +
									"</core:NameValueSet>" +
								"</ListOfNameValueSet>" +
							"</ItemDetail>" +
						"</ListOfItemDetail>" +
					"</OrderDetail>" +
					"<OrderSummary>" +
						"<NumberOfLines>1</NumberOfLines>" +
						"<OrderSubTotal>" +
							"<core:MonetaryAmount xmlns=\"\">25</core:MonetaryAmount>" +
						"</OrderSubTotal>" +
						"<OrderTotal>" +
							"<core:MonetaryAmount xmlns=\"\">25</core:MonetaryAmount>" +
						"</OrderTotal>" +
						"<TransportPackagingTotals>" +
							"<core:GrossVolume xmlns=\"\">" +
								"<core:MeasurementValue>25</core:MeasurementValue>" +
								"<core:UnitOfMeasurement>" +
									"<core:UOMCoded>Other</core:UOMCoded>" +
									"<core:UOMCodedOther>CubitFeet</core:UOMCodedOther>" +
								"</core:UnitOfMeasurement>" +
							"</core:GrossVolume>" +
						"</TransportPackagingTotals>" +
					"</OrderSummary>" +
				"</Order>");
		
		return xmlRequest.toString();
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}