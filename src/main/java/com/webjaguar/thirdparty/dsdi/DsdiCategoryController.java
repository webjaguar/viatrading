/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.08.2010
 */

package com.webjaguar.thirdparty.dsdi;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.thirdparty.dsdi.DsdiCategory;;

public class DsdiCategoryController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		List<DsdiCategory> categories = new ArrayList<DsdiCategory>();
		
		if (request.getMethod().equals("POST")) {
			for (String code: request.getParameterValues("code")) {
				DsdiCategory category = new DsdiCategory();
				category.setCode(code);
				try {
					category.setCatId(ServletRequestUtils.getIntParameter(request, code + "_catId"));
				} catch (Exception e) {
					category.setCatId(null);
				}
				try {
					category.setPriceChange(ServletRequestUtils.getDoubleParameter(request, code + "_priceChange"));					
				} catch (Exception e) {
					category.setPriceChange(null);
				}
				categories.add(category);
			}
			this.webJaguar.nonTransactionSafeUpdateDsdiCategories(categories);
		}
		
		categories = this.webJaguar.getDsdiCategories();
		
		Map<String, DsdiCategory> categoryMap = new HashMap<String, DsdiCategory>();
		Iterator<DsdiCategory> iter = categories.iterator();
		while (iter.hasNext()) {
			DsdiCategory category = iter.next();
			if (category.getSub().equals("*")) {
				categoryMap.put(category.getCat().toLowerCase(), category);
			} else {
				if (categoryMap.get(category.getCat().toLowerCase()) != null) {
					categoryMap.get(category.getCat().toLowerCase()).addSub(category);					
				}
				iter.remove();
			}
		}
		
		map.put("categories", categories);		
	
		return new ModelAndView("admin/vendors/dsdi/category", map);
	}
}