/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.15.2007
 */

package com.webjaguar.thirdparty.itext;

import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.PdfCell;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.Payment;
import com.webjaguar.model.Presentation;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.ProductLabelTemplate;
import com.webjaguar.model.PurchaseOrder;
import com.webjaguar.model.PurchaseOrderLineItem;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.domain.Utilities;

public class ITextApi {

	public void createPdfInvoice(File pdfFile, List<Order> orders, List<Customer> customers, HttpServletRequest request, Map<String, Object> map, File baseImageDir, ApplicationContext context)
			throws Exception {

		Document document = new Document();
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
		document.open();
		
		Locale locale = null;
		if(request==null){
			locale = new Locale("en");
		}else{
			locale = RequestContextUtils.getLocale(request);
		}

		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy", locale);
		NumberFormat nf = NumberFormat.getInstance(locale);
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		Font font1 = new Font(Font.HELVETICA, 8, Font.BOLD);
		Font font2 = new Font(Font.HELVETICA, 8, Font.NORMAL);
		Font font3 = new Font(Font.HELVETICA, 8, Font.BOLD, Color.white);
		Font font4 = new Font(Font.HELVETICA, 7, Font.NORMAL);
		Font font5 = new Font(Font.HELVETICA, 8, Font.BOLD, Color.red);

		StringBuffer sbuff = new StringBuffer();

		Map<String, Configuration> siteConfig = (Map) map.get("siteConfig");
		Map<String, Object> gSiteConfig = (Map<String, Object>) map.get("gSiteConfig");
		Map<String, String> countryMap = (Map<String, String>) map.get("countryMap");

		// header and footer images
		File headerImage = new File(baseImageDir, "Layout/pdf_invoice_header.gif");
		File footerImage1 = new File(baseImageDir, "Layout/pdf_invoice_footer_1.gif");
		File footerImage2 = new File(baseImageDir, "Layout/pdf_invoice_footer_2.gif");

		// thumbnail
		File thumbnailDir = new File(baseImageDir, "Product/thumb/");

		int index = 0;
		for (Order order : orders) {
			if (index > 0) {
				document.newPage();
			}
			if (map.get("headerText")!=null && (map.get("headerText").toString() != null || !map.get("headerText").toString().equals(""))) {
				PdfPTable headerText = new PdfPTable(1);
				headerText.setWidthPercentage(100);
				headerText.getDefaultCell().setBorderWidth(0);
				headerText.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
				String headerT = map.get("headerText").toString();
				headerText.addCell(headerT);
				document.add(headerText);
			}

			if (headerImage.exists()) {
				//System.out.println("!-- header image exists");
				document.add(Image.getInstance(headerImage.getAbsolutePath()));
			}

			// customer information
			PdfPTable headerTable = new PdfPTable(3);
			headerTable.setWidthPercentage(100);
			headerTable.getDefaultCell().setBorderWidth(0);
			headerTable.setSpacingAfter(5f);
			PdfPTable table = new PdfPTable(1);
			table.getDefaultCell().setBorderWidth(0);
			table.addCell(new Phrase(context.getMessage("billingInformation", new Object[0], locale) + ":", font1));
			table.addCell(new Paragraph(address(order.getBilling(), countryMap), font2));
			headerTable.addCell(table);
			table = new PdfPTable(1);
			table.getDefaultCell().setBorderWidth(0);
			table.addCell(new Phrase(context.getMessage("shippingInformation", new Object[0], locale) + ":", font1));
			table.addCell(new Paragraph(address(order.getShipping(), countryMap), font2));
			headerTable.addCell(table);

			// order information
			table = new PdfPTable(2);
			table.getDefaultCell().setBorderWidth(0);
			PdfPCell cell = new PdfPCell(new Phrase(context.getMessage("invoice", new Object[0], locale) + " # : ", font2));
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setBorderWidth(0);
			table.addCell(cell);
			table.addCell(new Phrase(order.getOrderId() + "", font1));
			cell = new PdfPCell(new Phrase(context.getMessage("date", new Object[0], locale) + " : ", font2));
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setBorderWidth(0);
			table.addCell(cell);
			table.addCell(new Phrase(dateFormatter.format(order.getDateOrdered()) + "", font2));
			if ((Boolean) gSiteConfig.get("gSALES_REP")) {
				cell = new PdfPCell(new Phrase(context.getMessage("salesRep", new Object[0], locale) + " : ", font2));
				cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell.setBorderWidth(0);
				table.addCell(cell);
				if (map.get("salesRep" + index) != null) {
					table.addCell(new Phrase(((SalesRep) map.get("salesRep" + index)).getName(), font2));
				} else {
					table.addCell("");
				}
				cell = new PdfPCell(new Phrase(context.getMessage("processedBy", new Object[0], locale) + " : ", font2));
				cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell.setBorderWidth(0);
				table.addCell(cell);
				if (map.get("salesRepProcessedBy" + index) != null) {
					table.addCell(new Phrase(((SalesRep) map.get("salesRepProcessedBy" + index)).getName(), font2));
				} else {
					table.addCell("");
				}
			}
			Customer customer = customers.get(index);
			if (customer != null && customer.getAccountNumber() != null && customer.getAccountNumber().length() > 0) {
				cell = new PdfPCell(new Phrase(context.getMessage("account", new Object[0], locale) + " # : ", font2));
				cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell.setBorderWidth(0);
				table.addCell(cell);
				table.addCell(new Phrase(customer.getAccountNumber(), font2));
			}
			headerTable.addCell(table);
			document.add(headerTable);

			// customer's email
			if (customer != null) {
				table = new PdfPTable(1);
				table.getDefaultCell().setBorderWidth(0);
				table.setWidthPercentage(100);
				table.setSpacingAfter(5f);
				Phrase phrase = new Phrase();
				phrase.add(new Phrase(context.getMessage("emailAddress", new Object[0], locale), font1));
				phrase.add(new Phrase(": " + customer.getUsername(), font2));
				table.addCell(phrase);
				document.add(table);
			}

			// purchase order
			if (order.getPurchaseOrder() != null && order.getPurchaseOrder().length() > 0) {
				table = new PdfPTable(1);
				table.getDefaultCell().setBorderWidth(0);
				table.setWidthPercentage(100);
				table.setSpacingAfter(5f);
				Phrase phrase = new Phrase();
				phrase.add(new Phrase(context.getMessage("purchaseOrder", new Object[0], locale), font1));
				phrase.add(new Phrase(": " + order.getPurchaseOrder(), font2));
				table.addCell(phrase);
				document.add(table);
			}

			// float widths[] = {1f, 2f, 4f, 1f, 1.5f, 1.5f};
			List<Float> columns = new ArrayList<Float>();
			columns.add(0.7f); // line#
			columns.add(2f); // sku
			columns.add(4f); // name
			for (ProductField productField : (List<ProductField>) map.get("productFieldsHeader" + index)) {
				columns.add(2f);
			}
			columns.add(1f); // qty
			if (order.ishasPacking()) {
				columns.add(1f);
			}
			if (order.ishasContent()) {
				columns.add(1f);
			}
			columns.add(1.5f); // price
			columns.add(1.5f); // total
			float widths[] = new float[columns.size()];
			for (int i = 0; i < columns.size(); i++) {
				widths[i] = columns.get(i);
			}

			int cols = widths.length;
			PdfPTable lineItemsTable = new PdfPTable(widths);
			lineItemsTable.setWidthPercentage(100);

			// create headers
			List<String> headers = new ArrayList<String>();
			headers.add("Line#");
			headers.add(context.getMessage("productSku", new Object[0], locale));
			headers.add(context.getMessage("productName", new Object[0], locale));
			for (ProductField productField : (List<ProductField>) map.get("productFieldsHeader" + index)) {
				headers.add(productField.getName());
			}
			headers.add("Qty");
			if (order.ishasPacking()) {
				headers.add(context.getMessage("packing", new Object[0], locale));
			}
			if (order.ishasContent()) {
				headers.add(context.getMessage("content", new Object[0], locale));
			}
			headers.add(context.getMessage("productPrice", new Object[0], locale));
			headers.add("Total");
			for (String header : headers) {
				cell = new PdfPCell(new Phrase(header, font3));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell.setBackgroundColor(Color.black);
				lineItemsTable.addCell(cell);
			}

			// line items
			int lineNum = 0;
			for (LineItem lineItem : order.getLineItems()) {
				cell = new PdfPCell(new Phrase(++lineNum + "", font2));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				lineItemsTable.addCell(cell);

				table = new PdfPTable(3);
				table.getDefaultCell().setBorderWidth(0);
				// thumbnail
				boolean withThumb = false;
				if (lineItem.getProduct().getThumbnail() != null && siteConfig.get("SHOW_IMAGE_ON_INVOICE").getValue().equals("true")) {
					if (lineItem.getProduct().getThumbnail().isAbsolute()) {
						try {
							table.addCell(Image.getInstance(new URL(lineItem.getProduct().getThumbnail().getImageUrl())));
							withThumb = true;
						} catch (Exception e) {
							// do nothing
						}
					} else {
						File thumbnail = new File(thumbnailDir, lineItem.getProduct().getThumbnail().getImageUrl());
						if (thumbnail.exists()) {
							table.addCell(Image.getInstance(thumbnail.getAbsolutePath()));
							withThumb = true;
						}
					}
				}
				cell = new PdfPCell(new Phrase(lineItem.getProduct().getSku(), font2));
				if (withThumb) {
					cell.setColspan(2);
				} else {
					cell.setColspan(3);
				}
				cell.setBorder(0);
				table.addCell(cell);
				lineItemsTable.addCell(table);

				table = new PdfPTable(1);
				table.getDefaultCell().setBorderWidth(0);
				table.addCell(new Phrase(lineItem.getProduct().getName(), font2));
				sbuff = new StringBuffer();
				for (ProductAttribute productAttribute : lineItem.getProductAttributes()) {
					sbuff.append(" - "
							+ productAttribute.getOptionName()
							+ ": "
							+ productAttribute.getValueName()
							+ productAttribute.getOptionPriceOriginal()
							+ ((productAttribute.getOptionPriceMessageF() == null) ? "" : context.getMessage(productAttribute.getOptionPriceMessageF(), new Object[0],
									locale)) + "\n");
				}
				if (sbuff.length() > 0) {
					table.addCell(new Paragraph(sbuff.toString(), font4));
				}
				lineItemsTable.addCell(table);
				for (ProductField productField1 : (List<ProductField>) map.get("productFieldsHeader" + index)) {
					cell = new PdfPCell(new Phrase(""));
					for (ProductField productField2 : lineItem.getProductFields()) {
						if (productField1.getId().compareTo(productField2.getId()) == 0) {
							cell = new PdfPCell(new Phrase(productField2.getValue(), font2));
							break;
						}
					}
					lineItemsTable.addCell(cell);
				}
				String casePack = ((Integer) lineItem.getPriceCasePackQty() != null) ? "\n (" + lineItem.getPriceCasePackQty() + " per " + siteConfig.get("PRICE_CASE_PACK_UNIT_TITLE").getValue()
						+ " )" : "";
				cell = new PdfPCell(new Phrase(lineItem.getQuantity() + casePack, font2));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				lineItemsTable.addCell(cell);
				if (order.ishasPacking()) {
					cell = new PdfPCell(new Phrase(lineItem.getProduct().getPacking(), font2));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					lineItemsTable.addCell(cell);
				}
				if (order.ishasContent()) {
					if (lineItem.getProduct().getCaseContent() != null) {
						cell = new PdfPCell(new Phrase(lineItem.getProduct().getCaseContent() + "", font2));
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						lineItemsTable.addCell(cell);
					} else {
						lineItemsTable.addCell("");
					}
				}
				if (lineItem.getUnitPrice() != null) {
					cell = new PdfPCell(new Phrase(nf.format(lineItem.getUnitPrice()), font2));
					cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
					lineItemsTable.addCell(cell);
				} else {
					lineItemsTable.addCell("");
				}
				if (lineItem.getTotalPrice() != null) {
					cell = new PdfPCell(new Phrase(nf.format(lineItem.getTotalPrice()), font2));
					cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
					lineItemsTable.addCell(cell);
				} else {
					lineItemsTable.addCell("");
				}
			}

			// separator
			cell = new PdfPCell(new Phrase(" "));
			cell.setColspan(cols);
			cell.setBackgroundColor(Color.gray);
			lineItemsTable.addCell(cell);

			// sub total
			cell = new PdfPCell(new Phrase(context.getMessage("subTotal", new Object[0], locale) + ":", font2));
			cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
			cell.setColspan(cols - 1);
			lineItemsTable.addCell(cell);
			cell = new PdfPCell(new Phrase(nf.format(order.getSubTotal()), font2));
			cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
			lineItemsTable.addCell(cell);

			// earnede credits
			if ((Boolean) gSiteConfig.get("gBUDGET") && siteConfig.get("BUDGET_ADD_PARTNER").getValue().equalsIgnoreCase("true") && order.getBudgetEarnedCredits() != null) {
				cell = new PdfPCell(new Phrase("Earned Credits :", font5));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				cell.setColspan(cols - 1);
				lineItemsTable.addCell(cell);
				cell = new PdfPCell(new Phrase(nf.format(order.getBudgetEarnedCredits()), font5));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				lineItemsTable.addCell(cell);
			}

			// promo
			if (order.getPromo() != null && order.getPromo().getTitle() != null) {
				sbuff = new StringBuffer();
				sbuff.append(context.getMessage("discountForPromoCode", new Object[0], locale));
				sbuff.append(" " + order.getPromo().getTitle());
				if (order.getPromo().isPercent()) {
					sbuff.append(" (" + order.getPromo().getDiscount() + "%)");
				} else {
					sbuff.append(" (" + context.getMessage(siteConfig.get("CURRENCY").getValue() + "_charCode", new Object[0], locale) + order.getPromo().getDiscount()
							+ ")");
				}
				cell = new PdfPCell(new Phrase(sbuff.toString(), font5));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				cell.setColspan(cols - 1);
				lineItemsTable.addCell(cell);
				if (order.getPromo().isPercent()) {
					cell = new PdfPCell(new Phrase("(" + nf.format(order.getSubTotal() * order.getPromo().getDiscount() / 100.00) + ")", font5));
				} else {
					cell = new PdfPCell(new Phrase("(" + nf.format(order.getPromo().getDiscount()) + ")", font5));
				}
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				lineItemsTable.addCell(cell);
			}

			if (order.getLineItemPromos() != null) {
				Iterator<String> iter = order.getLineItemPromos().keySet().iterator();
				while (iter.hasNext()) {
					String promoCode = iter.next();
					sbuff = new StringBuffer();
					sbuff.append(context.getMessage("discountForPromoCode", new Object[0], locale));
					sbuff.append(" " + promoCode);

					cell = new PdfPCell(new Phrase(sbuff.toString(), font5));
					cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
					cell.setColspan(cols - 1);
					lineItemsTable.addCell(cell);
					cell = new PdfPCell(new Phrase("(" + nf.format(order.getLineItemPromos().get(promoCode)) + ")", font5));
					cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
					lineItemsTable.addCell(cell);
				}
			}

			if (order.isTaxOnShipping()) {
				// shipping & handling
				cell = new PdfPCell(new Phrase(context.getMessage("shippingHandling", new Object[0], locale) + " (" + order.getShippingMethodPDF() + "):", font2));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				cell.setColspan(cols - 1);
				lineItemsTable.addCell(cell);
				if (order.getShippingCost() != null) {
					cell = new PdfPCell(new Phrase(nf.format(order.getShippingCost()), font2));
					cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
					lineItemsTable.addCell(cell);
				} else {
					lineItemsTable.addCell("");
				}

				// tax
				cell = new PdfPCell(new Phrase(context.getMessage("tax", new Object[0], locale) + ":", font2));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				cell.setColspan(cols - 1);
				lineItemsTable.addCell(cell);
				if (order.getTax() != null) {
					cell = new PdfPCell(new Phrase(nf.format(order.getTax()), font2));
					cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
					lineItemsTable.addCell(cell);
				} else {
					lineItemsTable.addCell("");
				}
			} else {
				// tax
				cell = new PdfPCell(new Phrase(context.getMessage("tax", new Object[0], locale) + ":", font2));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				cell.setColspan(cols - 1);
				lineItemsTable.addCell(cell);
				if (order.getTax() != null) {
					cell = new PdfPCell(new Phrase(nf.format(order.getTax()), font2));
					cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
					lineItemsTable.addCell(cell);
				} else {
					lineItemsTable.addCell("");
				}

				// shipping & handling
				cell = new PdfPCell(new Phrase(context.getMessage("shippingHandling", new Object[0], locale) + " (" + order.getShippingMethodPDF() + "):", font2));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				cell.setColspan(cols - 1);
				lineItemsTable.addCell(cell);
				if (order.getShippingCost() != null) {
					cell = new PdfPCell(new Phrase(nf.format(order.getShippingCost()), font2));
					cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
					lineItemsTable.addCell(cell);
				} else {
					lineItemsTable.addCell("");
				}
			}
			// credit (giftCard)
			if (order.getCreditUsed() != null) {
				cell = new PdfPCell(new Phrase(context.getMessage("credit", new Object[0], locale), font5));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				cell.setColspan(cols - 1);
				lineItemsTable.addCell(cell);
				cell = new PdfPCell(new Phrase(nf.format(order.getCreditUsed()), font5));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				lineItemsTable.addCell(cell);
			}

			// buysafe bond cost
			if (order.getBondCost() != null) {
				cell = new PdfPCell(new Phrase(context.getMessage("buySafeBondGuarntee", new Object[0], locale), font5));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				cell.setColspan(cols - 1);
				lineItemsTable.addCell(cell);
				cell = new PdfPCell(new Phrase(nf.format(order.getBondCost()), font2));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				lineItemsTable.addCell(cell);
			}

			// payment fee
			if (order.getCcFee() != null && order.getCcFee() != 0) {
				cell = new PdfPCell(new Phrase(context.getMessage("creditCardFee", new Object[0], locale) + ":", font2));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				cell.setColspan(cols - 1);
				lineItemsTable.addCell(cell);
				cell = new PdfPCell(new Phrase(nf.format(order.getCcFee()), font2));
				cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
				lineItemsTable.addCell(cell);
			}

			// grand total
			cell = new PdfPCell(new Phrase(context.getMessage("grandTotal", new Object[0], locale) + ":", font2));
			cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
			cell.setColspan(cols - 1);
			lineItemsTable.addCell(cell);
			cell = new PdfPCell(new Phrase(context.getMessage(siteConfig.get("CURRENCY").getValue() + "_charCode", new Object[0], locale)
					+ nf.format(order.getGrandTotal()), font1));
			cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
			cell.setBackgroundColor(Color.yellow);
			lineItemsTable.addCell(cell);

			// balance
			cell = new PdfPCell(new Phrase(context.getMessage("balance", new Object[0], locale) + ":", font2));
			cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
			cell.setColspan(cols - 1);
			lineItemsTable.addCell(cell);
			if (order.getAmountPaid() == null || order.getAmountPaid().equals("")) {
				order.setAmountPaid(0.00);
			}
			cell = new PdfPCell(new Phrase(context.getMessage(siteConfig.get("CURRENCY").getValue() + "_charCode", new Object[0], locale)
					+ nf.format(order.getGrandTotal() - order.getAmountPaid()), font1));
			cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
			cell.setBackgroundColor(Color.yellow);
			lineItemsTable.addCell(cell);
			document.add(lineItemsTable);

			// invoice message
			if (siteConfig.get("INVOICE_MESSAGE") != null && siteConfig.get("INVOICE_MESSAGE").getValue().length() > 0) {
				table = new PdfPTable(1);
				table.getDefaultCell().setBorderWidth(0);
				table.setWidthPercentage(100);
				table.setSpacingAfter(5f);
				table.addCell(new Paragraph(siteConfig.get("INVOICE_MESSAGE").getValue(), font2));
				document.add(table);
			}

			// payment method
			table = new PdfPTable(1);
			table.getDefaultCell().setBorderWidth(0);
			table.setWidthPercentage(100);
			table.setSpacingAfter(5f);
			Phrase phrase = new Phrase();
			phrase.add(new Phrase(context.getMessage("paymentMethod", new Object[0], locale), font1));
			phrase.add(new Phrase(": " + order.getPaymentMethod(), font2));
			table.addCell(phrase);
			document.add(table);

			// credit card information
			if (order.getCreditCard() != null) {
				table = new PdfPTable(1);
				table.getDefaultCell().setBorderWidth(0);
				table.setWidthPercentage(100);
				table.setSpacingAfter(5f);
				Phrase phrase2 = new Phrase();
				phrase2.add(new Phrase("Card Type", font1));
				phrase2.add(new Phrase(": " + order.getCreditCard().getType(), font2));
				table.addCell(phrase2);
				Phrase phrase3 = new Phrase();
				phrase3.add(new Phrase("Card Number", font1));
				phrase3.add(new Phrase(": XXXXXXXX" + order.getCreditCard().getNumber().substring(order.getCreditCard().getNumber().length() - 4, order.getCreditCard().getNumber().length()), font2));
				table.addCell(phrase3);
				Phrase phrase4 = new Phrase();
				phrase4.add(new Phrase("Expiration Date", font1));
				phrase4.add(new Phrase(": " + order.getCreditCard().getExpireMonth() + " / " + order.getCreditCard().getExpireYear(), font2));
				table.addCell(phrase4);
				document.add(table);
			}
			// payment history
			if (siteConfig.get("SHOW_PAYMENT_HISTORY_ON_PRINT_FRIENDLY").getValue().equals("true") && order.getPaymentHistory() != null && order.getPaymentHistory().size() > 0) {
				Phrase appPhrase = new Phrase();
				appPhrase.add(new Phrase("Payment History", font1));
				appPhrase.add(new Phrase(": ", font2));
				table.addCell(appPhrase);
				document.add(table);

				PdfPTable paymentTable = new PdfPTable(3);
				paymentTable.getDefaultCell().setBorderWidth(0);
				paymentTable.setWidthPercentage(100);

				PdfPCell paymentCell = new PdfPCell();
				paymentCell.setFixedHeight(250);

				// create headers
				List<String> paymentHeaders = new ArrayList<String>();
				paymentHeaders.add("Date");
				paymentHeaders.add("Amt");
				paymentHeaders.add("Memo");

				for (String header : paymentHeaders) {
					paymentCell = new PdfPCell(new Phrase(header, font2));
					paymentCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					paymentCell.setBorder(0);
					paymentTable.addCell(paymentCell);
				}
				for (Payment paymentHistory : order.getPaymentHistory()) {
					paymentCell = new PdfPCell(new Phrase(paymentHistory.getDate() + "", font2));
					paymentCell.setBorder(0);
					paymentTable.addCell(paymentCell);
					if (paymentHistory.getCancelledAmt() != null) {
						paymentCell = new PdfPCell(new Phrase("Cancelled Amt :" + paymentHistory.getCancelledAmt(), font5));
					} else {
						paymentCell = new PdfPCell(new Phrase(paymentHistory.getAmount() + "", font5));
					}
					paymentCell.setBorder(0);
					paymentTable.addCell(paymentCell);
					paymentCell = new PdfPCell(new Phrase(paymentHistory.getMemo(), font2));
					paymentCell.setBorder(0);
					paymentTable.addCell(paymentCell);
				}
				document.add(paymentTable);
			}
			// customer fields
			List<CustomerField> customerFields = (List<CustomerField>) map.get("customerFields" + index);
			if (!customerFields.isEmpty()) {
				table = new PdfPTable(1);
				table.getDefaultCell().setBorderWidth(0);
				table.setWidthPercentage(100);
				table.setSpacingAfter(5f);
				for (CustomerField customerField : customerFields) {
					phrase = new Phrase();
					phrase.add(new Phrase(customerField.getName(), font2));
					phrase.add(new Phrase(": " + customerField.getValue(), font2));
					table.addCell(phrase);
				}
				document.add(table);
			}

			// special instructions
			if (order.getInvoiceNote() != null && order.getInvoiceNote().length() > 0) {
				table = new PdfPTable(1);
				table.getDefaultCell().setBorderWidth(0);
				table.setWidthPercentage(100);
				table.addCell(new Phrase(siteConfig.get("SPECIAL_INSTRUCTIONS").getValue() + ":", font1));
				table.addCell(new Paragraph(order.getInvoiceNote(), font2));
				document.add(table);
			}

			// Budget Approval history
			if ((Boolean) gSiteConfig.get("gBUDGET") && order.getActionHistory() != null && order.getActionHistory().size() > 0) {

				Phrase appPhrase = new Phrase();
				appPhrase.add(new Phrase("Approval History", font1));
				appPhrase.add(new Phrase(": ", font2));
				table.addCell(appPhrase);
				document.add(table);

				PdfPTable actiontable = new PdfPTable(3);
				actiontable.getDefaultCell().setBorderWidth(0);
				actiontable.setWidthPercentage(100);

				PdfPCell actionCell = new PdfPCell();
				actionCell.setFixedHeight(250);

				// create headers
				List<String> actionHeaders = new ArrayList<String>();
				actionHeaders.add("Approved By");
				actionHeaders.add("Date");
				actionHeaders.add("Approval Note");

				for (String header : actionHeaders) {
					actionCell = new PdfPCell(new Phrase(header, font2));
					actionCell.setHorizontalAlignment(Element.ALIGN_CENTER);
					actiontable.addCell(actionCell);
				}

				for (Order actionHistory : order.getActionHistory()) {
					actionCell = new PdfPCell(new Phrase(actionHistory.getActionByName(), font2));
					actiontable.addCell(actionCell);
					actionCell = new PdfPCell(new Phrase(actionHistory.getActionDate() + "", font2));
					actiontable.addCell(actionCell);
					actionCell = new PdfPCell(new Phrase(actionHistory.getActionNote(), font2));
					actiontable.addCell(actionCell);
				}
				document.add(actiontable);
			}

			// footer image
			if (footerImage1.exists()) {
				document.add(Image.getInstance(footerImage1.getAbsolutePath()));
			}
			if (footerImage2.exists()) {
				document.add(Image.getInstance(footerImage2.getAbsolutePath()));
			}

			index++;

			if (request!=null && request.getParameter("__backToBack") != null && writer.getCurrentPageNumber() % 2 == 1) {
				// add blank page if odd page
				document.newPage();
				document.add(new Phrase(" "));
			}
		}

		document.close();
	}

	public void createPdfPackingList(File pdfFile, Order order, HttpServletRequest request, Map<String, Object> map, File baseImageDir, ApplicationContext context) throws Exception {

		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
		document.open();

		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy", RequestContextUtils.getLocale(request));
		NumberFormat nf = NumberFormat.getInstance(RequestContextUtils.getLocale(request));
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		Font font1 = new Font(Font.HELVETICA, 10, Font.BOLD);
		Font font2 = new Font(Font.HELVETICA, 10, Font.NORMAL);
		Font font3 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.white);
		Font font4 = new Font(Font.HELVETICA, 9, Font.NORMAL);

		StringBuffer sbuff = new StringBuffer();

		Map<String, Configuration> siteConfig = (Map) map.get("siteConfig");
		Map<String, String> countryMap = (Map<String, String>) map.get("countryMap");

		// header image
		File headerImage = new File(baseImageDir, "Layout/pdf_packing_header.gif");
		if (headerImage.exists()) {
			document.add(Image.getInstance(headerImage.getAbsolutePath()));
		}

		// shipping information
		PdfPTable headerTable = new PdfPTable(2);
		headerTable.setWidthPercentage(100);
		headerTable.getDefaultCell().setBorderWidth(0);
		headerTable.setSpacingAfter(5f);
		PdfPTable table = new PdfPTable(1);
		table.getDefaultCell().setBorderWidth(0);
		table.addCell(new Phrase("Shipping Information:", font1));
		table.addCell(new Paragraph(address(order.getShipping(), countryMap), font2));
		headerTable.addCell(table);

		// order information
		table = new PdfPTable(2);
		table.getDefaultCell().setBorderWidth(0);
		PdfPCell cell = new PdfPCell(new Phrase(context.getMessage("invoice", new Object[0], RequestContextUtils.getLocale(request)) + " # : ", font2));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorderWidth(0);
		table.addCell(cell);
		table.addCell(new Phrase(order.getOrderId() + "", font1));
		cell = new PdfPCell(new Phrase(context.getMessage("date", new Object[0], RequestContextUtils.getLocale(request)) + " : ", font2));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorderWidth(0);
		table.addCell(cell);
		table.addCell(new Phrase(dateFormatter.format(order.getDateOrdered()) + "", font2));
		cell = new PdfPCell(new Phrase(context.getMessage("shipping", new Object[0], RequestContextUtils.getLocale(request)) + " : ", font2));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorderWidth(0);
		table.addCell(cell);
		if (order.getShippingMethod() != null) {
			table.addCell(new Phrase(order.getShippingMethodPDF() + "", font2));
		} else {
			table.addCell("");
		}
		headerTable.addCell(table);
		document.add(headerTable);

		List<Float> columns = new ArrayList<Float>();
		columns.add(1f); // line#
		columns.add(2f); // sku
		if (map.containsKey("hasSupplierSku")) {
			columns.add(2f); // supplier sku
		}
		columns.add(4f); // name
		for (ProductField productField : (List<ProductField>) map.get("productFieldsHeader")) {
			columns.add(2f);
		}
		columns.add(1f); // qty
		if (order.ishasPacking()) {
			columns.add(2f);
		}
		if (order.ishasContent()) {
			columns.add(2f);
		}
		float widths[] = new float[columns.size()];
		for (int i = 0; i < columns.size(); i++) {
			widths[i] = columns.get(i);
		}

		int cols = widths.length;
		PdfPTable lineItemsTable = new PdfPTable(widths);
		lineItemsTable.setWidthPercentage(100);

		// create headers
		List<String> headers = new ArrayList<String>();
		headers.add("Line#");
		headers.add(context.getMessage("productSku", new Object[0], RequestContextUtils.getLocale(request)));
		if (map.containsKey("hasSupplierSku")) {
			headers.add(context.getMessage("supplier", new Object[0], RequestContextUtils.getLocale(request)) + " " + context.getMessage("sku", new Object[0], RequestContextUtils.getLocale(request)));
		}
		headers.add(context.getMessage("productName", new Object[0], RequestContextUtils.getLocale(request)));
		for (ProductField productField : (List<ProductField>) map.get("productFieldsHeader")) {
			headers.add(productField.getName());
		}
		headers.add("Qty");
		if (order.ishasPacking()) {
			headers.add(context.getMessage("packing", new Object[0], RequestContextUtils.getLocale(request)));
		}
		if (order.ishasContent()) {
			headers.add(context.getMessage("content", new Object[0], RequestContextUtils.getLocale(request)));
		}
		for (String header : headers) {
			cell = new PdfPCell(new Phrase(header, font3));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(Color.black);
			lineItemsTable.addCell(cell);
		}

		// line items
		int lineNum = 0;
		for (LineItem lineItem : order.getLineItems()) {
			cell = new PdfPCell(new Phrase(++lineNum + "", font2));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			lineItemsTable.addCell(cell);
			lineItemsTable.addCell(new Phrase(lineItem.getProduct().getSku(), font2));
			if (map.containsKey("hasSupplierSku")) {
				lineItemsTable.addCell(new Phrase(lineItem.getSupplier().getSupplierSku(), font2));
			}
			table = new PdfPTable(1);
			table.getDefaultCell().setBorderWidth(0);
			table.addCell(new Phrase(lineItem.getProduct().getName(), font2));
			sbuff = new StringBuffer();
			for (ProductAttribute productAttribute : lineItem.getProductAttributes()) {
				sbuff.append(" - "
						+ productAttribute.getOptionName()
						+ ": "
						+ productAttribute.getValueName()
						+ productAttribute.getOptionPriceOriginal()
						+ ((productAttribute.getOptionPriceMessageF() == null) ? "" : context.getMessage(productAttribute.getOptionPriceMessageF(), new Object[0],
								RequestContextUtils.getLocale(request))) + "\n");
			}
			if (sbuff.length() > 0) {
				table.addCell(new Paragraph(sbuff.toString(), font4));
			}
			lineItemsTable.addCell(table);
			for (ProductField productField1 : (List<ProductField>) map.get("productFieldsHeader")) {
				cell = new PdfPCell(new Phrase(""));
				for (ProductField productField2 : lineItem.getProductFields()) {
					if (productField1.getId().compareTo(productField2.getId()) == 0) {
						cell = new PdfPCell(new Phrase(productField2.getValue(), font2));
						break;
					}
				}
				lineItemsTable.addCell(cell);
			}
			String casePack = ((Integer) lineItem.getPriceCasePackQty() != null) ? "\n (" + lineItem.getPriceCasePackQty() + " per " + siteConfig.get("PRICE_CASE_PACK_UNIT_TITLE").getValue() + " )"
					: "";
			cell = new PdfPCell(new Phrase(lineItem.getQuantity() + casePack, font2));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			lineItemsTable.addCell(cell);
			if (order.ishasPacking()) {
				cell = new PdfPCell(new Phrase(lineItem.getProduct().getPacking(), font2));
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				lineItemsTable.addCell(cell);
			}
			if (order.ishasContent()) {
				if (lineItem.getProduct().getCaseContent() != null) {
					cell = new PdfPCell(new Phrase(lineItem.getProduct().getCaseContent() + "", font2));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					lineItemsTable.addCell(cell);
				} else {
					lineItemsTable.addCell("");
				}
			}
		}

		// separator
		cell = new PdfPCell(new Phrase(" "));
		cell.setColspan(cols);
		cell.setBackgroundColor(Color.gray);
		lineItemsTable.addCell(cell);

		document.add(lineItemsTable);

		// footer image
		File footerImage = new File(baseImageDir, "Layout/pdf_packing_footer_1.gif");
		if (footerImage.exists()) {
			document.add(Image.getInstance(footerImage.getAbsolutePath()));
		}
		footerImage = new File(baseImageDir, "Layout/pdf_packing_footer_2.gif");
		if (footerImage.exists()) {
			document.add(Image.getInstance(footerImage.getAbsolutePath()));
		}

		document.close();
	}

	private String address(Address address, Map<String, String> countryMap) {

		StringBuffer sbuff = new StringBuffer();
		sbuff.append(address.getFirstName() + " " + address.getLastName() + "\n");
		if (address.getCompany() != null && !address.getCompany().equals("")) {
			sbuff.append(address.getCompany() + "\n");
		}
		sbuff.append(address.getAddr1() + "\n");
		if (address.getAddr2() != null && !address.getAddr2().equals("")) {
			sbuff.append(address.getAddr2() + "\n");
		}
		sbuff.append(address.getCity() + ", " + address.getStateProvince() + " " + address.getZip() + "\n");
		if (countryMap.containsKey(address.getCountry())) {
			sbuff.append(countryMap.get(address.getCountry()) + "\n");
		} else {
			sbuff.append(address.getCountry() + "\n");
		}
		if (address.getPhone() != null && !address.getPhone().equals("")) {
			sbuff.append("Tel: " + address.getPhone() + "\n");
		}
		if (address.getCellPhone() != null && !address.getCellPhone().equals("")) {
			sbuff.append("Cel: " + address.getCellPhone() + "\n");
		}
		if (address.getFax() != null && !address.getFax().equals("")) {
			sbuff.append("Fax: " + address.getFax() + "\n");
		}
		return sbuff.toString();
	}

	// Purchase Order
	public void createPdfPO(File pdfFile, PurchaseOrder purchaseOrder, HttpServletRequest request, Map<String, Object> map, File baseImageDir, ApplicationContext context) throws Exception {

		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
		document.open();

		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy", RequestContextUtils.getLocale(request));
		NumberFormat nf = NumberFormat.getInstance(RequestContextUtils.getLocale(request));
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		Font font1 = new Font(Font.HELVETICA, 10, Font.BOLD);
		Font font2 = new Font(Font.HELVETICA, 10, Font.NORMAL);
		Font font3 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.white);
		Font font4 = new Font(Font.HELVETICA, 9, Font.NORMAL);
		Font font5 = new Font(Font.HELVETICA, 10, Font.BOLD, Color.red);

		StringBuffer sbuff = new StringBuffer();

		Map<String, Configuration> siteConfig = (Map) map.get("siteConfig");
		Map gSiteConfig = (Map) map.get("gSiteConfig");
		Map<String, String> countryMap = (Map<String, String>) map.get("countryMap");

		// header image
		File headerImage = new File(baseImageDir, "Layout/pdf_po_header.gif");
		if (headerImage.exists()) {
			document.add(Image.getInstance(headerImage.getAbsolutePath()));
		}

		// customer information
		PdfPTable headerTable = new PdfPTable(3);
		headerTable.setWidthPercentage(100);
		headerTable.getDefaultCell().setBorderWidth(0);
		headerTable.setSpacingAfter(5f);
		PdfPTable table = new PdfPTable(1);
		table.getDefaultCell().setBorderWidth(0);
		table.addCell(new Phrase(context.getMessage("supplier", new Object[0], RequestContextUtils.getLocale(request)) + " : ", font1));
		table.addCell(new Paragraph(purchaseOrder.getBillingAddress(), font2));
		headerTable.addCell(table);
		table = new PdfPTable(1);
		table.getDefaultCell().setBorderWidth(0);
		table.addCell(new Phrase(context.getMessage("shipTo", new Object[0], RequestContextUtils.getLocale(request)) + " : ", font1));
		table.addCell(new Paragraph(purchaseOrder.getShippingAddress(), font2));
		headerTable.addCell(table);

		// order information
		table = new PdfPTable(2);
		table.getDefaultCell().setBorderWidth(0);
		PdfPCell cell = null;
		if (purchaseOrder.isDropShip())
			cell = new PdfPCell(new Phrase(context.getMessage("dropShipNumber", new Object[0], RequestContextUtils.getLocale(request)) + " : ", font2));
		else
			cell = new PdfPCell(new Phrase(context.getMessage("poNumber", new Object[0], RequestContextUtils.getLocale(request)) + " : ", font2));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorderWidth(0);
		table.addCell(cell);
		table.addCell(new Phrase(purchaseOrder.getPoNumber() + "", font1));

		cell = new PdfPCell(new Phrase(context.getMessage("date", new Object[0], RequestContextUtils.getLocale(request)) + " : ", font2));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorderWidth(0);
		table.addCell(cell);
		table.addCell(new Phrase(dateFormatter.format(purchaseOrder.getCreated()) + "", font2));

		if (purchaseOrder.getDueDate() != null) {
			cell = new PdfPCell(new Phrase(context.getMessage("dueDate", new Object[0], RequestContextUtils.getLocale(request)) + " : ", font2));
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setBorderWidth(0);
			table.addCell(cell);
			table.addCell(new Phrase(dateFormatter.format(purchaseOrder.getDueDate()) + "", font2));
		}

		cell = new PdfPCell(new Phrase(context.getMessage("accountNumber", new Object[0], RequestContextUtils.getLocale(request)) + " : ", font2));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorderWidth(0);
		table.addCell(cell);
		table.addCell(new Phrase(purchaseOrder.getSupplier().getAccountNumber(), font2));

		cell = new PdfPCell(new Phrase(context.getMessage("orderBy", new Object[0], RequestContextUtils.getLocale(request)) + " : ", font2));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorderWidth(0);
		table.addCell(cell);
		table.addCell(new Phrase(purchaseOrder.getOrderBy(), font2));

		cell = new PdfPCell(new Phrase(context.getMessage("shipVia", new Object[0], RequestContextUtils.getLocale(request)) + " : ", font2));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setBorderWidth(0);
		table.addCell(cell);
		table.addCell(new Phrase(purchaseOrder.getShipVia(), font2));

		headerTable.addCell(table);
		document.add(headerTable);

		// float widths[] = {1f, 2f, 4f, 1f, 1.5f, 1.5f};
		List<Float> columns = new ArrayList<Float>();
		columns.add(1f); // line#
		columns.add(2f); // sku
		columns.add(4f); // name
		columns.add(2f); // supplier sku
		columns.add(1f); // qty
		columns.add(1.5f); // price
		columns.add(1.5f); // total
		float widths[] = new float[columns.size()];
		for (int i = 0; i < columns.size(); i++) {
			widths[i] = columns.get(i);
		}

		int cols = widths.length;
		PdfPTable lineItemsTable = new PdfPTable(widths);
		lineItemsTable.setWidthPercentage(100);

		// create headers
		List<String> headers = new ArrayList<String>();
		headers.add("Line#");
		headers.add(context.getMessage("productSku", new Object[0], RequestContextUtils.getLocale(request)));
		headers.add(context.getMessage("productName", new Object[0], RequestContextUtils.getLocale(request)));
		headers.add(context.getMessage("supplierSku", new Object[0], RequestContextUtils.getLocale(request)));
		headers.add("Qty");
		headers.add(context.getMessage("productPrice", new Object[0], RequestContextUtils.getLocale(request)));
		headers.add("total");
		for (String header : headers) {
			cell = new PdfPCell(new Phrase(header, font3));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(Color.black);
			lineItemsTable.addCell(cell);
		}

		// line items
		int lineNum = 0;
		for (PurchaseOrderLineItem lineItem : purchaseOrder.getPoLineItems()) {
			cell = new PdfPCell(new Phrase(++lineNum + "", font2));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			lineItemsTable.addCell(cell);
			lineItemsTable.addCell(new Phrase(lineItem.getSku(), font2));
			table = new PdfPTable(1);
			table.getDefaultCell().setBorderWidth(0);
			if (purchaseOrder.getOrderId() != null) {
				table.addCell(new Phrase(lineItem.getProduct().getName(), font2));
			} else {
				table.addCell(new Phrase(lineItem.getProductName(), font2));
			}
			if (lineItem.getProductAttributes() != null) {
				sbuff = new StringBuffer();
				for (ProductAttribute productAttribute : lineItem.getProductAttributes()) {
					sbuff.append(" - "
							+ productAttribute.getOptionName()
							+ ": "
							+ productAttribute.getValueName()
							+ productAttribute.getOptionPriceOriginal()
							+ ((productAttribute.getOptionPriceMessageF() == null) ? "" : context.getMessage(productAttribute.getOptionPriceMessageF(), new Object[0],
									RequestContextUtils.getLocale(request))) + "\n");
				}
				if (sbuff.length() > 0) {
					table.addCell(new Paragraph(sbuff.toString(), font4));
				}
			}

			lineItemsTable.addCell(table);
			lineItemsTable.addCell(new Phrase(lineItem.getSupplierSku(), font2));
			lineItemsTable.addCell(new Phrase(lineItem.getQty() + "", font2));
			lineItemsTable.addCell(new Phrase(nf.format(lineItem.getCost()), font2));
			lineItemsTable.addCell(new Phrase(nf.format(lineItem.getTotalCost()), font2));

		}

		// separator
		cell = new PdfPCell(new Phrase(" "));
		cell.setColspan(cols);
		cell.setBackgroundColor(Color.gray);
		lineItemsTable.addCell(cell);

		// sub total
		cell = new PdfPCell(new Phrase(context.getMessage("subTotal", new Object[0], RequestContextUtils.getLocale(request)) + ":", font2));
		cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
		cell.setColspan(cols - 1);
		lineItemsTable.addCell(cell);
		cell = new PdfPCell(new Phrase(nf.format(purchaseOrder.getSubTotal()), font2));
		cell.setBackgroundColor(Color.yellow);
		cell.setHorizontalAlignment(Cell.ALIGN_RIGHT);
		lineItemsTable.addCell(cell);

		document.add(lineItemsTable);

		// special instructions
		if (purchaseOrder.getSpecialInstruction() != null && purchaseOrder.getSpecialInstruction().length() > 0) {
			table = new PdfPTable(1);
			table.getDefaultCell().setBorderWidth(0);
			table.setWidthPercentage(100);
			table.addCell(new Phrase(siteConfig.get("PO_SPECIAL_INSTRUCTIONS").getValue() + ":", font1));
			table.addCell(new Paragraph(purchaseOrder.getSpecialInstruction(), font2));
			document.add(table);
		}

		// footer image
		File footerImage = new File(baseImageDir, "Layout/pdf_po_footer_1.gif");
		if (footerImage.exists()) {
			document.add(Image.getInstance(footerImage.getAbsolutePath()));
		}
		footerImage = new File(baseImageDir, "Layout/pdf_po_footer_2.gif");
		if (footerImage.exists()) {
			document.add(Image.getInstance(footerImage.getAbsolutePath()));
		}

		document.close();
	}

	public void createPdfProductLabels(File pdfFile, List<Product> products, HttpServletRequest request, ApplicationContext context, Integer quantity, ProductLabelTemplate productLabelTemplate)
			throws Exception {

		Rectangle pageSize = new Rectangle(433, 287);
		Document document = new Document(pageSize);
		document.setMargins(5f, 5f, 5f, 5f);

		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));

		int gPRODUCT_FIELDS = (Integer) ((Map<String, Object>) request.getAttribute("gSiteConfig")).get("gPRODUCT_FIELDS");

		document.open();

		PdfContentByte cb = writer.getDirectContent();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy", RequestContextUtils.getLocale(request));
		NumberFormat nf = NumberFormat.getInstance(RequestContextUtils.getLocale(request));
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);

		int index = 0;

		for (Product product : products) {
			if (index > 0) {
				document.newPage();
			}
			PdfPTable table = null;
			if (productLabelTemplate != null) {
				table = template8Rows(product, cb, productLabelTemplate, gPRODUCT_FIELDS, request);
			}
			if (table != null) {
				if (quantity == null || quantity <= 0) {
					document.add(table);

				} else {
					for (int i = 0; i < quantity; i++) {
						document.add(table);
						document.newPage();
					}
				}
			} else {
				// message
			}
		}
		index++;
		document.close();
	}

	public void createProcessingListLabels(File pdfFile, List<Product> products, HttpServletRequest request, ApplicationContext context, Integer quantity, ProductLabelTemplate productLabelTemplate, Map<String, Integer> prodQty)
			throws Exception {

		Rectangle pageSize = new Rectangle(433, 287);
		Document document = new Document(pageSize);
		document.setMargins(5f, 5f, 5f, 5f);

		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));

		int gPRODUCT_FIELDS = (Integer) ((Map<String, Object>) request.getAttribute("gSiteConfig")).get("gPRODUCT_FIELDS");

		document.open();

		PdfContentByte cb = writer.getDirectContent();
		NumberFormat nf = NumberFormat.getInstance(RequestContextUtils.getLocale(request));
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);

		int index = 0;

		for (Product product : products) {
			if (index > 0) {
				document.newPage();
			}
			
			PdfPTable table = null;
			if (productLabelTemplate != null) {
				table = template8Rows(product, cb, productLabelTemplate, gPRODUCT_FIELDS, request);
			}

			if (table != null) {

				if (quantity == null || quantity <= 0) {
					document.add(table);

				} else {
					for (int i = 0; i < (quantity*(prodQty.get(product.getName()))) ; i++) {
						document.add(table);
						document.newPage();
					}
				}
			} else {
				// message
			}
		}
		index++;		
		document.close();
	}
	
	private PdfPTable template5Rows(Product product, PdfContentByte cb) {

		Font font1 = new Font(Font.HELVETICA, 8, Font.BOLD);
		Font font2 = new Font(Font.HELVETICA, 12, Font.NORMAL);

		// This table has 5 rows(cells) and in first cell there is a table with two 2 cells and 4th row has a table with 3 cells.
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		table.getDefaultCell().setBorder(0);

		PdfPCell cell;

		float[] colsWidth = { 2f, 1f };
		PdfPTable table1 = new PdfPTable(colsWidth);
		table1.setWidthPercentage(100);
		table1.getDefaultCell().setBorder(0);

		cell = new PdfPCell(new Phrase(product.getSku(), font2));
		cell.setFixedHeight(20f);
		cell.setBorder(0);
		table1.addCell(cell);

		cell = new PdfPCell(new Phrase(product.getField24(), font2));
		cell.setFixedHeight(20f);
		cell.setBorder(0);
		table1.addCell(cell);

		table.addCell(table1);

		cell = new PdfPCell(new Phrase(product.getName(), font2));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setPaddingTop(20f);
		cell.setFixedHeight(60f);
		cell.setBorder(0);
		cell.setColspan(10);
		table.addCell(cell);

		cell = new PdfPCell(generateBarcode(product).createImageWithBarcode(cb, null, null));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setFixedHeight(60f);
		cell.setBorder(0);
		cell.setColspan(10);
		table.addCell(cell);

		float[] colsWidth2 = { 1f, 1f, 1f };
		PdfPTable table2 = new PdfPTable(colsWidth2);
		table2.setWidthPercentage(100);
		table2.getDefaultCell().setBorder(0);

		cell = new PdfPCell(new Phrase("Price: $" + ((product.getPrice1() == null) ? "" : product.getPrice1()) + "", font2));
		cell.setFixedHeight(20f);
		cell.setBorder(0);
		table2.addCell(cell);

		cell = new PdfPCell(new Phrase(product.getField7(), font2));
		cell.setFixedHeight(20f);
		cell.setBorder(0);
		table2.addCell(cell);

		cell = new PdfPCell(new Phrase(product.getField8(), font2));
		cell.setFixedHeight(20f);
		cell.setBorder(0);
		table2.addCell(cell);
		table.addCell(table2);

		cell = new PdfPCell(new Phrase("date", font1));
		cell.setPaddingTop(25f);
		cell.setBorder(0);
		cell.setColspan(10);
		table.addCell(cell);
		return table;
	}

	private PdfPTable template8Rows(Product product, PdfContentByte cb, ProductLabelTemplate productLabelTemplate, int gPRODUCT_FIELDS, HttpServletRequest request) {

		// This table has 8 rows(cells) and in first cell there is a table with two 2 cells and 8th row has a table with 2 cells.
		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		table.getDefaultCell().setBorder(0);

		PdfPCell cell;

		// row 1
		float[] colsWidth = { 2f, 1f };
		PdfPTable table1 = new PdfPTable(colsWidth);
		table1.setWidthPercentage(100);
		table1.getDefaultCell().setBorder(0);

		// row 1-cell1TopLeft
		cell = new PdfPCell(getPhraseValue(productLabelTemplate.getTopLeft(), product, gPRODUCT_FIELDS, request));
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setFixedHeight(30f);
		cell.setBorder(0);
		table1.addCell(cell);

		// row 1-cell2 TopRight
		cell = new PdfPCell(getPhraseValue(productLabelTemplate.getTopRight(), product, gPRODUCT_FIELDS, request));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setFixedHeight(30f);
		cell.setBorder(0);
		table1.addCell(cell);

		table.addCell(table1);

		// row 2
		cell = new PdfPCell(getPhraseValue(productLabelTemplate.getRow1(), product, gPRODUCT_FIELDS, request));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setFixedHeight(30f);
		cell.setBorder(0);
		cell.setColspan(10);
		table.addCell(cell);

		// row 3
		cell = new PdfPCell(getPhraseValue(productLabelTemplate.getRow2(), product, gPRODUCT_FIELDS, request));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setFixedHeight(30f);
		cell.setBorder(0);
		cell.setColspan(10);
		table.addCell(cell);

		// row 4
		cell = new PdfPCell(getPhraseValue(productLabelTemplate.getRow3(), product, gPRODUCT_FIELDS, request));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setFixedHeight(30f);
		cell.setBorder(0);
		cell.setColspan(10);
		table.addCell(cell);

		// row 5
		cell = new PdfPCell(getPhraseValue(productLabelTemplate.getRow4(), product, gPRODUCT_FIELDS, request));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setFixedHeight(30f);
		cell.setBorder(0);
		cell.setColspan(10);
		table.addCell(cell);

		// row 6
		cell = new PdfPCell(getPhraseValue(productLabelTemplate.getRow5(), product, gPRODUCT_FIELDS, request));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setFixedHeight(30f);
		cell.setBorder(0);
		cell.setColspan(10);
		table.addCell(cell);

		// row 7
		cell = new PdfPCell(getPhraseValue(productLabelTemplate.getRow6(), product, gPRODUCT_FIELDS, request));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setFixedHeight(30f);
		cell.setBorder(0);
		cell.setColspan(10);
		table.addCell(cell);

		// row 8
		float[] colsWidth2 = { 1f, 2f };
		PdfPTable table2 = new PdfPTable(colsWidth2);
		table2.setWidthPercentage(100);
		table2.getDefaultCell().setBorder(0);

		// row 8-cell1
		cell = new PdfPCell(getPhraseValue(productLabelTemplate.getBottomLeft(), product, gPRODUCT_FIELDS, request));
		cell.setFixedHeight(10f);
		cell.setBorder(0);
		table2.addCell(cell);

		// row 8-cell2
		cell = new PdfPCell(generateBarcode(product).createImageWithBarcode(cb, null, null));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setFixedHeight(45f);
		cell.setPaddingBottom(0f);
		cell.setBorder(0);
		table2.addCell(cell);

		table.addCell(table2);
		return table;
	}

	private Phrase getPhraseValue(String getValue, Product product, int gPRODUCT_FIELDS, HttpServletRequest request) {

		NumberFormat nf = NumberFormat.getInstance(RequestContextUtils.getLocale(request));
		Format dateFormatter = new SimpleDateFormat("MM dd yyyy", RequestContextUtils.getLocale(request));
		Phrase phrase = new Phrase();

		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);

		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		Phrase p = new Phrase();

		if (getValue != null) {
			phrase = getValue(getValue);
			if (phrase.getContent().contains("#sku#")) {
				p = new Phrase(phrase.getContent().replace("#sku#", product.getSku()), phrase.getFont());
			} else if (phrase.getContent().contains("#name#")) {
				p = new Phrase(phrase.getContent().replace("#name#", product.getName()), phrase.getFont());
			} else if (phrase.getContent().contains("#price1#")) {
				if (product.getPrice1() != null && !product.getPrice1().equals("")) {
					p = new Phrase(phrase.getContent().replace("#price1#", nf.format(product.getPrice1()) + ""), phrase.getFont());
				} else {
					p = new Phrase(phrase.getContent().replace("#price1#", product.getPrice1() + ""), phrase.getFont());
				}
			} else if (phrase.getContent().contains("#date#")) {
				// call dateCode function
				String labelDateFormat = dateCode(request);
				
				p = new Phrase(phrase.getContent().replace("#date#", labelDateFormat), phrase.getFont());
			} else {
				// product fields
				for (int i = 1; i <= gPRODUCT_FIELDS; i++) {
					if (phrase.getContent().contains("#field" + i + "#")) {
						try {
							m = c.getMethod("getField" + i);
							String s  = Utilities.removeChar((String) m.invoke(product, arglist), ",", true, true);

							if (!s.equals(null)){
							    
								/*
								 * 2021-07-15 viatrading product field 10 needs to round to 2 decimals.
								 * */
								
								if (i == 10) {
									p = new Phrase(phrase.getContent().replace("#field" + i + "#",
											((String) m.invoke(product, arglist) != null) ? "$" + nf.format(Double.valueOf(Utilities.removeChar((String) m.invoke(product, arglist), "$", true, true))) : ""));
								} else {
									p = new Phrase(phrase.getContent().replace("#field" + i + "#",
											((String) m.invoke(product, arglist) != null) ? Utilities.removeChar((String) m.invoke(product, arglist), ",", true, true) : ""));
								}								
							}
						} catch (Exception e) {
							// do nothing
						}
					}
				}
			}
		}
		return p;
	}
	
	private String dateCode(HttpServletRequest request){
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yy", RequestContextUtils.getLocale(request));
		String year = dateFormatter.format(new Date()).toString();
		 
		Integer month = Calendar.getInstance().get(Calendar.MONTH);
		String monthCode = "*";
		
		 switch (month) {
		 case 0: monthCode = "A"; break; //Jan
		 case 1: monthCode = "B"; break;//Feb
		 case 2: monthCode = "C"; break;//March
		 case 3: monthCode = "D";break; //April
		 case 4: monthCode = "E";break; //may
		 case 5: monthCode = "F";break;//june
		 case 6: monthCode = "G";break;//july
		 case 7: monthCode = "H";break;//aug
		 case 8: monthCode = "I";break;//sept
		 case 9: monthCode = "J";break;//oct
		 case 10: monthCode = "K";break;//nov
		 case 11: monthCode = "L";break; //dec
         default: 
        	 monthCode = "";
		 }
		 
		Integer date = Calendar.getInstance().get(Calendar.DATE);
        String labelDateFormat = year+ monthCode+"-"+date.toString();	
		return labelDateFormat;	
	}

	private Phrase getValue(String row) {

		Font font = new Font(Font.HELVETICA, 18, Font.NORMAL);
		Phrase phrase = new Phrase(row, font);
		if (row != null && row.contains(",")) {
			String[] str = row.split(",");
			if (str.length > 2) {
				font = new Font(Font.HELVETICA, Float.parseFloat(str[1]), (Integer.parseInt(str[2]) == 1 ? Font.BOLD : Font.NORMAL));
			}
			phrase = new Phrase(str[0], font);
		}
		return phrase;
	}

	private Barcode128 generateBarcode(Product product) {
		Barcode128 code128 = new Barcode128();
		code128.setBarHeight(code128.getSize() * 4.0f);
		if (product.getSku() == null) {
			code128.setCode("0000000000000");
		} else {
			code128.setCode(product.getSku());
		}
		code128.setBaseline(10f);
		code128.setSize(10f);
		// code128.setX(1f);
		return code128;
	}

	public void createCustomerLabels(File pdfFile, HttpServletRequest request, Integer quantity, Customer customer, SalesRep salesRep) throws FileNotFoundException, DocumentException {

		Rectangle pageSize = new Rectangle(433, 287);
		Document document = new Document(pageSize);
		document.setMargins(5f, 5f, 5f, 5f);
		Font font1 = new Font(Font.TIMES_ROMAN, 145, Font.BOLD);
		Font font2 = new Font(Font.HELVETICA, 30, Font.NORMAL);

		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
		document.open();
		document.newPage();

		PdfPTable table = new PdfPTable(1);
		table.setWidthPercentage(100f);
		table.getDefaultCell().setBorder(0);

		PdfPCell cell;

		// row 1
		float[] colsWidth = { 2f, 2f };
		PdfPTable table1 = new PdfPTable(colsWidth);
		table1.setWidthPercentage(100);
		table1.getDefaultCell().setBorder(0);

		// row 1-cell1-TopLeft
		cell = new PdfPCell(new Phrase(customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName(), font2));
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setFixedHeight(35);
		cell.setBorder(0);
		table1.addCell(cell);

		// row 1-cell1-TopRight
		cell = new PdfPCell(new Phrase(new Date() + "", font2));
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setFixedHeight(35);
		cell.setBorder(0);
		table1.addCell(cell);

		table.addCell(table1);

		// row 2
		cell = new PdfPCell(new Phrase("SOLD", font1));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setFixedHeight(180);
		cell.setBorder(0);
		table.addCell(cell);

		// row 3
		float[] colsWidth2 = { 5f, 1f };
		PdfPTable table2 = new PdfPTable(colsWidth2);
		table2.setWidthPercentage(100f);
		table2.getDefaultCell().setBorder(0);

		// row 3-cell1-Left
		cell = new PdfPCell(new Phrase((salesRep != null ? salesRep.getName() : ""), font2));
		// cell.setFixedHeight(50f);
		cell.setBorder(0);
		table2.addCell(cell);

		// row 3-cell1-Right
		cell = new PdfPCell(new Phrase("", font2));
		// cell.setFixedHeight(50f);
		cell.setBorder(0);
		table2.addCell(cell);

		table.addCell(table2);

		// Print based on quantity
		if (quantity == null || quantity <= 0) {
			document.add(table);
		} else {
			for (int i = 0; i < quantity; i++) {
				document.add(table);
				document.newPage();
			}
		}
		document.close();
	}

	public void createSalesRepPdfPresentation(File pdfFile, HttpServletRequest request, List<Product> products, File baseImageDir, Integer noOfProduct, Presentation presentation, SalesRep salesrep,
			Map<String, Object> map, Map<Integer, Double> presentationProductPrice, boolean admin) throws DocumentException, MalformedURLException, IOException {

		Document document = new Document();
		Map<String, Object> gSiteConfig = (Map<String, Object>) map.get("gSiteConfig");
		document.setMargins(10, 10, 10, 10);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		Font font2 = new Font(Font.TIMES_ROMAN, 18, Font.BOLD);
		Font font3 = new Font(Font.TIMES_ROMAN, 10);
		Font font4 = new Font(Font.TIMES_ROMAN, 10, Font.BOLD);
		Font font5 = new Font(Font.TIMES_ROMAN, 6);
		List<Product> products2 = new ArrayList<Product>();
		List<Product> products3 = new ArrayList<Product>();
		// header and footer images
		File headerImage = new File(baseImageDir, "Layout/pdf_presentation_header.gif");
		File footerImage1 = new File(baseImageDir, "Layout/pdf_presentation_footer_1.gif");
		File footerImage2 = new File(baseImageDir, "Layout/pdf_presentation_footer_2.gif");
		PdfPCell headerLogo = new PdfPCell();
		if (headerImage.exists()) {
			Image img = Image.getInstance(headerImage.getAbsolutePath());
			headerLogo.setImage(img);
		}
		headerLogo.setFixedHeight(72);
		headerLogo.setBorder(Rectangle.NO_BORDER);
		headerLogo.setHorizontalAlignment(Element.ALIGN_LEFT);
		PdfPCell headerCell = new PdfPCell();
		headerCell.setBorder(Rectangle.NO_BORDER);
		headerCell.setPhrase(new Phrase(presentation.getTitle(), font2));
		headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
		headerCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
		headerCell.setFixedHeight(72);
		PdfPTable header = new PdfPTable(2);// header table with 2 columns
		header.getDefaultCell().setBorder(0);
		float[] columnWidths = new float[] { 3f, 20f };
		header.setWidthPercentage(100);
		header.setWidths(columnWidths);
		if (headerImage.exists()) {
			header.addCell(headerLogo);
		} else {
			header.addCell("");
		}
		header.addCell(headerCell);
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfFile));
		writer.setStrictImageSequence(true);
		int perPageProducts = products.size() / noOfProduct;
		int remainingProducts = products.size() % noOfProduct;
		int totalPages = 1;
		if (remainingProducts == 1) {
			totalPages = perPageProducts + remainingProducts;
		} else if (remainingProducts > 1) {
			totalPages = perPageProducts + 1;
		} else {
			totalPages = perPageProducts + remainingProducts;
		}
		HeaderFooter footer = new HeaderFooter(new Phrase("Page ", font5), new Phrase(" of " + totalPages, font5));
		footer.setBorderWidth(0);
		footer.setAlignment(Element.ALIGN_RIGHT);
		document.setFooter(footer);
		document.open();
		for (Product p : products) {
			products2.add(p);
			PdfPCell blankCell = new PdfPCell();
			blankCell.setBorder(Rectangle.NO_BORDER);
			blankCell.setFixedHeight(10);
			PdfPCell blankCell2 = new PdfPCell();
			blankCell2.setBorder(Rectangle.NO_BORDER);
			blankCell2.setFixedHeight(5);
			if (products2.size() == noOfProduct) {
				document.newPage();
				PdfPTable table = new PdfPTable(1); // table with 1 columns(|)
				table.getDefaultCell().setBorder(0);
				table.setWidthPercentage(100);
				table.addCell(header);
				createImageCell(products2, baseImageDir, table, noOfProduct);
				if (noOfProduct < 5) {
					table.addCell(blankCell);
				}
				createPresentationTable(products2, map, table, presentationProductPrice, admin);

				PdfPCell salesRepCell = new PdfPCell();
				salesRepCell.setPhrase(new Phrase("Created by:  " + salesrep.getName() + "  Date Created:" + dateFormat.format(date) + "  This Offer Expires in: 30 Days", font3));
				salesRepCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				salesRepCell.setBorder(0);
				PdfPCell noteCell = new PdfPCell();
				noteCell.setPhrase(new Phrase("NOTE:" + presentation.getNote(), font4));
				noteCell.setBorder(0);
				noteCell.setFixedHeight(60);
				noteCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				if (salesrep.getName() != null) {
					table.addCell(salesRepCell);
				}
				table.addCell(blankCell2);
				if (presentation.getNote() != null) {
					table.addCell(noteCell);
				}
				document.add(table);
				products2.clear();
			}

		}
		for (Product p : products) {
			for (int i = 1; i <= remainingProducts; i++) {
				products3.add(products.get(products.size() - i));
			}
			if (products3.size() == remainingProducts && remainingProducts != 0) {
				document.newPage();
				PdfPTable table = new PdfPTable(1); // table with 1 columns(|)
				table.getDefaultCell().setBorder(0);
				table.setWidthPercentage(100);
				PdfPCell blankCell = new PdfPCell();
				blankCell.setBorder(Rectangle.NO_BORDER);
				blankCell.setFixedHeight(20);
				PdfPCell blankCell2 = new PdfPCell();
				blankCell2.setBorder(Rectangle.NO_BORDER);
				blankCell2.setFixedHeight(5);
				table.addCell(header);
				createImageCell(products3, baseImageDir, table, noOfProduct);
				if (noOfProduct < 5) {
					table.addCell(blankCell);
				}
				createPresentationTable(products3, map, table, presentationProductPrice, admin);

				PdfPCell salesRepCell = new PdfPCell();
				salesRepCell.setPhrase(new Phrase("Created by: " + salesrep.getName() + "        Date Created:" + dateFormat.format(date) + "        This Offer Expires in: 30 Days", font3));
				salesRepCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				salesRepCell.setBorder(0);
				PdfPCell noteCell = new PdfPCell();
				noteCell.setPhrase(new Phrase("NOTE:" + presentation.getNote(), font4));
				noteCell.setBorder(0);
				noteCell.setFixedHeight(60);
				noteCell.setHorizontalAlignment(Element.ALIGN_CENTER);
				if (salesrep.getName() != null) {
					table.addCell(salesRepCell);
				}
				table.addCell(blankCell2);
				if (presentation.getNote() != null) {
					table.addCell(noteCell);
				}
				document.add(table);
			}
		}
		document.close();
	}

	private void createImageCell(List<Product> products, File baseImageDir, PdfPTable table, Integer noOfProduct) throws BadElementException {

		Font font3 = new Font(Font.TIMES_ROMAN, 10, Font.BOLD);
		// thumbnail
		File imageDir = new File(baseImageDir, "Product/detailsbig/");
		table.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
		List<String> images = new ArrayList<String>();
		Map<String, String> imageSkuMap = new HashMap<String, String>();

		for (Product p : products) {
			for (ProductImage img : p.getImages()) {
				images.add(img.getImageUrl());
				imageSkuMap.put(img.getImageUrl(), p.getSku());
			}
		}
		try {
			switch (products.size()) {
			case 1:
				PdfPTable imgTable = new PdfPTable(1);
				imgTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				PdfPTable imgSubtable = new PdfPTable(1);
				imgSubtable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				PdfPCell cell = new PdfPCell();
				PdfPCell skucell = new PdfPCell();
				File im = null;
				for (String img : images) {
					im = new File(imageDir, img);
					Image img1 = Image.getInstance(im.getAbsolutePath());
					cell = new PdfPCell();
					skucell = new PdfPCell();
					imgSubtable = new PdfPTable(1);
					cell.setImage(img1);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(400);
					cell.setBorderWidth(1);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					skucell.setBorder(Rectangle.NO_BORDER);
					Paragraph sku = new Paragraph(imageSkuMap.get(img), font3);
					sku.setAlignment(Element.ALIGN_CENTER);
					skucell.addElement(sku);
					imgSubtable.addCell(cell);
					imgSubtable.addCell(skucell);
					imgTable.addCell(imgSubtable);
				}
				table.addCell(imgTable);
				break;

			case 2:
				imgTable = new PdfPTable(2);
				imgTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				cell = new PdfPCell();
				im = null;
				for (String img : images) {
					im = new File(imageDir, img);
					Image img1 = Image.getInstance(im.getAbsolutePath());
					skucell = new PdfPCell();
					cell = new PdfPCell();
					imgSubtable = new PdfPTable(1);
					cell.setImage(img1);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(300);
					cell.setBorderWidth(1);
					skucell.setBorder(Rectangle.NO_BORDER);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					Paragraph sku = new Paragraph(imageSkuMap.get(img), font3);
					sku.setAlignment(Element.ALIGN_CENTER);
					skucell.addElement(sku);
					imgSubtable.addCell(cell);
					imgSubtable.addCell(skucell);
					imgTable.addCell(imgSubtable);
				}
				table.addCell(imgTable);
				break;

			case 3:
				imgTable = new PdfPTable(2);
				imgTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				im = null;
				for (String img : images) {
					im = new File(imageDir, img);
					Image img1 = Image.getInstance(im.getAbsolutePath());
					imgSubtable = new PdfPTable(1);
					cell = new PdfPCell();
					skucell = new PdfPCell();
					cell.setImage(img1);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(200);
					cell.setBorderWidth(1);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					skucell.setBorder(Rectangle.NO_BORDER);
					skucell.setBorder(Rectangle.NO_BORDER);
					Paragraph sku = new Paragraph(imageSkuMap.get(img), font3);
					sku.setAlignment(Element.ALIGN_CENTER);
					skucell.addElement(sku);
					imgSubtable.addCell(cell);
					imgSubtable.addCell(skucell);
					if (images.indexOf(img) == 2) {
						table.addCell(imgTable);
						PdfPTable imgTable2 = new PdfPTable(1);
						PdfPTable imgTable3 = new PdfPTable(1);
						PdfPCell cell2 = new PdfPCell();
						PdfPCell cell3 = new PdfPCell();
						skucell = new PdfPCell();
						cell3.setImage(img1);
						cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell3.setBorderWidth(1);
						cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
						imgTable3.addCell(cell3);
						cell2.setFixedHeight(200);
						cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
						cell2.setBorder(Rectangle.NO_BORDER);
						cell2.addElement(imgTable3);
						skucell.setBorder(Rectangle.NO_BORDER);
						Paragraph sku2 = new Paragraph(imageSkuMap.get(img), font3);
						sku2.setAlignment(Element.ALIGN_CENTER);
						skucell.addElement(sku2);
						imgTable2.addCell(cell2);
						imgTable2.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
						imgTable2.addCell(skucell);
						table.addCell(imgTable2);
					} else {
						imgTable.addCell(imgSubtable);
					}
				}

				break;

			case 4:
				imgTable = new PdfPTable(2);
				imgTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				im = null;
				for (String img : images) {
					im = new File(imageDir, img);
					Image img1 = Image.getInstance(im.getAbsolutePath());
					imgSubtable = new PdfPTable(1);
					skucell = new PdfPCell();
					cell = new PdfPCell();
					cell.setImage(img1);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(180);
					cell.setBorderWidth(1);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					imgSubtable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
					skucell.setBorder(Rectangle.NO_BORDER);
					Paragraph sku = new Paragraph(imageSkuMap.get(img), font3);
					sku.setAlignment(Element.ALIGN_CENTER);
					skucell.addElement(sku);
					imgSubtable.addCell(cell);
					imgSubtable.addCell(skucell);
					imgTable.addCell(imgSubtable);
				}
				table.addCell(imgTable);
				break;

			case 5:
				imgTable = new PdfPTable(2);
				imgTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				im = null;
				for (String img : images) {
					im = new File(imageDir, img);
					Image img1 = Image.getInstance(im.getAbsolutePath());
					imgSubtable = new PdfPTable(1);
					cell = new PdfPCell();
					skucell = new PdfPCell();
					cell.setImage(img1);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(120);
					cell.setBorderWidth(1);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					skucell.setBorder(Rectangle.NO_BORDER);
					Paragraph sku = new Paragraph(imageSkuMap.get(img), font3);
					sku.setAlignment(Element.ALIGN_CENTER);
					skucell.addElement(sku);
					imgSubtable.addCell(cell);
					imgSubtable.addCell(skucell);
					if (images.indexOf(img) == 4) {
						table.addCell(imgTable);
						PdfPTable imgTable2 = new PdfPTable(1);
						PdfPTable imgTable3 = new PdfPTable(1);
						PdfPCell cell2 = new PdfPCell();
						PdfPCell cell3 = new PdfPCell();
						skucell = new PdfPCell();
						cell3.setImage(img1);
						cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
						cell3.setBorderWidth(1);
						cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
						imgTable3.addCell(cell3);
						cell2.setFixedHeight(120);
						cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
						cell2.setBorder(Rectangle.NO_BORDER);
						cell2.addElement(imgTable3);
						skucell.setBorder(Rectangle.NO_BORDER);
						Paragraph sku2 = new Paragraph(imageSkuMap.get(img), font3);
						sku2.setAlignment(Element.ALIGN_CENTER);
						skucell.addElement(sku2);
						imgTable2.addCell(cell2);
						imgTable2.addCell(skucell);
						table.addCell(imgTable2);
					} else {
						imgTable.addCell(imgSubtable);
					}
				}
				break;

			case 6:
				imgTable = new PdfPTable(3);
				imgTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				im = null;
				for (String img : images) {
					im = new File(imageDir, img);
					skucell = new PdfPCell();
					Image img1 = Image.getInstance(im.getAbsolutePath());
					imgSubtable = new PdfPTable(1);
					imgSubtable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
					cell = new PdfPCell();
					cell.setImage(img1);
					cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
					cell.setFixedHeight(180);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setBorderWidth(1);
					skucell.setBorder(Rectangle.NO_BORDER);
					Paragraph sku = new Paragraph(imageSkuMap.get(img), font3);
					sku.setAlignment(Element.ALIGN_CENTER);
					skucell.addElement(sku);
					imgSubtable.addCell(cell);
					imgSubtable.addCell(skucell);
					imgTable.addCell(imgSubtable);
				}
				table.addCell(imgTable);
				break;

			default:
				break;
			}

		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void createPresentationTable(List<Product> products, Map<String, Object> map, PdfPTable table, Map<Integer, Double> presentationProductPrice, boolean admin) {

		Font font2 = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL);
		Font font3 = new Font(Font.TIMES_ROMAN, 10, Font.BOLD);
		int index = 0;
		List<Float> columns = new ArrayList<Float>();
		columns.add(1.75f); // Item number
		if (map != null) {
			for (ProductField productField : (List<ProductField>) map.get("productFieldsHeader" + index)) {
				columns.add(2.25f);
			}
		}
		columns.add(1.45f); // inventory
		columns.add(1.25f); // price
		columns.add(3f); // notes
		float widths[] = new float[columns.size()];
		for (int i = 0; i < columns.size(); i++) {
			widths[i] = columns.get(i);
		}

		PdfPTable itemsTable = new PdfPTable(widths);
		itemsTable.setTotalWidth(300f);
		itemsTable.setWidthPercentage(100);
		itemsTable.getDefaultCell().setBorder(PdfCell.NO_BORDER);
		PdfPCell cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		cell.setFixedHeight(250);
		cell.setBorderColor(Color.GRAY);
		// create headers
		List<String> headers = new ArrayList<String>();
		headers.add("Item No");
		if (map != null) {
			for (ProductField productField : (List<ProductField>) map.get("productFieldsHeader" + index)) {
				headers.add(productField.getName());
			}
		}
		headers.add("Quantity Available");
		headers.add("Price");
		headers.add("Notes");

		for (String header : headers) {
			cell = new PdfPCell(new Phrase(header, font3));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setFixedHeight(30);
			cell.setBorderColor(Color.GRAY);
			itemsTable.addCell(cell);
		}

		// line items
		int lineNum = 0;
		for (Product product : products) {
			cell = new PdfPCell(new Phrase(product.getSku(), font2));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setBorderColor(Color.GRAY);
			itemsTable.addCell(cell);
			if (map != null) {
				for (ProductField productField1 : (List<ProductField>) map.get("productFieldsHeader" + index)) {
					cell = new PdfPCell(new Phrase(""));
					cell.setBorderColor(Color.GRAY);
					for (ProductField productField2 : product.getProductFields()) {
						if (productField1.getId().compareTo(productField2.getId()) == 0) {
							cell = new PdfPCell(new Phrase(productField2.getValue(), font2));
							cell.setFixedHeight(30);
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							cell.setBorderColor(Color.GRAY);
							break;
						}
					}
					itemsTable.addCell(cell);
				}
			}
			cell = new PdfPCell(new Phrase(product.getInventoryAFS() + "", font2));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setBorderColor(Color.GRAY);
			if (product.getInventoryAFS() == null) {
				itemsTable.addCell(" ");
			} else {
				itemsTable.addCell(cell);
			}
			if (admin == true) {
				if (product.getPrice1() != null) {
					cell = new PdfPCell(new Phrase("$" + product.getPrice1() + "0 ", font2));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setBorderColor(Color.GRAY);
					itemsTable.addCell(cell);
				} else {
					cell = new PdfPCell(new Phrase("$0.00", font2));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setBorderColor(Color.GRAY);
					itemsTable.addCell(cell);
				}
			} else {
				if (presentationProductPrice.get(product.getId()) != null) {
					cell = new PdfPCell(new Phrase("$" + presentationProductPrice.get(product.getId()) + "0 ", font2));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setBorderColor(Color.GRAY);
					itemsTable.addCell(cell);
				} else {
					cell = new PdfPCell(new Phrase("$0.00", font2));
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell.setBorderColor(Color.GRAY);
					itemsTable.addCell(cell);
				}
			}
			cell = new PdfPCell(new Phrase(" ", font2));
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setBorderColor(Color.GRAY);
			itemsTable.addCell(cell);
			index++;
		}
		PdfPTable itemsTable2 = new PdfPTable(1);
		PdfPCell cell3 = new PdfPCell();
		cell3.addElement(itemsTable);
		cell3.setBorderWidth(2);
		cell3.setBorderColor(Color.GRAY);
		cell3.setPadding(2);
		itemsTable2.addCell(cell3);
		table.addCell(itemsTable2);

	}
}
