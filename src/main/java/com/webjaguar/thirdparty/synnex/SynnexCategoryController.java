/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.29.2009
 */

package com.webjaguar.thirdparty.synnex;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class SynnexCategoryController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		List<SynnexCategory> categories = new ArrayList<SynnexCategory>();
		
		if (request.getMethod().equals("POST")) {
			for (String code: request.getParameterValues("code")) {
				SynnexCategory category = new SynnexCategory();
				category.setCode(code);
				try {
					category.setCatId(ServletRequestUtils.getIntParameter(request, code + "_catId"));
				} catch (Exception e) {
					category.setCatId(null);
				}
				try {
					category.setPriceChange(ServletRequestUtils.getDoubleParameter(request, code + "_priceChange"));
				} catch (Exception e) {
					category.setPriceChange(null);
				}
				categories.add(category);
			}
			this.webJaguar.nonTransactionSafeUpdateSynnexCategories(categories);
		}
		
		categories = this.webJaguar.getSynnexCategories();
		
		Map<String, SynnexCategory> categoryMap = new HashMap<String, SynnexCategory>();
		Iterator<SynnexCategory> iter = categories.iterator();
		while (iter.hasNext()) {
			SynnexCategory category = iter.next();
			if (category.getSub().equals("*")) {
				categoryMap.put(category.getCat(), category);				
			} else if (category.getSubSub().equals("*")) {
				categoryMap.put(category.getCat() + category.getSub(), category);
				if (categoryMap.get(category.getCat()) != null) {
					categoryMap.get(category.getCat()).addSub(category);
				}
				iter.remove();
			} else {
				if (categoryMap.get(category.getCat() + category.getSub()) != null) {
					categoryMap.get(category.getCat() + category.getSub()).addSub(category);					
				}
				iter.remove();
			}
		}
		
		map.put("categories", categories);		

		return new ModelAndView("admin/vendors/synnex/category", map);
	}
}
