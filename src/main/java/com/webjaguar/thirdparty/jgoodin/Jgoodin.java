package com.webjaguar.thirdparty.jgoodin;

import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;

public class Jgoodin {
	
	private WebJaguarFacade webJaguar;
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	private MailSender mailSender;
	
	
	// constructor
	public Jgoodin(Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig, MailSender mailSender, WebJaguarFacade webJaguar) {
		this.siteConfig = siteConfig;
		this.gSiteConfig = gSiteConfig;
		this.mailSender = mailSender;
		this.webJaguar = webJaguar;
	}

	public boolean getJgoodinProducts() {	
		if (!(Boolean) gSiteConfig.get("gSITE_ACTIVE")) {
			// return if site is disabled
			return false;
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		System.out.println("Product download start:" + new Date() );
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		
		try {
	    	
	    	URL url = new URL("http://media.jgoodin.com/dropship/jgi-master-feed.csv");
	    	URLConnection connection = url.openConnection();
			
	    	CSVReader reader = new CSVReader(new InputStreamReader(connection.getInputStream()), ',', CSVWriter.NO_QUOTE_CHARACTER); 
	      	
	    	//Item SKU
	    	CsvFeed csvFeed = new CsvFeed();
    		csvFeed.setColumnName("sku");
			csvFeed.setColumn(0);
			csvFeedList.add(csvFeed);
			
			//master sku
			csvFeedList.add(new CsvFeed("master_sku"));
			
			//Item Category
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("field_53");
			csvFeed.setColumn(1);
			csvFeedList.add(csvFeed);
			
			//Active
			csvFeedList.add(new CsvFeed("active"));
			
			//Web Name
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("name");
			csvFeed.setColumn(3);
			csvFeedList.add(csvFeed);
		
			//Sales Description
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("short_desc");
			csvFeed.setColumn(4);
			csvFeedList.add(csvFeed);
	
			//Shipping Weight
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("weight");
			csvFeed.setColumn(5);
			csvFeedList.add(csvFeed);
			
			//Base Metal
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("field_34");
			csvFeed.setColumn(6);
			csvFeedList.add(csvFeed);
	
			//Audience
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("field_9");
			csvFeed.setColumn(7);
			csvFeedList.add(csvFeed);
	
			//Country of Origin
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("field_5");
			csvFeed.setColumn(8);
			csvFeedList.add(csvFeed);
		
			//Style
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("field_33");
			csvFeed.setColumn(9);
			csvFeedList.add(csvFeed);
			
			//Setting Type
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("field_30");
			csvFeed.setColumn(10);
			csvFeedList.add(csvFeed);
		
			//Materials
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("field_1");
			csvFeed.setColumn(11);
			csvFeedList.add(csvFeed);
	
			//Stone Cut
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("field_31");
			csvFeed.setColumn(12);
			csvFeedList.add(csvFeed);
			
			//Carat Weight
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("field_28");
			csvFeed.setColumn(13);
			csvFeedList.add(csvFeed);
			
			//Color
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("field_47");
			csvFeed.setColumn(14);
			csvFeedList.add(csvFeed);
			
			//Plating Color
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("field_46");
			csvFeed.setColumn(15);
			csvFeedList.add(csvFeed);
			
			//WholesalePrice
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("price_table_1");
			csvFeed.setColumn(16);
			csvFeedList.add(csvFeed);
		
			//MinimumAdvertisedPrice
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("field_54");
			csvFeed.setColumn(17);
			csvFeedList.add(csvFeed);
		
			//MSRP
			csvFeed = new CsvFeed();
    		csvFeed.setColumnName("msrp");
			csvFeed.setColumn(18);
			csvFeedList.add(csvFeed);
		
			//Original Sku
			csvFeedList.add(new CsvFeed("field_55"));
			
			//Ring Size
			csvFeedList.add(new CsvFeed("field_56"));
			
			csvFeedList.add(new CsvFeed("price_1"));
			csvFeedList.add(new CsvFeed("cost_1"));
			
			csvFeedList.add(new CsvFeed("feed"));
			csvFeedList.add(new CsvFeed("feed_new"));
			
			String[] nextLine;
	    	while ((nextLine = reader.readNext()) != null) {
	    		HashMap<String, Object> map = new HashMap<String, Object>();
	    		for (CsvFeed feed: csvFeedList) {
	    			if(feed.getColumn() == null) {
	    				continue;
	    			}
	    			String value = nextLine[feed.getColumn()];
	    			value.replaceAll("\\|", ",");
	    			map.put(feed.getColumnName(), value);
	    		}
	    		
	    		//Original Sku
	    		map.put("field_55", nextLine[0]);				
	    		
	    		//Ring Size
	    		map.put("field_56", null);				
	    		
	    		String newSku = this.changeSku(nextLine[0]);
	    		map.put("sku", newSku);				
	    		map.put("active", nextLine[2].equalsIgnoreCase("Yes"));				
	    		
	    		//Price is 3 times  wholesale price
	    		try {
	    			map.put("price_1", Double.parseDouble(nextLine[16]) * 3);				
		    	} catch(Exception e) {
		    		map.put("price_1", null);				
		    	}
	    		
	    		//Cost is 60% of wholesale price
	    		try {
	    			map.put("cost_1", Double.parseDouble(nextLine[16]) * 0.6);				
		    	} catch(Exception e) {
		    		map.put("cost_1", null);				
		    	}
	    		map.put("master_sku", null);
				//Main Image
	    		map.put("image_1", nextLine[20]);				
				
	    		//Thumbnail Image
	    		map.put("image_2", nextLine[19]);
				
	    		//Other Images
	    		if(nextLine[21] != null && !nextLine[21].trim().isEmpty()) {
	    			String[] images = nextLine[21].split("\\|");
	    			for(int i=0; i< images.length; i++){
	    				map.put("image_"+(i+3), images[i]);
		    		}
	    		}
	    		
	    		map.put("feed", "JGOODIN");				
				map.put("feed_new", true);
				
				if(nextLine[22] != null && !nextLine[22].trim().isEmpty()) {
					String[] childSkus = nextLine[22].split("\\|");
					HashMap<String, Object> childSkuMap;
		    		
					for(String childSku : childSkus){
						childSkuMap = new HashMap<String, Object>();
						//get all data from master sku
						childSkuMap.putAll(map);
						
						//override sku
						String newChildSku = this.changeSku(childSku.trim());
						
						childSkuMap.put("sku", newChildSku);
						
						//override original sku
						childSkuMap.put("field_55", childSku.trim());
						
						//Ring Size
						try {
							String[] ringSize = childSku.split("-");
							if(ringSize.length > 1) {
								childSkuMap.put("field_56" +
										"", ringSize[ringSize.length-1]);				
						    } 
				    	} catch(Exception e){
				    		childSkuMap.put("field_56", null);				
				    	}
						
			    		//set master sku
						childSkuMap.put("master_sku", newSku);
						data.add(childSkuMap);
			    	}
				}
						
				data.add(map);
	    		
	    		// update
		    	if (data.size() >= 100) {
					this.webJaguar.updateJgoodinProduct(csvFeedList, data);
					data.clear();	
				}
	    	}
    		reader.close();
		
	    	if (data.size() > 0) {
	    		this.webJaguar.updateJgoodinProduct(csvFeedList, data);
				data.clear();	
			}
		
	    } catch (Exception e) {
			e.printStackTrace();
			notifyAdmin("JGoodin updateProduct() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		sbuff.append("Read jgi-master-feed.csv file." + "\n\n");
		notifyAdmin("updateProduct() - JGOODIN on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
		
		System.out.println("Product download end:" + new Date() );
		
		return true;
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
	
	private String changeSku(String sku) {
		
		Pattern pattern = Pattern.compile("([a-zA-Z]{1,})([0-9]{1,})(.*)", Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(sku);
		try {
			if(matcher.find()) {
				Integer middleNumber = Integer.parseInt(matcher.group(2));
				middleNumber = middleNumber + 98;
				return matcher.group(1)+middleNumber.toString()+matcher.group(3);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return sku;
		}
		return sku;
	}	
}