package com.webjaguar.thirdparty.cdsanddvds;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;


public class CdsAndDvdsDataFeedController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	

	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	private String feed = "CdsAndDvds";
	private static File productFile;
	private static Map<String, Configuration> siteConfig;
	List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
	
	public ModelAndView handleRequest(HttpServletRequest arg0,
			HttpServletResponse arg1) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		
		siteConfig = webJaguar.getSiteConfig();
		// CdsAndDvds configuration
		String[] config = siteConfig.get("CDS_AND_DVDS").getValue().split(",");
		
		// get generated data feed file
        File baseFile = new File(getServletContext().getRealPath("/admin/feed/"));
        if (!baseFile.exists()) {
        	baseFile.mkdir();
        }
        
    	try {
    		String fileName = config[3].trim();
    	    productFile = new File(baseFile, fileName);
    	   // productFile = new File("/Users/shahin/Desktop/ProductFeed.txt");
    		StringBuffer sbuff = new StringBuffer();
    	   	if (config[0].trim().equals( "" )) {
    	   		throw new UnknownHostException();
    	   	}
    	   	if (config[3].trim().equals( "" )) {
    	   		throw new Exception();
    	   	}
    	   	
    	   	// Connect and logon to FTP Server
    	    FTPClient ftp = new FTPClient();
    	    ftp.connect( config[0].trim() );
    	        
    	    // login
    	    ftp.login( config[1].trim(), config[2].trim() );
    	   
    	    if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
    	    	// download file
    	        FileOutputStream fos = new FileOutputStream(productFile);
    	        ftp.retrieveFile(config[3].trim(), fos);
    	        sbuff.append("Downloading file: " + config[3].trim() + "Compeleted.");
    	        fos.close();        	
    	    }
    	        
    	    // Logout from the FTP Server and disconnect
    	    ftp.logout();
    	    ftp.disconnect(); 
    	    Long startTime = new Date().getTime();
    	    parseProductTextFile(productFile);
    	    System.out.println("Time to complete all products "+(new Date().getTime() - startTime));
    	} catch (Exception e) {
    		notifyAdmin("CdsBooksDvds Datafeed downloadProducts() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
    		e.printStackTrace();
    	}
		
		return new ModelAndView( new RedirectView("/admin/datafeed/"), map);
	}
	
	public void parseProductTextFile(File productFile) {
 		try {
			
 			HashMap<String, Object> map = null;
 			BufferedReader bReader = new BufferedReader( new FileReader(productFile));
 			String line;
 			webJaguar.markAndInactiveOldProducts(feed, null, false);
	    	
 		    while ((line = bReader.readLine()) != null) {
 	    		/**
 			    * Splitting the content of tabbed separated line
 			    */
 			    String datavalue[] = line.split("\t");
 			   
 			    if(datavalue[0] != null && !datavalue[0].isEmpty()) {
 			    	map = new HashMap<String, Object>();
 				
 					try {
 						map.put("sku", datavalue[0]);
 					} catch (Exception e) { map.put("sku", null); }
 					try {
 						map.put("name", datavalue[1] );
 					} catch (Exception e) { map.put("name", null); }
 					try {
 						map.put("active", Integer.parseInt( datavalue[2] ));
 					} catch (Exception e) { map.put("active", 1); }
 					try {
 						map.put("long_desc", datavalue[3] );
 					} catch (Exception e) { map.put("long_desc", null); }
 					try {
 						map.put("image1", datavalue[4] );
 					} catch (Exception e) { map.put("image1", null); }
 					try {
 						map.put("weight", Double.parseDouble( datavalue[5] ));
 					} catch (Exception e) { map.put("weight", 0.0); }
 					try {
 						map.put("msrp", Double.parseDouble( datavalue[6] ));
 					} catch (Exception e) { map.put("msrp", null); }
 					try {
 						map.put("price_1", Double.parseDouble( datavalue[7] ));
 					} catch (Exception e) { map.put("price_1", null); }
 					try {
 						map.put("taxable", Integer.parseInt( datavalue[8] ));
 					} catch (Exception e) { map.put("taxable", 1); }
 					try {
 						map.put("searchable", Integer.parseInt( datavalue[9] ));
 					} catch (Exception e) { map.put("searchable", 1); }
 					try {
 						map.put("inventory", Integer.parseInt(datavalue[10]));						
 					} catch (Exception e) {
 						map.put("inventory", null);
 					}
 					try {
 						map.put("inventory_afs", Integer.parseInt(datavalue[11]));						
 					} catch (Exception e) {
 						map.put("inventory_afs", null);
 					}
 					try {
 						map.put("neg_inventory", Integer.parseInt(datavalue[12]));						
 					} catch (Exception e) {
 						map.put("neg_inventory", 0);
 					}
 					try {
 						map.put("show_neg_inventory", Integer.parseInt(datavalue[13]));						
 					} catch (Exception e) {
 						map.put("show_neg_inventory", 0);
 					}
 					try {
 						map.put("categoryId", Integer.parseInt(datavalue[14]));						
 					} catch (Exception e) {
 						map.put("categoryId", null);
 					}
 					try {
 						map.put("field_1", Double.parseDouble(datavalue[15]));						
 						map.put("field_2", 0);						
 	 				} catch (Exception e) {
 	 					map.put("field_1", null);						
 						map.put("field_2", null);						
 	 				}
 					map.put("feed", feed);				
					map.put("feed_new", true);
					
 					data.add(map);
 					if (data.size() == 3000) {
 						// products
 						Long startTime = new Date().getTime();
 	 					updateProduct(data, true);
 						data.clear();
 						map.clear();
 						System.out.println("Time to update 3000 "+(new Date().getTime() - startTime));
 		 			}
 				 }
 			  }
 			
 			// update
 	    	if (!data.isEmpty()) {
	 	    	updateProduct(data, true);
				data.clear();
				map.clear();
			}
 	    	webJaguar.markAndInactiveOldProducts(feed, null, true);
 		} catch (Exception e) {System.out.println("error 1 ");
			notifyAdmin("CdsAndDvds Datafeed downloadProducts() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
	    }
	}
	
	private void updateProduct(List<Map <String, Object>> data, boolean insertNewProduct) {
    	// update Products
    	webJaguar.nonTransactionSafeUpdateCdsDvdsProduct(getCsvFeedList(), data, insertNewProduct);
    }
    
    private List<CsvFeed> getCsvFeedList() {
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		
		csvFeedList.add(new CsvFeed("sku"));
			csvFeedList.add(new CsvFeed("name"));
			csvFeedList.add(new CsvFeed("active"));
			csvFeedList.add(new CsvFeed("long_desc"));
			csvFeedList.add(new CsvFeed("weight"));
			csvFeedList.add(new CsvFeed("msrp"));
			csvFeedList.add(new CsvFeed("price_1"));
			csvFeedList.add(new CsvFeed("inventory"));
			csvFeedList.add(new CsvFeed("inventory_afs"));
			csvFeedList.add(new CsvFeed("neg_inventory"));
			csvFeedList.add(new CsvFeed("show_neg_inventory"));
			csvFeedList.add(new CsvFeed("taxable"));
			csvFeedList.add(new CsvFeed("searchable"));
			csvFeedList.add(new CsvFeed("field_1"));
			csvFeedList.add(new CsvFeed("field_2"));
			csvFeedList.add(new CsvFeed("feed"));
			csvFeedList.add(new CsvFeed("feed_new"));
			
		return csvFeedList;
	}

    private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "jwalant@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		//msg.setCc("developers@advancedemedia.com");
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 

}