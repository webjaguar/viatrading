package com.webjaguar.thirdparty.shipping.ups;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.webjaguar.model.ShippingRate;

public class UpsRateQuote {
	
    // admin configurable fields
	String hostname = "www.ups.com";
	String protocol = "https";
	String prefix = "ups.app/xml";
	String service = "Rate";
	double unknownPackageMax = 150.0;
	
	// addressType: 0 commercial 1 Residential
		
	public List<ShippingRate> getRates(String fromZip, String fromStateProvince, String fromCountry, String userId, String password, String accessLicenceNumber, String toZip, String toStateProvince, String toCountry, 
			double unknownPackageWeight, boolean addressType, boolean hasUnknownPackage, List<Map <String, Object>> packages, Map <Integer, Double> independentPackages, String pickupType, String shipperNumber){
		
		// list to be returned
		List<ShippingRate> ratesList = new ArrayList<ShippingRate>();
		
		int numHeavyPackage = 0;
		double smallPackageWeight = Math.ceil(unknownPackageWeight);
		
		// adjust weight if necessary
		if (hasUnknownPackage && smallPackageWeight < 1) {
			smallPackageWeight = 1;
		}
			
		try {
			
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + service);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			
			if (smallPackageWeight > unknownPackageMax) {
				numHeavyPackage = (new Double(Math.floor(smallPackageWeight/unknownPackageMax))).intValue();
				smallPackageWeight = smallPackageWeight - numHeavyPackage*unknownPackageMax;
			} 			
			
			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer xmlRequest = new StringBuffer(
				"<?xml version=\"1.0\"?>" +
				"<AccessRequest xml:lang=\"en-US\">" +
				  "<AccessLicenseNumber>" + accessLicenceNumber + "</AccessLicenseNumber>" +
			      "<UserId>" + userId + "</UserId>" +
				  "<Password>" + password + "</Password>" +
				"</AccessRequest>");		            // Access Request
			xmlRequest.append(
				"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" +
				"<RatingServiceSelectionRequest xml:lang=\"en-US\">" +
				  "<Request>" +
				    "<TransactionReference>" +
				      "<CustomerContext>Rating and Service</CustomerContext>" +
				      "<XpciVersion>1.0001</XpciVersion>" +
				    "</TransactionReference>" +
				    "<RequestAction>Rate</RequestAction>" +
				    "<RequestOption>shop</RequestOption>" +
				  "</Request>" +
				  "<PickupType>" +
				  "<Code>" + pickupType + "</Code>" +			
				  "</PickupType>" +
				  "<Shipment>" +
				    "<Shipper>" );	
					if(!shipperNumber.isEmpty()) {
						 xmlRequest.append(	    
								 "<ShipperNumber>" + shipperNumber + "</ShipperNumber>");									
					}
					 xmlRequest.append(								  
				      "<Address>" );	
						if(!shipperNumber.isEmpty()) {
							 xmlRequest.append(	 									
								"<StateProvinceCode>"+fromStateProvince+"</StateProvinceCode>");
						}
						 xmlRequest.append(	
				        "<PostalCode>" +  fromZip + "</PostalCode>" +
				        "<CountryCode>" + fromCountry + "</CountryCode>" +
				      "</Address>" +
				    "</Shipper>"+	
				    "<ShipTo>" +
				      "<Address>" +
				        "<PostalCode>" + toZip + "</PostalCode>");
					 if(!shipperNumber.isEmpty()) {
						 xmlRequest.append(	 
				        "<StateProvinceCode>"+toStateProvince+"</StateProvinceCode>");
					 }
					 xmlRequest.append(	 
				        "<CountryCode>" + toCountry + "</CountryCode>" );
						if (addressType) {
							xmlRequest.append(
									// True if tab exists; false otherwise
									"<ResidentialAddressIndicator>" + addressType + "</ResidentialAddressIndicator>" ); 	
						}
						xmlRequest.append(
				      "</Address>" +
				    "</ShipTo>");	
						for (int i=0 ; i<numHeavyPackage; i++) {
							xmlRequest.append(
								    "<Package>" +
								      "<PackagingType>" +
								        "<Code>02</Code>" +
								        "<Description>Package</Description>" +
								      "</PackagingType>" +
								      "<Description>Rate Shopping</Description>" +
								      "<PackageWeight>" +
								        "<Weight>" + unknownPackageMax + "</Weight>" +
								      "</PackageWeight>" +
								     "</Package>");							
						}
						if (smallPackageWeight > 0) {
							xmlRequest.append(
								    "<Package>" +
								      "<PackagingType>" +
								        "<Code>02</Code>" +
								        "<Description>Package</Description>" +
								      "</PackagingType>" +
								      "<Description>Rate Shopping</Description>" +
								      "<PackageWeight>" +
								        "<Weight>" + smallPackageWeight + "</Weight>" +
								      "</PackageWeight>" +
								     "</Package>");					
						}
						if(independentPackages != null) {
							for (Integer packageNumber : independentPackages.keySet()) {
								xmlRequest.append(
									    "<Package>" +
									      "<PackagingType>" +
									        "<Code>02</Code>" +
									        "<Description>Package</Description>" +
									      "</PackagingType>" +
									      "<Description>Rate Shopping</Description>" +
									      "<PackageWeight>" +
									        "<Weight>" + independentPackages.get(packageNumber) + "</Weight>" +
									      "</PackageWeight>" +
									     "</Package>");							
							}	
						}
						if (packages != null) for (Map<String, Object> thisPackage: packages) {
							for (int i=0 ; i<(Integer) thisPackage.get("qty"); i++) {
								xmlRequest.append(
									"<Package>" +
								      "<PackagingType>" +
								        "<Code>02</Code>" +
								        "<Description>Package</Description>" +
								      "</PackagingType>" +
								      "<Description>Rate Shopping</Description>" +
								      "<PackageWeight>" +
								        "<Weight>" + thisPackage.get("packageWeight") + "</Weight>" +
								      "</PackageWeight>" +
								      "<Dimensions>" +
								      	"<Length>" + thisPackage.get("length") + "</Length>" +
								      	"<Width>" + thisPackage.get("width") + "</Width>" +
								      	"<Height>" + thisPackage.get("height") + "</Height>" +
								      "</Dimensions>" +
									"</Package>");
							}
						}
						if(!shipperNumber.isEmpty()) {			
							xmlRequest.append(						
							"<RateInformation>" +
								"<NegotiatedRatesIndicator/>"+
							"</RateInformation>");	
						}
						xmlRequest.append(
			  	  "</Shipment>" +
				"</RatingServiceSelectionRequest>");
		

			// POST data
			OutputStream oStream = connection.getOutputStream();
			oStream.write(xmlRequest.toString().getBytes());
			oStream.close();
			
            // retrieve results from UPS server
			SAXBuilder builder = new SAXBuilder(false);
            Document doc = builder.build(connection.getInputStream());  
          	Element response = doc.getRootElement().getChild("Response");
			String responseStatusCode = response.getChild("ResponseStatusCode").getText();
			
					
			// print out error message
			if (responseStatusCode.equals("0")) {
				System.out.println(response.getChild("Error").getChild("ErrorDescription").getText());
			}	
			
			// get a list of shipping rates
			else{
				List ratedShipmentList = doc.getRootElement().getChildren("RatedShipment");
				Iterator it = ratedShipmentList.iterator();
				while (it.hasNext()) {
					Element ratedShipment = (Element)it.next();
                    // construct ShippingRate object
					ShippingRate shippingRate = new ShippingRate();
					shippingRate.setCarrier("ups");						
					shippingRate.setCode(ratedShipment.getChild("Service").getChild("Code").getText());
					if(ratedShipment.getChild("NegotiatedRates")!=null && ratedShipment.getChild("NegotiatedRates").getChild("NetSummaryCharges")!=null && ratedShipment.getChild("NegotiatedRates").getChild("NetSummaryCharges").getChild("GrandTotal")!=null && ratedShipment.getChild("NegotiatedRates").getChild("NetSummaryCharges").getChild("GrandTotal").getChild("MonetaryValue")!=null )	{
						shippingRate.setPrice(ratedShipment.getChild("NegotiatedRates").getChild("NetSummaryCharges").getChild("GrandTotal").getChild("MonetaryValue").getText());
					} else {
						shippingRate.setPrice(ratedShipment.getChild("TotalCharges").getChild("MonetaryValue").getText());
					}
			        shippingRate.setGuaranteedDaysToDelivery( ratedShipment.getChild("GuaranteedDaysToDelivery").getText() );
			        // add to ratesList
					ratesList.add(shippingRate);				
			    }			
			}

	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
		
		// return list
		return ratesList;
		
	}

}
