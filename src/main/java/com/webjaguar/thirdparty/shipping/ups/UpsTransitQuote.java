package com.webjaguar.thirdparty.shipping.ups;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

public class UpsTransitQuote {
	
    // admin configurable fields
	String hostname = "www.ups.com";
	String protocol = "https";
	String prefix = "ups.app/xml";
	String service = "TimeInTransit";
	
	// addressType: 0 commercial 1 Residential
		
	public String getTransit(String fromZip, String fromCountry, String userId, String password, String accessLicenceNumber, String toZip, String toCountry, 
			double weight, boolean addressType){
		
		// list to be returned
		//List<ShippingRate> ratesList = new ArrayList<ShippingRate>();
		
		// adjust weight if necessary
		if (weight > 0 && weight < 1) {
			weight = 1;
		}
			
		try {

			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + service);
			//URL url = new URL("http://localhost:1234/webjaguar1");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer xmlRequest = new StringBuffer(
				"<?xml version=\"1.0\"?>" +
				"<AccessRequest xml:lang=\"en-US\">" +
				"<AccessLicenseNumber>" + accessLicenceNumber + "</AccessLicenseNumber>" +
				  "<UserId>" + userId + "</UserId>" +
				  "<Password>" + password + "</Password>" +
				"</AccessRequest>");		            // Access Request
			xmlRequest.append(
					"<?xml version=\"1.0\"?>" +
					"<TimeInTransitRequest xml:lang=\"en-US\">" +
					  "<Request>" +
					    "<TransactionReference>" +
					      "<CustomerContext>Sample test</CustomerContext>" +
					      "<XpciVersion>1.0001</XpciVersion>" +
					    "</TransactionReference>" +
					    "<RequestAction>TimeInTransit</RequestAction>" +
					  "</Request>" +
					  "<TransitFrom>" +
					    "<AddressArtifactFormat>" +
					      "<PoliticalDivision2></PoliticalDivision2>" +
					      "<PoliticalDivision1></PoliticalDivision1>" +
					      "<PostcodePrimaryLow>" + fromZip + "</PostcodePrimaryLow>" +
					      "<CountryCode>" + fromCountry + "</CountryCode>");
			if (addressType) {
				xmlRequest.append(
						// True if tab exists; false otherwise
						"<ResidentialAddressIndicator>" + addressType + "</ResidentialAddressIndicator>" ); 	
			}
			xmlRequest.append("</AddressArtifactFormat>" +
					  "</TransitFrom>" +
					  "<TransitTo>" +
					    "<AddressArtifactFormat>" +
					      "<PoliticalDivision2></PoliticalDivision2>" +
					      "<PoliticalDivision1></PoliticalDivision1>" +
					      "<PostcodePrimaryLow>" + toZip + "</PostcodePrimaryLow>" +
					      "<CountryCode>" + toCountry + "</CountryCode>");
			if (addressType) {
				xmlRequest.append(
						// True if tab exists; false otherwise
						"<ResidentialAddressIndicator>" + addressType + "</ResidentialAddressIndicator>" ); 	
			}
			xmlRequest.append("</AddressArtifactFormat>" +
					  "</TransitTo>" +
					    "<PickupDate>" + formatDate() + "</PickupDate>" + //YYYYMMDD
					  //"<MaximumListSize></MaximumListSize>" +
					  //"<InvoiceLineTotal>" +
					  //  "<CurrencyCode>USD</CurrencyCode>" +
					  //  "<MonetaryValue>500</MonetaryValue>" +
					  //"</InvoiceLineTotal>" +
					  "<ShipmentWeight>" +
					    "<UnitOfMeasurement>" +
					      "<Code>LBS</Code>" +
					    "</UnitOfMeasurement>" +
					    "<Weight>" + weight + "</Weight>" +
					  "</ShipmentWeight>" +
					"</TimeInTransitRequest>");
					
					

			// POST data
			OutputStream oStream = connection.getOutputStream();
			oStream.write(xmlRequest.toString().getBytes());
			oStream.close();
			
            // retrieve results from UPS server
			SAXBuilder builder = new SAXBuilder(false);
            Document doc = builder.build(connection.getInputStream());
			Element response = doc.getRootElement().getChild("Response");
			String responseStatusCode = response.getChild("ResponseStatusCode").getText();
			
			// print out error message
			if (responseStatusCode.equals("0")) {
				System.out.println(response.getChild("Error").getChild("ErrorDescription").getText());
			}	
			
			// get a list of shipping rates
			else{
				List serviceSummaryList = doc.getRootElement().getChild("TransitResponse").getChildren("ServiceSummary");
				Iterator it = serviceSummaryList.iterator();
				while (it.hasNext()) {
					Element serviceSummary = (Element)it.next();
                    // construct ShippingRate object
					//ShippingRate shippingRate = new ShippingRate();
					//shippingRate.setCarrier("ups");	
					//shippingRate.setCode(serviceSummary.getChild("Service").getChild("Code").getText());
			        //shippingRate.setDaysInTransit(serviceSummary.getChild("EstimatedArrival").getChild("BusinessTransitDays").getText());
					if ( serviceSummary.getChild("Service").getChild("Code").getText().equals( "GND" ))
						return serviceSummary.getChild("EstimatedArrival").getChild("BusinessTransitDays").getText();
			        // add to ratesList
					//ratesList.add(shippingRate);
			    }
			
			}

	    } catch (Exception e) {
	    	e.printStackTrace();
	    }
		
		// return list
		return null;
		
	}
	
	public String formatDate(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd"); 
		return sdf.format( Calendar.getInstance().getTime() ).toString();
	}

}
