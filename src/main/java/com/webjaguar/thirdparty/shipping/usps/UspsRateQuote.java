/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 10.10.2006
 */

package com.webjaguar.thirdparty.shipping.usps;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.webjaguar.model.ShippingRate;

public class UspsRateQuote {

	private String host = "http://production.shippingapis.com/ShippingAPI.dll?API=RateV3&XML=";
	// Each website should have its own userId
	private int packageid = 1;
	private String machinable = "True";   // apply to parcel post only and service is ALL
	
    // size: Regular, Large, Oversize; By length + girth, 84, 108, 130 inch;
	
	public List<ShippingRate> getRates(String userId, String service, String zipOriginal, String zipDestination, boolean hasUnknownPackage, Double unknownPackageWeight, Map <Integer, Double> independentPackages,String size ){

		
		int numHeavyPackage = 0;
		//Double smallPackageWeight = Math.ceil(unknownPackageWeight);
		Double smallPackageWeight = unknownPackageWeight;

		// adjust weight if necessary
		if (hasUnknownPackage && smallPackageWeight < 0.01) {
			smallPackageWeight = 0.01;
		}
		if (smallPackageWeight > 70) {
			numHeavyPackage = (new Double(Math.floor(smallPackageWeight/70))).intValue();
			smallPackageWeight = smallPackageWeight - numHeavyPackage*70;
		}
		
		float ounces = ( smallPackageWeight.floatValue() - ((float) smallPackageWeight.intValue()) )*16;
		
		// list to be returned
		List<ShippingRate> ratesList = new ArrayList<ShippingRate>();
		
		try{
		    
			// construct url and xml query string
			StringBuffer xmlRequest = new StringBuffer();
			xmlRequest.append("<RateV3Request USERID=\"" + userId.trim() + "\">");
			for(int i=1; i<= numHeavyPackage; i++) {
				xmlRequest.append(
					"<Package ID=\"" + packageid + "\">" +
				        "<Service>" + service + "</Service>" +
				        "<ZipOrigination>" + zipOriginal + "</ZipOrigination>" +
				        "<ZipDestination>" + zipDestination + "</ZipDestination>" +
				        "<Pounds>" + 70 + "</Pounds>" +
				        "<Ounces>" + 0 + "</Ounces>" +
				        "<Size>" + size + "</Size>" +
				        "<Machinable>" + this.machinable + "</Machinable>" +
                    "</Package>");
				packageid++;
			}
			if (smallPackageWeight > 0) {
				xmlRequest.append(
						"<Package ID=\"" + packageid + "\">" +
				        "<Service>" + service + "</Service>" +
				        "<ZipOrigination>" + zipOriginal + "</ZipOrigination>" +
				        "<ZipDestination>" + zipDestination + "</ZipDestination>" +
				        "<Pounds>" +  smallPackageWeight.intValue() + "</Pounds>" +
				        "<Ounces>" + ounces + "</Ounces>" +
				        "<Size>" + size + "</Size>" +
				        "<Machinable>" + this.machinable + "</Machinable>" +
                    "</Package>");
				packageid++;
			}
			if(independentPackages != null) {
				for (Integer packageNumber : independentPackages.keySet()) {
					float ounce = independentPackages.get(packageNumber).floatValue() - ((float) independentPackages.get(packageNumber).intValue());
					
					xmlRequest.append(
							"<Package ID=\"" + packageid + "\">" +
					        "<Service>" + service + "</Service>" +
					        "<ZipOrigination>" + zipOriginal + "</ZipOrigination>" +
					        "<ZipDestination>" + zipDestination + "</ZipDestination>" +
					        "<Pounds>" +  independentPackages.get(packageNumber).intValue() + "</Pounds>" +
					        "<Ounces>" + ounce + "</Ounces>" +
					        "<Size>" + size + "</Size>" +
					        "<Machinable>" + this.machinable + "</Machinable>" +
	                    "</Package>");
					packageid++;
				}	
			}
			xmlRequest.append("</RateV3Request>");
			System.out.println("Request "+xmlRequest.toString());
			String urlString = this.host + URLEncoder.encode(xmlRequest.toString(), "UTF-8");
			// send request to USPS server
			URL url = new URL(urlString.toString());
			SAXBuilder builder = new SAXBuilder(false);
		    Document doc = builder.build(url);
		    
		    //XMLOutputter printer = new XMLOutputter();
		    // get response, check error message first
		    Element root = doc.getRootElement(); 
		    // error occured
		    if(root.getChild("Package").getChild("Error")!=null){
		    	Element error = (Element)root.getChild("Package").getChild("Error");
		    	ShippingRate shippingRate = new ShippingRate();
		    	shippingRate.setCarrier("usps");
				shippingRate.setCode(error.getChild("Number").getText());
				shippingRate.setPrice(error.getChild("Description").getText());
		    }
		    
		    // get rates list 
		    else{
		    	Map<String, Double> classValueMap = new HashMap<String, Double>();
		    	List packageList = root.getChildren("Package");
		    	for(Iterator<Element> packgeIterator=packageList.iterator();packgeIterator.hasNext();) {
		    		List<Element> postageList = packgeIterator.next().getChildren("Postage");
		    		for(Iterator it=postageList.iterator();it.hasNext();){
				    	Element postage = (Element)it.next();
				    	// since first class mail has multiple options with same classid 0, we can consider only Parcel that has highest value compare to Envelope, Post Card or Letter
				    	if(isFirstClassEnvelopeOrLetterOrPostCard(postage)) {
				    		continue;
				    	}
				    	ShippingRate shippingRate = new ShippingRate();
				    	shippingRate.setCarrier("usps");
				    	Double rate = Double.parseDouble(postage.getChild("Rate").getText());
				    	if(classValueMap.get(postage.getAttributeValue("CLASSID")) != null) {
				    		rate = rate + classValueMap.get(postage.getAttributeValue("CLASSID"));
				    	}
				    	classValueMap.put(postage.getAttributeValue("CLASSID"), rate);
				    }  
		    	}
		    	
		    	for(String classId : classValueMap.keySet()) {
		    		ShippingRate shippingRate = new ShippingRate();
		    		shippingRate.setCarrier("usps");
		    		shippingRate.setCode(classId);
			    	//shippingRate.setCode(postage.getChild("MailService").getText());
			    	shippingRate.setPrice(classValueMap.get(classId).toString());
			    	ratesList.add(shippingRate);
		    	}
		    } 
		} catch (Exception e) {
	    	//e.printStackTrace();
	    }
		
	// return list
	return ratesList;
	
	}
	
	// call this method to check if this option is First Class Envelope, Post Card or Letter
	private boolean isFirstClassEnvelopeOrLetterOrPostCard(Element postage){
		
		if(postage.getChild("MailService").getText().contains("First-Class Mail")) {
			if(postage.getChild("MailService").getText().contains("Envelope") || postage.getChild("MailService").getText().contains("Postcards") || postage.getChild("MailService").getText().contains("Letter")) {
				return true;	
			} 
		} 
		return false;
	}
}