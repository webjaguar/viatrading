/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 03.05.2008
 */

package com.webjaguar.thirdparty.shipping.usps;

import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.webjaguar.model.ShippingRate;

public class UspsIntRateQuote {

	private String host = "http://production.shippingapis.com/ShippingAPI.dll?API=IntlRate&XML=";
	// Each website should have its own userId
	private int packageid = 1;
	private String mailType = "Package"; // Other values: Postcards, Matter for the blind, Envelope
	
	
	public List<ShippingRate> getRates(String userId, String country, boolean hasUnknownPackage, Double unknownPackageWeight, Map <Integer, Double>  independentPackages, Double valuteOfContents){

		int numHeavyPackage = 0;
		//Double smallPackageWeight = Math.ceil(unknownPackageWeight);
		Double smallPackageWeight = unknownPackageWeight;

		// adjust weight if necessary
		if (hasUnknownPackage && smallPackageWeight < 0.01) {
			smallPackageWeight = 0.01;
		}
		if (smallPackageWeight > 70) {
			numHeavyPackage = (new Double(Math.floor(smallPackageWeight/70))).intValue();
			smallPackageWeight = smallPackageWeight - numHeavyPackage*70;
		}
		
		float ounces = ( smallPackageWeight.floatValue() - ((float) smallPackageWeight.intValue()) )*16;
		
		// list to be returned
		List<ShippingRate> ratesList = new ArrayList<ShippingRate>();
		
		try{
		    
			// construct url and xml query string
			StringBuffer xmlRequest = new StringBuffer();
			xmlRequest.append("<IntlRateRequest USERID=\"" + userId + "\">");
			for(int i=1; i<= numHeavyPackage; i++) {
				xmlRequest.append(
					"<Package ID=\"" + packageid + "\">" +
				        "<Pounds>" + 70 + "</Pounds>" +
				        "<Ounces>" + 0 + "</Ounces>" +
				        "<MailType>" + mailType + "</MailType>" +
				        "<Country>" + country +  "</Country>" +
                    "</Package>");
				packageid++;
			}
			if (smallPackageWeight > 0) {
				xmlRequest.append(
						"<Package ID=\"" + packageid + "\">" +
				        "<Pounds>" +  smallPackageWeight.intValue() + "</Pounds>" +
				        "<Ounces>" + Math.round( ounces*10 ) + "</Ounces>" +
				        "<MailType>" + mailType + "</MailType>" +
				        "<Country>" + country +  "</Country>" +
                "</Package>");
				packageid++;
			}
			if(independentPackages != null) {
				for (Integer packageNumber : independentPackages.keySet()) {
					float ounce = independentPackages.get(packageNumber).floatValue() - ((float) independentPackages.get(packageNumber).intValue());
					
					xmlRequest.append(
							"<Package ID=\"" + packageid + "\">" +
					        "<Pounds>" +  independentPackages.get(packageNumber).intValue() + "</Pounds>" +
					        "<Ounces>" + Math.round( ounce*10 ) + "</Ounces>" +
					        "<MailType>" + mailType + "</MailType>" +
					        "<Country>" + country +  "</Country>" +
	                    "</Package>");
					packageid++;
				}	
			}
			xmlRequest.append("</IntlRateRequest>");
			
		    // send request to USPS server
			URL url = new URL(this.host + URLEncoder.encode(xmlRequest.toString(), "UTF-8"));
			SAXBuilder builder = new SAXBuilder(false);
		    Document doc = builder.build(url);
		    
		    // XMLOutputter printer = new XMLOutputter();
		    // get response, check error message first
		    Element root = doc.getRootElement(); 
		   
		    // error occured
		    if(root.getChild("Package").getChild("Error")!=null){
		    	Element error = (Element)root.getChild("Package").getChild("Error");
		    	ShippingRate shippingRate = new ShippingRate();
		    	shippingRate.setCarrier("usps");
				shippingRate.setCode(error.getChild("Number").getText());
				shippingRate.setPrice(error.getChild("Description").getText());
		    }
		    
		    // get rates list 
		    else{
		        Map<String, Double> classValueMap = new HashMap<String, Double>();
		        Map<String, Integer> packageCount = new HashMap<String, Integer>();
		    	List packageList = root.getChildren("Package");
		    	for(Iterator<Element> packgeIterator=packageList.iterator();packgeIterator.hasNext();) {
		    		List<Element> serviceList = packgeIterator.next().getChildren("Service");
		    		for(Iterator it=serviceList.iterator();it.hasNext();){
				    	Element service = (Element)it.next();
				    	ShippingRate shippingRate = new ShippingRate();
				    	shippingRate.setCarrier("usps");
				    	Double rate = Double.parseDouble(service.getChild("Postage").getText());
				    	if(classValueMap.get(service.getAttributeValue("ID")) != null) {
				    		rate = rate + classValueMap.get(service.getAttributeValue("ID"));
				    	}
				    	classValueMap.put(service.getAttributeValue("ID"), rate);
				    	
				    	if(packageCount.get(service.getAttributeValue("ID")) != null) {
				    		packageCount.put(service.getAttributeValue("ID"), packageCount.get(service.getAttributeValue("ID")) + 1);
				    	} else {
				    		packageCount.put(service.getAttributeValue("ID"), 1);
						}
				    }  
		    	}
		    	
		    	for(String Id : classValueMap.keySet()) {
		    		// Do not get shipping option, if it does not return quote for all packages.
		    		if(packageCount.get(Id) < (packageid - 1)) {
		    			continue;
					}
		    		ShippingRate shippingRate = new ShippingRate();
		    		shippingRate.setCarrier("usps");
		    		shippingRate.setCode("Int "+Id);
		    		shippingRate.setPrice(classValueMap.get(Id).toString());
					ratesList.add(shippingRate);
		    	}
		    } 
		} catch (Exception e) {
	    	e.printStackTrace();
	    }
		
	// return list
	return ratesList;
	
	}
}