/**
 * RatedPackageDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.rating;


/**
 * If requesting rates using the PackageDetails element (one package
 * at a time) in the request, the rates for each package will be returned
 * in this element. Currently total piece total weight rates are also
 * retuned in this element.
 */
public class RatedPackageDetail  implements java.io.Serializable {
    private TrackingId trackingId;

    private Money effectiveNetDiscount;

    /* Ground COD is package level. */
    private Money adjustedCodCollectionAmount;

    private OversizeClassType oversizeClass;

    private PackageRateDetail packageRateDetail;

    public RatedPackageDetail() {
    }

    public RatedPackageDetail(
           TrackingId trackingId,
           Money effectiveNetDiscount,
           Money adjustedCodCollectionAmount,
           OversizeClassType oversizeClass,
           PackageRateDetail packageRateDetail) {
           this.trackingId = trackingId;
           this.effectiveNetDiscount = effectiveNetDiscount;
           this.adjustedCodCollectionAmount = adjustedCodCollectionAmount;
           this.oversizeClass = oversizeClass;
           this.packageRateDetail = packageRateDetail;
    }


    /**
     * Gets the trackingId value for this RatedPackageDetail.
     * 
     * @return trackingId
     */
    public TrackingId getTrackingId() {
        return trackingId;
    }


    /**
     * Sets the trackingId value for this RatedPackageDetail.
     * 
     * @param trackingId
     */
    public void setTrackingId(TrackingId trackingId) {
        this.trackingId = trackingId;
    }


    /**
     * Gets the effectiveNetDiscount value for this RatedPackageDetail.
     * 
     * @return effectiveNetDiscount
     */
    public Money getEffectiveNetDiscount() {
        return effectiveNetDiscount;
    }


    /**
     * Sets the effectiveNetDiscount value for this RatedPackageDetail.
     * 
     * @param effectiveNetDiscount
     */
    public void setEffectiveNetDiscount(Money effectiveNetDiscount) {
        this.effectiveNetDiscount = effectiveNetDiscount;
    }


    /**
     * Gets the adjustedCodCollectionAmount value for this RatedPackageDetail.
     * 
     * @return adjustedCodCollectionAmount   * Ground COD is package level.
     */
    public Money getAdjustedCodCollectionAmount() {
        return adjustedCodCollectionAmount;
    }


    /**
     * Sets the adjustedCodCollectionAmount value for this RatedPackageDetail.
     * 
     * @param adjustedCodCollectionAmount   * Ground COD is package level.
     */
    public void setAdjustedCodCollectionAmount(Money adjustedCodCollectionAmount) {
        this.adjustedCodCollectionAmount = adjustedCodCollectionAmount;
    }


    /**
     * Gets the oversizeClass value for this RatedPackageDetail.
     * 
     * @return oversizeClass
     */
    public OversizeClassType getOversizeClass() {
        return oversizeClass;
    }


    /**
     * Sets the oversizeClass value for this RatedPackageDetail.
     * 
     * @param oversizeClass
     */
    public void setOversizeClass(OversizeClassType oversizeClass) {
        this.oversizeClass = oversizeClass;
    }


    /**
     * Gets the packageRateDetail value for this RatedPackageDetail.
     * 
     * @return packageRateDetail
     */
    public PackageRateDetail getPackageRateDetail() {
        return packageRateDetail;
    }


    /**
     * Sets the packageRateDetail value for this RatedPackageDetail.
     * 
     * @param packageRateDetail
     */
    public void setPackageRateDetail(PackageRateDetail packageRateDetail) {
        this.packageRateDetail = packageRateDetail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RatedPackageDetail)) return false;
        RatedPackageDetail other = (RatedPackageDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.trackingId==null && other.getTrackingId()==null) || 
             (this.trackingId!=null &&
              this.trackingId.equals(other.getTrackingId()))) &&
            ((this.effectiveNetDiscount==null && other.getEffectiveNetDiscount()==null) || 
             (this.effectiveNetDiscount!=null &&
              this.effectiveNetDiscount.equals(other.getEffectiveNetDiscount()))) &&
            ((this.adjustedCodCollectionAmount==null && other.getAdjustedCodCollectionAmount()==null) || 
             (this.adjustedCodCollectionAmount!=null &&
              this.adjustedCodCollectionAmount.equals(other.getAdjustedCodCollectionAmount()))) &&
            ((this.oversizeClass==null && other.getOversizeClass()==null) || 
             (this.oversizeClass!=null &&
              this.oversizeClass.equals(other.getOversizeClass()))) &&
            ((this.packageRateDetail==null && other.getPackageRateDetail()==null) || 
             (this.packageRateDetail!=null &&
              this.packageRateDetail.equals(other.getPackageRateDetail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTrackingId() != null) {
            _hashCode += getTrackingId().hashCode();
        }
        if (getEffectiveNetDiscount() != null) {
            _hashCode += getEffectiveNetDiscount().hashCode();
        }
        if (getAdjustedCodCollectionAmount() != null) {
            _hashCode += getAdjustedCodCollectionAmount().hashCode();
        }
        if (getOversizeClass() != null) {
            _hashCode += getOversizeClass().hashCode();
        }
        if (getPackageRateDetail() != null) {
            _hashCode += getPackageRateDetail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RatedPackageDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "RatedPackageDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackingId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "TrackingId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "TrackingId"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("effectiveNetDiscount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "EffectiveNetDiscount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Money"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("adjustedCodCollectionAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "AdjustedCodCollectionAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Money"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("oversizeClass");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "OversizeClass"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "OversizeClassType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packageRateDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "PackageRateDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "PackageRateDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
