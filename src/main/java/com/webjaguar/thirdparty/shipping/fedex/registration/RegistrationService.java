/**
 * RegistrationService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.registration;

public interface RegistrationService extends javax.xml.rpc.Service {
    public java.lang.String getRegistrationServicePortAddress();

    public com.webjaguar.thirdparty.shipping.fedex.registration.RegistrationPortType getRegistrationServicePort() throws javax.xml.rpc.ServiceException;

    public com.webjaguar.thirdparty.shipping.fedex.registration.RegistrationPortType getRegistrationServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
