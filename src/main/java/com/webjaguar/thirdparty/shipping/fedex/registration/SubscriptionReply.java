/**
 * SubscriptionReply.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.registration;


/**
 * The descriptive data returned for a FedEx Web Services SubscriptionRequest
 * to register a customer's account. A unique meter number specific to
 * the customer's FedEx account number is returned to the client. The
 * meter number should be used in all subsequent requests sent to FedEx
 * Web Services.
 */
public class SubscriptionReply  implements java.io.Serializable {
    /* Identifies the highest severity encountered when executing
     * the request; in order from high to low: FAILURE, ERROR, WARNING, NOTE,
     * SUCCESS. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.NotificationSeverityType highestSeverity;

    /* The descriptive data detailing the status of a sumbitted transaction. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.Notification[] notifications;

    /* Descriptive data that governs data payload language/translations. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail;

    /* Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply). */
    private com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version;

    /* Identifies the unique client device (i.e. meter number) assigned
     * by FedEx and associated with the FedEx account number sent in the
     * request. The returned meter number should be used with all subsequent
     * FedEx Web Services requests. */
    private java.lang.String meterNumber;

    /* Identifies the FedEx shipping service type(s) that the caller
     * has been registered to use. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionServiceType[] registeredServices;

    public SubscriptionReply() {
    }

    public SubscriptionReply(
           com.webjaguar.thirdparty.shipping.fedex.registration.NotificationSeverityType highestSeverity,
           com.webjaguar.thirdparty.shipping.fedex.registration.Notification[] notifications,
           com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail,
           com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version,
           java.lang.String meterNumber,
           com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionServiceType[] registeredServices) {
           this.highestSeverity = highestSeverity;
           this.notifications = notifications;
           this.transactionDetail = transactionDetail;
           this.version = version;
           this.meterNumber = meterNumber;
           this.registeredServices = registeredServices;
    }


    /**
     * Gets the highestSeverity value for this SubscriptionReply.
     * 
     * @return highestSeverity   * Identifies the highest severity encountered when executing
     * the request; in order from high to low: FAILURE, ERROR, WARNING, NOTE,
     * SUCCESS.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.NotificationSeverityType getHighestSeverity() {
        return highestSeverity;
    }


    /**
     * Sets the highestSeverity value for this SubscriptionReply.
     * 
     * @param highestSeverity   * Identifies the highest severity encountered when executing
     * the request; in order from high to low: FAILURE, ERROR, WARNING, NOTE,
     * SUCCESS.
     */
    public void setHighestSeverity(com.webjaguar.thirdparty.shipping.fedex.registration.NotificationSeverityType highestSeverity) {
        this.highestSeverity = highestSeverity;
    }


    /**
     * Gets the notifications value for this SubscriptionReply.
     * 
     * @return notifications   * The descriptive data detailing the status of a sumbitted transaction.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.Notification[] getNotifications() {
        return notifications;
    }


    /**
     * Sets the notifications value for this SubscriptionReply.
     * 
     * @param notifications   * The descriptive data detailing the status of a sumbitted transaction.
     */
    public void setNotifications(com.webjaguar.thirdparty.shipping.fedex.registration.Notification[] notifications) {
        this.notifications = notifications;
    }

    public com.webjaguar.thirdparty.shipping.fedex.registration.Notification getNotifications(int i) {
        return this.notifications[i];
    }

    public void setNotifications(int i, com.webjaguar.thirdparty.shipping.fedex.registration.Notification _value) {
        this.notifications[i] = _value;
    }


    /**
     * Gets the transactionDetail value for this SubscriptionReply.
     * 
     * @return transactionDetail   * Descriptive data that governs data payload language/translations.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail getTransactionDetail() {
        return transactionDetail;
    }


    /**
     * Sets the transactionDetail value for this SubscriptionReply.
     * 
     * @param transactionDetail   * Descriptive data that governs data payload language/translations.
     */
    public void setTransactionDetail(com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail) {
        this.transactionDetail = transactionDetail;
    }


    /**
     * Gets the version value for this SubscriptionReply.
     * 
     * @return version   * Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply).
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.VersionId getVersion() {
        return version;
    }


    /**
     * Sets the version value for this SubscriptionReply.
     * 
     * @param version   * Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply).
     */
    public void setVersion(com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version) {
        this.version = version;
    }


    /**
     * Gets the meterNumber value for this SubscriptionReply.
     * 
     * @return meterNumber   * Identifies the unique client device (i.e. meter number) assigned
     * by FedEx and associated with the FedEx account number sent in the
     * request. The returned meter number should be used with all subsequent
     * FedEx Web Services requests.
     */
    public java.lang.String getMeterNumber() {
        return meterNumber;
    }


    /**
     * Sets the meterNumber value for this SubscriptionReply.
     * 
     * @param meterNumber   * Identifies the unique client device (i.e. meter number) assigned
     * by FedEx and associated with the FedEx account number sent in the
     * request. The returned meter number should be used with all subsequent
     * FedEx Web Services requests.
     */
    public void setMeterNumber(java.lang.String meterNumber) {
        this.meterNumber = meterNumber;
    }


    /**
     * Gets the registeredServices value for this SubscriptionReply.
     * 
     * @return registeredServices   * Identifies the FedEx shipping service type(s) that the caller
     * has been registered to use.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionServiceType[] getRegisteredServices() {
        return registeredServices;
    }


    /**
     * Sets the registeredServices value for this SubscriptionReply.
     * 
     * @param registeredServices   * Identifies the FedEx shipping service type(s) that the caller
     * has been registered to use.
     */
    public void setRegisteredServices(com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionServiceType[] registeredServices) {
        this.registeredServices = registeredServices;
    }

    public com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionServiceType getRegisteredServices(int i) {
        return this.registeredServices[i];
    }

    public void setRegisteredServices(int i, com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionServiceType _value) {
        this.registeredServices[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubscriptionReply)) return false;
        SubscriptionReply other = (SubscriptionReply) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.highestSeverity==null && other.getHighestSeverity()==null) || 
             (this.highestSeverity!=null &&
              this.highestSeverity.equals(other.getHighestSeverity()))) &&
            ((this.notifications==null && other.getNotifications()==null) || 
             (this.notifications!=null &&
              java.util.Arrays.equals(this.notifications, other.getNotifications()))) &&
            ((this.transactionDetail==null && other.getTransactionDetail()==null) || 
             (this.transactionDetail!=null &&
              this.transactionDetail.equals(other.getTransactionDetail()))) &&
            ((this.version==null && other.getVersion()==null) || 
             (this.version!=null &&
              this.version.equals(other.getVersion()))) &&
            ((this.meterNumber==null && other.getMeterNumber()==null) || 
             (this.meterNumber!=null &&
              this.meterNumber.equals(other.getMeterNumber()))) &&
            ((this.registeredServices==null && other.getRegisteredServices()==null) || 
             (this.registeredServices!=null &&
              java.util.Arrays.equals(this.registeredServices, other.getRegisteredServices())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHighestSeverity() != null) {
            _hashCode += getHighestSeverity().hashCode();
        }
        if (getNotifications() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNotifications());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNotifications(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTransactionDetail() != null) {
            _hashCode += getTransactionDetail().hashCode();
        }
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        if (getMeterNumber() != null) {
            _hashCode += getMeterNumber().hashCode();
        }
        if (getRegisteredServices() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRegisteredServices());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRegisteredServices(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubscriptionReply.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "SubscriptionReply"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("highestSeverity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "HighestSeverity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "NotificationSeverityType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notifications");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Notifications"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Notification"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "TransactionDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "TransactionDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionId"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("meterNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "MeterNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registeredServices");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "RegisteredServices"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "SubscriptionServiceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
