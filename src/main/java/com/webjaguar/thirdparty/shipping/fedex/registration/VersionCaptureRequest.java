/**
 * VersionCaptureRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.registration;


/**
 * FedEx SSP Version Capture request.
 */
public class VersionCaptureRequest  implements java.io.Serializable {
    /* The descriptive data to be used in authentication of the sender's
     * identity (and right to use FedEx web services). */
    private com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail webAuthenticationDetail;

    /* The descriptive data identifying the client submitting the
     * transaction. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail clientDetail;

    /* The descriptive data for this customer transaction. The TransactionDetail
     * from the request is echoed back to the caller in the corresponding
     * reply. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail;

    /* Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply). */
    private com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version;

    /* Obtained by sending a Zip inquiry transaction */
    private java.lang.String originLocationId;

    /* Vendor product platform. */
    private java.lang.String vendorProductPlatform;

    public VersionCaptureRequest() {
    }

    public VersionCaptureRequest(
           com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail webAuthenticationDetail,
           com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail clientDetail,
           com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail,
           com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version,
           java.lang.String originLocationId,
           java.lang.String vendorProductPlatform) {
           this.webAuthenticationDetail = webAuthenticationDetail;
           this.clientDetail = clientDetail;
           this.transactionDetail = transactionDetail;
           this.version = version;
           this.originLocationId = originLocationId;
           this.vendorProductPlatform = vendorProductPlatform;
    }


    /**
     * Gets the webAuthenticationDetail value for this VersionCaptureRequest.
     * 
     * @return webAuthenticationDetail   * The descriptive data to be used in authentication of the sender's
     * identity (and right to use FedEx web services).
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail getWebAuthenticationDetail() {
        return webAuthenticationDetail;
    }


    /**
     * Sets the webAuthenticationDetail value for this VersionCaptureRequest.
     * 
     * @param webAuthenticationDetail   * The descriptive data to be used in authentication of the sender's
     * identity (and right to use FedEx web services).
     */
    public void setWebAuthenticationDetail(com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail webAuthenticationDetail) {
        this.webAuthenticationDetail = webAuthenticationDetail;
    }


    /**
     * Gets the clientDetail value for this VersionCaptureRequest.
     * 
     * @return clientDetail   * The descriptive data identifying the client submitting the
     * transaction.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail getClientDetail() {
        return clientDetail;
    }


    /**
     * Sets the clientDetail value for this VersionCaptureRequest.
     * 
     * @param clientDetail   * The descriptive data identifying the client submitting the
     * transaction.
     */
    public void setClientDetail(com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail clientDetail) {
        this.clientDetail = clientDetail;
    }


    /**
     * Gets the transactionDetail value for this VersionCaptureRequest.
     * 
     * @return transactionDetail   * The descriptive data for this customer transaction. The TransactionDetail
     * from the request is echoed back to the caller in the corresponding
     * reply.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail getTransactionDetail() {
        return transactionDetail;
    }


    /**
     * Sets the transactionDetail value for this VersionCaptureRequest.
     * 
     * @param transactionDetail   * The descriptive data for this customer transaction. The TransactionDetail
     * from the request is echoed back to the caller in the corresponding
     * reply.
     */
    public void setTransactionDetail(com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail) {
        this.transactionDetail = transactionDetail;
    }


    /**
     * Gets the version value for this VersionCaptureRequest.
     * 
     * @return version   * Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply).
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.VersionId getVersion() {
        return version;
    }


    /**
     * Sets the version value for this VersionCaptureRequest.
     * 
     * @param version   * Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply).
     */
    public void setVersion(com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version) {
        this.version = version;
    }


    /**
     * Gets the originLocationId value for this VersionCaptureRequest.
     * 
     * @return originLocationId   * Obtained by sending a Zip inquiry transaction
     */
    public java.lang.String getOriginLocationId() {
        return originLocationId;
    }


    /**
     * Sets the originLocationId value for this VersionCaptureRequest.
     * 
     * @param originLocationId   * Obtained by sending a Zip inquiry transaction
     */
    public void setOriginLocationId(java.lang.String originLocationId) {
        this.originLocationId = originLocationId;
    }


    /**
     * Gets the vendorProductPlatform value for this VersionCaptureRequest.
     * 
     * @return vendorProductPlatform   * Vendor product platform.
     */
    public java.lang.String getVendorProductPlatform() {
        return vendorProductPlatform;
    }


    /**
     * Sets the vendorProductPlatform value for this VersionCaptureRequest.
     * 
     * @param vendorProductPlatform   * Vendor product platform.
     */
    public void setVendorProductPlatform(java.lang.String vendorProductPlatform) {
        this.vendorProductPlatform = vendorProductPlatform;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof VersionCaptureRequest)) return false;
        VersionCaptureRequest other = (VersionCaptureRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.webAuthenticationDetail==null && other.getWebAuthenticationDetail()==null) || 
             (this.webAuthenticationDetail!=null &&
              this.webAuthenticationDetail.equals(other.getWebAuthenticationDetail()))) &&
            ((this.clientDetail==null && other.getClientDetail()==null) || 
             (this.clientDetail!=null &&
              this.clientDetail.equals(other.getClientDetail()))) &&
            ((this.transactionDetail==null && other.getTransactionDetail()==null) || 
             (this.transactionDetail!=null &&
              this.transactionDetail.equals(other.getTransactionDetail()))) &&
            ((this.version==null && other.getVersion()==null) || 
             (this.version!=null &&
              this.version.equals(other.getVersion()))) &&
            ((this.originLocationId==null && other.getOriginLocationId()==null) || 
             (this.originLocationId!=null &&
              this.originLocationId.equals(other.getOriginLocationId()))) &&
            ((this.vendorProductPlatform==null && other.getVendorProductPlatform()==null) || 
             (this.vendorProductPlatform!=null &&
              this.vendorProductPlatform.equals(other.getVendorProductPlatform())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getWebAuthenticationDetail() != null) {
            _hashCode += getWebAuthenticationDetail().hashCode();
        }
        if (getClientDetail() != null) {
            _hashCode += getClientDetail().hashCode();
        }
        if (getTransactionDetail() != null) {
            _hashCode += getTransactionDetail().hashCode();
        }
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        if (getOriginLocationId() != null) {
            _hashCode += getOriginLocationId().hashCode();
        }
        if (getVendorProductPlatform() != null) {
            _hashCode += getVendorProductPlatform().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(VersionCaptureRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionCaptureRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("webAuthenticationDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "WebAuthenticationDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "WebAuthenticationDetail"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ClientDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ClientDetail"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "TransactionDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "TransactionDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionId"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("originLocationId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "OriginLocationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendorProductPlatform");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VendorProductPlatform"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
