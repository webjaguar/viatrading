/**
 * RegisterCSPUserRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.registration;

/**
 * The descriptive data sent by the customer to obtain the end user
 * authentication credentials required to use FedEx Web Services. This
 * is typically a one-time only request for each customer. A unique key
 * and password will be returned to the client in the reply. This end
 * user key and password inherits the services enabled for the CSP credentials
 * and must be passed in all subsequent requests sent to FedEx web services.
 */
public class RegisterCSPUserRequest  implements java.io.Serializable {
    /* The descriptive data to be used in authentication of the sender's
     * identity (and right to use FedEx web services). */
    private com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail webAuthenticationDetail;

    /* Descriptive data identifying the client submitting the transaction. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail clientDetail;

    /* Descriptive data for this customer transaction. The TransactionDetail
     * from the request is echoed back to the caller in the corresponding
     * reply. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail;

    /* Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply). */
    private com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version;

    /* Billing address of the user */
    private com.webjaguar.thirdparty.shipping.fedex.registration.Address billingAddress;

    /* Contact and address data for this specific installation */
    private com.webjaguar.thirdparty.shipping.fedex.registration.ParsedContactAndAddress userContactAndAddress;

    /* Alternate means of contacting someone for this installation */
    private java.lang.String secondaryEmail;

    public RegisterCSPUserRequest() {
    }

    public RegisterCSPUserRequest(
           com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail webAuthenticationDetail,
           com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail clientDetail,
           com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail,
           com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version,
           com.webjaguar.thirdparty.shipping.fedex.registration.Address billingAddress,
           com.webjaguar.thirdparty.shipping.fedex.registration.ParsedContactAndAddress userContactAndAddress,
           java.lang.String secondaryEmail) {
           this.webAuthenticationDetail = webAuthenticationDetail;
           this.clientDetail = clientDetail;
           this.transactionDetail = transactionDetail;
           this.version = version;
           this.billingAddress = billingAddress;
           this.userContactAndAddress = userContactAndAddress;
           this.secondaryEmail = secondaryEmail;
    }


    /**
     * Gets the webAuthenticationDetail value for this RegisterCSPUserRequest.
     * 
     * @return webAuthenticationDetail   * The descriptive data to be used in authentication of the sender's
     * identity (and right to use FedEx web services).
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail getWebAuthenticationDetail() {
        return webAuthenticationDetail;
    }


    /**
     * Sets the webAuthenticationDetail value for this RegisterCSPUserRequest.
     * 
     * @param webAuthenticationDetail   * The descriptive data to be used in authentication of the sender's
     * identity (and right to use FedEx web services).
     */
    public void setWebAuthenticationDetail(com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail webAuthenticationDetail) {
        this.webAuthenticationDetail = webAuthenticationDetail;
    }


    /**
     * Gets the clientDetail value for this RegisterCSPUserRequest.
     * 
     * @return clientDetail   * Descriptive data identifying the client submitting the transaction.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail getClientDetail() {
        return clientDetail;
    }


    /**
     * Sets the clientDetail value for this RegisterCSPUserRequest.
     * 
     * @param clientDetail   * Descriptive data identifying the client submitting the transaction.
     */
    public void setClientDetail(com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail clientDetail) {
        this.clientDetail = clientDetail;
    }


    /**
     * Gets the transactionDetail value for this RegisterCSPUserRequest.
     * 
     * @return transactionDetail   * Descriptive data for this customer transaction. The TransactionDetail
     * from the request is echoed back to the caller in the corresponding
     * reply.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail getTransactionDetail() {
        return transactionDetail;
    }


    /**
     * Sets the transactionDetail value for this RegisterCSPUserRequest.
     * 
     * @param transactionDetail   * Descriptive data for this customer transaction. The TransactionDetail
     * from the request is echoed back to the caller in the corresponding
     * reply.
     */
    public void setTransactionDetail(com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail) {
        this.transactionDetail = transactionDetail;
    }


    /**
     * Gets the version value for this RegisterCSPUserRequest.
     * 
     * @return version   * Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply).
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.VersionId getVersion() {
        return version;
    }


    /**
     * Sets the version value for this RegisterCSPUserRequest.
     * 
     * @param version   * Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply).
     */
    public void setVersion(com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version) {
        this.version = version;
    }


    /**
     * Gets the billingAddress value for this RegisterCSPUserRequest.
     * 
     * @return billingAddress   * Billing address of the user
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.Address getBillingAddress() {
        return billingAddress;
    }


    /**
     * Sets the billingAddress value for this RegisterCSPUserRequest.
     * 
     * @param billingAddress   * Billing address of the user
     */
    public void setBillingAddress(com.webjaguar.thirdparty.shipping.fedex.registration.Address billingAddress) {
        this.billingAddress = billingAddress;
    }


    /**
     * Gets the userContactAndAddress value for this RegisterCSPUserRequest.
     * 
     * @return userContactAndAddress   * Contact and address data for this specific installation
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.ParsedContactAndAddress getUserContactAndAddress() {
        return userContactAndAddress;
    }


    /**
     * Sets the userContactAndAddress value for this RegisterCSPUserRequest.
     * 
     * @param userContactAndAddress   * Contact and address data for this specific installation
     */
    public void setUserContactAndAddress(com.webjaguar.thirdparty.shipping.fedex.registration.ParsedContactAndAddress userContactAndAddress) {
        this.userContactAndAddress = userContactAndAddress;
    }


    /**
     * Gets the secondaryEmail value for this RegisterCSPUserRequest.
     * 
     * @return secondaryEmail   * Alternate means of contacting someone for this installation
     */
    public java.lang.String getSecondaryEmail() {
        return secondaryEmail;
    }


    /**
     * Sets the secondaryEmail value for this RegisterCSPUserRequest.
     * 
     * @param secondaryEmail   * Alternate means of contacting someone for this installation
     */
    public void setSecondaryEmail(java.lang.String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RegisterCSPUserRequest)) return false;
        RegisterCSPUserRequest other = (RegisterCSPUserRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.webAuthenticationDetail==null && other.getWebAuthenticationDetail()==null) || 
             (this.webAuthenticationDetail!=null &&
              this.webAuthenticationDetail.equals(other.getWebAuthenticationDetail()))) &&
            ((this.clientDetail==null && other.getClientDetail()==null) || 
             (this.clientDetail!=null &&
              this.clientDetail.equals(other.getClientDetail()))) &&
            ((this.transactionDetail==null && other.getTransactionDetail()==null) || 
             (this.transactionDetail!=null &&
              this.transactionDetail.equals(other.getTransactionDetail()))) &&
            ((this.version==null && other.getVersion()==null) || 
             (this.version!=null &&
              this.version.equals(other.getVersion()))) &&
            ((this.billingAddress==null && other.getBillingAddress()==null) || 
             (this.billingAddress!=null &&
              this.billingAddress.equals(other.getBillingAddress()))) &&
            ((this.userContactAndAddress==null && other.getUserContactAndAddress()==null) || 
             (this.userContactAndAddress!=null &&
              this.userContactAndAddress.equals(other.getUserContactAndAddress()))) &&
            ((this.secondaryEmail==null && other.getSecondaryEmail()==null) || 
             (this.secondaryEmail!=null &&
              this.secondaryEmail.equals(other.getSecondaryEmail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getWebAuthenticationDetail() != null) {
            _hashCode += getWebAuthenticationDetail().hashCode();
        }
        if (getClientDetail() != null) {
            _hashCode += getClientDetail().hashCode();
        }
        if (getTransactionDetail() != null) {
            _hashCode += getTransactionDetail().hashCode();
        }
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        if (getBillingAddress() != null) {
            _hashCode += getBillingAddress().hashCode();
        }
        if (getUserContactAndAddress() != null) {
            _hashCode += getUserContactAndAddress().hashCode();
        }
        if (getSecondaryEmail() != null) {
            _hashCode += getSecondaryEmail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RegisterCSPUserRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "RegisterCSPUserRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("webAuthenticationDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "WebAuthenticationDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "WebAuthenticationDetail"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ClientDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ClientDetail"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "TransactionDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "TransactionDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionId"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billingAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "BillingAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Address"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userContactAndAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "UserContactAndAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ParsedContactAndAddress"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("secondaryEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "SecondaryEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
