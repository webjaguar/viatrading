/**
 * RateRequestPackageSummary.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.rating;


/**
 * Details about a multiple piece shipment rate request. Use this
 * to rate a total piece total weight shipemnt.
 */
public class RateRequestPackageSummary  implements java.io.Serializable {
    /* The total number of pieces in this shipment. */
    private org.apache.axis.types.PositiveInteger pieceCount;

    /* The total weight of all the pieces in this shipment. */
    private Weight totalWeight;

    /* The dimensions that are to be applied to each piece in this
     * shipment. One set of dimensions will applied to each piece. */
    private Dimensions perPieceDimensions;

    /* The total amount of insurance requested for this shipment.
     * This amount has 2 explicit decimal positions and has a max length
     * of 11 including the decimal. */
    private Money totalInsuredValue;

    /* Descriptive data providing details about how to calculate variable
     * handling charges at the package level. */
    private VariableHandlingChargeDetail variableHandlingChargeDetail;

    /* Descriptive data regarding special services requested by the
     * shipper for this shipment. If the shipper is requesting a special
     * service which requires additional data (e.g. COD), the special service
     * type must be present in the specialServiceTypes collection, and the
     * supporting detail must be provided in the appropriate sub-object.
     * For example, to request COD, "COD" must be included in the SpecialServiceTypes
     * collection and the CodDetail object must contain the required data. */
    private PackageSpecialServicesRequested specialServicesRequested;

    public RateRequestPackageSummary() {
    }

    public RateRequestPackageSummary(
           org.apache.axis.types.PositiveInteger pieceCount,
           Weight totalWeight,
           Dimensions perPieceDimensions,
           Money totalInsuredValue,
           VariableHandlingChargeDetail variableHandlingChargeDetail,
           PackageSpecialServicesRequested specialServicesRequested) {
           this.pieceCount = pieceCount;
           this.totalWeight = totalWeight;
           this.perPieceDimensions = perPieceDimensions;
           this.totalInsuredValue = totalInsuredValue;
           this.variableHandlingChargeDetail = variableHandlingChargeDetail;
           this.specialServicesRequested = specialServicesRequested;
    }


    /**
     * Gets the pieceCount value for this RateRequestPackageSummary.
     * 
     * @return pieceCount   * The total number of pieces in this shipment.
     */
    public org.apache.axis.types.PositiveInteger getPieceCount() {
        return pieceCount;
    }


    /**
     * Sets the pieceCount value for this RateRequestPackageSummary.
     * 
     * @param pieceCount   * The total number of pieces in this shipment.
     */
    public void setPieceCount(org.apache.axis.types.PositiveInteger pieceCount) {
        this.pieceCount = pieceCount;
    }


    /**
     * Gets the totalWeight value for this RateRequestPackageSummary.
     * 
     * @return totalWeight   * The total weight of all the pieces in this shipment.
     */
    public Weight getTotalWeight() {
        return totalWeight;
    }


    /**
     * Sets the totalWeight value for this RateRequestPackageSummary.
     * 
     * @param totalWeight   * The total weight of all the pieces in this shipment.
     */
    public void setTotalWeight(Weight totalWeight) {
        this.totalWeight = totalWeight;
    }


    /**
     * Gets the perPieceDimensions value for this RateRequestPackageSummary.
     * 
     * @return perPieceDimensions   * The dimensions that are to be applied to each piece in this
     * shipment. One set of dimensions will applied to each piece.
     */
    public Dimensions getPerPieceDimensions() {
        return perPieceDimensions;
    }


    /**
     * Sets the perPieceDimensions value for this RateRequestPackageSummary.
     * 
     * @param perPieceDimensions   * The dimensions that are to be applied to each piece in this
     * shipment. One set of dimensions will applied to each piece.
     */
    public void setPerPieceDimensions(Dimensions perPieceDimensions) {
        this.perPieceDimensions = perPieceDimensions;
    }


    /**
     * Gets the totalInsuredValue value for this RateRequestPackageSummary.
     * 
     * @return totalInsuredValue   * The total amount of insurance requested for this shipment.
     * This amount has 2 explicit decimal positions and has a max length
     * of 11 including the decimal.
     */
    public Money getTotalInsuredValue() {
        return totalInsuredValue;
    }


    /**
     * Sets the totalInsuredValue value for this RateRequestPackageSummary.
     * 
     * @param totalInsuredValue   * The total amount of insurance requested for this shipment.
     * This amount has 2 explicit decimal positions and has a max length
     * of 11 including the decimal.
     */
    public void setTotalInsuredValue(Money totalInsuredValue) {
        this.totalInsuredValue = totalInsuredValue;
    }


    /**
     * Gets the variableHandlingChargeDetail value for this RateRequestPackageSummary.
     * 
     * @return variableHandlingChargeDetail   * Descriptive data providing details about how to calculate variable
     * handling charges at the package level.
     */
    public VariableHandlingChargeDetail getVariableHandlingChargeDetail() {
        return variableHandlingChargeDetail;
    }


    /**
     * Sets the variableHandlingChargeDetail value for this RateRequestPackageSummary.
     * 
     * @param variableHandlingChargeDetail   * Descriptive data providing details about how to calculate variable
     * handling charges at the package level.
     */
    public void setVariableHandlingChargeDetail(VariableHandlingChargeDetail variableHandlingChargeDetail) {
        this.variableHandlingChargeDetail = variableHandlingChargeDetail;
    }


    /**
     * Gets the specialServicesRequested value for this RateRequestPackageSummary.
     * 
     * @return specialServicesRequested   * Descriptive data regarding special services requested by the
     * shipper for this shipment. If the shipper is requesting a special
     * service which requires additional data (e.g. COD), the special service
     * type must be present in the specialServiceTypes collection, and the
     * supporting detail must be provided in the appropriate sub-object.
     * For example, to request COD, "COD" must be included in the SpecialServiceTypes
     * collection and the CodDetail object must contain the required data.
     */
    public PackageSpecialServicesRequested getSpecialServicesRequested() {
        return specialServicesRequested;
    }


    /**
     * Sets the specialServicesRequested value for this RateRequestPackageSummary.
     * 
     * @param specialServicesRequested   * Descriptive data regarding special services requested by the
     * shipper for this shipment. If the shipper is requesting a special
     * service which requires additional data (e.g. COD), the special service
     * type must be present in the specialServiceTypes collection, and the
     * supporting detail must be provided in the appropriate sub-object.
     * For example, to request COD, "COD" must be included in the SpecialServiceTypes
     * collection and the CodDetail object must contain the required data.
     */
    public void setSpecialServicesRequested(PackageSpecialServicesRequested specialServicesRequested) {
        this.specialServicesRequested = specialServicesRequested;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RateRequestPackageSummary)) return false;
        RateRequestPackageSummary other = (RateRequestPackageSummary) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pieceCount==null && other.getPieceCount()==null) || 
             (this.pieceCount!=null &&
              this.pieceCount.equals(other.getPieceCount()))) &&
            ((this.totalWeight==null && other.getTotalWeight()==null) || 
             (this.totalWeight!=null &&
              this.totalWeight.equals(other.getTotalWeight()))) &&
            ((this.perPieceDimensions==null && other.getPerPieceDimensions()==null) || 
             (this.perPieceDimensions!=null &&
              this.perPieceDimensions.equals(other.getPerPieceDimensions()))) &&
            ((this.totalInsuredValue==null && other.getTotalInsuredValue()==null) || 
             (this.totalInsuredValue!=null &&
              this.totalInsuredValue.equals(other.getTotalInsuredValue()))) &&
            ((this.variableHandlingChargeDetail==null && other.getVariableHandlingChargeDetail()==null) || 
             (this.variableHandlingChargeDetail!=null &&
              this.variableHandlingChargeDetail.equals(other.getVariableHandlingChargeDetail()))) &&
            ((this.specialServicesRequested==null && other.getSpecialServicesRequested()==null) || 
             (this.specialServicesRequested!=null &&
              this.specialServicesRequested.equals(other.getSpecialServicesRequested())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPieceCount() != null) {
            _hashCode += getPieceCount().hashCode();
        }
        if (getTotalWeight() != null) {
            _hashCode += getTotalWeight().hashCode();
        }
        if (getPerPieceDimensions() != null) {
            _hashCode += getPerPieceDimensions().hashCode();
        }
        if (getTotalInsuredValue() != null) {
            _hashCode += getTotalInsuredValue().hashCode();
        }
        if (getVariableHandlingChargeDetail() != null) {
            _hashCode += getVariableHandlingChargeDetail().hashCode();
        }
        if (getSpecialServicesRequested() != null) {
            _hashCode += getSpecialServicesRequested().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RateRequestPackageSummary.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "RateRequestPackageSummary"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pieceCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "PieceCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "positiveInteger"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalWeight");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "TotalWeight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Weight"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("perPieceDimensions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "PerPieceDimensions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Dimensions"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalInsuredValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "TotalInsuredValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Money"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("variableHandlingChargeDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "VariableHandlingChargeDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "VariableHandlingChargeDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("specialServicesRequested");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "SpecialServicesRequested"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "PackageSpecialServicesRequested"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
