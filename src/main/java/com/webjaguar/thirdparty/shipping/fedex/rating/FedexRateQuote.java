/* Copyright 2006 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 08.08.2008
 */

package com.webjaguar.thirdparty.shipping.fedex.rating;

import com.webjaguar.model.ShippingRate;
import com.webjaguar.thirdparty.shipping.fedex.rating.ClientDetail;
import com.webjaguar.thirdparty.shipping.fedex.rating.WebAuthenticationCredential;
import com.webjaguar.thirdparty.shipping.fedex.rating.WebAuthenticationDetail;

import java.util.*;
import java.math.BigDecimal;

import java.util.Date;

import org.apache.axis.types.NonNegativeInteger;

import org.apache.axis.types.PositiveInteger;


public class FedexRateQuote {
	
	public List<ShippingRate> getRates(Address origAddress, Address destAddress, String weight, String key, String password, String meterNumber, String accountNumber,
			String signatureType, String packagingType, String DropOffType, String length, String width, String height, String unit, Boolean saturdayDelivery) {
		
        // list to be returned
		List<ShippingRate> ratesList = new ArrayList<ShippingRate>();
		
		// Build a RateRequest request object
	    RateAvailableServicesRequest rateAvailableServiceRequest = new RateAvailableServicesRequest();
	    ClientDetail clientDetail = createClientDetail(meterNumber, accountNumber);
        clientDetail.setClientProductId("HYBF");
        clientDetail.setClientProductVersion("3787");
	    rateAvailableServiceRequest.setClientDetail(clientDetail);
        rateAvailableServiceRequest.setWebAuthenticationDetail(createWebAuthenticationDetail(key, password));

	    TransactionDetail transactionDetail = new TransactionDetail();
	    transactionDetail.setCustomerTransactionId("webjaguar-08"); // The client will get the same value back in the response
	    rateAvailableServiceRequest.setTransactionDetail( transactionDetail );

        VersionId versionId = new VersionId("crs", 3, 0, 0);
        rateAvailableServiceRequest.setVersion( versionId );
	    //

	    rateAvailableServiceRequest.setOrigin( origAddress );
	    rateAvailableServiceRequest.setDestination( destAddress );
	    //
	    
	    // if customers want to use their own pricing use "Account" not "List"
	    //rateAvailableServiceRequest.setRateRequestTypes(rateRequestTypes);

	    Payment payment = new Payment(); // payment information
	    payment.setPaymentType(PaymentType.SENDER); // Payment options are RECIPIENT, SENDER, THIRD_PARTY
	    //
	    
	    rateAvailableServiceRequest.setDropoffType(getDropOffOptionType(DropOffType));
	    rateAvailableServiceRequest.setPackagingType(getPackagingOptionType(packagingType));
	    //rateAvailableServiceRequest.setServiceType( ServiceType.PRIORITY_OVERNIGHT);
	    
	    rateAvailableServiceRequest.setShipDate( new Date() );
	    Calendar rightNow = Calendar.getInstance();
		if ( ( saturdayDelivery != null && saturdayDelivery ) && rightNow.get( Calendar.DAY_OF_WEEK ) == Calendar.FRIDAY ) {
			ShipmentSpecialServicesRequested s = new ShipmentSpecialServicesRequested();
		    ShipmentSpecialServiceType[] specialServiceType = new ShipmentSpecialServiceType[1];
		    specialServiceType[0] = ShipmentSpecialServiceType.SATURDAY_DELIVERY;
		    s.setSpecialServiceTypes( specialServiceType );
		    rateAvailableServiceRequest.setSpecialServicesRequested( s );
		}  
	    
	    
        // The RateRequest.Items is a choice of one of the following:
        // Array of RateRequestPackageSummary - Details of multi piece shipment rate request - Use this to rate a total piece total weight shipment.
        // Array of RateRequestPackageDetail - Details of simgle piece shipment rate request - Currently only 1 occurance is supported.
        // Not passing one of these choices will result in "Schema Validation Failure", as one of these objects is required.

	    if(true) {
		    RateRequestPackageSummary packageSummary = new RateRequestPackageSummary();
		    packageSummary.setTotalWeight(new Weight(WeightUnits.LB, new BigDecimal(weight)));
		    packageSummary.setTotalInsuredValue(new Money("USD", new BigDecimal("0.00")));
		    PackageSpecialServicesRequested packageSpecialServicesRequested = new PackageSpecialServicesRequested();
		    SignatureOptionDetail SignatureOptionDetail = new SignatureOptionDetail();
		    SignatureOptionDetail.setOptionType( getSignatureOptionType(signatureType) );
		    packageSpecialServicesRequested.setSignatureOptionDetail( SignatureOptionDetail );
		    packageSummary.setSpecialServicesRequested( packageSpecialServicesRequested );
		    //
		    packageSummary.setPieceCount(new PositiveInteger("1",10));
		    if (!length.equals( "" ) && !width.equals( "" ) && !height.equals( "" )) {
		    	packageSummary.setPerPieceDimensions( getDimensions(length, width, height, unit));
		    }
		    //
		    PackageSpecialServicesRequested pssr = new PackageSpecialServicesRequested();
		    packageSummary.setSpecialServicesRequested(pssr);
		    rateAvailableServiceRequest.setRateRequestPackageSummary( packageSummary );
	    }
        else {
		    RequestedPackage rp = new RequestedPackage();

		    rp.setWeight(new Weight(WeightUnits.LB, new BigDecimal(weight)));
		    rp.setInsuredValue(new Money("USD", new BigDecimal("1.00")));
		    rp.setDimensions(new Dimensions(new NonNegativeInteger("1"), new NonNegativeInteger("1"), new NonNegativeInteger("1"), LinearUnits.IN));

		    PackageSpecialServicesRequested pssr = new PackageSpecialServicesRequested();
		    rp.setSpecialServicesRequested(pssr);
	    }
		try {
			// Initialize the service
			//XStream xstream = new XStream(new DomDriver());
			RateServiceLocator service;
			RatePortType port;
			service = new RateServiceLocator();

			updateEndPoint(service);
			port = service.getRateServicePort();

			// This is the call to the web service passing in a RateAvailableServices and returning a RateReply
			RateAvailableServicesReply reply = port.rateAvailableServices( rateAvailableServiceRequest );// Service call
			//System.out.println(xstream.toXML(reply));
			// check if the call was successful
			if (reply.getHighestSeverity().toString().equals(NotificationSeverityType._SUCCESS) ||
					reply.getHighestSeverity().toString().equals(NotificationSeverityType._NOTE) ||
					reply.getHighestSeverity().toString().equals(NotificationSeverityType._WARNING))  {

				RateAndServiceOptions[] rsd = reply.getOptions();
				for (int i = 0; i < rsd.length; i++) {
					ShippingRate shippingRate = new ShippingRate();
					shippingRate.setCode( rsd[i].getServiceDetail().getServiceType().getValue() );
					shippingRate.setCarrier("fedex");

					RatedShipmentDetail[] rpd = rsd[i].getRatedShipmentDetails();
					for(int j = 0; j < rpd.length; j++) {
						shippingRate.setPrice( rpd[j].getShipmentRateDetail().getTotalNetCharge().getAmount().toString() );
					}
					ratesList.add(shippingRate);
				}
			}	
			else if (reply.getHighestSeverity().toString().equals(NotificationSeverityType._ERROR) || 
					reply.getHighestSeverity().toString().equals(NotificationSeverityType._NOTE) ||
					reply.getHighestSeverity().toString().equals(NotificationSeverityType._WARNING)){
				printNotifications(reply.getNotifications());
			}

		} catch (Exception e) {
			// do nothing
		} 
        return ratesList;
        
    }
	private static ClientDetail createClientDetail(String meterNumber, String accountNumber) {

        ClientDetail clientDetail = new ClientDetail();
        // String accountNumber = System.getProperty("accountNumber");
        //String meterNumber = System.getProperty("meterNumber");
        // See if the accountNumber and meterNumber properties are set,
        // if set use those values, otherwise default them to "XXX"
        if (accountNumber == null || accountNumber == "") {
        	accountNumber = "248976535"; // Replace "XXX" with clients account number
        }
        clientDetail.setAccountNumber(accountNumber);
        clientDetail.setMeterNumber(meterNumber);
        return clientDetail;
	}

	private static WebAuthenticationDetail createWebAuthenticationDetail(String key, String password) {
        WebAuthenticationCredential wac = new WebAuthenticationCredential();
        // See if the key and password properties are set,
        // if set use those values, otherwise default them to "XXX"

        wac.setKey(key);
        wac.setPassword(password);
		
        WebAuthenticationCredential csp = new WebAuthenticationCredential();
        String cspKey = System.getProperty("cspKey");
        String cspPassword = System.getProperty("cspPassword");

        // See if the key and password properties are set,
        // if set use those values, otherwise default them to "XXX"
        if (cspKey == null) {
        	cspKey = "0IPR1Lx36Ng5ofW3"; // Replace "XXX" with clients key
        }
        if (cspPassword == null) {
        	cspPassword = "gXc253TrFxPXdjKxKSjfWT66D"; // Replace "XXX" with clients password
        }

        csp.setKey(cspKey);
        csp.setPassword(cspPassword);

		return new WebAuthenticationDetail(csp, wac);
	}

	private static void printNotifications(Notification[] notifications) {
		if (notifications == null || notifications.length == 0) {
			//System.out.println("  No notifications returned");
		}

		for (int i=0; i < notifications.length; i++){
			Notification n = notifications[i];
			System.out.print("  Notification no. " + i + ": ");
			if (n == null) {
				//System.out.println("null");
				continue;
			} else {
				//System.out.println("");
			}
			NotificationSeverityType nst = n.getSeverity();

			//System.out.println("    Severity: " + (nst == null ? "null" : nst.getValue()));
			//System.out.println("    Code: " + n.getCode());
			//System.out.println("    Message: " + n.getMessage());
			//System.out.println("    Source: " + n.getSource());
		}
	}

	private static void updateEndPoint(RateServiceLocator serviceLocator) {
		String endPoint = System.getProperty("endPoint");
		if (endPoint != null) {
			serviceLocator.setRateServicePortEndpointAddress(endPoint);
		}
	}
	
	private SignatureOptionType getSignatureOptionType(String signatureType) {
		if ( signatureType.equals( "DIRECT" ))
			return SignatureOptionType.DIRECT;
		else if ( signatureType.equals( "INDIRECT" ))
			return SignatureOptionType.INDIRECT;
		else if ( signatureType.equals( "ADULT" ))
			return SignatureOptionType.ADULT;
		else
			return SignatureOptionType.NO_SIGNATURE_REQUIRED;
	}
	
	private PackagingType getPackagingOptionType(String packagingType) {
		if ( packagingType.equals( "FEDEX_BOX" ))
			return PackagingType.FEDEX_BOX;
		else if ( packagingType.equals( "FEDEX_ENVELOPE" ))
			return PackagingType.FEDEX_ENVELOPE;
		else if ( packagingType.equals( "FEDEX_PAK" ))
			return PackagingType.FEDEX_PAK;
		else if ( packagingType.equals( "FEDEX_TUBE" ))
			return PackagingType.FEDEX_TUBE;
		else
			return PackagingType.YOUR_PACKAGING;
	}
	
	private DropoffType getDropOffOptionType(String dropOffType) {
		if ( dropOffType.equals( "BUSINESS_SERVICE_CENTER" ))
			return DropoffType.BUSINESS_SERVICE_CENTER;
		else if ( dropOffType.equals( "DROP_BOX" ))
			return DropoffType.DROP_BOX;
		else if ( dropOffType.equals( "REQUEST_COURIER" ))
			return DropoffType.REQUEST_COURIER;
		else if ( dropOffType.equals( "STATION" ))
			return DropoffType.STATION;
		else
			return DropoffType.REGULAR_PICKUP;
	}
	
	private Dimensions getDimensions(String length, String width, String height, String unit) {
		Dimensions d = new Dimensions(new NonNegativeInteger(length), new NonNegativeInteger(width), new NonNegativeInteger(height), LinearUnits.IN);
		if ( unit.equals( "cm" ) ){
			d.setUnits( LinearUnits.CM );
		}
		return d;
	}

}
