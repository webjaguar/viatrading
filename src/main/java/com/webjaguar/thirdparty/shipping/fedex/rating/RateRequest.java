/**
 * RateRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.rating;


/**
 * Descriptive data sent to FedEx by a customer in order to rate a
 * package/shipment.
 */
public class RateRequest  implements java.io.Serializable {
    /* Descriptive data to be used in authentication of the sender's
     * identity (and right to use FedEx web services). */
    private WebAuthenticationDetail webAuthenticationDetail;

    /* Descriptive data identifying the client submitting the transaction. */
    private ClientDetail clientDetail;

    /* Descriptive data for this customer transaction. The TransactionDetail
     * from the request is echoed back to the caller in the corresponding
     * reply. */
    private TransactionDetail transactionDetail;

    /* Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply). */
    private VersionId version;

    /* The descriptive data for the physical location from which the
     * shipment originates. */
    private Address origin;

    /* The descriptive data for the physical location from which the
     * shipment is being sent. */
    private Address destination;

    /* Used to calculate Argentina and Dominican Republic export taxes. */
    private Money customsValue;

    /* Used to calculate taxes for Jamaica and United Arab Emirates. */
    private InternationalDocumentContentType documentContent;

    /* Identifies the preferred currency of the requestor. */
    private java.lang.String currencyType;

    /* Descriptive data indicating the method and means of payment
     * to FedEx for providing shipping services. */
    private Payment payment;

    /* Details about how to calculate variable handling charges at
     * the shipment level. */
    private VariableHandlingChargeDetail variableHandlingChargeDetail;

    /* Identifies the method by which the package is to be tendered
     * to FedEx. This element does not dispatch a courier for package pickup. */
    private DropoffType dropoffType;

    /* Identifies the FedEx service to use in shipping the package.
     * See ServiceType for list of valid enumerated values. */
    private ServiceType serviceType;

    /* Identifies the packaging used by the requestor for the package.
     * See PackagingType for list of valid enumerated values. */
    private PackagingType packagingType;

    /* Identifies the date the package is tendered to FedEx. The date
     * component must be in the format: YYYY-MM-DD (e.g. 2006-06-26). */
    private java.util.Date shipDate;

    /* Identifies the collection of special services offered by FedEx. */
    private ShipmentSpecialServicesRequested specialServicesRequested;

    /* Indicates the type of rates to be returned. */
    private RateRequestType[] rateRequestTypes;

    /* Details about a multiple piece shipment rate request. Use this
     * to rate a total piece total weight shipemnt. */
    private RateRequestPackageSummary rateRequestPackageSummary;

    /* The total number of packages in the entire shipment. */
    private org.apache.axis.types.NonNegativeInteger packageCount;

    /* Details about a single piece shipment rate request. Currently
     * only 1 occurance is supported. */
    private RequestedPackage[] packages;

    public RateRequest() {
    }

    public RateRequest(
           WebAuthenticationDetail webAuthenticationDetail,
           ClientDetail clientDetail,
           TransactionDetail transactionDetail,
           VersionId version,
           Address origin,
           Address destination,
           Money customsValue,
           InternationalDocumentContentType documentContent,
           java.lang.String currencyType,
           Payment payment,
           VariableHandlingChargeDetail variableHandlingChargeDetail,
           DropoffType dropoffType,
           ServiceType serviceType,
           PackagingType packagingType,
           java.util.Date shipDate,
           ShipmentSpecialServicesRequested specialServicesRequested,
           RateRequestType[] rateRequestTypes,
           RateRequestPackageSummary rateRequestPackageSummary,
           org.apache.axis.types.NonNegativeInteger packageCount,
           RequestedPackage[] packages) {
           this.webAuthenticationDetail = webAuthenticationDetail;
           this.clientDetail = clientDetail;
           this.transactionDetail = transactionDetail;
           this.version = version;
           this.origin = origin;
           this.destination = destination;
           this.customsValue = customsValue;
           this.documentContent = documentContent;
           this.currencyType = currencyType;
           this.payment = payment;
           this.variableHandlingChargeDetail = variableHandlingChargeDetail;
           this.dropoffType = dropoffType;
           this.serviceType = serviceType;
           this.packagingType = packagingType;
           this.shipDate = shipDate;
           this.specialServicesRequested = specialServicesRequested;
           this.rateRequestTypes = rateRequestTypes;
           this.rateRequestPackageSummary = rateRequestPackageSummary;
           this.packageCount = packageCount;
           this.packages = packages;
    }


    /**
     * Gets the webAuthenticationDetail value for this RateRequest.
     * 
     * @return webAuthenticationDetail   * Descriptive data to be used in authentication of the sender's
     * identity (and right to use FedEx web services).
     */
    public WebAuthenticationDetail getWebAuthenticationDetail() {
        return webAuthenticationDetail;
    }


    /**
     * Sets the webAuthenticationDetail value for this RateRequest.
     * 
     * @param webAuthenticationDetail   * Descriptive data to be used in authentication of the sender's
     * identity (and right to use FedEx web services).
     */
    public void setWebAuthenticationDetail(WebAuthenticationDetail webAuthenticationDetail) {
        this.webAuthenticationDetail = webAuthenticationDetail;
    }


    /**
     * Gets the clientDetail value for this RateRequest.
     * 
     * @return clientDetail   * Descriptive data identifying the client submitting the transaction.
     */
    public ClientDetail getClientDetail() {
        return clientDetail;
    }


    /**
     * Sets the clientDetail value for this RateRequest.
     * 
     * @param clientDetail   * Descriptive data identifying the client submitting the transaction.
     */
    public void setClientDetail(ClientDetail clientDetail) {
        this.clientDetail = clientDetail;
    }


    /**
     * Gets the transactionDetail value for this RateRequest.
     * 
     * @return transactionDetail   * Descriptive data for this customer transaction. The TransactionDetail
     * from the request is echoed back to the caller in the corresponding
     * reply.
     */
    public TransactionDetail getTransactionDetail() {
        return transactionDetail;
    }


    /**
     * Sets the transactionDetail value for this RateRequest.
     * 
     * @param transactionDetail   * Descriptive data for this customer transaction. The TransactionDetail
     * from the request is echoed back to the caller in the corresponding
     * reply.
     */
    public void setTransactionDetail(TransactionDetail transactionDetail) {
        this.transactionDetail = transactionDetail;
    }


    /**
     * Gets the version value for this RateRequest.
     * 
     * @return version   * Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply).
     */
    public VersionId getVersion() {
        return version;
    }


    /**
     * Sets the version value for this RateRequest.
     * 
     * @param version   * Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply).
     */
    public void setVersion(VersionId version) {
        this.version = version;
    }


    /**
     * Gets the origin value for this RateRequest.
     * 
     * @return origin   * The descriptive data for the physical location from which the
     * shipment originates.
     */
    public Address getOrigin() {
        return origin;
    }


    /**
     * Sets the origin value for this RateRequest.
     * 
     * @param origin   * The descriptive data for the physical location from which the
     * shipment originates.
     */
    public void setOrigin(Address origin) {
        this.origin = origin;
    }


    /**
     * Gets the destination value for this RateRequest.
     * 
     * @return destination   * The descriptive data for the physical location from which the
     * shipment is being sent.
     */
    public Address getDestination() {
        return destination;
    }


    /**
     * Sets the destination value for this RateRequest.
     * 
     * @param destination   * The descriptive data for the physical location from which the
     * shipment is being sent.
     */
    public void setDestination(Address destination) {
        this.destination = destination;
    }


    /**
     * Gets the customsValue value for this RateRequest.
     * 
     * @return customsValue   * Used to calculate Argentina and Dominican Republic export taxes.
     */
    public Money getCustomsValue() {
        return customsValue;
    }


    /**
     * Sets the customsValue value for this RateRequest.
     * 
     * @param customsValue   * Used to calculate Argentina and Dominican Republic export taxes.
     */
    public void setCustomsValue(Money customsValue) {
        this.customsValue = customsValue;
    }


    /**
     * Gets the documentContent value for this RateRequest.
     * 
     * @return documentContent   * Used to calculate taxes for Jamaica and United Arab Emirates.
     */
    public InternationalDocumentContentType getDocumentContent() {
        return documentContent;
    }


    /**
     * Sets the documentContent value for this RateRequest.
     * 
     * @param documentContent   * Used to calculate taxes for Jamaica and United Arab Emirates.
     */
    public void setDocumentContent(InternationalDocumentContentType documentContent) {
        this.documentContent = documentContent;
    }


    /**
     * Gets the currencyType value for this RateRequest.
     * 
     * @return currencyType   * Identifies the preferred currency of the requestor.
     */
    public java.lang.String getCurrencyType() {
        return currencyType;
    }


    /**
     * Sets the currencyType value for this RateRequest.
     * 
     * @param currencyType   * Identifies the preferred currency of the requestor.
     */
    public void setCurrencyType(java.lang.String currencyType) {
        this.currencyType = currencyType;
    }


    /**
     * Gets the payment value for this RateRequest.
     * 
     * @return payment   * Descriptive data indicating the method and means of payment
     * to FedEx for providing shipping services.
     */
    public Payment getPayment() {
        return payment;
    }


    /**
     * Sets the payment value for this RateRequest.
     * 
     * @param payment   * Descriptive data indicating the method and means of payment
     * to FedEx for providing shipping services.
     */
    public void setPayment(Payment payment) {
        this.payment = payment;
    }


    /**
     * Gets the variableHandlingChargeDetail value for this RateRequest.
     * 
     * @return variableHandlingChargeDetail   * Details about how to calculate variable handling charges at
     * the shipment level.
     */
    public VariableHandlingChargeDetail getVariableHandlingChargeDetail() {
        return variableHandlingChargeDetail;
    }


    /**
     * Sets the variableHandlingChargeDetail value for this RateRequest.
     * 
     * @param variableHandlingChargeDetail   * Details about how to calculate variable handling charges at
     * the shipment level.
     */
    public void setVariableHandlingChargeDetail(VariableHandlingChargeDetail variableHandlingChargeDetail) {
        this.variableHandlingChargeDetail = variableHandlingChargeDetail;
    }


    /**
     * Gets the dropoffType value for this RateRequest.
     * 
     * @return dropoffType   * Identifies the method by which the package is to be tendered
     * to FedEx. This element does not dispatch a courier for package pickup.
     */
    public DropoffType getDropoffType() {
        return dropoffType;
    }


    /**
     * Sets the dropoffType value for this RateRequest.
     * 
     * @param dropoffType   * Identifies the method by which the package is to be tendered
     * to FedEx. This element does not dispatch a courier for package pickup.
     */
    public void setDropoffType(DropoffType dropoffType) {
        this.dropoffType = dropoffType;
    }


    /**
     * Gets the serviceType value for this RateRequest.
     * 
     * @return serviceType   * Identifies the FedEx service to use in shipping the package.
     * See ServiceType for list of valid enumerated values.
     */
    public ServiceType getServiceType() {
        return serviceType;
    }


    /**
     * Sets the serviceType value for this RateRequest.
     * 
     * @param serviceType   * Identifies the FedEx service to use in shipping the package.
     * See ServiceType for list of valid enumerated values.
     */
    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }


    /**
     * Gets the packagingType value for this RateRequest.
     * 
     * @return packagingType   * Identifies the packaging used by the requestor for the package.
     * See PackagingType for list of valid enumerated values.
     */
    public PackagingType getPackagingType() {
        return packagingType;
    }


    /**
     * Sets the packagingType value for this RateRequest.
     * 
     * @param packagingType   * Identifies the packaging used by the requestor for the package.
     * See PackagingType for list of valid enumerated values.
     */
    public void setPackagingType(PackagingType packagingType) {
        this.packagingType = packagingType;
    }


    /**
     * Gets the shipDate value for this RateRequest.
     * 
     * @return shipDate   * Identifies the date the package is tendered to FedEx. The date
     * component must be in the format: YYYY-MM-DD (e.g. 2006-06-26).
     */
    public java.util.Date getShipDate() {
        return shipDate;
    }


    /**
     * Sets the shipDate value for this RateRequest.
     * 
     * @param shipDate   * Identifies the date the package is tendered to FedEx. The date
     * component must be in the format: YYYY-MM-DD (e.g. 2006-06-26).
     */
    public void setShipDate(java.util.Date shipDate) {
        this.shipDate = shipDate;
    }


    /**
     * Gets the specialServicesRequested value for this RateRequest.
     * 
     * @return specialServicesRequested   * Identifies the collection of special services offered by FedEx.
     */
    public ShipmentSpecialServicesRequested getSpecialServicesRequested() {
        return specialServicesRequested;
    }


    /**
     * Sets the specialServicesRequested value for this RateRequest.
     * 
     * @param specialServicesRequested   * Identifies the collection of special services offered by FedEx.
     */
    public void setSpecialServicesRequested(ShipmentSpecialServicesRequested specialServicesRequested) {
        this.specialServicesRequested = specialServicesRequested;
    }


    /**
     * Gets the rateRequestTypes value for this RateRequest.
     * 
     * @return rateRequestTypes   * Indicates the type of rates to be returned.
     */
    public RateRequestType[] getRateRequestTypes() {
        return rateRequestTypes;
    }


    /**
     * Sets the rateRequestTypes value for this RateRequest.
     * 
     * @param rateRequestTypes   * Indicates the type of rates to be returned.
     */
    public void setRateRequestTypes(RateRequestType[] rateRequestTypes) {
        this.rateRequestTypes = rateRequestTypes;
    }

    public RateRequestType getRateRequestTypes(int i) {
        return this.rateRequestTypes[i];
    }

    public void setRateRequestTypes(int i, RateRequestType _value) {
        this.rateRequestTypes[i] = _value;
    }


    /**
     * Gets the rateRequestPackageSummary value for this RateRequest.
     * 
     * @return rateRequestPackageSummary   * Details about a multiple piece shipment rate request. Use this
     * to rate a total piece total weight shipemnt.
     */
    public RateRequestPackageSummary getRateRequestPackageSummary() {
        return rateRequestPackageSummary;
    }


    /**
     * Sets the rateRequestPackageSummary value for this RateRequest.
     * 
     * @param rateRequestPackageSummary   * Details about a multiple piece shipment rate request. Use this
     * to rate a total piece total weight shipemnt.
     */
    public void setRateRequestPackageSummary(RateRequestPackageSummary rateRequestPackageSummary) {
        this.rateRequestPackageSummary = rateRequestPackageSummary;
    }


    /**
     * Gets the packageCount value for this RateRequest.
     * 
     * @return packageCount   * The total number of packages in the entire shipment.
     */
    public org.apache.axis.types.NonNegativeInteger getPackageCount() {
        return packageCount;
    }


    /**
     * Sets the packageCount value for this RateRequest.
     * 
     * @param packageCount   * The total number of packages in the entire shipment.
     */
    public void setPackageCount(org.apache.axis.types.NonNegativeInteger packageCount) {
        this.packageCount = packageCount;
    }


    /**
     * Gets the packages value for this RateRequest.
     * 
     * @return packages   * Details about a single piece shipment rate request. Currently
     * only 1 occurance is supported.
     */
    public RequestedPackage[] getPackages() {
        return packages;
    }


    /**
     * Sets the packages value for this RateRequest.
     * 
     * @param packages   * Details about a single piece shipment rate request. Currently
     * only 1 occurance is supported.
     */
    public void setPackages(RequestedPackage[] packages) {
        this.packages = packages;
    }

    public RequestedPackage getPackages(int i) {
        return this.packages[i];
    }

    public void setPackages(int i, RequestedPackage _value) {
        this.packages[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RateRequest)) return false;
        RateRequest other = (RateRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.webAuthenticationDetail==null && other.getWebAuthenticationDetail()==null) || 
             (this.webAuthenticationDetail!=null &&
              this.webAuthenticationDetail.equals(other.getWebAuthenticationDetail()))) &&
            ((this.clientDetail==null && other.getClientDetail()==null) || 
             (this.clientDetail!=null &&
              this.clientDetail.equals(other.getClientDetail()))) &&
            ((this.transactionDetail==null && other.getTransactionDetail()==null) || 
             (this.transactionDetail!=null &&
              this.transactionDetail.equals(other.getTransactionDetail()))) &&
            ((this.version==null && other.getVersion()==null) || 
             (this.version!=null &&
              this.version.equals(other.getVersion()))) &&
            ((this.origin==null && other.getOrigin()==null) || 
             (this.origin!=null &&
              this.origin.equals(other.getOrigin()))) &&
            ((this.destination==null && other.getDestination()==null) || 
             (this.destination!=null &&
              this.destination.equals(other.getDestination()))) &&
            ((this.customsValue==null && other.getCustomsValue()==null) || 
             (this.customsValue!=null &&
              this.customsValue.equals(other.getCustomsValue()))) &&
            ((this.documentContent==null && other.getDocumentContent()==null) || 
             (this.documentContent!=null &&
              this.documentContent.equals(other.getDocumentContent()))) &&
            ((this.currencyType==null && other.getCurrencyType()==null) || 
             (this.currencyType!=null &&
              this.currencyType.equals(other.getCurrencyType()))) &&
            ((this.payment==null && other.getPayment()==null) || 
             (this.payment!=null &&
              this.payment.equals(other.getPayment()))) &&
            ((this.variableHandlingChargeDetail==null && other.getVariableHandlingChargeDetail()==null) || 
             (this.variableHandlingChargeDetail!=null &&
              this.variableHandlingChargeDetail.equals(other.getVariableHandlingChargeDetail()))) &&
            ((this.dropoffType==null && other.getDropoffType()==null) || 
             (this.dropoffType!=null &&
              this.dropoffType.equals(other.getDropoffType()))) &&
            ((this.serviceType==null && other.getServiceType()==null) || 
             (this.serviceType!=null &&
              this.serviceType.equals(other.getServiceType()))) &&
            ((this.packagingType==null && other.getPackagingType()==null) || 
             (this.packagingType!=null &&
              this.packagingType.equals(other.getPackagingType()))) &&
            ((this.shipDate==null && other.getShipDate()==null) || 
             (this.shipDate!=null &&
              this.shipDate.equals(other.getShipDate()))) &&
            ((this.specialServicesRequested==null && other.getSpecialServicesRequested()==null) || 
             (this.specialServicesRequested!=null &&
              this.specialServicesRequested.equals(other.getSpecialServicesRequested()))) &&
            ((this.rateRequestTypes==null && other.getRateRequestTypes()==null) || 
             (this.rateRequestTypes!=null &&
              java.util.Arrays.equals(this.rateRequestTypes, other.getRateRequestTypes()))) &&
            ((this.rateRequestPackageSummary==null && other.getRateRequestPackageSummary()==null) || 
             (this.rateRequestPackageSummary!=null &&
              this.rateRequestPackageSummary.equals(other.getRateRequestPackageSummary()))) &&
            ((this.packageCount==null && other.getPackageCount()==null) || 
             (this.packageCount!=null &&
              this.packageCount.equals(other.getPackageCount()))) &&
            ((this.packages==null && other.getPackages()==null) || 
             (this.packages!=null &&
              java.util.Arrays.equals(this.packages, other.getPackages())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getWebAuthenticationDetail() != null) {
            _hashCode += getWebAuthenticationDetail().hashCode();
        }
        if (getClientDetail() != null) {
            _hashCode += getClientDetail().hashCode();
        }
        if (getTransactionDetail() != null) {
            _hashCode += getTransactionDetail().hashCode();
        }
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        if (getOrigin() != null) {
            _hashCode += getOrigin().hashCode();
        }
        if (getDestination() != null) {
            _hashCode += getDestination().hashCode();
        }
        if (getCustomsValue() != null) {
            _hashCode += getCustomsValue().hashCode();
        }
        if (getDocumentContent() != null) {
            _hashCode += getDocumentContent().hashCode();
        }
        if (getCurrencyType() != null) {
            _hashCode += getCurrencyType().hashCode();
        }
        if (getPayment() != null) {
            _hashCode += getPayment().hashCode();
        }
        if (getVariableHandlingChargeDetail() != null) {
            _hashCode += getVariableHandlingChargeDetail().hashCode();
        }
        if (getDropoffType() != null) {
            _hashCode += getDropoffType().hashCode();
        }
        if (getServiceType() != null) {
            _hashCode += getServiceType().hashCode();
        }
        if (getPackagingType() != null) {
            _hashCode += getPackagingType().hashCode();
        }
        if (getShipDate() != null) {
            _hashCode += getShipDate().hashCode();
        }
        if (getSpecialServicesRequested() != null) {
            _hashCode += getSpecialServicesRequested().hashCode();
        }
        if (getRateRequestTypes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRateRequestTypes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRateRequestTypes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getRateRequestPackageSummary() != null) {
            _hashCode += getRateRequestPackageSummary().hashCode();
        }
        if (getPackageCount() != null) {
            _hashCode += getPackageCount().hashCode();
        }
        if (getPackages() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPackages());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPackages(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RateRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "RateRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("webAuthenticationDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "WebAuthenticationDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "WebAuthenticationDetail"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ClientDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ClientDetail"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "TransactionDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "TransactionDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "VersionId"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("origin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Origin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Address"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("destination");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Destination"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Address"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customsValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "CustomsValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Money"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("documentContent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "DocumentContent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "InternationalDocumentContentType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "CurrencyType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Payment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Payment"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("variableHandlingChargeDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "VariableHandlingChargeDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "VariableHandlingChargeDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dropoffType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "DropoffType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "DropoffType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ServiceType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ServiceType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packagingType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "PackagingType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "PackagingType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ShipDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("specialServicesRequested");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "SpecialServicesRequested"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ShipmentSpecialServicesRequested"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rateRequestTypes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "RateRequestTypes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "RateRequestType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rateRequestPackageSummary");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "RateRequestPackageSummary"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "RateRequestPackageSummary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packageCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "PackageCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "nonNegativeInteger"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packages");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Packages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "RequestedPackage"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
