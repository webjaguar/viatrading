/**
 * RequestedPackage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.rating;

public class RequestedPackage  implements java.io.Serializable {
    /* The package sequence number of this package in a multiple piece
     * shipment. */
    private java.lang.Integer sequenceNumber;

    /* Currently not supported. */
    private java.lang.Boolean deleted;

    /* Currently not supported. */
    private TrackingId trackingId;

    /* Details about how to calculate variable handling charges at
     * the package level. */
    private VariableHandlingChargeDetail variableHandlingChargeDetail;

    /* The amount of insurance requested for this package. This amount
     * has 2 explicit decimal positions and has a max length of 11 including
     * the decimal. */
    private Money insuredValue;

    /* The total wight of this package. This value has 1 explicit
     * decimal poistion and has a max length of 12 including the decimal. */
    private Weight weight;

    /* The dimensions of this package and the unit type used for the
     * measurements. */
    private Dimensions dimensions;

    /* The description that is associated with the tracking number
     * on the web site when printing email labels. */
    private java.lang.String emailLabelItemDescription;

    /* Reference information to be associated with this package. */
    private CustomerReference[] customerReferences;

    /* Currently not supported. */
    private PackageSpecialServicesRequested specialServicesRequested;

    public RequestedPackage() {
    }

    public RequestedPackage(
           java.lang.Integer sequenceNumber,
           java.lang.Boolean deleted,
           TrackingId trackingId,
           VariableHandlingChargeDetail variableHandlingChargeDetail,
           Money insuredValue,
           Weight weight,
           Dimensions dimensions,
           java.lang.String emailLabelItemDescription,
           CustomerReference[] customerReferences,
           PackageSpecialServicesRequested specialServicesRequested) {
           this.sequenceNumber = sequenceNumber;
           this.deleted = deleted;
           this.trackingId = trackingId;
           this.variableHandlingChargeDetail = variableHandlingChargeDetail;
           this.insuredValue = insuredValue;
           this.weight = weight;
           this.dimensions = dimensions;
           this.emailLabelItemDescription = emailLabelItemDescription;
           this.customerReferences = customerReferences;
           this.specialServicesRequested = specialServicesRequested;
    }


    /**
     * Gets the sequenceNumber value for this RequestedPackage.
     * 
     * @return sequenceNumber   * The package sequence number of this package in a multiple piece
     * shipment.
     */
    public java.lang.Integer getSequenceNumber() {
        return sequenceNumber;
    }


    /**
     * Sets the sequenceNumber value for this RequestedPackage.
     * 
     * @param sequenceNumber   * The package sequence number of this package in a multiple piece
     * shipment.
     */
    public void setSequenceNumber(java.lang.Integer sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }


    /**
     * Gets the deleted value for this RequestedPackage.
     * 
     * @return deleted   * Currently not supported.
     */
    public java.lang.Boolean getDeleted() {
        return deleted;
    }


    /**
     * Sets the deleted value for this RequestedPackage.
     * 
     * @param deleted   * Currently not supported.
     */
    public void setDeleted(java.lang.Boolean deleted) {
        this.deleted = deleted;
    }


    /**
     * Gets the trackingId value for this RequestedPackage.
     * 
     * @return trackingId   * Currently not supported.
     */
    public TrackingId getTrackingId() {
        return trackingId;
    }


    /**
     * Sets the trackingId value for this RequestedPackage.
     * 
     * @param trackingId   * Currently not supported.
     */
    public void setTrackingId(TrackingId trackingId) {
        this.trackingId = trackingId;
    }


    /**
     * Gets the variableHandlingChargeDetail value for this RequestedPackage.
     * 
     * @return variableHandlingChargeDetail   * Details about how to calculate variable handling charges at
     * the package level.
     */
    public VariableHandlingChargeDetail getVariableHandlingChargeDetail() {
        return variableHandlingChargeDetail;
    }


    /**
     * Sets the variableHandlingChargeDetail value for this RequestedPackage.
     * 
     * @param variableHandlingChargeDetail   * Details about how to calculate variable handling charges at
     * the package level.
     */
    public void setVariableHandlingChargeDetail(VariableHandlingChargeDetail variableHandlingChargeDetail) {
        this.variableHandlingChargeDetail = variableHandlingChargeDetail;
    }


    /**
     * Gets the insuredValue value for this RequestedPackage.
     * 
     * @return insuredValue   * The amount of insurance requested for this package. This amount
     * has 2 explicit decimal positions and has a max length of 11 including
     * the decimal.
     */
    public Money getInsuredValue() {
        return insuredValue;
    }


    /**
     * Sets the insuredValue value for this RequestedPackage.
     * 
     * @param insuredValue   * The amount of insurance requested for this package. This amount
     * has 2 explicit decimal positions and has a max length of 11 including
     * the decimal.
     */
    public void setInsuredValue(Money insuredValue) {
        this.insuredValue = insuredValue;
    }


    /**
     * Gets the weight value for this RequestedPackage.
     * 
     * @return weight   * The total wight of this package. This value has 1 explicit
     * decimal poistion and has a max length of 12 including the decimal.
     */
    public Weight getWeight() {
        return weight;
    }


    /**
     * Sets the weight value for this RequestedPackage.
     * 
     * @param weight   * The total wight of this package. This value has 1 explicit
     * decimal poistion and has a max length of 12 including the decimal.
     */
    public void setWeight(Weight weight) {
        this.weight = weight;
    }


    /**
     * Gets the dimensions value for this RequestedPackage.
     * 
     * @return dimensions   * The dimensions of this package and the unit type used for the
     * measurements.
     */
    public Dimensions getDimensions() {
        return dimensions;
    }


    /**
     * Sets the dimensions value for this RequestedPackage.
     * 
     * @param dimensions   * The dimensions of this package and the unit type used for the
     * measurements.
     */
    public void setDimensions(Dimensions dimensions) {
        this.dimensions = dimensions;
    }


    /**
     * Gets the emailLabelItemDescription value for this RequestedPackage.
     * 
     * @return emailLabelItemDescription   * The description that is associated with the tracking number
     * on the web site when printing email labels.
     */
    public java.lang.String getEmailLabelItemDescription() {
        return emailLabelItemDescription;
    }


    /**
     * Sets the emailLabelItemDescription value for this RequestedPackage.
     * 
     * @param emailLabelItemDescription   * The description that is associated with the tracking number
     * on the web site when printing email labels.
     */
    public void setEmailLabelItemDescription(java.lang.String emailLabelItemDescription) {
        this.emailLabelItemDescription = emailLabelItemDescription;
    }


    /**
     * Gets the customerReferences value for this RequestedPackage.
     * 
     * @return customerReferences   * Reference information to be associated with this package.
     */
    public CustomerReference[] getCustomerReferences() {
        return customerReferences;
    }


    /**
     * Sets the customerReferences value for this RequestedPackage.
     * 
     * @param customerReferences   * Reference information to be associated with this package.
     */
    public void setCustomerReferences(CustomerReference[] customerReferences) {
        this.customerReferences = customerReferences;
    }

    public CustomerReference getCustomerReferences(int i) {
        return this.customerReferences[i];
    }

    public void setCustomerReferences(int i, CustomerReference _value) {
        this.customerReferences[i] = _value;
    }


    /**
     * Gets the specialServicesRequested value for this RequestedPackage.
     * 
     * @return specialServicesRequested   * Currently not supported.
     */
    public PackageSpecialServicesRequested getSpecialServicesRequested() {
        return specialServicesRequested;
    }


    /**
     * Sets the specialServicesRequested value for this RequestedPackage.
     * 
     * @param specialServicesRequested   * Currently not supported.
     */
    public void setSpecialServicesRequested(PackageSpecialServicesRequested specialServicesRequested) {
        this.specialServicesRequested = specialServicesRequested;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RequestedPackage)) return false;
        RequestedPackage other = (RequestedPackage) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sequenceNumber==null && other.getSequenceNumber()==null) || 
             (this.sequenceNumber!=null &&
              this.sequenceNumber.equals(other.getSequenceNumber()))) &&
            ((this.deleted==null && other.getDeleted()==null) || 
             (this.deleted!=null &&
              this.deleted.equals(other.getDeleted()))) &&
            ((this.trackingId==null && other.getTrackingId()==null) || 
             (this.trackingId!=null &&
              this.trackingId.equals(other.getTrackingId()))) &&
            ((this.variableHandlingChargeDetail==null && other.getVariableHandlingChargeDetail()==null) || 
             (this.variableHandlingChargeDetail!=null &&
              this.variableHandlingChargeDetail.equals(other.getVariableHandlingChargeDetail()))) &&
            ((this.insuredValue==null && other.getInsuredValue()==null) || 
             (this.insuredValue!=null &&
              this.insuredValue.equals(other.getInsuredValue()))) &&
            ((this.weight==null && other.getWeight()==null) || 
             (this.weight!=null &&
              this.weight.equals(other.getWeight()))) &&
            ((this.dimensions==null && other.getDimensions()==null) || 
             (this.dimensions!=null &&
              this.dimensions.equals(other.getDimensions()))) &&
            ((this.emailLabelItemDescription==null && other.getEmailLabelItemDescription()==null) || 
             (this.emailLabelItemDescription!=null &&
              this.emailLabelItemDescription.equals(other.getEmailLabelItemDescription()))) &&
            ((this.customerReferences==null && other.getCustomerReferences()==null) || 
             (this.customerReferences!=null &&
              java.util.Arrays.equals(this.customerReferences, other.getCustomerReferences()))) &&
            ((this.specialServicesRequested==null && other.getSpecialServicesRequested()==null) || 
             (this.specialServicesRequested!=null &&
              this.specialServicesRequested.equals(other.getSpecialServicesRequested())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSequenceNumber() != null) {
            _hashCode += getSequenceNumber().hashCode();
        }
        if (getDeleted() != null) {
            _hashCode += getDeleted().hashCode();
        }
        if (getTrackingId() != null) {
            _hashCode += getTrackingId().hashCode();
        }
        if (getVariableHandlingChargeDetail() != null) {
            _hashCode += getVariableHandlingChargeDetail().hashCode();
        }
        if (getInsuredValue() != null) {
            _hashCode += getInsuredValue().hashCode();
        }
        if (getWeight() != null) {
            _hashCode += getWeight().hashCode();
        }
        if (getDimensions() != null) {
            _hashCode += getDimensions().hashCode();
        }
        if (getEmailLabelItemDescription() != null) {
            _hashCode += getEmailLabelItemDescription().hashCode();
        }
        if (getCustomerReferences() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomerReferences());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomerReferences(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSpecialServicesRequested() != null) {
            _hashCode += getSpecialServicesRequested().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequestedPackage.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "RequestedPackage"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sequenceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "SequenceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deleted");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Deleted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("trackingId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "TrackingId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "TrackingId"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("variableHandlingChargeDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "VariableHandlingChargeDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "VariableHandlingChargeDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insuredValue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "InsuredValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Money"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("weight");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Weight"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Weight"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dimensions");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Dimensions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Dimensions"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailLabelItemDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "EmailLabelItemDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerReferences");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "CustomerReferences"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "CustomerReference"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("specialServicesRequested");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "SpecialServicesRequested"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "PackageSpecialServicesRequested"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
