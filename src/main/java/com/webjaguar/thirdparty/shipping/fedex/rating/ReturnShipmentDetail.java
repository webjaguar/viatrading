/**
 * ReturnShipmentDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.rating;


/**
 * Information relating to a return shipment.
 */
public class ReturnShipmentDetail  implements java.io.Serializable {
    /* The type of return shipment that is being requested. At present
     * the only type of retrun shipment that is supported is PRINT_RETURN_LABEL.
     * With this option you can print a return label to insert into the box
     * of an outbound shipment. This option can not be used to print an outbound
     * label. */
    private ReturnType returnType;

    /* Return Merchant Authorization */
    private Rma rma;

    /* Specific information about the delivery of the email and options
     * for the shipment. */
    private EMailLabelDetail emailLabelDetail;

    public ReturnShipmentDetail() {
    }

    public ReturnShipmentDetail(
           ReturnType returnType,
           Rma rma,
           EMailLabelDetail emailLabelDetail) {
           this.returnType = returnType;
           this.rma = rma;
           this.emailLabelDetail = emailLabelDetail;
    }


    /**
     * Gets the returnType value for this ReturnShipmentDetail.
     * 
     * @return returnType   * The type of return shipment that is being requested. At present
     * the only type of retrun shipment that is supported is PRINT_RETURN_LABEL.
     * With this option you can print a return label to insert into the box
     * of an outbound shipment. This option can not be used to print an outbound
     * label.
     */
    public ReturnType getReturnType() {
        return returnType;
    }


    /**
     * Sets the returnType value for this ReturnShipmentDetail.
     * 
     * @param returnType   * The type of return shipment that is being requested. At present
     * the only type of retrun shipment that is supported is PRINT_RETURN_LABEL.
     * With this option you can print a return label to insert into the box
     * of an outbound shipment. This option can not be used to print an outbound
     * label.
     */
    public void setReturnType(ReturnType returnType) {
        this.returnType = returnType;
    }


    /**
     * Gets the rma value for this ReturnShipmentDetail.
     * 
     * @return rma   * Return Merchant Authorization
     */
    public Rma getRma() {
        return rma;
    }


    /**
     * Sets the rma value for this ReturnShipmentDetail.
     * 
     * @param rma   * Return Merchant Authorization
     */
    public void setRma(Rma rma) {
        this.rma = rma;
    }


    /**
     * Gets the emailLabelDetail value for this ReturnShipmentDetail.
     * 
     * @return emailLabelDetail   * Specific information about the delivery of the email and options
     * for the shipment.
     */
    public EMailLabelDetail getEmailLabelDetail() {
        return emailLabelDetail;
    }


    /**
     * Sets the emailLabelDetail value for this ReturnShipmentDetail.
     * 
     * @param emailLabelDetail   * Specific information about the delivery of the email and options
     * for the shipment.
     */
    public void setEmailLabelDetail(EMailLabelDetail emailLabelDetail) {
        this.emailLabelDetail = emailLabelDetail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReturnShipmentDetail)) return false;
        ReturnShipmentDetail other = (ReturnShipmentDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.returnType==null && other.getReturnType()==null) || 
             (this.returnType!=null &&
              this.returnType.equals(other.getReturnType()))) &&
            ((this.rma==null && other.getRma()==null) || 
             (this.rma!=null &&
              this.rma.equals(other.getRma()))) &&
            ((this.emailLabelDetail==null && other.getEmailLabelDetail()==null) || 
             (this.emailLabelDetail!=null &&
              this.emailLabelDetail.equals(other.getEmailLabelDetail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getReturnType() != null) {
            _hashCode += getReturnType().hashCode();
        }
        if (getRma() != null) {
            _hashCode += getRma().hashCode();
        }
        if (getEmailLabelDetail() != null) {
            _hashCode += getEmailLabelDetail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReturnShipmentDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ReturnShipmentDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ReturnType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ReturnType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rma");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Rma"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "Rma"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailLabelDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "EmailLabelDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "EMailLabelDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
