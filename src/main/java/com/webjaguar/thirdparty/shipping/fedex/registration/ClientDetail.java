/**
 * ClientDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.registration;


/**
 * The descriptive data identifying the client submitting the transaction.
 */
public class ClientDetail  implements java.io.Serializable {
    /* The FedEx account number assigned to the customer initiating
     * the request. */
    private java.lang.String accountNumber;

    /* Identifies the unique client device submitting the request.
     * This number is assigned by FedEx and identifies the unique device
     * from which the request is originating. */
    private java.lang.String meterNumber;

    /* Identifies the product. */
    private java.lang.String clientProductId;

    /* Identifies the product version. */
    private java.lang.String clientProductVersion;

    /* Governs any future language/translations used for human-readable
     * Notification.localizedMessages in responses to the request containing
     * this ClientDetail object. Different requests from the same client
     * may contain different Localization data. (Contrast with TransactionDetail.localization,
     * which governs data payload language/translation.) */
    private com.webjaguar.thirdparty.shipping.fedex.registration.Localization localization;

    public ClientDetail() {
    }

    public ClientDetail(
           java.lang.String accountNumber,
           java.lang.String meterNumber,
           java.lang.String clientProductId,
           java.lang.String clientProductVersion,
           com.webjaguar.thirdparty.shipping.fedex.registration.Localization localization) {
           this.accountNumber = accountNumber;
           this.meterNumber = meterNumber;
           this.clientProductId = clientProductId;
           this.clientProductVersion = clientProductVersion;
           this.localization = localization;
    }


    /**
     * Gets the accountNumber value for this ClientDetail.
     * 
     * @return accountNumber   * The FedEx account number assigned to the customer initiating
     * the request.
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this ClientDetail.
     * 
     * @param accountNumber   * The FedEx account number assigned to the customer initiating
     * the request.
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the meterNumber value for this ClientDetail.
     * 
     * @return meterNumber   * Identifies the unique client device submitting the request.
     * This number is assigned by FedEx and identifies the unique device
     * from which the request is originating.
     */
    public java.lang.String getMeterNumber() {
        return meterNumber;
    }


    /**
     * Sets the meterNumber value for this ClientDetail.
     * 
     * @param meterNumber   * Identifies the unique client device submitting the request.
     * This number is assigned by FedEx and identifies the unique device
     * from which the request is originating.
     */
    public void setMeterNumber(java.lang.String meterNumber) {
        this.meterNumber = meterNumber;
    }


    /**
     * Gets the clientProductId value for this ClientDetail.
     * 
     * @return clientProductId   * Identifies the product.
     */
    public java.lang.String getClientProductId() {
        return clientProductId;
    }


    /**
     * Sets the clientProductId value for this ClientDetail.
     * 
     * @param clientProductId   * Identifies the product.
     */
    public void setClientProductId(java.lang.String clientProductId) {
        this.clientProductId = clientProductId;
    }


    /**
     * Gets the clientProductVersion value for this ClientDetail.
     * 
     * @return clientProductVersion   * Identifies the product version.
     */
    public java.lang.String getClientProductVersion() {
        return clientProductVersion;
    }


    /**
     * Sets the clientProductVersion value for this ClientDetail.
     * 
     * @param clientProductVersion   * Identifies the product version.
     */
    public void setClientProductVersion(java.lang.String clientProductVersion) {
        this.clientProductVersion = clientProductVersion;
    }


    /**
     * Gets the localization value for this ClientDetail.
     * 
     * @return localization   * Governs any future language/translations used for human-readable
     * Notification.localizedMessages in responses to the request containing
     * this ClientDetail object. Different requests from the same client
     * may contain different Localization data. (Contrast with TransactionDetail.localization,
     * which governs data payload language/translation.)
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.Localization getLocalization() {
        return localization;
    }


    /**
     * Sets the localization value for this ClientDetail.
     * 
     * @param localization   * Governs any future language/translations used for human-readable
     * Notification.localizedMessages in responses to the request containing
     * this ClientDetail object. Different requests from the same client
     * may contain different Localization data. (Contrast with TransactionDetail.localization,
     * which governs data payload language/translation.)
     */
    public void setLocalization(com.webjaguar.thirdparty.shipping.fedex.registration.Localization localization) {
        this.localization = localization;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClientDetail)) return false;
        ClientDetail other = (ClientDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.meterNumber==null && other.getMeterNumber()==null) || 
             (this.meterNumber!=null &&
              this.meterNumber.equals(other.getMeterNumber()))) &&
            ((this.clientProductId==null && other.getClientProductId()==null) || 
             (this.clientProductId!=null &&
              this.clientProductId.equals(other.getClientProductId()))) &&
            ((this.clientProductVersion==null && other.getClientProductVersion()==null) || 
             (this.clientProductVersion!=null &&
              this.clientProductVersion.equals(other.getClientProductVersion()))) &&
            ((this.localization==null && other.getLocalization()==null) || 
             (this.localization!=null &&
              this.localization.equals(other.getLocalization())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getMeterNumber() != null) {
            _hashCode += getMeterNumber().hashCode();
        }
        if (getClientProductId() != null) {
            _hashCode += getClientProductId().hashCode();
        }
        if (getClientProductVersion() != null) {
            _hashCode += getClientProductVersion().hashCode();
        }
        if (getLocalization() != null) {
            _hashCode += getLocalization().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClientDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ClientDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("meterNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "MeterNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientProductId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ClientProductId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientProductVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ClientProductVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("localization");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Localization"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Localization"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
