/**
 * RegistrationServiceSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.registration;

public class RegistrationServiceSoapBindingStub extends org.apache.axis.client.Stub implements com.webjaguar.thirdparty.shipping.fedex.registration.RegistrationPortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[3];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("subscription");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "SubscriptionRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "SubscriptionRequest"), com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "SubscriptionReply"));
        oper.setReturnClass(com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionReply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "SubscriptionReply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("registerCSPUser");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "RegisterCSPUserRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "RegisterCSPUserRequest"), com.webjaguar.thirdparty.shipping.fedex.registration.RegisterCSPUserRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "RegisterCSPUserReply"));
        oper.setReturnClass(com.webjaguar.thirdparty.shipping.fedex.registration.RegisterCSPUserReply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "RegisterCSPUserReply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("versionCapture");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionCaptureRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionCaptureRequest"), com.webjaguar.thirdparty.shipping.fedex.registration.VersionCaptureRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionCaptureReply"));
        oper.setReturnClass(com.webjaguar.thirdparty.shipping.fedex.registration.VersionCaptureReply.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionCaptureReply"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

    }

    public RegistrationServiceSoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public RegistrationServiceSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public RegistrationServiceSoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Address");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.Address.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ClientDetail");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Contact");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.Contact.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "CspType");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.CspType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Localization");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.Localization.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Notification");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.Notification.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "NotificationParameter");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.NotificationParameter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "NotificationSeverityType");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.NotificationSeverityType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ParsedContact");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.ParsedContact.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ParsedContactAndAddress");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.ParsedContactAndAddress.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ParsedPersonName");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.ParsedPersonName.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Party");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.Party.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "RegisterCSPUserReply");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.RegisterCSPUserReply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "RegisterCSPUserRequest");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.RegisterCSPUserRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "SubscriptionReply");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionReply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "SubscriptionRequest");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "SubscriptionServiceType");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionServiceType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "TransactionDetail");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionCaptureReply");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.VersionCaptureReply.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionCaptureRequest");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.VersionCaptureRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionId");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.VersionId.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "WebAuthenticationCredential");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationCredential.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "WebAuthenticationDetail");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionReply subscription(com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionRequest subscriptionRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("subscription");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "subscription"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {subscriptionRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionReply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionReply) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionReply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.shipping.fedex.registration.RegisterCSPUserReply registerCSPUser(com.webjaguar.thirdparty.shipping.fedex.registration.RegisterCSPUserRequest registerCSPUserRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("registerCSPUser");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "registerCSPUser"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {registerCSPUserRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.shipping.fedex.registration.RegisterCSPUserReply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.shipping.fedex.registration.RegisterCSPUserReply) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.shipping.fedex.registration.RegisterCSPUserReply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.shipping.fedex.registration.VersionCaptureReply versionCapture(com.webjaguar.thirdparty.shipping.fedex.registration.VersionCaptureRequest versionCaptureRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("versionCapture");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "versionCapture"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {versionCaptureRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.shipping.fedex.registration.VersionCaptureReply) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.shipping.fedex.registration.VersionCaptureReply) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.shipping.fedex.registration.VersionCaptureReply.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
