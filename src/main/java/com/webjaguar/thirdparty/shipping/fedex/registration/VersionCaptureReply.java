/**
 * VersionCaptureReply.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.registration;


/**
 * FedEx SSP Version Capture reply.
 */
public class VersionCaptureReply  implements java.io.Serializable {
    /* Identifies the highest severity encountered when executing
     * the request; in order from high to low: FAILURE, ERROR, WARNING, NOTE,
     * SUCCESS. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.NotificationSeverityType highestSeverity;

    /* The descriptive data detailing the status of a sumbitted transaction. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.Notification[] notifications;

    /* Descriptive data that governs data payload language/translations.
     * The TransactionDetail from the request is echoed back to the caller
     * in the corresponding reply. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail;

    /* Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply). */
    private com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version;

    public VersionCaptureReply() {
    }

    public VersionCaptureReply(
           com.webjaguar.thirdparty.shipping.fedex.registration.NotificationSeverityType highestSeverity,
           com.webjaguar.thirdparty.shipping.fedex.registration.Notification[] notifications,
           com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail,
           com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version) {
           this.highestSeverity = highestSeverity;
           this.notifications = notifications;
           this.transactionDetail = transactionDetail;
           this.version = version;
    }


    /**
     * Gets the highestSeverity value for this VersionCaptureReply.
     * 
     * @return highestSeverity   * Identifies the highest severity encountered when executing
     * the request; in order from high to low: FAILURE, ERROR, WARNING, NOTE,
     * SUCCESS.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.NotificationSeverityType getHighestSeverity() {
        return highestSeverity;
    }


    /**
     * Sets the highestSeverity value for this VersionCaptureReply.
     * 
     * @param highestSeverity   * Identifies the highest severity encountered when executing
     * the request; in order from high to low: FAILURE, ERROR, WARNING, NOTE,
     * SUCCESS.
     */
    public void setHighestSeverity(com.webjaguar.thirdparty.shipping.fedex.registration.NotificationSeverityType highestSeverity) {
        this.highestSeverity = highestSeverity;
    }


    /**
     * Gets the notifications value for this VersionCaptureReply.
     * 
     * @return notifications   * The descriptive data detailing the status of a sumbitted transaction.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.Notification[] getNotifications() {
        return notifications;
    }


    /**
     * Sets the notifications value for this VersionCaptureReply.
     * 
     * @param notifications   * The descriptive data detailing the status of a sumbitted transaction.
     */
    public void setNotifications(com.webjaguar.thirdparty.shipping.fedex.registration.Notification[] notifications) {
        this.notifications = notifications;
    }

    public com.webjaguar.thirdparty.shipping.fedex.registration.Notification getNotifications(int i) {
        return this.notifications[i];
    }

    public void setNotifications(int i, com.webjaguar.thirdparty.shipping.fedex.registration.Notification _value) {
        this.notifications[i] = _value;
    }


    /**
     * Gets the transactionDetail value for this VersionCaptureReply.
     * 
     * @return transactionDetail   * Descriptive data that governs data payload language/translations.
     * The TransactionDetail from the request is echoed back to the caller
     * in the corresponding reply.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail getTransactionDetail() {
        return transactionDetail;
    }


    /**
     * Sets the transactionDetail value for this VersionCaptureReply.
     * 
     * @param transactionDetail   * Descriptive data that governs data payload language/translations.
     * The TransactionDetail from the request is echoed back to the caller
     * in the corresponding reply.
     */
    public void setTransactionDetail(com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail) {
        this.transactionDetail = transactionDetail;
    }


    /**
     * Gets the version value for this VersionCaptureReply.
     * 
     * @return version   * Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply).
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.VersionId getVersion() {
        return version;
    }


    /**
     * Sets the version value for this VersionCaptureReply.
     * 
     * @param version   * Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply).
     */
    public void setVersion(com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version) {
        this.version = version;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof VersionCaptureReply)) return false;
        VersionCaptureReply other = (VersionCaptureReply) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.highestSeverity==null && other.getHighestSeverity()==null) || 
             (this.highestSeverity!=null &&
              this.highestSeverity.equals(other.getHighestSeverity()))) &&
            ((this.notifications==null && other.getNotifications()==null) || 
             (this.notifications!=null &&
              java.util.Arrays.equals(this.notifications, other.getNotifications()))) &&
            ((this.transactionDetail==null && other.getTransactionDetail()==null) || 
             (this.transactionDetail!=null &&
              this.transactionDetail.equals(other.getTransactionDetail()))) &&
            ((this.version==null && other.getVersion()==null) || 
             (this.version!=null &&
              this.version.equals(other.getVersion())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHighestSeverity() != null) {
            _hashCode += getHighestSeverity().hashCode();
        }
        if (getNotifications() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNotifications());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNotifications(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTransactionDetail() != null) {
            _hashCode += getTransactionDetail().hashCode();
        }
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(VersionCaptureReply.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionCaptureReply"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("highestSeverity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "HighestSeverity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "NotificationSeverityType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("notifications");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Notifications"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Notification"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "TransactionDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "TransactionDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionId"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
