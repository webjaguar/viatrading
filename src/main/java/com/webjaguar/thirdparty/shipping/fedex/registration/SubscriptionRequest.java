/**
 * SubscriptionRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.registration;


/**
 * The descriptive data sent by the customer to register to use FedEx
 * Web Services. This is a one-time only request to register the customer's
 * FedEx account. A unique meter number specific to the customer's FedEx
 * account number will be returned to the client. The meter number should
 * be used in all subsequent requests sent to FedEx web services.
 */
public class SubscriptionRequest  implements java.io.Serializable {
    /* The descriptive data to be used in authentication of the sender's
     * identity (and right to use FedEx web services). */
    private com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail webAuthenticationDetail;

    /* The descriptive data identifying the client submitting the
     * transaction. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail clientDetail;

    /* The descriptive data for this customer transaction. The TransactionDetail
     * from the request is echoed back to the caller in the corresponding
     * reply. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail;

    /* Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply). */
    private com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version;

    /* A unique identifier assigned to CSPs by FedEx */
    private java.lang.String cspSolutionId;

    /* If the FedEx Web Services caller is a CSP, set this element
     * to one of the values associated with CspType. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.CspType cspType;

    /* The descriptive data element that provides information about
     * the party initiating the subscription request to FedEx Web Services. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.Party subscriber;

    /* The descriptive data for the physical shipping address for
     * the account. */
    private com.webjaguar.thirdparty.shipping.fedex.registration.Address accountShippingAddress;

    public SubscriptionRequest() {
    }

    public SubscriptionRequest(
           com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail webAuthenticationDetail,
           com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail clientDetail,
           com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail,
           com.webjaguar.thirdparty.shipping.fedex.registration.VersionId version,
           java.lang.String cspSolutionId,
           com.webjaguar.thirdparty.shipping.fedex.registration.CspType cspType,
           com.webjaguar.thirdparty.shipping.fedex.registration.Party subscriber,
           com.webjaguar.thirdparty.shipping.fedex.registration.Address accountShippingAddress) {
           this.webAuthenticationDetail = webAuthenticationDetail;
           this.clientDetail = clientDetail;
           this.transactionDetail = transactionDetail;
           this.version = version;
           this.cspSolutionId = cspSolutionId;
           this.cspType = cspType;
           this.subscriber = subscriber;
           this.accountShippingAddress = accountShippingAddress;
    }


    /**
     * Gets the webAuthenticationDetail value for this SubscriptionRequest.
     * 
     * @return webAuthenticationDetail   * The descriptive data to be used in authentication of the sender's
     * identity (and right to use FedEx web services).
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail getWebAuthenticationDetail() {
        return webAuthenticationDetail;
    }


    /**
     * Sets the webAuthenticationDetail value for this SubscriptionRequest.
     * 
     * @param webAuthenticationDetail   * The descriptive data to be used in authentication of the sender's
     * identity (and right to use FedEx web services).
     */
    public void setWebAuthenticationDetail(com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationDetail webAuthenticationDetail) {
        this.webAuthenticationDetail = webAuthenticationDetail;
    }


    /**
     * Gets the clientDetail value for this SubscriptionRequest.
     * 
     * @return clientDetail   * The descriptive data identifying the client submitting the
     * transaction.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail getClientDetail() {
        return clientDetail;
    }


    /**
     * Sets the clientDetail value for this SubscriptionRequest.
     * 
     * @param clientDetail   * The descriptive data identifying the client submitting the
     * transaction.
     */
    public void setClientDetail(com.webjaguar.thirdparty.shipping.fedex.registration.ClientDetail clientDetail) {
        this.clientDetail = clientDetail;
    }


    /**
     * Gets the transactionDetail value for this SubscriptionRequest.
     * 
     * @return transactionDetail   * The descriptive data for this customer transaction. The TransactionDetail
     * from the request is echoed back to the caller in the corresponding
     * reply.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail getTransactionDetail() {
        return transactionDetail;
    }


    /**
     * Sets the transactionDetail value for this SubscriptionRequest.
     * 
     * @param transactionDetail   * The descriptive data for this customer transaction. The TransactionDetail
     * from the request is echoed back to the caller in the corresponding
     * reply.
     */
    public void setTransactionDetail(com.webjaguar.thirdparty.shipping.fedex.registration.TransactionDetail transactionDetail) {
        this.transactionDetail = transactionDetail;
    }


    /**
     * Gets the version value for this SubscriptionRequest.
     * 
     * @return version   * Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply).
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.VersionId getVersion() {
        return version;
    }


    /**
     * Sets the version value for this SubscriptionRequest.
     * 
     * @param versionId   * Identifies the version/level of a service operation expected
     * by a caller (in each request) and performed by the callee (in each
     * reply).
     */
    public void setVersion(com.webjaguar.thirdparty.shipping.fedex.registration.VersionId versionId) {
        this.version = versionId;
    }


    /**
     * Gets the cspSolutionId value for this SubscriptionRequest.
     * 
     * @return cspSolutionId   * A unique identifier assigned to CSPs by FedEx
     */
    public java.lang.String getCspSolutionId() {
        return cspSolutionId;
    }


    /**
     * Sets the cspSolutionId value for this SubscriptionRequest.
     * 
     * @param cspSolutionId   * A unique identifier assigned to CSPs by FedEx
     */
    public void setCspSolutionId(java.lang.String cspSolutionId) {
        this.cspSolutionId = cspSolutionId;
    }


    /**
     * Gets the cspType value for this SubscriptionRequest.
     * 
     * @return cspType   * If the FedEx Web Services caller is a CSP, set this element
     * to one of the values associated with CspType.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.CspType getCspType() {
        return cspType;
    }


    /**
     * Sets the cspType value for this SubscriptionRequest.
     * 
     * @param cspType   * If the FedEx Web Services caller is a CSP, set this element
     * to one of the values associated with CspType.
     */
    public void setCspType(com.webjaguar.thirdparty.shipping.fedex.registration.CspType cspType) {
        this.cspType = cspType;
    }


    /**
     * Gets the subscriber value for this SubscriptionRequest.
     * 
     * @return subscriber   * The descriptive data element that provides information about
     * the party initiating the subscription request to FedEx Web Services.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.Party getSubscriber() {
        return subscriber;
    }


    /**
     * Sets the subscriber value for this SubscriptionRequest.
     * 
     * @param subscriber   * The descriptive data element that provides information about
     * the party initiating the subscription request to FedEx Web Services.
     */
    public void setSubscriber(com.webjaguar.thirdparty.shipping.fedex.registration.Party subscriber) {
        this.subscriber = subscriber;
    }


    /**
     * Gets the accountShippingAddress value for this SubscriptionRequest.
     * 
     * @return accountShippingAddress   * The descriptive data for the physical shipping address for
     * the account.
     */
    public com.webjaguar.thirdparty.shipping.fedex.registration.Address getAccountShippingAddress() {
        return accountShippingAddress;
    }


    /**
     * Sets the accountShippingAddress value for this SubscriptionRequest.
     * 
     * @param accountShippingAddress   * The descriptive data for the physical shipping address for
     * the account.
     */
    public void setAccountShippingAddress(com.webjaguar.thirdparty.shipping.fedex.registration.Address accountShippingAddress) {
        this.accountShippingAddress = accountShippingAddress;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubscriptionRequest)) return false;
        SubscriptionRequest other = (SubscriptionRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.webAuthenticationDetail==null && other.getWebAuthenticationDetail()==null) || 
             (this.webAuthenticationDetail!=null &&
              this.webAuthenticationDetail.equals(other.getWebAuthenticationDetail()))) &&
            ((this.clientDetail==null && other.getClientDetail()==null) || 
             (this.clientDetail!=null &&
              this.clientDetail.equals(other.getClientDetail()))) &&
            ((this.transactionDetail==null && other.getTransactionDetail()==null) || 
             (this.transactionDetail!=null &&
              this.transactionDetail.equals(other.getTransactionDetail()))) &&
            ((this.version==null && other.getVersion()==null) || 
             (this.version!=null &&
              this.version.equals(other.getVersion()))) &&
            ((this.cspSolutionId==null && other.getCspSolutionId()==null) || 
             (this.cspSolutionId!=null &&
              this.cspSolutionId.equals(other.getCspSolutionId()))) &&
            ((this.cspType==null && other.getCspType()==null) || 
             (this.cspType!=null &&
              this.cspType.equals(other.getCspType()))) &&
            ((this.subscriber==null && other.getSubscriber()==null) || 
             (this.subscriber!=null &&
              this.subscriber.equals(other.getSubscriber()))) &&
            ((this.accountShippingAddress==null && other.getAccountShippingAddress()==null) || 
             (this.accountShippingAddress!=null &&
              this.accountShippingAddress.equals(other.getAccountShippingAddress())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getWebAuthenticationDetail() != null) {
            _hashCode += getWebAuthenticationDetail().hashCode();
        }
        if (getClientDetail() != null) {
            _hashCode += getClientDetail().hashCode();
        }
        if (getTransactionDetail() != null) {
            _hashCode += getTransactionDetail().hashCode();
        }
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        if (getCspSolutionId() != null) {
            _hashCode += getCspSolutionId().hashCode();
        }
        if (getCspType() != null) {
            _hashCode += getCspType().hashCode();
        }
        if (getSubscriber() != null) {
            _hashCode += getSubscriber().hashCode();
        }
        if (getAccountShippingAddress() != null) {
            _hashCode += getAccountShippingAddress().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubscriptionRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "SubscriptionRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("webAuthenticationDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "WebAuthenticationDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "WebAuthenticationDetail"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ClientDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "ClientDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "TransactionDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "TransactionDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "VersionId"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cspSolutionId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "CspSolutionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cspType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "CspType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "CspType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subscriber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Subscriber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Party"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountShippingAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "AccountShippingAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "Address"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
