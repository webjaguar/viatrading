/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 8.10.2008
 */

package com.webjaguar.thirdparty.shipping.fedex.registration;

public class FedexSubscriptionRequest {
	// in fedex jar file change test url to production url

	String aemAccountNumber = "248976535";
	public String getMeterNumber(String personName, String companyName, String phoneNumber, String eMailAddress, String userKey, String userPassword) {

		SubscriptionRequest request = new SubscriptionRequest(); // Build a request object

		ClientDetail clientDetail = createClientDetail();
		clientDetail.setClientProductId("HYBF");
        clientDetail.setClientProductVersion("3787");
        request.setClientDetail(clientDetail);

        request.setWebAuthenticationDetail(createWebAuthenticationDetail(userKey, userPassword));

	    TransactionDetail transactionDetail = new TransactionDetail();
	    transactionDetail.setCustomerTransactionId("webjaguar-08"); //This is a reference field for the customer.  Any value can be used and will be provided in the response.
	    request.setTransactionDetail(transactionDetail);

        VersionId versionId = new VersionId("wsi", 1, 0, 0);
        request.setVersion(versionId);  


	    // optional
        request.setCspType( CspType.CERTIFIED_SOLUTION_PROVIDER );
	    
	    Party party = new Party();
	    party.setAccountNumber( aemAccountNumber );
	    Contact contact = new Contact();
	    contact.setPersonName( personName );
	    contact.setPhoneNumber( phoneNumber );
	    contact.setEMailAddress( eMailAddress );
	    party.setContact( contact );
	    
	    Address companyAddress = new Address();
	    companyAddress.setStreetLines(new String[] { "6b Liberty STE 145" });
	    companyAddress.setCity("ALISO VIEJO");
	    companyAddress.setPostalCode("92656");
	    companyAddress.setStateOrProvinceCode("CA");
	    companyAddress.setCountryCode("US");
	    party.setAddress( companyAddress );
	    
	    request.setSubscriber( party );
	    request.setAccountShippingAddress( companyAddress );

		try {
			RegistrationServiceLocator service;
			RegistrationPortType port;

			service = new RegistrationServiceLocator();
			updateEndPoint(service);
			port = service.getRegistrationServicePort();

			SubscriptionReply reply = port.subscription( request); // This is the call to the web service passing in a request object and returning a reply object

			if (isResponseOk(reply.getHighestSeverity())) // check if the call was successful
			{
				/*SubscriptionServiceType serviceType[] = reply.getRegisteredServices();
				for (int i=0; i<serviceType.length; i++) { // List of registered services
					System.out.println("Service Type: " + serviceType[i].toString());
				}*/
				return reply.getMeterNumber();
			}
			else {
				printNotifications(reply.getNotifications());
			}
		} catch (Exception e) {
		    //e.printStackTrace();
		}
        
		return null;
    }
	
	private static boolean isResponseOk(NotificationSeverityType notificationSeverityType) {
		if (notificationSeverityType == null) {
			return false;
		}
		if (notificationSeverityType.equals(NotificationSeverityType.WARNING) ||
			notificationSeverityType.equals(NotificationSeverityType.NOTE)    ||
			notificationSeverityType.equals(NotificationSeverityType.SUCCESS)) {
			return true;
		}
 		return false;
	}

	private static ClientDetail createClientDetail() {
        ClientDetail clientDetail = new ClientDetail();
        String accountNumber = System.getProperty("accountNumber");
        String meterNumber = System.getProperty("meterNumber");

        // See if the accountNumber and meterNumber properties are set,
        // if set use those values, otherwise default them to "XXX"

        if (accountNumber == null) {
        	accountNumber = "248976535"; // Replace "XXX" with clients account number
        }
        if (meterNumber == null) {
        	meterNumber = ""; // Replace "XXX" with clients meter number
        }
        clientDetail.setAccountNumber(accountNumber);
        clientDetail.setMeterNumber(meterNumber);
        return clientDetail;
	}

	private static WebAuthenticationDetail createWebAuthenticationDetail(String key, String password) {
        WebAuthenticationCredential wac = new WebAuthenticationCredential();
        wac.setKey(key);
        wac.setPassword(password);

        WebAuthenticationCredential csp = new WebAuthenticationCredential();
        String cspKey = System.getProperty("cspKey");
        String cspPassword = System.getProperty("cspPassword");

        // See if the key and password properties are set,
        // if set use those values, otherwise default them to "XXX"

        if (cspKey == null) {
        	cspKey = "0IPR1Lx36Ng5ofW3"; // Replace "XXX" with clients key
        }
        if (cspPassword == null) {
        	cspPassword = "gXc253TrFxPXdjKxKSjfWT66D"; // Replace "XXX" with clients password
        }

        csp.setKey(cspKey);
        csp.setPassword(cspPassword);

		return new WebAuthenticationDetail(csp, wac);
	}

	private static void printNotifications(Notification[] notifications) {
		System.out.println("Notifications:");
		if (notifications == null || notifications.length == 0) {
			System.out.println("  No notifications returned");
		}
		for (int i=0; i < notifications.length; i++){
			Notification n = notifications[i];
			System.out.print("  Notification no. " + i + ": ");
			if (n == null) {
				System.out.println("null");
				continue;
			} else {
				System.out.println("");
			}
			NotificationSeverityType nst = n.getSeverity();

			System.out.println("    Severity: " + (nst == null ? "null" : nst.getValue()));
			System.out.println("    Code: " + n.getCode());
			System.out.println("    Message: " + n.getMessage());
			System.out.println("    Source: " + n.getSource());
		}
	}

	private static void updateEndPoint(RegistrationServiceLocator serviceLocator) {
		String endPoint = System.getProperty("endPoint");
		if (endPoint != null) {
			serviceLocator.setRegistrationServicePortEndpointAddress(endPoint);
		}
	}

	private static String getSysProperty(String propertyName) {
		// See if the property is set, otherwise set some default values
		String propertyValue = System.getProperty(propertyName);
		if (propertyName.equals("TrackingNumber")) {
			if (propertyValue == null) {
				propertyValue = ""; // replace with your tracking number
			}
		} else if (propertyName.equals("Shipment.AccountNumber")) {
			if (propertyValue == null) {
				propertyValue = "XXX"; // replace with your shipment account number
			}
		} else if (propertyName.equals("Payor.AccountNumber")) {
			if (propertyValue == null) {
				propertyValue = "XXX"; // replace with your payor account number
			}
		} else if (propertyName.equals("ShipDate")) {
			if (propertyValue == null) {
				propertyValue = "20071227"; // replace with your ship date
			}
		} else if (propertyName.equals("accountNumber")) {
			if (propertyValue == null) {
				propertyValue = "XXX"; // replace with your account number
			}
		}

		return propertyValue;
	}

}
