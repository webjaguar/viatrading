/**
 * RegistrationServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.registration;

public class RegistrationServiceLocator extends org.apache.axis.client.Service implements com.webjaguar.thirdparty.shipping.fedex.registration.RegistrationService {

    public RegistrationServiceLocator() {
    }


    public RegistrationServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public RegistrationServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    //Use to get a proxy class for RegistrationServicePort
    //https://gatewaybeta.fedex.com:443/web-services
    //http://localhost:1234/webjaguar1
    //https://gateway.fedex.com/web-services
    private java.lang.String RegistrationServicePort_address = "https://gateway.fedex.com/web-services";

    public java.lang.String getRegistrationServicePortAddress() {
        return RegistrationServicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String RegistrationServicePortWSDDServiceName = "RegistrationServicePort";

    public java.lang.String getRegistrationServicePortWSDDServiceName() {
        return RegistrationServicePortWSDDServiceName;
    }

    public void setRegistrationServicePortWSDDServiceName(java.lang.String name) {
        RegistrationServicePortWSDDServiceName = name;
    }

    public com.webjaguar.thirdparty.shipping.fedex.registration.RegistrationPortType getRegistrationServicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(RegistrationServicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getRegistrationServicePort(endpoint);
    }

    public com.webjaguar.thirdparty.shipping.fedex.registration.RegistrationPortType getRegistrationServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.webjaguar.thirdparty.shipping.fedex.registration.RegistrationServiceSoapBindingStub _stub = new com.webjaguar.thirdparty.shipping.fedex.registration.RegistrationServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getRegistrationServicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setRegistrationServicePortEndpointAddress(java.lang.String address) {
        RegistrationServicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.webjaguar.thirdparty.shipping.fedex.registration.RegistrationPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                com.webjaguar.thirdparty.shipping.fedex.registration.RegistrationServiceSoapBindingStub _stub = new com.webjaguar.thirdparty.shipping.fedex.registration.RegistrationServiceSoapBindingStub(new java.net.URL(RegistrationServicePort_address), this);
                _stub.setPortName(getRegistrationServicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("RegistrationServicePort".equals(inputPortName)) {
            return getRegistrationServicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "RegistrationService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://fedex.com/ws/registration/v1", "RegistrationServicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
    	if ("RegistrationServicePort".equals(portName)) {
            setRegistrationServicePortEndpointAddress(address);
        }
        else 
        { // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
