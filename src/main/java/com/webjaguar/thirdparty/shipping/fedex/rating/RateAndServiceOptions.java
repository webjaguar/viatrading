/**
 * RateAndServiceOptions.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.rating;


/**
 * The descriptive data for the collection of available FedEx transportation
 * service options and associated courtesy rate quotes returned for the
 * request.
 */
public class RateAndServiceOptions  implements java.io.Serializable {
    /* Descriptive data about a FedEx transportation service that
     * was found to be available given the shipment criteria in the request. */
    private ServiceDetail serviceDetail;

    /* Descriptive data for the actual FedEx courtesy rate returned
     * for an available service. Note, this is a courtesy rate and may be
     * different from what is invoiced if the package is shipped. */
    private ReturnedRateType actualRateType;

    /* This element groups the shipment and package rating data for
     * a specific rate type for use in a rating reply, which groups result
     * data by rate type. */
    private RatedShipmentDetail[] ratedShipmentDetails;

    public RateAndServiceOptions() {
    }

    public RateAndServiceOptions(
           ServiceDetail serviceDetail,
           ReturnedRateType actualRateType,
           RatedShipmentDetail[] ratedShipmentDetails) {
           this.serviceDetail = serviceDetail;
           this.actualRateType = actualRateType;
           this.ratedShipmentDetails = ratedShipmentDetails;
    }


    /**
     * Gets the serviceDetail value for this RateAndServiceOptions.
     * 
     * @return serviceDetail   * Descriptive data about a FedEx transportation service that
     * was found to be available given the shipment criteria in the request.
     */
    public ServiceDetail getServiceDetail() {
        return serviceDetail;
    }


    /**
     * Sets the serviceDetail value for this RateAndServiceOptions.
     * 
     * @param serviceDetail   * Descriptive data about a FedEx transportation service that
     * was found to be available given the shipment criteria in the request.
     */
    public void setServiceDetail(ServiceDetail serviceDetail) {
        this.serviceDetail = serviceDetail;
    }


    /**
     * Gets the actualRateType value for this RateAndServiceOptions.
     * 
     * @return actualRateType   * Descriptive data for the actual FedEx courtesy rate returned
     * for an available service. Note, this is a courtesy rate and may be
     * different from what is invoiced if the package is shipped.
     */
    public ReturnedRateType getActualRateType() {
        return actualRateType;
    }


    /**
     * Sets the actualRateType value for this RateAndServiceOptions.
     * 
     * @param actualRateType   * Descriptive data for the actual FedEx courtesy rate returned
     * for an available service. Note, this is a courtesy rate and may be
     * different from what is invoiced if the package is shipped.
     */
    public void setActualRateType(ReturnedRateType actualRateType) {
        this.actualRateType = actualRateType;
    }


    /**
     * Gets the ratedShipmentDetails value for this RateAndServiceOptions.
     * 
     * @return ratedShipmentDetails   * This element groups the shipment and package rating data for
     * a specific rate type for use in a rating reply, which groups result
     * data by rate type.
     */
    public RatedShipmentDetail[] getRatedShipmentDetails() {
        return ratedShipmentDetails;
    }


    /**
     * Sets the ratedShipmentDetails value for this RateAndServiceOptions.
     * 
     * @param ratedShipmentDetails   * This element groups the shipment and package rating data for
     * a specific rate type for use in a rating reply, which groups result
     * data by rate type.
     */
    public void setRatedShipmentDetails(RatedShipmentDetail[] ratedShipmentDetails) {
        this.ratedShipmentDetails = ratedShipmentDetails;
    }

    public RatedShipmentDetail getRatedShipmentDetails(int i) {
        return this.ratedShipmentDetails[i];
    }

    public void setRatedShipmentDetails(int i, RatedShipmentDetail _value) {
        this.ratedShipmentDetails[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RateAndServiceOptions)) return false;
        RateAndServiceOptions other = (RateAndServiceOptions) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.serviceDetail==null && other.getServiceDetail()==null) || 
             (this.serviceDetail!=null &&
              this.serviceDetail.equals(other.getServiceDetail()))) &&
            ((this.actualRateType==null && other.getActualRateType()==null) || 
             (this.actualRateType!=null &&
              this.actualRateType.equals(other.getActualRateType()))) &&
            ((this.ratedShipmentDetails==null && other.getRatedShipmentDetails()==null) || 
             (this.ratedShipmentDetails!=null &&
              java.util.Arrays.equals(this.ratedShipmentDetails, other.getRatedShipmentDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getServiceDetail() != null) {
            _hashCode += getServiceDetail().hashCode();
        }
        if (getActualRateType() != null) {
            _hashCode += getActualRateType().hashCode();
        }
        if (getRatedShipmentDetails() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRatedShipmentDetails());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRatedShipmentDetails(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RateAndServiceOptions.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "RateAndServiceOptions"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ServiceDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ServiceDetail"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actualRateType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ActualRateType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "ReturnedRateType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ratedShipmentDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "RatedShipmentDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://fedex.com/ws/rate/v3", "RatedShipmentDetail"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
