/**
 * RegistrationPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.shipping.fedex.registration;

public interface RegistrationPortType extends java.rmi.Remote {
    public com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionReply subscription(com.webjaguar.thirdparty.shipping.fedex.registration.SubscriptionRequest subscriptionRequest) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.shipping.fedex.registration.RegisterCSPUserReply registerCSPUser(com.webjaguar.thirdparty.shipping.fedex.registration.RegisterCSPUserRequest registerCSPUserRequest) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.shipping.fedex.registration.VersionCaptureReply versionCapture(com.webjaguar.thirdparty.shipping.fedex.registration.VersionCaptureRequest versionCaptureRequest) throws java.rmi.RemoteException;
}
