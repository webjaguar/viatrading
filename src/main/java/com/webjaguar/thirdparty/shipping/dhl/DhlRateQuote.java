/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 10.10.2006
 */

package com.webjaguar.thirdparty.shipping.dhl;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.jdom.*;
import org.jdom.input.*;

import com.webjaguar.model.ShippingRate;

public class DhlRateQuote {
	
	// admin configurable fields
	private String url = "HTTPS://eCommerce.airborne.com/ApiLandingTest.asp";
	private String id = "ADVAN_1867";
	private String password = "HU05P7WQ45";
	private String shippingKey = "5A233F2B2C4E5C47415C555D505B3053434B514247595B5B50";
	private String accountNo = "795426159";
	private String codCode = "P";  // collect on delivery
	private int codValue = 45;
	private String date = formatDate();
	
	// shipmentType: L for letter, P for package
		
	public List getRates(String shipmentType,String weight,String length,String width,String height,String apCode, String apValue,
			String postalCode,String state){
		
		// the list to be returned
		List<ShippingRate> ratesList = new ArrayList<ShippingRate>();
		
		try{
            // make connection to DHL server
			URL url = new URL(this.url);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			
            // Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);
			
			// generate xml format request
			StringBuffer xmlRequest = new StringBuffer();
			xmlRequest.append(
			        "<?xml version=\"1.0\"?>" + 
			            "<ECommerce action=\"Request\" version=\"1.1\">" +
				            "<Requestor>" +
				                "<ID>" + this.id + "</ID>" +
				                "<Password>" + this.password + "</Password>" +
				            "</Requestor>" +
				            writeShipmentTag(shipmentType, weight, length, width, height, apCode, apValue,
				        			 postalCode, state) +
			         "</ECommerce>");
				        
            // post data
			OutputStream oStream = connection.getOutputStream();
			oStream.write(xmlRequest.toString().getBytes());
			oStream.close();
			
			// get response from server
			SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(connection.getInputStream());
			Element root = doc.getRootElement();
			
			// iterate through shipment tag, get rate estimate for each shippping method
			List shipmentList = root.getChildren("Shipment");  
			int index = 0;
			String[] dhlCodes = {"Express", "Next afternoon" ,"Second day service" ,"Ground" ,"Express 10:30AM" ,"Express Saturday"};
			for (Iterator it = shipmentList.iterator();it.hasNext();){  
				Element shipment = (Element)it.next();
				ShippingRate shippingRate = new ShippingRate();
				shippingRate.setCarrier("dhl");
				shippingRate.setIsAvailable(true);
				
				// if error occurs
				if(shipment.getChild("Faults")!=null){
					Element desc = shipment.getChild("Faults").getChild("Fault").getChild("Desc");
					shippingRate.setPrice(desc.getText());
					shippingRate.setIsAvailable(false);
				}
				
				// no error, get the total
				else {
					Element totalChargeEstimate = shipment.getChild("EstimateDetail").getChild("RateEstimate").getChild("TotalChargeEstimate");
					shippingRate.setPrice(totalChargeEstimate.getText().trim());
				}
				
				if(shippingRate.getIsAvailable()){
					if ( !shippingRate.getPrice().equals( "0.00" )) {
						ratesList.add(shippingRate);
					}
				    // add service type to each estimate
				    ratesList.get(ratesList.size()-1).setCode(dhlCodes[index]);
				    index++;
				}
			} 
		}catch(Exception e){
			System.err.print(e);
		}

		// return list of rates,errors
		return ratesList;
	}
	
	// generate Shipment tag one by one(one section for each shipping method)
	public String writeShipmentTag(String shipmentType,String weight,String length,String width,String height,String apCode, String apValue,
			String postalCode,String state) {
		
		// String to be returned
		StringBuffer shipmentTag = new StringBuffer();
		
		// construct shipment tag one by one
		StringTokenizer st = new StringTokenizer("E;N;S;G;1030;SAT",";");
		while(st.hasMoreTokens()){
		    String serviceCode = st.nextToken();
		    shipmentTag.append(
		        "<Shipment action = \"RateEstimate\" version = \"1.0\">" +
			        "<ShippingCredentials>" +
				        "<ShippingKey>" + this.shippingKey+ "</ShippingKey>" +
				        "<AccountNbr>" + this.accountNo + "</AccountNbr>" +
			        "</ShippingCredentials>" +
			        "<ShipmentDetail>" +
				        "<ShipDate>" + this.date + "</ShipDate>" +
				        "<Service>");
	 if(serviceCode.equals("1030")||serviceCode.equals("SAT")){
	     shipmentTag.append("<Code>E</Code>"); 
	 }
	 else{
		 shipmentTag.append("<Code>" + serviceCode  + "</Code>");
	 };
	                shipmentTag.append(
				        "</Service>" +
				        "<ShipmentType>" +
					        "<Code>" + shipmentType + "</Code>" +
				        "</ShipmentType>");
	            if(shipmentType.equals("P")){
				    shipmentTag.append(
				        "<Weight>" + weight + "</Weight>" +
						"<Dimensions>" +
						    "<Length>" + length + "</Length>" +
						    "<Width>" + width + "</Width>" +
							"<Height>" + height + "</Height>" +
						"</Dimensions>"	
				            		);	
				}
				    shipmentTag.append(
				        "<AdditionalProtection>" +
					        "<Code>" + apCode + "</Code>" +
					        "<Value>" + apValue + "</Value>" +
				        "</AdditionalProtection>");
				if(serviceCode.equals("1030")||serviceCode.equals("SAT")){
					shipmentTag.append(
					    "<SpecialServices>" +
			                "<SpecialService>" +
				            "<Code>" + serviceCode + "</Code>" +
			                "</SpecialService>" +
		                "</SpecialServices>");
				}
				shipmentTag.append(
			        "</ShipmentDetail>" +
			        "<Billing>" +
				        "<Party>" +
					        "<Code>S</Code>" +
				        "</Party>");
				if(this.codCode!=null){
				    shipmentTag.append(
				        "<CODPayment>" +
						    "<Code>" + this.codCode + "</Code>" +
							"<Value>" + this.codValue + "</Value>" +
						"</CODPayment>"
				        			);
				}
				shipmentTag.append(
			        "</Billing>" +
			        "<Receiver>" +
				        "<Address>" +
					        "<State>" + state + "</State>" +
					        "<PostalCode>" + postalCode + "</PostalCode>" +
					        "<Country>US</Country>" +
				        "</Address>" +
			        "</Receiver>" +
			    "</Shipment>" );
		   		
		}
		return shipmentTag.toString();
	}
	
	public String formatDate(){
		
		// Calendar object
        Calendar calendar = Calendar.getInstance();
		
        // dateFormat string buffer
		StringBuffer dateFormat = new StringBuffer();
		
		// append year
		dateFormat.append(calendar.get(Calendar.YEAR)).append("-");
		
		// append month
		int month = calendar.get(Calendar.MONTH) + 1;
		if(month<10){
			String monthString = "0" + month;
			dateFormat.append(monthString).append("-");
		}
		else{
			dateFormat.append(month).append("-");
		}
		
		// append day
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		if(day<10){
			String dayString = "0" + day;
			dateFormat.append(dayString);
		}
		else{
			dateFormat.append(day);
		}
		
		// return string
		return dateFormat.toString();
		
	}
	
}
