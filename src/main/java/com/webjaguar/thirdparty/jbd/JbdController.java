/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.28.2009
 */

package com.webjaguar.thirdparty.jbd;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;

public class JbdController extends MultiActionController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private DataSource dataSource; 
	private JdbcTemplate jdbcTemplate;
	public void setDataSource(DataSource dataSource) { 
		this.dataSource = dataSource; 
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
		
    public ModelAndView listOrders(HttpServletRequest request, HttpServletResponse response) {   	
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		ModelAndView modelAndView = new ModelAndView("frontend/jbd/orders", "model", model);

		Customer customer = (Customer) request.getAttribute("sessionCustomer");
		
		if (siteConfig.get("MAS200").getValue().trim().length() > 0) {
			List<Map<String, Object>> list = getOrders(customer.getAccountNumber());
			model.put("list", list);
			
			if (list.size() > 0) {
				Set<String> invoices = new HashSet<String>();
				for (Object object: getInvoices(customer.getAccountNumber(), "")) {
					invoices.add((String) ((Map<String, Object>) object).get("SalesOrderNumber"));
				}
				for (Map<String, Object> map: list) {
					map.put("isValidSalesOrderNumber", invoices.contains(map.get("SalesOrderNumber")));
				}				
			}
			
			model.put("jbdLayout", this.webJaguar.getSystemLayout("jbd-orders", request.getHeader("host"), request));
		}
				
        return modelAndView;    	
    }
    
    public ModelAndView viewOrder(HttpServletRequest request, HttpServletResponse response) { 
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		Map<String, Object> model = new HashMap<String, Object>();

		ModelAndView modelAndView = new ModelAndView("frontend/jbd/order", "model", model);
				
		Customer customer = (Customer) request.getAttribute("sessionCustomer");
		
		if (siteConfig.get("MAS200").getValue().trim().length() > 0) {
			String SalesOrderNumber = ServletRequestUtils.getStringParameter(request, "id", "");
			
			Map<String, Object> order = getOrder(SalesOrderNumber);
			if (order != null && order.get("CustomerNumber").equals(customer.getAccountNumber())) {
				model.put("order", order);
				
				// ship to
				model.put("shipping", this.webJaguar.getAddress(customer.getId(), (String) order.get("ShipToCode")));
				
				// lineitems
	    		model.put("lineItems", getOrderLineItems(SalesOrderNumber));		
	    		
	    		// tracking
	    		List<Map<String, Object>> tracking = getTracking(SalesOrderNumber);
	    		model.put("tracking", tracking);	
				
				if (tracking.size() > 0) {
					Set<String> invoices = new HashSet<String>();
					for (Object object: getInvoices(customer.getAccountNumber(), SalesOrderNumber)) {
						invoices.add((String) ((Map<String, Object>) object).get("InvoiceNumber"));
					}
					for (Map<String, Object> map: tracking) {
						map.put("isValidInvoiceNumber", invoices.contains(map.get("InvoiceNumber")));
					}				
				}
	    		
	    		model.put("countries", this.webJaguar.getCountryMap());
	    		model.put("invoiceLayout", this.webJaguar.getSystemLayout("jbd-order", request.getHeader("host"), request));
			}
		}
		
        return modelAndView;    	
    }
    
    public ModelAndView listInvoices(HttpServletRequest request, HttpServletResponse response) {   	
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		ModelAndView modelAndView = new ModelAndView("frontend/jbd/invoices", "model", model);

		Customer customer = (Customer) request.getAttribute("sessionCustomer");
		
		if (siteConfig.get("MAS200").getValue().trim().length() > 0) {
			String SalesOrderNumber = ServletRequestUtils.getStringParameter(request, "id", "");
			List<Map<String, Object>> list = getInvoices(customer.getAccountNumber(), SalesOrderNumber);
			model.put("list", list);		
			
			Set<String> orders = new HashSet<String>();
			for (Object object: getOrders(customer.getAccountNumber())) {
				orders.add((String) ((Map<String, Object>) object).get("SalesOrderNumber"));
			}
			for (Map<String, Object> map: list) {
				map.put("isValidSalesOrderNumber", orders.contains(map.get("SalesOrderNumber")));
			}
			
			model.put("jbdLayout", this.webJaguar.getSystemLayout("jbd-invoices", request.getHeader("host"), request));
		}
				
        return modelAndView;    	
    }
    
    public ModelAndView viewInvoice(HttpServletRequest request, HttpServletResponse response) { 
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		Map<String, Object> model = new HashMap<String, Object>();

		ModelAndView modelAndView = new ModelAndView("frontend/jbd/invoice", "model", model);
				
		Customer customer = (Customer) request.getAttribute("sessionCustomer");
		
		if (siteConfig.get("MAS200").getValue().trim().length() > 0) {
			String InvoiceNumber = ServletRequestUtils.getStringParameter(request, "id", "");
			
			Map<String, Object> order = getInvoice(InvoiceNumber);
			if (order != null && order.get("CustomerNumber").equals(customer.getAccountNumber())) {
				model.put("invoice", order);
				
				// ship to
				model.put("shipping", this.webJaguar.getAddress(customer.getId(), (String) order.get("ShipToCode")));
				
				// lineitems
	    		model.put("lineItems", getInvoiceLineItems(InvoiceNumber));				
	    		
	    		model.put("countries", this.webJaguar.getCountryMap());
	    		model.put("invoiceLayout", this.webJaguar.getSystemLayout("jbd-invoice", request.getHeader("host"), request));
			}
		}
		
        return modelAndView;    	
    }
    
    private List getOrders(String CustomerNumber) {
    	Object[] args = { CustomerNumber };
    	return jdbcTemplate.queryForList(
    			"SELECT " +
    			"    SalesOrderNumber " +
    			"  , OrderDate " +
    			"  , OrderStatus " +
    			"  , TaxableAmount " +
    			"  , NonTaxableAmount " +
    			"  , SUM(QuantityBackOrdered) as QuantityBackOrdered " +
    			"FROM " +
    			"  JBD_MASORDERHIST " +
    			"WHERE " +
    			"  CustomerNumber = ? " +
    			"GROUP BY " +
    			"  SalesOrderNumber " +
    			"ORDER BY " +
    			"  OrderDate DESC "
    		, args);
    }
    
    private Map getOrder(String SalesOrderNumber) {
    	Object[] args = { SalesOrderNumber };
    	try {
        	return jdbcTemplate.queryForMap(
        			"SELECT " +
        			"    SalesOrderNumber " +
        			"  , CustomerNumber " +
        			"  , ShipToCode " +
        			"  , OrderDate " +
        			"  , OrderStatus " +
        			"  , TaxableAmount " +
        			"  , NonTaxableAmount " +
        			"  , SUM(QuantityBackOrdered) as QuantityBackOrdered " +
        			"FROM " +
        			"  JBD_MASORDERHIST " +
        			"WHERE " +
        			"  SalesOrderNumber = ? " +
        			"GROUP BY " +
        			"  SalesOrderNumber "
        		, args);    		
    	} catch (Exception e) {
    		return null;
    	}
    }
    
    private List getOrderLineItems(String SalesOrderNumber) {
    	Object[] args = { SalesOrderNumber };
    	return jdbcTemplate.queryForList(
    			"SELECT " +
    			"    ItemNumber " +
    			"  , ItemDescription " +
    			"  , OriginalOrderQuantity " +
    			"  , QuantityShipped " +
    			"  , QuantityBackOrdered " +    			
    			"  , LastUnitPrice " +    			
    			"FROM " +
    			"  JBD_MASORDERHIST " +
    			"WHERE " +
    			"  SalesOrderNumber = ? "
    		, args);
    }
    
    private List getInvoices(String CustomerNumber, String SalesOrderNumber) {
    	Object[] args1 = { CustomerNumber };
    	Object[] args2 = { CustomerNumber, SalesOrderNumber };
    	StringBuffer sql = new StringBuffer(
    			"SELECT " +
    			"    InvoiceNumber " +
    			"  , InvoiceDate " +
    			"  , InvoiceType " +
    			"  , SalesOrderNumber " +
    			"  , TaxableAmount " +
    			"  , NonTaxableAmount " +
    			"FROM " +
    			"  MASINVHIST " +
    			"WHERE " +
    			"  CustomerNumber = ? "
    		);
    	if (SalesOrderNumber.length() > 0) {
        	sql.append(
        			" and SalesOrderNumber = ? "
        		);    		
    	}
    	sql.append(
    			"GROUP BY " +
    			"  InvoiceNumber " +
    			"ORDER BY " +
    			"  InvoiceDate DESC "
    		);
    	if (SalesOrderNumber.length() > 0) {
        	return jdbcTemplate.queryForList(sql.toString(), args2);
    	}

    	return jdbcTemplate.queryForList(sql.toString(), args1);
    }
    
    private Map getInvoice(String InvoiceNumber) {
    	Object[] args = { InvoiceNumber };
    	try {
        	return jdbcTemplate.queryForMap(
        			"SELECT " +
        			"    InvoiceNumber " +
        			"  , InvoiceType " +
        			"  , SalesOrderNumber " +
        			"  , CustomerNumber " +
        			"  , ShipToCode " +
        			"  , InvoiceDate " +
        			"  , TaxableAmount " +
        			"  , NonTaxableAmount " +
        			"FROM " +
        			"  MASINVHIST " +
        			"WHERE " +
        			"  InvoiceNumber = ? " +
        			"GROUP BY " +
        			"  InvoiceNumber "
        		, args);    		
    	} catch (Exception e) {
    		return null;
    	}
    }
    
    private List getInvoiceLineItems(String InvoiceNumber) {
    	Object[] args = { InvoiceNumber };
    	return jdbcTemplate.queryForList(
    			"SELECT " +
    			"    ItemNumber " +
    			"  , ItemDescription " +
    			"  , QuantityInvoiced " +
    			"  , QuantityShipped " +
    			"  , UnitPrice " +    			  			
    			"  , Extension " +    	
    			"FROM " +
    			"  MASINVHIST " +
    			"WHERE " +
    			"  InvoiceNumber = ? "
    		, args);
    }
    
    private List getTracking(String SalesOrderNumber) {
    	Object[] args = { SalesOrderNumber };
    	return jdbcTemplate.queryForList(
    			"SELECT " +
    			"    * " +    	
    			"FROM " +
    			"  MASTRACKING " +
    			"WHERE " +
    			"  SalesOrderNumber = ? "
    		, args);
    }    
}
