/**
 * PaymentMethodInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class PaymentMethodInput  implements java.io.Serializable {
    private com.webjaguar.thirdparty.eBizCharge.CreditCardInput creditCardInfo;

    private com.webjaguar.thirdparty.eBizCharge.CheckInput checkInfo;

    private java.lang.String methodID;

    private java.lang.String methodName;

    public PaymentMethodInput() {
    }

    public PaymentMethodInput(
           com.webjaguar.thirdparty.eBizCharge.CreditCardInput creditCardInfo,
           com.webjaguar.thirdparty.eBizCharge.CheckInput checkInfo,
           java.lang.String methodID,
           java.lang.String methodName) {
           this.creditCardInfo = creditCardInfo;
           this.checkInfo = checkInfo;
           this.methodID = methodID;
           this.methodName = methodName;
    }


    /**
     * Gets the creditCardInfo value for this PaymentMethodInput.
     * 
     * @return creditCardInfo
     */
    public com.webjaguar.thirdparty.eBizCharge.CreditCardInput getCreditCardInfo() {
        return creditCardInfo;
    }


    /**
     * Sets the creditCardInfo value for this PaymentMethodInput.
     * 
     * @param creditCardInfo
     */
    public void setCreditCardInfo(com.webjaguar.thirdparty.eBizCharge.CreditCardInput creditCardInfo) {
        this.creditCardInfo = creditCardInfo;
    }


    /**
     * Gets the checkInfo value for this PaymentMethodInput.
     * 
     * @return checkInfo
     */
    public com.webjaguar.thirdparty.eBizCharge.CheckInput getCheckInfo() {
        return checkInfo;
    }


    /**
     * Sets the checkInfo value for this PaymentMethodInput.
     * 
     * @param checkInfo
     */
    public void setCheckInfo(com.webjaguar.thirdparty.eBizCharge.CheckInput checkInfo) {
        this.checkInfo = checkInfo;
    }


    /**
     * Gets the methodID value for this PaymentMethodInput.
     * 
     * @return methodID
     */
    public java.lang.String getMethodID() {
        return methodID;
    }


    /**
     * Sets the methodID value for this PaymentMethodInput.
     * 
     * @param methodID
     */
    public void setMethodID(java.lang.String methodID) {
        this.methodID = methodID;
    }


    /**
     * Gets the methodName value for this PaymentMethodInput.
     * 
     * @return methodName
     */
    public java.lang.String getMethodName() {
        return methodName;
    }


    /**
     * Sets the methodName value for this PaymentMethodInput.
     * 
     * @param methodName
     */
    public void setMethodName(java.lang.String methodName) {
        this.methodName = methodName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentMethodInput)) return false;
        PaymentMethodInput other = (PaymentMethodInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.creditCardInfo==null && other.getCreditCardInfo()==null) || 
             (this.creditCardInfo!=null &&
              this.creditCardInfo.equals(other.getCreditCardInfo()))) &&
            ((this.checkInfo==null && other.getCheckInfo()==null) || 
             (this.checkInfo!=null &&
              this.checkInfo.equals(other.getCheckInfo()))) &&
            ((this.methodID==null && other.getMethodID()==null) || 
             (this.methodID!=null &&
              this.methodID.equals(other.getMethodID()))) &&
            ((this.methodName==null && other.getMethodName()==null) || 
             (this.methodName!=null &&
              this.methodName.equals(other.getMethodName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCreditCardInfo() != null) {
            _hashCode += getCreditCardInfo().hashCode();
        }
        if (getCheckInfo() != null) {
            _hashCode += getCheckInfo().hashCode();
        }
        if (getMethodID() != null) {
            _hashCode += getMethodID().hashCode();
        }
        if (getMethodName() != null) {
            _hashCode += getMethodName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentMethodInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PaymentMethodInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardInput"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checkInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CheckInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CheckInput"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("methodID");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MethodID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("methodName");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MethodName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
