/**
 * EBIZCHARGE_CUSTOMER_PROCESSINGSoapSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public class EBIZCHARGE_CUSTOMER_PROCESSINGSoapSkeleton implements com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoap, org.apache.axis.wsdl.Skeleton {
    private com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoap impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardInput"), com.webjaguar.thirdparty.eBizCharge.CreditCardInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CheckInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CheckInput"), com.webjaguar.thirdparty.eBizCharge.CheckInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHOD", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponse>BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHOD"));
        _oper.setSoapAction("BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHOD");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHOD") == null) {
            _myOperations.put("BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHOD", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHOD")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerInput"), com.webjaguar.thirdparty.eBizCharge.CustomerInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_ADD_CUSTOMER", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_ADD_CUSTOMERResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_ADD_CUSTOMERResponse>BANKCARD_ADD_CUSTOMERResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_ADD_CUSTOMER"));
        _oper.setSoapAction("BANKCARD_ADD_CUSTOMER");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_ADD_CUSTOMER") == null) {
            _myOperations.put("BANKCARD_ADD_CUSTOMER", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_ADD_CUSTOMER")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDBNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_GET_CUSTOMER", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_GET_CUSTOMERResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_GET_CUSTOMERResponse>BANKCARD_GET_CUSTOMERResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_GET_CUSTOMER"));
        _oper.setSoapAction("BANKCARD_GET_CUSTOMER");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_GET_CUSTOMER") == null) {
            _myOperations.put("BANKCARD_GET_CUSTOMER", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_GET_CUSTOMER")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDBNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_DEL_CUSTOMER", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_DEL_CUSTOMERResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_DEL_CUSTOMERResponse>BANKCARD_DEL_CUSTOMERResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_DEL_CUSTOMER"));
        _oper.setSoapAction("BANKCARD_DEL_CUSTOMER");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_DEL_CUSTOMER") == null) {
            _myOperations.put("BANKCARD_DEL_CUSTOMER", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_DEL_CUSTOMER")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDBNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardInput"), com.webjaguar.thirdparty.eBizCharge.CreditCardInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CheckInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CheckInput"), com.webjaguar.thirdparty.eBizCharge.CheckInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MethodName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponse>BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD"));
        _oper.setSoapAction("BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD") == null) {
            _myOperations.put("BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDBNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MethodID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResponse>BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD"));
        _oper.setSoapAction("BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD") == null) {
            _myOperations.put("BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInput"), com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_CUSTOMER_SALE", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_SALEResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_SALE"));
        _oper.setSoapAction("BANKCARD_CUSTOMER_SALE");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_CUSTOMER_SALE") == null) {
            _myOperations.put("BANKCARD_CUSTOMER_SALE", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_CUSTOMER_SALE")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInput"), com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_CUSTOMER_AUTHONLY", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_AUTHONLYResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_AUTHONLY"));
        _oper.setSoapAction("BANKCARD_CUSTOMER_AUTHONLY");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_CUSTOMER_AUTHONLY") == null) {
            _myOperations.put("BANKCARD_CUSTOMER_AUTHONLY", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_CUSTOMER_AUTHONLY")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInput"), com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_CUSTOMER_AUTHONLYL3", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_AUTHONLYL3Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_AUTHONLYL3"));
        _oper.setSoapAction("BANKCARD_CUSTOMER_AUTHONLYL3");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_CUSTOMER_AUTHONLYL3") == null) {
            _myOperations.put("BANKCARD_CUSTOMER_AUTHONLYL3", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_CUSTOMER_AUTHONLYL3")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInput"), com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_CUSTOMER_CREDIT_OR_REFUND", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_CREDIT_OR_REFUNDResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_CREDIT_OR_REFUND"));
        _oper.setSoapAction("BANKCARD_CUSTOMER_CREDIT_OR_REFUND");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_CUSTOMER_CREDIT_OR_REFUND") == null) {
            _myOperations.put("BANKCARD_CUSTOMER_CREDIT_OR_REFUND", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_CUSTOMER_CREDIT_OR_REFUND")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Amount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_CUSTOMER_CREDIT", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_CREDITResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_CREDIT"));
        _oper.setSoapAction("BANKCARD_CUSTOMER_CREDIT");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_CUSTOMER_CREDIT") == null) {
            _myOperations.put("BANKCARD_CUSTOMER_CREDIT", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_CUSTOMER_CREDIT")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_CUSTOMER_VOID", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_VOIDResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_VOID"));
        _oper.setSoapAction("BANKCARD_CUSTOMER_VOID");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_CUSTOMER_VOID") == null) {
            _myOperations.put("BANKCARD_CUSTOMER_VOID", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_CUSTOMER_VOID")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInput"), com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_CUSTOMER_PRIOR_AUTH_SALE", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_PRIOR_AUTH_SALEResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_PRIOR_AUTH_SALE"));
        _oper.setSoapAction("BANKCARD_CUSTOMER_PRIOR_AUTH_SALE");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_CUSTOMER_PRIOR_AUTH_SALE") == null) {
            _myOperations.put("BANKCARD_CUSTOMER_PRIOR_AUTH_SALE", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_CUSTOMER_PRIOR_AUTH_SALE")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("VERIFY_BANKCARD_CUSTOMER_SERVICE", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VERIFY_BANKCARD_CUSTOMER_SERVICEResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VERIFY_BANKCARD_CUSTOMER_SERVICEResponse>VERIFY_BANKCARD_CUSTOMER_SERVICEResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VERIFY_BANKCARD_CUSTOMER_SERVICE"));
        _oper.setSoapAction("VERIFY_BANKCARD_CUSTOMER_SERVICE");
        _myOperationsList.add(_oper);
        if (_myOperations.get("VERIFY_BANKCARD_CUSTOMER_SERVICE") == null) {
            _myOperations.put("VERIFY_BANKCARD_CUSTOMER_SERVICE", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("VERIFY_BANKCARD_CUSTOMER_SERVICE")).add(_oper);
    }

    public EBIZCHARGE_CUSTOMER_PROCESSINGSoapSkeleton() {
        this.impl = new com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoapImpl();
    }

    public EBIZCHARGE_CUSTOMER_PROCESSINGSoapSkeleton(com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoap impl) {
        this.impl = impl;
    }
    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHOD(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.CreditCardInput creditCardInfo, com.webjaguar.thirdparty.eBizCharge.CheckInput checkInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult ret = impl.BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHOD(merchantID, hashInfo, clientIP, creditCardInfo, checkInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMERResponseBANKCARD_ADD_CUSTOMERResult BANKCARD_ADD_CUSTOMER(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.CustomerInput customerInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMERResponseBANKCARD_ADD_CUSTOMERResult ret = impl.BANKCARD_ADD_CUSTOMER(merchantID, hashInfo, clientIP, customerInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult BANKCARD_GET_CUSTOMER(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String customerID, java.lang.String custDBNum) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult ret = impl.BANKCARD_GET_CUSTOMER(merchantID, hashInfo, clientIP, customerID, custDBNum);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMERResponseBANKCARD_DEL_CUSTOMERResult BANKCARD_DEL_CUSTOMER(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String custDBNum, java.lang.String customerID) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMERResponseBANKCARD_DEL_CUSTOMERResult ret = impl.BANKCARD_DEL_CUSTOMER(merchantID, hashInfo, clientIP, custDBNum, customerID);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String custDBNum, com.webjaguar.thirdparty.eBizCharge.CreditCardInput creditCardInfo, com.webjaguar.thirdparty.eBizCharge.CheckInput checkInfo, java.lang.String methodName) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult ret = impl.BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD(merchantID, hashInfo, clientIP, custDBNum, creditCardInfo, checkInfo, methodName);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResponseBANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String custDBNum, java.lang.String methodID) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResponseBANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult ret = impl.BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD(merchantID, hashInfo, clientIP, custDBNum, methodID);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput customerChargeInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_CUSTOMER_SALE(merchantID, hashInfo, clientIP, customerChargeInfo, itemDetailInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_AUTHONLY(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput customerChargeInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_CUSTOMER_AUTHONLY(merchantID, hashInfo, clientIP, customerChargeInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_AUTHONLYL3(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput customerChargeInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_CUSTOMER_AUTHONLYL3(merchantID, hashInfo, clientIP, customerChargeInfo, itemDetailInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_CREDIT_OR_REFUND(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput customerChargeInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_CUSTOMER_CREDIT_OR_REFUND(merchantID, hashInfo, clientIP, customerChargeInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_CREDIT(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.math.BigDecimal amount, java.lang.String referenceNumber) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_CUSTOMER_CREDIT(merchantID, hashInfo, clientIP, amount, referenceNumber);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_VOID(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_CUSTOMER_VOID(merchantID, hashInfo, clientIP, referenceNumber);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_PRIOR_AUTH_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber, com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput customerChargeInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_CUSTOMER_PRIOR_AUTH_SALE(merchantID, hashInfo, clientIP, referenceNumber, customerChargeInfo, itemDetailInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_CUSTOMER_SERVICEResponseVERIFY_BANKCARD_CUSTOMER_SERVICEResult VERIFY_BANKCARD_CUSTOMER_SERVICE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_CUSTOMER_SERVICEResponseVERIFY_BANKCARD_CUSTOMER_SERVICEResult ret = impl.VERIFY_BANKCARD_CUSTOMER_SERVICE(merchantID, hashInfo, clientIP);
        return ret;
    }

}
