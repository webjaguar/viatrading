/**
 * EBIZCHARGE_GIFTCARD_PROCESSINGSoapSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public class EBIZCHARGE_GIFTCARD_PROCESSINGSoapSkeleton implements com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_GIFTCARD_PROCESSINGSoap, org.apache.axis.wsdl.Skeleton {
    private com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_GIFTCARD_PROCESSINGSoap impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Program"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AccountDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AccountDetailInput"), com.webjaguar.thirdparty.eBizCharge.AccountDetailInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("GIFTCARD_ACTIVATION", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GIFTCARD_ACTIVATIONResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GiftCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GIFTCARD_ACTIVATION"));
        _oper.setSoapAction("GIFTCARD_ACTIVATION");
        _myOperationsList.add(_oper);
        if (_myOperations.get("GIFTCARD_ACTIVATION") == null) {
            _myOperations.put("GIFTCARD_ACTIVATION", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("GIFTCARD_ACTIVATION")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Program"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GCChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.GCChargeDetailInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("GIFTCARD_REDEMPTION", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GIFTCARD_REDEMPTIONResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GiftCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GIFTCARD_REDEMPTION"));
        _oper.setSoapAction("GIFTCARD_REDEMPTION");
        _myOperationsList.add(_oper);
        if (_myOperations.get("GIFTCARD_REDEMPTION") == null) {
            _myOperations.put("GIFTCARD_REDEMPTION", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("GIFTCARD_REDEMPTION")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Program"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GiftCardNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("GIFTCARD_VOID", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GIFTCARD_VOIDResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GiftCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GIFTCARD_VOID"));
        _oper.setSoapAction("GIFTCARD_VOID");
        _myOperationsList.add(_oper);
        if (_myOperations.get("GIFTCARD_VOID") == null) {
            _myOperations.put("GIFTCARD_VOID", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("GIFTCARD_VOID")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Program"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GiftCardNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("GIFTCARD_BALANCE", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GIFTCARD_BALANCEResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GiftCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GIFTCARD_BALANCE"));
        _oper.setSoapAction("GIFTCARD_BALANCE");
        _myOperationsList.add(_oper);
        if (_myOperations.get("GIFTCARD_BALANCE") == null) {
            _myOperations.put("GIFTCARD_BALANCE", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("GIFTCARD_BALANCE")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("VERIFY_GIFTCARD_SERVICE", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VERIFY_GIFTCARD_SERVICEResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VERIFY_GIFTCARD_SERVICEResponse>VERIFY_GIFTCARD_SERVICEResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VERIFY_GIFTCARD_SERVICE"));
        _oper.setSoapAction("VERIFY_GIFTCARD_SERVICE");
        _myOperationsList.add(_oper);
        if (_myOperations.get("VERIFY_GIFTCARD_SERVICE") == null) {
            _myOperations.put("VERIFY_GIFTCARD_SERVICE", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("VERIFY_GIFTCARD_SERVICE")).add(_oper);
    }

    public EBIZCHARGE_GIFTCARD_PROCESSINGSoapSkeleton() {
        this.impl = new com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_GIFTCARD_PROCESSINGSoapImpl();
    }

    public EBIZCHARGE_GIFTCARD_PROCESSINGSoapSkeleton(com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_GIFTCARD_PROCESSINGSoap impl) {
        this.impl = impl;
    }
    public com.webjaguar.thirdparty.eBizCharge.GiftCardResponse GIFTCARD_ACTIVATION(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String program, com.webjaguar.thirdparty.eBizCharge.AccountDetailInput accountDetailInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.GiftCardResponse ret = impl.GIFTCARD_ACTIVATION(merchantID, hashInfo, clientIP, program, accountDetailInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.GiftCardResponse GIFTCARD_REDEMPTION(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String program, com.webjaguar.thirdparty.eBizCharge.GCChargeDetailInput chargeDetailInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.GiftCardResponse ret = impl.GIFTCARD_REDEMPTION(merchantID, hashInfo, clientIP, program, chargeDetailInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.GiftCardResponse GIFTCARD_VOID(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String program, java.lang.String referenceNumber, java.lang.String giftCardNumber) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.GiftCardResponse ret = impl.GIFTCARD_VOID(merchantID, hashInfo, clientIP, program, referenceNumber, giftCardNumber);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.GiftCardResponse GIFTCARD_BALANCE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String program, java.lang.String giftCardNumber) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.GiftCardResponse ret = impl.GIFTCARD_BALANCE(merchantID, hashInfo, clientIP, program, giftCardNumber);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult VERIFY_GIFTCARD_SERVICE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult ret = impl.VERIFY_GIFTCARD_SERVICE(merchantID, hashInfo, clientIP);
        return ret;
    }

}
