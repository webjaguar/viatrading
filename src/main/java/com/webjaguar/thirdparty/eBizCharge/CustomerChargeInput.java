/**
 * CustomerChargeInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class CustomerChargeInput  implements java.io.Serializable {
    private java.lang.String paymentMethodID;

    private java.lang.String custDBNum;

    private java.lang.String cardCode;

    private java.math.BigDecimal amount;

    private java.math.BigDecimal taxAmount;

    private java.lang.String orderNumber;

    private java.lang.String description;

    private java.lang.String custReceipt;

    private java.lang.String custReceiptTemplate;

    private java.lang.String custReceiptEmail;

    public CustomerChargeInput() {
    }

    public CustomerChargeInput(
           java.lang.String paymentMethodID,
           java.lang.String custDBNum,
           java.lang.String cardCode,
           java.math.BigDecimal amount,
           java.math.BigDecimal taxAmount,
           java.lang.String orderNumber,
           java.lang.String description,
           java.lang.String custReceipt,
           java.lang.String custReceiptTemplate,
           java.lang.String custReceiptEmail) {
           this.paymentMethodID = paymentMethodID;
           this.custDBNum = custDBNum;
           this.cardCode = cardCode;
           this.amount = amount;
           this.taxAmount = taxAmount;
           this.orderNumber = orderNumber;
           this.description = description;
           this.custReceipt = custReceipt;
           this.custReceiptTemplate = custReceiptTemplate;
           this.custReceiptEmail = custReceiptEmail;
    }


    /**
     * Gets the paymentMethodID value for this CustomerChargeInput.
     * 
     * @return paymentMethodID
     */
    public java.lang.String getPaymentMethodID() {
        return paymentMethodID;
    }


    /**
     * Sets the paymentMethodID value for this CustomerChargeInput.
     * 
     * @param paymentMethodID
     */
    public void setPaymentMethodID(java.lang.String paymentMethodID) {
        this.paymentMethodID = paymentMethodID;
    }


    /**
     * Gets the custDBNum value for this CustomerChargeInput.
     * 
     * @return custDBNum
     */
    public java.lang.String getCustDBNum() {
        return custDBNum;
    }


    /**
     * Sets the custDBNum value for this CustomerChargeInput.
     * 
     * @param custDBNum
     */
    public void setCustDBNum(java.lang.String custDBNum) {
        this.custDBNum = custDBNum;
    }


    /**
     * Gets the cardCode value for this CustomerChargeInput.
     * 
     * @return cardCode
     */
    public java.lang.String getCardCode() {
        return cardCode;
    }


    /**
     * Sets the cardCode value for this CustomerChargeInput.
     * 
     * @param cardCode
     */
    public void setCardCode(java.lang.String cardCode) {
        this.cardCode = cardCode;
    }


    /**
     * Gets the amount value for this CustomerChargeInput.
     * 
     * @return amount
     */
    public java.math.BigDecimal getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this CustomerChargeInput.
     * 
     * @param amount
     */
    public void setAmount(java.math.BigDecimal amount) {
        this.amount = amount;
    }


    /**
     * Gets the taxAmount value for this CustomerChargeInput.
     * 
     * @return taxAmount
     */
    public java.math.BigDecimal getTaxAmount() {
        return taxAmount;
    }


    /**
     * Sets the taxAmount value for this CustomerChargeInput.
     * 
     * @param taxAmount
     */
    public void setTaxAmount(java.math.BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }


    /**
     * Gets the orderNumber value for this CustomerChargeInput.
     * 
     * @return orderNumber
     */
    public java.lang.String getOrderNumber() {
        return orderNumber;
    }


    /**
     * Sets the orderNumber value for this CustomerChargeInput.
     * 
     * @param orderNumber
     */
    public void setOrderNumber(java.lang.String orderNumber) {
        this.orderNumber = orderNumber;
    }


    /**
     * Gets the description value for this CustomerChargeInput.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this CustomerChargeInput.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the custReceipt value for this CustomerChargeInput.
     * 
     * @return custReceipt
     */
    public java.lang.String getCustReceipt() {
        return custReceipt;
    }


    /**
     * Sets the custReceipt value for this CustomerChargeInput.
     * 
     * @param custReceipt
     */
    public void setCustReceipt(java.lang.String custReceipt) {
        this.custReceipt = custReceipt;
    }


    /**
     * Gets the custReceiptTemplate value for this CustomerChargeInput.
     * 
     * @return custReceiptTemplate
     */
    public java.lang.String getCustReceiptTemplate() {
        return custReceiptTemplate;
    }


    /**
     * Sets the custReceiptTemplate value for this CustomerChargeInput.
     * 
     * @param custReceiptTemplate
     */
    public void setCustReceiptTemplate(java.lang.String custReceiptTemplate) {
        this.custReceiptTemplate = custReceiptTemplate;
    }


    /**
     * Gets the custReceiptEmail value for this CustomerChargeInput.
     * 
     * @return custReceiptEmail
     */
    public java.lang.String getCustReceiptEmail() {
        return custReceiptEmail;
    }


    /**
     * Sets the custReceiptEmail value for this CustomerChargeInput.
     * 
     * @param custReceiptEmail
     */
    public void setCustReceiptEmail(java.lang.String custReceiptEmail) {
        this.custReceiptEmail = custReceiptEmail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerChargeInput)) return false;
        CustomerChargeInput other = (CustomerChargeInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.paymentMethodID==null && other.getPaymentMethodID()==null) || 
             (this.paymentMethodID!=null &&
              this.paymentMethodID.equals(other.getPaymentMethodID()))) &&
            ((this.custDBNum==null && other.getCustDBNum()==null) || 
             (this.custDBNum!=null &&
              this.custDBNum.equals(other.getCustDBNum()))) &&
            ((this.cardCode==null && other.getCardCode()==null) || 
             (this.cardCode!=null &&
              this.cardCode.equals(other.getCardCode()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.taxAmount==null && other.getTaxAmount()==null) || 
             (this.taxAmount!=null &&
              this.taxAmount.equals(other.getTaxAmount()))) &&
            ((this.orderNumber==null && other.getOrderNumber()==null) || 
             (this.orderNumber!=null &&
              this.orderNumber.equals(other.getOrderNumber()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.custReceipt==null && other.getCustReceipt()==null) || 
             (this.custReceipt!=null &&
              this.custReceipt.equals(other.getCustReceipt()))) &&
            ((this.custReceiptTemplate==null && other.getCustReceiptTemplate()==null) || 
             (this.custReceiptTemplate!=null &&
              this.custReceiptTemplate.equals(other.getCustReceiptTemplate()))) &&
            ((this.custReceiptEmail==null && other.getCustReceiptEmail()==null) || 
             (this.custReceiptEmail!=null &&
              this.custReceiptEmail.equals(other.getCustReceiptEmail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPaymentMethodID() != null) {
            _hashCode += getPaymentMethodID().hashCode();
        }
        if (getCustDBNum() != null) {
            _hashCode += getCustDBNum().hashCode();
        }
        if (getCardCode() != null) {
            _hashCode += getCardCode().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getTaxAmount() != null) {
            _hashCode += getTaxAmount().hashCode();
        }
        if (getOrderNumber() != null) {
            _hashCode += getOrderNumber().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getCustReceipt() != null) {
            _hashCode += getCustReceipt().hashCode();
        }
        if (getCustReceiptTemplate() != null) {
            _hashCode += getCustReceiptTemplate().hashCode();
        }
        if (getCustReceiptEmail() != null) {
            _hashCode += getCustReceiptEmail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerChargeInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethodID");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PaymentMethodID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custDBNum");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDBNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardCode");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CardCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "TaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "OrderNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custReceipt");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustReceipt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custReceiptTemplate");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustReceiptTemplate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custReceiptEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustReceiptEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
