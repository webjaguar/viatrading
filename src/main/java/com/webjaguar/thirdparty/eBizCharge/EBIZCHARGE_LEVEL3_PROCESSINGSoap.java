/**
 * EBIZCHARGE_LEVEL3_PROCESSINGSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public interface EBIZCHARGE_LEVEL3_PROCESSINGSoap extends java.rmi.Remote {
    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_LEVEL3_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_LEVEL3_AUTHONLY(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_LEVEL3_PRIOR_AUTH_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_LEVEL3_PHONE_AUTH_POST(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String authCode, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_LEVEL3_CREDIT(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_LEVEL3_VOID(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_LEVEL3_SERVICEResponseVERIFY_BANKCARD_LEVEL3_SERVICEResult VERIFY_BANKCARD_LEVEL3_SERVICE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP) throws java.rmi.RemoteException;
}
