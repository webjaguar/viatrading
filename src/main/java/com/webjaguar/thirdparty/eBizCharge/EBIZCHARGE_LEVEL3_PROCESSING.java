/**
 * EBIZCHARGE_LEVEL3_PROCESSING.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public interface EBIZCHARGE_LEVEL3_PROCESSING extends javax.xml.rpc.Service {
    public java.lang.String getEBIZCHARGE_LEVEL3_PROCESSINGSoapAddress();

    public com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_LEVEL3_PROCESSINGSoap getEBIZCHARGE_LEVEL3_PROCESSINGSoap() throws javax.xml.rpc.ServiceException;

    public com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_LEVEL3_PROCESSINGSoap getEBIZCHARGE_LEVEL3_PROCESSINGSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
