/**
 * ChargeDetailInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class ChargeDetailInput  implements java.io.Serializable {
    private java.lang.String creditCardNumber;

    private java.lang.String magStripeData;

    private java.lang.String entryMode;

    private java.lang.String cardExpiration;

    private java.lang.String cardCode;

    private java.math.BigInteger checkNumber;

    private java.lang.String routing;

    private java.lang.String account;

    private java.lang.String accountType;

    private java.lang.String customerNumber;

    private java.math.BigDecimal amount;

    private java.math.BigDecimal shippingTotal;

    private java.lang.String shipFromZip;

    private java.math.BigDecimal taxAmount;

    private java.math.BigDecimal dutyAmount;

    private java.math.BigDecimal discountAmount;

    private java.lang.String orderNumber;

    private java.lang.String invoiceNumber;

    private java.lang.String PONumber;

    private java.lang.String AVSZip;

    private java.lang.String AVSStreetDigits;

    private java.lang.String description;

    private java.lang.String sigData;

    private java.lang.String custReceipt;

    private java.lang.String custReceiptTemplate;

    private java.lang.String isRecurring;

    private java.lang.String pares;

    private java.lang.Boolean verifyCardAuth;

    public ChargeDetailInput() {
    }

    public ChargeDetailInput(
           java.lang.String creditCardNumber,
           java.lang.String magStripeData,
           java.lang.String entryMode,
           java.lang.String cardExpiration,
           java.lang.String cardCode,
           java.math.BigInteger checkNumber,
           java.lang.String routing,
           java.lang.String account,
           java.lang.String accountType,
           java.lang.String customerNumber,
           java.math.BigDecimal amount,
           java.math.BigDecimal shippingTotal,
           java.lang.String shipFromZip,
           java.math.BigDecimal taxAmount,
           java.math.BigDecimal dutyAmount,
           java.math.BigDecimal discountAmount,
           java.lang.String orderNumber,
           java.lang.String invoiceNumber,
           java.lang.String PONumber,
           java.lang.String AVSZip,
           java.lang.String AVSStreetDigits,
           java.lang.String description,
           java.lang.String sigData,
           java.lang.String custReceipt,
           java.lang.String custReceiptTemplate,
           java.lang.String isRecurring,
           java.lang.String pares,
           java.lang.Boolean verifyCardAuth) {
           this.creditCardNumber = creditCardNumber;
           this.magStripeData = magStripeData;
           this.entryMode = entryMode;
           this.cardExpiration = cardExpiration;
           this.cardCode = cardCode;
           this.checkNumber = checkNumber;
           this.routing = routing;
           this.account = account;
           this.accountType = accountType;
           this.customerNumber = customerNumber;
           this.amount = amount;
           this.shippingTotal = shippingTotal;
           this.shipFromZip = shipFromZip;
           this.taxAmount = taxAmount;
           this.dutyAmount = dutyAmount;
           this.discountAmount = discountAmount;
           this.orderNumber = orderNumber;
           this.invoiceNumber = invoiceNumber;
           this.PONumber = PONumber;
           this.AVSZip = AVSZip;
           this.AVSStreetDigits = AVSStreetDigits;
           this.description = description;
           this.sigData = sigData;
           this.custReceipt = custReceipt;
           this.custReceiptTemplate = custReceiptTemplate;
           this.isRecurring = isRecurring;
           this.pares = pares;
           this.verifyCardAuth = verifyCardAuth;
    }


    /**
     * Gets the creditCardNumber value for this ChargeDetailInput.
     * 
     * @return creditCardNumber
     */
    public java.lang.String getCreditCardNumber() {
        return creditCardNumber;
    }


    /**
     * Sets the creditCardNumber value for this ChargeDetailInput.
     * 
     * @param creditCardNumber
     */
    public void setCreditCardNumber(java.lang.String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }


    /**
     * Gets the magStripeData value for this ChargeDetailInput.
     * 
     * @return magStripeData
     */
    public java.lang.String getMagStripeData() {
        return magStripeData;
    }


    /**
     * Sets the magStripeData value for this ChargeDetailInput.
     * 
     * @param magStripeData
     */
    public void setMagStripeData(java.lang.String magStripeData) {
        this.magStripeData = magStripeData;
    }


    /**
     * Gets the entryMode value for this ChargeDetailInput.
     * 
     * @return entryMode
     */
    public java.lang.String getEntryMode() {
        return entryMode;
    }


    /**
     * Sets the entryMode value for this ChargeDetailInput.
     * 
     * @param entryMode
     */
    public void setEntryMode(java.lang.String entryMode) {
        this.entryMode = entryMode;
    }


    /**
     * Gets the cardExpiration value for this ChargeDetailInput.
     * 
     * @return cardExpiration
     */
    public java.lang.String getCardExpiration() {
        return cardExpiration;
    }


    /**
     * Sets the cardExpiration value for this ChargeDetailInput.
     * 
     * @param cardExpiration
     */
    public void setCardExpiration(java.lang.String cardExpiration) {
        this.cardExpiration = cardExpiration;
    }


    /**
     * Gets the cardCode value for this ChargeDetailInput.
     * 
     * @return cardCode
     */
    public java.lang.String getCardCode() {
        return cardCode;
    }


    /**
     * Sets the cardCode value for this ChargeDetailInput.
     * 
     * @param cardCode
     */
    public void setCardCode(java.lang.String cardCode) {
        this.cardCode = cardCode;
    }


    /**
     * Gets the checkNumber value for this ChargeDetailInput.
     * 
     * @return checkNumber
     */
    public java.math.BigInteger getCheckNumber() {
        return checkNumber;
    }


    /**
     * Sets the checkNumber value for this ChargeDetailInput.
     * 
     * @param checkNumber
     */
    public void setCheckNumber(java.math.BigInteger checkNumber) {
        this.checkNumber = checkNumber;
    }


    /**
     * Gets the routing value for this ChargeDetailInput.
     * 
     * @return routing
     */
    public java.lang.String getRouting() {
        return routing;
    }


    /**
     * Sets the routing value for this ChargeDetailInput.
     * 
     * @param routing
     */
    public void setRouting(java.lang.String routing) {
        this.routing = routing;
    }


    /**
     * Gets the account value for this ChargeDetailInput.
     * 
     * @return account
     */
    public java.lang.String getAccount() {
        return account;
    }


    /**
     * Sets the account value for this ChargeDetailInput.
     * 
     * @param account
     */
    public void setAccount(java.lang.String account) {
        this.account = account;
    }


    /**
     * Gets the accountType value for this ChargeDetailInput.
     * 
     * @return accountType
     */
    public java.lang.String getAccountType() {
        return accountType;
    }


    /**
     * Sets the accountType value for this ChargeDetailInput.
     * 
     * @param accountType
     */
    public void setAccountType(java.lang.String accountType) {
        this.accountType = accountType;
    }


    /**
     * Gets the customerNumber value for this ChargeDetailInput.
     * 
     * @return customerNumber
     */
    public java.lang.String getCustomerNumber() {
        return customerNumber;
    }


    /**
     * Sets the customerNumber value for this ChargeDetailInput.
     * 
     * @param customerNumber
     */
    public void setCustomerNumber(java.lang.String customerNumber) {
        this.customerNumber = customerNumber;
    }


    /**
     * Gets the amount value for this ChargeDetailInput.
     * 
     * @return amount
     */
    public java.math.BigDecimal getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this ChargeDetailInput.
     * 
     * @param amount
     */
    public void setAmount(java.math.BigDecimal amount) {
        this.amount = amount;
    }


    /**
     * Gets the shippingTotal value for this ChargeDetailInput.
     * 
     * @return shippingTotal
     */
    public java.math.BigDecimal getShippingTotal() {
        return shippingTotal;
    }


    /**
     * Sets the shippingTotal value for this ChargeDetailInput.
     * 
     * @param shippingTotal
     */
    public void setShippingTotal(java.math.BigDecimal shippingTotal) {
        this.shippingTotal = shippingTotal;
    }


    /**
     * Gets the shipFromZip value for this ChargeDetailInput.
     * 
     * @return shipFromZip
     */
    public java.lang.String getShipFromZip() {
        return shipFromZip;
    }


    /**
     * Sets the shipFromZip value for this ChargeDetailInput.
     * 
     * @param shipFromZip
     */
    public void setShipFromZip(java.lang.String shipFromZip) {
        this.shipFromZip = shipFromZip;
    }


    /**
     * Gets the taxAmount value for this ChargeDetailInput.
     * 
     * @return taxAmount
     */
    public java.math.BigDecimal getTaxAmount() {
        return taxAmount;
    }


    /**
     * Sets the taxAmount value for this ChargeDetailInput.
     * 
     * @param taxAmount
     */
    public void setTaxAmount(java.math.BigDecimal taxAmount) {
        this.taxAmount = taxAmount;
    }


    /**
     * Gets the dutyAmount value for this ChargeDetailInput.
     * 
     * @return dutyAmount
     */
    public java.math.BigDecimal getDutyAmount() {
        return dutyAmount;
    }


    /**
     * Sets the dutyAmount value for this ChargeDetailInput.
     * 
     * @param dutyAmount
     */
    public void setDutyAmount(java.math.BigDecimal dutyAmount) {
        this.dutyAmount = dutyAmount;
    }


    /**
     * Gets the discountAmount value for this ChargeDetailInput.
     * 
     * @return discountAmount
     */
    public java.math.BigDecimal getDiscountAmount() {
        return discountAmount;
    }


    /**
     * Sets the discountAmount value for this ChargeDetailInput.
     * 
     * @param discountAmount
     */
    public void setDiscountAmount(java.math.BigDecimal discountAmount) {
        this.discountAmount = discountAmount;
    }


    /**
     * Gets the orderNumber value for this ChargeDetailInput.
     * 
     * @return orderNumber
     */
    public java.lang.String getOrderNumber() {
        return orderNumber;
    }


    /**
     * Sets the orderNumber value for this ChargeDetailInput.
     * 
     * @param orderNumber
     */
    public void setOrderNumber(java.lang.String orderNumber) {
        this.orderNumber = orderNumber;
    }


    /**
     * Gets the invoiceNumber value for this ChargeDetailInput.
     * 
     * @return invoiceNumber
     */
    public java.lang.String getInvoiceNumber() {
        return invoiceNumber;
    }


    /**
     * Sets the invoiceNumber value for this ChargeDetailInput.
     * 
     * @param invoiceNumber
     */
    public void setInvoiceNumber(java.lang.String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }


    /**
     * Gets the PONumber value for this ChargeDetailInput.
     * 
     * @return PONumber
     */
    public java.lang.String getPONumber() {
        return PONumber;
    }


    /**
     * Sets the PONumber value for this ChargeDetailInput.
     * 
     * @param PONumber
     */
    public void setPONumber(java.lang.String PONumber) {
        this.PONumber = PONumber;
    }


    /**
     * Gets the AVSZip value for this ChargeDetailInput.
     * 
     * @return AVSZip
     */
    public java.lang.String getAVSZip() {
        return AVSZip;
    }


    /**
     * Sets the AVSZip value for this ChargeDetailInput.
     * 
     * @param AVSZip
     */
    public void setAVSZip(java.lang.String AVSZip) {
        this.AVSZip = AVSZip;
    }


    /**
     * Gets the AVSStreetDigits value for this ChargeDetailInput.
     * 
     * @return AVSStreetDigits
     */
    public java.lang.String getAVSStreetDigits() {
        return AVSStreetDigits;
    }


    /**
     * Sets the AVSStreetDigits value for this ChargeDetailInput.
     * 
     * @param AVSStreetDigits
     */
    public void setAVSStreetDigits(java.lang.String AVSStreetDigits) {
        this.AVSStreetDigits = AVSStreetDigits;
    }


    /**
     * Gets the description value for this ChargeDetailInput.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this ChargeDetailInput.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the sigData value for this ChargeDetailInput.
     * 
     * @return sigData
     */
    public java.lang.String getSigData() {
        return sigData;
    }


    /**
     * Sets the sigData value for this ChargeDetailInput.
     * 
     * @param sigData
     */
    public void setSigData(java.lang.String sigData) {
        this.sigData = sigData;
    }


    /**
     * Gets the custReceipt value for this ChargeDetailInput.
     * 
     * @return custReceipt
     */
    public java.lang.String getCustReceipt() {
        return custReceipt;
    }


    /**
     * Sets the custReceipt value for this ChargeDetailInput.
     * 
     * @param custReceipt
     */
    public void setCustReceipt(java.lang.String custReceipt) {
        this.custReceipt = custReceipt;
    }


    /**
     * Gets the custReceiptTemplate value for this ChargeDetailInput.
     * 
     * @return custReceiptTemplate
     */
    public java.lang.String getCustReceiptTemplate() {
        return custReceiptTemplate;
    }


    /**
     * Sets the custReceiptTemplate value for this ChargeDetailInput.
     * 
     * @param custReceiptTemplate
     */
    public void setCustReceiptTemplate(java.lang.String custReceiptTemplate) {
        this.custReceiptTemplate = custReceiptTemplate;
    }


    /**
     * Gets the isRecurring value for this ChargeDetailInput.
     * 
     * @return isRecurring
     */
    public java.lang.String getIsRecurring() {
        return isRecurring;
    }


    /**
     * Sets the isRecurring value for this ChargeDetailInput.
     * 
     * @param isRecurring
     */
    public void setIsRecurring(java.lang.String isRecurring) {
        this.isRecurring = isRecurring;
    }


    /**
     * Gets the pares value for this ChargeDetailInput.
     * 
     * @return pares
     */
    public java.lang.String getPares() {
        return pares;
    }


    /**
     * Sets the pares value for this ChargeDetailInput.
     * 
     * @param pares
     */
    public void setPares(java.lang.String pares) {
        this.pares = pares;
    }


    /**
     * Gets the verifyCardAuth value for this ChargeDetailInput.
     * 
     * @return verifyCardAuth
     */
    public java.lang.Boolean getVerifyCardAuth() {
        return verifyCardAuth;
    }


    /**
     * Sets the verifyCardAuth value for this ChargeDetailInput.
     * 
     * @param verifyCardAuth
     */
    public void setVerifyCardAuth(java.lang.Boolean verifyCardAuth) {
        this.verifyCardAuth = verifyCardAuth;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ChargeDetailInput)) return false;
        ChargeDetailInput other = (ChargeDetailInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.creditCardNumber==null && other.getCreditCardNumber()==null) || 
             (this.creditCardNumber!=null &&
              this.creditCardNumber.equals(other.getCreditCardNumber()))) &&
            ((this.magStripeData==null && other.getMagStripeData()==null) || 
             (this.magStripeData!=null &&
              this.magStripeData.equals(other.getMagStripeData()))) &&
            ((this.entryMode==null && other.getEntryMode()==null) || 
             (this.entryMode!=null &&
              this.entryMode.equals(other.getEntryMode()))) &&
            ((this.cardExpiration==null && other.getCardExpiration()==null) || 
             (this.cardExpiration!=null &&
              this.cardExpiration.equals(other.getCardExpiration()))) &&
            ((this.cardCode==null && other.getCardCode()==null) || 
             (this.cardCode!=null &&
              this.cardCode.equals(other.getCardCode()))) &&
            ((this.checkNumber==null && other.getCheckNumber()==null) || 
             (this.checkNumber!=null &&
              this.checkNumber.equals(other.getCheckNumber()))) &&
            ((this.routing==null && other.getRouting()==null) || 
             (this.routing!=null &&
              this.routing.equals(other.getRouting()))) &&
            ((this.account==null && other.getAccount()==null) || 
             (this.account!=null &&
              this.account.equals(other.getAccount()))) &&
            ((this.accountType==null && other.getAccountType()==null) || 
             (this.accountType!=null &&
              this.accountType.equals(other.getAccountType()))) &&
            ((this.customerNumber==null && other.getCustomerNumber()==null) || 
             (this.customerNumber!=null &&
              this.customerNumber.equals(other.getCustomerNumber()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.shippingTotal==null && other.getShippingTotal()==null) || 
             (this.shippingTotal!=null &&
              this.shippingTotal.equals(other.getShippingTotal()))) &&
            ((this.shipFromZip==null && other.getShipFromZip()==null) || 
             (this.shipFromZip!=null &&
              this.shipFromZip.equals(other.getShipFromZip()))) &&
            ((this.taxAmount==null && other.getTaxAmount()==null) || 
             (this.taxAmount!=null &&
              this.taxAmount.equals(other.getTaxAmount()))) &&
            ((this.dutyAmount==null && other.getDutyAmount()==null) || 
             (this.dutyAmount!=null &&
              this.dutyAmount.equals(other.getDutyAmount()))) &&
            ((this.discountAmount==null && other.getDiscountAmount()==null) || 
             (this.discountAmount!=null &&
              this.discountAmount.equals(other.getDiscountAmount()))) &&
            ((this.orderNumber==null && other.getOrderNumber()==null) || 
             (this.orderNumber!=null &&
              this.orderNumber.equals(other.getOrderNumber()))) &&
            ((this.invoiceNumber==null && other.getInvoiceNumber()==null) || 
             (this.invoiceNumber!=null &&
              this.invoiceNumber.equals(other.getInvoiceNumber()))) &&
            ((this.PONumber==null && other.getPONumber()==null) || 
             (this.PONumber!=null &&
              this.PONumber.equals(other.getPONumber()))) &&
            ((this.AVSZip==null && other.getAVSZip()==null) || 
             (this.AVSZip!=null &&
              this.AVSZip.equals(other.getAVSZip()))) &&
            ((this.AVSStreetDigits==null && other.getAVSStreetDigits()==null) || 
             (this.AVSStreetDigits!=null &&
              this.AVSStreetDigits.equals(other.getAVSStreetDigits()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.sigData==null && other.getSigData()==null) || 
             (this.sigData!=null &&
              this.sigData.equals(other.getSigData()))) &&
            ((this.custReceipt==null && other.getCustReceipt()==null) || 
             (this.custReceipt!=null &&
              this.custReceipt.equals(other.getCustReceipt()))) &&
            ((this.custReceiptTemplate==null && other.getCustReceiptTemplate()==null) || 
             (this.custReceiptTemplate!=null &&
              this.custReceiptTemplate.equals(other.getCustReceiptTemplate()))) &&
            ((this.isRecurring==null && other.getIsRecurring()==null) || 
             (this.isRecurring!=null &&
              this.isRecurring.equals(other.getIsRecurring()))) &&
            ((this.pares==null && other.getPares()==null) || 
             (this.pares!=null &&
              this.pares.equals(other.getPares()))) &&
            ((this.verifyCardAuth==null && other.getVerifyCardAuth()==null) || 
             (this.verifyCardAuth!=null &&
              this.verifyCardAuth.equals(other.getVerifyCardAuth())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCreditCardNumber() != null) {
            _hashCode += getCreditCardNumber().hashCode();
        }
        if (getMagStripeData() != null) {
            _hashCode += getMagStripeData().hashCode();
        }
        if (getEntryMode() != null) {
            _hashCode += getEntryMode().hashCode();
        }
        if (getCardExpiration() != null) {
            _hashCode += getCardExpiration().hashCode();
        }
        if (getCardCode() != null) {
            _hashCode += getCardCode().hashCode();
        }
        if (getCheckNumber() != null) {
            _hashCode += getCheckNumber().hashCode();
        }
        if (getRouting() != null) {
            _hashCode += getRouting().hashCode();
        }
        if (getAccount() != null) {
            _hashCode += getAccount().hashCode();
        }
        if (getAccountType() != null) {
            _hashCode += getAccountType().hashCode();
        }
        if (getCustomerNumber() != null) {
            _hashCode += getCustomerNumber().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getShippingTotal() != null) {
            _hashCode += getShippingTotal().hashCode();
        }
        if (getShipFromZip() != null) {
            _hashCode += getShipFromZip().hashCode();
        }
        if (getTaxAmount() != null) {
            _hashCode += getTaxAmount().hashCode();
        }
        if (getDutyAmount() != null) {
            _hashCode += getDutyAmount().hashCode();
        }
        if (getDiscountAmount() != null) {
            _hashCode += getDiscountAmount().hashCode();
        }
        if (getOrderNumber() != null) {
            _hashCode += getOrderNumber().hashCode();
        }
        if (getInvoiceNumber() != null) {
            _hashCode += getInvoiceNumber().hashCode();
        }
        if (getPONumber() != null) {
            _hashCode += getPONumber().hashCode();
        }
        if (getAVSZip() != null) {
            _hashCode += getAVSZip().hashCode();
        }
        if (getAVSStreetDigits() != null) {
            _hashCode += getAVSStreetDigits().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getSigData() != null) {
            _hashCode += getSigData().hashCode();
        }
        if (getCustReceipt() != null) {
            _hashCode += getCustReceipt().hashCode();
        }
        if (getCustReceiptTemplate() != null) {
            _hashCode += getCustReceiptTemplate().hashCode();
        }
        if (getIsRecurring() != null) {
            _hashCode += getIsRecurring().hashCode();
        }
        if (getPares() != null) {
            _hashCode += getPares().hashCode();
        }
        if (getVerifyCardAuth() != null) {
            _hashCode += getVerifyCardAuth().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ChargeDetailInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("magStripeData");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MagStripeData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "EntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardExpiration");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CardExpiration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardCode");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CardCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("checkNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CheckNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("routing");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Routing"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("account");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Account"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountType");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AccountType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shippingTotal");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingTotal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipFromZip");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShipFromZip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taxAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "TaxAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dutyAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "DutyAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("discountAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "DiscountAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "OrderNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("invoiceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "InvoiceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PONumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PONumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AVSZip");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AVSZip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AVSStreetDigits");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AVSStreetDigits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sigData");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "SigData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custReceipt");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustReceipt"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custReceiptTemplate");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustReceiptTemplate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isRecurring");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "IsRecurring"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pares");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Pares"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("verifyCardAuth");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VerifyCardAuth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
