/**
 * FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult  implements java.io.Serializable {
    private java.lang.String authorizationCode;

    private java.lang.String avsCode;

    private java.lang.String OFPLanguage;

    private java.lang.String responseText;

    private java.lang.String status;

    private java.lang.String transactionId;

    public FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult() {
    }

    public FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult(
           java.lang.String authorizationCode,
           java.lang.String avsCode,
           java.lang.String OFPLanguage,
           java.lang.String responseText,
           java.lang.String status,
           java.lang.String transactionId) {
           this.authorizationCode = authorizationCode;
           this.avsCode = avsCode;
           this.OFPLanguage = OFPLanguage;
           this.responseText = responseText;
           this.status = status;
           this.transactionId = transactionId;
    }


    /**
     * Gets the authorizationCode value for this FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.
     * 
     * @return authorizationCode
     */
    public java.lang.String getAuthorizationCode() {
        return authorizationCode;
    }


    /**
     * Sets the authorizationCode value for this FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.
     * 
     * @param authorizationCode
     */
    public void setAuthorizationCode(java.lang.String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }


    /**
     * Gets the avsCode value for this FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.
     * 
     * @return avsCode
     */
    public java.lang.String getAvsCode() {
        return avsCode;
    }


    /**
     * Sets the avsCode value for this FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.
     * 
     * @param avsCode
     */
    public void setAvsCode(java.lang.String avsCode) {
        this.avsCode = avsCode;
    }


    /**
     * Gets the OFPLanguage value for this FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.
     * 
     * @return OFPLanguage
     */
    public java.lang.String getOFPLanguage() {
        return OFPLanguage;
    }


    /**
     * Sets the OFPLanguage value for this FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.
     * 
     * @param OFPLanguage
     */
    public void setOFPLanguage(java.lang.String OFPLanguage) {
        this.OFPLanguage = OFPLanguage;
    }


    /**
     * Gets the responseText value for this FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.
     * 
     * @return responseText
     */
    public java.lang.String getResponseText() {
        return responseText;
    }


    /**
     * Sets the responseText value for this FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.
     * 
     * @param responseText
     */
    public void setResponseText(java.lang.String responseText) {
        this.responseText = responseText;
    }


    /**
     * Gets the status value for this FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the transactionId value for this FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.
     * 
     * @return transactionId
     */
    public java.lang.String getTransactionId() {
        return transactionId;
    }


    /**
     * Sets the transactionId value for this FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.
     * 
     * @param transactionId
     */
    public void setTransactionId(java.lang.String transactionId) {
        this.transactionId = transactionId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult)) return false;
        FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult other = (FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.authorizationCode==null && other.getAuthorizationCode()==null) || 
             (this.authorizationCode!=null &&
              this.authorizationCode.equals(other.getAuthorizationCode()))) &&
            ((this.avsCode==null && other.getAvsCode()==null) || 
             (this.avsCode!=null &&
              this.avsCode.equals(other.getAvsCode()))) &&
            ((this.OFPLanguage==null && other.getOFPLanguage()==null) || 
             (this.OFPLanguage!=null &&
              this.OFPLanguage.equals(other.getOFPLanguage()))) &&
            ((this.responseText==null && other.getResponseText()==null) || 
             (this.responseText!=null &&
              this.responseText.equals(other.getResponseText()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.transactionId==null && other.getTransactionId()==null) || 
             (this.transactionId!=null &&
              this.transactionId.equals(other.getTransactionId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAuthorizationCode() != null) {
            _hashCode += getAuthorizationCode().hashCode();
        }
        if (getAvsCode() != null) {
            _hashCode += getAvsCode().hashCode();
        }
        if (getOFPLanguage() != null) {
            _hashCode += getOFPLanguage().hashCode();
        }
        if (getResponseText() != null) {
            _hashCode += getResponseText().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getTransactionId() != null) {
            _hashCode += getTransactionId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>FINANCE_AUTHORIZATIONResponse>FINANCE_AUTHORIZATIONResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AuthorizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("avsCode");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AvsCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OFPLanguage");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "OFPLanguage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseText");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ResponseText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionId");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "TransactionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
