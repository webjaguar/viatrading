/**
 * VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult  implements java.io.Serializable {
    private java.lang.String result;

    private java.lang.String error;

    public VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult() {
    }

    public VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult(
           java.lang.String result,
           java.lang.String error) {
           this.result = result;
           this.error = error;
    }


    /**
     * Gets the result value for this VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult.
     * 
     * @return result
     */
    public java.lang.String getResult() {
        return result;
    }


    /**
     * Sets the result value for this VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult.
     * 
     * @param result
     */
    public void setResult(java.lang.String result) {
        this.result = result;
    }


    /**
     * Gets the error value for this VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult)) return false;
        VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult other = (VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.result==null && other.getResult()==null) || 
             (this.result!=null &&
              this.result.equals(other.getResult()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResult() != null) {
            _hashCode += getResult().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VERIFY_GIFTCARD_SERVICEResponse>VERIFY_GIFTCARD_SERVICEResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
