/**
 * CreditCardInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class CreditCardInput  implements java.io.Serializable {
    private java.lang.String creditCardNumber;

    private java.lang.String magStripeData;

    private java.lang.String entryMode;

    private java.lang.String cardExpiration;

    private java.lang.String cardCode;

    private java.lang.String AVSZip;

    private java.lang.String AVSStreetDigits;

    private java.lang.String pares;

    private java.lang.Boolean verifyCardAuth;

    public CreditCardInput() {
    }

    public CreditCardInput(
           java.lang.String creditCardNumber,
           java.lang.String magStripeData,
           java.lang.String entryMode,
           java.lang.String cardExpiration,
           java.lang.String cardCode,
           java.lang.String AVSZip,
           java.lang.String AVSStreetDigits,
           java.lang.String pares,
           java.lang.Boolean verifyCardAuth) {
           this.creditCardNumber = creditCardNumber;
           this.magStripeData = magStripeData;
           this.entryMode = entryMode;
           this.cardExpiration = cardExpiration;
           this.cardCode = cardCode;
           this.AVSZip = AVSZip;
           this.AVSStreetDigits = AVSStreetDigits;
           this.pares = pares;
           this.verifyCardAuth = verifyCardAuth;
    }


    /**
     * Gets the creditCardNumber value for this CreditCardInput.
     * 
     * @return creditCardNumber
     */
    public java.lang.String getCreditCardNumber() {
        return creditCardNumber;
    }


    /**
     * Sets the creditCardNumber value for this CreditCardInput.
     * 
     * @param creditCardNumber
     */
    public void setCreditCardNumber(java.lang.String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }


    /**
     * Gets the magStripeData value for this CreditCardInput.
     * 
     * @return magStripeData
     */
    public java.lang.String getMagStripeData() {
        return magStripeData;
    }


    /**
     * Sets the magStripeData value for this CreditCardInput.
     * 
     * @param magStripeData
     */
    public void setMagStripeData(java.lang.String magStripeData) {
        this.magStripeData = magStripeData;
    }


    /**
     * Gets the entryMode value for this CreditCardInput.
     * 
     * @return entryMode
     */
    public java.lang.String getEntryMode() {
        return entryMode;
    }


    /**
     * Sets the entryMode value for this CreditCardInput.
     * 
     * @param entryMode
     */
    public void setEntryMode(java.lang.String entryMode) {
        this.entryMode = entryMode;
    }


    /**
     * Gets the cardExpiration value for this CreditCardInput.
     * 
     * @return cardExpiration
     */
    public java.lang.String getCardExpiration() {
        return cardExpiration;
    }


    /**
     * Sets the cardExpiration value for this CreditCardInput.
     * 
     * @param cardExpiration
     */
    public void setCardExpiration(java.lang.String cardExpiration) {
        this.cardExpiration = cardExpiration;
    }


    /**
     * Gets the cardCode value for this CreditCardInput.
     * 
     * @return cardCode
     */
    public java.lang.String getCardCode() {
        return cardCode;
    }


    /**
     * Sets the cardCode value for this CreditCardInput.
     * 
     * @param cardCode
     */
    public void setCardCode(java.lang.String cardCode) {
        this.cardCode = cardCode;
    }


    /**
     * Gets the AVSZip value for this CreditCardInput.
     * 
     * @return AVSZip
     */
    public java.lang.String getAVSZip() {
        return AVSZip;
    }


    /**
     * Sets the AVSZip value for this CreditCardInput.
     * 
     * @param AVSZip
     */
    public void setAVSZip(java.lang.String AVSZip) {
        this.AVSZip = AVSZip;
    }


    /**
     * Gets the AVSStreetDigits value for this CreditCardInput.
     * 
     * @return AVSStreetDigits
     */
    public java.lang.String getAVSStreetDigits() {
        return AVSStreetDigits;
    }


    /**
     * Sets the AVSStreetDigits value for this CreditCardInput.
     * 
     * @param AVSStreetDigits
     */
    public void setAVSStreetDigits(java.lang.String AVSStreetDigits) {
        this.AVSStreetDigits = AVSStreetDigits;
    }


    /**
     * Gets the pares value for this CreditCardInput.
     * 
     * @return pares
     */
    public java.lang.String getPares() {
        return pares;
    }


    /**
     * Sets the pares value for this CreditCardInput.
     * 
     * @param pares
     */
    public void setPares(java.lang.String pares) {
        this.pares = pares;
    }


    /**
     * Gets the verifyCardAuth value for this CreditCardInput.
     * 
     * @return verifyCardAuth
     */
    public java.lang.Boolean getVerifyCardAuth() {
        return verifyCardAuth;
    }


    /**
     * Sets the verifyCardAuth value for this CreditCardInput.
     * 
     * @param verifyCardAuth
     */
    public void setVerifyCardAuth(java.lang.Boolean verifyCardAuth) {
        this.verifyCardAuth = verifyCardAuth;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreditCardInput)) return false;
        CreditCardInput other = (CreditCardInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.creditCardNumber==null && other.getCreditCardNumber()==null) || 
             (this.creditCardNumber!=null &&
              this.creditCardNumber.equals(other.getCreditCardNumber()))) &&
            ((this.magStripeData==null && other.getMagStripeData()==null) || 
             (this.magStripeData!=null &&
              this.magStripeData.equals(other.getMagStripeData()))) &&
            ((this.entryMode==null && other.getEntryMode()==null) || 
             (this.entryMode!=null &&
              this.entryMode.equals(other.getEntryMode()))) &&
            ((this.cardExpiration==null && other.getCardExpiration()==null) || 
             (this.cardExpiration!=null &&
              this.cardExpiration.equals(other.getCardExpiration()))) &&
            ((this.cardCode==null && other.getCardCode()==null) || 
             (this.cardCode!=null &&
              this.cardCode.equals(other.getCardCode()))) &&
            ((this.AVSZip==null && other.getAVSZip()==null) || 
             (this.AVSZip!=null &&
              this.AVSZip.equals(other.getAVSZip()))) &&
            ((this.AVSStreetDigits==null && other.getAVSStreetDigits()==null) || 
             (this.AVSStreetDigits!=null &&
              this.AVSStreetDigits.equals(other.getAVSStreetDigits()))) &&
            ((this.pares==null && other.getPares()==null) || 
             (this.pares!=null &&
              this.pares.equals(other.getPares()))) &&
            ((this.verifyCardAuth==null && other.getVerifyCardAuth()==null) || 
             (this.verifyCardAuth!=null &&
              this.verifyCardAuth.equals(other.getVerifyCardAuth())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCreditCardNumber() != null) {
            _hashCode += getCreditCardNumber().hashCode();
        }
        if (getMagStripeData() != null) {
            _hashCode += getMagStripeData().hashCode();
        }
        if (getEntryMode() != null) {
            _hashCode += getEntryMode().hashCode();
        }
        if (getCardExpiration() != null) {
            _hashCode += getCardExpiration().hashCode();
        }
        if (getCardCode() != null) {
            _hashCode += getCardCode().hashCode();
        }
        if (getAVSZip() != null) {
            _hashCode += getAVSZip().hashCode();
        }
        if (getAVSStreetDigits() != null) {
            _hashCode += getAVSStreetDigits().hashCode();
        }
        if (getPares() != null) {
            _hashCode += getPares().hashCode();
        }
        if (getVerifyCardAuth() != null) {
            _hashCode += getVerifyCardAuth().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreditCardInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditCardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("magStripeData");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MagStripeData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "EntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardExpiration");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CardExpiration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardCode");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CardCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AVSZip");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AVSZip"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AVSStreetDigits");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AVSStreetDigits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pares");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Pares"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("verifyCardAuth");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VerifyCardAuth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
