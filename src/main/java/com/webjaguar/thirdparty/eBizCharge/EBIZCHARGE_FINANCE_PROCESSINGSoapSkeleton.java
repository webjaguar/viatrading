/**
 * EBIZCHARGE_FINANCE_PROCESSINGSoapSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public class EBIZCHARGE_FINANCE_PROCESSINGSoapSkeleton implements com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_FINANCE_PROCESSINGSoap, org.apache.axis.wsdl.Skeleton {
    private com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_FINANCE_PROCESSINGSoap impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "UserID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AccountSecurity"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ApplicantInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ApplicantInput"), com.webjaguar.thirdparty.eBizCharge.ApplicantInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CoApplicantInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ApplicantInput"), com.webjaguar.thirdparty.eBizCharge.ApplicantInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MothersMaidenName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "NearestRelPhoneNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "OperationCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "OrginationCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ProductCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PurchaseAmount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("FINANCE_APPLICATION", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FINANCE_APPLICATIONResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>FINANCE_APPLICATIONResponse>FINANCE_APPLICATIONResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FINANCE_APPLICATION"));
        _oper.setSoapAction("FINANCE_APPLICATION");
        _myOperationsList.add(_oper);
        if (_myOperations.get("FINANCE_APPLICATION") == null) {
            _myOperations.put("FINANCE_APPLICATION", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("FINANCE_APPLICATION")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "UserID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AccountNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ProductCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PurchaseAmount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("FINANCE_PROMO_DISCLOSURE", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FINANCE_PROMO_DISCLOSUREResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>FINANCE_PROMO_DISCLOSUREResponse>FINANCE_PROMO_DISCLOSUREResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FINANCE_PROMO_DISCLOSURE"));
        _oper.setSoapAction("FINANCE_PROMO_DISCLOSURE");
        _myOperationsList.add(_oper);
        if (_myOperations.get("FINANCE_PROMO_DISCLOSURE") == null) {
            _myOperations.put("FINANCE_PROMO_DISCLOSURE", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("FINANCE_PROMO_DISCLOSURE")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "UserID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AccountNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Address1"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Address2"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ExpirationMonth"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ExpirationYear"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PromoCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PromoDisclosure"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PurchaseAmount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ZipCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("FINANCE_AUTHORIZATION", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FINANCE_AUTHORIZATIONResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>FINANCE_AUTHORIZATIONResponse>FINANCE_AUTHORIZATIONResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FINANCE_AUTHORIZATION"));
        _oper.setSoapAction("FINANCE_AUTHORIZATION");
        _myOperationsList.add(_oper);
        if (_myOperations.get("FINANCE_AUTHORIZATION") == null) {
            _myOperations.put("FINANCE_AUTHORIZATION", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("FINANCE_AUTHORIZATION")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "UserID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AccountNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AuthCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AuthReversalAmount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "TransactionId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("FINANCE_REVERSE_AUTH", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FINANCE_REVERSE_AUTHResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>FINANCE_REVERSE_AUTHResponse>FINANCE_REVERSE_AUTHResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FINANCE_REVERSE_AUTH"));
        _oper.setSoapAction("FINANCE_REVERSE_AUTH");
        _myOperationsList.add(_oper);
        if (_myOperations.get("FINANCE_REVERSE_AUTH") == null) {
            _myOperations.put("FINANCE_REVERSE_AUTH", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("FINANCE_REVERSE_AUTH")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "UserID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("FINANCE_SETTLEMENT", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FINANCE_SETTLEMENTResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>FINANCE_SETTLEMENTResponse>FINANCE_SETTLEMENTResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FINANCE_SETTLEMENT"));
        _oper.setSoapAction("FINANCE_SETTLEMENT");
        _myOperationsList.add(_oper);
        if (_myOperations.get("FINANCE_SETTLEMENT") == null) {
            _myOperations.put("FINANCE_SETTLEMENT", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("FINANCE_SETTLEMENT")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "UserID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Password"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("FINANCE_RECONCILE", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FINANCE_RECONCILEResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>FINANCE_RECONCILEResponse>FINANCE_RECONCILEResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FINANCE_RECONCILE"));
        _oper.setSoapAction("FINANCE_RECONCILE");
        _myOperationsList.add(_oper);
        if (_myOperations.get("FINANCE_RECONCILE") == null) {
            _myOperations.put("FINANCE_RECONCILE", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("FINANCE_RECONCILE")).add(_oper);
    }

    public EBIZCHARGE_FINANCE_PROCESSINGSoapSkeleton() {
        this.impl = new com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_FINANCE_PROCESSINGSoapImpl();
    }

    public EBIZCHARGE_FINANCE_PROCESSINGSoapSkeleton(com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_FINANCE_PROCESSINGSoap impl) {
        this.impl = impl;
    }
    public com.webjaguar.thirdparty.eBizCharge.FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult[] FINANCE_APPLICATION(java.lang.String userID, java.lang.String password, java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String accountSecurity, com.webjaguar.thirdparty.eBizCharge.ApplicantInput applicantInfo, com.webjaguar.thirdparty.eBizCharge.ApplicantInput coApplicantInfo, java.lang.String mothersMaidenName, java.lang.String nearestRelPhoneNumber, java.lang.String operationCode, java.lang.String orginationCode, java.lang.String productCode, java.math.BigDecimal purchaseAmount) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult[] ret = impl.FINANCE_APPLICATION(userID, password, merchantID, hashInfo, accountSecurity, applicantInfo, coApplicantInfo, mothersMaidenName, nearestRelPhoneNumber, operationCode, orginationCode, productCode, purchaseAmount);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult[] FINANCE_PROMO_DISCLOSURE(java.lang.String userID, java.lang.String password, java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String accountNumber, java.lang.String productCode, java.math.BigDecimal purchaseAmount) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult[] ret = impl.FINANCE_PROMO_DISCLOSURE(userID, password, merchantID, hashInfo, accountNumber, productCode, purchaseAmount);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult[] FINANCE_AUTHORIZATION(java.lang.String userID, java.lang.String password, java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String accountNumber, java.lang.String address1, java.lang.String address2, java.lang.String expirationMonth, java.lang.String expirationYear, java.lang.String promoCode, java.lang.String promoDisclosure, java.math.BigDecimal purchaseAmount, java.lang.String zipCode) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult[] ret = impl.FINANCE_AUTHORIZATION(userID, password, merchantID, hashInfo, accountNumber, address1, address2, expirationMonth, expirationYear, promoCode, promoDisclosure, purchaseAmount, zipCode);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.FINANCE_REVERSE_AUTHResponseFINANCE_REVERSE_AUTHResult[] FINANCE_REVERSE_AUTH(java.lang.String userID, java.lang.String password, java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String accountNumber, java.lang.String authCode, java.math.BigDecimal authReversalAmount, java.lang.String transactionId) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.FINANCE_REVERSE_AUTHResponseFINANCE_REVERSE_AUTHResult[] ret = impl.FINANCE_REVERSE_AUTH(userID, password, merchantID, hashInfo, accountNumber, authCode, authReversalAmount, transactionId);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.FINANCE_SETTLEMENTResponseFINANCE_SETTLEMENTResult[] FINANCE_SETTLEMENT(java.lang.String userID, java.lang.String password, java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.FINANCE_SETTLEMENTResponseFINANCE_SETTLEMENTResult[] ret = impl.FINANCE_SETTLEMENT(userID, password, merchantID, hashInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.FINANCE_RECONCILEResponseFINANCE_RECONCILEResult[] FINANCE_RECONCILE(java.lang.String userID, java.lang.String password, java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.FINANCE_RECONCILEResponseFINANCE_RECONCILEResult[] ret = impl.FINANCE_RECONCILE(userID, password, merchantID, hashInfo);
        return ret;
    }

}
