/**
 * EBIZCHARGE_REPORTINGSoapSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public class EBIZCHARGE_REPORTINGSoapSkeleton implements com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_REPORTINGSoap, org.apache.axis.wsdl.Skeleton {
    private com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_REPORTINGSoap impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "StartDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "EndDate"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("VIEW_SETTLED_BATCH_SUMMARY", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VIEW_SETTLED_BATCH_SUMMARYResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VIEW_SETTLED_BATCH_SUMMARYResponse>VIEW_SETTLED_BATCH_SUMMARYResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VIEW_SETTLED_BATCH_SUMMARY"));
        _oper.setSoapAction("VIEW_SETTLED_BATCH_SUMMARY");
        _myOperationsList.add(_oper);
        if (_myOperations.get("VIEW_SETTLED_BATCH_SUMMARY") == null) {
            _myOperations.put("VIEW_SETTLED_BATCH_SUMMARY", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("VIEW_SETTLED_BATCH_SUMMARY")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BatchReference"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("VIEW_BANKCARD_SETTLED_BATCH_LISTING", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VIEW_BANKCARD_SETTLED_BATCH_LISTINGResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VIEW_BANKCARD_SETTLED_BATCH_LISTINGResponse>VIEW_BANKCARD_SETTLED_BATCH_LISTINGResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VIEW_BANKCARD_SETTLED_BATCH_LISTING"));
        _oper.setSoapAction("VIEW_BANKCARD_SETTLED_BATCH_LISTING");
        _myOperationsList.add(_oper);
        if (_myOperations.get("VIEW_BANKCARD_SETTLED_BATCH_LISTING") == null) {
            _myOperations.put("VIEW_BANKCARD_SETTLED_BATCH_LISTING", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("VIEW_BANKCARD_SETTLED_BATCH_LISTING")).add(_oper);
    }

    public EBIZCHARGE_REPORTINGSoapSkeleton() {
        this.impl = new com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_REPORTINGSoapImpl();
    }

    public EBIZCHARGE_REPORTINGSoapSkeleton(com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_REPORTINGSoap impl) {
        this.impl = impl;
    }
    public com.webjaguar.thirdparty.eBizCharge.BatchesOutput[] VIEW_SETTLED_BATCH_SUMMARY(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String startDate, java.lang.String endDate) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BatchesOutput[] ret = impl.VIEW_SETTLED_BATCH_SUMMARY(merchantID, hashInfo, clientIP, startDate, endDate);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BatchListingOutput[][] VIEW_BANKCARD_SETTLED_BATCH_LISTING(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String batchReference) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BatchListingOutput[][] ret = impl.VIEW_BANKCARD_SETTLED_BATCH_LISTING(merchantID, hashInfo, clientIP, batchReference);
        return ret;
    }

}
