/**
 * EBIZCHARGE_TRANSACTION_PROCESSINGSoapSkeleton.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class EBIZCHARGE_TRANSACTION_PROCESSINGSoapSkeleton implements com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_TRANSACTION_PROCESSINGSoap, org.apache.axis.wsdl.Skeleton {
    private com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_TRANSACTION_PROCESSINGSoap impl;
    private static java.util.Map _myOperations = new java.util.Hashtable();
    private static java.util.Collection _myOperationsList = new java.util.ArrayList();

    /**
    * Returns List of OperationDesc objects with this name
    */
    public static java.util.List getOperationDescByName(java.lang.String methodName) {
        return (java.util.List)_myOperations.get(methodName);
    }

    /**
    * Returns Collection of OperationDescs
    */
    public static java.util.Collection getOperationDescs() {
        return _myOperationsList;
    }

    static {
        org.apache.axis.description.OperationDesc _oper;
        org.apache.axis.description.FaultDesc _fault;
        org.apache.axis.description.ParameterDesc [] _params;
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_SALE", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_SALEResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_SALE"));
        _oper.setSoapAction("BANKCARD_SALE");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_SALE") == null) {
            _myOperations.put("BANKCARD_SALE", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_SALE")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_CREDIT_OR_REFUND", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CREDIT_OR_REFUNDResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CREDIT_OR_REFUND"));
        _oper.setSoapAction("BANKCARD_CREDIT_OR_REFUND");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_CREDIT_OR_REFUND") == null) {
            _myOperations.put("BANKCARD_CREDIT_OR_REFUND", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_CREDIT_OR_REFUND")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_AUTHONLY", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_AUTHONLYResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_AUTHONLY"));
        _oper.setSoapAction("BANKCARD_AUTHONLY");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_AUTHONLY") == null) {
            _myOperations.put("BANKCARD_AUTHONLY", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_AUTHONLY")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_AUTHONLYL3", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_AUTHONLYL3Result"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_AUTHONLYL3"));
        _oper.setSoapAction("BANKCARD_AUTHONLYL3");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_AUTHONLYL3") == null) {
            _myOperations.put("BANKCARD_AUTHONLYL3", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_AUTHONLYL3")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Amount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_CREDIT", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CREDITResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CREDIT"));
        _oper.setSoapAction("BANKCARD_CREDIT");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_CREDIT") == null) {
            _myOperations.put("BANKCARD_CREDIT", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_CREDIT")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_VOID", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_VOIDResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_VOID"));
        _oper.setSoapAction("BANKCARD_VOID");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_VOID") == null) {
            _myOperations.put("BANKCARD_VOID", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_VOID")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_PRIOR_AUTH_SALE", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_PRIOR_AUTH_SALEResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_PRIOR_AUTH_SALE"));
        _oper.setSoapAction("BANKCARD_PRIOR_AUTH_SALE");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_PRIOR_AUTH_SALE") == null) {
            _myOperations.put("BANKCARD_PRIOR_AUTH_SALE", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_PRIOR_AUTH_SALE")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AuthCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_PHONE_AUTH_POST", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_PHONE_AUTH_POSTResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_PHONE_AUTH_POST"));
        _oper.setSoapAction("BANKCARD_PHONE_AUTH_POST");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_PHONE_AUTH_POST") == null) {
            _myOperations.put("BANKCARD_PHONE_AUTH_POST", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_PHONE_AUTH_POST")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCHECK_SALE", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCHECK_SALEResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCHECK_SALE"));
        _oper.setSoapAction("BANKCHECK_SALE");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCHECK_SALE") == null) {
            _myOperations.put("BANKCHECK_SALE", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCHECK_SALE")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Amount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCHECK_CREDIT", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCHECK_CREDITResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCHECK_CREDIT"));
        _oper.setSoapAction("BANKCHECK_CREDIT");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCHECK_CREDIT") == null) {
            _myOperations.put("BANKCHECK_CREDIT", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCHECK_CREDIT")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FileName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AutoStart"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), java.lang.Boolean.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Format"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Encoding"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Fields"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Data"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "OverrideDuplicates"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), java.lang.Boolean.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_SEND_BATCH", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_SEND_BATCHResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BatchUploadStatus"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_SEND_BATCH"));
        _oper.setSoapAction("BANKCARD_SEND_BATCH");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_SEND_BATCH") == null) {
            _myOperations.put("BANKCARD_SEND_BATCH", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_SEND_BATCH")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "UploadRefNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_RUN_BATCH", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_RUN_BATCHResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_RUN_BATCHResponse>BANKCARD_RUN_BATCHResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_RUN_BATCH"));
        _oper.setSoapAction("BANKCARD_RUN_BATCH");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_RUN_BATCH") == null) {
            _myOperations.put("BANKCARD_RUN_BATCH", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_RUN_BATCH")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "UploadRefNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("BANKCARD_GET_BATCH_STATUS", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_GET_BATCH_STATUSResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BatchUploadStatus"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_GET_BATCH_STATUS"));
        _oper.setSoapAction("BANKCARD_GET_BATCH_STATUS");
        _myOperationsList.add(_oper);
        if (_myOperations.get("BANKCARD_GET_BATCH_STATUS") == null) {
            _myOperations.put("BANKCARD_GET_BATCH_STATUS", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("BANKCARD_GET_BATCH_STATUS")).add(_oper);
        _params = new org.apache.axis.description.ParameterDesc [] {
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false), 
            new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false), 
        };
        _oper = new org.apache.axis.description.OperationDesc("VERIFY_BANKCARD_SERVICE", _params, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VERIFY_BANKCARD_SERVICEResult"));
        _oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VERIFY_BANKCARD_SERVICEResponse>VERIFY_BANKCARD_SERVICEResult"));
        _oper.setElementQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VERIFY_BANKCARD_SERVICE"));
        _oper.setSoapAction("VERIFY_BANKCARD_SERVICE");
        _myOperationsList.add(_oper);
        if (_myOperations.get("VERIFY_BANKCARD_SERVICE") == null) {
            _myOperations.put("VERIFY_BANKCARD_SERVICE", new java.util.ArrayList());
        }
        ((java.util.List)_myOperations.get("VERIFY_BANKCARD_SERVICE")).add(_oper);
    }

    public EBIZCHARGE_TRANSACTION_PROCESSINGSoapSkeleton() {
        this.impl = new com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_TRANSACTION_PROCESSINGSoapImpl();
    }

    public EBIZCHARGE_TRANSACTION_PROCESSINGSoapSkeleton(com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_TRANSACTION_PROCESSINGSoap impl) {
        this.impl = impl;
    }
    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_SALE(merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo, itemDetailInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CREDIT_OR_REFUND(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_CREDIT_OR_REFUND(merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_AUTHONLY(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_AUTHONLY(merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_AUTHONLYL3(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_AUTHONLYL3(merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo, itemDetailInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CREDIT(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.math.BigDecimal amount, java.lang.String referenceNumber) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_CREDIT(merchantID, hashInfo, clientIP, amount, referenceNumber);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_VOID(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_VOID(merchantID, hashInfo, clientIP, referenceNumber);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_PRIOR_AUTH_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_PRIOR_AUTH_SALE(merchantID, hashInfo, clientIP, referenceNumber, chargeDetailInfo, billingDetailInfo, shippingDetailInfo, itemDetailInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_PHONE_AUTH_POST(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String authCode, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCARD_PHONE_AUTH_POST(merchantID, hashInfo, clientIP, authCode, chargeDetailInfo, billingDetailInfo, shippingDetailInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCHECK_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCHECK_SALE(merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCHECK_CREDIT(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, java.math.BigDecimal amount, java.lang.String referenceNumber) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BankCardResponse ret = impl.BANKCHECK_CREDIT(merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, amount, referenceNumber);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus BANKCARD_SEND_BATCH(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String fileName, java.lang.Boolean autoStart, java.lang.String format, java.lang.String encoding, java.lang.String fields, java.lang.String data, java.lang.Boolean overrideDuplicates) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus ret = impl.BANKCARD_SEND_BATCH(merchantID, hashInfo, clientIP, fileName, autoStart, format, encoding, fields, data, overrideDuplicates);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_RUN_BATCHResponseBANKCARD_RUN_BATCHResult BANKCARD_RUN_BATCH(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String uploadRefNum) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BANKCARD_RUN_BATCHResponseBANKCARD_RUN_BATCHResult ret = impl.BANKCARD_RUN_BATCH(merchantID, hashInfo, clientIP, uploadRefNum);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus BANKCARD_GET_BATCH_STATUS(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String uploadRefNum) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus ret = impl.BANKCARD_GET_BATCH_STATUS(merchantID, hashInfo, clientIP, uploadRefNum);
        return ret;
    }

    public com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult VERIFY_BANKCARD_SERVICE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP) throws java.rmi.RemoteException
    {
        com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult ret = impl.VERIFY_BANKCARD_SERVICE(merchantID, hashInfo, clientIP);
        return ret;
    }

}
