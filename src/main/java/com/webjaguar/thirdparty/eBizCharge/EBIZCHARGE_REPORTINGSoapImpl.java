/**
 * EBIZCHARGE_REPORTINGSoapImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public class EBIZCHARGE_REPORTINGSoapImpl implements com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_REPORTINGSoap{
    public com.webjaguar.thirdparty.eBizCharge.BatchesOutput[] VIEW_SETTLED_BATCH_SUMMARY(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String startDate, java.lang.String endDate) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BatchListingOutput[][] VIEW_BANKCARD_SETTLED_BATCH_LISTING(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String batchReference) throws java.rmi.RemoteException {
        return null;
    }

}
