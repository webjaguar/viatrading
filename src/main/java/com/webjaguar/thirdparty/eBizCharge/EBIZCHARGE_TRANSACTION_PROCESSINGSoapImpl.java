/**
 * EBIZCHARGE_TRANSACTION_PROCESSINGSoapImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class EBIZCHARGE_TRANSACTION_PROCESSINGSoapImpl implements com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_TRANSACTION_PROCESSINGSoap{
    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CREDIT_OR_REFUND(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_AUTHONLY(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_AUTHONLYL3(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CREDIT(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.math.BigDecimal amount, java.lang.String referenceNumber) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_VOID(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_PRIOR_AUTH_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_PHONE_AUTH_POST(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String authCode, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCHECK_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCHECK_CREDIT(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, java.math.BigDecimal amount, java.lang.String referenceNumber) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus BANKCARD_SEND_BATCH(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String fileName, java.lang.Boolean autoStart, java.lang.String format, java.lang.String encoding, java.lang.String fields, java.lang.String data, java.lang.Boolean overrideDuplicates) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_RUN_BATCHResponseBANKCARD_RUN_BATCHResult BANKCARD_RUN_BATCH(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String uploadRefNum) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus BANKCARD_GET_BATCH_STATUS(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String uploadRefNum) throws java.rmi.RemoteException {
        return null;
    }

    public com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult VERIFY_BANKCARD_SERVICE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP) throws java.rmi.RemoteException {
        return null;
    }

}
