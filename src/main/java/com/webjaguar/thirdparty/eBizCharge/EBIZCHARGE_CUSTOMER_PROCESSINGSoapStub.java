/**
 * EBIZCHARGE_CUSTOMER_PROCESSINGSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public class EBIZCHARGE_CUSTOMER_PROCESSINGSoapStub extends org.apache.axis.client.Stub implements com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[14];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHOD");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardInput"), com.webjaguar.thirdparty.eBizCharge.CreditCardInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CheckInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CheckInput"), com.webjaguar.thirdparty.eBizCharge.CheckInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponse>BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_ADD_CUSTOMER");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerInput"), com.webjaguar.thirdparty.eBizCharge.CustomerInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_ADD_CUSTOMERResponse>BANKCARD_ADD_CUSTOMERResult"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMERResponseBANKCARD_ADD_CUSTOMERResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_ADD_CUSTOMERResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_GET_CUSTOMER");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDBNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_GET_CUSTOMERResponse>BANKCARD_GET_CUSTOMERResult"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_GET_CUSTOMERResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_DEL_CUSTOMER");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDBNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_DEL_CUSTOMERResponse>BANKCARD_DEL_CUSTOMERResult"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMERResponseBANKCARD_DEL_CUSTOMERResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_DEL_CUSTOMERResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDBNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardInput"), com.webjaguar.thirdparty.eBizCharge.CreditCardInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CheckInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CheckInput"), com.webjaguar.thirdparty.eBizCharge.CheckInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MethodName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponse>BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDBNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MethodID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResponse>BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResponseBANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_CUSTOMER_SALE");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInput"), com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_SALEResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_CUSTOMER_AUTHONLY");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInput"), com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_AUTHONLYResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_CUSTOMER_AUTHONLYL3");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInput"), com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_AUTHONLYL3Result"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_CUSTOMER_CREDIT_OR_REFUND");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInput"), com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_CREDIT_OR_REFUNDResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_CUSTOMER_CREDIT");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Amount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_CREDITResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_CUSTOMER_VOID");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_VOIDResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_CUSTOMER_PRIOR_AUTH_SALE");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInput"), com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_PRIOR_AUTH_SALEResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("VERIFY_BANKCARD_CUSTOMER_SERVICE");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VERIFY_BANKCARD_CUSTOMER_SERVICEResponse>VERIFY_BANKCARD_CUSTOMER_SERVICEResult"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_CUSTOMER_SERVICEResponseVERIFY_BANKCARD_CUSTOMER_SERVICEResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VERIFY_BANKCARD_CUSTOMER_SERVICEResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

    }

    public EBIZCHARGE_CUSTOMER_PROCESSINGSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public EBIZCHARGE_CUSTOMER_PROCESSINGSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public EBIZCHARGE_CUSTOMER_PROCESSINGSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponse>BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_ADD_CUSTOMERResponse>BANKCARD_ADD_CUSTOMERResult");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMERResponseBANKCARD_ADD_CUSTOMERResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResponse>BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResponseBANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_DEL_CUSTOMERResponse>BANKCARD_DEL_CUSTOMERResult");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMERResponseBANKCARD_DEL_CUSTOMERResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_GET_CUSTOMERResponse>BANKCARD_GET_CUSTOMERResult");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponse>BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VERIFY_BANKCARD_CUSTOMER_SERVICEResponse>VERIFY_BANKCARD_CUSTOMER_SERVICEResult");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_CUSTOMER_SERVICEResponseVERIFY_BANKCARD_CUSTOMER_SERVICEResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CheckInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.CheckInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditCardInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.CreditCardInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerChargeInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.CustomerInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.HashInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.ItemDetailInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PaymentMethodInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.PaymentMethodInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHOD(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.CreditCardInput creditCardInfo, com.webjaguar.thirdparty.eBizCharge.CheckInput checkInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHOD");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHOD"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, creditCardInfo, checkInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMERResponseBANKCARD_ADD_CUSTOMERResult BANKCARD_ADD_CUSTOMER(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.CustomerInput customerInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_ADD_CUSTOMER");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_ADD_CUSTOMER"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, customerInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMERResponseBANKCARD_ADD_CUSTOMERResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMERResponseBANKCARD_ADD_CUSTOMERResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMERResponseBANKCARD_ADD_CUSTOMERResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult BANKCARD_GET_CUSTOMER(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String customerID, java.lang.String custDBNum) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_GET_CUSTOMER");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_GET_CUSTOMER"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, customerID, custDBNum});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMERResponseBANKCARD_DEL_CUSTOMERResult BANKCARD_DEL_CUSTOMER(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String custDBNum, java.lang.String customerID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_DEL_CUSTOMER");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_DEL_CUSTOMER"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, custDBNum, customerID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMERResponseBANKCARD_DEL_CUSTOMERResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMERResponseBANKCARD_DEL_CUSTOMERResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMERResponseBANKCARD_DEL_CUSTOMERResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String custDBNum, com.webjaguar.thirdparty.eBizCharge.CreditCardInput creditCardInfo, com.webjaguar.thirdparty.eBizCharge.CheckInput checkInfo, java.lang.String methodName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, custDBNum, creditCardInfo, checkInfo, methodName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResponseBANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String custDBNum, java.lang.String methodID) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, custDBNum, methodID});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResponseBANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResponseBANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BANKCARD_DEL_CUSTOMER_PAYMENT_METHODResponseBANKCARD_DEL_CUSTOMER_PAYMENT_METHODResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput customerChargeInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_CUSTOMER_SALE");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_SALE"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, customerChargeInfo, itemDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_AUTHONLY(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput customerChargeInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_CUSTOMER_AUTHONLY");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_AUTHONLY"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, customerChargeInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_AUTHONLYL3(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput customerChargeInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_CUSTOMER_AUTHONLYL3");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_AUTHONLYL3"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, customerChargeInfo, itemDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_CREDIT_OR_REFUND(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput customerChargeInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_CUSTOMER_CREDIT_OR_REFUND");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_CREDIT_OR_REFUND"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, customerChargeInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_CREDIT(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.math.BigDecimal amount, java.lang.String referenceNumber) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_CUSTOMER_CREDIT");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_CREDIT"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, amount, referenceNumber});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_VOID(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_CUSTOMER_VOID");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_VOID"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, referenceNumber});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CUSTOMER_PRIOR_AUTH_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber, com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput customerChargeInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_CUSTOMER_PRIOR_AUTH_SALE");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CUSTOMER_PRIOR_AUTH_SALE"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, referenceNumber, customerChargeInfo, itemDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_CUSTOMER_SERVICEResponseVERIFY_BANKCARD_CUSTOMER_SERVICEResult VERIFY_BANKCARD_CUSTOMER_SERVICE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("VERIFY_BANKCARD_CUSTOMER_SERVICE");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VERIFY_BANKCARD_CUSTOMER_SERVICE"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_CUSTOMER_SERVICEResponseVERIFY_BANKCARD_CUSTOMER_SERVICEResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_CUSTOMER_SERVICEResponseVERIFY_BANKCARD_CUSTOMER_SERVICEResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_CUSTOMER_SERVICEResponseVERIFY_BANKCARD_CUSTOMER_SERVICEResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
