/**
 * EBIZCHARGE_GIFTCARD_PROCESSING.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public interface EBIZCHARGE_GIFTCARD_PROCESSING extends javax.xml.rpc.Service {
    public java.lang.String getEBIZCHARGE_GIFTCARD_PROCESSINGSoapAddress();

    public com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_GIFTCARD_PROCESSINGSoap getEBIZCHARGE_GIFTCARD_PROCESSINGSoap() throws javax.xml.rpc.ServiceException;

    public com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_GIFTCARD_PROCESSINGSoap getEBIZCHARGE_GIFTCARD_PROCESSINGSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
