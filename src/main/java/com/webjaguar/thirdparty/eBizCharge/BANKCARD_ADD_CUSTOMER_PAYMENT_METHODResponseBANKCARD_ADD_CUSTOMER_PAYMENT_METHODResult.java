/**
 * BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult  implements java.io.Serializable {
    private java.lang.String status;

    private java.lang.String methodID;

    private java.lang.String error;

    private java.lang.String message;

    public BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult() {
    }

    public BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult(
           java.lang.String status,
           java.lang.String methodID,
           java.lang.String error,
           java.lang.String message) {
           this.status = status;
           this.methodID = methodID;
           this.error = error;
           this.message = message;
    }


    /**
     * Gets the status value for this BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the methodID value for this BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @return methodID
     */
    public java.lang.String getMethodID() {
        return methodID;
    }


    /**
     * Sets the methodID value for this BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @param methodID
     */
    public void setMethodID(java.lang.String methodID) {
        this.methodID = methodID;
    }


    /**
     * Gets the error value for this BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }


    /**
     * Gets the message value for this BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @return message
     */
    public java.lang.String getMessage() {
        return message;
    }


    /**
     * Sets the message value for this BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @param message
     */
    public void setMessage(java.lang.String message) {
        this.message = message;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult)) return false;
        BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult other = (BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.methodID==null && other.getMethodID()==null) || 
             (this.methodID!=null &&
              this.methodID.equals(other.getMethodID()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.message==null && other.getMessage()==null) || 
             (this.message!=null &&
              this.message.equals(other.getMessage())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getMethodID() != null) {
            _hashCode += getMethodID().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getMessage() != null) {
            _hashCode += getMessage().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponse>BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("methodID");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MethodID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("message");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Message"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
