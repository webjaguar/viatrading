/**
 * HashInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class HashInput  implements java.io.Serializable {
    private java.lang.String pinHash;

    private java.lang.String hashType;

    private java.lang.String hashSeed;

    private java.lang.String sourcekey;

    public HashInput() {
    }

    public HashInput(
           java.lang.String pinHash,
           java.lang.String hashType,
           java.lang.String hashSeed,
           java.lang.String sourcekey) {
           this.pinHash = pinHash;
           this.hashType = hashType;
           this.hashSeed = hashSeed;
           this.sourcekey = sourcekey;
    }


    /**
     * Gets the pinHash value for this HashInput.
     * 
     * @return pinHash
     */
    public java.lang.String getPinHash() {
        return pinHash;
    }


    /**
     * Sets the pinHash value for this HashInput.
     * 
     * @param pinHash
     */
    public void setPinHash(java.lang.String pinHash) {
        this.pinHash = pinHash;
    }


    /**
     * Gets the hashType value for this HashInput.
     * 
     * @return hashType
     */
    public java.lang.String getHashType() {
        return hashType;
    }


    /**
     * Sets the hashType value for this HashInput.
     * 
     * @param hashType
     */
    public void setHashType(java.lang.String hashType) {
        this.hashType = hashType;
    }


    /**
     * Gets the hashSeed value for this HashInput.
     * 
     * @return hashSeed
     */
    public java.lang.String getHashSeed() {
        return hashSeed;
    }


    /**
     * Sets the hashSeed value for this HashInput.
     * 
     * @param hashSeed
     */
    public void setHashSeed(java.lang.String hashSeed) {
        this.hashSeed = hashSeed;
    }


    /**
     * Gets the sourcekey value for this HashInput.
     * 
     * @return sourcekey
     */
    public java.lang.String getSourcekey() {
        return sourcekey;
    }


    /**
     * Sets the sourcekey value for this HashInput.
     * 
     * @param sourcekey
     */
    public void setSourcekey(java.lang.String sourcekey) {
        this.sourcekey = sourcekey;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HashInput)) return false;
        HashInput other = (HashInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.pinHash==null && other.getPinHash()==null) || 
             (this.pinHash!=null &&
              this.pinHash.equals(other.getPinHash()))) &&
            ((this.hashType==null && other.getHashType()==null) || 
             (this.hashType!=null &&
              this.hashType.equals(other.getHashType()))) &&
            ((this.hashSeed==null && other.getHashSeed()==null) || 
             (this.hashSeed!=null &&
              this.hashSeed.equals(other.getHashSeed()))) &&
            ((this.sourcekey==null && other.getSourcekey()==null) || 
             (this.sourcekey!=null &&
              this.sourcekey.equals(other.getSourcekey())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPinHash() != null) {
            _hashCode += getPinHash().hashCode();
        }
        if (getHashType() != null) {
            _hashCode += getHashType().hashCode();
        }
        if (getHashSeed() != null) {
            _hashCode += getHashSeed().hashCode();
        }
        if (getSourcekey() != null) {
            _hashCode += getSourcekey().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HashInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pinHash");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PinHash"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hashType");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hashSeed");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashSeed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourcekey");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Sourcekey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
