/**
 * FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult  implements java.io.Serializable {
    private java.lang.String accountNumber;

    private java.lang.String apr;

    private java.lang.String cofirstName;

    private java.lang.String colastName;

    private java.lang.String creditLimit;

    private java.lang.String creditLinePhoneNumber;

    private java.lang.String customerSupportPhoneNumber;

    private java.lang.String firstName;

    private java.lang.String lastName;

    private java.lang.String keyNumber;

    private java.lang.String riskTier;

    private java.lang.String secondaryProgramName;

    private java.lang.String status;

    private java.lang.String timeToCall;

    public FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult() {
    }

    public FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult(
           java.lang.String accountNumber,
           java.lang.String apr,
           java.lang.String cofirstName,
           java.lang.String colastName,
           java.lang.String creditLimit,
           java.lang.String creditLinePhoneNumber,
           java.lang.String customerSupportPhoneNumber,
           java.lang.String firstName,
           java.lang.String lastName,
           java.lang.String keyNumber,
           java.lang.String riskTier,
           java.lang.String secondaryProgramName,
           java.lang.String status,
           java.lang.String timeToCall) {
           this.accountNumber = accountNumber;
           this.apr = apr;
           this.cofirstName = cofirstName;
           this.colastName = colastName;
           this.creditLimit = creditLimit;
           this.creditLinePhoneNumber = creditLinePhoneNumber;
           this.customerSupportPhoneNumber = customerSupportPhoneNumber;
           this.firstName = firstName;
           this.lastName = lastName;
           this.keyNumber = keyNumber;
           this.riskTier = riskTier;
           this.secondaryProgramName = secondaryProgramName;
           this.status = status;
           this.timeToCall = timeToCall;
    }


    /**
     * Gets the accountNumber value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return accountNumber
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the apr value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return apr
     */
    public java.lang.String getApr() {
        return apr;
    }


    /**
     * Sets the apr value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param apr
     */
    public void setApr(java.lang.String apr) {
        this.apr = apr;
    }


    /**
     * Gets the cofirstName value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return cofirstName
     */
    public java.lang.String getCofirstName() {
        return cofirstName;
    }


    /**
     * Sets the cofirstName value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param cofirstName
     */
    public void setCofirstName(java.lang.String cofirstName) {
        this.cofirstName = cofirstName;
    }


    /**
     * Gets the colastName value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return colastName
     */
    public java.lang.String getColastName() {
        return colastName;
    }


    /**
     * Sets the colastName value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param colastName
     */
    public void setColastName(java.lang.String colastName) {
        this.colastName = colastName;
    }


    /**
     * Gets the creditLimit value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return creditLimit
     */
    public java.lang.String getCreditLimit() {
        return creditLimit;
    }


    /**
     * Sets the creditLimit value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param creditLimit
     */
    public void setCreditLimit(java.lang.String creditLimit) {
        this.creditLimit = creditLimit;
    }


    /**
     * Gets the creditLinePhoneNumber value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return creditLinePhoneNumber
     */
    public java.lang.String getCreditLinePhoneNumber() {
        return creditLinePhoneNumber;
    }


    /**
     * Sets the creditLinePhoneNumber value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param creditLinePhoneNumber
     */
    public void setCreditLinePhoneNumber(java.lang.String creditLinePhoneNumber) {
        this.creditLinePhoneNumber = creditLinePhoneNumber;
    }


    /**
     * Gets the customerSupportPhoneNumber value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return customerSupportPhoneNumber
     */
    public java.lang.String getCustomerSupportPhoneNumber() {
        return customerSupportPhoneNumber;
    }


    /**
     * Sets the customerSupportPhoneNumber value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param customerSupportPhoneNumber
     */
    public void setCustomerSupportPhoneNumber(java.lang.String customerSupportPhoneNumber) {
        this.customerSupportPhoneNumber = customerSupportPhoneNumber;
    }


    /**
     * Gets the firstName value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the lastName value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the keyNumber value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return keyNumber
     */
    public java.lang.String getKeyNumber() {
        return keyNumber;
    }


    /**
     * Sets the keyNumber value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param keyNumber
     */
    public void setKeyNumber(java.lang.String keyNumber) {
        this.keyNumber = keyNumber;
    }


    /**
     * Gets the riskTier value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return riskTier
     */
    public java.lang.String getRiskTier() {
        return riskTier;
    }


    /**
     * Sets the riskTier value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param riskTier
     */
    public void setRiskTier(java.lang.String riskTier) {
        this.riskTier = riskTier;
    }


    /**
     * Gets the secondaryProgramName value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return secondaryProgramName
     */
    public java.lang.String getSecondaryProgramName() {
        return secondaryProgramName;
    }


    /**
     * Sets the secondaryProgramName value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param secondaryProgramName
     */
    public void setSecondaryProgramName(java.lang.String secondaryProgramName) {
        this.secondaryProgramName = secondaryProgramName;
    }


    /**
     * Gets the status value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the timeToCall value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @return timeToCall
     */
    public java.lang.String getTimeToCall() {
        return timeToCall;
    }


    /**
     * Sets the timeToCall value for this FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.
     * 
     * @param timeToCall
     */
    public void setTimeToCall(java.lang.String timeToCall) {
        this.timeToCall = timeToCall;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult)) return false;
        FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult other = (FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.apr==null && other.getApr()==null) || 
             (this.apr!=null &&
              this.apr.equals(other.getApr()))) &&
            ((this.cofirstName==null && other.getCofirstName()==null) || 
             (this.cofirstName!=null &&
              this.cofirstName.equals(other.getCofirstName()))) &&
            ((this.colastName==null && other.getColastName()==null) || 
             (this.colastName!=null &&
              this.colastName.equals(other.getColastName()))) &&
            ((this.creditLimit==null && other.getCreditLimit()==null) || 
             (this.creditLimit!=null &&
              this.creditLimit.equals(other.getCreditLimit()))) &&
            ((this.creditLinePhoneNumber==null && other.getCreditLinePhoneNumber()==null) || 
             (this.creditLinePhoneNumber!=null &&
              this.creditLinePhoneNumber.equals(other.getCreditLinePhoneNumber()))) &&
            ((this.customerSupportPhoneNumber==null && other.getCustomerSupportPhoneNumber()==null) || 
             (this.customerSupportPhoneNumber!=null &&
              this.customerSupportPhoneNumber.equals(other.getCustomerSupportPhoneNumber()))) &&
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.keyNumber==null && other.getKeyNumber()==null) || 
             (this.keyNumber!=null &&
              this.keyNumber.equals(other.getKeyNumber()))) &&
            ((this.riskTier==null && other.getRiskTier()==null) || 
             (this.riskTier!=null &&
              this.riskTier.equals(other.getRiskTier()))) &&
            ((this.secondaryProgramName==null && other.getSecondaryProgramName()==null) || 
             (this.secondaryProgramName!=null &&
              this.secondaryProgramName.equals(other.getSecondaryProgramName()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.timeToCall==null && other.getTimeToCall()==null) || 
             (this.timeToCall!=null &&
              this.timeToCall.equals(other.getTimeToCall())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getApr() != null) {
            _hashCode += getApr().hashCode();
        }
        if (getCofirstName() != null) {
            _hashCode += getCofirstName().hashCode();
        }
        if (getColastName() != null) {
            _hashCode += getColastName().hashCode();
        }
        if (getCreditLimit() != null) {
            _hashCode += getCreditLimit().hashCode();
        }
        if (getCreditLinePhoneNumber() != null) {
            _hashCode += getCreditLinePhoneNumber().hashCode();
        }
        if (getCustomerSupportPhoneNumber() != null) {
            _hashCode += getCustomerSupportPhoneNumber().hashCode();
        }
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getKeyNumber() != null) {
            _hashCode += getKeyNumber().hashCode();
        }
        if (getRiskTier() != null) {
            _hashCode += getRiskTier().hashCode();
        }
        if (getSecondaryProgramName() != null) {
            _hashCode += getSecondaryProgramName().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getTimeToCall() != null) {
            _hashCode += getTimeToCall().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>FINANCE_APPLICATIONResponse>FINANCE_APPLICATIONResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apr");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Apr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cofirstName");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CofirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("colastName");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ColastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditLimit");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditLimit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditLinePhoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CreditLinePhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerSupportPhoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerSupportPhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstName");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "LastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("keyNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "KeyNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("riskTier");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "RiskTier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("secondaryProgramName");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "SecondaryProgramName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timeToCall");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "TimeToCall"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
