/**
 * EBIZCHARGE_CUSTOMER_PROCESSINGLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class EBIZCHARGE_CUSTOMER_PROCESSINGLocator extends org.apache.axis.client.Service implements com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSING {

    public EBIZCHARGE_CUSTOMER_PROCESSINGLocator() {
    }


    public EBIZCHARGE_CUSTOMER_PROCESSINGLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public EBIZCHARGE_CUSTOMER_PROCESSINGLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for EBIZCHARGE_CUSTOMER_PROCESSINGSoap
    private java.lang.String EBIZCHARGE_CUSTOMER_PROCESSINGSoap_address = "https://payments.ebizcharge.com/web_services/webterm/customer_processing.asmx";

    public java.lang.String getEBIZCHARGE_CUSTOMER_PROCESSINGSoapAddress() {
        return EBIZCHARGE_CUSTOMER_PROCESSINGSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String EBIZCHARGE_CUSTOMER_PROCESSINGSoapWSDDServiceName = "EBIZCHARGE_CUSTOMER_PROCESSINGSoap";

    public java.lang.String getEBIZCHARGE_CUSTOMER_PROCESSINGSoapWSDDServiceName() {
        return EBIZCHARGE_CUSTOMER_PROCESSINGSoapWSDDServiceName;
    }

    public void setEBIZCHARGE_CUSTOMER_PROCESSINGSoapWSDDServiceName(java.lang.String name) {
        EBIZCHARGE_CUSTOMER_PROCESSINGSoapWSDDServiceName = name;
    }

    public com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoap getEBIZCHARGE_CUSTOMER_PROCESSINGSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(EBIZCHARGE_CUSTOMER_PROCESSINGSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getEBIZCHARGE_CUSTOMER_PROCESSINGSoap(endpoint);
    }

    public com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoap getEBIZCHARGE_CUSTOMER_PROCESSINGSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoapStub _stub = new com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoapStub(portAddress, this);
            _stub.setPortName(getEBIZCHARGE_CUSTOMER_PROCESSINGSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setEBIZCHARGE_CUSTOMER_PROCESSINGSoapEndpointAddress(java.lang.String address) {
        EBIZCHARGE_CUSTOMER_PROCESSINGSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoapStub _stub = new com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoapStub(new java.net.URL(EBIZCHARGE_CUSTOMER_PROCESSINGSoap_address), this);
                _stub.setPortName(getEBIZCHARGE_CUSTOMER_PROCESSINGSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("EBIZCHARGE_CUSTOMER_PROCESSINGSoap".equals(inputPortName)) {
            return getEBIZCHARGE_CUSTOMER_PROCESSINGSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "EBIZCHARGE_CUSTOMER_PROCESSING");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "EBIZCHARGE_CUSTOMER_PROCESSINGSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("EBIZCHARGE_CUSTOMER_PROCESSINGSoap".equals(portName)) {
            setEBIZCHARGE_CUSTOMER_PROCESSINGSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
