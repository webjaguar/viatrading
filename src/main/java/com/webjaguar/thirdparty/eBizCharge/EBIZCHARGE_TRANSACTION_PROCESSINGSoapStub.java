/**
 * EBIZCHARGE_TRANSACTION_PROCESSINGSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class EBIZCHARGE_TRANSACTION_PROCESSINGSoapStub extends org.apache.axis.client.Stub implements com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_TRANSACTION_PROCESSINGSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[14];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_SALE");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_SALEResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_CREDIT_OR_REFUND");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CREDIT_OR_REFUNDResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_AUTHONLY");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_AUTHONLYResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_AUTHONLYL3");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_AUTHONLYL3Result"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_CREDIT");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Amount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CREDITResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_VOID");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_VOIDResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_PRIOR_AUTH_SALE");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_PRIOR_AUTH_SALEResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_PHONE_AUTH_POST");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AuthCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_PHONE_AUTH_POSTResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCHECK_SALE");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCHECK_SALEResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCHECK_CREDIT");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Amount"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"), java.math.BigDecimal.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCHECK_CREDITResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_SEND_BATCH");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FileName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AutoStart"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), java.lang.Boolean.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Format"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Encoding"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Fields"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Data"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "OverrideDuplicates"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), java.lang.Boolean.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BatchUploadStatus"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_SEND_BATCHResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_RUN_BATCH");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "UploadRefNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_RUN_BATCHResponse>BANKCARD_RUN_BATCHResult"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BANKCARD_RUN_BATCHResponseBANKCARD_RUN_BATCHResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_RUN_BATCHResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_GET_BATCH_STATUS");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "UploadRefNum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BatchUploadStatus"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_GET_BATCH_STATUSResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("VERIFY_BANKCARD_SERVICE");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VERIFY_BANKCARD_SERVICEResponse>VERIFY_BANKCARD_SERVICEResult"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VERIFY_BANKCARD_SERVICEResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

    }

    public EBIZCHARGE_TRANSACTION_PROCESSINGSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public EBIZCHARGE_TRANSACTION_PROCESSINGSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public EBIZCHARGE_TRANSACTION_PROCESSINGSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_RUN_BATCHResponse>BANKCARD_RUN_BATCHResult");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.BANKCARD_RUN_BATCHResponseBANKCARD_RUN_BATCHResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VERIFY_BANKCARD_SERVICEResponse>VERIFY_BANKCARD_SERVICEResult");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BatchUploadStatus");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.HashInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.ItemDetailInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_SALE");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_SALE"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo, itemDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CREDIT_OR_REFUND(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_CREDIT_OR_REFUND");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CREDIT_OR_REFUND"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_AUTHONLY(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_AUTHONLY");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_AUTHONLY"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_AUTHONLYL3(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_AUTHONLYL3");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_AUTHONLYL3"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo, itemDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_CREDIT(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.math.BigDecimal amount, java.lang.String referenceNumber) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_CREDIT");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_CREDIT"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, amount, referenceNumber});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_VOID(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_VOID");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_VOID"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, referenceNumber});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_PRIOR_AUTH_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_PRIOR_AUTH_SALE");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_PRIOR_AUTH_SALE"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, referenceNumber, chargeDetailInfo, billingDetailInfo, shippingDetailInfo, itemDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_PHONE_AUTH_POST(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String authCode, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_PHONE_AUTH_POST");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_PHONE_AUTH_POST"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, authCode, chargeDetailInfo, billingDetailInfo, shippingDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCHECK_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCHECK_SALE");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCHECK_SALE"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCHECK_CREDIT(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, java.math.BigDecimal amount, java.lang.String referenceNumber) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCHECK_CREDIT");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCHECK_CREDIT"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, amount, referenceNumber});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus BANKCARD_SEND_BATCH(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String fileName, java.lang.Boolean autoStart, java.lang.String format, java.lang.String encoding, java.lang.String fields, java.lang.String data, java.lang.Boolean overrideDuplicates) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_SEND_BATCH");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_SEND_BATCH"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, fileName, autoStart, format, encoding, fields, data, overrideDuplicates});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BANKCARD_RUN_BATCHResponseBANKCARD_RUN_BATCHResult BANKCARD_RUN_BATCH(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String uploadRefNum) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_RUN_BATCH");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_RUN_BATCH"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, uploadRefNum});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_RUN_BATCHResponseBANKCARD_RUN_BATCHResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BANKCARD_RUN_BATCHResponseBANKCARD_RUN_BATCHResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BANKCARD_RUN_BATCHResponseBANKCARD_RUN_BATCHResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus BANKCARD_GET_BATCH_STATUS(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String uploadRefNum) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_GET_BATCH_STATUS");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_GET_BATCH_STATUS"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, uploadRefNum});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BatchUploadStatus.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult VERIFY_BANKCARD_SERVICE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("VERIFY_BANKCARD_SERVICE");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VERIFY_BANKCARD_SERVICE"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
