/**
 * EBIZCHARGE_GIFTCARD_PROCESSINGSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public interface EBIZCHARGE_GIFTCARD_PROCESSINGSoap extends java.rmi.Remote {
    public com.webjaguar.thirdparty.eBizCharge.GiftCardResponse GIFTCARD_ACTIVATION(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String program, com.webjaguar.thirdparty.eBizCharge.AccountDetailInput accountDetailInfo) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.GiftCardResponse GIFTCARD_REDEMPTION(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String program, com.webjaguar.thirdparty.eBizCharge.GCChargeDetailInput chargeDetailInfo) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.GiftCardResponse GIFTCARD_VOID(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String program, java.lang.String referenceNumber, java.lang.String giftCardNumber) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.GiftCardResponse GIFTCARD_BALANCE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String program, java.lang.String giftCardNumber) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.VERIFY_GIFTCARD_SERVICEResponseVERIFY_GIFTCARD_SERVICEResult VERIFY_GIFTCARD_SERVICE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP) throws java.rmi.RemoteException;
}
