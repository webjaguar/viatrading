/**
 * CustomerInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public class CustomerInput  implements java.io.Serializable {
    private com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo;

    private com.webjaguar.thirdparty.eBizCharge.PaymentMethodInput paymentMethod;

    private java.lang.String customerID;

    private java.lang.String customerDescription;

    public CustomerInput() {
    }

    public CustomerInput(
           com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo,
           com.webjaguar.thirdparty.eBizCharge.PaymentMethodInput paymentMethod,
           java.lang.String customerID,
           java.lang.String customerDescription) {
           this.billingDetailInfo = billingDetailInfo;
           this.paymentMethod = paymentMethod;
           this.customerID = customerID;
           this.customerDescription = customerDescription;
    }


    /**
     * Gets the billingDetailInfo value for this CustomerInput.
     * 
     * @return billingDetailInfo
     */
    public com.webjaguar.thirdparty.eBizCharge.CustDetailInput getBillingDetailInfo() {
        return billingDetailInfo;
    }


    /**
     * Sets the billingDetailInfo value for this CustomerInput.
     * 
     * @param billingDetailInfo
     */
    public void setBillingDetailInfo(com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo) {
        this.billingDetailInfo = billingDetailInfo;
    }


    /**
     * Gets the paymentMethod value for this CustomerInput.
     * 
     * @return paymentMethod
     */
    public com.webjaguar.thirdparty.eBizCharge.PaymentMethodInput getPaymentMethod() {
        return paymentMethod;
    }


    /**
     * Sets the paymentMethod value for this CustomerInput.
     * 
     * @param paymentMethod
     */
    public void setPaymentMethod(com.webjaguar.thirdparty.eBizCharge.PaymentMethodInput paymentMethod) {
        this.paymentMethod = paymentMethod;
    }


    /**
     * Gets the customerID value for this CustomerInput.
     * 
     * @return customerID
     */
    public java.lang.String getCustomerID() {
        return customerID;
    }


    /**
     * Sets the customerID value for this CustomerInput.
     * 
     * @param customerID
     */
    public void setCustomerID(java.lang.String customerID) {
        this.customerID = customerID;
    }


    /**
     * Gets the customerDescription value for this CustomerInput.
     * 
     * @return customerDescription
     */
    public java.lang.String getCustomerDescription() {
        return customerDescription;
    }


    /**
     * Sets the customerDescription value for this CustomerInput.
     * 
     * @param customerDescription
     */
    public void setCustomerDescription(java.lang.String customerDescription) {
        this.customerDescription = customerDescription;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomerInput)) return false;
        CustomerInput other = (CustomerInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.billingDetailInfo==null && other.getBillingDetailInfo()==null) || 
             (this.billingDetailInfo!=null &&
              this.billingDetailInfo.equals(other.getBillingDetailInfo()))) &&
            ((this.paymentMethod==null && other.getPaymentMethod()==null) || 
             (this.paymentMethod!=null &&
              this.paymentMethod.equals(other.getPaymentMethod()))) &&
            ((this.customerID==null && other.getCustomerID()==null) || 
             (this.customerID!=null &&
              this.customerID.equals(other.getCustomerID()))) &&
            ((this.customerDescription==null && other.getCustomerDescription()==null) || 
             (this.customerDescription!=null &&
              this.customerDescription.equals(other.getCustomerDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBillingDetailInfo() != null) {
            _hashCode += getBillingDetailInfo().hashCode();
        }
        if (getPaymentMethod() != null) {
            _hashCode += getPaymentMethod().hashCode();
        }
        if (getCustomerID() != null) {
            _hashCode += getCustomerID().hashCode();
        }
        if (getCustomerDescription() != null) {
            _hashCode += getCustomerDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomerInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billingDetailInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethod");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PaymentMethod"));
        elemField.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PaymentMethodInput"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerID");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
