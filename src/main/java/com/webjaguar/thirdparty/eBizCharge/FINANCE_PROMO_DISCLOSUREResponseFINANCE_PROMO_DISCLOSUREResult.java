/**
 * FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult  implements java.io.Serializable {
    private java.lang.String declineReason;

    private java.lang.String OFPLanguage;

    private java.lang.String status;

    public FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult() {
    }

    public FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult(
           java.lang.String declineReason,
           java.lang.String OFPLanguage,
           java.lang.String status) {
           this.declineReason = declineReason;
           this.OFPLanguage = OFPLanguage;
           this.status = status;
    }


    /**
     * Gets the declineReason value for this FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult.
     * 
     * @return declineReason
     */
    public java.lang.String getDeclineReason() {
        return declineReason;
    }


    /**
     * Sets the declineReason value for this FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult.
     * 
     * @param declineReason
     */
    public void setDeclineReason(java.lang.String declineReason) {
        this.declineReason = declineReason;
    }


    /**
     * Gets the OFPLanguage value for this FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult.
     * 
     * @return OFPLanguage
     */
    public java.lang.String getOFPLanguage() {
        return OFPLanguage;
    }


    /**
     * Sets the OFPLanguage value for this FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult.
     * 
     * @param OFPLanguage
     */
    public void setOFPLanguage(java.lang.String OFPLanguage) {
        this.OFPLanguage = OFPLanguage;
    }


    /**
     * Gets the status value for this FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult)) return false;
        FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult other = (FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.declineReason==null && other.getDeclineReason()==null) || 
             (this.declineReason!=null &&
              this.declineReason.equals(other.getDeclineReason()))) &&
            ((this.OFPLanguage==null && other.getOFPLanguage()==null) || 
             (this.OFPLanguage!=null &&
              this.OFPLanguage.equals(other.getOFPLanguage()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDeclineReason() != null) {
            _hashCode += getDeclineReason().hashCode();
        }
        if (getOFPLanguage() != null) {
            _hashCode += getOFPLanguage().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>FINANCE_PROMO_DISCLOSUREResponse>FINANCE_PROMO_DISCLOSUREResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("declineReason");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "DeclineReason"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OFPLanguage");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "OFPLanguage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
