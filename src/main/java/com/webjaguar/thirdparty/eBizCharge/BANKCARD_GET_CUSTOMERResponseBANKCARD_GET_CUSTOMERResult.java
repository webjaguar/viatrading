/**
 * BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public class BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult  implements java.io.Serializable {
    private com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo;

    private com.webjaguar.thirdparty.eBizCharge.PaymentMethodInput[] paymentMethods;

    private java.lang.String custDBNum;

    private java.lang.String customerDescription;

    private java.lang.String error;

    public BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult() {
    }

    public BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult(
           com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo,
           com.webjaguar.thirdparty.eBizCharge.PaymentMethodInput[] paymentMethods,
           java.lang.String custDBNum,
           java.lang.String customerDescription,
           java.lang.String error) {
           this.billingDetailInfo = billingDetailInfo;
           this.paymentMethods = paymentMethods;
           this.custDBNum = custDBNum;
           this.customerDescription = customerDescription;
           this.error = error;
    }


    /**
     * Gets the billingDetailInfo value for this BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.
     * 
     * @return billingDetailInfo
     */
    public com.webjaguar.thirdparty.eBizCharge.CustDetailInput getBillingDetailInfo() {
        return billingDetailInfo;
    }


    /**
     * Sets the billingDetailInfo value for this BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.
     * 
     * @param billingDetailInfo
     */
    public void setBillingDetailInfo(com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo) {
        this.billingDetailInfo = billingDetailInfo;
    }


    /**
     * Gets the paymentMethods value for this BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.
     * 
     * @return paymentMethods
     */
    public com.webjaguar.thirdparty.eBizCharge.PaymentMethodInput[] getPaymentMethods() {
        return paymentMethods;
    }


    /**
     * Sets the paymentMethods value for this BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.
     * 
     * @param paymentMethods
     */
    public void setPaymentMethods(com.webjaguar.thirdparty.eBizCharge.PaymentMethodInput[] paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public com.webjaguar.thirdparty.eBizCharge.PaymentMethodInput getPaymentMethods(int i) {
        return this.paymentMethods[i];
    }

    public void setPaymentMethods(int i, com.webjaguar.thirdparty.eBizCharge.PaymentMethodInput _value) {
        this.paymentMethods[i] = _value;
    }


    /**
     * Gets the custDBNum value for this BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.
     * 
     * @return custDBNum
     */
    public java.lang.String getCustDBNum() {
        return custDBNum;
    }


    /**
     * Sets the custDBNum value for this BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.
     * 
     * @param custDBNum
     */
    public void setCustDBNum(java.lang.String custDBNum) {
        this.custDBNum = custDBNum;
    }


    /**
     * Gets the customerDescription value for this BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.
     * 
     * @return customerDescription
     */
    public java.lang.String getCustomerDescription() {
        return customerDescription;
    }


    /**
     * Sets the customerDescription value for this BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.
     * 
     * @param customerDescription
     */
    public void setCustomerDescription(java.lang.String customerDescription) {
        this.customerDescription = customerDescription;
    }


    /**
     * Gets the error value for this BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult)) return false;
        BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult other = (BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.billingDetailInfo==null && other.getBillingDetailInfo()==null) || 
             (this.billingDetailInfo!=null &&
              this.billingDetailInfo.equals(other.getBillingDetailInfo()))) &&
            ((this.paymentMethods==null && other.getPaymentMethods()==null) || 
             (this.paymentMethods!=null &&
              java.util.Arrays.equals(this.paymentMethods, other.getPaymentMethods()))) &&
            ((this.custDBNum==null && other.getCustDBNum()==null) || 
             (this.custDBNum!=null &&
              this.custDBNum.equals(other.getCustDBNum()))) &&
            ((this.customerDescription==null && other.getCustomerDescription()==null) || 
             (this.customerDescription!=null &&
              this.customerDescription.equals(other.getCustomerDescription()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBillingDetailInfo() != null) {
            _hashCode += getBillingDetailInfo().hashCode();
        }
        if (getPaymentMethods() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPaymentMethods());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPaymentMethods(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCustDBNum() != null) {
            _hashCode += getCustDBNum().hashCode();
        }
        if (getCustomerDescription() != null) {
            _hashCode += getCustomerDescription().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_GET_CUSTOMERResponse>BANKCARD_GET_CUSTOMERResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("billingDetailInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMethods");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PaymentMethods"));
        elemField.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PaymentMethodInput"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custDBNum");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDBNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
