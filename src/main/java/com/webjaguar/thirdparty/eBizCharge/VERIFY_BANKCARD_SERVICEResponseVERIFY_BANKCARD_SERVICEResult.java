/**
 * VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult  implements java.io.Serializable {
    private java.lang.String serviceIndicator;

    private java.lang.String mode;

    public VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult() {
    }

    public VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult(
           java.lang.String serviceIndicator,
           java.lang.String mode) {
           this.serviceIndicator = serviceIndicator;
           this.mode = mode;
    }


    /**
     * Gets the serviceIndicator value for this VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult.
     * 
     * @return serviceIndicator
     */
    public java.lang.String getServiceIndicator() {
        return serviceIndicator;
    }


    /**
     * Sets the serviceIndicator value for this VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult.
     * 
     * @param serviceIndicator
     */
    public void setServiceIndicator(java.lang.String serviceIndicator) {
        this.serviceIndicator = serviceIndicator;
    }


    /**
     * Gets the mode value for this VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult.
     * 
     * @return mode
     */
    public java.lang.String getMode() {
        return mode;
    }


    /**
     * Sets the mode value for this VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult.
     * 
     * @param mode
     */
    public void setMode(java.lang.String mode) {
        this.mode = mode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult)) return false;
        VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult other = (VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.serviceIndicator==null && other.getServiceIndicator()==null) || 
             (this.serviceIndicator!=null &&
              this.serviceIndicator.equals(other.getServiceIndicator()))) &&
            ((this.mode==null && other.getMode()==null) || 
             (this.mode!=null &&
              this.mode.equals(other.getMode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getServiceIndicator() != null) {
            _hashCode += getServiceIndicator().hashCode();
        }
        if (getMode() != null) {
            _hashCode += getMode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(VERIFY_BANKCARD_SERVICEResponseVERIFY_BANKCARD_SERVICEResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VERIFY_BANKCARD_SERVICEResponse>VERIFY_BANKCARD_SERVICEResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serviceIndicator");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ServiceIndicator"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mode");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Mode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
