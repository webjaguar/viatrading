/**
 * EBIZCHARGE_LEVEL3_PROCESSINGSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public class EBIZCHARGE_LEVEL3_PROCESSINGSoapStub extends org.apache.axis.client.Stub implements com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_LEVEL3_PROCESSINGSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[7];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_LEVEL3_SALE");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_LEVEL3_SALEResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_LEVEL3_AUTHONLY");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_LEVEL3_AUTHONLYResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_LEVEL3_PRIOR_AUTH_SALE");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_LEVEL3_PRIOR_AUTH_SALEResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_LEVEL3_PHONE_AUTH_POST");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AuthCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_LEVEL3_PHONE_AUTH_POSTResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_LEVEL3_CREDIT");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput"), com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BillingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ShippingDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput"), com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput"), com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[].class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_LEVEL3_CREDITResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("BANKCARD_LEVEL3_VOID");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_LEVEL3_VOIDResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("VERIFY_BANKCARD_LEVEL3_SERVICE");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MerchantID"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInfo"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput"), com.webjaguar.thirdparty.eBizCharge.HashInput.class, false, false);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ClientIP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VERIFY_BANKCARD_LEVEL3_SERVICEResponse>VERIFY_BANKCARD_LEVEL3_SERVICEResult"));
        oper.setReturnClass(com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_LEVEL3_SERVICEResponseVERIFY_BANKCARD_LEVEL3_SERVICEResult.class);
        oper.setReturnQName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VERIFY_BANKCARD_LEVEL3_SERVICEResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

    }

    public EBIZCHARGE_LEVEL3_PROCESSINGSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public EBIZCHARGE_LEVEL3_PROCESSINGSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public EBIZCHARGE_LEVEL3_PROCESSINGSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>VERIFY_BANKCARD_LEVEL3_SERVICEResponse>VERIFY_BANKCARD_LEVEL3_SERVICEResult");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_LEVEL3_SERVICEResponseVERIFY_BANKCARD_LEVEL3_SERVICEResult.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BankCardResponse");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ChargeDetailInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustDetailInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.CustDetailInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HashInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.HashInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ItemDetailInput");
            cachedSerQNames.add(qName);
            cls = com.webjaguar.thirdparty.eBizCharge.ItemDetailInput.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_LEVEL3_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_LEVEL3_SALE");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_LEVEL3_SALE"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo, itemDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_LEVEL3_AUTHONLY(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_LEVEL3_AUTHONLY");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_LEVEL3_AUTHONLY"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_LEVEL3_PRIOR_AUTH_SALE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_LEVEL3_PRIOR_AUTH_SALE");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_LEVEL3_PRIOR_AUTH_SALE"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, referenceNumber, chargeDetailInfo, billingDetailInfo, shippingDetailInfo, itemDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_LEVEL3_PHONE_AUTH_POST(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String authCode, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_LEVEL3_PHONE_AUTH_POST");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_LEVEL3_PHONE_AUTH_POST"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, authCode, chargeDetailInfo, billingDetailInfo, shippingDetailInfo, itemDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_LEVEL3_CREDIT(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput chargeDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput billingDetailInfo, com.webjaguar.thirdparty.eBizCharge.CustDetailInput shippingDetailInfo, com.webjaguar.thirdparty.eBizCharge.ItemDetailInput[] itemDetailInfo) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_LEVEL3_CREDIT");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_LEVEL3_CREDIT"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, chargeDetailInfo, billingDetailInfo, shippingDetailInfo, itemDetailInfo});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.BankCardResponse BANKCARD_LEVEL3_VOID(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP, java.lang.String referenceNumber) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("BANKCARD_LEVEL3_VOID");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BANKCARD_LEVEL3_VOID"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP, referenceNumber});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.BankCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.BankCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_LEVEL3_SERVICEResponseVERIFY_BANKCARD_LEVEL3_SERVICEResult VERIFY_BANKCARD_LEVEL3_SERVICE(java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String clientIP) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("VERIFY_BANKCARD_LEVEL3_SERVICE");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "VERIFY_BANKCARD_LEVEL3_SERVICE"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {merchantID, hashInfo, clientIP});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_LEVEL3_SERVICEResponseVERIFY_BANKCARD_LEVEL3_SERVICEResult) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_LEVEL3_SERVICEResponseVERIFY_BANKCARD_LEVEL3_SERVICEResult) org.apache.axis.utils.JavaUtils.convert(_resp, com.webjaguar.thirdparty.eBizCharge.VERIFY_BANKCARD_LEVEL3_SERVICEResponseVERIFY_BANKCARD_LEVEL3_SERVICEResult.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
