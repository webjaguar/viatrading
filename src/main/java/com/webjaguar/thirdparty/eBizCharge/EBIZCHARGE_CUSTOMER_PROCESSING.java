/**
 * EBIZCHARGE_CUSTOMER_PROCESSING.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public interface EBIZCHARGE_CUSTOMER_PROCESSING extends javax.xml.rpc.Service {
    public java.lang.String getEBIZCHARGE_CUSTOMER_PROCESSINGSoapAddress();

    public com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoap getEBIZCHARGE_CUSTOMER_PROCESSINGSoap() throws javax.xml.rpc.ServiceException;

    public com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoap getEBIZCHARGE_CUSTOMER_PROCESSINGSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
