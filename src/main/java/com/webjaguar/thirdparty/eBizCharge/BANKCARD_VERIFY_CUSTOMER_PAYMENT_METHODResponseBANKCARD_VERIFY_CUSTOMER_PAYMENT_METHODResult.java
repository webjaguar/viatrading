/**
 * BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult  implements java.io.Serializable {
    private java.lang.String status;

    private java.math.BigInteger validFlag;

    private java.lang.String error;

    public BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult() {
    }

    public BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult(
           java.lang.String status,
           java.math.BigInteger validFlag,
           java.lang.String error) {
           this.status = status;
           this.validFlag = validFlag;
           this.error = error;
    }


    /**
     * Gets the status value for this BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the validFlag value for this BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @return validFlag
     */
    public java.math.BigInteger getValidFlag() {
        return validFlag;
    }


    /**
     * Sets the validFlag value for this BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @param validFlag
     */
    public void setValidFlag(java.math.BigInteger validFlag) {
        this.validFlag = validFlag;
    }


    /**
     * Gets the error value for this BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult)) return false;
        BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult other = (BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.validFlag==null && other.getValidFlag()==null) || 
             (this.validFlag!=null &&
              this.validFlag.equals(other.getValidFlag()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getValidFlag() != null) {
            _hashCode += getValidFlag().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponseBANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", ">>BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResponse>BANKCARD_VERIFY_CUSTOMER_PAYMENT_METHODResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validFlag");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ValidFlag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
