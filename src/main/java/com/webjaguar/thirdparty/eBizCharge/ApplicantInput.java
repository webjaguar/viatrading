/**
 * ApplicantInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class ApplicantInput  implements java.io.Serializable {
    private java.lang.String employerPhone;

    private java.lang.String frequency;

    private java.lang.String homePhone;

    private java.lang.String mobilePhone;

    private java.lang.String suffix;

    private java.lang.String address1;

    private java.lang.String address2;

    private java.lang.String city;

    private java.lang.String dob;

    private java.lang.String driversLicenseExpirationDate;

    private java.lang.String driversLicenseNumber;

    private java.lang.String driversLicenseState;

    private java.lang.String EMail;

    private java.lang.String firstName;

    private java.lang.String income;

    private java.lang.String lastName;

    private java.lang.String middleInitial;

    private java.lang.String residenceType;

    private java.lang.String ssn;

    private java.lang.String state;

    private java.lang.String zipCode;

    public ApplicantInput() {
    }

    public ApplicantInput(
           java.lang.String employerPhone,
           java.lang.String frequency,
           java.lang.String homePhone,
           java.lang.String mobilePhone,
           java.lang.String suffix,
           java.lang.String address1,
           java.lang.String address2,
           java.lang.String city,
           java.lang.String dob,
           java.lang.String driversLicenseExpirationDate,
           java.lang.String driversLicenseNumber,
           java.lang.String driversLicenseState,
           java.lang.String EMail,
           java.lang.String firstName,
           java.lang.String income,
           java.lang.String lastName,
           java.lang.String middleInitial,
           java.lang.String residenceType,
           java.lang.String ssn,
           java.lang.String state,
           java.lang.String zipCode) {
           this.employerPhone = employerPhone;
           this.frequency = frequency;
           this.homePhone = homePhone;
           this.mobilePhone = mobilePhone;
           this.suffix = suffix;
           this.address1 = address1;
           this.address2 = address2;
           this.city = city;
           this.dob = dob;
           this.driversLicenseExpirationDate = driversLicenseExpirationDate;
           this.driversLicenseNumber = driversLicenseNumber;
           this.driversLicenseState = driversLicenseState;
           this.EMail = EMail;
           this.firstName = firstName;
           this.income = income;
           this.lastName = lastName;
           this.middleInitial = middleInitial;
           this.residenceType = residenceType;
           this.ssn = ssn;
           this.state = state;
           this.zipCode = zipCode;
    }


    /**
     * Gets the employerPhone value for this ApplicantInput.
     * 
     * @return employerPhone
     */
    public java.lang.String getEmployerPhone() {
        return employerPhone;
    }


    /**
     * Sets the employerPhone value for this ApplicantInput.
     * 
     * @param employerPhone
     */
    public void setEmployerPhone(java.lang.String employerPhone) {
        this.employerPhone = employerPhone;
    }


    /**
     * Gets the frequency value for this ApplicantInput.
     * 
     * @return frequency
     */
    public java.lang.String getFrequency() {
        return frequency;
    }


    /**
     * Sets the frequency value for this ApplicantInput.
     * 
     * @param frequency
     */
    public void setFrequency(java.lang.String frequency) {
        this.frequency = frequency;
    }


    /**
     * Gets the homePhone value for this ApplicantInput.
     * 
     * @return homePhone
     */
    public java.lang.String getHomePhone() {
        return homePhone;
    }


    /**
     * Sets the homePhone value for this ApplicantInput.
     * 
     * @param homePhone
     */
    public void setHomePhone(java.lang.String homePhone) {
        this.homePhone = homePhone;
    }


    /**
     * Gets the mobilePhone value for this ApplicantInput.
     * 
     * @return mobilePhone
     */
    public java.lang.String getMobilePhone() {
        return mobilePhone;
    }


    /**
     * Sets the mobilePhone value for this ApplicantInput.
     * 
     * @param mobilePhone
     */
    public void setMobilePhone(java.lang.String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }


    /**
     * Gets the suffix value for this ApplicantInput.
     * 
     * @return suffix
     */
    public java.lang.String getSuffix() {
        return suffix;
    }


    /**
     * Sets the suffix value for this ApplicantInput.
     * 
     * @param suffix
     */
    public void setSuffix(java.lang.String suffix) {
        this.suffix = suffix;
    }


    /**
     * Gets the address1 value for this ApplicantInput.
     * 
     * @return address1
     */
    public java.lang.String getAddress1() {
        return address1;
    }


    /**
     * Sets the address1 value for this ApplicantInput.
     * 
     * @param address1
     */
    public void setAddress1(java.lang.String address1) {
        this.address1 = address1;
    }


    /**
     * Gets the address2 value for this ApplicantInput.
     * 
     * @return address2
     */
    public java.lang.String getAddress2() {
        return address2;
    }


    /**
     * Sets the address2 value for this ApplicantInput.
     * 
     * @param address2
     */
    public void setAddress2(java.lang.String address2) {
        this.address2 = address2;
    }


    /**
     * Gets the city value for this ApplicantInput.
     * 
     * @return city
     */
    public java.lang.String getCity() {
        return city;
    }


    /**
     * Sets the city value for this ApplicantInput.
     * 
     * @param city
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }


    /**
     * Gets the dob value for this ApplicantInput.
     * 
     * @return dob
     */
    public java.lang.String getDob() {
        return dob;
    }


    /**
     * Sets the dob value for this ApplicantInput.
     * 
     * @param dob
     */
    public void setDob(java.lang.String dob) {
        this.dob = dob;
    }


    /**
     * Gets the driversLicenseExpirationDate value for this ApplicantInput.
     * 
     * @return driversLicenseExpirationDate
     */
    public java.lang.String getDriversLicenseExpirationDate() {
        return driversLicenseExpirationDate;
    }


    /**
     * Sets the driversLicenseExpirationDate value for this ApplicantInput.
     * 
     * @param driversLicenseExpirationDate
     */
    public void setDriversLicenseExpirationDate(java.lang.String driversLicenseExpirationDate) {
        this.driversLicenseExpirationDate = driversLicenseExpirationDate;
    }


    /**
     * Gets the driversLicenseNumber value for this ApplicantInput.
     * 
     * @return driversLicenseNumber
     */
    public java.lang.String getDriversLicenseNumber() {
        return driversLicenseNumber;
    }


    /**
     * Sets the driversLicenseNumber value for this ApplicantInput.
     * 
     * @param driversLicenseNumber
     */
    public void setDriversLicenseNumber(java.lang.String driversLicenseNumber) {
        this.driversLicenseNumber = driversLicenseNumber;
    }


    /**
     * Gets the driversLicenseState value for this ApplicantInput.
     * 
     * @return driversLicenseState
     */
    public java.lang.String getDriversLicenseState() {
        return driversLicenseState;
    }


    /**
     * Sets the driversLicenseState value for this ApplicantInput.
     * 
     * @param driversLicenseState
     */
    public void setDriversLicenseState(java.lang.String driversLicenseState) {
        this.driversLicenseState = driversLicenseState;
    }


    /**
     * Gets the EMail value for this ApplicantInput.
     * 
     * @return EMail
     */
    public java.lang.String getEMail() {
        return EMail;
    }


    /**
     * Sets the EMail value for this ApplicantInput.
     * 
     * @param EMail
     */
    public void setEMail(java.lang.String EMail) {
        this.EMail = EMail;
    }


    /**
     * Gets the firstName value for this ApplicantInput.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this ApplicantInput.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the income value for this ApplicantInput.
     * 
     * @return income
     */
    public java.lang.String getIncome() {
        return income;
    }


    /**
     * Sets the income value for this ApplicantInput.
     * 
     * @param income
     */
    public void setIncome(java.lang.String income) {
        this.income = income;
    }


    /**
     * Gets the lastName value for this ApplicantInput.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this ApplicantInput.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the middleInitial value for this ApplicantInput.
     * 
     * @return middleInitial
     */
    public java.lang.String getMiddleInitial() {
        return middleInitial;
    }


    /**
     * Sets the middleInitial value for this ApplicantInput.
     * 
     * @param middleInitial
     */
    public void setMiddleInitial(java.lang.String middleInitial) {
        this.middleInitial = middleInitial;
    }


    /**
     * Gets the residenceType value for this ApplicantInput.
     * 
     * @return residenceType
     */
    public java.lang.String getResidenceType() {
        return residenceType;
    }


    /**
     * Sets the residenceType value for this ApplicantInput.
     * 
     * @param residenceType
     */
    public void setResidenceType(java.lang.String residenceType) {
        this.residenceType = residenceType;
    }


    /**
     * Gets the ssn value for this ApplicantInput.
     * 
     * @return ssn
     */
    public java.lang.String getSsn() {
        return ssn;
    }


    /**
     * Sets the ssn value for this ApplicantInput.
     * 
     * @param ssn
     */
    public void setSsn(java.lang.String ssn) {
        this.ssn = ssn;
    }


    /**
     * Gets the state value for this ApplicantInput.
     * 
     * @return state
     */
    public java.lang.String getState() {
        return state;
    }


    /**
     * Sets the state value for this ApplicantInput.
     * 
     * @param state
     */
    public void setState(java.lang.String state) {
        this.state = state;
    }


    /**
     * Gets the zipCode value for this ApplicantInput.
     * 
     * @return zipCode
     */
    public java.lang.String getZipCode() {
        return zipCode;
    }


    /**
     * Sets the zipCode value for this ApplicantInput.
     * 
     * @param zipCode
     */
    public void setZipCode(java.lang.String zipCode) {
        this.zipCode = zipCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ApplicantInput)) return false;
        ApplicantInput other = (ApplicantInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.employerPhone==null && other.getEmployerPhone()==null) || 
             (this.employerPhone!=null &&
              this.employerPhone.equals(other.getEmployerPhone()))) &&
            ((this.frequency==null && other.getFrequency()==null) || 
             (this.frequency!=null &&
              this.frequency.equals(other.getFrequency()))) &&
            ((this.homePhone==null && other.getHomePhone()==null) || 
             (this.homePhone!=null &&
              this.homePhone.equals(other.getHomePhone()))) &&
            ((this.mobilePhone==null && other.getMobilePhone()==null) || 
             (this.mobilePhone!=null &&
              this.mobilePhone.equals(other.getMobilePhone()))) &&
            ((this.suffix==null && other.getSuffix()==null) || 
             (this.suffix!=null &&
              this.suffix.equals(other.getSuffix()))) &&
            ((this.address1==null && other.getAddress1()==null) || 
             (this.address1!=null &&
              this.address1.equals(other.getAddress1()))) &&
            ((this.address2==null && other.getAddress2()==null) || 
             (this.address2!=null &&
              this.address2.equals(other.getAddress2()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.dob==null && other.getDob()==null) || 
             (this.dob!=null &&
              this.dob.equals(other.getDob()))) &&
            ((this.driversLicenseExpirationDate==null && other.getDriversLicenseExpirationDate()==null) || 
             (this.driversLicenseExpirationDate!=null &&
              this.driversLicenseExpirationDate.equals(other.getDriversLicenseExpirationDate()))) &&
            ((this.driversLicenseNumber==null && other.getDriversLicenseNumber()==null) || 
             (this.driversLicenseNumber!=null &&
              this.driversLicenseNumber.equals(other.getDriversLicenseNumber()))) &&
            ((this.driversLicenseState==null && other.getDriversLicenseState()==null) || 
             (this.driversLicenseState!=null &&
              this.driversLicenseState.equals(other.getDriversLicenseState()))) &&
            ((this.EMail==null && other.getEMail()==null) || 
             (this.EMail!=null &&
              this.EMail.equals(other.getEMail()))) &&
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.income==null && other.getIncome()==null) || 
             (this.income!=null &&
              this.income.equals(other.getIncome()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.middleInitial==null && other.getMiddleInitial()==null) || 
             (this.middleInitial!=null &&
              this.middleInitial.equals(other.getMiddleInitial()))) &&
            ((this.residenceType==null && other.getResidenceType()==null) || 
             (this.residenceType!=null &&
              this.residenceType.equals(other.getResidenceType()))) &&
            ((this.ssn==null && other.getSsn()==null) || 
             (this.ssn!=null &&
              this.ssn.equals(other.getSsn()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.zipCode==null && other.getZipCode()==null) || 
             (this.zipCode!=null &&
              this.zipCode.equals(other.getZipCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEmployerPhone() != null) {
            _hashCode += getEmployerPhone().hashCode();
        }
        if (getFrequency() != null) {
            _hashCode += getFrequency().hashCode();
        }
        if (getHomePhone() != null) {
            _hashCode += getHomePhone().hashCode();
        }
        if (getMobilePhone() != null) {
            _hashCode += getMobilePhone().hashCode();
        }
        if (getSuffix() != null) {
            _hashCode += getSuffix().hashCode();
        }
        if (getAddress1() != null) {
            _hashCode += getAddress1().hashCode();
        }
        if (getAddress2() != null) {
            _hashCode += getAddress2().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getDob() != null) {
            _hashCode += getDob().hashCode();
        }
        if (getDriversLicenseExpirationDate() != null) {
            _hashCode += getDriversLicenseExpirationDate().hashCode();
        }
        if (getDriversLicenseNumber() != null) {
            _hashCode += getDriversLicenseNumber().hashCode();
        }
        if (getDriversLicenseState() != null) {
            _hashCode += getDriversLicenseState().hashCode();
        }
        if (getEMail() != null) {
            _hashCode += getEMail().hashCode();
        }
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getIncome() != null) {
            _hashCode += getIncome().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getMiddleInitial() != null) {
            _hashCode += getMiddleInitial().hashCode();
        }
        if (getResidenceType() != null) {
            _hashCode += getResidenceType().hashCode();
        }
        if (getSsn() != null) {
            _hashCode += getSsn().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getZipCode() != null) {
            _hashCode += getZipCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ApplicantInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ApplicantInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("employerPhone");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "EmployerPhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("frequency");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Frequency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("homePhone");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "HomePhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mobilePhone");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MobilePhone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("suffix");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Suffix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address1");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Address1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address2");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Address2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "City"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dob");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Dob"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("driversLicenseExpirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "DriversLicenseExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("driversLicenseNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "DriversLicenseNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("driversLicenseState");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "DriversLicenseState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMail");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "EMail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstName");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "FirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("income");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Income"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "LastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("middleInitial");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MiddleInitial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("residenceType");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ResidenceType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ssn");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Ssn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "State"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ZipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
