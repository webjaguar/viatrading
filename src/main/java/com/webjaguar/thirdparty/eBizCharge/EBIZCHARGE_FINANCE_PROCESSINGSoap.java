/**
 * EBIZCHARGE_FINANCE_PROCESSINGSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;


public interface EBIZCHARGE_FINANCE_PROCESSINGSoap extends java.rmi.Remote {
    public com.webjaguar.thirdparty.eBizCharge.FINANCE_APPLICATIONResponseFINANCE_APPLICATIONResult[] FINANCE_APPLICATION(java.lang.String userID, java.lang.String password, java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String accountSecurity, com.webjaguar.thirdparty.eBizCharge.ApplicantInput applicantInfo, com.webjaguar.thirdparty.eBizCharge.ApplicantInput coApplicantInfo, java.lang.String mothersMaidenName, java.lang.String nearestRelPhoneNumber, java.lang.String operationCode, java.lang.String orginationCode, java.lang.String productCode, java.math.BigDecimal purchaseAmount) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.FINANCE_PROMO_DISCLOSUREResponseFINANCE_PROMO_DISCLOSUREResult[] FINANCE_PROMO_DISCLOSURE(java.lang.String userID, java.lang.String password, java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String accountNumber, java.lang.String productCode, java.math.BigDecimal purchaseAmount) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.FINANCE_AUTHORIZATIONResponseFINANCE_AUTHORIZATIONResult[] FINANCE_AUTHORIZATION(java.lang.String userID, java.lang.String password, java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String accountNumber, java.lang.String address1, java.lang.String address2, java.lang.String expirationMonth, java.lang.String expirationYear, java.lang.String promoCode, java.lang.String promoDisclosure, java.math.BigDecimal purchaseAmount, java.lang.String zipCode) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.FINANCE_REVERSE_AUTHResponseFINANCE_REVERSE_AUTHResult[] FINANCE_REVERSE_AUTH(java.lang.String userID, java.lang.String password, java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo, java.lang.String accountNumber, java.lang.String authCode, java.math.BigDecimal authReversalAmount, java.lang.String transactionId) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.FINANCE_SETTLEMENTResponseFINANCE_SETTLEMENTResult[] FINANCE_SETTLEMENT(java.lang.String userID, java.lang.String password, java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo) throws java.rmi.RemoteException;
    public com.webjaguar.thirdparty.eBizCharge.FINANCE_RECONCILEResponseFINANCE_RECONCILEResult[] FINANCE_RECONCILE(java.lang.String userID, java.lang.String password, java.lang.String merchantID, com.webjaguar.thirdparty.eBizCharge.HashInput hashInfo) throws java.rmi.RemoteException;
}
