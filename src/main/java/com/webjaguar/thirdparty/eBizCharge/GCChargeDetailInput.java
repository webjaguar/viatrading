/**
 * GCChargeDetailInput.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class GCChargeDetailInput  implements java.io.Serializable {
    private java.lang.String cardHolder;

    private java.lang.String giftCardNumber;

    private java.lang.String magStripeData;

    private java.lang.String entryMode;

    private java.lang.String customerNumber;

    private java.lang.String amount;

    private java.lang.String tax;

    private java.lang.String orderNumber;

    public GCChargeDetailInput() {
    }

    public GCChargeDetailInput(
           java.lang.String cardHolder,
           java.lang.String giftCardNumber,
           java.lang.String magStripeData,
           java.lang.String entryMode,
           java.lang.String customerNumber,
           java.lang.String amount,
           java.lang.String tax,
           java.lang.String orderNumber) {
           this.cardHolder = cardHolder;
           this.giftCardNumber = giftCardNumber;
           this.magStripeData = magStripeData;
           this.entryMode = entryMode;
           this.customerNumber = customerNumber;
           this.amount = amount;
           this.tax = tax;
           this.orderNumber = orderNumber;
    }


    /**
     * Gets the cardHolder value for this GCChargeDetailInput.
     * 
     * @return cardHolder
     */
    public java.lang.String getCardHolder() {
        return cardHolder;
    }


    /**
     * Sets the cardHolder value for this GCChargeDetailInput.
     * 
     * @param cardHolder
     */
    public void setCardHolder(java.lang.String cardHolder) {
        this.cardHolder = cardHolder;
    }


    /**
     * Gets the giftCardNumber value for this GCChargeDetailInput.
     * 
     * @return giftCardNumber
     */
    public java.lang.String getGiftCardNumber() {
        return giftCardNumber;
    }


    /**
     * Sets the giftCardNumber value for this GCChargeDetailInput.
     * 
     * @param giftCardNumber
     */
    public void setGiftCardNumber(java.lang.String giftCardNumber) {
        this.giftCardNumber = giftCardNumber;
    }


    /**
     * Gets the magStripeData value for this GCChargeDetailInput.
     * 
     * @return magStripeData
     */
    public java.lang.String getMagStripeData() {
        return magStripeData;
    }


    /**
     * Sets the magStripeData value for this GCChargeDetailInput.
     * 
     * @param magStripeData
     */
    public void setMagStripeData(java.lang.String magStripeData) {
        this.magStripeData = magStripeData;
    }


    /**
     * Gets the entryMode value for this GCChargeDetailInput.
     * 
     * @return entryMode
     */
    public java.lang.String getEntryMode() {
        return entryMode;
    }


    /**
     * Sets the entryMode value for this GCChargeDetailInput.
     * 
     * @param entryMode
     */
    public void setEntryMode(java.lang.String entryMode) {
        this.entryMode = entryMode;
    }


    /**
     * Gets the customerNumber value for this GCChargeDetailInput.
     * 
     * @return customerNumber
     */
    public java.lang.String getCustomerNumber() {
        return customerNumber;
    }


    /**
     * Sets the customerNumber value for this GCChargeDetailInput.
     * 
     * @param customerNumber
     */
    public void setCustomerNumber(java.lang.String customerNumber) {
        this.customerNumber = customerNumber;
    }


    /**
     * Gets the amount value for this GCChargeDetailInput.
     * 
     * @return amount
     */
    public java.lang.String getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this GCChargeDetailInput.
     * 
     * @param amount
     */
    public void setAmount(java.lang.String amount) {
        this.amount = amount;
    }


    /**
     * Gets the tax value for this GCChargeDetailInput.
     * 
     * @return tax
     */
    public java.lang.String getTax() {
        return tax;
    }


    /**
     * Sets the tax value for this GCChargeDetailInput.
     * 
     * @param tax
     */
    public void setTax(java.lang.String tax) {
        this.tax = tax;
    }


    /**
     * Gets the orderNumber value for this GCChargeDetailInput.
     * 
     * @return orderNumber
     */
    public java.lang.String getOrderNumber() {
        return orderNumber;
    }


    /**
     * Sets the orderNumber value for this GCChargeDetailInput.
     * 
     * @param orderNumber
     */
    public void setOrderNumber(java.lang.String orderNumber) {
        this.orderNumber = orderNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GCChargeDetailInput)) return false;
        GCChargeDetailInput other = (GCChargeDetailInput) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cardHolder==null && other.getCardHolder()==null) || 
             (this.cardHolder!=null &&
              this.cardHolder.equals(other.getCardHolder()))) &&
            ((this.giftCardNumber==null && other.getGiftCardNumber()==null) || 
             (this.giftCardNumber!=null &&
              this.giftCardNumber.equals(other.getGiftCardNumber()))) &&
            ((this.magStripeData==null && other.getMagStripeData()==null) || 
             (this.magStripeData!=null &&
              this.magStripeData.equals(other.getMagStripeData()))) &&
            ((this.entryMode==null && other.getEntryMode()==null) || 
             (this.entryMode!=null &&
              this.entryMode.equals(other.getEntryMode()))) &&
            ((this.customerNumber==null && other.getCustomerNumber()==null) || 
             (this.customerNumber!=null &&
              this.customerNumber.equals(other.getCustomerNumber()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.tax==null && other.getTax()==null) || 
             (this.tax!=null &&
              this.tax.equals(other.getTax()))) &&
            ((this.orderNumber==null && other.getOrderNumber()==null) || 
             (this.orderNumber!=null &&
              this.orderNumber.equals(other.getOrderNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardHolder() != null) {
            _hashCode += getCardHolder().hashCode();
        }
        if (getGiftCardNumber() != null) {
            _hashCode += getGiftCardNumber().hashCode();
        }
        if (getMagStripeData() != null) {
            _hashCode += getMagStripeData().hashCode();
        }
        if (getEntryMode() != null) {
            _hashCode += getEntryMode().hashCode();
        }
        if (getCustomerNumber() != null) {
            _hashCode += getCustomerNumber().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getTax() != null) {
            _hashCode += getTax().hashCode();
        }
        if (getOrderNumber() != null) {
            _hashCode += getOrderNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GCChargeDetailInput.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GCChargeDetailInput"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardHolder");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CardHolder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GiftCardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("magStripeData");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "MagStripeData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entryMode");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "EntryMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "CustomerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tax");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Tax"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "OrderNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
