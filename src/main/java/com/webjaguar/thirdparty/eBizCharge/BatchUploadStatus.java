/**
 * BatchUploadStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class BatchUploadStatus  implements java.io.Serializable {
    private java.math.BigInteger uploadRefNum;

    private java.lang.String status;

    private java.lang.String started;

    private java.lang.String finished;

    private java.math.BigInteger transactions;

    private java.math.BigInteger remaining;

    private java.lang.String approved;

    private java.lang.String declined;

    private java.lang.String errors;

    public BatchUploadStatus() {
    }

    public BatchUploadStatus(
           java.math.BigInteger uploadRefNum,
           java.lang.String status,
           java.lang.String started,
           java.lang.String finished,
           java.math.BigInteger transactions,
           java.math.BigInteger remaining,
           java.lang.String approved,
           java.lang.String declined,
           java.lang.String errors) {
           this.uploadRefNum = uploadRefNum;
           this.status = status;
           this.started = started;
           this.finished = finished;
           this.transactions = transactions;
           this.remaining = remaining;
           this.approved = approved;
           this.declined = declined;
           this.errors = errors;
    }


    /**
     * Gets the uploadRefNum value for this BatchUploadStatus.
     * 
     * @return uploadRefNum
     */
    public java.math.BigInteger getUploadRefNum() {
        return uploadRefNum;
    }


    /**
     * Sets the uploadRefNum value for this BatchUploadStatus.
     * 
     * @param uploadRefNum
     */
    public void setUploadRefNum(java.math.BigInteger uploadRefNum) {
        this.uploadRefNum = uploadRefNum;
    }


    /**
     * Gets the status value for this BatchUploadStatus.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this BatchUploadStatus.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the started value for this BatchUploadStatus.
     * 
     * @return started
     */
    public java.lang.String getStarted() {
        return started;
    }


    /**
     * Sets the started value for this BatchUploadStatus.
     * 
     * @param started
     */
    public void setStarted(java.lang.String started) {
        this.started = started;
    }


    /**
     * Gets the finished value for this BatchUploadStatus.
     * 
     * @return finished
     */
    public java.lang.String getFinished() {
        return finished;
    }


    /**
     * Sets the finished value for this BatchUploadStatus.
     * 
     * @param finished
     */
    public void setFinished(java.lang.String finished) {
        this.finished = finished;
    }


    /**
     * Gets the transactions value for this BatchUploadStatus.
     * 
     * @return transactions
     */
    public java.math.BigInteger getTransactions() {
        return transactions;
    }


    /**
     * Sets the transactions value for this BatchUploadStatus.
     * 
     * @param transactions
     */
    public void setTransactions(java.math.BigInteger transactions) {
        this.transactions = transactions;
    }


    /**
     * Gets the remaining value for this BatchUploadStatus.
     * 
     * @return remaining
     */
    public java.math.BigInteger getRemaining() {
        return remaining;
    }


    /**
     * Sets the remaining value for this BatchUploadStatus.
     * 
     * @param remaining
     */
    public void setRemaining(java.math.BigInteger remaining) {
        this.remaining = remaining;
    }


    /**
     * Gets the approved value for this BatchUploadStatus.
     * 
     * @return approved
     */
    public java.lang.String getApproved() {
        return approved;
    }


    /**
     * Sets the approved value for this BatchUploadStatus.
     * 
     * @param approved
     */
    public void setApproved(java.lang.String approved) {
        this.approved = approved;
    }


    /**
     * Gets the declined value for this BatchUploadStatus.
     * 
     * @return declined
     */
    public java.lang.String getDeclined() {
        return declined;
    }


    /**
     * Sets the declined value for this BatchUploadStatus.
     * 
     * @param declined
     */
    public void setDeclined(java.lang.String declined) {
        this.declined = declined;
    }


    /**
     * Gets the errors value for this BatchUploadStatus.
     * 
     * @return errors
     */
    public java.lang.String getErrors() {
        return errors;
    }


    /**
     * Sets the errors value for this BatchUploadStatus.
     * 
     * @param errors
     */
    public void setErrors(java.lang.String errors) {
        this.errors = errors;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BatchUploadStatus)) return false;
        BatchUploadStatus other = (BatchUploadStatus) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.uploadRefNum==null && other.getUploadRefNum()==null) || 
             (this.uploadRefNum!=null &&
              this.uploadRefNum.equals(other.getUploadRefNum()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.started==null && other.getStarted()==null) || 
             (this.started!=null &&
              this.started.equals(other.getStarted()))) &&
            ((this.finished==null && other.getFinished()==null) || 
             (this.finished!=null &&
              this.finished.equals(other.getFinished()))) &&
            ((this.transactions==null && other.getTransactions()==null) || 
             (this.transactions!=null &&
              this.transactions.equals(other.getTransactions()))) &&
            ((this.remaining==null && other.getRemaining()==null) || 
             (this.remaining!=null &&
              this.remaining.equals(other.getRemaining()))) &&
            ((this.approved==null && other.getApproved()==null) || 
             (this.approved!=null &&
              this.approved.equals(other.getApproved()))) &&
            ((this.declined==null && other.getDeclined()==null) || 
             (this.declined!=null &&
              this.declined.equals(other.getDeclined()))) &&
            ((this.errors==null && other.getErrors()==null) || 
             (this.errors!=null &&
              this.errors.equals(other.getErrors())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUploadRefNum() != null) {
            _hashCode += getUploadRefNum().hashCode();
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getStarted() != null) {
            _hashCode += getStarted().hashCode();
        }
        if (getFinished() != null) {
            _hashCode += getFinished().hashCode();
        }
        if (getTransactions() != null) {
            _hashCode += getTransactions().hashCode();
        }
        if (getRemaining() != null) {
            _hashCode += getRemaining().hashCode();
        }
        if (getApproved() != null) {
            _hashCode += getApproved().hashCode();
        }
        if (getDeclined() != null) {
            _hashCode += getDeclined().hashCode();
        }
        if (getErrors() != null) {
            _hashCode += getErrors().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BatchUploadStatus.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "BatchUploadStatus"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("uploadRefNum");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "UploadRefNum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("started");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Started"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("finished");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Finished"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactions");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Transactions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("remaining");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Remaining"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approved");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Approved"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("declined");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Declined"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errors");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Errors"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
