/**
 * GiftCardResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.webjaguar.thirdparty.eBizCharge;

public class GiftCardResponse  implements java.io.Serializable {
    private java.lang.String status;

    private java.lang.String authCode;

    private java.lang.String referenceNumber;

    private java.lang.String result;

    private java.lang.String errorCode;

    private java.lang.String error;

    private java.lang.String command;

    private java.lang.String giftCardBalance;

    private java.lang.String pointsBalance;

    private java.lang.String rewardCashBalance;

    private java.lang.String amount;

    private java.lang.String card;

    private java.lang.String invoice;

    public GiftCardResponse() {
    }

    public GiftCardResponse(
           java.lang.String status,
           java.lang.String authCode,
           java.lang.String referenceNumber,
           java.lang.String result,
           java.lang.String errorCode,
           java.lang.String error,
           java.lang.String command,
           java.lang.String giftCardBalance,
           java.lang.String pointsBalance,
           java.lang.String rewardCashBalance,
           java.lang.String amount,
           java.lang.String card,
           java.lang.String invoice) {
           this.status = status;
           this.authCode = authCode;
           this.referenceNumber = referenceNumber;
           this.result = result;
           this.errorCode = errorCode;
           this.error = error;
           this.command = command;
           this.giftCardBalance = giftCardBalance;
           this.pointsBalance = pointsBalance;
           this.rewardCashBalance = rewardCashBalance;
           this.amount = amount;
           this.card = card;
           this.invoice = invoice;
    }


    /**
     * Gets the status value for this GiftCardResponse.
     * 
     * @return status
     */
    public java.lang.String getStatus() {
        return status;
    }


    /**
     * Sets the status value for this GiftCardResponse.
     * 
     * @param status
     */
    public void setStatus(java.lang.String status) {
        this.status = status;
    }


    /**
     * Gets the authCode value for this GiftCardResponse.
     * 
     * @return authCode
     */
    public java.lang.String getAuthCode() {
        return authCode;
    }


    /**
     * Sets the authCode value for this GiftCardResponse.
     * 
     * @param authCode
     */
    public void setAuthCode(java.lang.String authCode) {
        this.authCode = authCode;
    }


    /**
     * Gets the referenceNumber value for this GiftCardResponse.
     * 
     * @return referenceNumber
     */
    public java.lang.String getReferenceNumber() {
        return referenceNumber;
    }


    /**
     * Sets the referenceNumber value for this GiftCardResponse.
     * 
     * @param referenceNumber
     */
    public void setReferenceNumber(java.lang.String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }


    /**
     * Gets the result value for this GiftCardResponse.
     * 
     * @return result
     */
    public java.lang.String getResult() {
        return result;
    }


    /**
     * Sets the result value for this GiftCardResponse.
     * 
     * @param result
     */
    public void setResult(java.lang.String result) {
        this.result = result;
    }


    /**
     * Gets the errorCode value for this GiftCardResponse.
     * 
     * @return errorCode
     */
    public java.lang.String getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this GiftCardResponse.
     * 
     * @param errorCode
     */
    public void setErrorCode(java.lang.String errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the error value for this GiftCardResponse.
     * 
     * @return error
     */
    public java.lang.String getError() {
        return error;
    }


    /**
     * Sets the error value for this GiftCardResponse.
     * 
     * @param error
     */
    public void setError(java.lang.String error) {
        this.error = error;
    }


    /**
     * Gets the command value for this GiftCardResponse.
     * 
     * @return command
     */
    public java.lang.String getCommand() {
        return command;
    }


    /**
     * Sets the command value for this GiftCardResponse.
     * 
     * @param command
     */
    public void setCommand(java.lang.String command) {
        this.command = command;
    }


    /**
     * Gets the giftCardBalance value for this GiftCardResponse.
     * 
     * @return giftCardBalance
     */
    public java.lang.String getGiftCardBalance() {
        return giftCardBalance;
    }


    /**
     * Sets the giftCardBalance value for this GiftCardResponse.
     * 
     * @param giftCardBalance
     */
    public void setGiftCardBalance(java.lang.String giftCardBalance) {
        this.giftCardBalance = giftCardBalance;
    }


    /**
     * Gets the pointsBalance value for this GiftCardResponse.
     * 
     * @return pointsBalance
     */
    public java.lang.String getPointsBalance() {
        return pointsBalance;
    }


    /**
     * Sets the pointsBalance value for this GiftCardResponse.
     * 
     * @param pointsBalance
     */
    public void setPointsBalance(java.lang.String pointsBalance) {
        this.pointsBalance = pointsBalance;
    }


    /**
     * Gets the rewardCashBalance value for this GiftCardResponse.
     * 
     * @return rewardCashBalance
     */
    public java.lang.String getRewardCashBalance() {
        return rewardCashBalance;
    }


    /**
     * Sets the rewardCashBalance value for this GiftCardResponse.
     * 
     * @param rewardCashBalance
     */
    public void setRewardCashBalance(java.lang.String rewardCashBalance) {
        this.rewardCashBalance = rewardCashBalance;
    }


    /**
     * Gets the amount value for this GiftCardResponse.
     * 
     * @return amount
     */
    public java.lang.String getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this GiftCardResponse.
     * 
     * @param amount
     */
    public void setAmount(java.lang.String amount) {
        this.amount = amount;
    }


    /**
     * Gets the card value for this GiftCardResponse.
     * 
     * @return card
     */
    public java.lang.String getCard() {
        return card;
    }


    /**
     * Sets the card value for this GiftCardResponse.
     * 
     * @param card
     */
    public void setCard(java.lang.String card) {
        this.card = card;
    }


    /**
     * Gets the invoice value for this GiftCardResponse.
     * 
     * @return invoice
     */
    public java.lang.String getInvoice() {
        return invoice;
    }


    /**
     * Sets the invoice value for this GiftCardResponse.
     * 
     * @param invoice
     */
    public void setInvoice(java.lang.String invoice) {
        this.invoice = invoice;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GiftCardResponse)) return false;
        GiftCardResponse other = (GiftCardResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus()))) &&
            ((this.authCode==null && other.getAuthCode()==null) || 
             (this.authCode!=null &&
              this.authCode.equals(other.getAuthCode()))) &&
            ((this.referenceNumber==null && other.getReferenceNumber()==null) || 
             (this.referenceNumber!=null &&
              this.referenceNumber.equals(other.getReferenceNumber()))) &&
            ((this.result==null && other.getResult()==null) || 
             (this.result!=null &&
              this.result.equals(other.getResult()))) &&
            ((this.errorCode==null && other.getErrorCode()==null) || 
             (this.errorCode!=null &&
              this.errorCode.equals(other.getErrorCode()))) &&
            ((this.error==null && other.getError()==null) || 
             (this.error!=null &&
              this.error.equals(other.getError()))) &&
            ((this.command==null && other.getCommand()==null) || 
             (this.command!=null &&
              this.command.equals(other.getCommand()))) &&
            ((this.giftCardBalance==null && other.getGiftCardBalance()==null) || 
             (this.giftCardBalance!=null &&
              this.giftCardBalance.equals(other.getGiftCardBalance()))) &&
            ((this.pointsBalance==null && other.getPointsBalance()==null) || 
             (this.pointsBalance!=null &&
              this.pointsBalance.equals(other.getPointsBalance()))) &&
            ((this.rewardCashBalance==null && other.getRewardCashBalance()==null) || 
             (this.rewardCashBalance!=null &&
              this.rewardCashBalance.equals(other.getRewardCashBalance()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.card==null && other.getCard()==null) || 
             (this.card!=null &&
              this.card.equals(other.getCard()))) &&
            ((this.invoice==null && other.getInvoice()==null) || 
             (this.invoice!=null &&
              this.invoice.equals(other.getInvoice())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        if (getAuthCode() != null) {
            _hashCode += getAuthCode().hashCode();
        }
        if (getReferenceNumber() != null) {
            _hashCode += getReferenceNumber().hashCode();
        }
        if (getResult() != null) {
            _hashCode += getResult().hashCode();
        }
        if (getErrorCode() != null) {
            _hashCode += getErrorCode().hashCode();
        }
        if (getError() != null) {
            _hashCode += getError().hashCode();
        }
        if (getCommand() != null) {
            _hashCode += getCommand().hashCode();
        }
        if (getGiftCardBalance() != null) {
            _hashCode += getGiftCardBalance().hashCode();
        }
        if (getPointsBalance() != null) {
            _hashCode += getPointsBalance().hashCode();
        }
        if (getRewardCashBalance() != null) {
            _hashCode += getRewardCashBalance().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getCard() != null) {
            _hashCode += getCard().hashCode();
        }
        if (getInvoice() != null) {
            _hashCode += getInvoice().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GiftCardResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GiftCardResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authCode");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "AuthCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ReferenceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Result"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "ErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Error"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("command");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Command"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("giftCardBalance");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "GiftCardBalance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointsBalance");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "PointsBalance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rewardCashBalance");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "RewardCashBalance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("card");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Card"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("invoice");
        elemField.setXmlName(new javax.xml.namespace.QName("EBIZCHARGE_TRANSACTION_PROCESSING", "Invoice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
