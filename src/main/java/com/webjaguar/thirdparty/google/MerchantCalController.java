package com.webjaguar.thirdparty.google;



import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.io.OutputStream;


import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;


import com.google.checkout.notification.NewOrderNotification;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Order;
import com.webjaguar.model.ShippingRate;



public class MerchantCalController implements Controller{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	
	
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
			
			Map<String, Object>  gSiteConfig = globalDao.getGlobalSiteConfig();
			NewOrderNotification notification = new NewOrderNotification(request.getInputStream());
			Document doc =  notification.getDocument();
			
			
			// POST data
			StringBuffer req = new StringBuffer();
			req.append(parseXmlNotification(doc, gSiteConfig, request));
			
			OutputStream oStream = response.getOutputStream();
			oStream.write(req.toString().getBytes());
			oStream.close();
			
			return null;
	}
	
	
	
	private StringBuilder parseXmlNotification( Document doc, Map<String, Object> gSiteConfig, HttpServletRequest request ) throws Exception {
		
		doc.getDocumentElement().normalize();
		request.setAttribute( "gSiteConfig", gSiteConfig );
		
		Integer orderId =  Integer.parseInt( doc.getElementsByTagName("merchant-note").item(0).getTextContent() );
		Long addressId = Long.parseLong( doc.getElementsByTagName("anonymous-address").item(0).getAttributes().getNamedItem("id").getNodeValue() );
		String shippingMethod = doc.getElementsByTagName("method").item(0).getAttributes().getNamedItem("name").getNodeValue();
		
		Order order = this.webJaguar.getOrder(orderId, null);
		
		// get total weight
		NodeList itemWeight = doc.getElementsByTagName("item-weight");
		Double totalWeight = 0.0;
		for(int i=0; i<itemWeight.getLength(); i++){
			totalWeight = totalWeight + Double.parseDouble( itemWeight.item(i).getAttributes().getNamedItem("value").getNodeValue() );
		}
		order.setTotalWeight(totalWeight);
		
		// get new shipping address
		String country = doc.getElementsByTagName("country-code").item(0).getTextContent();
		String zip = doc.getElementsByTagName("postal-code").item(0).getTextContent();
		String city = doc.getElementsByTagName("city").item(0).getTextContent();
		String stateProvince = doc.getElementsByTagName("region").item(0).getTextContent();
		
		Address shippingAddress = order.getShipping();
		shippingAddress.setCountry(country);
		shippingAddress.setZip(zip);
		shippingAddress.setCity(city);
		shippingAddress.setStateProvince(stateProvince);
		
		order.setShipping(shippingAddress);
		
		List<ShippingRate> shippingRates = this.webJaguar.calculateShippingHandling(order, null, request, gSiteConfig);
		ShippingRate newShippingRate = null;
		for(ShippingRate sr: shippingRates){
			if(sr.getTitle().equalsIgnoreCase(shippingMethod)) {
				newShippingRate = sr;
				break;
			}
		}
		if(newShippingRate != null){
			order.setShippingCost(Double.parseDouble( newShippingRate.getPrice() ));
			
			if(order.getCustomShippingCost() != null) {
				Double shippingCost = Double.parseDouble( newShippingRate.getPrice() ) + order.getCustomShippingCost();
				newShippingRate.setPrice(shippingCost.toString());
			}
			
		} else {
			
			newShippingRate = new ShippingRate();
			
			if(order.getShippingMethod() != null) {
				newShippingRate.setTitle(order.getShippingMethod());
			} else if(order.getCustomShippingTitle() != null) {
				newShippingRate.setTitle(order.getCustomShippingTitle());
			}
			
			Double shippingCost = 0.0;
			if(order.getShippingCost() != null) {
				shippingCost = shippingCost + order.getShippingCost();
			} 
			if( order.getCustomShippingCost() != null ){
				shippingCost = shippingCost + order.getCustomShippingCost();
			}
			newShippingRate.setPrice(shippingCost.toString());
		}
		
		this.webJaguar.updateTaxAndTotal(gSiteConfig, globalDao, order);
		
		return merchantResponse(order, newShippingRate, addressId);
		
		
	}
	
	public StringBuilder merchantResponse ( Order order, ShippingRate shippingRate, Long addressId ) throws IOException {
		// generate Response
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document dom = null ;
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			dom = db.newDocument();
		}catch(ParserConfigurationException pce) {
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
			System.exit(1);
		}
		
		// root
		Element root = dom.createElement("merchant-calculation-results");
		root.setAttribute("xmlns", "http://checkout.google.com/schema/2");
		dom.appendChild(root);
		
		// results
		Element results = dom.createElement("results");
		root.appendChild(results);
		
		// result
		Element result = dom.createElement("result");
		result.setAttribute("shipping-name", shippingRate.getTitle());
		result.setAttribute("address-id", ""+addressId);
		results.appendChild(result);
				
		//shipping rate
		Element shipRate = dom.createElement("shipping-rate");
		shipRate.setAttribute("currency", "USD");
		shipRate.appendChild(dom.createTextNode(""+ ( shippingRate.getPrice() == null ? 0.0 : shippingRate.getPrice() )));
		result.appendChild(shipRate);
				
		//shippable
		Element shippable = dom.createElement("shippable");
		shippable.appendChild(dom.createTextNode("true"));
		result.appendChild(shippable);		
		
		//total tax
		Element totalTax = dom.createElement("total-tax");
		totalTax.setAttribute("currency", "USD");
		totalTax.appendChild(dom.createTextNode(""+order.getTax()));
		result.appendChild(totalTax);
					
		
		// convert to string
		StringBuilder stringBuilder = null;
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputFormat outputformat = new OutputFormat();
		outputformat.setIndenting(true);
		outputformat.setPreserveSpace(false);
		XMLSerializer serializer = new XMLSerializer();
		serializer.setOutputFormat(outputformat);
		serializer.setOutputByteStream(stream);
		serializer.asDOMSerializer();
		serializer.serialize(dom.getDocumentElement());

		stringBuilder = new StringBuilder(stream.toString());	
		return stringBuilder;
	}
	
}
