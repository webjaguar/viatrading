package com.webjaguar.thirdparty.google;


import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.google.checkout.checkout.Item;
import com.google.checkout.notification.ChargeAmountNotification;
import com.google.checkout.notification.NewOrderNotification;
import com.google.checkout.notification.OrderStateChangeNotification;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.dao.OrderDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.GoogleCheckOut;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.model.SiteMessage;

public class GoogleNotificationController implements Controller{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private OrderDao orderDao;
	public void setOrderDao(OrderDao orderDao) { this.orderDao = orderDao; }
	
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
    
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
			
			Map<String, Object>  gSiteConfig = globalDao.getGlobalSiteConfig();
		
			DocumentBuilderFactory factory = null;
		    DocumentBuilder builder = null;
		    Document ret = null;

		    try {
		      factory = DocumentBuilderFactory.newInstance();
		      builder = factory.newDocumentBuilder();
		    } catch (Exception e) {
		      e.printStackTrace();
		    }

		    try {
		      ret = builder.parse(new InputSource(request.getInputStream()));
		    } catch (Exception e) {
		      e.printStackTrace();
		    }
		    ret.getDocumentElement().normalize();
			Node node = ret.getFirstChild();
			
			if(node.getNodeName().equals("new-order-notification")) {
		    	NewOrderNotification notification = new NewOrderNotification(ret);
		    	if(notification != null) {
		    		parseNewOrderNotification(notification, gSiteConfig, request);
		    	}
			}
			if(node.getNodeName().equals("order-state-change-notification")) {
				OrderStateChangeNotification notification = new OrderStateChangeNotification(ret);
				if(notification != null) {
					parseOrderStateChangeNotification(notification);
				}
			}
			if(node.getNodeName().equals("charge-amount-notification")) {
				ChargeAmountNotification notification = new ChargeAmountNotification(ret);
				if(notification != null) {
					parseChargeAmountNotification(notification);
				}
			}
			
			return null;
	}
	
	public void parseNewOrderNotification(NewOrderNotification notification, Map<String, Object> gSiteConfig, HttpServletRequest request){
		
		Integer orderId = Integer.parseInt( notification.getMerchantPrivateDataNodes()[0].getTextContent() );
		Long googleOrderId = Long.parseLong( notification.getGoogleOrderNumber() );
		Double orderTotal = Double.parseDouble(Float.toString(notification.getOrderTotal()));
		String shippingMethod = notification.getShipping().getShippingName();
		
		GoogleCheckOut googlecheckOut  = new GoogleCheckOut();
		googlecheckOut.setOrderId(orderId);
		googlecheckOut.setGoogleOrderId(googleOrderId);
		
		Order order = orderDao.getOrder( orderId, null );
		
		if(!order.getGrandTotal().equals(orderTotal)){
			request.setAttribute( "gSiteConfig", gSiteConfig );
			Double totalWeight = 0.0;
			Iterator<Item> itr = notification.getItems().iterator();
			while(itr.hasNext()) {
				totalWeight = totalWeight + Double.parseDouble(Float.toString(itr.next().getItemWeight())) ;
			}
			order.setTotalWeight(totalWeight);
			
			Address address = order.getShipping();
			if(notification.getBuyerShippingAddress() != null) {
				address.setAddr1(notification.getBuyerShippingAddress().getAddress1());
				address.setAddr2(notification.getBuyerShippingAddress().getAddress2());
				address.setCity(notification.getBuyerShippingAddress().getCity());
				address.setStateProvince(notification.getBuyerShippingAddress().getRegion());
				address.setZip(notification.getBuyerShippingAddress().getPostalCode());
				address.setCountry(notification.getBuyerShippingAddress().getCountryCode());
				order.setShipping(address);
			}
			
			List<ShippingRate> shippingRates = this.webJaguar.calculateShippingHandling(order, null, request, gSiteConfig);
			ShippingRate newShippingRate = null;
			for(ShippingRate sr: shippingRates){
				if(sr.getTitle().equalsIgnoreCase(shippingMethod)) {
					newShippingRate = sr;
				}
			}
			if(newShippingRate != null){
				order.setShippingCost(Double.parseDouble( newShippingRate.getPrice() ));
			}
			this.webJaguar.updateTaxAndTotal(gSiteConfig, globalDao, order);
			this.webJaguar.updateOrder(order);
		}
		
		if (order.getPaymentMethod().equalsIgnoreCase( "google")) {
    		OrderStatus orderStatus = null;
    		if (order.getStatus().equalsIgnoreCase( "xg" )) { // google cancelled
    			// update status
    			orderStatus = new OrderStatus();
    			orderStatus.setOrderId( order.getOrderId() );
    			orderStatus.setStatus( "p" ); // pending
    			SiteMessage siteMessage = null;
    			try {
    				Integer messageId = null;
    				// multi store
    				List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
    				MultiStore multiStore = null;
    				if (multiStores.size() > 0) {
    					multiStore = multiStores.get(0);
    					messageId = multiStore.getMidNewOrders();
    				} else {
    					Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
    					
	    				messageId = Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_NEW_ORDERS" ).getValue() );		    					
    				}
    				siteMessage = this.webJaguar.getSiteMessageById( messageId );
    				if (siteMessage != null) {
    					// send email
    					orderStatus.setSubject( siteMessage.getSubject() );
    					orderStatus.setMessage( siteMessage.getMessage() );
    					orderStatus.setHtmlMessage(siteMessage.isHtml());
    				}
    			} catch (NumberFormatException e) {
    				// do nothing
    			}		    		
    		}
			orderDao.updateGoogleCheckOut(googlecheckOut, orderStatus);
		}
	}
	
	public void parseOrderStateChangeNotification(OrderStateChangeNotification notification){
		
		Integer orderId = 0;
		Long googleOrderId = Long.parseLong( notification.getGoogleOrderNumber() );
		
		orderId = orderDao.getOrderIdByGoogleOrderId(googleOrderId);
		GoogleCheckOut googlecheckOut  = new GoogleCheckOut();
		googlecheckOut.setOrderId(orderId);
		googlecheckOut.setGoogleOrderId(googleOrderId);
		
		Order order = orderDao.getOrder( orderId, null );
		if (order.getPaymentMethod().equalsIgnoreCase( "google")) {
    		OrderStatus orderStatus = null;
    		
    		// update status
    		orderStatus = new OrderStatus();
    		orderStatus.setOrderId( order.getOrderId() );
    		orderStatus.setComments("From "+notification.getPreviousFinancialOrderState()+" To "+notification.getNewFinancialOrderState());
    		if(notification.getNewFinancialOrderState().toString().equalsIgnoreCase("CANCELLED") || notification.getNewFinancialOrderState().toString().equalsIgnoreCase("CANCELLED_BY_GOOGLE")) {
    			orderStatus.setStatus( "x" ); // cancelled
        	} else {
        		orderStatus.setStatus( "p" ); // pending
        	}
    		SiteMessage siteMessage = null;
    		try {
    			Integer messageId = null;
    			// multi store
    			List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
    			MultiStore multiStore = null;
    			if (multiStores.size() > 0) {
    				multiStore = multiStores.get(0);
    				messageId = multiStore.getMidNewOrders();
    			} else {
    				Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
    				
	    			messageId = Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_NEW_ORDERS" ).getValue() );		    					
    			}
    			siteMessage = this.webJaguar.getSiteMessageById( messageId );
    			if (siteMessage != null) {
    				// send email
    				orderStatus.setSubject( siteMessage.getSubject() );
    				orderStatus.setMessage( siteMessage.getMessage() );
    				orderStatus.setHtmlMessage(siteMessage.isHtml());
    			}
    		} catch (NumberFormatException e) {
    			// do nothing
    		}
			orderDao.updateGoogleCheckOut(googlecheckOut, orderStatus);
		}
	}

	public void parseChargeAmountNotification(ChargeAmountNotification notification) throws MessagingException{
	
		Integer orderId = 0;
		Long googleOrderId = Long.parseLong(notification.getGoogleOrderNumber());
		Double chargeAmount = Double.parseDouble(Float.toString(notification.getTotalChargeAmount()));
	
		orderId = orderDao.getOrderIdByGoogleOrderId(googleOrderId);
		Order order = orderDao.getOrder( orderId, null );
		if(order.getGrandTotal().equals(chargeAmount)){
			GoogleCheckOut googlecheckOut  = new GoogleCheckOut();
			googlecheckOut.setOrderId(orderId);
			googlecheckOut.setGoogleOrderId(googleOrderId);
		
			Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
			if (order.getPaymentMethod().equalsIgnoreCase( "google")) {
				OrderStatus orderStatus = null;
			
				// update status
				orderStatus = new OrderStatus();
				orderStatus.setOrderId( order.getOrderId() );
				orderStatus.setComments("Charge Amount "+chargeAmount);
				orderStatus.setStatus( "pr" ); // processing
				SiteMessage siteMessage = null;
				try {
					Integer messageId = null;
					// multi store
					List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
					MultiStore multiStore = null;
					if (multiStores.size() > 0) {
						multiStore = multiStores.get(0);
						messageId = multiStore.getMidNewOrders();
					} else {
						messageId = Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_NEW_ORDERS" ).getValue() );		    					
					}
					siteMessage = this.webJaguar.getSiteMessageById( messageId );
					if (siteMessage != null) {
						// send email
						orderStatus.setSubject( siteMessage.getSubject() );
						orderStatus.setMessage( siteMessage.getMessage() );
						orderStatus.setHtmlMessage(siteMessage.isHtml());
					}
				} catch (NumberFormatException e) {
					// do nothing
				}
				orderDao.updateGoogleCheckOut(googlecheckOut, orderStatus);
			}
			
			// buy safe
			if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
				try {
					this.webJaguar.setShoppingCartCheckout( order, mailSender );
				} catch (Exception e) { 
					e.printStackTrace();
					OrderStatus status = new OrderStatus();
					status.setOrderId( order.getOrderId() );
					status.setStatus( "pr" ); // processing
					if( (order.getWantsBond() != null) && (order.getWantsBond() == true) ) {
						status.setComments("BuySafe Bond was selected but cancelled as system failed to connect Buysafe. Please check the amount charged and refund the bond cost to Customer.");
					} else {
						status.setComments("BuySafe Bond was not selected. System failed to connect BuySafe.");
					}
					order.setWantsBond(false);
					order.setBondCost(null);
					order.setGrandTotal();
					
					this.webJaguar.cancelBuySafeBond(order, status);
				}
			}
			
		}
	}

}
