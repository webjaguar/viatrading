/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 08.04.2009
 */

package com.webjaguar.thirdparty.dsi;

import java.util.ArrayList;
import java.util.List;

public class DsiCategory {

	private String code;
	private String manuf;
	private String cat;
	private String sub;
	private Integer catId;
	private Double priceChange;
	List<DsiCategory> subs;
	private String priceChangeBase;
	
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
	public void setManuf(String manuf) {
		this.manuf = manuf;
	}
	public String getManuf() {
		return manuf;
	}
	public String getCat()
	{
		return cat;
	}
	public void setCat(String cat)
	{
		this.cat = cat;
	}
	public String getSub()
	{
		return sub;
	}
	public void setSub(String sub)
	{
		this.sub = sub;
	}
	public Integer getCatId()
	{
		return catId;
	}
	public void setCatId(Integer catId)
	{
		this.catId = catId;
	}
	public Double getPriceChange()
	{
		return priceChange;
	}
	public void setPriceChange(Double priceChange)
	{
		this.priceChange = priceChange;
	}
	public List<DsiCategory> getSubs()
	{
		return subs;
	}
	public void setSubs(List<DsiCategory> subs)
	{
		this.subs = subs;
	}
	public void addSub(DsiCategory sub) 
	{
		if (subs == null) subs = new ArrayList<DsiCategory>();
		subs.add(sub);
	}
	public String getPriceChangeBase() {
		return priceChangeBase;
	}
	public void setPriceChangeBase(String priceChangeBase) {
		this.priceChangeBase = priceChangeBase;
	}

}
