/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 08.04.2009
 */

package com.webjaguar.thirdparty.dsi;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class DsiCategoryController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		List<DsiCategory> categories = new ArrayList<DsiCategory>();
		
		if (request.getMethod().equals("POST")) {
			for (String code: request.getParameterValues("code")) {
				DsiCategory category = new DsiCategory();
				category.setCode(code);
				try {
					category.setCatId(ServletRequestUtils.getIntParameter(request, code + "_catId"));
				} catch (Exception e) {
					category.setCatId(null);
				}
				try {
					category.setPriceChange(ServletRequestUtils.getDoubleParameter(request, code + "_priceChange"));					
				} catch (Exception e) {
					category.setPriceChange(null);
				}
				category.setPriceChangeBase(ServletRequestUtils.getStringParameter(request, code + "_priceChangeBase", ""));
				categories.add(category);
			}
			this.webJaguar.nonTransactionSafeUpdateDsiCategories(categories);
		}
		
		categories = this.webJaguar.getDsiCategories();
		
		Map<String, DsiCategory> categoryMap = new HashMap<String, DsiCategory>();
		Iterator<DsiCategory> iter = categories.iterator();
		while (iter.hasNext()) {
			DsiCategory category = iter.next();
			if (category.getCat().equals("*")) {
				categoryMap.put(category.getManuf().toLowerCase(), category);				
			} else if (category.getSub().equals("*")) {
				categoryMap.put(category.getManuf().toLowerCase() + "--" + category.getCat().toLowerCase(), category);
				if (categoryMap.get(category.getManuf().toLowerCase()) != null) {
					categoryMap.get(category.getManuf().toLowerCase()).addSub(category);					
				}
				iter.remove();
			} else {
				if (categoryMap.get(category.getManuf().toLowerCase() + "--" + category.getCat().toLowerCase()) != null) {
					categoryMap.get(category.getManuf().toLowerCase() + "--" + category.getCat().toLowerCase()).addSub(category);					
				}
				iter.remove();
			}
		}
		
		map.put("categories", categories);		

		return new ModelAndView("admin/vendors/dsi/category", map);
	}
}
