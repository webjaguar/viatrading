/* Copyright 2012 Advanced E-Media Solutions
 * @author Shahin(LogicZoom)
 * @since 09.10.2012
 */

package com.webjaguar.thirdparty.searchEngine;

import java.util.Properties;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.webjaguar.logic.WebJaguarFacade;

public class SearchEngineProspect {	
	
	private static final Logger logger = LoggerFactory.getLogger(SearchEngineProspect.class);

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private Properties site;
	public void setSite(Properties site) { this.site = site; }

	private final static int SECONDS_IN_DAY = 86400;

	public SearchEngineProspect() {
		super();
	}

	public boolean filter(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// Wrapping all with a big try/catch because if anything goes wrong
		// (i.e. missing properties file, referer string not what we
		// expect/tested
		// or any other issue), we do not want the user to even know.
		try {
			// Retrieve cookie preferences from configuration file
			Integer cookieMaxAge = SECONDS_IN_DAY * Integer.parseInt(site.getProperty("track.prospect.days"));
			String cookieName = site.getProperty("track.prospect.cookie.name");

			//logger.info("Beginning of Filter...");

			boolean hasProspectId = false;
			String prospectId = null;
			String referrer = request.getHeader("Referer");
			String host = request.getLocalName();

			//logger.info("Referer: " + referrer);
			//logger.info("Host: " + host);

			// If no referrer or came from another page on same site, we do not
			// need to go further
			if (referrer != null && !referrer.isEmpty() && !referrer.startsWith("http://"+host) && !referrer.startsWith("https://"+host)) {
				// Look through cookies to see if our cookie already exists, if
				// so, we will reuse it
				if (request.getCookies() != null) {
					Cookie[] cookies = request.getCookies();
					for (Cookie cookie : cookies) {
						if (cookie.getName().trim().equals(cookieName) && !cookie.getValue().trim().equals("")) {
							// We have confirmed this person has been here
							// before so we get the
							// previously assigned prospectId
							hasProspectId = true;
							prospectId = cookie.getValue().trim();
						}
						//logger.info(cookie.getName() + " : " + cookie.getValue());
					}
				}
				// We pass along information to the service. Service will
				// determine
				// if referrer is one of configured domains to monitor and
				// insert into
				// database if need be. IF prospectId is null at this point
				// (i.e. first time user), then it will be generated and
				// returned by
				// service so filter can set it in the cookie.
				prospectId = webJaguar.addWebReferralProspectInformation(prospectId, referrer);

				// Add the cookie to response if this was first time through
				if (!hasProspectId && prospectId != null) {
					// Define a new cookie and set its expiration
					Cookie cookie = new Cookie(cookieName, prospectId);
					cookie.setMaxAge(cookieMaxAge);
					HttpServletResponse res = (HttpServletResponse) response;
					res.addCookie(cookie);
				}
			} else {
				//logger.info("Request came from same host or referrer empty, skipped.");
			}
			return true;
		} catch (Exception e) {
			logger.error("There was an error processing.", e);
		} finally {
			return true;
		}
	}	
}