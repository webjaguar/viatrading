/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 10.10.2006
 */

package com.webjaguar.thirdparty.imagej;

import ij.ImagePlus;
import ij.io.FileSaver;
import ij.io.Opener;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.Font;

// class to manipulate images
public class ImageManipulator {
	
    // set support image extension
	String supportExt = "gif|jpg|jpeg";
	String imageExt = null;
	
	// resize image
	public boolean resize(String iPath,String oPath,int maxWidth,int maxHeight,String contentType){
		
		// check file format
		//if(isValidImageFormat(iPath)&& isValidImageFormat(oPath)){
			
		    // open original image	
		    Opener o= new Opener();
	        ImagePlus oldImp= o.openImage(iPath);
	
	        // resize it
	        if (oldImp != null) {
	    
		        // get size of original image
		        ImageProcessor ip= oldImp.getProcessor();
		        
		        // get the actual width and height of original image
		        int oldWidth = ip.getWidth();
		        int oldHeight = ip.getHeight();
		        		        
		        // determine the new width and height if resize needed
		        if(oldWidth > maxWidth || oldHeight > maxHeight){
		            
		        	int newWidth = 0;
		        	int newHeight = 0;
		            
		        	double widthRate = (double)oldWidth/maxWidth;
		        	double heightRate = (double)oldHeight/maxHeight;
		        			        	
		        	if(widthRate > heightRate){
		        		newWidth = ((Double)(oldWidth/widthRate)).intValue();
		        		newHeight = ((Double)(oldHeight/widthRate)).intValue();
		        		
		        	}
		        	else if (widthRate == heightRate){
		        		newWidth = ((Double)(oldWidth/widthRate)).intValue();
		        		newHeight = ((Double)(oldHeight/widthRate)).intValue();
		        		
		        	}
		        	else if(widthRate < heightRate){
		        		newWidth = ((Double)(oldWidth/heightRate)).intValue();
		        		newHeight = ((Double)(oldHeight/heightRate)).intValue();
		        		
		        	}
		    		
		            // now resize it
		            ip = ip.resize(newWidth,newHeight);
		
		            // save it
		            ImagePlus newImp = new ImagePlus(oPath,ip);
		            FileSaver fs = new FileSaver(newImp);
		            saveImage(fs,contentType,oPath);
		        	            
		        }
		    }
	        else{
		        System.out.println("image is null in resize");
		        return false;
	        }
	        
	        return true;
		//}
		//else{
		//	System.out.println("invalid path or file format");
		//}
		
    }
	
	// watermark image
	public void watermark(String iPath,String oPath, String watermarkText, Font font, Color c, String contentType){
		
        // check file format
		//if(isValidImageFormat(iPath)&& isValidImageFormat(oPath)){
		
            // open original image	
		    Opener o= new Opener();
		    ImagePlus imp= o.openImage(iPath);
		
		    if (imp != null){
			
                // get size of original image
		        ImageProcessor ip= imp.getProcessor();
		    
                // watermark it
                ip.setFont(font);

			    // place text at center of image
			    int x= ip.getWidth()/2 - ip.getStringWidth(watermarkText)/2;
			    int y= ip.getHeight()/2 + ip.getFontMetrics().getHeight()/2;
			    
			    // create a rectangle around text 
			    ip.setRoi(x-5, y-ip.getFontMetrics().getHeight()-5, ip.getStringWidth(watermarkText) + 10, ip.getFontMetrics().getHeight() + 10);
			
			    // set font color
			    // Color c= new Color(192,192,192);
			    ip.setColor(c);
			
			    // draw string
			    ip.drawString(watermarkText, x, y);
			    //ip.drawLine(0,y,ip.getWidth(),y);
			
			    // save it
			    ImagePlus imp2 = new ImagePlus(oPath,ip);
			    FileSaver fs = new FileSaver(imp2);
			    saveImage(fs,contentType,oPath);
			    
		    }
		    else{
			    System.out.println("image is null in watermark");
		    }
		//}
		//else{
		//	System.out.println("invalid path or file format");
		//}
	}
	
	// save image as jpeg or gif based on content type of the original image
	public void saveImage (FileSaver fs,String contentType,String oPath){
		
		if(contentType.equalsIgnoreCase("image/jpg")||contentType.equalsIgnoreCase("image/jpeg")
        		|| contentType.equalsIgnoreCase("image/pjpeg")){
            fs.saveAsJpeg(oPath);
        }
        else if(contentType.equalsIgnoreCase("image/gif")){
    	    fs.saveAsGif(oPath);
        }
		
	}
	
	// validate image format
	public boolean isValidImageFormat(String iPath){
		
        // return flag
		boolean flag = false;
		
		// check directory path
		int nIndex= iPath.lastIndexOf("/");
		if (nIndex > 0){
			String imgName= iPath.substring(nIndex+1);
			// check image name, format
			if (!imgName.equals("")){
				int extIndex = imgName.lastIndexOf(".");
				if(extIndex>0){
					String extName = imgName.substring(extIndex+1);
					this.imageExt = extName;
					if (supportExt.indexOf(extName.toLowerCase()) > -1)
						  flag= true;
				}
			}
		}
		return flag;
	}
	
	
}
