package com.webjaguar.thirdparty.tcintlinc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.Order;
import com.webjaguar.model.Payment;
import com.webjaguar.web.domain.KeyBean;

public class Tcintl {
	
	private static WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private static MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	static Map<String, Configuration> siteConfig;
	static Map<String, Object> gSiteConfig;
	
	public Tcintl() {
		super();
	}

	public Tcintl(Map<String, Configuration> siteConfig) {
		this.siteConfig = siteConfig;
	}
	
	public Tcintl(Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) {
		this.siteConfig = siteConfig;
		this.gSiteConfig = gSiteConfig;
	}
	
	public KeyBean importPayments() {
		System.out.println("importPayments ");
		
		// Mas90 configuration
		String[] config = siteConfig.get("MAS90").getValue().split(",");
		StringBuffer fileNamesList = new StringBuffer();

		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		KeyBean result = new KeyBean("false", null);
		
		List<String> accountNumbers = new ArrayList<String>();
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        
	        // ftp on wjserver500 uses port 2121. 
	        try {
	        	ftp.connect(config[0], Integer.parseInt(config[5]));
	        } catch (ArrayIndexOutOfBoundsException e) {
	        	ftp.connect(config[0]);	        	
	        }
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	        
	        
	        if (ftp.changeWorkingDirectory("MASPAYMENTS")) {
		        String[] fileNames = ftp.listNames();
		        nextFile: for (String fileName: fileNames) {
		        	if (fileName.startsWith("payment") && fileName.endsWith(".csv")) {
		        		try {
		        			InputStream inStream = ftp.retrieveFileStream(fileName);
		        			System.out.println("fileName " + fileName);
		        			if (inStream != null) {
			    	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream));
			    				
			    				List<Payment> lineItems = null;
			    				Payment lineItem = null;
			    				Payment payment = new Payment();
			    				String [] nextLine;
			    				while ((nextLine = reader.readNext()) != null) {
			    					if (nextLine[0].equalsIgnoreCase("Summary")) {
			    						System.out.println("Summary ");
			    						lineItems = new ArrayList<Payment>();
			    						lineItem = new Payment();
			    						
			    						int userId = 0;
			    						if (nextLine[1] == null || nextLine[1].isEmpty()) {
			    							// goto next Summary
			    							continue nextFile;
			    						} else {
			    							System.out.println("userId");
			    							userId = this.webJaguar.getCustomerIdByAccountNumber(nextLine[1]);
			    							if (userId == 0) {
			    								accountNumbers.add(nextLine[1]);
			    								continue nextFile;
			    							}
			    						}
			    						// MAS accountID
			    						payment.setUserId(userId);
			    						// Date Processed
			    						payment.setDate(new Date (nextLine[2]));
			    						// Payment Type
			    						payment.setPaymentMethod(nextLine[3]);
			    						// check Number
			    						payment.setMemo(nextLine[4]);
			    						// check Amount
			    						payment.setAmount(new Double(nextLine[5]));
			    						System.out.println("payment " + payment.getAmount());

			    					} else if (nextLine[0].equalsIgnoreCase("Items")) {
			    						// order Id
			    						lineItem.setOrderId(new Integer(nextLine[1]));
			    						// check Amount
			    						lineItem.setAmount(new Double(nextLine[2]));
			    						lineItems.add(lineItem);
			    						payment.setLineItems(lineItems);
			    					}
			    				}
			    				fileNamesList.append(fileName);
			    				this.webJaguar.insertCustomerPayment(payment);
			    				reader.close();
			    				inStream.close();
			    				
			    				// delete file
			    				//System.out.println("delete file " );
								//ftp.deleteFile(fileName);
			    	        }
		        		} catch (ConnectException ex) {  
		                    ex.printStackTrace(); 
		                    continue nextFile;
		                }
		        	}
		        }
		        for (String fileName: fileNames) { 
		        	// rename file
    				System.out.println("delete file " );
					ftp.rename(fileName, "DONE-"+fileName);
		        }
	        } else {
	        	//notifyAdmin("Folder MASPAYMENTS not found" + siteConfig.get("SITE_URL").getValue(), null);
	        	result.setValue("Folder MASPAYMENTS not found on ");
	        	return result;
	        }

	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			// do nothing
			e.printStackTrace();
			//notifyAdmin("Exception : TCI importPayments() on " + siteConfig.get("SITE_URL").getValue(), null);
			result.setValue("Exception : TCI importPayments() on " + "\n\n" + e.toString());
			return result;
		}
		
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n");
		sbuff.append("FileNames read : " + fileNamesList.toString() + "\n\n");

		if (!accountNumbers.isEmpty()) {
			sbuff.append("AccountNumber NOT FOUND: \n" + accountNumbers.toString());
		}
		
		//notifyAdmin("Success: TCI importPayments() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
		result.setName("true");
		result.setValue("Success: TCI importPayments() - on" + "\n\n" + sbuff.toString());
		return result;
	}

	/*
	 * input format: OrderId,Processed Date,Payment Type,check number,check amount
	 * input example: 1000157,01/30/2013,C.O.D.,013020131910-1,862.50
	 */
	public void importPayments2(String realPath) {
		System.out.println("importPayments2 ");
		
		// Mas90 configuration
		String[] config = siteConfig.get("MAS90").getValue().split(",");
		StringBuffer fileNamesList = new StringBuffer();

		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		
		File tciFolder = new File(realPath);
		if (!tciFolder.exists()) {
			tciFolder.mkdir();
		}
		File tciProcessedFolder = new File(tciFolder, "processed");
		if (!tciProcessedFolder.exists()) {
			tciProcessedFolder.mkdir();
		}
		
		List<String> accountNumbers = new ArrayList<String>();
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        
	        // ftp on wjserver500 uses port 2121. 
	        try {
	        	ftp.connect(config[0], Integer.parseInt(config[5]));
	        } catch (ArrayIndexOutOfBoundsException e) {
	        	ftp.connect(config[0]);	        	
	        }
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
			
	        
	        // FOR TESTING instead of FTP file, read local file
//			final String FILE_NAME = "/Users/shahinnaji/Desktop/inv.cvs";
//			InputStream inStream = new FileInputStream(FILE_NAME);
	        
	        
	        if (ftp.changeWorkingDirectory("MASPAYMENTS")) {
		        String[] fileNames = ftp.listNames();
		        fileNamesList.append("Number of Files: " + fileNames.length + "\n\n");
		        nextFile: for (String fileName: fileNames) {
		        	if (fileName.startsWith("payment") && fileName.endsWith(".csv")) {
		        		File file = new File(tciProcessedFolder, fileName);
		        		
		        		try {
		        			if (ftp.retrieveFile(fileName, new FileOutputStream(file))) {
		        				System.out.println("fileName: " + fileName);
		        				CSVReader reader = new CSVReader(new FileReader(file));
		        				
			    				List<Payment> lineItems = null;
			    				Payment lineItem = null;
			    				Payment payment = new Payment();
			    				String [] nextLine;
			    				while ((nextLine = reader.readNext()) != null) {
		    						lineItems = new ArrayList<Payment>();
		    						lineItem = new Payment();
		    						
		    						Order order = null;
		    						if (nextLine[0] == null || nextLine[0].isEmpty()) {
		    							// goto next Summary
		    							continue nextFile;
		    						} else {
		    							int OrderId = 0;
		    							order = this.webJaguar.getOrder(new Integer(nextLine[0]), null);
		    							if (order.getUserId() == 0) {
		    								accountNumbers.add(order.getOrderId() + " - " + order.getUserEmail());
		    								continue nextFile;
		    							}
		    						}
		    						// UserId
		    						payment.setUserId(order.getUserId());
		    						// Date Processed
		    						DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		    						Date dateProcessed = (Date)formatter.parse(nextLine[1]);  
		    						payment.setDate(dateProcessed);
		    						// Payment Type
		    						payment.setPaymentMethod(nextLine[2]);

		    						// check Number/memo
		    						payment.setMemo(nextLine[3]);

		    						// check Amount
		    						payment.setAmount(new Double(nextLine[4]));
		    					
		    						// order Id
		    						lineItem.setOrderId(order.getOrderId());
		    						// check Amount
		    						lineItem.setAmount(new Double(nextLine[4]));
		    						lineItems.add(lineItem);
		    						payment.setLineItems(lineItems);
		    						
		    						this.webJaguar.insertCustomerPayment(payment);

			    				}
			    				fileNamesList.append(fileName + "\n");
			    				
			    				reader.close();
			    				
			    				// delete file
			    				System.out.println("delete file " + fileName);
								ftp.deleteFile(fileName);
		        			}

		        		} catch (ConnectException ex) {  
		                    ex.printStackTrace(); 
		                    continue nextFile;
		                }
		        	}
		        }
	        } else {
	        	notifyAdmin("Folder MASPAYMENTS not found" + siteConfig.get("SITE_URL").getValue(), null);
	        }

	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			// do nothing
			e.printStackTrace();
			notifyAdmin("Exception : TCI importPayments() on " + siteConfig.get("SITE_URL").getValue(), null);
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n");
		sbuff.append("FileNames read : " + fileNamesList.toString() + "\n\n");

		if (!accountNumbers.isEmpty()) {
			sbuff.append("AccountNumber NOT FOUND: \n" + accountNumbers.toString());
		}
		
		notifyAdmin("Success: TCI importPayments() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
	

	/*
	 * To update inventory: 
	 * Create a csv file with columns: SKU, Inventory (Eg: ABCD,10) and upload it to the ftp. 
	 * Update Mas90 field in siteConfig with this format: ftp_domainName,ftp_username,ftp_password,invetory.csv,orders.csv,(if site is on wjserver500 use port number too)port number 
	 * Once data is imported file(inventory.csv) is deleted from the server. 
	 */
	public KeyBean updateInventory() {
		// Mas90 configuration
		String[] config = siteConfig.get("MAS90").getValue().split(",");
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		KeyBean result = new KeyBean("false", null);
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        
	        // ftp on wjserver500 uses port 2121. 
	        try {
	        	ftp.connect(config[0], Integer.parseInt(config[5]));
	        } catch (ArrayIndexOutOfBoundsException e) {
	        	ftp.connect(config[0]);	        	
	        }
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode(); 
	        
	        if (ftp.changeWorkingDirectory("MASINVENTORY")) {
	        	InputStream inStream = ftp.retrieveFileStream(config[3]);
				
		        // FOR TESTING instead of FTP file, read local file
				//final String FILE_NAME = "/Users/shahinnaji/Desktop/inv.cvs";
				//InputStream inStream = new FileInputStream(FILE_NAME);
		        
				boolean inventoryHistory = false;
				if (siteConfig.get("INVENTORY_HISTORY").getValue().equals("true")) {
					inventoryHistory = true;
				}
		        if (inStream != null) {
		        	CSVReader reader = new CSVReader(new InputStreamReader(inStream));
					String [] nextLine;
					while ((nextLine = reader.readNext()) != null) {
						HashMap<String, Object> map = new HashMap<String, Object>();
						String sku = nextLine[0].trim();
						String inventoryString = nextLine[1];
						if (inventoryString.endsWith("-")) {
							// move - at end to beginning of string
							inventoryString = "-" + inventoryString.substring(0, inventoryString.length()-1);
						}
						if (!sku.equals("")) {
							map.put("sku", sku);
							try {
								Integer inventory = Integer.parseInt(inventoryString);
								Inventory inventoryObject = new Inventory();
								inventoryObject.setSku(sku);
								inventoryObject = this.webJaguar.getInventory(inventoryObject);
								if(inventoryObject != null && inventoryObject.getInventoryAFS() != null && inventoryObject.getInventory() != null) {
									inventory = inventory - inventoryObject.getInventoryAFS();
									map.put("inventory_afs", inventoryObject.getInventoryAFS() + inventory);
									map.put("inventory", inventoryObject.getInventory() + inventory);

									
									// Add to Inventory history
									if (inventoryHistory) {
										InventoryActivity inventoryActivity = new InventoryActivity();	
										inventoryActivity.setSku(sku);
										inventoryActivity.setInventory(inventoryObject.getInventory());
										inventoryActivity.setinventoryAFS(inventoryObject.getInventoryAFS());
										inventoryActivity.setAccessUserId(0);
										inventoryActivity.setQuantity(inventory);
										inventoryActivity.setInventory(inventoryObject.getInventoryAFS() + inventory);
										inventoryActivity.setType("mass90 import");
										this.webJaguar.productIventoryHistory(inventoryActivity);
									}
									
									
								} else {
									map.put("inventory", inventory);
									map.put("inventory_afs", inventory);
								}
							} catch (NumberFormatException e) {
								continue;
							}
							data.add(map);
						}
					}
					reader.close();
					inStream.close();
					
					// delete file
					ftp.deleteFile(config[3]);
		        }
				
		        // Logout from the FTP Server and disconnect
		        ftp.logout();
		        ftp.disconnect();
	        } else {
	        	notifyAdmin("Folder MASINVENTORY not found" + siteConfig.get("SITE_URL").getValue(), null);
		        result.setValue("Folder MASINVENTORY not found on " + siteConfig.get("SITE_URL").getValue() + "\n\n" + sbuff.toString());
	        	return result;
	        }
	        
		} catch (Exception e) {
			// do nothing
			//notifyAdmin("1-Exception Mas90 updateInventory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
			result.setValue("1-Exception Mas90 updateInventory() on " + "\n\n" + e.toString());
			return result;
		}
		
		// update inventory
		if (!data.isEmpty()) {
			this.webJaguar.updateInventoryBySkus(null, data);
			
			Date end = new Date();
			sbuff.append("Started : " + dateFormatter.format(start) + "\n");
			sbuff.append("Ended : " + dateFormatter.format(end) + "\n");
			sbuff.append("Products updated : " + data.size() + "\n\n");
			notifyAdmin("Success: Mas90 updateInventory() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
			result.setName("true");
			result.setValue("Success: Mas90 updateInventory() - on " + "\n\n" + sbuff.toString());
			return result;

		} else {
			notifyAdmin("2- Mas90 updateInventory() on " + siteConfig.get("SITE_URL").getValue(), ""+data.size());
			result.setValue("2- Mas90 updateInventory() on " + "\n\n" + sbuff.toString());
			return result;
		}
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}

