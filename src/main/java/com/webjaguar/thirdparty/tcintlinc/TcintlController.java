/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 10.10.2012
 */

package com.webjaguar.thirdparty.tcintlinc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.webjaguar.model.Configuration;
import com.webjaguar.web.domain.KeyBean;

public class TcintlController extends MultiActionController implements Controller {

	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }

	public ModelAndView tciImportPayments(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
		Tcintl tci = new Tcintl(siteConfig);
		String realPath = getServletContext().getRealPath("/tci");
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		tci.importPayments2(realPath); 
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		notifyAdmin("Success: TcintlController - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());

		return new ModelAndView("admin/catalog/index");
	}
	
	public ModelAndView tciUpdateInventory(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		Tcintl tci = new Tcintl(siteConfig, gSiteConfig);
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		KeyBean result = tci.updateInventory();
		
		if (result.getValue() != null){
			//notifyAdmin("TCI UpdateInventory", ((String) result.getValue()).toString());
		}

		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		//notifyAdmin("Success: TcintlController - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());

		return new ModelAndView("admin/catalog/index");
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}