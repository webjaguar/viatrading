/* Copyright 2009 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 05.05.2011
 */

package com.webjaguar.thirdparty.stoneEdge;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;

import stoneEdge.ItemPackageType;
import stoneEdge.ItemType;
import stoneEdge.OrderType;
import stoneEdge.OrdersType;
import stoneEdge.PackageType;
import stoneEdge.ProductType;
import stoneEdge.SETIProductsType;

import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.PackingList;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.*;
import com.webjaguar.web.admin.customer.CreditImp;

public class StoneEdgeController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	
	private GlobalDao globalDao;	
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object>  gSiteConfig = globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		StringBuffer res = new StringBuffer();
		
		String functionName = request.getParameter("setifunction");
		System.out.println("New Request : Function Name : "+functionName);
		
		//send version
		if(functionName != null && functionName.equalsIgnoreCase("sendversion")) {
			res.append("SETIResponse: version=7.000");
		}
		
		//order count (Tested)
		if(functionName != null && functionName.equalsIgnoreCase("ordercount")) {
			if(validateUser(request, siteConfig)) {
				try {
					res.append(getOrderCount(request, siteConfig));
				} catch (Exception e) {
					res.append(errorResponse("SETIOrders", "Error in order count").toString());
					e.printStackTrace();
				}
			} else {
				res.append(errorResponse("SETIOrders", "Username Password mismatch").toString());
			}
		}
		
		//download orders (Tested)
		if(functionName != null && functionName.equalsIgnoreCase("downloadorders")) {
			if(validateUser(request, siteConfig)) {
				try {
					res.append(ordersList(request.getParameter("startnum") != null ? Integer.parseInt(request.getParameter("startnum")) : null, request.getParameter("batchsize") != null ? Integer.parseInt(request.getParameter("batchsize")) : null, siteConfig, gSiteConfig));
				} catch (Exception e) {
					res.append(errorResponse("SETIOrders", "Error in Order download").toString());
					e.printStackTrace();
				}			
			} else {
				res.append(errorResponse("SETIOrders", "Username Password mismatch").toString());
			}
		}
		
		//customers count (Tested)
		if(functionName != null && functionName.equalsIgnoreCase("getcustomerscount")) {
			if(validateUser(request, siteConfig)) {
				try {
					res.append(getCustomerCount());
				} catch (Exception e) {
					res.append(errorResponse("SETICustomers", "Error in customer count").toString());
					e.printStackTrace();
				}
			} else {
				res.append(errorResponse("SETICustomers", "Username Password mismatch").toString());
			}
		}
		
		//download customers (Tested)
		if(functionName != null && functionName.equalsIgnoreCase("downloadcustomers")) {
			if(validateUser(request, siteConfig)) {
				try {
					res.append(customersList(request.getParameter("startnum") != null ? Integer.parseInt(request.getParameter("startnum")) : null, request.getParameter("batchsize") != null ? Integer.parseInt(request.getParameter("batchsize")) : null, siteConfig, gSiteConfig));
				} catch (Exception e) {
					res.append(errorResponse("SETICustomers", "Error in customer downlaod").toString());
					e.printStackTrace();
				}
			} else {
				res.append(errorResponse("SETICustomers", "Username Password mismatch").toString());
			}
		}
		
		//products count (Tested)
		if(functionName != null && functionName.equalsIgnoreCase("getproductscount")) {
			if(validateUser(request, siteConfig)) {
				try {
					res.append(getProductCount());
				} catch (Exception e) {
					res.append(errorResponse("SETIProducts", "Error in Products Count").toString());
					e.printStackTrace();
				}
			} else {
				res.append(errorResponse("SETIProducts", "Username Password mismatch").toString());
			}
		}
		
		//download products (Tested)
		if(functionName != null && functionName.equalsIgnoreCase("downloadprods")) {
			if(validateUser(request, siteConfig)) {
				try {
					res.append(productsList(request.getParameter("startnum") != null ? Integer.parseInt(request.getParameter("startnum")) : null, request.getParameter("batchsize") != null ? Integer.parseInt(request.getParameter("batchsize")) : null, siteConfig, gSiteConfig));
				} catch (Exception e) {
					res.append(errorResponse("SETIProducts", "Error in Products Download").toString());
					e.printStackTrace();
				}
			} else {
				res.append(errorResponse("SETIProducts", "Username Password mismatch").toString());
			}
		}
		
		//download QOH (Tested)
		if(functionName != null && functionName.equalsIgnoreCase("downloadqoh")) {
			if(validateUser(request, siteConfig)) {
				try {
					res.append(qohList(request.getParameter("startnum") != null ? Integer.parseInt(request.getParameter("startnum")) : null, request.getParameter("batchsize") != null ? Integer.parseInt(request.getParameter("batchsize")) : null, siteConfig, gSiteConfig));
				} catch (Exception e) {
					res.append(errorResponse("SETIProducts", "Error in QOH Download").toString());
					e.printStackTrace();
				}
			} else {
				res.append(errorResponse("SETIProducts", "Username Password mismatch").toString());
			}
		}
		
		//synch inventory for multiple skus (Tested)
		if(functionName != null && functionName.equalsIgnoreCase("qohreplace")) {
			if(validateUser(request, siteConfig)) {
				try {
					res.append(synchInventory(request));
				} catch (Exception e) {
					res.append(errorResponse("SETIProducts", "Error in Inventory Synch").toString());
					e.printStackTrace();
				}
			} else {
				res.append(errorResponse("SETIProducts", "Username Password mismatch").toString());
			}
		}
		
		
		//synch inventory for single sku (Tested)
		if(functionName != null && functionName.equalsIgnoreCase("invupdate")) {
			if(validateUser(request, siteConfig)) {
				try {
					res.append(updateInventory(request));
				} catch (Exception e) {
					res.append(errorResponse("SETIProducts", "Error in Inventory Update").toString());
					e.printStackTrace();
				}
			} else {
				res.append(errorResponse("SETIProducts", "Username Password mismatch").toString());
			}
		}
		
		
		//update order
		if(functionName != null && functionName.equalsIgnoreCase("updatestatus")) {
			if(validateUser(request, siteConfig)) {
				try {
					res.append(updateOrder(request));
				} catch (Exception e) {
					res.append(errorResponse("SETIOrders", "Error in Order Update").toString());
					e.printStackTrace();
				}
			} else {
				res.append(errorResponse("SETIOrders", "Username Password mismatch").toString());
			}
		}
		
		
		// POST data
		OutputStream oStream = response.getOutputStream();
		oStream.write(res.toString().getBytes());
		oStream.close();
		
		return null;
	}
	
	
	public boolean validateUser(HttpServletRequest request, Map<String, Configuration> siteConfig) {
		
		String username = null;
		String password = null;
		
		if(siteConfig.get( "STONEEDGE_AUTHENTICATION" ).getValue() != null && !siteConfig.get( "STONEEDGE_AUTHENTICATION" ).getValue().isEmpty()) {
			try {
				username = siteConfig.get( "STONEEDGE_AUTHENTICATION" ).getValue().split(",")[0];
				password = siteConfig.get( "STONEEDGE_AUTHENTICATION" ).getValue().split(",")[1];
			} catch(Exception e) { e.printStackTrace(); }
		}
		
		// return error if username or password is null
		if(username == null || password == null || request.getParameter("setiuser") == null || request.getParameter("password") == null) {
			return false;
		}
		
		// return error if username and password do not match
		if(!request.getParameter("setiuser").equalsIgnoreCase(username) || !request.getParameter("password").equalsIgnoreCase(password)) {
			return false;
		}
		
		return true;
	}
	public String getOrderCount(HttpServletRequest request, Map<String, Configuration> siteConfig) {
		
		int orderCount = 0;
		StringBuffer res = new StringBuffer();
		
		OrderSearch search = new OrderSearch();
		if(request.getParameter("lastorder") != null && !request.getParameter("lastorder").equalsIgnoreCase("All")) {
			Integer startNumber = 0;
			try {
				startNumber = Integer.parseInt(request.getParameter("lastorder")) + 1;
			} catch(Exception e) {
				e.printStackTrace();
				search.setOrderNumEnd("0");
			}
			search.setOrderNumStart(startNumber.toString());
		}
		
		orderCount = this.webJaguar.getOrdersListCount(search);
		System.out.println("orderCount : "+orderCount);
		
		res.append("SETIResponse: ordercount="+orderCount);
		
		return res.toString();
	}
	
	
	public String getCustomerCount() {
		
		int customerCount = 0;
		StringBuffer res = new StringBuffer();
		
		CustomerSearch search = new CustomerSearch();
		search.setAffiliate(-1);
		customerCount = this.webJaguar.getCustomerListWithDateCount(search);
		res.append("SETIResponse: itemcount="+customerCount);
		
		System.out.println("customerCount : "+customerCount);
		return res.toString();
	}
	
	public String getProductCount() {
		
		int productCount = 0;
		StringBuffer res = new StringBuffer();
		
		productCount = this.webJaguar.getProductListCount(new ProductSearch());
		res.append("SETIResponse: itemcount="+productCount);
		
		System.out.println("productCount : "+productCount);
		return res.toString();
	}
	
	public StringBuilder errorResponse ( String type, String errorMessage ) throws IOException {
		// generate Response
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document dom = null ;
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			dom = db.newDocument();
		}catch(ParserConfigurationException pce) {
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
			System.exit(1);
		}
		
		// root
		Element root = dom.createElement(type);
		dom.appendChild(root);
		
		// results
		Element response = dom.createElement("Response");
		root.appendChild(response);
		
		// result
		Element responseCode = dom.createElement("ResponseCode");
		responseCode.setTextContent("3");
		response.appendChild(responseCode);
				
		//shipping rate
		Element responseDescription = dom.createElement("ResponseDescription");
		responseDescription.setTextContent(errorMessage);
		response.appendChild(responseDescription);
					
		
		// convert to string
		StringBuilder stringBuilder = null;
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputFormat outputformat = new OutputFormat();
		outputformat.setIndenting(true);
		outputformat.setPreserveSpace(false);
		XMLSerializer serializer = new XMLSerializer();
		serializer.setOutputFormat(outputformat);
		serializer.setOutputByteStream(stream);
		serializer.asDOMSerializer();
		serializer.serialize(dom.getDocumentElement());

		stringBuilder = new StringBuilder(stream.toString());	
		return stringBuilder;
	}
	
	public StringBuilder ordersList ( Integer orderStartNumber, Integer orderCount, Map<String, Configuration> siteConfig, Map<String, Object>  gSiteConfig ) throws Exception {
		// generate Response
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document dom = null ;
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			dom = db.newDocument();
		}catch(ParserConfigurationException pce) {
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
		}
		
		
		OrderSearch search = new OrderSearch();
		if(orderCount != null) {
			search.setLimit(orderCount);
		}
		if(orderStartNumber != null) {
			search.setOrderId(orderStartNumber);
		}
		System.out.println("orderCount : "+search.getLimit()+" orderStartNumber : "+search.getOrderId());
		List<Order> orderList = this.webJaguar.getQBOrdersList(search, siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());
		
		// root
		Element root = dom.createElement("SETIOrders");
		dom.appendChild(root);
		
		// Response
		Element response = dom.createElement("Response");
		root.appendChild(response);
		
		// ResponseCode
		Element responseCode = dom.createElement("ResponseCode");
		responseCode.appendChild(dom.createTextNode("1"));
		response.appendChild(responseCode);
				
		// ResponseDescription
		Element responseDescription = dom.createElement("ResponseDescription");
		responseDescription.appendChild(dom.createTextNode("success"));
		response.appendChild(responseDescription);
				
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		NumberFormat decimalFormatter = new DecimalFormat("#0.00");

		for(Order order : orderList) {
			
			// Order
			Element Order = dom.createElement("Order");
			root.appendChild(Order);
				
			// OrderNumber
			Element OrderNumber = dom.createElement("OrderNumber");
			OrderNumber.appendChild(dom.createTextNode(""+order.getOrderId()));
			Order.appendChild(OrderNumber);
					
			// OrderDate
			Element OrderDate = dom.createElement("OrderDate");
			OrderDate.appendChild(dom.createTextNode(""+dateFormatter.format(order.getDateOrdered())));
			Order.appendChild(OrderDate);
					
			// Billing
			Element Billing = dom.createElement("Billing");
			Order.appendChild(Billing);
					
			// FullName
			Element FullName = dom.createElement("FullName");
			FullName.appendChild(dom.createTextNode(order.getBilling().getFirstName()+" "+order.getBilling().getLastName()));
			Billing.appendChild(FullName);
					
			// CompanyName
			Element Company = dom.createElement("Company");
			Company.appendChild(dom.createTextNode(order.getBilling().getCompany() != null ? order.getBilling().getCompany() : ""));
			Billing.appendChild(Company);
					
			// Phone
			Element Phone = dom.createElement("Phone");
			Phone.appendChild(dom.createTextNode(order.getBilling().getPhone()));
			Billing.appendChild(Phone);
					
			// Email
			Element Email = dom.createElement("Email");
			Email.appendChild(dom.createTextNode(order.getUserEmail()));
			Billing.appendChild(Email);
					
			// Address
			Element Address = dom.createElement("Address");
			Billing.appendChild(Address);
					
			// Street1
			Element Street1 = dom.createElement("Street1");
			Street1.appendChild(dom.createTextNode(order.getBilling().getAddr1()));
			Address.appendChild(Street1);
					
			// Street2
			Element Street2 = dom.createElement("Street2");
			Street2.appendChild(dom.createTextNode(order.getBilling().getAddr2()));
			Address.appendChild(Street2);
					
			// City
			Element City = dom.createElement("City");
			City.appendChild(dom.createTextNode(order.getBilling().getCity()));
			Address.appendChild(City);
					
			// State
			Element State = dom.createElement("State");
			State.appendChild(dom.createTextNode(order.getBilling().getStateProvince()));
			Address.appendChild(State);
					
			// Code
			Element Code = dom.createElement("Code");
			Code.appendChild(dom.createTextNode(order.getBilling().getZip()));
			Address.appendChild(Code);
					
			// Country
			Element Country = dom.createElement("Country");
			Country.appendChild(dom.createTextNode(order.getBilling().getCountry()));
			Address.appendChild(Country);
					
			//Shipping		
			Element Shipping = dom.createElement("Shipping");
			Order.appendChild(Shipping);
			
			// FullName
			Element ShipFullName = dom.createElement("FullName");
			ShipFullName.appendChild(dom.createTextNode(order.getShipping().getFirstName()+" "+order.getShipping().getLastName()));
			Shipping.appendChild(ShipFullName);
					
			// CompanyName
			Element ShipCompany = dom.createElement("Company");
			ShipCompany.appendChild(dom.createTextNode(order.getShipping().getCompany() != null ? order.getShipping().getCompany() : ""));
			Shipping.appendChild(ShipCompany);
					
			// Phone
			Element ShipPhone = dom.createElement("Phone");
			ShipPhone.appendChild(dom.createTextNode(order.getShipping().getPhone()));
			Shipping.appendChild(ShipPhone);
					
			// Email
			Element ShipEmail = dom.createElement("Email");
			ShipEmail.appendChild(dom.createTextNode(order.getUserEmail()));
			Shipping.appendChild(ShipEmail);
					
			// Address
			Element ShipAddress = dom.createElement("Address");
			Shipping.appendChild(ShipAddress);
					
			// Street1
			Element ShipStreet1 = dom.createElement("Street1");
			ShipStreet1.appendChild(dom.createTextNode(order.getShipping().getAddr1()));
			ShipAddress.appendChild(ShipStreet1);
					
			// Street2
			Element ShipStreet2 = dom.createElement("Street2");
			ShipStreet2.appendChild(dom.createTextNode(order.getShipping().getAddr2()));
			ShipAddress.appendChild(ShipStreet2);
					
			// City
			Element ShipCity = dom.createElement("City");
			ShipCity.appendChild(dom.createTextNode(order.getShipping().getCity()));
			ShipAddress.appendChild(ShipCity);
					
			// State
			Element ShipState = dom.createElement("State");
			ShipState.appendChild(dom.createTextNode(order.getShipping().getStateProvince()));
			ShipAddress.appendChild(ShipState);
					
			// Code
			Element ShipCode = dom.createElement("Code");
			ShipCode.appendChild(dom.createTextNode(order.getShipping().getZip()));
			ShipAddress.appendChild(ShipCode);
					
			// Country
			Element ShipCountry = dom.createElement("Country");
			ShipCountry.appendChild(dom.createTextNode(order.getShipping().getCountry()));
			ShipAddress.appendChild(ShipCountry);
					

			// Items
			for(LineItem lineItem : order.getLineItems()) {
				
				// Product
				Element Product = dom.createElement("Product");
				Shipping.appendChild(Product);
						
				// SKU
				Element SKU = dom.createElement("SKU");
				SKU.appendChild(dom.createTextNode(lineItem.getProduct().getSku()));
				Product.appendChild(SKU);
						
				// Name
				Element Name = dom.createElement("Name");
				Name.appendChild(dom.createTextNode(lineItem.getProduct().getName()));
				Product.appendChild(Name);
						
				// Quantity
				Element Quantity = dom.createElement("Quantity");
				Quantity.appendChild(dom.createTextNode(""+lineItem.getQuantity()));
				Product.appendChild(Quantity);
						
				// ItemPrice
				Element ItemPrice = dom.createElement("ItemPrice");
				ItemPrice.appendChild(dom.createTextNode(lineItem.getUnitPrice() != null ? lineItem.getUnitPrice().toString() : ""));
				Product.appendChild(ItemPrice);
						
				// Weight
				Element Weight = dom.createElement("Weight");
				Weight.appendChild(dom.createTextNode(lineItem.getProduct().getWeight() != null ? lineItem.getProduct().getWeight().toString() : ""));
				Product.appendChild(Weight);
						
				// ProductType
				Element ProductType = dom.createElement("ProductType");
				ProductType.appendChild(dom.createTextNode("Tangible"));
				Product.appendChild(ProductType);
						
				// Taxable
				Element Taxable = dom.createElement("Taxable");
				Taxable.appendChild(dom.createTextNode(lineItem.getProduct().isTaxable() ? "Yes" : "No"));
				Product.appendChild(Taxable);
				
				// LineID
				Element LineID = dom.createElement("LineID");
				LineID.appendChild(dom.createTextNode(""+lineItem.getLineNumber()));
				Product.appendChild(LineID);
				
				// ItemTotal
				Element ItemTotal = dom.createElement("ItemTotal");
				ItemTotal.appendChild(dom.createTextNode(lineItem.getTotalPrice() != null ?  decimalFormatter.format(lineItem.getTotalPrice()).toString() : ""));
				Product.appendChild(ItemTotal);
						
				// Dimension
				Element Dimension = dom.createElement("Dimension");
				Product.appendChild(Dimension);
						
				// Length
				Element Length = dom.createElement("Length");
				Length.appendChild(dom.createTextNode(lineItem.getProduct().getPackageL() != null ? lineItem.getProduct().getPackageL().toString() : ""));
				Dimension.appendChild(Length);
						
				// Depth
				Element Depth = dom.createElement("Depth");
				Depth.appendChild(dom.createTextNode(lineItem.getProduct().getPackageW() != null ? lineItem.getProduct().getPackageW().toString() : ""));
				Dimension.appendChild(Depth);
						
				// Height
				Element Height = dom.createElement("Height");
				Height.appendChild(dom.createTextNode(lineItem.getProduct().getPackageH() != null ? lineItem.getProduct().getPackageH().toString() : ""));
				Dimension.appendChild(Height);				
				
				
				for(ProductAttribute attr : lineItem.getProductAttributes()) {
					
					// ItemOptions
					Element OrderOption = dom.createElement("OrderOption");
					Product.appendChild(OrderOption);
					
					// OptionName
					Element OptionName = dom.createElement("OptionName");
					OptionName.appendChild(dom.createTextNode(attr.getOptionName()));
					OrderOption.appendChild(OptionName);
					
					// SelectedOption
					Element SelectedOption = dom.createElement("SelectedOption");
					SelectedOption.appendChild(dom.createTextNode(attr.getValueName()));
					OrderOption.appendChild(SelectedOption);
					
					// OptionPrice
					Element OptionPrice = dom.createElement("OptionPrice");
					OptionPrice.appendChild(dom.createTextNode(attr.getOptionPrice() != null ? attr.getOptionPrice().toString() : "0.0"));
					OrderOption.appendChild(OptionPrice);
					
					// OptionCode
					Element OptionCode = dom.createElement("OptionCode");
					OptionCode.appendChild(dom.createTextNode(attr.getOptionCode()));
					OrderOption.appendChild(OptionCode);
					
					// OptionType
					Element OptionType = dom.createElement("OptionType");
					OptionType.appendChild(dom.createTextNode("select"));
					OrderOption.appendChild(OptionType);
					
					// OptionWeight
					Element OptionWeight = dom.createElement("OptionWeight");
					OptionWeight.appendChild(dom.createTextNode(attr.getOptionWeight() != null ? attr.getOptionWeight().toString() : "0.0"));
					OrderOption.appendChild(OptionWeight);
			
				}
			}

			//Payment		
			Element Payment = dom.createElement("Payment");
			Order.appendChild(Payment);
			
		    // Credit Card
			if(order.getPaymentMethod().equalsIgnoreCase("Credit Card") && order.getCreditCard() != null) {
				
				//CreditCard		
				Element CreditCard = dom.createElement("CreditCard");
				Payment.appendChild(CreditCard);
					
				//Issuer		
				Element Issuer = dom.createElement("Issuer");
				Issuer.appendChild(dom.createTextNode(order.getCreditCard().getType()));
				CreditCard.appendChild(Issuer);
					
				//Number		
				Element Number = dom.createElement("Number");
				Number.appendChild(dom.createTextNode(order.getCreditCard().getNumber()));
				CreditCard.appendChild(Number);
					
				//ExpirationDate		
				Element ExpirationDate = dom.createElement("ExpirationDate");
				ExpirationDate.appendChild(dom.createTextNode(order.getCreditCard().getExpireMonth()+"/"+order.getCreditCard().getExpireYear()));
				CreditCard.appendChild(ExpirationDate);
					
				//CCFullName		
				Element CCFullName = dom.createElement("FullName");
				CCFullName.appendChild(dom.createTextNode(order.getBilling().getFirstName()+" "+order.getBilling().getLastName()));
				CreditCard.appendChild(CCFullName);
					
				//CCCompany		
				Element CCCompany = dom.createElement("CCCompany");
				CCCompany.appendChild(dom.createTextNode(order.getBilling().getCompany()));
				CreditCard.appendChild(CCCompany);
					
				//OrderProcessingInfo		
				Element OrderProcessingInfo = dom.createElement("OrderProcessingInfo");
				OrderProcessingInfo.appendChild(dom.createTextNode(order.getCreditCard().getPaymentNote()));
				CreditCard.appendChild(OrderProcessingInfo);
					
				//AVS		
				Element AVS = dom.createElement("AVS");
				AVS.appendChild(dom.createTextNode(order.getCreditCard().getAvsCode()));
				CreditCard.appendChild(AVS);
					
				//TransID		
				Element TransID = dom.createElement("TransID");
				TransID.appendChild(dom.createTextNode(order.getCreditCard().getTransId()));
				CreditCard.appendChild(TransID);
					
				//AuthCode		
				Element AuthCode = dom.createElement("AuthCode");
				AuthCode.appendChild(dom.createTextNode(order.getCreditCard().getAuthCode()));
				CreditCard.appendChild(AuthCode);
					
				//ProcessLevel		
				Element ProcessLevel = dom.createElement("ProcessLevel");
				String level = "none";
				if(order.getCreditCard().getTransId() != null) {
					level = "Capture";
				} else if(order.getCreditCard().getAuthCode() != null) {
					level = "Auth Only";
				}
				ProcessLevel.appendChild(dom.createTextNode(level));
				CreditCard.appendChild(ProcessLevel);
			}

		    // Paypal
			else if(order.getPaymentMethod().equalsIgnoreCase("paypal") && order.getPaypal() != null) {
				
				//PayPal		
				Element PayPal = dom.createElement("PayPal");
				Payment.appendChild(PayPal);
					
				//Transaction		
				Element Transaction = dom.createElement("Transaction");
				PayPal.appendChild(Transaction);
					
				//TransID		
				Element TransID = dom.createElement("TransID");
				TransID.appendChild(dom.createTextNode(order.getPaypal().getTxn_id()));
				Transaction.appendChild(TransID);
				
				//Status		
				Element Status = dom.createElement("Status");
				Status.appendChild(dom.createTextNode(order.getPaypal().getPayment_status()));
				Transaction.appendChild(Status);
					
				//TransDate		
				Element TransDate = dom.createElement("TransDate");
				TransDate.appendChild(dom.createTextNode(order.getPaypal().getPayment_date()));
				Transaction.appendChild(TransDate);
				
			}
			
		    // GiftCard
			else if(order.getPaymentMethod().equalsIgnoreCase("giftcard") && order.getGiftCard() != null) {
				
				//GiftCard		
				Element GiftCard = dom.createElement("GiftCard");
				Payment.appendChild(GiftCard);
				
				//FaceValue		
				Element FaceValue = dom.createElement("FaceValue");
				FaceValue.appendChild(dom.createTextNode(order.getGiftCard().getAmount() != null ? order.getGiftCard().getAmount().toString() : ""));
				GiftCard.appendChild(FaceValue);
					
				if(order.getGiftCard().getCreditCard() != null) {
					//Number		
					Element Number = dom.createElement("Number");
					Number.appendChild(dom.createTextNode(order.getGiftCard().getCreditCard().getNumber()));
					GiftCard.appendChild(Number);
						
					if(order.getGiftCard().getCreditCard().getExpireMonth() != null  && order.getGiftCard().getCreditCard().getExpireYear() != null) {
						//ExpirationDate		
						Element ExpirationDate = dom.createElement("ExpirationDate");
						ExpirationDate.appendChild(dom.createTextNode(order.getGiftCard().getCreditCard().getExpireMonth()+"/"+order.getGiftCard().getCreditCard().getExpireYear()));
						GiftCard.appendChild(ExpirationDate);
					}
						
					if(order.getGiftCard().getCreditCard().getTransId() != null) {
						//TransID		
						Element TransID = dom.createElement("TransID");
						TransID.appendChild(dom.createTextNode(order.getGiftCard().getCreditCard().getTransId()));
						GiftCard.appendChild(TransID);
					}
						
					if(order.getGiftCard().getCreditCard().getAuthCode() != null) {
						//AuthCode		
						Element AuthCode = dom.createElement("AuthCode");
						AuthCode.appendChild(dom.createTextNode(order.getGiftCard().getCreditCard().getAuthCode()));
						GiftCard.appendChild(AuthCode);
					}
				}
			}
			
			else {

				//Generic1		
				Element Generic1 = dom.createElement("Generic1");
				Payment.appendChild(Generic1);
				
				//Name		
				Element Name = dom.createElement("Name");
				Name.appendChild(dom.createTextNode(order.getPaymentMethod()));
				Generic1.appendChild(Name);
					
				//Description		
				Element Description = dom.createElement("Description");
				Description.appendChild(dom.createTextNode(order.getPaymentMethod()));
				Generic1.appendChild(Description);
				
				//Field1		
				Element Field1 = dom.createElement("Field1");
				Element Field2 = dom.createElement("Field2");
				Element Field3 = dom.createElement("Field3");
				Element Field4 = dom.createElement("Field4");
				if(order.getGeMoney() != null && order.getPaymentMethod().equalsIgnoreCase("GE Money")) {
					Field1.appendChild(dom.createTextNode("Account Number : "+order.getGeMoney().getAcctNumber()));
					Field2.appendChild(dom.createTextNode("Auth Code : "+order.getGeMoney().getAuthCode()));
					Field3.appendChild(dom.createTextNode("Status : "+order.getGeMoney().getStatus()));
					Field4.appendChild(dom.createTextNode("Trans Date : "+order.getGeMoney().getTransDate()));
				}
				else if( order.getNetCommerce() != null && order.getPaymentMethod().equalsIgnoreCase("netcommerce")) {
					Field1.appendChild(dom.createTextNode("Merchant Number : "+order.getNetCommerce().getTxtMerchNum()));
					Field2.appendChild(dom.createTextNode("Aut Number : "+order.getNetCommerce().getTxtNumAut() ));
					Field3.appendChild(dom.createTextNode("Response Value : "+order.getNetCommerce().getRespVal()));
					Field4.appendChild(dom.createTextNode("Payment Date : "+order.getNetCommerce().getPaymentDate()));
				}
				else if(order.getBankAudi() != null && order.getPaymentMethod().equalsIgnoreCase("bankaudi")) {
					Field1.appendChild(dom.createTextNode("Autho Id : "+order.getBankAudi().getVpc_AuthorizeId()));
					Field2.appendChild(dom.createTextNode("Response Code : "+order.getBankAudi().getVpc_AcqResponseCode()));
					Field3.appendChild(dom.createTextNode("Trans Number : "+order.getBankAudi().getVpc_TransactionNo()));
					Field4.appendChild(dom.createTextNode("Trans Date : "+order.getBankAudi().getPaymentDate()));
				}
				else if(order.geteBillme() != null && order.getPaymentMethod().equalsIgnoreCase("ebillme")) {
					Field1.appendChild(dom.createTextNode("Order Number : "+order.geteBillme().getOrdernumber()));
					Field2.appendChild(dom.createTextNode("Buyer Account Number : "+order.geteBillme().getBuyer_accountnumber()));
					Field3.appendChild(dom.createTextNode("Status : "+order.geteBillme().getAuthstatus()));
					Field4.appendChild(dom.createTextNode("Order Ref ID : "+order.geteBillme().getOrderrefid()));
				}
				else if(order.getGoogleCheckOut() != null && order.getPaymentMethod().equalsIgnoreCase("Google")) {
					Field1.appendChild(dom.createTextNode("Google Order Id : "+order.getGoogleCheckOut().getGoogleOrderId()));
					Field2.appendChild(dom.createTextNode("Order Id : "+order.getOrderId()));
					Field3.appendChild(dom.createTextNode("Payment Date : "+order.getGoogleCheckOut().getGooglePaymentDate()));
				}
				else if(order.getAmazonIopn() != null && order.getPaymentMethod().equalsIgnoreCase("amazon")) {
					Field1.appendChild(dom.createTextNode("Amazon Order Number : "+order.getAmazonIopn().getAmazonOrderID()));
					Field2.appendChild(dom.createTextNode("Notification Ref ID : "+order.getAmazonIopn().getNotificationRefId()));
					Field3.appendChild(dom.createTextNode("Order Number : "+order.getAmazonIopn().getOrderNumber()));
					Field4.appendChild(dom.createTextNode("Payment Date : "+order.getAmazonIopn().getTimestamp()));
				}
				Generic1.appendChild(Field1);
				Generic1.appendChild(Field2);
				Generic1.appendChild(Field3);
				Generic1.appendChild(Field4);
			}
			
			
			//Totals		
			Element Totals = dom.createElement("Totals");
			Order.appendChild(Totals);

			//ProductTotal		
			Element ProductTotal = dom.createElement("ProductTotal");
			ProductTotal.appendChild(dom.createTextNode(order.getSubTotal() != null ? order.getSubTotal().toString() : ""));
			Totals.appendChild(ProductTotal);

			//Discount		
			Element Discount = dom.createElement("Discount");
			Totals.appendChild(Discount);

			//Amount		
			Element Amount = dom.createElement("Amount");
			Double discount = (order.getPromoAmount() != null ? order.getPromoAmount() : 0.0) + (order.getShippingPromoAmount() != null ? order.getShippingPromoAmount() : 0.0) + (order.getLineItemPromoAmount() != null ? order.getLineItemPromoAmount() : 0.0);
			Amount.appendChild(dom.createTextNode(discount.toString()));
			Totals.appendChild(Amount);

			//SubTotal		
			Element SubTotal = dom.createElement("SubTotal");
			SubTotal.appendChild(dom.createTextNode(order.getSubTotal() != null ? order.getSubTotal().toString() : ""));
			Totals.appendChild(SubTotal);

			
			if(order.getTax() != null) {
				//Tax		
				Element Tax = dom.createElement("Tax");
				Totals.appendChild(Tax);

				//TaxAmount		
				Element TaxAmount = dom.createElement("TaxAmount");
				TaxAmount.appendChild(dom.createTextNode(order.getTax().toString()));
				Tax.appendChild(TaxAmount);
			}
			
			//GrandTotal		
			Element GrandTotal = dom.createElement("GrandTotal");
			GrandTotal.appendChild(dom.createTextNode(order.getGrandTotal() != null ? order.getGrandTotal().toString() : ""));
			Totals.appendChild(GrandTotal);

			if(order.getShippingCost() != null) {
				
				//ShippingTotal		
				Element ShippingTotal = dom.createElement("ShippingTotal");
				Totals.appendChild(ShippingTotal);

				//Total		
				Element Total = dom.createElement("Total");
				Total.appendChild(dom.createTextNode(order.getShippingCost().toString()));
				ShippingTotal.appendChild(Total);

				//Description		
				Element Description = dom.createElement("Description");
				Description.appendChild(dom.createTextNode(order.getShippingMethod()));
				ShippingTotal.appendChild(Description);

			}
			
			//Others		
			Element Others = dom.createElement("Others");
			Order.appendChild(Others);

			//Comments		
			Element Comments = dom.createElement("Comments");
			Comments.appendChild(dom.createTextNode(order.getInvoiceNote()));
			Others.appendChild(Comments);

			//IPHostName		
			Element IPHostName = dom.createElement("IPHostName");
			IPHostName.appendChild(dom.createTextNode(order.getIpAddress()));
			Others.appendChild(IPHostName);

			if(order.getTotalWeight() != null) {
				//TotalOrderWeight		
				Element TotalOrderWeight = dom.createElement("TotalOrderWeight");
				TotalOrderWeight.appendChild(dom.createTextNode(order.getTotalWeight().toString()));
				Others.appendChild(TotalOrderWeight);
			}

			//WebCustomerID		
			Element WebCustomerID = dom.createElement("WebCustomerID");
			WebCustomerID.appendChild(dom.createTextNode(order.getUserId() != null ? order.getUserId().toString() : ""));
			Others.appendChild(WebCustomerID);

		}
		
		// convert to string
		StringBuilder stringBuilder = null;
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputFormat outputformat = new OutputFormat();
		outputformat.setIndenting(true);
		outputformat.setPreserveSpace(false);
		XMLSerializer serializer = new XMLSerializer();
		serializer.setOutputFormat(outputformat);
		serializer.setOutputByteStream(stream);
		serializer.asDOMSerializer();
		serializer.serialize(dom.getDocumentElement());

		stringBuilder = new StringBuilder(stream.toString());	
		return stringBuilder;
	}

	public StringBuilder customersList ( Integer customerStartNumber, Integer customerCount, Map<String, Configuration> siteConfig, Map<String, Object>  gSiteConfig ) throws Exception {
		// generate Response
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document dom = null ;
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			dom = db.newDocument();
		}catch(ParserConfigurationException pce) {
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
		}
		
		
		CustomerSearch search = new CustomerSearch();
		if(customerCount != null) {
			search.setLimit(customerCount);
		}
		if(customerStartNumber != null && customerStartNumber > 0) {
			search.setOffset(customerStartNumber -1 );
		}
		System.out.println("customerCount : "+search.getLimit()+" customerStartNumber : "+search.getOffset());
		List<Customer> customerList = this.webJaguar.getCustomers(search);
		System.out.println("Size : "+customerList.size());
		
		// root
		Element root = dom.createElement("SETICustomers");
		dom.appendChild(root);
		
		// Response
		Element response = dom.createElement("Response");
		root.appendChild(response);
		
		// ResponseCode
		Element responseCode = dom.createElement("ResponseCode");
		responseCode.appendChild(dom.createTextNode("1"));
		response.appendChild(responseCode);
				
		// ResponseDescription
		Element responseDescription = dom.createElement("ResponseDescription");
		responseDescription.appendChild(dom.createTextNode("success"));
		response.appendChild(responseDescription);
				
		for(Customer customer : customerList) {
			
			// Customer
			Element Customer = dom.createElement("Customer");
			root.appendChild(Customer);
				
			// WebID
			Element WebID = dom.createElement("WebID");
			WebID.appendChild(dom.createTextNode(""+customer.getId()));
			Customer.appendChild(WebID);
					
			// UserName
			Element UserName = dom.createElement("UserName");
			UserName.appendChild(dom.createTextNode(customer.getUsername()));
			Customer.appendChild(UserName);
					
			// BillAddr
			Element BillAddr = dom.createElement("BillAddr");
			Customer.appendChild(BillAddr);
					
			// FirstName
			Element FirstName = dom.createElement("FirstName");
			FirstName.appendChild(dom.createTextNode(customer.getAddress().getFirstName()));
			BillAddr.appendChild(FirstName);
					
			// LastName
			Element LastName = dom.createElement("LastName");
			LastName.appendChild(dom.createTextNode(customer.getAddress().getLastName()));
			BillAddr.appendChild(LastName);
					
			// Company
			Element Company = dom.createElement("Company");
			Company.appendChild(dom.createTextNode(customer.getAddress().getCompany() != null ? customer.getAddress().getCompany() : ""));
			BillAddr.appendChild(Company);
					
			// Phone
			Element Phone = dom.createElement("Phone");
			Phone.appendChild(dom.createTextNode(customer.getAddress().getPhone()));
			BillAddr.appendChild(Phone);
					
			// Fax
			Element Fax = dom.createElement("Fax");
			Fax.appendChild(dom.createTextNode(customer.getAddress().getFax()));
			BillAddr.appendChild(Fax);
					
			// Email
			Element Email = dom.createElement("Email");
			Email.appendChild(dom.createTextNode(customer.getUsername()));
			BillAddr.appendChild(Email);
					
			// TaxIDNumber
			Element TaxIDNumber = dom.createElement("TaxIDNumber");
			TaxIDNumber.appendChild(dom.createTextNode(customer.getTaxId()));
			BillAddr.appendChild(TaxIDNumber);
					
			// Address
			Element Address = dom.createElement("Address");
			BillAddr.appendChild(Address);
			
			// Addr1
			Element Addr1 = dom.createElement("Addr1");
			Addr1.appendChild(dom.createTextNode(customer.getAddress().getAddr1()));
			Address.appendChild(Addr1);
					
			// Addr2
			Element Addr2 = dom.createElement("Addr2");
			Addr2.appendChild(dom.createTextNode(customer.getAddress().getAddr2()));
			Address.appendChild(Addr2);
					
			// City
			Element City = dom.createElement("City");
			City.appendChild(dom.createTextNode(customer.getAddress().getCity()));
			Address.appendChild(City);
					
			// State
			Element State = dom.createElement("State");
			State.appendChild(dom.createTextNode(customer.getAddress().getStateProvince()));
			Address.appendChild(State);
					
			// Zip
			Element Zip = dom.createElement("Zip");
			Zip.appendChild(dom.createTextNode(customer.getAddress().getZip()));
			Address.appendChild(Zip);
					
			// Country
			Element Country = dom.createElement("Country");
			Country.appendChild(dom.createTextNode(customer.getAddress().getCountry()));
			Address.appendChild(Country);
					
			//ShipAddr		
			Element ShipAddr = dom.createElement("ShipAddr");
			Customer.appendChild(ShipAddr);
			
			// ShipFirstName
			Element ShipFirstName = dom.createElement("FirstName");
			ShipFirstName.appendChild(dom.createTextNode(customer.getAddress().getFirstName()));
			ShipAddr.appendChild(ShipFirstName);
					
			// ShipLastName
			Element ShipLastName = dom.createElement("LastName");
			ShipLastName.appendChild(dom.createTextNode(customer.getAddress().getLastName()));
			ShipAddr.appendChild(ShipLastName);
					
			// ShipCompany
			Element ShipCompany = dom.createElement("Company");
			ShipCompany.appendChild(dom.createTextNode(customer.getAddress().getCompany() != null ? customer.getAddress().getCompany() : ""));
			ShipAddr.appendChild(ShipCompany);
					
			// ShipPhone
			Element ShipPhone = dom.createElement("Phone");
			ShipPhone.appendChild(dom.createTextNode(customer.getAddress().getPhone()));
			ShipAddr.appendChild(ShipPhone);
					
			// ShipEmail
			Element ShipEmail = dom.createElement("Email");
			ShipEmail.appendChild(dom.createTextNode(customer.getUsername()));
			ShipAddr.appendChild(ShipEmail);
					
			// ShipAddress
			Element ShipAddress = dom.createElement("Address");
			ShipAddr.appendChild(ShipAddress);
			
			// ShipAddr1
			Element ShipAddr1 = dom.createElement("Addr1");
			ShipAddr1.appendChild(dom.createTextNode(customer.getAddress().getAddr1()));
			ShipAddress.appendChild(ShipAddr1);
					
			// ShipAddr2
			Element ShipAddr2 = dom.createElement("Addr2");
			ShipAddr2.appendChild(dom.createTextNode(customer.getAddress().getAddr2()));
			ShipAddress.appendChild(ShipAddr2);
					
			// ShipCity
			Element ShipCity = dom.createElement("City");
			ShipCity.appendChild(dom.createTextNode(customer.getAddress().getCity()));
			ShipAddress.appendChild(ShipCity);
					
			// ShipState
			Element ShipState = dom.createElement("State");
			ShipState.appendChild(dom.createTextNode(customer.getAddress().getStateProvince()));
			ShipAddress.appendChild(ShipState);
					
			// ShipZip
			Element ShipZip = dom.createElement("Zip");
			ShipZip.appendChild(dom.createTextNode(customer.getAddress().getZip()));
			ShipAddress.appendChild(ShipZip);
					
			// ShipCountry
			Element ShipCountry = dom.createElement("Country");
			ShipCountry.appendChild(dom.createTextNode(customer.getAddress().getCountry()));
			ShipAddress.appendChild(ShipCountry);
					
			// CustomFields
			Element CustomFields = dom.createElement("CustomFields");
			Customer.appendChild(CustomFields);
					
			// Items
			if(customer.getCustomerFields() != null) {
				for(CustomerField field : customer.getCustomerFields()) {
					
					// CustomFields
					Element CustomField = dom.createElement("CustomField");
					CustomFields.appendChild(CustomField);
							
					// FieldName
					Element FieldName = dom.createElement("FieldName");
					FieldName.appendChild(dom.createTextNode(field.getName()));
					CustomField.appendChild(FieldName);
							
					// FieldValue
					Element FieldValue = dom.createElement("FieldValue");
					FieldValue.appendChild(dom.createTextNode(field.getValue()));
					CustomField.appendChild(FieldValue);
				}
			}
		}
		// convert to string
		StringBuilder stringBuilder = null;
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputFormat outputformat = new OutputFormat();
		outputformat.setIndenting(true);
		outputformat.setPreserveSpace(false);
		XMLSerializer serializer = new XMLSerializer();
		serializer.setOutputFormat(outputformat);
		serializer.setOutputByteStream(stream);
		serializer.asDOMSerializer();
		serializer.serialize(dom.getDocumentElement());

		stringBuilder = new StringBuilder(stream.toString());	
		return stringBuilder;
	}


	public StringBuilder productsList ( Integer productStartNumber, Integer productCount, Map<String, Configuration> siteConfig, Map<String, Object>  gSiteConfig ) throws Exception {
		// generate Response
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document dom = null ;
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			dom = db.newDocument();
		}catch(ParserConfigurationException pce) {
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
		}
		
		
		ProductSearch search = new ProductSearch();
		if(productCount != null) {
			search.setLimit(productCount);
		}
		if(productStartNumber != null && productStartNumber > 0) {
			search.setOffset(productStartNumber -1);
		}
		System.out.println("productCount : "+search.getLimit()+" productStartNumber : "+search.getOffset());
		List<Product> productsList = this.webJaguar.getStoneEdgeProductList(search);
		System.out.println("Size : "+productsList.size());
		
		// root
		Element root = dom.createElement("SETIProducts");
		dom.appendChild(root);
		
		// Response
		Element response = dom.createElement("Response");
		root.appendChild(response);
		
		// ResponseCode
		Element responseCode = dom.createElement("ResponseCode");
		responseCode.appendChild(dom.createTextNode("1"));
		response.appendChild(responseCode);
				
		// ResponseDescription
		Element responseDescription = dom.createElement("ResponseDescription");
		responseDescription.appendChild(dom.createTextNode("success"));
		response.appendChild(responseDescription);
				
		for(Product product : productsList) {
			
			// Product
			Element Product = dom.createElement("Product");
			root.appendChild(Product);
				
			// Code
			Element Code = dom.createElement("Code");
			Code.appendChild(dom.createTextNode(product.getSku()));
			Product.appendChild(Code);
					
			// WebID
			Element WebID = dom.createElement("WebID");
			WebID.appendChild(dom.createTextNode(product.getId().toString()));
			Product.appendChild(WebID);
					
			// Name
			Element Name = dom.createElement("Name");
			Name.appendChild(dom.createTextNode(product.getName()));
			Product.appendChild(Name);
					
			// Price
			Element Price = dom.createElement("Price");
			Price.appendChild(dom.createTextNode(product.getPrice1() != null ? product.getPrice1().toString() : "0.0"));
			Product.appendChild(Price);
					
			// Description
			Element Description = dom.createElement("Description");
			Description.appendChild(dom.createTextNode(product.getShortDesc()));
			Product.appendChild(Description);
					
			// Weight
			Element Weight = dom.createElement("Weight");
			Weight.appendChild(dom.createTextNode(product.getWeight().toString()));
			Product.appendChild(Weight);
					
			// Taxable
			Element Taxable = dom.createElement("Taxable");
			Taxable.appendChild(dom.createTextNode(product.isTaxable() ? "Yes" : "No"));
			Product.appendChild(Taxable);
					
			// Discontinued
			Element Discontinued = dom.createElement("Discontinued");
			Discontinued.appendChild(dom.createTextNode(product.isActive() ? "No" : "Yes"));
			Product.appendChild(Discontinued);
					
			// QOH
			Element QOH = dom.createElement("QOH");
			QOH.appendChild(dom.createTextNode(product.getInventoryAFS() != null ? product.getInventoryAFS().toString() : "0"));
			Product.appendChild(QOH);
					
			if(product.getProductFields() != null) {
				// CustomFields
				Element CustomFields = dom.createElement("CustomFields");
				Product.appendChild(CustomFields);
				
				for(ProductField field : product.getProductFields()) {
					// CustomField
					Element CustomField = dom.createElement("CustomField");
					CustomFields.appendChild(CustomField);
					
					// FieldName
					Element FieldName = dom.createElement("FieldName");
					FieldName.appendChild(dom.createTextNode(field.getName()));
					CustomField.appendChild(FieldName);
							
					// FieldValue
					Element FieldValue = dom.createElement("FieldValue");
					FieldValue.appendChild(dom.createTextNode(field.getValue()));
					CustomField.appendChild(FieldValue);
				}
			}
					
		}
		// convert to string
		StringBuilder stringBuilder = null;
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputFormat outputformat = new OutputFormat();
		outputformat.setIndenting(true);
		outputformat.setPreserveSpace(false);
		XMLSerializer serializer = new XMLSerializer();
		serializer.setOutputFormat(outputformat);
		serializer.setOutputByteStream(stream);
		serializer.asDOMSerializer();
		serializer.serialize(dom.getDocumentElement());

		stringBuilder = new StringBuilder(stream.toString());	
		return stringBuilder;
	}

	public StringBuilder qohList ( Integer productStartNumber, Integer productCount, Map<String, Configuration> siteConfig, Map<String, Object>  gSiteConfig ) throws Exception {
		// generate Response
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document dom = null ;
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			dom = db.newDocument();
		}catch(ParserConfigurationException pce) {
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
		}
		
		
		ProductSearch search = new ProductSearch();
		if(productCount != null) {
			search.setLimit(productCount);
		}
		if(productStartNumber != null && productStartNumber > 0) {
			search.setOffset(productStartNumber - 1);
		}
		List<Product> productsList = this.webJaguar.getStoneEdgeProductList(search);
		System.out.println("quhproductCount : "+search.getLimit()+" quhproductStartNumber : "+search.getOffset());
		System.out.println("quhSize : "+productsList.size());
		
		// root
		Element root = dom.createElement("SETIProducts");
		dom.appendChild(root);
		
		// Response
		Element response = dom.createElement("Response");
		root.appendChild(response);
		
		// ResponseCode
		Element responseCode = dom.createElement("ResponseCode");
		responseCode.appendChild(dom.createTextNode("1"));
		response.appendChild(responseCode);
				
		// ResponseDescription
		Element responseDescription = dom.createElement("ResponseDescription");
		responseDescription.appendChild(dom.createTextNode("success"));
		response.appendChild(responseDescription);
				
		for(Product product : productsList) {
			
			// Product
			Element Product = dom.createElement("Product");
			root.appendChild(Product);
				
			// Code
			Element Code = dom.createElement("Code");
			Code.appendChild(dom.createTextNode(product.getSku()));
			Product.appendChild(Code);
					
			// WebID
			Element WebID = dom.createElement("WebID");
			WebID.appendChild(dom.createTextNode(product.getId().toString()));
			Product.appendChild(WebID);
					
			// QOH
			Element QOH = dom.createElement("QOH");
			QOH.appendChild(dom.createTextNode(product.getInventoryAFS() != null ? product.getInventoryAFS().toString() : "0"));
			Product.appendChild(QOH);
					
		}
		// convert to string
		StringBuilder stringBuilder = null;
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputFormat outputformat = new OutputFormat();
		outputformat.setIndenting(true);
		outputformat.setPreserveSpace(false);
		XMLSerializer serializer = new XMLSerializer();
		serializer.setOutputFormat(outputformat);
		serializer.setOutputByteStream(stream);
		serializer.asDOMSerializer();
		serializer.serialize(dom.getDocumentElement());

		stringBuilder = new StringBuilder(stream.toString());	
		return stringBuilder;
	}

	public StringBuilder synchInventory( HttpServletRequest request) throws Exception {
		// generate Response
		
		DocumentBuilderFactory factory = null;
	    DocumentBuilder builder = null;
	    Document update = null;
	    StringBuffer res = new StringBuffer();
		res.append("SETIResponse\n");
	    try {
	      factory = DocumentBuilderFactory.newInstance();
	      builder = factory.newDocumentBuilder();
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    try {
	    	System.out.println("Update Tag "+request.getParameter("update"));
	    	update = builder.parse(new InputSource(new StringReader(request.getParameter("update"))));
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    update.getDocumentElement().normalize();
		
		JAXBContext context = JAXBContext.newInstance(SETIProductsType.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		StringBuffer sb = new StringBuffer(request.getParameter("update"));
		ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
		SETIProductsType products = (SETIProductsType) unmarshaller.unmarshal(bis);
		
		
		
	    
	   // NodeList nodeList = update.getElementsByTagName("Product");
		
		Inventory inventory = null;
		InventoryActivity inventoryActivity = null;
	//	System.out.println("nodeListSynchInventory : "+nodeList.getLength());
		for(ProductType product : products.getProduct()) {
			String sku = product.getSKU();
			Integer qoh =  Integer.parseInt(product.getQOH().toString());
			
		//for(int i=0; i<nodeList.getLength(); i++) {
		/*	NodeList chileNodeList = nodeList.item(i).getChildNodes();
			Node node1 = chileNodeList.item(0);
			Node node2 = chileNodeList.item(1);
			String sku = null;
			Integer qoh = null;
			System.out.println("Node1 Name : "+node1.getLocalName()+"Node1 Value : "+node1.getTextContent()+" Node2 Value : "+node2.getTextContent());
			
			if(node1.getNodeName().equalsIgnoreCase("SKU")) {
				sku = node1.getTextContent();
			} 
			if(node2.getNodeName().equalsIgnoreCase("QOH")) {
				qoh = Integer.parseInt(node2.getTextContent());
			}*/
			
			if(sku != null && qoh != null) {
				//using same function developed by Rewati for Inventory management
				try {
					inventory = new Inventory();
					inventory.setSku(sku);
					inventory = this.webJaguar.getInventory(inventory);						
					
					if (inventory != null && inventory.getInventory() != null && (qoh-(inventory.getInventoryAFS() != null ? inventory.getInventoryAFS() : 0)  != 0 )) {
						inventoryActivity = new InventoryActivity();
						inventoryActivity.setSku(sku);
						inventoryActivity.setInventory(inventory.getInventory()+qoh-inventory.getInventoryAFS());
						inventoryActivity.setQuantity(qoh-inventory.getInventoryAFS());
						inventoryActivity.setAccessUserId(0);
						inventoryActivity.setType("STONEEDGE");
						this.webJaguar.adjustInventoryBySku(inventoryActivity);	
					} 
					
					if(inventory != null && inventory.getInventory() != null) {
						res.append(sku+"="+"OK\n");
					} else if(inventory != null && inventory.getInventory() == null) {
						res.append(sku+"="+"NA\n");
					} else {
						res.append(sku+"="+"NF\n");
					}
				} catch (Exception e) { e.printStackTrace(); }
			}
		}
		res.append("SETIEndOfData");
			
		// convert to string
		StringBuilder stringBuilder = new StringBuilder(res.toString());	
		return stringBuilder;
	}

	public StringBuilder updateInventory( HttpServletRequest request) throws Exception {
		// generate Response
		
		DocumentBuilderFactory factory = null;
	    DocumentBuilder builder = null;
	    Document update = null;
	    StringBuffer res = new StringBuffer();
		try {
	      factory = DocumentBuilderFactory.newInstance();
	      builder = factory.newDocumentBuilder();
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    try {
	    	System.out.println("Update "+request.getParameter("update"));
	    	update = builder.parse(new InputSource(new StringReader(request.getParameter("update"))));
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    update.getDocumentElement().normalize();
		//NodeList nodeList = update.getElementsByTagName("Product");
		
		Inventory inventory = null;
		InventoryActivity inventoryActivity = null;
		
		
		update.getDocumentElement().normalize();
		
		JAXBContext context = JAXBContext.newInstance(SETIProductsType.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		StringBuffer sb = new StringBuffer(request.getParameter("update"));
		ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
		SETIProductsType products = (SETIProductsType) unmarshaller.unmarshal(bis);
		
		
		for(ProductType product : products.getProduct()) {
		//for(int i=0; i<nodeList.getLength(); i++) {
			//NodeList chileNodeList = nodeList.item(i).getChildNodes();
			//Node node1 = chileNodeList.item(0);
			//Node node2 = chileNodeList.item(1);
			String sku = product.getSKU();
			Integer adjustment =  Integer.parseInt(product.getQOH().toString());
			
			/*if(node1.getNodeName().equalsIgnoreCase("SKU")) {
				sku = node1.getTextContent();
			} 
			if(node2.getNodeName().equalsIgnoreCase("QOH")) {
				adjustment = Integer.parseInt(node2.getTextContent());
			}*/
			System.out.println("sku : "+sku+" adjustment "+adjustment);
			
			if(sku != null && adjustment != null) {
				//using same function developed by Rewati for Inventory management
				try {
					inventory = new Inventory();
					inventory.setSku(sku);
					inventory = this.webJaguar.getInventory(inventory);						
					if (inventory != null && inventory.getInventory() != null && (adjustment != 0 )) {
						inventoryActivity = new InventoryActivity();
						inventoryActivity.setSku(sku);
						inventoryActivity.setInventory(inventory.getInventory()+adjustment);
						inventoryActivity.setQuantity(adjustment);
						inventoryActivity.setAccessUserId(0);
						inventoryActivity.setType("STONEEDGE");
						this.webJaguar.adjustInventoryBySku(inventoryActivity);	
					} 
					
					if(inventory != null && inventory.getInventory() != null) {
						res.append("SETIRESPONSE=OK;SKU="+sku+";QOH="+(inventory.getInventoryAFS()+adjustment)+";NOTE=");
					} else if(inventory != null && inventory.getInventory() == null) {
						res.append("SETIRESPONSE=False;SKU="+sku+";QOH=NA;NOTE=NotTracking");
					} else {
						res.append("SETIRESPONSE=False;SKU="+sku+";QOH=NF;NOTE=NotFound");
					}
				} catch (Exception e) { e.printStackTrace(); }
			}
		}
			
		// convert to string
		StringBuilder stringBuilder = new StringBuilder(res.toString());	
		return stringBuilder;
	}

	public StringBuilder updateOrder( HttpServletRequest request) throws Exception {
		// generate Response
		DocumentBuilderFactory factory = null;
	    DocumentBuilder builder = null;
	    Document update = null;
	    StringBuffer res = new StringBuffer();
	    Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
	    try {
	      factory = DocumentBuilderFactory.newInstance();
	      builder = factory.newDocumentBuilder();
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    try {
	    	update = builder.parse(new InputSource(new StringReader(request.getParameter("update"))));
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    update.getDocumentElement().normalize();
		System.out.println("Order Status Update "+request.getParameter("update"));
		JAXBContext context = JAXBContext.newInstance(OrdersType.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		StringBuffer sb = new StringBuffer(request.getParameter("update"));
		ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
		OrdersType orders = (OrdersType) unmarshaller.unmarshal(bis);
		
		Map<BigInteger, PackageType> packingTypeMap = new HashMap<BigInteger, PackageType>();
		Map<BigInteger, PackingList> packingListMap = new HashMap<BigInteger, PackingList>();
		for(OrderType order : orders.getOrder()) {
			Order webOrder = this.webJaguar.getOrder(Integer.parseInt(order.getOrderNumber()), "");
			// continue if order does not exist
			if(webOrder == null) {
				continue;
			}
			
			if(order.getPackages() != null) {
				for(PackageType pkg : order.getPackages().getPackage()){
					packingTypeMap.put(pkg.getPackageID(), pkg);
				}
			}
			
			System.out.println("PackingType Map : "+packingTypeMap);
			System.out.println("updateOrder : "+order.getOrderNumber());
			if(order.getItems() != null) {
				System.out.println("Items Size : "+order.getItems().getItem().size());
				for(ItemType item : order.getItems().getItem()){
					if(item.getPackages() != null) {
						System.out.println("Item Packages Size: "+item.getPackages().getPackage().size());
						for(ItemPackageType itemPackage : item.getPackages().getPackage()) {
							PackageType tempPackage = null;
							PackingList packingList = null;
							if(packingTypeMap.get(itemPackage.getPackageID()) != null) {
								tempPackage = packingTypeMap.get(itemPackage.getPackageID());
							}
							System.out.println("tempPackage: "+tempPackage);
							
							if(packingListMap.get(itemPackage.getPackageID()) != null) {
								packingList = packingListMap.get(itemPackage.getPackageID());
							} else {
								packingList = new PackingList();
								packingList.setOrderId(Integer.parseInt(order.getOrderNumber()));
								packingList.setTracking(tempPackage != null ? tempPackage.getTrackingID() : null);
								packingList.setPackingListNumber(order.getOrderNumber() + "-" + this.webJaguar.packingListCount(Integer.parseInt(order.getOrderNumber())));
								packingListMap.put(itemPackage.getPackageID(), packingList);
							}
							System.out.println("packingList OrderId : "+packingList.getOrderId()+" Tracking "+packingList.getTracking()+" Packing List number "+packingList.getPackingListNumber());
							System.out.println("packingListMap : "+packingListMap);
							
							LineItem lineItem = new LineItem();
							lineItem.setToBeShipQty( Integer.parseInt(itemPackage.getQuantity().toString()));
							System.out.println("lineItem To Be Ship Qty : "+lineItem.getToBeShipQty());
							
							Product product = new Product();
							product.setSku(item.getItemNumber());
							lineItem.setProduct(product);
							
							//set line number
							for(LineItem webOrderLineItem : webOrder.getLineItems()) {
								if(webOrderLineItem.getProduct().getSku().equalsIgnoreCase(item.getItemNumber())) {
									lineItem.setLineNumber(webOrderLineItem.getLineNumber());
									lineItem.setQuantity( webOrderLineItem.getQuantity() );
									break;
								}
							}
							
							lineItem.setOrderId(webOrder.getOrderId());
							packingList.getLineItems().add(lineItem);
						}
					}
				}
			}
			
			System.out.println("Order Status : "+order.getStatus() + " Weborder Status "+webOrder.getStatus());
			if(order.getStatus() != null && !order.getStatus().equalsIgnoreCase(webOrder.getStatus())) {
				String oldStatus = webOrder.getStatus();
				if(order.getStatus().equalsIgnoreCase("pending")) {
					webOrder.setStatus("p");
				} else if(order.getStatus().equalsIgnoreCase("processing")) {
					webOrder.setStatus("pr");
				} else if(order.getStatus().equalsIgnoreCase("canceled")) {
					webOrder.setStatus("x");
				} else if(order.getStatus().equalsIgnoreCase("shipped")) {
					webOrder.setStatus("s");
				}
				
				if(!oldStatus.equalsIgnoreCase(webOrder.getStatus())) {
					OrderStatus orderStatus = new OrderStatus();
					
					Map<String, Object> gSiteConfig = globalDao.getGlobalSiteConfig();
    				
					// cancelled order
	    			if (webOrder.getStatus().equalsIgnoreCase("x")) {    				
	    				// inventory
	    				if ((Boolean) gSiteConfig.get("gINVENTORY")) {
	    					// Delete packing list
	        				List<String> listOfPacking= this.webJaguar.getPackisListNumByOrderId(webOrder.getOrderId());
	        				if(listOfPacking != null) {
	        					for (String packing : listOfPacking) {
	        						this.webJaguar.deletePackingList(webOrder.getOrderId(), packing, webOrder.getAccessUserId(), siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"));
	        					}
	        				}
	        				Inventory inventory = new Inventory();
	        				for (LineItem lineItem : webOrder.getLineItems()) {
	    						inventory.setInventoryAFS(lineItem.getQuantity());
	        					inventory.setSku(lineItem.getProduct().getSku());
	        					this.webJaguar.updateInventory(inventory);    	
	        				}
	    				}
	    				// customer credit
	    				if (((Boolean) gSiteConfig.get("gGIFTCARD") || (Boolean) gSiteConfig.get("gVIRTUAL_BANK_ACCOUNT")) && webOrder.getCreditUsed() != null ) {
	    					CreditImp credit = new CreditImp();
	    					CustomerCreditHistory customerCredit = credit.updateCustomerCreditByOrderPayment(webOrder, "cancel", webOrder.getCreditUsed(), this.webJaguar.getAccessUser(request).getUsername());
	    					this.webJaguar.updateCredit(customerCredit);
	    					/*
	    					 * To-be-Deleted after testing VBA Module
	    					 * this.webJaguar.updateCredit( webOrder.getUserId(), webOrder.getCreditUsed() ); 
	    					 */				
	    				}
	    			}     			
	    			// shipped order
	    			if (orderStatus.getStatus().equals( "s" )) {    				
	    	    		// adds ship date to packinglist without shipdate and adjusts on hand
	        			this.webJaguar.ensurePackingListShipDate(webOrder, siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"), ((AccessUser) request.getAttribute( "accessUser" )).getId());
	            		// Order status changed to ship 
	    				if ( !webOrder.isProcessed() ) { 			
	    					this.webJaguar.generatePackingList(webOrder, false, new Date(), siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"));    					
	    				}    				
	    			}   
					
					
					this.webJaguar.insertOrderStatus( orderStatus );  
					
				}
			}
		}
		for(BigInteger packingNumber : packingListMap.keySet()) {
			this.webJaguar.insertPackingList(packingListMap.get(packingNumber), 0, siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"));
		}
		
		// convert to string
		res.append("SETIResponse: update=OK;Notes=");
		StringBuilder stringBuilder = new StringBuilder(res.toString());	
		return stringBuilder;
	}

}