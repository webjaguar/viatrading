/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.26.2010
 */

package com.webjaguar.thirdparty.linkShare;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

public class GatewayController extends WebApplicationObjectSupport implements Controller {

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {	
		
		String siteID = request.getParameter("siteID");
		if (siteID != null && siteID.trim().length() > 0) {
			GregorianCalendar now = new GregorianCalendar();
			now.add(Calendar.YEAR, 2);
			Long cookieAge = (now.getTimeInMillis() - (new Date()).getTime())/1000; // 2 years
			
			String path = request.getContextPath().equals("") ? "/" : request.getContextPath();
			
			// set site id
			Cookie siteIDCookie = new Cookie("linkShareSiteID", siteID);
			siteIDCookie.setPath(path);
			siteIDCookie.setMaxAge(cookieAge.intValue());			
			response.addCookie(siteIDCookie);
			
			// set date entered
			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd/kk:mm:ss");
			dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));			
			
			now.add(Calendar.YEAR, -2);		// back to today
			Cookie dateEnteredCookie = new Cookie("linkShareDate", dateFormatter.format(now.getTime()));
			dateEnteredCookie.setPath(path);
			dateEnteredCookie.setMaxAge(cookieAge.intValue());			
			response.addCookie(dateEnteredCookie);
			
			String url = ServletRequestUtils.getStringParameter(request, "url", "").trim();
			if (url.length() > 0) {
				return new ModelAndView(new RedirectView(url));
			}
		}
		
		return new ModelAndView(new RedirectView(""));
	}
}
