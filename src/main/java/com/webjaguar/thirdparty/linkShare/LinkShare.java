/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.27.2010
 */

package com.webjaguar.thirdparty.linkShare;

import java.util.Date;

public class LinkShare {
	
	private String siteID;
	private Date dateEntered;
	
	public String getSiteID() {
		return siteID;
	}
	public void setSiteID(String siteID) {
		this.siteID = siteID;
	}
	public Date getDateEntered() {
		return dateEntered;
	}
	public void setDateEntered(Date dateEntered) {
		this.dateEntered = dateEntered;
	}

}
