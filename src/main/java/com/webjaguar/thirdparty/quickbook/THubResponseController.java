package com.webjaguar.thirdparty.quickbook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.SalesRep;


public class THubResponseController extends WebApplicationObjectSupport implements Controller{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	Map<String, Configuration> siteConfig;
	Map<String, Object> gSiteConfig;
	String[] tHubAuth;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
			
		siteConfig =  (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		tHubAuth = siteConfig.get("THUB_AUTHENTICATION").getValue().split(",");
		
		if ( tHubAuth.length < 1) {
			return null;
		}
			DocumentBuilderFactory factory = null;
		    DocumentBuilder builder = null;
		    Document doc = null;
		    System.out.println(request.getParameter("request"));
		    try {
		      factory = DocumentBuilderFactory.newInstance();
		      builder = factory.newDocumentBuilder();
		    } catch (Exception e) { }
		    
		    try {
		    	// use this line, if XML is recieved as inputstream
		    	//doc = builder.parse(request.getInputStream());
		    	
		    	// next 3 lines, if xml is recieved as request parameter.
		    	InputSource is = new InputSource();
		      	is.setCharacterStream(new StringReader(request.getParameter("request")));
		      	doc = builder.parse(is);
		      	
		    } catch (Exception e) { e.printStackTrace();} 
		    
		    String xmlResponse = this.parseXML(doc).toString();
		   
		    // POST data
			OutputStream oStream = response.getOutputStream();
			oStream.write(xmlResponse.getBytes());
			oStream.close();
			
			
		   return null;
	}
	
	
	public StringBuilder parseXML (Document doc) throws Exception {
		
		StringBuilder stringBuilder = null;
		String userId = "";
		String password = "";
		if(doc.getElementsByTagName("UserID") != null) {
			try {
				userId = doc.getElementsByTagName("UserID").item(0).getTextContent();
			} catch (Exception e) { e.printStackTrace(); }
		}
		
		if(doc.getElementsByTagName("Password") != null) {
			try {
				password = doc.getElementsByTagName("Password").item(0).getTextContent();
			} catch (Exception e) { e.printStackTrace(); }
		}
		if (!tHubAuth[0].equals(userId) || !tHubAuth[1].equals(password) ) {
			return this.errorMessage();
		}
		
		// Level 1: Get Order response
		if(doc.getElementsByTagName("Command").item(0).getTextContent().equals("GetOrders")) {
			int orderStartNumber = 0;
			if(doc.getElementsByTagName("OrderStartNumber") != null) {
				try {
					orderStartNumber = Integer.parseInt(doc.getElementsByTagName("OrderStartNumber").item(0).getTextContent());
				} catch (Exception e) { e.printStackTrace(); }
			}
			
			int orderCount = 25;
			if(doc.getElementsByTagName("LimitOrderCount") != null) {
				try {
					orderCount = Integer.parseInt(doc.getElementsByTagName("LimitOrderCount").item(0).getTextContent());
				} catch (Exception e) { e.printStackTrace(); }
			}
			
			stringBuilder = this.ordersList(orderStartNumber, orderCount);
		}	
		
		// Level 2: Update Order Shipping Status response
		if(doc.getElementsByTagName("Command").item(0).getTextContent().equals("UpdateOrdersShippingStatus")) {
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			Document dom = null ;
			try {
				DocumentBuilder db = dbf.newDocumentBuilder();
				dom = db.newDocument();
			}catch(ParserConfigurationException pce) {
				System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
			}

			Element root = dom.createElement("RESPONSE");
			root.setAttribute("Version", "2.8");
			dom.appendChild(root);
			
			// Envelope
			Element Envelope = dom.createElement("Envelope");
			root.appendChild(Envelope);
			// Command
			Element Command = dom.createElement("Command");
			Command.appendChild(dom.createTextNode("UpdateOrdersShippingStatus"));
			Envelope.appendChild(Command);
			
			// StatusCode
			Element StatusCode = dom.createElement("StatusCode");
			StatusCode.appendChild(dom.createTextNode("0"));
			Envelope.appendChild(StatusCode);
					
			// StatusMessage
			Element StatusMessage = dom.createElement("StatusMessage");
			StatusMessage.appendChild(dom.createTextNode("All Ok"));
			Envelope.appendChild(StatusMessage);
					
			// Provider
			Element Provider = dom.createElement("Provider");
			Provider.appendChild(dom.createTextNode("GENERIC"));
			Envelope.appendChild(Provider);
			
			// Orders
			Element Orders = dom.createElement("Orders");
			root.appendChild(Orders);
			Order order = new Order();
			
			for(int i=0 ; i < doc.getElementsByTagName("Order").getLength(); ++i) {
				
				Element Order = dom.createElement("Order");
				Orders.appendChild(Order);
				
				// HostOrderID
				Element HostOrderID = dom.createElement("HostOrderID");
				HostOrderID.appendChild(dom.createTextNode(doc.getElementsByTagName("HostOrderID").item(i).getTextContent().toString()));
				Order.appendChild(HostOrderID);
				
				// LocalOrderID
				Element LocalOrderID = dom.createElement("LocalOrderID");
				LocalOrderID.appendChild(dom.createTextNode(doc.getElementsByTagName("LocalOrderID").item(i).getTextContent().toString()));
				Order.appendChild(LocalOrderID);
				
				// HostStatus
				Element HostStatus = dom.createElement("HostStatus");
				
				try {
					order = this.webJaguar.getOrder(Integer.parseInt(doc.getElementsByTagName("HostOrderID").item(i).getTextContent().toString().trim()), null);
				} 
				catch (Exception e) {					
				}
				if(order != null) {
					if(!doc.getElementsByTagName("ShippedOn").item(i).getTextContent().toString().isEmpty()) {
						if ( !order.isProcessed()) {  			
							this.webJaguar.generatePackingList(order, false, new Date(doc.getElementsByTagName("ShippedOn").item(i).getTextContent().toString()), siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"));
			    		}
		    			this.webJaguar.ensurePackingListShipDate(order, siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"), 0);
		    			order.setShipped(new Date(doc.getElementsByTagName("ShippedOn").item(i).getTextContent().toString()));
		    			this.webJaguar.updateOrder(order);
		    			OrderStatus orderStatus = new OrderStatus();
		    			orderStatus.setComments("Shipping on: "+ doc.getElementsByTagName("ShippedOn").item(i).getTextContent().toString() + " \n "+
		    					"ShippedVia: "+ doc.getElementsByTagName("ShippedVia").item(i).getTextContent().toString() +" \n "+
		    					"ServiceUsed: "+ doc.getElementsByTagName("ServiceUsed").item(i).getTextContent().toString());
		    			orderStatus.setStatus("s");
		    			orderStatus.setOrderId(Integer.parseInt(doc.getElementsByTagName("HostOrderID").item(i).getTextContent().toString().trim()));
		    			orderStatus.setUsername("THUB");
		    			orderStatus.setTrackNum(doc.getElementsByTagName("TrackingNumber").item(i).getTextContent().toString());
		    			this.webJaguar.insertOrderStatus(orderStatus);
					}
					HostStatus.appendChild(dom.createTextNode("Success"));
					Order.appendChild(HostStatus);
				}
				else {
					HostStatus.appendChild(dom.createTextNode("Failed"));
					Order.appendChild(HostStatus);
				}
			}

			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			OutputFormat outputformat = new OutputFormat();
			outputformat.setIndenting(true);
			outputformat.setPreserveSpace(false);
			XMLSerializer serializer = new XMLSerializer();
			serializer.setOutputFormat(outputformat);
			serializer.setOutputByteStream(stream);
			serializer.asDOMSerializer();
			serializer.serialize(dom.getDocumentElement());

			stringBuilder = new StringBuilder(stream.toString());
		}
		

		// Level 2: Update Order Payment Status response
		if(doc.getElementsByTagName("Command").item(0).getTextContent().equals("UpdateOrdersPaymentStatus")) {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			Document dom = null ;
			try {
				DocumentBuilder db = dbf.newDocumentBuilder();
				dom = db.newDocument();
			}catch(ParserConfigurationException pce) {
				System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
			}

			Element root = dom.createElement("RESPONSE");
			root.setAttribute("Version", "2.8");
			dom.appendChild(root);
			
			// Envelope
			Element Envelope = dom.createElement("Envelope");
			root.appendChild(Envelope);
			// Command
			Element Command = dom.createElement("Command");
			Command.appendChild(dom.createTextNode("UpdateOrdersPaymentStatus"));
			Envelope.appendChild(Command);
			
			// StatusCode
			Element StatusCode = dom.createElement("StatusCode");
			StatusCode.appendChild(dom.createTextNode("0"));
			Envelope.appendChild(StatusCode);
					
			// StatusMessage
			Element StatusMessage = dom.createElement("StatusMessage");
			StatusMessage.appendChild(dom.createTextNode("All Ok"));
			Envelope.appendChild(StatusMessage);
					
			// Provider
			Element Provider = dom.createElement("Provider");
			Provider.appendChild(dom.createTextNode("GENERIC"));
			Envelope.appendChild(Provider);
			
			// Orders
			Element Orders = dom.createElement("Orders");
			root.appendChild(Orders);
			
			Order order = new Order();
			
			for(int i=0 ; i < doc.getElementsByTagName("Order").getLength(); ++i) {
				
				Element Order = dom.createElement("Order");
				Orders.appendChild(Order);
				
				// HostOrderID
				Element HostOrderID = dom.createElement("HostOrderID");
				HostOrderID.appendChild(dom.createTextNode(doc.getElementsByTagName("HostOrderID").item(i).getTextContent().toString()));
				Order.appendChild(HostOrderID);
				
				// LocalOrderID
				Element LocalOrderID = dom.createElement("LocalOrderID");
				LocalOrderID.appendChild(dom.createTextNode(doc.getElementsByTagName("LocalOrderID").item(i).getTextContent().toString()));
				Order.appendChild(LocalOrderID);
				
				// PaymentStatus
				Element PaymentStatus = dom.createElement("PaymentStatus");
				
				try {
					order = this.webJaguar.getOrder(Integer.parseInt(doc.getElementsByTagName("HostOrderID").item(i).getTextContent().toString().trim()), null);
				} 
				catch (Exception e) {					
				}
				if(order != null) {
					Date date;
					if(!doc.getElementsByTagName("ClearedOn").item(i).getTextContent().toString().isEmpty()) {
						date = new Date(doc.getElementsByTagName("ClearedOn").item(i).getTextContent().toString());
					}
					else {
						date = null;
					}
					this.webJaguar.updateOrdersPaymentStatus(Integer.parseInt(doc.getElementsByTagName("HostOrderID").item(i).getTextContent().toString().trim()), doc.getElementsByTagName("PaymentStatus").item(i).getTextContent().toString(),date);
					
					PaymentStatus.appendChild(dom.createTextNode("Success"));
					Order.appendChild(PaymentStatus);
				}	
				else {
					PaymentStatus.appendChild(dom.createTextNode("Failed"));
					Order.appendChild(PaymentStatus);
				}
			}
			
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			OutputFormat outputformat = new OutputFormat();
			outputformat.setIndenting(true);
			outputformat.setPreserveSpace(false);
			XMLSerializer serializer = new XMLSerializer();
			serializer.setOutputFormat(outputformat);
			serializer.setOutputByteStream(stream);
			serializer.asDOMSerializer();
			serializer.serialize(dom.getDocumentElement());

			stringBuilder = new StringBuilder(stream.toString());
		}
		
		
		// Level 3: Update Inventory response
		if(doc.getElementsByTagName("Command").item(0).getTextContent().equals("UpdateInventory")) {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			Document dom = null ;
			try {
				DocumentBuilder db = dbf.newDocumentBuilder();
				dom = db.newDocument();
			}catch(ParserConfigurationException pce) {
				System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
			}
			
			
			if(doc.getElementsByTagName("UpdateInventory").item(0).getTextContent().equals("1")) {

				// root
				Element root = dom.createElement("RESPONSE");
				root.setAttribute("Version", "2.8");
				dom.appendChild(root);
				
				// Envelope
				Element Envelope = dom.createElement("Envelope");
				root.appendChild(Envelope);
				// Command
				Element Command = dom.createElement("Command");
				Command.appendChild(dom.createTextNode("UpdateInventory"));
				Envelope.appendChild(Command);
				
				// StatusCode
				Element StatusCode = dom.createElement("StatusCode");
				StatusCode.appendChild(dom.createTextNode("0"));
				Envelope.appendChild(StatusCode);
						
				// StatusMessage
				Element StatusMessage = dom.createElement("StatusMessage");
				StatusMessage.appendChild(dom.createTextNode("All Ok"));
				Envelope.appendChild(StatusMessage);
						
				// Provider
				Element Provider = dom.createElement("Provider");
				Provider.appendChild(dom.createTextNode("GENERIC"));
				Envelope.appendChild(Provider);
				
				// Items
				Element Items = dom.createElement("Items");
				root.appendChild(Items);
				Inventory inventory = new Inventory();
				InventoryActivity inventoryActivity = new InventoryActivity();
				for(int i=0 ; i < doc.getElementsByTagName("Item").getLength(); ++i) {		
					
					// Item
					Element Item = dom.createElement("Item");
					Items.appendChild(Item);
					
					// ItemCode
					Element ItemCode = dom.createElement("ItemCode");
					ItemCode.appendChild(dom.createTextNode(doc.getElementsByTagName("ItemCode").item(i).getTextContent().toString()));
					Item.appendChild(ItemCode);
					
					// QuantityInStockWEB
					Element QuantityInStockWEB = dom.createElement("QuantityInStockWEB");
					QuantityInStockWEB.appendChild(dom.createTextNode(doc.getElementsByTagName("QuantityInStock").item(i).getTextContent().toString()));
					Item.appendChild(QuantityInStockWEB);
					
					try {
						inventory.setSku(doc.getElementsByTagName("ItemCode").item(i).getTextContent().toString());
						inventory = this.webJaguar.getInventory(inventory);						
					} catch (Exception e) {
					}
					if (inventory != null && inventory.getInventory() != null && (Integer.parseInt(doc.getElementsByTagName("QuantityInStock").item(i).getTextContent().toString().trim())-inventory.getInventoryAFS() != 0 )) {
						inventoryActivity.setSku(doc.getElementsByTagName("ItemCode").item(i).getTextContent().toString());
						inventoryActivity.setInventory(inventory.getInventory());
						inventoryActivity.setQuantity(Integer.parseInt(doc.getElementsByTagName("QuantityInStock").item(i).getTextContent().toString().trim())-inventory.getInventoryAFS());
						inventoryActivity.setAccessUserId(0);
						inventoryActivity.setType("THUB");
						this.webJaguar.adjustInventoryBySku(inventoryActivity);	
						
						// InventoryUpdateStatus
						Element InventoryUpdateStatus = dom.createElement("InventoryUpdateStatus");
						InventoryUpdateStatus.appendChild(dom.createTextNode("0"));
						Item.appendChild(InventoryUpdateStatus);
						
					} else {
						
						// InventoryUpdateStatus
						Element InventoryUpdateStatus = dom.createElement("InventoryUpdateStatus");
						InventoryUpdateStatus.appendChild(dom.createTextNode("1"));
						Item.appendChild(InventoryUpdateStatus);
					}
				}	
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				OutputFormat outputformat = new OutputFormat();
				outputformat.setIndenting(true);
				outputformat.setPreserveSpace(false);
				XMLSerializer serializer = new XMLSerializer();
				serializer.setOutputFormat(outputformat);
				serializer.setOutputByteStream(stream);
				serializer.asDOMSerializer();
				serializer.serialize(dom.getDocumentElement());

				stringBuilder = new StringBuilder(stream.toString());
			}
		}		
		return stringBuilder;
	}
	
	public StringBuilder errorMessage() throws IOException {
		// generate Response
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document dom = null ;
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			dom = db.newDocument();
		}catch(ParserConfigurationException pce) {
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
		}

		// root
		Element root = dom.createElement("RESPONSE");
		root.setAttribute("Version", "2.8");
		dom.appendChild(root);
		
		// Error
		Element Error = dom.createElement("Error");
		root.appendChild(Error);
		
		// StatusCode
		Element StatusCode = dom.createElement("StatusCode");
		StatusCode.appendChild(dom.createTextNode("Invalid Username or Password"));
		Error.appendChild(StatusCode);

		// convert to string
		StringBuilder stringBuilder = null;
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputFormat outputformat = new OutputFormat();
		outputformat.setIndenting(true);
		outputformat.setPreserveSpace(false);
		XMLSerializer serializer = new XMLSerializer();
		serializer.setOutputFormat(outputformat);
		serializer.setOutputByteStream(stream);
		serializer.asDOMSerializer();
		serializer.serialize(dom.getDocumentElement());

		stringBuilder = new StringBuilder(stream.toString());	
		return stringBuilder;
	}
	
	public StringBuilder ordersList ( int orderStartNumber, int orderCount ) throws Exception {
		// generate Response
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document dom = null ;
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			dom = db.newDocument();
		}catch(ParserConfigurationException pce) {
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
		}
		
		
		// product fields
		List<ProductField> productFields = null;
		if( siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue() != null && siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue().equals("true") ) {
			productFields = this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
			Iterator pfIter = productFields.iterator();
			while ( pfIter.hasNext() ) {
				ProductField productField = (ProductField) pfIter.next();
				if ( !productField.isEnabled() || !productField.isShowOnInvoiceExport()) {
					pfIter.remove();
				}
			}
		}
		
		
		OrderSearch search = new OrderSearch();
		search.setLimit(orderCount);
		search.setOrderId(orderStartNumber);
		List<Order> orderList = this.webJaguar.getQBOrdersList(search, siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());
		
		// root
		Element root = dom.createElement("RESPONSE");
		root.setAttribute("Version", "2.8");
		dom.appendChild(root);
		
		// Envelope
		Element Envelope = dom.createElement("Envelope");
		root.appendChild(Envelope);
		
		// StatusCode
		Element StatusCode = dom.createElement("StatusCode");
		StatusCode.appendChild(dom.createTextNode("0"));
		Envelope.appendChild(StatusCode);
				
		// StatusMessage
		Element StatusMessage = dom.createElement("StatusMessage");
		StatusMessage.appendChild(dom.createTextNode("All Ok"));
		Envelope.appendChild(StatusMessage);
				
		// Provider
		Element Provider = dom.createElement("Provider");
		Provider.appendChild(dom.createTextNode("GENERIC"));
		Envelope.appendChild(Provider);
				
		// Orders
		Element Orders = dom.createElement("Orders");
		root.appendChild(Orders);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm:ss");
		SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		NumberFormat decimalFormatter = new DecimalFormat("#0.00");
		NumberFormat intFormatter = new DecimalFormat("#0");

		for(Order order : orderList) {
			
			// Order
			Element Order = dom.createElement("Order");
			Orders.appendChild(Order);
				
			// OrderID
			Element OrderID = dom.createElement("OrderID");
			OrderID.appendChild(dom.createTextNode(""+order.getOrderId()));
			Order.appendChild(OrderID);
					
			// ProviderOrderRef
			Element ProviderOrderRef = dom.createElement("ProviderOrderRef");
			ProviderOrderRef.appendChild(dom.createTextNode(""+order.getOrderId()));
			Order.appendChild(ProviderOrderRef);
					
			// Date
			Element Date = dom.createElement("Date");
			Date.appendChild(dom.createTextNode(""+dateFormatter.format(order.getDateOrdered())));
			Order.appendChild(Date);
					
			// Time
			Element Time = dom.createElement("Time");
			Time.appendChild(dom.createTextNode(""+timeFormatter.format(order.getDateOrdered())));
			Order.appendChild(Time);
					
			// TimeZone
			Element TimeZone = dom.createElement("TimeZone");
			TimeZone.appendChild(dom.createTextNode("CST"));
			Order.appendChild(TimeZone);
					
			// UpdatedOn
			Element UpdatedOn = dom.createElement("UpdatedOn");
			UpdatedOn.appendChild(dom.createTextNode((order.getLastModified() == null) ? dateTimeFormatter.format(order.getDateOrdered()) : dateTimeFormatter.format(order.getLastModified())));
			Order.appendChild(UpdatedOn);
					
			// StoreID
			Element StoreID = dom.createElement("StoreID");
			StoreID.appendChild(dom.createTextNode("1"));
			Order.appendChild(StoreID);
					
			// StoreName
			Element StoreName = dom.createElement("StoreName");
			StoreName.appendChild(dom.createTextNode(gSiteConfig.get( "gSITE_DOMAIN" ).toString()));
			Order.appendChild(StoreName);
					
			// CustomerID
			Element CustomerID = dom.createElement("CustomerID");
			CustomerID.appendChild(dom.createTextNode(""+order.getUserId()));
			Order.appendChild(CustomerID);
					
			// CustomerType
			Element CustomerType = dom.createElement("CustomerType");
			CustomerType.appendChild(dom.createTextNode(order.getOrderType() != null ? order.getOrderType() : ""));
			Order.appendChild(CustomerType);
					
			SalesRep salesRep = null;
			if(order.getSalesRepId() != null) {
				salesRep = this.webJaguar.getSalesRepById(order.getSalesRepId());
			}
			// SalesRep
			Element SalesRep = dom.createElement("SalesRep");
			SalesRep.appendChild(dom.createTextNode(salesRep != null ? salesRep.getName() : ""));
			Order.appendChild(SalesRep);
					
			// Comment
			Element Comment = dom.createElement("Comment");
			Comment.appendChild(dom.createTextNode(""));
			Order.appendChild(Comment);
					
			// MerchantNotes
			Element MerchantNotes = dom.createElement("MerchantNotes");
			MerchantNotes.appendChild(dom.createTextNode(""));
			Order.appendChild(MerchantNotes);
					
			// Currency
			Element Currency = dom.createElement("Currency");
			Currency.appendChild(dom.createTextNode("USD"));
			Order.appendChild(Currency);
					
			// Bill
			Element Bill = dom.createElement("Bill");
			Order.appendChild(Bill);
					
			// PayMethod
			Element PayMethod = dom.createElement("PayMethod");
			PayMethod.appendChild(dom.createTextNode(order.getPaymentMethod() != null ? order.getPaymentMethod() : ""));
			Bill.appendChild(PayMethod);
					
			// PayStatus
			Element PayStatus = dom.createElement("PayStatus");
			PayStatus.appendChild(dom.createTextNode((order.getPayment() != null) && (order.getPayment() < order.getGrandTotal()) ? "Pending" : "Paid"));
			Bill.appendChild(PayStatus);
					
			// PayDate
			Element PayDate = dom.createElement("PayDate");
			if(order.getPayment() != null) {
				PayDate.appendChild(dom.createTextNode(""+dateFormatter.format(order.getPayments().get(order.getPayments().size()-1).getDate())));
			}
			Bill.appendChild(PayDate);
					
			// FirstName
			Element FirstName = dom.createElement("FirstName");
			FirstName.appendChild(dom.createTextNode(order.getBilling().getFirstName()));
			Bill.appendChild(FirstName);
					
			// LastName 
			Element LastName = dom.createElement("LastName");
			LastName.appendChild(dom.createTextNode(order.getBilling().getLastName()));
			Bill.appendChild(LastName);
					
			// MiddleName 
			Element MiddleName = dom.createElement("MiddleName");
			MiddleName.appendChild(dom.createTextNode(""));
			Bill.appendChild(MiddleName);
					
			// CompanyName
			Element CompanyName = dom.createElement("CompanyName");
			CompanyName.appendChild(dom.createTextNode(order.getBilling().getCompany() != null ? order.getBilling().getCompany() : ""));
			Bill.appendChild(CompanyName);
					
			// Address1
			Element Address1 = dom.createElement("Address1");
			Address1.appendChild(dom.createTextNode(order.getBilling().getAddr1()));
			Bill.appendChild(Address1);
					
			// Address2
			Element Address2 = dom.createElement("Address2");
			Address2.appendChild(dom.createTextNode(order.getBilling().getAddr2()));
			Bill.appendChild(Address2);
					
			// City
			Element City = dom.createElement("City");
			City.appendChild(dom.createTextNode(order.getBilling().getCity()));
			Bill.appendChild(City);
					
			// State
			Element State = dom.createElement("State");
			State.appendChild(dom.createTextNode(order.getBilling().getStateProvince()));
			Bill.appendChild(State);
					
			// Zip
			Element Zip = dom.createElement("Zip");
			Zip.appendChild(dom.createTextNode(order.getBilling().getZip()));
			Bill.appendChild(Zip);
					
			// Country
			Element Country = dom.createElement("Country");
			Country.appendChild(dom.createTextNode(order.getBilling().getCountry()));
			Bill.appendChild(Country);
					
			// Email
			Element Email = dom.createElement("Email");
			Email.appendChild(dom.createTextNode(order.getUserEmail()));
			Bill.appendChild(Email);
					
			// Phone
			Element Phone = dom.createElement("Phone");
			Phone.appendChild(dom.createTextNode(order.getBilling().getPhone()));
			Bill.appendChild(Phone);
					
			// PONumber
			Element PONumber = dom.createElement("PONumber");
			PONumber.appendChild(dom.createTextNode(order.getPurchaseOrder() != null ? order.getPurchaseOrder() : null));
			Bill.appendChild(PONumber);
					
			// CreditCard
			Element CreditCard = dom.createElement("CreditCard");
			Bill.appendChild(CreditCard);
					
			// CreditCardType
			Element CreditCardType = dom.createElement("CreditCardType");
			CreditCardType.appendChild(dom.createTextNode(order.getCreditCard() != null ? order.getCreditCard().getType() : ""));
			CreditCard.appendChild(CreditCardType);
					
			// CreditCardType
			Element CreditCardCharge = dom.createElement("CreditCardCharge");
			CreditCardCharge.appendChild(dom.createTextNode(""));
			CreditCard.appendChild(CreditCardCharge);
					
			// ExpirationDate
			Element ExpirationDate = dom.createElement("ExpirationDate");
			ExpirationDate.appendChild(dom.createTextNode(order.getCreditCard() != null ? order.getCreditCard().getExpireMonth()+"/"+order.getCreditCard().getExpireYear() : ""));
			CreditCard.appendChild(ExpirationDate);
					
			// CreditCardName
			Element CreditCardName = dom.createElement("CreditCardName");
			CreditCardName.appendChild(dom.createTextNode(""));
			CreditCard.appendChild(CreditCardName);
					
			// CreditCardNumber
			Element CreditCardNumber = dom.createElement("CreditCardNumber");
			CreditCardNumber.appendChild(dom.createTextNode(order.getCreditCard() != null ? order.getCreditCard().getNumber() : ""));
			CreditCard.appendChild(CreditCardNumber);
					
			// CVV2
			Element CVV2 = dom.createElement("CVV2");
			CVV2.appendChild(dom.createTextNode(order.getCreditCard() != null ? order.getCreditCard().getCvv2Code() : ""));
			CreditCard.appendChild(CVV2);
					
			// AuthDetails
			Element AuthDetails = dom.createElement("AuthDetails");
			AuthDetails.appendChild(dom.createTextNode(order.getCreditCard() != null ? order.getCreditCard().getAuthCode() : ""));
			CreditCard.appendChild(AuthDetails);
					
			// TransactionID
			Element TransactionID = dom.createElement("TransactionID");
			TransactionID.appendChild(dom.createTextNode(order.getCreditCard() != null ? order.getCreditCard().getTransId() : ""));
			CreditCard.appendChild(TransactionID);
					
			// ReconciliationData
			Element ReconciliationData = dom.createElement("ReconciliationData");
			ReconciliationData.appendChild(dom.createTextNode(null));
			CreditCard.appendChild(ReconciliationData);
					
			//ship		
			Element Ship = dom.createElement("Ship");
			Order.appendChild(Ship);
			
			// ShipStatus
			// Valid values New, Shipped
			Element ShipStatus = dom.createElement("ShipStatus");
			ShipStatus.appendChild(dom.createTextNode(getTHubShipStatus(order.getStatus())));
			Ship.appendChild(ShipStatus);
					
			// ShipDate
			Element ShipDate = dom.createElement("ShipDate");
			if(order.getShipped() != null) {
				ShipDate.appendChild(dom.createTextNode(dateFormatter.format(order.getShipped()) != null ? dateFormatter.format(order.getShipped()) : ""));
			}
			Ship.appendChild(ShipDate);
					
			// Tracking
			Element Tracking = dom.createElement("Tracking");
			Tracking.appendChild(dom.createTextNode(order.getTrackcode()));
			Ship.appendChild(Tracking);
					
			// ShipCost
			Element ShipCost = dom.createElement("ShipCost");
			ShipCost.appendChild(dom.createTextNode(order.getShippingCost() != null ? order.getShippingCost().toString() : ""));
			Ship.appendChild(ShipCost);
					
			// ShipCarrierName
			Element ShipCarrierName = dom.createElement("ShipCarrierName");
			ShipCarrierName.appendChild(dom.createTextNode(order.getShippingMethod() != null ? order.getShippingMethod() : ""));
			Ship.appendChild(ShipCarrierName);
					
			// ShipMethod
			Element ShipMethod = dom.createElement("ShipMethod");
			ShipMethod.appendChild(dom.createTextNode(order.getShippingMethod() != null ? order.getShippingMethod() : ""));
			Ship.appendChild(ShipMethod);
					
			// FirstName
			Element ShipFirstName = dom.createElement("FirstName");
			ShipFirstName.appendChild(dom.createTextNode(order.getShipping().getFirstName()));
			Ship.appendChild(ShipFirstName);
					
			// LastName 
			Element ShipLastName = dom.createElement("LastName");
			ShipLastName.appendChild(dom.createTextNode(order.getShipping().getLastName()));
			Ship.appendChild(ShipLastName);
					
			// MiddleName 
			Element ShipMiddleName = dom.createElement("MiddleName");
			ShipMiddleName.appendChild(dom.createTextNode(""));
			Ship.appendChild(ShipMiddleName);
					
			// CompanyName
			Element ShipCompanyName = dom.createElement("CompanyName");
			ShipCompanyName.appendChild(dom.createTextNode(order.getShipping().getCompany() != null ? order.getShipping().getCompany() : ""));
			Ship.appendChild(ShipCompanyName);
					
			// Address1
			Element ShipAddress1 = dom.createElement("Address1");
			ShipAddress1.appendChild(dom.createTextNode(order.getShipping().getAddr1() != null ? order.getShipping().getAddr1() : ""));
			Ship.appendChild(ShipAddress1);
					
			// Address2
			Element ShipAddress2 = dom.createElement("Address2");
			ShipAddress2.appendChild(dom.createTextNode(order.getShipping().getAddr2() != null ? order.getShipping().getAddr2() : ""));
			Ship.appendChild(ShipAddress2);
					
			// City
			Element ShipCity = dom.createElement("City");
			ShipCity.appendChild(dom.createTextNode(order.getShipping().getCity() != null ? order.getShipping().getCity() : ""));
			Ship.appendChild(ShipCity);
					
			// State
			Element ShipState = dom.createElement("State");
			ShipState.appendChild(dom.createTextNode(order.getShipping().getStateProvince() != null ? order.getShipping().getStateProvince() : ""));
			Ship.appendChild(ShipState);
					
			// Zip
			Element ShipZip = dom.createElement("Zip");
			ShipZip.appendChild(dom.createTextNode(order.getShipping().getZip() != null ? order.getShipping().getZip() : ""));
			Ship.appendChild(ShipZip);
					
			// Country
			Element ShipCountry = dom.createElement("Country");
			ShipCountry.appendChild(dom.createTextNode(order.getShipping().getCountry() != null ? order.getShipping().getCountry() : ""));
			Ship.appendChild(ShipCountry);
					
			// Email
			Element ShipEmail = dom.createElement("Email");
			ShipEmail.appendChild(dom.createTextNode(order.getUserEmail() != null ? order.getUserEmail() : ""));
			Ship.appendChild(ShipEmail);
					
			// Phone
			Element ShipPhone = dom.createElement("Phone");
			ShipPhone.appendChild(dom.createTextNode(order.getShipping().getPhone() != null ? order.getShipping().getPhone() : ""));
			Ship.appendChild(ShipPhone);
					
					
			// Items
			Element Items = dom.createElement("Items");
			Order.appendChild(Items);
			
			Class<Product> product = Product.class;
    		Method m = null; 
			for(LineItem lineItem : order.getLineItems()) {
				
				// Item
				Element Item = dom.createElement("Item");
				Items.appendChild(Item);
						
				// ItemCode
				Element ItemCode = dom.createElement("ItemCode");
				ItemCode.appendChild(dom.createTextNode(lineItem.getProduct().getSku()));
				Item.appendChild(ItemCode);
						
				// ItemDescription
				Element ItemDescription = dom.createElement("ItemDescription");
				ItemDescription.appendChild(dom.createTextNode(lineItem.getProduct().getName() != null ? lineItem.getProduct().getName() : ""));
				Item.appendChild(ItemDescription);
						
				// Quantity
				Element Quantity = dom.createElement("Quantity");
				Quantity.appendChild(dom.createTextNode(""+lineItem.getQuantity()));
				Item.appendChild(Quantity);
						
				// UnitPrice
				Element UnitPrice = dom.createElement("UnitPrice");
				UnitPrice.appendChild(dom.createTextNode(lineItem.getUnitPrice() != null ? lineItem.getUnitPrice().toString() : ""));
				Item.appendChild(UnitPrice);
						
				// UnitCost
				Element UnitCost = dom.createElement("UnitCost");
				UnitCost.appendChild(dom.createTextNode(lineItem.getUnitCost() != null ? lineItem.getUnitCost().toString() : ""));
				Item.appendChild(UnitCost);
						
				// Vendor
				Element Vendor = dom.createElement("Vendor");
				Vendor.appendChild(dom.createTextNode(lineItem.getSupplier() != null && lineItem.getSupplier().getAddress() != null && lineItem.getSupplier().getAddress().getCompany() != null ? lineItem.getSupplier().getAddress().getCompany() : ""));
				Item.appendChild(Vendor);
						
				// ItemTotal
				Element ItemTotal = dom.createElement("ItemTotal");
				ItemTotal.appendChild(dom.createTextNode(lineItem.getTotalPrice() != null ?  decimalFormatter.format(lineItem.getTotalPrice()).toString() : ""));
				Item.appendChild(ItemTotal);
						
				// ItemUnitWeight
				Element ItemUnitWeight = dom.createElement("ItemUnitWeight");
				ItemUnitWeight.appendChild(dom.createTextNode(lineItem.getProduct().getWeight() != null ? lineItem.getProduct().getWeight().toString() : ""));
				Item.appendChild(ItemUnitWeight);
						
				// Length
				Element Length = dom.createElement("Length");
				Length.appendChild(dom.createTextNode(""));
				Item.appendChild(Length);
						
				// Depth
				Element Depth = dom.createElement("Depth");
				Depth.appendChild(dom.createTextNode(""));
				Item.appendChild(Depth);
						
				// Height
				Element Height = dom.createElement("Height");
				Height.appendChild(dom.createTextNode(""));
				Item.appendChild(Height);				
				
				// product fields
        		if(productFields != null && productFields.size() > 0) {
    				lineItem.setProductFields(productFields);
    				int index=1;
    				for(ProductField field : lineItem.getProductFields()){
            			m = product.getMethod("getField"+field.getId());
            			Element CustomField = dom.createElement("CustomField"+index++);
    					CustomField.appendChild(dom.createTextNode((m.invoke(lineItem.getProduct()) == null ? "" : m.invoke(lineItem.getProduct()).toString())));
    					Item.appendChild(CustomField);
               		}
            	} else {
            		for(int i=1; i<6 ; i++) {
            			// product fields
    					Element CustomField = dom.createElement("CustomField"+i);
    					CustomField.appendChild(dom.createTextNode(""));
    					Item.appendChild(CustomField);
    				}
            	}
        		

				// ItemOptions
				Element ItemOptions = dom.createElement("ItemOptions");
				Item.appendChild(ItemOptions);
				
				for(ProductAttribute attr : lineItem.getProductAttributes()) {
					// ItemOption
					Element ItemOption = dom.createElement("ItemOption");
					ItemOption.setAttribute("Name", attr.getOptionName());
					ItemOption.setAttribute("Value", attr.getValueString());
					ItemOptions.appendChild(ItemOption);
				}
			}
					
			
			// Charges
			Element Charges = dom.createElement("Charges");
			Order.appendChild(Charges);
			
			
			// Shipping
			Element Shipping = dom.createElement("Shipping");
			Shipping.appendChild(dom.createTextNode(order.getShippingCost() != null ? order.getShippingCost().toString() : ""));
			Charges.appendChild(Shipping);
					
			// Handling
			Element Handling = dom.createElement("Handling");
			Handling.appendChild(dom.createTextNode(""));
			Charges.appendChild(Handling);
					
			// Tax
			Element Tax = dom.createElement("Tax");
			Tax.appendChild(dom.createTextNode(order.getTax() != null ? order.getTax().toString() : ""));
			Charges.appendChild(Tax);
					
			// TaxOther
			Element TaxOther = dom.createElement("TaxOther");
			TaxOther.appendChild(dom.createTextNode("0"));
			Charges.appendChild(TaxOther);
					
			// ChannelFee
			Element ChannelFee = dom.createElement("ChannelFee");
			ChannelFee.appendChild(dom.createTextNode("0"));
			Charges.appendChild(ChannelFee);
					
			// PaymentFee
			Element PaymentFee = dom.createElement("PaymentFee");
			PaymentFee.appendChild(dom.createTextNode((order.getCcFee() == null) ? "" : order.getCcFee().toString()));
			Charges.appendChild(PaymentFee);
					
			// FeeDetails
			Element FeeDetails = dom.createElement("FeeDetails");
			Charges.appendChild(FeeDetails);
			
			// FeeDetail
			Element FeeDetail = dom.createElement("FeeDetail");
			FeeDetails.appendChild(FeeDetail);
			
			// FeeName
			Element FeeName = dom.createElement("FeeName");
			FeeName.appendChild(dom.createTextNode("Final Value Fee"));
			FeeDetail.appendChild(FeeName);
					
			// FeeValue
			Element FeeValue = dom.createElement("FeeValue");
			FeeValue.appendChild(dom.createTextNode("0"));
			FeeDetail.appendChild(FeeValue);
					
			// FeeDetail
			Element FeeDetail2 = dom.createElement("FeeDetail");
			FeeDetails.appendChild(FeeDetail2);
			
			// FeeName
			Element FeeName2 = dom.createElement("FeeName");
			FeeName2.appendChild(dom.createTextNode("CC Fee"));
			FeeDetail.appendChild(FeeName2);
					
			// FeeValue
			Element FeeValue2 = dom.createElement("FeeValue");
			FeeValue2.appendChild(dom.createTextNode((order.getCcFee() != null) ? order.getCcFee().toString() : ""));
			FeeDetail.appendChild(FeeValue2);
					
			// Discount
			Element Discount = dom.createElement("Discount");
			Double d = ((order.getPromoAmount() != null ? order.getPromoAmount() : 0) + (order.getShippingPromoAmount() != null ? order.getShippingPromoAmount() : 0));
			Discount.appendChild(dom.createTextNode(intFormatter.format(d)));
			Charges.appendChild(Discount);
					
			//GiftCertificate
			Element GiftCertificate = dom.createElement("GiftCertificate");
			if(order.getGiftCard() != null) {
				GiftCertificate.setAttribute("Code", order.getGiftCard().getCode());
				GiftCertificate.appendChild(dom.createTextNode(""+order.getGiftCard().getAmount()));
			}
			Charges.appendChild(GiftCertificate);
					
			//OtherCharge		
			Element OtherCharge = dom.createElement("OtherCharge");
			Charges.appendChild(OtherCharge);
					
			//Total		
			Element Total = dom.createElement("Total");
			Total.appendChild(dom.createTextNode(order.getGrandTotal().toString()));
			Charges.appendChild(Total);
					
			//Coupons		
			Element Coupons = dom.createElement("Coupons");
			Charges.appendChild(Coupons);
			
			
			//Coupon		
			Element Coupon = dom.createElement("Coupon");
			Coupons.appendChild(Coupon);
					
					
			//CouponCode
			Element CouponCode = dom.createElement("CouponCode");
			CouponCode.appendChild(dom.createTextNode(order.getPromoCode() != null ? order.getPromoCode() : ""));
			Coupon.appendChild(CouponCode);
					
			//CouponID  We pass CouponCode as ID
			Element CouponID = dom.createElement("CouponID");
			CouponID.appendChild(dom.createTextNode(order.getPromoCode() != null ? order.getPromoCode() : ""));
			Coupon.appendChild(CouponID);
					
			//CouponDescription
			Element CouponDescription = dom.createElement("CouponDescription");
			CouponDescription.appendChild(dom.createTextNode(order.getPromo() != null ? order.getPromo().getTitle() : ""));
			Coupon.appendChild(CouponDescription);
					
			//CouponValue
			Element CouponValue = dom.createElement("CouponValue");
			CouponValue.appendChild(dom.createTextNode(order.getPromoAmount().toString()));
			Coupon.appendChild(CouponValue);
					
			
			// need to work on shipping promo and promo per product
			
			
			for(int i=1; i<6 ; i++) {
				// CustomField
				Element CustomField = dom.createElement("CustomField"+i);
				CustomField.appendChild(dom.createTextNode(""));
				Order.appendChild(CustomField);
			}
		}
		
		// convert to string
		StringBuilder stringBuilder = null;
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		OutputFormat outputformat = new OutputFormat();
		outputformat.setIndenting(true);
		outputformat.setPreserveSpace(false);
		XMLSerializer serializer = new XMLSerializer();
		serializer.setOutputFormat(outputformat);
		serializer.setOutputByteStream(stream);
		serializer.asDOMSerializer();
		serializer.serialize(dom.getDocumentElement());

		stringBuilder = new StringBuilder(stream.toString());	
		return stringBuilder;
	}
	
	private String getTHubShipStatus(String orderStatus){
		if ( orderStatus == null || orderStatus.isEmpty() || !orderStatus.equalsIgnoreCase("s") ) {
			return "New";
		} else
			return "Shipped";
	}
}
