/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.27.2006
 */

package com.webjaguar.thirdparty.triguide;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class TriguideApi {	
	String hostname;
	String protocol = "http";
	String prefix = "pxml/xrefdata.asmx";	
	
	public TriguideApi(String hostname) {
		this.hostname = hostname;
	}
	
	public List<Map> ModelManufacturers(String cid, String pageSize, String pageIndex) {
		List<Map> manufacturers = new ArrayList<Map>();
		
		try {
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + "ModelManufacturers");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("cid=" + cid);
			request.append("&pageSize=" + pageSize);
			request.append("&pageIndex=" + pageIndex);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(connection.getInputStream());
	
			Element dataSet = doc.getRootElement().getChild("diffgram", Namespace.getNamespace("diffgram", "urn:schemas-microsoft-com:xml-diffgram-v1")).getChild("NewDataSet");
			Iterator results = dataSet.getChildren("table").iterator();
			while (results.hasNext()) {
				Element manufacturer = (Element) results.next();
				Map<String, String> map = new HashMap<String, String>();
				map.put("name", manufacturer.getChild("name").getText());
				map.put("id", manufacturer.getChild("id").getText());
				manufacturers.add(map);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return manufacturers;
	}
	
	public List<Map> ModelsByManufacturer(String cid, String modelManufacturerID, String familyID, String pageSize, String pageIndex) {
		List<Map> models = new ArrayList<Map>();
		
		try {
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + "ModelsByManufacturer");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("cid=" + cid);
			request.append("&modelManufacturerID=" + modelManufacturerID);
			request.append("&familyID=" + familyID);			
			request.append("&pageSize=" + pageSize);
			request.append("&pageIndex=" + pageIndex);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(connection.getInputStream());
	
			Element dataSet = doc.getRootElement().getChild("diffgram", Namespace.getNamespace("diffgram", "urn:schemas-microsoft-com:xml-diffgram-v1")).getChild("NewDataSet");
			Iterator results = dataSet.getChildren("table").iterator();
			while (results.hasNext()) {
				Element model = (Element) results.next();
				Map<String, String> map = new HashMap<String, String>();
				map.put("name", model.getChild("name").getText());
				map.put("id", model.getChild("id").getText());
				models.add(map);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return models;		
	}

	public List<Map> MySuppliesByModel(String cid, String modelID) {
		List<Map> supplies = new ArrayList<Map>();
		
		try {
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + "MySuppliesByModel");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("cid=" + cid);
			request.append("&modelID=" + modelID);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(connection.getInputStream());
	
			Element dataSet = doc.getRootElement().getChild("diffgram", Namespace.getNamespace("diffgram", "urn:schemas-microsoft-com:xml-diffgram-v1")).getChild("NewDataSet");
			Iterator results = dataSet.getChildren("table").iterator();
			while (results.hasNext()) {
				Element model = (Element) results.next();
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", model.getChild("id").getText());
				map.put("sku", model.getChild("sku").getText());
				map.put("is_oem", model.getChild("is_oem").getText());
				map.put("part_number", model.getChild("part_number").getText());				
				map.put("brand", model.getChild("brand").getText());				
				if (model.getChild("custom0") != null) map.put("custom0", model.getChild("custom0").getText());				
				if (model.getChild("custom1") != null) map.put("custom1", model.getChild("custom1").getText());	
				if (model.getChild("custom2") != null) map.put("custom2", model.getChild("custom2").getText());				
				supplies.add(map);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return supplies;		
	}
	
	public List<Map> MySuppliesByModelPreferredSort(String cid, String modelID, String preferredBrands) {
		List<Map> supplies = new ArrayList<Map>();
		
		try {
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + "MySuppliesByModelPreferredSort");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("cid=" + cid);
			request.append("&modelID=" + modelID);
			request.append("&preferredBrands=" + preferredBrands);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
			
			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(connection.getInputStream());
	
			Element dataSet = doc.getRootElement().getChild("diffgram", Namespace.getNamespace("diffgram", "urn:schemas-microsoft-com:xml-diffgram-v1")).getChild("NewDataSet");
			Iterator results = dataSet.getChildren("table").iterator();
			while (results.hasNext()) {
				Element model = (Element) results.next();
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", model.getChild("id").getText());
				map.put("sku", model.getChild("sku").getText());
				map.put("is_oem", model.getChild("is_oem").getText());
				map.put("part_number", model.getChild("part_number").getText());				
				map.put("brand", model.getChild("brand").getText());				
				if (model.getChild("custom0") != null) map.put("custom0", model.getChild("custom0").getText());				
				if (model.getChild("custom1") != null) map.put("custom1", model.getChild("custom1").getText());	
				if (model.getChild("custom2") != null) map.put("custom2", model.getChild("custom2").getText());				
				supplies.add(map);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return supplies;		
	}
	
	public List<Map> ModelsByManufacturerAndType(String cid, String modelManufacturerID, String modelType, String pageSize, String pageIndex) {
		List<Map> models = new ArrayList<Map>();
		
		try {
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + "ModelsByManufacturerAndType");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("cid=" + cid);
			request.append("&modelManufacturerID=" + modelManufacturerID);
			request.append("&modelType=" + modelType);			
			request.append("&pageSize=" + pageSize);
			request.append("&pageIndex=" + pageIndex);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(connection.getInputStream());
	
			Element dataSet = doc.getRootElement().getChild("diffgram", Namespace.getNamespace("diffgram", "urn:schemas-microsoft-com:xml-diffgram-v1")).getChild("NewDataSet");
			Iterator results = dataSet.getChildren("table").iterator();
			while (results.hasNext()) {
				Element model = (Element) results.next();
				Map<String, String> map = new HashMap<String, String>();
				map.put("name", model.getChild("name").getText());
				map.put("id", model.getChild("id").getText());
				models.add(map);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return models;		
	}
	
	public List<Map> ModelsBySearch(String cid, String match) {
		List<Map> models = new ArrayList<Map>();
		
		try {
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + "ModelsBySearch");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("cid=" + cid);
			request.append("&match=" + match);
			request.append("&matchType=" + 3);			
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(connection.getInputStream());
	
			Element dataSet = doc.getRootElement().getChild("diffgram", Namespace.getNamespace("diffgram", "urn:schemas-microsoft-com:xml-diffgram-v1")).getChild("NewDataSet");
			Iterator results = dataSet.getChildren("table").iterator();
			while (results.hasNext()) {
				Element model = (Element) results.next();
				Map<String, String> map = new HashMap<String, String>();
				map.put("name", model.getChild("name").getText());
				map.put("manufacturer", model.getChild("manufacturer").getText());
				map.put("id", model.getChild("id").getText());
				models.add(map);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return models;		
	}	
	
	public List<Map> OemSuppliesBySearch(String cid, String match) {
		List<Map> models = new ArrayList<Map>();
		
		try {
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + "OemSuppliesBySearch");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("cid=" + cid);
			request.append("&match=" + match);
			request.append("&matchType=" + 3);			
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(connection.getInputStream());
	
			Element dataSet = doc.getRootElement().getChild("diffgram", Namespace.getNamespace("diffgram", "urn:schemas-microsoft-com:xml-diffgram-v1")).getChild("NewDataSet");
			Iterator results = dataSet.getChildren("table").iterator();
			while (results.hasNext()) {
				Element model = (Element) results.next();
				Map<String, String> map = new HashMap<String, String>();
				map.put("part_number", model.getChild("part_number").getText());
				map.put("manufacturer", model.getChild("manufacturer").getText());
				map.put("id", model.getChild("id").getText());
				models.add(map);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return models;		
	}	
	
	public List<Map> MySuppliesByOemSupply(String cid, String oemSupplyID) {
		List<Map> supplies = new ArrayList<Map>();
		
		try {
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + "MySuppliesByOemSupply");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("cid=" + cid);
			request.append("&oemSupplyID=" + oemSupplyID);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(connection.getInputStream());
	
			Element dataSet = doc.getRootElement().getChild("diffgram", Namespace.getNamespace("diffgram", "urn:schemas-microsoft-com:xml-diffgram-v1")).getChild("NewDataSet");
			Iterator results = dataSet.getChildren("table").iterator();
			while (results.hasNext()) {
				Element model = (Element) results.next();
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", model.getChild("id").getText());
				map.put("sku", model.getChild("sku").getText());
				map.put("is_oem", model.getChild("is_oem").getText());
				map.put("part_number", model.getChild("part_number").getText());				
				map.put("brand", model.getChild("brand").getText());				
				if (model.getChild("custom0") != null) map.put("custom0", model.getChild("custom0").getText());				
				if (model.getChild("custom1") != null) map.put("custom1", model.getChild("custom1").getText());	
				if (model.getChild("custom2") != null) map.put("custom2", model.getChild("custom2").getText());				
				supplies.add(map);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		return supplies;		
	}	
	
	public List<Map> MySuppliesByOemSupplyPreferredSort(String cid, String oemSupplyID, String preferredBrands) {
		List<Map> supplies = new ArrayList<Map>();
		
		try {
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + "MySuppliesByOemSupplyPreferredSort");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("cid=" + cid);
			request.append("&oemSupplyID=" + oemSupplyID);
			request.append("&preferredBrands=" + preferredBrands);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(connection.getInputStream());
	
			Element dataSet = doc.getRootElement().getChild("diffgram", Namespace.getNamespace("diffgram", "urn:schemas-microsoft-com:xml-diffgram-v1")).getChild("NewDataSet");
			Iterator results = dataSet.getChildren("table").iterator();
			while (results.hasNext()) {
				Element model = (Element) results.next();
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", model.getChild("id").getText());
				map.put("sku", model.getChild("sku").getText());
				map.put("is_oem", model.getChild("is_oem").getText());
				map.put("part_number", model.getChild("part_number").getText());				
				map.put("brand", model.getChild("brand").getText());				
				if (model.getChild("custom0") != null) map.put("custom0", model.getChild("custom0").getText());				
				if (model.getChild("custom1") != null) map.put("custom1", model.getChild("custom1").getText());	
				if (model.getChild("custom2") != null) map.put("custom2", model.getChild("custom2").getText());				
				supplies.add(map);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		return supplies;		
	}		
	
	public List<Map> ModelsBySearchPaged(String cid, String match, String matchType, String pageSize, String pageIndex, Map<String, String> allRecords) {
		List<Map> models = new ArrayList<Map>();
		
		try {
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + "ModelsBySearchPaged");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("cid=" + cid);
			request.append("&match=" + match);
			request.append("&matchType=" + matchType);	
			request.append("&pageSize=" + pageSize);	
			request.append("&pageIndex=" + pageIndex);			
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(connection.getInputStream());
	
			Element dataSet = doc.getRootElement().getChild("diffgram", Namespace.getNamespace("diffgram", "urn:schemas-microsoft-com:xml-diffgram-v1")).getChild("NewDataSet");
			Iterator results = dataSet.getChildren("table").iterator();
			while (results.hasNext()) {
				Element model = (Element) results.next();
				Map<String, String> map = new HashMap<String, String>();
				map.put("name", model.getChild("name").getText());
				map.put("manufacturer", model.getChild("manufacturer").getText());
				map.put("id", model.getChild("id").getText());
				models.add(map);
			}
			allRecords.put( "count", dataSet.getChild( "all_records" ).getChild( "count" ).getText() );
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return models;		
	}		
	
	public List<Map> OemSuppliesBySearchPaged(String cid, String match, String matchType, String pageSize, String pageIndex, Map<String, String> allRecords) {
		List<Map> models = new ArrayList<Map>();
		
		try {
			URL url = new URL(protocol + "://" + hostname + "/" + prefix + "/" + "OemSuppliesBySearchPaged");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("cid=" + cid);
			request.append("&match=" + match);
			request.append("&matchType=" + matchType);	
			request.append("&pageSize=" + pageSize);	
			request.append("&pageIndex=" + pageIndex);			
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(connection.getInputStream());
	
			Element dataSet = doc.getRootElement().getChild("diffgram", Namespace.getNamespace("diffgram", "urn:schemas-microsoft-com:xml-diffgram-v1")).getChild("NewDataSet");
			Iterator results = dataSet.getChildren("table").iterator();
			while (results.hasNext()) {
				Element model = (Element) results.next();
				Map<String, String> map = new HashMap<String, String>();
				map.put("part_number", model.getChild("part_number").getText());
				map.put("manufacturer", model.getChild("manufacturer").getText());
				map.put("id", model.getChild("id").getText());
				models.add(map);
			}
			allRecords.put( "count", dataSet.getChild( "all_records" ).getChild( "count" ).getText() );
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return models;		
	}		
}
