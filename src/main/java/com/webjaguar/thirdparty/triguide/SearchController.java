/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 */

package com.webjaguar.thirdparty.triguide;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;

public class SearchController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
	private String[] skuSuffix;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {    	
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();
    	
    	TriguideApi triguide = new TriguideApi(siteConfig.get( "TRIGUIDE_URL" ).getValue());
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		ModelAndView modelAndView = new ModelAndView("frontend/triguide/search0", "model", map);
		
		if (((String) gSiteConfig.get("gSEARCH")).equals("triguide")) {
			String triguideID = siteConfig.get("SEARCH_ACCOUNT").getValue();
			
			// MANUFACTURER SEARCH
			if (request.getParameter("manufacturerId") != null && request.getParameter("modelId") == null && !request.getParameter("manufacturerId").equals("")) {
				String modelType = "-1";
				if (request.getParameter("modelType") != null) {
					modelType = request.getParameter("modelType");
				}
				map.put("triguideModels", triguide.ModelsByManufacturerAndType(triguideID, request.getParameter("manufacturerId"), modelType, "0", "0"));
				modelAndView.setViewName("frontend/triguide/search1");
			}	
			if (request.getParameter("modelId") != null) {
				map.put("triguideSupplies", triguide.MySuppliesByModelPreferredSort(triguideID, request.getParameter("modelId"), "*|"));		
				modelAndView.setViewName("frontend/triguide/search2");
			}
			
			// MODEL SEARCH
			if (request.getParameter("match") != null) {
				if (!request.getParameter("match").trim().equals("")) {
					Map<String, String> allRecords = new HashMap<String, String>();
					
					String matchType = ServletRequestUtils.getStringParameter( request, "matchType", "5" );
					Integer pageIndex = ServletRequestUtils.getIntParameter( request, "page", 1 );
					Integer pageSize = ServletRequestUtils.getIntParameter( request, "size", 20 );
					map.put("triguideModels", triguide.ModelsBySearchPaged(triguideID, request.getParameter("match"), matchType, pageSize.toString(), pageIndex.toString(), allRecords));				
					
					int count = Integer.parseInt( allRecords.get( "count" ) );
					int pageCount = count/pageSize;
					if (count%pageSize > 0) {
						pageCount++;
					}
					
					map.put("pageStart", ((pageIndex-1)*pageSize + 1));
					map.put("pageCount", pageCount);					
					map.put("pageIndex", pageIndex);	
					map.put("count", count);				
				}
				modelAndView.setViewName("frontend/triguide/search3");				
			}
		
			// OEM SUPPLY SEARCH
			if (request.getParameter("match2") != null) {
				if (!request.getParameter("match2").trim().equals("")) {
					Map<String, String> allRecords = new HashMap<String, String>();
					
					String matchType = ServletRequestUtils.getStringParameter( request, "matchType", "5" );
					Integer pageIndex = ServletRequestUtils.getIntParameter( request, "page", 1 );
					Integer pageSize = ServletRequestUtils.getIntParameter( request, "size", 20 );

					map.put("triguideModels", triguide.OemSuppliesBySearchPaged(triguideID, request.getParameter("match2"), matchType, pageSize.toString(), pageIndex.toString(), allRecords));				
					
					int count = Integer.parseInt( allRecords.get( "count" ) );
					int pageCount = count/pageSize;
					if (count%pageSize > 0) {
						pageCount++;
					}
					
					map.put("pageStart", ((pageIndex-1)*pageSize + 1));
					map.put("pageCount", pageCount);					
					map.put("pageIndex", pageIndex);	
					map.put("count", count);	
				}
				modelAndView.setViewName("frontend/triguide/search4");				
			}
			if (request.getParameter("oemSupplyId") != null) {
				map.put("triguideSupplies", triguide.MySuppliesByOemSupplyPreferredSort(triguideID, request.getParameter("oemSupplyId"), "*|"));		
				modelAndView.setViewName("frontend/triguide/search2");
			}
		}
		if (map.containsKey( "triguideSupplies" )) {
			// cross reference to local items
			List supplies = (List) map.get( "triguideSupplies" );
			Iterator iter = supplies.iterator();
			Map<Integer, Product> alsoConsiderMap = new HashMap<Integer, Product>();
			while (iter.hasNext()) {
				Map supply = (Map) iter.next();
				String sku = ((String) supply.get( "sku" )).trim();
				if (!sku.equals( "" )) { 
					Product product = null;
					for (int i=0; i<skuSuffix.length; i++) {
						product = this.webJaguar.getProductById( this.webJaguar.getProductIdBySku( sku + skuSuffix[i]), request );
						if (product != null) break;
					}
					if (product != null) {
						// also consider
						if ((Boolean) gSiteConfig.get( "gALSO_CONSIDER" ) && 
								((Configuration) siteConfig.get( "ALSO_CONSIDER_DETAILS_ONLY" )).getValue().equals( "false" )) {
							Product alsoConsider = this.webJaguar.getAlsoConsiderBySku( product.getAlsoConsider(), request );
							if (alsoConsider != null) {
								alsoConsiderMap.put( product.getId(), alsoConsider );
							}
						}
					}
					supply.put( "product", product );
				}
			}
			if (!alsoConsiderMap.isEmpty()) {	// not empty
				map.put( "alsoConsiderMap", alsoConsiderMap );								
			}
		}
				
        return modelAndView;    	
    }

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	public void setSkuSuffix(String[] skuSuffix)
	{
		this.skuSuffix = skuSuffix;
	}

}
