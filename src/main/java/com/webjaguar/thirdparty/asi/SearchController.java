/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 05.18.2010
 */

package com.webjaguar.thirdparty.asi;
/* TO BE REMOVE
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis.message.MessageElement;
import org.datacontract.schemas._2004._07.ASI_AsiData.AsiResponseOfXmlElement;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.tempuri.ProductServiceLocator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.asicentral.content_bridge._1_0.products.IProductService;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;

public class SearchController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	String[] config;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {    	

    	Map<String, Object> model = new HashMap<String, Object>();
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
    	
    	config = siteConfig.get("ASI").getValue().split(",");
    	
    	if (config.length > 0) {
        	try {
        		ProductServiceLocator service = new ProductServiceLocator();
        		getProduct(service);
        		
        		//model.put("products", products);

        	} catch (Exception e) {
        		//System.out.println(e.toString());
        	}     		
    	} else {
    		return null;
    	}
    	
    	ModelAndView modelAndView = new ModelAndView("frontend/asi/search", "model", model);
    	
        return modelAndView;
    }
    
    private void getProduct(ProductServiceLocator service) {
//    	IProductService port;
//		
//		//port = service.getWSHttpBinding_IProductService();
//		port = service.getBasicHttpBinding_IProductService();
//
//		int productListSize = 0;
//		int index = -1;
//		do {
//			// listByToken(token, start, count) 
//			// Number of records to return can not exceed maximum number of records per page (current maximum is 500)
//			//AsiResponseOfXmlElement productResponse = port.listByToken(config[0], (++index)*500, 500);
//			AsiResponseOfXmlElement productSearchResponse = port.searchByToken(config[0], "", (++index)*500, 500);
//			
//			for ( MessageElement re : productSearchResponse.getData().get_any()) {
//				
//	        	Document doc = re.getAsDocument();
//	        	NodeList productList = doc.getElementsByTagName("Product");
//	        	productListSize = productList.getLength();
//	        	
//	        	for (int i=0; i<productList.getLength(); i++) {
//	                // Get element
//	                Element element = (Element)productList.item(i);
//	                NodeList product = element.getChildNodes();
//	
//					HashMap<String, Object> map = new HashMap<String, Object>();
//					List<Integer> catIds = new ArrayList<Integer>();
//					
//					String ProductName = "";
//					StringBuffer keyword = new StringBuffer();
//					StringBuffer xmlString = new StringBuffer();
//	                for (int j=0; j<product.getLength(); j++) {
//	                	
//	    				Element productElement = (Element)product.item(j);
//	    				
//	    				Double priceChange = null;
//	    				
//	    				
//	    				// categories
//	    				if ( productElement.getTagName().equalsIgnoreCase("Category")) {
//	    				}
//	                }
//	        	}
//			}
//		while (false) {
//			
//		}

    }
}
 */