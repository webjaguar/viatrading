package com.webjaguar.thirdparty.asi;

public interface AsiDiffFacade {
	
	public boolean getAsiProductDiff();
	public boolean getAsiSupplierDiff();

}
