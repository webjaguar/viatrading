package com.webjaguar.thirdparty.asi;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import com.webjaguar.thirdparty.asi.asiAPI.SearchResult;
import com.webjaguar.web.domain.Utilities;

import com.webjaguar.logic.interfaces.AsiAPIFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Supplier;

public class AsiAPI implements AsiAPIFacade {
	
	/*
	 * Key: Authorization
	 * Value: AsiMemberAuth client_id=500002145&client_secret=6b0d16dd019affe67e046c4ce264b2b2
	 
	 * Once you have that set, you should be able to access the search API:
	 * Example:
	 * http://stage-api-2.asicentral.com/v1/products/search.json?q=bags
	 * 
	 * http://stage-api-2.asicentral.com/v1/products/4870064.xml
	 * 
	 * http://stage-api-2.asicentral.com/v1/suppliers/1371.xml
	 * 
	 */
	
		
	//Staging URL
	
//	static final String API_url = "http://stage-api-2.asicentral.com/";
//	static final String API_url = "https://stage-api.asicentral.com/";
//	static final String API_key = "Authorization";
//	static final String App_secret = "AsiMemberAuth client_id=500002145&client_secret=6b0d16dd019affe67e046c4ce264b2b2";
	
	//Production URL
	static final String API_url = "http://api.asicentral.com/";
	static final String API_key = "Authorization";
	static String API_secret = null;
	
	
	static Map<String, Configuration> siteConfig;
	String type = "diff";
	String service = "products/";
	String fileType = ".xml";  //.json
	
	File asiFeedDir;
	String[] productToken;
	String[] supplierToken;
	
	static String content;
	static File productFile;
	static File supplierFile;
	
	// constructor
	public AsiAPI() {
		super();
	}
	
	public AsiAPI(Map<String, Configuration> siteConfig, String tempPath, String fileType) {
		this.siteConfig = siteConfig;
		this.API_secret = "AsiMemberAuth client_id="+siteConfig.get("ASI_KEY").getValue()+"&client_secret="+siteConfig.get("ASI_SECRET").getValue();
			
		File tempFolder = new File(tempPath);	
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File asiFeedDir = new File(tempFolder, "asiFeed");
		if (!asiFeedDir.exists()) {
			asiFeedDir.mkdir();
		}
		this.asiFeedDir = asiFeedDir;
		
		productToken = siteConfig.get("ASI_FEED_PRODUCT_TOKEN").getValue().split(",");
		supplierToken = siteConfig.get("ASI_FEED_SUPPLIER_TOKEN").getValue().split(",");
	}
	
	// Products
	public int getAsiProductsCount(String query) {
		
		final String ASI_API_URL = API_url+"v1/products/search.xml?";
		
		String urlString = ASI_API_URL + query;
		
		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setRequestProperty(API_key, API_secret);
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);
			//System.out.println("Status Code "+connection.getResponseCode());
			
			StringBuilder productsXml = URLConnectionReader(connection.getInputStream());
			
			JAXBContext context;
    		Unmarshaller unmarshaller = null;
    		ByteArrayInputStream bis;
    		SearchResult searchResult = null;
    		try {
				context = JAXBContext.newInstance(SearchResult.class);
				unmarshaller = context.createUnmarshaller();
				StringBuffer sb = new StringBuffer(productsXml.toString());
				bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
				searchResult = (SearchResult) unmarshaller.unmarshal(bis);
			} catch (Exception e) { e.printStackTrace(); }
			
			if(searchResult != null && searchResult.getResults() != null){
				return searchResult.getResultsTotal().intValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public SearchResult getAsiProducts(String query) {
		
		final String ASI_API_URL = API_url+"v1/products/search.xml?";
		
		String urlString = ASI_API_URL + query;
		URL url;
		SearchResult searchResult = null;
		try {
			url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			
			// Setup HTTP POST parameters
			connection.setRequestProperty(API_key, API_secret);
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);
			//System.out.println("Status Code "+connection.getResponseCode());
			
			StringBuilder productsXml = URLConnectionReader(connection.getInputStream());
			JAXBContext context;
    		Unmarshaller unmarshaller = null;
    		ByteArrayInputStream bis;
    		
			try {
				context = JAXBContext.newInstance(SearchResult.class);
				unmarshaller = context.createUnmarshaller();
				StringBuffer sb = new StringBuffer(productsXml.toString());
				bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
				searchResult = (SearchResult) unmarshaller.unmarshal(bis);
				
			} catch (Exception e) { e.printStackTrace(); }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return searchResult;
	}
	
	public String getAsiProductXml(Integer id) {
		if (id <= 0 ) return null;
		
		final String ASI_API_URL = API_url+"v1/products/";
		try {
			String urlString = ASI_API_URL + id + ".xml";
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			//System.out.println("Status Code "+connection.getResponseCode());
			
			// Setup HTTP POST parameters
			connection.setRequestProperty(API_key, API_secret);
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);
			return URLConnectionReader(connection.getInputStream()).toString();
		} catch(Exception e){
			e.printStackTrace();
			return null;
		}	
	}
	
	public boolean getAsiProductImage(Integer id) {
		return true;
	}
	

	// Suppliers
	public int getAsiSuppliersCount(String query) {
		final String ASI_API_URL = API_url+"v1/suppliers/search.xml?";
		String urlString = ASI_API_URL + query;
		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			
			// Setup HTTP POST parameters
			connection.setRequestProperty(API_key, API_secret);
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);
			StringBuilder supplierXml = URLConnectionReader(connection.getInputStream());
			
			JAXBContext context;
    		Unmarshaller unmarshaller = null;
    		ByteArrayInputStream bis;
    		SearchResult searchResult = null;
    		try {
				context = JAXBContext.newInstance(SearchResult.class);
				unmarshaller = context.createUnmarshaller();
				StringBuffer sb = new StringBuffer(supplierXml.toString());
				bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
				searchResult = (SearchResult) unmarshaller.unmarshal(bis);
			} catch (Exception e) { e.printStackTrace(); }
			
			if(searchResult != null && searchResult.getResults() != null){
				return searchResult.getResultsTotal().intValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	public SearchResult getAsiSuppliers(String query) {
		
		final String ASI_API_URL = API_url+"v1/suppliers/search.xml?";
		String urlString = ASI_API_URL + query;
		//http://stage-api-2.asicentral.com/v1/suppliers/1371.xml
		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			
			// Setup HTTP POST parameters
			connection.setRequestProperty(API_key, API_secret);
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);
			
			StringBuilder supplierXml = URLConnectionReader(connection.getInputStream());
			
			JAXBContext context;
    		Unmarshaller unmarshaller = null;
    		ByteArrayInputStream bis;
    		SearchResult searchResult = null;
    		try {
				context = JAXBContext.newInstance(SearchResult.class);
				unmarshaller = context.createUnmarshaller();
				StringBuffer sb = new StringBuffer(supplierXml.toString());
				bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
				searchResult = (SearchResult) unmarshaller.unmarshal(bis);
			} catch (Exception e) { e.printStackTrace(); }
			
			return searchResult;
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Supplier getAsiSupplier(Integer id) {
		if (id <= 0 ) return null;
		Supplier supplier = null;
		
		final String ASI_API_URL = API_url+"v1/suppliers/";
		try {
			String urlString = ASI_API_URL + id + ".xml";
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			
			// Setup HTTP POST parameters
			connection.setRequestProperty(API_key, API_secret);
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);
			
			StringBuilder supplierXml = URLConnectionReader(connection.getInputStream());
			
			JAXBContext context;
    		Unmarshaller unmarshaller = null;
    		ByteArrayInputStream bis;
    		com.webjaguar.thirdparty.asi.asiAPI.Supplier asiSup = null;
			try {
				context = JAXBContext.newInstance(com.webjaguar.thirdparty.asi.asiAPI.Supplier.class);
				unmarshaller = context.createUnmarshaller();
				StringBuffer sb = new StringBuffer(supplierXml.toString());
				bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
				asiSup = (com.webjaguar.thirdparty.asi.asiAPI.Supplier) unmarshaller.unmarshal(bis);
			} catch (Exception e) { e.printStackTrace(); }
			
			supplier = new Supplier();
			Address supplierAddress = new Address();
			StringBuffer note = new StringBuffer();
			
			supplier.setAccountNumber(asiSup.getAsiNumber());
			supplierAddress.setCompany((asiSup.getName() != null) ? asiSup.getName().toString() : "");
			supplierAddress.setPhone((asiSup.getPhone() != null && asiSup.getPhone().getPhone() != null) ? asiSup.getPhone().getPhone().getWork() : "");
			supplierAddress.setFax((asiSup.getFax() != null && asiSup.getFax().getPhone() != null) ? asiSup.getFax().getPhone().getTollFree() : "");
			supplierAddress.setEmail((asiSup.getEmail() != null) ? asiSup.getEmail().toString() : "");
			if (asiSup.getAddresses() != null && asiSup.getAddresses().getPrimary() != null) {
				supplierAddress.setPrimary(true);
				supplierAddress.setAddr1((asiSup.getAddresses().getPrimary().getStreet1() != null) ? asiSup.getAddresses().getPrimary().getStreet1().toString() : "");
				supplierAddress.setStateProvince((asiSup.getAddresses().getPrimary().getState() != null) ? asiSup.getAddresses().getPrimary().getState().toString() : "");
				supplierAddress.setCity((asiSup.getAddresses().getPrimary().getCity() != null) ? asiSup.getAddresses().getPrimary().getCity().toString() : "");
				supplierAddress.setZip((asiSup.getAddresses().getPrimary().getZip() != null) ? asiSup.getAddresses().getPrimary().getZip().toString() : "");
				supplierAddress.setCountry("US");

			}
			
			note.append("OfficeHours:\n" + ((asiSup.getOfficeHours() != null) ? asiSup.getOfficeHours().toString() : ""));
			note.append("\n\nWebsite:\n" + ((asiSup.getWebsites() != null) ? Utilities.toString(asiSup.getWebsites().getWebsite(), "\n") : ""));
		    //note.append("\n\nTollFree:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
            note.append("\n\nArtEmail:\n" + ((asiSup.getArtwork() != null && asiSup.getArtwork().getEmail() != null) ? asiSup.getArtwork().getEmail() : ""));
            note.append("\n\nMarketingPolicy:\n" + ((asiSup.getMarketingPolicy() != null) ? asiSup.getMarketingPolicy().toString() : "" ));
            
            note.append("\n\nLineNames:\n" + ((asiSup.getLineNames() != null) ? Utilities.toString(asiSup.getLineNames().getLineName(), ", ") : ""));
            note.append("\n\nImprintMethodes:\n" + ((asiSup.getImprintingMethods() != null) ? Utilities.toString(asiSup.getImprintingMethods().getMethod(), ", ") : ""));
            note.append("\n\nFunctions:\n" + ((asiSup.getFunctions() != null) ? Utilities.toString(asiSup.getFunctions().getFunction(), ", ") : ""));
            note.append("\n\nComment:\n" + ((asiSup.getArtwork() != null && asiSup.getArtwork().getComment() != null) ? asiSup.getArtwork().getComment() : ""));

            supplier.setNote(note.toString());
            supplier.setAddress(supplierAddress);
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return supplier;
	}
	
	public StringBuilder URLConnectionReader(InputStream is) throws IOException {
	        StringBuilder sb = new StringBuilder();
	        
	        BufferedReader in = new BufferedReader(new InputStreamReader(is));
	        String inputLine;
	        while ((inputLine = in.readLine()) != null) { 
	            sb.append(inputLine);
	        }
	        in.close();
	        return sb;
	}
}
	