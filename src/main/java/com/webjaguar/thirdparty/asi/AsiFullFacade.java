package com.webjaguar.thirdparty.asi;

public interface AsiFullFacade {
	
	public boolean getAsiProductFull();
	public boolean getAsiSupplierFull();

}
