/* Copyright 2011 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 01.27.2011
 */

package com.webjaguar.thirdparty.asi;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;

public class AsiConfigurationController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }		
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    	throws Exception {	

		Map<String, Object> model = new HashMap<String, Object>();    	

    	// check if update button was pressed
		if (request.getParameter("__update") != null) {
			updateSiteConfig(request);
			model.put("message", "update.successful");
		}
     	model.put("siteConfig", webJaguar.getSiteConfig());
      	
    	return new ModelAndView("admin/vendors/asi/asiConfig", model);
    }
    
	private void updateSiteConfig(HttpServletRequest request) {

		List<Configuration> data = new ArrayList<Configuration>();

		String keys[] = request.getParameterValues("__key");
		
		for (int i=0; i<keys.length; i++){
			Configuration config = new Configuration();
			config.setKey(keys[i]);
			String value = request.getParameter(keys[i]);
			config.setValue(value);
			data.add(config); 
		}
		
		String mkeys[] = request.getParameterValues("__mkey");
		if (mkeys != null) {
			for (int i=0; i<mkeys.length; i++){
				Configuration config = new Configuration();
				config.setKey(mkeys[i]);
				StringBuffer sbuff = new StringBuffer();
				String values[] = request.getParameterValues(mkeys[i]);
				if (values != null) {
					for (int x=0; x<values.length; x++) {
						if(!values[x].isEmpty()) {
							sbuff.append( values[x] + "," );
						}
					}			
				}
				config.setValue(sbuff.toString());
				data.add(config); 
			}			
		}
		
		this.webJaguar.updateSiteConfig(data);	
	}   
	
}
