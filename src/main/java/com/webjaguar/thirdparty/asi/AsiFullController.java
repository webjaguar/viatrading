/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 8.24.2010
 */

package com.webjaguar.thirdparty.asi;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.model.Configuration;

public class AsiFullController extends WebApplicationObjectSupport implements Controller {

	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }

	int size=2048;
	static boolean checkSKU = false;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
		String diffDate = ServletRequestUtils.getStringParameter(request, "diffDate", null);
		AsiFull asiInit = new AsiFull(siteConfig, getServletContext().getRealPath("temp"), diffDate);
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		if (!asiInit.getAsiSupplierFull()) {
			notifyAdmin("Exception : ASI parseSupplierFull() on " + siteConfig.get("SITE_URL").getValue(), null);
		}
		
		if (!asiInit.getAsiProductFull()) {
			notifyAdmin("Exception : ASI parseProductFull() on " + siteConfig.get("SITE_URL").getValue(), null);
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		notifyAdmin("Success: asiFullController - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
		
		return new ModelAndView("admin/vendors/index");
	}

	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}