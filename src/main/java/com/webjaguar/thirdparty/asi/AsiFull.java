package com.webjaguar.thirdparty.asi;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import asiProduct.CriteriaSet;
import asiProduct.PriceGrid;
import asiProduct.Product;
import asiProduct.Categories.Category;
import asiProduct.CriteriaSet.Options.Option;
import asiProduct.PriceGrid.Price;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Supplier;

public class AsiFull implements AsiFullFacade {
	
	private static WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	static final String ASI_URL = "https://www.asicontentbridge.com/1.0/Public/DownloadService.ashx";
	static Map<String, Configuration> siteConfig;
	String type = "full";
	String diffDate;
	int size=2048;
	//static boolean checkSKU = false;
	
	File asiFeedDir;
	String[] productToken;
	String[] supplierToken;
	
	static String content;
	static File productFile;
	static File supplierFile;
	
	// constructor
	public AsiFull() {
		super();
	}
	
	public AsiFull(Map<String, Configuration> siteConfig, String tempPath, String diffDate) {
		this.siteConfig = siteConfig;
		this.diffDate = diffDate;
		
		File tempFolder = new File(tempPath);	
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File asiFeedDir = new File(tempFolder, "asiFeed");
		if (!asiFeedDir.exists()) {
			asiFeedDir.mkdir();
		}
		this.asiFeedDir = asiFeedDir;
		
		productToken = siteConfig.get("ASI_FEED_PRODUCT_TOKEN").getValue().split(",");
		supplierToken = siteConfig.get("ASI_FEED_SUPPLIER_TOKEN").getValue().split(",");
	}
	
	public boolean getAsiProductFull() {
		if ( productToken.length < 1) {
			return false;
		}

		System.out.println("Product full import start:" + new Date() );
		File zip_file = new File(asiFeedDir, "productfull.zip");
		downloadZipFile(ASI_URL, zip_file, productToken[0], type);
		unzipUsingUnix("unzip -oP "+productToken[1]+" productfull.zip", asiFeedDir);
		
		try {
			//productFile  = new File("/Users/shahinnaji/Desktop/ASI File Feed/NEW.XML");
			productFile = new File(asiFeedDir.getPath()+"/"+productToken[0]+".xml");
			parseXmlFile(productFile, siteConfig);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		System.out.println("Product full import end:" + new Date() );
		return true;
	}
	
	public boolean getAsiSupplierFull() {
		if ( productToken.length < 1) {
			return false;
		}
		
		System.out.println("Supplier full import start:" + new Date() );
		File zip_file = new File(asiFeedDir, "supplierfull.zip");
		downloadZipFile(ASI_URL, zip_file, supplierToken[0], type);
		unzipUsingUnix("unzip -oP "+supplierToken[1]+" supplierfull.zip", asiFeedDir);
		
		try {
			parseSupplierXmlFile(new File(asiFeedDir.getPath()+"/"+supplierToken[0]+".xml"), siteConfig);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		System.out.println("Supplier full import end:" + new Date() );
		return true;
	}

	private void unzipUsingUnix(String cmd, File workDir){
		String s = null;
		try {
			Process p = Runtime.getRuntime().exec(cmd, null, workDir);
			int i = p.waitFor();
			if (i == 0){
				BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
				// read the output from the command
				while ((s = stdInput.readLine()) != null) {
					System.out.println(s);
				}
			}
			else {
				BufferedReader stdErr = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				// read the output from the command
				while ((s = stdErr.readLine()) != null) {
					System.out.println(s);
				}
	
				}
		}
		catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public void downloadZipFile(String sourceUrl, File zip_file, String token, String type) {
		
		
		Date start = new Date();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		OutputStream outStream = null;
		URLConnection  urlConnection = null;
		InputStream inputStream = null;
		
		try {
			// https://<server location>/DownloadService.ashx?token=<token>&command=get_file&type=[full|diff]&sequence=[1- 4]
			URL Url = new URL(sourceUrl + "?token=" + token + "&command=get_file&type="+type);
			byte[] buffer;
			int ByteRead=0;
			outStream = new BufferedOutputStream(new FileOutputStream(zip_file));

			urlConnection = Url.openConnection();
			inputStream = urlConnection.getInputStream();
			buffer = new byte[size];
			while ((ByteRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, ByteRead);
			}
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
			//notifyAdmin("ASI downloadZipFile() on " + siteConfig.get("SITE_URL").getValue(), e.getMessage());
		}
	}
	
	public void parseXmlFile(File xml_file, Map<String, Configuration> siteConfig) {
		try {
			SAXParserFactory f = SAXParserFactory.newInstance();
	        SAXParser parser = f.newSAXParser();
	        DefaultHandler handler = new MyContentHandler();
	        content = FileUtils.readFileToString(productFile, "UTF-8");
	        parser.parse(xml_file,handler);
	    } catch (ParserConfigurationException e) {System.out.println("error 1 ");
	        //notifyAdmin("parseXmlFile ApplianceSpec on " + siteConfig.get("SITE_URL").getValue(), e.toString());
	    } catch (SAXException e) {System.out.println("error 2 ");
	    	//notifyAdmin("parseXmlFile ApplianceSpec on " + siteConfig.get("SITE_URL").getValue(), e.toString());
	    } catch (IOException e) {System.out.println("error 3 ");
	    	//notifyAdmin("parseXmlFile ApplianceSpec on " + siteConfig.get("SITE_URL").getValue(), e.toString());
	    }
	}

	public void parseSupplierXmlFile(File xmlFile, Map<String, Configuration> siteConfig) throws ParserConfigurationException, SAXException, IOException {
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(xmlFile);
		doc.getDocumentElement().normalize();
		NodeList supplierList = doc.getElementsByTagName("Supplier");
		
		for (int i=0; i<supplierList.getLength(); i++) {
			Element element = (Element)supplierList.item(i);

            NodeList supplierNodeList = element.getChildNodes();

			Supplier supplier = new Supplier();
			Address supplierAddress = new Address();
			StringBuffer note = new StringBuffer();
            
		
			for (int j=0; j<supplierNodeList.getLength(); j++) {
				Element supplierElement = (Element)supplierNodeList.item(j);
				
				try {
					if ( supplierElement.getTagName().equalsIgnoreCase("CompanyName")) {
    					supplierAddress.setCompany(supplierElement.getFirstChild().getNodeValue());
    				}
					if ( supplierElement.getTagName().equalsIgnoreCase("Email")) {
    					supplierAddress.setEmail(supplierElement.getFirstChild().getNodeValue());
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("MainAddress1")) {
    					supplierAddress.setAddr1(supplierElement.getFirstChild().getNodeValue());
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("MainCity")) {
    					supplierAddress.setCity(supplierElement.getFirstChild().getNodeValue());
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("MainState")) {
    					supplierAddress.setStateProvince(supplierElement.getFirstChild().getNodeValue());
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("MainPostalCode")) {
    					supplierAddress.setZip(supplierElement.getFirstChild().getNodeValue());
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("MainCountry")) {
    					supplierAddress.setCountry(supplierElement.getFirstChild().getNodeValue());
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("Phone")) {
    					supplierAddress.setPhone(((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("Fax")) {
    					supplierAddress.setFax(((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("OfficeHours")) {
    					note.append("OfficeHours:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("WebAddress")) {
    					note.append("\n\nWebsite:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("TollFree")) {
    					note.append("\n\nTollFree:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("ArtEmail")) {
    					note.append("\n\nArtEmail:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("MarketingPolicy")) {
    					note.append("\n\nMarketingPolicy:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
    				}
    				if ( supplierElement.getTagName().equalsIgnoreCase("SupplierRep")) {
    					note.append("\n\nSupplierRep:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
    				}
    				
    				if ( supplierElement.getTagName().equalsIgnoreCase("ASI")) {
    					supplier.setAccountNumber(supplierElement.getFirstChild().getNodeValue());
    				}
				} catch (NullPointerException e) {
				}
			}
			 supplier.setNote(note.toString());
	         supplier.setAddress(supplierAddress);
	         Integer supplierId = null;
	         supplierId = this.webJaguar.getSupplierIdByAccountNumber(supplier.getAccountNumber());
			 if (supplierId != null) {
				 supplier.setId(supplierId);
				 System.out.println("update ASI supplier Account# " + supplier.getAccountNumber());
				 this.webJaguar.updateSupplierById(supplier);
			 } else {
				this.webJaguar.insertSupplier(supplier);
			 }
			 
		}
		
	}
	
	public static class MyContentHandler extends DefaultHandler {
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		HashMap<String, Object> map;
		StringBuffer xml = new StringBuffer();
		StringBuffer keywords = new StringBuffer();
		StringBuffer tradeNameString = new StringBuffer();
		StringBuffer colorString = new StringBuffer();
		StringBuffer materialString = new StringBuffer();
		StringBuffer themeString = new StringBuffer();
		StringBuffer originString = new StringBuffer();
		
		boolean product;
		boolean productOptions, shippingOptions;
		boolean tradeName, productColor, productMaterial, theme, origin;
	    
		public void startDocument() throws SAXException {
			xml = new StringBuffer();
			keywords = new StringBuffer();
			tradeNameString = new StringBuffer();
			colorString = new StringBuffer();
			materialString = new StringBuffer();
			themeString = new StringBuffer();
			originString = new StringBuffer();
	    }
	    
	    public void endDocument() throws SAXException {
	       // for files that has less than 1000 Product
	       if (data.size() > 0) {
				// products
				updateProduct(data);
				data.clear();
				map.clear();
			}
	    }
	    
	    public void startElement(String nsURI, String strippedName, String tagName, Attributes attributes) throws SAXException {
	    	if (tagName.equalsIgnoreCase("Product")) { 

	    		map = new HashMap<String, Object>();
	    		map.put("feed", "ASI");				
				map.put("feed_new", true);
				map.put("product_layout", "007");
				map.put("compare", 1);
	    		product = true;
	    		
	    		//Id is not unique and will be change per catalog year
	    		//map.put("asi_unique_id", attributes.getValue("Id"));
	    		
	    		// read and save from file directly
				int start = content.indexOf("<Product Id=\""+attributes.getValue("Id")+"\"");
	    		int end = content.indexOf("</Product>", start) + 10;
	    		xml.append(content.substring(start, end));
	    		
	    		// convert to asi Product object
	    		JAXBContext context;
	    		Unmarshaller unmarshaller = null;
	    		ByteArrayInputStream bis;
	    		Product asiProd = null;
				try {
					context = JAXBContext.newInstance(asiProduct.Product.class);
					unmarshaller = context.createUnmarshaller();
					StringBuffer sb = new StringBuffer(xml);
					bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
					asiProd = (asiProduct.Product) unmarshaller.unmarshal(bis);
				} catch (Exception e) { e.printStackTrace(); }
				
				if(siteConfig.get("ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE").getValue().equals("true")){
				    webJaguar.applyPriceDifference(asiProd, null, siteConfig.get("ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS").getValue(), Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_TYPE").getValue()), Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_FORMULA").getValue()));
				}
				if(asiProd.getGeneralOptions() != null && asiProd.getGeneralOptions().getCriteriaSet() != null) {
					for(CriteriaSet criteria : asiProd.getGeneralOptions().getCriteriaSet()) {
						if(criteria.getName().equalsIgnoreCase("RushService") || criteria.getName().equalsIgnoreCase("ProductionTime")) {
							if(criteria.getOptions() != null && criteria.getOptions().getOption() != null){
					    		for(Option option: criteria.getOptions().getOption()) {
						    		if(option.getValue().equals("1")) {
										map.put("field_46", 1);
							    	}
								}
							}
						}
					}
				}
				if (asiProd.getPricing() != null && asiProd.getPricing().getPriceGrid() != null) {
					boolean outerBreak = false;
					for (PriceGrid priceGrid : asiProd.getPricing().getPriceGrid()){
						int index = 1;
						if (outerBreak) break;
						if (priceGrid.isBase() && !priceGrid.getId().equalsIgnoreCase("pricegrid-99")) {
							for (Price price : priceGrid.getPrice()) {
								map.put("price_"+ index, price.getValue());
								map.put("qty_break_" + (index-1), price.getQuantity());
								
								if (index==1) {
									map.put("minimum_qty", price.getQuantity());
									// get the highest cost
									//map.put("cost1", price.getCost());
									outerBreak = true;
								}
								index++;
							}
							// get the least cost
							map.put("cost1", priceGrid.getPrice().get(index-2).getCost());
						}
					}
				}
				map.put("asi_xml", xml.toString());
				map.put("field_38", asiProd.getName());
				// 0 or 1 (1 means rushService is available)
				map.put("field_39", asiProd.isRushService() ? "Yes" : "No");
				//UniqueId is still unique, where as product id may stay the same from catalog to catalog.
				
				// multiple by sth to generate a different sku;
	    		Integer newSku = Integer.rotateLeft(Integer.parseInt(asiProd.getId().toString()), 2);
				map.put("sku", newSku.toString());
				keywords.append(newSku.toString()+" ");
				
				map.put("field_40", asiProd.getUniqueId().toString() );
		    	map.put("asi_unique_id", asiProd.getUniqueId().toString());
		    	map.put("asi_id", asiProd.getSupplier().getASI());
		    	map.put("field_34", asiProd.getSupplier().getASI());
		    	map.put("field_42", asiProd.getNumber());
		    	map.put("field_43", asiProd.getSupplier().getName());
		    	map.put("long_desc", asiProd.getDescription());
		    	map.put("short_desc", asiProd.getSummary());
		    	map.put("name", asiProd.getSummary());
		    	if (asiProd.getCategories() != null && asiProd.getCategories().getCategory() != null && asiProd.getCategories().getCategory().size() > 0) {
			    	int index = 47;
		    		for (Category cat : asiProd.getCategories().getCategory()) {
			    		map.put("field_" + index, cat.getName());
			    		index++;
			    		if (index > 49) break;
			    	}
		    	}
		    	if (asiProd.getMedia() != null && asiProd.getMedia().getFile() != null && asiProd.getMedia().getFile().size() > 0) {
		    		int imageIndex = 1;
		    		for (asiProduct.Media.File file : asiProd.getMedia().getFile()) {
		    			if (file.getType().equalsIgnoreCase("Image")) {
			    			map.put("image"+imageIndex++, "https://www.asicontentbridge.com/1.0/Public/DownloadService.ashx?token="+file.getId().toString()+"&command=get_image&type=prodbigimgs");
		    			}
		    		}
		    	}
	    	}
	    	
	    	if (tagName.equalsIgnoreCase("ProductOptions")) { 
	    		productOptions = true; 
	    	}
	    	if (tagName.equalsIgnoreCase("ShippingOptions")) { 
	    		shippingOptions = true; 
	    	}
	    	if (productOptions && tagName.equalsIgnoreCase("CriteriaSet") && attributes.getValue("Name").equalsIgnoreCase("TradeName")) { 
	    		tradeName = true;
	    	}
	    	if (tradeName && tagName.equalsIgnoreCase("Option")) {
	    		tradeNameString.append(attributes.getValue("Value") + " ");
	    	}
	    	if (productOptions && tagName.equalsIgnoreCase("CriteriaSet") && attributes.getValue("Name").equalsIgnoreCase("ProductColor")) { 
	    		productColor = true;
	    	}
	    	if (productColor && tagName.equalsIgnoreCase("Option")) {
	    		colorString.append(attributes.getValue("Value") + " ");
	    	}
	    	if (productOptions && tagName.equalsIgnoreCase("CriteriaSet") && attributes.getValue("Name").equalsIgnoreCase("Material")) { 
	    		productMaterial = true;
	    	}
	    	if (productMaterial && tagName.equalsIgnoreCase("Option")) {
	    		materialString.append(attributes.getValue("Value") + " ");
	    	}
	    	if (productOptions && tagName.equalsIgnoreCase("CriteriaSet") && attributes.getValue("Name").equalsIgnoreCase("Origin")) { 
	    		origin = true;
	    	}
	    	if (origin && tagName.equalsIgnoreCase("Option")) {
	    		originString.append(attributes.getValue("Value") + " ");
	    	}
	    	if (productOptions && tagName.equalsIgnoreCase("CriteriaSet") && attributes.getValue("Name").equalsIgnoreCase("Theme")) { 
	    		theme = true;
	    	}
	    	if (theme && tagName.equalsIgnoreCase("Option")) {
	    		themeString.append(attributes.getValue("Value") + " ");
	    	}
	    }
	    
	    public void endElement(String nsURI, String strippedName, String tagName) throws SAXException {
	    	if (!tagName.equalsIgnoreCase("Products")) { }
	    	if (tagName.equalsIgnoreCase("Product")) { 
	    		product = false; 
	    		map.put("keywords", keywords.toString());
	    		map.put("field_44", tradeNameString.toString());
	    		map.put("field_41", colorString.toString());
	    		map.put("field_37", materialString.toString());
	    		map.put("field_36", originString.toString());
	    		map.put("field_35", themeString.toString());
	    		xml = new StringBuffer();
	    		keywords = new StringBuffer();
	    		tradeNameString = new StringBuffer();
	    		colorString = new StringBuffer();
	    		materialString = new StringBuffer();
	    		originString = new StringBuffer();
	    		themeString = new StringBuffer();
	    	}
	    	if (tagName.equalsIgnoreCase("ProductOptions")) { 
	    		productOptions = false; 
	    	}
	    	if (tagName.equalsIgnoreCase("ShippingOptions")) { 
	    		shippingOptions = false; 
	    	}
	    	if (tagName.equalsIgnoreCase("CriteriaSet")) { 
	    		tradeName = false;
	    		productColor = false;
	    		productMaterial = false;
	    		origin = false;
	    		theme = false;
	    	}
    	
	    	if (tagName.equalsIgnoreCase("Product")) {
	    		//if (!checkSKU || !webJaguar.isASIUniqueIdExist(Integer.parseInt(map.get("asi_unique_id").toString()))) {
	    			data.add(map);
	    			//System.out.println(map.get("asi_unique_id").toString());
	    		//}
	    		
	    	   
	    		if (data.size() == 1000) {
					// products
					updateProduct(data);
					data.clear();
					map.clear();
				}
	    	}
	    }
	    
	    public void characters(char buf[], int offset, int len) throws SAXException {
	       String value = new String(buf, offset, len);
	       if (value.trim().length() > 0) {

	       }  
	    }
	    
	    public void ignorableWhitespace(char buf[], int offset, int len) throws SAXException {

	    }
	    
	    public void skippedEntity(String name)  throws SAXException {
	    	System.out.println("skippedEntity " + name);
	   }
	    
	    public void warning(SAXParseException e) throws SAXException {
	        System.out.println("Warning: " + e.getMessage());
	    }

	    private void updateProduct(List<Map <String, Object>> data) {
	    	// update Products
			webJaguar.nonTransactionSafeUpdateASIFileFeedProduct(getCsvFeedList(), data);
	    }
	    
	    private List<CsvFeed> getCsvFeedList() {
			List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
			csvFeedList.add(new CsvFeed("sku"));
			csvFeedList.add(new CsvFeed("asi_id"));
			csvFeedList.add(new CsvFeed("asi_unique_id"));
			csvFeedList.add(new CsvFeed("asi_xml"));
			csvFeedList.add(new CsvFeed("name"));
			csvFeedList.add(new CsvFeed("short_desc"));
			csvFeedList.add(new CsvFeed("long_desc"));
			csvFeedList.add(new CsvFeed("keywords"));
			csvFeedList.add(new CsvFeed("feed"));
			csvFeedList.add(new CsvFeed("feed_new"));
			csvFeedList.add(new CsvFeed("product_layout"));
			csvFeedList.add(new CsvFeed("compare"));
			csvFeedList.add(new CsvFeed("price_1"));
			csvFeedList.add(new CsvFeed("price_2"));
			csvFeedList.add(new CsvFeed("price_3"));
			csvFeedList.add(new CsvFeed("price_4"));
			csvFeedList.add(new CsvFeed("price_5"));
			csvFeedList.add(new CsvFeed("price_6"));
			csvFeedList.add(new CsvFeed("price_7"));
			csvFeedList.add(new CsvFeed("price_8"));
			csvFeedList.add(new CsvFeed("price_9"));
			csvFeedList.add(new CsvFeed("price_10"));
			csvFeedList.add(new CsvFeed("qty_break_1"));
			csvFeedList.add(new CsvFeed("qty_break_2"));
			csvFeedList.add(new CsvFeed("qty_break_3"));
			csvFeedList.add(new CsvFeed("qty_break_4"));
			csvFeedList.add(new CsvFeed("qty_break_5"));
			csvFeedList.add(new CsvFeed("qty_break_6"));
			csvFeedList.add(new CsvFeed("qty_break_7"));
			csvFeedList.add(new CsvFeed("qty_break_8"));
			csvFeedList.add(new CsvFeed("qty_break_9"));
			csvFeedList.add(new CsvFeed("minimum_qty"));
			//Supplier ASI
			csvFeedList.add(new CsvFeed("field_34"));
			//Theme
			csvFeedList.add(new CsvFeed("field_35"));
			//Origin
			csvFeedList.add(new CsvFeed("field_36"));
			//Material
			csvFeedList.add(new CsvFeed("field_37"));
			//Name
			csvFeedList.add(new CsvFeed("field_38"));
			//RushService
			csvFeedList.add(new CsvFeed("field_39"));
			//UniqueId
			csvFeedList.add(new CsvFeed("field_40"));
			//Product Color
			csvFeedList.add(new CsvFeed("field_41"));
			//Number
			csvFeedList.add(new CsvFeed("field_42"));
			//Supplier Name
			csvFeedList.add(new CsvFeed("field_43"));
			//TradeName Name
			csvFeedList.add(new CsvFeed("field_44"));
			//Diff State (don't use here)
			//csvFeedList.add(new CsvFeed("field_45"));
			
			//1 day Rush Service
			csvFeedList.add(new CsvFeed("field_46"));
			// category1
			csvFeedList.add(new CsvFeed("field_47"));
			// category2
			csvFeedList.add(new CsvFeed("field_48"));
			// category3
			csvFeedList.add(new CsvFeed("field_49"));
			return csvFeedList;
		}
	 } 
}