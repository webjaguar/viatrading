/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 10.10.2010
 */

package com.webjaguar.thirdparty.asi;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.asi.asiAPI.Price;
import com.webjaguar.thirdparty.asi.asiAPI.Product.Variants.Variant;
import com.webjaguar.thirdparty.asi.asiAPI.SearchDimensionItems.Value;
import com.webjaguar.thirdparty.asi.asiAPI.SearchResult;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.Utilities;

public class AsiAPIController extends MultiActionController {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private MailSender mailSender;

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	int size = 2048;
	String sequence;

	public ModelAndView asiProduct(HttpServletRequest request, HttpServletResponse response, Layout layout) {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		Integer id = ServletRequestUtils.getIntParameter(request, "id", 0);
		AsiAPI asiProduct = new AsiAPI(siteConfig, getServletContext().getRealPath("temp"), "xml");

		String asiProductXml = asiProduct.getAsiProductXml(id);
		Product product = new Product();
		this.webJaguar.getProductFromAsiXml(asiProductXml, product);
		Integer supplierId = this.webJaguar.getSupplierIdByAccountNumber(product.getAsiId().toString());
		// Do not allow to see Product, if supplier is not downloaded already.
		if (supplierId == null) {
			return new ModelAndView(new RedirectView("home.jhtm"));
		}

		product.setDefaultSupplierId(supplierId);

		Integer existingProductId = this.webJaguar.getProductIdBySku(product.getSku());
		if (existingProductId != null) {
			product.setId(existingProductId);
		} else {
			this.webJaguar.insertProduct(product, request);
		}

		// insert Product Supplier Cost
		JAXBContext context;
		Unmarshaller unmarshaller = null;
		ByteArrayInputStream bis;
		com.webjaguar.thirdparty.asi.asiAPI.Product asiProd = null;
		try {
			context = JAXBContext.newInstance(com.webjaguar.thirdparty.asi.asiAPI.Product.class);
			unmarshaller = context.createUnmarshaller();
			StringBuffer sb = new StringBuffer(product.getAsiXML());
			bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
			asiProd = (com.webjaguar.thirdparty.asi.asiAPI.Product) unmarshaller.unmarshal(bis);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
		HashMap<String, Object> map = new HashMap<String, Object>();

		// get the least Cost
		if (asiProd.getLowestPrice() != null) {
			map.put("cost1", asiProd.getLowestPrice().getCost());
		} else if (asiProd.getVariants() != null) {
			Double lowCost = null;
			for (Variant variant : asiProd.getVariants().getVariant()) {
				if (variant.getPrices() != null && variant.getPrices().getPrice() != null) {
					for (Price iPrice : variant.getPrices().getPrice()) {
						if (lowCost == null || lowCost > iPrice.getCost().doubleValue()) {
							lowCost = iPrice.getCost().doubleValue();
						}
					}
				}
			}
			map.put("cost1", lowCost);
		}
		map.put("sku", product.getSku());
		map.put("supplier_sku", asiProd.getNumber());
		map.put("asi_id", product.getAsiId());
		data.add(map);

		this.webJaguar.insertProductSupplier(data, supplierId);

		if (product != null && product.getId() != null) {
			return new ModelAndView(new RedirectView("product.jhtm?id=" + product.getId()));
		} else {
			return new ModelAndView(new RedirectView("home.jhtm"));
		}

	}

	public ModelAndView asiSupplier(HttpServletRequest request, HttpServletResponse response, Layout layout) {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> map = new HashMap<String, Object>();

		Integer id = ServletRequestUtils.getIntParameter(request, "id", 0);
		AsiAPI asiProduct = new AsiAPI(siteConfig, getServletContext().getRealPath("temp"), "xml");

		Supplier supplier = asiProduct.getAsiSupplier(id);
		if (supplier == null) {
			// Supplier does exist anymore so we need to deactivate the Supplier is exist on local DB
			// notifyAdmin("Exception : ASI parseSupplierDiff() on " + siteConfig.get("SITE_URL").getValue(), null);
		} else {

			this.webJaguar.insertSupplier(supplier);
		}

		return new ModelAndView("admin/vendors/index");

	}

	public ModelAndView asiProducts(HttpServletRequest request, HttpServletResponse response, Layout layout) {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		StringBuffer queryString = new StringBuffer();
		StringBuffer queryTerm = new StringBuffer();
		Map<String, Object> myModel = new HashMap<String, Object>();
		try {
			if (ServletRequestUtils.getStringParameter(request, "q") != null) {
				queryTerm.append(ServletRequestUtils.getStringParameter(request, "q"));
				myModel.put("q", ServletRequestUtils.getStringParameter(request, "q"));
			} else {
				return null;
			}

			// force to search on specific list of suppliers
			// remove this code when ASI fix it on their service
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
				if (prop.get("asi.suppliers") != null) {
					queryTerm.append(" asi:" + (String) prop.get("asi.suppliers"));
				}
			} catch (Exception e) {
			}

			if (ServletRequestUtils.getStringParameter(request, "category") != null && !ServletRequestUtils.getStringParameter(request, "category").isEmpty()) {
				queryTerm.append(" category:" + ServletRequestUtils.getStringParameter(request, "category"));
				myModel.put("category", ServletRequestUtils.getStringParameter(request, "category"));
				myModel.put("categoryH", ServletRequestUtils.getStringParameter(request, "categoryH").replace("(", "_br_").split("_br_")[0]);
			}
			if (ServletRequestUtils.getStringParameter(request, "size") != null && !ServletRequestUtils.getStringParameter(request, "size").isEmpty()) {
				queryTerm.append(" size:" + ServletRequestUtils.getStringParameter(request, "size"));
				myModel.put("size", ServletRequestUtils.getStringParameter(request, "size"));
				myModel.put("sizeH", ServletRequestUtils.getStringParameter(request, "sizeH").replace("(", "_br_").split("_br_")[0]);
			}
			if (ServletRequestUtils.getStringParameter(request, "shape") != null && !ServletRequestUtils.getStringParameter(request, "shape").isEmpty()) {
				queryTerm.append(" shape:" + ServletRequestUtils.getStringParameter(request, "shape"));
				myModel.put("shape", ServletRequestUtils.getStringParameter(request, "shape"));
				myModel.put("shapeH", ServletRequestUtils.getStringParameter(request, "shapeH").replace("(", "_br_").split("_br_")[0]);
			}
			if (ServletRequestUtils.getStringParameter(request, "material") != null && !ServletRequestUtils.getStringParameter(request, "material").isEmpty()) {
				queryTerm.append(" material:" + ServletRequestUtils.getStringParameter(request, "material"));
				myModel.put("material", ServletRequestUtils.getStringParameter(request, "material"));
				myModel.put("materialH", ServletRequestUtils.getStringParameter(request, "materialH").replace("(", "_br_").split("_br_")[0]);
			}
			if (ServletRequestUtils.getStringParameter(request, "color") != null && !ServletRequestUtils.getStringParameter(request, "color").isEmpty()) {
				queryTerm.append(" color:" + ServletRequestUtils.getStringParameter(request, "color"));
				myModel.put("color", ServletRequestUtils.getStringParameter(request, "color"));
				myModel.put("colorH", ServletRequestUtils.getStringParameter(request, "colorH").replace("(", "_br_").split("_br_")[0]);
			}

			queryString.append("q=" + URLEncoder.encode(queryTerm.toString(), "UTF-8"));
			Utilities.replaceString(queryString, "+", "%20", false);

			queryString.append("&rpp=" + ServletRequestUtils.getIntParameter(request, "rpp", 25));
			myModel.put("rpp", ServletRequestUtils.getIntParameter(request, "rpp", 25));

			queryString.append("&page=" + ServletRequestUtils.getIntParameter(request, "page", 1));
			myModel.put("page", ServletRequestUtils.getIntParameter(request, "page", 1));

			if (ServletRequestUtils.getStringParameter(request, "sort") != null) {
				queryString.append("&sort=" + ServletRequestUtils.getStringParameter(request, "sort"));
				myModel.put("sort", ServletRequestUtils.getStringParameter(request, "sort"));
			}
			queryString.append("&dl=color,size,material,shape,category,supplier");
			myModel.put("dl", ServletRequestUtils.getStringParameter(request, "dl"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		AsiAPI asiProduct = new AsiAPI(siteConfig, getServletContext().getRealPath("temp"), "xml");

		// query should be URL encoded.
		SearchResult searchResult = asiProduct.getAsiProducts(queryString.toString());

		if (searchResult != null) {
			myModel.put("searchResult", searchResult);
			myModel.put("totalResult", searchResult.getResultsTotal());
			myModel.put("pages", (searchResult.getResultsTotal() / searchResult.getResultsPerPage()) + 1);
			myModel.put("pageStart", ((searchResult.getPage() - 1) * searchResult.getResultsPerPage() + 1));
			myModel.put("pageEnd", (searchResult.getPage() * searchResult.getResultsPerPage()) > searchResult.getResultsTotal() ? searchResult.getResultsTotal() : searchResult.getPage()
					* searchResult.getResultsPerPage());

			// remove product if it is sponsored
			if (searchResult.getResults() != null && searchResult.getResults().getProduct() != null) {
				Iterator<com.webjaguar.thirdparty.asi.asiAPI.Product> iter = searchResult.getResults().getProduct().iterator();
				while (iter.hasNext()) {
					com.webjaguar.thirdparty.asi.asiAPI.Product item = iter.next();
					if (item.getAd() != null) {
						iter.remove();
					}
				}
			}

			// Assuming that the product is already in the database
			List<com.webjaguar.thirdparty.asi.asiAPI.Product> list = searchResult.getResults().getProduct();
			List<Product> productList = new ArrayList<Product>();
			for (com.webjaguar.thirdparty.asi.asiAPI.Product pro : list) {
				Product product = this.webJaguar.getProductByAsiId(pro.getId().intValue(), request);
				// add it to list if active and searchable
				if (product != null && product.isActive() && product.isSearchable()) {
					productList.add(product);
				}

			}
			// Redirect Url if products are not found
			if (productList.isEmpty() && !((Configuration) siteConfig.get("PRODUCT_SEARCH_NOT_FOUND_URL")).getValue().isEmpty()) {
				return new ModelAndView(new RedirectView(siteConfig.get("PRODUCT_SEARCH_NOT_FOUND_URL").getValue()));
			}

			myModel.put("products", productList);
			myModel.put("asiProducts", list);

			if (searchResult.getDimensions() != null) {
				Map<String, Object> filtersMap = new HashMap<String, Object>();
				if (searchResult.getDimensions().getColors() != null) {
					Map<String, String> filterValues = new HashMap<String, String>();
					if (searchResult.getDimensions().getColors().getValue() != null) {
						for (Value item : searchResult.getDimensions().getColors().getValue()) {
							filterValues.put(item.getName() + " (" + item.getProducts() + ")", item.getContextPath());
						}
					}
					if (!filterValues.isEmpty()) {
						filtersMap.put("color", filterValues);
					}
				}
				if (searchResult.getDimensions().getMaterials() != null) {
					Map<String, String> filterValues = new HashMap<String, String>();
					if (searchResult.getDimensions().getMaterials().getValue() != null) {
						for (Value item : searchResult.getDimensions().getMaterials().getValue()) {
							filterValues.put(item.getName() + " (" + item.getProducts() + ")", item.getContextPath());
						}
					}
					if (!filterValues.isEmpty()) {
						filtersMap.put("material", filterValues);
					}
				}
				if (searchResult.getDimensions().getShapes() != null) {
					Map<String, String> filterValues = new HashMap<String, String>();
					if (searchResult.getDimensions().getShapes().getValue() != null) {
						for (Value item : searchResult.getDimensions().getShapes().getValue()) {
							filterValues.put(item.getName() + " (" + item.getProducts() + ")", item.getContextPath());
						}
					}
					if (!filterValues.isEmpty()) {
						filtersMap.put("shape", filterValues);
					}
				}
				if (searchResult.getDimensions().getSizes() != null) {
					Map<String, String> filterValues = new HashMap<String, String>();
					if (searchResult.getDimensions().getSizes().getValue() != null) {
						for (Value item : searchResult.getDimensions().getSizes().getValue()) {
							filterValues.put(item.getName() + " (" + item.getProducts() + ")", item.getContextPath());
						}
					}
					if (!filterValues.isEmpty()) {
						filtersMap.put("size", filterValues);
					}
				}

				if (searchResult.getDimensions().getCategories() != null) {
					Map<String, String> filterValues = new HashMap<String, String>();
					if (searchResult.getDimensions().getCategories().getValue() != null) {
						for (Value item : searchResult.getDimensions().getCategories().getValue()) {
							filterValues.put(item.getName() + " (" + item.getProducts() + ")", item.getContextPath());
						}
					}
					if (!filterValues.isEmpty()) {
						filtersMap.put("category", filterValues);
					}
				}
				if (!filtersMap.isEmpty()) {
					myModel.put("filters", filtersMap);
				}
			}
		}
		return new ModelAndView("frontend/asi/search", "model", myModel);
	}

	/*
	 * This is temporary method. we need to replace this with asiProducts We will loop through all ASI library base on the Search criteria and check if Product is available in our DB if product does not available on our System or inactive or Not searchable, then remove it from
	 * List, this will fix the Pagination Then we need to show the results for the firstpage and so on.
	 * This is VERY SLOW, We DON'T USE IT FOR NOW
	 */
	public ModelAndView asiProducts2(HttpServletRequest request, HttpServletResponse response, Layout layout) {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		StringBuffer queryString = new StringBuffer();
		StringBuffer queryTerm = new StringBuffer();
		Map<String, Object> myModel = new HashMap<String, Object>();
		try {
			if (ServletRequestUtils.getStringParameter(request, "q") != null) {
				queryTerm.append(ServletRequestUtils.getStringParameter(request, "q"));
				myModel.put("q", ServletRequestUtils.getStringParameter(request, "q"));
			} else {
				return null;
			}

			// force to search on specific list of suppliers
			// remove this code when ASI fix it on their service
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
				if (prop.get("asi.suppliers") != null) {
					queryTerm.append(" asi:" + (String) prop.get("asi.suppliers"));
				}
			} catch (Exception e) {
			}

			if (ServletRequestUtils.getStringParameter(request, "category") != null && !ServletRequestUtils.getStringParameter(request, "category").isEmpty()) {
				queryTerm.append(" category:" + ServletRequestUtils.getStringParameter(request, "category"));
				myModel.put("category", ServletRequestUtils.getStringParameter(request, "category"));
				myModel.put("categoryH", ServletRequestUtils.getStringParameter(request, "categoryH").replace("(", "_br_").split("_br_")[0]);
			}
			if (ServletRequestUtils.getStringParameter(request, "size") != null && !ServletRequestUtils.getStringParameter(request, "size").isEmpty()) {
				queryTerm.append(" size:" + ServletRequestUtils.getStringParameter(request, "size"));
				myModel.put("size", ServletRequestUtils.getStringParameter(request, "size"));
				myModel.put("sizeH", ServletRequestUtils.getStringParameter(request, "sizeH").replace("(", "_br_").split("_br_")[0]);
			}
			if (ServletRequestUtils.getStringParameter(request, "shape") != null && !ServletRequestUtils.getStringParameter(request, "shape").isEmpty()) {
				queryTerm.append(" shape:" + ServletRequestUtils.getStringParameter(request, "shape"));
				myModel.put("shape", ServletRequestUtils.getStringParameter(request, "shape"));
				myModel.put("shapeH", ServletRequestUtils.getStringParameter(request, "shapeH").replace("(", "_br_").split("_br_")[0]);
			}
			if (ServletRequestUtils.getStringParameter(request, "material") != null && !ServletRequestUtils.getStringParameter(request, "material").isEmpty()) {
				queryTerm.append(" material:" + ServletRequestUtils.getStringParameter(request, "material"));
				myModel.put("material", ServletRequestUtils.getStringParameter(request, "material"));
				myModel.put("materialH", ServletRequestUtils.getStringParameter(request, "materialH").replace("(", "_br_").split("_br_")[0]);
			}
			if (ServletRequestUtils.getStringParameter(request, "color") != null && !ServletRequestUtils.getStringParameter(request, "color").isEmpty()) {
				queryTerm.append(" color:" + ServletRequestUtils.getStringParameter(request, "color"));
				myModel.put("color", ServletRequestUtils.getStringParameter(request, "color"));
				myModel.put("colorH", ServletRequestUtils.getStringParameter(request, "colorH").replace("(", "_br_").split("_br_")[0]);
			}

			queryString.append("q=" + URLEncoder.encode(queryTerm.toString(), "UTF-8"));
			Utilities.replaceString(queryString, "+", "%20", false);

			queryString.append("&rpp=" + ServletRequestUtils.getIntParameter(request, "rpp", 25));
			myModel.put("rpp", ServletRequestUtils.getIntParameter(request, "rpp", 25));

			queryString.append("&page=" + ServletRequestUtils.getIntParameter(request, "page", 1));
			myModel.put("page", ServletRequestUtils.getIntParameter(request, "page", 1));

			if (ServletRequestUtils.getStringParameter(request, "sort") != null) {
				queryString.append("&sort=" + ServletRequestUtils.getStringParameter(request, "sort"));
				myModel.put("sort", ServletRequestUtils.getStringParameter(request, "sort"));
			}
			queryString.append("&dl=color,size,material,shape,category,supplier");
			myModel.put("dl", ServletRequestUtils.getStringParameter(request, "dl"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		AsiAPI asiProduct = new AsiAPI(siteConfig, getServletContext().getRealPath("temp"), "xml");
		// query should be URL encoded.
		SearchResult searchResult = asiProduct.getAsiProducts(queryString.toString());
		// Based on query we get the searchResult which contains only 25(based on rpp)
		// We can get the number of total result.
		int totalResult = searchResult.getResultsTotal().intValue();
		// Based on total number we do a for loop to store all active&no ad product in an ArrayList.
		ArrayList<Integer> totalProductId = new ArrayList<Integer>();
		for (int i = 1, num = 0; num < totalResult; i++, num += 100) {
			StringBuffer queryString2 = new StringBuffer();
			try {
				queryString2.append("q=" + URLEncoder.encode(queryTerm.toString(), "UTF-8"));
				Utilities.replaceString(queryString, "+", "%20", false);
				// Every time we get the 100(maximum asiAPI allowed) products
				queryString2.append("&rpp=" + 100);
				// Based on page number to get product
				queryString2.append("&page=" + i);

				if (ServletRequestUtils.getStringParameter(request, "sort") != null) {
					queryString2.append("&sort=" + ServletRequestUtils.getStringParameter(request, "sort"));
				}
				queryString2.append("&dl=color,size,material,shape,category,supplier");
			} catch (Exception e) {
				e.printStackTrace();
			}
			AsiAPI asiProduct2 = new AsiAPI(siteConfig, getServletContext().getRealPath("temp"), "xml");
			SearchResult searchResult2 = asiProduct2.getAsiProducts(queryString2.toString());
			if (searchResult2 != null) {
				// remove product if it is sponsored
				if (searchResult2.getResults() != null && searchResult2.getResults().getProduct() != null) {
					Iterator<com.webjaguar.thirdparty.asi.asiAPI.Product> iter = searchResult2.getResults().getProduct().iterator();
					while (iter.hasNext()) {
						com.webjaguar.thirdparty.asi.asiAPI.Product item = iter.next();
						if (item.getAd() != null) {
							iter.remove();
						}
					}
				}
				List<com.webjaguar.thirdparty.asi.asiAPI.Product> list = searchResult2.getResults().getProduct();
				for (com.webjaguar.thirdparty.asi.asiAPI.Product pro : list) {
					Product product = this.webJaguar.getProductByAsiId(pro.getId().intValue(), request);
					// add it to ArrayList if active and searchable
					if (product != null && product.isActive() && product.isSearchable()) {
						totalProductId.add(pro.getId().intValue());
					}
				}
			}
		}

		if (searchResult != null) {
			// This list is for asiList which is needed on search.jsp if param.view == 'asi'
			List<com.webjaguar.thirdparty.asi.asiAPI.Product> list = searchResult.getResults().getProduct();

			// Create a productList which will be shown on the current page.
			List<Product> productList = new ArrayList<Product>();
			// Based on the page to get productId index and do for loop to store product on the productList
			for (int i = (searchResult.getPage() - 1) * searchResult.getResultsPerPage(); i < (searchResult.getPage()) * searchResult.getResultsPerPage() && i < totalProductId.size(); i++) {
				Product product = this.webJaguar.getProductByAsiId(totalProductId.get(i), request);
				productList.add(product);
			}
			// Redirect Url if products are not found
			if (productList.isEmpty() && !((Configuration) siteConfig.get("PRODUCT_SEARCH_NOT_FOUND_URL")).getValue().isEmpty()) {
				return new ModelAndView(new RedirectView(siteConfig.get("PRODUCT_SEARCH_NOT_FOUND_URL").getValue()));
			}
			// Get the number of Page we need to show.
			int totalActivePage = ((totalProductId.size() % searchResult.getResultsPerPage()) > 0) ? (totalProductId.size() / searchResult.getResultsPerPage() + 1)
					: (totalProductId.size() / searchResult.getResultsPerPage());

			myModel.put("searchResult", searchResult);
			myModel.put("totalResult", totalProductId.size());
			myModel.put("pages", totalActivePage);
			myModel.put("pageStart", (searchResult.getPage() - 1) * searchResult.getResultsPerPage() + 1);
			myModel.put("pageEnd",
					(searchResult.getPage() * searchResult.getResultsPerPage()) > totalProductId.size() ? totalProductId.size() : searchResult.getPage() * searchResult.getResultsPerPage());
			myModel.put("products", productList);
			myModel.put("asiProducts", list);

			if (searchResult.getDimensions() != null) {
				Map<String, Object> filtersMap = new HashMap<String, Object>();
				if (searchResult.getDimensions().getColors() != null) {
					Map<String, String> filterValues = new HashMap<String, String>();
					if (searchResult.getDimensions().getColors().getValue() != null) {
						for (Value item : searchResult.getDimensions().getColors().getValue()) {
							filterValues.put(item.getName() + " (" + item.getProducts() + ")", item.getContextPath());
						}
					}
					if (!filterValues.isEmpty()) {
						filtersMap.put("color", filterValues);
					}
				}
				if (searchResult.getDimensions().getMaterials() != null) {
					Map<String, String> filterValues = new HashMap<String, String>();
					if (searchResult.getDimensions().getMaterials().getValue() != null) {
						for (Value item : searchResult.getDimensions().getMaterials().getValue()) {
							filterValues.put(item.getName() + " (" + item.getProducts() + ")", item.getContextPath());
						}
					}
					if (!filterValues.isEmpty()) {
						filtersMap.put("material", filterValues);
					}
				}
				if (searchResult.getDimensions().getShapes() != null) {
					Map<String, String> filterValues = new HashMap<String, String>();
					if (searchResult.getDimensions().getShapes().getValue() != null) {
						for (Value item : searchResult.getDimensions().getShapes().getValue()) {
							filterValues.put(item.getName() + " (" + item.getProducts() + ")", item.getContextPath());
						}
					}
					if (!filterValues.isEmpty()) {
						filtersMap.put("shape", filterValues);
					}
				}
				if (searchResult.getDimensions().getSizes() != null) {
					Map<String, String> filterValues = new HashMap<String, String>();
					if (searchResult.getDimensions().getSizes().getValue() != null) {
						for (Value item : searchResult.getDimensions().getSizes().getValue()) {
							filterValues.put(item.getName() + " (" + item.getProducts() + ")", item.getContextPath());
						}
					}
					if (!filterValues.isEmpty()) {
						filtersMap.put("size", filterValues);
					}
				}

				if (searchResult.getDimensions().getCategories() != null) {
					Map<String, String> filterValues = new HashMap<String, String>();
					if (searchResult.getDimensions().getCategories().getValue() != null) {
						for (Value item : searchResult.getDimensions().getCategories().getValue()) {
							filterValues.put(item.getName() + " (" + item.getProducts() + ")", item.getContextPath());
						}
					}
					if (!filterValues.isEmpty()) {
						filtersMap.put("category", filterValues);
					}
				}
				if (!filtersMap.isEmpty()) {
					myModel.put("filters", filtersMap);
				}
			}
		}

		Layout productSearchLayout = this.webJaguar.getSystemLayout("productSearch", request.getHeader("host"), request);

		if (request.getParameter("cid") != null && !request.getParameter("cid").isEmpty()) {
			Category category = this.webJaguar.getCategoryById(Integer.parseInt(request.getParameter("cid")), "");
			productSearchLayout.setHeaderHtml(category.getHtmlCode());
			productSearchLayout.setFooterHtml(category.getFooterHtmlCode());
			myModel.put("productSearchLayout", productSearchLayout);
		} else {
			myModel.put("productSearchLayout", productSearchLayout);
		}

		return new ModelAndView("frontend/asi/search", "model", myModel);
	}

	private void notifyAdmin(String subject, String message) {
		// send email notification
		String contactEmail = "developers@advancedemedia.com";
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}