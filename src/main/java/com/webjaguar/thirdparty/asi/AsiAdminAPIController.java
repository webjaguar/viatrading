/* Copyright 2012 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 07.07.2012
 */

package com.webjaguar.thirdparty.asi;

import java.io.FileInputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.Search;
import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.asi.asiAPI.SearchResult;
import com.webjaguar.web.domain.Utilities;

public class AsiAdminAPIController extends MultiActionController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	private Properties site;
	public void setSite(Properties site) { this.site = site; }

	public ModelAndView asiProductsDownload(HttpServletRequest request, HttpServletResponse response, Layout layout) {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		//mark all ASI products as Old Products
		this.webJaguar.markAndInactiveOldProducts("ASI", null, false);
    	
		AsiAPI asiProduct = new AsiAPI(siteConfig, getServletContext().getRealPath("temp"), "xml");
		
		/*
		 * Example: asiProductDownload.jhtm?suppIds=xxxx,xxxx,xxxx ---- Iterates through this common separated ids. 
		 * If suppIds is empty then gets Ids form Site Properties. 
		 * If we are adding new supplier Ids to a new list other than the existing list then make sure to activate the products of the existing list after the feed is completed.
		 */
		String[] supplierAccNumbers = null;
		Supplier supp = new Supplier();
		List<Supplier> supplierList = new ArrayList<Supplier>();
		Set<Integer> supplierAccNumberSet = new HashSet<Integer>();
		
		try {
			if(request.getParameter("suppIds") != null) {
				supplierAccNumbers = ((String) request.getParameter("suppIds")).split(",");
			} else {
				supplierAccNumbers = ((String) site.getProperty("asi.suppliers")).split(",");
			}
		} catch ( Exception e ) { }
		
		if(supplierAccNumbers != null) {
			for(String supplierId : supplierAccNumbers){
				supplierAccNumberSet.add(Integer.parseInt(supplierId));
			}
			for(Integer suppId: supplierAccNumberSet) {
				supp = this.webJaguar.getSupplierByAccountNumber(suppId.toString());
				if(supp != null) {
					supplierList.add(supp);
				}
			}
		}
		
		if(supplierList.isEmpty()) {
			supplierList= this.webJaguar.getSuppliers(new Search());
		}
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		HashMap<String, Object> map = null;
		String asiProductXml = null;
		Product prod = null;
		StringBuffer queryString = null;
		SearchResult searchResult = null;
		/*
		 * USE THIS TO GET PRODUCT DETAIL IN SEARCH RESULTS ONLY
		 * LINE # 78 to 84, 105 and 129 to 141
		 *
		 
		JAXBContext context = null;
		try {
			context = JAXBContext.newInstance(com.webjaguar.thirdparty.asi.asiAPI.Product.class);
		} catch (JAXBException e) { e.printStackTrace(); }
		StringBuilder  stringBuilder = null;
		File file = null;
		BufferedReader reader = null;
	    */
		for(Supplier supplier : supplierList){
    		queryString = new StringBuffer();
    		try {
				queryString.append("q=asi:"+supplier.getAccountNumber());
			} catch (Exception e) { e.printStackTrace(); }
    		
			int productsCount = asiProduct.getAsiProductsCount(queryString.toString());
    		int resultsPerPage = 50;
    		
    		int pages = productsCount%resultsPerPage > 0 ? (productsCount/resultsPerPage + 1) : productsCount/resultsPerPage;
    		for(int i=1; i<= pages; i++){
    			queryString = new StringBuffer();
    			try {
    				queryString.append("q="+URLEncoder.encode("asi:"+supplier.getAccountNumber(), "UTF-8"));
    			} catch (Exception e) { e.printStackTrace(); }
        		queryString.append("&page="+i);
        		queryString.append("&rpp="+resultsPerPage);
        		//queryString.append("&include_detail=1");
        		searchResult = asiProduct.getAsiProducts(queryString.toString());
    			
        		if(searchResult != null && searchResult.getResults() != null){
    				for(com.webjaguar.thirdparty.asi.asiAPI.Product product : searchResult.getResults().getProduct()) {
    					
    					prod = new Product();
    					
    					/*
    					 * USE THIS TO GET PRODUCT DETAIL ONE BY ONE
    					 * LINE # 116 to 123
    					 */
    					asiProductXml = asiProduct.getAsiProductXml(product.getId().intValue());
    					if(asiProductXml == null || asiProductXml.trim().isEmpty()) {
        	        		continue;
    					}
    					this.webJaguar.getProductFromAsiXml(asiProductXml, prod);
    					if(prod.getSku() == null || prod.getSku().trim().isEmpty()) {
    	        			continue;
    	        		}
        				
    					/*
    					 * USE THIS TO GET PRODUCT DETAIL IN SEARCH RESULTS ONLY
    					 *
    					 
    					try {
    						file = new File("asiProduct");
    						context.createMarshaller().marshal(product, file);
    						reader = new BufferedReader( new FileReader (file));
    					    String	line = null;
    					    stringBuilder = new StringBuilder();
    					    while( ( line = reader.readLine() ) != null ) {
    					        stringBuilder.append( line );
    					    }
    					} catch (Exception e) { 
    						//e.printStackTrace(); 
    					}
    					this.webJaguar.getProductFromAsiXml(stringBuilder.toString(), prod);
    					*/
    					
    					map = new HashMap<String, Object>();
    					map.put("name", prod.getName());
    					map.put("sku", prod.getSku());
    					map.put("short_desc", prod.getShortDesc());
    					map.put("long_desc", prod.getLongDesc());
    					map.put("keywords", prod.getKeywords());
    					map.put("price_1", prod.getPrice1());
    					map.put("price_2", prod.getPrice2());
    					map.put("price_3", prod.getPrice3());
    					map.put("price_4", prod.getPrice4());
    					map.put("price_5", prod.getPrice5());
    					map.put("price_6", prod.getPrice6());
    					map.put("price_7", prod.getPrice7());
    					map.put("price_8", prod.getPrice8());
    					map.put("price_9", prod.getPrice9());
    					map.put("price_10", prod.getPrice10());
    					map.put("minimum_qty", prod.getMinimumQty());
    					map.put("qty_break_1", prod.getQtyBreak1());
    					map.put("qty_break_2", prod.getQtyBreak2());
    					map.put("qty_break_3", prod.getQtyBreak3());
    					map.put("qty_break_4", prod.getQtyBreak4());
    					map.put("qty_break_5", prod.getQtyBreak5());
    					map.put("qty_break_6", prod.getQtyBreak6());
    					map.put("qty_break_7", prod.getQtyBreak7());
    					map.put("qty_break_8", prod.getQtyBreak8());
    					map.put("qty_break_9", prod.getQtyBreak9());
    					map.put("cost_1", prod.getCost1());
    		    		map.put("default_supplier_id", supplier.getId());
    					
    		    		if(prod.getImages() != null){
    		    			for(int j=1; j<=prod.getImages().size(); j++){
        		    			map.put("image"+j, prod.getImages().get(j-1).getImageUrl());
        	    			}
    		    		}
    		    		map.put("field_34", prod.getField34());
    					map.put("field_35", prod.getField35());
        		    	map.put("field_36", prod.getField36());
        		    	map.put("field_37", prod.getField37());
    		    		map.put("field_38", prod.getField38());
    					map.put("field_39", prod.getField39());
    					map.put("field_40", prod.getField40());
    					map.put("field_41", prod.getField41());
    		    		map.put("field_42", prod.getField42());
    			    	map.put("field_43", prod.getField43());
    			    	map.put("field_44", prod.getField44());
    			    	Integer existingProductId = this.webJaguar.getProductIdBySku(prod.getSku());
    					if(existingProductId == null) {
    			    		map.put("field_45", "Added - " + dateFormatter.format(new Date()));
        		    	} else {
        					map.put("field_45", "Modified - " + dateFormatter.format(new Date()));
        		    	}
    			    	map.put("field_46", prod.getField46());
    			    	map.put("field_47", prod.getField47());
    				    map.put("field_48", prod.getField48());
    				    map.put("field_49", prod.getField49());
    				    map.put("field_51", prod.getField51());
    				    map.put("field_52", prod.getField52());
    				    map.put("field_53", prod.getField53());
     				    
    				    map.put("product_layout", prod.getProductLayout());
    					map.put("compare", 1);
    		    		
    				    map.put("feed", "ASI");				
    					map.put("feed_new", true);
    					map.put("asi_id", prod.getAsiId());
    			    	map.put("asi_unique_id", prod.getAsiUniqueId());
    			    	map.put("asi_xml", prod.getAsiXML());
    					data.add(map);
    		    	   
    		    		if (data.size() == 100) {
    						// products
    						updateProduct(data);
    						data.clear();
    						map.clear();
    					}
    		    		// this is required to prevent memory issue.
    					prod = null;
    		    	}
    			}
    		}
    	}
		if (data.size() > 0) {
			// products
			updateProduct(data);
			data.clear();
			map.clear();
		}
        //mark all old products that are not part of feed as inactive
    	this.webJaguar.markAndInactiveOldProducts("ASI", null, true);
    	Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		notifyAdmin("asiSuppliersDownload() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
		return new ModelAndView(new RedirectView("../catalog/productList.jhtm"));

    }
    
    private void updateProduct(List<Map <String, Object>> data) {
    	this.webJaguar.nonTransactionSafeUpdateASIFileFeedProduct(getCsvFeedList(), data);
    }
    
    public ModelAndView asiSuppliersDownload(HttpServletRequest request, HttpServletResponse response, Layout layout) {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		AsiAPI asiProduct = new AsiAPI(siteConfig, getServletContext().getRealPath("temp"), "xml");
		
		
		//append list of suppliers id
		//remove this code when ASI fix it.
		StringBuffer queryTerm = new StringBuffer();
		Properties prop = new Properties();
		String[] supplierAccNumbers = null;
		Set<Integer> supplierAccNumberSet = new HashSet<Integer>();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			queryTerm.append("asi:"+(String) prop.get( "asi.suppliers" ));
			supplierAccNumbers = ((String) prop.get( "asi.suppliers" )).split(",");
		} catch ( Exception e ) { }
		
		if(supplierAccNumbers != null){
			for(String supplierId : supplierAccNumbers){
				supplierAccNumberSet.add(Integer.parseInt(supplierId));
			}
		}
		// query should be URL encoded.
		StringBuffer queryString = new StringBuffer();
		try {
			queryString.append(URLEncoder.encode(queryTerm.toString(), "UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		Utilities.replaceString(queryString, "+", "%20", false);
		
		int rpp = 100;
		int count = asiProduct.getAsiSuppliersCount(queryString.toString());
		
		
		int pages = count%rpp > 0 ? (count/rpp + 1) : count/rpp;
		for(int i=1; i<= pages; i++){
			queryString = new StringBuffer();
			try {
				queryString.append(URLEncoder.encode(queryTerm.toString(), "UTF-8"));
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			Utilities.replaceString(queryString, "+", "%20", false);
			
			
			queryString.append("&page="+i+"&rpp="+rpp);
			SearchResult searchResult = asiProduct.getAsiSuppliers(queryString.toString());
			if(searchResult != null && searchResult.getResults() != null){
				for(com.webjaguar.thirdparty.asi.asiAPI.Supplier supplier : searchResult.getResults().getSupplier()) {
					if(supplierAccNumberSet.isEmpty() || supplierAccNumberSet.contains(Integer.parseInt(supplier.getAsiNumber()))){
						if(webJaguar.getSupplierByAccountNumber(supplier.getAsiNumber()) == null){
							Supplier sup =  asiProduct.getAsiSupplier(supplier.getId().intValue());
							webJaguar.insertSupplier(sup);
						}
					}
				}
			}
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		
		notifyAdmin("asiSuppliersDownload() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
		return new ModelAndView(new RedirectView("../supplier/supplierList.jhtm"));

    }

    private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
		//String contactEmail = "jwalant@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
    
    private List<CsvFeed> getCsvFeedList() {
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		csvFeedList.add(new CsvFeed("name"));
		csvFeedList.add(new CsvFeed("sku"));
		csvFeedList.add(new CsvFeed("short_desc"));
		csvFeedList.add(new CsvFeed("long_desc"));
		csvFeedList.add(new CsvFeed("keywords"));
		csvFeedList.add(new CsvFeed("price_1"));
		csvFeedList.add(new CsvFeed("price_2"));
		csvFeedList.add(new CsvFeed("price_3"));
		csvFeedList.add(new CsvFeed("price_4"));
		csvFeedList.add(new CsvFeed("price_5"));
		csvFeedList.add(new CsvFeed("price_6"));
		csvFeedList.add(new CsvFeed("price_7"));
		csvFeedList.add(new CsvFeed("price_8"));
		csvFeedList.add(new CsvFeed("price_9"));
		csvFeedList.add(new CsvFeed("price_10"));
		csvFeedList.add(new CsvFeed("qty_break_1"));
		csvFeedList.add(new CsvFeed("qty_break_2"));
		csvFeedList.add(new CsvFeed("qty_break_3"));
		csvFeedList.add(new CsvFeed("qty_break_4"));
		csvFeedList.add(new CsvFeed("qty_break_5"));
		csvFeedList.add(new CsvFeed("qty_break_6"));
		csvFeedList.add(new CsvFeed("qty_break_7"));
		csvFeedList.add(new CsvFeed("qty_break_8"));
		csvFeedList.add(new CsvFeed("qty_break_9"));
		
		csvFeedList.add(new CsvFeed("cost_1"));
		csvFeedList.add(new CsvFeed("default_supplier_id"));
		
		//Supplier ASI
		csvFeedList.add(new CsvFeed("field_34"));
		//Theme
		csvFeedList.add(new CsvFeed("field_35"));
		//Origin
		csvFeedList.add(new CsvFeed("field_36"));
		//Material
		csvFeedList.add(new CsvFeed("field_37"));
		//Name
		csvFeedList.add(new CsvFeed("field_38"));
		//RushService
		csvFeedList.add(new CsvFeed("field_39"));
		//UniqueId
		csvFeedList.add(new CsvFeed("field_40"));
		//Product Color
		csvFeedList.add(new CsvFeed("field_41"));
		//Number
		csvFeedList.add(new CsvFeed("field_42"));
		//Supplier Name
		csvFeedList.add(new CsvFeed("field_43"));
		//TradeName Name
		csvFeedList.add(new CsvFeed("field_44"));
		//Diff State
		csvFeedList.add(new CsvFeed("field_45"));
		//1 day Rush Service
		csvFeedList.add(new CsvFeed("field_46"));
		// category1
		csvFeedList.add(new CsvFeed("field_47"));
		// category2
		csvFeedList.add(new CsvFeed("field_48"));
		// category3
		csvFeedList.add(new CsvFeed("field_49"));
		// shape
		csvFeedList.add(new CsvFeed("field_51"));
		// size
		csvFeedList.add(new CsvFeed("field_52"));
		// weight
		csvFeedList.add(new CsvFeed("field_53"));
		
		csvFeedList.add(new CsvFeed("product_layout"));
		csvFeedList.add(new CsvFeed("compare"));
		csvFeedList.add(new CsvFeed("minimum_qty"));
		csvFeedList.add(new CsvFeed("feed"));
		csvFeedList.add(new CsvFeed("feed_new"));
		csvFeedList.add(new CsvFeed("asi_id"));
		csvFeedList.add(new CsvFeed("asi_unique_id"));
		csvFeedList.add(new CsvFeed("asi_xml"));
		
		return csvFeedList;
	}
}