/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 08.20.2009
 */

package com.webjaguar.thirdparty.asi;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.axis.message.MessageElement;
import org.datacontract.schemas._2004._07.ASI_AsiData.AsiResponseOfXmlElement;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.tempuri.ProductServiceLocator;
import org.tempuri.SupplierServiceLocator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.asicentral.content_bridge._1_0.products.IProductService;
import com.asicentral.content_bridge._1_0.suppliers.ISupplierService;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Product;
import com.webjaguar.model.Supplier;

// NOT IN USE
/* TO BE REMOVE
public class AsiManualController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	Map<String, Configuration> siteConfig;
	// productToken,SupplierToken,[noname/name/summary],[allProduct]
	String[] config;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		siteConfig = this.webJaguar.getSiteConfig();
		config = siteConfig.get("ASI").getValue().split(",");
		
		
		boolean parseSupplier = ServletRequestUtils.getBooleanParameter(request, "parse", false);
		if (parseSupplier) {
			Map<String, Supplier> suppliers = getSuppliers();
			parseCategories(suppliers);
		}
		if (ServletRequestUtils.getBooleanParameter(request, "product", false)) {
			getProduct();	
		}

		return new ModelAndView("admin/vendors/index", map);
	}
	
	private Map<String, Supplier> getSuppliers() throws Exception {
	   
	    SupplierServiceLocator service = new SupplierServiceLocator();
	    Map<String, Supplier> suppliers = new HashMap <String, Supplier>();

	    ISupplierService port;
		
		port = service.getBasicHttpBinding_ISupplierService();

		// listByToken(token, start, count) 
		AsiResponseOfXmlElement supplierResponse = port.listByToken(config[1], 0, 300);

		for ( MessageElement re : supplierResponse.getData().get_any()) {
			
			Document doc = re.getAsDocument();
        	NodeList supplierList = doc.getElementsByTagName("Supplier");
        	for (int i=0; i<supplierList.getLength(); i++) {
                // Get element
                Element element = (Element)supplierList.item(i);

                NodeList supplierNodeList = element.getChildNodes();

				Supplier supplier = new Supplier();
				Address supplierAddress = new Address();
				StringBuffer note = new StringBuffer();
                for (int j=0; j<supplierNodeList.getLength(); j++) {
    				Element supplierElement = (Element)supplierNodeList.item(j);
    				
    				try {
    					if ( supplierElement.getTagName().equalsIgnoreCase("CompanyName")) {
        					supplierAddress.setCompany(supplierElement.getFirstChild().getNodeValue());
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("MainAddress1")) {
        					supplierAddress.setAddr1(supplierElement.getFirstChild().getNodeValue());
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("MainCity")) {
        					supplierAddress.setCity(supplierElement.getFirstChild().getNodeValue());
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("MainState")) {
        					supplierAddress.setStateProvince(supplierElement.getFirstChild().getNodeValue());
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("MainPostalCode")) {
        					supplierAddress.setZip(supplierElement.getFirstChild().getNodeValue());
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("MainCountry")) {
        					supplierAddress.setCountry(supplierElement.getFirstChild().getNodeValue());
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("Phone")) {
        					supplierAddress.setPhone(((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("Fax")) {
        					supplierAddress.setFax(((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("OfficeHours")) {
        					note.append("OfficeHours:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("WebAddress")) {
        					note.append("\n\nWebsite:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("TollFree")) {
        					note.append("\n\nTollFree:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("ArtEmail")) {
        					note.append("\n\nArtEmail:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("MarketingPolicy")) {
        					note.append("\n\nMarketingPolicy:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
        				}
        				if ( supplierElement.getTagName().equalsIgnoreCase("SupplierRep")) {
        					note.append("\n\nSupplierRep:\n" + ((supplierElement.getFirstChild().getNodeValue()==null) ? "" : supplierElement.getFirstChild().getNodeValue() ));
        				}
        				
        				if ( supplierElement.getTagName().equalsIgnoreCase("ASI")) {
        					supplier.setAccountNumber(supplierElement.getFirstChild().getNodeValue());
        				}
    				} catch (NullPointerException e) {
    				}
    				
                }
                supplier.setNote(note.toString());
                supplier.setAddress(supplierAddress);
				suppliers.put(supplier.getAccountNumber(), supplier);
				if (this.webJaguar.getSupplierIdByCompany(supplier.getAddress().getCompany()) != null) {
					//this.webJaguar.updateSupplier(supplier);
				} else {
					this.webJaguar.insertSupplier(supplier);
				}
        	}
		}
		
		return suppliers;
	}

    				
	private int parseCategories(Map<String, Supplier> suppliers) throws Exception {
	    int count = 0;
	   
	    ProductServiceLocator service = new ProductServiceLocator();
	    List<AsiCategory> categories = new ArrayList<AsiCategory>();

	    IProductService port;
		
		//port = service.getWSHttpBinding_IProductService();
		port = service.getBasicHttpBinding_IProductService();
		int productListSize;
		int index = -1;
		do {
			productListSize = 0;
			// listByToken(token, start, count) 
			// Number of records to return can not exceed maximum number of records per page (current maximum is 500)
			AsiResponseOfXmlElement productResponse = port.listByToken(config[0], (++index)*500, 500);
			if (productResponse.getData() != null) {
				for ( MessageElement respond : productResponse.getData().get_any()) {
					
					Document doc = respond.getAsDocument();
		        	NodeList productList = doc.getElementsByTagName("Product");
		        	productListSize = productList.getLength();
		        	for (int i=0; i<productList.getLength(); i++) {
		                // Get element
		                Element element = (Element)productList.item(i);
		
		                NodeList product = element.getChildNodes();
						String categoryName = "";
						String asi = "";
		                for (int j=0; j<product.getLength(); j++) {
		                	
		    				Element productElement = (Element)product.item(j);
		    				
		    				if ( productElement.getTagName().equalsIgnoreCase("Category")) {
		    					categoryName = productElement.getFirstChild().getNodeValue();
		    				}
		    				
		    				if ( productElement.getTagName().equalsIgnoreCase("ASI")) {
		    					asi = productElement.getFirstChild().getNodeValue();
		    					String productCategories[] = categoryName.split("[;]");
		    					for (int numCat = 0; numCat < productCategories.length; numCat++) {
		    						String catTree[] = productCategories[numCat].split("[-]");
		    						
		    						if (productCategories[numCat].contains("-")){
				    							
	    								AsiCategory category = new AsiCategory();
	    								category.setAsi(asi);
	    								
	    								if (productCategories[numCat].trim().toLowerCase().contains("BOXES & CASES-PENCIL".toLowerCase())) {
	    									category.setCat("BOXES & CASES-PENCIL");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("BROOMS-MOPS & VACUUMS".toLowerCase())) {
	    									category.setCat("BROOMS-MOPS & VACUUMS");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("CLIPS-UTILITY".toLowerCase())) {
	    									category.setCat("CLIPS-UTILITY");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								}
	    								
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("COINS-TOKENS & MEDALLIONS".toLowerCase())) {
	    									category.setCat("COINS-TOKENS & MEDALLIONS");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("DISPENSERS & CASES-TOOTHPICK".toLowerCase())) {
	    									category.setCat("DISPENSERS & CASES-TOOTHPICK");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("EGGS-PLASTIC".toLowerCase())) {
	    									category.setCat("EGGS-PLASTIC");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("GLASSES-DRINKING".toLowerCase())) {
	    									category.setCat("GLASSES-DRINKING");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("MAKE-UP/COSMETICS".toLowerCase())) {
	    									category.setCat("MAKE-UP/COSMETICS");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("MATCH-FOLDER SPECIALTIES".toLowerCase())) {
	    									category.setCat("MATCH-FOLDER SPECIALTIES");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("POCKET SECRETARIES-COMBINATION".toLowerCase())) {
	    									category.setCat("POCKET SECRETARIES-COMBINATION");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("POCKET SECRETARIES-PLAIN".toLowerCase())) {
	    									category.setCat("POCKET SECRETARIES-PLAIN");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("STIRRERS & STICKS-DRINK".toLowerCase())) {
	    									category.setCat("STIRRERS & STICKS-DRINK");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("T-SHIRTS".toLowerCase())) {
	    									category.setCat("T-SHIRTS");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("TOOLS-HARDWARE".toLowerCase())) {
	    									category.setCat("TOOLS-HARDWARE");
	    								try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("TOOLS-KITCHEN".toLowerCase())) {
	    									category.setCat("TOOLS-KITCHEN");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								}
	    								
	    								
	    								else if (productCategories[numCat].trim().toLowerCase().contains("WRITE ON-WIPE OFF BOARDS".toLowerCase())) {
	    									category.setCat("WRITE ON-WIPE OFF BOARDS");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								}
	    								

	    								else if (productCategories[numCat].trim().toLowerCase().contains("YO-YO'S".toLowerCase())) {
	    									category.setCat("YO-YO'S");
	    									try{
	    										category.setSub(catTree[2].trim());
	    									}
	    									catch (ArrayIndexOutOfBoundsException e ){
	    										category.setSub("*");
	    									}
	    								}
	    								
	    								else {
	    									category.setCat(catTree[0].trim());
	    									String sub = productCategories[numCat].substring(catTree[0].length()+1, productCategories[numCat].length());
	    									if (sub.toString().isEmpty()){
	    										category.setSub("*");
	    									} else{
	    										category.setSub(sub);
	    									}
	    								}
	    								category.setProductCategory(productCategories[numCat].trim());
	    								
	    								category.setSupplier(suppliers.get(asi).getAddress().getCompany());
	    								if (!category.containsCategory(categories)) {
	    									categories.add(category);
		    							}
	    								
	    								AsiCategory categoryMain = new AsiCategory();
		    							categoryMain.setCat(category.getCat().trim());
		    							categoryMain.setSub("*");
		    							categoryMain.setAsi(asi);
		    							categoryMain.setSupplier(suppliers.get(asi).getAddress().getCompany().trim());
		    							if (!categoryMain.containsCategory(categories)) {
		    								categories.add(categoryMain);
		    							}
		    							
	    								count++;
		    						} else {
		    							// no DASH categories
		    							AsiCategory categoryMain = new AsiCategory();
		    							categoryMain.setCat("*");
		    							categoryMain.setSub("*");
		    							categoryMain.setAsi(asi);
		    							categoryMain.setSupplier(suppliers.get(asi).getAddress().getCompany().trim());
		    							categoryMain.setProductCategory(productCategories[numCat].trim());
		    							if (!categoryMain.containsCategory(categories)) {
		    								categories.add(categoryMain);
		    							}
										AsiCategory category = new AsiCategory();	
										category.setCat(productCategories[numCat].trim());
										category.setSub("*");
										category.setAsi(asi);
										category.setSupplier(suppliers.get(asi).getAddress().getCompany().trim());
										category.setProductCategory(productCategories[numCat].trim());
										if (!category.containsCategory(categories)) {
											categories.add(category);
		    							}
										count++;
		    						}
		    					}
		    				}// if
		                } 
		        	}

					if (categories.size() >= 0 ) {
						this.webJaguar.nonTransactionSafeInsertAsiCategories(categories);
						
						categories.clear();
					}
				}
			}
		} while (productListSize >= 1);
	    return count;
	}	

	
	public int getProduct() {
		int quantity = 0;
		if (config.length < 1 || config[0].equals("")) {
			// return if ASI feature is disabled 
			System.out.println("return if ASI feature is disabled ");
			return 0;
		}				
		System.out.println("ASI Get Product start (manual): " + new Date() + " " + siteConfig.get("SITE_URL").getValue());
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();

		csvFeedList.add(new CsvFeed("sku"));
		csvFeedList.add(new CsvFeed("active"));
		if (config[2].equals("")) {
			//csvFeedList.add(new CsvFeed("name"));
		} else {
			csvFeedList.add(new CsvFeed("name"));
		}
		csvFeedList.add(new CsvFeed("feed"));
		csvFeedList.add(new CsvFeed("feed_new"));
		csvFeedList.add(new CsvFeed("long_desc"));
		csvFeedList.add(new CsvFeed("weight"));
		csvFeedList.add(new CsvFeed("minimum_qty"));
		csvFeedList.add(new CsvFeed("note"));
		csvFeedList.add(new CsvFeed("keywords"));
		csvFeedList.add(new CsvFeed("asi_id"));
		csvFeedList.add(new CsvFeed("option_code"));
		csvFeedList.add(new CsvFeed("add_to_list"));
		
		csvFeedList.add(new CsvFeed("price_1"));
		csvFeedList.add(new CsvFeed("price_2"));
		csvFeedList.add(new CsvFeed("price_3"));
		csvFeedList.add(new CsvFeed("price_4"));
		csvFeedList.add(new CsvFeed("price_5"));
		csvFeedList.add(new CsvFeed("qty_break_1"));
		csvFeedList.add(new CsvFeed("qty_break_2"));
		csvFeedList.add(new CsvFeed("qty_break_3"));
		csvFeedList.add(new CsvFeed("qty_break_4"));
		csvFeedList.add(new CsvFeed("qty_break_5"));
		
		csvFeedList.add(new CsvFeed("field_1"));
		csvFeedList.add(new CsvFeed("field_2"));
		csvFeedList.add(new CsvFeed("field_3"));
		csvFeedList.add(new CsvFeed("field_4"));
		csvFeedList.add(new CsvFeed("field_5"));
		csvFeedList.add(new CsvFeed("field_6"));
		csvFeedList.add(new CsvFeed("field_7"));
		csvFeedList.add(new CsvFeed("field_8"));
		csvFeedList.add(new CsvFeed("field_9"));
		csvFeedList.add(new CsvFeed("field_10"));
		
		csvFeedList.add(new CsvFeed("field_11"));
		csvFeedList.add(new CsvFeed("field_12"));
		csvFeedList.add(new CsvFeed("field_13"));
		csvFeedList.add(new CsvFeed("field_14"));
		csvFeedList.add(new CsvFeed("field_15"));
		csvFeedList.add(new CsvFeed("field_16"));
		csvFeedList.add(new CsvFeed("field_17"));
		csvFeedList.add(new CsvFeed("field_18"));
		csvFeedList.add(new CsvFeed("field_19"));
		csvFeedList.add(new CsvFeed("field_20"));
		
		csvFeedList.add(new CsvFeed("field_21"));
		csvFeedList.add(new CsvFeed("field_21"));
		csvFeedList.add(new CsvFeed("field_22"));
		csvFeedList.add(new CsvFeed("field_23"));
		csvFeedList.add(new CsvFeed("field_24"));
		csvFeedList.add(new CsvFeed("field_25"));
		csvFeedList.add(new CsvFeed("field_26"));
		csvFeedList.add(new CsvFeed("field_27"));
		csvFeedList.add(new CsvFeed("field_28"));
		csvFeedList.add(new CsvFeed("field_29"));
		csvFeedList.add(new CsvFeed("field_30"));
		
		csvFeedList.add(new CsvFeed("field_31"));
		csvFeedList.add(new CsvFeed("field_32"));
		csvFeedList.add(new CsvFeed("field_33"));
		csvFeedList.add(new CsvFeed("field_34"));
		csvFeedList.add(new CsvFeed("field_35"));
		csvFeedList.add(new CsvFeed("field_36"));
		csvFeedList.add(new CsvFeed("field_37"));
		csvFeedList.add(new CsvFeed("field_38"));
		csvFeedList.add(new CsvFeed("field_39"));
		csvFeedList.add(new CsvFeed("field_40"));
		csvFeedList.add(new CsvFeed("field_41"));
		csvFeedList.add(new CsvFeed("field_42"));
		
		// cat 1
		csvFeedList.add(new CsvFeed("field_45"));
		// sub 1
		csvFeedList.add(new CsvFeed("field_46"));
		// cat 2
		csvFeedList.add(new CsvFeed("field_47"));
		// sub 2
		csvFeedList.add(new CsvFeed("field_48"));
		// cat 3
		csvFeedList.add(new CsvFeed("field_49"));
		// sub 3
		csvFeedList.add(new CsvFeed("field_50"));
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		ProductServiceLocator service = new ProductServiceLocator();
			
//		boolean hasCatId = false;
		Map<String, AsiCategory> categoryMap = this.webJaguar.getAsiCategoryMap();
//        for (AsiCategory category: categoryMap.values()) {
//        	if (category.getCatId() != null) {
//        		hasCatId = true;
//        		break;
//        	}
//        }
//			
//		if (hasCatId) {
			parseProduct(service, csvFeedList, data, categoryMap);
		//}
		System.out.println("ASI Get Product end (manual): " + new Date() + " " + siteConfig.get("SITE_URL").getValue());	
		return quantity;
	}
	
	private void parseProduct(ProductServiceLocator service, List<CsvFeed> csvFeedList, List<Map <String, Object>> data, Map<String, AsiCategory> categoryMap) {
		List<Map <String, Object>> categoryData = new ArrayList<Map <String, Object>>();
		try {
			IProductService port;
			
			//port = service.getWSHttpBinding_IProductService();
			port = service.getBasicHttpBinding_IProductService();
	
			int productListSize = 0;
			int index = -1;
			do {
				//System.out.println("index= " + index+1 + " productListSize= " + productListSize);
				productListSize = 0;
				// listByToken(token, start, count) 
				// Number of records to return can not exceed maximum number of records per page (current maximum is 500)
				AsiResponseOfXmlElement productResponse = port.listByToken(config[0], (++index)*500, 500);
				if (productResponse != null && productResponse.getData() != null && productResponse.getData().get_any() != null) {
					for ( MessageElement re : productResponse.getData().get_any()) {
						
			        	Document doc = re.getAsDocument();
			        	NodeList productList = doc.getElementsByTagName("Product");
			        	productListSize = productList.getLength();
			        	
			        	for (int i=0; i<productList.getLength(); i++) {
			                // Get element
			                Element element = (Element)productList.item(i);
			                NodeList product = element.getChildNodes();
			
							HashMap<String, Object> map = new HashMap<String, Object>();
							List<Integer> catIds = new ArrayList<Integer>();
							
							boolean active = false;
							String ProductName = "";
							StringBuffer keyword = new StringBuffer();
							StringBuffer xmlString = new StringBuffer();
			                for (int j=0; j<product.getLength(); j++) {
			                	
			    				Element productElement = (Element)product.item(j);
			    				
			    				Double priceChange = null;
			    				
			    				// categories
			    				if ( productElement.getTagName().equalsIgnoreCase("Category")) {
			    					xmlString.append("<Category>" +productElement.getFirstChild().getNodeValue()+ "</Category>\n");
			    					keyword.append("," + productElement.getFirstChild().getNodeValue().replace(';', ',').replace(" ", ","));
				    				String categories[] = productElement.getFirstChild().getNodeValue().split("[;]");
				    				for (int numCat = 0; numCat < categories.length; numCat++) {
				    					
//				    					if (categoryMap.containsKey("*-*")) {
//					    					// all cat-sub of supplier
//					    					AsiCategory category = categoryMap.get("*-*");
//					    					if (category.getCatId() != null) catIds.add(category.getCatId());
//					    					if (category.getPriceChange() != null) priceChange = category.getPriceChange();
//					    					supplier = category.getSupplier();
//					    				}
				    					if (categoryMap.containsKey(categories[numCat].replaceAll(" ", "").toLowerCase() + "-*")) {
					    					// cat
					    					AsiCategory category = categoryMap.get(categories[numCat].replaceAll(" ", "").toLowerCase() + "-*");
					    					if (category.getCatId() != null) catIds.add(category.getCatId());
					    					if (category.getPriceChange() != null) priceChange = category.getPriceChange();
					    					if (numCat==0) {
					    						map.put("field_45", category.getCat());
					    					} else if (numCat==1) {
						    					map.put("field_47", category.getCat());
					    					} else if (numCat==2) {
					    						map.put("field_49", category.getCat() );
					    					}
					    					keyword.append("," + category.getCat().replace(" ", ","));
					    				}
					    				if (categoryMap.containsKey(categories[numCat].replaceAll(" ", "").toLowerCase())) {
					    					// sub
					    					AsiCategory category = categoryMap.get(categories[numCat].replaceAll(" ", "").toLowerCase());
					    					if (category.getCatId() != null) catIds.add(category.getCatId());
					    					if (category.getPriceChange() != null) priceChange = category.getPriceChange();
					    					if (numCat==0) {
					    						map.put("field_45", category.getCat());
						    					map.put("field_46", category.getSub());
					    					} else if (numCat==1) {
					    						map.put("field_47", category.getCat() );
						    					map.put("field_48", category.getSub() );
					    					} else if (numCat==2) {
					    						map.put("field_49", category.getCat() );
						    					map.put("field_50", category.getSub() );
					    					}
						    				
					    					keyword.append("," + category.getCat().replace(" ", ",") + "," + category.getSub().replace(" ", ","));
					    				}
				    				}
				    				
				    				if (catIds.isEmpty() && config[3].equals("onlyMappedProduct")) {
				    					// skip this item
				    					continue;
				    				}
			    				}
			    				
			    				
			    				// map asi product
			    				if ( productElement.getTagName().equalsIgnoreCase("UniqueId")) {
			    					String sku = productElement.getFirstChild().getNodeValue();	
			    					map.put("field_40", sku);
			    					xmlString.append("<UniqueId>" +sku+ "</UniqueId>\n");
			    					Integer newSku = Integer.rotateLeft(Integer.parseInt(sku), 2);
			    					map.put("sku", newSku.toString());
			    					xmlString.append("<sku>" +newSku+ "</sku>\n");
			    					keyword.append("," + newSku.toString());
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("LineName")) {
			    					if (config[2].equals("name")) {
			    						ProductName = ProductName + productElement.getFirstChild().getNodeValue();	
				    					map.put("name", ProductName);
			    					}
			    					xmlString.append("<LineName>" +productElement.getFirstChild().getNodeValue()+ "</LineName>\n");
			    					map.put("field_42", productElement.getFirstChild().getNodeValue());
			    					//keyword.append("," + ProductName);
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("ProdNum")) {
			    					if (config[2].equals("name")) {
			    						ProductName = ProductName + " " + productElement.getFirstChild().getNodeValue();	
			    						map.put("name", ProductName);
			    					}
		    						xmlString.append("<ProdNum>" +productElement.getFirstChild().getNodeValue()+ "</ProdNum>\n");
		    						map.put("field_41", productElement.getFirstChild().getNodeValue());
		    						//keyword.append("," + ProductName);
			    				}
			    				// supplier ID
			    				if ( productElement.getTagName().equalsIgnoreCase("ASI")) {
									map.put("asi_id", productElement.getFirstChild().getNodeValue() );
			    				}
			    				map.put("feed", "ASI");
			    				map.put("feed_new", true);
			    				map.put("option_code", "GenericOption");
			    				map.put("add_to_list", true);
			    				
			    				if ( productElement.getTagName().equalsIgnoreCase("AdditionalColor")) {
									map.put("field_1", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("AdditionalLocation")) {
									map.put("field_2", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Artwork")) {
									map.put("field_3", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("CopyChange")) {
									map.put("field_4", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("DropShipment")) {
									map.put("field_5", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("FOB")) {
									map.put("field_6", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("FullColorProcessing")) {
									map.put("field_7", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("GraphicDesign")) {
									map.put("field_8", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("ImprintCharge")) {
									map.put("field_9", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("ImprintColor")) {
									map.put("field_10", productElement.getFirstChild().getNodeValue().replace(";", "; ") );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("ImprintIncluded")) {
									map.put("field_11", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("ImprintMethod")) {
									map.put("field_12", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("ImprintOption")) {
									map.put("field_13", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("ImprintSize")) {
									map.put("field_14", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("LessThanMin")) {
									map.put("field_15", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Material")) {
									map.put("field_16", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Origin")) {
									map.put("field_17", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Packaging")) {
									map.put("field_18", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Personalization")) {
									map.put("field_19", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("PersonalizationFlag")) {
									map.put("field_20", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("PriceIncludes")) {
									map.put("field_21", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("ProductOption")) {
									map.put("field_22", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Size")) {
									map.put("field_23", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("ProductionTime")) {
									map.put("field_24", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("RunningCharge")) {
									map.put("field_25", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("RushTimeDetail")) {
									map.put("field_26", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("RushTime")) {
									map.put("field_27", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Samples")) {
									map.put("field_28", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Shape")) {
									map.put("field_29", productElement.getFirstChild().getNodeValue() );
									keyword.append("," + productElement.getFirstChild().getNodeValue().replace(" ", ","));
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("ShipMisc")) {
									map.put("field_30", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("SoldBlanks")) {
									map.put("field_31", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("SpecSamples")) {
									map.put("field_32", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("SplitDestination")) {
									map.put("field_33", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Summary")) {
									map.put("field_34", productElement.getFirstChild().getNodeValue() );
									if (config[2].equals("summary")) {
			    						map.put("name", productElement.getFirstChild().getNodeValue());
									}
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Theme")) {
									map.put("field_35", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("TradeName")) {
									map.put("field_36", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Warranty")) {
									map.put("field_37", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Warranty")) {
									map.put("field_37", productElement.getFirstChild().getNodeValue() );
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("ShipWeight")) {
			    					map.put("field_38", productElement.getFirstChild().getNodeValue() );
			    					try { 
			    						Double weight = Double.parseDouble( productElement.getFirstChild().getNodeValue());
			    						map.put("weight", (weight == null ? 0.00 : weight));
		    						} catch (NumberFormatException e) { map.put("weight", null);  }
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Color")) {
									map.put("field_39", productElement.getFirstChild().getNodeValue() );
									keyword.append("," + productElement.getFirstChild().getNodeValue().replace(";", ","));
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Description")) {
									map.put("long_desc", productElement.getFirstChild().getNodeValue() );
									//keyword.append("," + productElement.getFirstChild().getNodeValue().replace(" ", ","));
			    				}
			    				
			    				if ( productElement.getTagName().equalsIgnoreCase("Price1")) {
			    					try { 
			    						Double price = Double.parseDouble( productElement.getFirstChild().getNodeValue());
			    						map.put("price_1",  (priceChange == null ? price : price + price * priceChange/100));
			    						xmlString.append("<Price1>" +productElement.getFirstChild().getNodeValue()+ "</Price1>\n");
			    						active = true;
			    					} catch (NumberFormatException e) { map.put("price_1", null); active = false; e.printStackTrace(); }
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Price2")) {
		    						try { 
		    							Double price = Double.parseDouble( productElement.getFirstChild().getNodeValue());
		    							map.put("price_2", (priceChange == null ? price : price + price * priceChange/100));
		    							xmlString.append("<Price2>" +productElement.getFirstChild().getNodeValue()+ "</Price2>\n");
		    						} catch (NumberFormatException e) { map.put("price_2", null); e.printStackTrace(); }
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Price3")) {
			    					try { 
			    						Double price = Double.parseDouble( productElement.getFirstChild().getNodeValue());
			    						map.put("price_3", (priceChange == null ? price : price + price * priceChange/100));
			    						xmlString.append("<Price3>" +productElement.getFirstChild().getNodeValue()+ "</Price3>\n");
		    						} catch (NumberFormatException e) { map.put("price_3", null); e.printStackTrace(); }
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Price4")) {
			    					try { 
			    						Double price = Double.parseDouble( productElement.getFirstChild().getNodeValue());
			    						map.put("price_4", (priceChange == null ? price : price + price * priceChange/100));
			    						xmlString.append("<Price4>" +productElement.getFirstChild().getNodeValue()+ "</Price4>\n");
		    						} catch (NumberFormatException e) { map.put("price_4", null); e.printStackTrace(); }
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Price5")) {
			    					try { 
			    						Double price = Double.parseDouble( productElement.getFirstChild().getNodeValue());
			    						map.put("price_5", (priceChange == null ? price : price + price * priceChange/100));
			    						xmlString.append("<Price5>" +productElement.getFirstChild().getNodeValue()+ "</Price5>\n");
									} catch (NumberFormatException e) { map.put("price_5", null); e.printStackTrace(); }
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Price6")) {
			    					try { 
			    						Double price = Double.parseDouble( productElement.getFirstChild().getNodeValue());
			    						map.put("price_6", (priceChange == null ? price : price + price * priceChange/100));
			    						xmlString.append("<Price6>" +productElement.getFirstChild().getNodeValue()+ "</Price6>\n");
									} catch (NumberFormatException e) { map.put("price_6", null); e.printStackTrace(); }
			    				}
			    				
			    				// Minimum Qty
			    				if ( productElement.getTagName().equalsIgnoreCase("Quantity1")) {
									try { map.put("minimum_qty", ( Integer.parseInt( productElement.getFirstChild().getNodeValue() )));
									xmlString.append("<Quantity1>" +productElement.getFirstChild().getNodeValue()+ "</Quantity1>\n");
									} catch (NumberFormatException e) { map.put("minimum_qty", null); e.printStackTrace(); }
			    				}
			    				// Quantity Break
			    				if ( productElement.getTagName().equalsIgnoreCase("Quantity2")) {
									try { map.put("qty_break_1", ( Integer.parseInt( productElement.getFirstChild().getNodeValue() )));
									xmlString.append("<Quantity2>" +productElement.getFirstChild().getNodeValue()+ "</Quantity2>\n");
									} catch (NumberFormatException e) { map.put("qty_break_1", null); e.printStackTrace(); }
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Quantity3")) {
									try { map.put("qty_break_2", ( Integer.parseInt( productElement.getFirstChild().getNodeValue() )));
									xmlString.append("<Quantity3>" +productElement.getFirstChild().getNodeValue()+ "</Quantity3>\n");
									} catch (NumberFormatException e) { map.put("qty_break_2", null); e.printStackTrace(); }
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Quantity4")) {
									try { map.put("qty_break_3", ( Integer.parseInt( productElement.getFirstChild().getNodeValue() )));
									xmlString.append("<Quantity4>" +productElement.getFirstChild().getNodeValue()+ "</Quantity4>\n");
									} catch (NumberFormatException e) { map.put("qty_break_3", null); e.printStackTrace(); }
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Quantity5")) {
									try { map.put("qty_break_4", ( Integer.parseInt( productElement.getFirstChild().getNodeValue() )));
									xmlString.append("<Quantity5>" +productElement.getFirstChild().getNodeValue()+ "</Quantity5>\n");
									} catch (NumberFormatException e) { map.put("qty_break_4", null); e.printStackTrace(); }
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Quantity6")) {
									try { map.put("qty_break_5", ( Integer.parseInt( productElement.getFirstChild().getNodeValue() )));
									xmlString.append("<Quantity6>" +productElement.getFirstChild().getNodeValue()+ "</Quantity6>\n");
									} catch (NumberFormatException e) { map.put("qty_break_5", null); e.printStackTrace(); }
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Quantity7")) {
									try { map.put("qty_break_6", ( Integer.parseInt( productElement.getFirstChild().getNodeValue() )));
									xmlString.append("<Quantity7>" +productElement.getFirstChild().getNodeValue()+ "</Quantity7>\n");
									} catch (NumberFormatException e) { map.put("qty_break_6", null); e.printStackTrace(); }
			    				}
			    				
			    				if ( productElement.getTagName().equalsIgnoreCase("PriceCode1")) {
									xmlString.append("<PriceCode1>" +productElement.getFirstChild().getNodeValue()+ "</PriceCode1>\n");
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("PriceCode2")) {
									xmlString.append("<PriceCode2>" +productElement.getFirstChild().getNodeValue()+ "</PriceCode2>\n");
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("PriceCode3")) {
									xmlString.append("<PriceCode3>" +productElement.getFirstChild().getNodeValue()+ "</PriceCode3>\n");
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("PriceCode4")) {
									xmlString.append("<PriceCode4>" +productElement.getFirstChild().getNodeValue()+ "</PriceCode4>\n");
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("PriceCode5")) {
									xmlString.append("<PriceCode5>" +productElement.getFirstChild().getNodeValue()+ "</PriceCode5>\n");
			    				}
			    				
			    				if ( productElement.getTagName().equalsIgnoreCase("Cost1")) {
			    					try { 
			    						Double cost1 = Double.parseDouble( productElement.getFirstChild().getNodeValue());
			    						map.put("cost1", (cost1 == null ? 0.0 : cost1));
		    						} catch (NumberFormatException e) { map.put("cost1", 0.0); e.printStackTrace(); }
									xmlString.append("<Cost1>" +productElement.getFirstChild().getNodeValue()+ "</Cost1>\n");
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Cost2")) {
									xmlString.append("<Cost2>" +productElement.getFirstChild().getNodeValue()+ "</Cost2>\n");
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Cost3")) {
									xmlString.append("<Cost3>" +productElement.getFirstChild().getNodeValue()+ "</Cost3>\n");
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Cost4")) {
									xmlString.append("<Cost4>" +productElement.getFirstChild().getNodeValue()+ "</Cost4>\n");
			    				}
			    				if ( productElement.getTagName().equalsIgnoreCase("Cost5")) {
									xmlString.append("<Cost5>" +productElement.getFirstChild().getNodeValue()+ "</Cost5>\n");
			    				}
			    				
			    				if ( productElement.getTagName().equalsIgnoreCase("ImageId")) {
			    					try {
										map.put("image1", "https://www.asicontentbridge.com/1.0/Public/DownloadService.ashx?token=" + productElement.getFirstChild().getNodeValue() + "&command=get_image&type=prodimgs");
									} catch (Exception e) {
										break;
									}
			    				}
			    				if ( map.get("sku") != null) {
			    					map.put("productId", this.webJaguar.getProductIdBySku((String) map.get("sku")));
				    				if (map.get("productId") == null) {
				    					// new item
				    					Product newProduct = new Product((String) map.get("sku"));
				    					this.webJaguar.insertProduct(newProduct);
				    					map.put("productId", newProduct.getId());
				    				}
			    				}
			    				
			    				// create category association
			    				for (Integer catId: catIds) {
			    					HashMap<String, Object> mapCategory = new HashMap<String, Object>();
			    					mapCategory.put("productId", map.get("productId"));
			    					mapCategory.put("categoryId", catId);
			    					categoryData.add(mapCategory);
			    				}
			    				if (j == product.getLength()-1) {
			    					map.put("keywords", keyword.append(",").toString());
				    				map.put("note", xmlString.toString());
			    				}
			    				map.put("active", active);

			    				data.add(map);
			                	
			                }
			                if (data.size() >= 10) {
			                	
								this.webJaguar.nonTransactionSafeUpdateASIProduct(csvFeedList, data);
								
								// associate to categories
								if ( !categoryData.isEmpty() ) {
									this.webJaguar.nonTransactionSafeInsertCategories(categoryData);
								}
								categoryData.clear();
								data.clear();
							}
			                
			            }//MessageElement
					}
				}//productResponse.getData() != null
			} while (productListSize >= 1);			
			// make old asi items inactive
			this.webJaguar.deleteOldProducts("ASI", true);
	
		} catch (Exception e) {
			notifyAdmin("Sync getProduct() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			System.out.println("ASI EXCEPTION");
			e.printStackTrace();
		}
	}

}
*/