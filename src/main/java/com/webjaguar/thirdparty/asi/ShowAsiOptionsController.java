/* Copyright 2005, 2008 Advanced E-Media Solutions
 * author: Jwalant Patel
 */

package com.webjaguar.thirdparty.asi;


import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.thirdparty.asi.asiAPI.Charge;
import com.webjaguar.thirdparty.asi.asiAPI.Price;
import com.webjaguar.thirdparty.asi.asiAPI.Product.Variants.Variant;
import com.webjaguar.thirdparty.asi.asiAPI.ProductAttributeValue;
import com.webjaguar.thirdparty.asi.asiAPI.ProductOption;
import com.webjaguar.thirdparty.asi.asiAPI.ProductOptionSet;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.KeyBean;


public class ShowAsiOptionsController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		if(gSiteConfig.get("gASI") == null || !((String) gSiteConfig.get("gASI")).equals("File") ) {
			return null;
		}
	
		Product product = null;
		if(ServletRequestUtils.getIntParameter(request, "id") != null){
			product = this.webJaguar.getProductById(ServletRequestUtils.getIntParameter(request, "id"), request);
		}
			
		if(product == null || product.getAsiXML() == null) {
			return null;
		}
			
		JAXBContext context = JAXBContext.newInstance(com.webjaguar.thirdparty.asi.asiAPI.Product.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		StringBuffer sb = new StringBuffer(product.getAsiXML());
		ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
		com.webjaguar.thirdparty.asi.asiAPI.Product asiProduct = (com.webjaguar.thirdparty.asi.asiAPI.Product) unmarshaller.unmarshal(bis);
		
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		// set ASI Price with markup, if any
		boolean isMarkUpApplied = this.webJaguar.applyPriceDifference(null, asiProduct, siteConfig.get("ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS").getValue(),  Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_TYPE").getValue()), Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_FORMULA").getValue()));
		// set ASI Price with salesTag, if any
		if((siteConfig.get("ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE").getValue().equals("true") || !isMarkUpApplied) && product.getSalesTag() != null) {
			this.webJaguar.setASIPriceWithSalesTag(null, asiProduct, product.getSalesTag());
		}
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		//myModel.put("asiProduct", asiProduct); 
		myModel.put("product", product);
		 
	
		// Step 1 : Color Selection
		myModel.put("selectedColor", ServletRequestUtils.getStringParameter(request, "color"));
		myModel.put("colors", Constants.COLOR_CODE);
		
		Map<String, String> productColors = new TreeMap<String, String>();
		for(KeyBean colorCode : Constants.COLOR_CODE) {
			if(asiProduct.getAttributes() != null && asiProduct.getAttributes().getColors() != null) {
				if(asiProduct.getAttributes().getColors().getValues() != null) {
					if(asiProduct.getAttributes().getColors().getValues().getProductAttributeValue() != null && asiProduct.getAttributes().getColors().getValues().getProductAttributeValue() != null) {
						for(ProductAttributeValue value : asiProduct.getAttributes().getColors().getValues().getProductAttributeValue()) {
							if(value.getName().equalsIgnoreCase(colorCode.getName().toString())){
								productColors.put(value.getName(), colorCode.getValue().toString());
							}
						}
					}
					
					if(asiProduct.getAttributes().getColors().getValues().getString() != null) {
						for(String value : asiProduct.getAttributes().getColors().getValues().getString()) {
							if(value.equalsIgnoreCase(colorCode.getName().toString())){
								productColors.put(value, colorCode.getValue().toString());
							}
						}
					}
						
				}
			}
		}
		if(!productColors.isEmpty()) {
			myModel.put("productColors", productColors);
		}
		
		int quantity = 0;
		boolean samePriceBreakForAllSizes = true;
		Map<Integer, Double> priceQtyMap = null;
		Map<String, Map<Integer, Double>> variantPriceQtyMap = null;
		boolean minQtyMet = true;
		if(myModel.get("selectedColor") != null || myModel.get("productColors") == null) {
			
			List<String> variantList = null;
			if(asiProduct.getVariants() != null) {
				variantList = new ArrayList<String>();
				variantPriceQtyMap = new HashMap<String, Map<Integer,Double>>();
				for(Variant variant : asiProduct.getVariants().getVariant()){
					variantList.add(variant.getDescription());
					Map<Integer, Double> pqMap = null;
					if(variant.getPrices() != null && variant.getPrices().getPrice() != null) {
						pqMap = new TreeMap<Integer, Double>();
						myModel.put("minOrderQty", 1);
						for(int i=0; i< variant.getPrices().getPrice().size(); i++) {
							if(variant.getPrices().getPrice().get(i).getQuantity() != null && variant.getPrices().getPrice().get(i).getQuantity().getRange() != null) {
								if(i == 0) {
									myModel.put("minOrderQty", variant.getPrices().getPrice().get(i).getQuantity().getRange().getFrom());
								}
								pqMap.put(variant.getPrices().getPrice().get(i).getQuantity().getRange().getTo(), variant.getPrices().getPrice().get(i).getPrice().doubleValue());
							}
						}
						if(ServletRequestUtils.getIntParameter(request, "qty_"+variant.getDescription(), 0) > 0 && (Integer)myModel.get("minOrderQty") > ServletRequestUtils.getIntParameter(request, "qty_"+variant.getDescription(), 1)) {
							minQtyMet = false;
						}
						variantPriceQtyMap.put(variant.getDescription(), pqMap);
					}
				}
				myModel.put("variantList", variantList);
				myModel.put("variantPriceQtyMap", variantPriceQtyMap);
			} else {
				if(asiProduct.getAttributes() != null && asiProduct.getAttributes().getSizes() != null) {
					variantList = new ArrayList<String>();
					variantList.addAll(asiProduct.getAttributes().getSizes().getValues().getString());
				}
				priceQtyMap = new TreeMap<Integer, Double>();
				myModel.put("minOrderQty", 1);
				if(asiProduct.getPrices() != null && asiProduct.getPrices().getPrice() != null) {
					for(int i=0; i< asiProduct.getPrices().getPrice().size(); i++) {
						if(asiProduct.getPrices().getPrice().get(i).getQuantity() != null && asiProduct.getPrices().getPrice().get(i).getQuantity().getRange() != null) {
							if(i == 0) {
								myModel.put("minOrderQty", asiProduct.getPrices().getPrice().get(i).getQuantity().getRange().getFrom());
							}
							priceQtyMap.put(asiProduct.getPrices().getPrice().get(i).getQuantity().getRange().getTo(), asiProduct.getPrices().getPrice().get(i).getPrice().doubleValue());
						}
					}
				}
				myModel.put("priceQtyMap", priceQtyMap);
			}
			
			Map<String, Integer> variantQtyMap = new HashMap<String, Integer>();
			if(variantList == null || variantList.isEmpty()) {
				quantity = ServletRequestUtils.getIntParameter(request, "quantity", 0);
			} else {
				for(String variant : variantList) {
					if(ServletRequestUtils.getIntParameter(request, "qty_"+variant, 0) >= 0) {
						variantQtyMap.put(variant, ServletRequestUtils.getIntParameter(request, "qty_"+variant, 0));
						quantity = quantity + ServletRequestUtils.getIntParameter(request, "qty_"+variant, 0);
					} else {
						variantQtyMap.put(variant,0);
					}
				}
			}
			myModel.put("variantQtyMap", variantQtyMap);
		}
		myModel.put("quantity", quantity);
		//get price from quantity break
		
		if(minQtyMet && quantity >= (Integer)myModel.get("minOrderQty")) {
			//code for imprint options goes here
			double unitPrice = 0.0;
			if(product.isAsiIgnorePrice()) {
				myModel.put("unitPrice", this.webJaguar.unitPriceByQty(quantity, product));
			} else {
				if(asiProduct.getVariants() != null) {
					if(samePriceBreakForAllSizes) {
						double cumulativeTotal = 0.0;
						for(String variant : variantPriceQtyMap.keySet()){
							if(ServletRequestUtils.getIntParameter(request, "qty_"+variant, 0) > 0){
								for(Integer qty : variantPriceQtyMap.get(variant).keySet()) {
									if(ServletRequestUtils.getIntParameter(request, "qty_"+variant, 0) < qty) {
										cumulativeTotal = cumulativeTotal + variantPriceQtyMap.get(variant).get(qty) * ServletRequestUtils.getIntParameter(request, "qty_"+variant, 0);
										break;
									}
								}
							}
						}
						unitPrice = cumulativeTotal / quantity;
						myModel.put("unitPrice", unitPrice);
						
					} else {
						//code to find out the unit price if price breaks are different
					}
				} else {
					if(samePriceBreakForAllSizes) {
						for(Integer qty : priceQtyMap.keySet()) {
							if(quantity < qty) {
								unitPrice = priceQtyMap.get(qty);
								break;
							}
						}
					} else {
						//code to find out the unit price if price breaks are different
					}	
				}
				myModel.put("unitPrice", unitPrice);
			}
		}
		
		if(myModel.get("unitPrice") != null && asiProduct.getImprinting() != null && asiProduct.getImprinting().getMethods() != null) {
			
			List<String> imprintOptions = new ArrayList<String>();
			if(asiProduct.getImprinting() != null && asiProduct.getImprinting().getMethods() != null) {
				if(asiProduct.getImprinting().getMethods().getValues() != null ) {
					if(asiProduct.getImprinting().getMethods().getValues().getProductOption() != null) {
						if(asiProduct.getImprinting().getMethods().getValues().getProductOption() != null) {
							for(ProductOption productOption : asiProduct.getImprinting().getMethods().getValues().getProductOption()) {
								imprintOptions.add(productOption.getName());
							}
						}
					}
					
					if(asiProduct.getImprinting().getMethods().getValues().getString() != null) {
						for(String value : asiProduct.getImprinting().getMethods().getValues().getString()) {
							if(!imprintOptions.contains(value)) {
								imprintOptions.add(value);
							}
						}
					}
				}
			}
			myModel.put("imprintOptions", imprintOptions);
			
			myModel.put("imMethod", ServletRequestUtils.getStringParameter(request, "imMethod"));
			
			List<String> selectedImprintColors = new ArrayList<String>();
			for(int i=1; i<=100; i++) {
				if(ServletRequestUtils.getStringParameter(request, "imColor"+i) != null && !ServletRequestUtils.getStringParameter(request, "imColor"+i).isEmpty()) {
					selectedImprintColors.add(ServletRequestUtils.getStringParameter(request, "imColor"+i));
				}
			}
			HashMap<String, String> selectedImprint = new HashMap<String, String>();
			for(KeyBean colorCode : Constants.COLOR_CODE) {
				for(String selectedColor : selectedImprintColors) {
					if(selectedColor.equalsIgnoreCase(colorCode.getName().toString())) {
						selectedImprint.put(colorCode.getName().toString(), colorCode.getValue().toString());
					}
				}
			}
			myModel.put("selectedImprint", selectedImprint);
			myModel.put("imText", ServletRequestUtils.getStringParameter(request, "imText"));
			//calculate imprint charge
			if(!selectedImprint.isEmpty()) {
				
				double imprintCharge = 0.0;
				if(asiProduct.getImprinting() != null && asiProduct.getImprinting().getMethods() != null) {
					if(asiProduct.getImprinting().getMethods().getValues() != null && asiProduct.getImprinting().getMethods().getValues().getProductOption() != null) {
						if(asiProduct.getImprinting().getMethods().getValues().getProductOption() != null) {
							for(ProductOption productOption : asiProduct.getImprinting().getMethods().getValues().getProductOption()) {
									if(ServletRequestUtils.getStringParameter(request, "imMethod") != null && !ServletRequestUtils.getStringParameter(request, "imMethod").equalsIgnoreCase(productOption.getName())) {
										continue;
									}
								
									if(productOption.getValues() != null && productOption.getValues().getProductOptionValue() != null) {
										for(ProductAttributeValue productOptionValue : productOption.getValues().getProductOptionValue()) {
											if(productOptionValue.getCharges() != null) {
												if(productOptionValue.getCharges().getCharge() != null) {
													for(Charge charge : productOptionValue.getCharges().getCharge()){
														if(charge.getType().equalsIgnoreCase("Run")){
															for( Price price : charge.getPrices().getPrice()) {
																if(price.getQuantity() != null && price.getQuantity().getRange() != null) {
																	if((quantity >= price.getQuantity().getRange().getFrom()) && (quantity <= price.getQuantity().getRange().getTo())){
																		myModel.put("runningCharge", price.getPrice().doubleValue());
																		imprintCharge = imprintCharge + (quantity * price.getPrice().doubleValue());
																	}
																}
															}	
														}
														if(charge.getType().equalsIgnoreCase("Setup")){
															for( Price price : charge.getPrices().getPrice()) {
																if(price.getQuantity() != null && price.getQuantity().getRange() != null) {
																	if((quantity >= price.getQuantity().getRange().getFrom()) && (quantity <= price.getQuantity().getRange().getTo())){
																		myModel.put("setupCharge", price.getPrice().doubleValue());
																		imprintCharge = imprintCharge + (quantity * price.getPrice().doubleValue());
																	}
																}
															}	
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					if(selectedImprint.size() > 1) {
						
						if(asiProduct.getImprinting() != null && asiProduct.getImprinting().getOptions() != null) {
							if(asiProduct.getImprinting().getOptions().getOption() != null ) {
								for(ProductOptionSet productOption : asiProduct.getImprinting().getOptions().getOption()) {
									if(productOption.getName() != null && productOption.getName().contains("Additional")) {
										if(productOption.getGroups() != null && productOption.getGroups().getGroup() != null) {
											for(ProductOption prodOption : productOption.getGroups().getGroup()) {
												if(prodOption.getValues() != null && prodOption.getValues().getProductOptionValue() != null) {
													for(ProductAttributeValue productOptionValue : prodOption.getValues().getProductOptionValue()) {
														if(productOptionValue.getCharges() != null) {
															for(Charge charge : productOptionValue.getCharges().getCharge()){
																if(charge.getType().equalsIgnoreCase("Run") && charge.getPrices() != null && charge.getPrices().getPrice() != null){
																	for( Price price : charge.getPrices().getPrice()) {
																		if(price.getQuantity() != null && price.getQuantity().getRange() != null) {
																			if((quantity >= price.getQuantity().getRange().getFrom()) && (quantity <= price.getQuantity().getRange().getTo())){
																				myModel.put("additionalRunningCharge", price.getPrice().doubleValue());
																				imprintCharge = imprintCharge + (selectedImprint.size() - 1) * quantity * price.getPrice().doubleValue();
																			}
																		}
																	}
																}
																
																if(charge.getType().equalsIgnoreCase("Setup") && charge.getPrices() != null && charge.getPrices().getPrice() != null){
																	for( Price price : charge.getPrices().getPrice()) {
																		if(price.getQuantity() != null && price.getQuantity().getRange() != null) {
																			if((quantity >= price.getQuantity().getRange().getFrom()) && (quantity <= price.getQuantity().getRange().getTo())){
																				myModel.put("additionalSetupCharge", price.getPrice().doubleValue());
																				imprintCharge = imprintCharge + (selectedImprint.size() - 1)  * price.getPrice().doubleValue();
																			}
																		}
																	}
																}
															}
														}
													}	
												}
											}
										}
									}
								}
							}
						}
					}
					myModel.put("imprintCharge", imprintCharge);
				}	
			}
		return new ModelAndView("frontend/asi/asiOptions2", "model", myModel);
	}
	
}