/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 06.28.2010
 */

package com.webjaguar.thirdparty.asi;
/* TO BE REMOVE
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class AsiSupplierTreeController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
	
		Map<String, Object> map = new HashMap<String, Object>();
		List<AsiCategory> categories = new ArrayList<AsiCategory>();
		String supplier = ServletRequestUtils.getStringParameter(request, "_supplier", null);
		map.put("selectedSupplier", supplier);	
		
		if (request.getMethod().equals("POST") && request.getParameterValues("code") != null) {
			for (String code: request.getParameterValues("code")) {
				AsiCategory category = new AsiCategory();
				category.setCode(code);
				try {
					category.setCatId(ServletRequestUtils.getIntParameter(request, code + "_catId"));
				} catch (Exception e) {
					category.setCatId(null);
				}
				try {
					category.setPriceChange(ServletRequestUtils.getDoubleParameter(request, code + "_priceChange"));
				} catch (Exception e) {
					category.setPriceChange(null);
				}
				categories.add(category);
			}
			this.webJaguar.nonTransactionSafeUpdateAsiCategories(categories);
		}
		if (supplier != null) {
			categories = this.webJaguar.getAsiCategoriesBySupplier(supplier);
	
			Map<String, AsiCategory> categoryMap = new HashMap<String, AsiCategory>();
			Iterator<AsiCategory> iter = categories.iterator();
			while (iter.hasNext()) {
				AsiCategory category = iter.next();
				if (category.getCat().equals("*")) {
					categoryMap.put(category.getSupplier(), category);				
				} else if (category.getSub().equals("*")) {
					categoryMap.put(category.getSupplier() + category.getCat(), category);
					if (categoryMap.get(category.getSupplier()) != null) {
						categoryMap.get(category.getSupplier()).addSub(category);					
					}
					iter.remove();
				} else {
					if (categoryMap.get(category.getSupplier() + category.getCat()) != null) {
						categoryMap.get(category.getSupplier() + category.getCat()).addSub(category);					
					}
					iter.remove();
				}
			}
			map.put("categories", categories);	
		}
	
		map.put("supplierList", this.webJaguar.getAsiSupplier());
		return new ModelAndView("admin/vendors/asi/category", map);
	}
}
*/