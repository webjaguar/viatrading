/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 08.20.2009
 */

package com.webjaguar.thirdparty.asi;
/* TO BE REMOVE
import java.util.ArrayList;
import java.util.List;

public class AsiCategory {

	private String code;
	private String supplier;
	private String cat;
	private String sub;
	private String asi;
	private String productCategory;
	private Integer catId;
	private Double priceChange;
	List<AsiCategory> subs;
	
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
	public String getSupplier()
	{
		return supplier;
	}
	public void setSupplier(String supplier)
	{
		this.supplier = supplier;
	}
	public String getCat()
	{
		return cat;
	}
	public void setCat(String cat)
	{
		this.cat = cat;
	}
	public String getSub()
	{
		return sub;
	}
	public void setSub(String sub)
	{
		this.sub = sub;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public Integer getCatId()
	{
		return catId;
	}
	public void setCatId(Integer catId)
	{
		this.catId = catId;
	}
	public String getAsi() {
		return asi;
	}
	public void setAsi(String asi) {
		this.asi = asi;
	}
	public Double getPriceChange()
	{
		return priceChange;
	}
	public void setPriceChange(Double priceChange)
	{
		this.priceChange = priceChange;
	}
	public List<AsiCategory> getSubs()
	{
		return subs;
	}
	public void setSubs(List<AsiCategory> subs)
	{
		this.subs = subs;
	}
	public void addSub(AsiCategory sub) 
	{
		if (subs == null) subs = new ArrayList<AsiCategory>();
		subs.add(sub);
	}
	public String toString() {
		return this.supplier.replaceAll(" ", "") + this.cat.replaceAll(" ", "") + this.sub.replaceAll(" ", "");
	}
	public boolean containsCategory(List<AsiCategory> categories) {
		for (AsiCategory category:categories) {
			if (category.toString().trim().equalsIgnoreCase(this.toString().trim())) {
				return true;
			}
		}
		return false;
	}

}
*/