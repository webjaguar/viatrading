/* Copyright 2005, 2008 Advanced E-Media Solutions
 * author: Jwalant Patel
 */

package com.webjaguar.thirdparty.asi;


import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.thirdparty.asi.asiAPI.Charge;
import com.webjaguar.thirdparty.asi.asiAPI.Price;
import com.webjaguar.thirdparty.asi.asiAPI.ProductAttributeSet;
import com.webjaguar.thirdparty.asi.asiAPI.ProductAttributeValue;
import com.webjaguar.thirdparty.asi.asiAPI.Product.Variants.Variant;
import com.webjaguar.thirdparty.asi.asiAPI.ProductAttribute;
import com.webjaguar.thirdparty.asi.asiAPI.ProductImprintingSet;


public class ShowAsiOptions3Controller implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		Product product = null;
		if(ServletRequestUtils.getIntParameter(request, "id") != null){
			product = this.webJaguar.getProductById(ServletRequestUtils.getIntParameter(request, "id"), request);
		}
			
		if(product == null || product.getAsiXML() == null) {
			return null;
		}
			
		JAXBContext context = JAXBContext.newInstance(com.webjaguar.thirdparty.asi.asiAPI.Product.class);
		Unmarshaller unmarshaller = context.createUnmarshaller();
		StringBuffer sb = new StringBuffer(product.getAsiXML());
		ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
	    com.webjaguar.thirdparty.asi.asiAPI.Product asiProduct = (com.webjaguar.thirdparty.asi.asiAPI.Product) unmarshaller.unmarshal(bis);
	    
	    // set ASI Price with markup, if sales tag is not applied
	    boolean markUpApplied = false;
	    if(product.getSalesTag() == null) {
	    	markUpApplied = this.webJaguar.applyPriceDifference(null, asiProduct, siteConfig.get("ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS").getValue(),  Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_TYPE").getValue()), Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_FORMULA").getValue()));
		}
	        
	    Map<String, Object> model = new HashMap<String, Object>();
		model.put("product", product);
		
		if(product.isAsiIgnorePrice()) {
			// get unit price from webjaguar database and ignore ASI Price from XML
			model.put("unitPrice", this.webJaguar.unitPriceByQty(ServletRequestUtils.getIntParameter(request, "quantity", 1), product));
		} else {
			//get original unit price before applying sales tag
			this.getQtyAndUnitPrice(asiProduct, ServletRequestUtils.getIntParameter(request, "quantity", 1), ServletRequestUtils.getStringParameter(request, "variant"),  model, true);
		}
		
	    // if markup is not applied or markup is set to change the base price, than apply sales tag
	    if((siteConfig.get("ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE").getValue().equals("true") || !markUpApplied) && product.getSalesTag() != null) {
	       this.webJaguar.setASIPriceWithSalesTag(null, asiProduct, product.getSalesTag());
	    }
	    
		model.put("selectedMultiColorImprint", ServletRequestUtils.getStringParameter(request, "multiColorImprint"));
		model.put("selectedImprintColors", ServletRequestUtils.getStringParameter(request, "imprintColors"));
		model.put("selectedImprintLocation", ServletRequestUtils.getStringParameter(request, "multiLocationImprint"));
		model.put("selectedAdditionalInfo", ServletRequestUtils.getStringParameter(request, "additionalInfo"));
		
		
		//variants
		model.put("variant", ServletRequestUtils.getStringParameter(request, "variant"));
		//get variant
		Variant variant = null;
		if(ServletRequestUtils.getStringParameter(request, "variant") != null && asiProduct.getVariants() != null && asiProduct.getVariants().getVariant() != null) {
			for(Variant loopVariant : asiProduct.getVariants().getVariant()) {
				if(loopVariant.getDescription().equalsIgnoreCase(StringEscapeUtils.escapeXml(ServletRequestUtils.getStringParameter(request, "variant")))) {
					variant = loopVariant;
				}
			}
		}
		//set attributes
		this.getAsiAttributes(asiProduct, variant, model, request);
		
		
		if(product.isAsiIgnorePrice()) {
			//set qty, if asi price is ignored
			model.put("quantity", ServletRequestUtils.getIntParameter(request, "quantity", 1));
		} else {
			//set qty and unit price, if asi price is not ignored
			this.getQtyAndUnitPrice(asiProduct, ServletRequestUtils.getIntParameter(request, "quantity", 1), ServletRequestUtils.getStringParameter(request, "variant"),  model, false);
		}
		//additional charges like setup charge,cut charge, die charge, running charge, etc.
		ProductImprintingSet imprintingSet = asiProduct.getImprinting();
		if(variant != null && variant.getImprinting() != null) {
			imprintingSet = variant.getImprinting();
		}
		
		if(model.get("selectedImprintMethod") != null) {
			model.put("additionalCharge", this.getAdditionalCharges(model.get("selectedImprintMethod").toString(), imprintingSet, (Integer) model.get("quantity")));
		}
		
		if(ServletRequestUtils.getStringParameter(request, "divUpdate") != null) {
			return new ModelAndView("frontend/asi/ajaxOptionsUpdate", "model", model);
		} else {
			return new ModelAndView("frontend/asi/asiOptions3", "model", model);
		}
	}
	
	private void getAsiAttributes(com.webjaguar.thirdparty.asi.asiAPI.Product asiProduct, Variant variant, Map<String, Object> model, HttpServletRequest request) throws ServletRequestBindingException {
		
		
		model.put("asiProduct", asiProduct); 
		ProductAttributeSet attributeSet = asiProduct.getAttributes();
		if(variant != null && variant.getAttributes() != null) {
			attributeSet = variant.getAttributes();
		}
		
		// get color
		if(attributeSet != null && attributeSet.getColors() != null) {
			attributeSet.getColors().getValues().getString().remove("All Colors");
			List<String> productColors = this.getAttributeList(attributeSet.getColors());
			model.put("productColors", productColors);
			
			if(productColors.size() == 1) {
				model.put("selectedColor", productColors.get(0));
			} else {
				model.put("selectedColor", ServletRequestUtils.getStringParameter(request, "color"));
			}
		}
		
		// get material 
		if(attributeSet != null && attributeSet.getMaterials() != null) {
			List<String> materials = this.getAttributeList(attributeSet.getMaterials());
			model.put("materials", materials);
			
			if(materials.size() == 1) {
				model.put("selectedMaterial", materials.get(0));
			} else {
				model.put("selectedMaterial", ServletRequestUtils.getStringParameter(request, "material"));
			}
		}
		
		// get size
		if(attributeSet != null && attributeSet.getSizes() != null) {
			List<String> sizes = this.getAttributeList(attributeSet.getSizes());
			model.put("sizes", sizes);
			
			if(sizes.size() == 1) {
				model.put("selectedSize", sizes.get(0));
			} else {
				model.put("selectedSize", ServletRequestUtils.getStringParameter(request, "size"));
			}
		}
		
		// get shape 
		if(attributeSet != null && attributeSet.getShapes() != null) {
			List<String> shapes = this.getAttributeList(attributeSet.getShapes());
			model.put("shapes", shapes);
			if(shapes.size() == 1) {
				model.put("selectedShape", shapes.get(0));
			} else {
				model.put("selectedShape", ServletRequestUtils.getStringParameter(request, "shape"));
			}
		}
		
		ProductImprintingSet imprintingSet = asiProduct.getImprinting();
		if(variant != null && variant.getImprinting() != null) {
			imprintingSet = variant.getImprinting();
		}
		
		// get imprint methods 
		if(imprintingSet != null && imprintingSet.getMethods() != null && imprintingSet.getMethods().getValues() != null) {
			List<String> imprintMethods = null;
			
			if(imprintingSet.getMethods().getValues().getProductOption() != null && !imprintingSet.getMethods().getValues().getProductOption().isEmpty()) {
				if(imprintMethods == null) {
					imprintMethods = new ArrayList<String>();
				}
				
				for(com.webjaguar.thirdparty.asi.asiAPI.ProductOption productOption : imprintingSet.getMethods().getValues().getProductOption()) {
					imprintMethods.add(productOption.getName());
				}
			}
			if(imprintingSet.getMethods().getValues().getString() != null) {
				if(imprintMethods == null) {
					imprintMethods = new ArrayList<String>();
				}
				for(String value : imprintingSet.getMethods().getValues().getString()) {
					if(!imprintMethods.contains(value)) {
						imprintMethods.add(value);
					}
				}
			}
			if(imprintMethods != null) {
				model.put("imprintMethods", imprintMethods);
				if(imprintMethods.size() == 1) {
					model.put("selectedImprintMethod", imprintMethods.get(0));
				} else {
					model.put("selectedImprintMethod", ServletRequestUtils.getStringParameter(request, "imprintMethod"));
				}
			}
		} 
	}
	
	private List<String> getAttributeList(ProductAttribute attribute){
		List<String> values = new ArrayList<String>();
		if(!attribute.getValues().getProductAttributeValue().isEmpty()) {
			for(ProductAttributeValue attributeValue : attribute.getValues().getProductAttributeValue()) {
				values.add(attributeValue.getName());
			}
		} else {
			values.addAll(attribute.getValues().getString());
		}
		return values;
	}
	
	private void getQtyAndUnitPrice(com.webjaguar.thirdparty.asi.asiAPI.Product asiProduct, int quantity, String variant, Map<String, Object> model, boolean getOriginalPrice){
		/*
		 * TODO
		 * some products does not have any price.
		 * We need to enable Quote feature instead.
		 */
		// if qty is less than minimum required, set it to minimum quantity
		if(quantity < asiProduct.getHighestPrice().getQuantity().getRange().getFrom()) {
			quantity = asiProduct.getHighestPrice().getQuantity().getRange().getFrom();
		}
		model.put("quantity", quantity);
		
		//simple products without variants
		if(asiProduct.getVariants() == null && asiProduct.getPrices() != null && asiProduct.getPrices().getPrice() != null) {
			for(Price price : asiProduct.getPrices().getPrice()) {
				if(price.getQuantity() != null && price.getQuantity().getRange() != null) {
					if(price.getQuantity().getRange().getFrom() <= quantity && price.getQuantity().getRange().getTo() >= quantity) {
						if(getOriginalPrice) {
							model.put("asiOriginalPrice",  price.getPrice().doubleValue());
						} else {
							model.put("unitPrice",  price.getPrice().doubleValue());
						}
						break;
					}
				}
			}
		}
		
		//products with variants
		if(variant != null && asiProduct.getVariants() != null && asiProduct.getVariants().getVariant() != null) {
			for(Variant loopVariant : asiProduct.getVariants().getVariant()) {
				if(loopVariant.getPrices() != null && loopVariant.getPrices().getPrice() != null && loopVariant.getDescription().equalsIgnoreCase(StringEscapeUtils.escapeXml(variant))) {
					for(Price price : loopVariant.getPrices().getPrice()) {
						if(price.getQuantity() != null && price.getQuantity().getRange() != null) {
							if(price.getQuantity().getRange().getFrom() <= quantity && price.getQuantity().getRange().getTo() >= quantity) {
								if(getOriginalPrice) {
									model.put("asiOriginalPrice",  price.getPrice().doubleValue());
								} else {
									model.put("unitPrice",  price.getPrice().doubleValue());
								}
								break;
							}
						}
					}
				}
			}
		}
	}
	
	private double getAdditionalCharges(String imprintMethod, ProductImprintingSet imprintingSet, int quantity){
		double additionalCharge = 0.0;
		
		// full list of fixed charges 
		List<String> fixedChargeNames = new ArrayList<String>();
		fixedChargeNames.add("screen charge");
		fixedChargeNames.add("set up charge");
		fixedChargeNames.add("plate charge");
		fixedChargeNames.add("die charge");
		fixedChargeNames.add("digitizing charge");
		fixedChargeNames.add("mold charge");
		fixedChargeNames.add("tape charge");
		fixedChargeNames.add("tooling charge");
		fixedChargeNames.add("cut charge");
		
		if(imprintingSet != null && imprintingSet.getMethods() != null && imprintingSet.getMethods().getValues() != null) {
			if(imprintingSet.getMethods().getValues().getProductOption() != null) {
				for(com.webjaguar.thirdparty.asi.asiAPI.ProductOption productOption : imprintingSet.getMethods().getValues().getProductOption()) {
					if(productOption.getName().equalsIgnoreCase(imprintMethod)){
						if(productOption.getCharges() != null && productOption.getCharges().getCharge() != null) {
							for(Charge charge : productOption.getCharges().getCharge()){
								if(charge.isIsUndefined() != null && charge.isIsUndefined()) {
									continue;
								}
								//check if charge should apply per order or per quantity
								boolean perOrderCharge = false;
								if(charge.getUsageLevel() != null) {
									if(charge.getUsageLevel().equalsIgnoreCase("order")) {
										perOrderCharge = true;
									} else if(charge.getUsageLevel().equalsIgnoreCase("quantity")) {
										perOrderCharge = false;
									}	
								} else {
									if(charge.getName() != null && fixedChargeNames.contains(charge.getName().toLowerCase())) {
										perOrderCharge = true;
									} 
								}
								for(Price price : charge.getPrices().getPrice()) {
									if(price.getQuantity().getRange().getFrom() <= quantity && price.getQuantity().getRange().getTo() >= quantity) {
										additionalCharge = additionalCharge + ( perOrderCharge ? price.getPrice().doubleValue() : price.getPrice().doubleValue() * quantity);
									}
								}
							}	
						}
					}
				}
			}
		}
		return additionalCharge;
	}
}