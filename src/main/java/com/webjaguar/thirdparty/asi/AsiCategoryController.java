/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 08.20.2009
 */

package com.webjaguar.thirdparty.asi;
/* TO BE REMOVE
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Search;

public class AsiCategoryController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Search search = getSearch( request );
		Map<String, Object> myModel = new HashMap<String, Object>();

		List<AsiCategory> categories = new ArrayList<AsiCategory>();
		
		if (request.getMethod().equals("POST") && request.getParameterValues("code") != null) {
			for (String code: request.getParameterValues("code")) {
				AsiCategory category = new AsiCategory();
				category.setCode(code);
				try {
					category.setCatId(ServletRequestUtils.getIntParameter(request, code + "_catId"));
				} catch (Exception e) {
					category.setCatId(null);
				}
				categories.add(category);
			}
			this.webJaguar.nonTransactionSafeUpdateAsiCategories(categories);
		}
		
		int count = this.webJaguar.getAsiCategories(new Search()).size();
		if (count < search.getOffset()-1) {
			search.setPage(1);
		}
		search.setLimit(search.getPageSize());
		search.setOffset((search.getPage()-1)*search.getPageSize());
		
		List<AsiCategory> catSubList = this.webJaguar.getAsiCategories(search);
		myModel.put("catSubList", catSubList);
		
		int pageCount = count/search.getPageSize();
		if (count%search.getPageSize() > 0) {
			pageCount++;
		}
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", search.getOffset()+catSubList.size());
		myModel.put("count", count);

		return new ModelAndView("admin/vendors/asi/supplierTree", "model", myModel);
	}
	
	private Search getSearch(HttpServletRequest request) {
		//Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		Search search = (Search) request.getSession().getAttribute( "asiSearch" );
		if (search == null) {
			search = new Search();
			search.setPageSize(100);
			request.getSession().setAttribute( "asiSearch", search );
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}			
		
		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 100 );
			if (size < 100) {
				search.setPageSize( 100 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		return search;
	}
}
*/