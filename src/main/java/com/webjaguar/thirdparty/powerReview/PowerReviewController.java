/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 8.24.2010
 */

package com.webjaguar.thirdparty.powerReview;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;

public class PowerReviewController extends WebApplicationObjectSupport implements Controller {

	private static WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		String powerReviewXML_URI = siteConfig.get("POWER_REVIEW_XML").getValue();
		if (powerReviewXML_URI.isEmpty()) {
			return new ModelAndView("admin/vendors/index");
		}
		
		System.out.println("xml " +powerReviewXML_URI);
		// parse PowerReview
		System.out.println("PowerReview start:" + new Date() );
		parsePowerReviewXML(powerReviewXML_URI, siteConfig);
		System.out.println("PowerReview end:" + new Date() );
		
		
		return new ModelAndView("admin/vendors/index");
	}
	
	public void parsePowerReviewXML(String uri, Map<String, Configuration> siteConfig) {
		try {
			SAXParserFactory f = SAXParserFactory.newInstance();
	        SAXParser parser = f.newSAXParser();
	        DefaultHandler handler = new MyContentHandler();
	        parser.parse(uri,handler);
	    } catch (ParserConfigurationException e) {System.out.println("error 1 ");
	        notifyAdmin("parseXmlFile ApplianceSpec on " + siteConfig.get("SITE_URL").getValue(), e.toString());
	    } catch (SAXException e) {System.out.println("error 2 ");
	    	notifyAdmin("parseXmlFile ApplianceSpec on " + siteConfig.get("SITE_URL").getValue(), e.toString());
	    } catch (IOException e) {System.out.println("error 3 ");
	    	notifyAdmin("parseXmlFile ApplianceSpec on " + siteConfig.get("SITE_URL").getValue(), e.toString());
	    }
	}
	
	public static class MyContentHandler extends DefaultHandler {
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		HashMap<String, Object> map;
		
		boolean sku = false;
		boolean inlinequestionfiles = false;
		boolean inlinefiles = false;
		boolean inlinefile = false;
	    
		public void startDocument() throws SAXException {
			
	    }
	    
	    public void endDocument() throws SAXException {
	       // for files that has less than 500 Product
	       if (data.size() > 0) {
				// products
				updateProduct(data);
				data.clear();
				map.clear();
			}
	    }
	    
	    public void startElement(String nsURI, String strippedName, String tagName, Attributes attributes) throws SAXException {
	    	if (tagName.equalsIgnoreCase("Product")) { 
	    		map = new HashMap<String, Object>();
	    	}
	    	if (tagName.equalsIgnoreCase("pageid")) { sku = true; }
	    	if (tagName.equalsIgnoreCase("inlinefile")) { inlinefile = true; }
	    	if (tagName.equalsIgnoreCase("inlinefiles")) { inlinefiles = true; }
	    	if (tagName.equalsIgnoreCase("inlinequestionfiles")) { inlinequestionfiles = true; }
	    	
	    }
	    
	    public void endElement(String nsURI, String strippedName, String tagName) throws SAXException {
	    	if (tagName.equalsIgnoreCase("Product")) {
	    		data.add(map);
	    	   
	    		if (data.size() == 500) {
					// products
					updateProduct(data);
					data.clear();
					map.clear();
				}
	    	}
	    }
	    
	    public void characters(char buf[], int offset, int len) throws SAXException {
	       String value = new String(buf, offset, len);
	       if (value.trim().length() > 0) {
	    	   if (sku) {
		    	   map.put("sku", value);
		    	   sku = false;
		       }
	    	   if (inlinefiles && inlinefile) {
		    	   map.put("tab_3_content", value);
		    	   inlinefiles = false;
		       }
	    	   if (inlinequestionfiles && inlinefile) {
		    	   map.put("tab_6_content", value );
		    	   inlinequestionfiles = false;
		       }
	       }  
	    }
	    
	    public void ignorableWhitespace(char buf[], int offset, int len) throws SAXException {

	    }
	    
	    public void skippedEntity(String name)  throws SAXException {
	    	System.out.println("skippedEntity " + name);
	    }
	    
	    public void warning(SAXParseException e) throws SAXException {
	        System.out.println("Warning: " + e.getMessage());
	    }

	    private void updateProduct(List<Map <String, Object>> data) {
	    	// update Products
			webJaguar.updatePowerReview(data);
	    }
	 }

	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}