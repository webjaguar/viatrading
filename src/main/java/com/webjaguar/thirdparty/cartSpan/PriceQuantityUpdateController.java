/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.05.2010
 */

package com.webjaguar.thirdparty.cartSpan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;

public class PriceQuantityUpdateController extends WebApplicationObjectSupport implements Controller {
	
	WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }

	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		if ((Boolean) gSiteConfig.get("gSITE_ACTIVE") == false || siteConfig.get("CARTSPAN_API_CODE").getValue().trim().equals("")
				|| !ServletRequestUtils.getStringParameter(request, "api_code", "").equals(siteConfig.get("CARTSPAN_API_CODE").getValue())) {
			return null;
		}
		
		List<CsvFeed> csvFeedList = this.webJaguar.getCsvFeed("cartspan-inventory");
		
		List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();

		String update_csv = ServletRequestUtils.getStringParameter(request, "update_csv", "").trim();
		if (update_csv.length() > 0) {
			List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
			
			for (String item: update_csv.split("[|]")) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				
				String[] itemLine = item.split("[,]"); 
				
				for (CsvFeed csvFeed: csvFeedList) {
					if (csvFeed.getColumn() != null) {
						String sValue = itemLine[csvFeed.getColumn()].trim();
						if (csvFeed.isNumeric()) {
							try {
								map.put(csvFeed.getColumnName(), Double.parseDouble(sValue));
							} catch (NumberFormatException e) {
								map.put(csvFeed.getColumnName(), null);									
							}
						} else {
							map.put(csvFeed.getColumnName(), sValue);					
						}						
					}
				}
				data.add(map);
				if (data.size() == 5000) {
					// products
					
					int index = 0;
					for (int result: this.webJaguar.updateProduct(csvFeedList, data, null)) {
						data.get(index).put("result", result);
						results.add(data.get(index));
						index++;
					}
					data.clear();
				}
			}

			if (data.size() > 0) {
				// products
				
				int index = 0;
				for (int result: this.webJaguar.updateProduct(csvFeedList, data, null)) {
					data.get(index).put("result", result);
					results.add(data.get(index));
					index++;
				}
				data.clear();
			}
		}
		
		return new ModelAndView("frontend/cartSpan/priceQuantityUpdate", "results", results);
	}
}
