/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.05.2010
 */

package com.webjaguar.thirdparty.cartSpan;

import java.io.OutputStreamWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.PackingList;
import com.webjaguar.model.PackingListSearch;

public class PackingListDownloadController extends WebApplicationObjectSupport implements Controller {
	
	WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }

	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// http://www.yourdomain.com/cartSpan/orderDownload.jhtm?order_start=01/1/2011 11:45:00&order_end=9/27/2011 13:10:00&api_code=8120ALD7458709175
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		if ((Boolean) gSiteConfig.get("gSITE_ACTIVE") == false || siteConfig.get("CARTSPAN_API_CODE").getValue().trim().equals("")
				|| !ServletRequestUtils.getStringParameter(request, "api_code", "").equals(siteConfig.get("CARTSPAN_API_CODE").getValue())) {
			return null;
		}
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy kk:mm:ss");

		PackingListSearch pLSearch = new PackingListSearch();
		try {
			pLSearch.setStartDate(df.parse(ServletRequestUtils.getStringParameter(request, "order_start")));
		} catch (Exception e) {
			pLSearch.setStartDate(new Date());
		}
		try {
			pLSearch.setEndDate(df.parse(ServletRequestUtils.getStringParameter(request, "order_end")));
		} catch (Exception e) {
			pLSearch.setEndDate(new Date());
		}
		
		Map<String, String> storeIdMap = this.webJaguar.getStoreIdMap();
		String store_id = ServletRequestUtils.getStringParameter(request, "store_id", "");
		if (store_id.trim().length() > 0) {
			storeIdMap = this.webJaguar.getStoreIdMap();
			Set<String> hosts = new HashSet<String>();
			for (String storeId: store_id.split("[,]")) {
				if (storeId.equals("0")) {
					hosts.add(null);
				} else if (storeIdMap.containsKey(storeId)) {
					hosts.add(storeIdMap.get(storeId));					
				}
			}
			if (hosts.isEmpty()) {
				// add dummy store
				hosts.add("");
			}
			pLSearch.setHost(new ArrayList<String>());
			pLSearch.getHosts().addAll(hosts);
		}
		
		Map<String, String> multiStoreMap = new HashMap<String, String>();
		for (String storeId: storeIdMap.keySet()) {
			multiStoreMap.put(storeIdMap.get(storeId), storeId);
		}
		
		List<PackingList> packingLists = this.webJaguar.getExportPackingList(pLSearch);
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy kk:mm");
		NumberFormat nf = new DecimalFormat("#0.00");
		
		try {
			CSVWriter writer = new CSVWriter(new OutputStreamWriter(response.getOutputStream()), ',', CSVWriter.NO_QUOTE_CHARACTER);
			
			// headers
			List<String> line = new ArrayList<String>();
			line.add("Order_Id");			
			line.add("Increment_Id");
			line.add("Order_Date");
			line.add("Customer_Id");
			line.add("Customer_Prefix");
			line.add("Billing_First_Name");
			line.add("Billing_Last_Name");
			line.add("Billing_Company");
			line.add("Billing_Address_1");
			line.add("Billing_Address_2");
			line.add("Billing_City");
			line.add("Billing_State");
			line.add("Billing_Zip");
			line.add("Billing_Country");
			line.add("Billing_Email");
			line.add("Billing_Phone");
			line.add("ShipTo_First_Name");
			line.add("ShipTo_Last_Name");
			line.add("ShipTo_Company");
			line.add("ShipTo_Address_1");
			line.add("ShipTo_Address_2");
			line.add("ShipTo_City");
			line.add("ShipTo_State");
			line.add("ShipTo_Zip");
			line.add("ShipTo_Country");
			line.add("ShipTo_Phone");
			line.add("ShipTo_Email");
			line.add("Order_Total");
			line.add("Order_Total_Paid");
			line.add("Order_Discount");
			line.add("Order_Tax");
			line.add("Gift_Redeemed");
			line.add("Gift_Certificates");
			line.add("Shipping_Amount");
			line.add("Shipping_Weight");
			line.add("Shipping_Method");
			line.add("Order_Status");
			line.add("Comments");
			line.add("Payment_Method");
			line.add("Transaction_Id");
			line.add("Product_Id");
			line.add("Product_Description");
			line.add("Product_Quantity");
			line.add("Product_Price");
			
			String[] lines = new String[line.size()];
			writer.writeNext(line.toArray(lines));
			
			Map<Integer, String> customers = new HashMap<Integer, String>();
			
			for (PackingList packingList: packingLists) {
				
				for (LineItem lineItem : packingList.getLineItems()) {
					if (!customers.containsKey(packingList.getUserId())) {
						Customer customer = this.webJaguar.getCustomerById(packingList.getUserId());
						if (customer != null && customer.getAccountNumber() != null && customer.getAccountNumber().trim().length() > 0) {
							customers.put(packingList.getUserId(), customer.getAccountNumber());
						} else {
							customers.put(packingList.getUserId(), "" + packingList.getUserId());						
						}
					}
					
					line = new ArrayList<String>();
					line.add("" + ((packingList.getHost() == null) ? "0" : multiStoreMap.get(packingList.getHost())) + "-" + packingList.getPackingListNumber());
					line.add("");									// Increment_Id
					line.add(dateFormatter.format(packingList.getCreated()));
					line.add(customers.get(packingList.getUserId()));
					line.add("");									// Customer_Prefix
					line.add(packingList.getBilling().getFirstName().replace(",", " "));
					line.add(packingList.getBilling().getLastName().replace(",", " "));
					line.add((packingList.getBilling().getCompany() != null) ? packingList.getBilling().getCompany() : "");
					line.add(packingList.getBilling().getAddr1().replace(",", " "));
					line.add(packingList.getBilling().getAddr2().replace(",", " "));
					line.add(packingList.getBilling().getCity().replace(",", " "));
					line.add(packingList.getBilling().getStateProvince().replace(",", " "));
					line.add(packingList.getBilling().getZip().replace(",", " "));
					line.add(packingList.getBilling().getCountry());
					line.add(packingList.getUserEmail());
					line.add(packingList.getBilling().getPhone().replace(",", " "));
					line.add(packingList.getShipping().getFirstName().replace(",", " "));
					line.add(packingList.getShipping().getLastName().replace(",", " "));
					line.add((packingList.getShipping().getCompany() != null) ? packingList.getShipping().getCompany() : "");
					line.add(packingList.getShipping().getAddr1().replace(",", " "));
					line.add(packingList.getShipping().getAddr2().replace(",", " "));
					line.add(packingList.getShipping().getCity().replace(",", " "));
					line.add(packingList.getShipping().getStateProvince().replace(",", " "));
					line.add(packingList.getShipping().getZip().replace(",", " "));
					line.add(packingList.getShipping().getCountry());
					line.add(packingList.getShipping().getPhone().replace(",", " "));
					line.add("");									// ShipTo_Email
					if(packingList.getSubTotal() != null){
						line.add(nf.format(packingList.getSubTotal()));
					} else {
						line.add("");
					}
					line.add("");									// Order_Total_Paid
					line.add("");								    // Order_Discount
		    		line.add("");									// tax
					line.add("");									// Gift_Redeemed
					line.add("");									// Gift_Certificates
					line.add(((packingList.getShippingCost() != null) ? nf.format(packingList.getShippingCost()) : "0"));	
					line.add("");									// Shipping_Weight
					line.add((packingList.getShippingMethod() == null) ? "" :
								packingList.getShippingMethod().replace("\n", " ").replace("\r", " ").replace(",", " "));							// Shipping_Method
					line.add(getMessageSourceAccessor().getMessage("orderStatus_" + packingList.getStatus()));									// Order_Status
					String invoiceNote = packingList.getInvoiceNote() != null ? packingList.getInvoiceNote() : "";
					if(packingList.getTracking() != null && !packingList.getTracking().isEmpty()) {
						invoiceNote = invoiceNote +" Tracking Number : "+packingList.getTracking();
					}
					line.add(invoiceNote.replace("\n", " ").replace("\r", " ").replace(",", " "));								// Comments
					line.add((packingList.getPaymentMethod() == null) ? "" :
									packingList.getPaymentMethod().replace(",", " "));
					String transId = "";
					if (packingList.getCreditCard() != null && packingList.getCreditCard().getNumber().length() >= 4) {
						transId = packingList.getCreditCard().getNumber().substring(packingList.getCreditCard().getNumber().length()-4);
					} else if (packingList.getPaypal() != null && packingList.getPaypal().getTxn_id() != null) {
						transId = packingList.getPaypal().getTxn_id();
					}
					line.add(transId);
					line.add((lineItem.getProduct().getSku() == null) ? "" :
						lineItem.getProduct().getSku().replace("\n", " ").replace("\r", " ").replace(",", " "));
					line.add((lineItem.getProduct().getName() == null) ? "" :
						lineItem.getProduct().getName().replace("\n", " ").replace("\r", " ").replace(",", " "));
					line.add("" + (lineItem.getQuantity() * ((lineItem.getProduct().getCaseContent() == null) ? 1 :lineItem.getProduct().getCaseContent())));
					line.add(((lineItem.getUnitPrice() != null) ? nf.format(lineItem.getUnitPrice()) : "0"));
					
					lines = new String[line.size()];
					writer.writeNext(line.toArray(lines));
				}
			}
						
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
