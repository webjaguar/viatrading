/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 08.20.2009
 */

package com.webjaguar.thirdparty.shopworks;

import java.io.File;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class ShopworksListController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();
		
		File baseFile = new File(getServletContext().getRealPath("/admin/export/order/"));
		
		File file[] = baseFile.listFiles();
		Arrays.sort( file, new Comparator<File>() {
			public int compare(File o1, File o2) {
				if (((File)o1).lastModified() > ((File)o2).lastModified()) {
					return -1;
				} else if (((File)o1).lastModified() < ((File)o2).lastModified()) {
					return +1;
				} else {
					return 0;
				}
			}
		}); 
		int totalNumFiles = file.length;
		
		List<Map> exportedFiles = new ArrayList<Map>();
		if (totalNumFiles > 0) {
			for (int f=0; f<45 && f<=totalNumFiles-1; f++) {
				if (file[f].getName().startsWith("invoiceExport_shopworks_") & file[f].getName().endsWith(".txt")) {
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					exportedFiles.add( fileMap );				
				}
			}
		}
		model.put("exportedFiles", exportedFiles);
		// index.html
		model.put("totalNumFiles", totalNumFiles-1);

		return new ModelAndView("admin/orders/shopworks", model);
	}
}