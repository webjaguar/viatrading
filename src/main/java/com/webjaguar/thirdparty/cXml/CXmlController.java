package com.webjaguar.thirdparty.cXml;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.util.WebUtils;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.listener.SessionListener;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CreditCard;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Product;
import com.webjaguar.model.UserSession;
import com.webjaguar.thirdparty.cXml.cXmlAPI.CXML;
import com.webjaguar.thirdparty.cXml.cXmlAPI.Extrinsic;
import com.webjaguar.thirdparty.cXml.cXmlAPI.Header;
import com.webjaguar.thirdparty.cXml.cXmlAPI.ItemDetail;
import com.webjaguar.thirdparty.cXml.cXmlAPI.ItemOut;
import com.webjaguar.thirdparty.cXml.cXmlAPI.OrderRequest;
import com.webjaguar.thirdparty.cXml.cXmlAPI.OrderRequestHeader;
import com.webjaguar.thirdparty.cXml.cXmlAPI.PunchOutSetupRequest;
import com.webjaguar.thirdparty.cXml.cXmlAPI.PunchOutSetupResponse;
import com.webjaguar.thirdparty.cXml.cXmlAPI.Request;
import com.webjaguar.thirdparty.cXml.cXmlAPI.Response;
import com.webjaguar.thirdparty.cXml.cXmlAPI.StartPage;

public class CXmlController extends WebApplicationObjectSupport implements Controller{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
    
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
			siteConfig = this.webJaguar.getSiteConfig();
			gSiteConfig = this.globalDao.getGlobalSiteConfig();
			
			CXML cXml = null;
		    Writer writer = new StringWriter();
	    	char[] buffer = new char[1024];
	        try {
	        	Reader reader = new BufferedReader(new InputStreamReader(request.getInputStream(), "UTF-8"));
	            int n;
	            while ((n = reader.read(buffer)) != -1) {
	            	writer.write(buffer, 0, n);
	            }
	        } catch(Exception e) { e.printStackTrace(); }
	            
	     	System.out.println("XML: "+writer.toString());
	        JAXBContext context = null;
	    	Unmarshaller unmarshaller = null;
	    	ByteArrayInputStream bis;
	    	try {
				context = JAXBContext.newInstance(CXML.class);
				unmarshaller = context.createUnmarshaller();
				bis = new ByteArrayInputStream(writer.toString().getBytes("UTF-8"));
				cXml = (CXML) unmarshaller.unmarshal(bis);
			} catch (Exception e) { e.printStackTrace(); }
			
			com.webjaguar.thirdparty.cXml.cXmlAPI.Header header = (Header) cXml.getHeaderOrMessageOrRequestOrResponse().get(0);
			com.webjaguar.thirdparty.cXml.cXmlAPI.Request cXmlRequest = (Request) cXml.getHeaderOrMessageOrRequestOrResponse().get(1);
			
			Object object = cXmlRequest.getProfileRequestOrOrderRequestOrMasterAgreementRequestOrPurchaseRequisitionRequestOrPunchOutSetupRequestOrProviderSetupRequestOrStatusUpdateRequestOrGetPendingRequestOrSubscriptionListRequestOrSubscriptionContentRequestOrSupplierListRequestOrSupplierDataRequestOrSubscriptionStatusUpdateRequestOrCopyRequestOrCatalogUploadRequestOrAuthRequestOrDataRequestOrOrganizationDataRequest().get(0);

			PunchOutSetupRequest punchOutSetupRequest = null;
			OrderRequest orderRequest = null;
			
			if(object.getClass().getName().equals(com.webjaguar.thirdparty.cXml.cXmlAPI.PunchOutSetupRequest.class.getName())) {
				punchOutSetupRequest = (PunchOutSetupRequest) cXmlRequest.getProfileRequestOrOrderRequestOrMasterAgreementRequestOrPurchaseRequisitionRequestOrPunchOutSetupRequestOrProviderSetupRequestOrStatusUpdateRequestOrGetPendingRequestOrSubscriptionListRequestOrSubscriptionContentRequestOrSupplierListRequestOrSupplierDataRequestOrSubscriptionStatusUpdateRequestOrCopyRequestOrCatalogUploadRequestOrAuthRequestOrDataRequestOrOrganizationDataRequest().get(0);
			} else if(object.getClass().getName().equals(com.webjaguar.thirdparty.cXml.cXmlAPI.OrderRequest.class.getName())){
				orderRequest = (OrderRequest) cXmlRequest.getProfileRequestOrOrderRequestOrMasterAgreementRequestOrPurchaseRequisitionRequestOrPunchOutSetupRequestOrProviderSetupRequestOrStatusUpdateRequestOrGetPendingRequestOrSubscriptionListRequestOrSubscriptionContentRequestOrSupplierListRequestOrSupplierDataRequestOrSubscriptionStatusUpdateRequestOrCopyRequestOrCatalogUploadRequestOrAuthRequestOrDataRequestOrOrganizationDataRequest().get(0);
			}
			
			if(punchOutSetupRequest != null){
				this.handlePunchOutRequest(request, response, cXmlRequest, header, context, punchOutSetupRequest);
			}
			if(orderRequest != null){
				// save XML to a document to root directory/punchOutPurchaseOrder
				this.savePurchaseOrder(request, writer.toString());
				this.handleOrderRequest(request, response, cXmlRequest, header, context, orderRequest);
			
			}
			
			return null;
	}
	
	private void handlePunchOutRequest(HttpServletRequest request, HttpServletResponse response, com.webjaguar.thirdparty.cXml.cXmlAPI.Request cXmlRequest, com.webjaguar.thirdparty.cXml.cXmlAPI.Header header, JAXBContext context, PunchOutSetupRequest punchOutSetupRequest) throws IOException{
		
		com.webjaguar.thirdparty.cXml.cXmlAPI.URL url =  new com.webjaguar.thirdparty.cXml.cXmlAPI.URL();
		
		//append jSession Id to keep session in the subsequent requests
		// IMPORTANT:  Make sure to have following rule on .htaccess: 
		// RewriteRule ^cxml/(.*)/$ viewCart.jhtm?jsId=$1 [QSA]
		url.setvalue(siteConfig.get("SECURE_URL").getValue()+"cxml/"+request.getSession().getId()+"/");
		
		// save cXML values to session
		request.getSession().setAttribute("jsId", request.getSession().getId());
		request.getSession().setAttribute("browserFormPost", punchOutSetupRequest.getBrowserFormPost().getURL().getvalue());
		request.getSession().setAttribute("buyerCookie", punchOutSetupRequest.getBuyerCookie().getContent().get(0).toString());
		request.getSession().setAttribute("cXmlHeader", header);
		StartPage startPage = new StartPage();
		startPage.setURL(url);
		
		PunchOutSetupResponse punchOutResponse = new PunchOutSetupResponse();
		punchOutResponse.setStartPage(startPage);
		
		Response cXmlResponse = new Response();
		com.webjaguar.thirdparty.cXml.cXmlAPI.Status status = new com.webjaguar.thirdparty.cXml.cXmlAPI.Status();
		status.setCode("200");
		status.setText("success");
		
		cXmlResponse.setStatus(status);
		cXmlResponse.getProfileResponseOrPunchOutSetupResponseOrProviderSetupResponseOrGetPendingResponseOrSubscriptionListResponseOrSubscriptionContentResponseOrSupplierListResponseOrSupplierDataResponseOrAuthResponseOrDataResponseOrOrganizationDataResponse().add(punchOutResponse);
		
		CXML responseCXml = new CXML();
		responseCXml.setPayloadID(""+new Date().getTime());
		java.text.Format formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss-hh:mm");
		
		responseCXml.setTimestamp(""+formatter.format(new Date()));
		responseCXml.getHeaderOrMessageOrRequestOrResponse().add(cXmlResponse);
		responseCXml.setVersion("1.2.014");
		responseCXml.setXmlLang("en-US");
		
		this.createSession(request, response, punchOutSetupRequest.getExtrinsic());
		try {
			OutputStream oStream = response.getOutputStream();
			
			context = JAXBContext.newInstance(CXML.class);
			context.createMarshaller().marshal(responseCXml, oStream);
			oStream.close();
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	private void handleOrderRequest(HttpServletRequest request, HttpServletResponse response, com.webjaguar.thirdparty.cXml.cXmlAPI.Request cXmlRequest, com.webjaguar.thirdparty.cXml.cXmlAPI.Header header, JAXBContext context, OrderRequest orderRequest) throws IOException{
		
		Response cXmlResponse = new Response();
		com.webjaguar.thirdparty.cXml.cXmlAPI.Status status = new com.webjaguar.thirdparty.cXml.cXmlAPI.Status();
		status.setCode("200");
		status.setText("success");
		
		cXmlResponse.setStatus(status);
		
		CXML responseCXml = new CXML();
		responseCXml.setPayloadID(""+new Date().getTime());
		java.text.Format formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss-hh:mm");
		
		responseCXml.setTimestamp(""+formatter.format(new Date()));
		responseCXml.getHeaderOrMessageOrRequestOrResponse().add(cXmlResponse);
		responseCXml.setVersion("1.2.014");
		responseCXml.setXmlLang("en-US");
		
		Order order = this.createOrder(request, response, orderRequest);
		OrderStatus orderStatus = new OrderStatus();
		orderStatus.setStatus("p");
		orderStatus.setComments("Added By Punchout");
		
		this.webJaguar.insertOrder(order, orderStatus, false, true, 0, siteConfig, gSiteConfig);
		try {
			OutputStream oStream = response.getOutputStream();
			
			context = JAXBContext.newInstance(CXML.class);
			context.createMarshaller().marshal(responseCXml, oStream);
			oStream.close();
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	private void createSession(HttpServletRequest request, HttpServletResponse response, List<Extrinsic> extrinsicList){
		
		Customer customer = null;
		boolean customerExist = false;
		
		UserSession userSession = new UserSession();
		
		for(Extrinsic extrinsic : extrinsicList) {
		
			if(extrinsic.getName().equals("UserEmail")) {
				String emails[] = extrinsic.getContent().get(0).toString().split(",");
				if(emails != null) {
					for(int i=0; i< emails.length; i++) {
						customer = this.webJaguar.getCustomerByUsername(emails[i]);
						if(customer != null){
							break;
						}
					}
					
					if(customer != null) {
						customerExist = true;
						userSession.setUserid(customer.getId());
						userSession.setFirstName(customer.getAddress().getFirstName());
						userSession.setLastName(customer.getAddress().getLastName());
						userSession.setUsername(customer.getUsername());
					} else {
						customer = new Customer();
						customer.setUsername(emails[0]);
						if(emails.length > 1) {
							customer.setExtraEmail(emails[1]);
						}
					}
				}
				break;
			}
		}
		
		if(!customerExist) {
			Address address = new Address();
			for(Extrinsic extrinsic : extrinsicList) {
				if(extrinsic.getName().equals("FirstName")) {
					address.setFirstName(extrinsic.getContent().get(0).toString());
				}
				if(extrinsic.getName().equals("LastName")) {
					address.setLastName(extrinsic.getContent().get(0).toString());
				}
			}
			customer.setAddress(address);
			customer.setPassword("1234567890");
			this.webJaguar.insertCustomer(customer, address, true);
			
			userSession.setUserid(customer.getId());
			userSession.setFirstName(customer.getAddress().getFirstName());
			userSession.setLastName(customer.getAddress().getLastName());
		}
		
		this.webJaguar.setUserSession(request, userSession);
		
		SessionListener.getHttpSessionIdMap().put(request.getSession().getId(), request.getSession());
		SessionListener.getActiveSessions().put(request.getSession().getId(), userSession.getUserid());
	}
	
	
	
	private Order createOrder(HttpServletRequest request, HttpServletResponse response, OrderRequest orderRequest){
		
		Customer customer = null;
		Order order = new Order();
		//status
		order.setStatus("p");
		
		if(orderRequest.getOrderRequestHeader() != null) {
			
			order.setSubTotal(Double.parseDouble(orderRequest.getOrderRequestHeader().getTotal().getMoney().getvalue()));
			
			//get customer email and check if customer already exist on our database
			// if not, creat new customer
			if(orderRequest.getOrderRequestHeader().getShipTo() != null) {
				String email = null;
				
				if(orderRequest.getOrderRequestHeader().getShipTo().getAddress() != null && orderRequest.getOrderRequestHeader().getShipTo().getAddress().getEmail() != null) {
					email = orderRequest.getOrderRequestHeader().getShipTo().getAddress().getEmail().getvalue().trim();
					customer = this.webJaguar.getCustomerByUsername(email);
				}
				
				if(email != null && customer == null) {
					customer = new Customer();
					customer.setUsername(email);
					
					if(orderRequest.getOrderRequestHeader().getShipTo().getAddress() != null){
						customer.setAddress(this.getAddressFromCXML(orderRequest.getOrderRequestHeader().getShipTo().getAddress(), null, null, null));
					}	
					customer.setPassword("1234567890");
					this.webJaguar.insertCustomer(customer, customer.getAddress(), true);
				}
			}
			order.setUserEmail(customer.getUsername());
			order.setUserId(customer.getId());
			order.setShipping(this.getAddressFromCXML(orderRequest.getOrderRequestHeader().getShipTo().getAddress(), customer.getAddress().getFirstName(), customer.getAddress().getLastName(),  customer.getAddress().getCompany()));
			order.setBilling(this.getAddressFromCXML(orderRequest.getOrderRequestHeader().getBillTo().getAddress(), customer.getAddress().getFirstName(), customer.getAddress().getLastName(), customer.getAddress().getCompany()));
			
			
			//shipping
			if(orderRequest.getOrderRequestHeader().getShipping() != null) {
				//shipping method
				order.setShippingMethod(orderRequest.getOrderRequestHeader().getShipping().getDescription().getvalue());
				if(order.getShippingMethod().contains("FedEx")){
					order.setShippingCarrier("FedEx");
				} else if(order.getShippingMethod().contains("UPS")){
					order.setShippingCarrier("UPS");
				} else if(order.getShippingMethod().contains("USPS")){
					order.setShippingCarrier("USPS");
				}
				
				//shipping cost
				try {
					order.setShippingCost(Double.parseDouble(orderRequest.getOrderRequestHeader().getShipping().getMoney().getvalue()));
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			//tax
			if(orderRequest.getOrderRequestHeader().getTax() != null) {
				try {
					order.setTax(Double.parseDouble(orderRequest.getOrderRequestHeader().getTax().getMoney().getvalue()));
				} catch(Exception e){
					e.printStackTrace();
				}
			}
			
			//payment
			if(orderRequest.getOrderRequestHeader().getPayment() != null) {
				
				//payment card
				if(orderRequest.getOrderRequestHeader().getPayment().getPCard() != null){
					order.setPaymentMethod("Credit Card");
					
					CreditCard creditCard = new CreditCard();
					creditCard.setNumber(orderRequest.getOrderRequestHeader().getPayment().getPCard().getNumber());
					
					String ccExpDate = orderRequest.getOrderRequestHeader().getPayment().getPCard().getExpiration();
					creditCard.setExpireYear(ccExpDate.split("-")[0]);
					creditCard.setExpireMonth(ccExpDate.split("-")[1]);
					
					order.setCreditCard(creditCard);
				}
			}
		}
		
		
		//line items
		if(orderRequest.getItemOut() != null && !orderRequest.getItemOut().isEmpty()){
			List<LineItem> lineItems = new ArrayList<LineItem>();
			LineItem lineItem = null;
			for(ItemOut itemOut : orderRequest.getItemOut()){
				lineItem = new LineItem();
				
				lineItem.setQuantity(Integer.parseInt(itemOut.getQuantity()));
				lineItem.setLineNumber(Integer.parseInt(itemOut.getLineNumber()));
				
				for(Object itemDetailObject : itemOut.getItemDetailOrBlanketItemDetail()) {
					ItemDetail itemDetail = (ItemDetail) itemDetailObject;
					lineItem.setUnitPrice(Double.parseDouble(itemDetail.getUnitPrice().getMoney().getvalue()));
				}
				
				try {
					int productId = this.webJaguar.getProductIdBySku(itemOut.getItemID().getSupplierPartID());
					Product product = this.webJaguar.getProductById(productId, 0, false, customer);
					lineItem.setProduct(product);
				}catch(Exception e){
					e.printStackTrace();
				}
				lineItems.add(lineItem);
			}
			
			order.setLineItems(lineItems);
			
		}
		order.setGrandTotal();
		return order;
	}
	
	
	private void savePurchaseOrder(HttpServletRequest request, String xmlString) throws Exception {
    	File punchOutDir = new File(getServletContext().getRealPath("/punchOutPurchaseOrder"));
		if (!punchOutDir.exists()) {
			punchOutDir.mkdir();
		}
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
		File poFile = new File(punchOutDir, "purchase_order"+dateFormatter.format(new Date())+"");
    	FileOutputStream fo = new FileOutputStream(poFile, true);
    	PrintWriter pw = new PrintWriter(fo);
		
		pw.write(xmlString);
		pw.close();
    	fo.close();
    	System.out.println("Purchase Order Saved");
    }
    private Address getAddressFromCXML(com.webjaguar.thirdparty.cXml.cXmlAPI.Address cXmlAddress, String firstName, String lastName, String company){
		Address address = new Address();
		address.setFirstName(firstName);
		address.setLastName(lastName);
		address.setCompany(company);
		if(cXmlAddress.getPostalAddress() != null){
			// street
			if(cXmlAddress.getPostalAddress().getStreet() != null){
				if(cXmlAddress.getPostalAddress().getStreet().get(0) != null){
					address.setAddr1(cXmlAddress.getPostalAddress().getStreet().get(0).getvalue());
				}
			}
				
			// city
			if(cXmlAddress.getPostalAddress().getCity() != null){
				address.setCity(cXmlAddress.getPostalAddress().getCity());
			}
				
			// state
			if(cXmlAddress.getPostalAddress().getState() != null){
				address.setStateProvince(cXmlAddress.getPostalAddress().getState());
			}
				
			//country
			if(cXmlAddress.getPostalAddress().getCountry() != null){
				address.setCountry(cXmlAddress.getPostalAddress().getCountry().getIsoCountryCode());
			}
				
			//zip
			if(cXmlAddress.getPostalAddress().getPostalCode() != null){
				address.setZip(cXmlAddress.getPostalAddress().getPostalCode());
			}
		}
			
		//phone
		if(cXmlAddress.getPhone() != null){
			address.setPhone(cXmlAddress.getPhone().getTelephoneNumber().getCountryCode().getvalue()
							+cXmlAddress.getPhone().getTelephoneNumber().getAreaOrCityCode()
							+cXmlAddress.getPhone().getTelephoneNumber().getNumber()
							);
		}
		return address;
	}
}