/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.18.2009
 */

package com.webjaguar.thirdparty.etilize;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import spexlive.etilize.com.Catalog;
import spexlive.etilize.com.CatalogServiceIntf;
import spexlive.etilize.com.GetManufacturers;

import com.webjaguar.model.Configuration;

public class ManufacturerController implements Controller {
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {    	

    	Map<String, Object> map = new HashMap<String, Object>();
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
    	
    	String appId = siteConfig.get("ETILIZE").getValue();

    	if (appId.trim().length() > 0) {
        	try {
        		String wsdlLocation = "http://ws.spexlive.net/service/soap/catalog?appId=" + siteConfig.get("ETILIZE").getValue() + "&wsdl";
        		Catalog service = new Catalog(new URL(wsdlLocation), new QName("http://com.etilize.spexlive", "catalog"));
        		CatalogServiceIntf port = service.getCatalogHttpPort();
        		
        		GetManufacturers getManufacturers = new GetManufacturers();
        		getManufacturers.setCatalog("na");
        		getManufacturers.setSiteId(0);
        		
        		map.put("manufacturers", port.getManufacturers(getManufacturers));
        		
        	} catch (Exception e) {
        		//System.out.println(e.toString());
        	}     		
    	}
    	
    	ModelAndView modelAndView = new ModelAndView("admin/vendors/etilize/manufacturers", map);
    	
        return modelAndView;
    }

}
