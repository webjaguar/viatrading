/* Copyright 2011 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 12.20.2011
 */

package com.webjaguar.thirdparty.etilize;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.DoubleField;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ProductSearch;

public class LuceneIndexer extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> map = new HashMap<String, Object>();
		
		if (!siteConfig.get("LUCENE_REINDEX").getValue().equals("true")) {
			// check if enabled
			return null;
		}
		
		boolean isAEM = false;
		boolean isADMIN = false;
		for (GrantedAuthority role: SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
			if (role.getAuthority().equals("ROLE_AEM")) isAEM = true;
			if (role.getAuthority().startsWith("ROLE_ADMIN_")) isADMIN = true;
		}
		
		if (request.getParameter("now") != null && (isAEM || isADMIN)) {
			map.put("results", reIndex((Map<String, Object>) request.getAttribute("gSiteConfig")));
		} else {
			// TO BE DELETED
			// No need to set value. It was not working before with schedular. So we removed this condition.
			// schedule re-indexing
			this.webJaguar.updateSiteConfig("LUCENE_REINDEX", "true");
		}
				
		return new ModelAndView("admin/catalog/product/reIndex", map);
	}
	
	private String reIndex(Map<String, Object> gSiteConfig) throws Exception {
		ProductSearch search = new ProductSearch();
		
		StringBuffer sbuff = new StringBuffer();
		
		int productCount = this.webJaguar.getEtilizeLuceneProductCount();
		sbuff.append(productCount + "\n");
		int limit = 5000;
		search.setLimit(limit);
		
		File baseDir = new File(getServletContext().getRealPath("/etilize/lucene_index/"));
		if (!baseDir.exists()) {
			baseDir.mkdir();			
		}
		
		File indexDir = new File(baseDir, "products");
		if (indexDir.exists()) {
			if(indexDir.isDirectory()) {
				for(String fileName : indexDir.list()) {
					new File(indexDir, fileName).delete();
				}
			}
		}
		indexDir.mkdir();			
		
		Directory fsDir = FSDirectory.open(indexDir);
		Date start = new Date();
		
		StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_CURRENT, analyzer);
		IndexWriter writer = new IndexWriter(fsDir, config);
		
		for (int offset=0; offset<productCount;) {

			if (offset > 0) sbuff.append(", ");
			sbuff.append(offset);				
			
			for (Map<String, Object> product: this.webJaguar.getEtilizeLuceneProduct(limit, offset)) {
				Document doc = new Document();
				doc.add(new Field("id", product.get("id").toString(), TextField.TYPE_STORED));
				doc.add(new Field("sku", product.get("sku").toString(), TextField.TYPE_STORED));
				doc.add(new Field("feed", product.get("feed") != null ? product.get("feed").toString() : "null", TextField.TYPE_STORED));
				doc.add(new Field("inventory", product.get("inventory") != null ? product.get("inventory").toString() : "null", TextField.TYPE_STORED));
				
				Double price1 = Double.parseDouble(product.get("price_1") != null ? product.get("price_1").toString() : "0.0");
				doc.add(new DoubleField("price_1", price1, DoubleField.TYPE_STORED));
				
				
				doc.add(new Field("upc", (product.get("upc") != null) ? product.get("upc").toString() : "null", TextField.TYPE_STORED));
				doc.add(new Field("etilize_id", (product.get("etilize_id") != null) ? product.get("etilize_id").toString() : "null", TextField.TYPE_STORED));
				writer.addDocument(doc);
			}
	    	
	        offset = offset + limit;
	        search.setOffset(offset);
		}
		
		
		Date end = new Date();
		sbuff.append("\n");
		sbuff.append("Start on " + start + "\n");
		sbuff.append("End on " + end + "\n");
		sbuff.append("Indexed a total of " + writer.maxDoc() + "\n");
	    sbuff.append(end.getTime() - start.getTime() + " total milliseconds" + "\n");
	    sbuff.append("---------------" + "\n");

	    writer.commit();
	    writer.close();
	    
	    return sbuff.toString();
	}
	
	public void autoIndex() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		try {
			reIndex(gSiteConfig);
		} catch (Exception e) {
			notifyAdmin("Etilize Lucene autoIndex() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
		}
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}