/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 08.17.2009
 */

package com.webjaguar.thirdparty.etilize;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TopFieldDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import spexlive.etilize.com.AttributeSearchFilter;
import spexlive.etilize.com.Catalog;
import spexlive.etilize.com.CatalogServiceIntf;
import spexlive.etilize.com.CategoryId;
import spexlive.etilize.com.Description;
import spexlive.etilize.com.ForceSortOrder;
import spexlive.etilize.com.GetCategories;
import spexlive.etilize.com.GetCategoryTree;
import spexlive.etilize.com.ManufacturerId;
import spexlive.etilize.com.ProductSummary;
import spexlive.etilize.com.Resource;
import spexlive.etilize.com.Search;
import spexlive.etilize.com.SearchCriteria;
import spexlive.etilize.com.SearchResult;
import spexlive.etilize.com.SelectFilters;
import spexlive.etilize.com.SelectProductFields;
import spexlive.etilize.com.Sku;
import spexlive.etilize.com.SubcatalogFilter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductImage;

public class SearchController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {    	

    	Map<String, Object> model = new HashMap<String, Object>();
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
    	
    	String appId = siteConfig.get("ETILIZE").getValue();
    	if (appId.trim().length() > 0) {
        	try {
        		String wsdlLocation = "http://ws.spexlive.net/service/soap/catalog?appId=" + siteConfig.get("ETILIZE").getValue() + "&wsdl";
        		Catalog service = new Catalog(new URL(wsdlLocation), new QName("http://com.etilize.spexlive", "catalog"));
        		CatalogServiceIntf port = service.getCatalogHttpPort();
        		
        		Search search = new Search();
        		search.setCatalog("na");
        		search.setSiteId(0);
        		search.setPageNo(ServletRequestUtils.getIntParameter(request, "pageNo", 1));
        		search.setPageSize(ServletRequestUtils.getIntParameter(request, "pageSize", 10));

        		// sorting
        		String sort = ServletRequestUtils.getStringParameter(request, "sort", "");
        		if (sort.length() > 0) {
        			ForceSortOrder sortOrder = new ForceSortOrder();
            		sortOrder.setListName("allitems");
            		if (sort.equalsIgnoreCase("-price")) {
                		sortOrder.setAscending(false);            			
            		}
            		sortOrder.setForce(true);
            		search.setSortOrder(sortOrder);
        		}

        		// SearchCriteria
        		SearchCriteria searchCriteria = new SearchCriteria();
        		search.setCriteria(searchCriteria);

        		// keyword filter
        		String keywords = ServletRequestUtils.getStringParameter(request, "keywords", "");
        		if (keywords.trim().length() > 0) {
            		searchCriteria.setKeywordFilter(keywords);        			
        		}

        		// ManufacturerFilter
        		int manufacturerId = ServletRequestUtils.getIntParameter(request, "manufacturerId", 0);
        		if (manufacturerId > 0) {
        			ManufacturerId manufacturerFilter = new ManufacturerId();
        			manufacturerFilter.setId(manufacturerId);
        			searchCriteria.getManufacturerFilter().add(manufacturerFilter);
        		}
        		
        		// CategoryFilter
        		int categoryId = ServletRequestUtils.getIntParameter(request, "categoryId", 0);
        		int parId = ServletRequestUtils.getIntParameter(request, "parId", 1);
        		boolean categoryIdProcessed = false;
        		if (categoryId > 0) {
        			CategoryId categoryFilter = new CategoryId();
        			categoryFilter.setId(categoryId);
            		searchCriteria.getCategoryFilter().add(categoryFilter); 
            		categoryIdProcessed = true;
        		} else if (parId > 1) {
        			CategoryId categoryFilter = new CategoryId();
        			categoryFilter.setId(parId);
            		searchCriteria.getCategoryFilter().add(categoryFilter);        	
            		categoryIdProcessed = true;
        		}	
        		
        		// display
        		String display = ServletRequestUtils.getStringParameter(request, "display", "");	
        		String subcatalogFilterContent = "allitems";
        		if (display.trim().length() > 0) {
           			File assets = new File(getServletContext().getRealPath("/assets/"));
        			Properties prop = new Properties();
        			try {
        				prop.load( new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
        				if (prop.get("site.root") != null) {
        					assets = new File((String) prop.get("site.root"), "assets");
        				}
        			} catch (Exception e) {}  
        			File etilizeMapping = new File(assets, "etilizeMapping.xml");
        			if (etilizeMapping.exists()) {
                		try {
                			SAXBuilder builder = new SAXBuilder(false);
                			Document doc = builder.build(etilizeMapping);
                			
                			Iterator<Element> displays = doc.getRootElement().getChildren("Display").iterator();
                			while (displays.hasNext()) {
                				Element displayE = (Element) displays.next();
                				if (displayE.getAttributeValue("code").equals(display)) {
                					model.put("displayName", displayE.getAttributeValue("name"));
                					if (categoryIdProcessed == false) {
                        				Element categoryFilterE = displayE.getChild("CategoryFilter");
                        				if (categoryFilterE != null) {
                        					Iterator<Element> catIds = categoryFilterE.getChildren().iterator();
                        					while (catIds.hasNext()) {
                        	        			CategoryId categoryFilter = new CategoryId();
                        	        			categoryFilter.setId(Integer.parseInt(((Element) catIds.next()).getValue()));
                        	            		searchCriteria.getCategoryFilter().add(categoryFilter);                    						
                        					}
                        				}                					                						
                					}
                					Element subcatalogFilterE = displayE.getChild("SubcatalogFilter");
                					if (subcatalogFilterE != null) {
                						subcatalogFilterContent = subcatalogFilterE.getText();
                					}
                					break;
                				}
                			}
                		} catch (Exception e) {
                			// do nothing
                		}	
        			}        		
        		}
        		
        		// subcatalog
        		SubcatalogFilter subcatalogFilter = new SubcatalogFilter();
        		subcatalogFilter.setContent(subcatalogFilterContent);
        		searchCriteria.setSubcatalogFilter(subcatalogFilter);
        		
        		// AttributeFilter
        		List<String> attributes = new ArrayList<String>();
        		for (String attribute: ServletRequestUtils.getStringParameters(request, "attribute")) {
        			String attributeArray[] = attribute.split("[|]");
        			if (attributeArray.length == 4 && !attributes.contains(attribute)) {
            			try {
            				AttributeSearchFilter asf = new AttributeSearchFilter();
            				asf.setAttributeId(Long.parseLong(attributeArray[0]));
            				asf.setOperator(attributeArray[1].equals("") ? "EQ" : attributeArray[1]);
            				asf.setValueId(Integer.parseInt(attributeArray[2]));
                    		searchCriteria.getAttributeFilter().add(asf);			
            				attributes.add(attribute);
            			} catch (Exception e) {
            				// do nothing
            				e.printStackTrace();
            			}        				        				
        			}
        		}
        		model.put("attributes", attributes);

           		// SelectProductFields
        		SelectProductFields selectProductFields = new SelectProductFields();
        		selectProductFields.setDescriptions("all");
        		selectProductFields.setManufacturer("default");
        		selectProductFields.getResourceType().add("all"); 		
        		selectProductFields.getSkuType().add("all");
        		search.setSelectProductFields(selectProductFields);
        		
        		// SelectFilters
        		SelectFilters selectFilters = new SelectFilters();
        		selectFilters.setAttribute(20);
        		selectFilters.setCategory(20);
        		selectFilters.setManufacturer(20);
        		search.setSelectFilters(selectFilters);
        		
        		SearchResult searchResult = new SearchResult();
        		searchResult = port.search(search);
        	
        		model.put("searchResult", searchResult);
        		model.put("search", search);
        		
        		// category tree
        		if (parId > 1) {
            		try {
                		GetCategoryTree getCategoryTree = new GetCategoryTree();
                		getCategoryTree.setCatalog("na");
                		getCategoryTree.setSiteId(0);
                		getCategoryTree.setChildLevels(-1);
                		getCategoryTree.setSubcatalogFilter(subcatalogFilter);
                		getCategoryTree.setCategoryId(parId);
            			spexlive.etilize.com.Category category = port.getCategoryTree(getCategoryTree);
                		List<spexlive.etilize.com.Category> etilizeBreadCrumbs = new ArrayList<spexlive.etilize.com.Category>();    		
                		etilizeBreadCrumbs.add(category);
                		while (category.getParent() != null) {
                			category = category.getParent();
                			if (category.getId() > 1) {
                    			etilizeBreadCrumbs.add(0, category);    				
                			}
                		}
                		model.put("etilizeBreadCrumbs", etilizeBreadCrumbs);
            		} catch (Exception e) {
            			//System.out.println(e.toString());
            		}
        		}
        		
        		// category names
        		if (categoryId > 0) {
            		Map<String, spexlive.etilize.com.Category> categories = new HashMap<String, spexlive.etilize.com.Category>();
            		if (searchCriteria.getCategoryFilter().size() > 0) {
            			GetCategories getCategories = new GetCategories();
            			getCategories.setCatalog("na");
            			for (spexlive.etilize.com.Category category: port.getCategories(getCategories).getCategory()) {
            				categories.put(category.getId().toString(), category);
            			}
            			model.put("etilizeCategories", categories);
            		}        			
        		}
        		
        		// set navigation
        		setNavi(search, searchResult, model);
        		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss:SSSS a z");

        		// products
        		List<Product> products = new ArrayList<Product>();
        		Map<Integer, ProductSummary> productSummaryMap = new HashMap<Integer, ProductSummary>();
        		
        		int index = 0;
        		for (ProductSummary productSummary: searchResult.getProducts().getProductSummary()) {
        			Product product = null;
        			
        			// skus
        			List<Product> skuList = new ArrayList<Product>();
        			if (productSummary.getSkus() != null) {
                		for (Sku sku: productSummary.getSkus().getSku()) {
                			if (sku.getType().equalsIgnoreCase("Ingram Micro USA")) {
                				Product thisSku = new Product();
                				thisSku.setSku("IM-" + sku.getNumber());
                				thisSku.setFeed("INGRAM");
                				skuList.add(thisSku);
                			} else if (sku.getType().equalsIgnoreCase("Synnex")) {
                				Product thisSku = new Product();
                				thisSku.setSku("SYN-" + sku.getNumber());
                				thisSku.setFeed("SYNNEX");
                				skuList.add(thisSku);
                			} else if (sku.getType().equalsIgnoreCase("Tech Data")) {
                				Product thisSku = new Product();
                				thisSku.setSku("TD-" + sku.getNumber());
                				thisSku.setFeed("TECHDATA");
                				skuList.add(thisSku);
                			} else if (sku.getType().equalsIgnoreCase("DSI")) {
                				Product thisSku = new Product();
                				thisSku.setSku("DSI-" + sku.getNumber());
                				thisSku.setFeed("DSI");
                				skuList.add(thisSku);
                			} else if (sku.getType().equalsIgnoreCase("UPC")) {
                				Product thisSku = new Product();
                				thisSku.setUpc(sku.getNumber());
                				skuList.add(thisSku);
                			}
                		}
                		
                		// etilize id
                		Product thisSku = new Product();
                		thisSku.setEtilizeId(productSummary.getId());
                		skuList.add(thisSku); 
                	}
        			/*
        			if (skuList.size() > 0) {
        				System.out.print("skuList.size() start " + dateFormatter.format(new Date())  + " " );
        				List<Map<String, Object>> productList = this.webJaguar.getProductsBySkuList(skuList);
        				System.out.println("end " + dateFormatter.format(new Date()) + " - " + skuList.size());
        				String sku = null;
        				// check inventory
        				for (Map<String, Object> map: productList) {
        					if (map.get("inventory") != null && (Integer) map.get("inventory") > 0) {
        						if (map.get("price_1") != null && (Double) map.get("price_1") > 0) {
            						sku = (String) map.get("sku");
            						break;        							
        						}
        					}
        				}
        				if (sku == null && productList.size() > 0) {
            				sku = (String) productList.get(0).get("sku");
        				}
        				if (sku != null) {
        					product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(sku), request);
        				}
        			}*/
        			if (skuList.size() > 0) {
        				
        				TopFieldDocs exactDocs = null;
        				IndexReader reader = null;
        				IndexSearcher searcher = null;
        				try {
        					reader = IndexReader.open(FSDirectory.open(new File(getServletContext().getRealPath("/etilize/lucene_index/products"))));
        					searcher = new IndexSearcher(reader);
        					
        					Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_34);
        					QueryParser qp = new QueryParser(Version.LUCENE_34, "sku", analyzer);

        					String sortBy = ServletRequestUtils.getStringParameter(request, "sortBy", "price_1");
        					
        					Sort sort2 = new Sort();
        					SortField sf = new SortField("price_1", SortField.Type.DOUBLE, true);
        					if (sortBy.equals("price_1")) {
        						sort2 = new Sort(sf);
        					}
        					
        					StringBuffer queryString = new StringBuffer();
        					for(int i=0; i<skuList.size(); i++) {
        						queryString.append("( sku : \""+skuList.get(i).getSku()+ "\"");
            					queryString.append(" AND ( feed :\"" +skuList.get(i).getFeed()+ "\""  );
            					queryString.append(" OR upc :\""  +skuList.get(i).getUpc()+ "\""  );
            					queryString.append(" OR etilize_id :\""  +skuList.get(i).getEtilizeId()+ "\") )"  );
            					
            					if(i < skuList.size()-1) {
            						queryString.append(" OR ");
                				}
                			}
        					
        					//System.out.println("queryString "+queryString.toString());
        					Query query = null;
        					try {
        						query = qp.parse(queryString.toString());
        						exactDocs = searcher.search(query, 100000, sort2);			
        					} catch (Exception e) { }
        				} catch (Exception e) {
        					//System.out.println("Index not found exception");
        				}
        				
        				if (exactDocs != null) {
        					
        					ScoreDoc[] exactHits = exactDocs.scoreDocs;
        					
        					Integer productId = null;
        					//System.out.println("exactHits Count "+exactHits.length);				
        					
        					for (int i=0; i<exactHits.length; i++) {
        						org.apache.lucene.document.Document doc = searcher.doc(exactHits[i].doc);
        						productId = new Integer(doc.get("id"));
        						
        						if (doc.get("inventory") != null ) {
        							try {
        								Integer inventory = Integer.parseInt(doc.get("inventory"));
        								if(inventory > 0) {
        									if (doc.get("price_1") != null && Double.parseDouble(doc.get("price_1")) > 0) {
        										productId = new Integer(doc.get("id"));
                        						break;        							
                    						}
        								}
        							}catch(Exception e) { }
        						}
        					}
        					
        					if (productId == null && exactHits.length > 0) {
            					productId = new Integer( searcher.doc(exactHits[0].doc).get("id") );
            				}
            				if (productId != null) {
            					product = this.webJaguar.getProductById(productId, request);			
            				}
        				}
        			}
            		if (product == null) {
            			product = new Product();
            		}

            		// descriptions
            		if (productSummary.getDescriptions() != null) {
                		for (Description description: productSummary.getDescriptions().getDescription()) {
                			if (description.getType() == 2) {
                				product.setName(description.getContent());        				
                			} else if (description.getType() == 3) {
                				product.setShortDesc(description.getContent());        				
                			}
                		}            			
            		}
            		
            		// thumbnail
            		if (productSummary.getResources() != null) {
                		for (Resource resource: productSummary.getResources().getResource()) {
                			if (resource.getType().equalsIgnoreCase("Thumbnail")) {
        						ProductImage pi = new ProductImage();
        						pi.setImageUrl(resource.getUrl());
        						product.setThumbnail(pi);
                			}
                		}            			
            		}
            		
        			products.add(product);
            		productSummaryMap.put(index++, productSummary);
        		}

        		model.put("productSummaryMap", productSummaryMap);
        		model.put("products", products);

        	} catch (Exception e) {
        		//System.out.println(e.toString());
        	}     		
    	} else {
    		return null;
    	}
    	
    	ModelAndView modelAndView = new ModelAndView("frontend/etilize/search", "model", model);
    	//System.out.println("end " + new Date());
        return modelAndView;
    }
    
    private void setNavi(Search search, SearchResult searchResult, Map<String, Object> model) {
		int numOfPages = searchResult.getCount()/search.getPageSize();
		if (searchResult.getCount()%search.getPageSize() > 0) {
			numOfPages++;
		}
		model.put("numOfPages", numOfPages);        		
		
		int pageEnd = search.getPageNo() + 2;
		int pageStart = search.getPageNo() - 2;
		if (pageStart < 1) {
			pageStart = 1;
			pageEnd = 5;
		}
		if (pageEnd > numOfPages) {
			pageEnd = numOfPages; 
			if (pageEnd - 4 > 1) {
    			pageStart = pageEnd - 4;        				
			}
		}
		model.put("pageEnd", pageEnd); 
		model.put("pageStart", pageStart);   
		
		int viewingStart = 1;
		int viewingEnd = search.getPageSize();
		if (search.getPageNo() > 1) {
			viewingStart = search.getPageNo()*search.getPageSize() + 1;
			viewingEnd = search.getPageNo()*search.getPageSize() + search.getPageSize();	
		}
		if (viewingEnd > searchResult.getCount()) {
			viewingEnd = searchResult.getCount();
		}
		
		model.put("viewingStart", viewingStart);	
		model.put("viewingEnd", viewingEnd);	
    }

}
