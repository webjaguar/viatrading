/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.11.2009
 */

package com.webjaguar.thirdparty.etilize;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import spexlive.etilize.com.Catalog;
import spexlive.etilize.com.CatalogServiceIntf;
import spexlive.etilize.com.Category;
import spexlive.etilize.com.GetCategoryTree;
import spexlive.etilize.com.SubcatalogFilter;

import com.webjaguar.model.Configuration;

public class CategoryController implements Controller {
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {    	

    	//Map<String, Object> model = new HashMap<String, Object>();
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
    	
    	List<Category> categories = new ArrayList<Category>(); 
    	
    	String appId = siteConfig.get("ETILIZE").getValue();

    	if (appId.trim().length() > 0) {
        	try {
        		String wsdlLocation = "http://ws.spexlive.net/service/soap/catalog?appId=" + siteConfig.get("ETILIZE").getValue() + "&wsdl";
        		Catalog service = new Catalog(new URL(wsdlLocation), new QName("http://com.etilize.spexlive", "catalog"));
        		CatalogServiceIntf port = service.getCatalogHttpPort();
        		
        		// subcatalog
        		SubcatalogFilter subcatalogFilter = new SubcatalogFilter();
        		subcatalogFilter.setContent("allitems");
        		
        		GetCategoryTree getCategoryTree = new GetCategoryTree();
        		getCategoryTree.setCatalog("na");
        		getCategoryTree.setSiteId(0);
        		getCategoryTree.setChildLevels(-1);
        		if (request.getParameter("full") == null) {
            		getCategoryTree.setSubcatalogFilter(subcatalogFilter);        			
        		}
        		getCategoryTree.setCategoryId(1);
        		for (Category category: port.getCategoryTree(getCategoryTree).getChildren().getCategory()) {
        			categories.add(category);
        		}
        	} catch (Exception e) {
        		//System.out.println(e.toString());
        	}     		
    	}
    	
    	ModelAndView modelAndView = new ModelAndView("admin/vendors/etilize/category", "categories", categories);
    	
        return modelAndView;
    }

}
