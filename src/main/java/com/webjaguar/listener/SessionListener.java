/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.13.2009
 */

package com.webjaguar.listener;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionEvent;

public class SessionListener implements HttpSessionListener {

	private static Map<String, Integer> activeSessions = new HashMap<String, Integer>();
	private static Map<String, HttpSession> httpSessionIdMap = new HashMap<String, HttpSession>();

	public void sessionCreated(HttpSessionEvent se) {
		activeSessions.put(se.getSession().getId(), null);
		httpSessionIdMap.put(se.getSession().getId(), se.getSession());
	}

	public void sessionDestroyed(HttpSessionEvent se) {
		activeSessions.remove(se.getSession().getId());
		httpSessionIdMap.remove(se.getSession().getId());
	}

	public static Map<String, Integer> getActiveSessions() {
		return activeSessions;
	}
	
	public static Map<String, HttpSession> getHttpSessionIdMap() {
		return httpSessionIdMap;
	}
}