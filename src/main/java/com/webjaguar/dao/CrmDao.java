package com.webjaguar.dao;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;

import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Search;
import com.webjaguar.model.crm.CrmAccount;
import com.webjaguar.model.crm.CrmAccountSearch;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactGroup;
import com.webjaguar.model.crm.CrmContactGroupSearch;
import com.webjaguar.model.crm.CrmContactField;
import com.webjaguar.model.crm.CrmContactSearch;
import com.webjaguar.model.crm.CrmForm;
import com.webjaguar.model.crm.CrmQualifier;
import com.webjaguar.model.crm.CrmQualifierSearch;
import com.webjaguar.model.crm.CrmTask;
import com.webjaguar.model.crm.CrmTaskSearch;


public interface CrmDao {
	// Account
	Integer getCrmAccountListCount(CrmAccountSearch search);
	List<CrmAccount> getCrmAccountList(CrmAccountSearch search);
	List<CrmAccount> getCrmAccountNameList();
	CrmAccount getCrmAccountById(Integer accountId);
	CrmAccount getCrmAccountByName(String accountName);
	String getCrmAccountNameById(Integer accountId);
	void deleteCrmAccount(Integer accountId);
	void insertCrmAccount(CrmAccount crmAccount);
	void updateCrmAccount(CrmAccount crmAccount);
	boolean isCrmAccountExist(String accountName);
	void importCrmAccounts(List<CrmAccount> crmAccounts);
	Integer getCrmAccountId(String accountName, String accountNumber);
	List<CrmAccount> getCrmAccountAjax(String accountName);
	
	// Qualifier
	Integer getCrmQualifierListCount(CrmQualifierSearch search);
	List<CrmQualifier> getCrmQualifierList(CrmQualifierSearch search);
	void insertCrmQualifier(CrmQualifier crmQualifier);
	void deleteCrmQualifier(Integer qualifierId);
	void updateCrmQualifier(CrmQualifier crmQualifier);
	
	// Contact
	Integer getCrmContactListCount(CrmContactSearch search);
	List<Integer> getCrmContactIdsList(CrmContactSearch search);
	List<CrmContact> getCrmContactList(CrmContactSearch search);
	List<CrmContact> getCrmContactList2(CrmContactSearch search);
	List<CrmContact> getCrmContactExport(CrmContactSearch search);
	CrmContact getCrmContactById(Integer contactId);
	CrmQualifier getCrmQualifierById(Integer qualifierId);
	void deleteCrmContact(Integer contactId);
	void insertCrmContact(CrmContact crmContact);
	void updateCrmContact(CrmContact crmContact);
	void updateCrmTrackCode(Integer customerId,String trackCode);
	void updateCrmQualifier(Integer customerId,String qualifier);
	List<CrmContact> getCrmContactByEmail1(String email1);
	void insertCrmContactField(CrmContactField crmContactField);
	List<CrmContactField> getCrmContactFieldList(Search search);
	CrmContactField getCrmContactFieldById(Integer fieldId);
	CrmContactField getCrmContactFieldByGlobalName(String fieldName);
	void updateCrmContactField(CrmContactField crmContactField);
	void deleteCrmContactField(Integer fieldId);
	String createToken(Integer contactId);
	void convertCrmContactToCustomer(Integer contactId, Integer userId, String leadSource);
	Map<String, String> getContcatCustomerMap(String key);
	void updateCustomerMap(List<Configuration> data);
	void unsubscribeByCrmContactId(Integer crmContactId);
	Integer getFieldValueCountByFieldIdAndValue(Integer fieldId, String fieldValue);
	boolean validCrmContactToken(String token);
	void batchAssignUserToContacts(List<Integer> contactIdList, AccessUser accessUser);
	void removeUserFromContacts(List<Integer> contactIds);
	void batchCrmContactDelete(Set<Integer> contactIds);
	void batchCrmRating(List<Integer> contactIds, String rating);
	void batchCrmRating1(List<Integer> contactIds, String rating);
	void batchCrmRating2(List<Integer> contactIds, String rating);
	int getCrmContactId(String email);
	
	void batchCrmQualifier(List<Integer> contactIds, String qualifier);
	void batchCrmLanguage(List<Integer> contactIds, String qualifier);
	// group
	List<CrmContactGroup> getContactGroupList(CrmContactGroupSearch search);
	List<CrmContactGroup> getContactGroupNameList();
	CrmContactGroup getContactGroupById(Integer groupId);
	void deleteCrmContactGroupById(Integer groupId);	
	void insertCrmContactGroup(CrmContactGroup group) throws DataAccessException;
	void updateCrmContactGroup(CrmContactGroup group);
	List<Integer> getContactGroupIdList(Integer contactId);
	void batchCrmContactGroupIds(Integer contactId, Set groupIds, String action);
	
	// Task
	Integer getCrmTaskListCount(CrmTaskSearch search);
	List<CrmTask> getCrmTaskList(CrmTaskSearch search);
	CrmTask getCrmTaskById(Integer taskId);
	void insertCrmTask(CrmTask crmTask);
	void updateCrmTask(CrmTask crmTask);
	List<CrmTask> getCrmTaskExport(CrmTaskSearch search);
	void deleteCrmTask(Integer taskId);
	void batchCrmTaskDelete(Set<Integer> taskIds);
	
	// Form
	List<CrmForm> getCrmFormList(Search search);
	CrmForm getCrmFormById(Integer formId);
	CrmForm getCrmFormByFormNumber(String formNumber);
	void deleteCrmForm(Integer formId);
	void insertCrmForm(CrmForm crmForm);
	void updateCrmForm(CrmForm crmForm);
	
	void insertCrmTime(String emailAddress, Long crmTime);
	Long getCrmTime(String emailAddress);
	Long getDuplicateEntry(String emailAddress);
	void updateCrmTime(String emailAddress, Long crmTime);
	
	List<CrmContactField> getCrmContactFieldsByContactId(Integer contactId);
}