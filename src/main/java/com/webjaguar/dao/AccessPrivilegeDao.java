/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.webjaguar.model.AccessGroup;
import com.webjaguar.model.AccessGroupPrivilege;
import com.webjaguar.model.AccessPrivilege;
import com.webjaguar.model.AccessPrivilegeSearch;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.AccessUserAudit;
import com.webjaguar.model.Configuration;
import com.webjaguar.web.form.AccessPrivilegeForm;
import com.webjaguar.web.form.GroupPrivilegeForm;


public interface AccessPrivilegeDao {
	
	List getUserPrivilegeList(AccessPrivilegeSearch accessPrivilegeSearch);
	AccessPrivilege getPrivilegeById(Integer accessUserId);
	AccessUser getUserByUserName(String userName);
	AccessUser getAccessUserById(Integer accessUserId);
	AccessUser getAccessUserBySalesRepId(Integer salesRepId);
	void insertAccessPrivilegeUser(AccessPrivilegeForm privilegeForm);
	void disableAccessPrivilege();
	void updateAccessPrivilegeUser(AccessUser user, boolean changeUsername, boolean changePassword );
	void updateAccessPrivilege(List<Configuration> data);
	void deleteAccessPrivilege(String username);
	void deleteAccessPrivilegeUser(String username);
	boolean isUserExist(String username);
	
	// user audit
	List getUserAuditList(String username);
	void insertUserAudit(AccessUserAudit accessUserAudit);
	void deleteAccessUserAudit(Integer id);
	
	// group
	List<AccessGroup> getGroupPrivilegeList(AccessPrivilegeSearch accessPrivilegeSearch);
	AccessGroupPrivilege getPrivilegeByGroupId(Integer groupId);
	boolean isGroupUserExist(Integer groupId);
	void insertAccessGroup(GroupPrivilegeForm privilegeForm) throws DataAccessException;
	void updateGroupAccessPrivilege(final List<Configuration> data);
	void deleteAccessPrivilegeGroup(Integer groupId);
	void updateAccessGroup(AccessGroup group);
	void deleteAccessGroup(Integer groupId);
	List<Configuration> getGroupPrivileges(Integer groupId, Integer userId);
}
