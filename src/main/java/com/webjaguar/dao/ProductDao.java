/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;


import java.util.List;
import java.util.Map;
import java.util.Set;

import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.DataFeedSearch;
import com.webjaguar.model.KitParts;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MyListSearch;
import com.webjaguar.model.Option;
import com.webjaguar.model.OptionSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductCategory;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductFieldUnlimited;
import com.webjaguar.model.ProductFieldUnlimitedNameValue;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.ProductLabelTemplate;
import com.webjaguar.model.ProductLabelTemplateSearch;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.model.RangeValue;
import com.webjaguar.model.ShoppingListGroup;
import com.webjaguar.model.Supplier;
import com.webjaguar.web.admin.datafeed.WebjaguarCategory;

public interface ProductDao {
		
	// test
	void insertProduct(Product product);	
	void updateProduct(Product product);
	void updateApiProduct(Product product);
	void updateParentProduct(String parentSku, Integer parentId);
	void batchCategoryIds(Integer productId, Set categoryIds, String action);
	void batchFieldUpdate(Set productIds, Integer fieldId, String fieldValue);
	void deleteProduct(Product product);	
	void deleteProductById(Integer productId);
	void deleteProductBySku(String sku);
	List<Product> getProductsOnFrontendBySupplier(Integer supplierId);
	List getProductsBySupplierId(Integer supplierId);
	List getProductSkuBySId(Integer supplierId);
	int getProductsByCategoryIdCount(ProductSearch search);
	List<Product> getProductsByCategoryId(ProductSearch search);
	List<Product> getProductsFieldByCategoryId(ProductSearch search);
	Product getProductById(Integer productId, int numImages);
	Product getProductById(Integer productId, int numImages, String protectedAccess, String lang, boolean basedOnHand);
	Product getProductById2(Integer productId, int numImages, String protectedAccess, String lang, boolean basedOnHand);
	List getProductList(ProductSearch search);
	int getProductListCount(ProductSearch search);
	public List<Integer> getProductIdListBySearch(ProductSearch search);
	List<Product> getStoneEdgeProductList(ProductSearch search);
	List<Product> getProductExportList(int limit, int offset);
	List<Product> getProductExportList(ProductSearch search, int numImages);
	List<Product> getSiteMapProductList(ProductSearch search);
	List getProductSupplierExportList(int limit, int offset);
	void updateProductFields(List<ProductField> productFields);
	void updateI18nProductFields(List<ProductField> productFields);
	List<ProductField> getProductFields(String protectedAccess, int numProdFields);
	Map<String, Map<Integer, ProductField>> getI18nProductFields(int numProdFields);
	List<ProductField> getProductFieldsRanked(String protectedAccess, int numProdFields, String lang);
	List<Product> searchProducts(ProductSearch search);
	int searchProductsCount(ProductSearch search);
	List getShoppingListByUserid(MyListSearch search);
	int productCount();
	int productCount(ProductSearch search);
	int productSupplierCount();
	void addToList(Integer userId, Set productIds);
	void removeFromList(Integer userId, Set productIds);	
	void updateSalesTagToProductId(int productId, Integer salesTagId);
	void updateManufacturerToProductId(int productId, Integer manufacturerId);
	List<Product> getProductAjax(ProductSearch productSearch);
	List getShoppingListGroupByUserid(Integer userId);
	void insertShoppingGroupList(String groupName, Integer userId);
	ShoppingListGroup getShoppingListGroupByName(String groupName);
	void addToShoppingGroupList(Set productIds, Integer userId, Integer groupId);
	void removeMyListGroup(Set ids, Integer userId);
	List<ProductImage> getImages(Integer productId, int numImages);
	List<Map<String, Object>> getCostListByProductId(Integer productId);
	String getProductNameBySku(String sku);
	String getProductFieldById(int productId, int fieldNumber);
	String getProductAvialabilityById(Integer productId);
	String getProductRedirectUrl(String sku, Integer productId, String protectedAccess, String field);
	String getProductAsiXMLById(String sku);
	String getProductNoteBySku(String sku);
	boolean isEndQtyPricing(String sku);
	List<Product> getCustomerShoppingCartProductInfo(Integer userId);
	void inactiveProduct(final List<String> data, String column);
	int[] updatePowerReview(final List<Map <String, Object>> data);
	void updateProduct(String updateStatement, String whereStatement);
	Set<Object> getCategoryIdsByProductId(Integer productId);
	List<String> getProductSkuListByCategoryId(Integer cid);
	Product getProductByAsiId(Integer asiId);
	List<RangeValue> getRangeValues(String type);
	
	// product options
	List<ProductOption> getProductOptionsByOptionCode(String optionCode, Integer salesTagId, String protectedAccess);
	void updateProductOptions(Option option);	
	List<Option> getOptionsList(OptionSearch search);
	int getOptionsListCount();
	Option getOption(Integer id, String code);
	void deleteOption(Option option);
	
	Integer getProductIdBySku(String sku);
	String getProductSkuById(Integer id);
	boolean isValidProductId(int id);
	boolean duplicateOptionName(String productId, String name);
	
	//  Kit
	List<KitParts> getKitPartsByKitSku(String kitSku);
	
	// Product Supplier
	List getProductSupplierListBySku(String sku);
	Integer getProductSupplierCost(String sku, Integer defaultSupplierId);
	void updateProductSupplier(Supplier supplier);
	void insertProductSupplier(Supplier supplier);
	Supplier getProductSupplierBySIdSku( String sku, Integer supplierId );
	Supplier getProductSupplierPrimaryBySku( String sku );
	void deleteProductSupplier(String sku, Integer supplierId );
	void nonTransactionSafeUpdateStats(Product product);
	boolean isValidSupplierId(int id);
	void importProductSuppliers(List<Supplier> suppliers);
	int getDefaultSupplierIdBySku(String sku);
	int getProductSupplierMapCount();
	 List<Map<String, Object>> getProductSupplierMap(int limit, int offset);
	
	// Default Supplier
	void updateDefaultSupplierId(Integer supplierId , String productSku);
	
	
	//get LineItem sku
	String getLineItemSku(String sku);	
	
	// Inventory
	void updateInventory(List<LineItem> lineItems);
	void deductInventory(List<LineItem> lineItems);
	void addInventory(List<LineItem> lineItems);
	List<Product> getQuantityPending(List<Product> productList);
	
	// csv feed
	int[] updateProduct(final List <CsvFeed> csvFeedList, final List<Map <String, Object>> data);
	void updateProductImage(Map <Long, List<String>> data);
	void updateFreeShipping(String option_code, String field);

	// ASI
	int[] updateASIProduct(final List <CsvFeed> csvFeedList, final List<Map <String, Object>> data);
	boolean isASIUniqueIdExist(int uniqueId);
	
	// Webjaguar
	void markAndInactiveOldProducts(String feed, Integer dataFeedId, boolean endDocument);
	int[] updateWebjaguarProduct(final List <CsvFeed> csvFeedList, final List<Map <String, Object>> data);
	void insertWebjaguarCategory(final List<WebjaguarCategory> categoryList);
	List<Product> getWebjaguarDataFeedProductList(DataFeedSearch search, int numImages, String imagePath, boolean includeCategories);
	
	//CdsAndDvds
	int[] updatedCdsAndDvdsProduct(final List <CsvFeed> csvFeedList, final List<Map <String, Object>> data);
	void updateCategoryData(Map <Long, Integer> categoryData);
	int insertProductWithSkuOnly(String sku);
	
	// Concord
	void updateProductCategory(Map <Long, List<Long>> data);
	void deleteOldProducts(String feed, boolean makeInActive);
	void inventoryZeroInactive(String feed);
	
	Map<String, Long> getProductSkuMap();
	
	// master sku
	List<Integer> getSlaves(ProductSearch search);
	Double getProductWeightBySku(String sku);
	Map<String, Object> getMasterSkuDetailsBySku(String sku, String protectedAccess, boolean checkInventory);
	Map<String, Object> getMasterSkuBySku(String sku);
	String getProductFieldValue(String sku, String filed);
	
	// fragrancenet
	void insertProductCategory(Map <String, List<Long>> data);
	
	// Lucene
	List<Map<String, Object>> getLuceneProduct(int limit, int offset, boolean onlyParent, String parentSku, String[] searchFieldsList, boolean adminIndex);
	int getLuceneProductCount(boolean onlyParent);
	
	// i18n
	Map<String, Product> getI18nProduct(int id);
	
	void insertCategories(List<Map <String, Object>> data);
	
	// Etilize
	int getEtilizeCount();
	List<Product> getEtilizeList(int limit, int offset);
	List<Map<String, Object>> getProductsBySkuList(List<Product> skuList);
	List<Map<String, Object>> getEtilizeLuceneProduct(int limit, int offset);
	int getEtilizeLuceneProductCount();
	
	// product variant
	List<Product> getProductVariant(int productId, String sku, boolean backEnd);
	
	// pre-hanging
	List<Map<String, Object>> getPreHanging(String doorConfig, String species, String widthHeight, String optionCode);
	int[] updatePreHanging(final List<Map<String, Object>> data);
	void deletePreHanging(int id);
	void insertPreHanging(List<Map <String, Object>> data);
	
	// Lables
	ProductLabelTemplate getProductLabelTemplate(Integer id);
	List<ProductLabelTemplate> getProductLabelTemplates(ProductLabelTemplateSearch search);
	
	Map<Integer, String> getProductIdCategoryIdsMap();
	List<ProductFieldUnlimited> getProductFieldsUnlimited();
	ProductFieldUnlimited getProductFieldsUnlimitedByName(String fieldName);
	int getProductFieldUnlimitedNameValueCount();
	ProductFieldUnlimitedNameValue getProductFieldUnlimitedNameValue(String sku, int fieldId);
	
	void updateProductFields(ProductCategory product);
	void addProductCategory(ProductCategory product);
	void removeProductCategory(ProductCategory product);
	
	String isProductSoftLink(Integer id);
	String isProductSoftLinkSku(String sku);
}