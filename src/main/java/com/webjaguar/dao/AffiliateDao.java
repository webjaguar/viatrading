/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.CommissionReport;
import com.webjaguar.model.CommissionSearch;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;

public interface AffiliateDao {

	List getCommissionsByUserId(Integer userId, Integer level, CommissionSearch search);
	CommissionReport getCommissionsByUserIdOrderId(Integer userId, Integer orderId, Integer level);
	void updateCommissionStatus(CommissionReport comReport);
	CommissionReport getCommissionTotalByUserId(Integer userId, Integer level);
	CommissionReport getCommissionTotal(Integer userId);
	int getAffiliateListCount(CustomerSearch search);
	List<Customer> getAffiliateNameList();
	List<Customer> getAffiliateList(CustomerSearch search);
}
