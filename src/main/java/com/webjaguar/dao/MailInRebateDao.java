package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.MailInRebate;
import com.webjaguar.model.MailInRebateSearch;

public interface MailInRebateDao {

	List<MailInRebate> getMailInRebateList( MailInRebateSearch search );
	MailInRebate getMailInRebateById(Integer rebateId);
	MailInRebate getMailInRebateByName(String name);
	MailInRebate getMailInRebateBySku(String sku);
	void insertMailInRebate( MailInRebate mailInRebate );
	void updateMailInRebate( MailInRebate mailInRebate );
	void deleteMailInRebateById( Integer mailInRebateId );
	
}
