/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.05.2006
 */
package com.webjaguar.dao;

import java.util.List;
import java.util.Map;

import com.webjaguar.model.Layout;
import com.webjaguar.model.Language;

public interface LayoutDao {
	
	Layout getLayout(Layout layout);
	Layout getSystemLayout(Layout layout);
	List<Layout> getLayoutList();
	List<Layout> getSystemLayoutList(String type);	
	Layout getLayoutByCategoryId(int cid);
	
	// i18n
	Map<String, Layout> getI18nSystemLayout(Layout layout);
	Map<String, Layout> getI18nLayout(Layout layout);
	List<Language> getLanguageSettings();
	
	// update
	void updateHeadTag(Layout layout);
	void updateHeaderHtml(Layout layout);
	void updateTopBarHtml(Layout layout);
	void updateLeftBarTopHtml(Layout layout);
	void updateLeftBarBottomHtml(Layout layout);
	void updateRightBarTopHtml(Layout layout);
	void updateRightBarBottomHtml(Layout layout);
	void updateFooterHtml(Layout layout);	
	void updateAemFooterHtml(Layout layout);
	void updateLayout(Layout layout);
	void updateSystemLayout(Layout layout);
}