/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.19.2006
 */

package com.webjaguar.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.MailSender;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.CustomerBudgetPartner;
import com.webjaguar.model.CustomerReport;
import com.webjaguar.model.GoogleCheckOut;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.PackingList;
import com.webjaguar.model.PackingListSearch;
import com.webjaguar.model.Paypal;
import com.webjaguar.model.UpsOrder;
import com.webjaguar.model.VirtualBank;
import com.webjaguar.model.VirtualBankOrderSearch;
import com.webjaguar.thirdparty.payment.amazon.Iopn;
import com.webjaguar.thirdparty.payment.bankaudi.BankAudi;
import com.webjaguar.thirdparty.payment.ebillme.EBillme;
import com.webjaguar.thirdparty.payment.geMoney.GEMoney;
import com.webjaguar.thirdparty.payment.netcommerce.NetCommerce;

public interface OrderDao {

	void insertOrder(Order order, OrderStatus orderStatus, boolean backendAddedOrder);
	
	//Orders
	List<Order> getOrdersListByUser(OrderSearch search, boolean quote);
	Map<Integer, Integer> getFirstOrderAllUser();
	List<Order> getManifestsReportByUser(OrderSearch search, boolean quote);
	List<Order> getQuotesListByUser(OrderSearch search);
	List<Order> getOrdersListBySupplier(OrderSearch search);
	Order getOrder(int orderId, String sort);
	List<Order> getOrdersList(OrderSearch search);
	List<Order> getQBOrdersList(OrderSearch search, String sort);
	int getOrdersListCount(OrderSearch search);
	OrderStatus getLatestStatusHistory(Integer orderId);
	void updateOrder(Order order);
	void updateCreditCardPayment(Order order);
	void insertCreditCardHolderAuthentication(Order order);
	void updatePaypal(Paypal paypal, OrderStatus orderStatus);
	void insertOrderStatus(OrderStatus orderStatus);
	boolean isUserUsedPromoCode( String promoCode, Integer userId );
	List<String> getSKUsByUserId(Integer userId);
	int invoiceCount(OrderSearch search);
	List<Order> getInvoiceExportList(OrderSearch search, String sort);
	List<PackingList> getExportPackingList(PackingListSearch search);
	void updateNetCommercePayment(NetCommerce trans, OrderStatus orderStatus);
	void updatePdfOrder(Order order);
	void updateOrderPrinted(Order order);
	void updateOrderApproval(Order order);
	List<CustomerReport> getCustomerQuickViewList(Integer userId);
	boolean isValidOrderId( String orderId );
	List<LineItem> getLineItemByOrderId(Integer OrderId);
	void updateEbillmePayment(EBillme eBillme, OrderStatus orderStatus);
	void updateEbillmePayment(EBillme eBillme);
	void updateGoogleCheckOut(GoogleCheckOut googlecheckOut, OrderStatus orderStatus);
	Integer getOrderIdByGoogleOrderId(Long googleOrderId);
	List<Integer> getCustomersWithOrderCount(OrderSearch search);
	void cancelBuySafeBond(Order order, OrderStatus orderStatus);
	Order getLastOrderByUserId(Integer userId);
	Integer getCustomerOrdersNumber(Integer userId);
	
	//Packing List
	void insertPackingList(PackingList packingList, Integer accessUserId, boolean inventoryHistory);
	void insertPackingList(PackingList packingList, Integer accessUserId, boolean inventoryHistory, Order order);
	List<PackingList> getPackingListByOrderId(PackingListSearch packingListSearch);
	PackingList getPackingList(Integer orderId, String packingNumber);
	void updatePackingList(PackingList packingList);
	void ensurePackingListShipDate(Order order, boolean inventoryHistory,Integer accessUserId );
	void updateLineItemOnHand(PackingList packingList, boolean inventoryHistory,Integer accessUserId);
	void productIventoryHistory(InventoryActivity inventoryActivity );
	List<LineItem> getPackingListLineItemsQuantity(Integer orderId);
	List<LineItem> getPackingListLineItemsByPackingNum(String packingNumber);
	int packingListCount(Integer orderId);
	void deletePackingList(Integer orderId, String packingNumber, Integer accessUserId, boolean inventoryHistory);
	Date getShipDateByOrderId(Integer orderId);
	List<String> getPackisListNumByOrderId(int orderId);
	void updateInvoiceExported (Set<Integer> orderIds);
	//orderLookup
	List<UpsOrder> getOrderLookupReport(String customerPO);
	void importUpsOrders(UpsOrder upsWorldShip );
	void updateUpsOrders(UpsOrder upsWorldShip );
	void updateUpsOrdersTrackAndShipDate(UpsOrder upsWorldShip );

	// mas90, dsi, triplefin
	void updateOrdersPaymentStatus(int orderId, String status, Date date);
	void updateOrderExported(String column_name, Date endDate, Set<Integer> orderIds);
	
	void updateLineItemShipping(LineItem lineItem);
	
	// evergreen
	void updateEvergreenOrderSuc(int orderId, String suc);
	String getEvergreenOrderSuc(int orderId);
	
	// custom frame
	LineItem getCustomFrame(int orderId, int lineNum);
	
	// amazon 
	void updateAmazonPayment(Iopn iopn, OrderStatus orderStatus);
	void updateAmazonLineItem(List<LineItem> lineItems);
	
	// bank audi
	void updateBankAudiPayment(BankAudi trans, OrderStatus orderStatus);
	
	// mas200
	void updateMas200order(Order order);
	
	// GE Money
	void updateGEMoneyPayment(GEMoney trans, OrderStatus orderStatus);
	
	// Plow & Hearth
	void updatePlowAndHearthLineItems(List<LineItem> lineItems);
	
	//VBA
	List<VirtualBank> getConsignmentAndAffiliateOrders(VirtualBankOrderSearch orderSearch);
	void updateVbaOrders(List<Order> vbaOrders, List<LineItem> vbaOrdersLineItems, String userName);
	
	//Budget
	void insertOrderAprrovalDenialHistory(Order order, List<Integer> parentsList,MailSender mailSender,Map<String, Configuration> siteConfig);
	List<Order> getOrderActionHistory(Integer orderId);
	boolean getApproval(Integer orderId, Integer actionBy, boolean fullStatus);
	
	// eBizCharge customerDb update
	public void updateGatewayToken(String gatewayToken, Integer id);
	
	// for payment alert
	public Double getCustomerOrderBalance(Integer userId,Date dateOrdered);
	public Double getOrderAmountPaid( Integer orderId);
	public void updateOrderPaymentAlert(boolean paymentAlert, Integer orderId);
	
	// for budget
	public List<CustomerBudgetPartner> getCustomerPartnerHistory(CustomerBudgetPartner partner);
	public void updateCustomerPartnerHistory(CustomerBudgetPartner partner);
	
	public void updateOrderQuote(Integer orderId, String quote);
	
	public String getOrderQuote(Integer orderId);
	
	public void insertOrderUserDueDate(Order order);
	
	public void updateOrderSubStatus(Order order);
	
	public void updateIsFirstOrder(Order order);
	
	Integer isThereAnOrderUseThisPromo(String promoCode);

	Integer isThereAnOrderUseThisPromoByCustomer(String promoCode, Integer customerId);

	int getOrdersCountByStatus(String status);
}
