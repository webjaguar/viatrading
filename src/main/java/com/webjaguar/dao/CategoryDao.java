/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.webjaguar.model.Category;
import com.webjaguar.model.CategorySearch;

public interface CategoryDao {
	
	List<Category> getCategoryTree(Integer categoryId, boolean showHidden, String lang);
	
	Set<Integer> getParentCategories(Integer categoryId);
	
	List<Category> getCategoryList(CategorySearch search);
	
	Category getCategoryById(Integer categoryId, String protectedAccess);

	int insertCategory(Category category);

	void updateCategory(Category category);
	
	void deleteCategory(Category category);

	List<Category> getCategoryLinks(Integer parentId, String protectedAccess, String lang, boolean checkInventory);
	
	String getCategoryName(Integer categoryId);
	
	void updateRanking(List<Map <String, Integer>> data);
	
	void updateCategoryProductRanking(List<Map <String, Integer>> data);

	void updateStats(Category category);
	
	Integer getHomePageByHost(String host);
	
	List<Category> getCategoryLinksTree(int cid);

	Map<String, Category> getI18nCategory(int cid, String lang);
	
	void updateI18nCategory(Collection<Category> categories);
	
	List<Category> getCategories(String protectedAccess, boolean showHidden, Boolean showOnSearch, String lang);
	
	Map<String, Long> getCategoryExternalIdMap();
	
	List<Category> getSiteMapCategoryList(CategorySearch search);
	
	// concord
	Map<String, Map<String, String>> getCategoryExternalIdNewEggMap();
	
	List<Category> getCategoryTreeStructure(int catId, String direction);
}
