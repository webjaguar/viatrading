/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.09.2008
 */

package com.webjaguar.dao;

import java.util.List;
import java.util.Map;

import com.webjaguar.model.MultiStore;

public interface MultiStoreDao {

	void updateMultiStoreConfig(List<MultiStore> multiStores);	
	List<MultiStore> getMultiStore(String host);
	Map<String, String> getStoreIdMap();
}
