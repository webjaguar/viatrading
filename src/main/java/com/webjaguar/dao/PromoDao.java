/* Copyright 2006 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.23.2007
 */

package com.webjaguar.dao;

import java.util.List;
import java.util.Map;

import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Order;
import com.webjaguar.model.Promo;
import com.webjaguar.model.PromoSearch;
import com.webjaguar.model.SalesTag;

public interface PromoDao {

	// Promo
	List<Promo> getPromosList( PromoSearch promoSearch);
	List<Promo> getDynamicPromosList( PromoSearch promoSearch);
	List<Promo> getEligiblePromoList( Customer customer, Order order );
	List<Promo> getCampaignList(PromoSearch promoSearch);
	Promo getPromoById(Integer promoId);
	Promo getPromoByName( String promoName );
	Promo getPromoWithCondition( Promo promo, Map<String, Object> promoCondition );
	Promo getAdminPromoWithCondition( Promo promo, Map<String, Object> promoCondition );
	Integer getPromoIdByName( String promoName );
	void insertPromo( Promo promo );
	void insertPromos( List<Promo> promos );
	void updatePromo( Promo promo );
	void deletePromoById( Integer promoId );
	void deletePromoByIds( int[] promoIds );
	void updateRanking(List<Map <String, Integer>> data);
	Promo getPromo( String promoName );
	public Integer getPromoCount(PromoSearch promoSearch);
	
	// Sales Tags
	List<SalesTag> getSalesTagList();
	SalesTag getSalesTagById(Integer salesTagId);
	int insertSalesTag( SalesTag salesTag );
	void updateSalesTag( SalesTag salesTag );
	void deleteSalesTagById( Integer salesTagId );
	Map<String, Long> getSalesTagCodeMap();
	void insertSalesTag(final List <CsvFeed> csvFeedList, final List<Map <String, Object>> data);	
}
