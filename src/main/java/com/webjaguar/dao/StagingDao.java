/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.Customer;
import com.webjaguar.model.Order;
import com.webjaguar.model.StagingUser;

public interface StagingDao {
	
	// WorldShip
	void insertStagingOrder(Order order);
	List getWorldShipTrackcode();
	void updateWorldShipProcessed(List<Integer> validOrderIds);
	void deleteWorldShipTrackcode();
	// FedEx Label Manager
	List getFedexShipTrackcode();
	void updateFedexShipProcessed();
	void deleteFedexShipTrackcode();
	List<StagingUser> getStagingUsersData();
	void deleteStagingUsers(int id);
}
