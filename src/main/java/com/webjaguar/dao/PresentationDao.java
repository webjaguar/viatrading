/* Copyright 2012 Advanced E-Media Solutions
 *
 */
package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.Presentation;
import com.webjaguar.model.PresentationProduct;
import com.webjaguar.model.PresentationTemplate;
import com.webjaguar.model.PresentationTemplateSearch;

public interface PresentationDao {
	List<PresentationProduct> getPresentationProductList(Presentation presentation);
	List<Presentation> getPresentationList(Presentation presentation);
	Presentation getPresentation(Integer presentationId);
	void addProductToPresentation(String[] sku, Integer presentationId, Integer saleRepId);
	List<PresentationTemplate> getPresentationTemplateList(PresentationTemplateSearch templateSearch);
	void insertPresentation(Presentation presentation);
	void updateNoOfViewToPresentation(Integer presentationId);
	void updatePresentationPrice(String sku, double price, Integer presentationId, Integer salesRepId, boolean remove);
	void updatePresentation(Presentation presentation);
	void deletePresentation(Presentation presentation);
	void insertTemplate(PresentationTemplate template);
	void updateTemplate(PresentationTemplate template);
	void deleteTemplate(Integer templateId);
	PresentationTemplate  getTemplateById(Integer templateId);
	void removeProductFromPresentationList(String sku[], Integer saleRepId);
}
