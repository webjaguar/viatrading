package com.webjaguar.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.webjaguar.model.Event;
import com.webjaguar.model.EventMember;
import com.webjaguar.model.EventSearch;

public interface EventDao {

	Event getEventById(Integer eventId);
	List<Event> getEventList(EventSearch search);
	void insertEvent( Event event );
	void updateEvent( Event event );
	void deleteEvent( Integer eventId );
	
	int getEventMemberListCount(EventSearch search);
	List<EventMember> getEventMemberList(EventSearch search);
	void insertEventMember(EventMember event) throws DataAccessException;
	EventMember getEventMemberByEventIdUserId(Integer eventId, Integer userId);
	void updateEventMember(EventMember eventMember) throws DataAccessException;
	void deleteEventMemberById(Integer eventId, Integer userId);
	List<EventMember> getAllEventMemberList(EventSearch search);
	int getAllEventMemberListCount(EventSearch search);
}