package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.Customer;

public interface PredictiveDialingDao {
	public void insertCustomerToPredictiveDialing(List<Customer> customerList);
}
