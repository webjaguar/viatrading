/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.AbandonedShoppingCart;
import com.webjaguar.model.AbandonedShoppingCartSearch;
import com.webjaguar.model.CityReport;
import com.webjaguar.model.ConsignmentReport;
import com.webjaguar.model.CustomerCreditHistory;
import com.webjaguar.model.CustomerReport;
import com.webjaguar.model.EmailCampaign;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.ImportExportHistorySearch;
import com.webjaguar.model.InventoryReport;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.MetricReport;
import com.webjaguar.model.Order;
import com.webjaguar.model.ProductReport;
import com.webjaguar.model.Report;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.SalesRepReport;
import com.webjaguar.model.SiteMessageGroup;
import com.webjaguar.model.SiteMessageGroupSearch;
import com.webjaguar.model.StateReport;
import com.webjaguar.model.TicketReport;
import com.webjaguar.model.VbaPaymentReport;

public interface ReportDao {

	List<Report> getOrdersSaleReport(ReportFilter filter);
	List<Report> getOrdersSaleDetailReport(ReportFilter filter);
	List<MetricReport> getMetricReport(ReportFilter filter);
	Integer getNumCustomerActivated(String year, int month);
	Integer getNumCustomerRetained(String year, int month);
	// salesRep
	List<Report> getOrdersSaleReportDaily(ReportFilter filter);
	Integer getOrdersSaleReportDailyCount(ReportFilter filter);
	List<Report> getSalesRepDetailReport(ReportFilter filter);
	List<Report> getSalesRepDetailReport2(ReportFilter filter);
	List<SalesRepReport> getSalesRepReport(Integer salesRepId, ReportFilter salesRepFilter);
	List<SalesRepReport> getSalesRepReportDaily(Integer salesRepId, ReportFilter salesRepFilter);
	// import/export
	List<ImportExportHistory> getImportExportHistoryByType(ImportExportHistorySearch search);
	void insertImportExportHistory(ImportExportHistory imEx);
	List<Report> getCustomerQuickViewPurchase(Integer userId);
	List<Report> getCustomerQuickViewPurchaseLastYear(Integer userId);
	Integer getCountPendingProcessingOrder();
	List<Order> getPendingProcessingOrder();
	List<Report> getTrackcodeReport(ReportFilter reportFilter);
	// customer report
	List<CustomerReport> getInactiveCustomers(ReportFilter reportFilter);
	List<Integer> getInactiveCustomerIds(ReportFilter reportFilter);
	List<CustomerReport> getCustomerReportOverview(ReportFilter filter);
	// massEmail report
	List<EmailCampaign> getEmailCampaigns(ReportFilter reportFilter);
	// product report
	List<ProductReport> getProductListReport(ReportFilter reportFilter); 
	int getProductListReportCount(ReportFilter reportFilter);
	// promo report
	List<Report> getPromoCodeReport(ReportFilter reportFilter);
	//inventory
	List<InventoryActivity> getInventoryActivity(ReportFilter reportFilter);
	Integer getInventoryActivityCount(ReportFilter reportFilter);
	List<InventoryReport> getInventoryReport(ReportFilter reportFilter);
	Integer getInventoryReportCount(ReportFilter reportFilter);
	List<ConsignmentReport> getConsignmentReport(Integer supplierId, String sort);
	List<ConsignmentReport> getConsignmentSales(ConsignmentReport salesReport);
	List<CustomerCreditHistory> getCustomerCreditHistory(Integer supplierId, Integer userId);
	//VBA
	List<VbaPaymentReport> getVbaPaymentReport(VbaPaymentReport reportFilter);
	//Abandoned Shopping Cart
	List<AbandonedShoppingCart> getAbandonedShoppingCartList(AbandonedShoppingCartSearch abandonedShoppingCartSearch);
	//ticket report
	List<TicketReport> getTicketReport(ReportFilter reportFilter);
	//customer report
	List<Report> getCustomeReportDaily(ReportFilter reportFilter);
	//Best selling products
	List<ProductReport> getBestSellingProducts(ReportFilter reportFilter);
	//Top abandoned products
	List<ProductReport> getTopAbandonedProducts(ReportFilter reportFilter);
	//Top shipped cities
	List<CityReport> getTopShippedCities(ReportFilter reportFilter);
	//Top shipped states
	List<StateReport> getTopShippedStates(ReportFilter reportFilter);
	//Top sales reps
	List<SalesRepReport> getTopSalesReps(ReportFilter reportFilter);
	List<Report> getNewCRMContactReportByYear(Integer year);
	List<Report> getNewCustomerReportByYear(Integer year);
}
