/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.16.2006
 */

package com.webjaguar.dao;

import java.util.List;
import java.util.Map;

import com.webjaguar.model.AccessUser;
import com.webjaguar.model.City;
import com.webjaguar.model.County;
import com.webjaguar.model.LocationSearch;
import com.webjaguar.model.ZipCode;

public interface GlobalDao {

	Map<String, Object> getGlobalSiteConfig();
	void updateGlobalSiteConfig(Map<String, Object> data);
	AccessUser getAdminInfoByUserName(String username);
	AccessUser getAdminInfoByUsernamePassword(String username, String password);
	AccessUser getAdminInfo();
	void updateAdminInfo( AccessUser user );
	City getCityInfoByZipcode(String zipcode);
	List<City> getCities(City city);
	List<County> getCounties(County county);
	ZipCode getZipCode(String zipcode);
	List<ZipCode> getZipCodeList(LocationSearch locationSearch);

}