package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.TruckLoadProcess;
import com.webjaguar.model.TruckLoadProcessProducts;
import com.webjaguar.model.TruckLoadProcessSearch;

public interface TruckLoadProcessDao {
	
	void insertTruckLoadProcess( TruckLoadProcess truckLoadProcess );
	List<TruckLoadProcess> getTruckLoadProcessList(TruckLoadProcessSearch truckLoadProcessSearch);
	List<TruckLoadProcess> getTruckLoadProcessListReport(TruckLoadProcessSearch truckLoadProcessSearch);
	List<String> getTruckLoadProcessPTList();
	TruckLoadProcess getTruckLoadProcessById(Integer id);
	TruckLoadProcessProducts getTruckLoadDerivedProductById (Integer Id);
	void updateTruckLoadProcess(TruckLoadProcess truckLoadProcess);
	void updateTruckLoadPT(TruckLoadProcess truckLoadProcess);
}
