package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.Customer;
import com.webjaguar.model.InvoiceImport;
import com.webjaguar.model.InvoiceImportLineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.PaymentImport;
import com.webjaguar.model.ProductCategory;

public interface ProductImportDao {
	
	public List<ProductCategory> getUpdateProductFields();
	public List<ProductCategory> getAddProductCategory();
	public List<ProductCategory> getRemoveProductCategory();
	public void deleteAllUpdateProductFields();
	public void deleteAllAddProductCategory();
	public void deleteAllRemoveProductCategory();
	public List<Customer> getUserNotifications();
	public void removeUserNotifications();
	
}
