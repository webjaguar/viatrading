/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.Click;
import com.webjaguar.model.ClickDetail;
import com.webjaguar.model.ClickDetailSearch;
import com.webjaguar.model.EmailCampaign;
import com.webjaguar.model.EmailCampaignBalance;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.Unsubscribe;

public interface MassEmailDao {

	void insertEmailCampaign(EmailCampaign emailCampaign);
	void updateEmailCampaign(EmailCampaign emailCampaign);
	void finalizeEmailCampaign(EmailCampaign emailCampaign);
	EmailCampaign getEmailCampaignById(Integer campaignId);
	void addEmailCampaignBounce(Integer campaignId);
	void deductEmailCampaignPoint(Integer point);
	void addEmailCampaignPoint(Integer point);
	int emailInProcessCount();
	void insertEmailCampaignBalance(EmailCampaignBalance balance);
	void updateEmailCampaignBalance(EmailCampaignBalance balance);
	void deleteEmailCampaignById(Integer id);
	int getEmailBalance();
	List<EmailCampaignBalance> getBuyCreditList(ReportFilter buyCreditFilter);
	public void updateClickCount(Click click);
	public Integer insertOrGetClick(Click click);
	public List<Click> getClick(Click click);
	public List<Unsubscribe> getUnsubscribe(Unsubscribe unsubscribe);
	public List<ClickDetail> getCountDetail(ClickDetail clickDetail);
	public List<ClickDetail> getOpenDetail(ClickDetailSearch clickDetail);
	public void updateOpenCount(Click click);
	public void insertUnsubscribeDetails(String campaignId,Integer userId,String email);
}
