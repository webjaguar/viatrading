/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.26.2008
 */

package com.webjaguar.dao;

import java.util.List;
import java.util.Map;

import com.webjaguar.model.Brand;
import com.webjaguar.model.BudgetProduct;
import com.webjaguar.model.BudgetProductSearch;
import com.webjaguar.model.Product;

public interface BudgetDao {

	List<Brand> getBrands(Integer userId); 
	Double getOrderTotalByBrand(Brand brand, int userId, int year);
	void updateBudget(int userId, List<Brand> brands);
	List<Product> getProductExportByBrands(String protectedAccess);	
	List<Product> getProductListByBrand(Brand brand);
	Map<String, Object> getOrderReportByBrand(Brand brand);
	List<Map<String, Object>> getOrderReportByBrand(Brand brand, String sku);
	
	// budget by products
	Map<String, BudgetProduct> getBudgetProduct(int userId, int year, boolean shoppingCart);
	Map<String, BudgetProduct> getBudgetProduct(int userId, int year, List<Product> productList);
	List<BudgetProduct> getBudgetProductList(BudgetProductSearch search);
	BudgetProduct getBudgetProduct(int id);
	void updateBudgetProduct(BudgetProduct budgetProduct);
	List<Integer> getBudgetProductYears(BudgetProductSearch search);
	void insertBudgetProduct(BudgetProduct budgetProduct);
	void deleteBudgetProduct(int id);
}
