/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;

import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Address;
import com.webjaguar.model.Affiliate;
import com.webjaguar.model.BudgetPartner;
import com.webjaguar.model.CsvData;
import com.webjaguar.model.CsvIndex;
import com.webjaguar.model.CsvMap;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerBudgetPartner;
import com.webjaguar.model.CustomerCreditHistory;
import com.webjaguar.model.CustomerExtContact;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.CustomerGroup;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.DialingNote;
import com.webjaguar.model.LocationSearch;
import com.webjaguar.model.MobileCarrier;
import com.webjaguar.model.SpecialPricing;
import com.webjaguar.model.SpecialPricingSearch;
import com.webjaguar.model.StagingUser;
import com.webjaguar.model.VirtualBank;
import com.webjaguar.model.crm.CrmContact;

public interface CustomerDao {

	String createPasswordToken(Integer userId);

	String getTokenByUserId(Integer userId);

	String changeCustomerToken(Integer userId);

	boolean validToken(String token);

	Integer getUserIdByToken(String token);

	Integer getCustomerIdByAccountNumber(String accountNumber);

	void updatePassword(String newPassword, String token);

	List<Customer> getCustomerListWithDate(CustomerSearch search);

	List<Customer> getCustomers(CustomerSearch search);

	int getCustomerListWithDateCount(CustomerSearch search);

	List<Integer> getCustomerIdsList(CustomerSearch search);

	int getCustomersCount();

	List<Customer> getCustomerListByZipCode(LocationSearch search);

	int getCustomerListByZipCodeCount(LocationSearch search);

	Customer getCustomerById(Integer userId);
	
	List<Integer> getUserIdByCrmId(List<Integer> userId);

	
	Integer getUserIdByCrmId(Integer userId);
	
	Integer getCrmIdByUsername(String username);
	
	public Map<String,CsvMap> getAvailableUserList(List<String> username);
	
	public List<String> getUserIdListByUsername(List<String> username);
	
	public Map<String,Long> getUserIdsMapByUsername(List<String> username);
	
	Customer getCustomerByUsername(String username);

	Customer getCustomerByUsernameAndPassword(String username, String password) throws DataAccessException;
	
	Customer getCustomerByCardIdAndPassword(String cardId, String password) throws DataAccessException;

	Customer getCustomerByCardID(String cardID);

	void updateCustomer(Customer customer, boolean changeUsername, boolean changePassword);
	
	void updateCustomerBrokerImage(Customer customer);
	
	void removeCustomerBrokerImage(Customer customer);

	void updateCardIdCount(Integer userId);

	void insertCustomer(Customer customer) throws DataAccessException;

	void deleteCustomer(Customer customer) throws DataAccessException;

	void nonTransactionSafeUpdateLoginStats(Customer customer);

	List<Address> getAddressListByUserid(Integer userId);
	
	List<Address> getAllAddressList(Integer userId);

	Address getAddressById(Integer addressId);

	void updateAddress(Address address) throws DataAccessException;

	void insertAddress(Address address) throws DataAccessException;

	void deleteAddress(Address address) throws DataAccessException;

	Address getDefaultAddressByUserid(Integer userId);

	Address getDefaultShippingAddressByUserid(int userId);

	String getTaxIdByUserId(Integer userId);
	
	String getTextMessageServerById(Integer textMessageServerId);

	List getAffiliates(Integer root, Integer list_type);

	void updateEdge(Integer customerId, Integer parentId);

	Integer getParentId(Integer childId);

	Affiliate getAffiliatesCommission(Integer nodeId);

	int affiliateCount();

	List<Customer> getCustomerExportList(CustomerSearch search);

	int customerCount(CustomerSearch search);

	Customer getCustomerByToken(String token);

	List<CustomerField> getCustomerFields();
	
	List<DialingNote> getDialingNoteHistoryById(int id, boolean isCustomer);
	
	public void updateDialingNotesCrmToCustomer(Integer crmId, Integer customerId);

	void updateCustomerFields(List<CustomerField> customerFields);

	void updateCustomerNote(Customer customer);

	Integer getUserIdByPromoCode(String promoCode);

	void unsubscribeByCustomerId(Integer customerId, String unsubscibeReason);

	boolean isExistingCustomer(String accountNum);

	List<Customer> getCustomerList(CustomerSearch search);

	void updateCategoryIds(int userId, Set<Object> categoryIds);

	Set<Integer> getCategoryIds(int userId);

	void updateUserDescription(Customer customer);

	void updateSuspension(Integer customerId, String action);
	
	void updateQualifier(Integer customerId, String action);
	
	void updateTrackCode(Integer customerId, String trackCode);
	
	void updateMainSource(Integer customerId, String mainSource);
	
	void updatePaid(Integer customerId, String paid);
	
	void updateMedium(Integer customerId, String medium);
	
	void updateLanguageField(Integer customerId, String languageField);

	void updateCrmIds(Customer customer);

	Customer getCustomerOrderReportByUserID(Integer userId);

	Map<Integer, Customer> getQuoteTotalByUserIDPerPage(List customerList);

	// Special Pricing
	void updateSpecialPricingActive(SpecialPricing specialPricing);

	SpecialPricing getSpecialPricingByCustomerIdSku(SpecialPricing specialPricing);

	List<SpecialPricing> getSpecialPricingByCustomerID(SpecialPricingSearch search);

	void insertSpecialPricing(SpecialPricing specialPricing);

	void deleteSpecialPricingById(Integer id) throws DataAccessException;

	Map<String, SpecialPricing> getSpecialPricingByCustomerIDPerPage(Integer customerId, List productList);

	List<Customer> getCustomerListByParent(CustomerSearch search);

	int getCustomerListByParentCount(CustomerSearch search);

	void updateParent(Customer customer);

	List<Customer> getFamilyTree(int customerId, String direction);

	// Gift Card
	void updateCredit(CustomerCreditHistory credit);

	// Loyalty
	void updatePointsToCustomer(int customerId, double loyaltyPoints);

	Integer getCustomerIdBySupplierPrefix(String prefix);

	int productCountByCustomerSupplierId(int supplierId);

	// evergreen
	void updateEvergreenCustomerSuc(int customerId, String suc);

	int[] nonTransactionSafeDeleteCustomerGroup(int groupId, Set<String> accountNumber);

	int[] insertCustomerGroup(int groupId, Set<String> accountNumber);

	// ajax
	List<Customer> getCustomersAjax(CustomerSearch search);

	// group
	List<CustomerGroup> getCustomerGroupList(CustomerGroupSearch search);

	List<CustomerGroup> getCustomerGroupNameList();

	List<Integer> getCustomerGroupIdList(Integer customerId);

	void insertCustomerGroup(CustomerGroup group) throws DataAccessException;

	void updateCustomerGroup(CustomerGroup group);

	CustomerGroup getCustomerGroupById(Integer groupId);

	void deleteCustomerGroupById(Integer groupId);

	void batchGroupIds(Integer customerId, Set groupIds, String action);
	
	void batchAssignUserToCustomers(List<Integer> userIdList, AccessUser accessUser);


	boolean isGroupCustomerExist(Integer groupId);

	Map<Integer, String> getCustomerCompanyMap(List<VirtualBank> useridList);

	Map<Integer, String> getCustomerNameMap(List<Integer> userIdList);

	Address getAddress(int userId, String addressCode);

	List<Integer> getCustomerIdByGroupId(Integer groupId);

	Map<Integer, Double> getPaymentsTotalToCustomerMap(List<VirtualBank> useridList);

	String getUserName(Integer userId, Integer contactId);

	// budget partners
	void insertBudgetPartners(BudgetPartner partner);

	void updatePartners(BudgetPartner partner);

	String getPartnerNameById(Integer id);

	List<BudgetPartner> getPartnersList(Boolean active);

	BudgetPartner getPartnerById(BudgetPartner partner);

	void insertPartnerCustomer(CustomerBudgetPartner partner);

	void updatePartnerCustomer(CustomerBudgetPartner partner, boolean addHistory);

	List<CustomerBudgetPartner> getPartnersListByCustomerId(Integer userId, Boolean active);

	void insertPartnerCustomerHistory(CustomerBudgetPartner partner);

	Boolean getPartnerByName(String name);
	
	List<MobileCarrier> getMobileCarrierList();
	
	int insertDialingNoteHistory(DialingNote note);

	// Next_Index
	int getNextIndexByName(String accountName);

	// Lucene Customer
	int getLuceneCustomerListCount(CustomerSearch search);
	
	List<Map<String, Object>> getLuceneCustomer(int offset, int limit, Integer userId);
	
	int updateUserRatings(StagingUser user);
	
	Integer getCustomerIdByUsername(String username);
	
	Integer batchCustomerRating1(List<Integer> contactIds, String rating);
	
	Integer batchCustomerRating2(List<Integer> contactIds, String rating);
	
	Integer batchCustomerQualifier(List<Integer> contactIds, String qualifier);
	
	Integer batchCustomerLanguage(List<Integer> contactIds, String qualifier);

	void insertAndUpdateCustomerList(List<String> allEmailAddressList, Map<String, CsvMap> availableEmailMap, Map<String, List<CsvData>> customerCsvDataList,Map<String, List<CsvData>> addressCsvDataList, Map<String, List<CsvData>> crmCsvDataList, List<CsvIndex> customerCsvIndex, List<CsvIndex> addressCsvIndex, List<CsvIndex> crmCsvIndex, List<Map<String, Object>> addedItems, List<Map<String, Object>> updatedItems, List<Map<String, Object>> addedCrmItems, List<Map<String, Object>> updatedCrmItems, List<Map<String, Object>> notAddedCrmItems ,  List<Map<String, Object>> notAddedCustomerItems);
						
	String checkUserByCellPhone(String username2);
	
	public Customer getCellPhoneByUsernameAndPassword(String username, String password);
	
	public void updateUserNotifications(Customer customer);
	
	public void updateCrmNotifications(CrmContact crmcontact);

	void updateNewInformation(Integer customerId, Integer action);
    
	List<CustomerExtContact> getCustomerExtContactListByUserid(Integer userId);
	
	CustomerExtContact getCustomerExtContactById(Integer customerExtContactId);

	void updateCustomerExtContact(CustomerExtContact customerExtContact) throws DataAccessException;

	void insertCustomerExtContact(CustomerExtContact customerExtContact) throws DataAccessException;

	void deleteCustomerExtContact(CustomerExtContact customerExtContact) throws DataAccessException;
	
}
