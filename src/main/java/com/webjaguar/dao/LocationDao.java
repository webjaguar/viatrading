/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;

import com.webjaguar.model.Customer;
import com.webjaguar.model.Location;
import com.webjaguar.model.LocationSearch;


public interface LocationDao {
	
	List<Location> getLocationList(LocationSearch locationSearch); 
	List<Location> searchLocationList(LocationSearch search);
	Map<String, Location> getLocationMap(LocationSearch locationSearch); 
	Location getLocationById(Integer userId);
	void updateLocation(Location location);
	void insertLocation(Location location) throws DataAccessException;
	void deleteLocation(Integer locationId) throws DataAccessException;
	void insertLocationByCustomer(Customer customer);
	void addToLocationWishList(Integer userId, Integer locationId);
    void removeFromLocationWishList(Integer userId, Set locationIds);
    List<Location> getLocationWishListByUserid(LocationSearch locationSearch);
    void updateKeywordLocationByUserId(Location location) throws DataAccessException;
    List<Location> getLocationListByUserId(Integer userId, Integer limit);
	boolean isValidLocationId(int id);
}
