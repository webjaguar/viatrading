/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.GiftCard;
import com.webjaguar.model.GiftCardOrder;
import com.webjaguar.model.GiftCardSearch;
import com.webjaguar.model.GiftCardStatus;
import com.webjaguar.model.UserSession;

public interface GiftCardDao {
	
	int insertGiftCardOrder(GiftCardOrder giftCardOrder, GiftCardStatus giftCardStatus);
	List<GiftCard> getGiftCardList(GiftCardSearch search, Integer customerId);
	List<GiftCardOrder> getGiftCardOrderList(GiftCardSearch search, Integer customerId);
	void insertGiftCard(GiftCard giftCard);
	GiftCard getGiftCardByCode(String code, Boolean active);
	void updateGiftCard(GiftCard giftCard);
	void redeemGiftCardByCode(String code, UserSession userSession);
	void updateGiftCardOrderCreditCardPayment(GiftCardOrder giftCardOrder);
	GiftCardOrder getGiftCardOrderByGiftCardOrderId(String giftCardOrderId);
	void updateGiftCardByPayPal(GiftCard giftCard);
	GiftCard getGiftCardById( Integer giftCardOrderId);
	void updateGiftCardOrderByPayPal(GiftCardOrder giftCardOrder, GiftCardStatus giftCardStatus);
	List<GiftCardStatus> getGiftCardStatusHistory(Integer giftCardOrderId);
	void insertGiftCardStatus(GiftCardStatus giftCardStatus);
}
