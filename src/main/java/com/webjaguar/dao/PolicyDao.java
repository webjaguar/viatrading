/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.Policy;

public interface PolicyDao {
		
	void insertPolicy(Policy policy);

	void updatePolicy(Policy policy);
	
	void deletePolicy(Integer policyId);
	
	Policy getPolicyById(Integer policyId);
	
	List getPolicies();
	
	void updateRanking(List data);
	
	List getPolicyList();
}
