/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.26.2008
 */

package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.Subscription;
import com.webjaguar.model.SubscriptionSearch;

public interface SubscriptionDao {

	List<Subscription> getDueSubscriptions();
	List<Subscription> getSubscriptions(SubscriptionSearch search);
	Subscription getSubscription(String code);
	void updateSubscription(Subscription subscription);
	void deleteSubscription(Subscription subscription);

}
