/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.List;
import java.util.Map;

import com.webjaguar.model.Faq;
import com.webjaguar.model.FaqGroup;
import com.webjaguar.model.FaqSearch;

public interface FaqDao {
	
	// Faq
	void insertFaq(Faq faq);
	void updateFaq(Faq faq);
	void deleteFaq(Integer faqId);
	Faq getFaqById(Integer faqId);
	void updateRanking(List<Map<String, Integer>> data);
	List<Faq> getFaqList(FaqSearch search);
	Faq getFaqByQuestion(FaqSearch search);
	
	// FaqGroup
	void insertFaqGroup(FaqGroup faqGroup);
	void updateFaqGroup (FaqGroup faqGroup);
	void deleteFaqGroup (Integer groupId);
	FaqGroup getFaqGroupById (Integer groupId);
	void updateFaqGroupRanking(List<Map<String, Integer>> data);
	List<FaqGroup> getFaqGroupList(String protectedAccess);
	
}
