/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.04.2007
 */

package com.webjaguar.dao;

import java.util.List;
import java.util.Map;

import com.webjaguar.model.DataFeed;
import com.webjaguar.model.WebjaguarDataFeed;
import com.webjaguar.model.DataFeedSearch;
import com.webjaguar.model.Product;
import com.webjaguar.web.admin.datafeed.WebjaguarCategory;

public interface DataFeedDao {

	List<DataFeed> getDataFeedList(String feed);
	List<Product> getProductDataFeedList(DataFeedSearch search);
	void updateDataFeed(List<DataFeed> list, Map<String, Object> config);	
	void updateDataFeedList(List<DataFeed> list, List<WebjaguarDataFeed> configList);	
	Map<String, Object> getDataFeedConfig(String feed);
	int productDataFeedListCount(DataFeedSearch search);
	int webjaguarDataFeedProductListCount(DataFeedSearch search);
	void nonTransactionSafeUpdateWebjaguarCategories(List<WebjaguarCategory> categories);
	List<WebjaguarCategory> getWebjaguarCategories();
	Map<String, WebjaguarCategory> getWebjaguarCategoryMap();
	List<WebjaguarDataFeed> getWebjaguarDataFeedList(DataFeedSearch search);
	Integer getWebjaguarDataFeedListCount(DataFeedSearch search);
	WebjaguarDataFeed getWebjaguarDataFeed(Integer id, String token, String feedType);
	void insertWebjaguarDataFeed(WebjaguarDataFeed dataFeed);
	void updateWebjaguarDataFeed(WebjaguarDataFeed dataFeed);
	void deleteWebjaguarDataFeed(Integer id);
}
