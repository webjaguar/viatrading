/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.List;
import java.util.Map;

import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LocationSearch;
import com.webjaguar.model.QualifierSearch;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SalesRepSearch;
import com.webjaguar.model.crm.CrmQualifier;

public interface SalesRepDao {

	List<SalesRep> getSalesRepList(SalesRepSearch search);
	List<CrmQualifier> getQualifierList(QualifierSearch search);
	SalesRep getSalesRepByUserId(Integer userId);
	void insertSalesRep(SalesRep salesRep);
	void updateSalesRep(SalesRep salesRep);
	SalesRep getSalesRepById(Integer salesRepId);
	void deleteSalesRep(Integer salesRepId);
	boolean salesRepHasCustomer(Integer salesRepId);
	SalesRep getSalesRepByTerritoryZipcode(String zipcode);
	SalesRep getNextSalesRepInQueue();
	void markSalesRepAssignedToCustomerInCycle(Integer salesRepId, boolean isAssigned);
	List<SalesRep> getSalesRepListByTerritoryZipcode(LocationSearch search);
	Map<String, Long> getSaleRepAccountNumMap();
	SalesRep getSalesRep(SalesRep salesRep);
	List<Customer> getCustomerListBySalesRep(int salesRepId, String sort);
	void updateLoginStats(SalesRep salesRep);
	void deleteOldSalesReps();
	List<SalesRep> getSalesRepTree(int salesRepId);
	List<SalesRep> getSalesRepList(int salesRepId);
	List<String> getSalesRepGroupList();
	List<Integer> getInactiveSalesRepIdList();
	
	// csv feed
	void updateSalesRep(final List <CsvFeed> csvFeedList, final List<Map <String, Object>> data);
	void updateCustomerSalesRepAssoc(final List<Map <String, Object>> data);
}
