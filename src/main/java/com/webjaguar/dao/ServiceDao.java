/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.21.2007
 */

package com.webjaguar.dao;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import com.webjaguar.model.ServiceWork;
import com.webjaguar.model.ServiceSearch;
import com.webjaguar.model.ServiceableItem;
import com.webjaguar.model.ServiceableItemSearch;
import com.webjaguar.model.WorkOrder;

public interface ServiceDao {

	List<ServiceableItem> getServiceableItemList(ServiceableItemSearch search);
	ServiceableItem getServiceableItemById(int id);
	void updateServiceableItem(ServiceableItem item);
	void insertServiceableItem(ServiceableItem item);
	void deleteServiceableItem(ServiceableItem item) throws DataAccessException;
	List<ServiceWork> getServiceList(ServiceSearch search);
	ServiceWork getService(int serviceNum);
	void updateService(ServiceWork service);
	Integer getServiceableItemId(String itemId, String serialNum);
	void insertService(ServiceWork service);
	WorkOrder getWorkOrder(int serviceNum);
	void updateWorkOrder(WorkOrder workOrder);
	ServiceWork getLastService(ServiceableItem item);
	List<WorkOrder> getServiceHistory(ServiceableItem item);
	int getServiceCount(ServiceSearch search, String column);
	int updateServiceableItems(final List<ServiceableItem> items);
	void updateWorkOrderPrinted(int serviceNum);
	void updatePdfService(ServiceWork service, String type);
	Map<Integer, ServiceWork> getServiceMap();
	List<WorkOrder> getIncompleteWorkOrders(String customerField, int defaultInterval);
}
