package com.webjaguar.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.webjaguar.model.SearchEngineProspectEntity;

/**
 * SearchEngineDao is the Database Access Class which defines
 * all of the SQL and database access interacting with the 
 * 'search_engine_prospect' database table.
 */
@Repository
public interface SearchEngineProspectDao {
	
	List<SearchEngineProspectEntity> findByProspectId(String prospectId);
	
	/**
	 * Fetches single entity given its database identity column.
	 * @param id The id of the row
	 * @return A {@link SearchEngineProspectEntity}
	 */
	SearchEngineProspectEntity findById(Integer id);
	
	/**
	 * Returns the last (most recent) recorded data for a given prospect
	 * @param prospectId The prospect identifier
	 * @return A {@link SearchEngineProspectEntity}
	 */
	SearchEngineProspectEntity findLastByProspectId(String prospectId);
	
	/**
	 * Returns the first (oldest) recorded data for a given prospect
	 * @param prospectId The prospect identifier
	 * @return A {@link SearchEngineProspectEntity}
	 */	
	SearchEngineProspectEntity findFirstByProspectId(String prospectId);	
	
	/**
	 * Inserts prospect information into the database.
	 * @param prospectId The prospect identifier
	 * @param referrer The "normalized" referrer domain name
	 * @param queryString The query string
	 */
	void addProspectInformation(String prospectId, String referrer, String queryString);
}
