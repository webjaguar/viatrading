/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import com.webjaguar.model.Address;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Search;
import com.webjaguar.model.Supplier;

public interface SupplierDao {

	List<Supplier> getSuppliers(Integer categoryId) throws DataAccessException;
	List<Supplier> getSuppliers(Search search) throws DataAccessException;
	Integer getProductsCountBySupplierId(Integer supplierId);
	List<Supplier> getSupplierAjax(String company);
	Supplier getSupplierByUserid(Integer userId) throws DataAccessException;
	void insertSupplier(Supplier supplier) throws DataAccessException;
	Integer getSupplierIdByCompany(String company);
	String getSupplierCompanyBySupplierId(Integer suppId);
	Integer getSupplierIdByAccountNumber(String accountNumber);
	Supplier getSupplierByAccountNumber(String accountNumber);
	void insertSupplierByCustomer(Customer customer);
	void updateSupplier(Supplier supplier) throws DataAccessException;
	Supplier getSupplierById(Integer supplierId);
	void updateSupplierById(Supplier supplier) throws DataAccessException;
	void updateSupplierByIdWithMarkup(Supplier supplier) throws DataAccessException;
	void updateSupplierStatusById(Integer id, boolean active) throws DataAccessException;
	void deleteSupplierById(Integer supplierId);
	List<Supplier> getProductSupplierListByOrderId(int orderId, String filter);
	void updateProductSupplier(final int supplierId, final List<Map <String, Object>> data);
	void importSuppliers(List<Supplier> suppliers);
	int supplierCount();
	List<Supplier> getSupplierExportList(int limit, int offset);
	
	//  supplier address
	List<Address> getAddressListBySupplierId(Integer supplierId);
	Address getDefaultAddressBySupplierId(Integer supplierId);
	void updateSupplierAddress(Address address) throws DataAccessException;
	void insertSupplierAddress(Address address) throws DataAccessException;
	Address getSupplierAddressById(Integer addressId);
	void deleteSupplierAddress(Address address) throws DataAccessException;
	void updateSupplierPrimeAddressBySupplierId(Address address) throws DataAccessException;
	
	//supplier name map
	public Map<Integer, String> getSupplierNameMap(List<Integer> suppIdList);
}
