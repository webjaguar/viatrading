package com.webjaguar.dao;

import java.util.List;
import com.webjaguar.model.Deal;
import com.webjaguar.model.DealSearch;
import com.webjaguar.model.ViaDeal;

public interface DealDao {

	// Deal
	List<Deal> getDealsList( DealSearch search );
	Deal getDealById(Integer dealId);
	Deal getDealBySku(String getSku);
	void insertDeal( Deal deal );
	void updateDeal( Deal deal );
	void deleteDealById( Integer dealId );
	
	// via Deal
	List<ViaDeal> getViaDealsList( DealSearch search );
	ViaDeal getViaDealById(Integer dealId);
	ViaDeal getViaDealBySku(String getSku);
	void insertViaDeal( ViaDeal viaDeal );
	void updateViaDeal( ViaDeal viaDeal );
	void deleteViaDealById( Integer dealId );
	
}
