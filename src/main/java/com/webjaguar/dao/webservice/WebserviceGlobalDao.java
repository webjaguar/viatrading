/* Copyright 2011 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 01.31.2011
 */

package com.webjaguar.dao.webservice;

import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.City;
import com.webjaguar.model.County;
import com.webjaguar.model.LocationSearch;
import com.webjaguar.model.ZipCode;

public class WebserviceGlobalDao implements GlobalDao {
	
	private String siteId;
	public void setSiteId(String siteId) { this.siteId = siteId; }
	
	private String url;
    public void setUrl(String url) { this.url = url; }

	private boolean debug;
    public void setDebug(boolean debug) { this.debug = debug; }
    
    private Cipher cipher;
    private SecretKeySpec skeySpec;
	private String secretKey;
	public void setSecretKey(String secretKey) { 
		this.secretKey = secretKey; 
		try {
			this.cipher = Cipher.getInstance("AES");
			this.skeySpec = new SecretKeySpec(Hex.decodeHex(secretKey.toCharArray()), "AES");			
		} catch (Exception e) { 
			if (debug) e.printStackTrace();
		}
	}
    
    public void updateGlobalSiteConfig(Map<String, Object> data) {
    	boolean success = false;
		
		try {
			URL url = new URL(this.url + "/updateGlobalSiteConfig/" + siteId + "/" + md5(secretKey));
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("");
	    	for (String key: data.keySet()) {
	    		request.append("&gkey=" + key);
	    		if (data.get(key) == null) {
		    		request.append("&isNull=true&value=");	    			
	    		} else {
		    		request.append("&isNull=false");
		    		request.append("&value=" + URLEncoder.encode((String) data.get(key), "UTF-8"));	    			
	    		}
	    	}
	    	request.deleteCharAt(0);  // remove leading &
	    				
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(connection.getInputStream());
			Element root = doc.getRootElement();
			success = root.getChildText("Success").equals("true");
		} catch (Exception e) {
			if (debug) e.printStackTrace();
		}
		
		if (!success) {
			// do nothing
		}
	}   
    
	public Map<String, Object> getGlobalSiteConfig() {
		Map<String, Object> globalSiteConfig = new HashMap<String, Object>();

		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

		try {
			SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(this.url + "/getSiteConfig/" + siteId + "/" + md5(secretKey));
			List<Element> elements = doc.getRootElement().getChildren("Column");
			for (Element column: elements) {
				String name = column.getChildText("Name");
				Object value = null;
				String dataType = column.getChildText("DataType");
				if (dataType.contains("java.lang.String")) {
					value = new String(column.getChildText("Value"));
				} else if (dataType.contains("java.lang.Boolean")) {
					value = column.getChildText("Value").equals("true");
				} else if (dataType.contains("java.sql.Date")) {
					value = new java.sql.Date(dateFormatter.parse(column.getChildText("Value")).getTime());
				} else if (dataType.contains("java.lang.Integer")) {
					value = Integer.parseInt(column.getChildText("Value"));
				} else if (dataType.contains("java.lang.Long")) {
					value = Long.parseLong(column.getChildText("Value"));
				}
				globalSiteConfig.put(name, value);
			}
		} catch (Exception e) {
			if (debug) e.printStackTrace();
		}
		return globalSiteConfig;
	} 
	
	public AccessUser getAdminInfoByUserName(String username) {		
		return getAdminInfo(this.url + "/getAdminInfoByUsername/" + siteId + "/" + md5(secretKey) + "/" + username);
	}
	
	public AccessUser getAdminInfoByUsernamePassword(String username, String password) {
		AccessUser accessUser = getAdminInfo(this.url + "/getAdminInfoByUsername/" + siteId + "/" + md5(secretKey) + "/" + username);
		if (accessUser != null) {
			if (password.equals(accessUser.getPassword())) {
				return accessUser;
			}
		}
		return null;
	}
	
	public AccessUser getAdminInfo() {
		return getAdminInfo(this.url + "/getAdminInfoByAuthority/" + siteId + "/" + md5(secretKey) + "/ROLE_ADMIN_" + siteId);
	}
	
	private AccessUser getAdminInfo(String url) {
		AccessUser accessUser = null;
		
		try {
			SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(url);
			Element root = doc.getRootElement();
			if (!root.getAttribute("id").getValue().equals("")) {
				accessUser = new AccessUser();
				accessUser.setId(root.getAttribute("id").getIntValue());
	        	accessUser.setUsername(root.getChildText("Username"));
	        	accessUser.setPassword(decrypt(secretKey, root.getChildText("Password")));
	        	accessUser.setEnable(root.getChildText("Enabled").equalsIgnoreCase("true"));
	        	accessUser.setIpAddress(root.getChildTextTrim("IpAddress"));
	        	// get roles
				accessUser.setRoles(new ArrayList<String>());
				for (Object role: root.getChild("Roles").getChildren()) {
					accessUser.getRoles().add(((Element) role).getText());
				}
			}			
		} catch (Exception e) {
			if (debug) e.printStackTrace();
		}
		
		return accessUser;
	}
	
	public void updateAdminInfo(AccessUser user) {
		boolean success = false;
		
		try {
			URL url = new URL(this.url + "/updateAdminInfo/" + siteId + "/" + md5(secretKey));
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("username=" + encrypt(secretKey, user.getUsername()) 
													+ "&password=" + encrypt(secretKey, user.getPassword())
													+ "&enabled=" + user.isEnable());
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(connection.getInputStream());
			Element root = doc.getRootElement();
			success = root.getChildText("Success").equals("true");
		} catch (Exception e) {
			if (debug) e.printStackTrace();
		}
		
		if (!success) {
			// do nothing
		}
	}
	
	public City getCityInfoByZipcode(String zipcode) {
		try {
			SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(this.url + "/getCity/" + siteId + "/" + md5(secretKey) + "/" + zipcode);
			Element root = doc.getRootElement();
			for (Object element: root.getChildren("City")) {
				return new City(
						  ((Element) element).getChildText("Country")
						, ((Element) element).getChildText("State")
						, ((Element) element).getChildText("County")
						, ((Element) element).getChildText("Name"));
			}
		} catch (Exception e) {
			if (debug) e.printStackTrace();
		}
		
		return null;
	}

	public List<City> getCities(City city) {
		List<City> cities = new ArrayList<City>();
		
		try {
			SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(this.url + "/getCities/" + siteId + "/" + md5(secretKey) + "/?state=" + city.getState() + "&county=" + city.getCounty());
			Element root = doc.getRootElement();
			for (Object element: root.getChildren("City")) {
				cities.add(new City(
						  ((Element) element).getChildText("Country")
						, ((Element) element).getChildText("State")
						, ((Element) element).getChildText("County")
						, ((Element) element).getChildText("Name")));
			}
		} catch (Exception e) {
			if (debug) e.printStackTrace();
		}
		
		return cities;
	}
	
	public List<County> getCounties(County county) {
		List<County> counties = new ArrayList<County>();
		
		try {
			SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(this.url + "/getCounties/" + siteId + "/" + md5(secretKey) + "/" + county.getState());
			Element root = doc.getRootElement();
			for (Object element: root.getChildren("County")) {
				counties.add(new County(
						((Element) element).getChildText("Country")
						, ((Element) element).getChildText("State")
						, ((Element) element).getChildText("Name")));
			}
		} catch (Exception e) {
			if (debug) e.printStackTrace();
		}
		
		return counties;
	}
	
	public ZipCode getZipCode(String zipcode) {
		try {
			SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(this.url + "/getZipCode/" + siteId + "/" + md5(secretKey) + "/" + zipcode);
			Element root = doc.getRootElement();
			for (Object element: root.getChildren("ZipCode")) {
				ZipCode zipCode = new ZipCode();
				zipCode.setZipCode(((Element) element).getChildText("Zip"));
				zipCode.setStateAbbv(((Element) element).getChildText("State"));
				zipCode.setLatitude(Double.parseDouble(((Element) element).getChildText("Latitude")));
				zipCode.setLongitude(Double.parseDouble(((Element) element).getChildText("Longitude")));
				zipCode.setCity(((Element) element).getChildText("City"));
				zipCode.setCounty(((Element) element).getChildText("County"));
				zipCode.setZipClass(((Element) element).getChildText("Class"));
				return zipCode;
			}
		} catch (Exception e) {
			if (debug) e.printStackTrace();
		}
		
		return null;
	}
	
	public List<ZipCode> getZipCodeList(LocationSearch locationSearch) {
		List<ZipCode> zipCodes = new ArrayList<ZipCode>();
		
		try {
			SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(this.url + "/getZipCodeList/" + siteId + "/" + md5(secretKey) + "/?latitude=" + locationSearch.getZipcodeObject().getLatitude() 
					+ "&longitude=" + locationSearch.getZipcodeObject().getLongitude()
					+ "&radius=" + locationSearch.getRadius());
			Element root = doc.getRootElement();
			for (Object element: root.getChildren("ZipCode")) {
				ZipCode zipCode = new ZipCode();
				zipCode.setZipCode(((Element) element).getChildText("Zip"));
				zipCode.setStateAbbv(((Element) element).getChildText("State"));
				zipCode.setLatitude(Double.parseDouble(((Element) element).getChildText("Latitude")));
				zipCode.setLongitude(Double.parseDouble(((Element) element).getChildText("Longitude")));
				zipCode.setCity(((Element) element).getChildText("City"));
				zipCode.setCounty(((Element) element).getChildText("County"));
				zipCode.setZipClass(((Element) element).getChildText("Class"));
				zipCodes.add(zipCode);
			}
		} catch (Exception e) {
			if (debug) e.printStackTrace();
		}
		
		return zipCodes;
	}
	
	private String md5(String data) {
		String hashword = null;
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(data.getBytes());
			BigInteger hash = new BigInteger(1, md5.digest());
			hashword = hash.toString(16);
		} catch (NoSuchAlgorithmException nsae) {
			if (debug) nsae.printStackTrace();
			return null;
		}
		hashword = pad(hashword, 32, '0');
		return hashword;
	}
	
	private String pad(String s, int length, char pad) {
		StringBuffer buffer = new StringBuffer(s);
		while (buffer.length() < length) {
			buffer.insert(0, pad);
		}
		return buffer.toString();
	}
	
	private String encrypt(String secretKey, String input) {
		try {				
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
			byte[] encrypted = cipher.doFinal(input.getBytes());
			return(new String(Hex.encodeHex(encrypted)));
		} catch (Exception e) {
			if (debug) e.printStackTrace();
			return null;
		}
	}
	
	private String decrypt(String secretKey, String input) {
		try {
			cipher.init(Cipher.DECRYPT_MODE, skeySpec);
			byte[] original = cipher.doFinal(Hex.decodeHex(input.toCharArray()));
			return(new String(original));
		} catch (Exception e) {
			if (debug) e.printStackTrace();
			return null;
		}
	}
	

}
