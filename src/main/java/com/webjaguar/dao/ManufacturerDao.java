package com.webjaguar.dao;


import java.util.List;
import java.util.Map;

import com.webjaguar.model.Manufacturer;
import com.webjaguar.model.ManufacturerSearch;


public interface ManufacturerDao {
	
	Manufacturer getManufacturerById(Integer manufacturerId);
	Manufacturer getManufacturerByName(String name);
	List<Manufacturer> getManufacturerList(ManufacturerSearch search);
	void insertManufacturer( Manufacturer manufacturer );
	void deleteManufacturer(Integer manufacturerId);
	void updateManufacturer(Manufacturer manufacturer);
	void updateProductManufacturerName(String old, String newName);
	void updateShoppingcartManufacturerName(String old, String newName);
	Map<String, Manufacturer> getManufacturerMap();
	boolean isProductExist(String manufacturerName);
}
