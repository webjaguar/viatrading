/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.03.2010
 */

package com.webjaguar.dao;

import java.util.List;
import java.util.Map;

import com.webjaguar.thirdparty.echosign.EchoSign;
import com.webjaguar.thirdparty.echosign.EchoSignSearch;
import com.webjaguar.thirdparty.echosign.EchoSignWidgetSearch;
import com.webjaguar.thirdparty.echosign.Widget;

public interface EchoSignDao {

	void insertEchoSign(EchoSign echoSign, boolean insertIgnore);
	List<EchoSign> getEchoSignListByUserid(int userId);
	void updateEchoSign(EchoSign echoSign);
	EchoSign getEchoSign(String documentKey);
	Map<Integer, Long> getEchoSignSentMap(int year);
	List<EchoSign> getEchoSignBySearch(EchoSignSearch search);
	void insertEchoSignWidget(Widget widget);
	Widget getEchoSignWidgetByPath(String path);
	Widget getEchoSignWidget(String documentKey);
	List<Widget> getEchoSignWidgetBySearch(EchoSignWidgetSearch search);
	Map<Integer, EchoSign> getLatestEchoSignMap();
}
