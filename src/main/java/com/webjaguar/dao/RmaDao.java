/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.16.2008
 */

package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.LineItem;
import com.webjaguar.model.Rma;
import com.webjaguar.model.RmaSearch;

public interface RmaDao {
	
	Integer getLastOrderId(String serialNum);
	void insertRma(Rma rma);
	List<Rma> getRmaList(RmaSearch search);
	Rma getRma(Rma rma);
	void updateRma(Rma rma);
	LineItem getLineItem(int orderId, String serialNum);
}
