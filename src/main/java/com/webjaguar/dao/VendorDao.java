/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 06.26.2009
 */

package com.webjaguar.dao;

import java.util.List;
import java.util.Map;

import com.webjaguar.thirdparty.dsi.DsiCategory;
import com.webjaguar.thirdparty.ingrammicro.IngramMicroCategory;
import com.webjaguar.thirdparty.koleImports.KoleImportsCategory;
import com.webjaguar.thirdparty.dsdi.DsdiCategory;
import com.webjaguar.thirdparty.synnex.SynnexCategory;
import com.webjaguar.thirdparty.techdata.TechdataCategory;
import com.webjaguar.thirdparty.techdata.TechdataManufacturer;

public interface VendorDao {

	// TechData
	void insertTechdataManufacturers(List<TechdataManufacturer> manufacturers);
	void insertTechdataCategories(List<TechdataCategory> categories);
	List<TechdataCategory> getTechdataCategories();
	void updateTechdataCategories(List<TechdataCategory> categories);
	List<TechdataManufacturer> getTechdataManufacturers();
	List<String> getTechdataRestrictedProductLines();
	Map<String, TechdataCategory> getTechdataCategoryMap();
	Map<String, String> getTechdataManufacturerMap();
	
	// IngramMicro
	void insertIngramMicroCategories(List<IngramMicroCategory> categories);
	List<IngramMicroCategory> getIngramMicroCategories();
	void updateIngramMicroCategories(List<IngramMicroCategory> categories);
	Map<String, IngramMicroCategory> getIngramMicroCategoryMap();
	int updateIngramMicroInventoryBySkus(final List<Map <String, Object>> data);
	
	// Synnex
	void insertSynnexCategories(List<SynnexCategory> categories);
	List<SynnexCategory> getSynnexCategories();
	void updateSynnexCategories(List<SynnexCategory> categories);
	Map<String, SynnexCategory> getSynnexCategoryMap();
	
	// DSI
	void insertDsiCategories(List<DsiCategory> categories);
	List<DsiCategory> getDsiCategories();
	void updateDsiCategories(List<DsiCategory> categories);
	Map<String, DsiCategory> getDsiCategoryMap();

	// ASI
	List<String> getAsiSupplier();
	/* TO BE REMOVE
	List<AsiCategory> getAsiCategoriesBySupplier(String supplier);
	void updateAsiCategories(List<AsiCategory> categories);
	Map<String, AsiCategory> getAsiCategoryMap();
	void insertAsiCategories(List<AsiCategory> categories);
	List<AsiCategory> getAsiCategories(Search search);
	*/
	// Kole Imports
	void insertKoleImportsCategories(List<KoleImportsCategory> categories);
	List<KoleImportsCategory> getKoleImportsCategories();
	void updateKoleImportsCategories(List<KoleImportsCategory> categories);
	Map<String, KoleImportsCategory> getKoleImportsCategoryMap();
	
	// Dsdi Imports
	void insertDsdiCategories(List<DsdiCategory> categories);
	List<DsdiCategory> getDsdiCategories();
	void updateDsdiCategories(List<DsdiCategory> categories);
	Map<String, DsdiCategory> getDsdiCategoryMap();

}
