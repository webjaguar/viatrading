/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.PurchaseOrder;
import com.webjaguar.model.PurchaseOrderLineItem;
import com.webjaguar.model.PurchaseOrderSearch;
import com.webjaguar.web.form.PurchaseOrderForm;

public interface InventoryDao {

	void insertPurchaseOrder(PurchaseOrderForm purchaseOrderForm);
	void updatePurchaseOrder(PurchaseOrderForm purchaseOrderForm);
	PurchaseOrder getPurchaseOrder(int poId);
	void insertPurchaseOrderStatus(PurchaseOrderForm purchaseOrderForm, boolean inventoryHistory) throws DataAccessException;
	List getPurchaseOrderStatusHistory(int poId);
	List getPurchaseOrderList(PurchaseOrderSearch purchaseOrderSearch);
	Integer getPurchaseOrderIdByName( String poNumber );
	int purchaseOrderCount(Integer orderId);
	List<PurchaseOrderLineItem> getPOLineItemsQuantity(Integer orderId, boolean dropShip);
	PurchaseOrderLineItem getLineItemByPOId(PurchaseOrderLineItem lineItem);
	void deletePurchaseOrder(Integer poId);
	void updateInventoryBySkus(final List<CsvFeed> csvFeedList, final List<Map <String, Object>> data);
	void adjustInventoryBySku(InventoryActivity inventoryActivity);
	void importInventory(InventoryActivity inventoryActivity);
	Inventory getInventory(Inventory inventory);
	void updateInventory(Inventory inventory);
	public void addKitPartsHistory(InventoryActivity kitActivity);
	List<String>  getPoPTList();
	void updatePoPtById(int poId, String pt);
	void updatePoStatusById(int poId, String status);
	Integer getPurchaseOrderCountById( Integer poId );
	void addNewPurchaseOrderHistoryStatus(Integer poId, String status, String accessUser);
}
