package com.webjaguar.dao;

import java.util.List;

import com.webjaguar.model.Customer;
import com.webjaguar.model.InvoiceImport;
import com.webjaguar.model.InvoiceImportLineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderQuote;
import com.webjaguar.model.PaymentImport;

public interface ImportInvoiceDao {
	public List<InvoiceImport> getAllOrders();
	public List<InvoiceImportLineItem> getLineItems(Integer id);
	public List<PaymentImport> getOrderPayments(Integer id);
	public List<PaymentImport> getPayments();
	public void deleteAllOrders();
	public void deleteAllOrdersLineItems();
	public void deleteAllPayments();
	public void deleteOrder(Integer id);
	public void deleteOrderLineItems(Integer id);
	public void deletePayments(Integer id);
	public List<OrderQuote> getOrderQuoteFromStaging();
	public void  deleteAllOrderQuoteFromStaging();
}
