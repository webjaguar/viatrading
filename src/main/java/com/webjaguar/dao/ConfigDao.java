/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 10.10.2006
 */
package com.webjaguar.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;

import com.webjaguar.model.City;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Contact;
import com.webjaguar.model.Country;
import com.webjaguar.model.County;
import com.webjaguar.model.CsvData;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.CustomerGroup;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.Language;
import com.webjaguar.model.PaymentMethod;
import com.webjaguar.model.Search;
import com.webjaguar.model.ShippingHandling;
import com.webjaguar.model.ShippingMethod;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.SiteMessageGroup;
import com.webjaguar.model.SiteMessageGroupSearch;
import com.webjaguar.model.SiteMessageSearch;
import com.webjaguar.model.State;

public interface ConfigDao {
	
	// Country
    Country getCountryByCode(String code);
    String getCountryCodeByName(String name);
	void updateCountryList(List data);
	List<Country> getCountryList(boolean stats);
	List<Country> getEnabledCountryList();
	Map<String, Object> getRegionMap();
	List<Country> getRegion();

	
	// State	
	State getStateByCode(String country, String code);
	void updateStateList(List data);
	List<State> getStateList(String country);
	List<State> getNationalRegionList();
	
	// Shipping method
	ShippingMethod getShippingMethodById(Integer id);
	ShippingMethod getShippingMethodByCarrierCode(ShippingRate shippingRate);
	void updateShippingMethodList(List<ShippingMethod> data);
	List<ShippingMethod> getShippingMethodList(Boolean active);
	boolean isActiveCarrier(String carrierCode);
	ShippingHandling getShippingHandling();
	void updateShippingHandling(ShippingHandling shippingHandling);
	
	// custom shipping
	List<ShippingRate> getCustomShippingRateList();
	List<Contact> getCustomShippingContactList(Search search);
	List<Contact> getActiveCustomShippingContactList (Search search);
	Contact getCustomShippingContactById(Integer id);
	void insertCustomShippingContact(Contact contact);
	void updateCustomShippingContact(Contact contact);
	void deleteCustomShippingContact(Integer contactId) throws DataAccessException;
	void insertCustomShipping(ShippingRate shippingRate);
	void updateCustomShipping(ShippingRate shippingRate);
	ShippingRate getCustomShippingById(Integer customShippingId);
	Double getHandlingByZipcode(String country, String zipcode);
	
	// Payment Method
	List<PaymentMethod> getCustomPaymentMethodList(Integer custId);
	void updateCustomPaymentMethods(List<PaymentMethod> customPayments);
	
	// Site Configuration	
	Map<String, Configuration> getSiteConfig();
	void updateSiteConfig(List<Configuration> data);
	
	// Site Message
	void insertSiteMessage( SiteMessage siteMessage );
	void insertEmailHistory(Integer userId, Integer contactId, Integer messageId, Date created);
	void updateSiteMessage( SiteMessage siteMessage );
	SiteMessage getSiteMessageByName( String messageName );
	SiteMessage getSiteMessageById( Integer messageId );
	List<SiteMessage> getSiteMessageList();
	void deleteSiteMessage( Integer messageId );
	void inserSiteMessageMapping(SiteMessage siteMessage);
	List<SiteMessage> getSiteMessageList(SiteMessageSearch siteMessageSearch);
	
	// Site Message Group
	List<SiteMessageGroup> getSiteMessageGroupList();
	void insertSiteMessageGroup(SiteMessageGroup group);
	void updateSiteMessageGroup(SiteMessageGroup group);
	SiteMessageGroup getSiteMessageGroupById(Integer groupId);
	void deleteSiteMessageGroupById(Integer groupId);
	List<SiteMessageGroup> getSiteMessageGroupList(SiteMessageGroupSearch search);
	
	// language
	void updateSiteMessageLanguage (Language language);
	Language getLanguageSetting(String languageCode);
	List<Language> getLanguageSettings();
	void insertLanguage (String languageCode);
	void deleteLanguage(String languageCode);
	
	// Admin Message
	SiteMessage getAdminMessageById(int messageId);
	void updateAdminMessage(SiteMessage adminMessage);
	
	// Counties
	List<County> getCounties(String country, String state);
	void updateCountyList(List<County> counties);
	County getTaxRateByCounty(County county);
	
	// Cities
	List<City> getCities(City city );
	void updateCityList(List<City> cities);
	City getTaxRateByCity(City city);
	
	// CSV
	Map<String, CsvFeed> getCsvFeedMapping(String feed);
	List<CsvFeed> getCsvFeed(String feed);
	Map<String, CsvData> getCsvFeedMap(String feed);

	// Labels
	Map<String, String> getLabels(String ... prefix);
	public Map<Integer, Integer> getSiteMessageListByGroupId(Integer groupId);
	void updateLabels(List<Map<String, String>> labels);
	
	public Contact getCompanyContactId(String company);
	
}