/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 08.02.2007
 */

package com.webjaguar.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.webjaguar.model.Order;
import com.webjaguar.model.Payment;
import com.webjaguar.model.PaymentSearch;

public interface PaymentDao {

	List<Payment> getCustomerPaymentList(PaymentSearch search);
	List<Payment> getPaymentExportList(PaymentSearch search);
	List<Payment> getCustomerPaymentListByOrder(PaymentSearch search);
	void updatePaymentExported (Set<Integer> paymentIds);
	Payment getCustomerPaymentById(int id);
	void updateCustomerPayment(Payment payment, Collection<Order> invoices);
	void insertCustomerPayment(Payment payment, Collection<Order> invoices);
	void insertOrderPayment(Payment payment, Collection<Order> invoices);
	void insertCustomerPayment(Payment payment);
	void insertPayments(Payment payment);
	Map<Integer, Order> getInvoices(PaymentSearch search, String searchType);
	Integer getFirstPaymentId(Integer order_id);
	Double getPaymentAmount(Integer paymentId, Integer orderId);
	Integer checkPreviousPaid(Integer paymentId, Integer orderId);
}
