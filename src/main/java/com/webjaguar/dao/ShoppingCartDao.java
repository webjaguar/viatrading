/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;

public interface ShoppingCartDao {
	
	void addToCart(CartItem cartItem);
	
	Cart getCart(int userId, String manufacturerName);
	
	void incrementQuantityByCartItem(CartItem cartItem);
	
	void removeCartItem(CartItem cartItem);
    void removeCartItembyApiIndex(String apiIndex, Integer userId, Integer productId);

	
	public void updateCartItem(CartItem cartItem);
	
    void updateCartItemByApiIndex(CartItem cartItem, Integer productId);
	
	String getCustomImageUrl(CartItem cartItem);
	
	void deleteShoppingCart(int userid, String manufacturerName);

}
