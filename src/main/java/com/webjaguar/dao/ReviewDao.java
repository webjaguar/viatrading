/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;


import java.util.List;
import java.util.Map;

import com.webjaguar.model.CompanyReview;
import com.webjaguar.model.ProductReview;
import com.webjaguar.model.Review;
import com.webjaguar.model.ReviewSearch;


public interface ReviewDao {
	// product review
	List<ProductReview> getProductReviewList(ReviewSearch search);
	int getProductReviewListCount(ReviewSearch search);
	void insertProductReview(ProductReview productReview);
	void deleteProductReviewById(Integer productReviewId);
	List<ProductReview> getProductReviewListByProductSku(String productSku, Boolean active, Integer limit);
	ProductReview getProductReviewById(Integer productReviewId);
    void updateProductReview(ProductReview productReview);
    ProductReview getProductReviewAverage(String productSku);
    boolean checkPreviousReview(ProductReview productReview);
    // company review
    List<CompanyReview> getcompanyReviewList(ReviewSearch search);
	int getCompanyReviewListCount(ReviewSearch search);
	void insertCompanyReview(CompanyReview companyReview);
	void deleteCompanyReviewById(Integer companyReviewId);
	List<ProductReview> getCompanyReviewListByCompanyId(Integer companyId, Boolean active);
	CompanyReview getCompanyReviewById(Integer companyReviewId);
	void updateCompanyReview(CompanyReview companyReview);
	Review getCompanyReviewAverage(Integer companyId); 
}
