/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;

import com.webjaguar.model.BuyRequest;
import com.webjaguar.model.BuyRequestSearch;
import com.webjaguar.model.BuyRequestVersion;
import com.webjaguar.model.Product;

public interface BuyRequestDao {

	List<BuyRequest> getBuyRequestListByUserid(BuyRequestSearch buyRequestSearch);
	List<BuyRequest> searchBuyRequests(BuyRequestSearch search);
	int searchBuyRequestsCount(BuyRequestSearch search);
	void deleteBuyRequestById(Integer buyRequestId);
	void updateBuyRequest(BuyRequest buyRequest);
	BuyRequest getBuyRequestById(Integer buyRequestId, Integer userId);
	int insertBuyRequest(BuyRequest buyRequest);
	List<BuyRequestVersion> getBuyRequestVesrionList(BuyRequestVersion buyRequestVersion);
	void insertBuyRequestVesrion(BuyRequestVersion buyRequestVersion);
	
	// wishList
	List<BuyRequest> getBuyRequestWishListByUserid(BuyRequestSearch search);
	void addToBuyRequestWishList(Integer userId, Set buyRequestIds);
    void removeFromBuyRequestWishList(Integer userId, Set buyRequestIds);
}
