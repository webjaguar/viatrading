/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import com.webjaguar.model.Ticket;
import com.webjaguar.model.TicketSearch;
import com.webjaguar.model.TicketVersion;
import com.webjaguar.web.form.TicketForm;

public interface TicketDao {

	public List getTicketsListByUserid(Integer userId, TicketSearch ticketSearch);
	
	public List getTicketsList(TicketSearch ticketSearch);
	
	public Ticket getTicketById(Integer ticketId);
	
	public List<TicketForm> getTicketVersionListById(Integer ticketId, Integer userId);
	
	public int insertTicket(Ticket ticket);
	
	public void insertTicketVersion(TicketVersion ticketVersion);
	
	public void updateTicketLastModified(Integer ticketId, String versionCreatedByType);
	
	public void updateTicketStatus(Ticket ticket);

}
