/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 10.10.2006
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.ConfigDao;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.model.City;
import com.webjaguar.model.ConditionShipping;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Contact;
import com.webjaguar.model.Country;
import com.webjaguar.model.County;
import com.webjaguar.model.CsvData;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.EmailHistory;
import com.webjaguar.model.Language;
import com.webjaguar.model.PaymentMethod;
import com.webjaguar.model.Search;
import com.webjaguar.model.ShippingHandling;
import com.webjaguar.model.ShippingMethod;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.SiteMessageGroup;
import com.webjaguar.model.SiteMessageGroupSearch;
import com.webjaguar.model.SiteMessageSearch;
import com.webjaguar.model.State;

public class IbatisConfigDao extends SqlMapClientDaoSupport implements ConfigDao {

	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	// Country
	public Country getCountryByCode(String code) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("code", code);
		return (Country) getSqlMapClientTemplate().queryForObject("getCountryList", paramMap);
	}
	
	public String getCountryCodeByName(String name){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("name", name);
		return (String) getSqlMapClientTemplate().queryForObject("getCountryCode", paramMap);
	}
    public void updateCountryList(final List data) {
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
        String sql = "UPDATE country SET rank = ?, enabled = ?, tax_rate = ?, region = ? WHERE code = ?";		
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
			    Country country = (Country)data.get(i);
				ps.setInt(1, (Integer)(country.getRank()));
				ps.setBoolean(2, (Boolean)(country.getEnabled()));
				ps.setDouble(3, (Double)(country.getTaxRate()));
				ps.setString(4, (country.getRegion()));
				ps.setString(5,(country.getCode()));
			}
			public int getBatchSize() {
				return data.size();
			}
		});
			
	}

	public List<Country> getCountryList(boolean stats) {
		List<Country> countries = getSqlMapClientTemplate().queryForList("getCountryList", null);
		if (stats) {
			Map<String, Country> getCustomerCountByCountry = getSqlMapClientTemplate().queryForMap("getCustomerCountByCountry", null, "code");
			Map<String, Country> getOrderCountByCountry = getSqlMapClientTemplate().queryForMap("getOrderCountByCountry", null, "code");
			for (Country country: countries) {
				if (getCustomerCountByCountry.containsKey(country.getCode())) {
					country.setCustomerCount(getCustomerCountByCountry.get(country.getCode()).getCustomerCount());
				}
				if (getOrderCountByCountry.containsKey(country.getCode())) {
					country.setOrderCount(getOrderCountByCountry.get(country.getCode()).getOrderCount());
				}
			}
		}
		return countries;
	}

	public List<Country> getEnabledCountryList() {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("enabled", true);
		return getSqlMapClientTemplate().queryForList("getCountryList", paramMap);
	}
	
	// State
	
	public State getStateByCode(String country, String code) {
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put( "country", country );
		paramMap.put( "code", code );
		return (State)getSqlMapClientTemplate().queryForObject("getStateByCode", paramMap);
	}
	
	public List getStateList(String country) {
		return getSqlMapClientTemplate().queryForList("getStateList", country); 
	}
	
	public List getNationalRegionList() {
		return getSqlMapClientTemplate().queryForList("getNationalRegionList"); 
	}
	
    public void updateStateList(final List data) {
		
    	JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
        String sql = "UPDATE state SET tax_rate = ?, apply_shipping_tax = ?, region = ? WHERE code = ?";		
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				State state = (State) data.get(i);
				ps.setDouble(1, state.getTaxRate());					
				ps.setBoolean(2, state.isApplyShippingTax());
				ps.setString(3, state.getRegion());
				ps.setString(4, state.getCode());
			}
			public int getBatchSize() {
				return data.size();
			}
		});
		
	}
    
    public Map<String, Object> getRegionMap() {	
    	return getSqlMapClientTemplate().queryForMap("getRegionMap", null, "country_state");
	}
    public List<Country> getRegion(){
    	return getSqlMapClientTemplate().queryForList("getRegion");
    } 
	
	// Site Configuration
    public Map<String, Configuration> getSiteConfig() {
    	Map<String, Configuration> siteConfig = new HashMap<String, Configuration>();
    	Iterator iter = getSqlMapClientTemplate().queryForList("getSiteConfig", null).iterator();
    	while (iter.hasNext()) {
    		Configuration config = (Configuration) iter.next();
    		siteConfig.put(config.getKey(), config);
    	}
    	return siteConfig;
    }
    
    // ShippingMethod
    public ShippingMethod getShippingMethodById(final Integer id){
    	return (ShippingMethod)getSqlMapClientTemplate().queryForObject("getShippingMethodById",id);
    }
    
    public ShippingMethod getShippingMethodByCarrierCode(final ShippingRate shippingRate){
    	return (ShippingMethod)getSqlMapClientTemplate().queryForObject("getShippingMethodByCarrierCode",shippingRate);
    }
	
	public void updateShippingMethodList(List<ShippingMethod> data){
		for ( ShippingMethod shippingMethod : data )
		{
			getSqlMapClientTemplate().update( "updateShippingMethodList", shippingMethod );
		}		
	}
	
	public List<ShippingMethod> getShippingMethodList(Boolean active){
		return getSqlMapClientTemplate().queryForList("getShippingMethodList", active);
	}
	
	public boolean isActiveCarrier(String carrierCode){
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String sql = "SELECT count(*) FROM shipping_method WHERE carrier = ? AND shipping_active is TRUE";
		Object[] args = new Object[]{carrierCode};
		int i = jdbcTemplate.queryForInt(sql,args);
		if(i>=1){
			return true;
		}
		else{
			return false;
		}
	}
	
	public void updateSiteConfig(final List<Configuration> data) {		
    	JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
        String sql = "UPDATE site_config SET config_value = ? WHERE config_key = ?";		
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Configuration config = data.get(i);
				ps.setString(1, config.getValue());
				ps.setString(2, config.getKey());
			}
			public int getBatchSize() {
				return data.size();
			}
		});
	}
	
	//	 Site Message
	public void insertSiteMessage( SiteMessage siteMessage )
	{
		getSqlMapClientTemplate().insert("insertSiteMessage", siteMessage);
		this.inserSiteMessageMapping( siteMessage);

	}
	public void updateSiteMessage( SiteMessage siteMessage )
	{
		getSqlMapClientTemplate().update("updateSiteMessage", siteMessage, 1);
		getSqlMapClientTemplate().delete("deleteSiteMessageMapping", siteMessage.getMessageId());
		this.inserSiteMessageMapping(siteMessage);
	}
	public SiteMessage getSiteMessageById( Integer messageId )
	{
		SiteMessage siteMessage = (SiteMessage)getSqlMapClientTemplate().queryForObject("getSiteMessageById",messageId);
		if(messageId > 0) {
			siteMessage.setGroupIdList(getSqlMapClientTemplate().queryForList("getSiteMessageMappingById", messageId));
		}
		return siteMessage;
	}
	public SiteMessage getSiteMessageByName( String messageName )
	{
		return (SiteMessage)getSqlMapClientTemplate().queryForObject("getSiteMessageByName",messageName);
	}
	public List<SiteMessage> getSiteMessageList()
	{
		return getSqlMapClientTemplate().queryForList("getSiteMessageList", null);
	}
	public void deleteSiteMessage( Integer messageId )
	{
		getSqlMapClientTemplate( ).delete( "deleteSiteMessage", messageId );
	}
	public void inserSiteMessageMapping(SiteMessage siteMessage) {
		if (siteMessage.getGroupIdList() != null){
			for (Integer groupId: siteMessage.getGroupIdList()){
				if(groupId == null) {
					continue;
				}
				siteMessage.setGroupId(groupId);
				getSqlMapClientTemplate().insert("insertSiteMessageMapping", siteMessage);
			}
		}
	}
	public List<SiteMessage> getSiteMessageList(SiteMessageSearch siteMessageSearch) {
		return getSqlMapClientTemplate().queryForList("getSiteMesaageListBySearch", siteMessageSearch);
	}
	
	// Site Message Group
	public List<SiteMessageGroup> getSiteMessageGroupList()
	{
		return getSqlMapClientTemplate().queryForList("getSiteMessageGroupList", null);
	}	
	
	public void insertSiteMessageGroup(SiteMessageGroup group) throws DataAccessException {
		getSqlMapClientTemplate().insert("insertSiteMessageGroup", group);
	}
	
	public void insertEmailHistory(Integer userId, Integer contactId, Integer messageId, Date created) {
		EmailHistory emailHistory = new EmailHistory();
		emailHistory.setCrmContactId(contactId);
		emailHistory.setUserId(userId);
		emailHistory.setMessageId(messageId);
		emailHistory.setCreated(created);
		getSqlMapClientTemplate().insert("insertEmailHistory", emailHistory);
	}
	
	public void updateSiteMessageGroup(SiteMessageGroup group) {
		getSqlMapClientTemplate().update("updateSiteMessageGroup", group, 1);
	}
	
	public SiteMessageGroup getSiteMessageGroupById(Integer groupId) {
		return (SiteMessageGroup) getSqlMapClientTemplate().queryForObject("getSiteMessageGroupById", groupId);
	}
	
	public void deleteSiteMessageGroupById(Integer groupId) {
		getSqlMapClientTemplate().delete("deleteSiteMessageGroupById", groupId);
		getSqlMapClientTemplate().delete("deleteGroupSiteMessageByGroupId", groupId);
	}
	
	public List<SiteMessageGroup> getSiteMessageGroupList(SiteMessageGroupSearch search) {
		return getSqlMapClientTemplate().queryForList("getSiteMessageGroupListBySearch", search);
	}

	//language	
	public void updateSiteMessageLanguage (Language languages) {		
		getSqlMapClientTemplate().update("updateSiteMessageLanguage", languages);		
	}
	public Language getLanguageSetting(String languageCode) {		
		return (Language)getSqlMapClientTemplate().queryForObject("getLanguageSetting",languageCode);
	}
	public List<Language> getLanguageSettings() {		
		return getSqlMapClientTemplate().queryForList("getLanguageSettings");
	}	
	public void insertLanguage (String languageCode) {
		getSqlMapClientTemplate().insert("insertLanguage", languageCode);
	}
	public void deleteLanguage(String languageCode) {
		getSqlMapClientTemplate( ).delete("deleteLanguage", languageCode);
	}
	
	// Admin Message
	public SiteMessage getAdminMessageById(int messageId) {
		return (SiteMessage) getSqlMapClientTemplate().queryForObject("getAdminMessageById",messageId);		
	}
	public void updateAdminMessage(SiteMessage adminMessage) {
		getSqlMapClientTemplate().update("updateAdminMessage", adminMessage, 1);
	}
	
	public ShippingHandling getShippingHandling() {
		return (ShippingHandling) getSqlMapClientTemplate().queryForObject("getShippingHandling");
	}
	
	public void updateShippingHandling(ShippingHandling shippingHandling) {
		getSqlMapClientTemplate().update("updateShippingHandling", shippingHandling, 1);
	}
	
	// custom shipping
	public List<ShippingRate> getCustomShippingRateList() {
		List<ShippingRate> shippingRateList = getSqlMapClientTemplate().queryForList("getCustomShippingRateList", null);
		if(shippingRateList != null && shippingRateList.size() != 0){
			Iterator<ShippingRate> iter = shippingRateList.iterator();
			while(iter.hasNext()){
				ShippingRate shippingRate = iter.next();
				if (shippingRate!= null && getConditionShipping(shippingRate.getId()).getCondition()!=null) {
					
					if(getConditionShipping(shippingRate.getId()).getCondition().get("Group")!=null){
						Map<Object,String> innerMap = getConditionShipping(shippingRate.getId()).getCondition().get("Group");
						Set<Object> set = innerMap.keySet() ;
						String operator = innerMap.get(set.toArray()[0]);
						
						shippingRate.setGroupIdSet( set );
						shippingRate.setGroupType( operator );
					}
					if(getConditionShipping(shippingRate.getId()).getCondition().get("States")!=null){
						Map<Object,String> innerMap = getConditionShipping(shippingRate.getId()).getCondition().get("States");
						Set<Object> set = innerMap.keySet() ;
						String operator = innerMap.get(set.toArray()[0]);
						
						shippingRate.setStates( set.toArray()[0].toString() );
						shippingRate.setStateType( operator );
					}
				}
			}
		}
		return shippingRateList;
	}
	public List<Contact> getCustomShippingContactList(Search search) {
		return getSqlMapClientTemplate().queryForList("getCustomShippingContactList", search);
	} 
	
	public List<Contact> getActiveCustomShippingContactList(Search search) {
		return getSqlMapClientTemplate().queryForList("getActiveCustomShippingContactList", search);
	}
	public Contact getCustomShippingContactById(Integer contactId) {
		return (Contact) getSqlMapClientTemplate().queryForObject("getCustomShippingContactById", contactId);
	}
	public void insertCustomShippingContact(Contact contact) {
		getSqlMapClientTemplate().insert("insertCustomShippingContact", contact);
	}
	public void updateCustomShippingContact(Contact contact) {
		getSqlMapClientTemplate().update("updateCustomShippingContact", contact, 1);
	}
	public void deleteCustomShippingContact( Integer contactId ) throws DataAccessException {
		getSqlMapClientTemplate( ).delete( "deleteCustomShippingContact", contactId, 1 );
	}
	public void insertCustomShipping(ShippingRate shippingRate) {
		getSqlMapClientTemplate().insert("insertCustomShipping", shippingRate);
	    if (shippingRate.getGroupIdSet().size() != 0) {
			insertConditionShipping(shippingRate.getId(), setConditionToMap(shippingRate)); 
		}
	}
	public void updateCustomShipping(ShippingRate shippingRate) {
		getSqlMapClientTemplate().update("updateCustomShipping", shippingRate, 1);
		insertConditionShipping(shippingRate.getId(), setConditionToMap(shippingRate)); 
	}
	public ShippingRate getCustomShippingById(Integer customShippingId) {
		ShippingRate shippingRate = (ShippingRate) getSqlMapClientTemplate().queryForObject("getCustomShippingById", customShippingId);
		if (shippingRate!= null && getConditionShipping(customShippingId).getCondition()!=null) {
			
			if(getConditionShipping(customShippingId).getCondition().get("Group")!=null){
				Map<Object,String> innerMap = getConditionShipping(customShippingId).getCondition().get("Group");
				Set<Object> set = innerMap.keySet() ;
				String operator = innerMap.get(set.toArray()[0]);
				
				shippingRate.setGroupIdSet( set );
				shippingRate.setGroupType( operator );
			}
			if(getConditionShipping(shippingRate.getId()).getCondition().get("States")!=null){
				Map<Object,String> innerMap = getConditionShipping(shippingRate.getId()).getCondition().get("States");
				Set<Object> set = innerMap.keySet() ;
				String operator = innerMap.get(set.toArray()[0]);
				
				shippingRate.setStates( set.toArray()[0].toString() );
				shippingRate.setStateType( operator );
			}
		}
		return shippingRate;
	}
	
	private ConditionShipping setConditionToMap(ShippingRate shippingRate){
		ConditionShipping conditionShipping = new ConditionShipping();
		Map<String, Map<Object,String>> map = new HashMap<String, Map<Object,String>>();
		Map<Object, String> innerMap;
		if(shippingRate.getGroupIdSet()!=null && shippingRate.getGroupIdSet().size()!=0){
			innerMap = new HashMap< Object, String>();
			Iterator<Object> iter = shippingRate.getGroupIdSet().iterator();
			while(iter.hasNext()){
				innerMap.put(iter.next(), shippingRate.getGroupType());
			}
			map.put("Group", innerMap);
		}
		if(shippingRate.getStates()!=null && !shippingRate.getStates().equals("")){
			innerMap = new HashMap< Object, String>();
			innerMap.put(shippingRate.getStates(), shippingRate.getStateType());
			map.put("States", innerMap);
		}
		conditionShipping.setCondition(map);
		return conditionShipping;
	}

	private void insertConditionShipping(Integer shippingId, ConditionShipping conditionShipping)
	{
		getSqlMapClientTemplate().delete("deleteShippingCondition", shippingId);
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Iterator iter = conditionShipping.getCondition().entrySet().iterator();
		String query = "INSERT into condition_shipping ( shipping_id, type, value, operator ) VALUES (?, ?, ?, ? )";
		while ( iter.hasNext() ) {
			Map.Entry<String, Map<Object,String>> entry = (Map.Entry)iter.next();
			Iterator innerIter = entry.getValue().entrySet().iterator();
			while(innerIter.hasNext()){
				Map.Entry<Object,String> innerEntry = (Map.Entry)innerIter.next();
				Object[] args = { shippingId, entry.getKey(), innerEntry.getKey(), innerEntry.getValue()  };
				try {
					jdbcTemplate.update( query, args );
				}
				catch ( DataIntegrityViolationException e ) {
					// do nothing
				}
			}
		}
	}
	
	private ConditionShipping getConditionShipping(Integer shippingId)
	{
	 List<String> typeList = getSqlMapClientTemplate( ).queryForList( "getConditionTypeListByShippingId", shippingId );
	
	 JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
	 Map<Object,String> innerMap; 
	 Map<String, Map<Object,String>> map = new HashMap<String, Map<Object,String>>();
	 ConditionShipping condition = new ConditionShipping();	
	 
	 for(int i=0;i<typeList.size();i++){
		 innerMap = new HashMap<Object, String>();
		 String query = "SELECT value,operator FROM condition_shipping WHERE shipping_id = ? AND type= ? ORDER BY type";
		 Object[] args = { shippingId, typeList.get(i) };
		 SqlRowSet results = jdbcTemplate.queryForRowSet( query, args );
		 while(results.next()){
			 innerMap.put(results.getObject("value"), results.getString("operator"));
		 }
		 map.put(typeList.get(i), innerMap);
	     condition.setCondition(map);
	 }
	 return condition;
	}
	public Double getHandlingByZipcode(String country, String zipcode) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("country", country);		
		paramMap.put("zipcode", zipcode);		
		return (Double) getSqlMapClientTemplate().queryForObject( "getHandlingByZipcode", paramMap );
	}
	
	// Payment Method
	public List<PaymentMethod> getCustomPaymentMethodList(Integer custId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("custId", custId);		
		return getSqlMapClientTemplate().queryForList("getCustomPaymentMethodList", paramMap);
	}
	
	public void updateCustomPaymentMethods(List<PaymentMethod> customPayments) {
		for ( PaymentMethod customPayment : customPayments )
		{
			getSqlMapClientTemplate().update( "updateCustomPaymentMethod", customPayment );
		}		
	}
	
	// counties
	public List<County> getCounties(String country, String state) {
		Map<String, String> param = new HashMap<String, String>();
		param.put( "country", country );
		param.put( "state", state );
		List<County> counties = getSqlMapClientTemplate().queryForList("getCounties", param);
		if (counties.size() == 0) {
			// retrieve counties from Global
			counties = this.globalDao.getCounties(new County(country, state, null));
		}
		return counties;
	}
	
	public void updateCountyList(List<County> counties) {
		for (County county:counties) {
			getSqlMapClientTemplate().update("updateCounty", county);			
		}
	}
	
	public County getTaxRateByCounty(County county) {
		return (County) getSqlMapClientTemplate().queryForObject("getTaxRateByCounty", county);
	}
	
	// cities
	public List<City> getCities(City city ) {
		List<City> cities = getSqlMapClientTemplate().queryForList("getCities", city);
		if (cities.size() == 0) {
			// retrieve cities from Global
			cities = this.globalDao.getCities(city);
		}
		return cities;
	}
	
	public void updateCityList(List<City> cities) {
		for (City city:cities) {
			getSqlMapClientTemplate().update("updateCity", city);			
		}
	}
	
	public City getTaxRateByCity(City city) {
		return (City) getSqlMapClientTemplate().queryForObject("getTaxRateByCity", city);
	}
	
	// csv
	public Map<String, CsvFeed> getCsvFeedMapping(String feed) {
		return getSqlMapClientTemplate().queryForMap("getCsvFeed", feed, "headerName");
	}
	public List<CsvFeed> getCsvFeed(String feed) {
		return getSqlMapClientTemplate().queryForList("getCsvFeed", feed);
	}
	
	public Map<String, CsvData> getCsvFeedMap(String feed) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("feed", feed);
		return getSqlMapClientTemplate().queryForMap("getCsvFeedMap", paramMap, "headerName");
	}
	// Labels
	public Map<String, String> getLabels(String ... prefix) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("prefix", prefix);
		return getSqlMapClientTemplate().queryForMap("getLabels", paramMap, "label_type", "label_title");
	}
	
	public Map<Integer, Integer> getSiteMessageListByGroupId(Integer groupId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("groupId", groupId);
		return getSqlMapClientTemplate().queryForMap("getSiteMessageListByGroupId", paramMap, "message_id", "message_id");
	}
	
	public void updateLabels(List<Map<String, String>> labels) {
		for (Map<String, String> label: labels) {
			getSqlMapClientTemplate().update("updateLabel", label);
		}
	}
	
	public Contact getCompanyContactId(String company) {
		return (Contact) getSqlMapClientTemplate().queryForObject("getCompanyContactId", company);
	}
}
