/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.sql.SQLException;

import com.ibatis.sqlmap.client.extensions.ParameterSetter;
import com.ibatis.sqlmap.client.extensions.ResultGetter;
import com.ibatis.sqlmap.client.extensions.TypeHandlerCallback;

public abstract class EnumTypeHandler<E extends Enum> implements TypeHandlerCallback
{
	private Class<E> enumClass_;
	
	public EnumTypeHandler(Class<E> enumClass)
	{
		enumClass_ = enumClass;
	}

	@SuppressWarnings("unchecked")
	public void setParameter(ParameterSetter setter, Object parameter)
			throws SQLException
	{
		setter.setString(((E) parameter).name().toLowerCase());
	}

	public Object getResult(ResultGetter getter) throws SQLException
	{
		return valueOf(getter.getString().toUpperCase());
	}

	@SuppressWarnings("unchecked")
	public Object valueOf(String s)
	{
		return Enum.valueOf(enumClass_, s);
	}
}
