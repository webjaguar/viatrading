/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.webjaguar.dao.AccessPrivilegeDao;
import com.webjaguar.model.AccessGroup;
import com.webjaguar.model.AccessGroupPrivilege;
import com.webjaguar.model.AccessPrivilege;
import com.webjaguar.model.AccessPrivilegeSearch;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.AccessUserAudit;
import com.webjaguar.model.Configuration;
import com.webjaguar.web.form.AccessPrivilegeForm;
import com.webjaguar.web.form.GroupPrivilegeForm;

public class IbatisAccessPrivilegeDao extends SqlMapClientDaoSupport implements AccessPrivilegeDao {

	public List getUserPrivilegeList(AccessPrivilegeSearch accessPrivilegeSearch) {
		List<AccessUser> accessUserList = (List<AccessUser>) getSqlMapClientTemplate().queryForList("getUserPrivilegeList", accessPrivilegeSearch);

		// set group ids for each user
		for (AccessUser accessUser : accessUserList) {
			accessUser.setGroupSetNames(getGroupIds(accessUser.getId(), true));
		}
		return accessUserList;
	}

	public void updateAccessPrivilegeUser(AccessUser user, boolean changeUsername, boolean changePassword) {
		if (changeUsername)
			getSqlMapClientTemplate().update("updateUserUsername", user, 1);
		if (changePassword)
			getSqlMapClientTemplate().update("updateUserPassword", user, 1);
		getSqlMapClientTemplate().delete("deleteGoupUser", user.getId());
		if (user.getGroupSetIds() != null) {
			insertGroupIds(user.getId(), user.getGroupSetIds());
		}
		getSqlMapClientTemplate().update("updateAccessPrivilegeUser", user, 1);
	}

	public AccessPrivilege getPrivilegeById(Integer accessUserId) {
		AccessPrivilege accessPrivilege = new AccessPrivilege();
		accessPrivilege.setUser((AccessUser) getSqlMapClientTemplate().queryForObject("getAccessUserById", accessUserId));
		accessPrivilege.setRole(getSqlMapClientTemplate().queryForList("getPrivilegeById", accessUserId));
		if (accessPrivilege.getUser() != null) {
			accessPrivilege.getUser().setGroupSetIds(getGroupIds(accessUserId, false));
		}
		return accessPrivilege;
	}

	private Set<Object> getGroupIds(Integer accessUserId, boolean returnNameSet) throws DataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { accessUserId };

		Set<Object> groupSet = new TreeSet<Object>();
		String query = "SELECT group_id FROM access_user_access_group where user_id = ?";
		if (returnNameSet) {
			query = "SELECT access_group.group_name FROM access_group LEFT JOIN access_user_access_group ON access_user_access_group.group_id = access_group.id WHERE user_id = ?";
		}

		SqlRowSet results = jdbcTemplate.queryForRowSet(query, args);
		if (returnNameSet) {
			while (results.next()) {
				groupSet.add(results.getString("group_name"));
			}
		} else {
			while (results.next()) {
				groupSet.add(new Integer(results.getInt("group_id")));
			}
		}
		return groupSet;
	}

	private Set<Integer> getAccessUserIds(Integer groupId) throws DataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { groupId };
		Set<Integer> userIds = new TreeSet<Integer>();
		String query = "SELECT user_id FROM access_user_access_group where group_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(query, args);
		while (results.next()) {
			userIds.add(new Integer(results.getInt("user_id")));
		}
		return userIds;
	}

	public AccessUser getUserByUserName(String username) {
		AccessUser user = (AccessUser) getSqlMapClientTemplate().queryForObject("getUserByUserName", username);
		if (user != null)
			user.setRoles(getSqlMapClientTemplate().queryForList("getPrivilegeById", user.getId()));
		return user;
	}

	public AccessUser getAccessUserById(Integer accessUserId) {
		AccessUser user = (AccessUser) getSqlMapClientTemplate().queryForObject("getAccessUserById", accessUserId);
		if (user != null)
			user.setRoles(getSqlMapClientTemplate().queryForList("getPrivilegeById", user.getId()));
		return user;
	}

	public void disableAccessPrivilege() {
		getSqlMapClientTemplate().update("disableAccessPrivilege");
	}

	public void deleteAccessPrivilege(String username) {
		getSqlMapClientTemplate().delete("deleteAccessPrivilege", username);
	}

	public void deleteAccessPrivilegeUser(String username) {
		getSqlMapClientTemplate().delete("deleteAccessPrivilegeUser", username);
	}

	public void insertAccessPrivilegeUser(AccessPrivilegeForm privilegeForm) {
		getSqlMapClientTemplate().insert("insertAccessPrivilegeUser", privilegeForm.getPrivilege().getUser());
		if (privilegeForm.getPrivilege().getUser().getGroupSetIds() != null) {
			insertGroupIds(privilegeForm.getPrivilege().getUser().getId(), privilegeForm.getPrivilege().getUser().getGroupSetIds());
		}
	}

	private void insertGroupIds(Integer userId, Set<Object> groupIdSet) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Iterator<Object> iter = groupIdSet.iterator();
		String query = "INSERT into access_user_access_group (user_id, group_id) VALUES (?, ?)";
		while (iter.hasNext()) {
			Object[] args = { userId, (Integer) iter.next() };
			try {
				jdbcTemplate.update(query, args);
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}

	public void updateAccessPrivilege(final List<Configuration> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String sql = "INSERT INTO access_privilege (username, authority) VALUES (? , ?) ON DUPLICATE KEY UPDATE username = ?";
		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Configuration config = data.get(i);
				ps.setString(1, config.getKey());
				ps.setString(2, config.getValue());
				ps.setString(3, config.getKey());
			}

			public int getBatchSize() {
				return data.size();
			}
		});
	}

	public boolean isUserExist(String username) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { (String) username };
		if (jdbcTemplate.queryForInt("SELECT count(1)  FROM access_user where username = ? ", args) >= 1) {
			return true;
		} else {
			return false;
		}
	}

	public AccessUser getAccessUserBySalesRepId(Integer salesRepId) {
		return (AccessUser) getSqlMapClientTemplate().queryForObject("getAccessUserBySalesRepId", salesRepId);
	}

	// user Audit
	public List getUserAuditList(String username) {
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("username", username);
		return getSqlMapClientTemplate().queryForList("getUserAuditList", paramMap);
	}

	public void insertUserAudit(AccessUserAudit accessUserAudit) {
		getSqlMapClientTemplate().insert("insertUserAudit", accessUserAudit);
	}

	public void deleteAccessUserAudit(Integer id) {
		getSqlMapClientTemplate().delete("deleteAccessUserAudit", id);
	}

	// group
	public List<AccessGroup> getGroupPrivilegeList(AccessPrivilegeSearch accessPrivilegeSearch) {
		return getSqlMapClientTemplate().queryForList("getGroupPrivilegeList", accessPrivilegeSearch);
	}

	public AccessGroupPrivilege getPrivilegeByGroupId(Integer groupId) {
		AccessGroupPrivilege accessPrivilege = new AccessGroupPrivilege();
		accessPrivilege.setGroup((AccessGroup) getSqlMapClientTemplate().queryForObject("getGroupById", groupId));
		accessPrivilege.setRole(getSqlMapClientTemplate().queryForList("getPrivilegeByGroupId", groupId));
		return accessPrivilege;
	}

	public void updateGroupAccessPrivilege(final List<Configuration> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String sql = "INSERT INTO access_group_privilege (group_id, authority) VALUES (?, ?)";
		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Configuration config = data.get(i);
				ps.setString(1, config.getKey());
				ps.setString(2, config.getValue());
			}

			public int getBatchSize() {
				return data.size();
			}
		});

		// update users
		Integer groupId = null;
		Set<Integer> accessUserIds = null;
		Set<Object> groupIdSetByUserId = null;
		if (!data.isEmpty()) {
			groupId = Integer.parseInt(data.get(0).getKey());
		}
		if (groupId != null) {
			accessUserIds = getAccessUserIds(groupId);
		}
		if (accessUserIds != null) {
			for (Integer accessUserId : accessUserIds) {
				AccessUser user = getAccessUserById(accessUserId);
				jdbcTemplate.execute("DELETE FROM access_privilege WHERE username = '" + user.getUsername() + "'");
				groupIdSetByUserId = getGroupIds(accessUserId, false);
				if (groupIdSetByUserId != null) {
					for (Object gId : groupIdSetByUserId) {
						List<Configuration> data2 = getGroupPrivileges(Integer.parseInt(gId.toString()), accessUserId);
						if (data2 != null) {
							updateAccessPrivilege(data2);
						}
					}
				}
			}
		}
	}

	public boolean isGroupUserExist(Integer groupId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { (Integer) groupId };
		if (jdbcTemplate.queryForInt("SELECT count(user_id) FROM access_user_access_group WHERE access_user_access_group.group_id = ?", args) >= 1) {
			return true;
		} else {
			return false;
		}
	}

	public void insertAccessGroup(GroupPrivilegeForm privilegeForm) throws DataAccessException {
		getSqlMapClientTemplate().insert("insertAccessGroup", privilegeForm.getPrivilege().getGroup());
	}

	public void updateAccessGroup(AccessGroup group) {
		getSqlMapClientTemplate().update("updateAccessGroup", group, 1);
	}

	public void deleteAccessPrivilegeGroup(Integer groupId) {
		getSqlMapClientTemplate().delete("deleteAccessPrivilegeGroup", groupId);
	}

	public void deleteAccessGroup(Integer groupId) {
		getSqlMapClientTemplate().delete("deleteAccessGroup", groupId);
	}

	public List<Configuration> getGroupPrivileges(Integer groupId, Integer userId) {
		Map<String, Integer> paramMap = new HashMap<String, Integer>();
		paramMap.put("groupId", groupId);
		paramMap.put("userId", userId);
		return getSqlMapClientTemplate().queryForList("getGroupPrivileges", paramMap);
	}
}