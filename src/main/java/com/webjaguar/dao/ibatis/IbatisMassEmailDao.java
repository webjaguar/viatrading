/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.MassEmailDao;
import com.webjaguar.model.ClickDetail;
import com.webjaguar.model.Click;
import com.webjaguar.model.ClickDetailSearch;
import com.webjaguar.model.EmailCampaign;
import com.webjaguar.model.EmailCampaignBalance;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.Unsubscribe;

public class IbatisMassEmailDao extends SqlMapClientDaoSupport implements MassEmailDao {
	  
	  public void insertEmailCampaign(EmailCampaign emailCampaign) {
		  getSqlMapClientTemplate().insert("insertEmailCampaign", emailCampaign);
	  } 
	  public void updateEmailCampaign(EmailCampaign emailCampaign) {
		  getSqlMapClientTemplate().update("updateEmailCampaign", emailCampaign, 1);
	  }
	  public void finalizeEmailCampaign(EmailCampaign emailCampaign) {
		  getSqlMapClientTemplate().update("finalizeEmailCampaign", emailCampaign, 1);
	  }
	  public EmailCampaign getEmailCampaignById(Integer campaignId) {
		  return (EmailCampaign) getSqlMapClientTemplate().queryForObject ("getEmailCampaignById", campaignId);
	  }
	  public void addEmailCampaignBounce(Integer campaignId) {
		  getSqlMapClientTemplate().update("addEmailCampaignBounce", campaignId, 1);
	  }
	  public void deductEmailCampaignPoint(Integer point) {
		  getSqlMapClientTemplate().update("deductEmailCampaignPoint", point, 1);
	  }
	  public void addEmailCampaignPoint(Integer point) {
		  getSqlMapClientTemplate().update("addEmailCampaignPoint", point, 1);
	  }
	  public int emailInProcessCount() {
		return (Integer) getSqlMapClientTemplate().queryForObject("emailInProcessCount", null);
	  }
	  public void insertEmailCampaignBalance(EmailCampaignBalance balance) {
			getSqlMapClientTemplate().insert( "insertEmailCampaignBalance", balance);
	  }
	  public void updateEmailCampaignBalance(EmailCampaignBalance balance) {
			getSqlMapClientTemplate().update( "updateEmailCampaignBalance", balance, 1);
	  }
	  public void deleteEmailCampaignById(Integer id) {
			getSqlMapClientTemplate().delete("deleteEmailCampaignById", id);	
		}
	  public int getEmailBalance() {
		return (Integer) getSqlMapClientTemplate().queryForObject("getEmailBalance", null);
	  }
	  public List<EmailCampaignBalance> getBuyCreditList(ReportFilter buyCreditFilter) {
		  return getSqlMapClientTemplate().queryForList("getBuyCreditList", buyCreditFilter);
	  }
	  public Integer insertOrGetClick(Click click) 
	  {	
		  Integer clickId = null;
		  try {
			  clickId = (Integer) getSqlMapClientTemplate().insert( "insertClick", click);
		  } catch (DuplicateKeyException e) {
			  Click c = (Click) getSqlMapClientTemplate().queryForObject("getClickByCampIdByURL", click);
			  if(c!=null){
				  clickId = c.getId();
			  }else{
				  if(click!=null){
					  System.out.print("insertOrGetClick-----------------------click.getUrl()---->" + click.getUrl() + "---click.getCampaignId()---" + click.getCampaignId() +  "-----click.getId()------" + click.getId() + "------click.getUrlClickCounter()-------" + click.getUrlClickCounter());
				  }
			  }
		  }
		  return clickId;
	  }
	  public void updateClickCount(Click click) {
		  getSqlMapClientTemplate().update( "updateClickCount", click);
		  Map<String,Object> paramMap = new HashMap<String, Object>();
		  paramMap.put("clickDetail", click.getClickDetail());
		  
		  getSqlMapClientTemplate().insert("insertClickCountDetails", paramMap);
	  }
	  public void updateOpenCount(Click click) {
		  getSqlMapClientTemplate().update( "updateOpenCount", click.getCampaignId());
		  
		  Map<String,Object> paramMap = new HashMap<String, Object>();
		  paramMap.put("campaignId", click.getCampaignId());
		  paramMap.put("clickDetail", click.getClickDetail());
		  getSqlMapClientTemplate().insert("insertEmailOpenDetails", paramMap); 
	  }
	  public void insertUnsubscribeDetails(String campaignId, Integer userId, String email) {
		  Map<String,Object> paramMap = new HashMap<String, Object>();
		  paramMap.put("campaignId", campaignId);
		  paramMap.put("userId", userId);
		  paramMap.put("email", email);
		  getSqlMapClientTemplate().insert("insertUnsubscribeDetails", paramMap); 
	  }
	  public List<Click> getClick(Click click) {
		  return getSqlMapClientTemplate().queryForList("getClick", click);
	  }
	  public List<Unsubscribe> getUnsubscribe(Unsubscribe unsubscribe) {
		  return getSqlMapClientTemplate().queryForList("getUnsubscribe", unsubscribe);
	  }
	  public List<ClickDetail> getCountDetail(ClickDetail clickDetail) {
		  return getSqlMapClientTemplate().queryForList("getCountDetail", clickDetail);
	  }
	  public List<ClickDetail> getOpenDetail(ClickDetailSearch clickDetail) {
		  return getSqlMapClientTemplate().queryForList("getOpenDetail", clickDetail);
	  }
}
