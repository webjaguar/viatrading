/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.FaqDao;
import com.webjaguar.model.Faq;
import com.webjaguar.model.FaqGroup;
import com.webjaguar.model.FaqSearch;

public class IbatisFaqDao extends SqlMapClientDaoSupport implements FaqDao {
	
	// FAQ
	public List<Faq> getFaqList(FaqSearch search) {
		if(search.getKeyword() != null){
			StringTokenizer splitter = null;
			if(!search.getConjunction().equalsIgnoreCase("EXACT")){
				splitter = new StringTokenizer( search.getKeyword(), " ", false );
			} 
			while ( splitter != null && splitter.hasMoreTokens() )
			{
				String token = splitter.nextToken();
					// replace special characters with special meaning
					token = token.replace( "%", "\\%" );
					token = token.replace( "_", "\\_" );
					search.addKeyword( "%"+token.toLowerCase()+"%" );
			}
		}
		if (search.getGroupId() != null) {
			return getSqlMapClientTemplate().queryForList("getFaqListByGroupId", search);			
		} else {
			return getSqlMapClientTemplate().queryForList("getFaqList", search);								
		}
	}
	
	public Faq getFaqByQuestion(FaqSearch search) {
		Faq faq= (Faq) getSqlMapClientTemplate().queryForObject("getFaqByQuestion", search);
		return faq;
	}
	
	public Faq getFaqById(Integer faqId) {
		Faq faq= (Faq) getSqlMapClientTemplate().queryForObject("getFaqById", faqId);
		faq.setGroupIdList(getSqlMapClientTemplate().queryForList("getFaqMappingById", faqId));
		return faq;
	}
	
	public void deleteFaq(Integer faqId) {
		getSqlMapClientTemplate().delete("deleteFaq", faqId);	
	}
	
	public void insertFaq(Faq faq) {
		getSqlMapClientTemplate().insert("insertFaq", faq);
		this.inserFaqMapping(faq);
	}
	
	public void updateFaq(Faq faq) {
		getSqlMapClientTemplate().update("updateFaq", faq, 1);
		getSqlMapClientTemplate().delete("deleteFaqMapping", faq.getId());
		this.inserFaqMapping(faq);
	}
	
	public void inserFaqMapping(Faq faq) {
		if (faq.getGroupIdList() != null){
			for (Integer groupId: faq.getGroupIdList()){
				if(groupId == null) {
					continue;
				}
				faq.setGroupId(groupId);
				getSqlMapClientTemplate().insert("insertFaqMapping", faq);
			}
		}
	}
	
	public void updateRanking(final List<Map<String, Integer>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
        String sql = "UPDATE faq SET rank = ? WHERE id = ?";		
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map<String, Integer> entry = data.get(i);
				if (entry.get("rank") == null) {
					ps.setNull(1, Types.NULL);			
				} else {
					ps.setInt(1, ((Integer) entry.get("rank")).intValue());
				}
				ps.setInt(2, ((Integer) entry.get("id")).intValue());
			}
			public int getBatchSize() {
				return data.size();
			}
		});
	}

	// FaqGroup
	
    public void insertFaqGroup(FaqGroup faqGroup){
    	getSqlMapClientTemplate().insert("insertFaqGroup", faqGroup);
    }
	
	public void updateFaqGroup (FaqGroup faqGroup){
		getSqlMapClientTemplate().update("updateFaqGroup", faqGroup, 1);;
	}
	
	public void deleteFaqGroup (Integer groupId){
		getSqlMapClientTemplate().delete("deleteFaqGroup",groupId);
	}
	
	public FaqGroup getFaqGroupById (Integer groupId){
		return (FaqGroup)getSqlMapClientTemplate().queryForObject("getFaqGroupById",groupId);
	}
	
	public void updateFaqGroupRanking(final List<Map<String, Integer>> data){
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
        String sql = "UPDATE faq_group SET rank = ? WHERE id = ?";		
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map<String, Integer> entry = data.get(i);
				if (entry.get("rank") == null) {
					ps.setNull(1, Types.NULL);			
				} else {
					ps.setInt(1, ((Integer) entry.get("rank")).intValue());
				}
				ps.setInt(2, ((Integer) entry.get("id")).intValue());
			}
			public int getBatchSize() {
				return data.size();
			}
		});
	}
	
	public List getFaqGroupList(String protectedAccess){
		return getSqlMapClientTemplate().queryForList("getFaqGroupList",protectedAccess);
	}
}
