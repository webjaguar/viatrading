/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.05.2006
 */

package com.webjaguar.dao.ibatis;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.LayoutDao;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Language;

public class IbatisLayoutDao extends SqlMapClientDaoSupport implements LayoutDao {
	
	public Layout getLayout(Layout layout) {
		return (Layout) getSqlMapClientTemplate().queryForObject("getLayout", layout);
	}

	public void updateHeadTag(Layout layout) {
		getSqlMapClientTemplate().update("updateHeadTag", layout);
	}

	public void updateHeaderHtml(Layout layout) {
		getSqlMapClientTemplate().update("updateHeaderHtml", layout);
	}	
	
	public void updateTopBarHtml(Layout layout) {
		getSqlMapClientTemplate().update("updateTopBarHtml", layout);
	}
	
	public void updateLeftBarTopHtml(Layout layout) {
		getSqlMapClientTemplate().update("updateLeftBarTopHtml", layout);
	}
	
	public void updateLeftBarBottomHtml(Layout layout) {
		getSqlMapClientTemplate().update("updateLeftBarBottomHtml", layout);
	}
	
	public void updateRightBarTopHtml(Layout layout) {
		getSqlMapClientTemplate().update("updateRightBarTopHtml", layout);
	}
	
	public void updateRightBarBottomHtml(Layout layout) {
		getSqlMapClientTemplate().update("updateRightBarBottomHtml", layout);
	}
	
	public void updateFooterHtml(Layout layout) {
		getSqlMapClientTemplate().update("updateFooterHtml", layout);
	}
	
	public void updateAemFooterHtml(Layout layout) {
		getSqlMapClientTemplate().update("updateAemFooterHtml", layout);
	}
	
	public void updateLayout(Layout layout) {
		getSqlMapClientTemplate().update("updateLayout", layout, 1);		
	}
	
	public List<Layout> getLayoutList() {
		return getSqlMapClientTemplate().queryForList("getLayoutList", null);
	}
	
	public List<Layout> getSystemLayoutList(String type) {
		return getSqlMapClientTemplate().queryForList("getSystemLayoutList", type);
	}
	
	public Layout getLayoutByCategoryId(int cid) {
		int layoutId = 0;
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );

		Set<Integer> categoryIds = new HashSet<Integer>();
		Object[] args = { cid };
		String query = "SELECT parent, id, layout_id FROM category WHERE id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet( query, args );
		while ( results.next() )
		{
			layoutId = results.getInt( "layout_id" );
			if (layoutId > 0)
			{
				break;				
			}
			if (!categoryIds.add( results.getInt( "id" ) )) 
			{
				// prevent endless loop
				break;
			}
			args[0] = new Integer( results.getInt( "parent" ) );
			if (results.wasNull()) {
				// results.getInt() returns 0 if null
				break;				
			}
			results = jdbcTemplate.queryForRowSet( query, args );
		}
		if (layoutId == 0)  {
			layoutId = 1;	
		}
		
		return getLayout(new Layout(layoutId));		
	}
	
	public Layout getSystemLayout(Layout layout) {
		return (Layout) getSqlMapClientTemplate().queryForObject("getSystemLayout", layout);		
	}
	
	public void updateSystemLayout(Layout layout) {
		getSqlMapClientTemplate().update("updateSystemLayout", layout);			
	}
	
	public Map<String, Layout> getI18nSystemLayout(Layout layout) {
		return getSqlMapClientTemplate().queryForMap("getSystemLayout", layout, "lang");
	}
	
	public Map<String, Layout> getI18nLayout(Layout layout) {
		return getSqlMapClientTemplate().queryForMap("getLayout", layout, "lang");		
	}
	public List<Language> getLanguageSettings() {		
		return getSqlMapClientTemplate().queryForList("getLanguageSettings");
	}
	
}
