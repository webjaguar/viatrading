package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.CrmDao;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Search;
import com.webjaguar.model.crm.CrmAccountSearch;
import com.webjaguar.model.crm.CrmAccount;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactField;
import com.webjaguar.model.crm.CrmContactSearch;
import com.webjaguar.model.crm.CrmForm;
import com.webjaguar.model.crm.CrmFormField;
import com.webjaguar.model.crm.CrmQualifier;
import com.webjaguar.model.crm.CrmQualifierSearch;
import com.webjaguar.model.crm.CrmTask;
import com.webjaguar.model.crm.CrmTaskSearch;
import com.webjaguar.model.crm.CrmContactGroup;
import com.webjaguar.model.crm.CrmContactGroupSearch;


public class IbatisCrmDao extends SqlMapClientDaoSupport implements CrmDao {

	// Account
	public Integer getCrmAccountListCount(CrmAccountSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCrmAccountListCount", search);
	}
	public List<CrmAccount> getCrmAccountList(CrmAccountSearch search) {
		return getSqlMapClientTemplate().queryForList("getCrmAccountList", search);
	}
	public List<CrmAccount> getCrmAccountNameList() {
		return getSqlMapClientTemplate().queryForList("getCrmAccountNameList");
	}
	public CrmAccount getCrmAccountById(Integer accountId) {
		return (CrmAccount) getSqlMapClientTemplate().queryForObject( "getCrmAccountById", accountId );
	}
	public CrmAccount getCrmAccountByName(String accountName) {
		return (CrmAccount) getSqlMapClientTemplate().queryForObject( "getCrmAccountByName", accountName );
	}
	public String getCrmAccountNameById(Integer accountId) {
		return (String) getSqlMapClientTemplate().queryForObject( "getCrmAccountNameById", accountId );
	}
	public void deleteCrmAccount(Integer accountId) {
		getSqlMapClientTemplate().delete("deleteCrmAccount", accountId);	
	}
	public void insertCrmAccount(CrmAccount crmAccount) {
		getSqlMapClientTemplate().insert("insertCrmAccount", crmAccount);	
	}
	public void insertCrmQualifier(CrmQualifier crmQualifier) {
		getSqlMapClientTemplate().insert("insertCrmQualifier", crmQualifier);	
	}
	public void deleteCrmQualifier(Integer qualifierId) {
		getSqlMapClientTemplate().delete("deleteCrmQualifier", qualifierId);	
	}
	public void updateCrmQualifier(CrmQualifier crmQualifier) {
		getSqlMapClientTemplate().update("updateCrmQualifierObject", crmQualifier);	
	}
	public void updateCrmAccount(CrmAccount crmAccount) {
		getSqlMapClientTemplate().update("updateCrmAccount", crmAccount);
	}
	public boolean isCrmAccountExist(String accountName) {
		List<Integer> accounts = getSqlMapClientTemplate().queryForList( "isCrmAccountExistByAccountName",  accountName );
		if (accounts.isEmpty()) return false;
		else return true;
	}
	public Integer getCrmAccountId(String accountName, String accountNumber) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "accountName", accountName );
		paramMap.put( "accountNumber", accountNumber );
		return (Integer) getSqlMapClientTemplate().queryForObject( "getCrmAccountByAccountNameAccountNumber", paramMap );
	}
	public void importCrmAccounts(List<CrmAccount> crmAccounts) {
		for ( CrmAccount crmAccount : crmAccounts ) {
			getSqlMapClientTemplate().insert("importCrmAccount", crmAccount);
		}
	}
	public List<CrmAccount> getCrmAccountAjax(String accountName) {
		return (List<CrmAccount>) getSqlMapClientTemplate().queryForList( "getCrmAccountAjax", accountName );
	}
	
	// Qualifier
	public Integer getCrmQualifierListCount(CrmQualifierSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCrmQualifierListCount", search);
	}
	public List<CrmQualifier> getCrmQualifierList(CrmQualifierSearch search) {
		return getSqlMapClientTemplate().queryForList("getCrmQualifierList", search);
	}
	
	// Contact
	public Integer getCrmContactListCount(CrmContactSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCrmContactListCount", search);
	}
	public List<Integer> getCrmContactIdsList(CrmContactSearch search) {
		return getSqlMapClientTemplate().queryForList("getCrmContactIdsList", search);
	}
	
	public List<CrmContact> getCrmContactList(CrmContactSearch search) {
		List<CrmContact> contactList = getSqlMapClientTemplate().queryForList("getCrmContactList", search);
		return contactList;
	}
	
	public List<CrmContact> getCrmContactList2(CrmContactSearch search) {
		List<CrmContact> contactList = getSqlMapClientTemplate().queryForList("getCrmContactList2", search);
		return contactList;
	}
	
	public List<CrmContact> getCrmContactExport(CrmContactSearch search) {
		List<CrmContact> list = getSqlMapClientTemplate().queryForList("getCrmContactExport", search);
		for(CrmContact contact : list) {
			contact.setCrmContactFields(getCrmContactFieldsByContactId(contact.getId()));
		}
		return list;
	}
	public CrmContact getCrmContactById(Integer contactId) {
		CrmContact crmContact = (CrmContact) getSqlMapClientTemplate().queryForObject( "getCrmContactById", contactId );
		if( crmContact != null ) {
			crmContact.setCrmContactFields(getCrmContactFieldsByContactId(contactId));
		}
		return crmContact ;
	}
	public CrmQualifier getCrmQualifierById(Integer qualifierId) {
		CrmQualifier crmQualifier = (CrmQualifier) getSqlMapClientTemplate().queryForObject("getCrmQualifierById", qualifierId);
		return crmQualifier;
	}
	public void deleteCrmContact(Integer contactId) {
		getSqlMapClientTemplate().delete("deleteCrmContact", contactId);	
	}
	public void insertCrmContact(CrmContact crmContact) {
		if(crmContact.getLanguage()==null){
			crmContact.setLanguage("en");
		}
		getSqlMapClientTemplate().insert("insertCrmContact", crmContact);
		if(crmContact.getCrmContactFields() != null && crmContact.getCrmContactFields().size() > 0){
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put( "contactId", crmContact.getId() );
			for(CrmContactField crmContactField: crmContact.getCrmContactFields()){
				if(crmContactField.getId() != null && crmContactField.getFieldValue() != null && !crmContactField.getFieldValue().equalsIgnoreCase("")){
					paramMap.put( "fieldId", crmContactField.getId() );
					paramMap.put( "fieldValue", crmContactField.getFieldValue() );
					getSqlMapClientTemplate().insert("insertCrmContactFieldMapping", paramMap);
				}
			}
		}
	}
	public void updateCrmContact(CrmContact crmContact) {
		getSqlMapClientTemplate().update("updateCrmContact", crmContact);
		if(crmContact.getCrmContactFields() != null && crmContact.getCrmContactFields().size() > 0){
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put( "contactId", crmContact.getId() );
			for(CrmContactField crmContactField: crmContact.getCrmContactFields()){
				paramMap.put( "fieldId", crmContactField.getId() );
				getSqlMapClientTemplate().delete("deleteCrmContactFieldMapping" , paramMap);
				
				if(crmContactField.getFieldValue() != null && !crmContactField.getFieldValue().equalsIgnoreCase("") ){
					paramMap.put( "fieldValue", crmContactField.getFieldValue() );
					getSqlMapClientTemplate().insert("insertCrmContactFieldMapping", paramMap);
				}
			}
		}
	}
	public List<CrmContact> getCrmContactByEmail1(String email1) {
		List<CrmContact> queryForList = getSqlMapClientTemplate().queryForList( "getCrmContactByEmail1",  email1 );
		return queryForList;
		//no need for contactFields
	}
	public void insertCrmContactField(CrmContactField crmContactField) {
		getSqlMapClientTemplate().insert("insertCrmContactField", crmContactField);
	}
	public List<CrmContactField> getCrmContactFieldList(Search search) {
		return getSqlMapClientTemplate().queryForList("getCrmContactFieldList", search);
	}
	public CrmContactField getCrmContactFieldById(Integer fieldId) {
		return (CrmContactField) getSqlMapClientTemplate().queryForObject("getCrmContactFieldById", fieldId);
	}
	public CrmContactField getCrmContactFieldByGlobalName(String fieldName) {
		return (CrmContactField) getSqlMapClientTemplate().queryForObject("getCrmContactFieldByGlobalName", fieldName);
	}
	public void updateCrmContactField(CrmContactField crmContactField){
		getSqlMapClientTemplate().update("updateCrmContactField", crmContactField);
	}
	public void deleteCrmContactField(Integer fieldId){
		getSqlMapClientTemplate().delete("deleteCrmContactField", fieldId);
	}
	public List<CrmContactField> getCrmContactFieldsByContactId(Integer contactId){
		List<CrmContactField> crmContactFields = getSqlMapClientTemplate().queryForList("getCrmContactFieldList", contactId);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		Iterator<CrmContactField> iter = crmContactFields.iterator();
		   while ( iter.hasNext() ) {
			   CrmContactField crmContactField = iter.next();
			   if (!crmContactField.isEnabled()) {
				   iter.remove();
				   continue;
			   }
			   paramMap.put( "contactId", contactId );
			   paramMap.put( "fieldId", crmContactField.getId() );
			 
			   String fieldValue = (String) getSqlMapClientTemplate().queryForObject("getFieldValueByFieldIdContactId", paramMap);
			   crmContactField.setFieldValue(fieldValue);
			}
		   return crmContactFields;
	}
	public String createToken(Integer contactId) {
		  JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		  String token = (String) getSqlMapClientTemplate().queryForObject("getTokenByContactId", contactId);
		  if (token == null) {
			  token = UUID.randomUUID().toString();
		      String query = "UPDATE crm_contact set token = ? where id = ?";
		      Object[] args = { token, contactId };  
		      jdbcTemplate.update(query, args);
		  }
		  return token;
	}
	
	public boolean validCrmContactToken(String token) {
		  Integer contactId = (Integer) getSqlMapClientTemplate().queryForObject("getContactIdByToken", token);
		  if (contactId != null) {
			  return true;
		  }
		  return false;
	}
	
	public void updateCrmTrackCode(Integer customerId, String trackCode) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("trackCode", trackCode);
		getSqlMapClientTemplate().update("updateCrmTrackCode", paramMap);
	}
	
	public void updateCrmQualifier(Integer customerId, String qualifier) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("qualifier", qualifier);
		getSqlMapClientTemplate().update("updateCrmQualifierId", paramMap);
	}
	
	public void convertCrmContactToCustomer(Integer contactId, Integer userId, String leadSource) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("contactId", contactId);
		paramMap.put("userId", userId);
		paramMap.put("leadSource", leadSource);
		getSqlMapClientTemplate().update("convertCrmContactToCustomer", paramMap);
	}
	public Map<String, String> getContcatCustomerMap(String key) {
		if(key.equalsIgnoreCase("customer")){
			return getSqlMapClientTemplate().queryForMap("getContcatCustomerMap", null, "customer_property", "crm_contact_property" ) ;
		} else {
			return getSqlMapClientTemplate().queryForMap("getContcatCustomerMap", null, "crm_contact_property", "customer_property" ) ;
		}
	}
	public void updateCustomerMap(final List<Configuration> data) {		
    	JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
        String sql = "UPDATE crm_contact_customer_map SET crm_contact_property = ? WHERE customer_property = ?";		
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Configuration config = data.get(i);
				ps.setString(1, config.getValue());
				ps.setString(2, config.getKey());
			}
			public int getBatchSize() {
				return data.size();
			}
		});
	}
	public void unsubscribeByCrmContactId(Integer crmContactId) {
		getSqlMapClientTemplate().update("unsubscribeByCrmContactId", crmContactId, 1);	
	}
	public Integer getFieldValueCountByFieldIdAndValue(Integer fieldId, String fieldValue) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("fieldId", fieldId);
		paramMap.put("fieldValue", fieldValue);
		return (Integer) getSqlMapClientTemplate().queryForObject("getFieldValueCountByFieldIdAndValue", paramMap);	
	}
	public void batchAssignUserToContacts(List<Integer> contactIds, AccessUser accessUser) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("contactIds", contactIds);
		paramMap.put("salesRepId", (accessUser != null ? accessUser.getSalesRepId() : null));
		paramMap.put("accessUserId", (accessUser != null ? accessUser.getId() : null));
		getSqlMapClientTemplate().update("batchAssignUserToContacts", paramMap);	
		getSqlMapClientTemplate().update("batchAssignSalesRepToCrmCustomer", paramMap);	
	}
	
	public void removeUserFromContacts(List<Integer> contactIds){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("contactIds", contactIds);
		getSqlMapClientTemplate().update("removeUserFromContacts", paramMap);
		getSqlMapClientTemplate().update("removeSalesRepFromCustomer", paramMap);	
	}
	
	public void batchCrmContactDelete(Set<Integer> contactIds) { 
		Iterator<Integer> iter = contactIds.iterator();
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query = "DELETE FROM crm_contact WHERE id = ? AND user_id IS NULL";
		while ( iter.hasNext() ) {
			Object[] args = { (Integer) iter.next() };
			try {
				jdbcTemplate.update( query, args );
			}
			catch ( DataIntegrityViolationException e ) {
				// do nothing
			}
		}
	}
	public void batchCrmRating(List<Integer> contactIds, String rating) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("contactIds", contactIds);
		paramMap.put("rating", rating);
		getSqlMapClientTemplate().update("batchCrmRating", paramMap);
	}
	
	public void batchCrmRating1(List<Integer> contactIds, String rating) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("contactIds", contactIds);
		paramMap.put("rating", rating);
		getSqlMapClientTemplate().update("batchCrmRating1", paramMap);
	}
	
	public void batchCrmRating2(List<Integer> contactIds, String rating) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("contactIds", contactIds);
		paramMap.put("rating", rating);
		getSqlMapClientTemplate().update("batchCrmRating2", paramMap);
	}
	
	public void batchCrmQualifier(List<Integer> contactIds, String qualifier) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("contactIds", contactIds);
		paramMap.put("qualifier", qualifier);
		getSqlMapClientTemplate().update("batchCrmQualifier", paramMap);
	}
	
	public void batchCrmLanguage(List<Integer> contactIds, String language) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("contactIds", contactIds);
		paramMap.put("language", language);
		getSqlMapClientTemplate().update("batchCrmLanguage", paramMap);
	}
	
	// contact group
	public List<CrmContactGroup> getContactGroupList(CrmContactGroupSearch search) {
		return getSqlMapClientTemplate().queryForList("getContactGroupList", search);			
	}
	public List<CrmContactGroup> getContactGroupNameList() {
		return getSqlMapClientTemplate().queryForList("getContactGroupNameList", null);			
	}
	public CrmContactGroup getContactGroupById(Integer groupId) {
		return (CrmContactGroup) getSqlMapClientTemplate().queryForObject("getContactGroupById", groupId);
	}
	public void deleteCrmContactGroupById(Integer groupId) {	
		getSqlMapClientTemplate().delete("deleteContactGroupById", groupId);
		getSqlMapClientTemplate().delete("deleteGroupContactByGroupId", groupId);
	}
	public void insertCrmContactGroup(CrmContactGroup group) throws DataAccessException {
		getSqlMapClientTemplate().insert("insertContactGroup", group);
	}
	public void updateCrmContactGroup(CrmContactGroup group) {
		getSqlMapClientTemplate().update("updateContactGroup", group, 1);			
	}
	public List<Integer> getContactGroupIdList(Integer contactId) {
		return getSqlMapClientTemplate().queryForList("getContactGroupIdList", contactId);			
	}
	
	public void batchCrmContactGroupIds(Integer contactId, Set groupIds, String action) { 
		Iterator iter = groupIds.iterator();
		if ( action.equals( "add" ) ) {
			JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
			String query = "INSERT into crm_group_contact_join (group_id, contact_id, new_entry) VALUES (?, ?, false ) ON DUPLICATE KEY UPDATE new_entry = false ";
			while ( iter.hasNext() ) {
				Object[] args = { (Integer) iter.next(), contactId };
				try {
					jdbcTemplate.update( query, args );
				}
				catch ( DataIntegrityViolationException e ) {
					// do nothing
				}
			}
		} else if ( action.equals( "remove" )) {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			while ( iter.hasNext() ) {
				try {
					paramMap.put( "contactId", contactId );
					paramMap.put( "groupId", (Integer) iter.next() );
					getSqlMapClientTemplate().delete( "deleteContactIdGroupId", paramMap );
				}
				catch ( DataIntegrityViolationException e ) {
					// do nothing
				}
			}
		} else {
			insertGroupIds(contactId, groupIds);
		}
	}

	private void insertGroupIds(Integer contactId, Set groupIds)
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Iterator iter = groupIds.iterator();
		String query = "INSERT into crm_group_contact_join (group_id, contact_id, new_entry) VALUES (?, ?, true ) ON DUPLICATE KEY UPDATE new_entry = true ";
		while ( iter.hasNext() ) {
			Object[] args = { (Integer) iter.next(), contactId };
			try {
				jdbcTemplate.update( query, args );
			}
			catch ( DataIntegrityViolationException e ) {
				// do nothing
			}
		}
		getSqlMapClientTemplate().delete("deleteGroupContactId", contactId );
		getSqlMapClientTemplate().update("updateGroupContactId", contactId);
	}

	
	// Task
	public Integer getCrmTaskListCount(CrmTaskSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCrmTaskListCount", search);
	}
	public List<CrmTask> getCrmTaskList(CrmTaskSearch search) {
		return getSqlMapClientTemplate().queryForList("getCrmTaskList", search);
	}
	public CrmTask getCrmTaskById(Integer taskId) {
		return (CrmTask) getSqlMapClientTemplate().queryForObject( "getCrmTaskById", taskId );
	}
	public void insertCrmTask(CrmTask crmTask) {
		getSqlMapClientTemplate().insert("insertCrmTask", crmTask);	
	}
	public void updateCrmTask(CrmTask crmTask) {
		getSqlMapClientTemplate().update("updateCrmTask", crmTask);
	}
	public List<CrmTask> getCrmTaskExport(CrmTaskSearch search) {
		return getSqlMapClientTemplate().queryForList("getCrmTaskExport", search);
	}
	public void deleteCrmTask(Integer taskId) {
		getSqlMapClientTemplate().delete("deleteCrmTask", taskId);	
	}
	public void batchCrmTaskDelete(Set<Integer> taskIds) { 
		Iterator<Integer> iter = taskIds.iterator();
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query = "DELETE FROM crm_task WHERE id = ?";
		while ( iter.hasNext() ) {
			Object[] args = { (Integer) iter.next() };
			try {
				jdbcTemplate.update( query, args );
			}
			catch ( DataIntegrityViolationException e ) {
				// do nothing
			}
		}
	}
	
	// Form
	public List<CrmForm> getCrmFormList(Search search) {
		return getSqlMapClientTemplate().queryForList("getCrmFormList", search);
	}
	public CrmForm getCrmFormById(Integer formId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("formId", formId);
		CrmForm crmForm = (CrmForm) getSqlMapClientTemplate().queryForObject( "getCrmForm", paramMap );
		List<CrmFormField> formFields = getSqlMapClientTemplate().queryForList("getCrmFieldsByFormId", formId);
		crmForm.setCrmFields(formFields);
		return crmForm;
	}
	public void deleteCrmForm(Integer formId) {
		getSqlMapClientTemplate().delete("deleteCrmForm", formId);	
	}
	public void insertCrmForm(CrmForm crmForm) {
		getSqlMapClientTemplate().insert("insertCrmForm", crmForm);
		for(CrmFormField crmFormField: crmForm.getCrmFields()){
			crmFormField.setFormId(crmForm.getFormId());
			getSqlMapClientTemplate().insert("insertCrmFormFields", crmFormField);
		}
	}
	public void updateCrmForm(CrmForm crmForm) {
		getSqlMapClientTemplate().update("updateCrmForm", crmForm);
		getSqlMapClientTemplate().delete("deleteCrmFormFields", crmForm.getFormId());
		for(int i=0;i<crmForm.getNumberOfFields(); i++){
			crmForm.getCrmFields().get(i).setFormId(crmForm.getFormId());
			getSqlMapClientTemplate().insert("insertCrmFormFields", crmForm.getCrmFields().get(i));
		}
	}
	public CrmForm getCrmFormByFormNumber(String formNumber) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("formNumber", formNumber);
		CrmForm crmForm = (CrmForm) getSqlMapClientTemplate().queryForObject( "getCrmForm", paramMap );
		if(crmForm != null) {
			List<CrmFormField> formFields = getSqlMapClientTemplate().queryForList("getCrmFieldsByFormId",crmForm.getFormId());
			crmForm.setCrmFields(formFields);
		}
		return crmForm;
	}
	public int getCrmContactId(String email) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Object[] args = { email, email};
		try {
			return jdbcTemplate.queryForInt("SELECT id FROM crm_contact WHERE email_1=? OR email_2=? LIMIT 1", args);
		}
		catch ( DataIntegrityViolationException e ) {
			e.printStackTrace();
			return 0;
			// do nothing
		}
	}
	
	public void insertCrmTime(String emailAddress, Long crmTime){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("emailAddress", emailAddress);
		paramMap.put("crmTime", crmTime);
		getSqlMapClientTemplate().insert("insertCrmTime", paramMap);
	}
	
	public Long getCrmTime(String emailAddress){
		return (Long) getSqlMapClientTemplate().queryForObject("getCrmTime", emailAddress);
	}
	
	public Long getDuplicateEntry(String emailAddress){
		return (Long) getSqlMapClientTemplate().queryForObject("getDuplicateEntry", emailAddress);
	}
	
	public void updateCrmTime(String emailAddress, Long crmTime){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("emailAddress", emailAddress);
		paramMap.put("crmTime", crmTime);
		getSqlMapClientTemplate().update("updateCrmTime", paramMap);
	}
	
}