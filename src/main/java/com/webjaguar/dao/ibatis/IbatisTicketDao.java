/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.TicketDao;
import com.webjaguar.model.Ticket;
import com.webjaguar.model.TicketSearch;
import com.webjaguar.model.TicketVersion;
import com.webjaguar.web.form.TicketForm;

public class IbatisTicketDao extends SqlMapClientDaoSupport implements TicketDao {
		  
	  public List<Ticket> getTicketsListByUserid(Integer userId, TicketSearch ticketSearch) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "ticketSearch", ticketSearch );
		  paramMap.put( "userId", userId );
		  return getSqlMapClientTemplate().queryForList("getTicketsListByUserid", paramMap);
	  }
	  
	  public List getTicketsList(TicketSearch ticketSearch) {
		  return getSqlMapClientTemplate().queryForList("getTicketsList", ticketSearch);
	  }
	  
	  public Ticket getTicketById(Integer ticketId) {
		  return (Ticket) getSqlMapClientTemplate().queryForObject("getTicketById", ticketId);
	  }
	  
	  public List<TicketForm> getTicketVersionListById(Integer ticketId, Integer userId) {
		  Map<String, Integer> paramMap = new HashMap<String, Integer>();
		  paramMap.put( "ticketId", ticketId );
		  paramMap.put( "userId", userId );
		  return getSqlMapClientTemplate().queryForList("getTicketVersionListById", paramMap);
	  }
	  
	  public int insertTicket(Ticket ticket) {
			getSqlMapClientTemplate().insert( "insertTicket", ticket);
			return ticket.getTicketId();
	  }
	  
	  public void insertTicketVersion(TicketVersion ticketVersion) {
		  getSqlMapClientTemplate().insert("insertTicketVersion", ticketVersion);
	  } 
	  
	  public void updateTicketLastModified(Integer ticketId, String versionCreatedByType) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "ticketId", ticketId );
		  paramMap.put( "versionCreatedByType", versionCreatedByType );
		  getSqlMapClientTemplate().update("updateTicketLastModified", paramMap, 1);
	  }
	  
	  public void updateTicketStatus(Ticket ticket) {
		  getSqlMapClientTemplate().update("updateTicketStatus", ticket, 1);
	  }

}