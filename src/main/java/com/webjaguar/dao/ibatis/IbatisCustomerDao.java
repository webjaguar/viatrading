/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.Date;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.CustomerDao;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Address;
import com.webjaguar.model.Affiliate;
import com.webjaguar.model.BudgetPartner;
import com.webjaguar.model.CsvData;
import com.webjaguar.model.CsvIndex;
import com.webjaguar.model.CsvMap;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerBudgetPartner;
import com.webjaguar.model.CustomerCreditHistory;
import com.webjaguar.model.CustomerExtContact;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.CustomerGroup;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.CustomerSearch;

import com.webjaguar.model.DialingNote;
import com.webjaguar.model.LocationSearch;
import com.webjaguar.model.MobileCarrier;
import com.webjaguar.model.SpecialPricing;
import com.webjaguar.model.SpecialPricingSearch;
import com.webjaguar.model.StagingUser;
import com.webjaguar.model.VirtualBank;
import com.webjaguar.model.crm.CrmContact;

public class IbatisCustomerDao extends SqlMapClientDaoSupport implements CustomerDao {

	private GlobalDao globalDao;

	public void setGlobalDao(GlobalDao globalDao) {
		this.globalDao = globalDao;
	}

	public String createPasswordToken(Integer userId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String token = (String) getSqlMapClientTemplate().queryForObject("getTokenByUserId", userId);
		if (token == null) {
			token = UUID.randomUUID().toString();
			String query = "UPDATE users set token = ? where id = ?";
			Object[] args = { token, userId };
			jdbcTemplate.update(query, args);
		}
		return token;
	}

	public String changeCustomerToken(Integer userId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String token = UUID.randomUUID().toString();
		String query = "UPDATE users set token = ? where id = ?";
		Object[] args = { token, userId };
		jdbcTemplate.update(query, args);
		return token;
	}

	public String getTokenByUserId(Integer userId) {
		return (String) getSqlMapClientTemplate().queryForObject("getTokenByUserId", userId);
	}

	public boolean validToken(String token) {
		Integer customerId = (Integer) getSqlMapClientTemplate().queryForObject("getUserIdByToken", token);
		if (customerId != null) {
			return true;
		}
		return false;
	}

	public Integer getUserIdByToken(String token) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getUserIdByToken", token);
	}

	public void updatePassword(String newPassword, String token) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE users set last_modified = now(), last_password_reset = now() where token = ?";
		Object[] args1 = { token };
		jdbcTemplate.update(query, args1);
		query = "UPDATE users set password = md5(?), token = null where token = ?";
		Object[] args2 = { newPassword, token };
		jdbcTemplate.update(query, args2);
	}

	public void updateEdge(Integer customerId, Integer parentId) {
		parentId = (parentId == null) ? 0 : parentId;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("id", customerId);
		paramMap.put("affiliateParent", parentId);
		try {
			getSqlMapClientTemplate().update("updateAffiliateEdge", paramMap, 1);
			try {
				// remove all child
				if (parentId == -1) {
					getSqlMapClientTemplate().update("resetAllChidrenToAdmin", paramMap);
				}
			} catch (Exception e) {
			}
		} catch (Exception e) {
			getSqlMapClientTemplate().insert("insertAffiliateEdge", paramMap);
		}
	}

	public List getAffiliates(Integer root, Integer list_type) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("root", root);
		paramMap.put("list_type", list_type);
		return getSqlMapClientTemplate().queryForList("getAffiliates", paramMap);
	}

	public Affiliate getAffiliatesCommission(Integer nodeId) {
		return (Affiliate) getSqlMapClientTemplate().queryForObject("getAffiliatesCommission", nodeId);
	}

	public Customer getCustomerByToken(String token) {
		if (token == null)
			token = "";

		Customer customer = new Customer();
		customer.setToken(token);
		return (Customer) getSqlMapClientTemplate().queryForObject("getCustomer", customer);
	}

	public List<Customer> getCustomerListWithDate(CustomerSearch search) {
		List<Customer> customers = getSqlMapClientTemplate().queryForList("getCustomerListWithDate", search);
		Map<Integer, Customer> customerMap = getSqlMapClientTemplate().queryForMap("getCustomerOrderReport", null, "id");
		Map<Integer, Customer> shoppingCartMap = null;
		if (search.getCartNotEmpty() != null && search.getCartNotEmpty()) {
			shoppingCartMap = getSqlMapClientTemplate().queryForMap("getCustomerShoppingCartInfo", null, "id");
		}
		// get customer
		for (Customer customer : customers) {
			if (customerMap.containsKey(customer.getId())) {
				customer.setOrdersGrandTotal(customerMap.get(customer.getId()).getOrdersGrandTotal());
				customer.setOrderCount(customerMap.get(customer.getId()).getOrderCount());
			}
			if (shoppingCartMap != null && shoppingCartMap.containsKey(customer.getId())) {
				customer.setNumOfCartItems(shoppingCartMap.get(customer.getId()).getNumOfCartItems());
			}
		}
		return customers;
	}

	public List<Customer> getCustomers(CustomerSearch search) {
		return getSqlMapClientTemplate().queryForList("getCustomerList", search);
	}

	public int getCustomersCount() {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCustomersCount");
	}

	public int getCustomerListWithDateCount(CustomerSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCustomerListWithDateCount", search);
	}

	public List<Integer> getCustomerIdsList(CustomerSearch search) {
		return getSqlMapClientTemplate().queryForList("getCustomerIdsList", search);
	}

	public List<Customer> getCustomerListByZipCode(LocationSearch search) {
		return getSqlMapClientTemplate().queryForList("getCustomerListByZipCode", search);
	}

	public int getCustomerListByZipCodeCount(LocationSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCustomerListByZipCodeCount", search);
	}

	public Customer getCustomerById(Integer userId) {
		return (Customer) getSqlMapClientTemplate().queryForObject("getCustomerById", userId);
	}
	
	public List<Integer> getUserIdByCrmId(List<Integer> userIds) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userIds", userIds);
		return getSqlMapClientTemplate().queryForList("getUserIdByCrmId", paramMap);
	}
	
	public Integer getUserIdByCrmId(Integer userId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		return (Integer) getSqlMapClientTemplate().queryForObject("getUserIdByCrmId", paramMap);
	}

	public Integer getCrmIdByUsername(String username) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("username", username);
		return (Integer) getSqlMapClientTemplate().queryForObject("getCrmIdByUsername", paramMap);
	}
	
	public Map<String, CsvMap> getAvailableUserList(List<String> username) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("username", username);
		return getSqlMapClientTemplate().queryForMap("getAvailableUserList",paramMap,"name");
	}
	
	public Map<String, CsvMap> getAvailableCrmList(List<String> email) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("email", email);
		return getSqlMapClientTemplate().queryForMap("getAvailableCrmList",paramMap,"name");
	}
	
	public List<String> getUserIdListByUsername(List<String> username) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("username", username);
		return getSqlMapClientTemplate().queryForList("getUserIdListByUsername",paramMap);
	}
	
	public Map<String,Long> getUserIdsMapByUsername(List<String> username) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("username", username);
		return getSqlMapClientTemplate().queryForMap("getUserIdsMapByUsername",paramMap,"username","id");
	}
	
	public Map<Long, Long> getDefaultAddressByUsername(List<String> username) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("username", username);
		return getSqlMapClientTemplate().queryForMap("getDefaultAddressByUsername", paramMap,"userid", "id");
	}
	
	public Map<String,Long> getCrmIdsMapByUsername(List<String> username) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("username", username);
		return getSqlMapClientTemplate().queryForMap("getCrmIdsMapByUsername",paramMap,"email_1","id");
	}
	
	
    public void insertAndUpdateCustomerList(List<String> allEmailAddressList, Map<String, CsvMap> availableEmailMap, Map<String, List<CsvData>> customerCsvDataList,
		Map<String, List<CsvData>> addressCsvDataList, Map<String, List<CsvData>> crmCsvDataList, List<CsvIndex> customerCsvIndex, List<CsvIndex> addressCsvIndex, List<CsvIndex> crmCsvIndex, List<Map<String, Object>> addedItems, List<Map<String, Object>> updatedItems, List<Map<String, Object>> addedCrmItems, List<Map<String, Object>> updatedCrmItems, List<Map<String, Object>> notAddedCrmItems ,  List<Map<String, Object>> notAddedCustomerItems) {
		
    	Map<String,List<CsvData>> availableCustomerCsvDataList = new HashMap<String,List<CsvData>>();
		Map<String,List<CsvData>> availableAddressCsvDataList = new HashMap<String,List<CsvData>>();
		Map<String,List<CsvData>> availableCrmCsvDataList = new HashMap<String,List<CsvData>>();
		
		Map<String, CsvMap> availableCrmMap = getAvailableCrmList(allEmailAddressList);
		Map<String, Object> emailMap = verifyCustomerData(customerCsvDataList,addressCsvDataList,notAddedCustomerItems);
		Map<String, Object> crmMap = verifyCrmData(crmCsvDataList,notAddedCrmItems);
		Map<String,Long> crmIdListByUsername = null;
		
		Map<String, Object> thisMap = null;
		for (String user : allEmailAddressList){
			thisMap = new HashMap<String, Object>();
			thisMap.put("emailAddress", user);
			if(availableEmailMap.get(user)!=null){
				availableCustomerCsvDataList.put(user,customerCsvDataList.get(user));
				customerCsvDataList.remove(user);
				availableAddressCsvDataList.put(user,addressCsvDataList.get(user));
				addressCsvDataList.remove(user);
				updatedItems.add(thisMap);
			}else if(emailMap.get(user)==null){
				addedItems.add(thisMap);
			}
		}
				
		for (String user : allEmailAddressList){
			thisMap = new HashMap<String, Object>();
			thisMap.put("emailAddress", user);
			if(availableCrmMap.get(user)!=null){
				availableCrmCsvDataList.put(user,crmCsvDataList.get(user));
				crmCsvDataList.remove(user);
				updatedCrmItems.add(thisMap);
			}else if(crmMap.get(user)==null){
				addedCrmItems.add(thisMap);
			}
		}

		updateCustomerList(availableCustomerCsvDataList, customerCsvIndex);
		updateAddressList(availableAddressCsvDataList, addressCsvIndex);
		updateCrmList(availableCrmCsvDataList, crmCsvIndex);
		
		insertCustomerList(customerCsvDataList, customerCsvIndex, emailMap);
		insertAddressList(availableEmailMap, customerCsvDataList, addressCsvDataList,emailMap);

		insertCrmList(crmCsvDataList, notAddedCrmItems, crmMap);
		crmIdListByUsername = updateCustomerCrmContactList(crmCsvDataList, emailMap, crmIdListByUsername);
		if(crmIdListByUsername!=null)
		updateCrmUserList(crmCsvDataList, crmMap, crmIdListByUsername);
		updateCustomerDefaultAddress(customerCsvDataList);
	}


	public boolean updateCustomerList(final Map<String,List<CsvData>> customerCsvDataList, List<CsvIndex> customerCsvIndex){	
		
		if(customerCsvDataList.size()<=0)
			return false;
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		final List<String> list = new ArrayList<String>(customerCsvDataList.keySet());
		
		if (list.size()<=0)
			return false;
		
		List<CsvData> lCsvData = null;
		
		String[] str2 = new String[5000];
		StringBuffer str = null;
		
		boolean check;
		CsvData tempData = null;
		
		for(int i=0;i<list.size();i++){
			str = new StringBuffer();
			str.append("Update users set ");
			int ind = 0;
			
			check = false;
			lCsvData = (List<CsvData>) customerCsvDataList.get(list.get(i));	
			for(CsvData csvData: lCsvData){
				if(csvData.getValue()!=null && csvData.getValue().length()>0){
					if(ind==0){
						tempData = csvData;
					}
					if(ind>1){
						str.append(",");
					}
					if(ind>0){
						check = true;
						if(csvData.getHeaderName().equalsIgnoreCase("salesrepId") && (csvData.getValue().equals("-1") || csvData.getValue().equals("-1.0"))){
							str.append(csvData.getColumnName().trim());
							str.append("=");
							str.append("NULL");
							str.append("");	
						}else{
							if(csvData.getHeaderName().equalsIgnoreCase("password")){
								str.append(csvData.getColumnName().trim());
								str.append("=");
								str.append("'");
								str.append(securePassword(csvData.getValue().trim()).trim());
								str.append("'");
							}
							else{
								str.append(csvData.getColumnName().trim());
								str.append("='");
								str.append(csvData.getValue().trim());
								str.append("'");
							}
						}
					}
					ind++;
				}
			}
			str.append(" where username='");
			str.append(tempData.getValue().trim());
			str.append("';");
			if(check)
			str2[i % 5000] = str.toString();
			
			if((i+1) % 5000 == 0){
				jdbcTemplate.batchUpdate(str2);
				str2 = new String[5000];
			}
	   }
	
	   jdbcTemplate.batchUpdate(str2);
	   return true;
	}

	public boolean updateAddressList(final Map<String, List<CsvData>> addressCsvDataList, List<CsvIndex> addressCsvIndex) {
		
		if(addressCsvDataList.size()<=0 || addressCsvIndex.size()<=0)
			return false;
		
		final List<String> list = new ArrayList<String>(addressCsvDataList.keySet());
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		
		List<CsvData> lCsvData = null;
		
		String[] str2 = new String[5000];
		
		StringBuffer str = null;
		boolean check;
		
		final Map<String,Long> userIdsMap = getUserIdsMapByUsername(list);
		if(userIdsMap.size()<=0) 
			return false;
		
		for(int i=0;i<list.size();i++){
		
			str = new StringBuffer();
			str.append("Update address set ");
			int ind = 0;
			
			check = false;
			lCsvData = (List<CsvData>) addressCsvDataList.get(list.get(i));	
			
			for(CsvData csvData: lCsvData){
				if(csvData.getValue()!=null && csvData.getValue().length()>0){
					check = true;
					if(ind>0){
						str.append(",");
					}
					str.append(csvData.getColumnName().trim());
					str.append("='");
					str.append(csvData.getValue().trim());
					str.append("'");	
					ind++;
				}
			}
			str.append(" where userid='");
			str.append(userIdsMap.get(list.get(i)));
			str.append("';");
			if(check)
			str2[i % 5000] = str.toString();	
			
			if((i+1) % 5000 == 0){
				jdbcTemplate.batchUpdate(str2);
				str2 = new String[5000];
			}
	   }
	
	   jdbcTemplate.batchUpdate(str2);
	   return true;
	}
	
	public boolean updateCrmList(final Map<String, List<CsvData>> crmCsvDataList, List<CsvIndex> crmCsvIndex) {
		
		if(crmCsvDataList.size()<=0 || crmCsvIndex.size()<=0)
			return false;
		
		final List<String> list = new ArrayList<String>(crmCsvDataList.keySet());
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
			
		List<CsvData> lCsvData = null;
		
		String[] str2 = new String[5000];
	
		StringBuffer str = null;
		
		CsvData tempData = null;
		
		for(int i=0;i<list.size();i++){
		
			str = new StringBuffer();
			str.append("Update crm_contact set ");
			int ind = 0;
			
			lCsvData = (List<CsvData>) crmCsvDataList.get(list.get(i));	
			
			boolean check = false;
			for(CsvData csvData: lCsvData){
				if(csvData.getValue()!=null && csvData.getValue().length()>0){
					check = true;
					if(ind>0){
						str.append(",");
					}
					str.append(csvData.getColumnName().trim());
					str.append("='");
					str.append(csvData.getValue().trim());
					str.append("'");		
					ind++;
				}
			}
			str.append(" where email_1='");
			str.append(list.get(i));
			str.append("';");
			if(check)
			str2[i % 5000] = str.toString();
			
			if((i+1) % 5000 == 0){
				jdbcTemplate.batchUpdate(str2);
				str2 = new String[5000];
			}
	   }
	
	   jdbcTemplate.batchUpdate(str2);
	   return true;
	}
	
	public boolean insertCustomerList(final Map<String,List<CsvData>> customerCsvDataList, List<CsvIndex> customerCsvIndex, Map<String, Object> emailMap){	
		
		if(customerCsvDataList.size()<=0)
			return false;
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		final List<String> list = new ArrayList<String>(customerCsvDataList.keySet());
		
		if (list.size()<=0)
			return false;
		
		List<CsvData> lCsvData = null;
		
		String[] str3 = new String[5000];
	
		StringBuffer str = null;
		StringBuffer str2 = null;
		
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		
		for(int i=0;i<list.size();i++){
		
			str = new StringBuffer();
			str2 = new StringBuffer();
			str.append("Insert into users(created,last_modified,");
			str2.append(" values (");
			str2.append("'");
			str2.append(sdf.format(date));
			str2.append("'");
			str2.append(",");
			str2.append("'");
			str2.append(sdf.format(date));
			str2.append("'");
			str2.append(",");
			int ind = 0;
			
			lCsvData = (List<CsvData>) customerCsvDataList.get(list.get(i));	
			
			for(CsvData csvData: lCsvData){
				if(csvData.getValue()!=null && csvData.getValue().length()>0){
					if(ind>0){
						str.append(",");
						str2.append(",");
					}
					if(csvData.getHeaderName().equalsIgnoreCase("salesrepId") && (csvData.getValue().equals("-1")  || csvData.getValue().equals("-1.0"))){
						str.append(csvData.getColumnName().trim());
						str2.append("NULL");
					}else{
						if(csvData.getHeaderName().equalsIgnoreCase("password")){
							str.append(csvData.getColumnName().trim());
							str2.append("'");
							str2.append(securePassword(csvData.getValue().trim()).trim());
							str2.append("'");
						}else{
							str.append(csvData.getColumnName().trim());
							str2.append("'");
							str2.append(csvData.getValue().trim());
							str2.append("'");
						}
					}
					ind++;
				}
			}
			str.append(")");
			str2.append(");");
			str.append(str2);
			
			if(emailMap.get(list.get(i))==null){
				str3[i % 5000] = str.toString();
			} 
			
			if((i+1) % 5000 == 0){
				jdbcTemplate.batchUpdate(str3);
				str3 = new String[5000];
			}
	   }
	
	   jdbcTemplate.batchUpdate(str3);
	   return true;
	}
	
	public Map<String, Object> verifyCustomerData(Map<String, List<CsvData>> customerCsvDataList, Map<String, List<CsvData>> addressCsvDataList, List<Map<String, Object>> notAddedCustomerItems){	
		
		final List<String> list = new ArrayList<String>(customerCsvDataList.keySet());
		List<CsvData> lCsvData = null;
		Map<String, Object> thisMap = null;
		Map<String, Object> crmMap = new HashMap<String, Object>();
		
		for(int i=0;i<list.size();i++){
			lCsvData = (List<CsvData>) customerCsvDataList.get(list.get(i));
			boolean dataMatch = false;
			for(CsvData csvData: lCsvData){
				if(csvData.getValue()!=null && csvData.getValue().length()>0){
					if(csvData.getHeaderName().contains("password")){
						dataMatch = true;
					}
				}
			}
			if(!dataMatch){
				crmMap.put(list.get(i), list.get(i));
			}
		}
		
		for(int i=0;i<list.size();i++){
			lCsvData = (List<CsvData>) addressCsvDataList.get(list.get(i));
			int dataMatchAmt = 0;
			for(CsvData csvData: lCsvData){
				if(csvData.getValue()!=null && csvData.getValue().length()>0){
					if((csvData.getHeaderName().contains("firstname")) || (csvData.getHeaderName().contains("lastname"))){
						dataMatchAmt++;
					}
				}
			}
			if(dataMatchAmt<2){
				crmMap.put(list.get(i), list.get(i));
			}
		}
		
		for(String key:crmMap.keySet()){ 
			thisMap = new HashMap<String, Object>();
			thisMap.put("emailAddress", key);
			notAddedCustomerItems.add(thisMap);
			
		}
		return crmMap;	
	}
	
	public Map<String, Object> verifyCrmData(Map<String, List<CsvData>> crmCsvDataList, List<Map<String, Object>> notAddedCrmItems){	
		
		final List<String> list = new ArrayList<String>(crmCsvDataList.keySet());
		List<CsvData> lCsvData = null;
		Map<String, Object> thisMap = null;
		Map<String, Object> emailMap = new HashMap<String, Object>();
	
		for(int i=0;i<list.size();i++){
			lCsvData = (List<CsvData>) crmCsvDataList.get(list.get(i));
			int dataMatchAmt = 0;
			for(CsvData csvData: lCsvData){
				if(csvData.getValue()!=null && csvData.getValue().length()>0){
					if((csvData.getHeaderName().contains("firstname")) || (csvData.getHeaderName().contains("lastname"))){
						dataMatchAmt++;
					}
				}
			}
			if(dataMatchAmt<2){
				emailMap.put(list.get(i), list.get(i));
			}
		}
		
		for(String key:emailMap.keySet()){ 
			thisMap = new HashMap<String, Object>();
			thisMap.put("emailAddress", key);
			notAddedCrmItems.add(thisMap);	
		}
		return emailMap;	
	}
	
	public boolean verifyInputData(String str, String... type){	
		for(int i = 0; i < type.length; i++){
			if(!(str.contains(type[i]))){
				return false;
			}
		}		
		return true;
	}
	
	public boolean insertAddressList(Map<String, CsvMap> availableEmailMap, Map<String, List<CsvData>> customerCsvDataList, final Map<String, List<CsvData>> addressCsvDataList, Map<String, Object> emailMap) {
		
		if(addressCsvDataList.size()<=0)
			return false;
		
		final List<String> tempList = new ArrayList<String>(customerCsvDataList.keySet());
		final List<String> finalList = checkNewUsers(tempList, availableEmailMap);
		
		if(tempList.size()<=0 || finalList.size()<=0)
			return false;
		
		final Map<String,Long> getUserIdsMapByUsername  = getUserIdsMapByUsername(finalList);
		if(getUserIdsMapByUsername.size()<=0) return false;

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		final List<String> list = new ArrayList<String>(addressCsvDataList.keySet());
	
		List<CsvData> lCsvData = null;
		
		String[] str3 = new String[5000];
	
		StringBuffer str = null;
		StringBuffer str2 = null;
		
		for(int i=0;i<list.size();i++){
		
			str = new StringBuffer();
			str2 = new StringBuffer();
			str.append("INSERT INTO address (userid");
			str2.append(" values ('");
			str2.append(getUserIdsMapByUsername.get(finalList.get(i)));
			str2.append("'");
			
			lCsvData = (List<CsvData>) addressCsvDataList.get(list.get(i));	
			
			for(CsvData csvData: lCsvData){
				if(csvData.getValue()!=null && csvData.getValue().length()>0){
					str.append(",");
					str2.append(",");
					str.append(csvData.getColumnName().trim());
					str2.append("'");
					str2.append(csvData.getValue().trim());
					str2.append("'");
				}
			}
			str.append(")");
			str2.append(");");
			str.append(str2);
			
			if(emailMap.get(list.get(i))==null){
				str3[i % 5000] = str.toString();
			}	
			
			if((i+1) % 5000 == 0){
				jdbcTemplate.batchUpdate(str3);
				str3 = new String[5000];
			}
	   }
	
	   jdbcTemplate.batchUpdate(str3);
	   return true;
	}

	public boolean insertCrmList(final Map<String, List<CsvData>> crmCsvDataList, List<Map<String, Object>> notAddedCrmItems, Map<String, Object> crmMap) {
			
		if(crmCsvDataList.size()<=0)
			return false;
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		
		final List<String> finalList = new ArrayList<String>(crmCsvDataList.keySet()); 
		
		if (finalList.size()<=0)
			return false;
		
		List<CsvData> lCsvData = null;
		
		String[] str3 = new String[5000];
	
		StringBuffer str = null;
		StringBuffer str2 = null;
		
		for(int i=0;i<finalList.size();i++){
		
			str = new StringBuffer();
			str2 = new StringBuffer();
			str.append("insert into crm_contact (account_id,email_1");
			str2.append(" values ('1','");
			
			lCsvData = (List<CsvData>) crmCsvDataList.get(finalList.get(i));	
			str2.append(finalList.get(i));
			str2.append("'");
			
			for(CsvData csvData: lCsvData){
				if(csvData.getValue()!=null && csvData.getValue().length()>0 && (!csvData.getHeaderName().equalsIgnoreCase("username"))){
					str.append(",");
					str2.append(",");
					str.append(csvData.getColumnName().trim());
					str2.append("'");
					str2.append(csvData.getValue().trim());
					str2.append("'");
				}
			}
			str.append(")");
			str2.append(");");
			str.append(str2);
			
			if(crmMap.get(finalList.get(i))==null){
				str3[i % 5000] = str.toString();
			}
			
			if((i+1) % 5000 == 0){
				jdbcTemplate.batchUpdate(str3);
				str3 = new String[5000];
			}
	   }
	
	   jdbcTemplate.batchUpdate(str3);
	   return true;
	}

	public Map<String,Long> updateCustomerCrmContactList(final Map<String, List<CsvData>> crmCsvDataList, Map<String, Object> crmMap, Map<String,Long> crmIdListByUsername2) {
		
		if(crmCsvDataList.size()<=0)
			return null;
		
		final List<String> list = new ArrayList<String>(crmCsvDataList.keySet());
		
		if(list==null || list.size()<=0)
			return null;
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		
		//String str = "Update users set crm_contact_id=? where username=?";
	
		final Map<String,Long> crmIdListByUsername = getCrmIdsMapByUsername(list);
		crmIdListByUsername2 = crmIdListByUsername;
		if (crmIdListByUsername.size()<=0) return null;
		
		String[] str3 = new String[5000];
		
		StringBuffer str = null;
		StringBuffer str2 = null;
		int i = 0;
		
		for(String username : crmIdListByUsername.keySet()){
		
			str = new StringBuffer();
			str2 = new StringBuffer();
			str.append("Update users set crm_contact_id='"+crmIdListByUsername.get(username)+"'");
			str2.append(" where username='"+username+"'");
			
			str.append(str2);
			
			if(crmMap.get(username)==null){
				str3[i % 5000] = str.toString();
			}
			i++;
			
			if((i+1) % 5000 == 0){
				jdbcTemplate.batchUpdate(str3);
				str3 = new String[5000];
			}
	   }
	   jdbcTemplate.batchUpdate(str3);
	   return crmIdListByUsername;			
	}
	
	public boolean updateCrmUserList(final Map<String, List<CsvData>> crmCsvDataList, Map<String, Object> crmMap, Map<String,Long> crmIdListByUsername) {
		
		if(crmCsvDataList.size()<=0)
			return false;
		
		final List<String> list = new ArrayList<String>(crmCsvDataList.keySet());
		
		if(list==null || list.size()<=0)
			return false;
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		
		//String str = "Update users set crm_contact_id=? where username=?";
	
		final Map<String,Long> userListByUsername = getUserIdsMapByUsername(list);
		if (userListByUsername.size()<=0) return false;
		
		String[] str3 = new String[5000];
		
		StringBuffer str = null;
		StringBuffer str2 = null;
		int i = 0;
		
		for(String id : userListByUsername.keySet()){
		
			str = new StringBuffer();
			str2 = new StringBuffer();
			str.append("Update crm_contact set user_id='"+userListByUsername.get(id)+"'");
			str2.append(" where id='"+crmIdListByUsername.get(id)+"'");
			
			str.append(str2);
			
			if(crmMap.get(id)==null){
				str3[i % 5000] = str.toString();
			}
			i++;
			
			if((i+1) % 5000 == 0){
				jdbcTemplate.batchUpdate(str3);
				str3 = new String[5000];
			}
	   }
	   jdbcTemplate.batchUpdate(str3);
	   return true;			
	}
	
	public boolean updateCustomerDefaultAddress(Map<String, List<CsvData>> customerCsvDataList) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		final List<String> list = new ArrayList<String>(customerCsvDataList.keySet());
		
		if(list==null || list.size()<=0)
			return false;
		
		final Map<Long, Long> defaultAddressMap = getDefaultAddressByUsername(list);
		String[] str3 = new String[5000];
		
		if (defaultAddressMap.size()<=0) 
			return false;
		
		//String str = "? where userid=?";
		StringBuffer str = null;
				
		int i = 0;
		Iterator it = defaultAddressMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pairs = (Map.Entry)it.next();
	        
	        str = new StringBuffer();
			str.append("Update users set default_address_id=");
			str.append(pairs.getValue());
			str.append(" where id=");
			str.append(pairs.getKey());
			str.append(";");
			
			str3[i % 5000] = str.toString();
			i++;
			
			if((i+1) % 5000 == 0){
				jdbcTemplate.batchUpdate(str3);
				str3 = new String[5000];
			}
	    }
		
	   jdbcTemplate.batchUpdate(str3);
	   return true;	
	}
	
	public String securePassword(String password){
		String original = password;
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		md.update(original.getBytes());
		byte[] digest = md.digest();
		StringBuffer sb = new StringBuffer();
		for (byte b : digest) {
			sb.append(String.format("%02x", b & 0xff));
		}
		return sb.toString();
	}
	
	private List<String> checkNewUsers(List<String> tempList, Map<String,CsvMap> availableEmailMap) {	
		List<String> newUsers = new ArrayList<String>();	
		for(String user : tempList){
			if(availableEmailMap.get(user)==null){
				newUsers.add(user);
			}
		}
		return newUsers;
	}
	
	private List<String> checkOldUsers(List<String> tempList, Map<String,CsvMap> availableEmailMap) {	
		List<String> newUsers = new ArrayList<String>();	
		for(String user : tempList){
			if(availableEmailMap.get(user)!=null){
				newUsers.add(user);
			}
		}
		return newUsers;
	}
		
	public String getTaxIdByUserId(Integer userId) {
		return (String) getSqlMapClientTemplate().queryForObject("getTaxIdByUserId", userId);
	}
	
	public String getTextMessageServerById(Integer textMessageServerId) {
		return (String) getSqlMapClientTemplate().queryForObject("getTextMessageServerById", textMessageServerId);
	}

	public Customer getCustomerOrderReportByUserID(Integer userId) {
		return (Customer) getSqlMapClientTemplate().queryForObject("getCustomerOrderReportByUserID", userId);
	}

	public String checkUserByCellPhone(String username2) {
		if (username2 == null)
			username2 = "";

		return (String) getSqlMapClientTemplate().queryForObject("checkUserByCellPhone", username2);
	}
	
	public Customer getCellPhoneByUsernameAndPassword(String username, String password) throws DataAccessException {
		if (username == null)
			username = "";
		if (password == null)
			password = "";

		Customer customer = new Customer();
		customer.setUsername2(username);
		customer.setPassword(password);
		return (Customer) getSqlMapClientTemplate().queryForObject("getCellPhoneByUsernameAndPassword", customer);
	}
	
	public Customer getCustomerByUsername(String username) {
		if (username == null)
			username = "";

		Customer customer = new Customer();
		customer.setUsername(username);
		return (Customer) getSqlMapClientTemplate().queryForObject("getCustomer", customer);
	}

	public Customer getCustomerByUsernameAndPassword(String username, String password) throws DataAccessException {
		if (username == null)
			username = "";
		if (password == null)
			password = "";

		Customer customer = new Customer();
		customer.setUsername(username);
		customer.setPassword(password);
		return (Customer) getSqlMapClientTemplate().queryForObject("getCustomer", customer);
	}

	public Customer getCustomerByCardIdAndPassword(String cardId, String password) throws DataAccessException {
		if (cardId == null)
			cardId = "";
		if (password == null)
			password = "";

		Customer customer = new Customer();
		customer.setCardId(cardId);
		customer.setPassword(password);
		return (Customer) getSqlMapClientTemplate().queryForObject("getCustomer", customer);
	}

	public Customer getCustomerByCardID(String cardID) {
		if (cardID == null)
			cardID = "";

		Customer customer = new Customer();
		customer.setCardId(cardID);
		return (Customer) getSqlMapClientTemplate().queryForObject("getCustomer", customer);
	}

	public Integer getCustomerIdByAccountNumber(String accountNumber) {
		Integer customerId = 0;
		if (accountNumber == null || accountNumber.isEmpty())
			customerId = 0;
		customerId = (Integer) getSqlMapClientTemplate().queryForObject("getCustomerIdByAccountNumber", accountNumber);
		if (customerId == null) {
			customerId = 0;
		}
		return customerId;
	}
	
	public Integer getCustomerIdByUsername(String username) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCustomerIdByUsername", username);
	}

	public void updateCustomer(Customer customer, boolean changeUsername, boolean changePassword) {
		if (changeUsername)
			getSqlMapClientTemplate().update("updateCustomerUsername", customer, 1);
		if (changePassword)
			getSqlMapClientTemplate().update("updateCustomerPassword", customer, 1);
		getSqlMapClientTemplate().update("updateCustomer", customer);
		Integer affLevel = (Integer) globalDao.getGlobalSiteConfig().get("gAFFILIATE");
		if (affLevel != null && affLevel > 0) {
			getSqlMapClientTemplate().delete("deleteAffiliate", customer);
			if (customer.getIsAffiliate()) {
				getSqlMapClientTemplate().insert("insertAffiliate", checkAffiliate(customer));
			}
		}
		if (customer.getCatIds() != null) {
			updateCategoryIds(customer.getId(), customer.getCatIds());
		}
	}
	
	public Customer checkAffiliate(Customer customer){
		if(customer!=null && customer.getAffiliate()!=null){
			if(customer.getAffiliate().getFirstOrderPercent()==null){
				customer.getAffiliate().setFirstOrderPercent(false);
			}
			if(customer.getAffiliate().getOtherOrderPercent()==null){
				customer.getAffiliate().setOtherOrderPercent(false);
			}
		}
		return customer;
	}
	public void updateCustomerBrokerImage(Customer customer){
		getSqlMapClientTemplate().update("updateCustomerBrokerImage", customer);
	}
	
	public void removeCustomerBrokerImage(Customer customer){
		getSqlMapClientTemplate().update("removeCustomerBrokerImage", customer);
	}
	
	public int insertDialingNoteHistory(DialingNote note){
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "INSERT INTO dialing_note_history (crm_contact_id, customer_id, note, create_date) VALUES (?, ?, ?, now())";
		Object[] args = { note.getCrmId(), note.getCustomerId(), note.getNote() };
		try {
			return jdbcTemplate.update(query, args);
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
			// do nothing
		}/*
		return (Integer) getSqlMapClientTemplate().insert("insertDialingNoteHistory", note);*/
	}

	public void updateCardIdCount(Integer userId) {
		getSqlMapClientTemplate().update("updateCardIdCount", userId);
	}

	public void insertCustomer(Customer customer) throws DataAccessException {
		if (customer.isAutoGenerateAccountNumber()) {
			int nextAccountNumber = getNextIndexByName("customer_account_number");
			customer.setAccountNumber(String.valueOf(nextAccountNumber));
			getSqlMapClientTemplate().insert("insertCustomer", customer);
			updateNextIndexByName("customer_account_number");
		} else {
			getSqlMapClientTemplate().insert("insertCustomer", customer);
		}
		Address address = customer.getAddress();
		address.setUserId(customer.getId());
		address.setPrimary(true);
		insertPrimaryAddress(address);
		Integer affLevel = (Integer) globalDao.getGlobalSiteConfig().get("gAFFILIATE");
		if (affLevel != null && affLevel > 0) {
			// add check when importing affiliates.
			if (customer.getAffiliateParent() == null) {
				customer.setAffiliateParent(0);
			}
			getSqlMapClientTemplate().insert("insertAffiliateEdge", customer);
			if (customer.getIsAffiliate()) {
				getSqlMapClientTemplate().insert("insertAffiliate", checkAffiliate(customer));
			}
		}
		if (customer.getCatIds() != null) {
			insertCategoryIds(customer.getId(), customer.getCatIds());
		}
	}

	private void updateNextIndexByName(String accountName) {
		getSqlMapClientTemplate().update("updateNextIndex", accountName);
	}

	public int getNextIndexByName(String accountName) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getNextIndexValueByName", accountName);
	}
	
	public List<MobileCarrier> getMobileCarrierList(){
		return getSqlMapClientTemplate().queryForList("getMobileCarrierList");
	}

	public void updateCrmIds(Customer customer) {
		getSqlMapClientTemplate().delete("updateCrmIds", customer);
	}

	public void deleteCustomer(Customer customer) throws DataAccessException {
		// make sure to set default_address_id to null first. default_address_id is a foreign key
		// that prevents the primary address from being deleted.
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE users set default_address_id = null where id = ?";
		Object[] args1 = { customer.getId() };
		jdbcTemplate.update(query, args1);
		getSqlMapClientTemplate().delete("deleteCustomer", customer, 1);
		getSqlMapClientTemplate().delete("deleteAffiliateEdge", customer);
	}

	public Integer getParentId(Integer childId) {
		Integer parentId = (Integer) getSqlMapClientTemplate().queryForObject("getParentId", childId);
		return (parentId == null) ? 0 : parentId;
	}

	public Integer getUserIdByPromoCode(String promoCode) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getUserIdByPromoCode", promoCode);
	}

	public void nonTransactionSafeUpdateLoginStats(Customer customer) throws DataAccessException {
		getSqlMapClientTemplate().update("updateCustomerLoginStats", customer.getId(), 1);
	}

	public List<Address> getAddressListByUserid(Integer userId) {
		return getSqlMapClientTemplate().queryForList("getAddressListByUserid", userId);
	}
	
	public List<Address> getAllAddressList(Integer userId) {
		return getSqlMapClientTemplate().queryForList("getAllAddressList", userId);
	}

	public Address getAddressById(Integer addressId) {
		return (Address) getSqlMapClientTemplate().queryForObject("getAddressById", addressId);
	}

	public void updateAddress(Address address) throws DataAccessException {
		getSqlMapClientTemplate().update("updateAddress", address, 1);
		if (address.isPrimary()) {
			getSqlMapClientTemplate().update("updateDefaultAddress", address, 1);
			/*
			 * MTZ getSqlMapClientTemplate().update("updateSupplierByAddress", address);
			 */
		}
		getSqlMapClientTemplate().update("updateLastModified", address.getUserId(), 1);
	}

	private void insertPrimaryAddress(Address address) {
		getSqlMapClientTemplate().insert("insertPrimaryAddress", address);
		if (address.getCode() == null) {
			// generateAddressCode(address);
		}
		getSqlMapClientTemplate().update("updateAddress", address, 1);
		getSqlMapClientTemplate().update("updateDefaultAddress", address, 1);
	}

	public void insertAddress(Address address) throws DataAccessException {
		if (address.isPrimary()) {
			insertPrimaryAddress(address);
		} else {
			getSqlMapClientTemplate().insert("insertAddress", address);
			if (address.getCode() == null) {
				// generateAddressCode(address);
			}
		}
		getSqlMapClientTemplate().update("updateLastModified", address.getUserId(), 1);
	}

	/*
	 * private void generateAddressCode(Address address) { JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource()); if
	 * (jdbcTemplate.queryForList("SELECT config_value FROM site_config where config_key = 'MAS200' and TRIM(config_value) != ''").size() > 0) { // address code is only required for MAS200 // address code has to be unique per customer. different customers can have the same
	 * address code do { String code = UUID.randomUUID().toString().toUpperCase().replace("-", "").substring(0, 4); Object[] args = { (Integer) address.getUserId(), code }; if (jdbcTemplate.queryForInt("SELECT count(id) FROM address where userid = ? and address_code = ?", args)
	 * == 0) { Object[] args2 = { code, (Integer) address.getId() }; jdbcTemplate.update("UPDATE address set address_code = ? WHERE id = ?", args2); address.setCode(code); return; } } while (true); } }
	 */

	public void deleteAddress(Address address) throws DataAccessException {
		getSqlMapClientTemplate().delete("deleteAddress", address);
		getSqlMapClientTemplate().update("updateLastModified", address.getUserId(), 1);
	}

	public Address getDefaultAddressByUserid(Integer userId) {
		return (Address) getSqlMapClientTemplate().queryForObject("getDefaultAddressByUserid", userId);
	}

	public Address getDefaultShippingAddressByUserid(int userId) {
		return (Address) getSqlMapClientTemplate().queryForObject("getDefaultShippingAddressByUserid", userId);
	}

	public List<Customer> getCustomerExportList(CustomerSearch search) {
		return getSqlMapClientTemplate().queryForList("getCustomerExportList", search);
	}

	public int customerCount(CustomerSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCustomerListWithDateCount", search);
	}

	public List<CustomerField> getCustomerFields() {
		globalDao.getGlobalSiteConfig();
		int numOfFields = (Integer) globalDao.getGlobalSiteConfig().get("gCUSTOMER_FIELDS");

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("numOfFields", numOfFields);

		return getSqlMapClientTemplate().queryForList("getCustomerFields", paramMap);
	}
	
	public List<DialingNote> getDialingNoteHistoryById(int id, boolean isCustomer){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("id", id);
		paramMap.put("isCustomer", isCustomer);
		return getSqlMapClientTemplate().queryForList("getDialingNoteHistoryById", paramMap);
	}
	
	public void updateDialingNotesCrmToCustomer(final Integer crmId, final Integer customerId){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("crmId", crmId);
		paramMap.put("customerId", customerId);
		getSqlMapClientTemplate().update("updateDialingNotesCrmToCustomer", paramMap);
	}

	public void updateCustomerNote(Customer customer) {
		getSqlMapClientTemplate().update("updateCustomerNote", customer);
	}

	public void updateCustomerFields(List<CustomerField> customerFields) {
		for (CustomerField customerField : customerFields) {
			getSqlMapClientTemplate().update("updateCustomerFields", customerField);
		}
	}

	public void updateSpecialPricingActive(SpecialPricing specialPricing) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		getSqlMapClientTemplate().delete("updateSpecialPricingActive", specialPricing);
	}

	public SpecialPricing getSpecialPricingByCustomerIdSku(SpecialPricing specialPricing) {
		return (SpecialPricing) getSqlMapClientTemplate().queryForObject("getSpecialPricingByCustomerIdSku", specialPricing);
	}

	public List<SpecialPricing> getSpecialPricingByCustomerID(SpecialPricingSearch search) {
		return getSqlMapClientTemplate().queryForList("getSpecialPricingByCustomerID", search);
	}

	public void insertSpecialPricing(SpecialPricing specialPricing) {
		getSqlMapClientTemplate().insert("insertSpecialPricing", specialPricing);
	}

	public void deleteSpecialPricingById(Integer id) throws DataAccessException {
		getSqlMapClientTemplate().delete("deleteSpecialPricingById", id);
	}

	public Map<String, SpecialPricing> getSpecialPricingByCustomerIDPerPage(Integer customerId, List productList) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("productList", productList);
		return getSqlMapClientTemplate().queryForMap("getSpecialPricingByCustomerIDPerPage", paramMap, "sku");
	}

	public List<Customer> getCustomerListByParent(CustomerSearch search) {
		return getSqlMapClientTemplate().queryForList("getCustomerListByParent", search);
	}

	public int getCustomerListByParentCount(CustomerSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCustomerListByParentCount", search);
	}

	public void updateParent(Customer customer) {
		try {
			getSqlMapClientTemplate().update("updateCustomerParent", customer);
		} catch (DataAccessException ex) {
			// do nothing
		}
	}

	public List<Customer> getFamilyTree(int customerId, String direction) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("direction", direction);
		return getSqlMapClientTemplate().queryForList("getFamilyTree", paramMap);
	}

	public int affiliateCount() throws DataAccessException{
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		try {
			return jdbcTemplate.queryForInt("SELECT count(1) FROM affiliates");
		} catch(Exception e) {
			return 0;
		}
	}

	public void updateCredit(CustomerCreditHistory credit) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", credit.getUserId());
		paramMap.put("amount", credit.getCredit());
		getSqlMapClientTemplate().update("updateCredit", paramMap, 1);
		getSqlMapClientTemplate().insert("insertCreditHistory", credit);
	}

	public void updatePointsToCustomer(int customerId, double loyaltyPoints) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("loyaltyPoints", loyaltyPoints);
		getSqlMapClientTemplate().update("updatePointsToCustomer", paramMap, 1);
	}

	public Integer getCustomerIdBySupplierPrefix(String prefix) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCustomerIdBySupplierPrefix", prefix);
	}

	public int productCountByCustomerSupplierId(int supplierId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { (Integer) supplierId };
		// NEED to be fixed. we need to check the Prefix.
		// return jdbcTemplate.queryForInt("SELECT count(id) FROM product where supplier_id = ?", args);
		return 0;
	}

	public void unsubscribeByCustomerId(Integer customerId, String unsubscibeReason) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("unsubscibeReason", unsubscibeReason);
		getSqlMapClientTemplate().update("unsubscribeByCustomerId", paramMap, 1);
	}

	public boolean isExistingCustomer(String accountNum) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { (String) accountNum };
		int results = jdbcTemplate.queryForInt("SELECT count(id) FROM users where TRIM(account_number) = TRIM(?)", args);
		if (results > 0) {
			return true;
		} else {
			return false;
		}
	}

	public void updateEvergreenCustomerSuc(int customerId, String suc) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE users SET evergreen_suc = ? where id = ?";
		Object[] args = { suc, customerId };
		jdbcTemplate.update(query, args);
	}

	public List<Customer> getCustomerList(CustomerSearch search) {
		return getSqlMapClientTemplate().queryForList("getCustomerListByCategory", search);
	}

	private void insertCategoryIds(int userId, Set<Object> categoryIds) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "INSERT into category_users (category_id, users_id) VALUES (?, ?)";
		for (Object catId : categoryIds) {
			Object[] args = { (Integer) catId, userId };
			jdbcTemplate.update(query, args);
		}
	}

	public void updateCategoryIds(int userId, Set<Object> categoryIds) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "DELETE FROM category_users where users_id = ?";
		Object[] args1 = { userId };
		jdbcTemplate.update(query, args1);
		insertCategoryIds(userId, categoryIds);
		getSqlMapClientTemplate().update("updateLastModified", userId, 1);
	}

	public Set<Integer> getCategoryIds(int userId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { userId };
		Set<Integer> categoryIds = new HashSet<Integer>();
		String query = "SELECT category_id FROM category_users where users_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(query, args);
		while (results.next()) {
			categoryIds.add(new Integer(results.getInt("category_id")));
		}
		return categoryIds;
	}

	public void updateUserDescription(Customer customer) {
		getSqlMapClientTemplate().update("updateUserDescription", customer, 1);
	}

	public List<Customer> getCustomersAjax(CustomerSearch search) {
		return getSqlMapClientTemplate().queryForList("getCustomersAjax", search);
	}

	// customer group
	public List<CustomerGroup> getCustomerGroupList(CustomerGroupSearch search) {
		return getSqlMapClientTemplate().queryForList("getCustomerGroupList", search);
	}

	public List<CustomerGroup> getCustomerGroupNameList() {
		return getSqlMapClientTemplate().queryForList("getCustomerGroupNameList", null);
	}

	public CustomerGroup getCustomerGroupById(Integer groupId) {
		return (CustomerGroup) getSqlMapClientTemplate().queryForObject("getCustomerGroupById", groupId);
	}

	public void deleteCustomerGroupById(Integer groupId) {
		getSqlMapClientTemplate().delete("deleteCustomerGroupById", groupId);
		getSqlMapClientTemplate().delete("deleteGroupCustomerByGroupId", groupId);
	}

	public void insertCustomerGroup(CustomerGroup group) throws DataAccessException {
		getSqlMapClientTemplate().insert("insertCustomerGroup", group);
	}

	public void updateCustomerGroup(CustomerGroup group) {
		getSqlMapClientTemplate().update("updateCustomerGroup", group, 1);
	}

	public List<Integer> getCustomerGroupIdList(Integer customerId) {
		return getSqlMapClientTemplate().queryForList("getCustomerGroupIdList", customerId);
	}

	public List<Integer> getCustomerIdByGroupId(Integer groupId) {
		return (ArrayList<Integer>) getSqlMapClientTemplate().queryForList("getCustomerIdByGroupId", groupId);
	}

	public void batchGroupIds(Integer customerId, Set groupIds, String action) {
		Iterator iter = groupIds.iterator();
		if (action.equals("add")) {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
			String query = "INSERT into group_user_join (group_id, user_id, new_entry) VALUES (?, ?, false ) ON DUPLICATE KEY UPDATE new_entry = false ";
			while (iter.hasNext()) {
				Object[] args = { (Integer) iter.next(), customerId };
				try {
					jdbcTemplate.update(query, args);
				} catch (DataIntegrityViolationException e) {
					// do nothing
				}
			}
		} else if (action.equals("remove")) {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			while (iter.hasNext()) {
				try {
					paramMap.put("customerId", customerId);
					paramMap.put("groupId", (Integer) iter.next());
					getSqlMapClientTemplate().delete("deleteCustomerIdGroupId", paramMap);
				} catch (DataIntegrityViolationException e) {
					// do nothing
				}
			}
		} else {
			insertGroupIds(customerId, groupIds);
		}
	}
	
	public void batchAssignUserToCustomers(List<Integer> userIds, AccessUser accessUser) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userIds", userIds);
		paramMap.put("salesRepId", (accessUser != null ? accessUser.getSalesRepId() : null));
		paramMap.put("accessUserId", (accessUser != null ? accessUser.getId() : null));
		getSqlMapClientTemplate().update("batchAssignUserToCustomers", paramMap);	
		getSqlMapClientTemplate().update("batchAssignSalesRepToCustomer", paramMap);
		getSqlMapClientTemplate().update("batchUpdateCrmContacts", paramMap);
	}

	public void updateSuspension(Integer customerId, String action) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("action", action);
		getSqlMapClientTemplate().update("updateSuspension", paramMap);
	}
	
	public void updateNewInformation(Integer customerId, Integer action) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("action", action);
		getSqlMapClientTemplate().update("updateNewInformation", paramMap);
	}
	
	public void updateQualifier(Integer customerId, String action) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("action", action);
		getSqlMapClientTemplate().update("updateQualifier", paramMap);
	}
	
	public void updateTrackCode(Integer customerId, String trackCode) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("trackCode", trackCode);
		getSqlMapClientTemplate().update("updateTrackCode", paramMap);
	}
	
	public void updateMainSource(Integer customerId, String mainSource) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("mainSource", mainSource);
		getSqlMapClientTemplate().update("updateMainSource", paramMap);
	}
	
	public void updatePaid(Integer customerId, String paid) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("paid", paid);
		getSqlMapClientTemplate().update("updatePaid", paramMap);
	}
	
	public void updateMedium(Integer customerId, String medium) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("medium", medium);
		getSqlMapClientTemplate().update("updateMedium", paramMap);
	}
	
	public void updateLanguageField(Integer customerId, String languageField) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerId", customerId);
		paramMap.put("languageField", languageField);
		getSqlMapClientTemplate().update("updateLanguageField", paramMap);
	}

	private void insertGroupIds(Integer customerId, Set groupIds) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Iterator iter = groupIds.iterator();
		String query = "INSERT into group_user_join (group_id, user_id, new_entry) VALUES (?, ?, true ) ON DUPLICATE KEY UPDATE new_entry = true ";
		while (iter.hasNext()) {
			Object[] args = { (Integer) iter.next(), customerId };
			try {
				jdbcTemplate.update(query, args);
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
		getSqlMapClientTemplate().delete("deleteGroupCustomerId", customerId);
		getSqlMapClientTemplate().update("updateGroupCustomerId", customerId);
	}

	public Address getAddress(int userId, String addressCode) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("addressCode", addressCode);
		return (Address) getSqlMapClientTemplate().queryForObject("getAddress", paramMap);
	}

	public int[] insertCustomerGroup(final int groupId, final Set<String> accountNumber) {
		final String[] accountNumberArray = (String[]) accountNumber.toArray(new String[accountNumber.size()]);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "INSERT INTO group_user_join (group_id, user_id) " + "SELECT ?, id FROM users WHERE account_number = ?";
		return jdbcTemplate.batchUpdate(query.toString(), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setInt(1, groupId);
				ps.setString(2, accountNumberArray[i]);
			}

			public int getBatchSize() {
				return accountNumber.size();
			}
		});
	}

	public int[] nonTransactionSafeDeleteCustomerGroup(final int groupId, final Set<String> accountNumber) {
		final String[] accountNumberArray = (String[]) accountNumber.toArray(new String[accountNumber.size()]);
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "DELETE FROM group_user_join WHERE group_id = ? and user_id IN " + "(SELECT id FROM users WHERE account_number = ?)";
		return jdbcTemplate.batchUpdate(query.toString(), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ps.setInt(1, groupId);
				ps.setString(2, accountNumberArray[i]);
			}

			public int getBatchSize() {
				return accountNumber.size();
			}
		});
	}

	public boolean isGroupCustomerExist(Integer groupId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { (Integer) groupId };
		if (jdbcTemplate.queryForInt("SELECT count(user_id) FROM group_user_join WHERE group_user_join.group_id = ?", args) >= 1) {
			return true;
		} else {
			return false;
		}
	}

	public Map<Integer, Customer> getQuoteTotalByUserIDPerPage(List customerList) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerList", customerList);
		return getSqlMapClientTemplate().queryForMap("getQuoteTotalByUserIDPerPage", paramMap, "id");
	}

	public Map<Integer, String> getCustomerCompanyMap(List<VirtualBank> useridList) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("useridList", useridList);
		return getSqlMapClientTemplate().queryForMap("getCustomerCompanyMap", paramMap, "userid", "company");
	}

	public Map<Integer, String> getCustomerNameMap(List<Integer> userIdList) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userIdList", userIdList);
		return getSqlMapClientTemplate().queryForMap("getCustomerNameMap", paramMap, "userid", "name");
	}

	public Map<Integer, Double> getPaymentsTotalToCustomerMap(List<VirtualBank> useridList) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("useridList", useridList);
		return getSqlMapClientTemplate().queryForMap("getPaymentsTotalToCustomerMap", paramMap, "userid", "supplier_pay");
	}

	public String getUserName(Integer userId, Integer contactId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String email = null;
		if (userId != null && userId > 0) {
			Object[] args = { (Integer) userId };
			email = jdbcTemplate.queryForObject("SELECT username FROM users where id = ?", args, String.class);
		} else if (contactId != null && contactId > 0) {
			Object[] args = { (Integer) contactId };
			email = jdbcTemplate.queryForObject("SELECT email_1 FROM crm_contact where id = ?", args, String.class);
		}
		return email;
	}

	public void insertBudgetPartners(BudgetPartner partner) {
		getSqlMapClientTemplate().insert("insertBudgetPartners", partner);
	}

	public void updatePartners(BudgetPartner partner) {
		getSqlMapClientTemplate().update("updatePartners", partner);
	}

	public String getPartnerNameById(Integer id) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { id };
		return jdbcTemplate.queryForObject("SELECT partner_name FROM budget_partners WHERE id = ?", args, String.class);
	}

	public List<BudgetPartner> getPartnersList(Boolean active) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("active", active);
		return getSqlMapClientTemplate().queryForList("getPartnersList", paramMap);
	}

	public BudgetPartner getPartnerById(BudgetPartner partner) {
		return (BudgetPartner) getSqlMapClientTemplate().queryForObject("getPartnerById", partner);
	}

	public void insertPartnerCustomer(CustomerBudgetPartner partner) {
		getSqlMapClientTemplate().insert("insertPartnerCustomer", partner);
		// insert history
		insertPartnerCustomerHistory(partner);
	}

	public void updatePartnerCustomer(CustomerBudgetPartner partner, boolean addHistory) {
		getSqlMapClientTemplate().update("updatePartnerCustomer", partner);
		// insert history
		if(addHistory) {
			insertPartnerCustomerHistory(partner);
		}
	}

	public List<CustomerBudgetPartner> getPartnersListByCustomerId(Integer userId, Boolean active) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("active", active);
		return (ArrayList<CustomerBudgetPartner>) getSqlMapClientTemplate().queryForList("getPartnersListByCustomerId", paramMap);
	}

	public void insertPartnerCustomerHistory(CustomerBudgetPartner partner) {
		getSqlMapClientTemplate().insert("insertPartnerCustomerHistory", partner);
	}

	public Boolean getPartnerByName(String name) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		int count = 0;
		count = jdbcTemplate.queryForInt("SELECT count(1) from budget_partners where partner_name = ?", name);
		if (count > 0) {
			return true;
		} else {
			return false;
		}
	}

	public int getLuceneCustomerListCount(CustomerSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getLuceneCustomerListCount", search);
	}
	public List<Map<String, Object>>  getLuceneCustomer(int offset, int limit, Integer userId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("offset", offset);
		paramMap.put("limit", limit);
		paramMap.put("userId", userId);
		return getSqlMapClientTemplate().queryForList("getLuceneCustomer", paramMap);
	}
	// Update Data form staging table
	public int updateUserRatings(StagingUser user) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query = null;
		String crmQuery = null;
		String ratingValue = null;
		int count = 0;
		
		if(user.getRating1() != null && user.getRating2() != null) {
			try {
				if(user.getCrmContactId() != null && user.getCrmContactId() > 0) {
					crmQuery = "UPDATE crm_contact SET rating1=?, rating2=? WHERE id=?";
					Object[] crmArgs = { user.getRating1(), user.getRating2(), user.getCrmContactId() };
					count = jdbcTemplate.update(crmQuery, crmArgs);
				}else{
					if(user.getUsername()!=null && user.getUsername().length()>0){
						Object[] args = { user.getUsername() };
						Integer crmContactId = null;
						try {
							// check in users table
							crmContactId = jdbcTemplate.queryForObject("SELECT crm_contact_id FROM users where username = ?", args, Integer.class);
						} catch(Exception e) { }
						
						if(crmContactId == null) {
							try {
								// check in crm contact table as it might be just lead and not converted to cutomer yet.
								crmContactId = jdbcTemplate.queryForObject("SELECT id FROM crm_contact where email_1 = ?", args, Integer.class);
							} catch(Exception e) {
							}
						}
						if(crmContactId != null && crmContactId > 0){
							crmQuery = "UPDATE crm_contact SET rating1=?, rating2=? WHERE id=?";
							Object[] crmArgs = { user.getRating1(), user.getRating2(), crmContactId};
							count = jdbcTemplate.update(crmQuery, crmArgs);
						}
					}
				}
				if(user.getUsername()!=null && user.getUsername().length()>0){
					query = "UPDATE users SET rating1= ?, rating2= ? WHERE username= ?";
					Object[] args = { user.getRating1(), user.getRating2(), user.getUsername()};
					count = jdbcTemplate.update(query, args);
				}else{
					if(user.getCrmContactId() != null && user.getCrmContactId() > 0){
						Object[] args = { user.getCrmContactId() };
						String username = null;
						try {
							// check in user Table
							username = jdbcTemplate.queryForObject("SELECT username FROM users where crm_contact_id = ?", args, String.class);
						} catch(Exception e){ }
						
						if(username == null) {
							try {
								// check in crm contact table as it might be just lead and not converted to cutomer yet.
								username = jdbcTemplate.queryForObject("SELECT email_1 FROM crm_contact where id = ?", args, String.class);
							} catch(Exception e){ }
						}
						
						if(username != null && username.length() > 0){
							query = "UPDATE users SET rating1=?, rating2=? WHERE username=?";
							Object[] args2 = { user.getRating1(), user.getRating2(), username};
							count = jdbcTemplate.update(query, args2);
						}
					}
				}
				return count;
			} catch (DataIntegrityViolationException e) {
				return count;
				// do nothing
			}
		} else {
			if(user.getRating1() != null) {
				query = "UPDATE users SET rating1= ? WHERE username=?";
				crmQuery = "UPDATE crm_contact SET rating1= ? WHERE id=?";
				ratingValue = user.getRating1();
			}else if(user.getRating2() != null) {
				query = "UPDATE users SET rating2= ? WHERE username=?";
				crmQuery = "UPDATE crm_contact SET rating2=? WHERE id=?";
				ratingValue = user.getRating2();
			}
			if(ratingValue != null) {
				Object[] args = { ratingValue, user.getUsername()};
				try {
					if(user.getCrmContactId() != null && user.getCrmContactId() > 0) {
						Object[] crmArgs = { ratingValue, user.getCrmContactId() };
						count = jdbcTemplate.update(crmQuery, crmArgs);
					}else{
						if(user.getUsername()!=null && user.getUsername().length()>0){
							Object[] args3 = { user.getUsername() };
							Integer crmContactId = null;
							try {
								crmContactId = jdbcTemplate.queryForObject("SELECT crm_contact_id FROM users where username = ?", args3, Integer.class);
							} catch(Exception e){ }
							if(crmContactId != null && crmContactId > 0){
								Object[] crmArgs = { ratingValue, crmContactId};
								count = jdbcTemplate.update(crmQuery, crmArgs);
							}
						}
					}
					if(user.getUsername()!=null && !user.getUsername().trim().isEmpty()){
						count = jdbcTemplate.update(query, args);
					}else{
						if(user.getCrmContactId() != null && user.getCrmContactId() > 0){
							Object[] args4 = { user.getCrmContactId() };
							String username = null;
							try {
								username = jdbcTemplate.queryForObject("SELECT username FROM users where crm_contact_id = ?", args4, String.class);
							} catch(Exception e){ }
							if(username != null && !username.trim().isEmpty()){
								Object[] args2 = { ratingValue, username};
								count = jdbcTemplate.update(query, args2);
							}
						}
					}
					return count;
				} catch (DataIntegrityViolationException e) {
					return 0;
					// do nothing
				}
			}
		}
		return 0;
	}

	public Integer batchCustomerRating1(List<Integer> contactIds, String rating) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("contactIds", contactIds);
		paramMap.put("rating", rating);
		return getSqlMapClientTemplate().update("batchCustomerRating1", paramMap);
	}
	
	public Integer batchCustomerRating2(List<Integer> contactIds, String rating) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("contactIds", contactIds);
		paramMap.put("rating", rating);
		return getSqlMapClientTemplate().update("batchCustomerRating2", paramMap);
	}
	
	public Integer batchCustomerQualifier(List<Integer> contactIds, String qualifier) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("contactIds", contactIds);
		paramMap.put("qualifier", qualifier);
		return getSqlMapClientTemplate().update("batchCustomerQualifier", paramMap);
	}
	
	public Integer batchCustomerLanguage(List<Integer> contactIds, String language) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("contactIds", contactIds);
		paramMap.put("language", language);
		return getSqlMapClientTemplate().update("batchCustomerLanguage", paramMap);
	}
	
	public void updateUserNotifications(Customer customer){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("id", customer.getId());
		paramMap.put("emailNotify", customer.isEmailNotify());
		paramMap.put("unsubscribe", customer.isUnsubscribe());
		paramMap.put("textMessageNotify", customer.isTextMessageNotify());
		if(customer.getTaxId()!=null){
			paramMap.put("taxId", customer.getTaxId());
		}
		if(customer.getField1()!=null){
			paramMap.put("field1", customer.getField1());
		}
		if(customer.getField2()!=null){
			paramMap.put("field2", customer.getField2());
		}
		if(customer.getField3()!=null){
			paramMap.put("field3", customer.getField3());
		}
		if(customer.getField4()!=null){
			paramMap.put("field4", customer.getField4());
		}
		if(customer.getField5()!=null){
			paramMap.put("field5", customer.getField5());
		}
		if(customer.getField6()!=null){
			paramMap.put("field6", customer.getField6());
		}
		if(customer.getField7()!=null){
			paramMap.put("field7", customer.getField7());
		}
		if(customer.getField8()!=null){
			paramMap.put("field8", customer.getField8());
		}
		if(customer.getField9()!=null){
			paramMap.put("field9", customer.getField9());
		}
		if(customer.getField10()!=null){
			paramMap.put("field10", customer.getField10());
		}
		if(customer.getField11()!=null){
			paramMap.put("field11", customer.getField11());
		}
		if(customer.getField12()!=null){
			paramMap.put("field12", customer.getField12());
		}
		if(customer.getField13()!=null){
			paramMap.put("field13", customer.getField13());
		}
		if(customer.getField14()!=null){
			paramMap.put("field14", customer.getField14());
		}
		if(customer.getField15()!=null){
			paramMap.put("field15", customer.getField15());
		}
		if(customer.getField16()!=null){
			paramMap.put("field16", customer.getField16());
		}
		if(customer.getField17()!=null){
			paramMap.put("field17", customer.getField17());
		}
		if(customer.getField18()!=null){
			paramMap.put("field18", customer.getField18());
		}
		if(customer.getField19()!=null){
			paramMap.put("field19", customer.getField19());
		}
		if(customer.getField20()!=null){
			paramMap.put("field20", customer.getField20());
		}
		getSqlMapClientTemplate().update("updateUserNotifications", paramMap);
	}
	
	public void updateCrmNotifications(CrmContact crmcontact){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("id", crmcontact.getId());
		paramMap.put("unsubscribe", crmcontact.isUnsubscribe());
		getSqlMapClientTemplate().update("updateCrmNotifications", paramMap);
	}	
	
	public List<CustomerExtContact> getCustomerExtContactListByUserid(Integer userId) {
		return getSqlMapClientTemplate().queryForList("getCustomerExtContactListByUserid", userId);
	}

	public CustomerExtContact getCustomerExtContactById(Integer customerExtContactId) {
		return (CustomerExtContact) getSqlMapClientTemplate().queryForObject("getCustomerExtContactListById", customerExtContactId);
	}
	public void updateCustomerExtContact(CustomerExtContact customerExtContact) throws DataAccessException {
		getSqlMapClientTemplate().update("updateCustomerExtContact", customerExtContact, 1);
	}
	
	public void insertCustomerExtContact(CustomerExtContact customerExtContact) throws DataAccessException {
		getSqlMapClientTemplate().insert("insertCustomerExtContact", customerExtContact);
	}
	
	public void deleteCustomerExtContact(CustomerExtContact customerExtContact) throws DataAccessException {
		getSqlMapClientTemplate().delete("deleteCustomerExtContact", customerExtContact);
	}
}