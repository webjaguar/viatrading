package com.webjaguar.dao.ibatis;

import java.util.List;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.DealDao;
import com.webjaguar.model.Deal;
import com.webjaguar.model.DealSearch;
import com.webjaguar.model.ViaDeal;


public class IbatisDealDao extends SqlMapClientDaoSupport implements DealDao{

	public void deleteDealById(Integer dealId) {
		getSqlMapClientTemplate().delete("deleteDealById", dealId);
	}

	public Deal getDealById(Integer dealId) {
		return (Deal) getSqlMapClientTemplate( ).queryForObject( "getDealById", dealId );
	}

	public Deal getDealBySku(String getSku) {
		return (Deal) getSqlMapClientTemplate( ).queryForObject( "getDealBySku", getSku );
	}

	public List<Deal> getDealsList( DealSearch search ) {
		return  getSqlMapClientTemplate().queryForList("getDealList", search);
	}

	public void insertDeal(Deal deal) {
		getSqlMapClientTemplate().insert("insertDeal", deal);
	}

	public void updateDeal(Deal deal) {
		getSqlMapClientTemplate().update("updateDeal", deal);
	}
	
	public List<ViaDeal> getViaDealsList( DealSearch search ) {
		return  getSqlMapClientTemplate().queryForList("getViaDealsList", search);
	}
	
	public ViaDeal getViaDealById(Integer dealId) {
		return (ViaDeal) getSqlMapClientTemplate( ).queryForObject( "getViaDealById", dealId );
	}

	public ViaDeal getViaDealBySku(String getSku) {
		return (ViaDeal) getSqlMapClientTemplate( ).queryForObject( "getViaDealBySku", getSku );
	}

	public void insertViaDeal(ViaDeal viaDeal) {
		getSqlMapClientTemplate().insert("insertViaDeal", viaDeal);
	}

	public void updateViaDeal(ViaDeal viaDeal) {
		getSqlMapClientTemplate().update("updateViaDeal", viaDeal);
	}

	public void deleteViaDealById(Integer dealId) {
		getSqlMapClientTemplate().delete("deleteViaDealById", dealId);
	}

}
