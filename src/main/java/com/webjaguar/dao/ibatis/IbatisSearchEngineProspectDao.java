/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 */

package com.webjaguar.dao.ibatis;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.SearchEngineProspectDao;
import com.webjaguar.model.SearchEngineProspectEntity;

public class IbatisSearchEngineProspectDao extends SqlMapClientDaoSupport implements SearchEngineProspectDao
{
	
	private JdbcTemplate jdbcTemplate;
	
	private JdbcTemplate getJdbcTemplate() {
		if(jdbcTemplate == null) {
			jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		}
		return jdbcTemplate;
	}

	/**
	 * Fetches all records associated with a particular "Search Engine Prospect"
	 * @param prospectId The prospect identifier
	 * @return A {@link List} of {@link SearchEngineProspectEntity}
	 */
	public List<SearchEngineProspectEntity> findByProspectId(String prospectId){
		String sql = "SELECT * FROM search_engine_prospect WHERE prospect_id = ?";
		logger.trace("SQL Statement: "+ sql);
		List<SearchEngineProspectEntity> info =  (List<SearchEngineProspectEntity>)getJdbcTemplate().query(
				sql, new Object[] { prospectId}, 
				BeanPropertyRowMapper.newInstance(SearchEngineProspectEntity.class));
		return info;
	}
	
	/**
	 * Fetches single entity given its database identity column.
	 * @param id The id of the row
	 * @return A {@link SearchEngineProspectEntity}
	 */
	public SearchEngineProspectEntity findById(Integer id){
		String sql = "SELECT * FROM search_engine_prospect WHERE id = ?";
		SearchEngineProspectEntity info =  (SearchEngineProspectEntity)getJdbcTemplate().queryForObject(
				sql, new Object[] {id}, 
				BeanPropertyRowMapper.newInstance(SearchEngineProspectEntity.class));
		return info;
	}
	
	/**
	 * Returns the last (most recent) recorded data for a given prospect
	 * @param prospectId The prospect identifier
	 * @return A {@link SearchEngineProspectEntity}
	 */
	public SearchEngineProspectEntity findLastByProspectId(String prospectId){
		String sql = "SELECT * FROM search_engine_prospect WHERE prospect_id = ? ORDER BY timestamp DESC LIMIT 1";
		SearchEngineProspectEntity info =  (SearchEngineProspectEntity)getJdbcTemplate().queryForObject(
				sql, new Object[] {prospectId}, 
				BeanPropertyRowMapper.newInstance(SearchEngineProspectEntity.class));
		return info;
	}
	
	/**
	 * Returns the first (oldest) recorded data for a given prospect
	 * @param prospectId The prospect identifier
	 * @return A {@link SearchEngineProspectEntity}
	 */	
	public SearchEngineProspectEntity findFirstByProspectId(String prospectId){
		String sql = "SELECT * FROM search_engine_prospect WHERE prospect_id = ? ORDER BY timestamp LIMIT 1";
		SearchEngineProspectEntity info =  (SearchEngineProspectEntity)getJdbcTemplate().queryForObject(
				sql, new Object[] {prospectId}, 
				BeanPropertyRowMapper.newInstance(SearchEngineProspectEntity.class));
		return info;
	}	
	
	/**
	 * Inserts prospect information into the database.
	 * @param prospectId The prospect identifier
	 * @param referrer The "normalized" referrer domain name
	 * @param queryString The query string
	 */
	public void addProspectInformation(String prospectId, String referrer, String queryString){
		String sql = "INSERT INTO search_engine_prospect (prospect_id,referrer,query_string) VALUES (?,?,?)";
		getJdbcTemplate().update(sql,prospectId,referrer,queryString);
	}
}
