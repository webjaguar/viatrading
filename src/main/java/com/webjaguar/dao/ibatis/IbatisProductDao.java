/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.ProductDao;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.DataFeedSearch;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.KitParts;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MyListSearch;
import com.webjaguar.model.Option;
import com.webjaguar.model.OptionSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductCategory;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductFieldUnlimited;
import com.webjaguar.model.ProductFieldUnlimitedNameValue;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.ProductLabelTemplate;
import com.webjaguar.model.ProductLabelTemplateSearch;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductOptionValue;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.model.RangeValue;
import com.webjaguar.model.ShoppingListGroup;
import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.inflector.Inflector;
import com.webjaguar.web.admin.datafeed.WebjaguarCategory;
import com.webjaguar.web.domain.Constants;

public class IbatisProductDao extends SqlMapClientDaoSupport implements ProductDao {

	public void insertProduct(Product product) {
		getSqlMapClientTemplate().insert("insertProduct", product);
		if(product.getInventory() != null){
			product.setInventoryAFS(product.getInventory());
		}else{
			product.setInventoryAFS(0);
			product.setInventory(0);
		}
		getSqlMapClientTemplate().update("updateProduct", product, 1);
		getSqlMapClientTemplate().insert("insertProductAffiliateCommission", product);
		if (product.getCatIds() != null) {
			insertCategoryIds(product.getId(), product.getCatIds());
		}
		if (product.getImages() != null) {
			updateImages(product.getId(), product.getImages());
		}
		if (product.getI18nProduct() != null) {
			updateI18nProduct(product.getId(), product.getI18nProduct().values());
		}
		if (product.getVariants() != null) {
			insertProductVariants(product.getId(), product.getVariants());
		}
		if (product.getKitParts() != null) {
			insertKitParts(product.getSku(), product.getKitParts());
		}
	}

	public int insertProductWithSkuOnly(String sku) {
		Product product = new Product();
		product.setCreatedBy("Cdsbooksdvds Feed");
		product.setSku(sku);
		try {
			getSqlMapClientTemplate().insert("insertProduct", product);
			return product.getId();
		} catch (Exception e) {
			return this.getProductIdBySku(sku);
		}
	}

	public void deleteProduct(Product product) {
		getSqlMapClientTemplate().delete("deleteProduct", product);
	}

	public void deleteProductById(Integer productId) {
		getSqlMapClientTemplate().delete("deleteProductById", productId);
	}

	public void deleteProductBySku(String sku) {
		getSqlMapClientTemplate().delete("deleteProductBySku", sku);
	}

	public void updateProduct(Product product) {
		if (!product.isRetainOnCart()) {
			// delete from temp shopping cart when product is updated.
			getSqlMapClientTemplate().delete("deleteCartItemByProductId", product.getId());
			getSqlMapClientTemplate().delete("deleteCartItemAttributeByProductId", product.getId());
			getSqlMapClientTemplate().delete("deleteCartItemCustomLinesByProductId", product.getId());
		}

		getSqlMapClientTemplate().update("updateProduct", product, 1);
		getSqlMapClientTemplate().update("updateProductAffiliateCommission", product);
		getSqlMapClientTemplate().update("updateProductLastModified", product, 1);
		if (product.getCatIds() != null) {
			insertCategoryIds(product.getId(), product.getCatIds());
		}
		if (product.getImages() != null) {
			updateImages(product.getId(), product.getImages());
		}
		if (product.getI18nProduct() != null) {
			updateI18nProduct(product.getId(), product.getI18nProduct().values());
		}
		if (product.getVariants() != null) {
			insertProductVariants(product.getId(), product.getVariants());
		}
		if (product.getKitParts() != null) {
			insertKitParts(product.getSku(), product.getKitParts());
		}
	}
	
	public void updateApiProduct(Product product) {
		getSqlMapClientTemplate().update("updateApiProduct", product, 1);
		getSqlMapClientTemplate().update("updateProductLastModified", product, 1);
		if (product.getCatIds() != null && !product.getCatIds().isEmpty()) {
			insertCategoryIds(product.getId(), product.getCatIds());
		}
		if (product.getImages() != null && product.getImages().size() > 0) {
			updateImages(product.getId(), product.getImages());
		}
		if (product.getI18nProduct() != null && !product.getI18nProduct().isEmpty()) {
			updateI18nApiProduct(product.getId(), product.getI18nProduct().values());
		}
	}

	public List<Product> getProductsOnFrontendBySupplier(Integer supplierId) {
		return getSqlMapClientTemplate().queryForList("getProductsOnFrontendBySupplier", supplierId);
	}

	public List getProductsBySupplierId(Integer supplierId) {
		return getSqlMapClientTemplate().queryForList("getProductsBySupplierId", supplierId);
	}

	public void getCustomCategoryProductSearch(ProductSearch search) {
		Map<Integer, String> productFieldTypeMap = this.getProductFieldType(search.getgPRODUCT_FIELDS());
		for (ProductField productField : search.getProductField()) {
			productField.setFieldType(productFieldTypeMap.get(productField.getId()));
		}
	}

	public int getProductsByCategoryIdCount(ProductSearch search) {

		if (search.getSku() == null || search.getSku().equals("")) {
			search.setSku(null);
		}
		if (search.getProductField() != null && search.getProductField().size() > 0) {
			getCustomCategoryProductSearch(search);
		}
		if (search.getCategorySort() != null && search.getCategorySort().equals("viewed")) {
			search.setEndDate(null);
			search.setStartDate(null);
		}
		if (search.getCategoryIds() == null || search.getCategoryIds().isEmpty()) {
			if (search.getCategorySort() != null && search.getCategorySort().contains("best_seller")) {
				return (Integer) getSqlMapClientTemplate().queryForObject("getProductsByCategoryIdBestSellerCount", search);
			} else {
				return (Integer) getSqlMapClientTemplate().queryForObject("getProductsByCategoryIdCount", search);
			}
		} else {
			if (search.getCategorySort() != null && search.getCategorySort().contains("best_seller")) {
				return (Integer) getSqlMapClientTemplate().queryForObject("getProductsByCategoryIdsListBestSellerCount", search);
			} else {
				return (Integer) getSqlMapClientTemplate().queryForObject("getProductsByCategoryIdsListCount", search);
			}
		}
	}

	public List<Product> getProductsByCategoryId(ProductSearch search) {
		// split string:'field_24 ASC' to field (field_24), field number(24), orderBY (ASC)
		if (search.getSort().startsWith("field")) {
			Map<Integer, String> productFieldTypeMap = this.getProductFieldType(search.getgPRODUCT_FIELDS());
			String[] tempSort = search.getSort().split(" ");
			String[] sortFieldNumber = tempSort[0].split("_");
			if ((productFieldTypeMap != null && !productFieldTypeMap.isEmpty()) && productFieldTypeMap.get(Integer.parseInt(sortFieldNumber[1].trim())).equalsIgnoreCase("number")) {
				search.setSortTemp(tempSort[0]);
				search.setOrderBy(tempSort[1]);
				search.setFieldType("number");
			} else {
				search.setFieldType("string");
			}
		} else {
			search.setFieldType("string");
		}

		if (search.getSku() == null || search.getSku().equals("")) {
			search.setSku(null);
		}
		if (search.getProductField() != null && search.getProductField().size() > 0) {
			getCustomCategoryProductSearch(search);
		}
		if (search.getCategorySort() != null && search.getCategorySort().equals("viewed")) {
			search.setEndDate(null);
			search.setStartDate(null);
		}
		if (search.getCategoryIds() == null || search.getCategoryIds().isEmpty()) {
			if (search.getCategorySort() != null && search.getCategorySort().contains("best_seller")) {
				return getSqlMapClientTemplate().queryForList("getProductsByCategoryIdBestSeller", search);
			} else {
				return getSqlMapClientTemplate().queryForList("getProductsByCategoryId", search);
			}
		} else {
			if (search.getCategorySort() != null && search.getCategorySort().contains("best_seller")) {
				return getSqlMapClientTemplate().queryForList("getProductsByCategoryIdsListBestSeller", search);
			} else {
				return getSqlMapClientTemplate().queryForList("getProductsByCategoryIdsList", search);
			}
		}
	}

	public List<Product> getProductsFieldByCategoryId(ProductSearch search) {
		/*
		 * Need to implement Code for getProductsByCategoryIdBestSeller getProductsByCategoryIdsListBestSeller getProductsByCategoryIdsList
		 */

		if (search.getCategorySort() != null && search.getCategorySort().equals("viewed")) {
			search.setEndDate(null);
			search.setStartDate(null);
		}
		if (search.getCategoryIds() == null || search.getCategoryIds().isEmpty()) {
			if (search.getCategorySort() != null && search.getCategorySort().contains("best_seller")) {
				// In process
				return getSqlMapClientTemplate().queryForList("getProductsByCategoryIdBestSeller", search);
			} else {
				// DONE
				return getSqlMapClientTemplate().queryForList("getProductsFieldByCategoryId", search);
			}
		} else {
			if (search.getCategorySort() != null && search.getCategorySort().contains("best_seller")) {
				// In process
				return getSqlMapClientTemplate().queryForList("getProductsByCategoryIdsListBestSeller", search);
			} else {
				// In process
				return getSqlMapClientTemplate().queryForList("getProductsByCategoryIdsList", search);
			}
		}
	}

	public List<Product> getProductAjax(ProductSearch productSearch) {
		return getSqlMapClientTemplate().queryForList("getProductAjax", productSearch);
	}

	public String getProductNameBySku(String sku) {
		return (String) getSqlMapClientTemplate().queryForObject("getProductNameBySku", sku);
	}

	public String getProductFieldById(int productId, int fieldNumber) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("productId", productId);
		paramMap.put("fieldNumber", fieldNumber);

		return (String) getSqlMapClientTemplate().queryForObject("getProductFieldById", paramMap);
	}
	
	public List<RangeValue> getRangeValues(String type) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("type", type);
		return getSqlMapClientTemplate().queryForList("getRangeValues", paramMap);
	}

	public String getProductAvialabilityById(Integer productId) {
		return (String) getSqlMapClientTemplate().queryForObject("getProductAvialabilityById", productId);
	}

	public Product getProductById(Integer productId, int numImages) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("productId", productId);
		paramMap.put("protectedAccess", Constants.FULL_PROTECTED_ACCESS);
		Product product = (Product) getSqlMapClientTemplate().queryForObject("getProductById", paramMap);
		if (product != null) {
			product.setCatIds(getCategoryIdsByProductId(product.getId()));
			if (numImages > 0) {
				product.setImages(getImages(product.getId(), numImages));
			}
		}
		return product;
	}

	public Product getProductById(Integer productId, int numImages, String protectedAccess, String lang, boolean basedOnHand) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("productId", productId);
		paramMap.put("protectedAccess", protectedAccess);
		paramMap.put("frontend", "frontend");
		paramMap.put("basedOnHand", basedOnHand);
		paramMap.put("lang", lang);
		Product product = (Product) getSqlMapClientTemplate().queryForObject("getProductById", paramMap);
		if (product != null) {
			if (product.getSalesTag() != null) {
				product.setSalesTag((com.webjaguar.model.SalesTag) getSqlMapClientTemplate().queryForObject("getSalesTagById", product.getSalesTag().getTagId()));
			}
			if (numImages > 0) {
				product.setImages(getImages(product.getId(), numImages));
				if (!product.getImages().isEmpty()) {
					// get thumbnail
					ProductImage thumbnail = new ProductImage();
					thumbnail.setImageUrl(product.getImages().get(0).getImageUrl());
					product.setThumbnail(thumbnail);
				}
			}
		}
		return product;
	}
	
	public Product getProductById2(Integer productId, int numImages, String protectedAccess, String lang, boolean basedOnHand) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("productId", productId);
		paramMap.put("protectedAccess", protectedAccess);
		paramMap.put("frontend", "backend");
		paramMap.put("basedOnHand", basedOnHand);
		paramMap.put("lang", lang);
		Product product = (Product) getSqlMapClientTemplate().queryForObject("getProductById", paramMap);
		if (product != null) {
			if (product.getSalesTag() != null) {
				product.setSalesTag((com.webjaguar.model.SalesTag) getSqlMapClientTemplate().queryForObject("getSalesTagById", product.getSalesTag().getTagId()));
			}
			if (numImages > 0) {
				product.setImages(getImages(product.getId(), numImages));
				if (!product.getImages().isEmpty()) {
					// get thumbnail
					ProductImage thumbnail = new ProductImage();
					thumbnail.setImageUrl(product.getImages().get(0).getImageUrl());
					product.setThumbnail(thumbnail);
				}
			}
		}
		return product;
	}

	private void insertCategoryIds(Integer productId, Set categoryIds) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Iterator iter = categoryIds.iterator();
		String query = "INSERT into category_product (category_id, product_id, new_entry) VALUES (?, ?, true ) ON DUPLICATE KEY UPDATE new_entry = true  ";
		while (iter.hasNext()) {
			Object[] args = { (Integer) iter.next(), productId };
			try {
				jdbcTemplate.update(query, args);
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
		getSqlMapClientTemplate().delete("deleteProductCatId", productId);
		getSqlMapClientTemplate().update("updateProductCatId", productId);
	}

	public void batchCategoryIds(Integer productId, Set categoryIds, String action) {
		Iterator iter = categoryIds.iterator();
		if (action.equals("add")) {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
			String query = "INSERT into category_product (category_id, product_id, new_entry) VALUES (?, ?, false ) ON DUPLICATE KEY UPDATE new_entry = false  ";
			while (iter.hasNext()) {
				Object[] args = { (Integer) iter.next(), productId };
				try {
					jdbcTemplate.update(query, args);
				} catch (DataIntegrityViolationException e) {
					// do nothing
				}
			}
		} else if (action.equals("remove")) {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			while (iter.hasNext()) {
				try {
					paramMap.put("productId", productId);
					paramMap.put("categoryId", (Integer) iter.next());
					getSqlMapClientTemplate().delete("deleteProductIdCatId", paramMap);
				} catch (DataIntegrityViolationException e) {
					// do nothing
				}
			}
		} else {
			insertCategoryIds(productId, categoryIds);
		}
	}

	public void batchFieldUpdate(Set productIds, Integer fieldId, String fieldValue) {
		Iterator iter = productIds.iterator();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE product SET field_" + fieldId + " =  '" + fieldValue + "' WHERE  id= ? ";
		while (iter.hasNext()) {
			Object[] args = { (Integer) iter.next() };
			try {
				jdbcTemplate.update(query, args);
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}

	private void updateImages(Integer productId, List<ProductImage> images) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "DELETE FROM product_image where product_id = ?";
		Object[] args1 = { productId };
		jdbcTemplate.update(query, args1);
		query = "INSERT into product_image (image_index, product_id, image_url) VALUES (?, ?, ?)";
		int index = 1;
		for (ProductImage image : images) {
			Object[] args = { index++, productId, image.getImageUrl() };
			try {
				jdbcTemplate.update(query, args);
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}

	public Set<Object> getCategoryIdsByProductId(Integer productId) throws DataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { productId };
		Set<Object> categoryIds = new TreeSet<Object>();
		String query = "SELECT category_id FROM category_product where product_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(query, args);
		while (results.next()) {
			categoryIds.add(new Integer(results.getInt("category_id")));
		}
		return categoryIds;
	}

	private Set<Object> getSupplierIds(String sku) throws DataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { sku };
		Set<Object> supplierIds = new TreeSet<Object>();
		String query = "SELECT company FROM supplier WHERE id IN (SELECT supplier_id FROM product_supplier where sku = ?)";
		SqlRowSet results = jdbcTemplate.queryForRowSet(query, args);
		while (results.next()) {
			supplierIds.add(results.getString("company"));
		}
		return supplierIds;
	}

	public List<ProductImage> getImages(Integer productId, int numImages) throws DataAccessException {

		List<ProductImage> images = new ArrayList<ProductImage>();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("productId", productId);
		paramMap.put("numImages", numImages);
		Iterator iter = getSqlMapClientTemplate().queryForList("getProductImages", paramMap).iterator();
		while (iter.hasNext()) {
			images.add((ProductImage) iter.next());
		}
		return images;
	}

	public List getProductList(ProductSearch search) {
		// move Product Search to use left menu filtsers
		if (search.getCategory() == null) {
			return getSqlMapClientTemplate().queryForList("getProductList", search);
		}
		return getSqlMapClientTemplate().queryForList("getProductListBySearch", search);
	}

	public List<Integer> getProductIdListBySearch(ProductSearch search) {
		if (search.getCategory() == null) {
			return getSqlMapClientTemplate().queryForList("getProductIdListBySearch", search);
		}
		return getSqlMapClientTemplate().queryForList("getProductIdListBySearchCount", search);
	}

	public int getProductListCount(ProductSearch search) {
		if (search.getCategory() == null) {
			return (Integer) getSqlMapClientTemplate().queryForObject("getProductListCount", search);
		}
		return (Integer) getSqlMapClientTemplate().queryForObject("getProductListBySearchCount", search);
	}

	public List<Product> getStoneEdgeProductList(ProductSearch search) {
		return getSqlMapClientTemplate().queryForList("getStoneEdgeProductList", search);
	}

	public List<String> getProductSkuListByCategoryId(Integer cid) {
		return (ArrayList) getSqlMapClientTemplate().queryForList("getProductSkuListByCategoryId", cid);
	}

	public List<Product> getProductExportList(int limit, int offset) {
		ProductSearch search = new ProductSearch();
		search.setLimit(limit);
		search.setOffset(offset);
		return getProductExportList(search, 0);
	}

	public List<Product> getProductExportList(ProductSearch search, int numImages) {
		List<Product> productList = getSqlMapClientTemplate().queryForList("getProductExportList", search);
		Iterator<Product> iter = productList.iterator();
		while (iter.hasNext()) {
			Product product = iter.next();
			product.setCatIds(getCategoryIdsByProductId(product.getId()));
			product.setSupplierIds(getSupplierIds(product.getSku()));
			product.setImages(getImages(product.getId(), numImages));
			if (search.isWithI18n()) {
				// i18n
				product.setI18nProduct(getI18nProduct(product.getId()));
			}
		}
		return productList;
	}

	public List<Product> getSiteMapProductList(ProductSearch search) {
		return getSqlMapClientTemplate().queryForList("getSiteMapProductList", search);
	}

	public List getProductSupplierExportList(int limit, int offset) {
		Map<String, Integer> paramMap = new HashMap<String, Integer>();
		paramMap.put("limit", limit);
		paramMap.put("offset", offset);
		return getSqlMapClientTemplate().queryForList("getProductSupplierExportList", paramMap);
	}

	public void updateProductFields(List<ProductField> productFields) {
		for (ProductField productField : productFields) {
			getSqlMapClientTemplate().update("updateProductFields", productField);
		}
	}

	public void updateI18nProductFields(List<ProductField> productFields) {
		for (ProductField productField : productFields) {
			getSqlMapClientTemplate().update("updateI18nProductFields", productField);
		}
	}

	public void updateInventory(List<LineItem> lineItems) {
		for (LineItem lineItem : lineItems) {
			Inventory inventory = new Inventory();
			inventory.setInventory(lineItem.getQuantity() * (-1));
			inventory.setSku(lineItem.getProduct().getSku());
			getSqlMapClientTemplate().update("updateInventory", inventory);
		}
	}
	
	//get LineItem sku :- Check if product exists in orders_lineItems

	public String getLineItemSku (String sku){
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { sku };
		try{
		    return (String) jdbcTemplate.queryForObject("SELECT distinct product_sku FROM orders_lineitems where product_sku = ?", args, String.class);
		} catch (EmptyResultDataAccessException exception){
			return null;
		}
	}
	
	public List<Product> getQuantityPending(List<Product> productList) {
		List<Product> list = new ArrayList<Product>();
		Product productTemp = new Product();
		for (Product product : productList) {
			productTemp = (Product) getSqlMapClientTemplate().queryForObject("getQuantityPending", product.getSku());
			product.setOrderPendingQty(productTemp.getOrderPendingQty());
			product.setPurchasePendingQty(productTemp.getPurchasePendingQty());
			list.add(product);
		}
		return list;
	}

	// kit
	private void insertKitParts(String productSku, List<KitParts> kitParts) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "DELETE FROM kit_parts where kit_sku = '" + productSku + "'";
		jdbcTemplate.update(query);

		for (KitParts kit : kitParts) {
			getSqlMapClientTemplate().insert("insertKitParts", kit);
		}
	}

	public List<KitParts> getKitPartsByKitSku(String kitSku) {
		return getSqlMapClientTemplate().queryForList("getKitPartsByKitSku", kitSku);
	}

	public void deductInventory(List<LineItem> lineItems) {
		for (LineItem lineItem : lineItems) {
			lineItem.setInventoryQuantity(lineItem.getQuantity());
			getSqlMapClientTemplate().update("deductProductIventory", lineItem);
		}
	}

	public void addInventory(List<LineItem> lineItems) {
		for (LineItem lineItem : lineItems) {
			lineItem.setInventoryQuantity(lineItem.getQuantity());
			Inventory inventory = new Inventory();
			inventory.setInventory(lineItem.getQuantity());
			inventory.setInventory(lineItem.getQuantity());
			inventory.setSku(lineItem.getProduct().getSku());
			getSqlMapClientTemplate().update("updateInventory", inventory);
			getSqlMapClientTemplate().update("updateinventoryAFS", inventory);

		}
	}

	public void updateSalesTagToProductId(int productId, Integer salesTagId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("productId", productId);
		paramMap.put("salesTagId", salesTagId);
		getSqlMapClientTemplate().update("updateSalesTagToProductId", paramMap);
	}

	public void updateManufacturerToProductId(int productId, Integer manufacturerId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("productId", productId);
		paramMap.put("manufacturerId", manufacturerId);
		getSqlMapClientTemplate().update("updateManufacturerToProductId", paramMap);
	}

	public List<ProductField> getProductFields(String protectedAccess, int numProdFields) {

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("numProdFields", numProdFields);
		paramMap.put("protectedAccess", protectedAccess);
		paramMap.put("rank", null);

		List<ProductField> productFields = new ArrayList<ProductField>();
		for (int i = 0; i < numProdFields; i++) {
			ProductField productField = new ProductField();
			productField.setId(i + 1);
			productFields.add(productField);
		}

		Iterator iter = getSqlMapClientTemplate().queryForList("getProductFields", paramMap).iterator();
		while (iter.hasNext()) {
			ProductField productField = (ProductField) iter.next();
			productFields.set(productField.getId() - 1, productField);
		}

		return productFields;
	}

	public Map<Integer, String> getProductFieldType(int numProdFields) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("numProdFields", numProdFields);
		return getSqlMapClientTemplate().queryForMap("getProductFieldType", paramMap, "id", "fieldType");
	}

	public Map<String, Map<Integer, ProductField>> getI18nProductFields(int numProdFields) {
		Map<String, Map<Integer, ProductField>> map = new HashMap<String, Map<Integer, ProductField>>();

		for (Object productFieldO : getSqlMapClientTemplate().queryForList("getI18nProductFields", numProdFields)) {
			ProductField productField = (ProductField) productFieldO;
			if (!map.containsKey(productField.getLang())) {
				map.put(productField.getLang(), new HashMap<Integer, ProductField>());
			}
			map.get(productField.getLang()).put(productField.getId(), productField);
		}

		return map;
	}

	public List<ProductField> getProductFieldsRanked(String protectedAccess, int numProdFields, String lang) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("numProdFields", numProdFields);
		paramMap.put("protectedAccess", protectedAccess);
		paramMap.put("rank", "rank");
		paramMap.put("lang", lang);

		return getSqlMapClientTemplate().queryForList("getProductFields", paramMap);
	}

	public List<Product> searchProducts(ProductSearch search) {
		Map<Integer, String> productFieldTypeMap = this.getProductFieldType(search.getgPRODUCT_FIELDS());
		if (search != null && search.getProductField() != null) {
			for (ProductField productField : search.getProductField()) {
				productField.setFieldType(productFieldTypeMap.get(productField.getId()));
			}
		}

		StringTokenizer splitter = new StringTokenizer(search.getKeywords(), " ", false);
		while (splitter.hasMoreTokens()) {
			String token = splitter.nextToken();
			if (token.length() >= search.getKeywordsMinChars()) {
				// replace special characters with special meaning
				token = token.replace("%", "\\%");
				token = token.replace("_", "\\_");
				if (search.isInflector()) {
					search.addKeyword("%" + search.getDelimiter() + new Inflector().singularize(token.toLowerCase()) + (search.isEndDelimiter() ? search.getDelimiter() + "%" : "%"));
				} else {
					search.addKeyword("%" + search.getDelimiter() + token.toLowerCase() + (search.isEndDelimiter() ? search.getDelimiter() + "%" : "%"));
				}

			}
		}

		return getSqlMapClientTemplate().queryForList("searchProducts", search);
	}

	public int searchProductsCount(ProductSearch search) {
		Map<Integer, String> productFieldTypeMap = this.getProductFieldType(search.getgPRODUCT_FIELDS());
		if (search != null && search.getProductField() != null) {
			for (ProductField productField : search.getProductField()) {
				productField.setFieldType(productFieldTypeMap.get(productField.getId()));
			}
		}

		StringTokenizer splitter = new StringTokenizer(search.getKeywords(), " ", false);
		while (splitter.hasMoreTokens()) {
			String token = splitter.nextToken();
			if (token.length() >= search.getKeywordsMinChars()) {
				// replace special characters with special meaning
				token = token.replace("%", "\\%");
				token = token.replace("_", "\\_");
				if (search.isInflector()) {
					search.addKeyword("%" + search.getDelimiter() + new Inflector().singularize(token.toLowerCase()) + (search.isEndDelimiter() ? search.getDelimiter() + "%" : "%"));
				} else {
					search.addKeyword("%" + search.getDelimiter() + token.toLowerCase() + (search.isEndDelimiter() ? search.getDelimiter() + "%" : "%"));
				}
			}
		}

		return (Integer) getSqlMapClientTemplate().queryForObject("searchProductsCount", search);
	}

	public List getShoppingListByUserid(MyListSearch search) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", search.getUserId());
		paramMap.put("protectedAccess", search.getProtectedAccess());
		paramMap.put("groupId", search.getGroupId());
		return getSqlMapClientTemplate().queryForList("getShoppingListByUserid", paramMap);
	}

	public int productCount() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		return jdbcTemplate.queryForInt("SELECT count(id) FROM product");
	}

	public int productCount(ProductSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("productCount", search);
	}

	public int productSupplierCount() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		return jdbcTemplate.queryForInt("SELECT count(sku) FROM product_supplier");
	}

	public void addToList(Integer userId, Set productIds) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "INSERT into shopping_list (userid, product_id, added) VALUES (?, ?, now())";
		Iterator iter = productIds.iterator();
		while (iter.hasNext()) {
			Object[] args = { userId, (Integer) iter.next() };
			try {
				jdbcTemplate.update(query, args);
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}

	public void removeFromList(Integer userId, Set productIds) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "DELETE FROM shopping_list WHERE userid = ? AND product_id = ?";
		Iterator iter = productIds.iterator();
		while (iter.hasNext()) {
			Object[] args = { userId, (Integer) iter.next() };
			try {
				jdbcTemplate.update(query, args);
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}

	public List<ProductOption> getProductOptionsByOptionCode(String optionCode, Integer salesTagId, String protectedAccess) {
		List<ProductOption> options = new ArrayList<ProductOption>();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("optionCode", optionCode);
		paramMap.put("protectedAccess", protectedAccess);
		Iterator<ProductOption> iter = getSqlMapClientTemplate().queryForList("getProductOptionsByOptionCode", paramMap).iterator();
		while (iter.hasNext()) {
			ProductOption po = iter.next();
			paramMap.put("optionIndex", po.getIndex());
			paramMap.put("salesTagId", salesTagId);
			po.setValues(getSqlMapClientTemplate().queryForList("getProductOptionValuesByOptionCode", paramMap));
			options.add(po);
		}
		return options;
	}

	public boolean duplicateOptionName(String productId, String name) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { (String) productId, (String) name };
		if (jdbcTemplate.queryForInt("SELECT count(1)  FROM product_options where product_id = ? and name = ?", args) >= 1) {
			return true;
		} else {
			return false;
		}
	}

	public void updateProductOptions(Option option) {
		boolean newOption = (option.getId() == null);

		// delete from temp shopping cart when product option is updated.
		getSqlMapClientTemplate().delete("deleteCartItemByOptionCode", option.getCode());
		getSqlMapClientTemplate().delete("deleteCartItemByOptionCode", option.getOldCode());
		getSqlMapClientTemplate().delete("deleteCartItemAttributeByOptionCode", option.getCode());
		getSqlMapClientTemplate().delete("deleteCartItemAttributeByOptionCode", option.getOldCode());
		getSqlMapClientTemplate().delete("deleteCartItemCustomLinesByOptionCode", option.getCode());
		getSqlMapClientTemplate().delete("deleteCartItemCustomLinesByOptionCode", option.getOldCode());

		if (newOption) {
			getSqlMapClientTemplate().insert("insertOption", option);
			option.setId(getOption(null, option.getCode()).getId());
		} else {
			// update option
			getSqlMapClientTemplate().update("updateOption", option);
		}

		int index = 0;
		for (ProductOption po : option.getProductOptions()) {
			po.setProductId(option.getId());
			po.setIndex(index++);
			getSqlMapClientTemplate().update("updateProductOption", po);
			if (po.getValues() != null) {
				for (ProductOptionValue pov : po.getValues()) {
					pov.setProductId(option.getId());
					pov.setOptionIndex(po.getIndex());
					getSqlMapClientTemplate().update("updateProductOptionValue", pov);
				}
			}
			if (!newOption) {
				// delete product option values
				ProductOptionValue pov = new ProductOptionValue(po.getValues() == null ? 0 : po.getValues().size());
				pov.setProductId(option.getId());
				pov.setOptionIndex(po.getIndex());
				getSqlMapClientTemplate().delete("deleteProductOptionValues", pov);
			}
		}
		if (!newOption) {
			// delete product option
			ProductOption po = new ProductOption(option.getProductOptions().size());
			po.setProductId(option.getId());
			getSqlMapClientTemplate().delete("deleteProductOptionsAndValues", po);
		}
	}

	public Integer getProductIdBySku(String sku) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getProductIdBySku", sku);
	}
	
	public String getProductSkuById(Integer id) {
		return (String) getSqlMapClientTemplate().queryForObject("getProductSkuById", id);
	}

	public boolean isValidProductId(int id) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { (Integer) id };
		if (jdbcTemplate.queryForInt("SELECT count(id) FROM product where id = ?", args) == 0) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isASIUniqueIdExist(int uniqueId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { (Integer) uniqueId };
		if (jdbcTemplate.queryForInt("SELECT count(id) FROM product where asi_unique_id = ?", args) == 0) {
			return false;
		} else {
			return true;
		}
	}

	public List getProductSupplierListBySku(String sku) {
		return getSqlMapClientTemplate().queryForList("getProductSupplierListBySku", sku);
	}
	
	public Integer getProductSupplierCost(String sku, Integer defaultSupplierId){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("sku", sku);
		paramMap.put("defaultSupplierId", defaultSupplierId);
		return (Integer)getSqlMapClientTemplate().queryForObject("getProductSupplierCost", paramMap);
	}

	public List getProductSkuBySId(Integer supplierId) {
		return getSqlMapClientTemplate().queryForList("getProductSkuBySId", supplierId);
	}

	public void updateProductSupplier(Supplier supplier) {
		getSqlMapClientTemplate().update("updateProductSupplier", supplier, 1);
	}

	public void insertProductSupplier(Supplier supplier) {
		getSqlMapClientTemplate().insert("insertProductSupplier", supplier);
	}

	public int getDefaultSupplierIdBySku(String sku) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { (String) sku };
		return jdbcTemplate.queryForInt("SELECT default_supplier_id FROM product WHERE sku=?", args);
	}

	public void updateDefaultSupplierId(Integer supplierId, String productSku) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("sku", productSku);
		paramMap.put("supplierId", supplierId);
		getSqlMapClientTemplate().update("updateDefaultSupplierId", paramMap);
	}

	public void importProductSuppliers(List<Supplier> suppliers) {
		for (Supplier supplier : suppliers) {
			getSqlMapClientTemplate().insert("insertProductSupplier", supplier);
			if (supplier.isPrimary()) {
				updateDefaultSupplierId(supplier.getId(), supplier.getSku());
			}
		}
	}

	public boolean isValidSupplierId(int id) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { (Integer) id };
		if (jdbcTemplate.queryForInt("SELECT count(id) FROM supplier where id = ?", args) == 0) {
			return false;
		} else {
			return true;
		}
	}

	public Supplier getProductSupplierBySIdSku(String sku, Integer supplierId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("sku", sku);
		paramMap.put("supplierId", supplierId);
		return (Supplier) getSqlMapClientTemplate().queryForObject("getProductSupplierBySIdSku", paramMap);
	}

	public Supplier getProductSupplierPrimaryBySku(String sku) {
		return (Supplier) getSqlMapClientTemplate().queryForObject("getProductSupplierPrimaryBySku", sku);
	}

	public void deleteProductSupplier(String sku, Integer supplierId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("sku", sku);
		paramMap.put("supplierId", supplierId);
		getSqlMapClientTemplate().delete("deleteProductSupplier", paramMap);
	}

	public void nonTransactionSafeUpdateStats(Product product) {
		getSqlMapClientTemplate().update("updateProductStats", product.getId(), 1);
	}

	public List<Option> getOptionsList(OptionSearch search) {
		return getSqlMapClientTemplate().queryForList("getOptionsList", search);
	}

	public int getOptionsListCount() {
		return (Integer) getSqlMapClientTemplate().queryForObject("getOptionsListCount");
	}

	public Option getOption(Integer id, String code) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("optionId", id);
		paramMap.put("optionCode", code);
		Option option = (Option) getSqlMapClientTemplate().queryForObject("getOption", paramMap);
		if (option != null) {
			paramMap.put("optionId", option.getId());
			// get product options
			List<ProductOption> options = new ArrayList<ProductOption>();
			Iterator iter = getSqlMapClientTemplate().queryForList("getProductOptionsByOptionId", paramMap).iterator();
			while (iter.hasNext()) {
				ProductOption po = (ProductOption) iter.next();
				paramMap.put("optionIndex", po.getIndex());
				po.setValues(getSqlMapClientTemplate().queryForList("getProductOptionValuesByOptionId", paramMap));
				options.add(po);
			}
			option.setProductOptions(options);
		}
		return option;
	}

	public void deleteOption(Option option) {
		// delete from temp shopping cart when product option is deleted.
		getSqlMapClientTemplate().delete("deleteCartItemByOptionCode", option.getOldCode());
		getSqlMapClientTemplate().delete("deleteCartItemAttributeByOptionCode", option.getOldCode());
		getSqlMapClientTemplate().delete("deleteCartItemCustomLinesByOptionCode", option.getOldCode());

		getSqlMapClientTemplate().delete("deleteOption", option, 1);
	}

	public int[] updateProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		StringBuffer sql = new StringBuffer("UPDATE " + "product " + "SET " + "last_modified = now()");
		for (CsvFeed csvFeed : csvFeedList) {
			String[] columnName = csvFeed.getColumnName().split(",");
			for (int x = 0; x < columnName.length; x++) {
				if (columnName[x].equals("protected_level")) {
					sql.append(", protected_level = (b?)");
				} else {
					sql.append(", " + columnName[x] + " = ?");
				}
			}
		}
		// sql.deleteCharAt(sql.lastIndexOf(",")); // remove trailing ,
		sql.append(" WHERE feed_freeze = 0 AND sku = ?");
		return jdbcTemplate.batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map<String, Object> entry = data.get(i);
				int index = 1;
				for (CsvFeed csvFeed : csvFeedList) {
					String[] columnName = csvFeed.getColumnName().split(",");
					for (int x = 0; x < columnName.length; x++) {
						if (entry.get(csvFeed.getColumnName()) == null) {
							ps.setNull(index++, Types.NULL);
						} else if (entry.get(csvFeed.getColumnName()).getClass() == Boolean.class) {
							ps.setBoolean(index++, (Boolean) entry.get(csvFeed.getColumnName()));
						} else {
							ps.setString(index++, entry.get(csvFeed.getColumnName()).toString());
						}
					}
				}
				ps.setString(index++, entry.get("sku").toString());
			}

			public int getBatchSize() {
				return data.size();
			}
		});
	}

	public int[] updateASIProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		StringBuffer sql = new StringBuffer("UPDATE " + "product " + "SET " + "last_modified = now(), active = 1");
		for (CsvFeed csvFeed : csvFeedList) {
			String[] columnName = csvFeed.getColumnName().split(",");
			for (int x = 0; x < columnName.length; x++) {
				if (columnName[x].equals("protected_level")) {
					sql.append(", protected_level = (b?)");
				} else {
					sql.append(", " + columnName[x] + " = ?");
				}
			}
		}
		// sql.deleteCharAt(sql.lastIndexOf(",")); // remove trailing ,
		sql.append(" WHERE feed_freeze = 0 AND sku = ?");
		return jdbcTemplate.batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map<String, Object> entry = data.get(i);
				int index = 1;
				for (CsvFeed csvFeed : csvFeedList) {
					String[] columnName = csvFeed.getColumnName().split(",");
					for (int x = 0; x < columnName.length; x++) {
						if (entry.get(csvFeed.getColumnName()) == null) {
							ps.setNull(index++, Types.NULL);
						} else if (entry.get(csvFeed.getColumnName()).getClass() == Boolean.class) {
							ps.setBoolean(index++, (Boolean) entry.get(csvFeed.getColumnName()));
						} else {
							ps.setString(index++, entry.get(csvFeed.getColumnName()).toString());
						}
					}
				}
				ps.setString(index++, entry.get("sku").toString());
			}

			public int getBatchSize() {
				return data.size();
			}
		});
	}

	public void inactiveProduct(final List<String> data, String column) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String sql = "UPDATE product SET active = 0 WHERE " + column + " = ?";
		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				String asiUniquId = data.get(i);
				ps.setString(1, asiUniquId);
			}

			public int getBatchSize() {
				return data.size();
			}
		});
	}

	public void updateProductImage(Map<Long, List<String>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query1 = "DELETE FROM product_image where product_id = ?";
		String query2 = "INSERT into product_image (image_index, product_id, image_url) VALUES (?, ?, ?)";
		for (Long productId : data.keySet()) {
			Object[] args1 = { productId };
			jdbcTemplate.update(query1, args1);
			int index = 1;
			for (String image : data.get(productId)) {
				Object[] args2 = { index++, productId, image };
				try {
					jdbcTemplate.update(query2, args2);
				} catch (DataIntegrityViolationException e) {
					// do nothing
				}
			}
		}
	}

	public void insertWebjaguarCategory1(List<WebjaguarCategory> categoryList) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("categoryList", categoryList);
		getSqlMapClientTemplate().insert("insertWebjaguarCategory", paramMap);
	}

	public int[] updateWebjaguarProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());

		StringBuffer sql = new StringBuffer("UPDATE " + "product " + "SET " + "last_modified = now()");
		for (CsvFeed csvFeed : csvFeedList) {
			String[] columnName = csvFeed.getColumnName().split(",");
			for (int x = 0; x < columnName.length; x++) {
				if (columnName[x].equals("protected_level")) {
					sql.append(", protected_level = (b?)");
				} else {
					sql.append(", " + columnName[x] + " = ?");
				}
			}
		}
		// sql.deleteCharAt(sql.lastIndexOf(",")); // remove trailing ,
		sql.append(" WHERE sku = ?");

		int[] size = jdbcTemplate.batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map<String, Object> entry = data.get(i);
				int index = 1;
				for (CsvFeed csvFeed : csvFeedList) {
					String[] columnName = csvFeed.getColumnName().split(",");
					for (int x = 0; x < columnName.length; x++) {
						if (entry.get(csvFeed.getColumnName()) == null) {
							ps.setNull(index++, Types.NULL);
						} else if (entry.get(csvFeed.getColumnName()).getClass() == Boolean.class) {
							ps.setBoolean(index++, (Boolean) entry.get(csvFeed.getColumnName()));
						} else {
							ps.setString(index++, entry.get(csvFeed.getColumnName()).toString());
						}
					}
				}
				ps.setString(index++, entry.get("sku").toString());
			}

			public int getBatchSize() {
				return data.size();
			}
		});

		return size;
	}

	public int[] updatedCdsAndDvdsProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());

		StringBuffer sql = new StringBuffer("UPDATE " + "product " + "SET " + "last_modified = now()");
		for (CsvFeed csvFeed : csvFeedList) {
			String[] columnName = csvFeed.getColumnName().split(",");
			for (int x = 0; x < columnName.length; x++) {
				if (columnName[x].equals("protected_level")) {
					sql.append(", protected_level = (b?)");
				} else {
					sql.append(", " + columnName[x] + " = ?");
				}
			}
		}
		// sql.deleteCharAt(sql.lastIndexOf(",")); // remove trailing ,
		sql.append(" WHERE sku = ?");

		int[] size = jdbcTemplate.batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map<String, Object> entry = data.get(i);
				int index = 1;
				for (CsvFeed csvFeed : csvFeedList) {
					String[] columnName = csvFeed.getColumnName().split(",");
					for (int x = 0; x < columnName.length; x++) {
						if (entry.get(csvFeed.getColumnName()) == null) {
							ps.setNull(index++, Types.NULL);
						} else if (entry.get(csvFeed.getColumnName()).getClass() == Boolean.class) {
							ps.setBoolean(index++, (Boolean) entry.get(csvFeed.getColumnName()));
						} else {
							ps.setString(index++, entry.get(csvFeed.getColumnName()).toString());
						}
					}
				}
				ps.setString(index++, entry.get("sku").toString());
			}

			public int getBatchSize() {
				return data.size();
			}
		});

		return size;
	}

	public void updateCategoryData(final Map<Long, Integer> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "INSERT into category_product (category_id, product_id, new_entry) VALUES (?, ?, true ) ON DUPLICATE KEY UPDATE new_entry = true  ";

		for (Long productId : data.keySet()) {
			Object[] args = { data.get(productId), productId };
			try {
				jdbcTemplate.update(query, args);
			} catch (DataIntegrityViolationException e) {
				e.printStackTrace();
				// do nothing
			}
		}
	}

	/*
	 * Input : feed name (String) ; start or end of products (boolean) if start of products : set feed_new to false if end of products : set inactive where feed_new is false
	 */
	public void markAndInactiveOldProducts(String feed, Integer dataFeedId, boolean endDocument) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());

		String query = null;
		if (endDocument) {
			if (dataFeedId != null) {
				query = "UPDATE product SET active = 0 WHERE feed_new = false AND feed = ? AND feed_id = ? AND feed_freeze = 0";
			} else {
				query = "UPDATE product SET active = 0 WHERE feed_new = false AND feed = ? AND feed_id IS NULL AND feed_freeze = 0";
			}
		} else {
			if (dataFeedId != null) {
				query = "UPDATE product SET feed_new = false WHERE feed = ? AND feed_id = ?";
			} else {
				query = "UPDATE product SET feed_new = false WHERE feed = ? AND feed_id IS NULL";
			}
		}

		if (dataFeedId != null) {
			Object[] args = { feed, dataFeedId };
			jdbcTemplate.update(query, args);
		} else {
			Object[] args = { feed };
			jdbcTemplate.update(query, args);
		}
	}

	public void updateProductCategory(Map<Long, List<Long>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query1 = "DELETE FROM category_product where product_id = ?";
		String query2 = "INSERT into category_product (product_id, category_id) VALUES (?, ?)";
		for (Long productId : data.keySet()) {
			Object[] args1 = { productId };
			jdbcTemplate.update(query1, args1);
			for (Long categoryId : data.get(productId)) {
				Object[] args2 = { productId, categoryId };
				try {
					jdbcTemplate.update(query2, args2);
				} catch (DataIntegrityViolationException e) {
					// do nothing
				}
			}
		}
	}

	public Map<String, Long> getProductSkuMap() {
		return getSqlMapClientTemplate().queryForMap("getProductSkuMap", null, "sku", "id");
	}

	public List<Map<String, Object>> getCostListByProductId(Integer productId) {
		return getSqlMapClientTemplate().queryForList("getCostListByProductId", productId);
	}

	public void deleteOldProducts(String feed, boolean makeInActive) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query1 = "DELETE FROM product where feed = ? AND feed_new = false";
		if (makeInActive) {
			query1 = "UPDATE product set active = false where feed = ? AND feed_new = false";
		}
		String query2 = "UPDATE product set feed_new = false where feed = ?";
		Object[] args = { feed };
		jdbcTemplate.update(query1, args);
		jdbcTemplate.update(query2, args);
	}

	public void inventoryZeroInactive(String feed) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query1 = "UPDATE product set active = false where feed = ? AND inventory <= 0 AND active = true";
		Object[] args = { feed };
		jdbcTemplate.update(query1, args);
	}

	// master sku
	public List<Integer> getSlaves(ProductSearch search) {
		return getSqlMapClientTemplate().queryForList("getSlaves", search);
	}

	public Double getProductWeightBySku(String sku) {
		return (Double) getSqlMapClientTemplate().queryForObject("getProductWeightBySku", sku);
	}

	public Map<String, Object> getMasterSkuDetailsBySku(String sku, String protectedAccess, boolean checkInventory) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("sku", sku);
		paramMap.put("protectedAccess", protectedAccess);
		paramMap.put("checkInventory", checkInventory);
		return (Map<String, Object>) getSqlMapClientTemplate().queryForObject("getMasterSkuDetailsBySku", paramMap);
	}

	public int getProductSupplierMapCount() {
		return (Integer) getSqlMapClientTemplate().queryForObject("getProductSupplierMapCount");
	}

	public List<Map<String, Object>> getProductSupplierMap(int limit, int offset) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("limit", limit);
		paramMap.put("offset", offset);
		return (List<Map<String, Object>>) getSqlMapClientTemplate().queryForList("getProductSupplierMap", paramMap);
	}

	public Map<String, Object> getMasterSkuBySku(String sku) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("sku", sku);
		return (Map<String, Object>) getSqlMapClientTemplate().queryForObject("getMasterSkuBySku", paramMap);
	}

	public String getProductFieldValue(String sku, String field) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		// Object[] args = { "field_"+masterField, sku };
		Object[] args = { sku };
		return jdbcTemplate.queryForObject("SELECT " + field + " FROM product where sku = ?", args, String.class);
	}

	// fragrancenet
	public void insertProductCategory(Map<String, List<Long>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "INSERT into category_product (product_id, category_id) select id, ? from product where sku = ?";
		for (String sku : data.keySet()) {
			for (Long categoryId : data.get(sku)) {
				Object[] args = { categoryId, sku };
				try {
					jdbcTemplate.update(query, args);
				} catch (DataIntegrityViolationException e) {
					// do nothing
				}
			}
		}
	}

	public List getShoppingListGroupByUserid(Integer userId) {
		return getSqlMapClientTemplate().queryForList("getShoppingListGroupByUserid", userId);
	}

	public ShoppingListGroup getShoppingListGroupByName(String groupName) {
		return (ShoppingListGroup) getSqlMapClientTemplate().queryForObject("getShoppingListGroupByName", groupName);
	}

	public void insertShoppingGroupList(String groupName, Integer userId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("groupName", groupName);
		getSqlMapClientTemplate().insert("insertShoppingGroupList", paramMap);
	}

	public void addToShoppingGroupList(Set productIds, Integer userId, Integer groupId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "INSERT INTO shopping_list (userid, product_id, shopping_list_group_id, added) VALUES (?, ?, ?, now()) ON DUPLICATE KEY UPDATE shopping_list_group_id = ?";
		Iterator iter = productIds.iterator();
		while (iter.hasNext()) {
			Object[] args = { userId, (Integer) iter.next(), groupId, groupId };
			try {
				jdbcTemplate.update(query, args);
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}

	public void removeMyListGroup(Set ids, Integer userId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "DELETE FROM shopping_list_group WHERE id = ? AND userid = ?";
		Iterator iter = ids.iterator();
		while (iter.hasNext()) {
			Integer groupId = (Integer) iter.next();
			updateMyShopplingList(groupId, userId);
			Object[] args = { groupId, userId };
			try {
				jdbcTemplate.update(query, args);
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}

	private void updateMyShopplingList(Integer groupId, Integer userId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE shopping_list set shopping_list_group_id = 0 where shopping_list_group_id = ? AND userid = ?";
		Object[] args = { groupId, userId };
		jdbcTemplate.update(query, args);
	}

	// Manufacturer

	// Lucene
	public List<Map<String, Object>> getLuceneProduct(int limit, int offset, boolean onlyParent, String parentSku, String[] searchFieldsList, boolean adminIndex) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("limit", limit);
		paramMap.put("offset", offset);
		paramMap.put("onlyParent", onlyParent);
		paramMap.put("parentSku", parentSku);
		paramMap.put("searchFieldsList", searchFieldsList);
		paramMap.put("adminIndex", adminIndex);
		
		return getSqlMapClientTemplate().queryForList("getLuceneProduct", paramMap);
	}

	public int getLuceneProductCount(boolean onlyParent) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("onlyParent", onlyParent);
		return (Integer) getSqlMapClientTemplate().queryForObject("getLuceneProductCount", paramMap);
	}

	// i18n
	public Map<String, Product> getI18nProduct(int id) {
		return getSqlMapClientTemplate().queryForMap("getI18nProduct", id, "lang");
	}

	private void updateI18nProduct(int productId, Collection<Product> products) {
		for (Product product : products) {
			product.setId(productId);
			getSqlMapClientTemplate().update("updateI18nProduct", product);
		}
	}
	
	private void updateI18nApiProduct(int productId, Collection<Product> products) {
		for (Product product : products) {
			product.setId(productId);
			getSqlMapClientTemplate().update("updateI18nApiProduct", product);
		}
	}

	public void insertCategories(List<Map<String, Object>> data) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("data", data);
		getSqlMapClientTemplate().insert("insertCategories", paramMap);
	}

	// Etilize
	public int getEtilizeCount() {
		return (Integer) getSqlMapClientTemplate().queryForObject("getEtilizeCount");
	}

	public List<Product> getEtilizeList(int limit, int offset) {
		Map<String, Integer> paramMap = new HashMap<String, Integer>();
		paramMap.put("limit", limit);
		paramMap.put("offset", offset);
		return getSqlMapClientTemplate().queryForList("getEtilizeProduct", paramMap);
	}

	public List<Map<String, Object>> getEtilizeLuceneProduct(int limit, int offset) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("limit", limit);
		paramMap.put("offset", offset);
		return getSqlMapClientTemplate().queryForList("getEtilizeLuceneProduct", paramMap);
	}

	public int getEtilizeLuceneProductCount() {
		return (Integer) getSqlMapClientTemplate().queryForObject("getEtilizeLuceneProductCount");
	}

	public List<Map<String, Object>> getProductsBySkuList(List<Product> skuList) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("skuList", skuList);
		return getSqlMapClientTemplate().queryForList("getProductsBySkuList", paramMap);
	}

	public void updateFreeShipping(String option_code, String field) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE product SET weight = 0 where TRIM(option_code) = ? and weight != 0";
		Object[] args = { option_code };
		jdbcTemplate.update(query, args);
		if (field.length() > 0) {
			query = "UPDATE product SET field_" + field + " = ? where field_" + field + " = '0'";
			jdbcTemplate.update(query, args);
		}
	}

	public String getProductRedirectUrl(String sku, Integer productId, String protectedAccess, String field) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("sku", sku);
		paramMap.put("productId", productId);
		paramMap.put("protectedAccess", protectedAccess);
		paramMap.put("field", field);
		return (String) getSqlMapClientTemplate().queryForObject("getProductRedirectUrl", paramMap);
	}

	public String getProductAsiXMLById(String sku) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { sku };
		return (String) jdbcTemplate.queryForObject("SELECT asi_xml FROM product WHERE sku = ?", args, String.class);
	}

	public String getProductNoteBySku(String sku) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { sku };
		return (String) jdbcTemplate.queryForObject("SELECT note FROM product WHERE sku = ?", args, String.class);
	}

	public boolean isEndQtyPricing(String sku) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { sku };
		try {
			return (Boolean) jdbcTemplate.queryForObject("SELECT end_qty_pricing FROM product, supplier WHERE asi_id = account_number and sku = ?", args, Boolean.class);
		} catch (Exception e) {
			return false;
		}
	}

	public List<Product> getCustomerShoppingCartProductInfo(Integer userId) {
		return getSqlMapClientTemplate().queryForList("getCustomerShoppingCartProductInfo", userId);
	}

	// product variants
	private void insertProductVariants(int productId, List<Product> variants) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "DELETE FROM product_variant where product_id = ?";
		Object[] args1 = { productId };
		jdbcTemplate.update(query, args1);

		for (Product variant : variants) {
			variant.setId(productId);
			getSqlMapClientTemplate().insert("insertProductVariant", variant);
		}
	}

	// product variant
	public List<Product> getProductVariant(int productId, String sku, boolean backEnd) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("productId", productId);
		paramMap.put("sku", sku);
		paramMap.put("backEnd", backEnd);
		return getSqlMapClientTemplate().queryForList("getProductVariant", paramMap);
	}

	// pre-hanging
	public List<Map<String, Object>> getPreHanging(String doorConfig, String species, String widthHeight, String optionCode) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("doorConfig", doorConfig);
		paramMap.put("species", species);
		paramMap.put("widthHeight", widthHeight);
		paramMap.put("optionCode", optionCode);
		return getSqlMapClientTemplate().queryForList("getPreHanging", paramMap);
	}

	public int[] updatePreHanging(final List<Map<String, Object>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String sql = "UPDATE " + "pre_hanging " + "SET " + "price = ?, " + "dimensions = ? " + "WHERE id = ?";
		return jdbcTemplate.batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map<String, Object> entry = data.get(i);
				if (entry.get("price") == null) {
					ps.setNull(1, Types.NULL);
				} else {
					ps.setDouble(1, (Double) entry.get("price"));
				}
				ps.setString(2, (String) entry.get("dimensions"));
				ps.setInt(3, (Integer) entry.get("id"));
			}

			public int getBatchSize() {
				return data.size();
			}
		});
	}

	public void deletePreHanging(int id) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "DELETE FROM pre_hanging where id = ?";
		Object[] args = { id };
		jdbcTemplate.update(query, args);
	}

	public void insertPreHanging(List<Map<String, Object>> data) {
		for (Map<String, Object> prehang : data) {
			getSqlMapClientTemplate().insert("insertPreHanging", prehang);
		}
	}

	// Power ReviewList
	public int[] updatePowerReview(final List<Map<String, Object>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String sql = "UPDATE product SET last_modified = now(), " + "tab_3_content = ?, " + "tab_6_content = ? " + "WHERE id = ?";
		return jdbcTemplate.batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map<String, Object> entry = data.get(i);
				ps.setString(1, (String) entry.get("tab_3_content"));
				ps.setString(2, (String) entry.get("tab_6_content"));
				ps.setInt(3, (Integer) entry.get("id"));
			}

			public int getBatchSize() {
				return data.size();
			}
		});
	}

	// Dangerous if used improperly
	public void updateProduct(String updateStatement, String whereStatement) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE product SET last_modified = now(), " + updateStatement + " WHERE " + whereStatement;
		jdbcTemplate.update(query);
	}
	
	public void updateParentProduct(String parentSku, Integer parentId){
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE product SET master_sku = '" + parentSku + "' WHERE id = " + parentId;
		jdbcTemplate.update(query);
	}

	// webjaguar datafeed
	public List<Product> getWebjaguarDataFeedProductList(DataFeedSearch search, int numImages, String imagePath, boolean includeCategories) {
		List<Product> productList = getSqlMapClientTemplate().queryForList("getWebjaguarDataFeedProductList", search);
		if (productList != null) {
			for (Product product : productList) {
				if (includeCategories) {
					product.setCatIds(getCategoryIdsByProductId(product.getId()));
				}

				List<ProductImage> images = getImages(product.getId(), numImages);
				for (ProductImage image : images) {
					if (!image.isAbsolute()) {
						image.setImageUrl(imagePath + image.getImageUrl());
					}
				}
				product.setImages(images);
			}
		}
		return productList;
	}

	public ProductLabelTemplate getProductLabelTemplate(Integer id) {
		return (ProductLabelTemplate) getSqlMapClientTemplate().queryForObject("getProductLabelTemplate", id);
	}

	public List<ProductLabelTemplate> getProductLabelTemplates(ProductLabelTemplateSearch search) {
		List<ProductLabelTemplate> list = getSqlMapClientTemplate().queryForList("getProductLabelTemplates", search);
		return list;
	}

	public Product getProductByAsiId(Integer asiId) {
		return (Product) getSqlMapClientTemplate().queryForObject("getProductByAsiId", asiId);
	}
	
	public Map<Integer, String> getProductIdCategoryIdsMap() {
		List<Product> prodCatsList = getSqlMapClientTemplate().queryForList("getProductIdCategoryIdsMap");
		Map<Integer, String> prodCatsMap = new HashMap<Integer, String>();
		
		for(Product product : prodCatsList) {
			prodCatsMap.put(product.getId(), product.getCategoryIds());
		}
		return prodCatsMap;
	}

	public List<ProductFieldUnlimited> getProductFieldsUnlimited() {
		return (List<ProductFieldUnlimited>) getSqlMapClientTemplate().queryForList("getProductFieldsUnlimited");
	}


	public ProductFieldUnlimited getProductFieldsUnlimitedByName(String fieldName) {
		return (ProductFieldUnlimited) getSqlMapClientTemplate().queryForObject("getProductFieldsUnlimitedByName", fieldName);
	}

	public int getProductFieldUnlimitedNameValueCount() {
		return (Integer) getSqlMapClientTemplate().queryForObject("getProductFieldUnlimitedNameValueCount");
	}


	public ProductFieldUnlimitedNameValue getProductFieldUnlimitedNameValue(String sku, int fieldId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("sku", sku);
		paramMap.put("fieldId", fieldId);
		return (ProductFieldUnlimitedNameValue) getSqlMapClientTemplate().queryForObject("getProductFieldUnlimitedNameValue", paramMap);
	}

	public void insertWebjaguarCategory(List<WebjaguarCategory> categoryList) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("categoryList", categoryList);
		getSqlMapClientTemplate().insert("insertWebjaguarCategory", paramMap);
	}
	
	public void updateProductFields(ProductCategory product){
		getSqlMapClientTemplate().update("updateProductFieldsStaging", product);
	}
	
	public void addProductCategory(ProductCategory product) {
		getSqlMapClientTemplate().insert("addProductCategoryStaging", product);
	}
	
	public void removeProductCategory(ProductCategory product) {
		getSqlMapClientTemplate().delete("removeProductCategoryStaging", product);
	}
	
	public String isProductSoftLink(Integer id) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("id", id);
		return (String) getSqlMapClientTemplate().queryForObject("isProductSoftLink", paramMap);
	}
	
	public String isProductSoftLinkSku(String sku) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("sku", sku);
		return (String) getSqlMapClientTemplate().queryForObject("isProductSoftLinkSku", paramMap);
	}
	
}