/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;


import java.util.*;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.GiftCardDao;
import com.webjaguar.model.GiftCard;
import com.webjaguar.model.GiftCardOrder;
import com.webjaguar.model.GiftCardSearch;
import com.webjaguar.model.GiftCardStatus;
import com.webjaguar.model.UserSession;


public class IbatisGiftCardDao extends SqlMapClientDaoSupport implements GiftCardDao {
	
	public int insertGiftCardOrder(GiftCardOrder giftCardOrder, GiftCardStatus giftCardStatus) {
		getSqlMapClientTemplate().insert("insertGitCard", giftCardOrder);
		//GiftCard Status
		giftCardStatus.setGiftCardOrderId(giftCardOrder.getGiftCardOrderId());
		getSqlMapClientTemplate().insert( "insertGiftCardStatusHistory", giftCardStatus );
				
		return giftCardOrder.getGiftCardOrderId();
	}
	
	public List<GiftCard> getGiftCardList(GiftCardSearch search, Integer customerId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "customerId", customerId );
		paramMap.put( "search", search );
		return getSqlMapClientTemplate().queryForList("getGiftCardList", paramMap);
	} 
	
	public GiftCard getGiftCardById( Integer giftCardOrderId) {
		return (GiftCard) getSqlMapClientTemplate().queryForObject("getGiftCardById", giftCardOrderId);
	}
	
	public void insertGiftCardStatus(GiftCardStatus giftCardStatus) {
		getSqlMapClientTemplate().update("insertGiftCardStatusHistory", giftCardStatus);
		getSqlMapClientTemplate().update("updateGiftCardStatus", giftCardStatus); 
	}
	
	public List<GiftCardOrder> getGiftCardOrderList(GiftCardSearch search, Integer customerId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "customerId", customerId );
		paramMap.put( "search", search );
		return getSqlMapClientTemplate().queryForList("getGiftCardOrderList", paramMap);
	}
	
	public GiftCardOrder getGiftCardOrderByGiftCardOrderId(String giftCardOrderId) {
		return (GiftCardOrder) getSqlMapClientTemplate().queryForObject("getGiftCardOrderByGiftCardOrderId", giftCardOrderId);
	}
	
	public void insertGiftCard(GiftCard giftCard) {
		if (giftCard.getPaymentMethod() != null && giftCard.getPaymentMethod().equalsIgnoreCase("paypal")) {
			giftCard.setActive( false );
		} else {
			giftCard.setActive( true );
		}
		getSqlMapClientTemplate().insert( "insertGiftCard", giftCard );
	}
	
	public GiftCard getGiftCardByCode(String code, Boolean active) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "code", code );
		paramMap.put( "active", active );
		return (GiftCard) getSqlMapClientTemplate().queryForObject( "getGiftCardByCode", paramMap );
	}
	
	public void updateGiftCard(GiftCard giftCard) {
		getSqlMapClientTemplate().update( "updateGiftCard", giftCard, 1 );
	}	
	
	public void updateGiftCardByPayPal(GiftCard giftCard) {
		getSqlMapClientTemplate().update( "updateGiftCardByPayPal", giftCard, 1 );
	}
	
	public void updateGiftCardOrderByPayPal(GiftCardOrder giftCardOrder, GiftCardStatus giftCardStatus) {
		updateGiftCardOrderCreditCardPayment(giftCardOrder);
		insertGiftCardStatus(giftCardStatus);
	}
	
	public List<GiftCardStatus> getGiftCardStatusHistory(Integer giftCardOrderId) {
		return  getSqlMapClientTemplate().queryForList("getGiftCardStatusHistory", giftCardOrderId);		
	}
	
	public void redeemGiftCardByCode(String code, UserSession userSession) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "userSession", userSession );
		paramMap.put( "code", code );
		getSqlMapClientTemplate().update( "redeemGiftCardByCode", paramMap, 1 );
	}
	
	public void updateGiftCardOrderCreditCardPayment(GiftCardOrder giftCardOrder) {
		getSqlMapClientTemplate().update( "updateGiftCardOrderCreditCardPayment", giftCardOrder, 1 );
	}
}
