/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.CategoryDao;
import com.webjaguar.model.Category;
import com.webjaguar.model.CategoryLinkType;
import com.webjaguar.model.CategorySearch;

public class IbatisCategoryDao extends SqlMapClientDaoSupport implements CategoryDao
{

	public Category getCategoryById(Integer categoryId, String protectedAccess)
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "categoryId", categoryId );
		paramMap.put( "protectedAccess", protectedAccess );
		return (Category) getSqlMapClientTemplate().queryForObject( "getCategoryById", paramMap );
	}

	public String getCategoryName(Integer categoryId)
	{
		return (String) getSqlMapClientTemplate().queryForObject( "getCategoryNameById", categoryId );
	}
	
	public String getCategoryURL(Integer categoryId)
	{
		return (String) getSqlMapClientTemplate().queryForObject( "getCategoryURLById", categoryId );
	}

	public List<Category> getCategoryLinks(Integer parentId, String protectedAccess, String lang, boolean checkInventory)
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "parentId", parentId );
		paramMap.put( "protectedAccess", protectedAccess );
		paramMap.put( "linkType", CategoryLinkType.HIDDEN );
		paramMap.put( "lang", lang );
		paramMap.put("checkInventory", checkInventory);
		return getSqlMapClientTemplate().queryForList( "getCategoryLinks", paramMap );
	}

	public void deleteCategory(Category category)
	{
		getSqlMapClientTemplate().delete( "deleteCategory", category, 1 );
	}

	public List<Category> getCategoryTree(Integer categoryId, boolean showHidden, String lang)
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		List<Category> categoryTree = new ArrayList<Category>();
		Set<Integer> categoryTreeIds = new HashSet<Integer>();
		Object[] args = { lang, categoryId };
		String query = "SELECT parent, IF(LENGTH(TRIM(i18n_name))>0, i18n_name, name) as name, id, link_type FROM category " +
						"LEFT JOIN category_i18n ON category.id = category_i18n.category_id and lang = ? " +
						"WHERE id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet( query, args );
		while ( results.next() )
		{
			Category node = new Category();
			node.setName( results.getString( "name" ) );
			node.setId( new Integer( results.getInt( "id" ) ) );
			CategoryLinkType linkType = Enum.valueOf( CategoryLinkType.class, results.getString( "link_type" ).toUpperCase() );
			if ( !showHidden )
			{
				if ( linkType == CategoryLinkType.HIDDEN )
				{
					// do not show breadcrumbs
					return null;
				}
			}
			node.setLinkType( linkType );
			if (!categoryTreeIds.add( node.getId() )) 
			{
				// prevent endless loop
				break;
			}
			categoryTree.add( 0, node );
			args[1] = new Integer( results.getInt( "parent" ) );
			if (results.wasNull()) {
				// results.getInt() returns 0 if null
				break;				
			}
			results = jdbcTemplate.queryForRowSet( query, args );
		}
		return categoryTree;
	}

	public Set<Integer> getParentCategories(Integer categoryId)
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Set<Integer> parentCategoryIds = new HashSet<Integer>();
		Object[] args = { categoryId };
		String query = "SELECT parent FROM category WHERE id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet( query, args );
		while ( results.next() )
		{
			if (results.getInt( "parent" ) == 0) {
				// results.getInt() returns 0 if null
				break;				
			}
			if (!parentCategoryIds.add( results.getInt( "parent" ) )) 
			{
				// prevent endless loop
				break;
			}
			args[0] = ( results.getInt( "parent" ) );
			results = jdbcTemplate.queryForRowSet( query, args );
		}
		return parentCategoryIds;
	}

	public void updateRanking(final List<Map <String, Integer>> data)
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String sql = "UPDATE category SET rank = ? WHERE id = ?";
		jdbcTemplate.batchUpdate( sql, new BatchPreparedStatementSetter()
		{
			public void setValues(PreparedStatement ps, int i) throws SQLException
			{
				Map<String, Integer> entry = data.get( i );
				if ( entry.get( "rank" ) == null )
				{
					ps.setNull( 1, Types.NULL );
				}
				else
				{
					ps.setInt( 1, entry.get( "rank" ) );
				}
				ps.setInt( 2, entry.get( "id" ) );
			}

			public int getBatchSize()
			{
				return data.size();
			}
		} );
	}
	
	public void updateCategoryProductRanking(final List<Map <String, Integer>> data)
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String sql = "UPDATE category_product SET rank = ? WHERE product_id = ? AND category_id = ?";
		jdbcTemplate.batchUpdate( sql, new BatchPreparedStatementSetter()
		{
			public void setValues(PreparedStatement ps, int i) throws SQLException
			{
				Map<String, Integer> entry = data.get( i );
				if ( entry.get( "rank" ) == null )
				{
					ps.setNull( 1, Types.NULL );
				}
				else
				{
					ps.setInt( 1, entry.get( "rank" ) );
				}
				ps.setInt( 2, entry.get( "productId" ) );
				ps.setInt( 3, entry.get( "categoryId" ) );
			}

			public int getBatchSize()
			{
				return data.size();
			}
		} );
	}
	
	

	public int insertCategory(Category category)
	{
		if(category.getId() != null) {
			// For feed that contains category id from master site and category does not exist on child sites.
			JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
			Object[] args = { category.getName(), category.getRank(), category.getId() };
			String query = "INSERT INTO category (name, created, rank, id)	VALUES ( ?, now(), ?, ? )";
			jdbcTemplate.update( query, args );
		} else {
			// for new category insert that dont have id on category object
			getSqlMapClientTemplate().insert( "insertCategory", category );
		}
		getSqlMapClientTemplate().update( "updateCategoryById", category, 1 );
		if ( category.isHomePage() )
		{
			getSqlMapClientTemplate().update( "updateHomePageById", category.getId() );
		}
		return category.getId();
	}

	public void updateCategory(Category category)
	{
		getSqlMapClientTemplate().update( "updateCategoryById", category, 1 );
		getSqlMapClientTemplate().update( "updateCategoryLastModified", category.getId(), 1 );
		if ( category.isHomePage() )
		{
			getSqlMapClientTemplate().update( "updateHomePageById", category.getId() );
		}
		else
		{
			getSqlMapClientTemplate().delete( "deleteHomePageById", category.getId() );
		}
	}

	public List<Category> getSiteMapCategoryList(CategorySearch search)
	{
		return getSqlMapClientTemplate().queryForList( "getSiteMapCategoryList", search );
	}

	public List<Category> getCategoryList(CategorySearch search)
	{
		return getSqlMapClientTemplate().queryForList( "getCategoryList", search );
	}

	public void updateStats(Category category)
	{
		getSqlMapClientTemplate().update( "updateCategoryStats", category.getId(), 1 );
	}

	public Integer getHomePageByHost(String host)
	{
		return (Integer) getSqlMapClientTemplate().queryForObject( "getHomePageByHost", host );
	}
	
	public List<Category> getCategoryLinksTree(int cid) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		List<Category> categoryTree = new ArrayList<Category>();
		Set<Integer> categoryTreeIds = new HashSet<Integer>();
		Object[] args = { cid };
		String query = "SELECT parent, id, show_subcats FROM category WHERE id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet( query, args );
		while ( results.next() )
		{
			Category node = new Category();
			node.setId( new Integer( results.getInt( "id" ) ) );
			node.setShowSubcats( results.getBoolean( "show_subcats" ) );
			if (!categoryTreeIds.add( node.getId() )) 
			{
				// prevent endless loop
				break;
			}
			categoryTree.add( 0, node );
			if (node.isShowSubcats()) {
				break;
			}
			args[0] = new Integer( results.getInt( "parent" ) );
			if (results.wasNull()) {
				// results.getInt() returns 0 if null 
				break;				
			}
			results = jdbcTemplate.queryForRowSet( query, args );
		}
		if (!categoryTreeIds.isEmpty()) {
			if (!categoryTree.get( 0 ).isShowSubcats()) {
				Category node = new Category();
				node.setId( null );
				categoryTree.add( 0, node );
			}
		}

		return categoryTree;		
	}
	
	public Map<String, Category> getI18nCategory(int cid, String lang) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("cid", cid);
		paramMap.put("lang", lang);
		return getSqlMapClientTemplate().queryForMap("getI18nCategory", paramMap, "lang");
	}
	
	public void updateI18nCategory(Collection<Category> categories) {
		for (Category category: categories) {
			getSqlMapClientTemplate().update("updateI18nCategory", category);
		}
	}
	
	public List<Category> getCategories(String protectedAccess, boolean showHidden, Boolean showOnSearch, String lang) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("protectedAccess", protectedAccess);
		paramMap.put("showHidden", showHidden);
		paramMap.put("showOnSearch", showOnSearch);
		paramMap.put("lang", lang);
		return getSqlMapClientTemplate().queryForList("getCategories", paramMap);		
	}
	
	public Map<String, Long> getCategoryExternalIdMap() {
		return getSqlMapClientTemplate().queryForMap("getCategoryExternalIdMap", null, "external_id", "id");
	}
	
	// concord
	public Map<String, Map<String, String>> getCategoryExternalIdNewEggMap() {
		return getSqlMapClientTemplate().queryForMap("getCategoryExternalIdNewEggMallMap", null, "external_id");
	}
	
	public List<Category> getCategoryTreeStructure(int catId, String direction) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("categoryId", catId);
		paramMap.put("direction", direction);
		return getSqlMapClientTemplate().queryForList("getCategoryTreeStructure", paramMap);
	}
	
	public String getIndexCatIds(Integer catId) {
		return (String)getSqlMapClientTemplate().queryForObject("getIndexCatIds", catId);
	}
	
}
