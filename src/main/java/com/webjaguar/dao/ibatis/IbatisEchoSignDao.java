/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.03.2010
 */

package com.webjaguar.dao.ibatis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.EchoSignDao;
import com.webjaguar.thirdparty.echosign.EchoSign;
import com.webjaguar.thirdparty.echosign.EchoSignSearch;
import com.webjaguar.thirdparty.echosign.EchoSignWidgetSearch;
import com.webjaguar.thirdparty.echosign.Widget;

public class IbatisEchoSignDao extends SqlMapClientDaoSupport implements EchoSignDao {
	  
	public void insertEchoSign(EchoSign echoSign, boolean insertIgnore) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("echoSign", echoSign);
		paramMap.put("insertIgnore", insertIgnore);		
		getSqlMapClientTemplate().insert("insertEchoSign", paramMap);		
	}
	
	public List<EchoSign> getEchoSignListByUserid(int userId) {
		return getSqlMapClientTemplate().queryForList("getEchoSignListByUserid", userId);
	}
	
	public void updateEchoSign(EchoSign echoSign) {
		getSqlMapClientTemplate().update("updateEchoSign", echoSign);
	}
	
	public EchoSign getEchoSign(String documentKey) {
		return (EchoSign) getSqlMapClientTemplate().queryForObject("getEchoSign", documentKey);
	}
	
	public Map<Integer, Long> getEchoSignSentMap(int year) {
		return getSqlMapClientTemplate().queryForMap("getEchoSignSentMap", year, "month", "count");
	}
	
	public List<EchoSign> getEchoSignBySearch(EchoSignSearch search) {
		return getSqlMapClientTemplate().queryForList("getEchoSignBySearch", search);
	}
	
	public void insertEchoSignWidget(Widget widget) {
		getSqlMapClientTemplate().insert("insertEchoSignWidget", widget);	
	}
	
	public Widget getEchoSignWidgetByPath(String path) {
		return (Widget) getSqlMapClientTemplate().queryForObject("getEchoSignWidgetByPath", path);
	}
	
	public Widget getEchoSignWidget(String documentKey) {
		return (Widget) getSqlMapClientTemplate().queryForObject("getEchoSignWidget", documentKey);
	}
	
	public List<Widget> getEchoSignWidgetBySearch(EchoSignWidgetSearch search) {
		return getSqlMapClientTemplate().queryForList("getEchoSignWidgetBySearch", search);
	}
	
	public Map<Integer, EchoSign> getLatestEchoSignMap() {
		return getSqlMapClientTemplate().queryForMap("getLatestEchoSignMap", null, "userId");
	}
}
