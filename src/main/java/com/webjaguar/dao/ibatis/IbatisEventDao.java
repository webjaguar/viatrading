package com.webjaguar.dao.ibatis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.EventDao;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Event;
import com.webjaguar.model.EventMember;
import com.webjaguar.model.EventSearch;

public class IbatisEventDao extends SqlMapClientDaoSupport implements EventDao {

	public Event getEventById(Integer eventId) {
		return (Event) getSqlMapClientTemplate().queryForObject( "getEventById", eventId );
	}
	public List<Event> getEventList(EventSearch search) {
		return getSqlMapClientTemplate().queryForList( "getEventList", search );
	}
	public void deleteEvent(Integer eventId) {
		getSqlMapClientTemplate().delete("deleteEvent", eventId);	
	}
	public void insertEvent(Event event) {
		getSqlMapClientTemplate().insert("insertEvent", event);	
	}
	public void updateEvent(Event event) {
		getSqlMapClientTemplate().update("updateEvent", event);
	}
	public int getEventMemberListCount(EventSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getEventMemberListCount", search);
	}
	public int getAllEventMemberListCount(EventSearch search){
		return (Integer) getSqlMapClientTemplate().queryForObject("getAllEventMemberListCount", search);
	}
	public List<EventMember> getEventMemberList(EventSearch search) {
		List<EventMember> eventMembers = getSqlMapClientTemplate().queryForList( "getEventMemberList", search );
		Map<Integer, Customer> customerMap = getSqlMapClientTemplate().queryForMap("getCustomerOrderReport", null, "id");
		for (EventMember member : eventMembers ) {
			if (customerMap.containsKey(member.getUserId())) {
				member.setOrdersGrandTotal( customerMap.get(member.getUserId()).getOrdersGrandTotal() );
				member.setOrderCount( customerMap.get(member.getUserId()).getOrderCount() );
				member.setLastOrderDate(customerMap.get(member.getUserId()).getLastOrderDate());
			}
			member.setNumOf300PalRegister((Integer)getSqlMapClientTemplate().queryForObject( "getNumOf300PalRegister", member.getUserId()));
			member.setNumOfAuctionRegister((Integer)getSqlMapClientTemplate().queryForObject( "getNumOfAuctionRegister", member.getUserId()));
			member.setNumOf300PalOrders((Integer)getSqlMapClientTemplate().queryForObject( "getNumOf300PalOrders", member.getUserId()));
			member.setNumOfAuctionOrders((Integer)getSqlMapClientTemplate().queryForObject( "getNumOfAuctionOrders", member.getUserId()));
			Integer salesof300Pal = (Integer)getSqlMapClientTemplate().queryForObject( "getSales300Pal", member.getUserId());
			member.setSales300Pal(salesof300Pal == null? 0 : salesof300Pal);
			Integer salesAuction = (Integer)getSqlMapClientTemplate().queryForObject( "getSalesAuction", member.getUserId());
			member.setSalesAuction(salesAuction == null? 0 : salesAuction);
		}
		return eventMembers;
	}
	
	public List<EventMember> getAllEventMemberList(EventSearch search){
		List<EventMember> eventMembers = getSqlMapClientTemplate().queryForList( "getAllEventMemberList", search );
		Map<Integer, Customer> customerMap = getSqlMapClientTemplate().queryForMap("getCustomerOrderReport", null, "id");
		for (EventMember member : eventMembers ) {
			if (customerMap.containsKey(member.getUserId())) {
				member.setOrdersGrandTotal( customerMap.get(member.getUserId()).getOrdersGrandTotal() );
				member.setOrderCount( customerMap.get(member.getUserId()).getOrderCount() );
				member.setLastOrderDate(customerMap.get(member.getUserId()).getLastOrderDate());
			}
			member.setNumOf300PalRegister((Integer)getSqlMapClientTemplate().queryForObject( "getNumOf300PalRegister", member.getUserId()));
			member.setNumOfAuctionRegister((Integer)getSqlMapClientTemplate().queryForObject( "getNumOfAuctionRegister", member.getUserId()));
			member.setNumOf300PalOrders((Integer)getSqlMapClientTemplate().queryForObject( "getNumOf300PalOrders", member.getUserId()));
			member.setNumOfAuctionOrders((Integer)getSqlMapClientTemplate().queryForObject( "getNumOfAuctionOrders", member.getUserId()));
			Integer salesof300Pal = (Integer)getSqlMapClientTemplate().queryForObject( "getSales300Pal", member.getUserId());
			member.setSales300Pal(salesof300Pal == null? 0 : salesof300Pal);
			Integer salesAuction = (Integer)getSqlMapClientTemplate().queryForObject( "getSalesAuction", member.getUserId());
			member.setSalesAuction(salesAuction == null? 0 : salesAuction);
		}
		return eventMembers;
	}
	
	public void insertEventMember(EventMember event) throws DataAccessException {
		getSqlMapClientTemplate().insert("insertEventMember", event);	
		EventMember member = getEventMemberByEventIdUserId(event.getEventId(), event.getUserId());
		event.setUserIndex(member.getUserIndex());
	}
	public EventMember getEventMemberByEventIdUserId(Integer eventId, Integer userId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "eventId", eventId );
		paramMap.put( "userId", userId );
		return (EventMember) getSqlMapClientTemplate().queryForObject( "getEventMemberByEventIdUserId", paramMap );
	}
	public void updateEventMember(EventMember eventMember) throws DataAccessException {
		getSqlMapClientTemplate().update("updateEventMember", eventMember, 1);
	}
	public void deleteEventMemberById(Integer eventId, Integer userId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "eventId", eventId );
		paramMap.put( "userId", userId );
		getSqlMapClientTemplate().delete("deleteEventMemberById", paramMap);	
	}
}
