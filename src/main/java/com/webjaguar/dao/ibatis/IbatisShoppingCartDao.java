/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.io.StringReader;
import java.util.*;

import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.ShoppingCartDao;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Price;
import com.webjaguar.model.ProductAttribute;

public class IbatisShoppingCartDao extends SqlMapClientDaoSupport implements ShoppingCartDao {

	public void addToCart(CartItem cartItem) {
		cartItem.setIndex(getLastCartIndex(cartItem.getUserId()));
		getSqlMapClientTemplate().update("insertCartItem", cartItem, 1);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("cartItem", cartItem);
		// save attributes
		if (cartItem.getProductAttributes() != null) {
			for (ProductAttribute attribute:cartItem.getProductAttributes()) {
				paramMap.put("attr", attribute);
				getSqlMapClientTemplate().update("insertCartItemAttributes", paramMap);
			}
		}
		if (cartItem.getAsiProductAttributes() != null) {
			for (ProductAttribute attribute:cartItem.getAsiProductAttributes()) {
				paramMap.put("asiAttr", attribute);
				getSqlMapClientTemplate().update("insertCartItemAsiAttributes", paramMap);
			}
		}
		// save custom lines
		if (cartItem.getCustomLines() != null) {
			insertCustomLines(cartItem);
		}
	}
	
	public Cart getCart(int userId, String manufacturerName) {
		Cart cart = new Cart();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("manufacturerName", manufacturerName);
		Iterator<CartItem> iter = getSqlMapClientTemplate().queryForList("getCartItemsByUserid", paramMap).iterator();
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		while (iter.hasNext()) {
			CartItem cartItem = iter.next();
			Iterator<ProductAttribute> productAttrIter = getSqlMapClientTemplate().queryForList("getCartItemAttributes", cartItem).iterator();
			while (productAttrIter.hasNext()) {
				cartItem.addProductAttribute(productAttrIter.next());
			}
			Iterator<ProductAttribute> asiProductAttrIter = getSqlMapClientTemplate().queryForList("getCartItemAsiAttributes", cartItem).iterator();
			while (asiProductAttrIter.hasNext()) {
				cartItem.addAsiProductAttribute(asiProductAttrIter.next());
			}
			// custom lines
			Object[] args = {userId, cartItem.getProduct().getId(), cartItem.getIndex()};
			String query = "SELECT custom_line, quantity FROM shopping_cart_customlines WHERE userid = ? and product_id = ? and cart_index = ?";
			SqlRowSet results = jdbcTemplate.queryForRowSet( query, args );
			while (results.next()) {
				cartItem.addCustomLine(results.getString("custom_line"), results.getInt("quantity"));
			}
			
			// custom frame
			if (cartItem.getCustomXml() != null) {
				try {
					SAXBuilder builder = new SAXBuilder(false);
					
					Document doc = builder.build(new StringReader(cartItem.getCustomXml()));
					
					// name
					cartItem.getProduct().setName(doc.getRootElement().getChild("productName").getValue());
					
					// price
					List<Price> prices = new ArrayList<Price>();
					prices.add(new Price(Double.parseDouble(doc.getRootElement().getChild("totalPrice").getValue()), null, null, null, null, null));
					cartItem.getProduct().setPrice(prices);					
				} catch (Exception e) {
					// do nothing
				}
			}
			cart.addCartItem(cartItem);
		}
		
		return cart;
	}
	
	public void updateCartItem(CartItem cartItem) {
		getSqlMapClientTemplate().update("updateCartItem", cartItem);
		// save custom lines
		if (cartItem.getCustomLines() != null) {
			// delete old custom lines
			getSqlMapClientTemplate().delete("deleteCartItemCustomLines", cartItem);
			// insert new custom lines
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
			String sql = "SELECT product_id FROM shopping_cart WHERE userid = ? and cart_index = ?";
			Object[] args = {cartItem.getUserId(), cartItem.getIndex()};
			cartItem.getProduct().setId(jdbcTemplate.queryForInt(sql,args));
			insertCustomLines(cartItem);
		}
	}
	public void updateCartItemByApiIndex(CartItem cartItem, Integer productId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("cartItem", cartItem);
		paramMap.put("productId", productId);
		getSqlMapClientTemplate().update("updateCartItemByApiIndex",paramMap );
		// save custom lines
		if (cartItem.getCustomLines() != null) {
			// delete old custom lines
			getSqlMapClientTemplate().delete("deleteCartItemCustomLines", cartItem);
			// insert new custom lines
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
			String sql = "SELECT product_id FROM shopping_cart WHERE userid = ? and cart_index = ?";
			Object[] args = {cartItem.getUserId(), cartItem.getIndex()};
			cartItem.getProduct().setId(jdbcTemplate.queryForInt(sql,args));
			insertCustomLines(cartItem);
		}
	}
	public void incrementQuantityByCartItem(CartItem cartItem) {
		getSqlMapClientTemplate().update("incrementQuantityByCartItem", cartItem);
		
		// save custom lines
		if (cartItem.getCustomLines() != null) {
			insertCustomLines(cartItem);
		}
	}
	
	public void removeCartItem(CartItem cartItem) {
		getSqlMapClientTemplate().delete("deleteCartItem", cartItem);
		getSqlMapClientTemplate().delete("deleteCartItemAttributes", cartItem);
		getSqlMapClientTemplate().delete("deleteCartItemCustomLines", cartItem);
		getSqlMapClientTemplate().delete("deleteCartItemAsiAttributes", cartItem);
	}
	
	public void removeCartItembyApiIndex(String apiIndex, Integer userId , Integer productId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("apiIndex", apiIndex);
		paramMap.put("productId", productId);
	
		getSqlMapClientTemplate().delete("deleteCartItembyApiIndex", paramMap);
		getSqlMapClientTemplate().delete("deleteCartItemAttributesbyApiIndex", paramMap);

	}

	
	private int getLastCartIndex(int userId) {
		Integer last_index = (Integer) getSqlMapClientTemplate().queryForObject("getLastCartIndexByUserid", userId);
		if (last_index != null) {
			return (last_index + 1);
		} else {
			return 1;
		}
	}
	
	private void insertCustomLines(CartItem cartItem) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("cartItem", cartItem);
		for (String customLine: cartItem.getCustomLines().keySet()) {
			paramMap.put("key", customLine);
			paramMap.put("value", cartItem.getCustomLines().get(customLine));	    	  
			getSqlMapClientTemplate().insert("insertCartItemCustomLine", paramMap);			  
		}
	}
	
	public String getCustomImageUrl(CartItem cartItem) {
		return (String) getSqlMapClientTemplate().queryForObject("getCustomImageUrl", cartItem);
	}
	
	public void deleteShoppingCart(int userid, String manufacturerName) {
		// clear shopping cart items from database
		if (manufacturerName != null && manufacturerName.trim().length() > 0) {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("userId", userid);
			paramMap.put("manufacturerName", manufacturerName);
			List<CartItem> cartItems = getSqlMapClientTemplate().queryForList("getCartItemsByUserid", paramMap);
			for (CartItem cartItem: cartItems) {
				// remove only items belonging to manufacturer
				removeCartItem(cartItem);
			}
			return;
		}
		getSqlMapClientTemplate().delete("deleteCartItemsByUserid", userid);
		getSqlMapClientTemplate().delete("deleteCartItemAttributesByUserid", userid);			
		getSqlMapClientTemplate().delete("deleteCartItemCustomLinesByUserid", userid);			
		getSqlMapClientTemplate().delete("deleteCartItemAsiAttributesByUserid", userid);			
	}
}
