/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.PaymentDao;
import com.webjaguar.model.Order;
import com.webjaguar.model.Payment;
import com.webjaguar.model.PaymentSearch;

public class IbatisPaymentDao extends SqlMapClientDaoSupport implements PaymentDao {
	  
	  public List<Payment> getCustomerPaymentList(PaymentSearch search) {
		  return getSqlMapClientTemplate().queryForList( "getCustomerPaymentList", search );
	  }
	  
	  public List<Payment> getPaymentExportList(PaymentSearch search) {
		  List<Payment> payments = getSqlMapClientTemplate().queryForList( "getPaymentExportList", search );
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  
		  for (Payment payment : payments) {
			  paramMap.put( "paymentId", payment.getId() );
			  payment.setLineItems( getSqlMapClientTemplate().queryForList("getLineItemsByPaymentIdExport", paramMap));
		  }
		  return payments;
	  }
	  
	  public void updatePaymentExported (Set<Integer> paymentIds) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "paymentIds", paymentIds.toArray() );
		  getSqlMapClientTemplate().update( "updatePaymentExported", paramMap );
	  }
	  
	  public Payment getCustomerPaymentById(int id) {
		  return (Payment) getSqlMapClientTemplate().queryForObject("getCustomerPaymentById", id);
	  }
	  
	  public void updateCustomerPayment(Payment payment, Collection<Order> invoices) {
		  getSqlMapClientTemplate().update("updateCustomerPayment", payment, 1);
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put("payment_id", payment.getId() );
		  paramMap.put("last_modified", new Timestamp( new Date().getTime() ) );
		  
		  for (Order order: invoices) {	
			  paramMap.put("order_id", order.getOrderId() );
			  
			  // if payment is cancelled set the values
			  if(order.isCancelledPayment()) {
				  paramMap.put("cancel_payment", true );
				  paramMap.put("cancelled_amount", order.getPaymentCancelledAmt());
				  paramMap.put("cancelled_by", order.getPaymentCancelledBy());
			  }else {
				  paramMap.put("cancel_payment", false );
				  paramMap.put("cancelled_amount", null);
				  paramMap.put("cancelled_by", null);
			  }
			  			  
			  if (order.getPayment() == null ) {
				getSqlMapClientTemplate().delete( "deleteOrderPayment", paramMap );
			  } else {
				  paramMap.put("payment_amount", order.getPayment() );
				  getSqlMapClientTemplate().update( "updateOrderPayment", paramMap );
			  }
			  if(order.getCreditUsed() != null) {
				  paramMap.put("creditUsed", order.getCreditUsed()); 
			  } else {
				  paramMap.put("creditUsed", null); 
			  }
			 getSqlMapClientTemplate().update("updateOrderAmountPaid", paramMap);
		  }
	  }
	  
	  public void insertCustomerPayment(Payment payment, Collection<Order> invoices) {
		  getSqlMapClientTemplate().insert("insertCustomerPayment", payment);
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "payment_id", payment.getId() );
		  paramMap.put( "last_modified", new Timestamp( new Date().getTime() ) );
		  for (Order order: invoices) {
			  paramMap.put( "cancel_payment", order.isCancelledPayment()  ); 
			  paramMap.put("cancelled_amount", order.getPaymentCancelledAmt());
			  paramMap.put("cancelled_by", order.getPaymentCancelledBy());
			  paramMap.put( "order_id", order.getOrderId() );
			  if (order.getPayment() != null && order.getPayment() != 0) {
				  paramMap.put( "payment_amount", order.getPayment() );
				  getSqlMapClientTemplate().update( "updateOrderPayment", paramMap );
			  }
			  if(order.getCreditUsed() != null) {
				  paramMap.put("creditUsed", order.getCreditUsed()); 
			  } else {
				  paramMap.put("creditUsed", null); 
			  }
			  getSqlMapClientTemplate().update("updateOrderAmountPaid", paramMap);
		  }
	  }
	  
	  public void insertOrderPayment(Payment payment, Collection<Order> invoices) {
		  getSqlMapClientTemplate().insert("insertCustomerPayment", payment);
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "payment_id", payment.getId() );
		  paramMap.put( "last_modified", new Timestamp( new Date().getTime() ) );
		  for (Order order: invoices) {
			  payment.setOrderId(order.getOrderId());
			  getSqlMapClientTemplate().insert("insertOrderPayment", payment);
		  }
	  }
	  
	  public void insertPayments(Payment payment) {
		  getSqlMapClientTemplate().insert("insertPayments", payment);
	  }
	  
	  public void insertCustomerPayment(Payment payment) {
		  getSqlMapClientTemplate().insert("insertCustomerPayment", payment);
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "payment_id", payment.getId() );
		  paramMap.put( "last_modified", new Timestamp( new Date().getTime()));
		  for (Payment lineItem: payment.getLineItems()) {
			  if (lineItem.getAmount() != null && lineItem.getOrderId() != null) {
				  paramMap.put( "order_id", lineItem.getOrderId());
				  paramMap.put( "payment_amount", lineItem.getAmount());
				  paramMap.put( "cancel_payment", false  ); 
				  getSqlMapClientTemplate().update( "updateOrderPayment", paramMap );
			  }
			  getSqlMapClientTemplate().update("updateOrderAmountPaid", paramMap);
		  }
	  }
	  
	  public Map<Integer, Order> getInvoices(PaymentSearch search, String searchType) {
		  if (searchType.equals( "open" )) {
			  return getSqlMapClientTemplate().queryForMap( "getOpenInvoices", search, "orderId" );
		  } else {
			  return getSqlMapClientTemplate().queryForMap( "getPaidInvoices", search, "orderId" );			  
		  }
	  }
	  
	  public Integer getFirstPaymentId(Integer order_id) {
		  JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		  Object[] args = { order_id };		 
		  return jdbcTemplate.queryForInt("SELECT MIN(payment_id) FROM payments_orders where order_id= ?", args);		  
	  }
	  
	  public Double getPaymentAmount(Integer paymentId, Integer orderId) {
		  JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		  Object[] args = { paymentId, orderId };	
		  return jdbcTemplate.queryForObject("SELECT payment_amount FROM payments_orders WHERE payment_id =? AND order_id = ?", args, Double.class);
	  }
	  
	  public List<Payment> getCustomerPaymentListByOrder(PaymentSearch search){
		  return getSqlMapClientTemplate().queryForList( "getCustomerPaymentListByOrder", search.getOrderId() );		  
	  }
	  
	  public Integer checkPreviousPaid(Integer paymentId, Integer orderId) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put("paymentId", paymentId);
		  paramMap.put("orderId", orderId);
		  return(Integer) getSqlMapClientTemplate().queryForObject("checkPreviousPaid",paramMap);
	  }
}
