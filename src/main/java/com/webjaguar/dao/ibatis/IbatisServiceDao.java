/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.21.2007
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.ServiceDao;
import com.webjaguar.model.Order;
import com.webjaguar.model.ServiceWork;
import com.webjaguar.model.ServiceSearch;
import com.webjaguar.model.ServiceableItem;
import com.webjaguar.model.ServiceableItemSearch;
import com.webjaguar.model.WorkOrder;
import com.webjaguar.model.WorkOrderLineItem;

public class IbatisServiceDao extends SqlMapClientDaoSupport implements ServiceDao {
	  
	public 	List<ServiceableItem> getServiceableItemList(ServiceableItemSearch search) {
		return getSqlMapClientTemplate().queryForList("getServiceableItemList", search);
	}

	public ServiceableItem getServiceableItemById(int id) {
		return (ServiceableItem) getSqlMapClientTemplate().queryForObject("getServiceableItemById", id);		
	}
	
	public void updateServiceableItem(ServiceableItem item) {
		getSqlMapClientTemplate().update("updateServiceableItem", item, 1);
	}
	
	public void insertServiceableItem(ServiceableItem item) {
		getSqlMapClientTemplate().insert("insertServiceableItem", item);
		item.setId( getServiceableItemId(null, item.getSerialNum()) );
	}
	
	public void deleteServiceableItem(ServiceableItem item) throws DataAccessException {
		getSqlMapClientTemplate().delete("deleteServiceableItem", item, 1);
	}
	
	public List<ServiceWork> getServiceList(ServiceSearch search) {
		List<ServiceWork> services = getSqlMapClientTemplate().queryForList("getServiceList", search);
		Map<Integer, Order> orderMap = getSqlMapClientTemplate().queryForMap("getServiceOrders", null, "workOrderNum");
		// get order
		for (ServiceWork service: services) {
			if (orderMap.containsKey(service.getServiceNum())) {
				service.setOrderId(orderMap.get(service.getServiceNum()).getOrderId());
				service.setPdfUrl(orderMap.get(service.getServiceNum()).getPdfUrl());
			}
		}
		return services;
	}
	
	public ServiceWork getService(int serviceNum) {
		return (ServiceWork) getSqlMapClientTemplate().queryForObject("getService", serviceNum);
	}

	public void updateService(ServiceWork service) {
		getSqlMapClientTemplate().update("updateService", service, 1);
		getSqlMapClientTemplate().update( "updateServiceLastModified", service, 1 );
	}
	
	public Integer getServiceableItemId(String itemId, String serialNum) {
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put( "itemId", itemId );
		paramMap.put( "serialNum", serialNum );
		return (Integer) getSqlMapClientTemplate().queryForObject("getServiceableItemId", paramMap);		
	}
	
	public void insertService(ServiceWork service) {
		if (service.getItem().getId() == null) {
			insertServiceableItem(service.getItem());
		}
		getSqlMapClientTemplate().insert("insertService", service);
	}
	
	public WorkOrder getWorkOrder(int serviceNum) {
		WorkOrder workOrder = (WorkOrder) getSqlMapClientTemplate().queryForObject("getWorkOrder", serviceNum);
		if (workOrder != null) {
			workOrder.setService((ServiceWork) getSqlMapClientTemplate().queryForObject("getService", serviceNum));
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put( "serviceNum", serviceNum);
			workOrder.setLineItems( getSqlMapClientTemplate().queryForList( "getLineItemsByServiceNum", paramMap ) );
			
			return workOrder;
		} else {
			return null;
		}		
	}
	
	public void updateWorkOrder(WorkOrder workOrder) {
		if (workOrder.isNewWorkOrder()) {
			getSqlMapClientTemplate().insert("insertWorkOrder", workOrder);
		} else {
			getSqlMapClientTemplate().update("updateWorkOrderLastModified", workOrder, 1);			
		}
		getSqlMapClientTemplate().update("updateWorkOrder", workOrder, 2);		
		
		if (workOrder.getDeletedLineItems() != null) {
			for (Integer lineNumber: workOrder.getDeletedLineItems()) {
				WorkOrderLineItem lineItem = new WorkOrderLineItem();
				lineItem.setServiceNum( workOrder.getService().getServiceNum() );
				lineItem.setLineNumber( lineNumber );
				getSqlMapClientTemplate().delete( "deleteWorkOrderLineItem", lineItem );
			}
		}
		
		if (workOrder.getInsertedLineItems() != null) {
			// get last line num
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
			Object[] args = { workOrder.getService().getServiceNum() };
			int lastLineNum = 0;
			try {
				lastLineNum = jdbcTemplate.queryForInt( "SELECT line_num FROM work_order_lineitems WHERE service_num = ? ORDER BY line_num DESC LIMIT 1", args );
			} catch (EmptyResultDataAccessException e) {
				// do nothing
			}
			for (WorkOrderLineItem lineItem: workOrder.getInsertedLineItems()) {
				lineItem.setLineNumber( ++lastLineNum );
			}
			// insert line items
			insertLineItems(workOrder, workOrder.getInsertedLineItems());
		}
		updateLineItems(workOrder);
	}
	
	private void insertLineItems(WorkOrder workOrder, List<WorkOrderLineItem> lineItems) {
		for (WorkOrderLineItem lineItem : lineItems) {
			lineItem.setServiceNum(workOrder.getService().getServiceNum());
			getSqlMapClientTemplate().insert("insertWorkOrderLineItem", lineItem);
		}		
	}
	
	private void updateLineItems(final WorkOrder workOrder) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String sql = "UPDATE work_order_lineitems SET quantity = ?, notes = ?, in_sn = ?, out_sn = ? WHERE service_num = ? and line_num = ?";
		jdbcTemplate.batchUpdate( sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				WorkOrderLineItem lineItem = (WorkOrderLineItem) workOrder.getLineItems().get(i);
				ps.setInt(1, (Integer) lineItem.getQuantity());
				ps.setString(2, lineItem.getNotes());
				ps.setString(3, lineItem.getInSN());
				ps.setString(4, lineItem.getOutSN());
				ps.setInt(5, (Integer) workOrder.getService().getServiceNum());
				ps.setInt(6, (Integer) lineItem.getLineNumber());
			}

			public int getBatchSize() {
				return workOrder.getLineItems().size();
			}
		} );
	}
	
	public ServiceWork getLastService(ServiceableItem item) {
		return (ServiceWork) getSqlMapClientTemplate().queryForObject("getLastService", item);
	}
	
	public List<WorkOrder> getServiceHistory(ServiceableItem item) {
		List<WorkOrder> workOrders = getSqlMapClientTemplate().queryForList("getServiceHistory", item);
		int index = 0;
		for (WorkOrder workOrder: workOrders) {
			workOrders.set(index++, getWorkOrder(workOrder.getService().getServiceNum()));
		}
		return workOrders;
	}
	
	public int getServiceCount(ServiceSearch search, String column) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("search", search);
		paramMap.put("column", column);		
		return (Integer) getSqlMapClientTemplate().queryForObject("getServiceCount", paramMap);
	}
	
	public int updateServiceableItems(final List<ServiceableItem> items) {
		int count = 0;
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String sql = "UPDATE serviceable_item SET item_active = ? WHERE id = ?";
		int[] success = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ServiceableItem item = items.get(i);
				ps.setBoolean(1, item.isActive());
				ps.setInt(2, item.getId());
			}

			public int getBatchSize() {
				return items.size();
			}
		} );
		for (int i=0; i<success.length; i++) {
			if (success[i] == 1) count++;
		}
		
		return count;
	}
	
	public void updateWorkOrderPrinted(int serviceNum) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE work_order SET work_order_printed = true where service_num = ?";
		Object[] args = { serviceNum };
		jdbcTemplate.update(query, args);
	}
	
	public void updatePdfService(ServiceWork service, String type) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE service SET service_pdf_url = ? WHERE service_num = ?";
		Object[] args = { service.getServicePdfUrl(),  service.getServiceNum() };		
		if (type.equals("po")) {
			query = "UPDATE service SET purchase_order_pdf_url = ? WHERE service_num = ?";
			args[0] = service.getPurchaseOrderPdfUrl();
		} else if (type.equals("wo3rd")) {
			query = "UPDATE service SET wo_3rd_pdf_url = ? WHERE service_num = ?";
			args[0] = service.getWo3rdPdfUrl();			
		}
		jdbcTemplate.update(query, args);
	}
	
	public Map<Integer, ServiceWork> getServiceMap() {
		return getSqlMapClientTemplate().queryForMap("getServiceMap", null, "serviceNum");
	}
	
	public List<WorkOrder> getIncompleteWorkOrders(String customerField, int defaultInterval) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerField", customerField);
		paramMap.put("defaultInterval", defaultInterval);
		return getSqlMapClientTemplate().queryForList("getIncompleteWorkOrders", paramMap);
	}
}
