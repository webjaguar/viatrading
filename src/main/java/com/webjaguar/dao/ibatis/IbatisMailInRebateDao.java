package com.webjaguar.dao.ibatis;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.MailInRebateDao;
import com.webjaguar.model.MailInRebate;
import com.webjaguar.model.MailInRebateSearch;

public class IbatisMailInRebateDao extends SqlMapClientDaoSupport implements MailInRebateDao{

	public void deleteMailInRebateById(Integer mailInRebateId) {
		getSqlMapClientTemplate().delete("deleteMailInRebateById", mailInRebateId);
	}

	public List<MailInRebate> getMailInRebateList( MailInRebateSearch search ) {
		return getSqlMapClientTemplate().queryForList("getMailInRebateList", search);
	}

	public MailInRebate getMailInRebateById(Integer rebateId) {
		return (MailInRebate) getSqlMapClientTemplate( ).queryForObject( "getMailInRebateById", rebateId );
	}

	public MailInRebate getMailInRebateByName(String name) {
		return (MailInRebate) getSqlMapClientTemplate( ).queryForObject( "getMailInRebateByName", name );
	}

	public MailInRebate getMailInRebateBySku(String sku) {
		return (MailInRebate) getSqlMapClientTemplate( ).queryForObject( "getMailInRebateBySku", sku );
	}

	public void insertMailInRebate(MailInRebate mailInRebate) {
		getSqlMapClientTemplate().insert("insertMailInRebate", mailInRebate);
		
	}

	public void updateMailInRebate(MailInRebate mailInRebate) {
		getSqlMapClientTemplate().update("updateMailInRebate", mailInRebate);
		
	}

}
