package com.webjaguar.dao.ibatis;

import java.util.List;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.webjaguar.dao.ProductImportDao;
import com.webjaguar.model.Customer;
import com.webjaguar.model.ProductCategory;


public class IbatisProductImportDao extends SqlMapClientDaoSupport implements ProductImportDao{
	
	public List<ProductCategory> getUpdateProductFields(){
		return getSqlMapClientTemplate().queryForList("getUpdateProductFields");
	}
	
	public List<ProductCategory> getAddProductCategory(){
		return getSqlMapClientTemplate().queryForList("getAddProductCategory");
	}
	
	public List<ProductCategory> getRemoveProductCategory(){
		return getSqlMapClientTemplate().queryForList("getRemoveProductCategory");
	}
	
	public void deleteAllUpdateProductFields(){
		getSqlMapClientTemplate().delete("deleteAllUpdateProductFields");
	}
	
	public void deleteAllAddProductCategory(){
		getSqlMapClientTemplate().delete("deleteAllAddProductCategory");
	}
	
	public void deleteAllRemoveProductCategory(){
		getSqlMapClientTemplate().delete("deleteAllRemoveProductCategory");
	}
	
	public List<Customer> getUserNotifications(){
		return getSqlMapClientTemplate().queryForList("getUserNotifications");
	}
	
	public void removeUserNotifications(){
		getSqlMapClientTemplate().delete("removeUserNotifications");
	}

}
