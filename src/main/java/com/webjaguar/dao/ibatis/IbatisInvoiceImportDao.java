package com.webjaguar.dao.ibatis;

import java.util.List;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.webjaguar.dao.ImportInvoiceDao;
import com.webjaguar.model.InvoiceImport;
import com.webjaguar.model.InvoiceImportLineItem;
import com.webjaguar.model.OrderQuote;
import com.webjaguar.model.PaymentImport;

public class IbatisInvoiceImportDao extends SqlMapClientDaoSupport implements ImportInvoiceDao{
	
	public List<InvoiceImport> getAllOrders(){
		return getSqlMapClientTemplate().queryForList("getAllOrders");
	}
	
	public List<InvoiceImportLineItem> getLineItems(Integer id){
		return getSqlMapClientTemplate().queryForList("getLineItems", id);
	}
	
	public List<PaymentImport> getOrderPayments(Integer id){
		return getSqlMapClientTemplate().queryForList("getOrderPayments", id);
	}

	public List<PaymentImport> getPayments(){
		return getSqlMapClientTemplate().queryForList("getPayments");
	}
	
	public void deleteAllOrders(){
		getSqlMapClientTemplate().delete("deleteAllOrders");
	}
	
	public void  deleteAllOrdersLineItems(){
		getSqlMapClientTemplate().delete("deleteAllOrdersLineItems");
	}
	
	public void deleteAllPayments(){
		getSqlMapClientTemplate().delete("deleteAllPayments");
		getSqlMapClientTemplate().delete("deleteAllPaymentsOrders");
	}
	
	public void deleteOrder(Integer id){
		getSqlMapClientTemplate().delete("deleteOrder", id);
	}
	
	public void  deleteOrderLineItems(Integer id){
		getSqlMapClientTemplate().delete("deleteOrderLineItems", id);
	}
	
	public void deletePayments(Integer id){
		getSqlMapClientTemplate().delete("deletePayments", id);
		getSqlMapClientTemplate().delete("deletePaymentOrders", id);
	}
	
	public List<OrderQuote> getOrderQuoteFromStaging(){
		return getSqlMapClientTemplate().queryForList("getOrderQuoteFromStaging");
	}
	
	public void  deleteAllOrderQuoteFromStaging(){
		getSqlMapClientTemplate().delete("deleteAllOrderQuoteFromStaging");
	}

}
