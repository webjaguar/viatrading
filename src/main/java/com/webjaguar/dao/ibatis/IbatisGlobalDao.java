/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.24.2006
 */

package com.webjaguar.dao.ibatis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.City;
import com.webjaguar.model.County;
import com.webjaguar.model.LocationSearch;
import com.webjaguar.model.ZipCode;

public class IbatisGlobalDao extends SqlMapClientDaoSupport implements GlobalDao {
	
	private String siteId;
	public void setSiteId(String siteId) { this.siteId = siteId; }

    public void updateGlobalSiteConfig(Map<String, Object> data) {
    	Map<String, Object> gSiteConfig = getGlobalSiteConfig();
    	gSiteConfig.putAll(data);
    	getSqlMapClientTemplate().update("updateGlobalSiteConfig", gSiteConfig);
	}   
    
	public Map<String, Object> getGlobalSiteConfig() {
    	JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
    	Object[] args = { siteId };
    	return jdbcTemplate.queryForMap("SELECT * FROM aem_site_config WHERE site_id = ?", args);
	} 
	
	public AccessUser getAdminInfoByUserName(String username) {
		AccessUser adminUser = (AccessUser) getSqlMapClientTemplate().queryForObject("getAdminInfoByUserName", username);
		if (adminUser != null)
			adminUser.setRoles( getSqlMapClientTemplate().queryForList( "getAdminPrivilegeById", adminUser.getId() ) );
		return adminUser;
	}
	
	public AccessUser getAdminInfoByUsernamePassword(String username, String password) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "username", username );
		paramMap.put( "password", password );
		AccessUser adminUser = (AccessUser) getSqlMapClientTemplate().queryForObject("getAdminInfoByUsernamePassword", paramMap);
		if (adminUser != null)
			adminUser.setRoles( getSqlMapClientTemplate().queryForList( "getAdminPrivilegeById", adminUser.getId() ) );
		return adminUser;
	}
	
	public void updateAdminInfo( AccessUser user ) {
		getSqlMapClientTemplate().update("updateAdminInfo", user, 1);
	}
	
	public AccessUser getAdminInfo( ) {
		return (AccessUser) getSqlMapClientTemplate().queryForObject("getAdminInfo", "ROLE_ADMIN_" + siteId );
	}
	
	public City getCityInfoByZipcode(String zipcode) {
		return (City) getSqlMapClientTemplate().queryForObject("getCityInfoByZipcode", zipcode);
	}

	public List<City> getCities(City city) {
		return getSqlMapClientTemplate().queryForList("getCities", city);		
	}
	
	public List<County> getCounties(County county) {
		return getSqlMapClientTemplate().queryForList("getCounties", county);		
	}
	
	public ZipCode getZipCode(String zipcode) {
		return (ZipCode) getSqlMapClientTemplate().queryForObject("getZipCode", zipcode);
	}
	
	public List<ZipCode> getZipCodeList(LocationSearch locationSearch) {
		final double piByRad = 0.017453;	// Pi divided by 180
		final double latitudeMiles = 69.1; // Number of miles per degree of latitude.
		final double earthRadius = 3958;// Radius of the earth in miles
		double longitudeMiles = Math.abs( latitudeMiles * Math.cos( locationSearch.getZipcodeObject().getLatitude() * piByRad ) );
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "locationSearch", locationSearch );
		paramMap.put( "latitudeMiles", latitudeMiles );
		paramMap.put( "longitudeMiles", longitudeMiles );
		  
		return getSqlMapClientTemplate().queryForList("getZipCodeList", paramMap);
	}
}
