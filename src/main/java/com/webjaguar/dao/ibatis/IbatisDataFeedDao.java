/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.04.2007
 */

package com.webjaguar.dao.ibatis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.DataFeedDao;
import com.webjaguar.model.DataFeed;
import com.webjaguar.model.WebjaguarDataFeed;
import com.webjaguar.model.DataFeedSearch;
import com.webjaguar.model.Product;
import com.webjaguar.web.admin.datafeed.WebjaguarCategory;

public class IbatisDataFeedDao extends SqlMapClientDaoSupport implements DataFeedDao {
	  
	public List<DataFeed> getDataFeedList(String feed) {
		return getSqlMapClientTemplate().queryForList("getDataFeedList", feed);
	}
	
	public List<Product> getProductDataFeedList(DataFeedSearch search) {
		List<Product> productList = getSqlMapClientTemplate().queryForList("getProductDataFeedList", search);
		if ( productList != null ) {
			for (Product product : productList) {
				product.setCatIds(getCategoryIds( product.getId() ));
			}
		}
		return productList;
	}
	
	private Set<Object> getCategoryIds(Integer productId) throws DataAccessException
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Object[] args = { productId };
		Set<Object> categoryIds = new TreeSet<Object>();
		String query = "SELECT category_id FROM category_product where product_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet( query, args );
		while ( results.next() )
		{
			categoryIds.add( new Integer( results.getInt( "category_id" ) ) );
		}
		return categoryIds;
	}
	// I changed String to Object Spring 3
	public void updateDataFeed(List<DataFeed> list, Map<String, Object> config) {
		for (DataFeed dataFeed: list) {
			getSqlMapClientTemplate().update("updateDataFeed", dataFeed);
		}
		if(config != null) {
			getSqlMapClientTemplate().update("updateDataFeedConfig", config);
		} 
	}
	
	public void updateDataFeedList(List<DataFeed> list, List<WebjaguarDataFeed> webjaguarDataFeedList) {
		for (DataFeed dataFeed: list) {
			getSqlMapClientTemplate().update("updateDataFeed", dataFeed);
		}
		for(WebjaguarDataFeed dataFeed : webjaguarDataFeedList) {
			int value = getSqlMapClientTemplate().update("updateDataFeedConfig", dataFeed);
			if(value == 0) {
				getSqlMapClientTemplate().insert("insertDataFeedConfig", dataFeed);
			}
		}
	}
	
	public Map<String, Object> getDataFeedConfig(String feed) {
    	JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
    	Object[] args = { feed };
    	return jdbcTemplate.queryForMap("SELECT * FROM data_feed_config WHERE feed = ?", args);
	}
	
	public int productDataFeedListCount(DataFeedSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("productDataFeedListCount", search);
	}
	
	public int webjaguarDataFeedProductListCount(DataFeedSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("webjaguarDataFeedProductListCount", search);
	}
	
	public void nonTransactionSafeUpdateWebjaguarCategories(List<WebjaguarCategory> categories) {
		for (WebjaguarCategory category: categories) {
			getSqlMapClientTemplate().update("updateWebjaguarCategory", category);
		}
	}
	
	public List<WebjaguarCategory> getWebjaguarCategories() {
		return (List<WebjaguarCategory>) getSqlMapClientTemplate().queryForList("getWebjaguarCategories");
	}
	
	public Map<String, WebjaguarCategory> getWebjaguarCategoryMap() {
		return getSqlMapClientTemplate().queryForMap("getWebjaguarCategoryMap", null, "breadCrumb");
	}
	
	public Integer getWebjaguarDataFeedListCount(DataFeedSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getWebjaguarDataFeedListCount", search);
	}
	public List<WebjaguarDataFeed> getWebjaguarDataFeedList(DataFeedSearch search) {
		return (List<WebjaguarDataFeed>) getSqlMapClientTemplate().queryForList("getWebjaguarDataFeedList", search);
	}
	public WebjaguarDataFeed getWebjaguarDataFeed(Integer id, String token, String feedType) {
		
		Map<String,Object> paramMap = new HashMap<String, Object>();
		if(feedType.equalsIgnoreCase("mtz")) {
			paramMap.put("feedType", "mtz");
		} else {
			if(id != null) {
				paramMap.put("id", id);
			} else {
				paramMap.put("token", token);
			}
		}
		return (WebjaguarDataFeed) getSqlMapClientTemplate().queryForObject("getWebjaguarDataFeed", paramMap);
	}
	public void insertWebjaguarDataFeed(WebjaguarDataFeed dataFeed) {
		getSqlMapClientTemplate().insert("insertWebjaguarDataFeed", dataFeed);
	}
	public void updateWebjaguarDataFeed(WebjaguarDataFeed dataFeed) {
		getSqlMapClientTemplate().update("updateWebjaguarDataFeed", dataFeed);
	}
	public void deleteWebjaguarDataFeed(Integer id) {
		getSqlMapClientTemplate().delete("deleteWebjaguarDataFeed", id);
	}
	
}
