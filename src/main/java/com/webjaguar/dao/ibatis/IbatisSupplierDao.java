/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.SupplierDao;
import com.webjaguar.model.Address;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Search;
import com.webjaguar.model.Supplier;


public class IbatisSupplierDao extends SqlMapClientDaoSupport implements SupplierDao {
	  
	  public List<Supplier> getSuppliers(Integer categoryId) {
		  return getSqlMapClientTemplate().queryForList("getSupplierList", categoryId);
	  }
	  
	  public List<Supplier> getSuppliers(Search search) {
		  return getSqlMapClientTemplate().queryForList("getSuppliersList", search);
	  } 
	  public Integer getProductsCountBySupplierId(Integer supplierId) {
		  return (Integer)getSqlMapClientTemplate().queryForObject("getProductsCountBySupplierId", supplierId);
	  } 
	  public void insertSupplier(Supplier supplier) throws DataAccessException {
		 getSqlMapClientTemplate().insert("insertSupplier", supplier);
		 getSqlMapClientTemplate().update("updateSupplierById", supplier, 1);
		 supplier.getAddress().setSupplierId(supplier.getId());
		 
		 /* This code was form mytradezone.
		 supplier.setId((Integer) getSqlMapClientTemplate().queryForObject("getSupplierIdByUserid", supplier.getUserId()));
		 insertCategoryIds(supplier.getId(), supplier.getCatIds());
		 */
		 
		 insertSupplierAddress(supplier.getAddress());
	  }
	  
	  public void updateSupplier(Supplier supplier) throws DataAccessException {
		  getSqlMapClientTemplate().update("updateSupplier", supplier, 1);
		  supplier.setId((Integer) getSqlMapClientTemplate().queryForObject("getSupplierIdByUserid", supplier.getUserId()));
		  getSqlMapClientTemplate().delete("deleteSupplierCatId", supplier.getId());		  
		  insertCategoryIds(supplier.getId(), supplier.getCatIds());
		  updateSupplierAddress(supplier.getAddress());
	  }
	  
	  public Integer getSupplierIdByCompany(String company) {
		  return (Integer) getSqlMapClientTemplate().queryForObject("getSupplierIdByCompany", company);
	  }
	  
	  public String getSupplierCompanyBySupplierId(Integer suppId) {
		  return (String) getSqlMapClientTemplate().queryForObject("getSupplierCompanyBySupplierId", suppId);
	  }
	  
	  public Integer getSupplierIdByAccountNumber(String accountNumber) {
		  return (Integer) getSqlMapClientTemplate().queryForObject("getSupplierIdByAccountNumber", accountNumber);
	  }
	  
	  public Supplier getSupplierByAccountNumber(String accountNumber) {
		  return (Supplier) getSqlMapClientTemplate().queryForObject("getSupplierByAccountNumber", accountNumber);
	  }
	  
	  public void insertSupplierByCustomer(Customer customer) {
		  getSqlMapClientTemplate().insert("insertSupplierByCustomer", customer);
		  if (customer.getSupplierId() == null) {
			  // update customer database
			  getSqlMapClientTemplate().update("updateCustomerSupplier", customer);			  
		  }
	  }
	  
	  private void insertCategoryIds(Integer supplierId, Set<Integer> categoryIds) {
		  JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		  Iterator<Integer> iter = categoryIds.iterator();
	      String query = "INSERT into category_supplier (category_id, supplier_id) VALUES (?, ?)";
		  while (iter.hasNext()) {
		       Object[] args = { iter.next(), supplierId };		  
		       jdbcTemplate.update(query, args);
		  }
	  }

	  private Set<Integer> getCategoryIds(Integer supplierId) throws DataAccessException {
		  JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
	       Object[] args = { supplierId };
	       Set<Integer> categoryIds = new HashSet<Integer>();
	       String query = "SELECT category_id FROM category_supplier where supplier_id = ?";
	       SqlRowSet results = jdbcTemplate.queryForRowSet(query, args);
	       while ( results.next() ) {
	    	      categoryIds.add(new Integer(results.getInt("category_id")));
	       }
	       return categoryIds;
	  }
	  
	  public Supplier getSupplierByUserid(Integer userId) throws DataAccessException {
		  Supplier supplier = new Supplier();
		  supplier.setId((Integer) getSqlMapClientTemplate().queryForObject("getSupplierIdByUserid", userId));
		  supplier.setCatIds( getCategoryIds(supplier.getId()) );
		  return supplier;
	  }
	  
	  public Supplier getDefaultSupplierBySku(String sku) throws DataAccessException {
		  return (Supplier) getSqlMapClientTemplate().queryForObject("getDefaultSupplierBySku", sku);
	  }
	  public List<Supplier> getSupplierAjax(String company) {
			return getSqlMapClientTemplate().queryForList( "getSupplierAjax", company );
		}
	  
	  public Supplier getSupplierById(Integer supplierId) {
		  return (Supplier) getSqlMapClientTemplate().queryForObject("getSupplierById", supplierId);
	  }
	  
	  public void updateSupplierById(Supplier supplier) throws DataAccessException {
		  getSqlMapClientTemplate().update("updateSupplierById", supplier, 1);
	  }
	  
	  public void updateSupplierByIdWithMarkup(Supplier supplier) throws DataAccessException {
		  getSqlMapClientTemplate().update("updateSupplierByIdWithMarkup", supplier, 1);
	  }
	  
	  public void updateSupplierStatusById(Integer id, boolean active) throws DataAccessException {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "id", id );
		  paramMap.put( "active", active );
		  getSqlMapClientTemplate().update("updateSupplierStatusById", paramMap, 1);
	  }
	  
	  public void deleteSupplierById(Integer supplierId) {
			getSqlMapClientTemplate().delete("deleteSupplierById", supplierId);
	  }
	
	  public List<Supplier> getProductSupplierListByOrderId(int orderId, String filter) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "orderId", orderId );
		  paramMap.put( "filter", filter );
		  return getSqlMapClientTemplate().queryForList("getProductSupplierListByOrderId", paramMap);		  
	  }
	  
	  public void updateProductSupplier(final int supplierId, final List<Map <String, Object>> data) {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
			String sql = "UPDATE " +
							"product_supplier " +
						 "SET " +
						 	"price = ?, " +
						 	"last_modified = now() " +
						 "WHERE " +
						 	"supplier_id = ? and " +
						 	"sku = ?";
			jdbcTemplate.batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
				public void setValues(PreparedStatement ps, int i) throws SQLException {
					Map<String, Object> entry = data.get(i);

					if (entry.get("cost") == null) {
						ps.setNull(1, Types.NULL);
					} else {
						ps.setDouble(1, (Double) entry.get("cost"));						
					}
					ps.setInt(2, supplierId);
					ps.setString(3, (String) entry.get("sku"));
				}

				public int getBatchSize() {
					return data.size();
				}
			});		  
	  }
	  
	  public List<Address> getAddressListBySupplierId(Integer supplierId) {
		  return getSqlMapClientTemplate().queryForList("getAddressListBySupplierId", supplierId);
	  }
	  
	  public Address getDefaultAddressBySupplierId(Integer supplierId) {
		  return (Address) getSqlMapClientTemplate().queryForObject("getDefaultAddressBySupplierId", supplierId);		  
	  }
	  
	  public void updateSupplierAddress(Address address) throws DataAccessException {
		  getSqlMapClientTemplate().update("updateSupplierAddress", address, 1);
		  if (address.isPrimary()) {
			  getSqlMapClientTemplate().update("updateDefaultSupplierAddress", address, 1);		  
		  }	
	  }
	  
	  public void updateSupplierPrimeAddressBySupplierId(Address address) throws DataAccessException {
		  getSqlMapClientTemplate().update("updateSupplierPrimeAddressBySupplierId", address, 1);
	  }
	  
	  private void insertPrimaryAddress(Address address) {
		  getSqlMapClientTemplate().insert("insertSupplierAddress", address);
		  getSqlMapClientTemplate().update("updateDefaultSupplierAddress", address, 1);		  
		  updateSupplierPrimeAddressBySupplierId(address);	
	  }
	  
	  public void insertSupplierAddress(Address address) throws DataAccessException {
		  if (address.isPrimary()) {
			  insertPrimaryAddress(address);
		  } else {
			  getSqlMapClientTemplate().insert("insertSupplierAddress", address);
		  }	
	  }
	  public Address getSupplierAddressById(Integer addressId) {
		  return (Address) getSqlMapClientTemplate().queryForObject("getSupplierAddressById", addressId);
	  }	
	  public void deleteSupplierAddress(Address address) throws DataAccessException {
		  getSqlMapClientTemplate().delete("deleteSupplierAddress", address);	
	  }
	  
	  //supplier name map
	  public Map<Integer, String> getSupplierNameMap(List<Integer> suppIdList) { 
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put("suppIdList", suppIdList);
		  return getSqlMapClientTemplate().queryForMap("getSupplierNameMap",paramMap, "id", "name") ;
	  }	
	  
	  public int supplierCount() {
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
			return jdbcTemplate.queryForInt("SELECT count(id) FROM supplier");
	 }
		
	  public List getSupplierExportList(int limit, int offset) {
			Map<String, Integer> paramMap = new HashMap<String, Integer>();
			paramMap.put("limit", limit);
			paramMap.put("offset", offset);
			return getSqlMapClientTemplate().queryForList("getSupplierExportList", paramMap);
	  }
	  
	  public void importSuppliers(List<Supplier> suppliers) {
			for (Supplier supplier : suppliers) {
				getSqlMapClientTemplate().update("updateSupplierById", supplier, 1);
			}
	  }
}
