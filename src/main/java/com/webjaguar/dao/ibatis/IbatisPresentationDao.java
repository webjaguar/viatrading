/* Copyright 2012 Advanced E-Media Solutions
 *
 */
package com.webjaguar.dao.ibatis;


import java.util.Date;
import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.PresentationDao;
import com.webjaguar.model.Presentation;
import com.webjaguar.model.PresentationProduct;
import com.webjaguar.model.PresentationTemplate;
import com.webjaguar.model.PresentationTemplateSearch;

public class IbatisPresentationDao extends SqlMapClientDaoSupport implements PresentationDao {

/*
 * (non-Javadoc)
 * @see com.webjaguar.dao.PresentationDao#getPresentationProductList(com.webjaguar.model.Presentation)
 */
	public List<PresentationProduct> getPresentationProductList(Presentation presentation) {
		return getSqlMapClientTemplate().queryForList("getPresentationProductList", presentation);
	}
	
	public List<Presentation> getPresentationList(Presentation presentation) {
		return getSqlMapClientTemplate().queryForList("getPresentationList",presentation);
	}
	
	public Presentation getPresentation(Integer presentationId) {
		return (Presentation) getSqlMapClientTemplate().queryForObject("getPresentation", presentationId);
	}
	
	public void addProductToPresentation(String[] sku, Integer presentationId, Integer saleRepId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query1 = "INSERT into presentation_product (sale_rep_id, sku, presentation_id) VALUES (?, ?, ?)";
		for (int i=1 ;  i <= sku.length ; ++i ) {
			if(! sku[i-1].isEmpty()) {
				Object[] args1 = { saleRepId, sku[i-1],  presentationId};
				try {
					jdbcTemplate.update( query1, args1 );
				}
				catch ( DataIntegrityViolationException e ) {
					// do nothing
				}
			}
		}
		String query2 = "update presentation set modified = ? where presentation_id = ?";
		
		Object[] args2 = { new Date(),  presentationId};
		try
		{
			jdbcTemplate.update( query2, args2 );
		}
		catch ( DataIntegrityViolationException e )
		{
			// do nothing
		}
	}
	
	public void insertPresentation(Presentation presentation) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query = "INSERT into presentation (name,title,template_id,salesrep_id, added, modified,user_id, notes, last_updated_by) VALUES (?, ?, ?, ?, ?, ?,?,?,?)";
		Object[] args = { presentation.getName(), presentation.getTitle(), presentation.getTemplateId(), presentation.getSaleRepId(), presentation.getCreated(), new Date(), presentation.getUserId(), presentation.getNote(), presentation.getSaleRepId()};
		try
		{
			jdbcTemplate.update( query, args );
		}
		catch ( DataIntegrityViolationException e )
		{
			// do nothing
		}
	}
	
	public void updatePresentationPrice(String sku, double price, Integer presentationId, Integer salesRepId, boolean remove) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query;
		if (remove) {
			query = "delete from presentation_product where  presentation_id = ? and sku = ?";
			Object[] args = { presentationId, sku};
			try
			{
				jdbcTemplate.update( query, args );
			}
			catch ( DataIntegrityViolationException e )
			{
				// do nothing
			}
		} else {
			query = "update presentation_product set price = ?  where  presentation_id = ? and sku = ?";
			Object[] args = { price, presentationId, sku};
			try
			{
				jdbcTemplate.update( query, args );
			}
			catch ( DataIntegrityViolationException e )
			{
				// do nothing
			}
		}
		query = "update presentation set last_updated_by = ?  where  presentation_id = ? ";
		Object[] args1 = { salesRepId, presentationId };
		try
		{
			jdbcTemplate.update( query, args1 );
		}
		catch ( DataIntegrityViolationException e )
		{
			// do nothing
		}
	}
	
	public void updatePresentation(Presentation presentation) {
		getSqlMapClientTemplate().update("updatePresentation", presentation);
	}
	
	public void deletePresentation(Presentation presentation) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query = "delete from presentation where presentation_id=?";
		Object[] args = { presentation.getId()};
		try
		{
			jdbcTemplate.update( query, args );
		}
		catch ( DataIntegrityViolationException e )
		{
			// do nothing
		}
	}
	
	public void updateNoOfViewToPresentation(Integer presentationId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query = "update presentation set viewed = viewed +1 where presentation_id=?";
		Object[] args = { presentationId};
		try
		{
			jdbcTemplate.update( query, args );
		}
		catch ( DataIntegrityViolationException e )
		{
			// do nothing
		}
	}

	public List<PresentationTemplate> getPresentationTemplateList(PresentationTemplateSearch templateSearch) {
		return getSqlMapClientTemplate().queryForList("getPresentationTemplateList",templateSearch);
	}
	
	public void insertTemplate(PresentationTemplate template) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query = "INSERT into presentation_template (name) VALUES (?)";
		Object[] args = { template.getName()};
		try
		{
			jdbcTemplate.update( query, args );
		}
		catch ( DataIntegrityViolationException e )
		{
			e.printStackTrace();
			// do nothing
		}
		String sql = "select template_id from presentation_template where name = ?";
		Integer id = jdbcTemplate.queryForInt(sql, args);
		template.setId(id);
		updateTemplate(template);
	}
	
	public void updateTemplate(PresentationTemplate template) {
		getSqlMapClientTemplate().update("updateTemplate", template);
	}
	
	public void deleteTemplate(Integer templateId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query = "delete from presentation_template where template_id = ?";
		Object[] args = { templateId};
		try
		{
			jdbcTemplate.update( query, args );
		}
		catch ( DataIntegrityViolationException e )
		{
			// do nothing
		}
	}
	
	public PresentationTemplate getTemplateById(Integer templateId) {
		return (PresentationTemplate) getSqlMapClientTemplate().queryForObject("getTemplateById", templateId);
	}
	
	public void removeProductFromPresentationList(String sku[], Integer saleRepId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query1 = "DELETE from presentation_product WHERE sale_rep_id = ? and  sku = ? and presentation_id is null ";
		for (int i=1 ;  i <= sku.length ; ++i )
		{
			Object[] args1 = { saleRepId, sku[i-1]};
			try
			{
				jdbcTemplate.update( query1, args1 );
			}
			catch ( DataIntegrityViolationException e )
			{
				// do nothing
			}
		}
	}

}
