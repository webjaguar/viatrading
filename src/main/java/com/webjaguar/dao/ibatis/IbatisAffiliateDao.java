/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.AffiliateDao;
import com.webjaguar.model.CommissionReport;
import com.webjaguar.model.CommissionSearch;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;



public class IbatisAffiliateDao extends SqlMapClientDaoSupport implements AffiliateDao {
	
	
	  public List getCommissionsByUserId(Integer userId, Integer level, CommissionSearch search) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
  		  paramMap.put( "level", level );
  		  paramMap.put( "userId", userId );
  		  paramMap.put( "search", search );
		  List<CommissionReport> commissionReports = getSqlMapClientTemplate().queryForList("getCommissionsByUserId", paramMap);
		  for ( CommissionReport comReport : commissionReports ) {
			  if (comReport.getOrderStatus().equals( "x" ) || comReport.getOrderStatus().equals( "xp" )) {
				  comReport.setMyCommission( 0.0 );
			  }
		  }
		  return commissionReports;
	  }
      
      public CommissionReport getCommissionsByUserIdOrderId(Integer userId, Integer orderId, Integer level) {
    	  Map<String, Object> paramMap = new HashMap<String, Object>();
  		  paramMap.put( "userId", userId );
  		  paramMap.put( "orderId", orderId );
  		  paramMap.put( "level", level );
    	  return (CommissionReport) getSqlMapClientTemplate().queryForObject( "getCommissionsByUserIdOrderId", paramMap );

      }
      
      public CommissionReport getCommissionTotalByUserId(Integer userId, Integer level) {
    	  Map<String, Object> paramMap = new HashMap<String, Object>();
  		  paramMap.put( "userId", userId );
  		  if ( level ==1 ) {
  			paramMap.put( "commission_level", "orders.commission_level1" );
  			paramMap.put( "affiliate_id", "affiliate_id_level1" );
  		  } else if ( level == 2 ) {
  			paramMap.put( "commission_level", "orders.commission_level2" );
  			paramMap.put( "affiliate_id", "affiliate_id_level2" );
  		  }
  		  
    	  return (CommissionReport) getSqlMapClientTemplate().queryForObject( "getCommissionTotalByUserId", paramMap );
      }
      
      public CommissionReport getCommissionTotal(Integer userId) {
    	  Map<String, Object> paramMap = new HashMap<String, Object>();
  		  paramMap.put( "userId", userId );
  		  return (CommissionReport) getSqlMapClientTemplate().queryForObject( "getCommissionTotal", paramMap );
      }

      public void updateCommissionStatus(CommissionReport comReport) {
		  getSqlMapClientTemplate().update("updateCommissionStatus", comReport, 1);
      }
      
      public int getAffiliateListCount(CustomerSearch search) {
		  return (Integer) getSqlMapClientTemplate().queryForObject("getAffiliateListCount", search);
	  }

      public List<Customer> getAffiliateList(CustomerSearch search) {
		  return getSqlMapClientTemplate().queryForList("getAffiliateList", search);
	  }
      
      public List<Customer> getAffiliateNameList() {
		  return getSqlMapClientTemplate().queryForList("getAffiliateNameList");
	  }
}