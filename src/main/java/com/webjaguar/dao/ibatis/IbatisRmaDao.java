/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.16.2008
 */

package com.webjaguar.dao.ibatis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.RmaDao;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Rma;
import com.webjaguar.model.RmaSearch;

public class IbatisRmaDao extends SqlMapClientDaoSupport implements RmaDao {
	
	public Integer getLastOrderId(String serialNum) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getLastOrderId", serialNum);	
	}
	
	public void insertRma(Rma rma) {
		getSqlMapClientTemplate().insert("insertRma", rma);
	}
	
	public List<Rma> getRmaList(RmaSearch search) {
		return getSqlMapClientTemplate().queryForList("getRmaList", search);
	}

	public Rma getRma(Rma rma) {
		return (Rma) getSqlMapClientTemplate().queryForObject("getRma", rma);		
	}
	
	public void updateRma(Rma rma) {
		getSqlMapClientTemplate().update("updateRma", rma, 1);
	}
	
	public LineItem getLineItem(int orderId, String serialNum) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("orderId", orderId);
		paramMap.put("serialNum", serialNum);
		return (LineItem) getSqlMapClientTemplate().queryForObject("getLineItem", paramMap);
	}
}
