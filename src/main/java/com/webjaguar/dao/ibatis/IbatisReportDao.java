/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.ReportDao;
import com.webjaguar.model.AbandonedShoppingCart;
import com.webjaguar.model.AbandonedShoppingCartSearch;
import com.webjaguar.model.CityReport;
import com.webjaguar.model.ConsignmentReport;
import com.webjaguar.model.CustomerCreditHistory;
import com.webjaguar.model.CustomerReport;
import com.webjaguar.model.EmailCampaign;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.ImportExportHistorySearch;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.InventoryReport;
import com.webjaguar.model.MetricReport;
import com.webjaguar.model.Order;
import com.webjaguar.model.ProductReport;
import com.webjaguar.model.Report;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.SalesRepReport;
import com.webjaguar.model.StateReport;
import com.webjaguar.model.TicketReport;
import com.webjaguar.model.VbaPaymentReport;


public class IbatisReportDao extends SqlMapClientDaoSupport implements ReportDao {
		  
	  public List<ImportExportHistory> getImportExportHistoryByType(ImportExportHistorySearch search) {
		  return getSqlMapClientTemplate().queryForList("getImportExportHistoryByType", search);
	  }

	  public void insertImportExportHistory(ImportExportHistory imEx) {
			getSqlMapClientTemplate().insert("insertImportExportHistory", imEx);
	  }
	  
	  public List<Report> getOrdersSaleReport(ReportFilter filter) {
		  if ( filter.getDateType().equals( "orderDate" ) )
			  return getSqlMapClientTemplate().queryForList("getOrdersSaleReport", filter);
		  else
			  return getSqlMapClientTemplate().queryForList("getOrdersSaleReportShipDate", filter);
	  }
	  public List<Report> getOrdersSaleDetailReport(ReportFilter filter) {
		  return getSqlMapClientTemplate().queryForList("getOrdersSaleDetailReport", filter);
	  }	 

	  public List<MetricReport> getMetricReport(ReportFilter filter) { 
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  List<MetricReport> metricReport =  new ArrayList<MetricReport>();
		  if(filter.getQuarter().equalsIgnoreCase("1")) {
			  paramMap.put( "filter", filter);
			  paramMap.put("year", filter.getYear());
			for(int i = 1, j=0; i<13; i++) {
				  paramMap.put( "month", i );
				  List<MetricReport> metricReportResult = null;
				  if ( filter.getDateType().equals( "orderDate" ) ) {
					  metricReportResult = getSqlMapClientTemplate().queryForList("getMetricReport",paramMap);
				  } else {
					  metricReportResult = getSqlMapClientTemplate().queryForList("getMetricReportShipDate",paramMap); 
				  }
				  if (metricReportResult.size() > j) {
					  metricReport.add(metricReportResult.get(j));				  
				  }
			 }			
		  } else  {
			  for(int i = 0, j=0; i< 5; i++) {
				  int Year = Integer.parseInt(filter.getYear());
				  filter.setMonth(null);
				  paramMap.put( "year", Year-i );
				  paramMap.put( "filter", filter);List<MetricReport> metricReportResult = null;
				  if ( filter.getDateType().equals( "orderDate" ) ) {
					  metricReportResult = getSqlMapClientTemplate().queryForList("getMetricReport",paramMap);
				  } else {
					  metricReportResult = getSqlMapClientTemplate().queryForList("getMetricReportShipDate",paramMap); 
				  }
				  if (metricReportResult.size() > j) {
					  metricReport.add(metricReportResult.get(j));				  
				  }
			}
		  }
		  return metricReport;
	  }
	  public Integer getNumCustomerActivated(String year, int month) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "year", year );
		  paramMap.put( "month", month );
		  return (Integer) getSqlMapClientTemplate().queryForObject("getNumCustomerActivated", paramMap);	  
	  }
	  
	  public Integer getNumCustomerRetained(String year, int month) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "year", year );
		  paramMap.put( "month", month );
		  return (Integer) getSqlMapClientTemplate().queryForObject("getNumCustomerRetained", paramMap);	  
	  }
	  public List<Report> getSalesRepDetailReport(ReportFilter filter) {
		  return getSqlMapClientTemplate().queryForList("getSalesRepDetailReport", filter);
	  }
	  public List<Report> getSalesRepDetailReport2(ReportFilter filter) {
		  return getSqlMapClientTemplate().queryForList("getSalesRepDetailReport2", filter);
	  }
	  public List<Report> getOrdersSaleReportDaily(ReportFilter filter) {
		  if ( filter.getDateType().equals( "orderDate" ) )
			  return getSqlMapClientTemplate().queryForList("getOrdersSaleReportDaily", filter);
		  else
			  return getSqlMapClientTemplate().queryForList("getOrdersSaleReportDailyShipDate", filter);
	  }
	  public Integer getOrdersSaleReportDailyCount(ReportFilter filter) {
		  if ( filter.getDateType().equals( "orderDate" ) )
			  return (Integer) getSqlMapClientTemplate().queryForObject("getOrdersSaleReportDailyCount", filter);
		  else
			  return (Integer) getSqlMapClientTemplate().queryForObject("getOrdersSaleReportDailyShipDateCount", filter);
	  }
	  public List<CustomerReport> getCustomerReportOverview(ReportFilter filter) {
		  return getSqlMapClientTemplate().queryForList("getCustomerReportOverview", filter);
	  }

	  public List<Report> getCustomerQuickViewPurchase(Integer userId) {
		  return getSqlMapClientTemplate().queryForList( "getCustomerQuickViewPurchase", userId );			  
	  }
	  
	  public List<Report> getCustomerQuickViewPurchaseLastYear(Integer userId) {
		  return getSqlMapClientTemplate().queryForList( "getCustomerQuickViewPurchaseLastYear", userId );			  
	  }
	  
	  public Integer getCountPendingProcessingOrder() {
		  return (Integer) getSqlMapClientTemplate().queryForObject( "getCountPendingProcessingOrder" );
	  }
	  
	  public List<Integer> getInactiveCustomerIds(ReportFilter reportFilter) {
		  return (List<Integer>) getSqlMapClientTemplate().queryForList( "getInactiveCustomerIds", reportFilter );
	  }
	  
	  public List<CustomerReport> getInactiveCustomers(ReportFilter reportFilter) {
		  return getSqlMapClientTemplate().queryForList( "getInactiveCustomers", reportFilter );
	  }
	  
	  public List<Report> getTrackcodeReport(ReportFilter reportFilter) {
		  return getSqlMapClientTemplate().queryForList( "getTrackcodeReport", reportFilter );
	  }
	  
	  public List<Order> getPendingProcessingOrder() {
		  return getSqlMapClientTemplate().queryForList( "getPendingProcessingOrder" );
	  }
	  
	  public List<SalesRepReport> getSalesRepReport(Integer salesRepId, ReportFilter salesRepFilter) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "salesRepId", salesRepId );
		  paramMap.put( "salesRepFilter", salesRepFilter );
		  List<SalesRepReport> penProcList;
		  List<SalesRepReport> shippedAndPenPro = new ArrayList<SalesRepReport>();

		  if ( salesRepFilter.getDateType().equalsIgnoreCase( "shipDate" ) )
			  penProcList = getSqlMapClientTemplate().queryForList( "getSalesRepReportByPenProcShipDate", paramMap );
		  else
			  penProcList = getSqlMapClientTemplate().queryForList( "getSalesRepReportByPenProc", paramMap );
		  
		  for ( int i=0 ; i < 12; i++ ) {
			try {
				penProcList.get( 0 ).setSalesRepId( salesRepId );
				if ( penProcList.get( i ).getMonth() != i+1 ) {
					SalesRepReport r = new SalesRepReport(i+1, null, null, salesRepId, null);
					penProcList.add( i, r );
				}
			} catch (IndexOutOfBoundsException e) {
				SalesRepReport r = new SalesRepReport(i+1, null, null, salesRepId, null);
				penProcList.add( i, r );
			} catch (NullPointerException e) {
				SalesRepReport r = new SalesRepReport(i+1, null, null, salesRepId, null);
				penProcList.add( i, r );
			}
		  }

		  if (penProcList.size() < 13) {
			  SalesRepReport r = new SalesRepReport(null, null, null, salesRepId, null);
			  penProcList.add( 12, r );
		  }
		  Map<Integer, SalesRepReport> shippedMap;
		  if ( salesRepFilter.getDateType().equalsIgnoreCase( "shipDate" ) )
			  shippedMap = getSqlMapClientTemplate().queryForMap( "getSalesRepReportByShippedShipDate", paramMap, "month" );
		  else
			  shippedMap = getSqlMapClientTemplate().queryForMap( "getSalesRepReportByShipped", paramMap, "month" );

		  for (SalesRepReport penProc : penProcList ) {
			  if ( salesRepFilter.getQuarter().equals( "0" ) && (penProc.getMonth() == null )) {
				  try {
					  SalesRepReport salesRepReport = new SalesRepReport(-1, shippedMap.get( null ).getGrandTotalShipped(), penProc.getGrandTotalPenProc(), penProc.getSalesRepId(), penProc.getUserId());
					  shippedAndPenPro.add( salesRepReport );
				  } catch (Exception e) {
					  SalesRepReport salesRepReport = new SalesRepReport(-1, 0.0, penProc.getGrandTotalPenProc(), penProc.getSalesRepId(), penProc.getUserId());
					  shippedAndPenPro.add( salesRepReport );
				  }
			  } else if ( salesRepFilter.getQuarter().equals( "1" ) && penProc.getMonth() != null && penProc.getMonth() >= 1 && penProc.getMonth() <= 3) {
				  try {
					  SalesRepReport salesRepReport = new SalesRepReport(penProc.getMonth(), shippedMap.get( penProc.getMonth() ).getGrandTotalShipped(), penProc.getGrandTotalPenProc(), penProc.getSalesRepId(), penProc.getUserId());
					  shippedAndPenPro.add( salesRepReport );
				  } catch (Exception e) {
					  SalesRepReport salesRepReport = new SalesRepReport(penProc.getMonth(), 0.0, penProc.getGrandTotalPenProc(), penProc.getSalesRepId(), penProc.getUserId());
					  shippedAndPenPro.add( salesRepReport );
				  }
			  } else if ( salesRepFilter.getQuarter().equals( "2" ) && penProc.getMonth() != null && penProc.getMonth() >= 4 && penProc.getMonth() <= 6) {
				  try {
					  SalesRepReport salesRepReport = new SalesRepReport(penProc.getMonth(), shippedMap.get( penProc.getMonth() ).getGrandTotalShipped(), penProc.getGrandTotalPenProc(), penProc.getSalesRepId(), penProc.getUserId());
					  shippedAndPenPro.add( salesRepReport );
				  } catch (Exception e) {
					  SalesRepReport salesRepReport = new SalesRepReport(penProc.getMonth(), 0.0, penProc.getGrandTotalPenProc(), penProc.getSalesRepId(), penProc.getUserId());
					  shippedAndPenPro.add( salesRepReport );
				  }
			  } else if ( salesRepFilter.getQuarter().equals( "3" ) && penProc.getMonth() != null && penProc.getMonth() >= 7 && penProc.getMonth() <= 9) {
				  try {
					  SalesRepReport salesRepReport = new SalesRepReport(penProc.getMonth(), shippedMap.get( penProc.getMonth() ).getGrandTotalShipped(), penProc.getGrandTotalPenProc(), penProc.getSalesRepId(), penProc.getUserId());
					  shippedAndPenPro.add( salesRepReport );
				  } catch (Exception e) {
					  SalesRepReport salesRepReport = new SalesRepReport(penProc.getMonth(), 0.0, penProc.getGrandTotalPenProc(), penProc.getSalesRepId(), penProc.getUserId());
					  shippedAndPenPro.add( salesRepReport );
				  }
			  } else if ( salesRepFilter.getQuarter().equals( "4" ) && penProc.getMonth() != null && penProc.getMonth() >= 10 && penProc.getMonth() <= 12) {
				  try {
					  SalesRepReport salesRepReport = new SalesRepReport(penProc.getMonth(), shippedMap.get( penProc.getMonth() ).getGrandTotalShipped(), penProc.getGrandTotalPenProc(), penProc.getSalesRepId(), penProc.getUserId());
					  shippedAndPenPro.add( salesRepReport );
				  } catch (Exception e) {
					  SalesRepReport salesRepReport = new SalesRepReport(penProc.getMonth(), 0.0, penProc.getGrandTotalPenProc(), penProc.getSalesRepId(), penProc.getUserId());
					  shippedAndPenPro.add( salesRepReport );
				  }
			  }
		  }
		  return shippedAndPenPro;
      }
	  
	  public List<SalesRepReport> getSalesRepReportDaily(Integer salesRepId, ReportFilter salesRepFilter) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "salesRepId", salesRepId );
		  paramMap.put( "salesRepFilter", salesRepFilter );
		  
		  List<SalesRepReport> shippedAndPenProList = new ArrayList<SalesRepReport>();
		  List<SalesRepReport> penProcList = new ArrayList<SalesRepReport>();
		  if ( salesRepFilter.getDateType().equalsIgnoreCase( "shipDate" ) )
			  penProcList = getSqlMapClientTemplate().queryForList( "getSalesRepReportByPenProcDailyShipDate", paramMap );
		  else
			  penProcList = getSqlMapClientTemplate().queryForList( "getSalesRepReportByPenProcDaily", paramMap );
		  
		  Calendar rightNow = Calendar.getInstance();
		  rightNow.set( Integer.parseInt(salesRepFilter.getYear()), rightNow.get( Calendar.MONTH ), rightNow.get( Calendar.DAY_OF_MONTH ) );
		  for (int i=0 ; i < 30; i++) {
			  try {
				  if ( penProcList.get( i ).getDay() == rightNow.get( Calendar.DAY_OF_YEAR ) ) {
					  shippedAndPenProList.add( penProcList.get( i ) );
				  } else {
					  SalesRepReport r = new SalesRepReport(rightNow.getTime(), 0.0, 0.0, 0, 0, penProcList.get( i ).getNumOrderPenProc(), penProcList.get( i ).getUniqueUserPenProc(), salesRepId, null);
					  r.setDay( rightNow.get( Calendar.DAY_OF_YEAR ) );
					  penProcList.add( i, r );
					  shippedAndPenProList.add( r );
				  }
			  } catch (Exception e) {
				  SalesRepReport r = new SalesRepReport(rightNow.getTime(), 0.0, 0.0, 0, 0, 0, 0, salesRepId, null);
				  r.setDay( rightNow.get( Calendar.DAY_OF_YEAR ) );
				  penProcList.add( i, r );
				  shippedAndPenProList.add( r );
			  }
			  rightNow.add( Calendar.DAY_OF_MONTH, -1 );
		  }
		  Map<Integer, SalesRepReport> shippedMap = new HashMap<Integer, SalesRepReport>();
		  if ( salesRepFilter.getDateType().equalsIgnoreCase( "shipDate" ) )
			  shippedMap = getSqlMapClientTemplate().queryForMap( "getSalesRepReportByShippedDailyShipDate", paramMap, "day" );
		  else
			  shippedMap = getSqlMapClientTemplate().queryForMap( "getSalesRepReportByShippedDaily", paramMap, "day" );
		  
		  for (SalesRepReport report : shippedAndPenProList ) {
			  report.setSalesRepId( salesRepId );
			  try {
				  report.setGrandTotalShipped( shippedMap.get( report.getDay() ).getGrandTotalShipped() );
				  report.setNumOrderShipped( shippedMap.get( report.getDay() ).getNumOrderShipped());
				  report.setUniqueUserShipped( shippedMap.get( report.getDay() ).getUniqueUserShipped() );
			  } catch (Exception e) {
			  	  report.setGrandTotalShipped( 0.0 );
			  	  report.setNumOrderShipped( 0 );
			  	report.setUniqueUserShipped( 0 );
			  	  
			  }
		  }
		  return shippedAndPenProList;
	  }
	  
	  // promo report
	  public List<Report> getPromoCodeReport(ReportFilter reportFilter) {
			return  getSqlMapClientTemplate().queryForList("getPromoCodeReport", reportFilter);
	  }
	  
	  // Mass Email
	  public List<EmailCampaign> getEmailCampaigns(ReportFilter reportFilter) {
		  return getSqlMapClientTemplate().queryForList("getEmailCampaignList", reportFilter);
	  }
	  
	  public List<ProductReport> getProductListReport(ReportFilter reportFilter) {
		  if ( reportFilter.getStatus() != null ) {
			  if (reportFilter.getStatus().equals( "nxs" )) 
				  reportFilter.setStatusTemp(" ( status = 'p' OR status = 'pr' OR status = 'wp' OR status = 'rls' )  ");
			   else	if ( reportFilter.getStatus().equals( "ppr" ) )
					reportFilter.setStatusTemp(" ( status = 'p' OR status = 'pr' ) ");
				else if ( reportFilter.getStatus().equals( "pprs" ) )
					reportFilter.setStatusTemp(" ( status = 'p' OR status = 'pr' OR status = 's' OR status = 'wp' OR status = 'rls')  ");
				else if ( reportFilter.getStatus().equals( "" ))
					reportFilter.setStatusTemp(" ( status != 'xq' ) ");
				else
					reportFilter.setStatusTemp(" ( status = '" + reportFilter.getStatus() + "' ) ");
			} else {
				  reportFilter.setStatusTemp(" ( status != 'xq' ) ");
			}
		  return getSqlMapClientTemplate().queryForList("getProductListReport", reportFilter);
	  }
	  public int getProductListReportCount(ReportFilter reportFilter) {
		  if ( reportFilter.getStatus() != null ) {
			  if ( reportFilter.getStatus().equals( "nxs" )) 
				  reportFilter.setStatusTemp(" ( status = 'p' OR status = 'pr' OR status = 'wp' OR status = 'rls') ");
			   else if ( reportFilter.getStatus().equals( "ppr" ) )
					reportFilter.setStatusTemp(" ( status = 'p' OR status = 'pr' ) ");
				else if ( reportFilter.getStatus().equals( "pprs" ) )
					reportFilter.setStatusTemp(" ( status = 'p' OR status = 'pr' OR status = 's' OR status = 'wp' OR status = 'rls') ");
				else if ( reportFilter.getStatus().equals( "" ))
					reportFilter.setStatusTemp(" ( status != 'xq' ) ");
				else
					reportFilter.setStatusTemp(" ( status = '" + reportFilter.getStatus() + "' ) ");
		  } else {
			  reportFilter.setStatusTemp(" ( status != 'xq' ) ");
		  }
		return (Integer) getSqlMapClientTemplate().queryForObject("getProductListReportCount", reportFilter);
	  }
	  public List<InventoryActivity> getInventoryActivity(ReportFilter reportFilter) {
		  return getSqlMapClientTemplate().queryForList("getInventoryActivity", reportFilter);
	  }
	  public Integer getInventoryActivityCount(ReportFilter reportFilter) {
		  return (Integer) getSqlMapClientTemplate().queryForObject("getInventoryActivityCount", reportFilter);
	  }
	  public List<InventoryReport> getInventoryReport(ReportFilter reportFilter) {
		  return getSqlMapClientTemplate().queryForList("getInventoryReport", reportFilter);
	  }
	  public Integer getInventoryReportCount(ReportFilter reportFilter) {
		  return (Integer) getSqlMapClientTemplate().queryForObject("getInventoryReportCount", reportFilter);
	  }	  
	  public List<ConsignmentReport> getConsignmentReport(Integer supplierId, String sort) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("supplierId", supplierId);
		paramMap.put("sort", sort);
		return getSqlMapClientTemplate().queryForList("getConsignmentReport", paramMap);
	  }
	  public List<ConsignmentReport> getConsignmentSales(ConsignmentReport salesReport) {
		  return getSqlMapClientTemplate().queryForList("getConsignmentSales", salesReport);
	  }
	  public List<VbaPaymentReport> getVbaPaymentReport(VbaPaymentReport reportFilter) {
		  if(reportFilter.getReportView()!= null && reportFilter.getReportView().equalsIgnoreCase("consignment")) {
				return getSqlMapClientTemplate().queryForList("getVbaPaymentConsignmentReport", reportFilter);			  
		  }	else {
			  return getSqlMapClientTemplate().queryForList("getVbaPaymentAffiliateReport", reportFilter);
		  }
	  }	
	  public List<CustomerCreditHistory> getCustomerCreditHistory(Integer supplierId, Integer userId) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put("userId", userId);
		  return getSqlMapClientTemplate().queryForList("getCustomerCreditHistory", paramMap);			  
	  }
	  
	  public List<AbandonedShoppingCart> getAbandonedShoppingCartList(AbandonedShoppingCartSearch abandonedShoppingCartSearch) {
			return getSqlMapClientTemplate().queryForList("getAbandonedShoppingCartListBySearch", abandonedShoppingCartSearch);
	  }
	  public List<TicketReport> getTicketReport(ReportFilter reportFilter){
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  return getSqlMapClientTemplate().queryForList("getTicketReport", reportFilter);
	  }
	  public List<Report> getCustomeReportDaily(ReportFilter reportFilter){
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  return getSqlMapClientTemplate().queryForList("getCustomerReportDaily", reportFilter);
	  }
	  public List<ProductReport> getBestSellingProducts(ReportFilter reportFilter){
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  return getSqlMapClientTemplate().queryForList("getBestSellingProducts", reportFilter);
	  }
	  public List<ProductReport> getTopAbandonedProducts(ReportFilter reportFilter){
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  return getSqlMapClientTemplate().queryForList("getTopAbandonedProducts", reportFilter);
	  }
	  public List<CityReport> getTopShippedCities(ReportFilter reportFilter){
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  return getSqlMapClientTemplate().queryForList("getTopShippedCities", reportFilter);
	  }
	  public List<StateReport> getTopShippedStates(ReportFilter reportFilter){
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  return getSqlMapClientTemplate().queryForList("getTopShippedStates", reportFilter);
	  }
	  public List<SalesRepReport> getTopSalesReps(ReportFilter reportFilter){
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  return getSqlMapClientTemplate().queryForList("getTopSalesReps", reportFilter);
	  }
	  public List<Report> getNewCRMContactReportByYear(Integer year) {
		  return getSqlMapClientTemplate().queryForList( "getNewCRMContactReportByYear", year );			  
	  }
	  
	  public List<Report> getNewCustomerReportByYear(Integer year) {
		  return getSqlMapClientTemplate().queryForList( "getNewCustomerReportByYear", year );			  
	  }
}