/*
 * Copyright 2006 Advanced E-Media Solutions 
 * @author Eduardo Asprec
 * @since 09.19.2006
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.mail.MailSender;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.OrderDao;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CustomerBudgetPartner;
import com.webjaguar.model.CustomerReport;
import com.webjaguar.model.GoogleCheckOut;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.PackingList;
import com.webjaguar.model.PackingListSearch;
import com.webjaguar.model.Paypal;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.SpecialPricing;
import com.webjaguar.model.KitParts;
import com.webjaguar.model.UpsOrder;
import com.webjaguar.model.VirtualBank;
import com.webjaguar.model.VirtualBankOrderSearch;
import com.webjaguar.thirdparty.payment.amazon.Iopn;
import com.webjaguar.thirdparty.payment.bankaudi.BankAudi;
import com.webjaguar.thirdparty.payment.ebillme.EBillme;
import com.webjaguar.thirdparty.payment.geMoney.GEMoney;
import com.webjaguar.thirdparty.payment.netcommerce.NetCommerce;
import com.webjaguar.web.domain.Constants;

public class IbatisOrderDao extends SqlMapClientDaoSupport implements OrderDao {

	public void insertOrder(Order order, OrderStatus orderStatus, boolean backendAddedOrder) {
		
		if((order.getOrderType()==null) || (!order.getOrderType().equalsIgnoreCase("auction")))
			order.setDateOrdered( new Timestamp( new Date().getTime() ) );
		
		if ( order.getTrackcode() == null || order.getTrackcode().trim().equals( "" ) ) {
			order.setTrackcode( "" );
		}
		order.setBackendOrder(backendAddedOrder);
		order.setStatus(orderStatus.getStatus());
		
		getSqlMapClientTemplate().insert("insertOrder", order);
		getSqlMapClientTemplate().update("updateOrder", order, 1);
		// insert order status
		orderStatus.setOrderId( order.getOrderId() );
		getSqlMapClientTemplate().insert("insertOrderStatusHistory", orderStatus);
		
		// GE Money
		if (order.getGeMoney() != null && order.getGeMoney().getAcctNumber() != null && order.getGeMoney().getAcctNumber().trim().length() > 0) {
			order.getGeMoney().setClientTransID(order.getOrderId());
			updateGEMoneyPayment(order.getGeMoney(), null); 
		}

		// LinkShare
		if (order.getLinkShare() != null) {
			getSqlMapClientTemplate().update("updateLinkShare", order);
		}
		
		// insert line items
		insertLineItems(order, order.getLineItems());
		
		// work order
		if (order.getWorkOrderNum() != null) {
			getSqlMapClientTemplate().update("completeWorkOrder", order.getWorkOrderNum(), 2);
		}
		
		// subscription
		if (order.getSubscriptionCode() != null) {
			getSqlMapClientTemplate().update("updateLastOrder", order);
			getSqlMapClientTemplate().update("updateWeeklyNextOrder", order.getSubscriptionCode());
			getSqlMapClientTemplate().update("updateMonthlyNextOrder", order.getSubscriptionCode());
			getSqlMapClientTemplate().update("updateYearlyNextOrder", order.getSubscriptionCode());
		}
	}

	public List<Order> getOrdersListByUser(OrderSearch search, boolean quote)
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "search", search);
		paramMap.put( "quote", quote );		
		return getSqlMapClientTemplate().queryForList("getOrdersListByUser", paramMap);
	}
	
	public Integer isThereAnOrderUseThisPromo(String promoCode) {
		 return  (Integer) getSqlMapClientTemplate().queryForObject("isThereAnOrderUseThisPromo", promoCode);
	}
	
	public Integer isThereAnOrderUseThisPromoByCustomer(String promoCode, Integer customerId) {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put( "promoCode", promoCode);
			paramMap.put( "customerId", customerId);	
		 return  (Integer) getSqlMapClientTemplate().queryForObject("isThereAnOrderUseThisPromoByCustomer", paramMap);
	}
	
	public Map<Integer, Integer> getFirstOrderAllUser(){
		return getSqlMapClientTemplate().queryForMap("getFirstOrderAllUser",null,"uid","oid");
	}
	
	public List<Order> getManifestsReportByUser(OrderSearch search, boolean quote)
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "search", search);
		paramMap.put( "quote", quote );		
		List<Order> orders = getSqlMapClientTemplate().queryForList("getOrdersListByUser", paramMap);
		for (Order order: orders) {
			paramMap.put( "orderId", order.getOrderId() );
			order.setLineItems( getSqlMapClientTemplate().queryForList("getLineItemsByOrderIdExport", paramMap));
			for (LineItem lineItem : order.getLineItems()) {
				paramMap.put( "lineNum", lineItem.getLineNumber() );
				lineItem.setProductAttributes( getSqlMapClientTemplate().queryForList("getLineItemAttributes", paramMap));
			}			
		}
		return orders;
	}
	
	public List<Order> getQuotesListByUser(OrderSearch search)
	{
		return getSqlMapClientTemplate().queryForList("getQuotesListByUser", search);
	}
	
	public List<Order> getOrdersListBySupplier(OrderSearch search)
	{
		return getSqlMapClientTemplate().queryForList("getOrdersListBySupplier", search);
	}
	
	public Integer getOrderIdByGoogleOrderId(Long googleOrderId)
	{
		return (Integer) getSqlMapClientTemplate().queryForObject("getOrderIdByGoogleOrderId", googleOrderId );
	}
	
	public Order getOrder(int orderId, String sort)
	{
		Order order = (Order) getSqlMapClientTemplate().queryForObject("getOrder", orderId );
		if ( order != null )
		{
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put( "orderId", order.getOrderId() );
			paramMap.put( "sort", sort );
			order.setLineItems( getSqlMapClientTemplate().queryForList( "getLineItemsByOrderId", paramMap ) );
			JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
			for ( LineItem lineItem : order.getLineItems() )
			{
				paramMap.put( "lineNum", lineItem.getLineNumber() );
				lineItem.setProductAttributes( getSqlMapClientTemplate().queryForList( "getLineItemAttributes", paramMap ) );
				
				lineItem.setPromoAmount();
				
				// custom lines
				Object[] args = {order.getOrderId(), lineItem.getLineNumber()};
				String query = "SELECT custom_line, quantity FROM orders_lineitem_customlines WHERE order_id = ? and line_num = ?";
				SqlRowSet results = jdbcTemplate.queryForRowSet( query, args );
				while (results.next()) {
					lineItem.addCustomLine(results.getString("custom_line"), results.getInt("quantity"));
				}				
				
				if ( lineItem.getQuantity() != lineItem.getProcessed() )
					order.setFulfilled( false );
				if ( (lineItem.getQuantity() == lineItem.getProcessed()) || (lineItem.getProcessed() != 0) )
					order.setProcessed( true );
				
				// serial numbers
				query = "SELECT serial_num FROM orders_lineitem_serialnums WHERE order_id = ? and line_num = ?";
				results = jdbcTemplate.queryForRowSet(query, args);
				while (results.next()) {
					lineItem.addSerialNum(results.getString("serial_num"));
				}
				
				// categoryIds and categoryIdSet
				Map<String, Object> paramMap2 = new HashMap<String, Object>();
				paramMap2.put("productId", lineItem.getProduct().getId());
				paramMap2.put("protectedAccess", Constants.FULL_PROTECTED_ACCESS);
				Product product = (Product) getSqlMapClientTemplate().queryForObject("getProductById", paramMap2);
				if (product != null) {
					lineItem.getProduct().setCatIds(getCategoryIdsByProductId(product.getId()));
				}
				
			}
			order.setStatusHistory( getSqlMapClientTemplate().queryForList( "getStatusHistory", order.getOrderId() ) );
			order.setPaymentHistory( getSqlMapClientTemplate().queryForList( "getCustomerPaymentListByOrder", order.getOrderId() ) );
			order.setPromoAmount();
		}
		return order;
	}
	
	public List<Order> getQBOrdersList(OrderSearch search, String sort)
	{
		// pass all orders to QB
		search.setStatus(null);
		List<Order> orders = getSqlMapClientTemplate().queryForList( "getQBOrdersList", search );

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "sort", sort );

		for (Order order: orders) {
			paramMap.put( "orderId", order.getOrderId() );
			order.setLineItems( getSqlMapClientTemplate().queryForList("getLineItemsByOrderIdExport", paramMap));
			for (LineItem lineItem : order.getLineItems()) {
				paramMap.put( "lineNum", lineItem.getLineNumber() );
				lineItem.setProductAttributes( getSqlMapClientTemplate().queryForList("getLineItemAttributes", paramMap));
			}			
		}
		return orders;
	}
	
	
	
	public List<Order> getOrdersList(OrderSearch search)
	{
		if ( search.getStatus() != null ) {
			if ( search.getStatus().equals( "nxs" ) )
				search.setStatusTemp(" (  status = 'p' OR status = 'pr'  OR status = 'wp' OR status = 'rls' ) ");
			else if ( search.getStatus().equals( "pprps" ) )
				search.setStatusTemp(" ( status = 'p' OR status = 'pr'  OR status = 'ps' ) ");
			else if ( search.getStatus().equals( "pprpss" ) )
				search.setStatusTemp(" ( status = 'p' OR status = 'pr'  OR status = 'ps' or status = 's' ) ");
			else if ( search.getStatus().equals( "ppr" ) )
				search.setStatusTemp(" ( status = 'p' OR status = 'pr'  OR status = 'ars' OR status = 'wp' OR status = 'rls') ");
			else if ( search.getStatus().equals( "pprs" ) )
				search.setStatusTemp(" ( status = 'p' OR status = 'pr' OR status = 's'  OR status = 'wp' OR status = 'rls' ) ");
			else if ( search.getStatus().equals( "prars" )) 
				search.setStatusTemp(" ( status = 'pr'  OR status = 'ars') ");
			else if ( search.getStatus().equals( "" ))
				search.setStatusTemp(" ( status != 'xq' ) ");
			else
				search.setStatusTemp(" ( status = '" + search.getStatus() + "' ) ");
		} else {
			search.setStatusTemp(" ( status != 'xq' ) ");
		}
		return getSqlMapClientTemplate().queryForList( "getOrdersList", search );
	}

	public int getOrdersListCount(OrderSearch search)
	{
		if ( search.getStatus() != null ) {
			if (search.getStatus().equals( "nxs" )) {
				search.setStatusTemp(" ( status = 'p' OR status = 'pr' OR status = 'wp'  OR status = 'rls' ) ");
			} else if ( search.getStatus().equals( "ppr" ) )
				search.setStatusTemp(" ( status = 'p' OR status = 'pr'  OR status = 'ars') ");
			else if ( search.getStatus().equals( "pprs" ) )
				search.setStatusTemp(" ( status = 'p' OR status = 'pr' OR status = 's' OR status = 'wp'  OR status = 'rls') ");
			else if ( search.getStatus().equals( "prars" )) 
				search.setStatusTemp(" ( status = 'pr'  OR status = 'ars') ");
			else if ( search.getStatus().equals( "" ))
				search.setStatusTemp(" ( status != 'xq' ) ");
			else
				search.setStatusTemp(" ( status = '" + search.getStatus() + "' ) ");
		} else {
			search.setStatusTemp(" ( status != 'xq' ) ");
		}
		return (Integer) getSqlMapClientTemplate().queryForObject( "getOrdersListCount", search );
	}	
	
	public int getOrdersCountByStatus(String  status)
	{
		return (Integer) getSqlMapClientTemplate().queryForObject( "getOrdersCountByStatus", status );
	}	
	
	public OrderStatus getLatestStatusHistory(Integer orderId)
	{
		OrderStatus orderStatus = (OrderStatus) getSqlMapClientTemplate().queryForObject( "getLatestStatusHistory", orderId );
		return orderStatus;	
	}
	
	public void updateOrder(Order order)
	{
		getSqlMapClientTemplate().update( "updateOrder", order, 1 );
		getSqlMapClientTemplate().update( "updateOrderLastModified", order.getOrderId(), 1 );
		if (order.getDeletedLineItems() != null) {
			for (LineItem lineItem: order.getDeletedLineItems()) {
				getSqlMapClientTemplate().delete( "deleteLineItem", lineItem );
				getSqlMapClientTemplate().delete( "deleteLineItemAttributes", lineItem );
				getSqlMapClientTemplate().delete( "deleteLineItemCustomLines", lineItem );
				getSqlMapClientTemplate().delete("deleteLineItemSerialNums", lineItem);
			}
		}	
		if (order.getInsertedLineItems() != null) {
			// get last line num
			JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
			Object[] args = { order.getOrderId() };
			int lastLineNum = 0;
			try {
				lastLineNum = jdbcTemplate.queryForInt( "SELECT line_num FROM orders_lineitems WHERE order_id = ? ORDER BY line_num DESC LIMIT 1", args );
			} catch (EmptyResultDataAccessException e) {
				// do nothing
			}
			for (LineItem lineItem: order.getInsertedLineItems()) {
				lineItem.setLineNumber( ++lastLineNum );
			}
			// insert line items
			insertLineItems(order, order.getInsertedLineItems());
		}
		updateLineItems( order );
	}

	private void updateLineItems(final Order order)
	{
		for ( LineItem lineItem : order.getLineItems() ) {
			insertSpecialPricing(lineItem, order.getUserId());
			// serial numbers
			getSqlMapClientTemplate().delete("deleteLineItemSerialNums", lineItem);
			if (lineItem.getSerialNums() != null) {
				getSqlMapClientTemplate().insert("insertLineItemSerialNums", lineItem);
			}
			// Insert orders_lineitem_attributes
						if(lineItem.getNewProductAttributes() != null && !lineItem.getNewProductAttributes().isEmpty()) {
							System.out.println(lineItem.getNewProductAttributes().get(0).getOptionName());

								lineItem.setProductAttributes(lineItem.getNewProductAttributes());
								getSqlMapClientTemplate().insert("insertLineItemAttributes", lineItem);
						}
						Map<String, Object> paramMap = new HashMap<String, Object>();				

						if(lineItem.getProductAttributes() != null && ! lineItem.getProductAttributes().isEmpty()) {
							for(ProductAttribute productAtrribute: lineItem.getProductAttributes()) {
								
								if (productAtrribute.getOptionName()!= null || productAtrribute.getValueName()!=null){
								   getSqlMapClientTemplate().update( "updateLineItemAttributes", productAtrribute);
								} else {
									 getSqlMapClientTemplate().delete("deleteLineItemAttribute", productAtrribute);
								}
							
							}
						}
		}
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String sql = "UPDATE orders_lineitems SET quantity = ?, original_price = ?, percent = ?, discount = ?, unit_price = ?, case_content = ?, packing = ?, taxable = ?, low_inventory_message = ?, product_name = ?, location = ?, attachment = ?  WHERE order_id = ? and line_num = ?";
		jdbcTemplate.batchUpdate( sql, new BatchPreparedStatementSetter()
		{
			public void setValues(PreparedStatement ps, int i) throws SQLException
			{
				LineItem lineItem = (LineItem) order.getLineItems().get( i );
				ps.setInt( 1, (Integer) (lineItem.getQuantity()) );
				if ( lineItem.getOriginalPrice() == null ) {
					ps.setNull( 2, Types.NULL );
				}
				else {
					ps.setDouble( 2, lineItem.getOriginalPrice() );
				}
				ps.setBoolean( 3, lineItem.isPercent() );
				if ( lineItem.getDiscount() == null ) {
					ps.setNull( 4, Types.NULL );
				}
				else {
					ps.setDouble( 4, lineItem.getDiscount() );
				}
				if ( lineItem.getUnitPrice() == null ) {
					ps.setNull( 5, Types.NULL );
				}
				else {
					ps.setDouble( 5, lineItem.getUnitPrice() );
				}
				if ( lineItem.getProduct().getCaseContent() == null ) {
					ps.setNull( 6, Types.NULL );
				}
				else {
					ps.setDouble( 6, lineItem.getProduct().getCaseContent() );
				}
				ps.setString( 7, lineItem.getProduct().getPacking() );
				ps.setBoolean( 8, lineItem.getProduct().isTaxable() );
				ps.setString( 9, lineItem.getLowInventoryMessage() );
				ps.setString( 10, lineItem.getProduct().getName() );
				ps.setString( 11, lineItem.getLocation() );
				ps.setString( 12, lineItem.getAttachment() );
				ps.setInt( 13, (Integer) lineItem.getOrderId() );
				ps.setInt( 14, (Integer) lineItem.getLineNumber() );
			}

			public int getBatchSize()
			{
				return order.getLineItems().size();
			}
		} );
		
		String sqlNew = "UPDATE orders_lineitems SET promo_id = ? , promo_code = ? , promo_discount = ? , promo_percent = ? WHERE order_id = ? AND line_num = ?";
		
		jdbcTemplate.batchUpdate( sqlNew, new BatchPreparedStatementSetter() {

			public void setValues(PreparedStatement ps, int i) throws SQLException {
				LineItem lineItem = (LineItem) order.getLineItems().get( i );
				ps.setInt(1,lineItem.getPromo() != null ? lineItem.getPromo().getPromoId() : Types.NULL);
				ps.setString(2,lineItem.getPromo() != null ? lineItem.getPromo().getTitle() : null);
				Double disc = 0.0;
				if(lineItem.getPromo() != null && lineItem.getPromo().getDiscount() != null) {
					disc = lineItem.getPromo().getDiscount();
				}
				ps.setDouble(3, disc);
							
			ps.setBoolean( 4,(Boolean) (lineItem.getPromo() != null ? lineItem.getPromo().isPercent() : false));
			ps.setInt( 5, (Integer) lineItem.getOrderId() );
			ps.setInt( 6, (Integer) lineItem.getLineNumber() );
			
				
			}

			public int getBatchSize() {
				// TODO Auto-generated method stub
				return order.getLineItems().size();
			}
			
		});

	}
	
	public void updateLineItemOnHand(PackingList p, boolean inventoryHistory,Integer accessUserId) {
		for ( LineItem lineItem : p.getLineItems() )
		{
			if (lineItem.getQuantity() != 0) {
				lineItem.setInventoryQuantity( lineItem.getQuantity() );
				Inventory inventoryObject = new Inventory();
				inventoryObject.setInventory(lineItem.getQuantity() * (-1));
				inventoryObject.setSku(lineItem.getProduct().getSku());
				Inventory inventory  = (Inventory) getSqlMapClientTemplate().queryForObject("getInventory", inventoryObject );
				if(inventory != null && inventory.getInventory() != null) {
					getSqlMapClientTemplate().update( "updateInventory", inventoryObject );
				}						
				if(inventoryHistory && inventory != null && inventory.getInventory() != null) {
					InventoryActivity inventoryActivity = new InventoryActivity();
					inventoryActivity.setSku(lineItem.getProduct().getSku());
					inventoryActivity.setReference(p.getPackingListNumber());
					inventoryActivity.setAccessUserId(accessUserId);
					//System.out.println("-----------------------------------ibatisorderdao line 359--------packingList----------------------" + lineItem.getProduct().getSku() + "-------" + lineItem.getQuantity() + "------------" + inventory.getInventory());
					inventoryActivity.setType("packingList");
					inventoryActivity.setQuantity(lineItem.getQuantity() * (-1));
					inventoryActivity.setInventory(inventory.getInventory() + (lineItem.getQuantity() * (-1)));
					productIventoryHistory(inventoryActivity );
				}
			}
		}
	}

	public void updateOrderPrinted(Order order)
	{
		getSqlMapClientTemplate().update( "updateOrderPrinted", order, 1 );
	}
	
	public void updateOrderApproval(Order order)
	{
		getSqlMapClientTemplate().update( "updateOrderApproval", order, 1 );
	}
	
	public void updateCreditCardPayment(Order order)
	{
		getSqlMapClientTemplate().update( "updateCreditCardPayment", order );
	}
	
	public void insertCreditCardHolderAuthentication(Order order) {
		getSqlMapClientTemplate().insert("insertCreditCardHolderAuthentication", order);
	}

	public void updatePaypal(Paypal paypal, OrderStatus orderStatus)
	{
		getSqlMapClientTemplate().update( "updatePaypal", paypal );
		if ( orderStatus != null )
		{
			insertOrderStatus(orderStatus);
		}
	}

	public void updateGoogleCheckOut(GoogleCheckOut googlecheckOut, OrderStatus orderStatus)
	{
		getSqlMapClientTemplate().update( "updateGoogleCheckOut", googlecheckOut );
		if ( orderStatus != null )
		{
			insertOrderStatus(orderStatus);
		}
	}

	public void cancelBuySafeBond(Order order, OrderStatus orderStatus)
	{
		getSqlMapClientTemplate().update( "cancelBuySafeBond", order );
		if ( orderStatus != null )
		{
			insertOrderStatus(orderStatus);
		}
	}

	public void insertOrderStatus(OrderStatus orderStatus) {
		try{
			getSqlMapClientTemplate().update("insertOrderStatusHistory", orderStatus);
		}catch(Exception ex){
			ex.printStackTrace();
		}
		try{
			getSqlMapClientTemplate().update("updateOrderStatus", orderStatus);
		}catch(Exception ex){	
			ex.printStackTrace();
		}
		if (orderStatus.getDeliveryPerson() != null || orderStatus.getPriority() != null && false) {
			// priority
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
			String query = "UPDATE orders SET order_priority = ? where order_id = ?";
			Object[] args = {orderStatus.getPriority(), orderStatus.getOrderId()};
			jdbcTemplate.update(query, args);			
		}
		if (orderStatus.getDeliveryPerson() != null || orderStatus.getPriority() != null && false) {
			// delivery person
			JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
			String query = "UPDATE orders SET delivery_person = ? where order_id = ?";
			Object[] args = {orderStatus.getDeliveryPerson(), orderStatus.getOrderId()};
			jdbcTemplate.update(query, args);			
		}		
	}
	
	public void insertOrderUserDueDate(Order order) {
		try{
			getSqlMapClientTemplate().update("insertOrderUserDueDate", order);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void updateIsFirstOrder(Order order) {
		try{
			getSqlMapClientTemplate().update("updateIsFirstOrder", order);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	public void updateOrderSubStatus(Order order) {
		try{
			getSqlMapClientTemplate().update("updateOrderSubStatus", order);
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public boolean isUserUsedPromoCode(String promoCode, Integer userId)
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Object[] args = { (String) promoCode, (Integer) userId };
		if ( jdbcTemplate.queryForInt( "SELECT count(1)  FROM orders where promo_code = ? and userid = ?", args ) >= 1 )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public List<String> getSKUsByUserId(Integer userId) 
	{
		return getSqlMapClientTemplate().queryForList( "getSKUsByUserId", userId );
	}
	
	private void insertLineItems(Order order, List<LineItem> lineItems) {
		for ( LineItem lineItem : lineItems )
		{
			insertSpecialPricing(lineItem, order.getUserId());
			lineItem.setOrderId( order.getOrderId() );
			if (lineItem.getSubscriptionInterval() != null) {
				lineItem.setSubscriptionCode(generateSubscriptionCode());
			}
			List<KitParts> kitPartsList = getSqlMapClientTemplate().queryForList( "getKitPartsByKitSku", lineItem.getProduct().getSku() );
			if( kitPartsList != null) {
				lineItem.getProduct().setKitParts(kitPartsList);
			}
			
			// Check if customerSupplier prefix = product prefix then set product as consignment product
			// For viatrading, the LiquidateNow product SKU must contain supplier prefix and must contain  the characters �-LN-�
			if(lineItem.getSupplier() != null) { 
				if( lineItem.getSupplier().getSupplierPrefix() != null && lineItem.getProduct().getSku().toLowerCase().startsWith(lineItem.getSupplier().getSupplierPrefix().toLowerCase() + "-ln-")) {
					lineItem.setConsignmentProduct(true);
				}
			}		
			
			getSqlMapClientTemplate().insert( "insertLineItem", lineItem );
			// save attributes
			if ( lineItem.getProductAttributes() != null && lineItem.getProductAttributes().size() > 0 )
			{
				getSqlMapClientTemplate().update( "insertLineItemAttributes", lineItem );
			}
			if ( lineItem.getAsiProductAttributes() != null && lineItem.getAsiProductAttributes().size() > 0 )
			{
				getSqlMapClientTemplate().update( "insertLineItemAsiAttributes", lineItem );
			}
			if ( lineItem.getProduct().getKitParts() != null ) 
			{
				Map<String, Object> paramMap = new HashMap<String, Object>();				
				for ( KitParts kitParts : lineItem.getProduct().getKitParts()) {
					paramMap.put( "orderId", order.getOrderId() );
					paramMap.put( "lineNum", lineItem.getLineNumber() );
					paramMap.put( "kitSku", lineItem.getProduct().getSku() );
					paramMap.put( "kitPartSku", kitParts.getKitPartsSku() );
					paramMap.put( "quantity", kitParts.getQuantity() * lineItem.getQuantity());
					getSqlMapClientTemplate().update( "insertLineItemKit", paramMap );
				}				
			}
			// save custom lines
			if ( lineItem.getCustomLines() != null && lineItem.getCustomLines().size() > 0)
			{
				Map<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put( "orderId", order.getOrderId() );
				paramMap.put( "lineNum", lineItem.getLineNumber() );
				for (String customLine: lineItem.getCustomLines().keySet()) {
					paramMap.put("key", customLine);
					paramMap.put("value", lineItem.getCustomLines().get(customLine));	    	  
					getSqlMapClientTemplate().insert("insertLineItemCustomLine", paramMap);			  
				}
			}
			// insert subscription
			if (lineItem.getSubscriptionCode() != null) {
				getSqlMapClientTemplate().insert("insertSubscription", lineItem);
				getSqlMapClientTemplate().insert("insertSubscriptionAttributes", lineItem);
				getSqlMapClientTemplate().update("updateWeeklyNextOrder", lineItem.getSubscriptionCode());
				getSqlMapClientTemplate().update("updateMonthlyNextOrder", lineItem.getSubscriptionCode());
				getSqlMapClientTemplate().update("updateYearlyNextOrder", lineItem.getSubscriptionCode());
			}
			
			// serial numbers
			if (lineItem.getSerialNums() != null) {
				getSqlMapClientTemplate().insert("insertLineItemSerialNums", lineItem);
			}
		}		
	}
	
	private void insertSpecialPricing( LineItem lineItem, int userId ) {
		if ( lineItem.isSetSpecialPricing() && lineItem.getUnitPrice() != null ) {
			SpecialPricing sp = new SpecialPricing(lineItem.getProduct().getSku(), userId, lineItem.getUnitPrice(), true);
			try { // special pricing Foreign Key
				getSqlMapClientTemplate().insert("insertSpecialPricing", sp );
			} catch (Exception e) 
			{}// do nothing
		}
	}
	
	public int invoiceCount(OrderSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("invoiceCount", search);
	}
	
	public List<Order> getInvoiceExportList(OrderSearch search, String sort) {
		if ( search.getStatus() != null ) {
			if ( search.getStatus().equals( "ppr" ) )
				search.setStatusTemp(" ( status = 'p' OR status = 'pr' OR status = 'ars' ) AND (status != 'xq')");
			else if ( search.getStatus().equals( "pprs" ) )
				search.setStatusTemp(" ( status = 'p' OR status = 'pr' OR status = 's' OR status = 'rls' OR status = 'wp') AND (status != 'xq')");
			else if ( search.getStatus().equals( "prars" )) 
				search.setStatusTemp(" ( status = 'pr'  OR status = 'ars') AND (status != 'xq')");
			else if ( search.getStatus() == null || search.getStatus().equals( "" ))
				search.setStatusTemp("(status != 'xq')");
			else
				search.setStatusTemp(" ( status = '" + search.getStatus() + "' ) ");
		} else {
			search.setStatusTemp(" ( status != 'xq' ) ");
		}
		List<Order> orders = getSqlMapClientTemplate().queryForList("getInvoiceExportList", search);

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "sort", sort );

		for (Order order: orders) {
			paramMap.put( "orderId", order.getOrderId() );
			order.setLineItems( getSqlMapClientTemplate().queryForList("getLineItemsByOrderIdExport", paramMap));
			for (LineItem lineItem : order.getLineItems()) {
				paramMap.put( "lineNum", lineItem.getLineNumber() );
				lineItem.setProductAttributes( getSqlMapClientTemplate().queryForList("getLineItemAttributes", paramMap));
			}			
		}
		return orders;
	}
	
	public List<PackingList> getExportPackingList(PackingListSearch search) {
		List<PackingList> packingLists = getSqlMapClientTemplate().queryForList("getExportPackingList", search);
		
		for (PackingList packingList: packingLists) {
			packingList.setLineItems( getSqlMapClientTemplate().queryForList( "getPackingListLineItemsByPackingNum", packingList.getPackingListNumber() ) );
		}
		return packingLists;
	}
	
	public void updateNetCommercePayment(NetCommerce trans, OrderStatus orderStatus) {
		getSqlMapClientTemplate().update( "updateNetCommercePayment", trans );
		if ( orderStatus != null )
		{
			insertOrderStatus(orderStatus);
		}		
	}
	
	public void updatePdfOrder(Order order) {
		getSqlMapClientTemplate().update("updatePdfOrder", order);
	}
	
	public void insertPackingList(PackingList packingList, Integer accessUserId, boolean inventoryHistory, Order order)
	{
		getSqlMapClientTemplate().insert( "insertPackingList", packingList );
		// insert line items
		insertPackingListLineItems(packingList, accessUserId, inventoryHistory, order);
	}
	
	private void insertPackingListLineItems(PackingList packingList, Integer accessUserId, boolean inventoryHistory, Order order) {
		for ( LineItem lineItem : packingList.getLineItems() )
		{
			if (lineItem.getQuantity() != 0) {
				lineItem.setPackingListNumber( packingList.getPackingListNumber() );
				getSqlMapClientTemplate().insert( "insertPackingListLineItem", lineItem );
				updateLineItemProcessed(lineItem);
				if(packingList.getShipped()!=null){
					lineItem.setInventoryQuantity( lineItem.getQuantity() );
					Inventory inventoryObject = new Inventory();
					inventoryObject.setInventory(lineItem.getQuantity() * (-1));
					inventoryObject.setSku(lineItem.getProduct().getSku());
					getSqlMapClientTemplate().update("updateInventory", inventoryObject);		
					if(order.getStatus().equalsIgnoreCase("s")){
						inventoryObject.setInventoryAFS(lineItem.getQuantity() * (-1));
						getSqlMapClientTemplate().update("updateinventoryAFS", inventoryObject);	
					}
					Inventory inventory  = (Inventory)  getSqlMapClientTemplate().queryForObject("getInventory", inventoryObject);
					if(inventoryHistory && inventory != null && inventory.getInventory() != null) {
						InventoryActivity inventoryActivity = new InventoryActivity();
						inventoryActivity.setSku(lineItem.getProduct().getSku());
						inventoryActivity.setReference(packingList.getPackingListNumber());
						inventoryActivity.setAccessUserId(accessUserId);
						inventoryActivity.setType("packingList");
						//System.out.println("-----------------------------------ibatisorderdao line 619--------packingList----------------------" + lineItem.getProduct().getSku() + "-------" + lineItem.getQuantity() + "------------" + inventory.getInventory());
						inventoryActivity.setQuantity(lineItem.getQuantity() * (-1));
						inventoryActivity.setInventory(inventory.getInventory());
						
						getSqlMapClientTemplate().insert( "productIventoryHistory", inventoryActivity );
					}
				}
			}
		}		
	}

	public void insertPackingList(PackingList packingList, Integer accessUserId, boolean inventoryHistory)
	{
		getSqlMapClientTemplate().insert( "insertPackingList", packingList );
		// insert line items
		insertPackingListLineItems(packingList, accessUserId, inventoryHistory);
	}
	
	private void insertPackingListLineItems(PackingList packingList, Integer accessUserId, boolean inventoryHistory) {
		for ( LineItem lineItem : packingList.getLineItems() )
		{
			if (lineItem.getQuantity() != 0) {
				lineItem.setPackingListNumber( packingList.getPackingListNumber() );
				getSqlMapClientTemplate().insert( "insertPackingListLineItem", lineItem );
				updateLineItemProcessed(lineItem);
				if(packingList.getShipped()!=null){
					lineItem.setInventoryQuantity( lineItem.getQuantity() );
					Inventory inventoryObject = new Inventory();
					inventoryObject.setInventory(lineItem.getQuantity() * (-1));
					inventoryObject.setSku(lineItem.getProduct().getSku());
					getSqlMapClientTemplate().update("updateInventory", inventoryObject);				
					
					Inventory inventory  = (Inventory)  getSqlMapClientTemplate().queryForObject("getInventory", inventoryObject);
					if(inventoryHistory && inventory != null && inventory.getInventory() != null) {
						InventoryActivity inventoryActivity = new InventoryActivity();
						inventoryActivity.setSku(lineItem.getProduct().getSku());
						inventoryActivity.setReference(packingList.getPackingListNumber());
						inventoryActivity.setAccessUserId(accessUserId);
						inventoryActivity.setType("packingList");
						//System.out.println("-----------------------------------ibatisorderdao line 619--------packingList----------------------" + lineItem.getProduct().getSku() + "-------" + lineItem.getQuantity() + "------------" + inventory.getInventory());
						inventoryActivity.setQuantity(lineItem.getQuantity() * (-1));
						inventoryActivity.setInventory(inventory.getInventory());
						
						getSqlMapClientTemplate().insert( "productIventoryHistory", inventoryActivity );
					}
				}
			}
		}		
	}
	
	public List<LineItem> getLineItemByOrderId(Integer orderId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "orderId", orderId );
		paramMap.put( "sort", null );
		return getSqlMapClientTemplate().queryForList( "getLineItemsByOrderId", paramMap );
	}
	
	public List<PackingList> getPackingListByOrderId( PackingListSearch packingListSearch )
	{
		return getSqlMapClientTemplate().queryForList( "getPackingListByOrderId", packingListSearch );
	}
	
	public void deletePackingList(Integer orderId, String packingNumber, Integer accessUserId, boolean inventoryHistory)
	{
		Order order = getOrder(orderId, null);
		List<LineItem> list = getSqlMapClientTemplate().queryForList( "getPackingListLineItemsByPackingNum", packingNumber );
		for ( LineItem lineItem : list ) {
			lineItem.setQuantity( lineItem.getQuantity() * (-1) );
			lineItem.setOrderId( orderId );
			updateLineItemProcessed(lineItem);
		}
		if (getPackingList(orderId, packingNumber) != null && getPackingList(orderId, packingNumber).getShipped() != null) {
			for ( LineItem lineItem : list  ) {
				Inventory inventoryObject = new Inventory();
				inventoryObject.setInventory(lineItem.getQuantity() * (-1) );
				inventoryObject.setSku(lineItem.getProduct().getSku());
				getSqlMapClientTemplate().update("updateInventory", inventoryObject);
				if(order.getStatus().equalsIgnoreCase("s")){
					inventoryObject.setInventoryAFS(lineItem.getQuantity()*(-1));
					getSqlMapClientTemplate().update("updateinventoryAFS", inventoryObject);
				}
				Inventory inventory  = (Inventory) getSqlMapClientTemplate().queryForObject("getInventory", inventoryObject );
				if(inventoryHistory && inventory != null && inventory.getInventory() != null) {
					InventoryActivity inventoryActivity = new InventoryActivity();
					inventoryActivity.setSku(lineItem.getProduct().getSku());
					inventoryActivity.setReference(packingNumber);
					inventoryActivity.setAccessUserId(accessUserId);
					inventoryActivity.setType("packingList");
					//System.out.println("-----------------------------------ibatisdao line 664--------packingList----------------------" + lineItem.getProduct().getSku() + "-------" + lineItem.getQuantity() + "------------" + inventory.getInventory());
					inventoryActivity.setQuantity(lineItem.getQuantity() * (-1));
					inventoryActivity.setInventory(inventory.getInventory());
					
					getSqlMapClientTemplate().insert( "productIventoryHistory", inventoryActivity );
				}
			}
		}
		getSqlMapClientTemplate().delete( "deletePackingList", packingNumber, 1 );
	}
	
	public PackingList getPackingList(Integer orderId, String packingNumber)
	{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "orderId", orderId );
		paramMap.put( "packingNumber", packingNumber );
		PackingList packingList = (PackingList) getSqlMapClientTemplate().queryForObject( "getPackingListByOrderIdPackingNum", paramMap );
		if ( packingList != null ) {
			packingList.setLineItems( getSqlMapClientTemplate().queryForList( "getPackingListLineItemsByPackingNum", packingNumber ) );
		}
		return packingList;	
	}

	public void updatePackingList(PackingList packingList) {
		getSqlMapClientTemplate().update( "updatePackingList", packingList, 1 );
	}
	
	// adds ship date to packinglist without shipdate and adjusts on hand
	public void ensurePackingListShipDate(Order order, boolean inventoryHistory,Integer accessUserId ) {
		PackingListSearch packingListSearch = new PackingListSearch();
		packingListSearch.setOrderId(order.getOrderId());
		for (PackingList packingList : getPackingListByOrderId(packingListSearch) ){
			if(packingList.getShipped() == null ) {
				packingList.setShipped(new Date());
				updatePackingList(packingList);
				packingList.setLineItems(getPackingListLineItemsByPackingNum(packingList.getPackingListNumber()));
				updateLineItemOnHand( packingList, inventoryHistory,accessUserId);
			}
		}
	}
	
	public void productIventoryHistory(InventoryActivity inventoryActivity ) {
		getSqlMapClientTemplate().insert( "productIventoryHistory", inventoryActivity );
	}
	
	public List<LineItem> getPackingListLineItemsQuantity(Integer orderId) {
		return  getSqlMapClientTemplate().queryForList( "getPackingListLineItemsQuantity", orderId );
	}
	public List<LineItem> getPackingListLineItemsByPackingNum(String packingNumber) {
		return  getSqlMapClientTemplate().queryForList( "getPackingListLineItemsByPackingNum", packingNumber );
	}
	
	public int packingListCount(Integer orderId) {
		return (Integer) getSqlMapClientTemplate().queryForObject("packingListCount", orderId);
	}
	
	public Date getShipDateByOrderId(Integer orderId) {
		return (Date) getSqlMapClientTemplate().queryForObject("getShipDateByOrderId", orderId);
	}
	
	public List<String> getPackisListNumByOrderId(int orderId){
		return   getSqlMapClientTemplate().queryForList("getPackisListNumByOrderId", orderId);
	}
	
	public void updateLineItemProcessed(LineItem lineItem)
	{	
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "orderId", lineItem.getOrderId() );
		paramMap.put( "lineNumber", lineItem.getLineNumber() );
		paramMap.put( "delta", lineItem.getQuantity() );
		getSqlMapClientTemplate().update( "updateLineItemProcessed", paramMap );
	}
	
	private String generateSubscriptionCode() {
		do {
			StringBuffer code = new StringBuffer(UUID.randomUUID().toString().toUpperCase().replace("-", "").substring(0, 16));
			code.insert(5, "-");
			code.insert(12, "-");	
			if (getSqlMapClientTemplate().queryForObject("getSubscription", code.toString()) == null) {
				return code.toString();				
			}
		} while (true);
	}
	public Order getLastOrderByUserId(Integer userId) {
		return (Order) getSqlMapClientTemplate().queryForObject("getLastOrderByUserId", userId);
	}
	public Integer getCustomerOrdersNumber(Integer userId) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCustomerOrdersNumber", userId);
	}
	public List<CustomerReport> getCustomerQuickViewList(Integer userId) {
		return  getSqlMapClientTemplate().queryForList( "getCustomerQuickViewList", userId );
	}
	
	public List<Integer> getCustomersWithOrderCount(OrderSearch search) {
		return getSqlMapClientTemplate().queryForList( "getCustomersWithOrderCount", search);
	}
	
	public void updateOrdersPaymentStatus(int orderId, String status, Date date) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "orderId", orderId );
		paramMap.put( "status",  status);
		paramMap.put( "date",  date);
		getSqlMapClientTemplate().update( "updateOrdersPaymentStatus", paramMap );
	}
	public void updateInvoiceExported (Set<Integer> orderIds) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "orderIds", orderIds.toArray() );
		getSqlMapClientTemplate().update( "updateInvoiceExported", paramMap );
	}
	//orderLookup
	 public  List<UpsOrder> getOrderLookupReport(String customerPO) {
		  return getSqlMapClientTemplate().queryForList("getOrderLookupReport", customerPO);
	  }
	 public void importUpsOrders(UpsOrder upsWorldShip) {
		  getSqlMapClientTemplate().insert( "importUpsOrders", upsWorldShip );
	  }
	 public void updateUpsOrders(UpsOrder upsWorldShip) {
		  getSqlMapClientTemplate().update( "updateUpsOrders", upsWorldShip );
	  }
	 public void updateUpsOrdersTrackAndShipDate(UpsOrder upsWorldShip) {
		  getSqlMapClientTemplate().update( "updateUpsOrdersTrackAndShipDate", upsWorldShip );
	  }
	
	// mas90, dsi, triplefin
	public void updateOrderExported(String column_name, Date endDate, Set<Integer> orderIds) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("column_name", column_name);
		paramMap.put("endDate", endDate);
		paramMap.put("orderIds", (orderIds == null) ? null : orderIds.toArray());
		getSqlMapClientTemplate().update("updateOrderExported", paramMap);
	}
	
	public void updateLineItemShipping(LineItem lineItem) {
		getSqlMapClientTemplate().update("updateLineItemShipping", lineItem);
	}
	
	public boolean isValidOrderId( String orderId )
	{
		try {
			Integer intOrderId = Integer.parseInt( orderId );
			JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
			Object[] args = { (Integer) intOrderId };
			if ( jdbcTemplate.queryForInt( "SELECT count(1) FROM orders where order_id = ? ", args ) == 1 )
			{
				return true;
			}
			else
			{
				return false;
			}
		} catch (Exception e) {
			return false;
		}
		
	}
	
	public void updateEvergreenOrderSuc(int orderId, String suc) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE orders SET export_success = ? where order_id = ?";
		Object[] args = {suc, orderId};
		jdbcTemplate.update(query, args);
	}
	
	// custom frame
	public LineItem getCustomFrame(int orderId, int lineNum) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("orderId", orderId);
		paramMap.put("lineNum", lineNum);
		return (LineItem) getSqlMapClientTemplate().queryForObject("getCustomFrame", paramMap);
	}
	
	public String getEvergreenOrderSuc(int orderId) {
    	JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
    	Object[] args = { orderId };
    	return (String) jdbcTemplate.queryForObject("SELECT export_success FROM orders WHERE order_id = ?", args, String.class);		
	}
	
	public void updateEbillmePayment(EBillme eBillme, OrderStatus orderStatus) {
		getSqlMapClientTemplate().update("updateEbillmePayment", eBillme);
		if (orderStatus != null) {
			insertOrderStatus(orderStatus);
		}		
	}
	
	public void updateEbillmePayment(EBillme eBillme) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE orders SET eb_xml_file = concat(?, \",\", IFNULL(eb_xml_file, \"\")) where eb_orderrefid = ?";
		Object[] args = {eBillme.getXmlFile(), eBillme.getOrderrefid()};
		jdbcTemplate.update(query, args);
	}

	// amazon
	public void updateAmazonPayment(Iopn iopn, OrderStatus orderStatus) {
		getSqlMapClientTemplate().update("updateAmazonPayment", iopn);
		if (orderStatus != null) {
			insertOrderStatus(orderStatus);
		}		
	}
	public void updateAmazonLineItem(List<LineItem> lineItems) {
		for (LineItem lineItem: lineItems) {
			getSqlMapClientTemplate().update("updateAmazonLineItem", lineItem);			
		}
	}
	
	// bank audi
	public void updateBankAudiPayment(BankAudi trans, OrderStatus orderStatus) {
		getSqlMapClientTemplate().update("updateBankAudiPayment", trans);
		if (orderStatus != null) {
			insertOrderStatus(orderStatus);
		}		
	}
	
	// mas200
	public void updateMas200order(Order order) {
		getSqlMapClientTemplate().update("updateMas200order", order);
	}
	
	// GE Money
	public void updateGEMoneyPayment(GEMoney trans, OrderStatus orderStatus) {
		getSqlMapClientTemplate().update("updateGEMoneyPayment", trans);
		if (orderStatus != null) {
			insertOrderStatus(orderStatus);
		}		
	}
	
	// Plow & Hearth
	public void updatePlowAndHearthLineItems(List<LineItem> lineItems) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE orders SET external_order_id = ? where order_id = ?";
		for (LineItem lineItem: lineItems) {
			getSqlMapClientTemplate().update("updatePlowAndHearthLineItem", lineItem);			
			Object[] args = { lineItem.getExternalOrderId(), lineItem.getOrderId() };
			jdbcTemplate.update(query, args);
		}
	}
	
	// VBA		
	public List<VirtualBank> getConsignmentAndAffiliateOrders(VirtualBankOrderSearch orderSearch) {		
		if ( orderSearch.getOrderStatus() != null ) {
			if ( orderSearch.getOrderStatus().equals( "ppr" ) )
				orderSearch.setStatusTemp(" ( status = 'p' OR status = 'pr'  OR status = 'ars' ) ");
			else if ( orderSearch.getOrderStatus().equals( "pprs" ) )
				orderSearch.setStatusTemp(" ( status = 'p' OR status = 'pr' OR status = 's' ) ");
			else if ( orderSearch.getOrderStatus().equals( "prars" )) 
				orderSearch.setStatusTemp(" ( status = 'pr'  OR status = 'ars') ");
			else if ( orderSearch.getOrderStatus().equals( "" ))
				orderSearch.setStatusTemp(" ( status != 'xq' ) ");
			else
				orderSearch.setStatusTemp(" ( status = '" + orderSearch.getOrderStatus() + "' ) ");
		} else {
			orderSearch.setStatusTemp(" ( status != 'xq' ) ");
		}		
		return getSqlMapClientTemplate().queryForList( "getConsignmentAndAffiliateOrders",orderSearch );
	}
	public void updateVbaOrders(List<Order> vbaOrders, List<LineItem> vbaOrdersLineItems, String userName) {
		//Affiliate
		for(Order order: vbaOrders) {
			getSqlMapClientTemplate().update("updateAffiliateOrders", order);
		}
		//Consignment
		for (LineItem lineItem: vbaOrdersLineItems) {
			getSqlMapClientTemplate().update("updateConsignmentOrders", lineItem );
		}		
	}
	
	//Budget
	public void insertOrderAprrovalDenialHistory(Order order, List<Integer> parentsList, MailSender mailSender, Map<String, Configuration> siteConfig) {
		getSqlMapClientTemplate().insert("insertOrderAction" , order);
	}
	
	public List<Order> getOrderActionHistory(Integer orderId) {
		return getSqlMapClientTemplate().queryForList("getOrderActionHistory", orderId);
	}
	
	public boolean getApproval(Integer orderId, Integer actionBy, boolean fullStatus) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("actionBy", actionBy);
		paramMap.put("orderId", orderId);
		Integer value = (Integer) getSqlMapClientTemplate().queryForObject("getApproval", paramMap);
		Integer approvedValue = (Integer) getSqlMapClientTemplate().queryForObject("getApprovedValue", paramMap);
		if(fullStatus && (value >= 1)) {
			return true;
		} else if (!fullStatus && (approvedValue >= 1)) {
			return true;
		} else {
			return false;
		}
	}
	
	public void updateGatewayToken(String gatewayToken, Integer id){
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { gatewayToken, id };
		jdbcTemplate.update("UPDATE users SET gateway_token = ? where id = ?", args);
	}
	

	// Get customer's order balance
	public Double getCustomerOrderBalance(Integer userId,Date dateOrdered){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "userId", userId);
		paramMap.put( "dateOrdered", dateOrdered );		
		return(Double) getSqlMapClientTemplate().queryForObject("getCustomerOrderBalance",paramMap);
	}
	
	public Double getOrderAmountPaid( Integer orderId){
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = {orderId};
		
		return jdbcTemplate.queryForObject("SELECT amount_paid FROM orders WHERE order_id = ?", args, Double.class);
	}
	
	public void updateOrderPaymentAlert(boolean paymentAlert, Integer orderId){
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { paymentAlert, orderId};
		jdbcTemplate.update("UPDATE orders SET payment_alert = ? where order_id = ?", args);
	}
	
	public List<CustomerBudgetPartner> getCustomerPartnerHistory(CustomerBudgetPartner partner){
		return getSqlMapClientTemplate().queryForList("getCustomerPartnerHistory", partner);
	}
	public void updateCustomerPartnerHistory(CustomerBudgetPartner partner) {
		getSqlMapClientTemplate().update("updateCustomerPartnerHistory", partner );
	}
	
	public void updateOrderQuote(Integer orderId, String quote){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "orderId", orderId);
		paramMap.put( "quote", quote);
		getSqlMapClientTemplate().update("updateOrderQuote", paramMap);
	}
	
	public String getOrderQuote(Integer orderId){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "orderId", orderId);	
		return(String) getSqlMapClientTemplate().queryForObject("getOrderQuote",paramMap);
	}
	
	public Set<Object> getCategoryIdsByProductId(Integer productId) throws DataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { productId };
		Set<Object> categoryIds = new TreeSet<Object>();
		String query = "SELECT category_id FROM category_product where product_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(query, args);
		while (results.next()) {
			categoryIds.add(new Integer(results.getInt("category_id")));
		}
		return categoryIds;
	}

}
