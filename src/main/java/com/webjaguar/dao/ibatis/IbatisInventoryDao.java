/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.webjaguar.dao.InventoryDao;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.LineItem;

import com.webjaguar.model.PurchaseOrder;
import com.webjaguar.model.PurchaseOrderStatus;
import com.webjaguar.model.PurchaseOrderLineItem;
import com.webjaguar.model.PurchaseOrderSearch;
import com.webjaguar.web.form.PurchaseOrderForm;

public class IbatisInventoryDao extends SqlMapClientDaoSupport implements InventoryDao {
	
	public void insertPurchaseOrder(PurchaseOrderForm purchaseOrderForm) {
		purchaseOrderForm.getPurchaseOrder().setCreated( new Timestamp( new Date().getTime() ) );
		getSqlMapClientTemplate().insert( "insertPurchaseOrder", purchaseOrderForm.getPurchaseOrder() );
		getSqlMapClientTemplate().update( "updatePurchaseOrder", purchaseOrderForm.getPurchaseOrder(), 1 );

		// insert order status
		if ( purchaseOrderForm.isNewPurchaseOrder()) {
			purchaseOrderForm.getPurchaseOrderStatus().setPoId( purchaseOrderForm.getPurchaseOrder().getPoId() );
			purchaseOrderForm.getPurchaseOrderStatus().setUpdatedBy( purchaseOrderForm.getPurchaseOrder().getOrderBy() );
			getSqlMapClientTemplate().insert( "insertPurchaseOrderHistoryStatus", purchaseOrderForm.getPurchaseOrderStatus() );
		}
		// insert line items
		insertLineItems(purchaseOrderForm.getPurchaseOrder(), purchaseOrderForm.isNewPurchaseOrder());
		if ( purchaseOrderForm.getPurchaseOrder().isDropShip() )
			updateLineItemProcessed(purchaseOrderForm);		
	}
	
	private void insertLineItems(PurchaseOrder purchaseOrder, Boolean isNewPurchaseOrder) {
		for ( PurchaseOrderLineItem lineItem : purchaseOrder.getPoLineItems()) {
			lineItem.setPoId( purchaseOrder.getPoId() );
			getSqlMapClientTemplate().insert( "insertPOLineItem", lineItem );
			if(isNewPurchaseOrder && !purchaseOrder.isDropShip()){
				Inventory lineInventory = new Inventory(); 	
				lineInventory.setSku(lineItem.getSku());
				lineInventory.setInventoryAFS(lineItem.getQty());
				updateInventory(lineInventory);
				
				
				InventoryActivity inventoryActivity = new InventoryActivity();
				Inventory inventory = new Inventory();
				inventory.setSku(lineItem.getSku());
				 inventory  = (Inventory) getSqlMapClientTemplate().queryForObject("getInventory", inventory);
				if (inventory!=null && inventory.getInventory()!=null) {
					inventoryActivity.setSku(lineItem.getSku());
					inventoryActivity.setReference(lineItem.getPoId().toString());
					inventoryActivity.setAccessUserId(purchaseOrder.getAccessUserId());
					inventoryActivity.setType("PO created/updated");
					inventoryActivity.setQuantity(lineItem.getQty());
					inventoryActivity.setInventory(inventory.getInventory());
				   getSqlMapClientTemplate().insert( "productIventoryHistory", inventoryActivity );
				}
				
				System.out.println("insertLineItems for PO and update inventory history -1 ");
				
			}
		}
		for ( PurchaseOrderLineItem lineItem : purchaseOrder.getPoInsertedLineItems()) {
			lineItem.setPoId( purchaseOrder.getPoId() );
			getSqlMapClientTemplate().insert( "insertPOLineItem", lineItem );
			Inventory lineInventory = new Inventory(); 	
			lineInventory.setSku(lineItem.getSku());
			lineInventory.setInventoryAFS(lineItem.getQty());
			updateInventory(lineInventory);
			
			InventoryActivity inventoryActivity = new InventoryActivity();
			Inventory inventory = new Inventory();
			inventory.setSku(lineItem.getSku());
			 inventory  = (Inventory) getSqlMapClientTemplate().queryForObject("getInventory", inventory );
			if (inventory.getInventory()!=null) {
				inventoryActivity.setSku(lineItem.getSku());
				inventoryActivity.setReference(lineItem.getPoId().toString());
				inventoryActivity.setAccessUserId(purchaseOrder.getAccessUserId());
				inventoryActivity.setType("PO updated/added");
				inventoryActivity.setQuantity(lineItem.getQty());
				inventoryActivity.setInventory(inventory.getInventory());
			   getSqlMapClientTemplate().insert( "productIventoryHistory", inventoryActivity );
			}
			
			System.out.println("insertLineItems for already created PO and update inventory history -2 ");

		}		
	}
	
	public void updatePurchaseOrder(PurchaseOrderForm purchaseOrderForm) {
		getSqlMapClientTemplate().update( "updatePurchaseOrder", purchaseOrderForm.getPurchaseOrder(), 1 );
		if (purchaseOrderForm.getPurchaseOrder().getPoDeletedLineItems() != null && !purchaseOrderForm.getPurchaseOrder().getPoDeletedLineItems().isEmpty()) {
			for (PurchaseOrderLineItem lineItem: purchaseOrderForm.getPurchaseOrder().getPoDeletedLineItems()) {
				getSqlMapClientTemplate().delete( "deletePOLineItem", lineItem );
				Inventory lineInventory = new Inventory(); 	
				lineInventory.setSku(lineItem.getSku());
				lineInventory.setInventoryAFS(-lineItem.getQty());
				updateInventory(lineInventory);
			}
		}	
		if ( !purchaseOrderForm.getPurchaseOrder().getPoInsertedLineItems().isEmpty() ) {
			// get last line num
			JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
			Object[] args = { purchaseOrderForm.getPurchaseOrder().getPoId() };
			int lastLineNum = 0;
			try {
				lastLineNum = jdbcTemplate.queryForInt( "SELECT line_number FROM purchase_order_lineitems WHERE po_id = ? ORDER BY line_number DESC LIMIT 1", args );
			} catch (EmptyResultDataAccessException e) {
				// do nothing
			}
			for (PurchaseOrderLineItem lineItem: purchaseOrderForm.getPurchaseOrder().getPoInsertedLineItems()) {
				lineItem.setLineNumber( ++lastLineNum );
			}
			// insert line items
			insertLineItems(purchaseOrderForm.getPurchaseOrder(), purchaseOrderForm.isNewPurchaseOrder());
		}
		// update line items
		updateLineItems(purchaseOrderForm);
		
		// update line items processed
		updateLineItemProcessed(purchaseOrderForm);
	}
	
	private void updateLineItemProcessed(final PurchaseOrderForm purchaseOrderForm) {
		if ( !purchaseOrderForm.getPurchaseOrder().getStatus().equals( "rece" )) {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			for ( PurchaseOrderLineItem lineItem : purchaseOrderForm.getPurchaseOrder().getPoLineItems()) {
				paramMap.put( "orderId", purchaseOrderForm.getPurchaseOrder().getOrderId() );
				paramMap.put( "lineNumber", lineItem.getLineNumber() );
				paramMap.put( "delta", lineItem.getQty() - lineItem.getDeltaQty() );
				getSqlMapClientTemplate().update( "updateLineItemProcessed", paramMap );
			}
			for ( PurchaseOrderLineItem lineItem : purchaseOrderForm.getPurchaseOrder().getPoInsertedLineItems()) {
				paramMap.put( "orderId", purchaseOrderForm.getPurchaseOrder().getOrderId() );
				paramMap.put( "lineNumber", lineItem.getLineNumber() );
				paramMap.put( "delta", lineItem.getQty() - lineItem.getDeltaQty() );
				getSqlMapClientTemplate().update( "updateLineItemProcessed", paramMap );
			}
			for ( PurchaseOrderLineItem lineItem : purchaseOrderForm.getPurchaseOrder().getPoDeletedLineItems()) {
				paramMap.put( "orderId", purchaseOrderForm.getPurchaseOrder().getOrderId() );
				paramMap.put( "lineNumber", lineItem.getLineNumber() );
				paramMap.put( "delta", 0 - lineItem.getDeltaQty() );
				getSqlMapClientTemplate().update( "updateLineItemProcessed", paramMap );
			} 
			if(purchaseOrderForm != null && purchaseOrderForm.getPurchaseOrder().isDropShip() && purchaseOrderForm.getPurchaseOrderStatus().getStatus().equals("x")){
				for ( PurchaseOrderLineItem lineItem : purchaseOrderForm.getPurchaseOrder().getPoLineItems()) {
					paramMap.put( "orderId", purchaseOrderForm.getPurchaseOrder().getOrderId() );
					paramMap.put( "lineNumber", lineItem.getLineNumber() );
					paramMap.put( "delta", 0 - lineItem.getQty());
					getSqlMapClientTemplate().update( "updateLineItemProcessed", paramMap );
				}
			}
		}	
	}
	
	private void updateLineItems(final PurchaseOrderForm purchaseOrderForm) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String sql = "UPDATE purchase_order_lineitems SET sku = ?, supplier_sku = ?, qty = ?, unit_cost = ?, product_name = ? WHERE po_id = ? and line_number = ?";
		jdbcTemplate.batchUpdate( sql, new BatchPreparedStatementSetter()
		{
			public void setValues(PreparedStatement ps, int i) throws SQLException
			{
				PurchaseOrderLineItem lineItem = (PurchaseOrderLineItem) purchaseOrderForm.getPurchaseOrder().getPoLineItems().get( i );
				ps.setString( 1, (lineItem.getSku()) );
				ps.setString( 2, (lineItem.getSupplierSku()) );
				ps.setInt( 3, (Integer) (lineItem.getQty()) );
				if ( lineItem.getCost() == null )
				{
					ps.setNull( 4, Types.NULL );
				}
				else
				{
					ps.setDouble( 4, lineItem.getCost() );
				}
				ps.setString( 5,  (lineItem.getProductName()) );
				ps.setInt( 6, (Integer) (lineItem.getPoId()) );
				ps.setInt( 7, (Integer) (lineItem.getLineNumber()) );
			}

			public int getBatchSize()
			{
				return purchaseOrderForm.getPurchaseOrder().getPoLineItems().size();
			}
		} );
	}
	
	private void addProductInventory(PurchaseOrder purchaseOrder, boolean inventoryHistory) {
		if ( purchaseOrder.getStatus().equals( "rece" )) {
			for ( PurchaseOrderLineItem lineItem : purchaseOrder.getPoLineItems() ) {
				Inventory inventoryObject = new Inventory();
				inventoryObject.setSku(lineItem.getSku());
				inventoryObject.setInventory(lineItem.getQty());
				inventoryObject.setInventoryAFS(lineItem.getQty());
				//updateInventory(inventoryObject);
				// update only inventory on hand since the AFS value already be updated when place this purchase order.
				getSqlMapClientTemplate().update("updateInventory", inventoryObject);
				Inventory inventory  = (Inventory) getSqlMapClientTemplate().queryForObject("getInventory", inventoryObject );
				if(inventoryHistory && inventory != null && inventory.getInventory() != null) {
					InventoryActivity inventoryActivity = new InventoryActivity();
					inventoryActivity.setSku(lineItem.getSku());
					inventoryActivity.setReference(purchaseOrder.getPoId().toString());
					inventoryActivity.setAccessUserId(purchaseOrder.getAccessUserId());
					inventoryActivity.setType("purchaseOrder");
					inventoryActivity.setQuantity(lineItem.getQty());
					inventoryActivity.setInventory(inventory.getInventory());
					
					getSqlMapClientTemplate().insert( "productIventoryHistory", inventoryActivity );
				}
			}
		} 
	}
	
	public void insertPurchaseOrderStatus(PurchaseOrderForm purchaseOrderForm, boolean inventoryHistory) throws DataAccessException {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Object[] args = { purchaseOrderForm.getPurchaseOrder().getSpecialInstruction(), purchaseOrderForm.getPurchaseOrder().getPoId() };
		try {
			jdbcTemplate.update( "UPDATE purchase_orders SET special_instruction = ? WHERE po_id = ?", args );
		} catch (Exception e) {
			// do nothing
			e.printStackTrace();
		}
		// If the PO is received and then the status is cancel and is not a dropship, we deduct the AFS AND onHand both.  But when PO is received or cancel, we don't allow anyone change it.
		if(getPurchaseOrderCountByStatus(purchaseOrderForm.getPurchaseOrder().getPoId()) > 0 && !purchaseOrderForm.getPurchaseOrder().isDropShip() && purchaseOrderForm.getPurchaseOrderStatus().getStatus().equals("x")) {
			for (PurchaseOrderLineItem lineItem : purchaseOrderForm.getPurchaseOrder().getPoLineItems()) {
				Inventory lineInventory = new Inventory(); 	
				lineInventory.setSku(lineItem.getSku());
				// this should deduct the inventory only if the purchase order is cancelled
				lineInventory.setInventoryAFS(-lineItem.getQty());
				lineInventory.setInventory(-lineItem.getQty());
				updateInventory(lineInventory);
			}
		}
		// directly cancel PO, check is not dropship, is cancel status and there is a PO in database.
		else if( getPurchaseOrderCountById(purchaseOrderForm.getPurchaseOrder().getPoId()) > 0 && !purchaseOrderForm.getPurchaseOrder().isDropShip() && purchaseOrderForm.getPurchaseOrderStatus().getStatus().equals("x")){
			for (PurchaseOrderLineItem lineItem : purchaseOrderForm.getPurchaseOrder().getPoLineItems()) {
					Inventory lineInventory = new Inventory(); 	
					lineInventory.setSku(lineItem.getSku());
					// this should deduct the inventory only if the purchase order is cancelled
					lineInventory.setInventoryAFS(-lineItem.getQty());
					lineInventory.setInventory(0);
					updateInventory(lineInventory);
			}
			
		}
		
		try{
			getSqlMapClientTemplate().update("updatePurchaseOrderStatus", purchaseOrderForm.getPurchaseOrderStatus());
			System.out.println("purchaseOrderForm.getPurchaseOrderStatus()" + "===updatePOStatus");
		}catch(Exception ex){
			ex.printStackTrace();
			return;
		}
		
		try{
			getSqlMapClientTemplate().insert("insertPurchaseOrderHistoryStatus", purchaseOrderForm.getPurchaseOrderStatus());
			System.out.println("purchaseOrderForm.getPurchaseOrderStatus()" + "===insertPOHistoryStatus");
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
		
		if ( !purchaseOrderForm.getPurchaseOrder().isDropShip() ) {
			addProductInventory(purchaseOrderForm.getPurchaseOrder(), inventoryHistory);
		} else {
			updateLineItemProcessed(purchaseOrderForm);
		}			
	}
	
	public PurchaseOrder getPurchaseOrder(int poId) {
		PurchaseOrder purchaseOrder = (PurchaseOrder) getSqlMapClientTemplate().queryForObject( "getPurchaseOrder", poId );		
		if ( purchaseOrder != null ) {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put( "poId", purchaseOrder.getPoId() );
			if ( purchaseOrder.getOrderId() != null ) {
				paramMap.put( "orderId", purchaseOrder.getOrderId() );
				purchaseOrder.setPoLineItems( getSqlMapClientTemplate().queryForList( "getLineItemsByPOIdOrderId", paramMap ) );
				for ( LineItem lineItem : purchaseOrder.getPoLineItems() ) {
					paramMap.put( "lineNum", lineItem.getLineNumber() );
					lineItem.setProductAttributes( getSqlMapClientTemplate().queryForList( "getLineItemAttributes", paramMap ) );
				}
			} else {
				purchaseOrder.setPoLineItems( getSqlMapClientTemplate().queryForList( "getLineItemsByPOId", paramMap ) );
			}
		}
		return purchaseOrder;
	}
	
	public List getPurchaseOrderStatusHistory(int poId) {
		return getSqlMapClientTemplate().queryForList("getPurchaseOrderStatusHistory", poId);
	}

	public List getPurchaseOrderList(PurchaseOrderSearch search) {
		if ( search.getStatus() != null ) {
			if ( search.getStatus().equals( "001" ) )
				search.setStatusTemp(" ( purchase_orders.status = 'pend' OR purchase_orders.status = 'send' ) ");
			else if ( search.getStatus().equals( "001s01" ) )
				search.setStatusTemp(" ( purchase_orders.status= 'pend' OR purchase_orders.status= 'send' OR purchase_orders.status= 'send01' ) ");
			else if ( search.getStatus().equals( "001s01s05" ) )
				search.setStatusTemp(" ( purchase_orders.status= 'pend' OR purchase_orders.status= 'send' OR purchase_orders.status= 'send01' OR purchase_orders.status= 'send05' ) ");
			else if ( search.getStatus().equals( "001s01s05s10" ) )
				search.setStatusTemp(" ( purchase_orders.status= 'pend' OR purchase_orders.status= 'send' OR purchase_orders.status= 'send01' OR purchase_orders.status= 'send05' OR purchase_orders.status= 'send10' ) ");
			else if ( search.getStatus() == null || search.getStatus().equals( "" ))
				search.setStatusTemp("");
			else
				search.setStatusTemp(" ( purchase_orders.status= '" + search.getStatus() + "' ) ");
		}
		System.out.println("search.getDateType(): "+search.getDateType());
		List<PurchaseOrder> purchasOrders;
		List<PurchaseOrderStatus> purchaseOrderStatuses;
		if(search.getDateType().equals("arrivedDate") || search.getDateType().equals("stagedDate")) {
			if (search.getDateType().equals("arrivedDate") ) {
				search.setHistoryStatus("Send10");
			}
			else if (search.getDateType().equals("stagedDate") ) {
				search.setHistoryStatus("rece");
			}
			purchasOrders = getSqlMapClientTemplate().queryForList("getPurchaseOrderListWithArrivedOrStaged", search);
		}
		else {
			purchasOrders = getSqlMapClientTemplate().queryForList("getPurchaseOrderList", search);
		}

		for (PurchaseOrder purchaseOrder: purchasOrders) {
			purchaseOrderStatuses = getPurchaseOrderStatusHistory(purchaseOrder.getPoId());
			for (PurchaseOrderStatus purchaseOrderStatus: purchaseOrderStatuses) {
				if (purchaseOrderStatus.getStatus().equalsIgnoreCase("rece")) {  //Staged
					purchaseOrder.setStagedDate(purchaseOrderStatus.getDateChanged());
				}
				else if (purchaseOrderStatus.getStatus().equalsIgnoreCase("Send10")) {  //Arrived
					purchaseOrder.setArrivedDate(purchaseOrderStatus.getDateChanged());
				} 	
			}
		}
		return purchasOrders;
	}
	
	public List<String>  getPoPTList(){
		return getSqlMapClientTemplate().queryForList("getPoPTList");
	}
	
	public void updatePoPtById(int poId, String pt){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "poId", poId );
		paramMap.put( "pt", pt );
		getSqlMapClientTemplate().update("updatePoPtById" , paramMap);
	}
	
	public void updatePoStatusById(int poId, String status){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "poId", poId );
		paramMap.put( "status", status );
		getSqlMapClientTemplate().update("updatePoStatusById" , paramMap);
	}
	
	public Integer getPurchaseOrderIdByName( String poNumber )
	{
		return (Integer) getSqlMapClientTemplate().queryForObject("getPurchaseOrderIdByName", poNumber);  
	}
	
	public Integer getPurchaseOrderCountByStatus( Integer poId )
	{
		return (Integer) getSqlMapClientTemplate().queryForObject("getPurchaseOrderCountByStatus", poId);  
	}
	
	public Integer getPurchaseOrderCountById( Integer poId ){
		return (Integer) getSqlMapClientTemplate().queryForObject("getPurchaseOrderCountById", poId); 
	}
	
	public int purchaseOrderCount(Integer orderId) {
		return (Integer) getSqlMapClientTemplate().queryForObject("purchaseOrderCount", orderId);
	}
	
	public List<PurchaseOrderLineItem> getPOLineItemsQuantity(Integer orderId, boolean dropShip) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "orderId",orderId );
		paramMap.put( "dropShip",dropShip );
		return  getSqlMapClientTemplate().queryForList( "getPOLineItemsQuantity", paramMap);
	}
	
	public void deletePurchaseOrder(Integer poId) {	
		getSqlMapClientTemplate().delete( "deletePurchaseOrder", poId, 1 );
	}
	
	public void updateInventoryBySkus(final List<CsvFeed> csvFeedList, final List<Map <String, Object>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		StringBuffer sql = new StringBuffer("UPDATE product SET last_modified = now() ");
		if (csvFeedList != null) {
			for (CsvFeed csvFeed : csvFeedList) {
				String[] columnName = csvFeed.getColumnName().split(",");
				for (int x = 0; x < columnName.length; x++) {
					if (columnName[x].equals("protected_level")) {
						sql.append(", protected_level = (b?)");
					} else {
						sql.append(", " + columnName[x] + " = ?");
					}
				}
			}
		}
		sql.append(" ,inventory = ?, inventory_afs = ? WHERE sku = ?");
		jdbcTemplate.batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map<String, Object> entry = data.get(i);
				int index = 1;
				if (csvFeedList != null) {
					for (CsvFeed csvFeed : csvFeedList) {
						String[] columnName = csvFeed.getColumnName().split(",");
						for (int x = 0; x < columnName.length; x++) {
							if (entry.get(csvFeed.getColumnName()) == null) {
								ps.setNull(index++, Types.NULL);
							} else if (entry.get(csvFeed.getColumnName()).getClass() == Boolean.class) {
								ps.setBoolean(index++, (Boolean) entry.get(csvFeed.getColumnName()));
							} else {
								ps.setString(index++, entry.get(csvFeed.getColumnName()).toString());
							}
						}
					}
				}
				ps.setInt(index++, ((Integer) entry.get("inventory")).intValue());
				ps.setInt(index++, ((Integer) entry.get("inventory_afs")).intValue());
				ps.setString(index++, entry.get("sku").toString());
			}

			public int getBatchSize() {
				return data.size();
			}
		} );
	}
	
	public void adjustInventoryBySku(InventoryActivity inventoryActivity) {
		Inventory inventory = new Inventory();
		inventory.setInventoryAFS(inventoryActivity.getQuantity());
		if(!inventoryActivity.isAdjustAvForSaleOnly()){
		inventory.setInventory(inventoryActivity.getQuantity());
		}
		inventory.setSku(inventoryActivity.getSku());
		addInventory(inventory, inventoryActivity.isAdjustAvForSaleOnly());
		if(inventoryActivity.getInventory() != null) {
			
				getSqlMapClientTemplate().insert( "productIventoryHistory", inventoryActivity );
			
		}
	}
	
	public void importInventory(InventoryActivity inventoryActivity) {
		Inventory inventory = new Inventory();
		Inventory currentInventory = new Inventory();
		currentInventory.setSku(inventoryActivity.getSku());
		currentInventory = getInventory(currentInventory);
		inventory.setSku(inventoryActivity.getSku());
		inventory.setNegInventory(inventoryActivity.isNegInventory());
		inventory.setShowNegInventory(inventoryActivity.isShowNegInventory());
		inventory.setLowInventory(inventoryActivity.getLowInventory());
		inventory.setInventoryAFS(inventoryActivity.getinventoryAFS());
		inventory.setInventory(inventoryActivity.getInventory());
		inventory.setBackToNull(inventoryActivity.isBackToNull());
		if (currentInventory.getInventory() == null) {
			currentInventory.setInventory(0);
		}
		if (inventory.getInventory() != null) {
			inventoryActivity.setQuantity(inventory.getInventory() - currentInventory.getInventory());			
		}
		
		getSqlMapClientTemplate().update("importInventory" , inventory);
		if((inventoryActivity.getInventory() != null && inventoryActivity.getQuantity() != 0) || inventoryActivity.getType().equals("Changed to Null")) {
			getSqlMapClientTemplate().insert( "productIventoryHistory", inventoryActivity );
		}
	}
	
	public Inventory getInventory(Inventory inventory) {
		return (Inventory) getSqlMapClientTemplate().queryForObject("getInventory", inventory);		
	}

	public void updateInventory(Inventory inventory) {
		if (inventory.getInventoryAFS()!=null) {
			getSqlMapClientTemplate().update("updateinventoryAFS", inventory);
		} 
		if (inventory.getInventory()!=null) {
			getSqlMapClientTemplate().update("updateInventory", inventory);
		}
	}
	
	private void addInventory(Inventory inventory, boolean adjustAvForSaleOnly) {
		if(!adjustAvForSaleOnly){
			getSqlMapClientTemplate().update("addInventory", inventory);
		} else {
			getSqlMapClientTemplate().update("adjustAvForSaleOnly", inventory);
		}
	}
	
	public void addKitPartsHistory(InventoryActivity kitActivity) {
		getSqlMapClientTemplate().insert( "productIventoryHistory", kitActivity );
	}

	public PurchaseOrderLineItem getLineItemByPOId(PurchaseOrderLineItem lineItem) {
		return (PurchaseOrderLineItem) getSqlMapClientTemplate().queryForObject("getLineItemByPOId", lineItem);
	}
	
	public void addNewPurchaseOrderHistoryStatus(Integer poId, String status, String accessUser) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("poId", poId);
		paramMap.put("status", status);
		paramMap.put("comments", null);
		paramMap.put("updatedBy", accessUser);
		paramMap.put("sendEmail", 0);
		getSqlMapClientTemplate().insert("addNewPurchaseOrderHistoryStatus", paramMap);	
	}
}
