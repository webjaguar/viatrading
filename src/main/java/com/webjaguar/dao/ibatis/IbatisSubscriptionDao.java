/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.26.2008
 */

package com.webjaguar.dao.ibatis;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.SubscriptionDao;
import com.webjaguar.model.Subscription;
import com.webjaguar.model.SubscriptionSearch;

public class IbatisSubscriptionDao extends SqlMapClientDaoSupport implements SubscriptionDao {
	
	public List<Subscription> getDueSubscriptions() {
		return getSqlMapClientTemplate().queryForList("getDueSubscriptions");
	}

	public List<Subscription> getSubscriptions(SubscriptionSearch search) {
		return getSqlMapClientTemplate().queryForList("getSubscriptions", search);
	}
	
	public Subscription getSubscription(String code) {
		Subscription subscription = (Subscription) getSqlMapClientTemplate().queryForObject("getSubscription", code);
		if (subscription != null) {
			subscription.setProductAttributes(getSqlMapClientTemplate().queryForList("getSubscriptionAttributes", code));
		}
		return subscription;
	}
	
	public void updateSubscription(Subscription subscription) {
		getSqlMapClientTemplate().update("updateSubscription", subscription, 1);
	}
	
	public void deleteSubscription(Subscription subscription) {
		getSqlMapClientTemplate().delete("deleteSubscription", subscription, 1);
	}
}
