/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.SalesRepDao;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LocationSearch;
import com.webjaguar.model.QualifierSearch;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SalesRepSearch;
import com.webjaguar.model.crm.CrmQualifier;

public class IbatisSalesRepDao extends SqlMapClientDaoSupport implements SalesRepDao {
	  
	  public List<SalesRep> getSalesRepList(SalesRepSearch search) {
		  return getSqlMapClientTemplate().queryForList("getSalesRepList", search);
	  }
	  
	  public List<CrmQualifier> getQualifierList(QualifierSearch search) {
		  return getSqlMapClientTemplate().queryForList("getQualifierSearchList", search);
	  }
	  
	  public SalesRep getSalesRepByUserId(Integer userId) {
		  return (SalesRep) getSqlMapClientTemplate().queryForObject("getSalesRepByUserId", userId);
	  }
	  
	  public void insertSalesRep(SalesRep salesRep) {
		  getSqlMapClientTemplate().insert("insertSalesRep", salesRep);
	  }
	  
	  public void updateSalesRep(SalesRep salesRep) {
		  getSqlMapClientTemplate().update("updateSalesRep", salesRep, 1);
	  }
	  
	  public SalesRep getSalesRepById(Integer salesRepId) {
			return (SalesRep) getSqlMapClientTemplate().queryForObject("getSalesRepById", salesRepId);
	  }
	  
	  public void deleteSalesRep(Integer salesRepId) {
		  getSqlMapClientTemplate().update("deleteSalesRep", salesRepId);
	  }
	  
	  public boolean salesRepHasCustomer(Integer salesRepId){
		  if((Integer) getSqlMapClientTemplate().queryForObject("salesRepHasCustomer", salesRepId) == 0){
			  return false;
		  }else{
			  return true;
		  }
	  }
	  
	  public SalesRep getSalesRepByTerritoryZipcode(String zipcode) {
		  return (SalesRep) getSqlMapClientTemplate().queryForObject("getSalesRepByTerritoryZipcode", zipcode);
	  }

	  public void markSalesRepAssignedToCustomerInCycle(Integer salesRepId, boolean isAssigned) {
		  	Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("salesRepId", salesRepId);
			paramMap.put("isAssigned", isAssigned);
			getSqlMapClientTemplate().update("markSalesRepAssignedToCustomerInCycle", paramMap);
	  }
	  
	  public SalesRep getNextSalesRepInQueue() {
		  return (SalesRep) getSqlMapClientTemplate().queryForObject("getNextSalesRepInQueue");
	  }

	  public List<SalesRep> getSalesRepListByTerritoryZipcode(LocationSearch search) {
		  return getSqlMapClientTemplate().queryForList("getSalesRepListByTerritoryZipcode", search);
	  }

	  public Map<String, Long> getSaleRepAccountNumMap() {
		  return getSqlMapClientTemplate().queryForMap("getSaleRepAccountNumMap", null, "sales_rep_account_num", "sales_rep_id");
	  }
	  
	  public SalesRep getSalesRep(SalesRep salesRep) {
		  return (SalesRep) getSqlMapClientTemplate().queryForObject("getSalesRep", salesRep);
	  }	  
	  
	// csv feed
	public void updateSalesRep(final List <CsvFeed> csvFeedList, final List<Map <String, Object>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		StringBuffer sql = new StringBuffer(
			"UPDATE " +
				"sales_rep " +
			"SET " +
				"sales_rep_last_modified = now()");
		for (CsvFeed csvFeed: csvFeedList) {
			sql.append(", " + csvFeed.getColumnName() + " = ?");				
		}
		sql.append(" WHERE sales_rep_account_num = ?");
		jdbcTemplate.batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map<String, Object> entry = data.get(i);
				int index = 1;
				for (CsvFeed csvFeed: csvFeedList) {
					if (entry.get(csvFeed.getColumnName()) == null) {
						ps.setNull(index++, Types.NULL);						
					} else if (entry.get(csvFeed.getColumnName()).getClass() == Boolean.class) {
						ps.setBoolean(index++, (Boolean) entry.get(csvFeed.getColumnName()));						
					} else {
						ps.setString(index++, entry.get(csvFeed.getColumnName()).toString());
					}
				}
				ps.setString(index++, entry.get("sales_rep_account_num").toString());
			}

			public int getBatchSize() {
				return data.size();
			}
		});		
	}
	
	// csv feed
	public void updateCustomerSalesRepAssoc(final List<Map <String, Object>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String sql = "UPDATE " +
						"users " +
					 "SET " +
					 	"sales_rep_id = ?, " +
					 	"last_modified = now() " +
					 "WHERE " +
					 	"account_number = ?";
		jdbcTemplate.batchUpdate(sql.toString(), new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map<String, Object> entry = data.get(i);
				
				if (entry.get("sales_rep_id") == null) {
					ps.setNull(1, Types.NULL);						
				} else {
					ps.setLong(1, (Long) entry.get("sales_rep_id"));
				}
				ps.setString(2, (String) entry.get("account_number"));
			}

			public int getBatchSize() {
				return data.size();
			}
		});		
	}

	public List<Customer> getCustomerListBySalesRep(int salesRepId, String sort) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("salesRepId", salesRepId);
		paramMap.put("sort", sort);
		List<Customer> customers = getSqlMapClientTemplate().queryForList("getCustomerListBySalesRep", paramMap);
		Map<Integer, Customer> customerMap = getSqlMapClientTemplate().queryForMap("getCustomerOrderReport", null, "id");
		// get customer
		for (Customer customer: customers) {
			if (customerMap.containsKey(customer.getId())) {
				customer.setOrdersGrandTotal( customerMap.get(customer.getId()).getOrdersGrandTotal() );
				customer.setOrderCount( customerMap.get(customer.getId()).getOrderCount() );
			}
		}
		return customers;
	}
	
	public void updateLoginStats(SalesRep salesRep) {
		getSqlMapClientTemplate().update("updateSalesRepLoginStats", salesRep.getId(), 1);		  	
	}
	
	public void deleteOldSalesReps() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		
		// clear customer reference
		String query = "UPDATE users SET sales_rep_id = null where sales_rep_id IN "
						+ "(SELECT sales_rep_id FROM sales_rep WHERE feed_new = false)";
		jdbcTemplate.update(query);

		// clear order reference
		query = "UPDATE orders SET salesrep_id = null where salesrep_id IN "
						+ "(SELECT sales_rep_id FROM sales_rep WHERE feed_new = false)";
		jdbcTemplate.update(query);
		query = "UPDATE orders SET salesrep_processedby_id = null where salesrep_processedby_id IN "
						+ "(SELECT sales_rep_id FROM sales_rep WHERE feed_new = false)";
		jdbcTemplate.update(query);
		
		// delete sales reps
		query = "DELETE FROM sales_rep where feed_new = false";
		jdbcTemplate.update(query);

		// reset current
		query = "UPDATE sales_rep set feed_new = false";
		jdbcTemplate.update(query);
	}
	
	public List<SalesRep> getSalesRepTree(int salesRepId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		List<SalesRep> salesRepTree = new ArrayList<SalesRep>();
		Set<Integer> salesRepIds = new HashSet<Integer>();
		Object[] args = { salesRepId };
		String query = "SELECT salesrep_parent, sales_rep_id, name FROM sales_rep WHERE sales_rep_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet(query, args);
		while (results.next()) {
			SalesRep node = new SalesRep();
			node.setId(results.getInt("sales_rep_id"));
			node.setName(results.getString("name"));
			if (!salesRepIds.add(node.getId())) {
				// prevent endless loop
				break;
			}
			salesRepTree.add(0, node);
			args[0] = results.getInt("salesrep_parent");
			if (results.wasNull()) {
				// results.getInt() returns 0 if null 
				break;				
			}
			results = jdbcTemplate.queryForRowSet(query, args);
		}

		return salesRepTree;		
	}
	
	public List<String> getSalesRepGroupList(){
		return getSqlMapClientTemplate().queryForList("getSalesRepGroupList");
	}
	
	public List<SalesRep> getSalesRepList(int salesRepId) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		
		jdbcTemplate.execute("DROP TABLE IF EXISTS sales_rep_tree");
		jdbcTemplate.execute("CREATE TABLE sales_rep_tree (id INT PRIMARY KEY, level SMALLINT) ENGINE=HEAP");
		jdbcTemplate.execute("INSERT INTO sales_rep_tree VALUES (" + salesRepId + ", 1)");
		while (jdbcTemplate.update("INSERT IGNORE INTO sales_rep_tree"
						   + " SELECT sales_rep_id, level+1 FROM sales_rep, sales_rep_tree WHERE sales_rep.salesrep_parent = sales_rep_tree.id") > 0);

		List<SalesRep> salesRepList = new ArrayList<SalesRep>();
		String query = "SELECT sales_rep_id, level, name, salesrep_parent FROM sales_rep_tree LEFT JOIN sales_rep ON id = sales_rep_id";
		SqlRowSet results = jdbcTemplate.queryForRowSet(query);
		while (results.next()) {
			SalesRep salesRep = new SalesRep();
			salesRep.setId(results.getInt("sales_rep_id"));
			salesRep.setName(results.getString("name"));
			salesRep.setParent(results.getInt("salesrep_parent"));
			salesRepList.add(salesRep);
		}
		
		jdbcTemplate.execute("DROP TABLE sales_rep_tree");
		
		return salesRepList;
	}
	public List<Integer> getInactiveSalesRepIdList() {
		return getSqlMapClientTemplate().queryForList("getInactiveSalesRepIdList");
	}
}
