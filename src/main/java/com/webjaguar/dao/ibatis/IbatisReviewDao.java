/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.ReviewDao;
import com.webjaguar.model.CompanyReview;
import com.webjaguar.model.ProductReview;
import com.webjaguar.model.Review;
import com.webjaguar.model.ReviewSearch;


public class IbatisReviewDao extends SqlMapClientDaoSupport implements ReviewDao {
	
	//Product Review
	public List<ProductReview> getProductReviewList(ReviewSearch search) {
		return getSqlMapClientTemplate().queryForList("getProductReviewList", search);
	}
	public int getProductReviewListCount(ReviewSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getProductReviewListCount", search);
	}
	public void insertProductReview(ProductReview productReview) {
		getSqlMapClientTemplate().insert("insertProductReview", productReview);
	} 
	public void deleteProductReviewById(Integer productReviewId) {
		getSqlMapClientTemplate().delete("deleteProductReviewById", productReviewId);
    }
    public List<ProductReview> getProductReviewListByProductSku(String productSku, Boolean active, Integer limit) {
    	Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "active", active );
		paramMap.put( "productSku", productSku );
		paramMap.put( "limit", limit );
		return getSqlMapClientTemplate().queryForList("getProductReviewListByProductSku", paramMap);
	}
    public ProductReview getProductReviewById(Integer productReviewId) {
		return (ProductReview) getSqlMapClientTemplate( ).queryForObject( "getProductReviewById", productReviewId );
	}
    public void updateProductReview(ProductReview productReview) {
		getSqlMapClientTemplate( ).update( "updateProductReview", productReview, 1 );
	}
	public ProductReview getProductReviewAverage(String productSku) {
		return (ProductReview) getSqlMapClientTemplate( ).queryForObject( "getProductReviewAverage", productSku );
	}
	public boolean checkPreviousReview(ProductReview productReview) {
		boolean review = false;
		if((Integer) getSqlMapClientTemplate( ).queryForObject( "checkPreviousReview",  productReview ) > 0) {
			review = true;
		}
		return review;
	}
	
	// Company Review
	public List<CompanyReview> getcompanyReviewList(ReviewSearch search) {
		return getSqlMapClientTemplate().queryForList("getCompanyReviewList", search);
	}
	public int getCompanyReviewListCount(ReviewSearch search) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getCompanyReviewListCount", search);
	}
	public void insertCompanyReview(CompanyReview companyReview) {
		getSqlMapClientTemplate().insert("insertCompanyReview", companyReview);
	} 
	public void deleteCompanyReviewById(Integer companyReviewId) {
		getSqlMapClientTemplate().delete("deleteCompanyReviewById", companyReviewId);
    }
	public List<ProductReview> getCompanyReviewListByCompanyId(Integer companyId, Boolean active) {
    	Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "active", active );
		paramMap.put( "companyId", companyId );
		return getSqlMapClientTemplate().queryForList("getCompanyReviewListByCompanyId", paramMap);
	}
    public CompanyReview getCompanyReviewById(Integer companyReviewId) {
		return (CompanyReview) getSqlMapClientTemplate( ).queryForObject( "getCompanyReviewById",companyReviewId );
	}
    public void updateCompanyReview(CompanyReview companyReview) {
		getSqlMapClientTemplate( ).update( "updateCompanyReview", companyReview, 1 );
	}
	public Review getCompanyReviewAverage(Integer companyId) {
		return (Review) getSqlMapClientTemplate( ).queryForObject( "getCompanyReviewAverage", companyId );
	}
}