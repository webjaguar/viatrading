package com.webjaguar.dao.ibatis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.TruckLoadProcessDao;
import com.webjaguar.model.Product;
import com.webjaguar.model.TruckLoadProcess;
import com.webjaguar.model.TruckLoadProcessProducts;
import com.webjaguar.model.TruckLoadProcessSearch;
import com.webjaguar.web.domain.Utilities;

public class IbatisTruckLoadProcessDao extends SqlMapClientDaoSupport implements TruckLoadProcessDao {

	public void insertTruckLoadProcess(TruckLoadProcess truckLoadProcess) {
		
		//insert truck load process
		getSqlMapClientTemplate().insert("insertTruckLoadProcess", truckLoadProcess);

		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );		
		if (truckLoadProcess.getId() != null) {
			//insert original sku
			for (Product original : truckLoadProcess.getOriginalProduct()) {
				original.setId(truckLoadProcess.getId());
				getSqlMapClientTemplate().insert("insertTruckLoadoriginal", original);
			}
			
			//insert produced sku
			Double extMsrp = 0.0;
			for (TruckLoadProcessProducts derivedProduct : truckLoadProcess.getDerivedProduct()) {
				derivedProduct.setId(truckLoadProcess.getId());
				extMsrp = extMsrp + (derivedProduct.getQuantity() * derivedProduct.getUnitPrice());
				getSqlMapClientTemplate().insert("insertTruckLoadProduced", derivedProduct);
			}
			truckLoadProcess.setExtMsrp(extMsrp);
			// insert extMsrp
			Object[] args2 = { truckLoadProcess.getExtMsrp(), truckLoadProcess.getId() };
			String sql = "update truckload_processed set ext_msrp=? where id = ?";
			jdbcTemplate.update(sql, args2);
		}
	}
	
	public TruckLoadProcessSearch setStatusTempInSearch(TruckLoadProcessSearch truckLoadProcessSearch) {
		if (truckLoadProcessSearch.getStatus() != null) {
			if ( truckLoadProcessSearch.getStatus().equals("x")) {
				truckLoadProcessSearch.setStatusTemp(" ( status = 'x') ");
			}
			if (truckLoadProcessSearch.getStatus().equals("c")) {
				truckLoadProcessSearch.setStatusTemp(" ( status = 'c') ");	
			}
			if (truckLoadProcessSearch.getStatus().equals("pr")) {
				truckLoadProcessSearch.setStatusTemp(" ( status = 'pr') ");	
			}
			if (truckLoadProcessSearch.getStatus().equals("xpr")) {
				truckLoadProcessSearch.setStatusTemp(" ( status = 'pr' or status = 'x') ");	
			}
			if (truckLoadProcessSearch.getStatus().equals("cpr")) {
				truckLoadProcessSearch.setStatusTemp(" ( status = 'pr' or status = 'c') ");	
			}
			if (truckLoadProcessSearch.getStatus().equals("xc")) {
				truckLoadProcessSearch.setStatusTemp(" ( status = 'x' or status = 'c') ");	
			}
		}
		return truckLoadProcessSearch;
	}
	
	public List<TruckLoadProcess> getTruckLoadProcessList(TruckLoadProcessSearch truckLoadProcessSearch) {
		truckLoadProcessSearch = setStatusTempInSearch(truckLoadProcessSearch);
		return getSqlMapClientTemplate().queryForList("getTruckLoadProcessList", truckLoadProcessSearch);
	}
	
	public List<TruckLoadProcess> getTruckLoadProcessListReport(TruckLoadProcessSearch truckLoadProcessSearch) {
		truckLoadProcessSearch = setStatusTempInSearch(truckLoadProcessSearch);
		List<TruckLoadProcess> list = getSqlMapClientTemplate().queryForList("getTruckLoadProcessList", truckLoadProcessSearch);
		List<TruckLoadProcess> reportList = new ArrayList<TruckLoadProcess>();
		
		//search filter for original and produced sku
		for(TruckLoadProcess p : list) {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			p = getTruckLoadProcessById(p.getId());
			boolean bool = false;
			if(truckLoadProcessSearch.getOriginalSku() != null && !truckLoadProcessSearch.getOriginalSku().equals("")) {
				for(Product original : p.getOriginalProduct()) {
					bool = (bool == true) || (Utilities.like(original.getSku(),truckLoadProcessSearch.getOriginalSku())) ? true : false;
				}
			}
			if(truckLoadProcessSearch.getProducedSku()  != null && !truckLoadProcessSearch.getProducedSku().equals("") ) {
				for(TruckLoadProcessProducts produced : p.getDerivedProduct()) {
					bool = (bool == true) || (produced.getProduct() != null && Utilities.like(produced.getProduct().getSku(),truckLoadProcessSearch.getProducedSku())) ? true : false;
				}
			}
			if ((truckLoadProcessSearch.getOriginalSku() == null && truckLoadProcessSearch.getProducedSku()  == null) || 
					(truckLoadProcessSearch.getOriginalSku() != null && truckLoadProcessSearch.getProducedSku()  != null && truckLoadProcessSearch.getOriginalSku().equals("") && truckLoadProcessSearch.getProducedSku().equals("") ) 
					|| bool == true) {

				reportList.add(p);
			}
		}
		return reportList;
	}
	
	
	public List<String> getTruckLoadProcessPTList() {
		return getSqlMapClientTemplate().queryForList("getTruckLoadProcessPTList");
	}
	
	public TruckLoadProcess getTruckLoadProcessById(Integer id) {
		TruckLoadProcess truckLoad = new TruckLoadProcess();
		truckLoad = (TruckLoadProcess) getSqlMapClientTemplate().queryForObject("getTruckLoadProcessById" , id);
		truckLoad.setOriginalProduct((List<Product>) getSqlMapClientTemplate().queryForList("getTruckLoadProcessOriginalById" , id));
		truckLoad.setDerivedProduct((List<TruckLoadProcessProducts>) getSqlMapClientTemplate().queryForList("getTruckLoadProcessDerivedById" , id));
		return truckLoad;
	}
	
	public TruckLoadProcessProducts getTruckLoadDerivedProductById (Integer Id){
		TruckLoadProcessProducts derivedProduct = (TruckLoadProcessProducts) getSqlMapClientTemplate().queryForObject("getTruckLoadDerivedProductById",Id);	
		return derivedProduct;
	}
	public void updateTruckLoadProcess(TruckLoadProcess truckLoadProcess){
		getSqlMapClientTemplate().update("updateTruckLoadProcess", truckLoadProcess);
		for(Product original : truckLoadProcess.getOriginalProduct()) {
			original.setId(truckLoadProcess.getId());
			getSqlMapClientTemplate().insert("insertTruckLoadoriginal", original);
		}
		Double extMsrp = 0.0;
		for (TruckLoadProcessProducts derivedProduct : truckLoadProcess.getDerivedProduct()) {
			derivedProduct.setId(truckLoadProcess.getId());
			getSqlMapClientTemplate().insert("insertTruckLoadProduced", derivedProduct);
			extMsrp = extMsrp + (derivedProduct.getQuantity() * derivedProduct.getUnitPrice());
		}
		// update extMsrp
		truckLoadProcess.setExtMsrp(extMsrp);
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Object[] args2 = { truckLoadProcess.getExtMsrp(), truckLoadProcess.getId() };
		String sql = "update truckload_processed set ext_msrp=ext_msrp + ? where id = ?";
		jdbcTemplate.update(sql, args2);
	}
	
	public void updateTruckLoadPT(TruckLoadProcess truckLoadProcess){
		getSqlMapClientTemplate().update("updateTruckLoadPT", truckLoadProcess);
	}
}
