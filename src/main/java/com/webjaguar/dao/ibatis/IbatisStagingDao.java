/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.util.*;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.StagingDao;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Order;
import com.webjaguar.model.StagingUser;


public class IbatisStagingDao extends SqlMapClientDaoSupport implements StagingDao {
	
	// WorldShip
	public void insertStagingOrder(Order order) {
		// WorldShip wouldn't generate right shipping price for address with commercial type and Company name empty.
		// replace empty company with the person name.
		if ( order.getShipping().getCompany() == null || order.getShipping().getCompany().equals( "" )) {
			order.getShipping().setCompany( order.getShipping().getFirstName() + " " + order.getShipping().getLastName() );
		}
		getSqlMapClientTemplate().insert( "insertStagingOrder", order );
	}
	public List getWorldShipTrackcode() {
		  return getSqlMapClientTemplate().queryForList("getWorldShipTrackcode");
	}
	public void updateWorldShipProcessed(List<Integer> validOrderIds) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("validOrderIds", validOrderIds);
		getSqlMapClientTemplate().update("updateWorldShipProcessed", paramMap);
	}
	public void deleteWorldShipTrackcode() {
		getSqlMapClientTemplate().delete("deleteWorldShipTrackcode");
    }
	
	// FedEx Label Manager (We use the Same WorldShip Queries for now)
	public void deleteFedexShipTrackcode() {
		getSqlMapClientTemplate().delete("deleteWorldShipTrackcode");
	}
	public List getFedexShipTrackcode() {
		return getSqlMapClientTemplate().queryForList("getWorldShipTrackcode");
	}
	public void updateFedexShipProcessed() {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("validOrderIds", null);
		getSqlMapClientTemplate().update("updateWorldShipProcessed");		
	}
	public List<StagingUser> getStagingUsersData() {
		return getSqlMapClientTemplate().queryForList("getStagingUsersData");
	}
	public void deleteStagingUsers(int id) {
		getSqlMapClientTemplate().delete("deleteStagingUsers", id);
    }
}
