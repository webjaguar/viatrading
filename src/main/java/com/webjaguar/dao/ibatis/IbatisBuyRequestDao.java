/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.BuyRequestDao;
import com.webjaguar.model.BuyRequest;
import com.webjaguar.model.BuyRequestSearch;
import com.webjaguar.model.BuyRequestVersion;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.model.TicketVersion;

public class IbatisBuyRequestDao extends SqlMapClientDaoSupport implements BuyRequestDao {
		  
	public List<BuyRequest> getBuyRequestListByUserid(BuyRequestSearch buyRequestSearch) {
		return getSqlMapClientTemplate().queryForList("getBuyRequestListByUserid", buyRequestSearch);
	}
	
	public List<BuyRequest> searchBuyRequests(BuyRequestSearch search) {
		StringTokenizer splitter = new StringTokenizer( search.getKeywords(), " ", false );
		while ( splitter.hasMoreTokens() ) {
			String token = splitter.nextToken();
			if (token.length() >= search.getKeywordsMinChars()) {
				// replace special characters with special meaning
				token = token.replace( "%", "\\%" );
				token = token.replace( "_", "\\_" );
				search.addKeyword( "%" + token.toLowerCase() + "%" );
			}
		}
		return getSqlMapClientTemplate().queryForList( "searchBuyRequests", search );
	}
	
	public int searchBuyRequestsCount(BuyRequestSearch search) {
		StringTokenizer splitter = new StringTokenizer( search.getKeywords(), " ", false );
		while ( splitter.hasMoreTokens() ) {
			String token = splitter.nextToken();
			if (token.length() >= search.getKeywordsMinChars()) {
				// replace special characters with special meaning
				token = token.replace( "%", "\\%" );
				token = token.replace( "_", "\\_" );
				search.addKeyword( "%" + token.toLowerCase() + "%" );
			}
		}
		return (Integer) getSqlMapClientTemplate().queryForObject( "searchBuyRequestCount", search );
	}
	
	public BuyRequest getBuyRequestById(Integer buyRequestId, Integer userId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "buyRequestId", buyRequestId );
		paramMap.put( "userId", userId );
	    BuyRequest buyRequest = (BuyRequest) getSqlMapClientTemplate().queryForObject("getBuyRequestById", paramMap);
	    if ( buyRequest != null ) {
	    	buyRequest.setCatIdsSet( getBuyRequestCategoryIds(buyRequest.getId()) );
	    }
	    return buyRequest;
	}
	
	public void deleteBuyRequestById(Integer buyRequestId) {
		getSqlMapClientTemplate().delete( "deleteBuyRequestById", buyRequestId, 1 );
	}
	
	public void updateBuyRequest(BuyRequest buyRequest) {
		getSqlMapClientTemplate().update( "updateBuyRequest", buyRequest, 1 );
		insertCategoryIds( buyRequest.getId(), buyRequest.getCatIdsSet() );
	}
	
	private Set<Object> getBuyRequestCategoryIds(Integer buyRequestId) throws DataAccessException
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Object[] args = { buyRequestId };
		Set<Object> categoryIds = new TreeSet<Object>();
		String query = "SELECT category_id FROM category_buyrequest where buyrequest_id = ?";
		SqlRowSet results = jdbcTemplate.queryForRowSet( query, args );
		while ( results.next() )
		{
			categoryIds.add( new Integer( results.getInt( "category_id" ) ) );
		}
		return categoryIds;
	}
	
	public int insertBuyRequest(BuyRequest buyRequest) {
		KeyHolder keyHolder = new GeneratedKeyHolder();
		String sql = "insert into buy_request (owner_id, created) values (?, now())";
		int types[] = { java.sql.Types.INTEGER };
		SqlUpdate su = new SqlUpdate( getSqlMapClientTemplate().getDataSource(), sql, types, 1 );
		Object[] args = { buyRequest.getOwnerId() };
		su.update( args, keyHolder );
		buyRequest.setId( new Integer( keyHolder.getKey().intValue() ) );
		getSqlMapClientTemplate().update( "updateBuyRequest", buyRequest, 1 );
		insertCategoryIds(buyRequest.getId(), buyRequest.getCatIdsSet());
		return keyHolder.getKey().intValue();
    }
	
	private void insertCategoryIds(Integer buyRequestId, Set categoryIds)
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Iterator iter = categoryIds.iterator();
		String query = "INSERT into category_buyrequest (category_id, buyrequest_id, new_entry) VALUES (?, ?, true) ON DUPLICATE KEY UPDATE new_entry = true  ";
		while ( iter.hasNext() )
		{
			Object[] args = { (Integer) iter.next(), buyRequestId };
			try
			{
				jdbcTemplate.update( query, args );
			}
			catch ( DataIntegrityViolationException e )
			{
				// do nothing
			}
		}
		getSqlMapClientTemplate().delete( "deleteBuyRequestCatId", buyRequestId );
		jdbcTemplate.execute( "update category_buyrequest set new_entry = false" );
	}
	
	public List<BuyRequestVersion> getBuyRequestVesrionList(BuyRequestVersion buyRequestVersion) {
		return getSqlMapClientTemplate().queryForList("getBuyRequestVesrionList", buyRequestVersion);
	}
	
	public void insertBuyRequestVesrion(BuyRequestVersion buyRequestVersion) {
		getSqlMapClientTemplate().insert("insertBuyRequestVesrion", buyRequestVersion);
	} 
	
	public List<BuyRequest> getBuyRequestWishListByUserid(BuyRequestSearch search) {
		return getSqlMapClientTemplate().queryForList("getBuyRequestWishListByUserid", search);
	}
	
	public void addToBuyRequestWishList(Integer userId, Set buyRequestIds)
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query = "INSERT into buy_request_list (userid, buy_request_id, created) VALUES (?, ?, now())";
		Iterator iter = buyRequestIds.iterator();
		while ( iter.hasNext() )
		{
			Object[] args = { userId, (Integer) iter.next() };
			try
			{
				jdbcTemplate.update( query, args );
			}
			catch ( DataIntegrityViolationException e )
			{
				// do nothing
			}
		}
	}

	public void removeFromBuyRequestWishList(Integer userId, Set buyRequestIds)
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String query = "DELETE FROM buy_request_list WHERE userid = ? AND buy_request_id = ?";
		Iterator iter = buyRequestIds.iterator();
		while ( iter.hasNext() )
		{
			Object[] args = { userId, (Integer) iter.next() };
			try
			{
				jdbcTemplate.update( query, args );
			}
			catch ( DataIntegrityViolationException e )
			{
				// do nothing
			}
		}
	}

}
