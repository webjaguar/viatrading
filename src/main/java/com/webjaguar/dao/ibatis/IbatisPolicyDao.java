/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.PolicyDao;
import com.webjaguar.model.Policy;

public class IbatisPolicyDao extends SqlMapClientDaoSupport implements PolicyDao {
	
	public List getPolicyList() {
		return getSqlMapClientTemplate().queryForList("getPolicyList", null);
	}

	public int getTotal() {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		return jdbcTemplate.queryForInt("SELECT count(id) FROM policy");
	}
	
	public Policy getPolicyById(Integer policyId) {
		return (Policy) getSqlMapClientTemplate().queryForObject("getPolicyById", policyId);
	}
	
	public List getPolicies() {
		return getSqlMapClientTemplate().queryForList("getPolicies", null);		
	}
	
	public void deletePolicy(Integer policyId) {
		getSqlMapClientTemplate().delete("deletePolicy", policyId);	
	}
	
	public void insertPolicy(Policy policy) {
		getSqlMapClientTemplate().insert("insertPolicy", policy);
	}
	
	public void updatePolicy(Policy policy) {
		getSqlMapClientTemplate().update("updatePolicy", policy, 1);
	}
	
	public void updateRanking(final List data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
        String sql = "UPDATE policy SET rank = ? WHERE id = ?";		
        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map entry = (Map) data.get(i);
				if (entry.get("rank") == null) {
					ps.setNull(1, Types.NULL);			
				} else {
					ps.setInt(1, ((Integer) entry.get("rank")).intValue());
				}
				ps.setInt(2, ((Integer) entry.get("id")).intValue());
			}
			public int getBatchSize() {
				return data.size();
			}
		});
	}	
}
