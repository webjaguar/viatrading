package com.webjaguar.dao.ibatis;

import java.util.List;
import java.util.Map;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.ManufacturerDao;
import com.webjaguar.model.Manufacturer;
import com.webjaguar.model.ManufacturerSearch;

public class IbatisManufacturerDao extends SqlMapClientDaoSupport implements ManufacturerDao {

	public void insertManufacturer(Manufacturer manufacturer) {
		getSqlMapClientTemplate().insert("insertManufacturer", manufacturer);	
	}

	public void deleteManufacturer(Integer manufacturerId) {
		getSqlMapClientTemplate().delete("deleteManufacturer", manufacturerId);
	}

	public void updateManufacturer(Manufacturer manufacturer) {
		getSqlMapClientTemplate().update("updateManufacturer", manufacturer);
	}
	
	public void updateProductManufacturerName(String old, String newName) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Object[] args = { newName, old };
		try {
			jdbcTemplate.update( "UPDATE product SET manufacture_name = ?  WHERE manufacture_name = ?", args );
		}
		catch ( DataIntegrityViolationException e )
		{
			// do nothing
		}
	
	}
	
	public void updateShoppingcartManufacturerName(String old, String newName) {

		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Object[] args = { newName, old };
		try {
			jdbcTemplate.update( "UPDATE shopping_cart SET manufacture_name = ?  WHERE manufacture_name = ?", args );
		}
		catch ( DataIntegrityViolationException e )
		{
			// do nothing
		}
	
	}

	public List<Manufacturer> getManufacturerList(ManufacturerSearch search) {
		return getSqlMapClientTemplate().queryForList("getManufacturerList", search);
	}

	public Manufacturer getManufacturerById(Integer manufacturerId) {
		return (Manufacturer) getSqlMapClientTemplate().queryForObject("getManufacturerById", manufacturerId);
	}

	public Manufacturer getManufacturerByName(String name) {
		return (Manufacturer) getSqlMapClientTemplate().queryForObject("getManufacturerByName", name);
	}
	
	public Map<String, Manufacturer> getManufacturerMap() {
		return getSqlMapClientTemplate().queryForMap("getManufacturerList", null, "name");
	}
	
	public boolean isProductExist(String manufacturerName) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Object[] args = { (String) manufacturerName };
		if ( jdbcTemplate.queryForInt( "SELECT count(id) FROM product WHERE manufacture_name = ?", args ) >= 1 ) {
			return true;
		}
		else {
			return false;
		}
	}
}