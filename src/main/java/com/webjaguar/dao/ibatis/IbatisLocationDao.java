/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.LocationDao;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Location;
import com.webjaguar.model.LocationSearch;

public class IbatisLocationDao extends SqlMapClientDaoSupport implements LocationDao {
	
	
	  public List<Location> getLocationList(LocationSearch locationSearch) {
		  return getSqlMapClientTemplate().queryForList("getLocationList", locationSearch);
	  }
	  
	  public List<Location> searchLocationList(LocationSearch search) {
		  StringTokenizer splitter = new StringTokenizer( search.getKeywords(), ",", false );
			while ( splitter.hasMoreTokens() ) {
				String token = splitter.nextToken();
				if (token.length() >= search.getKeywordsMinChars()) {
					// replace special characters with special meaning
					token = token.replace( "%", "\\%" );
					token = token.replace( "_", "\\_" );
					search.addKeyword( "%" + token.toLowerCase() + "%" );
				}
			}
			return getSqlMapClientTemplate().queryForList("searchLocationList", search);	
	  }
	  
	  public Map<String, Location> getLocationMap(LocationSearch locationSearch) {
		  Map<String, Location> shippedMap = new HashMap<String, Location>();
		  return getSqlMapClientTemplate().queryForMap( "getLocationList", locationSearch, "zip" );
	  }

	  public Location getLocationById(Integer userId) {
		  return (Location) getSqlMapClientTemplate().queryForObject("getLocationById", userId);
	  }
	  
	  public void updateLocation(Location location) {
		  getSqlMapClientTemplate().update("updateLocation", location, 1);
	  }
	  
	  public void insertLocation(Location location) throws DataAccessException {
		  getSqlMapClientTemplate().insert("insertLocation", location);
	  }
	  
	  public void deleteLocation(Integer locationId) throws DataAccessException {
		  getSqlMapClientTemplate().delete("deleteLocation", locationId);	
	  }
	  
	  public void insertLocationByCustomer(Customer customer) {
		  getSqlMapClientTemplate().insert("insertLocationByCustomer", customer);
	  }
	  
	  public void addToLocationWishList(Integer userId, Integer locationId) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "userId", userId );
		  paramMap.put( "locationId", locationId );
		  getSqlMapClientTemplate().insert("addToLocationWishList", paramMap);
	  }

	  public void removeFromLocationWishList(Integer userId, Set locationIds) {
		  JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		  String query = "DELETE FROM location_list WHERE userid = ? AND location_id = ?";
		  Iterator iter = locationIds.iterator();
		  while ( iter.hasNext() ) {
			  Object[] args = { userId, (Integer) iter.next() };
			  try {
				  jdbcTemplate.update( query, args );
			  }
			  catch ( DataIntegrityViolationException e ) {
				  // do nothing
			  }
		  }
	  }
	  
	  public List<Location> getLocationWishListByUserid(LocationSearch locationSearch) {
		  return getSqlMapClientTemplate().queryForList("getLocationWishListByUserid", locationSearch);
      }
	  
	  public void updateKeywordLocationByUserId(Location location) throws DataAccessException {
		  getSqlMapClientTemplate().update("updateKeywordLocationByUserId", location, 1);
	  }
	  
	  public List<Location> getLocationListByUserId(Integer userId, Integer limit) {
		  Map<String, Object> paramMap = new HashMap<String, Object>();
		  paramMap.put( "userId", userId );
		  paramMap.put( "limit", limit );
		  return getSqlMapClientTemplate().queryForList("getLocationListByUserId", paramMap);
      }
	  
	  public boolean isValidLocationId(int id)
		{
			JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
			Object[] args = { (Integer) id };
			if ( jdbcTemplate.queryForInt( "SELECT count(id) FROM locations where id = ?", args ) == 0 )
			{
				return false;
			}
			else
			{
				return true;
			}
		}
}
