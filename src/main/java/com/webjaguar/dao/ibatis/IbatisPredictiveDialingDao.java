package com.webjaguar.dao.ibatis;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.PredictiveDialingDao;
import com.webjaguar.model.Customer;

public class IbatisPredictiveDialingDao extends SqlMapClientDaoSupport implements PredictiveDialingDao{
	
	public void insertCustomerToPredictiveDialing(List<Customer> customerList){
		getSqlMapClientTemplate().delete("cleanPredictiveDialingCustomerList");
		for(Customer customer : customerList){
			getSqlMapClientTemplate().insert("insertCustomerToPredictiveDialing",customer);
		}
	}
}
