/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 06.26.2009
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.VendorDao;
import com.webjaguar.thirdparty.dsi.DsiCategory;
import com.webjaguar.thirdparty.ingrammicro.IngramMicroCategory;
import com.webjaguar.thirdparty.koleImports.KoleImportsCategory;
import com.webjaguar.thirdparty.dsdi.DsdiCategory;
import com.webjaguar.thirdparty.synnex.SynnexCategory;
import com.webjaguar.thirdparty.techdata.TechdataCategory;
import com.webjaguar.thirdparty.techdata.TechdataManufacturer;

public class IbatisVendorDao extends SqlMapClientDaoSupport implements VendorDao {
	  
	// TechData
	public void insertTechdataManufacturers(List<TechdataManufacturer> manufacturers) {
		for (TechdataManufacturer manufacturer: manufacturers) {
			getSqlMapClientTemplate().insert("insertTechdataManufacturer", manufacturer);
		}
	}
	public void insertTechdataCategories(List<TechdataCategory> categories) {
		for (TechdataCategory category: categories) {
			getSqlMapClientTemplate().insert("insertTechdataCategory", category);
		}		
	}
	public List<TechdataCategory> getTechdataCategories() {
		Map<String, String> paramMap = new HashMap<String, String>();
		// grps
		paramMap.put("type", "grps");
		List<TechdataCategory> categories = getSqlMapClientTemplate().queryForList("getTechdataCategories", paramMap);
		// cats
		paramMap.put("type", "cats");
		categories.addAll(getSqlMapClientTemplate().queryForList("getTechdataCategories",  paramMap));
		// subs
		paramMap.put("type", "subs");
		categories.addAll(getSqlMapClientTemplate().queryForList("getTechdataCategories",  paramMap));
		return categories;
	}
	public void updateTechdataCategories(List<TechdataCategory> categories) {
		for (TechdataCategory category: categories) {
			try {
				getSqlMapClientTemplate().update("updateTechdataCategory", category);						
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}
	public List<TechdataManufacturer> getTechdataManufacturers() {
		return getSqlMapClientTemplate().queryForList("getTechdataManufacturers");
	}
	public List<String> getTechdataRestrictedProductLines() {
		return getSqlMapClientTemplate().queryForList("getTechdataRestrictedProductLines");
	}
	public Map<String, TechdataCategory> getTechdataCategoryMap() {
		return getSqlMapClientTemplate().queryForMap("getTechdataCategoryMap", null, "code");
	}
	public Map<String, String> getTechdataManufacturerMap() {
		return getSqlMapClientTemplate().queryForMap("getTechdataManufacturers", null, "vendCode", "vendName");
	}
	
	// IngramMicro
	public void insertIngramMicroCategories(List<IngramMicroCategory> categories) {
		for (IngramMicroCategory category: categories) {
			getSqlMapClientTemplate().insert("insertIngramMicroCategory", category);
		}
	}
	public List<IngramMicroCategory> getIngramMicroCategories() {
		return getSqlMapClientTemplate().queryForList("getIngramMicroCategories");
	}
	public void updateIngramMicroCategories(List<IngramMicroCategory> categories) {
		for (IngramMicroCategory category: categories) {
			try {
				getSqlMapClientTemplate().update("updateIngramMicroCategory", category);				
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}
	public Map<String, IngramMicroCategory> getIngramMicroCategoryMap() {
		return getSqlMapClientTemplate().queryForMap("getIngramMicroCategoryMap", null, "code");
	}
	public int updateIngramMicroInventoryBySkus(final List<Map <String, Object>> data) {
		int count = 0;
		
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String sql = "UPDATE product SET inventory = ?, inventory_afs = ?, last_modified = now() WHERE sku = ?";
		int[] success = jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Map<String, Object> entry = data.get(i);
				ps.setInt(1, ((Integer) entry.get("inventory")).intValue());
				ps.setInt(2, ((Integer) entry.get("inventory")).intValue());
				ps.setString(3, entry.get("sku").toString());
			}

			public int getBatchSize() {
				return data.size();
			}
		} );
		for (int i=0; i<success.length; i++) {
			if (success[i] == 1) count++;
		}
		
		return count;
	}	
	
	// Synnex
	public void insertSynnexCategories(List<SynnexCategory> categories) {
		for (SynnexCategory category: categories) {
			getSqlMapClientTemplate().insert("insertSynnexCategory", category);			
		}
	}
	public List<SynnexCategory> getSynnexCategories() {
		return getSqlMapClientTemplate().queryForList("getSynnexCategories");		
	}
	public void updateSynnexCategories(List<SynnexCategory> categories) {
		for (SynnexCategory category: categories) {
			try {
				getSqlMapClientTemplate().update("updateSynnexCategory", category);				
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}		
	}
	public Map<String, SynnexCategory> getSynnexCategoryMap() {
		return getSqlMapClientTemplate().queryForMap("getSynnexCategoryMap", null, "code");		
	}
	
	// DSI
	public void insertDsiCategories(List<DsiCategory> categories) {
		for (DsiCategory category: categories) {
			getSqlMapClientTemplate().insert("insertDsiCategory", category);
		}
	}
	public List<DsiCategory> getDsiCategories() {
		return getSqlMapClientTemplate().queryForList("getDsiCategories");
	}
	public void updateDsiCategories(List<DsiCategory> categories) {
		for (DsiCategory category: categories) {
			try {
				getSqlMapClientTemplate().update("updateDsiCategory", category);				
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}
	public Map<String, DsiCategory> getDsiCategoryMap() {
		return getSqlMapClientTemplate().queryForMap("getDsiCategoryMap", null, "code");
	}
	
	// ASI
	public List<String> getAsiSupplier() {
		return getSqlMapClientTemplate().queryForList("getAsiSupplier");
	}
	/* TO BE REMOVE
	public List<AsiCategory> getAsiCategoriesBySupplier(String supplier) {
		Map<String, String> paramMap = new HashMap<String, String>();
		// grps
		paramMap.put("type", "suppliers");
		paramMap.put("supplier", supplier);
		List<AsiCategory> categories = getSqlMapClientTemplate().queryForList("getAsiCategoriesBySupplier", paramMap);
		// cats
		paramMap.put("type", "cats");
		categories.addAll(getSqlMapClientTemplate().queryForList("getAsiCategoriesBySupplier",  paramMap));
		// subs
		paramMap.put("type", "subs");
		categories.addAll(getSqlMapClientTemplate().queryForList("getAsiCategoriesBySupplier",  paramMap));
		return categories;
	}
	public void updateAsiCategories(List<AsiCategory> categories) {
		for (AsiCategory category: categories) {
			try {
				getSqlMapClientTemplate().update("updateAsiCategory", category);				
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}
	public Map<String, AsiCategory> getAsiCategoryMap() {
		return getSqlMapClientTemplate().queryForMap("getAsiCategoryMap", null, "code");
	}
	public void insertAsiCategories(List<AsiCategory> categories) {
		for (AsiCategory category: categories) {
			getSqlMapClientTemplate().insert("insertAsiCategory", category);
		}
	}
	public List<AsiCategory> getAsiCategories(Search search) {
		return getSqlMapClientTemplate().queryForList("getAsiCategories", search);
	}
	*/
	// Kole Imports
	public void insertKoleImportsCategories(List<KoleImportsCategory> categories) {
		for (KoleImportsCategory category: categories) {
			getSqlMapClientTemplate().insert("insertKoleImportsCategory", category);
		}
	}
	public List<KoleImportsCategory> getKoleImportsCategories() {
		return getSqlMapClientTemplate().queryForList("getKoleImportsCategories");
	}
	public void updateKoleImportsCategories(List<KoleImportsCategory> categories) {
		for (KoleImportsCategory category: categories) {
			try {
				getSqlMapClientTemplate().update("updateKoleImportsCategory", category);				
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}
	public Map<String, KoleImportsCategory> getKoleImportsCategoryMap() {
		return getSqlMapClientTemplate().queryForMap("getKoleImportsCategoryMap", null, "code");
	}
	
	// Dsdi Imports
	public void insertDsdiCategories(List<DsdiCategory> categories) {
		for (DsdiCategory category: categories) {
			getSqlMapClientTemplate().insert("insertDsdiCategory", category);
		}
	}
	public List<DsdiCategory> getDsdiCategories() {
		return getSqlMapClientTemplate().queryForList("getDsdiCategories");
	}
	public void updateDsdiCategories(List<DsdiCategory> categories) {
		for (DsdiCategory category: categories) {
			try {
				getSqlMapClientTemplate().update("updateDsdiCategory", category);				
			} catch (DataIntegrityViolationException e) {
				// do nothing
			}
		}
	}
	public Map<String, DsdiCategory> getDsdiCategoryMap() {
		return getSqlMapClientTemplate().queryForMap("getDsdiCategoryMap", null, "code");
	}
	
}

