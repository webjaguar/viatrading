/*
 * Copyright 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 * @since 01.23.2007
 */

package com.webjaguar.dao.ibatis;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.orm.ibatis.SqlMapClientCallback;

import com.ibatis.sqlmap.client.SqlMapExecutor;
import com.webjaguar.model.ConditionPromo;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.Promo;
import com.webjaguar.model.PromoSearch;
import com.webjaguar.model.SalesTag;

public class IbatisPromoDao extends org.springframework.orm.ibatis.support.SqlMapClientDaoSupport implements com.webjaguar.dao.PromoDao
{
	// Promo
	public List<Promo> getPromosList( PromoSearch promoSearch) {
		return getSqlMapClientTemplate( ).queryForList( "getPromoList", promoSearch );
	}
	public List<Promo> getDynamicPromosList( PromoSearch promoSearch) {
		return getSqlMapClientTemplate( ).queryForList( "getDynamicPromoList", promoSearch );
	}
	public List<Promo> getCampaignList( PromoSearch promoSearch) {
		return getSqlMapClientTemplate( ).queryForList( "getCampaignList", promoSearch );
	}
	public List<Promo> getEligiblePromoList( Customer customer, Order order) {
		Map<String,Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customer", customer);
		
		// calculate subtotal for each brand
		Map<String, Double> brandTotalMap = new HashMap<String, Double>();
		if(order != null) {
			paramMap.put("orderShippingId", order.getCustomShippingId());
			for(LineItem lineItem : order.getLineItems()) {
				if(lineItem.getManufactureName() != null && !lineItem.getManufactureName().equals("")) {
					Double newTotal = lineItem.getTotalPrice();
					if(brandTotalMap.get(lineItem.getManufactureName()) != null) {
						newTotal = newTotal + brandTotalMap.get(lineItem.getManufactureName());
					}
					brandTotalMap.put(lineItem.getManufactureName(), newTotal);
				} 
			}
			paramMap.put("brandSet", brandTotalMap.keySet().toArray());
		}
		
		List<Promo> eligiblePromoList = getSqlMapClientTemplate( ).queryForList( "getEligiblePromoList", paramMap );
		
		// remove those promos whose brand minimum total is above than shopping cart subtotal for that brand
		Iterator<Promo> promoIter = eligiblePromoList.iterator();
		while (promoIter.hasNext()) {
			Promo promo = promoIter.next();
			if(promo.getMinOrderPerBrand() != null && promo.getDiscountType().equalsIgnoreCase("product")) {
				if(getConditionPromo(promo.getPromoId()).getCondition().get("Brand")!=null){
					Map<Object,String> innerMap = getConditionPromo(promo.getPromoId()).getCondition().get("Brand");
					Set<Object> set = innerMap.keySet() ;
					
					promo.setBrandSet( set );
				}
				
				Iterator<Object> itr = promo.getBrandSet().iterator();
				while(itr.hasNext()) {
					String tempPromo = itr.next().toString();
					if(brandTotalMap.get(tempPromo) != null && brandTotalMap.get(tempPromo) < promo.getMinOrderPerBrand()) {
						promoIter.remove();
						break;
					}
				}
			}
		}
		
		// keep only those promos which qualifies for highest 'minimum order per brands' and remove others for same brand
		Map<String, Promo> brandPromoMap = new HashMap<String, Promo>();
		for(Promo promo : eligiblePromoList) {
			for(Object brand : promo.getBrandSet()) {
				if(brandPromoMap.get(brand.toString()) != null) {
					if(brandPromoMap.get(brand.toString()).getMinOrderPerBrand() < promo.getMinOrderPerBrand()) {
						brandPromoMap.put(brand.toString(), promo);
					}
				} else {
					brandPromoMap.put(brand.toString(), promo);
				}
			}
		}
		
		Iterator<Promo> iterPromo = eligiblePromoList.iterator();
		while(iterPromo.hasNext()) {
			Promo promo = iterPromo.next();
			if(promo.getDiscountType().equalsIgnoreCase("product")) {
				for(Object brand : promo.getBrandSet()) {
					if(brandPromoMap.get(brand) != null && !brandPromoMap.get(brand).getPromoId().equals(promo.getPromoId())) {
						iterPromo.remove();
						break;
					}
				}
			}
		}
		
		return eligiblePromoList;
	}

	public Promo getPromoById(Integer promoId) {
		Promo promo = (Promo) getSqlMapClientTemplate( ).queryForObject( "getPromoById", promoId );
		if (promo!= null && getConditionPromo(promoId).getCondition()!=null) {
			
			if(getConditionPromo(promoId).getCondition().get("Group")!=null){
				Map<Object,String> innerMap = getConditionPromo(promoId).getCondition().get("Group");
				Set<Object> set = innerMap.keySet() ;
				String operator = innerMap.get(set.toArray()[0]);
				
				promo.setGroupIdSet( set );
				promo.setGroupType( operator );
			}
			
			if(getConditionPromo(promoId).getCondition().get("States")!=null){
				Map<Object,String> innerMap = getConditionPromo(promoId).getCondition().get("States");
				Set<Object> set = innerMap.keySet() ;
				String operator = innerMap.get(set.toArray()[0]);
				
				promo.setStates( set.toArray()[0].toString() );
				promo.setStateType( operator );
			}
		
			if(getConditionPromo(promoId).getCondition().get("Sku")!=null){
				Map<Object,String> innerMap = getConditionPromo(promoId).getCondition().get("Sku");
				Set<Object> set = innerMap.keySet() ;
				
				promo.setSkuSet( set );
			}
			 
			if(getConditionPromo(promoId).getCondition().get("CategoryIds")!=null){
				Map<Object,String> innerMap = getConditionPromo(promoId).getCondition().get("CategoryIds");
				Set<Object> set = innerMap.keySet() ;
				
				promo.setCategoryIdSet(set);
			}
			
			if(getConditionPromo(promoId).getCondition().get("ParentSku")!=null){
				Map<Object,String> innerMap = getConditionPromo(promoId).getCondition().get("ParentSku");
				Set<Object> set = innerMap.keySet() ;
				promo.setParentSkuSet( set );
			}
			
			if(getConditionPromo(promo.getPromoId()).getCondition().get("Brand")!=null){
				Map<Object,String> innerMap = getConditionPromo(promo.getPromoId()).getCondition().get("Brand");
				Set<Object> set = innerMap.keySet() ;
				
				promo.setBrandSet( set );
			}
			if(getConditionPromo(promo.getPromoId()).getCondition().get("customShippingId")!=null){
				Map<Object,String> innerMap = getConditionPromo(promo.getPromoId()).getCondition().get("customShippingId");
				Set<Object> set = innerMap.keySet() ;
				
				promo.setShippingIdSet( set );
			}
		}
		return promo;
	}
	
	public Promo getPromoByName( String promoName ) {
		Promo promo = (Promo) getSqlMapClientTemplate( ).queryForObject( "getPromoByName", promoName );
		if (promo!= null && getConditionPromo(promo.getPromoId()).getCondition()!=null) {
			
			if(getConditionPromo(promo.getPromoId()).getCondition().get("Group")!=null){
			Map<Object,String> innerMap = getConditionPromo(promo.getPromoId()).getCondition().get("Group");
			Set<Object> set = innerMap.keySet() ;
			String operator = innerMap.get(set.toArray()[0]);
			
			promo.setGroupIdSet( set );
			promo.setGroupType( operator );
			}
			
			if(getConditionPromo(promo.getPromoId()).getCondition().get("States")!=null){
				Map<Object,String> innerMap = getConditionPromo(promo.getPromoId()).getCondition().get("States");
				Set<Object> set = innerMap.keySet() ;
				String operator = innerMap.get(set.toArray()[0]);
				
				promo.setStates( set.toArray()[0].toString() );
				promo.setStateType( operator );
			}
			
			if(getConditionPromo(promo.getPromoId()).getCondition().get("Sku")!=null){
				Map<Object,String> innerMap = getConditionPromo(promo.getPromoId()).getCondition().get("Sku");
				Set<Object> set = innerMap.keySet() ;
				
				promo.setSkuSet( set );
			}
			
			if(getConditionPromo(promo.getPromoId()).getCondition().get("ParentSku")!=null){
				Map<Object,String> innerMap = getConditionPromo(promo.getPromoId()).getCondition().get("ParentSku");
				Set<Object> set = innerMap.keySet() ;
				
				promo.setParentSkuSet( set );
			}
			
			if(getConditionPromo(promo.getPromoId()).getCondition().get("CategoryIds")!=null){
				Map<Object,String> innerMap = getConditionPromo(promo.getPromoId()).getCondition().get("CategoryIds");
				Set<Object> set = innerMap.keySet() ;
				
				promo.setCategoryIdSet(set);
			}
			
			if(getConditionPromo(promo.getPromoId()).getCondition().get("Brand")!=null){
				Map<Object,String> innerMap = getConditionPromo(promo.getPromoId()).getCondition().get("Brand");
				Set<Object> set = innerMap.keySet() ;
				
				promo.setBrandSet( set );
			}
			
			if(getConditionPromo(promo.getPromoId()).getCondition().get("customShippingId")!=null){
				Map<Object,String> innerMap = getConditionPromo(promo.getPromoId()).getCondition().get("customShippingId");
				Set<Object> set = innerMap.keySet() ;
				
				promo.setShippingIdSet( set );
			}
		}
		return promo;
	}
	public Promo getPromo( String promoName ) {
		Promo promo = (Promo) getSqlMapClientTemplate( ).queryForObject( "getPromo", promoName );
		return promo;
	}

	public Promo getPromoWithCondition( Promo promo, Map<String, Object> conditionPromo ) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "promo", promo );
		paramMap.put( "minOrder", conditionPromo.get("minOrder") );
		paramMap.put( "tempPromoCode", conditionPromo.get("tempPromoCode") );
		paramMap.put( "userId", conditionPromo.get("userId") );
		paramMap.put( "groupType", conditionPromo.get("groupType") );
		paramMap.put( "state", conditionPromo.get("state") );
		paramMap.put( "stateType", conditionPromo.get("stateType") );
		paramMap.put( "customShippingId", conditionPromo.get("customShippingId") );
		paramMap.put( "orderShippingId", conditionPromo.get("orderShippingId") );
		paramMap.put( "cartSkuSet", conditionPromo.get("cartSkuSet") );
		return (Promo) getSqlMapClientTemplate( ).queryForObject( "getPromoWithCondition", paramMap );
	}
	
	public Promo getAdminPromoWithCondition( Promo promo, Map<String, Object> conditionPromo ) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put( "promo", promo );
		paramMap.put( "minOrder", conditionPromo.get("minOrder") );
		paramMap.put( "tempPromoCode", conditionPromo.get("tempPromoCode") );
		paramMap.put( "userId", conditionPromo.get("userId") );
		paramMap.put( "groupType", conditionPromo.get("groupType") );
		paramMap.put( "state", conditionPromo.get("state") );
		paramMap.put( "stateType", conditionPromo.get("stateType") );
		paramMap.put( "customShippingId", conditionPromo.get("customShippingId") );
		paramMap.put( "orderShippingId", conditionPromo.get("orderShippingId") );
		paramMap.put( "cartSkuSet", conditionPromo.get("cartSkuSet") );
		paramMap.put( "orderId", conditionPromo.get("orderId") );
		return (Promo) getSqlMapClientTemplate( ).queryForObject( "getAdminPromoWithCondition", paramMap );
	}

	public Integer getPromoIdByName( String promoName ) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getPromoIdByName", promoName);  
	}

	public void insertPromo(Promo promo) {
	    getSqlMapClientTemplate( ).insert( "insertPromo", promo );
	    Integer promoId = getPromoIdByName(promo.getTitle());	
	    promo.setPromoId(promoId);
//	    System.out.println("--ALLAM----");
	    if (promo.getGroupIdSet().size() != 0 || promo.getStates() != null || 
	    		((promo.getSkuSet() != null) && (promo.getSkuSet().size() != 0)) || 
	    		((promo.getBrandSet() != null) && (promo.getBrandSet().size() != 0)) ||
	    		((promo.getCategoryIdSet() != null) && (promo.getCategoryIdSet().size() != 0))) {
	    	
			insertConditionPromo(promoId, setConditionToMap(promo));
		}
	}
	
	public void insertPromos( final List<Promo> promos) {
//	    getSqlMapClientTemplate( ).insert( "insertPromos", promos );
	    try {
	        Integer updateCount = (Integer) getSqlMapClientTemplate().execute( new SqlMapClientCallback<Object>() {
	            public Object doInSqlMapClient( SqlMapExecutor executor ) throws SQLException {
	                Iterator<Promo> iter = promos.iterator();
	                while( iter.hasNext() ) {
	                	Promo promo = iter.next();
	                	executor.startBatch();
	                    executor.insert( "insertPromo", promo );
//	                    Integer promoId = (Integer) executor.insert("getPromoIdByName",promo.getTitle() );
	                    executor.executeBatch();
            	 
	                }
					return iter;
	                	
	            }
	        });
	      
	    } catch (Exception e ) {
	       System.out.println(e);
	    }
	    
	    try{
	    	
		      Iterator<Promo> iter = promos.iterator();
	          while( iter.hasNext() ) {
	        	  
	          	 Promo promo = iter.next();
	          	 Integer promoId = getPromoIdByName(promo.getTitle());
	     	     System.out.println("promoId1: " + promoId);
	     	     promo.setPromoId(promoId);
	     	   if (promo.getGroupIdSet().size() != 0 || promo.getStates() != null || 
	   	    		((promo.getSkuSet() != null) && (promo.getSkuSet().size() != 0)) || 
	   	    		((promo.getBrandSet() != null) && (promo.getBrandSet().size() != 0)) ) {
	   			        insertConditionPromo(promoId, setConditionToMap(promo));
	  		      }
	          }
			
	    }catch(Exception e){
	    	System.out.println(e);
	    }

	}
	
	public Integer getPromoCount(PromoSearch promoSearch) {
		return (Integer) getSqlMapClientTemplate().queryForObject("getPromoCount", promoSearch);		
	}

	public void updatePromo(Promo promo) {
		getSqlMapClientTemplate( ).update( "updatePromo", promo, 1 );
		insertConditionPromo(promo.getPromoId(), setConditionToMap(promo));
	}
	
	private ConditionPromo setConditionToMap(Promo promo){
		ConditionPromo conditionPromo = new ConditionPromo();
		Map<String, Map<Object,String>> map = new HashMap<String, Map<Object,String>>();
		Map<Object, String> innerMap;
		if(promo.getGroupIdSet()!=null && promo.getGroupIdSet().size()!=0){
			innerMap = new HashMap< Object, String>();
			Iterator<Object> iter = promo.getGroupIdSet().iterator();
			while(iter.hasNext()){
				innerMap.put(iter.next(), promo.getGroupType());
			}
			map.put("Group", innerMap);
		}
		if(promo.getStates()!=null && !promo.getStates().equals("")){
			innerMap = new HashMap< Object, String>();
			innerMap.put(promo.getStates(), promo.getStateType());
			map.put("States", innerMap);
		}
		if(promo.getSkuSet()!=null && promo.getSkuSet().size()!=0){
			innerMap = new HashMap< Object, String>();
			Iterator<Object> iter = promo.getSkuSet().iterator();
			while(iter.hasNext()){
				innerMap.put(iter.next(), "include");
			}
			map.put("Sku", innerMap);
		}
		
		if(promo.getParentSkuSet()!=null && promo.getParentSkuSet().size()!=0){
			innerMap = new HashMap< Object, String>();
			Iterator<Object> iter = promo.getParentSkuSet().iterator();
			while(iter.hasNext()){
				innerMap.put(iter.next(), "include");
			}
			map.put("ParentSku", innerMap);
		}
		if(promo.getBrandSet()!=null && promo.getBrandSet().size()!=0){
			innerMap = new HashMap< Object, String>();
			Iterator<Object> iter = promo.getBrandSet().iterator();
			while(iter.hasNext()){
				innerMap.put(iter.next(), "include");
			}
			map.put("Brand", innerMap);
		}
		if(promo.getCategoryIdSet()!=null && promo.getCategoryIdSet().size()!=0){
			innerMap = new HashMap< Object, String>();
			Iterator<Object> iter = promo.getCategoryIdSet().iterator();
			while(iter.hasNext()){
				innerMap.put(iter.next(), "include");
			}
			map.put("CategoryIds", innerMap);
		}
		if(promo.getShippingIdSet()!=null && promo.getShippingIdSet().size()!=0 && promo.getDiscountType().equalsIgnoreCase("shipping")){
			innerMap = new HashMap< Object, String>();
			Iterator<Object> iter = promo.getShippingIdSet().iterator();
			while(iter.hasNext()){
				innerMap.put(iter.next(), "include");
			}
			map.put("customShippingId", innerMap);
		}
		conditionPromo.setCondition(map);
		return conditionPromo;
	}

	private void insertConditionPromo(Integer promoId, ConditionPromo conditionPromo)
	{
		getSqlMapClientTemplate().delete("deletePromoCondition", promoId);
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		Iterator iter = conditionPromo.getCondition().entrySet().iterator();
		String query = "INSERT into condition_promo ( promo_id, type, value, operator ) VALUES (?, ?, ?, ? )";
		while ( iter.hasNext() ) {
			Map.Entry<String, Map<Object,String>> entry = (Map.Entry)iter.next();
			Iterator innerIter = entry.getValue().entrySet().iterator();
			while(innerIter.hasNext()){
				Map.Entry<Object,String> innerEntry = (Map.Entry)innerIter.next();
				Object[] args = { promoId, entry.getKey(), innerEntry.getKey(), innerEntry.getValue()  };
//				System.out.println(promoId + "," +  entry.getKey() + ","  + innerEntry.getKey() + "," + innerEntry.getValue());
				try {
					jdbcTemplate.update( query, args );
				}
				catch ( DataIntegrityViolationException e ) {
					// do nothing
				}
			}
		}
	}
	
	private ConditionPromo getConditionPromo(Integer promoId)
	{
	 List<String> typeList = getSqlMapClientTemplate( ).queryForList( "getConditionTypeListByPromoId", promoId );
	
	 JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
	 Map<Object,String> innerMap; 
	 Map<String, Map<Object,String>> map = new HashMap<String, Map<Object,String>>();
	 ConditionPromo condition = new ConditionPromo();	
	 
	 for(int i=0;i<typeList.size();i++){
		 innerMap = new HashMap<Object, String>();
		 String query = "SELECT value,operator FROM condition_promo WHERE promo_id = ? AND type= ? ORDER BY type";
		 Object[] args = { promoId, typeList.get(i) };
		 SqlRowSet results = jdbcTemplate.queryForRowSet( query, args );
		 while(results.next()){
			 innerMap.put(results.getObject("value"), results.getString("operator"));
		 }
		 map.put(typeList.get(i), innerMap);
	 }
	 condition.setCondition(map);
	 return condition;
	}
	
	public void deletePromoById(Integer promoId) {
		getSqlMapClientTemplate( ).delete( "deletePromo", promoId );
		getSqlMapClientTemplate().delete("deletePromoCondition", promoId);
	}
	
	public void deletePromoByIds(int[] promoIds) {

		getSqlMapClientTemplate( ).delete( "deletePromos", promoIds );
	}
	
	public void updateRanking(final List<Map <String, Integer>> data)
	{
		JdbcTemplate jdbcTemplate = new JdbcTemplate( getSqlMapClientTemplate().getDataSource() );
		String sql = "UPDATE promos SET rank = ? WHERE promo_id = ?";
		jdbcTemplate.batchUpdate( sql, new BatchPreparedStatementSetter()
		{
			public void setValues(PreparedStatement ps, int i) throws SQLException
			{
				Map<String, Integer> entry = data.get( i );
				if ( entry.get( "rank" ) == null )
				{
					ps.setNull( 1, Types.NULL );
				}
				else
				{
					ps.setInt( 1, entry.get( "rank" ) );
				}
				ps.setInt( 2, entry.get( "id" ) );
			}

			public int getBatchSize()
			{
				return data.size();
			}
		} );
	}
	
	//Sales Tag
	public List<SalesTag > getSalesTagList() {
		return getSqlMapClientTemplate( ).queryForList( "getSalesTagList", null );
	}
	
	public SalesTag getSalesTagById(Integer salesTagId) {
		return (SalesTag) getSqlMapClientTemplate( ).queryForObject( "getSalesTagById", salesTagId );
	}
	
	public int insertSalesTag( SalesTag salesTag )
	{
		getSqlMapClientTemplate().insert( "insertSalesTag", salesTag );
		return salesTag.getTagId();

	}

	public void updateSalesTag( SalesTag salesTag ) {
		getSqlMapClientTemplate( ).update( "updateSalesTag", salesTag, 1 );
	}

	public void deleteSalesTagById( Integer salesTagId ) {
		getSqlMapClientTemplate( ).delete( "deleteSalesTagById", salesTagId );
	}
	
	public Map<String, Long> getSalesTagCodeMap() {
		return getSqlMapClientTemplate().queryForMap("getSalesTagCodeMap", null, "title", "tag_id");
	}
	
	public void deleteSalesTags(String source) {
		getSqlMapClientTemplate( ).delete( "deleteSalesTags", source );
	}
	
	public void insertSalesTag(final List <CsvFeed> csvFeedList, final List<Map <String, Object>> data) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "INSERT into sales_tags (tag_id, title, discount, start_date, end_date) VALUES (?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE title = ?, discount = ?, start_date = ?, end_date = ? ";
		for (Map<String, Object> map: data) {
			Object[] args = { map.get( "tag_id" ).toString(), map.get( "title" ).toString(), map.get( "discount" ).toString(), map.get( "start_date" ).toString(), map.get( "end_date" ).toString(), map.get( "title" ).toString(), map.get( "discount" ).toString(), map.get( "start_date" ).toString(), map.get( "end_date" ).toString() };	
			try {
				jdbcTemplate.update(query, args);
			}
			catch ( DataIntegrityViolationException e ) {
				// do nothing
				e.printStackTrace();
			}
		}
	}

}
