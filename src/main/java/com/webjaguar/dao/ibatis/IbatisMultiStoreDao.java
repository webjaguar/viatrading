/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.09.2008
 */

package com.webjaguar.dao.ibatis;

import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.MultiStoreDao;
import com.webjaguar.model.MultiStore;

public class IbatisMultiStoreDao extends SqlMapClientDaoSupport implements MultiStoreDao {
	
	public void updateMultiStoreConfig(List<MultiStore> multiStores) {
		for (MultiStore multiStore: multiStores) {
			getSqlMapClientTemplate().update("updateMultiStoreConfig", multiStore);			
		}
	}
	
	public List<MultiStore> getMultiStore(String host) {
		return getSqlMapClientTemplate().queryForList("getMultiStore", host);
	}
	
	public Map<String, String> getStoreIdMap() {
		return getSqlMapClientTemplate().queryForMap("getStoreIdMap", null, "store_id", "host");
	}
}
