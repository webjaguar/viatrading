/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.dao.ibatis;

import com.webjaguar.model.CategoryLinkType;

public class CategoryLinkTypeHandler extends EnumTypeHandler<CategoryLinkType> {
	public CategoryLinkTypeHandler() {
		super(CategoryLinkType.class);
	}

}
