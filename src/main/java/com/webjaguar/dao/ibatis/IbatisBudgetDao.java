/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.26.2008
 */

package com.webjaguar.dao.ibatis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.webjaguar.dao.BudgetDao;
import com.webjaguar.model.Brand;
import com.webjaguar.model.BudgetProduct;
import com.webjaguar.model.BudgetProductSearch;
import com.webjaguar.model.Product;

public class IbatisBudgetDao extends SqlMapClientDaoSupport implements BudgetDao {
	  
	public List<Brand> getBrands(Integer userId) {
		return getSqlMapClientTemplate().queryForList("getBrands", userId);
	}
	
	public Double getOrderTotalByBrand(Brand brand, int userId, int year) {
		Double total = null;
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("brand", brand);
		paramMap.put("userId", userId);
		paramMap.put("year", year);		
		
		Double subTotal1 = (Double) getSqlMapClientTemplate().queryForObject("getOrderTotalByBrandWithoutCase", paramMap);
		if (subTotal1 != null) {
			total = subTotal1;
		}
		Double subTotal2 = (Double) getSqlMapClientTemplate().queryForObject("getOrderTotalByBrandWithCase", paramMap);
		if (subTotal2 != null) {
			total = (total != null) ? total : 0.0 + subTotal2;
		}
		
		return total;
	}
	
	public void updateBudget(int userId, List<Brand> brands) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		for (Brand brand: brands) {
			paramMap.put("brand", brand);
			getSqlMapClientTemplate().update("updateBudgetByBrand", paramMap);
		}
	}
		
	public List<Product> getProductExportByBrands(String protectedAccess) {
		return getSqlMapClientTemplate().queryForList("getProductExportByBrands", protectedAccess);
	}
	
	public List<Product> getProductListByBrand(Brand brand) {
		return getSqlMapClientTemplate().queryForList("getProductListByBrand", brand);
	}
	
	public Map<String, Object> getOrderReportByBrand(Brand brand) {
		return getSqlMapClientTemplate().queryForMap("getOrderReportByBrand", brand, "sku");
	}
	
	public List<Map<String, Object>> getOrderReportByBrand(Brand brand, String sku) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("brand", brand);		
		paramMap.put("sku", sku);		
		return getSqlMapClientTemplate().queryForList("getOrderReportByBrandSku", paramMap);
	}
	
	public Map<String, BudgetProduct> getBudgetProduct(int userId, int year, boolean shoppingCart) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("year", year);
		paramMap.put("shoppingCart", shoppingCart);
		return getSqlMapClientTemplate().queryForMap("getBudgetProduct", paramMap, "product.sku");
	}
	
	public Map<String, BudgetProduct> getBudgetProduct(int userId, int year, List<Product> productList) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("year", year);
		paramMap.put("productList", productList);
		return getSqlMapClientTemplate().queryForMap("getBudgetProduct", paramMap, "product.sku");		
	}
	
	public List<BudgetProduct> getBudgetProductList(BudgetProductSearch search) {
		return getSqlMapClientTemplate().queryForList("getBudgetProductList", search);
	}
	
	public BudgetProduct getBudgetProduct(int id) {
		return (BudgetProduct) getSqlMapClientTemplate().queryForObject("getBudgetProductById", id);
	}
	
	public void updateBudgetProduct(BudgetProduct budgetProduct) {
		getSqlMapClientTemplate().update("updateBudgetProduct", budgetProduct);
	}
	
	public List<Integer> getBudgetProductYears(BudgetProductSearch search) {
		return getSqlMapClientTemplate().queryForList("getBudgetProductYears", search);
	}
	
	public void insertBudgetProduct(BudgetProduct budgetProduct) {
		getSqlMapClientTemplate().insert("insertBudgetProduct", budgetProduct);
	}
	
	public void deleteBudgetProduct(int id) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		Object[] args = { id };
		jdbcTemplate.update("DELETE FROM budget_product where id = ?", args);
	}
}
