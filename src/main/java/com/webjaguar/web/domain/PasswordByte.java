package com.webjaguar.web.domain;

public class PasswordByte extends Password {

	public PasswordByte(String text) {
		super(text);
		// TODO Auto-generated constructor stub
		byte[] bytes = this.getText().getBytes();
		for (byte tempByte : bytes) {
			if ((tempByte >= 33 && tempByte <= 47) || // !"#$%&'()*+,-./
					(tempByte >= 58 && tempByte <= 64) || // :;<=>?@
					(tempByte >= 91 && tempByte <= 96) || // [\]^_`
					(tempByte >= 123 && tempByte <= 126) // {|}~
			) {
				this.setNumOfSpecial(this.getNumOfSpecial() + 1);
			}

			char tempChar = (char) tempByte;
			if (Character.isDigit(tempChar)) {
				this.setNumOfDigits(this.getNumOfDigits() + 1);
			}

			if (Character.isLetter(tempChar)) {
				this.setNumOfLetters(this.getNumOfLetters() + 1);
			}

			if (Character.isUpperCase(tempChar)) {
				this.setNumOfUpperLetters(this.getNumOfUpperLetters() + 1);
			}

			if (Character.isLowerCase(tempChar)) {
				this.setNumOfLowerLetters(this.getNumOfLowerLetters() + 1);
			}
		}
		this.setLength(this.getNumOfSpecial() + this.getNumOfLetters() + this.getNumOfDigits());
	}
}
