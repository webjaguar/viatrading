package com.webjaguar.web.domain;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;


public final class Encoder {

  private static final String SIGNING_ALGORITHM = "HmacSHA1";

  public static String sign(final String source, final String key) {
   if (key == null) {
     throw new IllegalArgumentException("key cannot be null");
   }
   Mac mac = null;

   try {
     mac = Mac.getInstance(SIGNING_ALGORITHM);
     mac.init(new SecretKeySpec(key.getBytes(), SIGNING_ALGORITHM));
   } catch (Exception e) {
     throw new RuntimeException(e);
   }

   final byte[] result = mac.doFinal(source.getBytes());
   final String signature = new String(Base64.encodeBase64(result));

   return signature;
 }

 public static String encode(final String source) {
   final String encoded = new String(Base64.encodeBase64(source.getBytes()));
   return encoded;
 }
 public static String decode(final String source) {
	   final String decoded = new String(Base64.decodeBase64(source.getBytes()));
	   return decoded;
 }
 
}