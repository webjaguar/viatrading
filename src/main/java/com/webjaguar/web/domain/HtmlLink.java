package com.webjaguar.web.domain;
public class HtmlLink {

	String link;
	String linkText;
	int startOffset;
	int endOffset;

	/**
	 * 
	 */
	public HtmlLink() {
		super();
	}

	public HtmlLink(String link, String linkText){
		this.link = link;
		this.linkText = linkText;
	}

	@Override
	public String toString() {
		return new StringBuffer("Link : ")
		.append(this.link + "\n")
		.append(" Link Text : ")
		.append(this.linkText + "\n")
		.append(" Link Start : ")
		.append(this.startOffset + "\n")
		.append(" Link End : ")
		.append(this.endOffset + "\n")
		.append("\n\n").toString();
	}
		
	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the linkText
	 */
	public String getLinkText() {
		return linkText;
	}

	/**
	 * @param linkText the linkText to set
	 */
	public void setLinkText(String linkText) {
		this.linkText = linkText;
	}

	/**
	 * @return the startOffset
	 */
	public int getStartOffset() {
		return startOffset;
	}

	/**
	 * @param startOffset the startOffset to set
	 */
	public void setStartOffset(int startOffset) {
		this.startOffset = startOffset;
	}

	/**
	 * @return the endOffset
	 */
	public int getEndOffset() {
		return endOffset;
	}

	/**
	 * @param endOffset the endOffset to set
	 */
	public void setEndOffset(int endOffset) {
		this.endOffset = endOffset;
	}
	}