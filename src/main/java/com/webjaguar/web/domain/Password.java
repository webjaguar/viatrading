package com.webjaguar.web.domain;

public class Password {

	private String text;
	private int numOfSpecial = 0;
	private int numOfLetters = 0;
	private int numOfUpperLetters = 0;
	private int numOfLowerLetters = 0;
	private int numOfDigits = 0;
	private int length = 0;

	public Password(String text) {
		super();
		this.text = text;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getNumOfSpecial() {
		return numOfSpecial;
	}
	public void setNumOfSpecial(int numOfSpecial) {
		this.numOfSpecial = numOfSpecial;
	}
	public int getNumOfLetters() {
		return numOfLetters;
	}
	public void setNumOfLetters(int numOfLetters) {
		this.numOfLetters = numOfLetters;
	}
	public int getNumOfUpperLetters() {
		return numOfUpperLetters;
	}
	public void setNumOfUpperLetters(int numOfUpperLetters) {
		this.numOfUpperLetters = numOfUpperLetters;
	}
	public int getNumOfLowerLetters() {
		return numOfLowerLetters;
	}
	public void setNumOfLowerLetters(int numOfLowerLetters) {
		this.numOfLowerLetters = numOfLowerLetters;
	}
	public int getNumOfDigits() {
		return numOfDigits;
	}
	public void setNumOfDigits(int numOfDigits) {
		this.numOfDigits = numOfDigits;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}

}
