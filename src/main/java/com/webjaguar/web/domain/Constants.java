package com.webjaguar.web.domain;

import java.math.BigInteger;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.oro.text.perl.Perl5Util;
import com.webjaguar.model.Category;

public class Constants {
	public final static int SECONDS_IN_DAY = 86400;
	// 64 protected Level
	public final static String FULL_PROTECTED_ACCESS = "1111111111111111111111111111111111111111111111111111111111111111";

	public static final String[] protectedLevels() {
		String[] protectedLevels = new String[64];
		for (int i = 0; i < protectedLevels.length; i++) {
			StringBuffer protectedLevel = new StringBuffer("1");
			for (int y = 0; y < i; y++)
				protectedLevel.append("0");
			protectedLevels[i] = protectedLevel.toString();
		}
		return protectedLevels;
	}

	public static final KeyBean[] MONTHS = new KeyBean[] { new KeyBean("Jan", "1"), new KeyBean("Feb", "2"), new KeyBean("Mar", "3"), new KeyBean("Apr", "4"), new KeyBean("May", "5"),
			new KeyBean("Jun", "6"), new KeyBean("Jul", "7"), new KeyBean("Aug", "8"), new KeyBean("Sep", "9"), new KeyBean("Oct", "10"), new KeyBean("Nov", "11"), new KeyBean("Dec", "12") };

	public static final KeyBean[] ASI_DISCOUNT_CODE = new KeyBean[] { new KeyBean("L", "0.70"), new KeyBean("M", "0.65"), new KeyBean("N", "0.60"), new KeyBean("O", "0.55"), new KeyBean("P", "0.50"),
			new KeyBean("Q", "0.45"), new KeyBean("R", "0.40"), new KeyBean("S", "0.35"), new KeyBean("T", "0.30"), new KeyBean("U", "0.25"), new KeyBean("V", "0.20"), new KeyBean("W", "0.15"),
			new KeyBean("X", "0.10"), new KeyBean("Y", "0.05"), new KeyBean("Z", "0.00") };
	public static final KeyBean[] COLOR_CODE = new KeyBean[] { new KeyBean("ACQA", "#00FFFF"), new KeyBean("BLACK", "#000000"), new KeyBean("BLUE", "#0000FF"), new KeyBean("BROWN", "#A52A2A"),
			new KeyBean("BERRY", "#EF6795"), new KeyBean("CHOCOLATE BROWN", "#4E2E28"), new KeyBean("FOREST", "#228B22"), new KeyBean("GOLD", "#FFD700"), new KeyBean("ORANGE", "#FFA500"),
			new KeyBean("ROYAL", "#4169E1"), new KeyBean("FUCHSIA", "#FF00FF"), new KeyBean("GRAY", "#808080"), new KeyBean("GRAY HEATHER", "#525252"), new KeyBean("GREEN", "#008000"),
			new KeyBean("KHAKI", "#F0E68C"), new KeyBean("LIGHT GRAY", "#D3D3D3"), new KeyBean("LIME", "#00FF00"), new KeyBean("MAROON", "#800000"), new KeyBean("NAVY", "#000080"),
			new KeyBean("NAVY HEATHER", "#1B3148"), new KeyBean("OLIVE", "#808000"), new KeyBean("PURPLE", "#800080"), new KeyBean("RED", "#FF0000"), new KeyBean("SILVER", "#C0C0C0"),
			new KeyBean("TEAL", "#008080"), new KeyBean("WHITE", "#FFFFFF"), new KeyBean("YELLOW", "#FFFF00") };
	public static final KeyBean[] PRODUCT_LAYOUT = new KeyBean[] { new KeyBean("000", "Default"), new KeyBean("001", "Layout 1"), new KeyBean("002", "Layout 2"), new KeyBean("003", "Dynamic"),
			new KeyBean("004", "Default rounded"), new KeyBean("005", "ASI"), new KeyBean("006", "Layout 6"), new KeyBean("007", "ASI 2"), new KeyBean("008", "Via"),
			new KeyBean("009", "HomeTheaterGear"), new KeyBean("010", "ROI"), new KeyBean("011", "CustomFrameStore"), new KeyBean("012", "ASI 3"), new KeyBean("013", "CustomFrameStore 2"),
			new KeyBean("014", "PPG"), new KeyBean("015", "Parent-Child"), new KeyBean("016", "Parent-Child 2"), new KeyBean("017", "CustomFrameStore 3"), new KeyBean("018", "PPG Non Apparel"),
			new KeyBean("019", "Fantasia"),new KeyBean("020", "Cosmetix"), new KeyBean("021", "Directpromoshop"), new KeyBean("022", "Bnoticed"), new KeyBean("023", "Yayme"), new KeyBean("046", "Via1"),
			new KeyBean("047", "Single Price"), new KeyBean("048", "Price break"), new KeyBean("049", "Loads"), new KeyBean("050", "On Sale"), new KeyBean("051", "LiquidateNow")};

	public static final Map<String, String> getSpecialChar() {
		Map<String, String> fieldNumber = new HashMap<String, String>();
		fieldNumber.put("tab", "\t");
		fieldNumber.put("newLine", "\n");
		fieldNumber.put("premierDelimiter", "&||&");
		return fieldNumber;
	};
	
	public static Date getTimeZone(Date date, String fromTimeZone, String toTimeZone) {
		TimeZone fromTimezone =TimeZone.getTimeZone(fromTimeZone);//get Timezone object
	    TimeZone toTimezone=TimeZone.getTimeZone(toTimeZone);
	    
	    long fromOffset = fromTimezone.getOffset(date.getTime());//get offset
	    long toOffset = toTimezone.getOffset(date.getTime());
	    
		
	    //calculate offset difference and calculate the actual time
	    long convertedTime = date.getTime() - (fromOffset - toOffset);
	    Date newDate = new Date(convertedTime);
		return newDate;
	}

	public static final Map<String, Integer> getApparelSize() {
		Map<String, Integer> fieldNumber = new HashMap<String, Integer>();
		fieldNumber.put("YS", 10);
		fieldNumber.put("YM", 20);
		fieldNumber.put("YL", 30);
		fieldNumber.put("XS", 40);
		fieldNumber.put("XSM", 40);
		fieldNumber.put("S", 50);
		fieldNumber.put("Small", 50);
		fieldNumber.put("SM", 50);
		fieldNumber.put("M", 60);
		fieldNumber.put("Medium", 60);
		fieldNumber.put("MD", 60);
		fieldNumber.put("L", 70);
		fieldNumber.put("Large", 70);
		fieldNumber.put("LG", 70);
		fieldNumber.put("LT", 70);
		fieldNumber.put("L T", 70);
		fieldNumber.put("XL", 80);
		fieldNumber.put("X-Large", 80);
		fieldNumber.put("XT", 80);
		fieldNumber.put("XLG", 80);
		fieldNumber.put("XLT", 80);
		fieldNumber.put("XL T", 80);
		fieldNumber.put("2XL", 90);
		fieldNumber.put("XX-Large", 90);
		fieldNumber.put("2XLG", 90);
		fieldNumber.put("2XLT", 90);
		fieldNumber.put("2XL T", 90);
		fieldNumber.put("XXLG", 90);
		fieldNumber.put("3XL", 100);
		fieldNumber.put("XXX-Large", 100);
		fieldNumber.put("3XLG", 100);
		fieldNumber.put("3XLT", 100);
		fieldNumber.put("3XL T", 100);
		fieldNumber.put("XXXLG", 100);
		fieldNumber.put("4XL", 110);
		fieldNumber.put("XXXX-Large", 110);
		fieldNumber.put("4XLG", 110);
		fieldNumber.put("XXXXLG", 110);
		fieldNumber.put("4XLT", 110);
		fieldNumber.put("4XL T", 110);
		fieldNumber.put("5XL", 120);
		fieldNumber.put("XXXXX-Large", 120);
		fieldNumber.put("5XLG", 120);
		fieldNumber.put("XXXXXLG", 120);
		fieldNumber.put("5XLT", 120);
		fieldNumber.put("5XL T", 120);
		fieldNumber.put("6XL", 130);
		fieldNumber.put("6XLT", 130);
		fieldNumber.put("6XL T", 130);
		fieldNumber.put("LGT", 140);
		fieldNumber.put("LG T", 140);
		fieldNumber.put("XLGT", 150);
		fieldNumber.put("XLG T", 150);
		fieldNumber.put("2XLGT", 160);
		fieldNumber.put("2XLG T", 160);
		return fieldNumber;
	};

	public static final String[] getDays() {
		String[] days = new String[31];
		for (int i = 0; i < days.length; i++)
			days[i] = "" + (i + 1);

		return days;
	}

	public static final List<String> getYears(int numYears) {
		List<String> years = new ArrayList<String>();
		for (Integer y = Calendar.getInstance().get(Calendar.YEAR); y < Calendar.getInstance().get(Calendar.YEAR) + numYears; y++) {
			years.add(y.toString().substring(2, 4));
		}
		return years;
	}

	public static final String[] getYears(int numYearsBefore, int numYearsAfter) {
		String[] years = new String[numYearsBefore + numYearsAfter];
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);

		int past = year - numYearsBefore;
		int future = year + numYearsAfter;
		int i = 0;
		while (past < future) {
			years[i++] = "" + past++;
		}
		return years;
	}

	public static final String[] getHours() {
		String[] hours = new String[12];
		for (int i = 0; i < hours.length; i++)
			hours[i] = "" + (i + 1);

		return hours;
	}

	public static final String[] getMins() {
		String[] mins = new String[4];
		for (int i = 0; i < mins.length; i = i + 1)
			mins[i] = "" + (i * 15);

		return mins;
	}

	public static boolean isInEffect(Date startDate, Date endDate) {
		// today's date
		com.webjaguar.model.DateBean today = new com.webjaguar.model.DateBean();

		int start = startDate.compareTo(today.getTime());
		int end = endDate.compareTo(today.getTime());
		if (start == 0)
			return true;
		else if (start > 0 || end < 0)
			return false;
		else
			return true;
	}

	public static String getQuarter(Calendar date) {
		int month = date.get(Calendar.MONTH);
		return (month >= Calendar.JANUARY && month <= Calendar.MARCH) ? "Q1" : (month >= Calendar.APRIL && month <= Calendar.JUNE) ? "Q2"
				: (month >= Calendar.JULY && month <= Calendar.SEPTEMBER) ? "Q3" : "Q4";
	}

	public static String getMonth(Calendar date) {
		int month = date.get(Calendar.MONTH);
		return (month == Calendar.JANUARY) ? "JAN" : (month == Calendar.FEBRUARY) ? "FEB" : (month == Calendar.MARCH) ? "MAR" : (month == Calendar.APRIL) ? "APR" : (month == Calendar.MAY) ? "MAY"
				: (month == Calendar.JUNE) ? "JUN" : (month == Calendar.JULY) ? "JUL" : (month == Calendar.AUGUST) ? "AUG" : (month == Calendar.SEPTEMBER) ? "SEP"
						: (month == Calendar.OCTOBER) ? "OCT" : (month == Calendar.NOVEMBER) ? "NOV" : "DEC";
	}

	public static String calendatToString(Calendar date) {
		if (date == null)
			return "";
		int day = date.get(Calendar.DAY_OF_MONTH);
		int month = date.get(Calendar.MONTH) + 1;
		int year = date.get(Calendar.YEAR);
		int hour = date.get(Calendar.HOUR);
		int min = date.get(Calendar.MINUTE);
		int sec = date.get(Calendar.SECOND);

		return "'day': " + day + ",'month': " + month + ",'year': " + year + ",'hour': " + hour + ",'min': " + min + ",'sec': " + sec;
	}

	public static final String EMAIL_REGEXP = "/^[a-z0-9]+([_\\.-][a-z0-9]*)*@([a-z0-9]+([\\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i";

	public static boolean validateEmail(String email, String emailRegExp) {
		Perl5Util perl5Util = new Perl5Util();
		String regExp = EMAIL_REGEXP;
		if (emailRegExp != null && !emailRegExp.isEmpty()) {
			regExp = emailRegExp;
		}
		return perl5Util.match(regExp, email);
	}

	public static final String URL_REGEXP = "^(((https?|ftp|file)://)|www).+$";

	public static boolean validateUrl(String Url) {
		return Url.matches(URL_REGEXP);
	}

	public static boolean isDigit(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	public static String showBreadCrumb(List<Category> breadCrumb) {
		StringBuffer sb = new StringBuffer();
		for (Category category : breadCrumb) {
			sb.append(category.getName() + " ");
		}
		return sb.toString();
	}

	public static final Map<String, Integer> getMyTradeZoneFields() {
		Map<String, Integer> fieldsMap = new HashMap<String, Integer>();
		fieldsMap.put("Size", 1);
		fieldsMap.put("Color", 2);
		fieldsMap.put("Condition", 3);
		fieldsMap.put("Type", 4);
		fieldsMap.put("Shape", 5);
		fieldsMap.put("Style", 6);
		fieldsMap.put("Texture", 7);
		fieldsMap.put("Material", 8);
		fieldsMap.put("Flavor", 9);
		fieldsMap.put("Brand Name", 10);
		fieldsMap.put("Manufacturer Part Number (MPN)", 11);
		fieldsMap.put("Made In/Place Of Origin", 12);
		fieldsMap.put("UPC Code", 13);
		fieldsMap.put("FOB", 14);
		fieldsMap.put("Availability", 15);
		fieldsMap.put("Gender", 16);
		fieldsMap.put("Age Group", 17);
		fieldsMap.put("Platform", 18);
		fieldsMap.put("Season", 19);
		fieldsMap.put("Eco-Friendly", 20);
		fieldsMap.put("Author", 21);
		fieldsMap.put("Publisher", 22);
		fieldsMap.put("Year", 23);
		fieldsMap.put("Handmade", 24);
		fieldsMap.put("Ingredients", 25);
		fieldsMap.put("Main Category", 26);
		fieldsMap.put("Main Sub-Category", 27);
		fieldsMap.put("Secondary Category", 28);
		fieldsMap.put("Secondary Sub-Category", 29);
		fieldsMap.put("Redirect URL", 30);

		return fieldsMap;
	}

	public static Map<String, String> bankAudiResponseCode() {
		Map<String, String> responseCode = new HashMap<String, String>();

		responseCode.put("0", "Transaction Successful");
		responseCode.put("?", "Transaction status is unknown");
		responseCode.put("1", "Unknown Error");
		responseCode.put("2", "Bank Declined Transaction");
		responseCode.put("3", "No Reply from Bank");
		responseCode.put("4", "Expired Card");
		responseCode.put("5", "Insufficient funds");
		responseCode.put("6", "Error Communicating with Bank");
		responseCode.put("7", "Payment Server System Error");
		responseCode.put("8", "Transaction Type Not Supported");
		responseCode.put("9", "Bank declined transaction (Do not contact Bank)");
		responseCode.put("A", "Transaction Aborted");
		responseCode.put("C", "Transaction Cancelled");
		responseCode.put("D", "Deferred transaction has been received and is awaiting processing");
		responseCode.put("E", "Invalid Credit Card");
		responseCode.put("F", "3D Secure Authentication failed");
		responseCode.put("I", "Card Security Code verification failed");
		responseCode.put("G", "Invalid Merchant");
		responseCode.put("L", "Shopping Transaction Locked (Please try the transaction again later)");
		responseCode.put("N", "Cardholder is not enrolled in Authentication scheme");
		responseCode.put("P", "Transaction has been received by the Payment Adaptor and is being processed");
		responseCode.put("R", "Transaction was not processed - Reached limit of retry attempts allowed");
		responseCode.put("S", "Duplicate SessionID (OrderInfo)");
		responseCode.put("T", "Address Verification Failed");
		responseCode.put("U", "Card Security Code Failed");
		responseCode.put("V", "Address Verification and Card Security Code Failed");
		responseCode.put("X", "Credit Card Blocked");
		responseCode.put("Y", "Invalid URL");
		responseCode.put("B", "Transaction was not completed");
		responseCode.put("M", "Please enter all required fields");
		responseCode.put("J", "Transaction already in use");
		responseCode.put("BL", "Card Bin Limit Reached");
		responseCode.put("CL", "Card Limit Reached");
		responseCode.put("LM", "Merchant Amount Limit Reached");
		responseCode.put("Q", "IP Blocked");
		responseCode.put("R", "Transaction was not processed - Reached limit of retry attempts allowed");
		responseCode.put("Z", "Bin Blocked");

		return responseCode;
	}

	public static String md5(String signature, String md5_key) {

		String cipher = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(signature.getBytes());
			BigInteger hash = new BigInteger(1, md.digest((md5_key != null) ? md5_key.getBytes() : "".getBytes()));
			cipher = hash.toString(16);
			cipher = pad(cipher, 32, '0');
		} catch (Exception e) {
		}
		return cipher;
	}

	private static String pad(String s, int length, char pad) {
		StringBuffer buffer = new StringBuffer(s);
		while (buffer.length() < length) {
			buffer.insert(0, pad);
		}
		return buffer.toString();
	}

	// static value. Later we should add them to DB
	public static Set<String> getWorkOrderStatuses() {
		Set<String> statuses = new TreeSet<String>();
		statuses.add("Pending");
		statuses.add("Customer Contacted");
		statuses.add("Customer Unavailable");
		statuses.add("In-Process");
		statuses.add("Dispatched");
		statuses.add("Parts Ordered");
		statuses.add("Awaiting Pickup");
		statuses.add("Awaiting Approval");
		statuses.add("Estimate Requested");
		statuses.add("Out for Delivery");
		statuses.add("Parts on Backorder");
		statuses.add("Received");
		statuses.add("Pre Testing");
		statuses.add("Post Testing");
		statuses.add("Awaiting Support");
		statuses.add("In Diagnostics");
		statuses.add("Canceled");
		statuses.add("Delivered");
		statuses.add("To be Reuse");
		statuses.add("Completed - Pending Invoicing");
		statuses.add("Declined Repair");
		statuses.add("Technician Onsite");
		return statuses;
	}

	// static value. Later we should add them to DB
	public static Set<String> getWorkOrderServiceTypes() {
		Set<String> serviceTypes = new TreeSet<String>();
		serviceTypes.add("Total Care");
		serviceTypes.add("Onsite");
		serviceTypes.add("Onsite Warranty");
		serviceTypes.add("In-house");
		serviceTypes.add("In-house Warranty");
		serviceTypes.add("Managed Care");
		serviceTypes.add("Annual Maintenance Contract");
		serviceTypes.add("Rework");
		serviceTypes.add("CPP Billing");
		serviceTypes.add("In-house Received");
		serviceTypes.add("Printer Shipping");
		return serviceTypes;
	}

	public static final String nextCustomerAccountName = "customer_account_number";

}