package com.webjaguar.web.domain;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.web.util.WebUtils;

import com.webjaguar.model.Product;

public class Utilities {
	
	public static String escapeJS(String value) {
        return StringEscapeUtils.escapeJavaScript(value);
    }

	public static boolean contains(ArrayList<Integer> ids, Integer id, String[] idList) {
		boolean contains = false;
		if(ids != null) {
			contains =  ids.contains(id);
		} else if(idList != null) {
			contains = Arrays.asList(idList).contains(id.toString());
		}
		return contains;
	}
	
	public static String getProductFieldValue(Product product, String field) throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		try {
			m = c.getMethod("getField" + field);
		} catch (Exception e) {
			m = c.getMethod("getField1");
		}
		return (String) m.invoke(product, arglist);
	}

	public static void addCookie(HttpServletResponse response, String name, String value, int maxAge, String path) {
		Cookie cookie = new Cookie(name, value);
		cookie.setMaxAge(maxAge);
		cookie.setPath(path);
		response.addCookie(cookie);
	}

	public static void removeCookie(HttpServletResponse response, String name, String value) {
		addCookie(response, name, value, 0, null);
	}

	public static String getCookieValue(HttpServletRequest request, String name) {
		Cookie cookie = WebUtils.getCookie(request, name);
		if (cookie != null) {
			return cookie.getValue();
		}
		return null;
	}

	public static float Round(float numberl, int decimal) {
		float p = (float) Math.pow(10, decimal);
		numberl = numberl * p;
		float tmp = Math.round(numberl);
		return (float) tmp / p;
	}

	public static Double roundFactory(Double input, int decimalPlace, int roundingMode) {
		if (input == null)
			return null;
		BigDecimal bd = new BigDecimal(input);
		return bd.setScale(decimalPlace, roundingMode).doubleValue();
	}

	public static double decimalFormat(double value, String format) {
		return Double.parseDouble(new DecimalFormat(format == null || format.isEmpty() ? "#0.00" : format).format(value));
	}

	// on jsps we need a string format for all payment gateways.
	public static String decimalFormatString(double value, String format) {
		NumberFormat nf = new DecimalFormat("#0.00");
		if (format != null && !format.isEmpty()) {
			nf = new DecimalFormat(format);
		}
		return nf.format(value);
	}

	public static String urlEncode(String value) throws UnsupportedEncodingException {
		return URLEncoder.encode(value, "UTF-8");
	}

	public static String asiBigImageName(String imageName) {
		if (imageName != null && !imageName.isEmpty()) {
			return imageName.replace("prodimgs", "prodbigimgs").replace("&", "%26").trim();
		} else
			return "";

	}

	public static String replaceUrl(String url) {
		if(url != null && !url.isEmpty() && url.contains("pgmastersite")) {
			return url.replace("detailsbig","thumb").replace("&", "%26").trim();
		} 
		return url;
	}
	
	public static String newLineToBreakLine(String text) {
		return text.replace("\n", "<br />");
	}

	// JSTL split function does not work properly for line break ( not \n but
	// actual line break )
	public static String[] split(String text, String splittingChar) {
		return text.split(splittingChar);
	}

	public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		// distance in miles, multiply by 1.609344, if you want to convert to
		// kms
		dist = dist * 60 * 1.1515;
		return dist;
	}

	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	private static double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}

	public static StringBuffer replaceString(StringBuffer source, String target, String replacement, boolean ignoreCase) {
		if (ignoreCase) {
			StringBuffer sb = new StringBuffer();
			Matcher m = Pattern.compile("(?i)" + target).matcher(source);
			while (m.find()) {
				m.appendReplacement(sb, replacement);
			}
			m.appendTail(sb);
			source = sb;
		} else {
			while (source.indexOf(target) != -1) {
				source.replace(source.indexOf(target), source.indexOf(target) + target.length(), replacement);
			}
		}
		return source;
	}

	public static String toString(List<?> l, String sep) {
		StringBuilder sb = new StringBuilder();
		sep = (sep == null ? "" : sep);
		for (Object object : l) {
			sb.append(object.toString()).append(sep);
		}
		return sb.delete(sb.length() - sep.length(), sb.length()).toString();
	}

	public static String removeChar(String targetStr, String chr, boolean fromStart, boolean fromEnd) {
		StringBuilder sb = new StringBuilder(targetStr);

		if (fromStart && targetStr.startsWith(chr)) {
			sb.replace(0, 1, "");
		}
		if (fromEnd && targetStr.endsWith(chr)) {
			sb.replace(sb.length() - (chr.length()), sb.length() - (chr.length() - 1), "");
		}

		return sb.toString().trim();
	}

	public static double applyDiscountCode(double amt, String code) {

		for (KeyBean keyBean : Constants.ASI_DISCOUNT_CODE) {
			if (keyBean.getName().equals(code)) {
				return amt * (1 - Double.parseDouble(keyBean.getValue().toString()));
			}
		}
		// default : return amt
		return amt;
	}

	public static String trimAndSplit(String targetStr, String splitChar, boolean firstHalf, String trimChar, boolean trimFromStart, boolean trimFromEnd) {
		// call removeChar function to trim special char from start and end
		targetStr = removeChar(targetStr, trimChar, trimFromStart, trimFromEnd);

		if (targetStr.contains(splitChar)) {
			// if some special characters does not parse properly, it might be
			// due to regular expression.
			// use \\ to escape special character. For example, pipe character
			// in next line
			String delim = splitChar.equalsIgnoreCase("|") ? "\\|" : splitChar;
			return firstHalf ? targetStr.split(delim)[0] : targetStr.split(delim)[1];
		} else {
			return targetStr;
		}
	}

	public static String limitString(String text, int startIndex, int endIndex, String endWith) {
		if (text == null || text.length() <= endIndex) {
			return text;
		} else {
			if (endWith == null || endWith.isEmpty()) {
				return text.substring(startIndex, endIndex);
			} else {
				return text.substring(startIndex, endIndex) + endWith;
			}
		}
	}

	public static String AESencrypt(String secretKey, String input) {
		try {
			Cipher cipher = Cipher.getInstance("AES");
			SecretKeySpec skeySpec = new SecretKeySpec(Hex.decodeHex(secretKey.toCharArray()), "AES");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
			byte[] encrypted = cipher.doFinal(input.getBytes());
			char[] value = Hex.encodeHex(encrypted);
			return (new String(value));
		} catch (Exception e) {
			return null;
		}
	}

	public static String AESdecrypt(String secretKey, String input) {
		try {
			Cipher cipher = Cipher.getInstance("AES");
			SecretKeySpec skeySpec = new SecretKeySpec(Hex.decodeHex(secretKey.toCharArray()), "AES");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec);
			// char[] valueArray = input.toCharArray();
			byte[] original = cipher.doFinal(Hex.decodeHex(input.toCharArray()));
			return (new String(original));
		} catch (Exception e) {
			return null;
		}
	}

	public static boolean like(String str, String expr) {
		expr = expr.toLowerCase(); // ignoring locale for now
		expr = expr.replace(".", "\\."); // "\\" is escaped to "\"
		expr = expr.replace("?", ".");
		str = str.toLowerCase();
		return str.matches(".*" + expr + ".*");
	}

	public static String sha256(String signature) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(signature.getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if (hex.length() == 1)
					hexString.append('0');
				hexString.append(hex);
			}

			return hexString.toString();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public static Map<String, String> getQueryMap(String query) {
		String[] params = query.split("&");
		Map<String, String> map = new HashMap<String, String>();
		for (String param : params) {
			String name = param.split("=")[0];
			String value = param.split("=")[1];
			map.put(name, value);
		}
		return map;
	}

	public static boolean containsSpace(String s) {
		if (s.contains(" ")) {
			return true;
		}
		return false;
	}
	
	public static double getOrderBalance (double amount, double ccFee){
		if(ccFee == 0){
			return amount;
		}else{
			return amount + (amount*ccFee)/100;
		}
	}
	
	public static double stringToDouble(String str){
		if(str != null && !str.equals("")){
			try{
				return Double.valueOf(str);
			}catch(Exception e){
			}
		}
		return 0;
	}
	
	public static long intToLong(Integer i){
		if(i != null){
			try{
				return Long.valueOf(i);
			}catch(Exception e){
			}
		}
		return -1L;
	}
	
	public static boolean containsDash(String str){
		return str.contains("-") ;
	}
}