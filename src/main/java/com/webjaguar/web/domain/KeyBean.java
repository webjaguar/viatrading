package com.webjaguar.web.domain;

public class KeyBean {
	
	private Object name;
	private Object value;
	
	public Object getName() {
		return name;
	}
	public void setName(Object name) {
		this.name = name;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public KeyBean(Object name, Object value) {
		super();
		this.name = name;
		this.value = value;
	}
	public KeyBean() {
		super();
	}
	
	

}
