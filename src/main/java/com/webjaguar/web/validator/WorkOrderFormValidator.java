/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 01.17.2008
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.webjaguar.model.WorkOrder;

public class WorkOrderFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(WorkOrder.class);
	}
	
	public void validate(Object o, Errors errors) {
		WorkOrder workOrder = (WorkOrder) o;
		if (workOrder.getStartHour().length() > 0 ^ workOrder.getStartMin().length() > 0) {
			errors.rejectValue("startTime", "INVALID_TIME");
		}
		if (workOrder.getStopHour().length() > 0 ^ workOrder.getStopMin().length() > 0) {
			errors.rejectValue("stopTime", "INVALID_TIME");
		}
		if (workOrder.getInstallPercentage() != null && 
				(workOrder.getInstallPercentage() > 100 || workOrder.getInstallPercentage() < 0)) {
			errors.rejectValue("installPercentage", "typeMismatch.workOrder.installPercentage");			
		}
	}

}
