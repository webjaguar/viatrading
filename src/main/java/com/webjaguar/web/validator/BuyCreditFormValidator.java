/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.web.validator;

import java.util.Calendar;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.BuyCreditForm;

public class BuyCreditFormValidator implements Validator
{
	public boolean supports(Class aClass)
	{
		return aClass.equals( BuyCreditForm.class );
	}

	public void validate(Object obj, Errors errors)
	{
		validateEmail( "billing.email", ((BuyCreditForm) obj).getBilling().getEmail(), errors );
		validateBillingAddress( obj, errors );
	}

	public void validateEmail(String fieldName, String email, Errors errors)
	{
		if (!Constants.validateEmail(email, null)){
			errors.rejectValue( fieldName, "form.invalidEmail", "invalid email" );
		}
	}

	public void validateBillingAddress(Object obj, Errors errors)
	{
		BuyCreditForm form = (BuyCreditForm) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "billing.firstName", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "billing.lastName", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "billing.addr1", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "billing.city", "form.required", "required" );
		if (form.getBilling().isStateProvinceNA()) {
			form.getBilling().setStateProvince("");
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace( errors, "billing.stateProvince", "form.required", "required" );			
		}
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "billing.zip", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "billing.country", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "billing.phone", "form.required", "required");
	}

	public void validateCC(BuyCreditForm form, Errors errors)
	{
		if ( !isValidCC( form.getCreditCard().getNumber() ) )
		{
			errors.rejectValue( "creditCard.number", "order.exception.creditcard.number" );
		}
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "creditCard.cardCode", "order.exception.creditcard.cardcode" );
				
		// check expiration date
		Calendar rightNow = Calendar.getInstance();
		int expireYear = 2000 + Integer.parseInt(form.getCreditCard().getExpireYear());
		boolean expired = true;
		if (expireYear > rightNow.get(Calendar.YEAR)) {
			expired = false;
		} else if (expireYear == rightNow.get(Calendar.YEAR)) {
			int month = Integer.parseInt(form.getCreditCard().getExpireMonth());
			if (month >= rightNow.get(Calendar.MONTH) + 1) {
				expired = false;
			}
		}
		if (expired) {
			errors.rejectValue( "creditCard.expireYear", "order.exception.creditcard.expired" );			
		}
	}

	private boolean isValidCC(String cardNumber)
	{
		String digitsOnly = cardNumber;
		int sum = 0;
		int digit = 0;
		int addend = 0;
		boolean timesTwo = false;
		if ( digitsOnly.length() == 0 )
		{ // if no digits at all
			return false; // invalid number
		}
		else
		{
			for ( int i = digitsOnly.length() - 1; i >= 0; i-- )
			{
				// Parse each digit character to integer
				digit = Integer.parseInt( digitsOnly.substring( i, i + 1 ) );
				// multiply by 2
				addend = digit * 2;
				if ( timesTwo )
				{
					if ( addend > 9 )
					{
						addend -= 9;
					}
				}
				else
				{
					addend = digit;
				}
				sum += addend;
				timesTwo = !timesTwo;
			}
			int modulus = sum % 10;
			if ( modulus == 0 )
				return true; // if fits Luhn algorithm
			else
				return false; // otherwise
		}
	}
}
