package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.model.UpsOrder;


public class TruckValidator implements Validator {

	public boolean supports(Class aClass) {
		return aClass.equals(UpsOrder.class);
	}
	
	public void validate(Object obj, Errors errors) {
	    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customerPO", "form.required");
	    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "trackingNumber", "form.required");
	}
}