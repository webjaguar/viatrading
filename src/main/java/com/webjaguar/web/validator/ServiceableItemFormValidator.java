/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.25.2007
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.form.ServiceableItemForm;

public class ServiceableItemFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(ServiceableItemForm.class);
	}
	
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "item.itemId", "form.required");		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "item.serialNum", "form.required");		
	}

}
