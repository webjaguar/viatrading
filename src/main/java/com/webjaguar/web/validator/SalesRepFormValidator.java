/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.19.2007
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.SalesRepForm;

public class SalesRepFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(SalesRepForm.class);
	}
	
	public void validate(Object obj, Errors errors) {
		SalesRepForm salesRepForm = (SalesRepForm) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "salesRep.name", "form.required", "required");
		validateEmail("salesRep.email", salesRepForm.getSalesRep().getEmail(), errors);
		
		// password
		if (salesRepForm.getSalesRep().getPassword() != null && salesRepForm.getSalesRep().getPassword().length() > 0 && salesRepForm.getSalesRep().getPassword().length() < 5) {
			Object[] errorArgs = { new Integer(5) };
			errors.rejectValue("salesRep.password", "form.charLessMin", errorArgs, "should be at least 5 chars");
		}	
	}
    
    private void validateEmail(String fieldName, String email, Errors errors) {
		if (!Constants.validateEmail(email, null)) {
			errors.rejectValue(fieldName, "form.invalidEmail", "invalid email");
		}
    }
}
