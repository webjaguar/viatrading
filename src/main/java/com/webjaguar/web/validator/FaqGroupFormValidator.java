package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.form.FaqGroupForm;

public class FaqGroupFormValidator implements Validator {
	
	public boolean supports(Class aClass) {
		return aClass.equals(FaqGroupForm.class);
	}
	
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "faqGroup.name", "form.required", "required");
	}

}
