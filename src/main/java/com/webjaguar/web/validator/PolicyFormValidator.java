package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.form.PolicyForm;

public class PolicyFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(PolicyForm.class);
	}
	
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "policy.name", "form.required", "required");
	}

}
