/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.form.FaqForm;

public class FaqFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(FaqForm.class);
	}
	
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "faq.question", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "faq.answer", "form.required", "required");
	}

}
