/*
 * Copyright 2005, 2007 Advanced E-Media Solutions 
 * @author: Shahin Naji
 * 
 */

package com.webjaguar.web.validator;

import org.springframework.validation.ValidationUtils;

public class PromoFormValidator implements org.springframework.validation.Validator
{
	public boolean supports(Class aClass)
	{
		return aClass.equals( com.webjaguar.web.form.PromoForm.class );
	}

	public void validate(Object command, org.springframework.validation.Errors errors)
	{
		com.webjaguar.web.form.PromoForm promoForm = (com.webjaguar.web.form.PromoForm) command;

		// check title
		org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "promo.title", "form.required", "required" );
		if ( promoForm.getPromo().getTitle().length() > 100 )
		{
			Object[] errorArgs = { new Integer( 100 ) };
			errors.rejectValue( "promo.title", "form.charOverMax", errorArgs, "should be at most 100 chars" );
		}
		promoForm.getPromo().setTitle( promoForm.getPromo().getTitle().trim() );
		
		// check discount
		org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "promo.discount", "form.required", "required" );
		if ( promoForm.getPromo().getDiscount() != null )
		{
			if ( promoForm.getPromo().getDiscount().doubleValue() < 0 )
			{
				errors.rejectValue( "promo.discount", "form.negAmount", "should be a positive amount" );
			}
			else if ( promoForm.getPromo().isPercent() )
			{
				if ( promoForm.getPromo().getDiscount().doubleValue() > 100 )
				{
					promoForm.getPromo().setDiscount( 100.00 );
				}

			}
		}

		// check min Order
		org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "promo.minOrder", "form.required", "required" );
		if ( promoForm.getPromo().getMinOrder() != null )
		{
			if ( promoForm.getPromo().getMinOrder().doubleValue() < 0 )
			{
				errors.rejectValue( "promo.minOrder", "form.negAmount", "should be a positive amount" );

			}
		}
		if ( promoForm.getPromo().getMinOrderPerBrand() != null )
		{	
			String[] brands = promoForm.getPromo().getBrands().split(",");
			if(brands.length > 1) {
				errors.rejectValue( "promo.brands", "form.multipleBrandsNotAllowed", "Multiple Brands are not allowed if minimum order per brand is available" );
			}
			
			if ( promoForm.getPromo().getMinOrderPerBrand().doubleValue() < 0 )
			{
				errors.rejectValue( "promo.minOrderPerBrand", "form.negAmount", "should be a positive amount" );
			}
			if ( promoForm.getPromo().getMinOrder() == null || promoForm.getPromo().getMinOrderPerBrand() >  promoForm.getPromo().getMinOrder())
			{
				errors.rejectValue( "promo.minOrder", "form.lessAmount", "should be a equal or greater than brand minimum amount" );
			}
		}
		
		// check products and brands
		if(promoForm.getPromo().getDiscountType().equalsIgnoreCase("product") || promoForm.getPromo().isProductAvailability()) {
			if(promoForm.getPromo().getParentSkus() == null || promoForm.getPromo().getParentSkus().equals("")) {
				//only validate skus and brands if categoryIds is empty or null
				if(promoForm.getPromo().getCategoryIds() == null || promoForm.getPromo().getCategoryIds().equals("")) {
					if(promoForm.getPromo().getSkus() == null || promoForm.getPromo().getSkus().equals("")) {
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "promo.brands", "form.required", "required");
					}
					if(promoForm.getPromo().getBrands() == null || promoForm.getPromo().getBrands().equals("")) {
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "promo.skus", "form.required", "required");
					}
				} else {
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "promo.categoryIds", "form.required", "required");
				}
			}
		}
	}
}
