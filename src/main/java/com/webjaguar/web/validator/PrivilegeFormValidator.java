/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.form.AccessPrivilegeForm;

public class PrivilegeFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(AccessPrivilegeForm.class);
	}
	
	public void validate(Object obj, Errors errors) {
		AccessPrivilegeForm privilegeForm = (AccessPrivilegeForm) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "privilege.user.username", "form.required", "required");

		if ( privilegeForm.getPrivilege().getUser().getPassword().length() > 0 || privilegeForm.isNewPrivilege() ) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "privilege.user.password", "form.required", "required");
			validatePassword((AccessPrivilegeForm) obj, errors);			
		}
	}
	
	public void validatePassword(AccessPrivilegeForm privilegeForm, Errors errors) {
		// password
		if (privilegeForm.getPrivilege().getUser().getPassword().length() < 5) 
		{
			Object[] errorArgs = { new Integer(5) };
			errors.rejectValue("privilege.user.password", "form.charLessMin", errorArgs, "should be at least 5 chars");
		} else if (!privilegeForm.getPrivilege().getUser().getPassword().equals(privilegeForm.getConfirmPassword())) {
			errors.rejectValue("confirmPassword", "form.passwordMismatch", "matching passwords required");
		}

	}
}
