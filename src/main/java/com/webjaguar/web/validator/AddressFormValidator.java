/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.form.AddressForm;

public class AddressFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(AddressForm.class);
	}
	
	public void validate(Object o, Errors errors) {
		AddressForm addressForm = (AddressForm) o;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.firstName", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.lastName", "form.required", "required");			
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.addr1", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.city", "form.required", "required");
		if (addressForm.getAddress().isStateProvinceNA()) {
			addressForm.getAddress().setStateProvince("");
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.stateProvince", "form.required", "required");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.zip", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.country", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.phone", "form.required", "required");
		if (addressForm.isCompanyRequired()) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address.company", "form.required", "required");				
		}
	}

}
