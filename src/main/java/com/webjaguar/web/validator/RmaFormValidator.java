/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.16.2008
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.model.Rma;

public class RmaFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(Rma.class);
	}
	
	public void validate(Object o, Errors errors) {
		Rma rma = (Rma) o;
		if (rma.getStatus().equalsIgnoreCase("completed")) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "completeDate", "REQUIRED_IF_COMPLETED");			
		} else {
			if (rma.getCompleteDate() != null) {
				errors.rejectValue("completeDate", "REQUIRED_IF_COMPLETED");
			}
		}
	}

}
