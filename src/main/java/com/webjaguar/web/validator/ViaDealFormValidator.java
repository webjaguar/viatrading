/*
 * Copyright 2005, 2009 Advanced E-Media Solutions 
 * @author: Jwalant Patel
 * 
 */

package com.webjaguar.web.validator;

import com.webjaguar.web.form.ViaDealForm;

public class ViaDealFormValidator implements org.springframework.validation.Validator
{
	public boolean supports(Class aClass)
	{
		return aClass.equals( ViaDealForm.class );
	}

	public void validate(Object command, org.springframework.validation.Errors errors)
	{
		ViaDealForm ViaDealForm = (ViaDealForm) command;

		// check title
		org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "viaDeal.title", "form.required", "required" );
		if ( ViaDealForm.getViaDeal().getTitle().length() > 100 )
		{
			Object[] errorArgs = { new Integer( 100 ) };
			errors.rejectValue( "viaDeal.title", "form.charOverMax", errorArgs, "should be at most 100 chars" );
		}
		ViaDealForm.getViaDeal().setTitle( ViaDealForm.getViaDeal().getTitle().trim() );
		
		// check quantity
//		if( ViaDealForm.getViaDeal().getBuySku() != null && !ViaDealForm.getViaDeal().getBuySku().equals("") && ViaDealForm.getViaDeal().getBuyQuantity() == 0  ) {
//			errors.rejectValue( "viaDeal.buyQuantity", "typeMismatch.deal.quantity", "select quantity");
//		}
		
		// check Parent Sku
		org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "viaDeal.parentSku", "form.required", "required" );
//		if( (ViaDealForm.getViaDeal().getParentSku() == null || ViaDealForm.getViaDeal().getParentSku().equals("")) && (ViaDealForm.getViaDeal().getBuyAmount() == null || ViaDealForm.getViaDeal().getBuyAmount() == 0.0) ) {
//			org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "viaDeal.buySku", "form.required", "required" );	
//		}
		
		// check discount
		org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "viaDeal.discount", "form.required", "required" );
		if ( ViaDealForm.getViaDeal().getDiscount() != null )
		{
			if ( ViaDealForm.getViaDeal().getDiscount().doubleValue() < 0 )
			{
				errors.rejectValue( "viaDeal.discount", "form.negAmount", "should be a positive amount" );
			}
			else if ( ViaDealForm.getViaDeal().isDiscountType() )
			{
				if ( ViaDealForm.getViaDeal().getDiscount().doubleValue() > 100 )
				{
					ViaDealForm.getViaDeal().setDiscount( 100.00 );
				}

			}
		}
	}
}