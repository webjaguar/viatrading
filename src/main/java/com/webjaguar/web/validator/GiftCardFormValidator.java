/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.model.GiftCardOrder;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.GiftCardForm;

public class GiftCardFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(GiftCardForm.class);
	}
	
	public void validate(Object obj, Errors errors) {
		GiftCardForm giftCardForm = (GiftCardForm) obj;
		validateEmail("giftCard.recipientEmail", giftCardForm, errors);	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "giftCard.amount", "giftcard.form.amount", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "giftCardOrder.quantity", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "giftCard.recipientFirstName", "asterisk", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "giftCard.senderFirstName", "asterisk", "required");
    }
	
	public void validatePaymentMethod(GiftCardOrder giftCardOrder, Errors errors)
	{
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "giftCardOrder.paymentMethod", "payment.required" );
	}
	
	public void validateCC(GiftCardForm giftCardForm, Errors errors)
	{
		if ( !isValidCC( giftCardForm.getGiftCardOrder().getCreditCard().getNumber() ) )
		{
			errors.rejectValue( "giftCardOrder.creditCard.number", "giftCard.exception.creditcard.number" );
		}
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "giftCardOrder.creditCard.cardCode", "giftCard.exception.creditcard.cardcode" );
	}

	private boolean isValidCC(String cardNumber)
	{
		String digitsOnly = cardNumber;
		int sum = 0;
		int digit = 0;
		int addend = 0;
		boolean timesTwo = false;
		if ( digitsOnly.length() == 0 )
		{ // if no digits at all
			return false; // invalid number
		}
		else
		{
			for ( int i = digitsOnly.length() - 1; i >= 0; i-- )
			{
				// Parse each digit character to integer
				digit = Integer.parseInt( digitsOnly.substring( i, i + 1 ) );
				// multiply by 2
				addend = digit * 2;
				if ( timesTwo )
				{
					if ( addend > 9 )
					{
						addend -= 9;
					}
				}
				else
				{
					addend = digit;
				}
				sum += addend;
				timesTwo = !timesTwo;
			}
			int modulus = sum % 10;
			if ( modulus == 0 )
				return true; // if fits Luhn algorithm
			else
				return false; // otherwise
		}
	}
    
    private void validateEmail(String fieldName, GiftCardForm giftCardForm, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, fieldName, "form.required", "required");    	
		if( giftCardForm.getGiftCard().getRecipientEmail() != null && !giftCardForm.getGiftCard().getRecipientEmail().equals( "" )) {
			if (!Constants.validateEmail(giftCardForm.getGiftCard().getRecipientEmail(), null)) {
				errors.rejectValue(fieldName, "giftCard.recipientEmail", "invalid email");
			}
			if (!giftCardForm.getGiftCard().getRecipientEmail().equals(giftCardForm.getConfirmEmail())) {
				errors.rejectValue("confirmEmail", "giftCard.emailMismatch", "matching email required");
			}
		}
    }
}