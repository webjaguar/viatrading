/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 04.01.2008
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.webjaguar.model.Subscription;

public class SubscriptionFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(Subscription.class);
	}
	
	public void validate(Object o, Errors errors) {
		Subscription subscription = (Subscription) o;

		// check unit price
		if (subscription.getUnitPrice() == null || subscription.getUnitPrice() <= 0) {
			errors.rejectValue("unitPrice", "typeMismatch.subscription.unitPrice");
		}
		
		// check quantity
		if (subscription.getQuantity() < 1) {
			errors.rejectValue("quantity", "typeMismatch.subscription.quantity");
		}
	}

}
