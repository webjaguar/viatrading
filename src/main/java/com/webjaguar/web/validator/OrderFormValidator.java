/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.web.validator;

import java.util.Calendar;
import java.util.List;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.model.PaymentMethod;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.OrderForm;

public class OrderFormValidator implements Validator {
	
	public boolean supports(Class aClass) {
		return aClass.equals( OrderForm.class );
	}

	public void validate(Object obj, Errors errors) {
		validateEmail( "username", ((OrderForm) obj).getUsername(), errors );
		validateShippingAddress( obj, errors );
		validateBillingAddress( obj, errors );
	}

	public void validateEmail(String fieldName, String email, Errors errors) {
		if (!Constants.validateEmail(email, null)) {
			errors.rejectValue( fieldName, "form.invalidEmail", "invalid email" );
		}
	}

	public void validateShippingAddress(Object obj, Errors errors) {
		OrderForm orderForm = (OrderForm) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.shipping.firstName", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.shipping.lastName", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.shipping.addr1", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.shipping.city", "form.required", "required" );
		if (orderForm.getOrder().getShipping().isStateProvinceNA()) {
			orderForm.getOrder().getShipping().setStateProvince("");
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.shipping.stateProvince", "form.required", "required" );			
		}
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.shipping.zip", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.shipping.country", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "order.shipping.phone", "form.required", "required");
		if (orderForm.isCompanyRequired()) {
			ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.shipping.company", "form.required", "required" );			
		}
	}

	public void validateBillingAddress(Object obj, Errors errors) {
		OrderForm orderForm = (OrderForm) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.billing.firstName", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.billing.lastName", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.billing.addr1", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.billing.city", "form.required", "required" );
		if (orderForm.getOrder().getBilling().isStateProvinceNA()) {
			orderForm.getOrder().getBilling().setStateProvince("");
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.billing.stateProvince", "form.required", "required" );			
		}
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.billing.zip", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.billing.country", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "order.billing.phone", "form.required", "required");
		if (orderForm.isCompanyRequired()) {
			ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.billing.company", "form.required", "required" );			
		}
	}

	public void validateShippingSelection(Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "shippingRateIndex", "shipping.required" );
	}

	public void validatePaymentMethod(List<PaymentMethod> paymentMethodList, Object obj, Errors errors) { 
		OrderForm orderForm = (OrderForm) obj;
		
		if(orderForm.getOrder().getGrandTotal() > 0){
			ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.paymentMethod", "payment.required" );
		} else if(orderForm.getOrder().getPaymentMethod() == null || orderForm.getOrder().getPaymentMethod() == "") {
			boolean cusotmPayments = false;
			for(PaymentMethod paymentMethod: paymentMethodList) {
				if(paymentMethod.isEnabled() && ! paymentMethod.isInternal()) {
					cusotmPayments = true;
				}
			}
			if(cusotmPayments && !orderForm.getOrder().isPromoAvailable()	) {
				ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.paymentMethod", "payment.required" );
			} else{
				orderForm.getOrder().setPaymentMethod("NONE");
			}
		}
	}

	public void validatePurchaseOrder(Object obj, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.purchaseOrder", "purchaseOrder.required" );    								
	}
	public void validateCC(OrderForm orderForm, Errors errors, boolean temCreditCard) {
		if (!isDigitsOnly(orderForm.getOrder().getCreditCard().getNumber()) || !isValidCC(orderForm.getOrder().getCreditCard().getNumber())) {
			if(temCreditCard) {
				errors.rejectValue( "tempCreditCard.number", "order.exception.creditcard.number" );
			} else {
				errors.rejectValue( "order.creditCard.number", "order.exception.creditcard.number" );
			}
		} else {
			// check type
			boolean validType = false;
			int cardLength = orderForm.getOrder().getCreditCard().getNumber().length();
			if (orderForm.getOrder().getCreditCard().getType() == null) {
				if(temCreditCard) {
					errors.rejectValue("tempCreditCard.type", "order.exception.creditcard.type");
				} else {
					errors.rejectValue("order.creditCard.type", "order.exception.creditcard.type");
				}
			} else if (orderForm.getOrder().getCreditCard().getType().equalsIgnoreCase("VISA")) {
				int prefix = Integer.parseInt(orderForm.getOrder().getCreditCard().getNumber().substring(0, 1));
				if (prefix == 4 && (cardLength == 13 || cardLength == 16)) {
					validType = true;
				}
			} else if (orderForm.getOrder().getCreditCard().getType().equalsIgnoreCase("AMEX")) {
				int prefix = Integer.parseInt(orderForm.getOrder().getCreditCard().getNumber().substring(0, 2));			
				if ((prefix == 34 || prefix == 37) && cardLength == 15) {
					validType = true;
				}
			} else if (orderForm.getOrder().getCreditCard().getType().equalsIgnoreCase("MC")) {
				int prefix = Integer.parseInt(orderForm.getOrder().getCreditCard().getNumber().substring(0, 2));			
				if ((prefix >= 51 || prefix <= 55) && cardLength == 16) {
					validType = true;
				}
			} else if (orderForm.getOrder().getCreditCard().getType().equalsIgnoreCase("DISC")) {
				int prefix = Integer.parseInt(orderForm.getOrder().getCreditCard().getNumber().substring(0, 4));		
				if (prefix == 6011 && cardLength == 16) {
					validType = true;
				}
			} else if (orderForm.getOrder().getCreditCard().getType().equalsIgnoreCase("JCB")) {
				int prefix = Integer.parseInt(orderForm.getOrder().getCreditCard().getNumber().substring(0, 4));		
				if ((prefix == 1800 || prefix == 2131) && cardLength == 15) {
					validType = true;
				} else if (cardLength == 16) {
					prefix = Integer.parseInt(orderForm.getOrder().getCreditCard().getNumber().substring(0, 1));
					if (prefix == 3) {
						validType = true;
					}
				}
			}
			if (!validType) {
				if(temCreditCard) {
					errors.rejectValue("tempCreditCard.type", "order.exception.creditcard.type");
				} else {
					errors.rejectValue("order.creditCard.type", "order.exception.creditcard.type");
				}
			}
		}
		if(temCreditCard) {
			ValidationUtils.rejectIfEmptyOrWhitespace( errors, "tempCreditCard.cardCode", "order.exception.creditcard.cardcode" );
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace( errors, "order.creditCard.cardCode", "order.exception.creditcard.cardcode" );
		}
		// check expiration date
		Calendar rightNow = Calendar.getInstance();
		int expireYear = 2000 + Integer.parseInt(orderForm.getOrder().getCreditCard().getExpireYear());
		boolean expired = true;
		if (expireYear > rightNow.get(Calendar.YEAR)) {
			expired = false;
		} else if (expireYear == rightNow.get(Calendar.YEAR)) {
			int month = Integer.parseInt(orderForm.getOrder().getCreditCard().getExpireMonth());
			if (month >= rightNow.get(Calendar.MONTH) + 1) {
				expired = false;
			}
		}
		if (expired) {
			if(temCreditCard) {
				errors.rejectValue( "tempCreditCard.expireYear", "order.exception.creditcard.expired" );	
			}else {
				errors.rejectValue( "order.creditCard.expireYear", "order.exception.creditcard.expired" );	
			}
		}
	}

	private boolean isValidCC(String cardNumber) {
		String digitsOnly = cardNumber;
		int sum = 0;
		int digit = 0;
		int addend = 0;
		boolean timesTwo = false;
		if ( digitsOnly.length() == 0 ) { // if no digits at all
			return false; // invalid number
		}
		else {
			for ( int i = digitsOnly.length() - 1; i >= 0; i-- ) {
				// Parse each digit character to integer
				digit = Integer.parseInt( digitsOnly.substring( i, i + 1 ) );
				// multiply by 2
				addend = digit * 2;
				if ( timesTwo ) {
					if ( addend > 9 ) {
						addend -= 9;
					}
				}
				else {
					addend = digit;
				}
				sum += addend;
				timesTwo = !timesTwo;
			}
			int modulus = sum % 10;
			if ( modulus == 0 )
				return true; // if fits Luhn algorithm
			else
				return false; // otherwise
		}
	}
	
	private boolean isDigitsOnly(String value) {
		String digits = "0123456789";
		
		StringBuffer sbuff = new StringBuffer(value);
		for (int i=sbuff.length()-1; i>=0; i--) {
			if (digits.indexOf(sbuff.charAt(i)) < 0) {
				return false;
			}
		}
		
		return true;
	}
	
}