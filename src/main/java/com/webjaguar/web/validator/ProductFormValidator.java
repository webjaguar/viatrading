/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 */

package com.webjaguar.web.validator;

import java.lang.reflect.Method;
import java.util.*;

import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.model.Product;
import com.webjaguar.web.form.ProductForm;

public class ProductFormValidator implements Validator
{
	public boolean supports( Class aClass )
	{
		return aClass.equals( ProductForm.class );
	}

	public void validate( Object command, Errors errors )
	{
		ProductForm productForm = (ProductForm) command;
		
		// check category ids
		if ( productForm.getProduct().getCategoryIds() != null )
		{
			String[] categoryIds = productForm.getProduct().getCategoryIds().split( "," );
			for ( int x = 0; x < categoryIds.length; x++ )
			{
				try
				{
					if ( !( "".equals( categoryIds[x].trim() ) ) )
					{
						productForm.getProduct().getCatIds().add( Integer.valueOf( categoryIds[x].trim() ) );
					}
				}
				catch ( NumberFormatException e )
				{
					errors.rejectValue( "product.categoryIds", "form.catIdInvalid", new Object[] { categoryIds[x].trim() }, "contains invalid values" );
				}
			}
		}

		// sku
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "product.sku", "form.required", "required");
		
		// check prefix
		if (productForm.getSkuPrefix() != null && productForm.getSkuPrefix().length() > 0) {
			if (!productForm.getProduct().getSku().startsWith(productForm.getSkuPrefix())) {
				errors.rejectValue("product.sku", "form.skuInvalidPrefix");
			}
		}

		// check name
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "product.name", "form.required", "required" );
		if ( productForm.getProduct().getName() != null && productForm.getProduct().getName().length() > 120 )
		{
			Object[] errorArgs = { new Integer( 120 ) };
			errors.rejectValue( "product.name", "form.charOverMax", errorArgs, "should be at most 120 chars" );
		}

		// Retrieve Prices
		Double prices[] = { null, null, null, null, null, null, null, null, null, null };
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		for ( int i = 0; i < productForm.getPriceTiers(); i++ )
		{
			try
			{
				m = c.getMethod( "getPrice" + ( i + 1 ) );
				prices[i] = (Double) m.invoke( productForm.getProduct(), arglist );
			}
			catch ( Exception e )
			{
				// do nothing
			}
		}
		// Retrieve QtyBreaks
		Integer qtyBreaks[] = { null, null, null, null, null, null, null, null, null };
		for ( int i = 0; i < productForm.getPriceTiers()-1; i++ )
		{
			try
			{
				m = c.getMethod( "getQtyBreak" + ( i + 1 ) );
				qtyBreaks[i] = (Integer) m.invoke( productForm.getProduct(), arglist );
			}
			catch ( Exception e )
			{
				// do nothing
			}
		}

		// check prices / qty breaks
		int price = productForm.getPriceTiers() - 1;
		boolean missingPrice = true;
		for ( ; price >= 0; price-- )
		{
			if ( prices[price] != null )
			{
				for ( int x = price - 1; x >= 0; x-- )
				{
					if ( prices[x] == null )
					{
						errors.rejectValue( "product.price" + ( x + 1 ), "form.missingPrice" );
						missingPrice = false;
					}
				}
				break;
			}
		}
		/* Do not validate lower price as ASI customer may have same price for multiple qty break
		if ( missingPrice )
		{
			for ( int i = 0; i < prices.length - 1; i++ )
			{
				if ( prices[i+1] != null )
				{
					if ( prices[i + 1].doubleValue() >= prices[i].doubleValue() )
					{
						errors.rejectValue( "product.price" + ( i + 2 ), "form.lowPrice" );
						break;
					}
				}
			}
		}*/
		for ( int qtyBreak = price; qtyBreak > 0; qtyBreak-- )
		{
			if ( qtyBreaks[qtyBreak - 1] == null )
			{
				errors.rejectValue( "product.price" + ( qtyBreak + 1 ), "form.missingQtyBreak" );
			}
			else if ( qtyBreaks[qtyBreak - 1] == 1 )
			{
				errors.rejectValue( "product.price" + ( qtyBreak + 1 ), "form.invalidQtyBreak" );
			}
			else if ( qtyBreak > 1 && qtyBreaks[qtyBreak - 2] != null && qtyBreaks[qtyBreak - 1] <= qtyBreaks[qtyBreak - 2] )
			{
				errors.rejectValue( "product.price" + ( qtyBreak + 1 ), "form.invalidQtyBreak" );
			}
		}

		List<Integer> invalidQtyBreaks = new ArrayList<Integer>();
		// FieldError to ObjectError spring3
		Iterator<ObjectError> iter = errors.getAllErrors().iterator();
		while ( iter.hasNext() ) {
			// spring3 cast to FieldError
			FieldError fe = (FieldError) iter.next();
			if ( fe.getField().startsWith( "product.qtyBreak" ) )
			{
				invalidQtyBreaks.add( Integer.parseInt( fe.getField().substring( 16 ) ) + 1 );
			}
		}
		for ( Integer qtyBreak : invalidQtyBreaks )
		{
			errors.rejectValue( "product.price" + qtyBreak, "form.invalidQtyBreak" );
		}
		
		// check subscription discount
		if (productForm.getProduct().getSubscriptionDiscount() != null) {
			if (productForm.getProduct().getSubscriptionDiscount() < 0 ) {
				errors.rejectValue("product.subscriptionDiscount", "form.negAmount", "should be a positive amount");
			} else if (productForm.getProduct().getSubscriptionDiscount() > 100 ) {
				productForm.getProduct().setSubscriptionDiscount(100.00);
			}
		}
	}

}
