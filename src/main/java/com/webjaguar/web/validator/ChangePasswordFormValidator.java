/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.form.ChangePasswordForm;

public class ChangePasswordFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(ChangePasswordForm.class);
	}
	
	public void validate(Object obj, Errors errors) {
		ChangePasswordForm changePasswordForm = (ChangePasswordForm) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "currentPassword", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "user.password", "form.required", "required");
		validatePassword(changePasswordForm, errors);
	}
	
	public void validatePassword(ChangePasswordForm changePasswordForm, Errors errors) {
		// password
		if (changePasswordForm.getUser().getPassword().length() > 0) {
		  if (changePasswordForm.getUser().getPassword().length() < 5) {
			Object[] errorArgs = { new Integer(5) };
			errors.rejectValue("user.password", "form.charLessMin", errorArgs, "should be at least 5 chars");
		  } else if (!changePasswordForm.getUser().getPassword().equals(changePasswordForm.getConfirmPassword())) {
			errors.rejectValue("confirmPassword", "form.passwordMismatch", "matching passwords required");
		  }
		}		
	}

}
