/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.28.2007
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.form.ServiceForm;

public class ServiceFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(ServiceForm.class);
	}
	
	public void validate(Object o, Errors errors) {
		ServiceForm form = (ServiceForm) o;
		if (form.getService().getStatus().equalsIgnoreCase( "completed" )) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "service.completeDate", "REQUIRED_IF_COMPLETED");			
		} else {
			if (form.getService().getCompleteDate() != null) {
				errors.rejectValue("service.completeDate", "REQUIRED_IF_COMPLETED");
			}
		}
	}

}
