/*
 * Copyright 2005, 2007 Advanced E-Media Solutions 
 * @author: Shahin Naji
 * @since 01/26/2007
 * 
 */

package com.webjaguar.web.validator;

public class SalesTagFormValidator implements org.springframework.validation.Validator
{
	public boolean supports(Class aClass)
	{
		return aClass.equals( com.webjaguar.web.form.salesTagForm.class );
	}

	public void validate(Object command, org.springframework.validation.Errors errors)
	{
		com.webjaguar.web.form.salesTagForm salesTagForm = (com.webjaguar.web.form.salesTagForm) command;

		// check title
		org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "salesTag.title", "form.required", "required" );
		if ( salesTagForm.getSalesTag().getTitle().length() > 100 )
		{
			Object[] errorArgs = { new Integer( 100 ) };
			errors.rejectValue( "salesTag.title", "form.charOverMax", errorArgs, "should be at most 100 chars" );
		}

		// check discount
		org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "salesTag.discount", "form.required", "required" );
		if ( salesTagForm.getSalesTag().getDiscount() != null )
		{
			if ( salesTagForm.getSalesTag().getDiscount().doubleValue() < 0 )
			{
				errors.rejectValue( "salesTag.discount", "form.negAmount", "should be a positive amount" );
			}
			else if (salesTagForm.getSalesTag().isPercent() && salesTagForm.getSalesTag().getDiscount().doubleValue() > 100 )
			{
				salesTagForm.getSalesTag().setDiscount( 100.00 );
			}
		}

	}

}
