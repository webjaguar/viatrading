/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.form.CategoryForm;

public class CategoryFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(CategoryForm.class);
	}
	
	public void validate(Object o, Errors errors) {
		CategoryForm form = (CategoryForm) o;
		
		// category name
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "category.name", "form.required", "required");
		
		// html code
		// TEXT is limited to 65,535 characters. MEDIUMTEXT is 16,777,215
		if (form.getCategory().getHtmlCode() != null && form.getCategory().getHtmlCode().length() > 1000000) {
			Object[] errorArgs = { form.getCategory().getHtmlCode().length(), new Integer(1000000) };
			errors.rejectValue("category.htmlCode", "form.charOverLimit", errorArgs, 
					form.getCategory().getHtmlCode().length() + " chars (1,000,000 limit)");
		}
		
		// protected html code
		// TEXT is limited to 65,535 characters. MEDIUMTEXT is 16,777,215
		if (form.getCategory().getProtectedHtmlCode() != null && form.getCategory().getProtectedHtmlCode().length() > 1000000) {
			Object[] errorArgs = { form.getCategory().getProtectedHtmlCode().length(), new Integer(1000000) };
			errors.rejectValue("category.protectedHtmlCode", "form.charOverLimit", errorArgs, 
					form.getCategory().getProtectedHtmlCode().length() + " chars (1,000,000 limit)");
		}
		
		// footer html code
		// TEXT is limited to 65,535 characters. MEDIUMTEXT is 16,777,215
		if (form.getCategory().getFooterHtmlCode() != null && form.getCategory().getFooterHtmlCode().length() > 1000000) {
			Object[] errorArgs = { form.getCategory().getFooterHtmlCode().length(), new Integer(1000000) };
			errors.rejectValue("category.footerHtmlCode", "form.charOverLimit", errorArgs, 
					form.getCategory().getFooterHtmlCode().length() + " chars (1,000,000 limit)");
		}

		// rightbar top html code
		// TEXT is limited to 65,535 characters. MEDIUMTEXT is 16,777,215
		if (form.getCategory().getRightBarTopHtmlCode() != null && form.getCategory().getRightBarTopHtmlCode().length() > 1000000) {
			Object[] errorArgs = { form.getCategory().getRightBarTopHtmlCode().length(), new Integer(1000000) };
			errors.rejectValue("category.rightBarTopHtmlCode", "form.charOverLimit", errorArgs, 
					form.getCategory().getRightBarTopHtmlCode().length() + " chars (1,000,000 limit)");
		}	
	}

}
