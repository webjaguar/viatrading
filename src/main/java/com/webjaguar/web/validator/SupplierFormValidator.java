/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.SupplierForm;

public class SupplierFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(SupplierForm.class);
	}
	
	public void validate(Object obj, Errors errors) {
		SupplierForm supplierForm = (SupplierForm) obj;
		validateAddress(errors);
		validateEmail("supplier.address.email", supplierForm.getSupplier().getAddress().getEmail(), errors);
	}
	
	public void validateAddress(Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "supplier.address.company", "form.required", "required");	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "supplier.accountNumber", "form.required", "required");	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "supplier.address.addr1", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "supplier.address.city", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "supplier.address.stateProvince", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "supplier.address.zip", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "supplier.address.country", "form.required", "required");		
	}
    
    private void validateEmail(String fieldName, String email, Errors errors) {
		if (!Constants.validateEmail(email, null)) {
			errors.rejectValue(fieldName, "form.invalidEmail", "invalid email");
		}
    }
}