/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.28.2007
 */

package com.webjaguar.web.validator;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.EmailMessageForm;

public class EmailMessageFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(EmailMessageForm.class);
	}
	
	public void validate(Object obj, Errors errors) {
		EmailMessageForm form = (EmailMessageForm) obj;
		try {
			new InternetAddress(form.getFrom());
			if (!form.getFrom().contains( "@" )) {
				errors.rejectValue("from", "config.exception.contactEmail");				
			}
		} catch (AddressException ex) {
			errors.rejectValue("from", "config.exception.contactEmail");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "to", "form.required", "required");
		if (!errors.hasFieldErrors("to") && !validateEmails(form.getTo())) {
			errors.rejectValue("to", "form.invalidEmail");
		} 
		if (form.getCc() != null && form.getCc().trim().length() > 0) {
			if (!validateEmails(form.getCc())) {
				errors.rejectValue("cc", "form.invalidEmail");
			}			
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "subject", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "message", "form.required", "required");
	}
    
    private boolean validateEmails(String field) {
		boolean valid = true;
		String[] emails = field.split( "[,]" ); // split by commas
		int counter = 0;
		for ( int x = 0; x < emails.length; x++ ) {
			if (emails[x].trim().length() > 0) {
				if (Constants.validateEmail(emails[x].trim(), null)) {
					counter++;
				} else {
					return false;
				}
			}
		}
		if (counter < 1) {
			return false;
		}
		
		return valid;
    }
}
