/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.31.2007
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.form.PaymentForm;

public class PaymentFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(PaymentForm.class);
	}
	
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "payment.amount", "form.required", "required");			
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "payment.memo", "form.required", "required");			
	}

}
