/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.15.2009
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.model.BudgetProduct;

public class BudgetProductFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(BudgetProduct.class);
	}
	
	public void validate(Object o, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "qty", "form.required", "required");			
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "product.sku", "form.required", "required");			
	}

}
