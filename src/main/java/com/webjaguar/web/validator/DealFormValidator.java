/*
 * Copyright 2005, 2009 Advanced E-Media Solutions 
 * @author: Jwalant Patel
 * 
 */

package com.webjaguar.web.validator;

import com.webjaguar.web.form.DealForm;

public class DealFormValidator implements org.springframework.validation.Validator
{
	public boolean supports(Class aClass)
	{
		return aClass.equals( DealForm.class );
	}

	public void validate(Object command, org.springframework.validation.Errors errors)
	{
		DealForm dealForm = (DealForm) command;

		// check title
		org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "deal.title", "form.required", "required" );
		if ( dealForm.getDeal().getTitle().length() > 100 )
		{
			Object[] errorArgs = { new Integer( 100 ) };
			errors.rejectValue( "deal.title", "form.charOverMax", errorArgs, "should be at most 100 chars" );
		}
		dealForm.getDeal().setTitle( dealForm.getDeal().getTitle().trim() );
		
		// check quantity
		if( dealForm.getDeal().getBuySku() != null && !dealForm.getDeal().getBuySku().equals("") && dealForm.getDeal().getBuyQuantity() == 0  ) {
			errors.rejectValue( "deal.buyQuantity", "typeMismatch.deal.quantity", "select quantity");
		}
		
		// check Sku
		org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "deal.getSku", "form.required", "required" );
		if( (dealForm.getDeal().getBuySku() == null || dealForm.getDeal().getBuySku().equals("")) && (dealForm.getDeal().getBuyAmount() == null || dealForm.getDeal().getBuyAmount() == 0.0) ) {
			org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "deal.buySku", "form.required", "required" );	
		}
		
		// check discount
		org.springframework.validation.ValidationUtils.rejectIfEmptyOrWhitespace( errors, "deal.discount", "form.required", "required" );
		if ( dealForm.getDeal().getDiscount() != null )
		{
			if ( dealForm.getDeal().getDiscount().doubleValue() < 0 )
			{
				errors.rejectValue( "deal.discount", "form.negAmount", "should be a positive amount" );
			}
			else if ( dealForm.getDeal().isDiscountType() )
			{
				if ( dealForm.getDeal().getDiscount().doubleValue() > 100 )
				{
					dealForm.getDeal().setDiscount( 100.00 );
				}

			}
		}
	}
}