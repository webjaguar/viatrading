/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.validator;

import java.lang.reflect.Method;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.model.Address;
import com.webjaguar.model.Customer;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.PasswordByte;
import com.webjaguar.web.form.CustomerForm;

public class CustomerFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(CustomerForm.class);
	}

	public void validate(Object obj, Errors errors) {
		CustomerForm customerForm = (CustomerForm) obj;
		if (customerForm.isTouchScreen() && customerForm.getCustomer().getUsername() != "") {
			validateEmail(customerForm, errors);
			validatePassword(customerForm, errors);
		} else if (!customerForm.isTouchScreen()) {
			validateEmail(customerForm, errors);
			validatePassword(customerForm, errors);
		}
		validateAddress(customerForm.getCustomer().getAddress(), errors);
		if (customerForm.isCompanyRequired()) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.company", "form.required", "required");
		}

		Class<Customer> c = Customer.class;
		Method m = null;
		Object arglist[] = null;

		// custom notes
		// TEXT is limited to 65,535 characters. MEDIUMTEXT is 16,777,215
		try {
			for (int i = 1; i <= 3; i++) {
				m = c.getMethod("getCustomNote" + i);
				String customNote = (String) m.invoke(customerForm.getCustomer(), arglist);
				if (customNote != null && customNote.length() > 1000000) {
					Object[] errorArgs = { customNote.length(), new Integer(1000000) };
					errors.rejectValue("customer.customNote" + i, "form.charOverLimit", errorArgs, customNote.length() + " chars (1,000,000 limit)");
				}
			}
		} catch (Exception e) {
			// do nothing
		}
	}

	public void validatePassword(CustomerForm customerForm, Errors errors) {
		// Strong password require at least 1 special character and 1 Digit in Password.
		if (customerForm.isStrongPassword()) {
			PasswordByte password = new PasswordByte(customerForm.getCustomer().getPassword());
			if ((password.getLength() < customerForm.getMinPasswordLength()) || (password.getNumOfDigits() < 1 || password.getNumOfSpecial() < 1)) {
				Object[] errorArgs = { new Integer(customerForm.getMinPasswordLength()) };
				errors.rejectValue("customer.password", "form.charLessMinSpDig", errorArgs, "should be at least " + customerForm.getMinPasswordLength() + " chars");
			} else if (!customerForm.getCustomer().getPassword().equals(customerForm.getConfirmPassword())) {
				errors.rejectValue("confirmPassword", "form.passwordMismatch", "matching passwords required");
			}
		} else {
			if (customerForm!=null && customerForm.getCustomer()!=null && customerForm.getCustomer().getPassword()!=null && customerForm.getCustomer().getPassword().length() > 0) {
				if (customerForm.getCustomer().getPassword().length() < customerForm.getMinPasswordLength()) {
					Object[] errorArgs = { new Integer(customerForm.getMinPasswordLength()) };
					errors.rejectValue("customer.password", "form.charLessMin", errorArgs, "should be at least " + customerForm.getMinPasswordLength() + " chars");
				} else if (!customerForm.getCustomer().getPassword().equals(customerForm.getConfirmPassword())) {
					errors.rejectValue("confirmPassword", "form.passwordMismatch", "matching passwords required");
				}
			}
		}
	}

	public void validateAddress(Address address, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.firstName", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.lastName", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.addr1", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.city", "form.required", "required");
		if (address.isStateProvinceNA()) {
			address.setStateProvince("");
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.stateProvince", "form.required", "required");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.country", "form.required", "required");

		if (address.getCountry().equalsIgnoreCase("us")) {
			// (123)456-7890, 123-456-7890, 1234567890, (123)-456-7890
			// final String US_PHONE_REGEXP = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
			// with ext (xxx) xxx-xxxx Ext-xxxxx
			final String US_PHONE_REGEXP = "^ \\(?   (\\d{3})   \\)?    [- ]?   (\\d{3})     [- ]?   (\\d{4})   (\\s Ext-\\d{1,5}) ? $";
			final String New_format = "^\\((\\d{3})\\)(\\d{3})[- ](\\d{4})(\\s Ext-\\d{1,5})?$";
			final String US_ZIP_REGEXP = "^\\d{5}(-\\d{4})?$";
			// if added as 10 digit number we add (xxx) xxx-xxxx format to it.
			if (Constants.isDigit(address.getPhone().trim()) && address.getPhone().trim().length() == 10) {
				address.setPhone("(" + address.getPhone().trim().substring(0, 3) + ") " + address.getPhone().trim().substring(3, 6) + "-" + address.getPhone().trim().substring(6));
			}
			if (!(Pattern.compile(New_format).matcher(address.getPhone().trim()).matches())) {
				errors.rejectValue("customer.address.phone", "form.usPhone", "invalid USA phone");
			}
			
			if ((address.getCellPhone().trim() != null && !address.getCellPhone().trim().equals("")) && !(Pattern.compile(New_format).matcher(address.getCellPhone().trim()).matches())) {
				errors.rejectValue("customer.address.cellPhone", "form.usPhone", "invalid USA phone");
			}
			if ((address.getZip() == null || address.getZip().isEmpty()) ) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.zip", "form.required", "required");
			} else {
				if (!(Pattern.compile(US_ZIP_REGEXP).matcher(address.getZip().trim()).matches())) {
					errors.rejectValue("customer.address.zip", "form.usZipcode", "invalid USA zip code");
				}
			}
			address.setPhone(address.getPhone().trim());
			address.setZip(address.getZip().trim());
		} else if (address.getCountry().equalsIgnoreCase("ca")) {
			final String CA_ZIP_REGEXP_SPACE = "[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]\\d[a-zA-Z] \\d[a-zA-Z]\\d";
			final String CA_ZIP_REGEXP = "[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]\\d[a-zA-Z]\\d[a-zA-Z]\\d";
			if (address.getZip() == null || address.getZip().isEmpty()) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.zip", "form.required", "required");
			} else {
				if (!Pattern.compile(CA_ZIP_REGEXP_SPACE).matcher(address.getZip()).matches() && Pattern.compile(CA_ZIP_REGEXP).matcher(address.getZip()).matches()) {
					address.setZip(address.getZip().substring(0, 3) + " " + address.getZip().substring(3, 6));
				}
				if (!(Pattern.compile(CA_ZIP_REGEXP).matcher(address.getZip()).matches() || Pattern.compile(CA_ZIP_REGEXP_SPACE).matcher(address.getZip()).matches())) {
					errors.rejectValue("customer.address.zip", "form.canadaZipcode", "invalid Canada zipcode");
				}
			}
		} else {
//			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.zip", "form.required", "required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.phone", "form.required", "required");
		}
	}

	private void validateEmail(CustomerForm customerForm, Errors errors) {
		
		Customer customer = customerForm.getCustomer();
		
		if(customer.getUsernameDisabled()==null){
			if (customer.getUsername() == null || customer.getUsername().isEmpty()) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.username", "form.required", "required");
			} else {
				if (!Constants.validateEmail(customer.getUsername(), customerForm.getEmailRegExp())) {
					errors.rejectValue("customer.username", "form.invalidEmail", "invalid email");
				}
			}
		}
		else if(customer.getUsernameDisabled()!=null && !customer.getUsernameDisabled()){
			if (customer.getUsername() == null || customer.getUsername().isEmpty()) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.username", "form.required", "required");
			} else {
				if (!Constants.validateEmail(customer.getUsername(), customerForm.getEmailRegExp())) {
					errors.rejectValue("customer.username", "form.invalidEmail", "invalid email");
				}
			}
		}

		if (customer.getExtraEmail() != null) {
			StringTokenizer st = new StringTokenizer(customer.getExtraEmail(), ", ", false);
			while (st.hasMoreTokens()) {
				String tempToken = st.nextToken();
				if (!Constants.validateEmail(tempToken, customerForm.getEmailRegExp())) {
					errors.rejectValue("customer.extraEmail", "form.invalidEmail", "invalid email");
				}
			}
		}
		
	}
}
