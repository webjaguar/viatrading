/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.CustomerForm;

public class AccountFormValidator implements Validator {
	public boolean supports(Class aClass) {
		return aClass.equals(CustomerForm.class);
	}
	
	public void validate(Object obj, Errors errors) {
		validateEmail("customer.username", ((CustomerForm) obj).getCustomer().getUsername(), errors);
		validatePassword((CustomerForm) obj, errors);
	}
	
	public void validatePassword(CustomerForm customerForm, Errors errors) {
		// password
		if (customerForm.getCustomer().getPassword().length() > 0) {
		  if (customerForm.getCustomer().getPassword().length() < 5) {
			Object[] errorArgs = { new Integer(5) };
			errors.rejectValue("customer.password", "form.charLessMin", errorArgs, "should be at least 5 chars");
		  } else if (!customerForm.getCustomer().getPassword().equals(customerForm.getConfirmPassword())) {
			errors.rejectValue("confirmPassword", "form.passwordMismatch", "matching passwords required");
		  }
		}		
	}
    
    private void validateEmail(String fieldName, String email, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, fieldName, "form.required", "required");    	

		if (!Constants.validateEmail(email, null)) {
			errors.rejectValue(fieldName, "form.invalidEmail", "invalid email");
		}
    }
}