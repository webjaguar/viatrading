/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.24.2006
 */

package com.webjaguar.web.interceptor;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mobile.device.site.SitePreference;
import org.springframework.mobile.device.site.SitePreferenceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.site.SitePreference;
import org.springframework.mobile.device.site.SitePreferenceUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.ServletRequestUtils;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.AccessUserAudit;
import com.webjaguar.model.Configuration;

public class AdminInterceptor extends HandlerInterceptorAdapter {

	private GlobalDao globalDao;	
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		Map<String, Object> gSiteConfig = globalDao.getGlobalSiteConfig();
		int gSESSION_TIMEOUT = (Integer) gSiteConfig.get("gSESSION_TIMEOUT");
      	setSessionTimeout(request, gSESSION_TIMEOUT);
      	request.getSession().setAttribute( "adminLogin", true);
      	
      	String userName = null;  			
      	if(SecurityContextHolder.getContext().getAuthentication().getName()!=null){
      		userName = SecurityContextHolder.getContext().getAuthentication().getName();
      	}else{
	      	//System.out.println("SecurityContextHolder.getContext().getAuthentication().getName() is null---------------------------------------------------");
      	}
      	
      	AccessUser user = this.webJaguar.getUserByUserName(userName);
      	/*if(user!=null){
	      
      		URL url2 = new URL(request.getScheme()+"://www.viatrading.com/dv/login.php?VICKIAdmin=Yes&FirstName=" + user.getFirstName() + "&LastName=" + user.getLastName());
	      	//System.out.println("url2.---------------------------------------------------" + url2.getPath());
			HttpURLConnection con = (HttpURLConnection) url2.openConnection();
		    con.setRequestMethod("GET");
		    con.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
		    con.addRequestProperty("User-Agent", "Mozilla");
		    con.addRequestProperty("Referer", "google.com");
		    con.setRequestProperty("Accept", "text/html, text/*");
		    con.addRequestProperty("Content-Type","application/json; charset=UTF-8"); 
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuilder sb = new StringBuilder(); 
			while((inputLine = in.readLine()) != null)
			{
			    if(inputLine!=null && !inputLine.isEmpty()){
			    	sb.append(inputLine);
			    	//System.out.println(inputLine);
			    }
			}		
      	}else{
      		//System.out.println("userName------------------------------------------------" + userName);
      		//System.out.println("AccessUser user is null------------------------------------------------");
      	}*/
      	
		// set up link for i18n
		if (request.getMethod() == "GET") {
			StringBuffer url = new StringBuffer(request.getRequestURL() + "?");
			Map params = request.getParameterMap();
			if (!params.isEmpty()) {
				Iterator iter = params.keySet().iterator();
				while (iter.hasNext()) {
					String param = (String) iter.next();
					if (!param.equals("language")) {
						url.append(param);
						url.append("=");
						url.append(request.getParameter(param));						
						url.append("&");
					}
				}
			}
			request.setAttribute("url", url);
		}

		//New Admin
		if (ServletRequestUtils.getBooleanParameter(request, "newAdmin") != null) {
			if (ServletRequestUtils.getBooleanParameter(request, "newAdmin", false)) {
				request.getSession().setAttribute("newAdmin", true);
			} else {
				request.getSession().removeAttribute("newAdmin");
				request.removeAttribute("newAdmin");
			}
		} else if(request.getSession().getAttribute("newAdmin") != null) {
			request.getSession().setAttribute("newAdmin", true);
			request.setAttribute("newAdmin", true);
		}
				
		String adminLanguages[] = {"en", "fr", "pt", "es"};
		
		request.setAttribute("_contextpath", request.getContextPath());
		// set up admin languages
		if (adminLanguages != null) {
			List<String> displayLanguages = new ArrayList<String>();
			for (int i=0; i<adminLanguages.length; i++) {
				displayLanguages.add(adminLanguages[i]);
			}
			Locale locale = RequestContextUtils.getLocale(request);
			displayLanguages.remove(locale.getLanguage());
			request.setAttribute("adminLanguages", displayLanguages);			
		}

		// access privilege
		AccessUser accessUser = null;
		Collection<GrantedAuthority> gAuthorities = (Collection<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		for (GrantedAuthority athority : gAuthorities) {
			if (!(athority.getAuthority().equals( "ROLE_AEM" ) || 
					athority.getAuthority().startsWith("ROLE_ADMIN_"))) {
			    accessUser = this.webJaguar.getUserByUserName( SecurityContextHolder.getContext().getAuthentication().getName());
			    
			    
			    
			    if ( accessUser != null ) {
			    AccessUserAudit accessUserAudit = new AccessUserAudit(accessUser.getUsername(), request.getContextPath() + request.getServletPath(), request.getRemoteAddr());
			    	this.webJaguar.insertUserAudit( accessUserAudit );
			    	request.setAttribute("accessUser", accessUser);
			    }
			    break;
			}
		}
		if (request.getAttribute("accessUser") == null) {
			request.setAttribute("accessUser", new AccessUser("Admin", 0));
		}
		
		
		
		if ((Boolean) gSiteConfig.get("gSITE_ACTIVE") == false) {
			// prevent access privilege users from accessing a disabled site
			if ( accessUser != null )
				return false;
		}
		request.setAttribute("gSiteConfig", gSiteConfig);
		
		if  ( accessUser != null ) {
			String[] ips = accessUser.getIpAddress().split( "[,\n]" ); // split by line breaks or commas 
			for ( int x = 0; x < ips.length; x++ )
			{
				String ip = ips[x].trim();
				if ( !("".equals(ip)) ) {
					if (!request.getRemoteAddr().equals( ip )) {
						if ( x == ips.length -1 ) {
							request.setAttribute("hideadmin", true);
						}
					} else {
						break;
					}
				}
			}
		}
		
		// site configuration
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();
		request.setAttribute( "siteConfig", siteConfig );
		
		Properties prop = new Properties();
		prop.load( new FileInputStream( request.getRealPath( "WEB-INF/properties/site.properties" ) ) );
		String siteId = (String) prop.get( "site.id" );
		String siteRoot = (String) prop.get( "site.root" );
		request.setAttribute( "prop_site_id", siteId );
		request.setAttribute( "prop_site_root", siteRoot );
		
		// quote View
		request.setAttribute("orderName", "order");
		
		// multi store
		if (request.getServletPath().contains("/orders/") || request.getServletPath().contains("/customers/")) {
			request.setAttribute("multiStores", this.webJaguar.getMultiStore("all"));
		}
		
		// Mobile Layout
		SitePreference sitePreference = SitePreferenceUtils.getCurrentSitePreference(request);
		/* ON ADMIN we need to implement MOBILE LAYOUT yet. */
		// prepare mobile view for rendering.
		// site_preference=mobile
		request.setAttribute("_mobile", ((Boolean) gSiteConfig.get( "gMOBILE_LAYOUT" ) && (sitePreference == SitePreference.MOBILE)) ? "_mobile" : "");
		
		return true;
	}

	private void setSessionTimeout(HttpServletRequest request, int gSESSION_TIMEOUT) {
		// minimum of 5 minutes and maximum of 1 hour
		if (gSESSION_TIMEOUT <= 60 && gSESSION_TIMEOUT > 5 && 
				gSESSION_TIMEOUT*60 != request.getSession().getMaxInactiveInterval()) {
			request.getSession().setMaxInactiveInterval(gSESSION_TIMEOUT*60);				
		}
	}
}
