/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.interceptor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class LoginInterceptor extends HandlerInterceptorAdapter {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
    	UserSession userSession = this.webJaguar.getUserSession(request);		

		if (userSession == null) {
			Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
			String url = request.getContextPath() + request.getServletPath();
			String query = request.getQueryString();
			Map<String, Object> model = new HashMap<String, Object>();
			// Mobile Layout
	    	String mobile = "";
			if ((request.getAttribute("_mobile").toString().equalsIgnoreCase("_mobile"))) {
				mobile = request.getAttribute("_mobile").toString();
			}
			if (!mobile.equalsIgnoreCase("_mobile")) {
				model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request, null));
			}
			
			String scheme = "http";
			if (siteConfig.get("SECURE_URL").getValue().startsWith("https")) {
				scheme = "https";
			}
			String secureUrl = scheme + "://" + request.getHeader("host") + request.getContextPath() + "/";
			model.put("secureUrl", secureUrl);
			
			ModelAndView modelAndView = new ModelAndView("frontend/login" + mobile, "model", model);
			if (siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
				modelAndView = new ModelAndView("frontend/layout/template6/login", "model", model);
			}
			if (query != null) {
				modelAndView.addObject("signonForwardAction", url+"?"+query);
			}
			else {
				modelAndView.addObject("signonForwardAction", url);
			}
			
			Layout layout = (Layout) request.getAttribute( "layout" );
			layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Login"));
			if(url.contains("addToList.jhtm")){
				layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Login"));
			}
			if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
			{
				layout.setLeftBarTopHtml( "" );
				layout.setLeftBarBottomHtml( "" );
				layout.setHideLeftBar( true );
			} 
			this.webJaguar.updateCartQuantity(userSession, request, layout);
			model.put("loginLayout", this.webJaguar.getSystemLayout("login", request.getHeader("host"), request));
			
			throw new ModelAndViewDefiningException(modelAndView);
		}
		else {
			return true;
		}
	}
}