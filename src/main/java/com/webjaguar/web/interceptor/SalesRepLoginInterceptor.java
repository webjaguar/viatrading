/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.02.2009
 */

package com.webjaguar.web.interceptor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;

public class SalesRepLoginInterceptor extends HandlerInterceptorAdapter {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("SALESREP_LOGIN").getValue().equals("false")) {
			return false;
		}

		if (this.webJaguar.getSalesRep(request) == null) {
			String url = request.getContextPath() + request.getServletPath();
			String query = request.getQueryString();
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
			ModelAndView modelAndView = new ModelAndView("frontend/salesRep/login", "model", model);
			if (query != null) {
				modelAndView.addObject("signonForwardAction", url+"?"+query);
			} else {
				modelAndView.addObject("signonForwardAction", url);
			}
			
			Layout layout = (Layout) request.getAttribute("layout");
			if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
				layout.setLeftBarTopHtml("");
				layout.setLeftBarBottomHtml("");
				layout.setHideLeftBar(true);
			} 
			// multi store
			model.put("loginLayout", this.webJaguar.getSystemLayout("srLogin", request.getHeader("host"), request));
			
			throw new ModelAndViewDefiningException(modelAndView);
		} else {
			return true;
		}
	}
}
