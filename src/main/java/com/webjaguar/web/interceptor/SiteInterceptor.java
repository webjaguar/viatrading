/*
 * Copyright 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.web.interceptor;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DeviceUtils;
import org.springframework.mobile.device.site.SitePreference;
import org.springframework.mobile.device.site.SitePreferenceUtils;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.UserSession;
import com.webjaguar.thirdparty.searchEngine.SearchEngineProspect;
import com.webjaguar.thirdparty.triguide.TriguideApi;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.Utilities;

public class SiteInterceptor extends HandlerInterceptorAdapter {
	private static final Logger logger = LoggerFactory.getLogger(SiteInterceptor.class);

	private GlobalDao globalDao;

	public void setGlobalDao(GlobalDao globalDao) {
		this.globalDao = globalDao;
	}

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private SearchEngineProspect searchEngineProspect;

	public void setSearchEngineProspect(SearchEngineProspect searchEngineProspect) {
		this.searchEngineProspect = searchEngineProspect;
	}

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		
		// logger.info("Beginning of SiteInterceptor...");
		// global configuration
		Map<String, Object> gSiteConfig = globalDao.getGlobalSiteConfig();
		if ((Boolean) gSiteConfig.get("gSITE_ACTIVE") == false)
			return false;
		request.setAttribute("gSiteConfig", gSiteConfig);

		if ((Boolean) gSiteConfig.get("gSEARCH_ENGINE_PROSPECT")) {
			searchEngineProspect.filter(request, response, handler);
		}

		// site configuration
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();
		request.setAttribute("siteConfig", siteConfig);
		request.setAttribute("_template", siteConfig.get("TEMPLATE").getValue());
		request.setAttribute("_leftBar", siteConfig.get("LEFTBAR_DEFAULT_TYPE").getValue());

		// without this mod rewrite pages will save multiple cookies with different paths
		String path = request.getContextPath().equals("") ? "/" : request.getContextPath();

		// get jsId from cXML return URL
		if (request.getParameter("jsId") != null) {
			request.getSession().setAttribute("jsId", request.getParameter("jsId"));
		}
		// get tracking code
		String trackcode = request.getParameter("trackcode");

		if (trackcode == null && request.getParameter("tc") != null) {
			// alternative tracking code
			trackcode = request.getParameter("tc");
		}
		if (trackcode != null) {
			request.setAttribute("trackcode", trackcode);
			Cookie trackcodeCookie = new Cookie("trackcode", trackcode);
			int days = Integer.parseInt(siteConfig.get("COOKIE_AGE").getValue());
			trackcodeCookie.setMaxAge(Constants.SECONDS_IN_DAY * days);
			trackcodeCookie.setPath(path);
			response.addCookie(trackcodeCookie);
		} else {
			Cookie trackcodeCookie = WebUtils.getCookie(request, "trackcode");
			if (trackcodeCookie != null)
				trackcode = trackcodeCookie.getValue();
		}
		if (trackcode == null)
			trackcode = "";
		request.setAttribute("trackcode", trackcode);
		request.getSession().setAttribute("trkcd", trackcode);
		request.getSession().setAttribute("parameterTrackcode", trackcode);

		if (request.getParameter("crmcontactid") != null) {
			request.getSession().setAttribute("ccid", request.getParameter("crmcontactid"));
		}
		
		// get affiliate promo code
		String promocode = request.getParameter("promocode");
		if (promocode != null) {
			request.setAttribute("promocode", promocode);
			Cookie promocodeCookie = new Cookie("promocode", promocode);
			int days = Integer.parseInt(siteConfig.get("COOKIE_AGE").getValue());
			promocodeCookie.setMaxAge(Constants.SECONDS_IN_DAY * days);
			promocodeCookie.setPath(path);
			response.addCookie(promocodeCookie);
		} else {
			Cookie promocodeCookie = WebUtils.getCookie(request, "promocode");
			if (promocodeCookie != null)
				promocode = promocodeCookie.getValue();
		}
		if (promocode == null)
			promocode = "";

		request.setAttribute("promocode", promocode);

		// get user session
		UserSession userSession = (UserSession) WebUtils.getSessionAttribute(request, "userSession");

    	String url2 = request.getServletPath();
		if(url2.contains("viewCart.jhtm") && userSession!=null){
			String pcode = ServletRequestUtils.getStringParameter(request, "promocode", "");
    		userSession.setPromocode(pcode);
    		
		}
		
		if(url2.contains("checkout1.jhtm") && userSession!=null){
			userSession.setPromocode(promocode);
    		
		}

		// KeepMeLogin Module
		if (Integer.parseInt(siteConfig.get("COOKIE_AGE_KEEP_ME_LOGIN").getValue()) > -1 && userSession == null) {
			
			String UKWJToken = Utilities.getCookieValue(request, "UKWJ");
			Integer userId = null;
			if (UKWJToken != null) {
				userId = this.webJaguar.getUserIdByToken(UKWJToken);
			}
			Customer customer = null;
			if (userId != null) {
				customer = this.webJaguar.getCustomerById(userId);
			}
			if (customer != null) {
				UserSession newUserSession = new UserSession();
				newUserSession.setFirstName(customer.getAddress().getFirstName());
				newUserSession.setLastName(customer.getAddress().getLastName());
				newUserSession.setUserid(customer.getId());
				newUserSession.setUsername(customer.getUsername());
				userSession = newUserSession;
				// need more work to change the token value in cookie and db.
				// this.webJaguar.changeCustomerToken(customer.getId());
			}
		}
		
		int layoutId = Integer.parseInt(siteConfig.get("DEFAULT_LAYOUT_ID").getValue());
		Cookie layoutCookie = WebUtils.getCookie(request, "layout");
		if (layoutCookie != null && siteConfig.get("SAVE_LAYOUT_ON_COOKIE").getValue().equals("true")) {
			layoutId = Integer.parseInt(layoutCookie.getValue());
		}
		Layout layout = this.webJaguar.getLayout(layoutId, request);
		if(layout!=null && layout.getTopBarHtml()!=null && request!=null && request.getServletPath()!=null && request.getServletPath().contains("unsubscribe.jhtm")){
			layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Unsubscribe"));
		}

		Customer customer = null;
		if (userSession != null) {
			// check if user is still valid
			if ((customer = this.webJaguar.getCustomerById(userSession.getUserid())) != null) {
				request.setAttribute("sessionCustomer", customer);
				if (customer != null) {
					SalesRep salesRep = this.webJaguar.getSalesRepById(customer.getSalesRepId());
					if (salesRep != null) {
						request.setAttribute("sessionSalesRep", salesRep);
						// save a cookie if SalesRep Exist.
						Integer cookieMaxAge = Constants.SECONDS_IN_DAY * 180;
						Utilities.addCookie(response, "SR", "1", cookieMaxAge, path);
						
					} else {
						// remove cookie
						Utilities.removeCookie(response, "SR", null);
					}
				}
				if ((Boolean) gSiteConfig.get("gSHOPPING_CART")) {
					// get cart
					if (request.getSession().getAttribute("userCart") == null) {
						Cart cart = new Cart();
						this.webJaguar.updateCart(cart, request);
						request.getSession().setAttribute("userCart", cart);
					}
				}
			} else {
				request.getSession().invalidate();
				userSession = null;
			}
		} else {
			// check if login is required for this domain/host
			if (siteConfig.get("PROTECTED_HOST").getValue().equals(request.getHeader("host"))) {
				if (!(request.getServletPath().equals("/login.jhtm") || request.getServletPath().equals("/forgetPassword.jhtm"))) {
					throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("login.jhtm?noMessage=t")));
				}
			}
		}
		// added for Add to Presentation Button
		if (this.webJaguar.getSalesRep(request) != null) {
			request.setAttribute("cookieSalesRep", this.webJaguar.getSalesRep(request));
		}
		
		
		request.setAttribute("userSession", userSession);
		
		SitePreference sitePreference = SitePreferenceUtils.getCurrentSitePreference(request);
        // prepare mobile view for rendering
		// site_preference=mobile
		
		String _mobile= "";
		if((Boolean) gSiteConfig.get( "gMOBILE_LAYOUT" )){
			try {
				Device currentDevice = DeviceUtils.getCurrentDevice(request);
				if(sitePreference == SitePreference.MOBILE) {
					_mobile = "_mobile";
				} else if(sitePreference == SitePreference.NORMAL) {
					_mobile = "";
				} else if(currentDevice.isMobile() || currentDevice.isTablet()) {
					_mobile = "_mobile";
				} 
			} catch(Exception e) { }
		}
		
		request.setAttribute("_mobile", _mobile);
		
		// Encode customer ID in cookies
		
		Boolean CustCookieExists = false;
		if(customer!= null && customer.getId()!=null && !customer.getId().toString().isEmpty()){
		   try{
				String orig = customer.getId().toString();
			    byte[] encoded = Base64.encodeBase64(orig.getBytes());         
			    String encode = new String(encoded);   
		
			   
			    if (request.getCookies() != null) {
					Cookie[] cookies = request.getCookies();
					for (Cookie cookie : cookies) {
						if (cookie.getName().trim().equals("CI") && !cookie.getValue().trim().equals("")) {
							CustCookieExists = true;
							cookie.setValue(encode);		
							response.addCookie(cookie);	
							//System.out.println("new value"+cookie.getValue());
					}
				  }
				}		    		    
				if(!CustCookieExists){
					Cookie CustomerCookie = new Cookie("CI", encode);
					//int days = Integer.parseInt(siteConfig.get("COOKIE_AGE").getValue());
					CustomerCookie.setMaxAge(-1);
					CustomerCookie.setPath(path);							
					response.addCookie(CustomerCookie);	
				} 
				
			} catch (Exception e){
					Cookie CustID = new Cookie("CI", "ERROR");
					CustID.setPath(path);
					response.addCookie(CustID);
			}
		 } 
		
		if(customer == null || customer.getId() == null || customer.getId().toString().isEmpty()){
			Utilities.removeCookie(response, "CI", null);
		}
		
			
		// layout and trackcode
		// i18n and multi store
		layoutId = Integer.parseInt(siteConfig.get("DEFAULT_LAYOUT_ID").getValue());
		layoutCookie = WebUtils.getCookie(request, "layout");
		if (layoutCookie != null && siteConfig.get("SAVE_LAYOUT_ON_COOKIE").getValue().equals("true")) {
			layoutId = Integer.parseInt(layoutCookie.getValue());
		}
		
		String url = request.getServletPath();
		if(url.contains("formProcessor.jhtm")){
			layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Form"));
		}else if(userSession != null && url.contains("invoice.jhtm")){
			layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Order"));
		}

		if (userSession != null) {
			layout.initLayout(request.getAttribute("_mobile").toString().equalsIgnoreCase("_mobile"));
			layout.replace("#firstname#", userSession.getFirstName());
			layout.replace("#lastname#", userSession.getLastName());
			layout.replace("#email#", userSession.getUsername());
		} else {
			layout.replace("#firstname#", "");
			layout.replace("#lastname#", "");
			layout.replace("#email#", "");
		}
		layout.replace("#trackcode#", trackcode);
				
		if(!url2.contains("viewCart.jhtm")){
			this.webJaguar.updateCartQuantity(userSession, request, layout);
		}

		request.setAttribute("layout", layout);

		if (((String) gSiteConfig.get("gSEARCH")).equals("triguide")) {
			// triguide manufacturers
			String triguideID = siteConfig.get("SEARCH_ACCOUNT").getValue();
			if (!triguideID.equals("") && request.getSession().getAttribute("triguideManufacturers") == null) {
				TriguideApi triguide = new TriguideApi(siteConfig.get("TRIGUIDE_URL").getValue());
				request.getSession().setAttribute("triguideManufacturers", triguide.ModelManufacturers(triguideID, "0", "0"));
			}
		}

		// i18n
		String lang = RequestContextUtils.getLocale(request).getLanguage();
		request.setAttribute("lng",lang);
		if (((String) gSiteConfig.get("gI18N")).length() > 0 && this.webJaguar.getLanguageSetting(lang) != null) {
			request.setAttribute("_lang", "_" + lang);
		} else {
			request.setAttribute("_lang", "");
			lang = null;
		}

		if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
			// set the currency for Canada
			if (customer != null && customer.getPriceTable() >= 6) {
				Configuration conf = new Configuration();
				conf.setKey("CURRENCY");
				conf.setValue("CAD");
				siteConfig.put("CURRENCY", conf);
			}
		}

		// for mod rewrite
		request.setAttribute("_contextpath", request.getContextPath());

		if (siteConfig.get("HORIZONTAL_DYNAMIC_MENU").getValue().trim().length() > 0) {
			String protectedAccess = null;
			// check if protected feature is enabled
			int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
			if (protectedLevels > 0) {
				if (customer != null) {
					protectedAccess = customer.getProtectedAccess();
				} else {
					protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
				}
			}
			Set<Integer> extraIds = new TreeSet<Integer>();
			for (String extraId : siteConfig.get("HORIZONTAL_DYNAMIC_MENU").getValue().split(",")) {
				try {
					extraIds.add(Integer.parseInt(extraId.trim()));
				} catch (Exception e) {
					// do nothing
				}
			}
			request.setAttribute("dynamicMenu", this.webJaguar.getCategoryTree(null, protectedAccess, false, null, extraIds, lang));
		}
		
		return true;
	}
}