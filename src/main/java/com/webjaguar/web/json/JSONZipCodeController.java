package com.webjaguar.web.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ZipCode;

public class JSONZipCodeController extends MultiActionController {
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }
	
	  public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) {
	    	
	    	Map<String, Object> map = new HashMap<String, Object>();
	    	Map<String, Configuration> siteConfig =  (Map<String, Configuration>) request.getAttribute( "siteConfig" );
	    	
	    	int zipCodeNum = ServletRequestUtils.getIntParameter(request, "zipCode", -1);

	    	
	    	ZipCode zipCode = this.webJaguar.getZipCode(String.valueOf(zipCodeNum));
	    	
	    	if(zipCode != null){
	    		map.put("jsonObject", new JSONObject(zipCode));    	
	    	}else{
	    		map.put("jsonObject", null );    	
	    	}
	    	
	    	
	    	return new ModelAndView("json/jsonZipCode", map);
	    }
}
