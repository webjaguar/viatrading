/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.05.2009
 */

package com.webjaguar.web.json;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.ShippingMethod;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.thirdparty.shipping.ups.UpsRateQuote;

public class JSONController extends MultiActionController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }
	
    public ModelAndView category(HttpServletRequest request, HttpServletResponse response) {
    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	Map<String, Configuration> siteConfig =  (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	
    	int cid = ServletRequestUtils.getIntParameter(request, "cid", -1);
    	int bid = ServletRequestUtils.getIntParameter(request, "bid", -1);
    	
    	Category category = this.webJaguar.getCategoryById(cid, "");
    	
    	JSONCategory jsonCat = new JSONCategory();
    	if (category != null) {
        	// sub categories
        	jsonCat.setSubcats(new ArrayList<JSONCategory>());
        	for (Category subcat: this.webJaguar.getCategoryLinks(cid, request)) {
        		JSONCategory jsonSub = new JSONCategory();
        		jsonSub.setName(subcat.getName());
        		jsonSub.setId(subcat.getId());
            	jsonCat.getSubcats().add(jsonSub);
        	}
        	// category tree
        	jsonCat.setTree(new ArrayList<JSONCategory>());
        	for (Category cat: this.webJaguar.getCategoryTree(category.getId(), true, null)) {
        		JSONCategory jsonSub = new JSONCategory();
        		jsonSub.setName(cat.getName());
        		jsonSub.setId(cat.getId());
            	jsonCat.getTree().add(jsonSub);
        	}
        	if (jsonCat.getTree().size() > 0 && jsonCat.getTree().get(0).getId() == bid) {
        		jsonCat.getTree().remove(0);
        	}
    	}
    	map.put("jsonObject", new JSONObject(jsonCat));    	
    	
    	return new ModelAndView("json/json", map);
    }
    
    public ModelAndView ups(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
    	
    	String toZip = ServletRequestUtils.getStringParameter(request, "toZip", "").trim();
    	String toStateProvince = null;
    	String toCountry = ServletRequestUtils.getStringParameter(request, "toCountry", "").trim();
    	String sku = ServletRequestUtils.getStringParameter(request, "sku", "").trim();
    	int qty = ServletRequestUtils.getIntParameter(request, "qty", 1);
    	
		List<Map <String, Object>> packages = new ArrayList<Map <String, Object>>();
    	double unknownPackageWeight = 0;	
		boolean hasUnknownPackage = false;
		int packageNumber = 0;
		Map<Integer, Double> independentPackages = new HashMap<Integer, Double>();
		LineItem lineItem = new LineItem();
		lineItem.setProduct(this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(sku), request));
		lineItem.setQuantity(qty);
		
		if (lineItem.getProduct() == null || lineItem.getProduct().getWeight() == null || lineItem.getProduct().getWeight() <= 0) {
			hasUnknownPackage = true;
		} else if (lineItem.getCubicSize() != null) {
			Map<String, Object> packageMap = new HashMap<String, Object>();
			packageMap.put("packageWeight", Math.ceil(lineItem.getPackageWeight()));
			packageMap.put("qty", lineItem.getQuantity());
			packageMap.put("length", lineItem.getProduct().getPackageL());
			packageMap.put("width", lineItem.getProduct().getPackageW());
			packageMap.put("height", lineItem.getProduct().getPackageH());
			
			packages.add(packageMap);
		} else if (lineItem.getProduct().getUpsMaxItemsInPackage() != null){	
			int seperatePackage = 0;
			if(lineItem.getProduct().getCaseContent() != null) {
			   	seperatePackage = (new Double(Math.floor(lineItem.getQuantity() * lineItem.getProduct().getCaseContent() / lineItem.getProduct().getUpsMaxItemsInPackage()))).intValue();
			} else {
				seperatePackage = (new Double(Math.floor(lineItem.getQuantity() / lineItem.getProduct().getUpsMaxItemsInPackage()))).intValue();
			}
			// packages with max quantity
			for(int i=0; i< seperatePackage ; i++) {
				independentPackages.put(packageNumber++, lineItem.getProduct().getUpsMaxItemsInPackage() * lineItem.getProduct().getWeight());
			}
			// package with remaining weight
			double remainingWeight = 0;
			if(lineItem.getProduct().getCaseContent() != null) {
				remainingWeight = ((lineItem.getQuantity()*lineItem.getProduct().getCaseContent()) - (seperatePackage * lineItem.getProduct().getUpsMaxItemsInPackage())) * lineItem.getProduct().getWeight();
			} else {
				remainingWeight = (lineItem.getQuantity() - (seperatePackage * lineItem.getProduct().getUpsMaxItemsInPackage())) * lineItem.getProduct().getWeight();
			}
			if(remainingWeight > 0) {
				independentPackages.put(packageNumber++, remainingWeight);
			}
		} else {	
			hasUnknownPackage = true;
			unknownPackageWeight += lineItem.getPackageWeight() * lineItem.getQuantity();
		}
		
		boolean addressType = false;
		if (siteConfig.get("ADDRESS_RESIDENTIAL_COMMERCIAL").getValue().equals("1")) {
			addressType = true; 
    	}
		
		double handlingTotal = 0.0;
		// handling is not easy to get due to the fact that incremental can be based on price or weight
		// option prices will then have to be considered as well as price (sale, price table, special price, qty break, etc.)
		
    	JSONObject jsonObject = new JSONObject();
    	try {
        	List<ShippingRate> rates = new ArrayList<ShippingRate>();
        	if (webJaguar.isActiveCarrier("ups") && toZip.length() > 0) {
            	UpsRateQuote upsRateQuote = new UpsRateQuote();
            	// getRates(String fromZip, String fromCountry, String userId, String password, String accessLicenceNumber, String toZip, String toCountry, double unknownPackageWeight, boolean addressType, boolean hasUnknownPackage, List<Map <String, Object>> packages, String pickupType
            	if (request.getParameter("debug") != null) {
                	System.out.println(siteConfig.get("SOURCE_ZIP").getValue()+ "," +siteConfig.get("SOURCE_STATEPROVINCE").getValue()+ ", " +siteConfig.get("SOURCE_COUNTRY").getValue()+ ", " +siteConfig.get("UPS_USERID").getValue()+ ", " +siteConfig.get("UPS_PASSWORD").getValue()+ ", " +siteConfig.get("UPS_ACCESS_LICENSE_NUMBER").getValue()+ ", " +
                			toZip+ "," +toStateProvince+ ", " +toCountry+ ", " +unknownPackageWeight+ ", " +addressType+ ", " +hasUnknownPackage+ ", " +packages+ ", " +siteConfig.get("UPS_PICKUPTYPE").getValue()+", " +siteConfig.get("UPS_SHIPPER_NUMBER"));            		
            	}

        		rates = upsRateQuote.getRates(siteConfig.get("SOURCE_ZIP").getValue(),siteConfig.get("SOURCE_STATEPROVINCE").getValue(), siteConfig.get("SOURCE_COUNTRY").getValue(), siteConfig.get("UPS_USERID").getValue(), siteConfig.get("UPS_PASSWORD").getValue(), siteConfig.get("UPS_ACCESS_LICENSE_NUMBER").getValue(), 
                			toZip, toStateProvince, toCountry, unknownPackageWeight, addressType, hasUnknownPackage, packages, independentPackages, siteConfig.get("UPS_PICKUPTYPE").getValue(), siteConfig.get("UPS_SHIPPER_NUMBER").getValue());
            	        		
        	}
        	Iterator<ShippingRate> iter = rates.iterator();
        	while (iter.hasNext()) {
        		ShippingRate shippingRate = (ShippingRate) iter.next();
        		ShippingMethod shippingMethod = webJaguar.getShippingMethodByCarrierCode(shippingRate);
        		if (shippingMethod != null && shippingMethod.getShippingActive()) {
        			shippingRate.setTitle(shippingMethod.getShippingTitle());
        			shippingRate.setPrice(roundShippingRate(Double.parseDouble(shippingRate.getPrice()) + handlingTotal).toString());
    			} else {
    				iter.remove();
    			}
        	}
        	
    		List<JSONObject> list = new ArrayList<JSONObject>();
    		// TODO this is temp we need to provide State later
    		if(!siteConfig.get("UPS_SHIPPER_NUMBER").getValue().isEmpty()){		
    			//System.out.println("Shipper");
    			JSONObject rate = new JSONObject();
        		rate.put("title", "No State");
        		rate.put("price", "0");
        		list.add(rate);
    		} else {
    			for (ShippingRate sRate: rates) {
            		JSONObject rate = new JSONObject();
            		rate.put("title", sRate.getTitle());
            		rate.put("price", sRate.getPrice());
            		list.add(rate);
            	}
    		}
        	
        	jsonObject.put("rates", list);
    	} catch (Exception e) {
    		if (request.getParameter("debug") != null) e.printStackTrace();
    	}
    	
    	map.put("jsonObject", jsonObject);
    	
    	
    	return new ModelAndView("json/json", map);
    }
    
	private Double roundShippingRate(Double shippingRateDouble) {
		int decimalPlace = 2;
		BigDecimal bd = new BigDecimal(shippingRateDouble);
	    bd = bd.setScale(decimalPlace,BigDecimal.ROUND_HALF_UP);
	    shippingRateDouble = bd.doubleValue();
	    return shippingRateDouble;
	}
}
