/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.23.2009
 */

package com.webjaguar.web.json;

public class JSONMasterSku {
	
	private int slaveCount;
	private String slaveLowestPrice;
	
	public int getSlaveCount()
	{
		return slaveCount;
	}
	public void setSlaveCount(int slaveCount)
	{
		this.slaveCount = slaveCount;
	}
	public String getSlaveLowestPrice()
	{
		return slaveLowestPrice;
	}
	public void setSlaveLowestPrice(String slaveLowestPrice)
	{
		this.slaveLowestPrice = slaveLowestPrice;
	}

}
