/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.04.2009
 */

package com.webjaguar.web.json;

import java.util.List;

public class JSONCategory {
	
    private int id;
	private String name;
	private List<JSONCategory> subcats;
	private List<JSONCategory> tree;
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public List<JSONCategory> getSubcats()
	{
		return subcats;
	}
	public void setSubcats(List<JSONCategory> subcats)
	{
		this.subcats = subcats;
	}
	public List<JSONCategory> getTree()
	{
		return tree;
	}
	public void setTree(List<JSONCategory> tree)
	{
		this.tree = tree;
	}

}
