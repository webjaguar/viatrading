package com.webjaguar.web.api;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.webjaguar.model.api.ApiCustomer;
import com.webjaguar.model.api.ApiProduct;


@JsonSerialize(include=Inclusion.NON_NULL)
public class ApiResponse implements Serializable{
	
	ApiResponse(){}
	public ApiResponse(String message) {
		this.message = message;
		
	}
	
	private ApiProduct product;
	
	private String message;
	private List<DynamicPromoResponse> dPromos;
	
	private List<SumOfDiscountResponse> listSumOfdiscount;
	private ApiCustomer customer;
	private List<ApiCustomer> customerList;
	private List<Integer> apiCustomerIdList;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	public List<DynamicPromoResponse> getdPromos() {
		return dPromos;
	}
	public void setdPromos(List<DynamicPromoResponse> dPromos) {
		this.dPromos = dPromos;
	}
	public List<SumOfDiscountResponse> getListSumOfdiscount() {
		return listSumOfdiscount;
	}
	public void setListSumOfdiscount(List<SumOfDiscountResponse> listSumOfdiscount) {
		this.listSumOfdiscount = listSumOfdiscount;
	}
	public ApiCustomer getCustomer() {
		return customer;
	}
	public void setCustomer(ApiCustomer customer) {
		this.customer = customer;
	}
	public List<ApiCustomer> getCustomerList() {
		return customerList;
	}
	public void setCustomerList(List<ApiCustomer> customerList) {
		this.customerList = customerList;
	}
	public List<Integer> getApiCustomerIdList() {
		return apiCustomerIdList;
	}
	public void setApiCustomerIdList(List<Integer> apiCustomerIdList) {
		this.apiCustomerIdList = apiCustomerIdList;
	}
	public ApiProduct getProduct() {
		return product;
	}
	public void setProduct(ApiProduct product) {
		this.product = product;
	}
	
}
