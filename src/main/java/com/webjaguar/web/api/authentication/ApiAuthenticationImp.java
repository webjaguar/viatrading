package com.webjaguar.web.api.authentication;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.webjaguar.model.Configuration;
import com.webjaguar.web.domain.Encoder;

public class ApiAuthenticationImp implements ApiAuthentication {

	public boolean validateRequest(HttpServletRequest request) {
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		final String authorization = request.getHeader("Authorization");
		
		if (authorization != null && authorization.startsWith("Basic")) {
	        try{
				// Authorization: Basic base64credentials
		        String base64Credentials = authorization.substring("Basic".length()).trim();
		        String credentials = Encoder.decode(base64Credentials);
		        if(credentials.equalsIgnoreCase(siteConfig.get("WJ_API").getValue())){
		        	return true;
		        } else {
		        	return false;
			    }
		    } catch(Exception e) {
				return false;
			}
		}
		return false;
	}

}
