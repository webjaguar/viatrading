package com.webjaguar.web.api.authentication;

import javax.servlet.http.HttpServletRequest;

public interface ApiAuthentication {
	boolean validateRequest(HttpServletRequest request);

}
