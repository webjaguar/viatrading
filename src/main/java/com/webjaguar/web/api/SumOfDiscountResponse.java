package com.webjaguar.web.api;

public class SumOfDiscountResponse {
	SumOfDiscountResponse() {
		
	}
	
	private int index;
	private String message = "Success";
	private Double sumOfDiscount = 0.0;
	
	public Double getSumOfDiscount() {
		return sumOfDiscount;
	}
	public void setSumOfDiscount(Double sumOfDiscount) {
		this.sumOfDiscount = sumOfDiscount;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	


}
