package com.webjaguar.web.api;

import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.paymentech.orbital.sdk.request.Fields.NEW_ORDER_REQUEST;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.Price;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.model.Promo;
import com.webjaguar.model.api.ApiPromo;
import com.webjaguar.model.api.ApiSumOfDiscount;
import com.webjaguar.model.api.ListApiSumOfDiscount;
import com.webjaguar.model.api.PromosApi;
import com.webjaguar.web.api.authentication.ApiAuthenticationImp;

@Controller
public class SumOfDiscountApi extends WebApplicationObjectSupport {

	@Autowired
	private WebJaguarFacade webJaguar;
	protected Map<String, Configuration> siteConfig;
	protected Map<String, Object> gSiteConfig;

	@RequestMapping(value="/api/SumOfDiscount.jhtm", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ApiResponse handelRequest(HttpServletRequest request,  HttpServletResponse response) throws Exception {
		
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		
		
		
        String message="";
        ApiResponse apiResponse = new ApiResponse();
        // authentication
        ApiAuthenticationImp authenticate = new ApiAuthenticationImp();
		if(!authenticate.validateRequest(request)) {
			message = "Invalid Username and Password.";
			response.setStatus(401);
			apiResponse.setMessage(message);
			return apiResponse;
		}
		
		ListApiSumOfDiscount listSumOfDiscount = new ListApiSumOfDiscount();

    	//Read the Json data
		
		try {
			  InputStreamReader reader = new InputStreamReader(request.getInputStream());
              JSONTokener tokenizer = new JSONTokener(reader);
              JSONObject jsonObject = new JSONObject(tokenizer);
              listSumOfDiscount = (ListApiSumOfDiscount) new ObjectMapper().readValue(jsonObject.toString(), ListApiSumOfDiscount.class);
		} catch(Exception e) {
           	message = "Error : Can't Read JSON data ";
			response.setStatus(400);
			apiResponse.setMessage(message);
			return apiResponse;
        }
		
		if(listSumOfDiscount.getListApiSumOfDiscount().size() > 100) {
			message = "Error : Please update the list in batch of 100";
			response.setStatus(400);
			apiResponse.setMessage(message);
			return apiResponse;
		}
		
		List<SumOfDiscountResponse> list = new ArrayList<SumOfDiscountResponse>();
		List<Promo> listOfPromo = new ArrayList<Promo>();
	
		for(ApiSumOfDiscount sumOfDiscountApi : listSumOfDiscount.getListApiSumOfDiscount()) {
			
                SumOfDiscountResponse sumOfDiscountResponse = new SumOfDiscountResponse();
                sumOfDiscountResponse.setIndex(sumOfDiscountApi.getIndex());

				
				Double sum = 0.0;
				String promoTitle = sumOfDiscountApi.getPromoCodeTitle();
				
				Promo promo = this.webJaguar.getPromo(promoTitle);

				if(promo != null) {
					int promoId = promo.getPromoId();
					promo = this.webJaguar.getPromoById(promoId);
					
					// check if one time use only					
					if(promo.isOnetimeUseOnly() && isUsedBefore(promoTitle)) {
						sumOfDiscountResponse.setMessage("Error! This promo is used before!");
						list.add(sumOfDiscountResponse);
						continue;
					}
					
					// check if new customer only					
					Customer customer = this.webJaguar.getCustomerById(sumOfDiscountApi.getCustomerId());
					if (promo.getpromoForNewCustomersOnly() && !isNewCustomer(customer, promo)) {
						sumOfDiscountResponse.setMessage("Error! This promo is only valid for new customer!");
						list.add(sumOfDiscountResponse);
						continue;
					}					
				}
								
			
			     DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			     
			     Customer customer = this.webJaguar.getCustomerById(sumOfDiscountApi.getCustomerId());
			   
			     
			     if(promo != null) {
				     if(promo.isOnetimeUsePerCustomer()) {
				    	 Integer customerId =  customer.getId();
				    	 if (customerId != null && promoTitle != null) {
					    	 if (isUsedBeforeByCustomer(promoTitle, customerId)) {
					    		 sumOfDiscountResponse.setMessage("Error! This promo can only be used once!");
								 list.add(sumOfDiscountResponse);
								 continue;
					    	 }
				    	 }
					}
			     }  
			     
			     // userId validation(userId from promo and userId from json object)
			     int userId = 0;
			    	  if(sumOfDiscountApi.getCustomerId() != 0) {
			    	   
				      	 userId =  sumOfDiscountApi.getCustomerId();
				     }

			     int promoUserId = 0;
			     if(promo != null) {
			    	     if(promo.getUserId() != null) {
			    	    	   
			    	    	   promoUserId =  promo.getUserId();
					  }
			     }
			    if(promoUserId == 0 ) {
			      	userId = 0;
			    }
			   

			    // ----
				if(promo != null && customer != null && promo.getDiscountType().equals("product") && promoUserId == userId  ) {
					    
					  if( ( !promo.getpromoForNewCustomersOnly() ) || ( promo.getpromoForNewCustomersOnly() && isNewCustomer(customer, promo) )) {
						    System.out.println(sdf.format(promo.getStartDate()));
						    String currentDate = sdf.format(new Date());
						    String startDate = sdf.format(promo.getStartDate());
						    String endDate = sdf.format(promo.getEndDate());
						    Date date1 = sdf.parse(currentDate);
						    Date date2 = sdf.parse(startDate);
						    Date date3 = sdf.parse(endDate);
						    if( (date1.compareTo(date2) > 0 && date3.compareTo(date1) > 0) ) {
						    	


						    	Double discount = promo.getDiscount();
								boolean isPercent = promo.isPercent();
								
								int[] productIds = sumOfDiscountApi.getProductIds();
								Map<Integer,Integer> map1 = getHashMapFromArray(productIds);
								System.out.println(promo.getDiscountType() + "----- ");
								  Set<Object> set = promo.getSkuSet();
								  System.out.println(set + "-----skuSet");
								  
								  Set<Object> categoryIdSet = promo.getCategoryIdSet();
								  System.out.println(categoryIdSet + "-----categoryIdSet ");
								  
								for(int i = 0; i < productIds.length; i++) {
									
//									Product product = this.webJaguar.getProductById(productIds[i], request);
									Product product = this.webJaguar.getProductById( productIds[i], 0, false, null);
									
									    if(product == null) {
									    	sumOfDiscountResponse.setMessage("Error! productId == "+ productIds[i] +" at rank " + i   +" is invalid!");
									    	sum = 0.0;
									    	break;
									    }
									    System.out.println(product.getSku() + "----- ");
									    if(set == null && categoryIdSet == null)  {											 
											continue;
										} else {
											if (!set.contains(product.getSku()) && childHasParent(promo, product) == false && childHasParentCategory(promo, product) == false) {												
												continue;
											}
										}		
									    									    							  
										Double price = product.getPrice1();
																			
										// checking Minimum Quantity
										int minQt = 1;
										Promo promo1 = this.webJaguar.getPromo(promoTitle);
										
										if( promo1.getMinQty() == null) {
											
										} else {
											minQt = promo1.getMinQty();
										}
											
										double minOrderAmt = 0.0;
										if( promo.getMinOrder() != null) {
											minOrderAmt = promo.getMinOrder();
										} 	
										
										System.out.println(product.getPrice1() * map1.get(productIds[i])  + "lineItem min order amount" );
										
										if(map1.get(productIds[i]) >= minQt && product.getPrice1() * map1.get(productIds[i]) >= minOrderAmt) {
											map1.put(productIds[i], 0);
											if(isPercent) {
												Double percent = (discount * price) / 100;
												sum = sum + percent;
											} else {
												if(price >= discount) {
													sum = sum + discount;
												}
											}
										} else {
											sumOfDiscountResponse.setMessage("Error! check Promo minimum order amount and quantity");
										}
								}
								sumOfDiscountResponse.setSumOfDiscount(sum);
								
						    } else {
						    	sumOfDiscountResponse.setMessage("Error! check Promo startDate and EndDate!");
						    } // checking dates
					  } else {
						  sumOfDiscountResponse.setMessage("Error! this promo is for new cutomer only");
					  } // checking new customer
					   
				} else if(promo != null && customer != null && promo.getDiscountType().equals("order") && promoUserId == userId  ) {
					  if(( !promo.getpromoForNewCustomersOnly() ) || ( promo.getpromoForNewCustomersOnly() && isNewCustomer(customer, promo) )) {						  
						  if(promo.isInEffect()) {
							  	Double discount = promo.getDiscount();
								boolean isPercent = promo.isPercent();
								
								Integer customerId = sumOfDiscountApi.getCustomerId();
								
								
								// checking Minimum Order 
								double minOrderAmt = 0.0;
								if( promo.getMinOrder() == null) {
									
								} else {
									minOrderAmt = promo.getMinOrder();
								}
								
								sum = 0.0;								
								double subTotal = sumOfDiscountApi.getSubTotal();
								if (subTotal > minOrderAmt) {
									if (isPercent) {
										double discountAmt = (subTotal * discount) / 100;
										sum = sum + discountAmt;
									} else {
										if (subTotal > discount) {
											sum = sum + discount;
										}										
									}
								}	
								sumOfDiscountResponse.setSumOfDiscount(sum);
							}  
					  } else {
						  sumOfDiscountResponse.setMessage("Error! this promo is for new cutomer only");
					  }
				} else {
										
					if(customer == null) {
						sumOfDiscountResponse.setMessage("Error! customer doesn't exist");
					}else if(promo == null) {
						sumOfDiscountResponse.setMessage("Error! promoTitle doesn't exist");
					}else if(promoUserId != userId ) {
			    		message = "Error : userId doesn't match!";
						sumOfDiscountResponse.setMessage(message);
			     }	
			}
				list.add(sumOfDiscountResponse);
		}
		try {
			
		    }catch(Exception e) {
		  }
		
		apiResponse.setListSumOfdiscount(list);

		return apiResponse;
	}
	
	private boolean isNewCustomer(Customer customer, Promo promo) {
		
		if(promo.getpromoForNewCustomersOnly() ) {
			OrderSearch OS = new OrderSearch();
			OS.setUserId(customer.getId());
			List<Order> list = this.webJaguar.getOrdersList(OS);
			if(list.size() >= 1 ) {
				Iterator<Order> itr = list.iterator();
				while(itr.hasNext()) {
					Order OD = itr.next();
					if(!OD.getStatus().equals("x")) {						
						return false;					
					}
			     }	
			}
		}
		return true;
	}
	
	private Map getHashMapFromArray(int[] input) {
		
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		int count = 0;
		for(int i = 0; i < input.length; i++) {
			if(map.containsKey(input[i])) {
			    count = map.get(input[i]);
				map.put(input[i], count + 1 );
			} else {
				map.put(input[i], 1);
			}
		}
		
		return map;
	}
	
	// check if this child sku has a parentSkus 
	private boolean childHasParent(Promo promo, Product product) {
		Set<Object> set = promo.getParentSkuSet();
		Iterator<Object> itr = set.iterator();
		while(itr.hasNext()) {
			String next = (String) itr.next();
			 if(product.getSku().equalsIgnoreCase(next)) {
	    		   return true;
	    	   }
			ProductSearch search = new ProductSearch();
			
			search.setParentSku(next.toString());
	       List<Product> list =  this.webJaguar.getProductList(search);
	       Iterator<Product> itr2 = list.iterator();
	       while(itr2.hasNext()) {
	    	   Product next2 = itr2.next();
	    	   if(product.getSku().equalsIgnoreCase(next2.getSku())) {
	    		   return true;
	    	     }
	       }
		}
		
		return false;
	}
	
	// check if this child sku is in a parent category
	private boolean childHasParentCategory(Promo promo, Product product) {
		Set<Object> promoCategoryIdset = promo.getCategoryIdSet();
		
		Set<Object> parentCategoryIdSet = product.getCatIds();
		Iterator<Object> itr = parentCategoryIdSet.iterator();
		while (itr.hasNext()) {
			Object next = itr.next();
			if (promoCategoryIdset.contains(next.toString())) {
				return true;
			}
		}
		return false;
	}
	
	private boolean isUsedBefore(String promoCode) {
		
          int count = this.webJaguar.isThereAnOrderUseThisPromo(promoCode);
            
          if(count >= 1) {
        	     return true;
          }
          return false;
	}
	
	private boolean isUsedBeforeByCustomer(String promoCode, Integer customerId) {
		
        int count = this.webJaguar.isThereAnOrderUseThisPromoByCustomer(promoCode, customerId);
        
        
        if(count >= 1) {
      	     return true;
        }
        return false;
	}

}
