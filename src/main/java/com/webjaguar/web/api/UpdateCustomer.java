package com.webjaguar.web.api;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.model.api.ApiAddress;
import com.webjaguar.model.api.ApiCustomer;
import com.webjaguar.web.api.authentication.ApiAuthenticationImp;

@Controller
public class UpdateCustomer {
	@Autowired
	private WebJaguarFacade webJaguar;
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	@RequestMapping(value="/api/updateCustomer.jhtm", method = RequestMethod.POST)

	 public @ResponseBody ApiResponse handelRequest(HttpServletRequest request,  HttpServletResponse response) throws Exception{
		siteConfig = this.webJaguar.getSiteConfig();
		ApiResponse apiResponse = new ApiResponse();
		
		ApiAuthenticationImp authenticate = new ApiAuthenticationImp();
		if(!authenticate.validateRequest(request)) {
			apiResponse.setMessage("Invalid Username and Password.");
			response.setStatus(401);
			return apiResponse;
		}
		ApiCustomer apiCustomer = new ApiCustomer();
		
		//Read the Json data
		try{
			InputStreamReader reader = new InputStreamReader(request.getInputStream());
	        JSONTokener tokenizer = new JSONTokener(reader);
	        JSONObject jsonObject = new JSONObject(tokenizer);
	        Object mapper = new ObjectMapper();
	        apiCustomer = ((ObjectMapper) mapper).readValue(jsonObject.toString(), ApiCustomer.class);
		} catch(Exception e){
			System.out.println(e);
			apiResponse.setMessage("Error : Can't Read JSON data");
			response.setStatus(400);
			return apiResponse;
		}
		
		Customer wjCustomer = new Customer();
		if(apiCustomer.getId() != null && !apiCustomer.getId().equals("")){
			System.out.println("get by id");
			wjCustomer = this.webJaguar.getCustomerById(apiCustomer.getId());
		}else{
			System.out.println("get by email");
			 wjCustomer = this.webJaguar.getCustomerByUsername(apiCustomer.getUsername());
		}
        
        
        if(wjCustomer != null){
	        wjCustomer.setHost((apiCustomer.getHost()!=null) ? apiCustomer.getHost() : wjCustomer.getHost());
	        wjCustomer.setExtraEmail((apiCustomer.getExtraEmail()!=null) ? apiCustomer.getExtraEmail() : wjCustomer.getExtraEmail());
	        wjCustomer.setPayment((apiCustomer.getPayment()!=null) ? apiCustomer.getPayment() : wjCustomer.getPayment());
	        wjCustomer.setCredit((apiCustomer.getCredit()!=null) ? apiCustomer.getCredit() : wjCustomer.getCredit());
	        wjCustomer.setNote((apiCustomer.getNote()!=null) ? apiCustomer.getNote() : wjCustomer.getNote());
	        wjCustomer.setAccountNumber((apiCustomer.getAccountNumber()!=null) ? apiCustomer.getAccountNumber() : wjCustomer.getAccountNumber());
	        if (apiCustomer.getPassword()!=null && !apiCustomer.getPassword().trim().isEmpty()) {
	        	wjCustomer.setPassword( apiCustomer.getPassword());
	        }
	        
	        // languageCode
	        if(apiCustomer.getLanguage() != null) {
	          	if(apiCustomer.getLanguage() == LanguageCode.es) {
	          		System.out.println(apiCustomer.getLanguage() + "{{{}}}");
	          		wjCustomer.setLanguageCode(LanguageCode.es);
	          	}else {
	          		wjCustomer.setLanguageCode(LanguageCode.en);
	          	}
		      }
	        
	        // update fields
	        if(apiCustomer.getField1() != null) {
	         wjCustomer.setField1(apiCustomer.getField1()); 
	        }
       
	        if(apiCustomer.getField2() != null) {
           	wjCustomer.setField2(apiCustomer.getField2()); 
	        }
	        if(apiCustomer.getField3() != null) {
	        	wjCustomer.setField3(apiCustomer.getField3()); 
	        }
	        if(apiCustomer.getField4() != null) {
	        	wjCustomer.setField4(apiCustomer.getField4()); 
	        }
	        if(apiCustomer.getField5() != null) {
	        wjCustomer.setField5(apiCustomer.getField5()); 
	        }
	        if(apiCustomer.getField6() != null) {
	        wjCustomer.setField6(apiCustomer.getField6()); 
	        }
	        if(apiCustomer.getField7() != null) {
	        wjCustomer.setField7(apiCustomer.getField7()); 
	        }
	        if(apiCustomer.getField8() != null) {
	        wjCustomer.setField8(apiCustomer.getField8()); 
	        }
	        if(apiCustomer.getField9() != null) {
	        wjCustomer.setField9(apiCustomer.getField9()); 
	        }
	        if(apiCustomer.getField10() != null) {
	        wjCustomer.setField10(apiCustomer.getField10());
	        }
	        if(apiCustomer.getField11() != null) {
        		wjCustomer.setField11(apiCustomer.getField11()); 
	        }
	        if(apiCustomer.getField12() != null) {
	        	wjCustomer.setField12(apiCustomer.getField12()); 
	        }
	        if(apiCustomer.getField13() != null) {
	        wjCustomer.setField13(apiCustomer.getField13()); 
	        }
	        if(apiCustomer.getField14() != null) {
	        wjCustomer.setField14(apiCustomer.getField14()); 
	        }
	        if(apiCustomer.getField15() != null) {
	        wjCustomer.setField15(apiCustomer.getField15());
	        }
	        if(apiCustomer.getField16() != null) {
        		wjCustomer.setField16(apiCustomer.getField16()); 
	        }
	        if(apiCustomer.getField17() != null) {
	        	wjCustomer.setField17(apiCustomer.getField17()); 
	        }
	        if(apiCustomer.getField18() != null) {
	        	wjCustomer.setField18(apiCustomer.getField18()); 
	        }
	        if(apiCustomer.getField19() != null) {
	        	wjCustomer.setField19(apiCustomer.getField19()); 
	        }
	        if(apiCustomer.getField20() != null) {
	        	wjCustomer.setField20(apiCustomer.getField20());
	        }
	        if(apiCustomer.getField21() != null) {
	        	wjCustomer.setField21(apiCustomer.getField21());
	        }
	        if(apiCustomer.getField22() != null) {
	        	wjCustomer.setField22(apiCustomer.getField22());
	        }
	        if(apiCustomer.getField23() != null) {
	        	wjCustomer.setField23(apiCustomer.getField23());
	        }
	        if(apiCustomer.getField24() != null) {
	        	wjCustomer.setField24(apiCustomer.getField24());
	        }
	        if(apiCustomer.getField25() != null) {
	        	wjCustomer.setField25(apiCustomer.getField25());
	        }
	        if(apiCustomer.getField26() != null) {
	        	wjCustomer.setField26(apiCustomer.getField26());
	        }
	        if(apiCustomer.getField27() != null) {
	        	wjCustomer.setField27(apiCustomer.getField27());
	        }
	        if(apiCustomer.getField28() != null) {
	        	wjCustomer.setField28(apiCustomer.getField28());
	        }
	        if(apiCustomer.getField29() != null) {
	        	wjCustomer.setField29(apiCustomer.getField29());
	        }
	        if(apiCustomer.getField30() != null) {
	        	wjCustomer.setField30(apiCustomer.getField30());
	        }
	        
	        
	        if(apiCustomer.isEmailNotify() != null) {
	        	 	wjCustomer.setEmailNotify(apiCustomer.isEmailNotify());
	        }
	        if(apiCustomer.isUnsubscribe() != null) {
	        		wjCustomer.setUnsubscribe(apiCustomer.isUnsubscribe());
	        }
	        if(apiCustomer.getRating1() != null) {
	        		wjCustomer.setRating1(apiCustomer.getRating1());
	        }
	        if(apiCustomer.getRating2() != null) {
	        		wjCustomer.setRating2(apiCustomer.getRating2());
	        }
	        if(apiCustomer.getLanguageField() != null) {
	        		wjCustomer.setLanguage(apiCustomer.getLanguageField());
	        }
	        if(apiCustomer.getWebsiteUrl() != null) {
	        		wjCustomer.setWebsiteUrl(apiCustomer.getWebsiteUrl());
	        }
	        if(apiCustomer.getEbayName() != null) {
	        		wjCustomer.setEbayName(apiCustomer.getEbayName());
	        }
	        if(apiCustomer.getAmazonName() != null) {
	        		wjCustomer.setAmazonName(apiCustomer.getAmazonName());
	        }
	        if(apiCustomer.getUnsubscribeReason() != null) {
	        		wjCustomer.setUnsubscribeReason(apiCustomer.getUnsubscribeReason());
	        }

	        if(apiCustomer.isTextMessageNotify() != null) {
	        		wjCustomer.setTextMessageNotify(apiCustomer.isTextMessageNotify());
	        } 
	        
	        if(apiCustomer.getWhatsappNotify() != null) {
        		wjCustomer.setWhatsappNotify(apiCustomer.getWhatsappNotify());
	        } 
	        
	        if(apiCustomer.isUnsubscribeTextMessage() != null) {
	        		wjCustomer.setUnsubscribeTextMessage(apiCustomer.isUnsubscribeTextMessage());
	        }
	        if(apiCustomer.getTrackcode() != null) {
	        		wjCustomer.setTrackcode(apiCustomer.getTrackcode());
	        }
	        if(apiCustomer.getTaxId() != null) {
	        wjCustomer.setTaxId(apiCustomer.getTaxId());
	        }
	        if(apiCustomer.getSalesRepId() != null) {
	        wjCustomer.setSalesRepId(apiCustomer.getSalesRepId());
	        }
	        
	        // registeredby
	        if(apiCustomer.getRegisteredBy() != null) {
		         wjCustomer.setRegisteredBy(apiCustomer.getRegisteredBy());
		       }	        
	        
			//Set Address
	        if(apiCustomer.getApiAddress() != null){
	        	wjCustomer.setAddress(this.setAddress(wjCustomer.getAddress(), apiCustomer.getApiAddress()));
            }
	        
	        //set list of addresses		
	        if(apiCustomer.getApiAddressList() != null) {
	    		for(ApiAddress apiAddr : apiCustomer.getApiAddressList()){				
					Address wjAddress = this.webJaguar.getAddressById(apiAddr.getId());
						if(!wjAddress.equals(null)){					
							wjAddress = this.setAddress(wjAddress, apiAddr);						
							this.webJaguar.updateAddress(wjAddress);
							if(wjAddress.isPrimary()){
								wjCustomer.setAddress(wjAddress);
							}				      
						} else {
							apiResponse.setMessage("Error : Address not found");
			        		response.setStatus(400);
			        		return apiResponse;
					    }
				}
	        }   
        } else {
        	apiResponse.setMessage("Error : Customer not found");
        	response.setStatus(400);
			return apiResponse;
        }
        try{
        	 if (apiCustomer.getPassword()!=null && !apiCustomer.getPassword().trim().isEmpty()) {
        		 this.webJaguar.updateCustomer(wjCustomer, false, true);
        	 } else {
        		 this.webJaguar.updateCustomer(wjCustomer, false, false);
        	 }        	
           apiResponse.setMessage("Success");
           response.setStatus(200);
        } catch(Exception e){
        	e.printStackTrace();
        	apiResponse.setMessage("Error : Exception in UPDATE");
        	response.setStatus(400);
        }
		return apiResponse;
	 }
	
	public Address setAddress(Address wjAddress, ApiAddress apiAddr){
		wjAddress.setFirstName(apiAddr.getFirstName() != null ?  apiAddr.getFirstName() : wjAddress.getFirstName());
		wjAddress.setLastName(apiAddr.getLastName() != null ? apiAddr.getLastName() : wjAddress.getLastName());
		wjAddress.setCompany(apiAddr.getCompany() != null ? apiAddr.getCompany() : wjAddress.getCompany());
		wjAddress.setAddr1(apiAddr.getAddr1() != null? apiAddr.getAddr1() : wjAddress.getAddr1());	
		wjAddress.setAddr2(apiAddr.getAddr2() != null? apiAddr.getAddr2() : wjAddress.getAddr2());
		wjAddress.setCity(apiAddr.getCity() != null ? apiAddr.getCity() : wjAddress.getCity());
		wjAddress.setStateProvince(apiAddr.getStateProvince() != null ? apiAddr.getStateProvince() : wjAddress.getStateProvince());
		wjAddress.setZip(apiAddr.getZip() != null ? apiAddr.getZip() : wjAddress.getZip());
		wjAddress.setCountry(apiAddr.getCountry() != null ? apiAddr.getCountry() : wjAddress.getCountry());
		wjAddress.setPhone(apiAddr.getPhone()!= null ? apiAddr.getPhone() : wjAddress.getPhone());
		wjAddress.setCellPhone(apiAddr.getCellPhone() != null ? apiAddr.getCellPhone() : wjAddress.getCellPhone());
		wjAddress.setFax(apiAddr.getFax() != null ? apiAddr.getFax() : wjAddress.getFax());					
		wjAddress.setCode(apiAddr.getCode()!= null ? apiAddr.getCode() : wjAddress.getCode());
		wjAddress.setEmail(apiAddr.getEmail() != null ? apiAddr.getEmail() : wjAddress.getEmail());
		wjAddress.setResidential(apiAddr.isResidential() && apiAddr.isResidential() == true ? true : false);
		wjAddress.setLiftGate(apiAddr.isLiftGate()  ? apiAddr.isLiftGate() : false);
		return wjAddress;
	}
}
