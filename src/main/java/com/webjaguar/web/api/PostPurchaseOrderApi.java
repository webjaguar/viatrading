package com.webjaguar.web.api;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Order;
import com.webjaguar.model.Product;
import com.webjaguar.model.PurchaseOrder;
import com.webjaguar.model.PurchaseOrderLineItem;
import com.webjaguar.model.api.OrderApi;
import com.webjaguar.web.form.PurchaseOrderForm;


@Controller
public class PostPurchaseOrderApi extends WebApplicationObjectSupport{	
	
	@Autowired
	private WebJaguarFacade webJaguar;
	protected Map<String, Configuration> siteConfig;
	protected Map<String, Object> gSiteConfig;
	@RequestMapping(value="/api/postPurchaseOrder.jhtm", method = RequestMethod.POST)
    public @ResponseBody String PurchaseOrderApi (HttpServletRequest request,  HttpServletResponse response) throws Exception{
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		String apiResponse = "PO successfully placed";
		try{
		    InputStreamReader reader = new InputStreamReader(request.getInputStream());
	        JSONTokener tokenizer = new JSONTokener(reader);
	        JSONObject jsonObject = new JSONObject(tokenizer);
	      
	        PurchaseOrder PurchaseOrder = new ObjectMapper().readValue(jsonObject.toString(), PurchaseOrder.class);
	        PurchaseOrderForm purchaseOrderForm = new PurchaseOrderForm();
	        AtomicInteger lineNumber = new AtomicInteger(1);
	        PurchaseOrder.setStatus("pend");
	 
	    	purchaseOrderForm.setPurchaseOrder(PurchaseOrder);
	    	
	    	 for(PurchaseOrderLineItem item : purchaseOrderForm.getPurchaseOrder().getPoLineItems()){ 
	    		 System.out.println("---lineItems--");
	    		 Product product =  this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(item.getSku()), 0, true, null);
	    		 item.setProductName(product.getName());
	    		 item.setLineNumber(lineNumber.getAndIncrement());	     				 
	    	 }    		
	    	
	    	 this.webJaguar.insertPurchaseOrder(purchaseOrderForm);
	    	 System.out.println("---Inserted--");
		}
		catch (Exception e){
			 System.out.println("---failed--");
			 apiResponse = "PO failed";
			 e.printStackTrace();
		}
		return apiResponse;
}
}