package com.webjaguar.web.api;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.record.formula.functions.And;
import org.apache.tools.ant.taskdefs.Tstamp.Unit;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cete.dynamicpdf.text.ad;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ViaDeal;
import com.webjaguar.model.api.ApiCartItem;
import com.webjaguar.model.api.ApiCrmContact;
import com.webjaguar.model.api.ListApiCartItem;
import com.webjaguar.thirdparty.cXml.cXmlAPI.Total;
import com.webjaguar.web.api.authentication.ApiAuthenticationImp;

@Controller
public class viewCartApi {

	@Autowired
	private WebJaguarFacade webJaguar;
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	@RequestMapping(value="/api/viewCart.jhtm", method = RequestMethod.POST, produces = {"application/json"})
	 public @ResponseBody ApiViewCartResponse handelRequest(HttpServletRequest request,  HttpServletResponse response) throws Exception{
		
		siteConfig = this.webJaguar.getSiteConfig();
		String message="";
		ApiViewCartResponse apiViewCartResponse = new ApiViewCartResponse();
		ApiAuthenticationImp authenticate = new ApiAuthenticationImp();
		if(!authenticate.validateRequest(request)) {
			apiViewCartResponse.setMessage("Invalid Username and Password.");
			response.setStatus(401);
			return apiViewCartResponse;
		}
		
		ListApiCartItem listApiCartItem = new ListApiCartItem();
//		Read the JSON data
		try {
			InputStreamReader reader = new InputStreamReader(request.getInputStream());
            JSONTokener tokenizer = new JSONTokener(reader);
            JSONObject jsonObject = new JSONObject(tokenizer);  
            listApiCartItem = (ListApiCartItem) new ObjectMapper().readValue(jsonObject.toString(), ListApiCartItem.class);
		} catch(Exception e) {
         	message = "Error : Can't Read JSON data ";
			response.setStatus(400);
			apiViewCartResponse.setMessage(message);
			e.printStackTrace();
			return apiViewCartResponse;
      }
		List<ViewCartResponse> list = new ArrayList<ViewCartResponse>();		
		Map<String, Integer> parentSkuTotalQtyMap = new HashMap<String, Integer>();
		Map<String, Double> parentSkuTotalAmtMap = new HashMap<String, Double>();
		
		for(ApiCartItem apiLineItem : listApiCartItem.getListApiCartItem()) {
			Integer productId = apiLineItem.getProduct().getId();
			Product product = this.webJaguar.getProductById(productId, 0, true, null);
			String parentSku = product.getMasterSku();
			
			if (parentSku != null && !parentSku.isEmpty() && parentSkuTotalQtyMap.containsKey(parentSku)) {
				parentSkuTotalQtyMap.replace(parentSku, parentSkuTotalQtyMap.get(parentSku) + apiLineItem.getQuantity());
				parentSkuTotalAmtMap.replace(parentSku, parentSkuTotalAmtMap.get(parentSku) + product.getPrice1() * apiLineItem.getQuantity());
			} else {
				parentSkuTotalQtyMap.put(parentSku,  apiLineItem.getQuantity());
				parentSkuTotalAmtMap.put(parentSku, product.getPrice1() * apiLineItem.getQuantity());
			}	
		}
		
		for (ApiCartItem apiCartItem : listApiCartItem.getListApiCartItem()) {
			Integer productId = apiCartItem.getProduct().getId();
			Product product = this.webJaguar.getProductById(productId, 0, true, null);
			String parentSku = product.getMasterSku();
			
			ViewCartResponse viewCartResponse = new ViewCartResponse();
			viewCartResponse.setProductId(apiCartItem.getProduct().getId());
			viewCartResponse.setLineItemTotal(apiCartItem.getTotalPrice());
			if (apiCartItem.getDiscount() != null) {
				if (apiCartItem.isPercent()) {
					viewCartResponse.setDiscount((apiCartItem.getDiscount() / 100.00) * product.getPrice1() * apiCartItem.getQuantity());
				} else {
					viewCartResponse.setDiscount(apiCartItem.getDiscount() * apiCartItem.getQuantity());
				}
			} else {
				viewCartResponse.setDiscount(0.0);
			}
			
			ViaDeal viaDeal = this.webJaguar.getViaDealBySku(parentSku);
			if (viaDeal != null && viaDeal.isTriggerType()) {
// 				triggerType is add up quantity
				if (parentSku != null && !parentSku.isEmpty() && parentSkuTotalQtyMap.containsKey(parentSku) && parentSkuTotalQtyMap.get(parentSku) >= viaDeal.getTriggerThreshold() ) {
					if (viaDeal.isDiscountType()) {
						//discount type is percent
						Double discount =  viaDeal.getDiscount();
						Double discountAmountPerCartItem = product.getPrice1() * (discount / 100.00);
//						totoal discount
						viewCartResponse.setDiscount(discountAmountPerCartItem * apiCartItem.getQuantity());					
						viewCartResponse.setLineItemTotal(apiCartItem.getTotalPrice() - discountAmountPerCartItem * apiCartItem.getQuantity());
					} else {
//						discount type is fixed amount
//						total discount
						viewCartResponse.setDiscount(viaDeal.getDiscount() * apiCartItem.getQuantity());
						viewCartResponse.setLineItemTotal(apiCartItem.getTotalPrice() - viaDeal.getDiscount() * apiCartItem.getQuantity());
					}	
				}
			} else if (viaDeal != null && !viaDeal.isTriggerType()) {
// 				triggerType is add up price
				if (parentSku != null && !parentSku.isEmpty() && parentSkuTotalQtyMap.containsKey(parentSku) && parentSkuTotalAmtMap.get(parentSku) >= viaDeal.getTriggerThreshold() ) {				
					if (viaDeal.isDiscountType()) {
//						discount type is percent
						Double discount =  viaDeal.getDiscount();
						Double discountAmountPerCartItem = product.getPrice1() * (discount / 100.00);
//						total discount
						viewCartResponse.setDiscount(discountAmountPerCartItem * apiCartItem.getQuantity());			
						viewCartResponse.setLineItemTotal(apiCartItem.getTotalPrice() - discountAmountPerCartItem * apiCartItem.getQuantity());
					} else {
//					discount type is fixed amount
//						total discount
						viewCartResponse.setDiscount(viaDeal.getDiscount() * apiCartItem.getQuantity());							
						viewCartResponse.setLineItemTotal(apiCartItem.getTotalPrice() - viaDeal.getDiscount() * apiCartItem.getQuantity());
					}	
				}
			}
			list.add(viewCartResponse);
		}
		apiViewCartResponse.setListViewCartItem(list);
		apiViewCartResponse.setMessage("viewCartApi finished successfully");
		return apiViewCartResponse;
	}
}
