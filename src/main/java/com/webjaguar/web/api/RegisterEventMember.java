package com.webjaguar.web.api;

import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.EventMember;
import com.webjaguar.model.api.ApiEventMember;
import com.webjaguar.model.api.CartApi;

@Controller
public class RegisterEventMember  {
	
	@Autowired
	private WebJaguarFacade webJaguar;
	
	@RequestMapping(value="/api/registerEvent.jhtm", method = RequestMethod.POST)

    public @ResponseBody String handelRequest(HttpServletRequest request,  HttpServletResponse response) throws Exception {
		
    	InputStreamReader reader = new InputStreamReader(request.getInputStream());
        JSONTokener tokenizer = new JSONTokener(reader);
        JSONObject jsonObject = new JSONObject(tokenizer);
        ApiEventMember newMember = new ApiEventMember();
        response.setHeader("Access-Control-Allow-Origin", "*");
        try {
        	 newMember  = new ObjectMapper().readValue(jsonObject.toString(), ApiEventMember.class); 
        } catch (Exception e) {
        	e.printStackTrace();
        	response.setStatus(400);
        	return "Error reading JSON data";
        }

        if (newMember.getEventId()==null || newMember.getEventId().equals("")  || newMember.getUserId()==null || newMember.getUserId().equals("")) {
        	response.setStatus(400);
        	return "EventId or UserId is missing";
        	
        }
        
        
        Customer customer  = this.webJaguar.getCustomerById(newMember.getUserId());
        
        if (customer==null) {
        	response.setStatus(400);
        	return "Customer not found";
        }
        
        if (this.webJaguar.getEventById(newMember.getEventId())==null) {
        	response.setStatus(400);
        	return "Event not found";
        }
        
        
    	EventMember member = new EventMember();
		member.setUserId(customer.getId());
		member.setEventId(newMember.getEventId());
		member.setUsername(customer.getUsername());
		member.setFirstName(customer.getAddress().getFirstName());
		member.setLastName(customer.getAddress().getLastName());
		member.setCardId(customer.getCardId());
		member.setSalesRepId(customer.getSalesRepId());
		//member.setHtmlCode(eventForm.getEvent().getHtmlCode());
		try {
		this.webJaguar.insertEventMember(member);
			return "User successfully registered to the event";
		} catch (DataIntegrityViolationException ex) {
			return "You are already registered to this event.";
		}
	

    }
	

}
