package com.webjaguar.web.api;

import java.io.InputStreamReader;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Order;
import com.webjaguar.model.api.OrderApi;
import com.webjaguar.web.api.authentication.ApiAuthenticationImp;

@Controller
public class postPaymentMethodApi extends WebApplicationObjectSupport {
	
	@Autowired
	private WebJaguarFacade webJaguar;
	protected Map<String, Configuration> siteConfig;
	protected Map<String, Object> gSiteConfig;
	

	@RequestMapping(value="/api/postPaymentMethod.jhtm", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ApiResponse handelRequest(HttpServletRequest request,  HttpServletResponse response) throws Exception{
				
	    InputStreamReader reader = new InputStreamReader(request.getInputStream());
        JSONTokener tokenizer = new JSONTokener(reader);
        JSONObject jsonObject = new JSONObject(tokenizer);
        
        OrderApi OrderApi = new ObjectMapper().readValue(jsonObject.toString(), OrderApi.class);
    
        String message = "";
        ApiResponse apiResponse = new ApiResponse();
        // authentication
        ApiAuthenticationImp authenticate = new ApiAuthenticationImp();
		if(!authenticate.validateRequest(request)) {
			message = "Invalid Username and Password.";
			response.setStatus(401);
			apiResponse.setMessage(message);
			return apiResponse;
		}
        
		if(OrderApi.getOrderId() == null ) {
			message = "Order Id can't be empty";
			apiResponse.setMessage(message);
			return apiResponse;
		}
      
        try {
            Order order = this.webJaguar.getOrder(OrderApi.getOrderId(), null);
            if (order != null) {
                order.setPaymentMethod(OrderApi.getPayMethod());
                this.webJaguar.updateOrder(order);
                apiResponse.setMessage("Upder order paymtent finished, order id is " + order.getOrderId());
            } else {
            	apiResponse.setMessage("Order id not found, payment method update failed");
            }
		} catch (Exception e) {
			// TODO: handle exception
        	e.printStackTrace();
        	apiResponse.setMessage("Update payment method failed");
        	return apiResponse;
		}
        return apiResponse;
	}
	
}
