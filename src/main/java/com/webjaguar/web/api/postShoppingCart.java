package com.webjaguar.web.api;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.api.CartApi;
import com.webjaguar.model.api.OrderApi;

@Controller
public class postShoppingCart extends WebApplicationObjectSupport{
	
	@Autowired
	private WebJaguarFacade webJaguar;
	
	@RequestMapping(value="/api/AddToCart.jhtm", method = RequestMethod.POST)
    public @ResponseBody String handelRequest(HttpServletRequest request,  HttpServletResponse response) throws Exception{
	
			CartItem cartItem = new CartItem();
			List<ProductAttribute> Attributes = new ArrayList<ProductAttribute>();
			ProductAttribute attr = new ProductAttribute();
			InputStreamReader reader = new InputStreamReader(request.getInputStream());
	        JSONTokener tokenizer = new JSONTokener(reader);
	        JSONObject jsonObject = new JSONObject(tokenizer);
	       
	        CartApi cartApi  = new ObjectMapper().readValue(jsonObject.toString(), CartApi.class); 
	        try{
	        	//this.webJaguar.getProductById(cartApi.getProductId(), request);
		        cartItem.setProduct(this.webJaguar.getProductById(cartApi.getProductId(), request));
		        cartItem.setQuantity(cartApi.getQty());
		        cartItem.setUserId(cartApi.getUserId());
		        cartItem.setIncludeRetailDisplay(false);
		        cartItem.setApiIndex((cartApi.getApiIndex().trim()));
		        
		        //Viatrading will be having only one option per CartItem
		        if((cartApi.getOptionName()!= null && cartApi.getOptionName() !="") && (cartApi.getValueName()!= null && cartApi.getValueName() !="")
		        		&& (cartApi.getValueIndex()!= null && !cartApi.getValueIndex().equals(""))) {		        	
		        	attr.setOptionName(cartApi.getOptionName());
		        	attr.setValueName(cartApi.getValueName());
		        	attr.setValueIndex(cartApi.getValueIndex());
		        	Attributes.add(attr);
		        	cartItem.setProductAttributes(Attributes);

		        }
		        this.webJaguar.addToCart(cartItem, cartApi.getUserId());

	        } catch(Exception e){
	        	e.printStackTrace();
	            return "Insert Failed";
	        }
	        
	        Cart cart = this.webJaguar.getUserCart(cartApi.getUserId(), null); 
            this.webJaguar.updateCart(cart, request);	      
            Double tempCartTotal = cart.getSubTotal();
			for(CartItem item: cart.getCartItems()) {
				if(tempCartTotal != null && item.getTempCartTotal() != null && !tempCartTotal.equals(item.getTempCartTotal())) {
					item.setTempCartTotal(tempCartTotal);
					this.webJaguar.updateCartItem(item);
					
				}
			}
			
	        return "Insert Successful";
	}
	
	@RequestMapping(value="/api/UpdateCart.jhtm", method = RequestMethod.POST)
    public @ResponseBody String UpdateCart(HttpServletRequest request,  HttpServletResponse response) throws Exception{
	
			CartItem cartItem = new CartItem();
			InputStreamReader reader = new InputStreamReader(request.getInputStream());
	        JSONTokener tokenizer = new JSONTokener(reader);
	        JSONObject jsonObject = new JSONObject(tokenizer);
	       
	        CartApi cartApi  = new ObjectMapper().readValue(jsonObject.toString(), CartApi.class); 
	        try{
	        	if(cartApi.getApiIndex()== ""){
	        		cartApi.setApiIndex(null);

	        	}
	        	cartItem.setApiIndex(cartApi.getApiIndex());
	            cartItem.setQuantity(cartApi.getQty());
	            cartItem.setUserId(cartApi.getUserId());
	            
           
                this.webJaguar.updateCartItemByApiIndex(cartItem, cartApi.getProductId());   
	        }catch(Exception e){
	        	e.printStackTrace();
	    		return "Update Failed";
	        }
	        
	        Cart cart = this.webJaguar.getUserCart(cartApi.getUserId(), null); 
            this.webJaguar.updateCart(cart, (Map<String, Object>) request.getAttribute("gSiteConfig"), (Map<String, Configuration>) request.getAttribute("siteConfig"), RequestContextUtils.getLocale(request).getLanguage(), request.getHeader("host"), null);      
            Double tempCartTotal = cart.getSubTotal();
			for(CartItem item: cart.getCartItems()) {
				if(tempCartTotal != null && item.getTempCartTotal() != null && !tempCartTotal.equals(item.getTempCartTotal())) {
					item.setTempCartTotal(tempCartTotal);
					this.webJaguar.updateCartItem(item);
				}
			}
			
		return "Update Successful";
	}
	
	@RequestMapping(value="/api/DeleteItem.jhtm", method = RequestMethod.POST)
    public @ResponseBody String DeleteCartItem(HttpServletRequest request,  HttpServletResponse response) throws Exception{
	
			InputStreamReader reader = new InputStreamReader(request.getInputStream());
	        JSONTokener tokenizer = new JSONTokener(reader);
	        JSONObject jsonObject = new JSONObject(tokenizer);
	       
	        CartApi cartApi  = new ObjectMapper().readValue(jsonObject.toString(), CartApi.class);  
	        try{
	        	if(cartApi.getApiIndex() == "") {
	        		cartApi.setApiIndex(null);
	        	}
	        	// need to pass apiIndex
               this.webJaguar.removeCartItembyApiIndex(cartApi.getApiIndex(), cartApi.getUserId() , cartApi.getProductId());	         
	        }catch(Exception e){
	        	e.printStackTrace();
	    		return "Delete Failed";
	        }
		return "Delete Successful";
	}
	
	@RequestMapping(value="/api/DeleteCart.jhtm", method = RequestMethod.POST)
    public @ResponseBody String DeleteCart(HttpServletRequest request,  HttpServletResponse response) throws Exception{
	
			InputStreamReader reader = new InputStreamReader(request.getInputStream());
	        JSONTokener tokenizer = new JSONTokener(reader);
	        JSONObject jsonObject = new JSONObject(tokenizer);
	       
	        CartApi cartApi  = new ObjectMapper().readValue(jsonObject.toString(), CartApi.class);  
	        try{ 
	        	Cart cart = this.webJaguar.getUserCart(cartApi.getUserId(), null); 
                if(cart!= null && !cart.getCartItems().isEmpty()){
                    this.webJaguar.deleteShoppingCart(cartApi.getUserId(), null); 
                } else
                {
            	   return "Cart does not exist";
                }
	        }catch(Exception e){
	        	e.printStackTrace();
	    		return "CartDelete Failed";
	        }
		return "CartDelete Successful";
	}
	
}


