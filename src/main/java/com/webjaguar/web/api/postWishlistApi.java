package com.webjaguar.web.api;

import java.io.InputStreamReader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.api.CartApi;
import com.webjaguar.model.api.WishListApi;

@Controller
public class postWishlistApi extends WebApplicationObjectSupport{

	
	@Autowired
	private WebJaguarFacade webJaguar;
	
	@RequestMapping(value="/api/AddToWishList.jhtm", method = RequestMethod.POST)
    public @ResponseBody String handelRequest(HttpServletRequest request,  HttpServletResponse response) throws Exception{
		   String Apiresponse ="";
		   WishListApi wishList = new   WishListApi ();
		   try{
			InputStreamReader reader = new InputStreamReader(request.getInputStream());
		
	        JSONTokener tokenizer = new JSONTokener(reader);
	        JSONObject jsonObject = new JSONObject(tokenizer);
			
	     
	        wishList  = new ObjectMapper().readValue(jsonObject.toString(), WishListApi.class); 
		}catch(Exception e){
			Apiresponse ="Error reading JSON data";
		}
        
        String [] productIds= new String[1];
        if(wishList.getProductId()!= null && !wishList.getProductId().equals("")){
	        productIds [0]= wishList.getProductId().toString().trim();

	   } else {
		   Apiresponse ="Please pass Product ID";
		   return Apiresponse;
	   }
        
        if(wishList.getUserId()== null ||  wishList.getUserId().equals("")){
			   Apiresponse ="Please pass User ID";
			   return Apiresponse;
		   } else {
		        System.out.println("wishList.getUserId()"+wishList.getUserId());
		          try{
					   String user=this.webJaguar.getUserName(wishList.getUserId(), null);
				
				   }catch(Exception e){
					Apiresponse ="User does not exist";
					   return Apiresponse;
				   }
							  
		   }
        System.out.println("wishList.getProductId()"+wishList.getProductId());
        productIds [0]= wishList.getProductId().toString().trim();
        try{
    		this.webJaguar.addToList(productIds, wishList.getUserId());
    		Apiresponse ="Product "+ productIds [0]+" added to wishlist for userId "+wishList.getUserId();

        }catch(Exception e){
			Apiresponse ="Error inserting data";
		}
		
		return Apiresponse;

	}
	
	@RequestMapping(value="/api/DeleteFromWishList.jhtm", method = RequestMethod.POST)
    public @ResponseBody String DeleteRecord(HttpServletRequest request,  HttpServletResponse response) throws Exception{
		String Apiresponse ="";
		   WishListApi wishList = new   WishListApi ();
		   try{
			InputStreamReader reader = new InputStreamReader(request.getInputStream());
		
	        JSONTokener tokenizer = new JSONTokener(reader);
	        JSONObject jsonObject = new JSONObject(tokenizer);
			
	     
	        wishList  = new ObjectMapper().readValue(jsonObject.toString(), WishListApi.class); 
		}catch(Exception e){
			Apiresponse ="Error reading JSON data";
		}
		   String [] productIds = new String[1];
		   if(wishList.getProductId()!= null && !wishList.getProductId().equals("")){
		        productIds [0]= wishList.getProductId().toString().trim();

		   } else {
			   Apiresponse ="Please pass Product ID";
			   return Apiresponse;
		   }
		   
		   if(wishList.getUserId()== null ||  wishList.getUserId().equals("")){
			   Apiresponse ="Please pass User ID";
			   return Apiresponse;
		   } else {
		        System.out.println("wishList.getUserId()"+wishList.getUserId());
		          try{
					   String user=this.webJaguar.getUserName(wishList.getUserId(), null);
				
				   }catch(Exception e){
					Apiresponse ="User does not exist";
					   return Apiresponse;
				   }
							  
		   }
		   try{
			   this.webJaguar.removeFromList(productIds, wishList.getUserId());
	    		Apiresponse ="Product "+ productIds [0]+" deleted from wishlist for userId "+wishList.getUserId();

	        }catch(Exception e){
				Apiresponse ="Error inserting data";
			}
			
			return Apiresponse;
		
		
	}
}
