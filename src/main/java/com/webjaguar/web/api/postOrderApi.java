package com.webjaguar.web.api;
import java.io.DataInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.Promo;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.Supplier;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.model.api.LineItemApi;
import com.webjaguar.model.api.OrderApi;
import com.webjaguar.model.api.OrderprodAttrApi;

@Controller
public class postOrderApi extends WebApplicationObjectSupport{
	
	@Autowired
	private WebJaguarFacade webJaguar;
	protected Map<String, Configuration> siteConfig;
	protected Map<String, Object> gSiteConfig;
	
	@Autowired
	private JavaMailSenderImpl mailSender;
	
	@RequestMapping(value="/api/postOrder.jhtm", method = RequestMethod.POST)
    public @ResponseBody String handelRequest(HttpServletRequest request,  HttpServletResponse response) throws Exception{
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", RequestContextUtils.getLocale(request));
		
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		int gAFFILIATE = (Integer) gSiteConfig.get( "gAFFILIATE" );
		OrderStatus orderStatus = new OrderStatus();

	    InputStreamReader reader = new InputStreamReader(request.getInputStream());
        JSONTokener tokenizer = new JSONTokener(reader);
        JSONObject jsonObject = new JSONObject(tokenizer);
        Order order = new Order();
        OrderApi OrderApi = new ObjectMapper().readValue(jsonObject.toString(), OrderApi.class);
        String message="";
        try{
        order.setUserId(OrderApi.getCustId());
        order.setUserEmail(this.webJaguar.getUserName(order.getUserId(), null));
        Customer customer = this.webJaguar.getCustomerById(order.getUserId());
        customer.getId();
       
       //set Order values
       
		order.setShippingMethod(OrderApi.getShipMethod());
		order.setShippingMessage(OrderApi.getShipMsg());
		order.setShippingCost(OrderApi.getShipCost());
		order.setTax(OrderApi.getTaxAmt());
	
		order.setTaxRate(OrderApi.getTaxRate());
		order.setSubTotal(OrderApi.getOrderSubTotal());
		order.setGrandTotal(OrderApi.getOrderGrandTotal());
		order.setOrderType(OrderApi.getType());
		order.setInvoiceNote(OrderApi.getNote());
		order.setSalesRepId(OrderApi.getSrId());
		order.setTrackcode(OrderApi.getTckcode());
		//Newly Added
	    order.setSalesRepProcessedById(OrderApi.getSrProcessedById());
	   if(OrderApi.getAgreedPaymentDate()!=null){
		    order.setAgreedPaymentDate(dateFormatter.parse(OrderApi.getAgreedPaymentDate()));

	   }
	   if(OrderApi.getAgreedPickupDate()!=null){
		    order.setAgreedPickUpDate(dateFormatter.parse(OrderApi.getAgreedPickupDate()));

	   }
	    
		order.setCreditUsed(OrderApi.getCreditUsed());
		order.setBackendOrder(OrderApi.isBackendOrder());
		order.setPurchaseOrder(OrderApi.getPoNumber());
		order.setNc(OrderApi.getNc());
		order.setPaymentMethod(OrderApi.getPayMethod());
		order.setCcFee(OrderApi.getCcFee());
		order.setPromoAmount(OrderApi.getPromoAmt());
		order.setQualifier(OrderApi.getQualifier());
		Promo promo =null;
		//Promo
		if(OrderApi.getPromoTitle() != null && !OrderApi.getPromoTitle().isEmpty()){
			promo=this.webJaguar.getPromoByName(OrderApi.getPromoTitle());
			order.setPromo(this.webJaguar.getPromo(OrderApi.getPromoTitle()));
		}
				
		//set billing address
		Address UserAddress = new Address();
		UserAddress.setUserId(OrderApi.getCustId());
		if(!OrderApi.getBillingAddress().getfName().equals("") && !OrderApi.getBillingAddress().getlName().equals("") ){
			UserAddress.setFirstName(OrderApi.getBillingAddress().getfName());
			UserAddress.setLastName(OrderApi.getBillingAddress().getlName());
		} else {
			UserAddress = null;
		}
		
		UserAddress.setCompany(OrderApi.getBillingAddress().getCompany());
		UserAddress.setAddr1(OrderApi.getBillingAddress().getAddr1());
		UserAddress.setAddr2(OrderApi.getBillingAddress().getAddr2());
		UserAddress.setCity(OrderApi.getBillingAddress().getCity());
		UserAddress.setStateProvince(OrderApi.getBillingAddress().getStateProvince());
		UserAddress.setStateProvinceNA(OrderApi.getBillingAddress().isStateProvinceNA());
		UserAddress.setZip(OrderApi.getBillingAddress().getZip());
		UserAddress.setCountry(OrderApi.getBillingAddress().getCountry());
		UserAddress.setFax(OrderApi.getBillingAddress().getFax());
		UserAddress.setPhone(OrderApi.getBillingAddress().getPhone());
		UserAddress.setCellPhone(OrderApi.getBillingAddress().getCellPhone());
		order.setBilling(UserAddress);
		
		
		UserAddress =  new Address();
		//set Shipping Address
		UserAddress.setUserId(OrderApi.getCustId());
		
		if(!OrderApi.getShippingAdress().getfName().equals("") && !OrderApi.getShippingAdress().getlName().equals("") ){
			UserAddress.setFirstName(OrderApi.getShippingAdress().getfName());
			UserAddress.setLastName(OrderApi.getShippingAdress().getlName());
		} else {
			UserAddress = null;
		}
       
		UserAddress.setCompany(OrderApi.getShippingAdress().getCompany());
		UserAddress.setAddr1(OrderApi.getShippingAdress().getAddr1());
		UserAddress.setAddr2(OrderApi.getShippingAdress().getAddr2());
		UserAddress.setCity(OrderApi.getShippingAdress().getCity());
		UserAddress.setStateProvince(OrderApi.getShippingAdress().getStateProvince());
		UserAddress.setStateProvinceNA(OrderApi.getShippingAdress().isStateProvinceNA());
		UserAddress.setZip(OrderApi.getShippingAdress().getZip());
		UserAddress.setCountry(OrderApi.getShippingAdress().getCountry());
		UserAddress.setFax(OrderApi.getShippingAdress().getFax());
		UserAddress.setCode(OrderApi.getShippingAdress().getCode());
		UserAddress.setLiftGate(OrderApi.getShippingAdress().isLiftGate());
		UserAddress.setPhone(OrderApi.getShippingAdress().getPhone());
		UserAddress.setCellPhone(OrderApi.getShippingAdress().getCellPhone());
		UserAddress.setResidential(OrderApi.getShippingAdress().isResidential());

		order.setShipping(UserAddress);
		
      //if address entered is not present in user addresses, then add it.
		List <Address> ExistingAdress = this.webJaguar.getAddressListByUserid(OrderApi.getCustId());
		boolean billingExists = false;
		boolean shippingExists = false;

		for(Address addr:ExistingAdress){
			if(addr.compareTo(order.getBilling()) == 1){
				billingExists = true;
				break;
			}
		}
		
		//add billing address
		if(!billingExists){
			this.webJaguar.insertAddress(order.getBilling());
		}
		
		//check if billing and shipping address are same
		if(order.getBilling().compareTo(order.getShipping()) == 0){
			for(Address addr:ExistingAdress){			
				//check if shipping address exists
				if(addr.compareTo(order.getShipping()) == 1){
					shippingExists =  true;
					break;
				}
			}
			
			//add shipping address
			if(!shippingExists){
				this.webJaguar.insertAddress(order.getShipping());
			}
		}
		
	
		//Set LineItems
		 List<LineItem> lineItems = new ArrayList<LineItem>();
		 int lineNumber=1;
		 for(LineItemApi apiItem : OrderApi.getItems()){   
			 LineItem lineItem = new LineItem();
			 
			 Integer id = this.webJaguar.getProductIdBySku(apiItem.getSku());
			 Product product = this.webJaguar.getProductById(id, 0, true, null);
			// Product product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(apiItem.getSku()), request);
			 lineItem.setProductId(product.getId());
			 lineItem.setProduct(product);
			 lineItem.setLineNumber(lineNumber);
			 lineItem.setProduct(product);
			 lineItem.setQuantity(apiItem.getQty());
			 lineItem.setOriginalPrice(apiItem.getOriginalPrice());
			 lineItem.setDiscount(apiItem.getDiscount());	
			 lineItem.setProduct(product);
			 
			 //newly added sept21 2018
			 lineItem.getProduct().setName(apiItem.getName());
			 lineItem.setPercent(apiItem.isDiscountTypePercent());
			 lineItem.setLocation(apiItem.getLocation());



			 if (OrderApi.getPromoTitle()!=null && promo !=null && ((promo.getSkuSet()!=null && promo.getSkuSet().contains(apiItem.getSku())) || (promo.getParentSkuSet()!=null && promo.getParentSkuSet().contains(product.getMasterSku())))) {
				    System.out.println("setPromo---");

				 lineItem.setPromo(promo);
			 }
			 
			 lineItem.setIncludeRetailDisplay(false);
			 Double discountAmount = 0.0;
			 if(lineItem.getOriginalPrice()!= null && lineItem.getDiscount() >= 0){		
				 if(apiItem.isDiscountTypePercent()) {
					    discountAmount = lineItem.getOriginalPrice() * (lineItem.getDiscount() / 100.00);
					    discountAmount = lineItem.getOriginalPrice() - discountAmount;

				 } else {
						discountAmount = lineItem.getOriginalPrice() - discountAmount;

				 }
			    lineItem.setUnitPrice(discountAmount);
			 }
			 
			// Consignment
				if ((Boolean) gSiteConfig.get("gCUSTOMER_SUPPLIER")) {
					// primary supplier is not done yet.
					Supplier supplier = this.webJaguar.getProductSupplierPrimaryBySku(lineItem.getProduct().getSku());
					if (supplier != null) {
						lineItem.setSupplier(supplier);
						lineItem.setCost(supplier.getPrice());
						lineItem.setCostPercent(supplier.isPercent());
						lineItem.getProduct().setDefaultSupplierId(supplier.getId());
					}
				}
				
			  if(apiItem.getOrderprodAttrApi()!=null){
					 List<ProductAttribute> ProductAttributes = new ArrayList<ProductAttribute>();
				 
					 for (OrderprodAttrApi AttrApi : apiItem.getOrderprodAttrApi() ){
						 ProductAttribute Attr = new ProductAttribute();
						 Attr.setOptionName(AttrApi.getOptionName());
						 Attr.setValueName(AttrApi.getValueName());
						 Attr.setOptionPrice(AttrApi.getOptionPrice());
						 ProductAttributes.add(Attr);	 
					 }

					 lineItem.setProductAttributes(ProductAttributes);
			 }		  
			    lineItems.add(lineItem);
		        lineNumber++;
         }
		      
		     order.setLineItems(lineItems);	
		     
		     if (!((String) gSiteConfig.get("gI18N")).isEmpty() && customer != null) {
		    	 order.setLanguageCode(customer.getLanguageCode());
				}
		     
		     
	        this.webJaguar.insertOrder(order, orderStatus, false, order.isBackendOrder(), gAFFILIATE, siteConfig, gSiteConfig);
	         message = "Order placed";
	         //Notify customer      
	         
	         String languageCode = order.getLanguageCode().toString();
		     Integer newOrderId;
		     Integer messageId = null;
			
				
				if (languageCode.equalsIgnoreCase((LanguageCode.en).toString())) {
					messageId = Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_NEW_ORDERS").getValue());
				
	      
						
						try{
				            SiteMessage siteMessage = this.webJaguar.getSiteMessageById(messageId);
						    if (siteMessage != null) {
								orderStatus.setUserNotified(true);
								orderStatus.setSubject(siteMessage.getSubject());
								orderStatus.setMessage(siteMessage.getMessage());
								orderStatus.setHtmlMessage(siteMessage.isHtml());
						    }
						    
			                notifyCustomer(orderStatus, request, order, null,customer);
			                message = "Order placed and email sent to customer";
				         } catch(Exception e){
				        	 e.printStackTrace();
				        	 message = "Order placed but Email failed";
				         }
				}
	        
            return message;
        }catch(Exception e){
        	e.printStackTrace();
        	return "Order Failed";
        }
	}
	
	private void notifyCustomer(OrderStatus orderStatus, HttpServletRequest request, Order order, MultiStore multiStore, Customer customer) {
		SalesRep salesRep = null;
		if ((Boolean) gSiteConfig.get("gSALES_REP") && order.getSalesRepId() != null) {
			salesRep = this.webJaguar.getSalesRepById(order.getSalesRepId());
		}
		// send email
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));

		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append(dateFormatter.format(new Date()) + "\n\n");

		// replace dyamic elements
		try {
			this.webJaguar.replaceDynamicElement(orderStatus, customer, salesRep, order, secureUrl, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		messageBuffer.append(orderStatus.getMessage());

		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File pdfFile = new File(tempFolder, "order_" + order.getOrderId() + ".pdf");
		try {
			// construct email message
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.setBcc(contactEmail);
			if (salesRep != null)
			helper.setCc(salesRep.getEmail());
			helper.setSubject(orderStatus.getSubject());
			if (orderStatus.isHtmlMessage()) {
				helper.setText(messageBuffer.toString(), true);
			} else {
				helper.setText(messageBuffer.toString());
			}
			if ((Boolean) gSiteConfig.get("gPDF_INVOICE") && siteConfig.get("ATTACHE_PDF_ON_PLACE_ORDER").getValue().equals("true")) {
				// attach pdf invoice
				if (this.webJaguar.createPdfInvoice(pdfFile, order.getOrderId(), request)) {
					helper.addAttachment("order_" + order.getOrderId() + ".pdf", pdfFile);
				}
			}
			mailSender.send(mms);
		} catch (Exception ex) {
			if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
				ex.printStackTrace();
			}
			ex.printStackTrace();
		}
		if (pdfFile.exists()) {
			pdfFile.delete();
		}
	}
	
}
