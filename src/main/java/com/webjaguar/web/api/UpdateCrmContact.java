package com.webjaguar.web.api;



import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.api.ApiCrmContact;
import com.webjaguar.model.api.CrmContactFieldApi;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactField;
import com.webjaguar.web.api.authentication.ApiAuthenticationImp;


@Controller
public class UpdateCrmContact {
	@Autowired
	private WebJaguarFacade webJaguar;
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;

	
	   @RequestMapping(value="/api/updateCrmContact.jhtm", method = RequestMethod.POST)
	  public @ResponseBody ApiResponse handelRequest(HttpServletRequest request,  HttpServletResponse response) throws Exception {
			siteConfig = this.webJaguar.getSiteConfig();
			ApiResponse apiResponse = new ApiResponse();
			
			ApiAuthenticationImp authenticate = new ApiAuthenticationImp();
			if(!authenticate.validateRequest(request)) {
				apiResponse.setMessage("Invalid Username and Password.");
				response.setStatus(401);
				return apiResponse;
			}
			ApiCrmContact apiCrmContact = new ApiCrmContact();
			//Read the Json data
			try{
				InputStreamReader reader = new InputStreamReader(request.getInputStream());
		        JSONTokener tokenizer = new JSONTokener(reader);
		        JSONObject jsonObject = new JSONObject(tokenizer);
		        
		        apiCrmContact = new ObjectMapper().readValue(jsonObject.toString(), ApiCrmContact.class);
			} catch(Exception e){
				apiResponse.setMessage("Error : Can't Read JSON data");
				e.printStackTrace();
				response.setStatus(400);
				return apiResponse;
			}
			
//			CrmContact wjCrmContact = new CrmContact();
			
	        if(apiCrmContact != null) {
	        	System.out.println(apiCrmContact + "????");
	        	        if(apiCrmContact.getId() == null) {
	        	         	apiResponse.setMessage("id is mandatory!");
	        				response.setStatus(400);
	        				return apiResponse;
	        	        }
	        			int contactId = apiCrmContact.getId();
	        			CrmContact wjCrmContact = this.webJaguar.getCrmContactById(contactId);
	        			
	        			if(wjCrmContact == null) {
	        				apiResponse.setMessage("this crmContact does'nt exist!");
	        				response.setStatus(400);
	        				return apiResponse;
	        			} else {
	    		        	// Accountname
	    		        	if(apiCrmContact.getAccountName() != null && !apiCrmContact.getAccountName().isEmpty()){
	    		        		System.out.println("I am here!!!" + apiCrmContact.getAccountName());
	    		        		wjCrmContact.setAccountName(apiCrmContact.getAccountName());
	    		        	}
	    		         // AccountId
	    		        if(apiCrmContact.getAccountId() != null){
	    		        		wjCrmContact.setAccountId(apiCrmContact.getAccountId());
	    		        	}
	    		         // FirstName
	    		        	if(apiCrmContact.getFirstName() != null && !apiCrmContact.getFirstName().isEmpty()) {
	    		        		
	    		        		wjCrmContact.setFirstName(apiCrmContact.getFirstName());
	    		        	}
	            	
	    	            // LastName
	    	           	if(apiCrmContact.getLastName() != null && !apiCrmContact.getLastName().isEmpty()) {
	    	           		wjCrmContact.setLastName(apiCrmContact.getLastName());
	    	           	}
	    	           	
	    	          	// language
	    	         	if(apiCrmContact.getLanguage() != null ) {
		    	          	wjCrmContact.setLanguage(apiCrmContact.getLanguage());
	    	           	}
	    	          	// trackcode
	    	         	if(apiCrmContact.getTrackcode() != null ) {
		    	          	wjCrmContact.setTrackcode(apiCrmContact.getTrackcode());
	    	           	}
	    	         	
	    	         	// unsubscribe
	    	          	if(apiCrmContact.isUnsubscribe()) {
	    		          	wjCrmContact.setUnsubscribe(apiCrmContact.isUnsubscribe());
	    	          	} else {
	    		          	wjCrmContact.setUnsubscribe(false);
	    	          	}
	    	          	
	    	           	
	    	           	
	    	           	
	    	          	if(apiCrmContact.getLeadSource() != null && !apiCrmContact.getLeadSource().isEmpty()) {
	    	           		wjCrmContact.setLeadSource(apiCrmContact.getLeadSource());
	    	           	}
	    	          	
	    	        	    if(apiCrmContact.getTitle() != null && !apiCrmContact.getTitle().isEmpty()) {
	    	        	    		wjCrmContact.setTitle(apiCrmContact.getTitle());
	    	           	}
	    		        	
	    	        	    if(apiCrmContact.getDescription() != null && !apiCrmContact.getDescription().isEmpty()) {
	    	        	    		wjCrmContact.setDescription(apiCrmContact.getDescription());
    	              	}
	    	        	    
	    	        	    if(apiCrmContact.getDescription() != null && !apiCrmContact.getDescription().isEmpty()) {
    	        	    			wjCrmContact.setDescription(apiCrmContact.getDescription());
	              	}
	    	        	    
	    	        	    if(apiCrmContact.getShortDesc() != null && !apiCrmContact.getShortDesc().isEmpty()) {
	    	        	    		wjCrmContact.setShortDesc(apiCrmContact.getShortDesc());
	              	}
	    		        	
	    	        	    if(apiCrmContact.getEmail1() != null && !apiCrmContact.getEmail1().isEmpty()) {
	    	        	    		wjCrmContact.setEmail1(apiCrmContact.getEmail1());
              	    }
	    	        	    if(apiCrmContact.getEmail2() != null && !apiCrmContact.getEmail2().isEmpty()) {
    	        	    		    wjCrmContact.setEmail2(apiCrmContact.getEmail2());
          	        }
	    	        	    if(apiCrmContact.getPhone1() != null && !apiCrmContact.getPhone1().isEmpty()) {
	    	        	     	wjCrmContact.setPhone1(apiCrmContact.getPhone1());
      	             }
	    	        	    if(apiCrmContact.getPhone2() != null && !apiCrmContact.getPhone2().isEmpty()) {
	    	        	     	wjCrmContact.setPhone2(apiCrmContact.getPhone2());
      	             }
	    	        	    if(apiCrmContact.getWebsite() != null && !apiCrmContact.getWebsite().isEmpty()) {
	    	        	    		wjCrmContact.setWebsite(apiCrmContact.getWebsite());
      	             }
	    	        	    if(apiCrmContact.getFax() != null && !apiCrmContact.getFax().isEmpty()) {
	    	        	    		wjCrmContact.setFax(apiCrmContact.getFax());
  	                   }
	    	        	    if(apiCrmContact.getStreet() != null && !apiCrmContact.getStreet().isEmpty()) {
	    	        	    		wjCrmContact.setStreet(apiCrmContact.getStreet());
	                   }
	    	        	    if(apiCrmContact.getCity() != null && !apiCrmContact.getCity().isEmpty()) {
	    	        	    		wjCrmContact.setCity(apiCrmContact.getCity());
                 	 }
	    	        	    if(apiCrmContact.getStateProvince() != null && !apiCrmContact.getStateProvince().isEmpty()) {
	    	        	      	wjCrmContact.setStateProvince(apiCrmContact.getStateProvince());
	    	        	    	}
	    	        	    if(apiCrmContact.getZip() != null && !apiCrmContact.getZip().isEmpty()) {
	    	        	      	wjCrmContact.setZip(apiCrmContact.getZip());
	    	        	    	}
	    	        	    if(apiCrmContact.getCountry() != null && !apiCrmContact.getCountry().isEmpty()) {
	    	        	    		wjCrmContact.setCountry(apiCrmContact.getCountry());
	    	        	    	}
	    	        	    if(apiCrmContact.getAddressType() != null && !apiCrmContact.getAddressType().isEmpty()) {
	    	        	      	wjCrmContact.setAddressType(apiCrmContact.getAddressType());
    	        	      	}
	    	        	    if(apiCrmContact.getOtherStreet() != null && !apiCrmContact.getOtherStreet().isEmpty()) {
	    	        	    		wjCrmContact.setOtherStreet(apiCrmContact.getOtherStreet());
    	        	      	}
	    	        	    if(apiCrmContact.getOtherCity() != null && !apiCrmContact.getOtherCity().isEmpty()) {
    	        	    			wjCrmContact.setOtherCity(apiCrmContact.getOtherCity());
	        	      	}
	    	        	    if(apiCrmContact.getOtherZip() != null && !apiCrmContact.getOtherZip().isEmpty()) {
	        	    			wjCrmContact.setOtherZip(apiCrmContact.getOtherZip());
	    	        	    }
	    	        	    if(apiCrmContact.getOtherStateProvince() != null && !apiCrmContact.getOtherStateProvince().isEmpty()) {
	        	    			wjCrmContact.setOtherStateProvince(apiCrmContact.getOtherStateProvince());
	    	        	    }
	    	        	    if(apiCrmContact.getOtherCountry() != null && !apiCrmContact.getOtherCountry().isEmpty()) {
	        	    			wjCrmContact.setOtherCountry(apiCrmContact.getOtherCountry());
	    	        	    }
	    	        	    
	    	        	  	
	    			       	
	    			   if(apiCrmContact.getCrmContactFields() != null) {
	    			       	 List<CrmContactField> list = new ArrayList<CrmContactField>();
	    					 for(CrmContactFieldApi apiField : apiCrmContact.getCrmContactFields()){  
	    						 CrmContactField crmField = new CrmContactField();
	    						 crmField.setId(apiField.getId());
	    						 crmField.setFieldValue(apiField.getFieldValue());
	    						 list.add(crmField);
	    					 }
	    						 
	    				       wjCrmContact.setCrmContactFields(list);
	    			     }

	    		        
	        		}
	        		     try{
	        		            this.webJaguar.updateCrmContact(wjCrmContact);
	        		          
	        		            apiResponse.setMessage("Success");
	        		            response.setStatus(200);
	        		         } catch(Exception e){
	        		         	e.printStackTrace();
	        		         	apiResponse.setMessage("Error : Exception in The Update");
	        		         	response.setStatus(400);
	        		         }
	        				
	        				
	        				
	        				
	           }		
	          return apiResponse;
	          
	        }
			
	    }
	

