package com.webjaguar.web.api;

import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.model.api.ApiCustomer;
import com.webjaguar.web.api.authentication.ApiAuthenticationImp;

@Controller
public class InsertCustomer {
	@Autowired
	private WebJaguarFacade webJaguar;
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	@RequestMapping(value="/api/insertCustomer.jhtm", method = RequestMethod.POST)

	 public @ResponseBody ApiResponse handelRequest(HttpServletRequest request,  HttpServletResponse response) throws Exception{
		siteConfig = this.webJaguar.getSiteConfig();
		ApiResponse apiResponse = new ApiResponse();
		
		ApiAuthenticationImp authenticate = new ApiAuthenticationImp();
		if(!authenticate.validateRequest(request)) {
			apiResponse.setMessage("Invalid Username and Password.");
			response.setStatus(401);
			return apiResponse;
		}
		ApiCustomer apiCustomer = new ApiCustomer();
		
		//Read the Json data
		try{
			InputStreamReader reader = new InputStreamReader(request.getInputStream());
	        JSONTokener tokenizer = new JSONTokener(reader);
	        JSONObject jsonObject = new JSONObject(tokenizer);
	        
	        apiCustomer = new ObjectMapper().readValue(jsonObject.toString(), ApiCustomer.class);
		} catch(Exception e){
			apiResponse.setMessage("Error : Can't Read JSON data");
			e.printStackTrace();
			response.setStatus(400);
			return apiResponse;
		}
		

		Customer wjCustomer = new Customer();
		Address address = new Address();
		
        if(apiCustomer != null) {
             wjCustomer.setRegisteredBy(apiCustomer.getRegisteredBy());

		        	String userName = apiCustomer.getUsername();
		    		Customer customer = this.webJaguar.getCustomerByUsername(userName);
		    		if(customer != null) {
		    			int userId =  customer.getId();
		    			address.setUserId(userId);
		    			List<Address> listOfAddress = this.webJaguar.getAllAddressList(userId);
		    			for(int i = 0; i < listOfAddress.size(); i++) {
		    				if(apiCustomer.getApiAddress().getAddr1().equalsIgnoreCase(listOfAddress.get(i).getAddr1())) {
		    					apiResponse.setMessage("this address already exist for this customer!");
			    				 response.setStatus(400);
			    				 return apiResponse;
		    				}
		    			}
		    			fillAddress(apiCustomer, apiResponse, address,  response,  wjCustomer );
		    			if(apiResponse.getMessage() == null) {
		    				System.out.println("+++++");
		    				this.webJaguar.insertAddress(address);
		    				apiResponse.setMessage(" Address is inserted! succesfully");
		    				 response.setStatus(200);
		    				 return apiResponse;
		    			} else {
		    				return apiResponse;
		    			}
		    			
		    		} else {
		    		  	//UserName
		            	if(apiCustomer.getUsername() != null && !apiCustomer.getUsername().isEmpty()){
		            		wjCustomer.setUsername(apiCustomer.getUsername());
		            	}else{
		            		response.setStatus(400);
		            		apiResponse.setMessage("Please Pass UserName/Email address");
		            		return apiResponse;
		            	}
		            	//Password
		            	if(apiCustomer.getPassword() != null && !apiCustomer.getPassword().isEmpty()){
		            		wjCustomer.setPassword(apiCustomer.getPassword());
		            	}else{
		            		response.setStatus(400);
		            		apiResponse.setMessage("Please Pass password");
		            		return apiResponse;
		            	}
		            	
		            	System.out.println("PASS"+wjCustomer.getPassword());
		            	
		            	fillAddress(apiCustomer, apiResponse, address,  response,  wjCustomer );
		            	
		            	if(apiResponse.getMessage() != null) {
		    				return apiResponse;
		    			}
		            	
		    	        // languageCode
		    	        if(apiCustomer.getLanguage() != null) {
		    	        
		    	          	if(apiCustomer.getLanguage() == LanguageCode.es) {
		    	          		System.out.println(apiCustomer.getLanguage() + "{{{}}}");
		    	          		wjCustomer.setLanguageCode(LanguageCode.es);
		    	          	}else {
		    	          		wjCustomer.setLanguageCode(LanguageCode.en);
		    	          	}
		    		      }
		    	        wjCustomer.setHost(apiCustomer.getHost());
		    	        wjCustomer.setExtraEmail(apiCustomer.getExtraEmail());
		    	        wjCustomer.setPayment(apiCustomer.getPayment());
		    	        wjCustomer.setCredit(apiCustomer.getCredit());
		    	        wjCustomer.setNote(apiCustomer.getNote());
//		    	        wjCustomer.setAccountNumberPrefix(apiCustomer.getAccountNumberPrefix());
		    	        wjCustomer.setAccountNumber(apiCustomer.getAccountNumber());
//		    	        wjCustomer.setWebsiteUrl(apiCustomer.getWebsiteUrl());
//		    	        wjCustomer.setQbTaxCode(apiCustomer.getQbTaxCode());
		    	        wjCustomer.setTrackcode(apiCustomer.getTrackcode());
		    	        wjCustomer.setTaxId(apiCustomer.getTaxId());
		    	        wjCustomer.setSalesRepId(apiCustomer.getSalesRepId());
		    	        
		            	wjCustomer.setField1(apiCustomer.getField1()); 
		            	wjCustomer.setField2(apiCustomer.getField2()); 
		            	wjCustomer.setField3(apiCustomer.getField3()); 
		            	wjCustomer.setField4(apiCustomer.getField4()); 
		            	wjCustomer.setField5(apiCustomer.getField5()); 
		            	wjCustomer.setField6(apiCustomer.getField6()); 
		            	wjCustomer.setField7(apiCustomer.getField7()); 
		            	wjCustomer.setField8(apiCustomer.getField8()); 
		            	wjCustomer.setField9(apiCustomer.getField9()); 
		            	wjCustomer.setField10(apiCustomer.getField10());
		            	wjCustomer.setField11(apiCustomer.getField11()); 
		            	wjCustomer.setField12(apiCustomer.getField12()); 
		            	wjCustomer.setField13(apiCustomer.getField13()); 
		            	wjCustomer.setField14(apiCustomer.getField14()); 
		            	wjCustomer.setField15(apiCustomer.getField15());
		            	wjCustomer.setField16(apiCustomer.getField16()); 
		            	wjCustomer.setField17(apiCustomer.getField17()); 
		            	wjCustomer.setField18(apiCustomer.getField18()); 
		            	wjCustomer.setField19(apiCustomer.getField19()); 
		            	wjCustomer.setField20(apiCustomer.getField20());
		            	wjCustomer.setField21(apiCustomer.getField21()); 
		            	wjCustomer.setField22(apiCustomer.getField22()); 
		            	wjCustomer.setField23(apiCustomer.getField23()); 
		            	wjCustomer.setField24(apiCustomer.getField24()); 
		            	wjCustomer.setField25(apiCustomer.getField25()); 
		            	wjCustomer.setField26(apiCustomer.getField26()); 
		            	wjCustomer.setField27(apiCustomer.getField27()); 
		            	wjCustomer.setField28(apiCustomer.getField28()); 
		            	wjCustomer.setField29(apiCustomer.getField29()); 
		            	wjCustomer.setField30(apiCustomer.getField30());
		            	
		    	        wjCustomer.setEmailNotify(apiCustomer.isEmailNotify());
		    	        wjCustomer.setUnsubscribe(apiCustomer.isUnsubscribe());
		    	        wjCustomer.setRating1(apiCustomer.getRating1());
		    	        wjCustomer.setRating2(apiCustomer.getRating2());
		    	        
		    	        wjCustomer.setLanguage(apiCustomer.getLanguageField());
		    	        wjCustomer.setWebsiteUrl(apiCustomer.getWebsiteUrl());
		    	        wjCustomer.setEbayName(apiCustomer.getEbayName());
		    	        wjCustomer.setAmazonName(apiCustomer.getAmazonName());
		    	        wjCustomer.setUnsubscribeReason(apiCustomer.getUnsubscribeReason());
		    	        wjCustomer.setTextMessageNotify(apiCustomer.isTextMessageNotify());
		    	        wjCustomer.setWhatsappNotify(apiCustomer.getWhatsappNotify());
		    	        wjCustomer.setUnsubscribeTextMessage(apiCustomer.isUnsubscribeTextMessage());
		    	        
		    	        //Basic Registration default values
		    	        if(siteConfig.get("PROTECTED_ACCESS_NEW_REGISTRANT")!=null && !siteConfig.get("PROTECTED_ACCESS_NEW_REGISTRANT").getValue().isEmpty()){
		    	        	wjCustomer.setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_NEW_REGISTRANT").getValue());
		    	        }
		    	        if(siteConfig.get("PROTECTED_ACCESS_NEW_REGISTRANT")!=null && !siteConfig.get("PROTECTED_ACCESS_NEW_REGISTRANT").getValue().isEmpty()){
		    	        	wjCustomer.setPriceTable(Integer.parseInt(siteConfig.get("NEW_REGISTRANT_PRICE_PROTECTED_ACCESS").getValue()));
		    	        }
		    	        if (siteConfig.get("CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER")!=null && siteConfig.get("CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER").getValue().equals("true")) {
		    	        	wjCustomer.setAutoGenerateAccountNumber(true);
		    	        }else{
		    	        	wjCustomer.setAutoGenerateAccountNumber(false);
		    	        }
		    		}
		    		

        } 
        try{

          // wjCustomer.setRegisteredBy("Api");

        	if(apiCustomer.getRegisteredBy() != null) {
        		wjCustomer.setRegisteredBy(apiCustomer.getRegisteredBy());
        	} else {
        		 wjCustomer.setRegisteredBy("Api");
        	}
           

           this.webJaguar.insertCustomer(wjCustomer,null, true);

           
           apiResponse.setMessage("Success");
           response.setStatus(200);
        } catch(Exception e){
        	e.printStackTrace();
        	apiResponse.setMessage("Error : Exception in INSERT");
        	response.setStatus(400);
        }
		return apiResponse;
	 }
	
	private ApiResponse fillAddress(ApiCustomer apiCustomer, ApiResponse apiResponse, Address address, HttpServletResponse response, Customer wjCustomer ) {
    	//FirstName
    	boolean addressField= true;
    	if(apiCustomer.getApiAddress().getFirstName() != null && !apiCustomer.getApiAddress().getFirstName().isEmpty()){
    		address.setFirstName(apiCustomer.getApiAddress().getFirstName());
    	}else{
    		response.setStatus(400);
    		apiResponse.setMessage("Please Pass First Name");
    		addressField = false;
    		return apiResponse;
    	}
    	//LastName
    	if(apiCustomer.getApiAddress().getLastName() != null && !apiCustomer.getApiAddress().getLastName().isEmpty()){
    		address.setLastName(apiCustomer.getApiAddress().getLastName());
    	}else{
    		response.setStatus(400);
    		apiResponse.setMessage("Please Pass Last Name");
    		addressField = false;
    		return apiResponse;
    	}
    	//Addr1
    	if(apiCustomer.getApiAddress().getAddr1() != null && !apiCustomer.getApiAddress().getAddr1().isEmpty()){
    		address.setAddr1(apiCustomer.getApiAddress().getAddr1());
    	}else{
    		response.setStatus(400);
    		apiResponse.setMessage("Please Pass Address");
    		addressField = false;
    		return apiResponse;
    	}
    	//Country
    	if(apiCustomer.getApiAddress().getCountry() != null && !apiCustomer.getApiAddress().getCountry().isEmpty()){
    		address.setCountry(apiCustomer.getApiAddress().getCountry());
    	}else{
    		response.setStatus(400);
    		apiResponse.setMessage("Please Pass Country");
    		addressField = false;
    		return apiResponse;
    	}
    	//City
    	if(apiCustomer.getApiAddress().getCity() != null && !apiCustomer.getApiAddress().getCity().isEmpty()){
    		address.setCity(apiCustomer.getApiAddress().getCity());
    	}else{
    		response.setStatus(400);
    		apiResponse.setMessage("Please Pass City");
    		addressField = false;
    		return apiResponse;
    	}
    	//State
    	if(apiCustomer.getApiAddress().getStateProvince() != null && !apiCustomer.getApiAddress().getStateProvince().isEmpty()){
    		address.setStateProvince(apiCustomer.getApiAddress().getStateProvince());
    	}else{
    		response.setStatus(400);
    		apiResponse.setMessage("Please Pass State");
    		addressField = false;
    		return apiResponse;
    	}
    	//Zip
    	if(apiCustomer.getApiAddress().getZip() != null && !apiCustomer.getApiAddress().getZip().isEmpty()){
    		address.setZip(apiCustomer.getApiAddress().getZip());
    	}else{
    		response.setStatus(400);
    		apiResponse.setMessage("Please Pass Zip");
    		addressField = false;
    		return apiResponse;
    	}
    	//Phone
    	if(apiCustomer.getApiAddress().getPhone() != null && !apiCustomer.getApiAddress().getPhone().isEmpty()){
    		address.setPhone(apiCustomer.getApiAddress().getPhone());
    	}else{
    		response.setStatus(400);
    		apiResponse.setMessage("Please Pass Phone");
    		addressField = false;
    	}
    	
    	address.setCompany(apiCustomer.getApiAddress().getCompany());
    	address.setAddr2(apiCustomer.getApiAddress().getAddr2());
//    	address.setAddr3(apiCustomer.getApiAddress().getAddr3());
    	address.setStateProvinceNA(apiCustomer.getApiAddress().isStateProvinceNA());
    	address.setCellPhone(apiCustomer.getApiAddress().getCellPhone());
    	address.setFax(apiCustomer.getApiAddress().getFax());
    	address.setEmail(apiCustomer.getApiAddress().getEmail());
    	address.setResidential(apiCustomer.getApiAddress().isResidential());
    address.setLiftGate(apiCustomer.getApiAddress().isLiftGate());
    
    	if(addressField) {
    		wjCustomer.setAddress(address);
    	   }
    	
    	return apiResponse;
    		
	}
}
//*******************TODO for More Improvment**********
/*1). Sales rep
 * 2). Multi Store
 * */
