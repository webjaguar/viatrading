package com.webjaguar.web.api;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=Inclusion.NON_NULL)
public class ViewCartResponse {
	
		private int productId;
		private double discount;
		private double lineItemTotal;
		public int getProductId() {
			return productId;
		}
		public void setProductId(int productId) {
			this.productId = productId;
		}
		public double getDiscount() {
			return discount;
		}
		public void setDiscount(double discount) {
			this.discount = discount;
		}
		public double getLineItemTotal() {
			return lineItemTotal;
		}
		public void setLineItemTotal(double lineItemTotal) {
			this.lineItemTotal = lineItemTotal;
		}
}
