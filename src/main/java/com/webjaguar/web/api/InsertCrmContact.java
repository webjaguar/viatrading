package com.webjaguar.web.api;

import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.api.ApiCrmContact;
import com.webjaguar.model.api.ApiCustomer;
import com.webjaguar.model.api.CrmContactFieldApi;
import com.webjaguar.model.api.LineItemApi;
import com.webjaguar.model.crm.CrmAccount;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactField;
import com.webjaguar.model.crm.CrmForm;
import com.webjaguar.web.api.authentication.ApiAuthenticationImp;

@Controller
public class InsertCrmContact {
	@Autowired
	private WebJaguarFacade webJaguar;
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	@Autowired
	private JavaMailSenderImpl mailSender;
	
	@RequestMapping(value="/api/insertCrmContact.jhtm", method = RequestMethod.POST)
	 public @ResponseBody ApiResponse handelRequest(HttpServletRequest request,  HttpServletResponse response) throws Exception{
		siteConfig = this.webJaguar.getSiteConfig();
		ApiResponse apiResponse = new ApiResponse();
		
		ApiAuthenticationImp authenticate = new ApiAuthenticationImp();
		if(!authenticate.validateRequest(request)) {
			apiResponse.setMessage("Invalid Username and Password.");
			response.setStatus(401);
			return apiResponse;
		}
		ApiCrmContact apiCrmContact = new ApiCrmContact();
		//Read the Json data
		try{
			InputStreamReader reader = new InputStreamReader(request.getInputStream());
	        JSONTokener tokenizer = new JSONTokener(reader);
	        JSONObject jsonObject = new JSONObject(tokenizer);
	        
	        apiCrmContact = new ObjectMapper().readValue(jsonObject.toString(), ApiCrmContact.class);
		} catch(Exception e){
			apiResponse.setMessage("Error : Can't Read JSON data");
			e.printStackTrace();
			response.setStatus(400);
			return apiResponse;
		}
		
		
		CrmContact wjCrmContact = new CrmContact();
		
        if(apiCrmContact != null) {
        	
	         	//AccountId
	          	if(apiCrmContact.getAccountId() == null){
	        		    response.setStatus(400);
	        		    apiResponse.setMessage("Please Pass AccountId");
	        	     	return apiResponse;
	         	}else {
	        		  CrmAccount crmAccount = this.webJaguar.getCrmAccountById(apiCrmContact.getAccountId());
	        		   if(crmAccount == null) {
	        			response.setStatus(400);
	            		apiResponse.setMessage("Please Pass an existing AccountId");
	            		return apiResponse;
	        		   }else {
	        			   wjCrmContact.setAccountId(apiCrmContact.getAccountId());
	        		   }
	        		
	            }
	          	
	          	// language
	          	if(apiCrmContact.getLanguage() != null) {
		          	wjCrmContact.setLanguage(apiCrmContact.getLanguage());
	          	}
	          	
	          	// unsubscribe
	          	if(apiCrmContact.isUnsubscribe()) {
		          	wjCrmContact.setUnsubscribe(apiCrmContact.isUnsubscribe());
	          	}

		        	
		        	// createdBy 
		        	if(	wjCrmContact.getCreatedBy() != null) {
		        		wjCrmContact.setCreatedBy(wjCrmContact.getCreatedBy());
		        	} else {
		        		wjCrmContact.setCreatedBy("Api");
		        	}
		        	
		        	// Accountname
		        	if(apiCrmContact.getAccountName() != null && !apiCrmContact.getAccountName().isEmpty()){
		        		wjCrmContact.setAccountName(apiCrmContact.getAccountName());
		        	}else{
		        		response.setStatus(400);
		        		apiResponse.setMessage("Please Pass AccountName");
		        		return apiResponse;
		        	}
        	
		         // FirstName
		        	if(apiCrmContact.getFirstName() != null && !apiCrmContact.getFirstName().isEmpty()) {
		        		wjCrmContact.setFirstName(apiCrmContact.getFirstName());
		        	}else{
		        		response.setStatus(400);
		        		apiResponse.setMessage("Please Pass FirstName");
		        		return apiResponse;
		        	}
        	
	            // LastName
	           	if(apiCrmContact.getLastName() != null && !apiCrmContact.getLastName().isEmpty()) {
	           		wjCrmContact.setLastName(apiCrmContact.getLastName());
	           	}else{
	           		response.setStatus(400);
	           		apiResponse.setMessage("Please Pass LastName");
	           		return apiResponse;
	           	}
		        	wjCrmContact.setLeadSource(apiCrmContact.getLeadSource());
		        	wjCrmContact.setTitle(apiCrmContact.getTitle());
		        	wjCrmContact.setDescription(apiCrmContact.getDescription());
		        	wjCrmContact.setShortDesc(apiCrmContact.getShortDesc());
		        	wjCrmContact.setEmail1(apiCrmContact.getEmail1());
		        	wjCrmContact.setEmail2(apiCrmContact.getEmail2());
		        	wjCrmContact.setPhone1(apiCrmContact.getPhone1());
		        	wjCrmContact.setPhone2(apiCrmContact.getPhone2());
		        	wjCrmContact.setWebsite(apiCrmContact.getWebsite());
		        	wjCrmContact.setFax(apiCrmContact.getFax());
		        	wjCrmContact.setStreet(apiCrmContact.getStreet());
		        	wjCrmContact.setCity(apiCrmContact.getCity());
		        	wjCrmContact.setTrackcode(apiCrmContact.getTrackcode());
		
		        	wjCrmContact.setStateProvince(apiCrmContact.getStateProvince());
		        	wjCrmContact.setZip(apiCrmContact.getZip());
		        	wjCrmContact.setCountry(apiCrmContact.getCountry());
		        	wjCrmContact.setAddressType(apiCrmContact.getAddressType());
		        	wjCrmContact.setOtherStreet(apiCrmContact.getOtherStreet());
		        	wjCrmContact.setOtherCity(apiCrmContact.getOtherCity());
		        	wjCrmContact.setOtherZip(apiCrmContact.getOtherZip());
		        	wjCrmContact.setOtherCountry(apiCrmContact.getOtherCountry());
		        	wjCrmContact.setOtherStateProvince(apiCrmContact.getOtherStateProvince());
		        	
		       	 List<CrmContactField> list = new ArrayList<CrmContactField>();
//				 int lineNumber=1;
		       	 if(apiCrmContact.getCrmContactFields() != null) {
		       		 for(CrmContactFieldApi apiField : apiCrmContact.getCrmContactFields()){  
						 CrmContactField crmField = new CrmContactField();
						 crmField.setId(apiField.getId());
						 crmField.setFieldValue(apiField.getFieldValue());
						 list.add(crmField);
					 }
		       	 }
				
					 
			   wjCrmContact.setCrmContactFields(list);	 
					 
        }
        try{
            
            this.webJaguar.insertCrmContact(wjCrmContact);
            
            CrmForm crmForm = null;
            String recipientCcEmail = null;
            String recipientBccEmail = null;
            File attachment = null;
            File attachment1 = null;
            File attachment2 = null;
            File attachment3 = null;
            File attachment4 = null;
            
            //hardcoded for notification, confirmed by Alain from Viatrading
            //2020-03-04
                        
            String recipientEmail = "crm@viatrading.com";
            this.webJaguar.newLeadEmail(wjCrmContact, recipientEmail, recipientCcEmail, recipientBccEmail, request, crmForm, attachment, attachment1, attachment2, attachment3, attachment4, mailSender); 
            
            apiResponse.setMessage("Success");
            response.setStatus(200);
         } catch(Exception e){
         	e.printStackTrace();
         	apiResponse.setMessage("Error : Exception in INSERT");
         	response.setStatus(400);
         }
		
		return apiResponse;
	}
}
