package com.webjaguar.web.api;

import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.api.ApiProduct;
import com.webjaguar.web.api.authentication.ApiAuthenticationImp;

@Controller
public class createProduct {
	
	@Autowired
	private WebJaguarFacade webJaguar;
	protected Map<String, Configuration> siteConfig;
	protected Map<String, Object> gSiteConfig;
	
	private static final Logger LOG = LoggerFactory.getLogger(createProduct.class);
	
	@RequestMapping(value="/api/createProduct.jhtm", method = RequestMethod.POST)
    public @ResponseBody ApiResponse handelRequest(HttpServletRequest request,  HttpServletResponse response, JSONObject jsonObjectFromUpdateApi) throws Exception{
		logAPI("createProduct");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		ApiResponse apiResponse = new ApiResponse();
		HashMap<String,String> validationMap= new HashMap<String,String>();
		ApiAuthenticationImp authenticate = new ApiAuthenticationImp();
		if(!authenticate.validateRequest(request)) {
			apiResponse.setMessage("Invalid Username and Password.");
			response.setStatus(401);
			LOG.warn("Invalid Username and Password.");
			return apiResponse;
		}
		ApiProduct apiProduct = new ApiProduct();
		JSONObject jsonObject = new JSONObject();
		//Read the Json data
		try
		{
				InputStreamReader reader = new InputStreamReader(request.getInputStream());
		        JSONTokener tokenizer = new JSONTokener(reader);
		        if(jsonObjectFromUpdateApi.length()>0) {
		        	jsonObject=jsonObjectFromUpdateApi;
		        }else {
		        	jsonObject = new JSONObject(tokenizer);
		        }
			    //apiProduct = new ObjectMapper().readValue(jsonObject.toString(), ApiProduct.class);
			    ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.NON_NULL);
			    apiProduct = mapper.readValue(jsonObject.toString(), ApiProduct.class);
		  	} catch(UnrecognizedPropertyException e) {
					apiResponse.setMessage(e.getMessage().substring(0, e.getMessage().indexOf("(")));
					response.setStatus(400);
					logAPIError("createProduct",e);
					return apiResponse;	
			} catch(Exception e){
		        	e.printStackTrace();
		        	apiResponse.setMessage("Error reading input data");
					response.setStatus(400);
					logAPIError("createProduct",e);
					return apiResponse;
		    }
		
                Product wjProduct = new Product (apiProduct);
              
    			if(apiProduct.isNegInventory()!=null) {
    				wjProduct.setNegInventory(apiProduct.isNegInventory());
    			}
    			if(apiProduct.isShowNegInventory()!=null) {
    				wjProduct.setShowNegInventory(apiProduct.isShowNegInventory());
    			}
                if((apiProduct.getCategoryIds()!=null && !apiProduct.getCategoryIds().equals(""))){
                	wjProduct.setCatIds(setCatids(wjProduct));
                }
		        System.out.println("sku: "+wjProduct.getSku()+" - "+wjProduct.getWeight());
		        if(apiProduct.getSku().equals("") || apiProduct.getSku().equals(null)){
				   apiResponse.setMessage("Please pass a valid Sku");
				   LOG.warn("Please pass a valid Sku");
				   response.setStatus(400);
				   return apiResponse;
		        }
		        if(apiProduct.getName().equals("") || apiProduct.getName().equals(null)){
					   apiResponse.setMessage("Please enter valid Name for the product");
					   response.setStatus(400);
					   LOG.warn("Please enter valid Name for the product");
					   return apiResponse;
			    }
		        
		        wjProduct.setCreatedBy("API");
		        
		        //check duplicate sku
				Integer id = this.webJaguar.getProductIdBySku(wjProduct.getSku());
				if(id!=null){
					apiResponse.setMessage("An item using this sku already exists");
					response.setStatus(400);
					LOG.warn("An item using this sku already exists : id = "+id);
					return apiResponse;
				}

				try{					
				    this.webJaguar.insertProduct(wjProduct, null);
				    Product createdProd= this.webJaguar.getProductById(wjProduct.getId(), request);
				    apiResponse.setProduct(new ApiProduct(createdProd));
				}catch(Exception e){
					apiResponse.setMessage("Error : Product creation failed");
					response.setStatus(400);
					logAPIError("createProduct",e);
					return apiResponse;					
				}
				apiResponse.setMessage("Product with Id "+wjProduct.getId() +"successfully created");
				logAPISuccess("createProduct");
				return apiResponse;
	}
	
	@RequestMapping(value="/api/updateProduct.jhtm", method = RequestMethod.POST)
    public @ResponseBody ApiResponse updateProduct(HttpServletRequest request,  HttpServletResponse response) throws Exception{
		logAPI("updateProduct");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		ApiResponse apiResponse = new ApiResponse();

		ApiAuthenticationImp authenticate = new ApiAuthenticationImp();
		if(!authenticate.validateRequest(request)) {
			apiResponse.setMessage("Invalid Username and Password.");
			response.setStatus(401);
			return apiResponse;
		}
		 JSONObject jsonObject = new JSONObject();
		//Read the Json data
		ApiProduct apiProduct = new ApiProduct();
		
		//Read the Json data
		try {
			InputStreamReader reader = new InputStreamReader(request.getInputStream());
	        JSONTokener tokenizer = new JSONTokener(reader);
	        jsonObject = new JSONObject(tokenizer);	        	        
		   // apiProduct = new ObjectMapper().readValue(jsonObject.toString(), ApiProduct.class);
	        ObjectMapper mapper = new ObjectMapper().setSerializationInclusion(Include.NON_NULL);
		    apiProduct = mapper.readValue(jsonObject.toString(), ApiProduct.class);
        } catch(UnrecognizedPropertyException e) {
			apiResponse.setMessage(e.getMessage().substring(0, e.getMessage().indexOf("(")));
			response.setStatus(400);
			return apiResponse;	
		} catch(Exception e){
        	e.printStackTrace();
        	apiResponse.setMessage("Error reading input data");
			response.setStatus(400);
			return apiResponse;
        }
        
      if(apiProduct.getSku() == null || apiProduct.getSku().isEmpty()){
    	  apiResponse.setMessage("Please specify the sku");
			response.setStatus(400);
			return apiResponse;
      }
      
      //  Product product = new Product (apiProduct);
    
        //check if product exists and set the id 
    	Integer id = this.webJaguar.getProductIdBySku(apiProduct.getSku());
    	Product product=null;
    	
		if(id != null){
		//	existingProduct= this.webJaguar.getProductById(id, request);
			product = this.webJaguar.getProductById(id, 0, true, null);
			
			if(apiProduct.isNegInventory()==null) {
				apiProduct.setNegInventory(product.isNegInventory());
			}
			if(apiProduct.isShowNegInventory()==null) {
				apiProduct.setShowNegInventory(product.isShowNegInventory());
			}
			
			
		}else{
			if(apiProduct.isCreateOnNotFound()) {
				apiResponse=this.handelRequest(request, response,jsonObject);
				return apiResponse;
			}
			apiResponse.setMessage("Product not found");
			response.setStatus(400);
			return apiResponse;
		}
		
		product.setProduct(apiProduct);
		
        if((apiProduct.getCategoryIds()!=null && !apiProduct.getCategoryIds().equals(""))){
        	product.setCatIds(setCatids(new Product(apiProduct)));
        }
        
        if (apiProduct.getLang() != null && !apiProduct.getLang().trim().isEmpty()) {
        	if (apiProduct.getLang().trim().equals("es")) {
        		Product i18nProduct = new Product();
        		i18nProduct.setLang("es");
        		if (apiProduct.getI18nName() != null && !apiProduct.getI18nName().isEmpty()) {
        			i18nProduct.setI18nName(apiProduct.getI18nName());
        		}
        		if (apiProduct.getI18nLongDesc() != null && !apiProduct.getI18nLongDesc().isEmpty()) {
        			String i18nLongDesc = apiProduct.getI18nLongDesc();
        			if (i18nLongDesc.length() >= 65535) {
        				i18nLongDesc = apiProduct.getI18nLongDesc().substring(0, 65535);
        			}
        			i18nProduct.setI18nLongDesc(i18nLongDesc);
        		}
        		if (apiProduct.getI18nShortDesc() != null && !apiProduct.getI18nShortDesc().isEmpty()) {
        			i18nProduct.setI18nShortDesc(apiProduct.getI18nShortDesc());
        		}
        		if (apiProduct.getI18nField16() != null && !apiProduct.getI18nField16().isEmpty()) {
        			i18nProduct.setField16(apiProduct.getI18nField16());
        		}
        		if (apiProduct.getI18nField17() != null && !apiProduct.getI18nField17().isEmpty()) {
        			i18nProduct.setField17(apiProduct.getI18nField17());
        		}
        		if (apiProduct.getI18nField18() != null && !apiProduct.getI18nField18().isEmpty()) {
            		i18nProduct.setField18(apiProduct.getI18nField18());
        		}
        		if (apiProduct.getI18nField69() != null && !apiProduct.getI18nField69().isEmpty()) {
            		i18nProduct.setField69(apiProduct.getI18nField69());
        		}
                product.addI18nProduct("es", i18nProduct);
        	}
        }
        
        product.setLastModifiedBy("API");

        try{
        	
	        this.webJaguar.updateApiProduct(product, null);
	        
	    	apiResponse.setMessage("Product "+product.getSku() +" updated");
	    	Product updatedProduct =this.webJaguar.getProductById(product.getId(), 0, true, null);
	    	updatedProduct.setI18nProduct(this.webJaguar.getI18nProduct(product.getId()));		
	    	apiResponse.setProduct(new ApiProduct(updatedProduct));
	    	response.setStatus(200);
        } catch(Exception e){
        	e.printStackTrace();
        	apiResponse.setMessage("Error in Update. Please check your input");
			response.setStatus(400);
			logAPIError("updateProduct", e);
			return apiResponse;
        }
        logAPISuccess("updateProduct");
		return apiResponse;
	}
	
	// get category ids
	public Set<Object>setCatids(Product product){
		Set<Object> categories = new HashSet<Object>();
		if (product.getCategoryIds() != null && product.getCategoryIds().length() > 0) {
			String[] categoryIds = product.getCategoryIds().split(",");
			for (int x = 0; x < categoryIds.length; x++) {
				if (!("".equals(categoryIds[x].trim()))) {
					categories.add(Integer.valueOf(categoryIds[x].trim()));
				}
			}
		}
		return categories;
		
	}	
	
	private void logAPI(String api) {
		try {
			String apiString = "WJ_API_"+api;
			MDC.put(apiString, "1");
			LOG.info(apiString+ " metrics", "1");
			MDC.remove(apiString);
		} catch (Exception e) {}
	}
	
	private void logAPISuccess(String api) {
		try {
			String apiString = "WJ_API_"+api+"_success";
			MDC.put(apiString, "1");
			LOG.info(apiString+" metrics", "1");
			MDC.remove(apiString);
		} catch (Exception e) {}
	}
	
	private void logAPIError(String api, Exception e) {
		try {
			String apiString = "WJ_API_"+api+"_error";
			MDC.put(apiString, "1");
			LOG.error(apiString+" metrics", e);
			MDC.remove(apiString);
		} catch (Exception ex) {}
	}
}
