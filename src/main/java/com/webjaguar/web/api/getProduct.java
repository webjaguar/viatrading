package com.webjaguar.web.api;


import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.api.ApiProduct;
import com.webjaguar.web.domain.Encoder;

@Controller
public class getProduct extends WebApplicationObjectSupport{
	
	@Autowired
	private WebJaguarFacade webJaguar;
	protected Map<String, Configuration> siteConfig;
	private static final Logger LOG = LoggerFactory.getLogger(getProduct.class);
	
	public boolean validateRequest(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		final String authorization = request.getHeader("Authorization");
		if (authorization != null && authorization.startsWith("Basic")) {
	        try{
				// Authorization: Basic base64credentials
		        String base64Credentials = authorization.substring("Basic".length()).trim();
		        String credentials = Encoder.decode(base64Credentials);
		        if(credentials.equalsIgnoreCase(siteConfig.get("WJ_API").getValue())){
		        	return true;
		        } else {
		        	return false;
			    }
		    } catch(Exception e) {
				return false;
			}
		}
		return false;
	}
	
	@RequestMapping(value="/api/getProduct.jhtm", method = RequestMethod.GET, produces={"application/json"})
    public @ResponseBody ApiResponse handelRequest(HttpServletRequest request,  HttpServletResponse response, @RequestParam(value = "id", required = false) Integer productId, @RequestParam(value = "sku", required = false) String sku) throws Exception {
		logAPI("getProduct");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		ApiResponse apiResponse= new ApiResponse();
		
		if( !this.validateRequest(request)) {
			apiResponse.setMessage("Invalid Username and Password.");
			return apiResponse;
		}
		
		if(productId==null && sku ==null ) {
			apiResponse.setMessage("id and sku are null");
			return apiResponse;
		} 
		
		if(sku!=null && !sku.equals("")) {
			productId = this.webJaguar.getProductIdBySku(sku);
		}
		
		Product wjProduct = this.webJaguar.getProductById(productId, request);
		ApiProduct apiProduct = null;
		if(wjProduct != null) {
			apiProduct = new ApiProduct(wjProduct);			
			logAPISuccess("getProduct");
		}else {
			apiResponse.setMessage("No Product Found");
			response.setStatus(400);
			return apiResponse;
		}
		
		apiResponse.setProduct(apiProduct);
		System.out.println("Line 40 ");
		return apiResponse;
	}
	
	private void logAPI(String api) {
		try {
			String apiString = "WJ_API_"+api;
			MDC.put(apiString, "1");
			LOG.info(apiString+ "metrics", "1");
			MDC.remove(apiString);
		} catch (Exception e) {}
	}
	
	private void logAPISuccess(String api) {
		try {
			String apiString = "WJ_API_"+api+"_success";
			MDC.put(apiString, "1");
			LOG.info(apiString+" metrics", "1");
			MDC.remove(apiString);
		} catch (Exception e) {}
	}
	
	@SuppressWarnings("unused")
	private void logAPIError(String api, Exception e) {
		try {
			String apiString = "WJ_API_"+api+"_error";
			MDC.put(apiString, "1");
			LOG.error(apiString+" metrics", e);
			MDC.remove(apiString);
		} catch (Exception ex) {}
	}
}
