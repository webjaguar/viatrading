package com.webjaguar.web.api;

import java.io.Serializable;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.webjaguar.model.api.ApiCustomer;
import com.webjaguar.model.api.ApiCartItem;
import com.webjaguar.model.api.ListApiCartItem;

@JsonSerialize(include=Inclusion.NON_NULL)
public class ApiViewCartResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String message;
	
	private List<ViewCartResponse> listViewCartItem;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<ViewCartResponse> getListViewCartItem() {
		return listViewCartItem;
	}

	public void setListViewCartItem(List<ViewCartResponse> listViewCartItem) {
		this.listViewCartItem = listViewCartItem;
	}
	
}
