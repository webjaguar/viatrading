package com.webjaguar.web.api;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.UserSession;

public class UserIDApi implements Controller{
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
    	Map<String, Object> model = new HashMap<String, Object>();
    	Integer id = null;
    	UserSession userSession = this.webJaguar.getUserSession(request);	
    	if ( userSession != null ){
		Customer customer = (Customer) request.getAttribute("sessionCustomer");
		id = customer.getId();
    	}
		System.out.println("id--:"+id);
    	model.put("id",id);
		return new ModelAndView("frontend/layout/template6/account/UserID", "model", model);	
	}	
}
	
	
    