package com.webjaguar.web.api;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.TruckLoadProcess;
import com.webjaguar.model.TruckLoadProcessProducts;
import com.webjaguar.model.api.ApiProduct;
import com.webjaguar.model.api.ApiTruckLoadProcess;
import com.webjaguar.model.api.ApiTruckLoadProcessProduct;
import com.webjaguar.web.api.authentication.ApiAuthenticationImp;

@Controller
public class InsertTruckLoadProcess {
	@Autowired
	private WebJaguarFacade webJaguar;
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	@RequestMapping(value="/api/insertTruckLoadProcess.jhtm", method = RequestMethod.POST)
	 public @ResponseBody ApiResponse handelRequest(HttpServletRequest request,  HttpServletResponse response){
		siteConfig = this.webJaguar.getSiteConfig();
		ApiResponse apiResponse = new ApiResponse();
		
		ApiAuthenticationImp authenticate = new ApiAuthenticationImp();
		if(!authenticate.validateRequest(request)) {
			apiResponse.setMessage("Invalid Username and Password.");
			response.setStatus(401);
			return apiResponse;
		}		
		
		ApiTruckLoadProcess apiTruckLoadProcess = new ApiTruckLoadProcess();
		//Read the Json data
		try{
			InputStreamReader reader = new InputStreamReader(request.getInputStream());
	        JSONTokener tokenizer = new JSONTokener(reader);
	        JSONObject jsonObject = new JSONObject(tokenizer);
	        
	        apiTruckLoadProcess = new ObjectMapper().readValue(jsonObject.toString(), ApiTruckLoadProcess.class);
		} catch(Exception e){
			apiResponse.setMessage("Error : Can't Read JSON data");
			e.printStackTrace();
			response.setStatus(400);
			return apiResponse;
		}
		
		
		TruckLoadProcess truckLoadProcess = new TruckLoadProcess();
		
		
		if (apiTruckLoadProcess != null) {					
			// Name
			if (apiTruckLoadProcess.getName() != null && !apiTruckLoadProcess.getName().isEmpty()) {
				truckLoadProcess.setName(apiTruckLoadProcess.getName());
			}
			
			// Original Product
			ArrayList<Product> wjProductList = new ArrayList<Product>();
			if (apiTruckLoadProcess.getOriginalProduct() != null) {
				for (ApiProduct apiProduct : apiTruckLoadProcess.getOriginalProduct()) {
					Product wjProduct = new Product(apiProduct);
					wjProductList.add(wjProduct);
				}								
				truckLoadProcess.setOriginalProduct(wjProductList);
			}
			
			// Delivered Product
			ArrayList<TruckLoadProcessProducts> wjTruckLoadProcessProductsList = new ArrayList<TruckLoadProcessProducts>();
			if (apiTruckLoadProcess.getDerivedProduct() != null) {
				for (ApiTruckLoadProcessProduct apiTruckLoadProcessProduct : apiTruckLoadProcess.getDerivedProduct()) {
					TruckLoadProcessProducts wjTruckLoadProcessProducts = new TruckLoadProcessProducts(apiTruckLoadProcessProduct); 
					wjTruckLoadProcessProductsList.add(wjTruckLoadProcessProducts);				
				}				
				truckLoadProcess.setDerivedProduct(wjTruckLoadProcessProductsList);
			}
			
			// Custom 1
			if (apiTruckLoadProcess.getCustom1() != null) {
				truckLoadProcess.setCustom1(apiTruckLoadProcess.getCustom1());
			}
			
			// Custom 2
			if (apiTruckLoadProcess.getCustom2() != null) {
				truckLoadProcess.setCustom2(apiTruckLoadProcess.getCustom2());
			}
			
			// Custom 3
			if (apiTruckLoadProcess.getCustom3() != null) {
				truckLoadProcess.setCustom3(apiTruckLoadProcess.getCustom3());
			}
			
			// Custom 4
			if (apiTruckLoadProcess.getCustom4() != null) {
				truckLoadProcess.setCustom4(apiTruckLoadProcess.getCustom4());
			}
			
			// PT
			if (apiTruckLoadProcess.getPt() != null) {
				truckLoadProcess.setPt(apiTruckLoadProcess.getPt());
			}
			
			// Status
			if (apiTruckLoadProcess.getStatus() != null) {
				truckLoadProcess.setStatus(apiTruckLoadProcess.getStatus());
			}
			
			// CreatedBy
			if (apiTruckLoadProcess.getCreatedBy() != null) {
				truckLoadProcess.setCreatedBy(apiTruckLoadProcess.getCreatedBy());
			}
			
			// Type
			if (apiTruckLoadProcess.getType() != null) {
				truckLoadProcess.setType(apiTruckLoadProcess.getType());
			}
			
			// Team Hours
			if (apiTruckLoadProcess.getTeamHours() != null) {
				truckLoadProcess.setTeamHours(apiTruckLoadProcess.getTeamHours());
			}
			
			// Team Cost
			if (apiTruckLoadProcess.getTeamCost() != null) {
				truckLoadProcess.setTeamCost(apiTruckLoadProcess.getTeamCost());
			}
			
			// Team Cost
			if (apiTruckLoadProcess.getTeamCost() != null) {
				truckLoadProcess.setTeamCost(apiTruckLoadProcess.getTeamCost());
			}
			
			// Num of Team Members
			if (apiTruckLoadProcess.getNumOfTeamMembers() != null) {
				truckLoadProcess.setNumOfTeamMembers(apiTruckLoadProcess.getNumOfTeamMembers());
			}
			
			// Date Started
			if (apiTruckLoadProcess.getDateStarted() != null) {
				truckLoadProcess.setDateStarted(apiTruckLoadProcess.getDateStarted());
			}
			
			// Date Ended
			if (apiTruckLoadProcess.getDateEnded() != null) {
				truckLoadProcess.setDateEnded(apiTruckLoadProcess.getDateEnded());
			}			
		}
		
        try{            
            this.webJaguar.insertTruckLoadProcess(truckLoadProcess);
            apiResponse.setMessage("Success");
            response.setStatus(200);
         } catch(Exception e){
         	e.printStackTrace();
         	apiResponse.setMessage("Error : Exception in INSERT");
         	response.setStatus(400);
         }
			
		return apiResponse;
				
	}	
}
