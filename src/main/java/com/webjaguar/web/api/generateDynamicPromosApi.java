package com.webjaguar.web.api;

import java.io.InputStreamReader;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Promo;
import com.webjaguar.model.api.ApiPromo;
import com.webjaguar.model.api.PromosApi;
import com.webjaguar.web.api.authentication.ApiAuthenticationImp;

@Controller
public class generateDynamicPromosApi extends WebApplicationObjectSupport {

	@Autowired
	private WebJaguarFacade webJaguar;
	protected Map<String, Configuration> siteConfig;
	protected Map<String, Object> gSiteConfig;

	@RequestMapping(value="/api/generateDynamicPromos.jhtm", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ApiResponse handelRequest(HttpServletRequest request,  HttpServletResponse response) throws Exception {
		
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
        String message="";
        ApiResponse apiResponse = new ApiResponse();
        // authentication
        ApiAuthenticationImp authenticate = new ApiAuthenticationImp();
		if(!authenticate.validateRequest(request)) {
			message = "Invalid Username and Password.";
			response.setStatus(401);
			apiResponse.setMessage(message);
			return apiResponse;
		}
		
	    PromosApi promos = new PromosApi();

    	//Read the Json data
		
		try {
			  InputStreamReader reader = new InputStreamReader(request.getInputStream());
              JSONTokener tokenizer = new JSONTokener(reader);
              JSONObject jsonObject = new JSONObject(tokenizer);
              promos = (PromosApi) new ObjectMapper().readValue(jsonObject.toString(), PromosApi.class);
		} catch(Exception e) {
//			System.out.println(e);
           	message = "Error : Can't Read JSON data ";
			response.setStatus(400);
			apiResponse.setMessage(message);
			return apiResponse;
        }
		
		if(promos.getDynamicPromoList().size() > 100){
			message = "Error : Please update the list in batch of 100";
			response.setStatus(400);
			apiResponse.setMessage(message);
			return apiResponse;
		}
		
		List<DynamicPromoResponse> list = new ArrayList<DynamicPromoResponse>();
		List<Promo> listOfPromo = new ArrayList<Promo>();
	
		for(ApiPromo apiPromo : promos.getDynamicPromoList()) {
			if(apiPromo.getDiscount() == -1 || (apiPromo.getDiscount() > 100 && apiPromo.isPercent())  ) {
				DynamicPromoResponse dynamicPromoResponse = new DynamicPromoResponse();
				dynamicPromoResponse.setIndex(apiPromo.getIndex()); 
				dynamicPromoResponse.setMessage("Error: Discount is Mandatory! and max is 100%");
				list.add(dynamicPromoResponse);
				
			}else if(apiPromo.getStartDate() == null || apiPromo.getEndDate() == null ) {
//				System.out.println(apiPromo.getStartDate() + "Allam");
				DynamicPromoResponse dynamicPromoResponse = new DynamicPromoResponse();
				dynamicPromoResponse.setIndex(apiPromo.getIndex()); 
				dynamicPromoResponse.setMessage("Error: endDate/StartDate are Mandatory!");
				list.add(dynamicPromoResponse);
			}else {
//				System.out.println(apiPromo.getStartDate());
			Promo promo = new Promo();
			promo.setUserId(apiPromo.getUserId());
			promo.setDiscount(apiPromo.getDiscount());
			promo.setTitle(apiPromo.getPrefix()+generateRandomString());
			promo.setDynamic(true);
			promo.setPercent(apiPromo.isPercent());
			promo.setOnetimeUsePerCustomer(apiPromo.isOnetimeUsePerCustomer());
			promo.setOnetimeUseOnly(apiPromo.isOnetimeUseOnly());
			promo.setpromoForNewCustomersOnly(apiPromo.isPromoForNewCustomersOnly());
			promo.setMinOrder(apiPromo.getMinOrder());
			promo.setStartDate(apiPromo.getStartDate());
			promo.setEndDate(apiPromo.getEndDate());
			promo.setDiscountType(apiPromo.getDiscountType());
			promo.setMinQty(apiPromo.getMinQty());
			promo.setCreatedBy("Api");

			if(!promo.getDiscountType().equalsIgnoreCase("product")) {
				promo.setBrands(null); 
				promo.setSkus(null);
				promo.setMinOrderPerBrand(null);
			}
			
			// get group ids
			Set<Object> groups = new HashSet<Object>();
			if (apiPromo.getGroupIds() != null) {
				System.out.println("Allam");
				String[] groupIds = apiPromo.getGroupIds().split( "," );
				for ( int x = 0; x < groupIds.length; x++ ) {
					if ( !("".equals( groupIds[x].trim() )) ) {
						groups.add( Integer.valueOf( groupIds[x].trim() ) );
					}
				}
				promo.setGroupIdSet( groups );
			}	
			
			promo.setGroupType(apiPromo.getGroupType());
			
			// get states
			promo.setStates(apiPromo.getStates());
			promo.setStateType(apiPromo.getStateType());
			
			// get groups 
			promo.setGroups(apiPromo.getGroups());
			
			// get group Note
			promo.setGroupNote(apiPromo.getGroupNote());
			
			// get skus
			Set<Object> skus = null;
			if (apiPromo.getSkus() != null) {
				skus = new HashSet<Object>();
				String[] sku = apiPromo.getSkus().split( "," );
				for ( int x = 0; x < sku.length; x++ ) {
					if ( !("".equals( sku[x].trim() )) ) {
						skus.add( sku[x].trim() );
					}
				}
			}
			promo.setSkuSet( skus );

			
			//get parent skus
	      Set<Object> parentSkus = null;
			
			if (apiPromo.getParentSkus() != null) {
				parentSkus = new HashSet<Object>();
				String[] parents = apiPromo.getParentSkus().split( "," );
				for ( int x = 0; x < parents.length; x++ ) {
					if ( !("".equals( parents[x].trim() )) ) {
						parentSkus.add( parents[x].trim() );
					}
				}
			}
			
			promo.setParentSkuSet(parentSkus);
			
			
			// get brands 
			// if minimum order per brand is available, only one brand will be allowed
			Set<Object> brands = null;
			if (apiPromo.getBrands() != null) {
				brands = new HashSet<Object>();
				if(apiPromo.getMinimumOrderPerBrands() == -1) {
					String[] brand = apiPromo.getBrands().split( "," );
					for ( int x = 0; x < brand.length; x++ ) {
						if ( !("".equals( brand[x].trim() )) ) {
							brands.add( brand[x].trim() );
						}
					}
					System.out.println(brands.toString() + "------");

				}else {
					String[] brand = apiPromo.getBrands().split( "," );
					for ( int x = 0; x < 1; x++ ) {
						if ( !("".equals( brand[x].trim() )) ) {
							brands.add( brand[x].trim() );
						}
					}
					System.out.println(brands.toString() + "------");
				}
			}
			promo.setBrandSet( brands );

			promo.setPrefix(apiPromo.getPrefix());
			promo.setCreatedBy(apiPromo.getCreatedBy());
//			promo.setCreatedOn(apiPromo.getCreatedOn());
			promo.setCreatedOn(Calendar.getInstance().getTime());
//			System.out.println(Calendar.getInstance().getTime());
			
			DynamicPromoResponse dynamicPromoResponse = new DynamicPromoResponse();
			dynamicPromoResponse.setPromoCode(promo.getTitle());
			dynamicPromoResponse.setUserId(promo.getUserId());
			dynamicPromoResponse.setIndex(apiPromo.getIndex()); 
			list.add(dynamicPromoResponse);
			listOfPromo.add(promo);
		 }
		}
		try {
			 this.webJaguar.insertPromos(listOfPromo);
//			System.out.println(j);
		    }catch(Exception e) {
//			System.out.println(e);
		  }
		
		apiResponse.setdPromos(list);

		return apiResponse;
	}
	
	private String generateRandomString() {
		char[] chars = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 10; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		String output = sb.toString();
		return output;
	}
}
