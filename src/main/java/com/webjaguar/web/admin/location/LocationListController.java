/* Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.22.2009
 */

package com.webjaguar.web.admin.location;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.LocationSearch;

public class LocationListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		if ( request.getParameter( "__add" ) != null ) {
			return new ModelAndView( new RedirectView( "locationForm.jhtm" ) );
		}
		
		LocationSearch locationSearch = getLocationSearch( request );
		Map<String, Object> model = new HashMap<String, Object>();
		
		// get location
		PagedListHolder locationList = new PagedListHolder( this.webJaguar.getLocationList(locationSearch) );
		locationList.setPageSize( locationSearch.getPageSize() );
		locationList.setPage( locationSearch.getPage() - 1 );
		model.put( "locations", locationList );

		return new ModelAndView( "admin/location/list", "model", model );
	}
	
	private LocationSearch getLocationSearch(HttpServletRequest request) {
		LocationSearch locationSearch = (LocationSearch) request.getSession().getAttribute( "locationSearch" );
		if (locationSearch == null) {
			locationSearch = new LocationSearch();
			request.getSession().setAttribute( "locationSearch", locationSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			locationSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}	

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				locationSearch.setPage( 1 );
			} else {
				locationSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				locationSearch.setPageSize( 10 );
			} else {
				locationSearch.setPageSize( size );				
			}
		}	
		return locationSearch;
	}
}
