/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.23.2007
 */

package com.webjaguar.web.admin.location;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.mail.MailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.Location;

public class ImportController extends WebApplicationObjectSupport implements Controller {
	
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	private HSSFFormulaEvaluator evaluator;
	
	String formView = "admin/location/import";
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {	
        
		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			return new ModelAndView("admin/location/import", "message", "excelfile.notWritable");	
		}    	
    	
		// check if upload button was pressed
		if (request.getParameter("__upload") != null) {
	    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	    	MultipartFile file = multipartRequest.getFile("file");
	    	if (file != null && !file.isEmpty()) {
	    		File excel_file = new File(baseFile, "locations_import.xls");
	    		file.transferTo(excel_file);
	    		file = null; // delete to save resources
	    		
	    		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
	    		List<Location> importedlocations = new ArrayList<Location>();
	        	try {
	                List<Map<String, Object>> addedItems = new ArrayList<Map<String, Object>>();
	                List<Map<String, Object>> updatedItems = new ArrayList<Map<String, Object>>();
	                Map<String, Object> map = new HashMap<String, Object>();
	        		if (processFile(excel_file, gSiteConfig, map, importedlocations, addedItems, updatedItems)) {
	    				this.webJaguar.insertImportExportHistory( new ImportExportHistory("product", SecurityContextHolder.getContext().getAuthentication().getName(), true ));
	    				
	    				for (Location location: importedlocations) {
	        				if(location.getId() == null) {
		        				this.webJaguar.insertLocation(location);
	        				} else {
                				this.webJaguar.updateLocation(location);
                			}        					
	        			}
	        			if (updatedItems.size() > 0) map.put("updatedItems", updatedItems);	    				
	        			if (addedItems.size() > 0) map.put("addedItems", addedItems);	        			
	        			return new ModelAndView("admin/location/importSuccess", map);
	        		} else {
	        			return new ModelAndView(formView, map);
	        		}
	        	} catch (IOException e) {
	    			return new ModelAndView(formView, "message", "excelfile.invalid");
	    		} finally {
	    	    	// delete excel file
	    			excel_file.delete();	    			
	    		} 
	    	} else {
	        	return new ModelAndView(formView, "message", "excelfile.invalid");	    		
	    	}
		}    	
    	return new ModelAndView(formView);
    }
    
    private boolean processFile(File excel_file, Map<String, Object> gSiteConfig, 
    		Map<String, Object> map, List<Location> importedLocations, List<Map<String, Object>> addedItems, List<Map<String, Object>> updatedItems) throws IOException, Exception {
    	
    	
    	List<Map<String, Object>> invalidItems = new ArrayList<Map<String, Object>>();
    	
    	try {
    		
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(excel_file));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
		    
			HSSFSheet sheet = wb.getSheetAt(0); // first sheet
			HSSFRow row     = sheet.getRow(0);  // first row
			
			evaluator = new HSSFFormulaEvaluator(sheet, wb);
			
			Map<String, Short> header = new HashMap<String, Short>();
			Set<String> emails = new HashSet<String>();
			Set<Short> category = new HashSet<Short>();
			
			if (wb.getNumberOfSheets() > 1) {
				// multiple sheets
				map.put("message", "excelfile.multipleSheets");
				return false;					
			}
			if (row == null) {
				// first line should be the headers
				map.put("message", "excelfile.emptyHeaders");
				return false;				
			}
			
		    Iterator iter = row.cellIterator();
		    
		    // get header names
			while (iter.hasNext()) {
				HSSFCell cell = (HSSFCell) iter.next();
				String headerName = "";
				try {
					headerName = cell.getStringCellValue().trim();					
				} catch (NumberFormatException e) {
					// do nothing
				}
				if (!headerName.equals( "" )) {
						if (header.put(headerName.toLowerCase(), cell.getCellNum()) != null) {
							// duplicate header
							map.put("message", "excelfile.duplicateHeaders");
							map.put("arguments", headerName);
							return false;														
					}
				} 
			}
			
			// check required headers
			if (!header.containsKey("address1") || !header.containsKey("city") || !header.containsKey("stateprovince")) { // required fields
				map.put("message", "excelfile.missingHeaders");
				return false;
			} 		
	    	
			
			nextRow: for (int i=1; i<=sheet.getLastRowNum(); i++) {
				row = sheet.getRow(i);
				if (row == null) {
					continue;
				}	
	
				Map<String, Object> thisMap = new HashMap<String, Object>();
				
				//required fields
				String address1 = null;				
				address1 = getStringValue(row, header.get("address1")).trim();
				if(address1 == "") {
					address1 = null;
				}
				String city = null;
				city = getStringValue(row, header.get("city")).trim();
				if(city == "") {
					city = null;
				}
				String stateProvince = null;
				stateProvince = getStringValue(row, header.get("stateprovince")).trim();
				if(stateProvince == "") {
					stateProvince = null;
				}
				
				if(address1 != null && city != null && stateProvince != null) {
					Integer id = null;
					Location location = new Location();
					
					if (header.containsKey("id")) {
						try {
							id = getIntegerValue(row, header.get("id"));
						} catch (Exception e) {
							thisMap.put("id", getStringValue(row, header.get("id")));
							thisMap.put("reason", "Invalid loaction ID");
							invalidItems.add(thisMap);
							continue nextRow;
						}	
						if (id != null && !this.webJaguar.isValidLocationId(id)) { // id not valid
								thisMap.put("id", id);
								thisMap.put("reason", "Invalid location ID");
								invalidItems.add(thisMap);
								continue nextRow;							
						} else {
							processRow(row, header, location);
							thisMap.put("locationname", getStringValue(row, header.get("location")));			
							updatedItems.add(thisMap);
						}
					} else {
						processRow(row, header, location);
						thisMap.put("locationname", getStringValue(row, header.get("location")));			
						addedItems.add(thisMap);
					}		
					importedLocations.add(location);
				}
			}
			
			 if (importedLocations.size() == 0) {	// no locations to import
				map.put("message", "excelfile.empty");
				return false;			
			}		
			return true;
			
        } catch (Exception e) {
        	map.put("message", "excelfile.version");
	    	return false;
        }
	    	  
    }

    private Integer getIntegerValue(HSSFRow row, short cellnum) throws Exception {
		HSSFCell cell = row.getCell(cellnum);
    	Integer value = null;
    	
    	if (cell != null) {
    		switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = new Integer(cell.getStringCellValue());										
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Double cellValue = cell.getNumericCellValue();
					if (cellValue != cellValue.intValue()) {
						throw new Exception();	
					}
					value =  cellValue.intValue();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue formulaCellValue = evaluator.evaluate(cell);					
					if (formulaCellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						cellValue =  new Double(formulaCellValue.getNumberValue());
						if (cellValue != cellValue.intValue()) {
							throw new Exception();	
						}
						value =  cellValue.intValue();
					}
					break;
    		} // switch
    	}
    	return value;
    }
    
    private String getStringValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		String value = "";

    	if (cell != null) {
			switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = cell.getStringCellValue();
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Integer intValue = null;
					try {
						intValue = getIntegerValue(row, cellnum);
					} catch (Exception e) {}
					if (intValue != null) {
						value = intValue.toString();
						break;
					}
					value = new Double(cell.getNumericCellValue()).toString();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue cellValue = evaluator.evaluate(cell);
					if (cellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						value = new Double(cellValue.getNumberValue()).toString();						
					} else if (cellValue.getCellType() == HSSFCell.CELL_TYPE_STRING) {
						value = cellValue.getStringValue();
					}
					break;
			
			} // switch		
    	}
		return value;
    }    
 
    private void processRow(HSSFRow row, Map<String, Short> header, Location location) throws Exception {
    	NumberFormat formatter = new DecimalFormat("#");
    	
    	if(header.containsKey("id")) {
    		location.setId(getIntegerValue(row, header.get("id")));    		
    	}
    	if(header.containsKey("location")) {
    		location.setName(getStringValue(row, header.get("location")) );    		
    	}
    	if(header.containsKey("address1")) {
    		location.setAddr1(getStringValue(row, header.get("address1")) );    		
    	}
    	if(header.containsKey("address2")) {
    		location.setAddr2(getStringValue(row, header.get("address2")) );    		
    	}
    	if(header.containsKey("country")) {
    		location.setCountry(getStringValue(row, header.get("country")) );    		
    	}
    	if(header.containsKey("city")) {
    		location.setCity(getStringValue(row, header.get("city")) );    		
    	}
    	if(header.containsKey("stateprovince")) {
    		location.setStateProvince(getStringValue(row, header.get("stateprovince")) );    		
    	}
    	if(header.containsKey("zip")) {
    		location.setZip(getStringValue(row, header.get("zip")) );    		
    	}    	
    	if(header.containsKey("phone")) {
    		try {
    			if(row.getCell(header.get("phone")).getNumericCellValue() != 0) {
    				location.setPhone( formatter.format(row.getCell(header.get("phone")).getNumericCellValue()) );
    			} else{
    				location.setPhone( getStringValue(row, header.get("phone")) );
    			}
    		} catch (Exception e) {
    			location.setPhone(getStringValue(row, header.get("phone")) );
    		}
    	}
    	if(header.containsKey("fax")) {
    		location.setFax(getStringValue(row, header.get("fax")) );    		
    	}
    	if(header.containsKey("keywords")) {
    		location.setKeywords(getStringValue(row, header.get("keywords")) );    		
    	}
    	if(header.containsKey("note")) {
    		location.setHtmlCode(getStringValue(row, header.get("note")) );    		
    	}    	
    }  
}