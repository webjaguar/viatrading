/* Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.22.2009
 */

package com.webjaguar.web.admin.location;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.LocationForm;
import com.webjaguar.logic.WebJaguarFacade;

public class LocationFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public LocationFormController() {
		setSessionForm(true);
		setCommandName("locationForm");
		setCommandClass(LocationForm.class);
		setFormView("admin/location/form");
		setSuccessView("locationList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {

		LocationForm locationForm = (LocationForm) command;
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteLocation( locationForm.getLocation().getId() );			
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		if (locationForm.isNewLocation()) {
			this.webJaguar.insertLocation( locationForm.getLocation());
		} else {
			this.webJaguar.updateLocation( locationForm.getLocation());
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		LocationForm locationForm = new LocationForm();
		if ( request.getParameter("id") != null ) {
			locationForm = new LocationForm(this.webJaguar.getLocationById( new Integer(request.getParameter("id"))), false);
		} else {
			locationForm = new LocationForm();
		}
		
		return locationForm;
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		LocationForm locationForm = (LocationForm) command;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location.addr1", "form.required", "Address 1 is required.");    	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location.city", "form.required", "City is required.");  
		if (locationForm.getLocation().getCountry().equalsIgnoreCase("US") || locationForm.getLocation().getCountry().equalsIgnoreCase("CA")) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "location.stateProvince", "form.required", "State is required.");
		}
	}
	
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
    	LocationForm locationForm = (LocationForm) command;

    	Map<String, Object> map = new HashMap<String, Object>();
		map.put("countries",this.webJaguar.getEnabledCountryList());
		map.put("states",this.webJaguar.getStateList("US"));
		map.put("caProvinceList", this.webJaguar.getStateList("CA"));
		return map;
    }
}