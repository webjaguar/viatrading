package com.webjaguar.web.admin.report;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SalesRepReport;
import com.webjaguar.web.domain.Constants;


public class SalesRepReportController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ReportFilter salesRepFilter = getReportFilter( request );
		Map<String, Object> myModel = new HashMap<String, Object>();
		List<List<SalesRepReport>> salesRepReports = new ArrayList();

		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = null;
			
			Integer salesRepId = null;
			if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
			}

			if (salesRepId == null) {
				salesReps = this.webJaguar.getSalesRepList();				
			} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
				salesReps = this.webJaguar.getSalesRepList(salesRepId);
			} else {
				salesReps = new ArrayList<SalesRep>();
				salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
			}
			for (SalesRep salesRep: salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);
			}
			if (salesRepId != null) {
				// make sure access users sees only his customers
				if (salesRepFilter.getSalesRepId() == -1) {
					salesRepFilter.setSalesRepId(salesRepId);
				} else if (!salesRepMap.containsKey(salesRepFilter.getSalesRepId())) {
					salesRepFilter.setSalesRepId(salesRepId);				
				}
				// associate reps to parents
				for (SalesRep salesRep: salesReps) {
					if (salesRepMap.containsKey(salesRep.getParent())) {
						salesRepMap.get(salesRep.getParent()).getSubAccounts().add(salesRep);					
					}
				}
				myModel.put("salesRepTree", salesRepMap.get(salesRepId));
				
				List<SalesRepReport> salesRepReportsById = this.webJaguar.getSalesRepReport(salesRepFilter.getSalesRepId(), salesRepFilter);
				salesRepReports.add( salesRepReportsById );
			} else {
				if(salesRepFilter.getSalesRepId() != -1) {
					List<SalesRepReport> salesRepReportsById = this.webJaguar.getSalesRepReport(salesRepFilter.getSalesRepId(), salesRepFilter);
					salesRepReports.add( salesRepReportsById );
				} else {
					for (SalesRep salesRep: salesReps) {
						List<SalesRepReport> salesRepReportsById = this.webJaguar.getSalesRepReport(salesRep.getId(), salesRepFilter);
						salesRepReports.add( salesRepReportsById );
					}
				}
			}			
			
			myModel.put("salesRepMap", salesRepMap);
			myModel.put("salesReps", salesReps);
		}
		myModel.put( "salesRepReports", salesRepReports );
		myModel.put( "salesRepFilter", salesRepFilter );
		myModel.put( "yearList", Constants.getYears( 5, 5 ) );
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/report/salesReps", "model", myModel);
		}
        return new ModelAndView("admin/report/salesReps", "model", myModel);
	}

	private ReportFilter getReportFilter(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		ReportFilter salesRepFilter = (ReportFilter) request.getSession().getAttribute( "salesRepFilter" );
		if (salesRepFilter == null) {
			salesRepFilter = new ReportFilter();
			salesRepFilter.setYear( "" + Calendar.getInstance().get( Calendar.YEAR ) );
			request.getSession().setAttribute( "salesRepFilter", salesRepFilter );
		}
		
		// quarter
		if (request.getParameter("quarter") != null) {
			salesRepFilter.setQuarter( ServletRequestUtils.getStringParameter( request, "quarter", "1" ));
		}
		
		// year
		if (request.getParameter("year") != null) {
			salesRepFilter.setYear( ServletRequestUtils.getStringParameter( request, "year", "" ));
		}
		
		// Backend Order
		if (request.getParameter("backendOrder") != null) {
			salesRepFilter.setBackendOrder( ServletRequestUtils.getStringParameter( request, "backendOrder", "" ));
		}
		
		// shipping Method
		if (request.getParameter("shippingMethod") != null) {
			salesRepFilter.setShippingMethod( ServletRequestUtils.getStringParameter( request, "shippingMethod", "" ));
		}
		
		// Date Type
		if (request.getParameter("dateType") != null) {
			salesRepFilter.setDateType( ServletRequestUtils.getStringParameter( request, "dateType", "" ));
		}
		
		// sales rep
		if (request.getParameter("salesRepId") != null) {
			salesRepFilter.setSalesRepId(ServletRequestUtils.getIntParameter( request, "salesRepId", -1 ));
		}
		
		return salesRepFilter;
	}
}