package com.webjaguar.web.admin.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.listener.SessionListener;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AbandonedShoppingCart;
import com.webjaguar.model.AbandonedShoppingCartSearch;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SalesRepSearch;
import com.webjaguar.model.crm.CrmContactSearch;

public class AbandonedShoppingCartController implements Controller {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> model = new HashMap<String, Object>();
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		CustomerSearch customerSearch = new CustomerSearch();

		// loggedIn
		ArrayList<Integer> loggedInIds = new ArrayList<Integer>();
		loggedInIds.addAll(SessionListener.getActiveSessions().values());
		customerSearch.setLoggedIn(loggedInIds);

		// get customers
		List<Integer> customerIds = this.webJaguar.getCustomerIdsList(customerSearch);

		// check if options button was clicked
		if (request.getParameter("__groupAssignPacher") != null) {
				// get group ids
			Set<Object> groups = new HashSet<Object>();
			String groupIdString = ServletRequestUtils.getStringParameter(request, "__group_id");
			String batchType = ServletRequestUtils.getStringParameter(request, "__batch_type");
			String[] groupIds = groupIdString.split(",");
			for (int x = 0; x < groupIds.length; x++) {
				if (!("".equals(groupIds[x].trim()))) {
					try {
						groups.add(Integer.valueOf(groupIds[x].trim()));
					} catch (Exception e) {
						// do nothing
					}
				}
			}
			String ids[] = request.getParameterValues("__selected_id");
			if ((ids != null) && (request.getParameter("__groupAssignAll") == null)) {
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.batchGroupIds(Integer.parseInt(ids[i]), groups, batchType);
				}
			} else if (request.getParameter("__groupAssignAll") != null) {
				for (Integer id : customerIds) {
					this.webJaguar.batchGroupIds(id, groups, batchType);
				}
			}
		}

		AbandonedShoppingCartSearch abandonedShoppingCartSearch = this.webJaguar.getAbandonedShoppingCartSearch(request);
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map<Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = null;

			Integer salesRepId = null;
			if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
			}

			if (salesRepId == null) {
				salesReps = this.webJaguar.getSalesRepList();
			} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
				salesReps = this.webJaguar.getSalesRepList(salesRepId);
			} else {
				salesReps = new ArrayList<SalesRep>();
				salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
			}
			for (SalesRep salesRep : salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);
			}
			if (salesRepId != null) {
				// make sure access users sees only his customers
				if (abandonedShoppingCartSearch.getSalesRepId() == null) {
					abandonedShoppingCartSearch.setSalesRepId(salesRepId);
				} else if (!salesRepMap.containsKey(abandonedShoppingCartSearch.getSalesRepId())) {
					abandonedShoppingCartSearch.setSalesRepId(salesRepId);
				}
				// associate reps to parents
				for (SalesRep salesRep : salesReps) {
					if (salesRepMap.containsKey(salesRep.getParent())) {
						salesRepMap.get(salesRep.getParent()).getSubAccounts().add(salesRep);
					}
				}
				model.put("salesRepTree", salesRepMap.get(salesRepId));
			}

			model.put("salesRepMap", salesRepMap);
			model.put("salesReps", salesReps);
		}

		List<AbandonedShoppingCart> abandonedShoppingCartsFromDB = this.webJaguar.getAbandonedShoppingCartList(abandonedShoppingCartSearch);
		PagedListHolder abandonedShoppingCartList = new PagedListHolder(abandonedShoppingCartsFromDB);

		abandonedShoppingCartList.setPageSize(abandonedShoppingCartSearch.getPageSize());
		abandonedShoppingCartList.setPage(abandonedShoppingCartSearch.getPage() - 1);

		model.put("abandonedShoppingCartSearch", abandonedShoppingCartSearch);
		model.put("abandonedShoppingCarts", abandonedShoppingCartList);
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/report/abandonedShoppingCart", "model", model);
		}
		return new ModelAndView("admin/report/abandonedShoppingCart", "model", model);
	}

}
