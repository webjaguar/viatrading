/* Copyright 2006 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 05.01.2009
 */

package com.webjaguar.web.admin.report;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller; 


public class ReportIndexController implements Controller {
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return new ModelAndView("admin/report/index");
    }
}
