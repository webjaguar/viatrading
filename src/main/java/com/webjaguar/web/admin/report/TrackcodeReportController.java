package com.webjaguar.web.admin.report;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ReportFilter;


public class TrackcodeReportController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		ReportFilter trackcodeFilter = getReportFilter( request );
		Map<String, Object> myModel = new HashMap<String, Object>();

		PagedListHolder trackcodeList = new PagedListHolder(this.webJaguar.getTrackcodeReport(trackcodeFilter));		
		
		trackcodeList.setPageSize(trackcodeFilter.getPageSize());
		trackcodeList.setPage(trackcodeFilter.getPage()-1);
		myModel.put( "trackcodeReport", trackcodeList );
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)
			return new ModelAndView("admin-responsive/report/trackcode", "model", myModel);
        return new ModelAndView("admin/report/trackcode", "model", myModel);
	}

	private ReportFilter getReportFilter(HttpServletRequest request) {
		ReportFilter trackcodeFilter = (ReportFilter) request.getSession().getAttribute( "trackcodeFilter" );
		if (trackcodeFilter == null) {
			trackcodeFilter = new ReportFilter();
			trackcodeFilter.setSort( "grand_total DESC" );
			request.getSession().setAttribute( "trackcodeFilter", trackcodeFilter );
		}
		
		// sort
		if (request.getParameter("sort") != null) {
			trackcodeFilter.setSort( ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				trackcodeFilter.setPage( 1 );
			} else {
				trackcodeFilter.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				trackcodeFilter.setPageSize( 10 );
			} else {
				trackcodeFilter.setPageSize( size );				
			}
		}

		return trackcodeFilter;
	}
}
