package com.webjaguar.web.admin.report;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.CustomerReport;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.SalesRep;

public class CustomerInactiveReportController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ReportFilter inActiveCustomerFilter = getReportFilter( request );
		// set end date to end of day
		if (inActiveCustomerFilter.getEndDate() != null) {
			inActiveCustomerFilter.getEndDate().setTime(inActiveCustomerFilter.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}
		// set created end date to end of day
		if (inActiveCustomerFilter.getCreatedEndDate() != null) {
			inActiveCustomerFilter.getCreatedEndDate().setTime(inActiveCustomerFilter.getCreatedEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		
		// get customers
		List<Integer> customerIds = this.webJaguar.getInactiveCustomerIds(inActiveCustomerFilter);
		
		// check if options button was clicked
		if (request.getParameter("__groupAssignPacher") != null) {
			// get group ids
			Set<Object> groups = new HashSet<Object>();
			String groupIdString = ServletRequestUtils.getStringParameter( request, "__group_id" );
			String batchType = ServletRequestUtils.getStringParameter( request, "__batch_type" );
			String[] groupIds = groupIdString.split( "," );
			for ( int x = 0; x < groupIds.length; x++ ) {
				if ( !("".equals( groupIds[x].trim() )) ) {
					try {
						groups.add( Integer.valueOf( groupIds[x].trim() ) );
					}catch ( Exception e ) {
						// do nothing
					}
				}
			}
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i=0; i<ids.length; i++) {
					this.webJaguar.batchGroupIds( Integer.parseInt(ids[i]), groups, batchType);
				}
			}
			
			if ((ids != null) && (request.getParameter("__groupAssignAll") == null)) {
				for (int i=0; i<ids.length; i++) {
					this.webJaguar.batchGroupIds(Integer.parseInt(ids[i]), groups, batchType);
				}
			} else if (request.getParameter("__groupAssignAll") != null) {
				for(Integer id: customerIds) {
					this.webJaguar.batchGroupIds(id, groups, batchType);
				}
			}
		}

		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = null;
			
			Integer salesRepId = null;
			if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
			}

			if (salesRepId == null) {
				salesReps = this.webJaguar.getSalesRepList();				
			} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
				salesReps = this.webJaguar.getSalesRepList(salesRepId);
			} else {
				salesReps = new ArrayList<SalesRep>();
				salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
			}
			for (SalesRep salesRep: salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);				
			}
			if (salesRepId != null) {
				// make sure access users sees only his customers
				if (inActiveCustomerFilter.getSalesRepId() == -1) {
					inActiveCustomerFilter.setSalesRepId(salesRepId);
				} else if (!salesRepMap.containsKey(inActiveCustomerFilter.getSalesRepId())) {
					inActiveCustomerFilter.setSalesRepId(salesRepId);				
				}
				// associate reps to parents
				for (SalesRep salesRep: salesReps) {
					if (salesRepMap.containsKey(salesRep.getParent())) {
						salesRepMap.get(salesRep.getParent()).getSubAccounts().add(salesRep);					
					}
				}
				myModel.put("salesRepTree", salesRepMap.get(salesRepId));
			}			
			
			myModel.put("salesRepMap", salesRepMap);
			myModel.put("salesReps", salesReps);

			
			// get sales rep
			Iterator<SalesRep> iter = this.webJaguar.getSalesRepList().iterator();
			while (iter.hasNext()) {
				SalesRep salesRep = iter.next();
				salesRepMap.put(salesRep.getId(), salesRep);
			}
			myModel.put("salesRepMap", salesRepMap);
		}
		
		
		
		
		// get customers
		if (customerIds.size() < inActiveCustomerFilter.getOffset()-1) {
			inActiveCustomerFilter.setPage(1);
		}
		inActiveCustomerFilter.setLimit(inActiveCustomerFilter.getPageSize());
		inActiveCustomerFilter.setOffset((inActiveCustomerFilter.getPage()-1)*inActiveCustomerFilter.getPageSize());
		List<CustomerReport> inactiveCustomers = this.webJaguar.getInactiveCustomers(inActiveCustomerFilter);
		int pageCount = customerIds.size()/inActiveCustomerFilter.getPageSize();
		if (customerIds.size()%inActiveCustomerFilter.getPageSize() > 0) {
			pageCount++;
		}
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", inActiveCustomerFilter.getOffset()+inactiveCustomers.size());
		myModel.put("count", customerIds.size());
		
		myModel.put( "inactiveCustomerReports", inactiveCustomers );
		myModel.put( "inActiveCustomerFilter", inActiveCustomerFilter );
		myModel.put( "customerFieldList", this.webJaguar.getCustomerFields());
		if((Boolean) gSiteConfig.get("gGROUP_CUSTOMER")){
			myModel.put( "groupList", this.webJaguar.getCustomerGroupList( new CustomerGroupSearch() ) );
	    }
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/report/customersInactive", "model", myModel);
		}
		return new ModelAndView("admin/report/customersInactive", "model", myModel);
	}
	
	private ReportFilter getReportFilter(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		ReportFilter inActiveCustomerFilter = (ReportFilter) request.getSession().getAttribute( "inActiveCustomerFilter" );
		if (inActiveCustomerFilter == null) {
			inActiveCustomerFilter = new ReportFilter();
			Calendar rightNow = Calendar.getInstance();
			inActiveCustomerFilter.setEndDate( rightNow.getTime() );
			rightNow.add( Calendar.YEAR, -10 );
			inActiveCustomerFilter.setStartDate( rightNow.getTime() );
			inActiveCustomerFilter.setSalesRepId( -1 );
			request.getSession().setAttribute( "inActiveCustomerFilter", inActiveCustomerFilter );
		}
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				inActiveCustomerFilter.setStartDate( df.parse( request.getParameter("startDate")) );
			} catch (ParseException e) {
				inActiveCustomerFilter.setStartDate(null);
	        }
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				inActiveCustomerFilter.setEndDate( df.parse( request.getParameter("endDate")) );
			} catch (ParseException e) {
				inActiveCustomerFilter.setEndDate(null);
	        }
		}
		
		// created startDate
		if (request.getParameter("createdStartDate") != null) {
			try {
				inActiveCustomerFilter.setCreatedStartDate( df.parse( request.getParameter("createdStartDate")) );
			} catch (ParseException e) {
				inActiveCustomerFilter.setCreatedStartDate(null);
	        }
		}

		// created endDate
		if (request.getParameter("createdEndDate") != null) {
			try {
				inActiveCustomerFilter.setCreatedEndDate( df.parse( request.getParameter("createdEndDate")) );
			} catch (ParseException e) {
				inActiveCustomerFilter.setCreatedEndDate(null);
	        }
		}
		
		// sort
		if (request.getParameter("sort") != null) {
			inActiveCustomerFilter.setSort( ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// firstname
		if (request.getParameter("firstname") != null) {
			inActiveCustomerFilter.setFirstName( ServletRequestUtils.getStringParameter( request, "firstname", "" ));
		}
		// lastname
		if (request.getParameter("lastname") != null) {
			inActiveCustomerFilter.setLastName( ServletRequestUtils.getStringParameter( request, "lastname", "" ));
		}
		// rating 1
		if (request.getParameter("rating1") != null) {
			inActiveCustomerFilter.setRating1( ServletRequestUtils.getStringParameter( request, "rating1", "" ));
		}
		// rating 2
		if (request.getParameter("rating2") != null) {
			inActiveCustomerFilter.setRating2( ServletRequestUtils.getStringParameter( request, "rating2", "" ));
		}
		// username
		if (request.getParameter("email") != null) {
			inActiveCustomerFilter.setUsername( ServletRequestUtils.getStringParameter( request, "email", "" ));
		}
		// phone
		if (request.getParameter("phone") != null) {
			inActiveCustomerFilter.setPhone( ServletRequestUtils.getStringParameter( request, "phone", "" ));
		}
		
		// state
		if (request.getParameter("state") != null) {
			inActiveCustomerFilter.setState( ServletRequestUtils.getStringParameter( request, "state", "" ));
		}
		
		// country
		if (request.getParameter("country") != null) {
			inActiveCustomerFilter.setCountry( ServletRequestUtils.getStringParameter( request, "country", "" ));
		}
		
		// sales rep
		if (request.getParameter("salesRepId") != null) {
			inActiveCustomerFilter.setSalesRepId( ServletRequestUtils.getIntParameter( request, "salesRepId", -1 ));
		}
		
		// numOrder
		if (request.getParameter("numOrder") != null) {
			inActiveCustomerFilter.setNumOrder( (ServletRequestUtils.getIntParameter( request, "numOrder", 0 ) == 0) ? null : ServletRequestUtils.getIntParameter( request, "numOrder", 0 ));
			inActiveCustomerFilter.setNumOrderOperator( ServletRequestUtils.getIntParameter( request, "numOrderOperator", 0 ));
		}
		// Order Total
		if (request.getParameter("totalOrder") != null) {
			inActiveCustomerFilter.setTotalOrder( (ServletRequestUtils.getDoubleParameter( request, "totalOrder", 0.0 ) == 0.0) ? null : ServletRequestUtils.getDoubleParameter( request, "totalOrder", 0.0 ));
			inActiveCustomerFilter.setTotalOrderOperator( ServletRequestUtils.getIntParameter( request, "totalOrderOperator", 0 ));
		}
		
		// Customer Field
		if (request.getParameter("customerField") != null) {
			inActiveCustomerFilter.setCustomerField( ServletRequestUtils.getStringParameter( request, "customerField", null ));
		}
		
		// Customer Field Number
		if (request.getParameter("customerFieldNumber") != null) {
			inActiveCustomerFilter.setCustomerFieldNumber( ServletRequestUtils.getStringParameter( request, "customerFieldNumber", null ));
		}
		
		// group ID
		if (request.getParameter("groupId") != null) {
			int groupId = ServletRequestUtils.getIntParameter( request, "groupId", -1 );		
			if (groupId == -1) {
				inActiveCustomerFilter.setGroupId( null );			
			} else {
				inActiveCustomerFilter.setGroupId( groupId );
			}		
		}
		
		// last ordered date
		if (request.getParameter("lastOrderedDate") != null) {
			try {
				inActiveCustomerFilter.setLastOrderedDate(df.parse( request.getParameter("lastOrderedDate")) );			
			} catch (ParseException e) {
				inActiveCustomerFilter.setLastOrderedDate(null);
	        }
		}
		// Average Order
		if (request.getParameter("averageOrder") != null) {
			inActiveCustomerFilter.setAverageOrder((ServletRequestUtils.getDoubleParameter( request, "averageOrder", 0.0 ) == 0.0) ? null : ServletRequestUtils.getDoubleParameter( request, "averageOrder", 0.0 ));
			inActiveCustomerFilter.setAverageOrderOperator( ServletRequestUtils.getIntParameter( request, "averageOrderOperator", 0 ));
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				inActiveCustomerFilter.setPage( 1 );
			} else {
				inActiveCustomerFilter.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				inActiveCustomerFilter.setPageSize( 10 );
			} else {
				inActiveCustomerFilter.setPageSize( size );				
			}
		}
		
		return inActiveCustomerFilter;
	}
}