package com.webjaguar.web.admin.report;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Report;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.SalesRep;

public class OrderReportController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		//ReportFilter reportFilter = getReportFilter( request );
		Map<String, Object> myModel = new HashMap<String, Object>();
		String shipOrderDateType = ServletRequestUtils.getStringParameter( request, "_dateType_shipped_order", "orderDate" );
		Calendar rightNow = Calendar.getInstance();
		String shipOrderYear = ServletRequestUtils.getStringParameter( request, "_year_shipped_order", "" + rightNow.get(Calendar.YEAR) );
		ReportFilter shipOrderReportFilter = new ReportFilter();
		shipOrderReportFilter.setYear( shipOrderYear );
		shipOrderReportFilter.setDateType( shipOrderDateType );

		String shipDailyOrderDateType = ServletRequestUtils.getStringParameter( request, "_dateType_shippedDaily_order", "orderDate");
		ReportFilter shipDailyOrderReportFilter = new ReportFilter();
		shipDailyOrderReportFilter.setYear( shipOrderYear );
		shipDailyOrderReportFilter.setDateType( shipDailyOrderDateType );
		
		myModel.put( "shipOrderReportFilter", shipOrderReportFilter );
		myModel.put( "shipDailyOrderReportFilter", shipDailyOrderReportFilter );
		
		List<Report> salesReports = this.webJaguar.getOrdersSaleReport(shipOrderReportFilter);
		myModel.put( "salesReportsList",  salesReports );
		Double maxValue = 0.0;
		Double minValue = Double.MAX_VALUE;
		for ( Report report : salesReports ){
			if ( report.getGrandTotal() > maxValue )
				maxValue = report.getGrandTotal();
			if ( report.getGrandTotal() < minValue )
				minValue = report.getGrandTotal();
		}
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			Iterator<SalesRep> iter = this.webJaguar.getSalesRepList().iterator();
			while (iter.hasNext()) {
				SalesRep salesRep = iter.next();
				salesRepMap.put(salesRep.getId(), salesRep);
			}
			myModel.put("salesRepMap", salesRepMap);
		}
		myModel.put( "current", new Date() );
		myModel.put( "salesReportsListDaily",  this.webJaguar.getOrdersSaleReportDaily(shipDailyOrderReportFilter) );
		myModel.put( "saleReportsGraph", textEncode(salesReports, maxValue, minValue) );
		myModel.put( "openOrder", this.webJaguar.getCountPendingProcessingOrder() );
		myModel.put( "penProcOrderReport", this.webJaguar.getPendingProcessingOrder() );
		myModel.put( "affiliateReport", this.webJaguar.getCommissionReport());
        return new ModelAndView("admin/report/sales", "model", myModel);
	}
	
	private String[] textEncode(List<Report> valueArray, Double maxValue, Double minValue) {
		StringBuffer chartData = new StringBuffer("t:");
		String[] data = {"",""};
		for ( Report report : valueArray ){
			chartData.append( getPercentages(report.getGrandTotal(), maxValue ) ); 
			if ( report.getMonth() < 12 )
		    	chartData.append( "," );
		}

		chartData.append( "&chds=0,100" );
		data[0] = chartData.toString();
		data[1] = "|0|" + maxValue;
		return data;
	}
	
	private String getPercentages(Double value, Double maxValue) {
		return  "" + Math.rint( 100 * value / maxValue );
	}

	/*private ReportFilter getReportFilter(HttpServletRequest request) {
		SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd", RequestContextUtils.getLocale( request ) );
		
		ReportFilter reportFilter = (ReportFilter) request.getSession().getAttribute( "reportFilter" );
		if (reportFilter == null) {
			reportFilter = new ReportFilter();
			request.getSession().setAttribute( "reportFilter", reportFilter );
		}

		 // and the current date and time
		 Calendar calendar = new GregorianCalendar();
		 Date trialTime = new Date();
		 calendar.setTime(trialTime);
		 
		 System.out.println("s " + calendar.get(Calendar.YEAR) + "-"+ ( calendar.get(Calendar.MONTH) + 1 ) +"-1");
		 System.out.println("e " + calendar.get(Calendar.YEAR) + "-"+ ( calendar.get(Calendar.MONTH) + 1 ) + "-31");
			
		// sorting 
		try {
			reportFilter.setStartDate( df.parse( ServletRequestUtils.getStringParameter( request, "start", calendar.get(Calendar.YEAR) + "-"+ ( calendar.get(Calendar.MONTH) + 1 ) + "-1" ) ) );
		} catch (Exception e){e.printStackTrace();}

		// subject
		try {
			reportFilter.setEndDate( df.parse( ServletRequestUtils.getStringParameter( request, "end", calendar.get(Calendar.YEAR) + "-"+ ( calendar.get(Calendar.MONTH) + 1 ) + "-31" ) ));
		} catch (Exception e){e.printStackTrace();}
		
		return reportFilter;
	}*/
	

}
