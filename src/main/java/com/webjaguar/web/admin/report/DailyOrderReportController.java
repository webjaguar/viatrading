package com.webjaguar.web.admin.report;

/**
 * @author Rewati Raman (Advanced E media)
 *
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Report;
import com.webjaguar.model.ReportFilter;

public class DailyOrderReportController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Object> myModel = new HashMap<String, Object>();
		ReportFilter shipDailyOrderReportFilter = getShipDailyOrderReportFilter(request);
		
		// set end date to end of day
		if (shipDailyOrderReportFilter.getEndDate() != null) {
			shipDailyOrderReportFilter.getEndDate().setTime(shipDailyOrderReportFilter.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}
		
		int count = this.webJaguar.getOrdersSaleReportDailyCount( shipDailyOrderReportFilter );
		if (count < shipDailyOrderReportFilter.getOffset()-1) {
			shipDailyOrderReportFilter.setPage(1);
		}
		shipDailyOrderReportFilter.setLimit(shipDailyOrderReportFilter.getPageSize());
		shipDailyOrderReportFilter.setOffset((shipDailyOrderReportFilter.getPage()-1)*shipDailyOrderReportFilter.getPageSize());
		java.util.List<Report> salesReportsListDaily = this.webJaguar.getOrdersSaleReportDaily(shipDailyOrderReportFilter);
		myModel.put( "salesReportsListDaily",  salesReportsListDaily );
		int pageCount = count/shipDailyOrderReportFilter.getPageSize();
		if (count%shipDailyOrderReportFilter.getPageSize() > 0) {
			pageCount++;
		}
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", shipDailyOrderReportFilter.getOffset()+salesReportsListDaily.size());
		myModel.put("count", count);
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/report/dailyOrder", "model", myModel);
		}
		return new ModelAndView("admin/report/dailyOrder", "model", myModel);
	}
	
	private ReportFilter getShipDailyOrderReportFilter(HttpServletRequest request) {
		ReportFilter shipDailyOrderReportFilter = (ReportFilter) request.getSession().getAttribute( "shipDailyOrderReportFilter" );
		if (shipDailyOrderReportFilter == null) {
			shipDailyOrderReportFilter = new ReportFilter();
			Calendar rightNow = Calendar.getInstance();
			shipDailyOrderReportFilter.setEndDate( rightNow.getTime() );
			rightNow.add( Calendar.DATE, -Calendar.getInstance().get(Calendar.DAY_OF_YEAR)+1);
			shipDailyOrderReportFilter.setStartDate( rightNow.getTime() );
			request.getSession().setAttribute( "shipDailyOrderReportFilter", shipDailyOrderReportFilter );
		}
		
		// Date Type
		if (request.getParameter("dateType") != null) {
			shipDailyOrderReportFilter.setDateType( ServletRequestUtils.getStringParameter( request, "dateType", "orderDate" ));
		}
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				shipDailyOrderReportFilter.setStartDate( df.parse( request.getParameter("startDate")) );
			} catch (ParseException e) {
				shipDailyOrderReportFilter.setStartDate(null);
	        }
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				shipDailyOrderReportFilter.setEndDate( df.parse( request.getParameter("endDate")) );
			} catch (ParseException e) {
				shipDailyOrderReportFilter.setEndDate(null);
	        }
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				shipDailyOrderReportFilter.setPage( 1 );
			} else {
				shipDailyOrderReportFilter.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				shipDailyOrderReportFilter.setPageSize( 10 );
			} else {
				shipDailyOrderReportFilter.setPageSize( size );				
			}
		}
		shipDailyOrderReportFilter.setYear(null);
		
		return shipDailyOrderReportFilter;
	}
}