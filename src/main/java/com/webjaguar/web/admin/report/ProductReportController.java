package com.webjaguar.web.admin.report;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ProductReport;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.SalesRep;


public class ProductReportController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ReportFilter productListFilter = getReportFilter( request );
		Map<String, Object> myModel = new HashMap<String, Object>();
		// set end date to end of day
		if (productListFilter.getEndDate() != null) {
			productListFilter.getEndDate().setTime(productListFilter.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}

		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = null;
			
			Integer salesRepId = null;
			if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
			}

			if (salesRepId == null) {
				salesReps = this.webJaguar.getSalesRepList();				
			} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
				salesReps = this.webJaguar.getSalesRepList(salesRepId);
			} else {
				salesReps = new ArrayList<SalesRep>();
				salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
			}
			for (SalesRep salesRep: salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);				
			}
			if (salesRepId != null) {
				// make sure access users sees only his customers
				if (productListFilter.getSalesRepId() == -1) {
					productListFilter.setSalesRepId(salesRepId);
				} else if (!salesRepMap.containsKey(productListFilter.getSalesRepId())) {
					productListFilter.setSalesRepId(salesRepId);				
				}
				// associate reps to parents
				for (SalesRep salesRep: salesReps) {
					if (salesRepMap.containsKey(salesRep.getParent())) {
						salesRepMap.get(salesRep.getParent()).getSubAccounts().add(salesRep);					
					}
				}
				myModel.put("salesRepTree", salesRepMap.get(salesRepId));
			}			
			
			myModel.put("salesRepMap", salesRepMap);
			myModel.put("salesReps", salesReps);
		}
		
		int count = this.webJaguar.getProductListReportCount( productListFilter );
		if (count < productListFilter.getOffset()-1) {
			productListFilter.setPage(1);
		}
		
		productListFilter.setLimit(productListFilter.getPageSize());
		productListFilter.setOffset((productListFilter.getPage()-1)*productListFilter.getPageSize());
		
		List<ProductReport> productReportList = this.webJaguar.getProductListReport(productListFilter);
		
		myModel.put("productList", productReportList);
		myModel.put("salesTagCodes", this.webJaguar.getSalesTagList() );
		
		int pageCount = count/productListFilter.getPageSize();
		if (count%productListFilter.getPageSize() > 0) {
			pageCount++;
		}
		
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", productListFilter.getOffset()+productReportList.size());
		myModel.put("count", count);

		myModel.put( "productFieldList", this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/report/productList", "model", myModel);
		}
		return new ModelAndView("admin/report/productList", "model", myModel);
	}

	private ReportFilter getReportFilter(HttpServletRequest request) {
		ReportFilter productListFilter = (ReportFilter) request.getSession().getAttribute( "productListFilter" );
		if (productListFilter == null) {
			productListFilter = new ReportFilter();
			Calendar rightNow = Calendar.getInstance();
			productListFilter.setEndDate( rightNow.getTime() );
			rightNow.add( Calendar.YEAR, -1 );
			productListFilter.setStartDate( rightNow.getTime() );
			productListFilter.setSort( "date_ordered DESC" );
			request.getSession().setAttribute( "productListFilter", productListFilter );
		}
		
		// sort
		if (request.getParameter("sort") != null) {
			productListFilter.setSort( ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// Backend Order
		if (request.getParameter("backendOrder") != null) {
			productListFilter.setBackendOrder( ServletRequestUtils.getStringParameter( request, "backendOrder", "" ));
		}
		
		// sales rep
		if (request.getParameter("salesRepId") != null) {
			productListFilter.setSalesRepId( ServletRequestUtils.getIntParameter( request, "salesRepId", -1 ));
		}
		
		// Order Type
		if (request.getParameter("orderType") != null) {
			productListFilter.setOrderType( ServletRequestUtils.getStringParameter( request, "orderType", "" ));
		}
		
		// Date Type
		if (request.getParameter("dateType") != null) {
			productListFilter.setDateType( ServletRequestUtils.getStringParameter( request, "dateType", "" ));
		}
		
		// shipping Method
		if (request.getParameter("shippingMethod") != null) {
			productListFilter.setShippingMethod( ServletRequestUtils.getStringParameter( request, "shippingMethod", "" ));
		}
		
		// order status
		if (request.getParameter("status") != null) {
			productListFilter.setStatus(ServletRequestUtils.getStringParameter( request, "status", "" ));
		}
		
		// sku
		if (request.getParameter("sku") != null) {
			productListFilter.setSku( ServletRequestUtils.getStringParameter( request, "sku", "" ));
			productListFilter.setSkuOperator( ServletRequestUtils.getIntParameter( request, "skuOperator", 0 ));
		}
		
		// parentSku
		if (request.getParameter("parentSku") != null) {
			productListFilter.setParentSku(ServletRequestUtils.getStringParameter(request, "parentSku", ""));
		}
				
		// productName
		if (request.getParameter("productName") != null) {
			productListFilter.setProductName( ServletRequestUtils.getStringParameter(request, "productName", ""));
		}
		
		// billTo
		if (request.getParameter("billTo") != null) {
			productListFilter.setBillToName(ServletRequestUtils.getStringParameter( request, "billTo", "" ));
		}	
		
		// email
		if (request.getParameter("username") != null) {
			productListFilter.setUsername(ServletRequestUtils.getStringParameter( request, "username", "" ));
		}	
		
		// Product Field
		if (request.getParameter("productField") != null) {
			productListFilter.setProductField( ServletRequestUtils.getStringParameter( request, "productField", null ));
		}
		
		// Product Field Number
		if (request.getParameter("productFieldNumber") != null) {
			productListFilter.setProductFieldNumber( ServletRequestUtils.getStringParameter( request, "productFieldNumber", null ));
		}
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				productListFilter.setStartDate( df.parse( request.getParameter("startDate")) );
			} catch (ParseException e) {
				productListFilter.setStartDate(null);
	        }
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				productListFilter.setEndDate( df.parse( request.getParameter("endDate")) );
			} catch (ParseException e) {
				productListFilter.setEndDate(null);
	        }
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				productListFilter.setPage( 1 );
			} else {
				productListFilter.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				productListFilter.setPageSize( 10 );
			} else {
				productListFilter.setPageSize( size );				
			}
		}

		return productListFilter;
	}
}