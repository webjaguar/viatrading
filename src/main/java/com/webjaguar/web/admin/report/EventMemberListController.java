package com.webjaguar.web.admin.report;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.EventMember;
import com.webjaguar.model.EventSearch;
import com.webjaguar.model.SalesRep;

public class EventMemberListController implements Controller {
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{	
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		EventSearch eventSearch = getEventSearch( request );
		eventSearch.setLimit(null);
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		List<EventMember> eventMemberList = this.webJaguar.getAllEventMemberList(eventSearch);
		//Check if options clicked
		//arrange customer group
		if (request.getParameter("__groupAssignPacher") != null) {
			// get group ids
			Set<Object> groups = new HashSet<Object>();
			String groupIdString = ServletRequestUtils.getStringParameter(request, "__group_id");
			String batchType = ServletRequestUtils.getStringParameter(request, "__batch_type");
			String[] groupIds = groupIdString.split(",");
			for (int x = 0; x < groupIds.length; x++) {
				if (!("".equals(groupIds[x].trim()))) {
					try {
						groups.add(Integer.valueOf(groupIds[x].trim()));
					} catch (Exception e) {
						// do nothing
					}
				}
			}
			String ids[] = request.getParameterValues("__selected_id");
			if ((ids != null) && (request.getParameter("__groupAssignAll") == null)) {
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.batchGroupIds(Integer.parseInt(ids[i]), groups, batchType);
				}
			} 
			else if(request.getParameter("__groupAssignAll") != null){
				for (EventMember eventMembers : eventMemberList) {
					this.webJaguar.batchGroupIds(eventMembers.getUserId(), groups, batchType);
				}
			}
		}
		
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = this.webJaguar.getSalesRepList();
			for (SalesRep salesRep: salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);				
			}
			myModel.put("salesRepMap", salesRepMap);
			myModel.put("salesReps", salesReps);
		}
		
		
		eventSearch = getEventSearch(request);
		int count = this.webJaguar.getAllEventMemberListCount(eventSearch);
		if (count < eventSearch.getOffset()-1) {
			eventSearch.setPage(1);
		}
		eventSearch.setLimit(eventSearch.getPageSize());
		eventSearch.setOffset((eventSearch.getPage()-1)*eventSearch.getPageSize());
		
		eventMemberList = this.webJaguar.getAllEventMemberList(eventSearch);
		myModel.put("eventMemberList", eventMemberList);
		
		int pageCount = count/eventSearch.getPageSize();
		if (count%eventSearch.getPageSize() > 0) {
			pageCount++;
		}
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", eventSearch.getOffset()+eventMemberList.size());
		myModel.put("count", count);
		
		return new ModelAndView("admin/report/eventMemberList","model", myModel);
	}
	
	private EventSearch getEventSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		EventSearch eventMemberSearch = (EventSearch) request.getSession().getAttribute( "eventMemberReportSearch" );
		if (eventMemberSearch == null) {
			eventMemberSearch = new EventSearch();
			eventMemberSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "eventMemberReportSearch", eventMemberSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			eventMemberSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "username DESC" ));
		}
		
//		// event name
//		if (request.getParameter("eventName") != null) {
//			eventMemberSearch.setEventName(ServletRequestUtils.getStringParameter( request, "eventName", "" ));
//		}
		
		// Card ID
		if (request.getParameter("cardId") != null) {
			eventMemberSearch.setCardId( ServletRequestUtils.getStringParameter( request, "cardId", "" ));
		}
		
		// first name
		if (request.getParameter("firstName") != null) {
			eventMemberSearch.setFirstName(ServletRequestUtils.getStringParameter( request, "firstName", "" ));
		}
		
		// last name
		if (request.getParameter("lastName") != null) {
			eventMemberSearch.setLastName(ServletRequestUtils.getStringParameter( request, "lastName", "" ));
		}	
		
		// sales rep
		if (request.getParameter("salesRepId") != null) {
			eventMemberSearch.setSalesRepId(ServletRequestUtils.getIntParameter( request, "salesRepId", -1 ));
		}
		
		// email
		if (request.getParameter("email") != null) {
			eventMemberSearch.setEmail( ServletRequestUtils.getStringParameter( request, "email", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				eventMemberSearch.setPage( 1 );
			} else {
				eventMemberSearch.setPage( page );				
			}
		}			
		
		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				eventMemberSearch.setPageSize( 10 );
			} else {
				eventMemberSearch.setPageSize( size );				
			}
		}
		
		return eventMemberSearch;
	}	
}
