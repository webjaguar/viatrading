package com.webjaguar.web.admin.report;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.InventoryReport;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.web.domain.Constants;

public class InventoryReportController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ReportFilter inventoryReportFilter = getReportFilter( request );
		 
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		int count = this.webJaguar.getInventoryReportCount(inventoryReportFilter);
		if (count < inventoryReportFilter.getOffset()-1) {
			inventoryReportFilter.setPage(1);
		}
		inventoryReportFilter.setLimit(inventoryReportFilter.getPageSize());
		inventoryReportFilter.setOffset((inventoryReportFilter.getPage()-1)*inventoryReportFilter.getPageSize());
		List<InventoryReport> inventoryReportList = this.webJaguar.getInventoryReport(inventoryReportFilter);
		int pageCount = count/inventoryReportFilter.getPageSize();
		if (count%inventoryReportFilter.getPageSize() > 0) {
			pageCount++;
		}
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", inventoryReportFilter.getOffset()+inventoryReportList.size());
		myModel.put("count", count);
		myModel.put( "years", Constants.getYears(4,1) );
        myModel.put( "inventoryReportList", inventoryReportList);
    	
		
		return new ModelAndView("admin/report/inventory", "model", myModel);
	}
	
	private ReportFilter getReportFilter(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		ReportFilter inventoryReportFilter = (ReportFilter) request.getSession().getAttribute( "inventoryReportFilter" );
		if (inventoryReportFilter == null) {
			inventoryReportFilter = new ReportFilter();
			request.getSession().setAttribute( "inventoryReportFilter", inventoryReportFilter );
		}
		// sort
		if (request.getParameter("sort") != null) {
			inventoryReportFilter.setSort( ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}else {
			inventoryReportFilter.setSort( ServletRequestUtils.getStringParameter( request, "sort", "sku" ));
		}
		
		// sku
		if (request.getParameter("sku") != null) {
			inventoryReportFilter.setSku(request.getParameter("sku"));
			inventoryReportFilter.setSkuOperator( ServletRequestUtils.getIntParameter(request, "skuOperator", 0));
	    }

		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		// date range
		if (request.getParameter("year") != null) {
			try {
				inventoryReportFilter.setStartDate( df.parse( "01/01/"+request.getParameter("year")) );
				inventoryReportFilter.setEndDate( df.parse( "12/31/"+request.getParameter("year")) );
				
				inventoryReportFilter.getEndDate().setTime( inventoryReportFilter.getEndDate().getTime()
						+ 23*60*60*1000 // hours
						+ 59*60*1000 	// minutes
						+ 59*1000);		// seconds
				} catch (ParseException e) {
				inventoryReportFilter.setStartDate(null);
				inventoryReportFilter.setEndDate(null);
		    }
		}
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				inventoryReportFilter.setPage( 1 );
			} else {
				inventoryReportFilter.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				inventoryReportFilter.setPageSize( 10 );
			} else {
				inventoryReportFilter.setPageSize( size );				
			}
		}
		
		return inventoryReportFilter;
	}
}