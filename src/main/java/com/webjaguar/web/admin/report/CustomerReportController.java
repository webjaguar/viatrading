package com.webjaguar.web.admin.report;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.SalesRep;

public class CustomerReportController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ReportFilter customerFilter = getReportFilter( request );
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		// set end date to end of day
		if (customerFilter.getEndDate() != null) {
			customerFilter.getEndDate().setTime(customerFilter.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}
		// set created end date to end of day
		if (customerFilter.getCreatedEndDate() != null) {
			customerFilter.getCreatedEndDate().setTime(customerFilter.getCreatedEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}
		Map<String, Object> myModel = new HashMap<String, Object>();

		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = null;
			
			Integer salesRepId = null;
			if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
			}

			if (salesRepId == null) {
				salesReps = this.webJaguar.getSalesRepList();				
			} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
				salesReps = this.webJaguar.getSalesRepList(salesRepId);
			} else {
				salesReps = new ArrayList<SalesRep>();
				salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
			}
			for (SalesRep salesRep: salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);				
			}
			if (salesRepId != null) {
				// make sure access users sees only his customers
				if (customerFilter.getSalesRepId() == -1) {
					customerFilter.setSalesRepId(salesRepId);
				} else if (!salesRepMap.containsKey(customerFilter.getSalesRepId())) {
					customerFilter.setSalesRepId(salesRepId);				
				}
				// associate reps to parents
				for (SalesRep salesRep: salesReps) {
					if (salesRepMap.containsKey(salesRep.getParent())) {
						salesRepMap.get(salesRep.getParent()).getSubAccounts().add(salesRep);					
					}
				}
				myModel.put("salesRepTree", salesRepMap.get(salesRepId));
			}			
			
			myModel.put("salesRepMap", salesRepMap);
			myModel.put("salesReps", salesReps);
		}
		
		PagedListHolder customerReports = new PagedListHolder(this.webJaguar.getCustomerReportOverview(customerFilter));		
		
		customerReports.setPageSize(customerFilter.getPageSize());
		customerReports.setPage(customerFilter.getPage()-1);
		
		myModel.put( "customerReports", customerReports );
		
		myModel.put( "productFieldList", this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
		myModel.put( "customerFieldList", this.webJaguar.getCustomerFields());

		myModel.put( "customerFieldList", this.webJaguar.getCustomerFields());
		if((Boolean) gSiteConfig.get("gGROUP_CUSTOMER")){
			myModel.put( "groupList", this.webJaguar.getCustomerGroupList( new CustomerGroupSearch() ) );
	    }
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/report/customers", "model", myModel);
		}
		return new ModelAndView("admin/report/customers", "model", myModel);
	}
	
	private ReportFilter getReportFilter(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		ReportFilter customerFilter = (ReportFilter) request.getSession().getAttribute( "customerFilter" );
		if (customerFilter == null) {
			customerFilter = new ReportFilter();
			Calendar rightNow = Calendar.getInstance();
			customerFilter.setEndDate( rightNow.getTime() );
			rightNow.add( Calendar.DATE, -30 );
			customerFilter.setStartDate( rightNow.getTime() );
			customerFilter.setSalesRepId( -1 );
			request.getSession().setAttribute( "customerFilter", customerFilter );
		}
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				customerFilter.setStartDate( df.parse( request.getParameter("startDate")) );
			} catch (ParseException e) {
				customerFilter.setStartDate(null);
	        }
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				customerFilter.setEndDate( df.parse( request.getParameter("endDate")) );
			} catch (ParseException e) {
				customerFilter.setEndDate(null);
	        }
		}
		
		// sort
		if (request.getParameter("sort") != null) {
			customerFilter.setSort( ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// Backend Order
		if (request.getParameter("backendOrder") != null) {
			customerFilter.setBackendOrder( ServletRequestUtils.getStringParameter( request, "backendOrder", "" ));
		}
		
		// sales rep
		if (request.getParameter("salesRepId") != null) {
			customerFilter.setSalesRepId(ServletRequestUtils.getIntParameter( request, "salesRepId", -1 ));
		}
		
		// Order Type
		if (request.getParameter("orderType") != null) {
			customerFilter.setOrderType( ServletRequestUtils.getStringParameter( request, "orderType", "" ));
		}
		
		// firstname
		if (request.getParameter("firstname") != null) {
			customerFilter.setFirstName( ServletRequestUtils.getStringParameter( request, "firstname", "" ));
		}
		
		// lastname
		if (request.getParameter("lastname") != null) {
			customerFilter.setLastName( ServletRequestUtils.getStringParameter( request, "lastname", "" ));
		}
		
		// shipping Method
		if (request.getParameter("shippingMethod") != null) {
			customerFilter.setShippingMethod( ServletRequestUtils.getStringParameter( request, "shippingMethod", "" ));
		}
		
		// total order
		if (request.getParameter("totalOrder") != null) {
			customerFilter.setTotalOrder( (ServletRequestUtils.getDoubleParameter( request, "totalOrder", 0.0 ) == 0.0) ? null : ServletRequestUtils.getDoubleParameter( request, "_totalOrder", 0.0 ));
		}
		
		// Product Field
		if (request.getParameter("productField") != null) {
			customerFilter.setProductField( ServletRequestUtils.getStringParameter( request, "productField", null ));
		}
		
		// Product Field Number
		if (request.getParameter("productFieldNumber") != null) {
			customerFilter.setProductFieldNumber( ServletRequestUtils.getStringParameter( request, "productFieldNumber", null ));
		}
		
		// Customer Field
		if (request.getParameter("customerField") != null) {
			customerFilter.setCustomerField( ServletRequestUtils.getStringParameter( request, "customerField", null ));
		}
		
		// Customer Field Number
		if (request.getParameter("customerFieldNumber") != null) {
			customerFilter.setCustomerFieldNumber( ServletRequestUtils.getStringParameter( request, "customerFieldNumber", null ));
		}
		
		// group ID
		if (request.getParameter("groupId") != null) {
			int groupId = ServletRequestUtils.getIntParameter( request, "groupId", -1 );		
			if (groupId == -1) {
				customerFilter.setGroupId( null );			
			} else {
				customerFilter.setGroupId( groupId );
			}		
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				customerFilter.setPage( 1 );
			} else {
				customerFilter.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				customerFilter.setPageSize( 10 );
			} else {
				customerFilter.setPageSize( size );				
			}
		}
		
		return customerFilter;
	}
}