package com.webjaguar.web.admin.report;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SalesRepReport;
import com.webjaguar.web.domain.Constants;

public class SalesRepReportDailyController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ReportFilter salesRepFilter = getReportFilter( request );
		Map<String, Object> myModel = new HashMap<String, Object>();
		List<SalesRepReport> salesRepReport = new ArrayList<SalesRepReport>();

		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = null;
			
			Integer salesRepId = null;
			if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
			}

			if (salesRepId == null) {
				salesReps = this.webJaguar.getSalesRepList();				
			} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
				salesReps = this.webJaguar.getSalesRepList(salesRepId);
			} else {
				salesReps = new ArrayList<SalesRep>();
				salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
			}
			for (SalesRep salesRep: salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);				
			}
			if (salesRepId != null) {
				// make sure access users sees only his customers
				if (salesRepFilter.getSalesRepId() == -1) {
					salesRepFilter.setSalesRepId(salesRepId);
				} else if (!salesRepMap.containsKey(salesRepFilter.getSalesRepId())) {
					salesRepFilter.setSalesRepId(salesRepId);				
				}
				// associate reps to parents
				for (SalesRep salesRep: salesReps) {
					if (salesRepMap.containsKey(salesRep.getParent())) {
						salesRepMap.get(salesRep.getParent()).getSubAccounts().add(salesRep);					
					}
				}
				myModel.put("salesRepTree", salesRepMap.get(salesRepId));
			}			
			salesRepReport = this.webJaguar.getSalesRepReportDaily(salesRepFilter.getSalesRepId(), salesRepFilter);
			myModel.put("salesRepMap", salesRepMap);
			myModel.put("salesReps", salesReps);
		}
		myModel.put( "salesRepReport", salesRepReport );
		myModel.put( "salesRepFilter", salesRepFilter );
		myModel.put( "yearList", Constants.getYears(5,5) );
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/report/salesRepsDaily", "model", myModel);
		}
        return new ModelAndView("admin/report/salesRepsDaily", "model", myModel);
	}

	private ReportFilter getReportFilter(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		ReportFilter salesRepDailyFilter = (ReportFilter) request.getSession().getAttribute( "salesRepDailyFilter" );
		if (salesRepDailyFilter == null) {
			salesRepDailyFilter = new ReportFilter();
			salesRepDailyFilter.setYear( "" + Calendar.getInstance().get( Calendar.YEAR ) );
			request.getSession().setAttribute( "salesRepDailyFilter", salesRepDailyFilter );
		}
		
		// year
		if (request.getParameter("year") != null) {
			salesRepDailyFilter.setYear( ServletRequestUtils.getStringParameter( request, "year", "" ));
		}
		
		// Date Type
		if (request.getParameter("dateType") != null) {
			salesRepDailyFilter.setDateType( ServletRequestUtils.getStringParameter( request, "dateType", "" ));
		}
		
		// SalesRep Id
		if (request.getParameter("salesRepId") != null) {
			salesRepDailyFilter.setSalesRepId( ServletRequestUtils.getIntParameter( request, "salesRepId", 0 ));
		}
		
		return salesRepDailyFilter;
	}
}
