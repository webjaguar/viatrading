package com.webjaguar.web.admin.report;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ReportFilter;

public class PromoCodeReportController implements Controller{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ReportFilter promoCodeFilter = getReportFilter( request );
		Map<String, Object> myModel = new HashMap<String, Object>();

		PagedListHolder promoCodeList = new PagedListHolder(this.webJaguar.getPromoCodeReport(promoCodeFilter));		
		
		promoCodeList.setPageSize(promoCodeFilter.getPageSize());
		promoCodeList.setPage(promoCodeFilter.getPage()-1);
		myModel.put( "promoCodeReport", promoCodeList );
        return new ModelAndView("admin/report/promoCode", "model", myModel);
	}
	
	private ReportFilter getReportFilter(HttpServletRequest request) {
		ReportFilter promoCodeFilter = (ReportFilter) request.getSession().getAttribute( "promoCodeFilter" );
		if (promoCodeFilter == null) {
			promoCodeFilter = new ReportFilter();
			promoCodeFilter.setSort( "grand_total DESC" );
			request.getSession().setAttribute( "promoCodeFilter", promoCodeFilter );
		}
		
		// sort
		if (request.getParameter("sort") != null) {
			promoCodeFilter.setSort( ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				promoCodeFilter.setPage( 1 );
			} else {
				promoCodeFilter.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				promoCodeFilter.setPageSize( 10 );
			} else {
				promoCodeFilter.setPageSize( size );				
			}
		}

		return promoCodeFilter;
	}

}
