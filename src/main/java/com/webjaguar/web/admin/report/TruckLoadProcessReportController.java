package com.webjaguar.web.admin.report;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.TruckLoadProcessSearch;

public class TruckLoadProcessReportController implements Controller {
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		TruckLoadProcessSearch truckLoadProcessSearch = getTruckLoadProcessSearch(request);
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		// set end date to end of day
		if (truckLoadProcessSearch.getEndDate() != null) {
			truckLoadProcessSearch.getEndDate().setTime(truckLoadProcessSearch.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}
		
		// gets list of truckLoad from database.
		PagedListHolder truckLoadProcessList = new PagedListHolder(this.webJaguar.getTruckLoadProcessListReport(truckLoadProcessSearch));
		
		truckLoadProcessList.setPageSize(truckLoadProcessSearch.getPageSize());
		truckLoadProcessList.setPage(truckLoadProcessSearch.getPage()-1);
		
		myModel.put("truckLoadProcessList", truckLoadProcessList);
		
		
		return new ModelAndView("admin/report/truckLoad", "model", myModel);
	}
	
	// sets value into sear filter.
	private TruckLoadProcessSearch getTruckLoadProcessSearch(HttpServletRequest request) {
		TruckLoadProcessSearch truckLoadProcessSearch = (TruckLoadProcessSearch) request.getSession().getAttribute( "truckLoadProcessSearch" );
			if (truckLoadProcessSearch == null) {
				truckLoadProcessSearch = new TruckLoadProcessSearch();
			request.getSession().setAttribute( "truckLoadProcessSearch", truckLoadProcessSearch );
		}
			
			SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
			// startDate
			if (request.getParameter("startDate") != null) {
				try {
					truckLoadProcessSearch.setStartDate( df.parse( request.getParameter("startDate")) );
				} catch (ParseException e) {
					truckLoadProcessSearch.setStartDate(null);
		        }
			}

			// endDate
			if (request.getParameter("endDate") != null) {
				try {
					truckLoadProcessSearch.setEndDate( df.parse( request.getParameter("endDate")) );
				} catch (ParseException e) {
					truckLoadProcessSearch.setEndDate(null);
		        }
			}
			
			// name
			if (request.getParameter("name") != null) {
				truckLoadProcessSearch.setName( ServletRequestUtils.getStringParameter( request, "name", "" ));
			}
			
			// originalSku
			if (request.getParameter("originalSku") != null) {
				truckLoadProcessSearch.setOriginalSku( ServletRequestUtils.getStringParameter( request, "originalSku", "" ));
			}
			
			// producedSku
			if (request.getParameter("producedSku") != null) {
				truckLoadProcessSearch.setProducedSku( ServletRequestUtils.getStringParameter( request, "producedSku", "" ));
			}
			
			// custom1
			if (request.getParameter("custom1") != null) {
				truckLoadProcessSearch.setCustom1( ServletRequestUtils.getStringParameter( request, "custom1", "" ));
			}
			
			// custom2
			if (request.getParameter("custom2") != null) {
				truckLoadProcessSearch.setCustom2( ServletRequestUtils.getStringParameter( request, "custom2", "" ));
			}
			
			// custom3
			if (request.getParameter("custom3") != null) {
				truckLoadProcessSearch.setCustom3( ServletRequestUtils.getStringParameter( request, "custom3", "" ));
			}
			
			// custom4
			if (request.getParameter("custom4") != null) {
				truckLoadProcessSearch.setCustom4( ServletRequestUtils.getStringParameter( request, "custom4", "" ));
			}
			
			// status
			if (request.getParameter("status") != null) {
				truckLoadProcessSearch.setStatus( ServletRequestUtils.getStringParameter( request, "status", "" ));
			}
			
			
			// sort
			if (request.getParameter("sort") != null) {
				truckLoadProcessSearch.setSort( ServletRequestUtils.getStringParameter( request, "sort", "" ));
			}
			
			// page
			if (request.getParameter("page") != null) {
				int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
				if (page < 1) {
					truckLoadProcessSearch.setPage( 1 );
				} else {
					truckLoadProcessSearch.setPage( page );				
				}
			}			
			
			// size
			if (request.getParameter("size") != null) {
				int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
				if (size < 10) {
					truckLoadProcessSearch.setPageSize( 10 );
				} else {
					truckLoadProcessSearch.setPageSize( size );				
				}
			}
		return truckLoadProcessSearch;
	}


}
