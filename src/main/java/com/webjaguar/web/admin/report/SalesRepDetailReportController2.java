package com.webjaguar.web.admin.report;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Report;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.SalesRep;

public class SalesRepDetailReportController2 implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		ReportFilter salesRepDetailFilter = getReportFilter( request );
		Map<String, Object> myModel = new HashMap<String, Object>();
		// set end date to end of day
		if (salesRepDetailFilter.getEndDate() != null) {
			salesRepDetailFilter.getEndDate().setTime(salesRepDetailFilter.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}
		
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
	
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = null;
			
			Integer salesRepId = null;
			if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();				
			}			
			if (salesRepId == null) {
				salesReps = this.webJaguar.getSalesRepList();
			} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
				salesReps = this.webJaguar.getSalesRepList(salesRepId);
			} else {
				salesReps = new ArrayList<SalesRep>();
				salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
			}
			for (SalesRep salesRep: salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);
			}
			if (salesRepId != null) {
				// make sure access users sees only his customers
				if (salesRepDetailFilter.getSalesRepId() == -1) {
					salesRepDetailFilter.setSalesRepId(salesRepId);
				} else if (!salesRepMap.containsKey(salesRepDetailFilter.getSalesRepId())) {
					salesRepDetailFilter.setSalesRepId(salesRepId);
					
				}
				// associate reps to parents
				for (SalesRep salesRep: salesReps) {
					if (salesRepMap.containsKey(salesRep.getParent())) {
						salesRepMap.get(salesRep.getParent()).getSubAccounts().add(salesRep);					
					}
				}
				myModel.put("salesRepTree", salesRepMap.get(salesRepId));
			} 
			
			myModel.put("salesRepMap", salesRepMap);
			myModel.put("salesReps", salesReps);
		}
		myModel.put( "productFieldList", this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
		myModel.put( "customerFieldList", this.webJaguar.getCustomerFields());
		List<Report> salesDetailReports = this.webJaguar.getSalesRepDetailReport2( salesRepDetailFilter );
		myModel.put( "salesRepDetailReportsList",  salesDetailReports );		
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/report/salesRepDetail2", "model", myModel);
		}
		return new ModelAndView("admin/report/salesRepDetail2", "model", myModel);
		
	}

	private ReportFilter getReportFilter(HttpServletRequest request) {
		ReportFilter salesRepDetailFilter = (ReportFilter) request.getSession().getAttribute( "salesRepDetailFilter" );
		if (salesRepDetailFilter == null) {
			salesRepDetailFilter = new ReportFilter();
			Calendar rightNow = Calendar.getInstance();
			salesRepDetailFilter.setEndDate(rightNow.getTime());
			rightNow.add(Calendar.MONTH, -1);
			salesRepDetailFilter.setStartDate(rightNow.getTime());
			salesRepDetailFilter.setSort( "grand_total" );
			request.getSession().setAttribute( "salesRepDetailFilter2", salesRepDetailFilter );
		}
		
		// sort
		if (request.getParameter("sort") != null) {
			salesRepDetailFilter.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// Backend Order
		if (request.getParameter("backendOrder") != null) {
			salesRepDetailFilter.setBackendOrder( ServletRequestUtils.getStringParameter( request, "backendOrder", "" ));
		}
		
		// sales rep
		if (request.getParameter("salesRepId") != null) {
			salesRepDetailFilter.setSalesRepId(ServletRequestUtils.getIntParameter( request, "salesRepId", -1 ));			
		}
		
		// Order Type
		if (request.getParameter("orderType") != null) {
			salesRepDetailFilter.setOrderType( ServletRequestUtils.getStringParameter( request, "orderType", null ));
		}
		
		// Date Type
		if (request.getParameter("dateType") != null) {
			salesRepDetailFilter.setDateType( ServletRequestUtils.getStringParameter( request, "dateType", "orderDate" ));
		}
		
		// shipping Method
		if (request.getParameter("shippingMethod") != null) {
			salesRepDetailFilter.setShippingMethod( ServletRequestUtils.getStringParameter( request, "shippingMethod", "" ));
		}
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				salesRepDetailFilter.setStartDate( df.parse( request.getParameter("startDate")) );
			} catch (ParseException e) {
				salesRepDetailFilter.setStartDate(null);
	        }
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				salesRepDetailFilter.setEndDate( df.parse( request.getParameter("endDate")) );
			} catch (ParseException e) {
				salesRepDetailFilter.setEndDate(null);
	        }
		}

		return salesRepDetailFilter;
	}
}