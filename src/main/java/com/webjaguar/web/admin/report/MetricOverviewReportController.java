package com.webjaguar.web.admin.report;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.MetricReport;
import com.webjaguar.model.Report;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.domain.Constants;

public class MetricOverviewReportController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		ReportFilter metricOverviewFilter = getReportFilter( request );
		Map<String, Object> myModel = new HashMap<String, Object>(); 
		List<MetricReport> metricReport = new ArrayList<MetricReport>();
			metricReport = this.webJaguar.getMetricReport(metricOverviewFilter);
		
		myModel.put( "years", Constants.getYears(8,1) );
		int totalRegistrations = 0;
		int totalActivations = 0;
		if(metricOverviewFilter.getQuarter().equalsIgnoreCase("0")) {
			List<Integer> Years = new ArrayList<Integer>();
			for(int i= 0; i < 5 ; i++) {
				int year = Integer.parseInt(metricOverviewFilter.getYear());
				Years.add(year-i);
			}
			myModel.put("fiveYears", Years);
		}
		if(metricOverviewFilter.getQuarter().equalsIgnoreCase("1")) {
			if (metricOverviewFilter.getYear().equalsIgnoreCase(""+Calendar.getInstance().get( Calendar.YEAR ))) {	
				int month = Calendar.getInstance().get( Calendar.MONTH );
				totalRegistrations= metricReport.get(month).getTotalRegistration();
				totalActivations = metricReport.get(month).getTotalActivations();			
				for (int i = month+1 ; i < metricReport.size() ;i++) {
					metricReport.get(i).setTotalRegistration(0);
					metricReport.get(i).setTotalActivations(0);
				}
			}
		}
		myModel.put("totalRegistrations", totalRegistrations);
		myModel.put("totalActivations", totalActivations);
		
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = null;
			
			Integer salesRepId = null;
			if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
			}

			if (salesRepId == null) {
				salesReps = this.webJaguar.getSalesRepList();				
			} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
				salesReps = this.webJaguar.getSalesRepList(salesRepId);
			} else {
				salesReps = new ArrayList<SalesRep>();
				salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
			}
			for (SalesRep salesRep: salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);				
			}
			if (salesRepId != null) {
				// make sure access users sees only his customers
				if (metricOverviewFilter.getSalesRepId() == -1) {
					metricOverviewFilter.setSalesRepId(salesRepId);
				} else if (!salesRepMap.containsKey(metricOverviewFilter.getSalesRepId())) {
					metricOverviewFilter.setSalesRepId(salesRepId);				
				}
				// associate reps to parents
				for (SalesRep salesRep: salesReps) {
					if (salesRepMap.containsKey(salesRep.getParent())) {
						salesRepMap.get(salesRep.getParent()).getSubAccounts().add(salesRep);					
					}
				}
				myModel.put("salesRepTree", salesRepMap.get(salesRepId));
			}			
			
			myModel.put("salesRepMap", salesRepMap);
			myModel.put("salesReps", salesReps);
		}
		
		myModel.put("reports", metricReport);
		myModel.put("currentYear", Calendar.getInstance().get( Calendar.YEAR ) );
		myModel.put("currentMonth", Calendar.getInstance().get( Calendar.MONTH) );
		if (metricOverviewFilter.getQuarter().equalsIgnoreCase("1")) {
			if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)
				return new ModelAndView("admin-responsive/report/metricOverviewQtr", "model", myModel);
			return new ModelAndView("admin/report/metricOverviewQtr", "model", myModel);
		} else
		{
			if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)
				return new ModelAndView("admin-responsive/report/metricOverviewYear", "model", myModel);
			return new ModelAndView("admin/report/metricOverviewYear", "model", myModel);
		}
	}
	
	private ReportFilter getReportFilter(HttpServletRequest request) {
		ReportFilter metricOverviewFilter = (ReportFilter) request.getSession().getAttribute( "metricOverviewFilter" );
		if (metricOverviewFilter == null) {
			metricOverviewFilter = new ReportFilter();
			metricOverviewFilter.setYear( "" + Calendar.getInstance().get( Calendar.YEAR ) );
			request.getSession().setAttribute( "metricOverviewFilter", metricOverviewFilter );
		}
		// Backend Order
		if (request.getParameter("quarter") != null) {
			metricOverviewFilter.setQuarter( ServletRequestUtils.getStringParameter( request, "quarter", "" ));
		}
		
		// year
		if (request.getParameter("year") != null) {
			metricOverviewFilter.setYear( ServletRequestUtils.getStringParameter( request, "year", "" ));
		}
		
		// Date Type
		if (request.getParameter("dateType") != null) {
			metricOverviewFilter.setDateType( ServletRequestUtils.getStringParameter( request, "dateType", "" ));
		}
		
		// sales rep
		if (request.getParameter("salesRepId") != null) {
			metricOverviewFilter.setSalesRepId(ServletRequestUtils.getIntParameter( request, "salesRepId", -1 ));
		}
		
		// Backend Order
		if (request.getParameter("backendOrder") != null) {
			metricOverviewFilter.setBackendOrder( ServletRequestUtils.getStringParameter( request, "backendOrder", "" ));
		}
		
		// Order Type
		if (request.getParameter("orderType") != null) {
			metricOverviewFilter.setOrderType( ServletRequestUtils.getStringParameter( request, "orderType", "" ));
		}
		
		return metricOverviewFilter;
	}
}

