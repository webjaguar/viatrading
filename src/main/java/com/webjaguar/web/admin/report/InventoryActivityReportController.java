package com.webjaguar.web.admin.report;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.web.domain.Constants;

public class InventoryActivityReportController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ReportFilter inventoryActivityFilter = getReportFilter( request );
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		int count = this.webJaguar.getInventoryActivityCount(inventoryActivityFilter);
		if (count < inventoryActivityFilter.getOffset()-1) {
			inventoryActivityFilter.setPage(1);
		}
		inventoryActivityFilter.setLimit(inventoryActivityFilter.getPageSize());
		inventoryActivityFilter.setOffset((inventoryActivityFilter.getPage()-1)*inventoryActivityFilter.getPageSize());
		List<InventoryActivity> inventoryActivityList = this.webJaguar.getInventoryActivity(inventoryActivityFilter);
		int pageCount = count/inventoryActivityFilter.getPageSize();
		if (count%inventoryActivityFilter.getPageSize() > 0) {
			pageCount++;
		}
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", inventoryActivityFilter.getOffset()+inventoryActivityList.size());
		myModel.put("count", count);
		myModel.put( "years", Constants.getYears(4,1) );
        myModel.put( "inventoryActivityList", inventoryActivityList);
 		
		return new ModelAndView("admin/report/inventoryActivity", "model", myModel);
	}
	
	private ReportFilter getReportFilter(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		ReportFilter inventoryActivityFilter = (ReportFilter) request.getSession().getAttribute( "inventoryActivityFilter" );
		if (inventoryActivityFilter == null) {
			inventoryActivityFilter = new ReportFilter();
			inventoryActivityFilter.setSort("date DESC");
			request.getSession().setAttribute( "inventoryActivityFilter", inventoryActivityFilter );
		}
		// sort
		if (request.getParameter("sort") != null) {
			inventoryActivityFilter.setSort( ServletRequestUtils.getStringParameter( request, "sort", "date" ));
		} 
		
		// sku
		if (request.getParameter("sku") != null) {
			inventoryActivityFilter.setSkuOperator( ServletRequestUtils.getIntParameter(request, "skuOperator", 0));
			inventoryActivityFilter.setSku(request.getParameter("sku").trim());
	    }
		
		// type
		if (request.getParameter("type") != null) {
			inventoryActivityFilter.setType(request.getParameter("type").trim());
	    }

		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		// date range
		if (request.getParameter("year") != null) {
			try {
				inventoryActivityFilter.setStartDate( df.parse( "01/01/"+request.getParameter("year")) );
				inventoryActivityFilter.setEndDate( df.parse( "12/31/"+request.getParameter("year")) );
				
				inventoryActivityFilter.getEndDate().setTime( inventoryActivityFilter.getEndDate().getTime()
						+ 23*60*60*1000 // hours
						+ 59*60*1000 	// minutes
						+ 59*1000);		// seconds
				} catch (ParseException e) {
					inventoryActivityFilter.setStartDate(null);
					inventoryActivityFilter.setEndDate(null);
		    }
		}
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				inventoryActivityFilter.setPage( 1 );
			} else {
				inventoryActivityFilter.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				inventoryActivityFilter.setPageSize( 10 );
			} else {
				inventoryActivityFilter.setPageSize( size );				
			}
		}
		
		return inventoryActivityFilter;
	}
}