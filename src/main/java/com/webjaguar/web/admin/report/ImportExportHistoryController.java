/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.30.2008
 */

package com.webjaguar.web.admin.report;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ImportExportHistorySearch;


public class ImportExportHistoryController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		ImportExportHistorySearch search = getImportExportHistorySearch( request ); 

		Map<String, Object> myModel = new HashMap<String, Object>();
				
		PagedListHolder importExportHistoryList = new PagedListHolder(this.webJaguar.getImportExportHistoryByType( search ));		
		
		importExportHistoryList.setPageSize(search.getPageSize());
		importExportHistoryList.setPage(search.getPage()-1);
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)
			return new ModelAndView("admin-responsive/report/importExportHistoryList", "model", myModel);
		myModel.put("importExportHistoryList", importExportHistoryList);
    	
        return new ModelAndView("admin/report/importExportHistoryList", "model", myModel);
	}
	
	private ImportExportHistorySearch getImportExportHistorySearch(HttpServletRequest request) {
		ImportExportHistorySearch importExportHistorySearch = (ImportExportHistorySearch) request.getSession().getAttribute( "importExportHistorySearch" );
		if (importExportHistorySearch == null) {
			importExportHistorySearch = new ImportExportHistorySearch();
			request.getSession().setAttribute( "importExportHistorySearch", importExportHistorySearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			importExportHistorySearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}	
		
		// username
		if (request.getParameter("_username") != null) {
			importExportHistorySearch.setUsername(ServletRequestUtils.getStringParameter( request, "_username", "" ));
		}
		
		// import
		if (request.getParameter("_import") != null) {
			importExportHistorySearch.setImported(ServletRequestUtils.getStringParameter( request, "_import", "" ));
		}		
		
		// type
		if (request.getParameter("_type") != null) {
			importExportHistorySearch.setType(ServletRequestUtils.getStringParameter( request, "_type", "" ));
		}	
				
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				importExportHistorySearch.setPage( 1 );
			} else {
				importExportHistorySearch.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				importExportHistorySearch.setPageSize( 10 );
			} else {
				importExportHistorySearch.setPageSize( size );				
			}
		}
		return importExportHistorySearch;
	}	
}