package com.webjaguar.web.admin.report;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Report;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.domain.Constants;

public class OrderDetailReportController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		ReportFilter orderDetailFilter = getReportFilter( request );
		Map<String, Object> myModel = new HashMap<String, Object>();

		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = null;
			
			Integer salesRepId = null;
			if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
			}

			if (salesRepId == null) {
				salesReps = this.webJaguar.getSalesRepList();				
			} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
				salesReps = this.webJaguar.getSalesRepList(salesRepId);
			} else {
				salesReps = new ArrayList<SalesRep>();
				salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
			}
			for (SalesRep salesRep: salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);				
			}
			if (salesRepId != null) {
				// make sure access users sees only his customers
				if (orderDetailFilter.getSalesRepId() == -1) {
					orderDetailFilter.setSalesRepId(salesRepId);
				} else if (!salesRepMap.containsKey(orderDetailFilter.getSalesRepId())) {
					orderDetailFilter.setSalesRepId(salesRepId);				
				}
				// associate reps to parents
				for (SalesRep salesRep: salesReps) {
					if (salesRepMap.containsKey(salesRep.getParent())) {
						salesRepMap.get(salesRep.getParent()).getSubAccounts().add(salesRep);					
					}
				}
				myModel.put("salesRepTree", salesRepMap.get(salesRepId));
			}			
			
			myModel.put("salesRepMap", salesRepMap);
			myModel.put("salesReps", salesReps);
		}

		myModel.put( "productFieldList", this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
		myModel.put( "customerFieldList", this.webJaguar.getCustomerFields());
		
		List<Report> salesDetailReports = this.webJaguar.getOrdersSaleDetailReport(orderDetailFilter);
		myModel.put( "salesDetailReportsList",  salesDetailReports );
		myModel.put( "years", Constants.getYears(19,5) );
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/report/salesDetail", "model", myModel);
		}
        return new ModelAndView("admin/report/salesDetail", "model", myModel);

	}

	private ReportFilter getReportFilter(HttpServletRequest request) {
		ReportFilter orderDetailFilter = (ReportFilter) request.getSession().getAttribute( "orderDetailFilter" );
		if (orderDetailFilter == null) {
			orderDetailFilter = new ReportFilter();
			orderDetailFilter.setYear( "" + Calendar.getInstance().get( Calendar.YEAR ) );
			request.getSession().setAttribute( "orderDetailFilter", orderDetailFilter );
		}
		
		// year
		if (request.getParameter("year") != null) {
			orderDetailFilter.setYear( ServletRequestUtils.getStringParameter( request, "year", "" ));
		}
		
		// Backend Order
		if (request.getParameter("backendOrder") != null) {
			orderDetailFilter.setBackendOrder( ServletRequestUtils.getStringParameter( request, "backendOrder", "" ));
		}
		
		// Date Type
		if (request.getParameter("dateType") != null) {
			orderDetailFilter.setDateType( ServletRequestUtils.getStringParameter( request, "dateType", "orderDate" ));
		}
		
		// Order Type
		if (request.getParameter("orderType") != null) {
			orderDetailFilter.setOrderType( ServletRequestUtils.getStringParameter( request, "orderType", "" ));
		}
		
		// shipping Method
		if (request.getParameter("shippingMethod") != null) {
			orderDetailFilter.setShippingMethod( ServletRequestUtils.getStringParameter( request, "shippingMethod", "" ));
		}
		
		// Product Field
		if (request.getParameter("productField") != null) {
			orderDetailFilter.setProductField( ServletRequestUtils.getStringParameter( request, "productField", null ));
		}
		
		// Product Field Number
		if (request.getParameter("productFieldNumber") != null) {
			orderDetailFilter.setProductFieldNumber( ServletRequestUtils.getStringParameter( request, "productFieldNumber", null ));
		}
		
		// Customer Field
		if (request.getParameter("customerField") != null) {
			orderDetailFilter.setCustomerField( ServletRequestUtils.getStringParameter( request, "customerField", null ));
		}
		
		// Customer Field Number
		if (request.getParameter("customerFieldNumber") != null) {
			orderDetailFilter.setCustomerFieldNumber( ServletRequestUtils.getStringParameter( request, "customerFieldNumber", null ));
		}

		// sales rep
		if (request.getParameter("salesRepId") != null) {
			orderDetailFilter.setSalesRepId(ServletRequestUtils.getIntParameter( request, "salesRepId", -1 ));
		}
		
		return orderDetailFilter;
	}
}