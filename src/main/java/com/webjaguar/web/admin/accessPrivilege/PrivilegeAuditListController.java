/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.accessPrivilege;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class PrivilegeAuditListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception  {

		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i=0; i<ids.length; i++) {
					this.webJaguar.deleteAccessUserAudit(Integer.parseInt(ids[i]));
				}
			}
		}
		Map<String, Object> myModel = new HashMap<String, Object>();
		String username = ServletRequestUtils.getStringParameter( request, "username" );
		PagedListHolder userAuditList = new PagedListHolder(this.webJaguar.getUserAuditList( username ));

		userAuditList.setPageSize(ServletRequestUtils.getIntParameter( request, "size", 10));
		String page = request.getParameter("page");
		if (page != null) {
			userAuditList.setPage(Integer.parseInt(page)-1);
		}
		
		myModel.put("users", userAuditList);
		myModel.put("username", username);
    	
        return new ModelAndView("admin/privilege/auditList", "model", myModel);
	}
}