/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.accessPrivilege;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.AccessPrivilegeForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;

public class PrivilegeFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public PrivilegeFormController() {
		setSessionForm(true);
		setCommandName("accessPrivilegeForm");
		setCommandClass(AccessPrivilegeForm.class);
		setFormView("admin/privilege/form");
		setSuccessView("privilegeList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {

		AccessPrivilegeForm privilegeForm = (AccessPrivilegeForm) command;
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteAccessPrivilege( privilegeForm.getPrivilege().getUser().getUsername() );	
			this.webJaguar.deleteAccessPrivilegeUser( privilegeForm.getPrivilege().getUser().getUsername() );
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		// get group ids
		Set<Object> groupSet = new HashSet<Object>();
		if (privilegeForm.getPrivilege().getUser().getGroupIds() != null){
			String[] groupIds = privilegeForm.getPrivilege().getUser().getGroupIds().split( "," );
			for (int x = 0; x < groupIds.length; x++) {
				if (!("".equals( groupIds[x].trim() ))) {
					groupSet.add( Integer.valueOf( groupIds[x].trim() ) );
				}
			}
		}
		privilegeForm.getPrivilege().getUser().setGroupSetIds(groupSet);
		
		if (privilegeForm.isNewPrivilege()) {
			try {
				this.webJaguar.insertAccessPrivilegeUser( privilegeForm );
				updateAuthority(request, privilegeForm);
			} catch ( Exception e ) {
				e.printStackTrace();
				Map<String, Object> map = new HashMap<String, Object>();
				map.put( "message", "error" );
				return showForm(request, response, errors, map);	
			}
		} else {
			this.webJaguar.deleteAccessPrivilege( privilegeForm.getPrivilege().getUser().getUsername() );
			this.webJaguar.updateAccessPrivilegeUser( privilegeForm.getPrivilege().getUser(), !privilegeForm.getPrivilege().getUser().getUsername().equalsIgnoreCase(privilegeForm.getExistingUsername()),
					privilegeForm.getPrivilege().getUser().getPassword().length() > 0);
			updateAuthority(request, privilegeForm);
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		AccessPrivilegeForm privilegeForm = (AccessPrivilegeForm) command;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "privilege.user.firstName", "form.required", "First Name is required.");    	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "privilege.user.lastName", "form.required", "Last Name is required.");    	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "privilege.user.email", "form.required", "Email is required.");    	

		// chech if user exist on local database.
		
		if ( !privilegeForm.getPrivilege().getUser().getUsername().equalsIgnoreCase( privilegeForm.getExistingUsername() )) {
			if ( this.webJaguar.isUserExist( privilegeForm.getPrivilege().getUser().getUsername() )) {		
				errors.rejectValue( "privilege.user.username", "USER_ALREADY_EXISTS", "Please use another username." );
			} 
		}
		
		if(privilegeForm.getPrivilege().getUser().getSalesRepId() != null) {
			 AccessUser accessUser = this.webJaguar.getAccessUserBySalesRepId(privilegeForm.getPrivilege().getUser().getSalesRepId());
			 if(accessUser != null && privilegeForm.getPrivilege().getUser().getId() != null && !privilegeForm.getPrivilege().getUser().getId().equals(accessUser.getId())){
				errors.rejectValue( "privilege.user.salesRepId", "SALES_REP_ALREADY_ASSIGNED", "Please use another Sales Rep." );
			 }
		}
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws ModelAndViewDefiningException  {
		AccessPrivilegeForm privilegeForm = new AccessPrivilegeForm();
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		if (request.getParameter("id") != null) {
			privilegeForm = new AccessPrivilegeForm(this.webJaguar.getPrivilegeById(new Integer(request.getParameter("id"))));
			privilegeForm.setExistingUsername( privilegeForm.getPrivilege().getUser().getUsername() );
			privilegeForm.setCurrentPassword( privilegeForm.getPrivilege().getUser().getPassword() );
			privilegeForm.getPrivilege().syncRoles();
			getGroupIds(privilegeForm);
		} else if (request.getParameter("id") == null && (Integer) gSiteConfig.get("gNUMBER_ACCESS_PRIVILEGE") <= this.webJaguar.getUserPrivilegeList(null).size()) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("privilegeList.jhtm")));
		}
		
		return privilegeForm;
	}
	
	private void updateAuthority(HttpServletRequest request, AccessPrivilegeForm privilegeForm) {
        
		List<Configuration> data = new ArrayList<Configuration>();

		String keys[] = request.getParameterValues("__key");
		
		for (int i=0; i<keys.length; i++){
			Configuration config = new Configuration();
			config.setKey(privilegeForm.getPrivilege().getUser().getUsername());
			if (request.getParameter(keys[i]) != null) {
				config.setValue(request.getParameter(keys[i]));
				data.add(config);
			}			 
		}
		if (privilegeForm.getPrivilege().getUser().getGroupSetIds() != null && !privilegeForm.getPrivilege().getUser().getGroupSetIds().isEmpty()) {
			for (Object groupId : privilegeForm.getPrivilege().getUser().getGroupSetIds()) {
				List<Configuration> data2 = this.webJaguar.getGroupPrivileges((Integer) groupId, privilegeForm.getPrivilege().getUser().getId());
				data.addAll(data2);
			}
		}
		this.webJaguar.updateAccessPrivilege(data);	
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors)
	{
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("salesReps", this.webJaguar.getSalesRepList());
		return map;
	}
	
	protected void getGroupIds(AccessPrivilegeForm privilegeForm) {
		Set<Object> groupSet = privilegeForm.getPrivilege().getUser().getGroupSetIds();
		Iterator<Object> iter = groupSet.iterator();
		StringBuffer groupIds = new StringBuffer();
		if (iter.hasNext()) {
			groupIds.append(iter.next());
		}
		while ( iter.hasNext() ) {
			groupIds.append(", ");
			groupIds.append(iter.next());
		}
		privilegeForm.getPrivilege().getUser().setGroupIds(groupIds.toString());
	}
}