/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.accessPrivilege;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessGroup;
import com.webjaguar.model.AccessPrivilegeSearch;
import com.webjaguar.model.PrivilegeSearch;

/*
 * add comment here
 * this is test
 */
public class GroupListController extends AbstractCommandController {

	public GroupListController() {
		setCommandClass(PrivilegeSearch.class);
	}   
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handle(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {
    	
    	AccessPrivilegeSearch accessPrivilegeSearch = getAccessPrivilegeSearch( request ); 
    	Map<String, Object> myModel = new HashMap<String, Object>();
		
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("privilegeGroup.jhtm"));
		}
		
		int count = this.webJaguar.getGroupPrivilegeList(null).size();
		if (count < accessPrivilegeSearch.getOffset()-1) {
			accessPrivilegeSearch.setPage(1);
		}
		accessPrivilegeSearch.setLimit(accessPrivilegeSearch.getPageSize());
		accessPrivilegeSearch.setOffset((accessPrivilegeSearch.getPage()-1)*accessPrivilegeSearch.getPageSize());
		
		List<AccessGroup> accessGroupList = this.webJaguar.getGroupPrivilegeList(accessPrivilegeSearch);
		myModel.put("accessGroupList", accessGroupList);
		
		int pageCount = count/accessPrivilegeSearch.getPageSize();
		if (count%accessPrivilegeSearch.getPageSize() > 0) {
			pageCount++;
		}
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", accessPrivilegeSearch.getOffset()+accessGroupList.size());
		myModel.put("count", count);

		return new ModelAndView("admin/privilege/groupList", "model", myModel);
	}
    
    private AccessPrivilegeSearch getAccessPrivilegeSearch(HttpServletRequest request) {
		AccessPrivilegeSearch accessPrivilegeSearch = (AccessPrivilegeSearch) request.getSession().getAttribute( "accessPrivilegeGroupSearch" );
		if (accessPrivilegeSearch == null) {
			accessPrivilegeSearch = new AccessPrivilegeSearch();
			request.getSession().setAttribute( "accessPrivilegeGroupSearch", accessPrivilegeSearch );
		}
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				accessPrivilegeSearch.setPage( 1 );
			} else {
				accessPrivilegeSearch.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 1 );
			if (size < 1) {
				accessPrivilegeSearch.setPageSize( 1 );
			} else {
				accessPrivilegeSearch.setPageSize( size );				
			}
		}
		
		return accessPrivilegeSearch;
	}
}