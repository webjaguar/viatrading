/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.accessPrivilege;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataAccessException;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.GroupPrivilegeForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;

public class GroupPrivilegeFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public GroupPrivilegeFormController() {
		setSessionForm(true);
		setCommandName("groupPrivilegeForm");
		setCommandClass(GroupPrivilegeForm.class);
		setFormView("admin/privilege/groupForm");
		setSuccessView("privilegeGroupList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {

		GroupPrivilegeForm privilegeForm = (GroupPrivilegeForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			if (this.webJaguar.isGroupUserExist(privilegeForm.getPrivilege().getGroup().getId())) {
				model.put("message", "groupHasUser");
				return showForm(request, response, errors, model);	
			} else {
				this.webJaguar.deleteAccessGroup(privilegeForm.getPrivilege().getGroup().getId());
				return new ModelAndView(new RedirectView(getSuccessView()));
			}
		}
		try {
			if (privilegeForm.isNewPrivilege()) {
				this.webJaguar.insertAccessGroup(privilegeForm);
				updateAuthority(request, privilegeForm);
			} else {
				this.webJaguar.deleteAccessPrivilegeGroup(privilegeForm.getPrivilege().getGroup().getId());
				this.webJaguar.updateAccessGroup(privilegeForm.getPrivilege().getGroup());
				updateAuthority(request, privilegeForm);
			}
		} catch (DataAccessException e) {
			e.printStackTrace();
			model.put("message", "form.hasDuplicate");
			return showForm(request, response, errors, model);	
		}	
		return new ModelAndView(new RedirectView(getSuccessView()));
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		GroupPrivilegeForm privilegeForm = (GroupPrivilegeForm) command;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "privilege.group.groupName", "form.required", "Group Name is required.");    	   	
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws ModelAndViewDefiningException  {
		GroupPrivilegeForm privilegeForm = new GroupPrivilegeForm();
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		if ( request.getParameter("id") != null ) {
			privilegeForm = new GroupPrivilegeForm(this.webJaguar.getPrivilegeByGroupId(new Integer(request.getParameter("id"))));
			privilegeForm.getPrivilege().syncRoles();
		}
		
		return privilegeForm;
	}
	
	private void updateAuthority(HttpServletRequest request, GroupPrivilegeForm privilegeForm) {
        
		List<Configuration> data = new ArrayList<Configuration>();

		String keys[] = request.getParameterValues("__key");
		
		for (int i=0; i<keys.length; i++){
			Configuration config = new Configuration();
			config.setKey(privilegeForm.getPrivilege().getGroup().getId().toString());
			if ( request.getParameter(keys[i]) != null) {
				config.setValue(request.getParameter(keys[i]));
				data.add(config);
			}			 
		}			
		this.webJaguar.updateGroupAccessPrivilege(data);	
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors)
	{
		Map<String, Object> map = new HashMap<String, Object>();
		return map;
	}
}