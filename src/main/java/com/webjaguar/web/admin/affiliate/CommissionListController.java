/*
 * Copyright 2005, 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 * @since 07.05.2007
 */

package com.webjaguar.web.admin.affiliate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CommissionReport;
import com.webjaguar.model.CommissionSearch;
import com.webjaguar.model.Customer;

public class CommissionListController implements Controller {

	private WebJaguarFacade webJaguar;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
    	CommissionSearch search = getCommissionSearch( request );
    	int userId = org.springframework.web.bind.ServletRequestUtils.getIntParameter( request, "id", 0 );
		int level = org.springframework.web.bind.ServletRequestUtils.getIntParameter( request, "commission_level", 1 );
		if ( request.getParameter( "l" ) != null )
			level = Integer.parseInt( request.getParameter( "l" ) );
		
		Map<String, Object> myModel = new HashMap<String, Object>();

		if ( userId == 0 ) {
			myModel.put("message", "user.exception.notfound");		
		} else {
			Customer user = this.webJaguar.getCustomerById( userId );
			if ( user == null )
				myModel.put("message", "user.exception.notfound");
			else
				myModel.put( "owner", user );
		}
		
		List<CommissionReport> commissionList = new Vector<CommissionReport>();
		commissionList = this.webJaguar.getCommissionsByUserId( userId, level, search );
		
		
		PagedListHolder commissionListHolder = new PagedListHolder(commissionList);
		commissionListHolder.setPageSize( search.getPageSize() );
		commissionListHolder.setPage( search.getPage() - 1 );
		
		myModel.put( "level", level );
		myModel.put( "commissions", commissionListHolder );
		myModel.put( "total", this.webJaguar.getCommissionTotalByUserId( userId, level ) );
		
        return new ModelAndView("admin/affiliate/commissionList", "model", myModel);
	}
	private CommissionSearch getCommissionSearch(HttpServletRequest request) {
		CommissionSearch commissionSearch = (CommissionSearch) request.getSession().getAttribute( "commissionSearch" );
		if (commissionSearch == null) {
			commissionSearch = new CommissionSearch();
			request.getSession().setAttribute( "commissionSearch", commissionSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			commissionSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}		

		// order number
		if (request.getParameter("orderNum") != null) {
			commissionSearch.setOrderNum( ServletRequestUtils.getStringParameter( request, "orderNum", "" ));
		}
		
		int userId = org.springframework.web.bind.ServletRequestUtils.getIntParameter( request, "id", 0 );
		String level = request.getParameter("commission_level");
		if (request.getParameter("commission_level") != null) {
			try {
				request.getSession().setAttribute("levelSortBy", Integer.parseInt( level ));
			} catch (NumberFormatException e) {
				request.getSession().setAttribute("levelSortBy", 1);				
			}
		}
		if (request.getSession().getAttribute( "levelSortBy") == null) {
			request.getSession().setAttribute("levelSortBy", 1);
			commissionSearch.setLevel( "1" );
		} else {
			switch ((Integer) request.getSession().getAttribute( "levelSortBy")) {
				case 1:
					commissionSearch.setLevel( "1" );
					break;
				case 2:
					commissionSearch.setLevel( "2" );
					break;
				default:
					commissionSearch.setLevel( "1" );
				    break;
			}
		}			
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				commissionSearch.setPage( 1 );
			} else {
				commissionSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				commissionSearch.setPageSize( 10 );
			} else {
				commissionSearch.setPageSize( size );				
			}
		}
		
		return commissionSearch;
	}
	
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}
}