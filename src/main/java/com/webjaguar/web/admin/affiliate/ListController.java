/*
 * Copyright 2005, 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 * @since 07.05.2007
 */

package com.webjaguar.web.admin.affiliate;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CommissionReport;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.SalesRep;

public class ListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
    	
    	CustomerSearch customerSearch = getCustomerSearch(request);
		Map<String, Object> model = new HashMap<String, Object>();

		// get affiliates
		int count = this.webJaguar.getAffiliateListCount(customerSearch);
		if (count < customerSearch.getOffset()-1) {
			customerSearch.setPage(1);
		}
		customerSearch.setLimit(customerSearch.getPageSize());
		customerSearch.setOffset((customerSearch.getPage()-1)*customerSearch.getPageSize());
		List<Customer> affiliateList = this.webJaguar.getAffiliateList(customerSearch);
		int pageCount = count/customerSearch.getPageSize();
		if (count%customerSearch.getPageSize() > 0) {
			pageCount++;
		}
		model.put("pageCount", pageCount);
		model.put("pageEnd", customerSearch.getOffset()+affiliateList.size());
		model.put("count", count);


		for (Customer affiliate : affiliateList) {
			CommissionReport cr = this.webJaguar.getCommissionTotal(affiliate.getId());
			affiliate.setCommissionTotal(cr.getCommissionTotal());
			affiliate.setAffiliateNumOrder(cr.getNumOrder());
		}

		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			Iterator<SalesRep> iter = this.webJaguar.getSalesRepList().iterator();
			while (iter.hasNext()) {
				SalesRep salesRep = iter.next();
				salesRepMap.put(salesRep.getId(), salesRep);
			}
			model.put("salesRepMap", salesRepMap);
		}
		model.put("affiliates", affiliateList );
		//Responsive Admin
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null) { 
			return new ModelAndView("admin-responsive/affiliate/list", "model", model);
		}
        return new ModelAndView("admin/affiliate/list", "model", model);
	}
	
	private CustomerSearch getCustomerSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		CustomerSearch customerSearch = (CustomerSearch) request.getSession().getAttribute( "affiliateSearch" );
		if (customerSearch == null) {
			customerSearch = new CustomerSearch();
			customerSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "affiliateSearch", customerSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			customerSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// first name
		if (request.getParameter("firstName") != null) {
			customerSearch.setFirstName(ServletRequestUtils.getStringParameter( request, "firstName", "" ));
		}
		
		// last name
		if (request.getParameter("lastName") != null) {
			customerSearch.setLastName(ServletRequestUtils.getStringParameter( request, "lastName", "" ));
		}	
		
		// sales rep
		if (request.getParameter("sales_rep_id") != null) {
			customerSearch.setSalesRepId(ServletRequestUtils.getIntParameter( request, "sales_rep_id", -1 ));
		}
		
		// email
		if (request.getParameter("email") != null) {
			customerSearch.setEmail( ServletRequestUtils.getStringParameter( request, "email", "" ));
		}	
		
		// account number
		if (request.getParameter("accountNumber") != null) {
			customerSearch.setAccountNumber( ServletRequestUtils.getStringParameter( request, "accountNumber", "" ));
		}
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				customerSearch.setPage( 1 );
			} else {
				customerSearch.setPage( page );				
			}
		}			
		
		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				customerSearch.setPageSize( 10 );
			} else {
				customerSearch.setPageSize( size );				
			}
		}
		
		return customerSearch;
	}	
}