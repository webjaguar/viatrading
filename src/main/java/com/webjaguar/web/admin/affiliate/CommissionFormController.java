/*
 * Copyright 2005, 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 * @since 07.05.2007
 */

package com.webjaguar.web.admin.affiliate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.CommissionForm;

public class CommissionFormController extends org.springframework.web.servlet.mvc.SimpleFormController
{

	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public CommissionFormController()
	{
		setSessionForm( true );
		setCommandName( "commissionForm" );
		setCommandClass( CommissionForm.class );
		setFormView( "admin/affiliate/form" );
		setSuccessView( "commissionList.jhtm" );
		this.setValidateOnBinding( true );
	}

	public org.springframework.web.servlet.ModelAndView onSubmit(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, Object command,
			org.springframework.validation.BindException errors) throws javax.servlet.ServletException
	{

		CommissionForm comForm = (CommissionForm) command;
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("oid", comForm.getOrderId());
		myModel.put("id", comForm.getUserId());
		myModel.put("l", comForm.getLevel());
		java.util.Map<String, com.webjaguar.model.Configuration> siteConfig = webJaguar.getSiteConfig();

		// check if cancel button was pressed
		if ( request.getParameter( "_cancel" ) != null )
		{
			return new ModelAndView( new RedirectView( getSuccessView() ), myModel  );
		}

		this.webJaguar.updateCommissionStatus( comForm.getCommissionReport() );
		
		return new ModelAndView( new RedirectView( getSuccessView() ),  myModel );
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception
	{
		super.initBinder( request, binder );
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, true, 10 ) );		
	}
	
	protected boolean suppressBinding(HttpServletRequest request)
	{
		if ( request.getParameter( "_cancel" ) != null )
		{
			return true;
		}
		return false;
	}

	protected Object formBackingObject(javax.servlet.http.HttpServletRequest request) throws Exception
	{
		CommissionForm comForm = new CommissionForm();

		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );

		if ( request.getParameter( "uId" ) != null )
		{
			int userId =  Integer.parseInt(request.getParameter( "uId" ) );
			int orderId = Integer.parseInt(request.getParameter( "oId" ) );
			int level = Integer.parseInt(request.getParameter( "l" ) );

			if ( Integer.parseInt( request.getParameter( "l" ) ) == 1 )
				comForm.setCommissionReport( this.webJaguar.getCommissionsByUserIdOrderId( userId, orderId, level ) );
			else if ( Integer.parseInt( request.getParameter( "l" ) ) == 2 )
				comForm.setCommissionReport( this.webJaguar.getCommissionsByUserIdOrderId( userId, orderId, level ) );
			comForm.setUserId( userId );
			comForm.setOrderId( orderId );
			comForm.setLevel( level );
		}

		return comForm;

	}
}
