/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 11.07.2006
 */

package com.webjaguar.web.admin.orders;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.record.formula.functions.And;
import org.glassfish.external.statistics.annotations.Reset;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.dao.StagingDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerBudgetPartner;
import com.webjaguar.model.CustomerCreditHistory;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.Deal;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.PackingListSearch;
import com.webjaguar.model.PaymentSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.Promo;
import com.webjaguar.model.PromoSearch;
import com.webjaguar.model.SalesRepSearch;
import com.webjaguar.model.State;
import com.webjaguar.model.Supplier;
import com.webjaguar.model.UpsOrder;
import com.webjaguar.model.ViaDeal;
import com.webjaguar.web.admin.customer.CreditImp;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.Utilities;
import com.webjaguar.web.form.InvoiceForm;

import BorderFree.GetApplicationStyle;

public class InvoiceFormController extends SimpleFormController{
	
	protected WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private StagingDao stagingDao;
	public void setStagingDao(StagingDao stagingDao) { this.stagingDao = stagingDao; }
	private HashMap<String, String> err = null;
	
	protected Map<String, Configuration> siteConfig;
	protected Map<String, Object> gSiteConfig;
		
	public InvoiceFormController() {
		setSessionForm(true);
		setCommandName("invoiceForm");
		setCommandClass(InvoiceForm.class);
		setFormView("admin/orders/form");
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
		// allows null Double
		binder.registerCustomEditor(Double.class, new CustomNumberEditor(Double.class, true));
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy[hh:mm a]", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, false, 20 ) );	
		
		binder.registerCustomEditor(Date.class, "order.agreedPaymentDate", new CustomDateEditor(df, true, 20));
	    binder.registerCustomEditor(Date.class, "order.agreedPickUpDate", new CustomDateEditor(df, true, 20));
	    binder.registerCustomEditor(Date.class, "order.userDueDate", new CustomDateEditor(df, true, 20));

		
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		
		InvoiceForm invoiceForm = (InvoiceForm) command;
		Map<String, Object> map = new HashMap<String, Object>();
		
		invoiceForm.getOrder().setDateOrdered( new Timestamp(invoiceForm.getOrderDate().getTime()) );
		
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			if(request.getSession().getAttribute("prompoTypeOrder") != null) {
				request.getSession().setAttribute("prompoTypeOrder", null);
			}
			return new ModelAndView(new RedirectView("invoice.jhtm?order="+invoiceForm.getOrder().getOrderId()));
		}

		invoiceForm.getOrder().setSubTotal(updateLineItems(request, invoiceForm, errors));
			
		//set tax on shipping
		State state = this.webJaguar.getStateByCode(invoiceForm.getOrder().getShipping().getCountry(), invoiceForm.getOrder().getShipping().getStateProvince());
		if (state != null) {
			invoiceForm.getOrder().setTaxOnShipping(state.isApplyShippingTax());
		}
		this.webJaguar.updateGrandTotal(gSiteConfig, invoiceForm.getOrder());
				
		UpsOrder upsWorldShip = null;
		if(siteConfig.get("WORLD_SHIP_IMPORT_ORDERS").getValue().equals("true")) {			
			upsWorldShip = new UpsOrder();
			upsWorldShip.setCustomerPO2(request.getParameter("customerPO2"));
			upsWorldShip.setCarrierPhone(request.getParameter("carrierPhone"));
		}

		Order oldOrder = this.webJaguar.getOrder(invoiceForm.getOrder().getOrderId(), null);
		boolean partnersUpdated = false;
		//Check if order has a payment made using customer's credit 
		if((oldOrder.getGrandTotal().compareTo(invoiceForm.getOrder().getGrandTotal()) != 0) && oldOrder.getCreditUsed() != null && oldOrder.getCreditUsed() > 0 ) {			
			map.put("paymentMessage", "This order has a credit payment, please cancel the payment and then make the changes.");
			return showForm(request, response, errors, map);			
		}
		
		if (request.getParameter("__update") != null) {
			map.put("upsWorldShip", upsWorldShip);
			
			for (int skuIndex = 1; skuIndex <= 20; skuIndex++) {
				String sku = ServletRequestUtils.getStringParameter( request, "__addProduct" + skuIndex + "_sku", "" );
				if ( !sku.isEmpty() ) {
					Integer existingId = this.webJaguar.getProductIdBySku(sku);
					//Product product = this.webJaguar.getProductById( existingId, request );
					Product product = this.webJaguar.getProductById(existingId, 1, false, null);
					if (product == null && !siteConfig.get("ORDER_ADD_VALID_SKU").getValue().equals("true")) {
						product = new Product();
						product.setSku( sku );
					}
					if (product == null && siteConfig.get("ORDER_ADD_VALID_SKU").getValue().equals("true")) {
						errors.reject("invalidSku", "Invalid Sku");
					}
					if (product != null) {
						// check if the product is LNOW product and has cost, if doesn't has cost, cannot add to invoice
						if(sku.contains("-")){
							String[] skuContainsDash=Utilities.split(sku, "-");
							Integer supplierID = this.webJaguar.getCustomerIdBySupplierPrefix(skuContainsDash[0]);
							if(supplierID != null){
								Supplier supplier = this.webJaguar.getProductSupplierPrimaryBySku(sku);
								if(supplier == null){
									errors.reject("invalidSku", "This sku is LNOW sku but doesn't have supplier");
								}else if(supplier.getPrice() == null){
									errors.reject("invalidSku", "This sku has supplier but doesn't have cost value");
								}else{
									LineItem lineItem = new LineItem();
									this.setLineItem(lineItem, product, skuIndex, request, invoiceForm);
									invoiceForm.getOrder().getInsertedLineItems().add(lineItem);
								}
							}else{
								LineItem lineItem = new LineItem();
								this.setLineItem(lineItem, product, skuIndex, request, invoiceForm);
								invoiceForm.getOrder().getInsertedLineItems().add(lineItem);
							}
							
						}else{
							LineItem lineItem = new LineItem();
							this.setLineItem(lineItem, product, skuIndex, request, invoiceForm);
							invoiceForm.getOrder().getInsertedLineItems().add(lineItem);
						}
				
					}
				}
			}
			
			// validate promo code again and set promo to insertedLineItems
//			Customer customer = this.webJaguar.getCustomerById( invoiceForm.getOrder().getUserId() );
//			this.webJaguar.validatePromoCode(invoiceForm.getOrder(), err, customer, invoiceForm.getOrder().getPromo().getTitle(), gSiteConfig);	
//			for (Map.Entry<String, String> entry : err.entrySet()){
//				try{
//				invoiceForm.getOrder().getPromo().setDiscount(null);
//				}catch(Exception e){
//					//nothing
//				}
//				errors.rejectValue("order.promo.discount", entry.getKey(), entry.getValue());
//			}
			
			// insertedLineItems logic for viaDeal start====				
			
			if (invoiceForm.getOrder().getLineItems() != null || invoiceForm.getOrder().getInsertedLineItems() != null) {		
		//	    get the map first
				Map<String, Integer> parentSkuTotalQtyMap = new HashMap<String, Integer>();
				Map<String, Double> parentSkuTotalAmtMap = new HashMap<String, Double>();
			    
				List<LineItem> lineItems = new ArrayList<LineItem>();
				if (invoiceForm.getOrder().getInsertedLineItems() != null ) {
					for (LineItem insertedLineItem: invoiceForm.getOrder().getInsertedLineItems()) {
						lineItems.add(insertedLineItem);
					}
				}
				if (invoiceForm.getOrder().getLineItems() != null) {
					for (LineItem lineItem: invoiceForm.getOrder().getLineItems()) {
						lineItems.add(lineItem);
					}
				}
			    
			    for (LineItem lineItem : lineItems) {
					Integer productId = lineItem.getProduct().getId();
					Product product = this.webJaguar.getProductById(productId, 0, true, null);
					String parentSku = product.getMasterSku();		
					if (parentSku != null && !parentSku.isEmpty() && parentSkuTotalQtyMap.containsKey(parentSku)) {
						parentSkuTotalQtyMap.replace(parentSku, parentSkuTotalQtyMap.get(parentSku) + lineItem.getQuantity());
						if (product.getPrice1() != null && parentSkuTotalAmtMap.get(parentSku) != null) {
							parentSkuTotalAmtMap.replace(parentSku, parentSkuTotalAmtMap.get(parentSku) + product.getPrice1() * lineItem.getQuantity());
						}
					} else {
						parentSkuTotalQtyMap.put(parentSku,  lineItem.getQuantity());
						if (product.getPrice1() != null) {
							parentSkuTotalAmtMap.put(parentSku, product.getPrice1() * lineItem.getQuantity());
						}
					}	
			    }
			    
		//	    check viaDeal module
			    for (LineItem lineItem : lineItems) {
					Integer productId = lineItem.getProduct().getId();
					Product product = this.webJaguar.getProductById(productId, 0, true, null);
					String parentSku = product.getMasterSku();
								
					ViaDeal viaDeal = this.webJaguar.getViaDealBySku(parentSku);
					if (viaDeal != null && viaDeal.isTriggerType()) {
		// 				triggerType is add up quantity
						if (parentSku != null && !parentSku.isEmpty() && parentSkuTotalQtyMap.containsKey(parentSku) && parentSkuTotalQtyMap.get(parentSku) >= viaDeal.getTriggerThreshold() ) {
							if (viaDeal.isDiscountType()) {
		//						discount type is percent
								Double discount =  viaDeal.getDiscount();
								lineItem.setDiscount(discount);				
								lineItem.setPercent(true);
							} else {
		//						discount type is fixed amount
								Double discount =  viaDeal.getDiscount();
								lineItem.setDiscount(discount);
								lineItem.setPercent(false);
							}	
						}
					} else if (viaDeal != null && !viaDeal.isTriggerType()) {
		// 				triggerType is add up price
						if (parentSku != null && !parentSku.isEmpty() && parentSkuTotalQtyMap.containsKey(parentSku) && parentSkuTotalAmtMap.get(parentSku) >= viaDeal.getTriggerThreshold() ) {				
							if (viaDeal.isDiscountType()) {
		//						discount type is percent
								Double discount =  viaDeal.getDiscount();
								lineItem.setDiscount(discount);	
								lineItem.setPercent(true);
							} else {
		//					discount type is fixed amount
								Double discount =  viaDeal.getDiscount();
								lineItem.setDiscount(discount);			
								lineItem.setPercent(false);
							}	
						}
					}				
			    }	
			}			
			// insertedLineItems logic for viaDeal end ====
						
			Map<Integer, Double> partnerHistoryMap = new HashMap<Integer, Double>();
			if(invoiceForm.getOrder().getOrderId() != null && invoiceForm.getPartnersList() != null && (Boolean) gSiteConfig.get("gBUDGET") && siteConfig.get("BUDGET_ADD_PARTNER").getValue().equalsIgnoreCase("true")) {
				Double budgetAmount = 0.00;
				for(CustomerBudgetPartner orderPartner: invoiceForm.getPartnersList()) {
					// if there is a change in the partners amount then update the values and recalculate. 
					if(request.getParameter("__partner_amount_"+orderPartner.getPartnerId()) != null && (orderPartner.getAmount().compareTo(Double.parseDouble(request.getParameter("__partner_amount_"+orderPartner.getPartnerId()))) != 0)){
						orderPartner.setAmount(Double.parseDouble(request.getParameter("__partner_amount_"+orderPartner.getPartnerId())));
						partnersUpdated = true;
						partnerHistoryMap.put(orderPartner.getPartnerId(), orderPartner.getAmount());
					}
					budgetAmount = budgetAmount + orderPartner.getAmount();
				}
				// As value of "__partner_amount_" is negative multiply with negative value. 
				invoiceForm.getOrder().setBudgetEarnedCredits((-1) * budgetAmount);
				invoiceForm.setBudgetPartnerHistory(partnerHistoryMap);
				map.put("partnerList", invoiceForm.getPartnersList());
			}			
		}
		
		// check if there is a change in the order by comparing the grand totals
		if ((oldOrder.getCreditUsed() != null && oldOrder.getCreditUsed() > 0) && (oldOrder.getGrandTotal() != invoiceForm.getOrder().getGrandTotal())) {
			if (oldOrder.getCreditUsed() == oldOrder.getGrandTotal() ) {
				invoiceForm.getOrder().setCreditUsed(invoiceForm.getOrder().getGrandTotal());					
			} else if(oldOrder.getGrandTotal() > invoiceForm.getOrder().getGrandTotal()) {
				if(oldOrder.getCreditUsed() < (oldOrder.getGrandTotal() - invoiceForm.getOrder().getGrandTotal())) {
					invoiceForm.getOrder().setCreditUsed(oldOrder.getCreditUsed());	
				} else {
					invoiceForm.getOrder().setCreditUsed(oldOrder.getCreditUsed() - (oldOrder.getGrandTotal() - invoiceForm.getOrder().getGrandTotal()));						
				}
			}
		}

		// check if save button was pressed
		if (request.getParameter("__save") != null) {
			if(request.getSession().getAttribute("prompoTypeOrder") != null) {
				request.getSession().setAttribute("prompoTypeOrder", null);
			}
			if (errors.hasErrors()) {
				return showForm(request, response, errors, map);				
			}
			Customer customer = this.webJaguar.getCustomerById( invoiceForm.getOrder().getUserId() );
			
			if ((Boolean) gSiteConfig.get("gINVENTORY") && invoiceForm.getOrder().getOrderId() != null) {
				for(LineItem lineItem: invoiceForm.getOrder().getInsertedLineItems()){
					if(lineItem.getUpdateOnHandQty() != null){
						this.webJaguar.updateOnHand(lineItem.getProduct().getSku(), lineItem.getUpdateOnHandQty());
						
						System.out.println("inventory history record 233");
						InventoryActivity inventoryActivity = new InventoryActivity();
					
	
							inventoryActivity.setSku(lineItem.getProduct().getSku());
							inventoryActivity.setReference(invoiceForm.getOrder().getOrderId().toString());
							inventoryActivity.setAccessUserId(invoiceForm.getOrder().getAccessUserId());
							inventoryActivity.setType("Order update");
							inventoryActivity.setQuantity(lineItem.getUpdateOnHandQty());
							Inventory inventory = new Inventory();
							inventory.setSku(lineItem.getProduct().getSku());
							inventory = this.webJaguar.getInventory(inventory);
							inventoryActivity.setInventory(inventory.getInventory());
						   this.webJaguar.addKitPartsHistory(inventoryActivity);
						
					}
				}
			}
			
			if ((oldOrder.getCreditUsed() != null && oldOrder.getCreditUsed() > 0) && (oldOrder.getGrandTotal() != invoiceForm.getOrder().getGrandTotal())) {
				CreditImp credit= new CreditImp();

				if (oldOrder.getCreditUsed() >= oldOrder.getGrandTotal() ) {
					CustomerCreditHistory customerCredit =credit.updateCustomerCreditByOrderPayment(invoiceForm.getOrder(), "payment", oldOrder.getGrandTotal() - invoiceForm.getOrder().getGrandTotal(), this.webJaguar.getAccessUser(request).getUsername());
	    			this.webJaguar.updateCredit(customerCredit);
				} else if(oldOrder.getGrandTotal() > invoiceForm.getOrder().getGrandTotal()) {
					if(oldOrder.getCreditUsed() < (oldOrder.getGrandTotal() - invoiceForm.getOrder().getGrandTotal())) {
						CustomerCreditHistory customerCredit =credit.updateCustomerCreditByOrderPayment(invoiceForm.getOrder(), "payment", oldOrder.getCreditUsed(), this.webJaguar.getAccessUser(request).getUsername());
						this.webJaguar.updateCredit(customerCredit);
					} else {
						CustomerCreditHistory customerCredit=credit.updateCustomerCreditByOrderPayment(invoiceForm.getOrder(), "payment", oldOrder.getGrandTotal() - invoiceForm.getOrder().getGrandTotal(), this.webJaguar.getAccessUser(request).getUsername());
						this.webJaguar.updateCredit(customerCredit);
					}
				}
			}
			if((Boolean) gSiteConfig.get("gBUDGET") && siteConfig.get("BUDGET_ADD_PARTNER").getValue().equalsIgnoreCase("true") && invoiceForm.getBudgetPartnerHistory() != null && !invoiceForm.getBudgetPartnerHistory().isEmpty()) {
				// get accessUser username
		    	String accessUser = this.webJaguar.getAccessUser(request).getUsername();
				Iterator it = invoiceForm.getBudgetPartnerHistory().entrySet().iterator();
				while (it.hasNext()) { 

					Map.Entry partner = (Map.Entry)it.next();
			    	CustomerBudgetPartner partnerHistory = new CustomerBudgetPartner();
					partnerHistory.setCreatedBy(accessUser);
					partnerHistory.setDate(new Date());
					partnerHistory.setPartnerId((Integer) partner.getKey());
					partnerHistory.setOrderId(invoiceForm.getOrder().getOrderId());
					partnerHistory.setUserId(invoiceForm.getOrder().getUserId());
			    	
					// update customer's credit
					List<CustomerBudgetPartner> partnerList = this.webJaguar.getCustomerPartnerHistory(partnerHistory);
					if(partnerList != null) {
						for(CustomerBudgetPartner updatePartner: partnerList) {
							if(updatePartner.getPartnerId()!= null && updatePartner.getPartnerId().compareTo((Integer) partner.getKey()) == 0) {
								//update customer's budget credits with old earned credits applied - new earned credits applied.
								updatePartner.setAmount(((-1) * updatePartner.getAmount()) - ((-1) * (Double) partner.getValue()));
								this.webJaguar.updatePartnerCustomer(updatePartner, false);
							}
						}
					}
					
					// update customer partner history
					partnerHistory.setAmount(((Double) partner.getValue()));
					this.webJaguar.updateCustomerPartnerHistory(partnerHistory);
			    }
			}
			
			// save order
			this.webJaguar.updateOrder(invoiceForm.getOrder());	
			
			if(upsWorldShip != null) {				
				upsWorldShip.setAccountNumber(customer.getAccountNumber());
				upsWorldShip.setCompanyName(customer.getAddress().getCompany());
				upsWorldShip.setAddr1(customer.getAddress().getAddr1());
				upsWorldShip.setCity(customer.getAddress().getCity());
				upsWorldShip.setStateProvince(customer.getAddress().getStateProvince());
				upsWorldShip.setCustomerPO(invoiceForm.getOrder().getPurchaseOrder());
				//Superior Washer has only one lineItems so LineItem is set to '0'
				upsWorldShip.setLineItems(invoiceForm.getOrder().getLineItems().get(0).getProduct().getSku());
				upsWorldShip.setWeight(invoiceForm.getOrder().getLineItems().get(0).getProduct().getWeight());
				upsWorldShip.setNumberPackages(invoiceForm.getOrder().getLineItems().get(0).getQuantity());
				upsWorldShip.setShippingCompany(invoiceForm.getOrder().getShippingMethod());
				upsWorldShip.setOrderId(invoiceForm.getOrder().getOrderId());
				this.webJaguar.updateUpsOrders(upsWorldShip);
			}
			
			if(request.getParameter("orderName") != null ) {
				return new ModelAndView(new RedirectView("invoice.jhtm?order="+invoiceForm.getOrder().getOrderId()+"&orderName="+request.getParameter("orderName")));
			} else {
				return new ModelAndView(new RedirectView("invoice.jhtm?order="+invoiceForm.getOrder().getOrderId()));
			}
		} 
		
		return showForm(request, response, errors, map);
		
	}
	
	public Object formBackingObject(HttpServletRequest request) 
	throws Exception {
		
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
	    InvoiceForm invoiceForm = new InvoiceForm();
	    if ( request.getParameter("orderId") != null ) {
	    	invoiceForm = new InvoiceForm(this.webJaguar.getOrder(new Integer(request.getParameter("orderId")), siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue()));
	    	if ( invoiceForm.getOrder().getPromo() == null ) {
	    		invoiceForm.getOrder().setPromo( new Promo() );	    		
	    	}
	    	invoiceForm.getOrder().setDeletedLineItems( new ArrayList<LineItem>() ); 
	    }
	    
	  //set values for price override
	    try{
		   for(LineItem li : invoiceForm.getOrder().getLineItems()){
		    	Product prod = this.webJaguar.getProductById(li.getProductId(), request);
		    	if (prod != null) {
			    	li.getProduct().setRestrictPriceChange(prod.isRestrictPriceChange());
		    	}
		    }
	    }catch(Exception e){
	    	e.printStackTrace();
	    	//do nothing
	    }
		   
	    invoiceForm.setOrderDate( invoiceForm.getOrder().getDateOrdered() );
	    invoiceForm.getOrder().setInsertedLineItems(new ArrayList<LineItem>());
	    if(invoiceForm!=null && invoiceForm.getOrder()!=null && invoiceForm.getOrder().getQualifier()!=null && invoiceForm.getOrder().getQualifier()<=0)
	    {
	    	Customer customer = this.webJaguar.getCustomerById(invoiceForm.getOrder().getUserId());
	    	invoiceForm.getOrder().setQualifier((customer!=null && customer.getQualifier()!=null) ? customer.getQualifier() : 0);
	    }else if(invoiceForm!=null && invoiceForm.getOrder()!=null && invoiceForm.getOrder().getQualifier()==null && invoiceForm.getOrder().getUserId()!=null){
	    	Customer customer = this.webJaguar.getCustomerById(invoiceForm.getOrder().getUserId());
	    	if(customer!=null && customer.getQualifier()!=null){
	    		invoiceForm.getOrder().setQualifier(customer.getQualifier());
	    	}
	    }
	    return invoiceForm;
    }
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		InvoiceForm invoiceForm = (InvoiceForm) command;
		Map<String, Double> map = invoiceForm.getOrder().getLineItemPromos();
		Set set = map.keySet();
		Iterator it = set.iterator();
		while(it.hasNext()) {
			System.out.println(it.next() + "aaaaaaaaa");
		}
		if (invoiceForm.getOrder().getPromo().getTitle() != null &&
				invoiceForm.getOrder().getPromo().getTitle().trim().equals( "" )){
			invoiceForm.getOrder().getPromo().setTitle( null );
		}
		
		if ( invoiceForm.getOrder().getPromo().getTitle() != null ) {
	
			try{
				invoiceForm.getOrder().getPromo().setDiscount((Utilities.roundFactory(ServletRequestUtils.getDoubleParameter(request,"order.promo.discount"),2,BigDecimal.ROUND_HALF_UP)));
			
			} catch(Exception e){
				invoiceForm.getOrder().getPromo().setDiscount(0.00);
			}
		

			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "order.promo.discount", "form.required", "required");
			Customer customer = this.webJaguar.getCustomerById( invoiceForm.getOrder().getUserId() );
			err = new HashMap<String, String>();
		
			
			invoiceForm.getOrder().setSubTotal(updateLineItems(request, invoiceForm, errors));
			
            // if the admin recalculate the same promo  twice then  do nothing
			if (invoiceForm.getOrder()!=null) {
				
            Integer orderId = invoiceForm.getOrder().getOrderId();
            OrderSearch search = new OrderSearch();
            if (orderId!=null) {
            Order order = this.webJaguar.getOrder(orderId, "line_num ASC");
           
            String promoTitle1 = order.getPromo().getTitle();
            String promoTitle2 = request.getParameter("order.promo.title");
            if(promoTitle1 != null) {
            	 promoTitle1 = promoTitle1.trim();
            	 // reset promo code on oder if the PROMO type is product, reset promoTitle1 to null and validate the promo code.
            	 Promo promoOnOrder = this.webJaguar.getPromo(promoTitle1);
            	 if (promoOnOrder.getDiscountType().equals("product")) {
            		 promoTitle1 = null;
            	 }
            }
                        
            if(promoTitle2 != null) {
             promoTitle2 = promoTitle2.trim();
            }
            if(!promoTitle2.equalsIgnoreCase((promoTitle1))) {	  
            	
//				======Add insertedLineItem when it's first time added before validation=======start           	
//				for (int skuIndex = 1; skuIndex <= 20; skuIndex++) {
//					String sku = ServletRequestUtils.getStringParameter( request, "__addProduct" + skuIndex + "_sku", "" );
//					if ( !sku.isEmpty() ) {
//						Integer existingId = this.webJaguar.getProductIdBySku(sku);
//						//Product product = this.webJaguar.getProductById( existingId, request );
//						Product product = this.webJaguar.getProductById(existingId, 1, false, null);
//						if (product == null && !siteConfig.get("ORDER_ADD_VALID_SKU").getValue().equals("true")) {
//							product = new Product();
//							product.setSku( sku );
//						}
//						if (product == null && siteConfig.get("ORDER_ADD_VALID_SKU").getValue().equals("true")) {
//							errors.reject("invalidSku", "Invalid Sku");
//						}
//						if (product != null) {
//							// check if the product is LNOW product and has cost, if doesn't has cost, cannot add to invoice
//							if(sku.contains("-")){
//								String[] skuContainsDash=Utilities.split(sku, "-");
//								Integer supplierID = this.webJaguar.getCustomerIdBySupplierPrefix(skuContainsDash[0]);
//								if(supplierID != null){
//									Supplier supplier = this.webJaguar.getProductSupplierPrimaryBySku(sku);
//									if(supplier == null){
//										errors.reject("invalidSku", "This sku is LNOW sku but doesn't have supplier");
//									}else if(supplier.getPrice() == null){
//										errors.reject("invalidSku", "This sku has supplier but doesn't have cost value");
//									}else{
//										LineItem lineItem = new LineItem();
//										this.setLineItem(lineItem, product, skuIndex, request, invoiceForm);
//										invoiceForm.getOrder().getInsertedLineItems().add(lineItem);
//									}
//								}else{
//									LineItem lineItem = new LineItem();
//									this.setLineItem(lineItem, product, skuIndex, request, invoiceForm);
//									invoiceForm.getOrder().getInsertedLineItems().add(lineItem);
//								}
//								
//							}else{
//								LineItem lineItem = new LineItem();
//								this.setLineItem(lineItem, product, skuIndex, request, invoiceForm);
//								invoiceForm.getOrder().getInsertedLineItems().add(lineItem);
//							}
//					
//						}
//					}
//				}         	          	
//				======Add insertedLineItem when it's first time added before validation=======end
            	
              	this.webJaguar.validatePromoCode(invoiceForm.getOrder(), err, customer, invoiceForm.getOrder().getPromo().getTitle(), gSiteConfig);
          
//				======Remove insertedLineItem when it's first time added after validation, prevent duplicate, it is added again in" update"=======start
//              	for (int skuIndex = 1; skuIndex <= 20; skuIndex++) {
//					String sku = ServletRequestUtils.getStringParameter( request, "__addProduct" + skuIndex + "_sku", "" );
//					if ( !sku.isEmpty() ) {
//						Integer existingId = this.webJaguar.getProductIdBySku(sku);
//						//Product product = this.webJaguar.getProductById( existingId, request );
//						Product product = this.webJaguar.getProductById(existingId, 1, false, null);
//						if (product == null && !siteConfig.get("ORDER_ADD_VALID_SKU").getValue().equals("true")) {
//							product = new Product();
//							product.setSku( sku );
//						}
//						if (product == null && siteConfig.get("ORDER_ADD_VALID_SKU").getValue().equals("true")) {
//							errors.reject("invalidSku", "Invalid Sku");
//						}
//						if (product != null) {
//							// check if the product is LNOW product and has cost, if doesn't has cost, cannot add to invoice
//							if(sku.contains("-")){
//								String[] skuContainsDash=Utilities.split(sku, "-");
//								Integer supplierID = this.webJaguar.getCustomerIdBySupplierPrefix(skuContainsDash[0]);
//								if(supplierID != null){
//									Supplier supplier = this.webJaguar.getProductSupplierPrimaryBySku(sku);
//									if(supplier == null){
//										errors.reject("invalidSku", "This sku is LNOW sku but doesn't have supplier");
//									}else if(supplier.getPrice() == null){
//										errors.reject("invalidSku", "This sku has supplier but doesn't have cost value");
//									}else{
//										LineItem lineItem = new LineItem();
//										this.setLineItem(lineItem, product, skuIndex, request, invoiceForm);
//										invoiceForm.getOrder().getInsertedLineItems().remove(invoiceForm.getOrder().getInsertedLineItems().size() - 1);
//									}
//								}else{
//									LineItem lineItem = new LineItem();
//									this.setLineItem(lineItem, product, skuIndex, request, invoiceForm);
//									invoiceForm.getOrder().getInsertedLineItems().remove(invoiceForm.getOrder().getInsertedLineItems().size() - 1);
//								}								
//							}else{
//								LineItem lineItem = new LineItem();
//								this.setLineItem(lineItem, product, skuIndex, request, invoiceForm);
//								invoiceForm.getOrder().getInsertedLineItems().remove(invoiceForm.getOrder().getInsertedLineItems().size() - 1);
//							}					
//						}
//					}
//				}
//				======Remove insertedLineItem when it's first time added after validation, prevent duplicate, it is added again in" update"=======end
            	
              	// help to add multiple promos of type product and one promo of type order if they are valid
             	if(err.isEmpty()) {
            		
			            		Order orderNew = invoiceForm.getOrder();
			            		System.out.println("existing title "+invoiceForm.getOrder().getPromo().getTitle());
			            		System.out.println("existing TYPE "+invoiceForm.getOrder().getPromo().getDiscountType());

			            		if(invoiceForm.getOrder().getPromo().getDiscountType().equalsIgnoreCase("order")) {
			            			
			            			Promo p = new Promo();
			            			String title = invoiceForm.getOrder().getPromo().getTitle();
			            			p = this.webJaguar.getPromoByName(title);
			            			orderNew.setPromo(p);
			            			//request.getSession().setAttribute("prompoTypeOrder", title);
			            		} else if(invoiceForm.getOrder().getPromo().getDiscountType().equalsIgnoreCase("product")) {
			            			//String s =  (String) request.getSession().getAttribute("prompoTypeOrder");
				            		//System.out.println("prompoTypeOrder code "+s);

						/*
						 * if(s != null ) { Promo pr = this.webJaguar.getPromoByName(s);
						 * orderNew.setPromo(pr); }
						 */
			            		    // add it if it's of promo of type product			            				        				
			                		orderNew.getLineItemPromos().put(invoiceForm.getOrder().getPromo().getTitle(), invoiceForm.getOrder().getPromo().getDiscount());
			                		System.out.println("testing");
			            		}
			            		invoiceForm.setOrder(orderNew);
             	}
            }
			}
			}
			
			//this.webJaguar.validateAdminPromoCode(request, invoiceForm.getOrder(), err, customer, invoiceForm.getOrder().getPromo().getTitle(), gSiteConfig, invoiceForm.getOrder().getPromo().getDiscount());

			for (Map.Entry<String, String> entry : err.entrySet()){
				try{
				invoiceForm.getOrder().getPromo().setDiscount(null);
//				Reset lineItem Promo
				for (LineItem lineItem : invoiceForm.getOrder().getLineItems()) {
					if (lineItem != null) {
						lineItem.setPromo(null);
						lineItem.setPromoAmount();
					}
				}			
				}catch(Exception e){
					//nothing
				}
				errors.rejectValue("order.promo.discount", entry.getKey(), entry.getValue());
			}
		}
		
		if (invoiceForm.getOrder().getPromo().getDiscount() != null) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "order.promo.title", "form.required", "required");			
			if (invoiceForm.getOrder().getPromo().getDiscount() < 0) {
				errors.rejectValue("order.promo.discount", "form.negAmount", "should be a positive amount");
			} else {
				if (invoiceForm.getOrder().getPromo().isPercent()) {
					if ( invoiceForm.getOrder().getPromo().getDiscount() > 100.0 )
						errors.rejectValue("order.promo.discount", "maxis100", "Maximum is 100%");					
				} else {
					if ( invoiceForm.getOrder().getPromo().getDiscount() > invoiceForm.getOrder().getSubTotal() ) {
						errors.rejectValue("order.promo.discount", "maxisSubtotal", "Discount  is greater than Subtotal ");											
					}
				}
			}
		}
		//this.webJaguar.updateTaxAndTotal(gSiteConfig, globalDao, invoiceForm.getOrder());

	}

	protected double updateLineItems(HttpServletRequest request, InvoiceForm invoiceForm, BindException errors) throws ServletRequestBindingException{		
		
		// subtotal
		double subTotal = 0;
		
		Map<String, String> serialNumsMap = new HashMap<String, String>();
		boolean hasDuplicateSNs = false;
		
		Iterator<LineItem> iter = invoiceForm.getOrder().getLineItems().iterator();
		while (iter.hasNext()) {
			LineItem lineItem = iter.next();
			// check if removed
			if (ServletRequestUtils.getBooleanParameter( request, "__remove_" + lineItem.getLineNumber(), false )) {
				invoiceForm.getOrder().getDeletedLineItems().add( lineItem );
				iter.remove();
				if ((Boolean) gSiteConfig.get("gINVENTORY") ) {
					this.webJaguar.updateOnHand(lineItem.getProduct().getSku(), lineItem.getQuantity());
					System.out.println("inventory history record 500");
					InventoryActivity inventoryActivity = new InventoryActivity();				

						inventoryActivity.setSku(lineItem.getProduct().getSku());
						inventoryActivity.setReference(invoiceForm.getOrder().getOrderId().toString());
						inventoryActivity.setAccessUserId(invoiceForm.getOrder().getAccessUserId());
						inventoryActivity.setType("Order update");
						inventoryActivity.setQuantity(lineItem.getQuantity());
						Inventory inventory = new Inventory();
						inventory.setSku(lineItem.getProduct().getSku());
						inventory = this.webJaguar.getInventory(inventory);
						inventoryActivity.setInventory(inventory.getInventory());
					   this.webJaguar.addKitPartsHistory(inventoryActivity);
				}
			}			
		}
//		int lengthOfLineItems = invoiceForm.getOrder().getLineItems().size();
		for (LineItem lineItem:invoiceForm.getOrder().getLineItems()) {
			
			// get quantity
			int oldQuantity = lineItem.getQuantity();
			lineItem.setQuantity( ServletRequestUtils.getIntParameter( request, "__quantity_" + lineItem.getLineNumber(), lineItem.getQuantity() ) );
			if ((Boolean) gSiteConfig.get("gINVENTORY") && oldQuantity != lineItem.getQuantity()) {
				PackingListSearch packingListSearch = new PackingListSearch();
				packingListSearch.setOrderId(invoiceForm.getOrder().getOrderId());
				if (!this.webJaguar.getPackingListByOrderId(packingListSearch).isEmpty()) {
					lineItem.setQuantity(oldQuantity);
					errors.reject("form.deletePackingList");
				} else {
					this.webJaguar.updateOnHand(lineItem.getProduct().getSku(), -(lineItem.getQuantity() - oldQuantity));
					
					System.out.println("inventory history record 532");
					InventoryActivity inventoryActivity = new InventoryActivity();
				

						inventoryActivity.setSku(lineItem.getProduct().getSku());
						inventoryActivity.setReference(invoiceForm.getOrder().getOrderId().toString());
						inventoryActivity.setAccessUserId(invoiceForm.getOrder().getAccessUserId());
						inventoryActivity.setType("Order update");
						inventoryActivity.setQuantity( -(lineItem.getQuantity() - oldQuantity));
						Inventory inventory = new Inventory();
						inventory.setSku(lineItem.getProduct().getSku());
						inventory = this.webJaguar.getInventory(inventory);
						inventoryActivity.setInventory(inventory.getInventory());
					   this.webJaguar.addKitPartsHistory(inventoryActivity);
				}
			}
			// get original price 
			try {
				lineItem.setOriginalPrice(Utilities.roundFactory(ServletRequestUtils.getDoubleParameter( request, "__originalPrice_" + lineItem.getLineNumber()), 3, BigDecimal.ROUND_HALF_UP));
			} catch(Exception e) {
				// do nothing
			}
			// case content
			try {
				lineItem.getProduct().setCaseContent( ServletRequestUtils.getIntParameter( request, "__caseContent_" + lineItem.getLineNumber(), lineItem.getProduct().getCaseContent() ) );
			} catch(Exception e) {
				// do nothing
			}
			
			try {
				lineItem.setLocation( ServletRequestUtils.getStringParameter( request, "__location_" + lineItem.getLineNumber(), "" ) );

			} catch(Exception e) {
				// do nothing
			}
			
			// get discount			
			try {				
				if (request.getParameter("__discount_" + lineItem.getLineNumber()) != null && !request.getParameter("__discount_" + lineItem.getLineNumber()).isEmpty() && Utilities.roundFactory(ServletRequestUtils.getDoubleParameter( request, "__discount_" + lineItem.getLineNumber()), 2, BigDecimal.ROUND_HALF_UP) != 0.0) {
					lineItem.setDiscount(Utilities.roundFactory(ServletRequestUtils.getDoubleParameter( request, "__discount_" + lineItem.getLineNumber()), 2, BigDecimal.ROUND_HALF_UP));		
				} else {
					lineItem.setDiscount(0.0);		
				}
			} catch(Exception e) {
				// do nothing
			}
			// percent
			lineItem.setPercent( ServletRequestUtils.getBooleanParameter( request, "__percent_" + lineItem.getLineNumber()) );
			
			lineItem.setIncludeRetailDisplay(false);
			
			// taxable
			lineItem.getProduct().setTaxable( ServletRequestUtils.getBooleanParameter( request, "__taxable_" + lineItem.getLineNumber(), false ) );
			
			if ( (Boolean) gSiteConfig.get( "gSPECIAL_PRICING" ) ) {
				lineItem.setSetSpecialPricing( ServletRequestUtils.getBooleanParameter( request, "__special_pricing_" + lineItem.getLineNumber(), false ) );
			}	
			if ( (Boolean) gSiteConfig.get( "gINVENTORY" ) ) {
				lineItem.setLowInventoryMessage( ServletRequestUtils.getStringParameter( request, "__low_inventory_message_" + lineItem.getLineNumber(), "" ) );
			}	
			// get title
			if (request.getParameter("__name_" + lineItem.getLineNumber()) != null) {
				lineItem.getProduct().setName( ServletRequestUtils.getStringParameter( request, "__name_" + lineItem.getLineNumber(), "" ) );
			}
			lineItem.setUnitPrice( lineItem.getDiscountedUnitPrice() );
			// update sub total			
			if (lineItem.getOriginalPrice() != null) { 
				subTotal = lineItem.getTotalPrice() + subTotal;
			}
			
			// get S/N
			String serialNum[] = ServletRequestUtils.getStringParameters(request, "__serialNum_" + lineItem.getLineNumber());
			if (serialNum != null) {
				lineItem.setSerialNums(null);
				for (int i=0; i<serialNum.length; i++) {
					String sNum = serialNum[i].trim();
					if (sNum.length() > 0) {
						lineItem.addSerialNum(sNum);
						if (serialNumsMap.containsKey(sNum.toLowerCase())) {
							serialNumsMap.put(sNum.toLowerCase(), "duplicate");
							hasDuplicateSNs = true;
						} else {
							serialNumsMap.put(sNum.toLowerCase(), null);
						}
					}
				}
			}			
			int totalNewOptions = ServletRequestUtils.getIntParameter(request, "index_"+lineItem.getLineNumber(), 1) - 1;
			List<ProductAttribute> newProductAttributeList = new ArrayList<ProductAttribute>(); 
			for (int i=1; i <= totalNewOptions; i++) {
				ProductAttribute productAttri = new ProductAttribute();	
				String optionName = ServletRequestUtils.getStringParameter(request, "__optionName_" +i+"_"+lineItem.getLineNumber(), "");
				String optionValueName = ServletRequestUtils.getStringParameter(request, "__optionValueString_" +i+"_"+lineItem.getLineNumber(), "");
				
				productAttri.setOptionName(optionName);
				productAttri.setValueName(optionValueName);
				productAttri.setOptionIndex(i);
				if((productAttri.getOptionName() != null && productAttri.getOptionName() != "") || (productAttri.getValueName() != null && productAttri.getValueName() != "")) {
					newProductAttributeList.add(productAttri);
				}
			}
			if(!newProductAttributeList.isEmpty()) {

				lineItem.setNewProductAttributes(newProductAttributeList);
			}

			// product attributes
			for (ProductAttribute productAttri : lineItem.getProductAttributes()) {
				if (request.getParameter("__optionName_update_" + productAttri.getId()) != null || request.getParameter("__optionValueString_update_" + productAttri.getId()) != null) {
					productAttri.setOptionName(ServletRequestUtils.getStringParameter(request, "__optionName_update_" +productAttri.getId(), ""));
					productAttri.setValueName(ServletRequestUtils.getStringParameter(request, "__optionValueString_update_" + productAttri.getId(), ""));
					if(productAttri.getOptionName() == "" && productAttri.getValueName() == "") {
						//lineItem.getProductAttributes().remove(productAttri);
						productAttri.setOptionName(null);
						productAttri.setValueName(null);
					}
				}
			} 
		}
		
		// update lineItem start calculation for viaDeal start ====
		
		if (invoiceForm.getOrder().getLineItems() != null) {		
	//	    get the map first
			Map<String, Integer> parentSkuTotalQtyMap = new HashMap<String, Integer>();
			Map<String, Double> parentSkuTotalAmtMap = new HashMap<String, Double>();
		    
		    List<LineItem> lineItems = invoiceForm.getOrder().getLineItems();
		    
//		    if (invoiceForm.getOrder().getInsertedLineItems() != null && !invoiceForm.getOrder().getInsertedLineItems().isEmpty()) {
//		    	for (LineItem lineItemInserted : invoiceForm.getOrder().getInsertedLineItems() ) {
//		    		lineItems.add(lineItemInserted);
//		    	}
//		    }
		    
		    for (LineItem lineItem : lineItems) {
				Integer productId = lineItem.getProduct().getId();
				Product product = this.webJaguar.getProductById(productId, 0, true, null);
				String parentSku = product.getMasterSku();		
				if (parentSku != null && !parentSku.isEmpty() && parentSkuTotalQtyMap.containsKey(parentSku)) {
					parentSkuTotalQtyMap.replace(parentSku, parentSkuTotalQtyMap.get(parentSku) + lineItem.getQuantity());
					if (product.getPrice1() != null && parentSkuTotalAmtMap.get(parentSku) != null) {
						parentSkuTotalAmtMap.replace(parentSku, parentSkuTotalAmtMap.get(parentSku) + product.getPrice1() * lineItem.getQuantity());
					}
				} else {
					parentSkuTotalQtyMap.put(parentSku,  lineItem.getQuantity());
					if ( product.getPrice1() != null) {
						parentSkuTotalAmtMap.put(parentSku, product.getPrice1() * lineItem.getQuantity());
					}
				}	
		    }
		    
	//	    check viaDeal module
		    for (LineItem lineItem : lineItems) {
				Integer productId = lineItem.getProduct().getId();
				Product product = this.webJaguar.getProductById(productId, 0, true, null);
				String parentSku = product.getMasterSku();
							
				ViaDeal viaDeal = this.webJaguar.getViaDealBySku(parentSku);
				if (viaDeal != null && viaDeal.isTriggerType()) {
	// 				triggerType is add up quantity
					if (parentSku != null && !parentSku.isEmpty() && parentSkuTotalQtyMap.containsKey(parentSku) && parentSkuTotalQtyMap.get(parentSku) >= viaDeal.getTriggerThreshold() ) {
						if (viaDeal.isDiscountType()) {
							//discount type is percent
							Double discount =  viaDeal.getDiscount();
							lineItem.setDiscount(discount);				
							lineItem.setPercent(true);
						} else {
	//						discount type is fixed amount
							Double discount =  viaDeal.getDiscount();
							lineItem.setDiscount(discount);
							lineItem.setPercent(false);
						}	
					}
				} else if (viaDeal != null && !viaDeal.isTriggerType()) {
	// 				triggerType is add up price
					if (parentSku != null && !parentSku.isEmpty() && parentSkuTotalQtyMap.containsKey(parentSku) && parentSkuTotalAmtMap.get(parentSku) >= viaDeal.getTriggerThreshold() ) {				
						if (viaDeal.isDiscountType()) {
	//						discount type is percent
							Double discount =  viaDeal.getDiscount();
							lineItem.setDiscount(discount);	
							lineItem.setPercent(true);
						} else {
	//					    discount type is fixed amount
							Double discount =  viaDeal.getDiscount();
							lineItem.setDiscount(discount);			
							lineItem.setPercent(false);
						}	
					}
				}				
		    }	
		}
		
		// update lineItem start calculation for viaDeal end ====
		
		// inserted line items
		if (invoiceForm.getOrder().getInsertedLineItems() != null) {
			Iterator<LineItem> iterInserted = invoiceForm.getOrder().getInsertedLineItems().iterator();
			int index = 0;
			while (iterInserted.hasNext()) {

				LineItem lineItem = iterInserted.next();
				// check if removed
				if (ServletRequestUtils.getBooleanParameter( request, "__remove_new_" + index, false )) {
					iterInserted.remove();
				} else {

					// get quantity
					lineItem.setQuantity( ServletRequestUtils.getIntParameter( request, "__quantity_new_" + index, lineItem.getQuantity() ) );
					if ((Boolean) gSiteConfig.get("gINVENTORY") && invoiceForm.getOrder().getOrderId() != null) {
						lineItem.setUpdateOnHandQty(-lineItem.getQuantity());
						//this.webJaguar.updateOnHand(lineItem.getProduct().getSku(), -lineItem.getQuantity());
					}
					// get original price 
					try {
						lineItem.setOriginalPrice(Utilities.roundFactory(ServletRequestUtils.getDoubleParameter( request, "__originalPrice_new_" + index), 3, BigDecimal.ROUND_HALF_UP));
					} catch(Exception e) {
						// do nothing
					}
					// case content
					try {
						lineItem.getProduct().setCaseContent( ServletRequestUtils.getIntParameter( request, "__caseContent_new_" + lineItem.getLineNumber(), lineItem.getProduct().getCaseContent() ) );
					} catch(Exception e) {
						// do nothing
					}
					
					try {
						lineItem.setLocation( ServletRequestUtils.getStringParameter( request, "__location_new" + index, "" ) );
					} catch(Exception e) {
						// do nothing
					}
					
					lineItem.setIncludeRetailDisplay(false);

					// get discount 
					try {
						lineItem.setDiscount(Utilities.roundFactory(ServletRequestUtils.getDoubleParameter(request, "__discount_new_" + index), 2, BigDecimal.ROUND_HALF_UP));			
					} catch(Exception e) {
						// do nothing
					}
					// percent
					lineItem.setPercent( ServletRequestUtils.getBooleanParameter( request, "__percent_new_" + index, false) );
					
					// taxable
					lineItem.getProduct().setTaxable( ServletRequestUtils.getBooleanParameter( request, "__taxable_new_" + index, false ) );
					
					if ( (Boolean) gSiteConfig.get( "gSPECIAL_PRICING" ) ) {
						lineItem.setSetSpecialPricing( ServletRequestUtils.getBooleanParameter( request, "__special_pricing_new_" + index, false ) );
					}		
					if ( (Boolean) gSiteConfig.get( "gINVENTORY" ) ) {
						lineItem.setLowInventoryMessage( ServletRequestUtils.getStringParameter( request, "__low_inventory_message_new_" + index, "" ) );
					}
					// get title
					if (request.getParameter("__name_new_" + index) != null) {
						lineItem.getProduct().setName( request.getParameter("__name_new_" + index) );
					}
					lineItem.setUnitPrice( lineItem.getDiscountedUnitPrice() );
					// update sub total
					if (lineItem.getOriginalPrice() != null) subTotal = lineItem.getTotalPrice() + subTotal;
					
					// get S/N
					String serialNum[] = ServletRequestUtils.getStringParameters(request, "__serialNum_new_" + index);
					if (serialNum != null) {
						lineItem.setSerialNums(null);
						for (int i=0; i<serialNum.length; i++) {
							String sNum = serialNum[i].trim();
							if (sNum.length() > 0) {
								lineItem.addSerialNum(sNum);
								if (serialNumsMap.containsKey(sNum.toLowerCase())) {
									serialNumsMap.put(sNum.toLowerCase(), "duplicate");
									hasDuplicateSNs = true;
								} else {
									serialNumsMap.put(sNum.toLowerCase(), null);
								}
							}
						}
					}
					int totalNewOptions = ServletRequestUtils.getIntParameter(request, "index_new_"+index, 1) - 1;
					List<ProductAttribute> newProductAttributeList = new ArrayList<ProductAttribute>(); 
					for (int i=1; i <= totalNewOptions; i++) {
						ProductAttribute productAttri = new ProductAttribute();	
						String optionName = ServletRequestUtils.getStringParameter(request, "__optionName_new_" +i+"_"+index, "");

						String optionValueName = ServletRequestUtils.getStringParameter(request, "__optionValueString_new_" +i+"_"+index, "");

						if(optionName != "" && optionName != null) {
							productAttri.setOptionName(ServletRequestUtils.getStringParameter(request, "__optionName_new_" +i+"_"+index, ""));
						}
						if(optionValueName != "" && optionValueName != null) {
							productAttri.setValueName(ServletRequestUtils.getStringParameter(request, "__optionValueString_new_" +i+"_"+index, ""));
						}
						productAttri.setOptionIndex(i);				
						if((productAttri.getOptionName() != null && productAttri.getOptionName() != "") || (productAttri.getValueName() != null && productAttri.getValueName() != "")) {
							newProductAttributeList.add(productAttri);
						}
					}
					if(!newProductAttributeList.isEmpty()) {
						lineItem.setProductAttributes(newProductAttributeList);
					}
					
					
					
				}
				index++;
			}	
		}
		request.setAttribute("serialNumsMap", serialNumsMap);
		if (hasDuplicateSNs) {
			errors.reject("form.hasDuplicateSerialNums");
		}
		
		// commission level 1
		try {
			invoiceForm.getOrder().getAffiliate().setTotalCommissionLevel1(Utilities.roundFactory(ServletRequestUtils.getDoubleParameter( request, "__commissionLevel1"), 2, BigDecimal.ROUND_HALF_UP));
		} catch(Exception e) {}
		// commission level 2
		try {
			invoiceForm.getOrder().getAffiliate().setTotalCommissionLevel2(Utilities.roundFactory(ServletRequestUtils.getDoubleParameter( request, "__commissionLevel2"), 2, BigDecimal.ROUND_HALF_UP));
		} catch(Exception e) {}
		
		return subTotal;
	}
	
	protected boolean suppressBinding(HttpServletRequest request)
	{
		if ( request.getParameter( "_cancel" ) != null )
		{
			return true;
		}
		return false;
	}

	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
		InvoiceForm invoiceForm = (InvoiceForm) command;
		Map<String, Object> map = new HashMap<String, Object>();
		Customer customer = this.webJaguar.getCustomerById( invoiceForm.getOrder().getUserId() );
		map.put("salesRep", this.webJaguar.getSalesRepById( invoiceForm.getOrder().getSalesRepId() ));
		SalesRepSearch salesRepSearch= new SalesRepSearch();
		salesRepSearch.setShowActiveSalesRep(true);
		map.put("salesRepList", this.webJaguar.getSalesRepList(salesRepSearch));
		salesRepSearch= new SalesRepSearch();
		salesRepSearch.setShowNonActiveSalesRep(true);
		map.put("salesRepListInactive", this.webJaguar.getSalesRepList(salesRepSearch));
		map.put("countries",this.webJaguar.getEnabledCountryList());
		map.put("states",this.webJaguar.getStateList("US"));
		map.put("caProvinceList", this.webJaguar.getStateList("CA"));
		map.put("customPayments", this.webJaguar.getCustomPaymentMethodList(null));
		PromoSearch promoSearch = new PromoSearch("rank DESC");
		promoSearch.setActive(true);
		map.put("promoList", this.webJaguar.getPromoList(promoSearch));
		map.put("customShippingRateList", this.webJaguar.getCustomShippingRateList( false, true ));
		map.put("addressList", this.webJaguar.getAddressListByUserid( invoiceForm.getOrder().getUserId() ));
		
		UpsOrder upsWorldShip = null;
		if(siteConfig.get("WORLD_SHIP_IMPORT_ORDERS").getValue().equals("true")) {
			if (invoiceForm.getOrder().getOrderId() != null) {
				if (invoiceForm.getOrder().getPurchaseOrder() != null && !invoiceForm.getOrder().getPurchaseOrder().isEmpty()) {
					List<UpsOrder> upsOrderList = this.webJaguar.getOrderLookupReport(invoiceForm.getOrder().getPurchaseOrder());	
					for(UpsOrder order:upsOrderList ) {
						upsWorldShip = 	order;
					}	
					map.put("upsWorldShip", upsWorldShip);
				}
			}			
		}
		
		if(((String) gSiteConfig.get("gI18N")).length()>0){
			map.put("languageCodes", this.webJaguar.getLanguageSettings());
		}
		if (invoiceForm.getOrder().getWorkOrderNum() != null) {
			map.put("workOrder", this.webJaguar.getWorkOrder(invoiceForm.getOrder().getWorkOrderNum()));
		}
		if(customer.getAddress().getPhone()!=null){
			map.put("phoneNumber", customer.getAddress().getPhone());
		}
		if(customer.getNote()!=null){
			Utilities utility= new Utilities();
			String customerNote=utility.limitString(customer.getNote(), 0, 100, "...");
			map.put("customerNote",customerNote);
		}
		
		List<CustomerField> customerFields = new ArrayList<CustomerField>();
		// customer fields
		Class<Customer> c1 = Customer.class;
		Method m1 = null;
		Object arglist1[] = null;
		if ( customer != null ) {
			for ( CustomerField customerField : this.webJaguar.getCustomerFields() )
			{
				if ( customerField.isEnabled() && customerField.isShowOnInvoice() )
				{
					m1 = c1.getMethod( "getField" + customerField.getId() );
					customerField.setValue( (String) m1.invoke( customer, arglist1 ) );
					if ( customerField.getValue() != null && customerField.getValue().length() > 0 ) {
						customerFields.add( customerField );
					}
				}
			}
		}
		if(invoiceForm.getOrder().getPaymentMethod()!= null && invoiceForm.getOrder().getPaymentMethod().equalsIgnoreCase("vba")) {
			customer.setCredit(customer.getCredit() - invoiceForm.getOrder().getGrandTotal());
		}
		if(invoiceForm.isApplyUserPoint()) {
			map.put("creditMessage", "creditMessage");
		}
		
		//BUDGET
		// If creditAllowed is empty get value from global
		if((Boolean) gSiteConfig.get("gBUDGET") && customer.getCreditAllowed() ==  null && siteConfig.get("BUDGET_PERCENTAGE").getValue() != null && !(siteConfig.get("BUDGET_PERCENTAGE").getValue().isEmpty())) {	
			customer.setCreditAllowed(Double.parseDouble(siteConfig.get("BUDGET_PERCENTAGE").getValue()));
		}
		// Budget Earned Credits
		if(invoiceForm.getOrder().getOrderId() != null &&  (Boolean) gSiteConfig.get("gBUDGET") && siteConfig.get("BUDGET_ADD_PARTNER").getValue().equalsIgnoreCase("true")) {
			CustomerBudgetPartner partner = new CustomerBudgetPartner();
			partner.setOrderId(invoiceForm.getOrder().getOrderId());
			partner.setUserId(customer.getId());
			List<CustomerBudgetPartner> partnerList = this.webJaguar.getCustomerPartnerHistory(partner);
			if(partnerList != null && !partnerList.isEmpty()) {
				invoiceForm.setPartnersList(partnerList);
				map.put("partnerList", partnerList);
			}
			Double value = null;
			if(invoiceForm.getOrder().getBudgetEarnedCredits() != null && customer.getBudgetPlan() != null && customer.getBudgetPlan().equalsIgnoreCase("plan-b")){				
				value= invoiceForm.getOrder().getSubTotal() - invoiceForm.getOrder().getBudgetEarnedCredits();
			} else {
				value= invoiceForm.getOrder().getSubTotal();
			}
			map.put("subTotalByPlan", value);
		}		
		map.put("customerFields", customerFields);
		map.put("customer", customer);
		map.put("customerOrderInfo", this.webJaguar.getCustomerOrderReportByUserID(customer.getId()));
		map.put("productFieldsHeader", this.webJaguar.getProductFieldsHeader( invoiceForm.getOrder(), (Integer) gSiteConfig.get("gPRODUCT_FIELDS") )); 		
		
	    // payments
	    if ((Boolean) gSiteConfig.get("gPAYMENTS")) {	    	
	    	// get open invoices
		    PaymentSearch search = new PaymentSearch();
		    search.setUserId(customer.getId());
		    Map <Integer, Order> openInvoices = this.webJaguar.getInvoices(search, "open");
		    Map<Integer, Order> sortedOpenInvoices = new TreeMap<Integer, Order>();
		    for (Integer orderId: openInvoices.keySet()) {
		    	sortedOpenInvoices.put(orderId, openInvoices.get(orderId));
	    	}
		    if (sortedOpenInvoices.size() > 0) {
		    	request.setAttribute("openInvoices", sortedOpenInvoices.values());
		    }
	    }
		// cc expiraton years
	    map.put("expireYears", Constants.getYears(10));
	    
	    //Quote View
	    if(request.getParameter("orderName")!= null && request.getParameter("orderName").equals("quotes")) {
			request.setAttribute("orderName", "quotes");	
		}
	    
	    // check for the credit alert
	    if((Boolean) gSiteConfig.get("gPAYMENTS") && ((customer.getPaymentAlert() != null && customer.getPaymentAlert() > 0) || invoiceForm.getOrder().isPaymentAlert() )){
			SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		    // by default we go back 1 year to calculate Payment Due
			Calendar now = Calendar.getInstance();  
		    now.add(Calendar.YEAR, -1);
			Date dateOrdered = now.getTime();
			if(!siteConfig.get("PAYMENT_ALERT_START_DATE").getValue().isEmpty()) {
				try {
					dateOrdered = df.parse(siteConfig.get("PAYMENT_ALERT_START_DATE").getValue());
				} catch (Exception e) {
					e.printStackTrace();
				}
			} 
			
			Double balance = this.webJaguar.getCustomerOrderBalance(customer.getId(),dateOrdered);
			if(balance != null && balance > customer.getPaymentAlert()){
				map.put("message", "paymentAlert");
				invoiceForm.getOrder().setPaymentAlert(true);
			}
	    }
		return map;
    }
	
	// multiple browser tabs/windows session issue
	// http://forum.springframework.org/showthread.php?t=42241
	protected String getFormSessionAttributeName(HttpServletRequest request) {
		String orderId = request.getParameter("orderId");
		return super.getFormSessionAttributeName(request) + orderId;	    	  
	}
	
	private LineItem setLineItem(LineItem lineItem, Product product, int skuIndex, HttpServletRequest request, InvoiceForm invoiceForm){
		lineItem.setProduct(product);
		lineItem.setProduct(product);
		lineItem.setProductId( product.getId() );
		lineItem.setQuantity(ServletRequestUtils.getIntParameter( request, "__addProduct" + skuIndex + "_qty", 1 ));
		lineItem.setUnitPrice(this.webJaguar.getUnitPrice(gSiteConfig, siteConfig, invoiceForm.getOrder().getUserId(), product.getSku(), lineItem.getQuantity()));
		lineItem.setOriginalPrice( lineItem.getUnitPrice() );
		
		// Consignment
		if ((Boolean) gSiteConfig.get("gCUSTOMER_SUPPLIER")) {
			// primary supplier is not done yet.
			Supplier supplier = this.webJaguar.getProductSupplierPrimaryBySku(lineItem.getProduct().getSku());
			if (supplier != null) {
				lineItem.setSupplier(supplier);
				lineItem.setCost(supplier.getPrice());
				lineItem.setCostPercent(supplier.isPercent());
				lineItem.getProduct().setDefaultSupplierId(supplier.getId());
			}
		}
		
		return lineItem;
	}
}
