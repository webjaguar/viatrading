package com.webjaguar.web.admin.orders;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerCreditHistory;
import com.webjaguar.model.UserSession;
import org.springframework.web.servlet.view.RedirectView;

public class CustomerAjaxCreditController implements Controller{
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private String type;
	public void setType(String type) { this.type = type; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		if(request.getParameter("userId") != null && (request.getParameter("addCredit") != null || request.getParameter("deductCredit") != null)) {			

			CustomerCreditHistory customerCredit = new CustomerCreditHistory();	    	
			UserSession userSession = this.webJaguar.getUserSession(request);			
			if (type == null) {
				
				customerCredit.setDate(new Date());
				customerCredit.setUserId(Integer.parseInt(request.getParameter("userId")));				
				customerCredit.setUserName(this.webJaguar.getAccessUser(request).getUsername());			
				customerCredit.setTransactionId(UUID.randomUUID().toString().toUpperCase().replace( "-", "" ).substring( 0, 9 ));				
				if(request.getParameter("addCredit") != null) {	
					customerCredit.setCredit(Double.valueOf(request.getParameter("addCredit")));
					customerCredit.setReference("payment");			
				} else if(request.getParameter("deductCredit") != null) {
					customerCredit.setCredit((-1) * Double.valueOf(request.getParameter("deductCredit")));
					customerCredit.setReference("cancel");
				}				
				this.webJaguar.updateCredit(customerCredit);
				
			} else if(type != null && type.equalsIgnoreCase("frontend") ){ 
				
				Customer manager = this.webJaguar.getCustomerById(userSession.getUserid());
				
				if(request.getParameter("addCredit") != null && manager.getCredit()!= null && manager.getCredit() >= Double.parseDouble(request.getParameter("addCredit"))) {	
					
					//adding sub-account credit
					customerCredit.setDate(new Date());
					customerCredit.setUserId(Integer.parseInt(request.getParameter("userId")));
					customerCredit.setUserName(manager.getUsername());				
					customerCredit.setTransactionId(UUID.randomUUID().toString().toUpperCase().replace( "-", "" ).substring( 0, 9 ));					
					customerCredit.setCredit(Double.valueOf(request.getParameter("addCredit")));
					customerCredit.setReference("payment");				
					this.webJaguar.updateCredit(customerCredit);
					
					//deducting manager's credit
					customerCredit.setUserId(manager.getId());			
					customerCredit.setTransactionId(UUID.randomUUID().toString().toUpperCase().replace( "-", "" ).substring( 0, 9 ));					
					customerCredit.setCredit((-1) * Double.parseDouble(request.getParameter("addCredit")));
					customerCredit.setReference("deduct");					
					this.webJaguar.updateCredit(customerCredit);
				}
			}
		}
		return null;  
	}
}
	
