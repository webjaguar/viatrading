/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.22.2006
 */

package com.webjaguar.web.admin.orders;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import javax.activation.DataHandler;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import merchantAtAPIs.remote.DocumentProcessingInfo;
import org.apache.axis.attachments.AttachmentPart;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;
import org.springframework.web.servlet.support.RequestContextUtils;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.dao.ImportInvoiceDao;
import com.webjaguar.dao.ProductDao;
import com.webjaguar.dao.StagingDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CreditCard;
import com.webjaguar.model.Customer;
import com.webjaguar.model.InvoiceImport;
import com.webjaguar.model.InvoiceImportLineItem;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Payment;
import com.webjaguar.model.PaymentImport;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductCategory;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.Supplier;

public class ProductImportController extends AbstractCommandController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	public ProductImportController() {
		setCommandClass(OrderStatus.class);
	}

    public ModelAndView handle(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("admin/orders/invoice", model);
	}
    
    private boolean autoProductImport(){
    	
		

    	List<ProductCategory> updateProductFieldsList = this.webJaguar.getUpdateProductFields();
		

    	for(ProductCategory productCategory : updateProductFieldsList){
    		try{
    			this.webJaguar.updateProductFields(productCategory);
    		}catch(Exception ex){
    		}
    	}
		

    	this.webJaguar.deleteAllUpdateProductFields();
		

    	
    	
    	
    	List<ProductCategory> addProductCategoryList = this.webJaguar.getAddProductCategory();
		

    	for(ProductCategory productCategory : addProductCategoryList){
    		try{
    			this.webJaguar.addProductCategory(productCategory);
    		}catch(Exception ex){
    		}
    	}
		

    	this.webJaguar.deleteAllAddProductCategory();
		

    	
    	List<ProductCategory> removeProductCategoryList = this.webJaguar.getRemoveProductCategory();
		

    	for(ProductCategory productCategory : removeProductCategoryList){
    		try{
    			this.webJaguar.removeProductCategory(productCategory);
    		}catch(Exception ex){
    		}
    	}
		

    	this.webJaguar.deleteAllRemoveProductCategory();
		

		return false;
	}
 
}
