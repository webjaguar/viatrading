/* Copyright 2006 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.30.2009
 */

package com.webjaguar.web.admin.orders;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class DisplayShipDateController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
    	
		Map<String, Object> model = new HashMap<String, Object>();
		Integer orderId = ServletRequestUtils.getIntParameter( request, "orderId", -1 );
		if (orderId != -1) {
			Date shipDate = this.webJaguar.getShipDateByOrderId( orderId );
			model.put("shipDate", shipDate);
		}
        return new ModelAndView("admin/orders/shipDate", "model", model);
	}
}
