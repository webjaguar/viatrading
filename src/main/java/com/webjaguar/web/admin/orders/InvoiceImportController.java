/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.22.2006
 */

package com.webjaguar.web.admin.orders;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import javax.activation.DataHandler;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import merchantAtAPIs.remote.DocumentProcessingInfo;
import org.apache.axis.attachments.AttachmentPart;
import org.apache.commons.codec.binary.Base64;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.itextpdf.text.log.SysoCounter;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.dao.ImportInvoiceDao;
import com.webjaguar.dao.ProductDao;
import com.webjaguar.dao.StagingDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CreditCard;
import com.webjaguar.model.Customer;
import com.webjaguar.model.InvoiceImport;
import com.webjaguar.model.InvoiceImportLineItem;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.PackingList;
import com.webjaguar.model.Payment;
import com.webjaguar.model.PaymentImport;
import com.webjaguar.model.Product;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.Supplier;
import com.webjaguar.web.form.EmailMessageForm;
import com.webjaguar.web.form.PackingListForm;

@Controller
public class InvoiceImportController extends WebApplicationObjectSupport {

	@Autowired
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	@Autowired
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	@Autowired
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
    @Autowired
    private ImportInvoiceDao invoiceImportDao;
	public void setInvoiceImportDao(ImportInvoiceDao importInvoiceDao) { this.invoiceImportDao = importInvoiceDao; }
	@Autowired
	private ProductDao productDao;
	public void setProductDao(ProductDao productDao) { this.productDao = productDao; }
	@Autowired
    private StagingDao stagingDao;
	public void setStagingDao(StagingDao stagingDao) { this.stagingDao = stagingDao; }
	
	@RequestMapping(value = "/admin/orders/copyInvoiceFromStagingToProd.jhtm")
    public @ResponseBody Boolean autoInvoiceImport(){
		System.out.println("Invoice import trigged on manually");
		InetAddress ip;
        String hostname;
        try {
            ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
	        Date date = new Date();
	        Timestamp timeStamp = new Timestamp(date.getTime());
	        
            System.out.println("Invoice import trigged at: " +  timeStamp);
            System.out.println("Your current IP address : " + ip);
            System.out.println("Your current Hostname : " + hostname);
            
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    	
    	List<InvoiceImport> ordersList = this.invoiceImportDao.getAllOrders();
		List<InvoiceImportLineItem> orderLineItemList = null;
		List<InvoiceImportLineItem> tempList = new LinkedList<InvoiceImportLineItem>();

		Map<String, Object> gSiteConfig = globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		OrderStatus orderStatus = new OrderStatus();
		

		for(InvoiceImport order : ordersList){

			Order temp = copyOrderToInvoice(order);
			
			Locale locale = new Locale("en");
			SimpleDateFormat df = new SimpleDateFormat( "MM-dd-yyyy kk:mm:ss", locale);
			df.setTimeZone(TimeZone.getTimeZone("GMT"));	
			try {
				Date date = df.parse(temp.getDateOrdered().toString());
				order.setDateOrdered(new Timestamp(date.getTime()));
				if (temp.getShipped() != null) {
					Date shippedDate =temp.getShipped();
					temp.setShipped(shippedDate);
				}				
			} catch (ParseException e1) {
				e1.printStackTrace();
			} 
			
			orderLineItemList = this.invoiceImportDao.getLineItems(temp.getOrderId());			
			tempList = new LinkedList<InvoiceImportLineItem>();
			
			orderStatus.setSubject(this.webJaguar.getSiteMessageById(2).getSubject());
			orderStatus.setMessage(this.webJaguar.getSiteMessageById(2).getMessage());
			orderStatus.setStatus(order.getStatus());
			
			for(InvoiceImportLineItem invoiceImportLineItem : orderLineItemList){
				Product pro = this.productDao.getProductById2(invoiceImportLineItem.getProductId(), 0, "0", "eng", false);
				if(pro!=null){
					invoiceImportLineItem.setProduct(pro);
					tempList.add(invoiceImportLineItem);
				}
			}
				
			List<PaymentImport> paymentList = this.invoiceImportDao.getOrderPayments(temp.getOrderId());
			
			temp.setLineItems(copyOrderToInvoiceLineItems(tempList));
			boolean error = false;
			try{
				this.webJaguar.insertOrder(temp, orderStatus, false, false, 1, siteConfig, gSiteConfig);	
				if (order.getShipped() != null ) {
					
					PackingList pl =new PackingList();
					pl.setOrderId(temp.getOrderId());
					pl.setShipped(temp.getShipped());		
					pl.setPackingListNumber(temp.getOrderId() + "-" + this.webJaguar.packingListCount(temp.getOrderId()));
					
					List<LineItem> tmp = new LinkedList<LineItem>();
					for(LineItem item : temp.getLineItems()){
							item.setPackingListNumber(pl.getPackingListNumber());
							tmp.add(item);
					}
					pl.setLineItems(tmp);			
										
					boolean inventoryHistory = false;
					if(siteConfig.get("INVENTORY_HISTORY").getValue().equals("true")) {
						inventoryHistory = true;
					}
					this.webJaguar.insertPackingList(pl, null, inventoryHistory, temp);										
				}							
			}catch(Exception ex){				
				error = true;
			}finally{
				if(error){					
					this.invoiceImportDao.deleteOrder(temp.getOrderId());
					this.invoiceImportDao.deleteOrderLineItems(temp.getOrderId());
					this.invoiceImportDao.deletePayments(temp.getOrderId());
				}
			}
			
			if(!error){
			
			List<Order> orders = new ArrayList<Order>();
			orders.add(temp);
			
			Customer customer = this.webJaguar.getCustomerById(order.getUserId());
			MultiStore multiStore = null;
			notifyCustomer(orderStatus, temp, multiStore, customer);
			
			for(PaymentImport paymentImport : paymentList){
				this.webJaguar.insertOrderPayment(copyPayment(paymentImport), orders);
			}
			
			SiteMessage supplierSiteMessage = null;
			
			try {
				supplierSiteMessage = this.webJaguar.getSiteMessageById(Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_SUPPLIER_ORDERS").getValue()));
			} catch (NumberFormatException e) { }
			
			if (supplierSiteMessage != null) {
				Set<String> supplierEmails = new HashSet();
					for (LineItem lineItem : temp.getLineItems()) {
						if(lineItem.isConsignmentProduct()) {
							OrderStatus tempOrderStatus = new OrderStatus(order.getOrderId(), order.getStatus(), supplierSiteMessage.getSubject(), supplierSiteMessage.getMessage() );
							tempOrderStatus.setHtmlMessage(supplierSiteMessage.isHtml());
							Supplier supplier = this.webJaguar.getProductSupplierPrimaryBySku(lineItem.getProduct().getSku());
								if(supplier != null && supplier.getAddress() != null && !supplierEmails.contains( supplier.getAddress().getEmail() ) ) {
								supplierEmails.add( supplier.getAddress().getEmail() );
								notifySuppliers( tempOrderStatus, temp, supplier, siteConfig, lineItem, multiStore);
								}
							}
						}
					}
				}
			}
		
			this.invoiceImportDao.deleteAllOrders();
			this.invoiceImportDao.deleteAllOrdersLineItems();
			this.invoiceImportDao.deleteAllPayments();
		
			return true;
	}
    
    public Order copyOrderToInvoice(InvoiceImport importOrder){
    	
    	Order order = new Order();
    	
    	order.setOrderId(importOrder.getOrderId());
    	order.setUserId(importOrder.getUserId());
    	order.setBilling(importOrder.getBilling());
    	order.setShipping(importOrder.getShipping());
    	order.setOrderType(importOrder.getOrderType());
    	order.setSubTotal(order.getSubTotal());
    	order.setCreditCard(new CreditCard());
    	order.setTotalWeight(order.getTotalWeight());
    	order.setSalesRepId(importOrder.getSalesRepId());
    	order.setSalesRepProcessedById(importOrder.getSalesRepProcessedById());
    	
    	order.setTotalQuantity(importOrder.getTotalQuantity());
    	order.setTax(importOrder.getTax());
    	order.setGrandTotal(importOrder.getGrandTotal());
    	order.setSubTotal(importOrder.getSubTotal());
    	order.setShippingCarrier(importOrder.getShippingCarrier());
    	order.setShippingMethod(importOrder.getShippingMethod());
    	order.setShippingMessage(importOrder.getShippingMessage());
    	order.setCustomShippingId(importOrder.getCustomShippingId());
    	order.setShippingCost(importOrder.getShippingCost());
    	order.setDateOrdered(importOrder.getDateOrdered());
    	order.setDueDate(importOrder.getDueDate());	
    	order.setLastModified(importOrder.getLastModified());
    	order.setAmountPaid(importOrder.getAmountPaid());	
    	order.setCcFee(importOrder.getCcFee());	
    	order.setCcFeeRate(importOrder.getCcFeeRate());	
    	order.setCcFeePercent(importOrder.isCcFeePercent());
    	order.setVbaAmountToPay(importOrder.getVbaAmountToPay());
    	order.setVbaDateOfPay(importOrder.getVbaDateOfPay());
    	order.setVbaPaymentMethod(importOrder.getVbaPaymentMethod());
    	order.setVbaPaymentStatus(importOrder.getVbaPaymentStatus());
    	order.setVbaTransactionId(importOrder.getVbaTransactionId());
    	order.setCustomField1(importOrder.getCustomField1());
    	order.setCustomField2(importOrder.getCustomField2());
    	order.setCustomField3(importOrder.getCustomField3());
    	order.setStatus(importOrder.getStatus());
    	order.setPaymentMethod(importOrder.getPaymentMethod());
    	
    	order.setShipped(importOrder.getShipped());
    	
    	return order;
    }
    
    public List<LineItem> copyOrderToInvoiceLineItems(List<InvoiceImportLineItem> invoiceImportLineItemList){
    	
    	List<LineItem> lineItems = new LinkedList<LineItem>();
    	Supplier sup = null;
    	
    	for(InvoiceImportLineItem invoiceImportLineItem : invoiceImportLineItemList){
    		LineItem lineItem = new LineItem();
    		lineItem.setOrderId(invoiceImportLineItem.getOrderId());
    		lineItem.setLineNumber(invoiceImportLineItem.getLineNumber());
    		lineItem.setProductId(invoiceImportLineItem.getProductId());    		
    		Product pro = invoiceImportLineItem.getProduct();
    		if(pro!=null){
    			pro.setName(invoiceImportLineItem.getpName());
    		}
    		lineItem.setProduct(pro);
    		lineItem.setLineNumber(invoiceImportLineItem.getLineNumber());
    		lineItem.setQuantity(invoiceImportLineItem.getQuantity());
    		lineItem.setProductId(invoiceImportLineItem.getProduct().getId());
    		lineItem.setUnitPrice(invoiceImportLineItem.getUnitPrice());
    		lineItem.setOriginalPrice(invoiceImportLineItem.getUnitPrice());
    		lineItem.setIncludeRetailDisplay(invoiceImportLineItem.getIncludeRetailDisplay());
    		lineItem.setProduct(invoiceImportLineItem.getProduct());
    		lineItem.setProductAttributes(invoiceImportLineItem.getProductAttributes());
    		lineItem.setAsiProductAttributes(invoiceImportLineItem.getAsiProductAttributes());
    		lineItem.setLowInventoryMessage(invoiceImportLineItem.getLowInventoryMessage());
    		lineItem.setSubscriptionInterval(invoiceImportLineItem.getSubscriptionInterval());
    		lineItem.setCustomLines(invoiceImportLineItem.getCustomLines());
    		
    		sup = null; 
    		if(pro!=null){
    			sup = this.webJaguar.getProductSupplierPrimaryBySku(invoiceImportLineItem.getProduct().getSku()); 
    		}
    		   		
    		lineItem.setSupplier(sup);
    		
    		Supplier supplier = null;
    		if(pro!=null && sup!=null){
    			supplier = this.webJaguar.getProductSupplierBySIdSku(pro.getSku(), sup.getId());
    		}
    		
    		if(supplier!=null){
    			lineItem.setCost(supplier.getPrice());
    			lineItem.setCostPercent(supplier.isPercent());
    		}
    		lineItem.setCommissionLevel1(invoiceImportLineItem.getCommissionLevel1());
    		lineItem.setCommissionLevel2(invoiceImportLineItem.getCommissionLevel2());
    		lineItem.setCustomXml(invoiceImportLineItem.getCustomXml());
    		lineItem.setCustomImageUrl(invoiceImportLineItem.getCustomImageUrl());
    		lineItem.setDealGroup(invoiceImportLineItem.getDealGroup());
    		lineItem.setAsiAdditionalCharge(invoiceImportLineItem.getAsiAdditionalCharge());
    		lineItem.setOptionFixedCharge(invoiceImportLineItem.getOptionFixedCharge());
    		lineItem.setPriceCasePackQty(invoiceImportLineItem.getPriceCasePackQty());
    		lineItem.setAttachment(invoiceImportLineItem.getAttachment());
    		lineItem.setItemGroup(invoiceImportLineItem.getItemGroup());
    		lineItem.setItemGroupMainItem(invoiceImportLineItem.isItemGroupMainItem());
    		lineItem.setParentSkuDiscount(invoiceImportLineItem.getDiscount());
    		lineItem.setParentSkuOriginalPrice(invoiceImportLineItem.getOriginalPrice());
    		if(lineItem.getSupplier() != null && lineItem.getProduct()!=null && lineItem.getProduct().getSku()!=null) {
                if(lineItem.getSupplier().getSupplierPrefix() != null && lineItem.getProduct().getSku().toLowerCase().startsWith(lineItem.getSupplier().getSupplierPrefix().toLowerCase() + "-ln-")) {
                        lineItem.setConsignmentProduct(true);
                }
            }
    		lineItem.setVbaAmountToPay(invoiceImportLineItem.getVbaAmountToPay());
    		lineItem.setVbaDateOfPay(invoiceImportLineItem.getVbaDateOfPay());
    		lineItem.setVbaNote(invoiceImportLineItem.getVbaPaymentMethod());
    		lineItem.setVbaPaymentMethod(invoiceImportLineItem.getVbaPaymentMethod());
    		lineItem.setVbaPaymentStatus(invoiceImportLineItem.getVbaPaymentStatus());
    		lineItem.setVbaProductSku(invoiceImportLineItem.getVbaProductSku());
    		lineItem.setVbaTransactionId(invoiceImportLineItem.getVbaTransactionId());
    		lineItems.add(lineItem);
    	}
    	
    	return lineItems;
    }
    
    public Payment copyPayment(PaymentImport paymentImport){
    	
    	Payment payment = new Payment();
    	
    	payment.setAmount(paymentImport.getAmount());	
    	payment.setCancelledAmt(paymentImport.getCancelledAmt());	
    	payment.setCancelledBy(paymentImport.getCancelledBy());	
    	payment.setCustomer(paymentImport.getCustomer());	
    	payment.setDate(paymentImport.getDate());	
    	payment.setId(paymentImport.getId());	
    	payment.setMemo(paymentImport.getMemo());	
    	payment.setOrderId(paymentImport.getOrderId());	
    	payment.setPaymentMethod(paymentImport.getPaymentMethod());	
    	payment.setUserId(paymentImport.getUserId());

    	return payment;
    }
    
    private void notifyCustomer(OrderStatus orderStatus, Order order, MultiStore multiStore, Customer customer) {
		
    	Map<String, Object> gSiteConfig = globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
    	
    	SalesRep salesRep = null;
		if ((Boolean) gSiteConfig.get("gSALES_REP") && order.getSalesRepId() != null) {
			salesRep = this.webJaguar.getSalesRepById(order.getSalesRepId());
		}
		// send email
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
		Locale locale = new Locale("en"); 
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", locale);

		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append(dateFormatter.format(new Date()) + "\n\n");

		// replace dyamic elements
		try {
			this.webJaguar.replaceDynamicElement(orderStatus, customer, salesRep, order, secureUrl, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		messageBuffer.append(orderStatus.getMessage());

		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File pdfFile = new File(tempFolder, "order_" + order.getOrderId() + ".pdf");
		try {
			// construct email message
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.setBcc(contactEmail);
			if (salesRep != null)
				helper.setCc(salesRep.getEmail());
			helper.setSubject("Your order is placed");
			if (orderStatus.isHtmlMessage()) {
				helper.setText(messageBuffer.toString(), true);
			} else {
				helper.setText(messageBuffer.toString());
			}
			if ((Boolean) gSiteConfig.get("gPDF_INVOICE") && siteConfig.get("ATTACHE_PDF_ON_PLACE_ORDER").getValue().equals("true")) {
				// attach pdf invoice
				if (this.webJaguar.createPdfInvoice(pdfFile, order.getOrderId(), null, siteConfig, gSiteConfig)) {
					helper.addAttachment("order_" + order.getOrderId() + ".pdf", pdfFile);
				}
			}
			//mailSender.send(mms);
		} catch (Exception ex) {
			if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
				ex.printStackTrace();
			}
			ex.printStackTrace();
		}
		if (pdfFile.exists()) {
			pdfFile.delete();
		}
	}
    
    private void notifySuppliers(OrderStatus orderStatus, Order order, Supplier supplier, Map<String, Configuration> siteConfig, LineItem lineItem, MultiStore multiStore) {
		// send email
    	DecimalFormat df = new DecimalFormat("0.00");
    	DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		symbols.setDecimalSeparator('.');
		DecimalFormat df2 = new DecimalFormat("#,###.##", symbols);
		String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
		String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
		
		StringBuffer messageBuffer = new StringBuffer();
		// subject
		orderStatus.setSubject( orderStatus.getSubject().replace( "#firstname#", (supplier.getAddress().getFirstName() != null) ? supplier.getAddress().getFirstName() : "" ) );
		orderStatus.setSubject( orderStatus.getSubject().replace( "#lastname#", (supplier.getAddress().getLastName() != null) ? supplier.getAddress().getLastName() : "" ) );
		orderStatus.setSubject( orderStatus.getSubject().replace( "#email#", (supplier.getAddress().getEmail() != null) ? supplier.getAddress().getEmail() : "" ) );
		orderStatus.setSubject( orderStatus.getSubject().replace( "#order#", order.getOrderId().toString() ) );
		orderStatus.setSubject( orderStatus.getSubject().replace( "#status#", getMessageSourceAccessor().getMessage( "orderStatus_" + orderStatus.getStatus() ) ) );
		orderStatus.setSubject( orderStatus.getSubject().replace( "#tracknum#", (orderStatus.getTrackNum() != null) ? orderStatus.getTrackNum() : "" ) );
		//orderStatus.setSubject( orderStatus.getSubject().replace( "#orderlink#", secureUrl + "supplierInvoice.jhtm?orderId=" + orderStatus.getOrderId() ) );
		String viewInvoiceHrefInSubject = secureUrl + "supplierInvoice.jhtm?orderId=" + orderStatus.getOrderId();
		StringBuilder sb_aTagInSubject = new StringBuilder();
		sb_aTagInSubject.append("<a href=").append("\"").append(viewInvoiceHrefInSubject).append("\"").append(">").append("View Invoice").append("</a>");
		orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#orderlink#", sb_aTagInSubject.toString()));
		
		StringBuilder sb_aTagInSubject_orderlinkes = new StringBuilder();
		sb_aTagInSubject_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHrefInSubject).append("\"").append(">").append("Ver Factura").append("</a>");
		orderStatus.setSubject(orderStatus.getSubject().replace("#orderlinkes#", sb_aTagInSubject_orderlinkes.toString()));
		
		if(lineItem != null) {
			orderStatus.setSubject( orderStatus.getSubject().replace( "#sku#", (lineItem.getProduct().getSku() != null) ? lineItem.getProduct().getSku() : "" ) );
			if(lineItem.getProduct().getId()!=null){
				Product product = this.webJaguar.getProductById(lineItem.getProduct().getId(), 0, false, null);
				orderStatus.setSubject( orderStatus.getSubject().replace( "#name#", (product.getName() != null) ? product.getName()  : "" ) );
			}else{
				orderStatus.setSubject( orderStatus.getSubject().replace( "#name#",  "" ) );
			}			
			
			orderStatus.setSubject( orderStatus.getSubject().replace( "#qty#", (lineItem.getQuantity()+"")));
			orderStatus.setSubject( orderStatus.getSubject().replace( "#consignmentAmount#", (lineItem.getConsignmentValue() != null) ? df2.format(( lineItem.getConsignmentValue()))+"" : "" ) );		
			Product product = this.webJaguar.getProductById(lineItem.getProduct().getId(), 0, false, null);
			Double lotUnit = null;
			Double consignmentAmt = null;
			Double netRecoveryPerUnit = null;
			Long temp = 0L;
			if(lineItem.getConsignmentValue() != null){
				try{
					temp = (long) (lineItem.getConsignmentValue() * 100);
					consignmentAmt = (double) temp / 100; 
				}catch(Exception ex){
					consignmentAmt = 0.00;
				}
			}else{
				consignmentAmt = 0.00;
			}
			if(product.getField7()!=null){
				try{
					lotUnit = lineItem.getQuantity()* Double.valueOf(product.getField7());
				}catch(Exception ex){
					try{
						lotUnit = Double.valueOf(lineItem.getQuantity());
					}catch(Exception e){
						lotUnit = 0.0;
					}
				}
			}else{
				try{
					lotUnit = Double.valueOf(lineItem.getQuantity());
				}catch(Exception e){
					lotUnit = 0.0;
				}
			}
			netRecoveryPerUnit = consignmentAmt/lotUnit;
			try{
				orderStatus.setSubject(orderStatus.getSubject().replace("#netRecoveryPerUnit#", df2.format(netRecoveryPerUnit)));
			}catch(Exception ex){
				orderStatus.setSubject(orderStatus.getSubject().replace("#netRecoveryPerUnit#", ""));
			}
		}
		
		// message
		orderStatus.setMessage( orderStatus.getMessage().replace( "#firstname#", (supplier.getAddress().getFirstName() != null) ? supplier.getAddress().getFirstName() : "" ) );
		orderStatus.setMessage( orderStatus.getMessage().replace( "#lastname#", (supplier.getAddress().getLastName() != null) ? supplier.getAddress().getLastName() : "" ) );
		orderStatus.setMessage( orderStatus.getMessage().replace( "#email#", (supplier.getAddress().getEmail() != null) ? supplier.getAddress().getEmail() : "" ) );
		orderStatus.setMessage( orderStatus.getMessage().replace( "#order#", order.getOrderId().toString() ) );
		orderStatus.setMessage( orderStatus.getMessage().replace( "#status#", getMessageSourceAccessor().getMessage( "orderStatus_" + orderStatus.getStatus() ) ) );
		orderStatus.setMessage( orderStatus.getMessage().replace( "#tracknum#", (orderStatus.getTrackNum() != null) ? orderStatus.getTrackNum() : "" ) );
		//orderStatus.setMessage( orderStatus.getMessage().replace( "#orderlink#", secureUrl + "supplierInvoice.jhtm?orderId=" + orderStatus.getOrderId() ) );
		String viewInvoiceHrefInMessage = secureUrl + "supplierInvoice.jhtm?orderId=" + orderStatus.getOrderId();
		StringBuilder sb_aTagInMessage = new StringBuilder();
		sb_aTagInMessage.append("<a href=").append("\"").append(viewInvoiceHrefInMessage).append("\"").append(">").append("View Invoice").append("</a>");
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#orderlink#", sb_aTagInMessage.toString()));
		
		StringBuilder sb_aTagInMessage_orderlinkes = new StringBuilder();
		sb_aTagInMessage_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHrefInMessage).append("\"").append(">").append("Ver Factura").append("</a>");
		orderStatus.setMessage(orderStatus.getMessage().replace("#orderlinkes#", sb_aTagInMessage_orderlinkes.toString()));
		
		if(lineItem != null) {
			orderStatus.setMessage( orderStatus.getMessage().replace( "#sku#", (lineItem.getProduct().getSku() != null) ? lineItem.getProduct().getSku() : "" ) );
			if(lineItem.getProduct().getId()!=null){
				// Using admin getProductById function since when we send email to supplier, the product is possible inactive, which will return null.
				// We also need get the product name which comes from database instead of from orders, so that we cannot use product as null
				Product product = this.webJaguar.getProductById(lineItem.getProduct().getId(), 0, false, null);
				orderStatus.setMessage( orderStatus.getMessage().replace( "#name#", (product.getName() != null) ? product.getName()  : "" ) );
			}else{
				orderStatus.setMessage( orderStatus.getMessage().replace( "#name#",  "" ) );
			}	
			orderStatus.setMessage( orderStatus.getMessage().replace( "#qty#", (lineItem.getQuantity()+"")));
			orderStatus.setMessage( orderStatus.getMessage().replace( "#consignmentAmount#", (lineItem.getConsignmentValue() != null) ? df2.format(( lineItem.getConsignmentValue()))+"" : "" ) );
			Product product = this.webJaguar.getProductById(lineItem.getProduct().getId(), 0, false, null);
			Double lotUnit = null;
			Double consignmentAmt = null;
			Double netRecoveryPerUnit = null;
			Long temp = 0L;
			if(lineItem.getConsignmentValue() != null){
				try{
					temp = (long) (lineItem.getConsignmentValue() * 100);
					consignmentAmt = (double) temp / 100;
				}catch(Exception ex){
					consignmentAmt = 0.00;
				}
			}else{
				consignmentAmt = 0.00;
			}
			if(product.getField7()!=null){
				try{
					lotUnit = lineItem.getQuantity()* Double.valueOf(product.getField7());
				}catch(Exception ex){
					try{
						lotUnit = Double.valueOf(lineItem.getQuantity());
					}catch(Exception e){
						lotUnit = 0.0;
					}
				}
			}else{
				try{
					lotUnit = Double.valueOf(lineItem.getQuantity());
				}catch(Exception e){
					lotUnit = 0.0;
				}
			}
			netRecoveryPerUnit = consignmentAmt/lotUnit;
			try{
				orderStatus.setMessage(orderStatus.getMessage().replace("#netRecoveryPerUnit#", df2.format(netRecoveryPerUnit)));
			}catch(Exception ex){
				orderStatus.setMessage(orderStatus.getMessage().replace("#netRecoveryPerUnit#", ""));
			}
		}
		messageBuffer.append( orderStatus.getMessage() );

		try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(supplier.getAddress().getEmail());
			helper.setFrom(contactEmail);
			helper.setBcc( contactEmail );
			helper.setSubject( orderStatus.getSubject() );
			if ( orderStatus.isHtmlMessage() ) { 
	    		helper.setText( messageBuffer.toString(), true );
	    	} else {
	    		helper.setText(messageBuffer.toString());
	    	}			
			//mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing
			ex.printStackTrace();
		}    			
	}
    
    @RequestMapping(value="orders/ajaxMessagePreview.jhtm")
	public @ResponseBody String ajaxMessagePreview(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
    	String siteMessageContent = request.getParameter("message");
		if(siteMessageContent != null) {
			if(siteMessageContent.contains("src=\"/")) {
				siteMessageContent=siteMessageContent.replace("src=\"/", "src=\"?");
			}
			if(siteMessageContent.contains("href=\"/")) {
				siteMessageContent=siteMessageContent.replace("href=\"/", "href=\"?");
			}
		}
		return siteMessageContent;
	}
}
