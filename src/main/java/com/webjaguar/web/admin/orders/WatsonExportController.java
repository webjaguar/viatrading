/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.10.2007
 */

package com.webjaguar.web.admin.orders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;

import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.domain.Constants;

public class WatsonExportController extends SimpleFormController {
	
	boolean gSALES_REP;
	boolean gSALES_PROMOTIONS;
	boolean gADD_INVOICE;
	boolean gPURCHASE_ORDER;
	int gMULTI_STORE = 0;
	int gPRODUCT_FIELDS = 0;
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	private MailSender mailSender;
    public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public WatsonExportController() {
		setSessionForm(false);
		setCommandName("orderSearch");
		setCommandClass(OrderSearch.class);
		setFormView("admin/orders/watsonExport");
	}
	
	private File baseFile;
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors)  throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		OrderSearch search = getOrderSearch(request);
		
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		SimpleDateFormat df = new SimpleDateFormat(siteConfig.get("SITE_DATE_FORMAT").getValue(), RequestContextUtils.getLocale(request));

		
		//if (search.getFileType().equals( "" )) {
		//	return showForm(request, response, errors, map);
		//}
		// set end date to end of day
		if (search.getEndDate() != null) {
			search.getEndDate().setTime(search.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}

    	Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
		//if (gSALES_REP) {
			// get sales rep
		//	Iterator<SalesRep> iter = this.webJaguar.getSalesRepList().iterator();
		//	while (iter.hasNext()) {
		//		SalesRep salesRep = iter.next();
		//		salesRepMap.put(salesRep.getId(), salesRep);
		//	}
		//}
		
		List<File> exportedFiles = new ArrayList<File>();
		int orderCount = 0;
		
		if (baseFile.canWrite()) {
			File file[] = baseFile.listFiles();
			for (int f=0; f<file.length; f++) {
				if (file[f].getName().startsWith("invoiceExport") & file[f].getName().endsWith("."+search.getFileType())) {
					file[f].delete();
				}
			}
			map.put("exportedFiles", exportedFiles);
			
			orderCount = this.webJaguar.invoiceCount(search);
			search.setLimit(null);
			
			/*if (search.getFileType().equals("xls")) {
				createExcelFiles(orderCount, search, siteConfig,  gSiteConfig, exportedFiles, salesRepMap);				
			} else if (search.getFileType().equals("vaf")){
				createVafFile(orderCount, search, siteConfig, exportedFiles);
			} else if (search.getFileType().equals("thub") && siteConfig.get( "INVOICE_EXPORT" ).getValue().toString().contains( "thub" )){
				createThubFile(orderCount, search, siteConfig, exportedFiles, salesRepMap);		
			} else if (search.getFileType().equals("csv") && siteConfig.get( "INVOICE_EXPORT" ).getValue().toString().contains( "csv" )){
				createCsvFile(orderCount, search, siteConfig, gSiteConfig, exportedFiles, salesRepMap);		
			} else if (search.getFileType().equals("packnwood") && siteConfig.get( "INVOICE_EXPORT" ).getValue().toString().contains( "packnwood" )){
				createPacknwoodCsvFile(orderCount, search, siteConfig, gSiteConfig, exportedFiles, salesRepMap);		
			} else if (search.getFileType().equals("xml") && siteConfig.get( "INVOICE_EXPORT" ).getValue().toString().contains( "xml" )){
				createXMLFile(orderCount, search, siteConfig, exportedFiles, salesRepMap);		
			}*/
			
			Integer salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
			if(salesRepId != null){
				search.setSalesRepId(salesRepId);
			}
			
			System.out.println("orderCount "+orderCount);

			
	        
			if (orderCount > 0) {
				map.put("arguments", orderCount);
				map.put("message", "invoiceexport.success");
			} else {
				map.put("message", "invoiceexport.empty");				
			}
			//notifyAdmin(siteConfig, request);
			this.webJaguar.insertImportExportHistory( new ImportExportHistory("invoice", SecurityContextHolder.getContext().getAuthentication().getName(), false ));
		}
		
		createCsvFile(orderCount, search, exportedFiles, salesRepMap);	
		map.put("exportedFiles", exportedFiles);

		return showForm(request, response, errors, map);
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		OrderSearch form = getOrderSearch( request );
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
	    
		gSALES_REP = (Boolean) gSiteConfig.get("gSALES_REP");
		gSALES_PROMOTIONS = (Boolean) gSiteConfig.get("gSALES_PROMOTIONS");
		gADD_INVOICE = (Boolean) gSiteConfig.get("gADD_INVOICE");
		gPURCHASE_ORDER= (Boolean) gSiteConfig.get("gPURCHASE_ORDER");
		gMULTI_STORE = (Integer) gSiteConfig.get("gMULTI_STORE");
		gPRODUCT_FIELDS = (Integer) gSiteConfig.get("gPRODUCT_FIELDS");
		String fileType = ServletRequestUtils.getStringParameter(request, "fileType2", "xls");
		form.setFileType(fileType);
		
		baseFile = new File(getServletContext().getRealPath("/admin/excel/"));

		
		//request.setAttribute("exportedFiles", exportedFiles);
		return form;
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder( request, binder );
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, true, 10 ) );		
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		OrderSearch search = (OrderSearch) command;
		// fileType2 coming from menu
		if (request.getParameter("fileType2") != null && request.getParameter("__new") == null) {
			search.setFileType(request.getParameter("fileType2"));
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (!baseFile.canWrite()) {
			if (search.getFileType().equals("xls")) {
				map.put("message", "excelfile.notWritable");						
			} else {
				map.put("message", "file.notWritable");										
			}
		}
		
		File file[] = baseFile.listFiles();
		List<Map<String, Object>> exportedFiles = new ArrayList<Map<String, Object>>();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith("watsonInvoiceExport")) {
				HashMap<String, Object> fileMap = new HashMap<String, Object>();
				fileMap.put("file", file[f]);
				fileMap.put("lastModified", new Date(file[f].lastModified()));
				exportedFiles.add( fileMap );
			}
		}
		map.put("exportedFiles", exportedFiles);

		return map;
	}
	
    private HSSFCell getCell(HSSFSheet sheet, int row, short col) {
    	return sheet.createRow(row).createCell(col);
    }
    
    private void setText(HSSFCell cell, String text) {
    	cell.setCellValue(text);
    }
    
	private void createCsvFile(int orderCount, OrderSearch search, List<File> exportedFiles, Map<Integer, SalesRep> salesRepMap) throws Exception {
		
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy kk:mm:ss");
		
		Set<Integer> orderIds = new HashSet<Integer>();
		
		NumberFormat nf = new DecimalFormat("#0.00");
		
		baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if(!baseFile.exists()) {
			baseFile.mkdir();
		}
		File invoiceFile = new File(baseFile, "watsonInvoiceExport.csv");
		
		try {
			CSVWriter writer = new CSVWriter(new FileWriter(invoiceFile), CSVWriter.DEFAULT_SEPARATOR, CSVWriter.DEFAULT_QUOTE_CHARACTER, CSVWriter.DEFAULT_ESCAPE_CHARACTER);
			
			// headers
			List<String> line = new ArrayList<String>();
			line.add("Order Type");
			line.add("Order Date");
			line.add("Sku");
			line.add("Name");
			line.add("Quantity");
			line.add("Price");
			line.add("LineItem Total");
			line.add("Order Sub Total");
			line.add("Tax");
			line.add("Promo Code");
			line.add("Discount");
			line.add("Shipping Method");
			line.add("Shipping Cost");
			line.add("Grand Total");
			line.add("Payment Method");
			line.add("Status");
			line.add("Sales Rep");
			line.add("Shipping First Name");
			line.add("Shipping Last Name");
			line.add("Shipping Company");
			line.add("Shipping Address 1");
			line.add("Shipping Address 2");
			line.add("Shipping City");
			line.add("Shipping State/Province");
			line.add("Shipping Zip/Postal Code");
			
		
			String[] lines = new String[line.size()];
			writer.writeNext(line.toArray(lines));
			
			Map<Integer, String> customers = new HashMap<Integer, String>();
			
			int limit = 3000;
			search.setLimit(limit);

			for (int offset = 0; offset < orderCount;) {
				search.setOffset(offset);
				
				List<Order> orders = this.webJaguar.getInvoiceExportList(search, siteConfig.get("INVOICE_LINEITEMS_SORTING").getValue());
				
				for (Order order: orders) {
					order.setPromoAmount();
					order.setTotalProductWeight();
					orderIds.add(order.getOrderId());
					
					SalesRep orderSalesRep = null;
					
					if (!customers.containsKey(order.getUserId())) {
						Customer customer = this.webJaguar.getCustomerById(order.getUserId());
						if (customer != null && customer.getAccountNumber() != null && customer.getAccountNumber().trim().length() > 0) {
							customers.put(order.getUserId(), customer.getAccountNumber());
						} else {
							customers.put(order.getUserId(), "" + order.getUserId());						
						}
					}
					if(order.getSalesRepId() != null) {
						orderSalesRep = this.webJaguar.getSalesRepById(order.getSalesRepId());
					}
					
					//Line Items
					for (LineItem lineItem : order.getLineItems()) {
						
						line = new ArrayList<String>();
						
						// Order Type
						line.add(""  + (order.getOrderType() != null ? order.getOrderType() : "NONE"));	
						// Order date
						line.add(df.format(order.getDateOrdered()));	
						
						//Sku
						line.add((lineItem.getProduct().getSku() == null) ? "NONE" :lineItem.getProduct().getSku().replace("\n", " ").replace("\r", " ").replace(",", " ")); 
						//Name
						line.add((lineItem.getProduct().getName() == null) ? "NONE" :lineItem.getProduct().getName().replace("\n", " ").replace("\r", " ").replace(",", " ")); 
						//Quantity
						line.add(lineItem.getQuantity() + "0");
						//Price
						line.add((lineItem.getUnitPrice() == null) ? "NONE" : lineItem.getUnitPrice().toString());
						//Sub Total
						line.add((lineItem.getTotalPrice() == null) ? "NONE" : nf.format(lineItem.getTotalPrice()));
						//Sub Total
						line.add((order.getSubTotal() == null) ? "" : nf.format(order.getSubTotal()));
						// Tax
						line.add((lineItem.getTax() != null) ? nf.format(lineItem.getTax()) : "0");										// Order_tax
						// Promo Code
						line.add( (order.getPromoCode() == null) ? "NONE" : order.getPromoCode());	   															
						// Promo Amount
						line.add((order.getPromo() != null && order.getPromo().getDiscountType() != null &&	order.getPromo().getDiscountType().equalsIgnoreCase("order"))? 
							nf.format(order.getPromoAmount() + order.getLineItemPromoAmount()) : nf.format(order.getLineItemPromoAmount()));	   															
						
						if(order.getCustomShippingTitle() != null && !order.getCustomShippingTitle().trim().isEmpty()) {
							// custom shipping title
							line.add((order.getCustomShippingTitle() == null) ? "NONE" : order.getCustomShippingTitle());
							// custom shipping
							line.add((order.getCustomShippingCost() == null) ? "NONE" : nf.format(order.getCustomShippingCost()));
								
						} else {
							// shipping & handling title
							line.add((order.getShippingMethod() == null) ? "NONE" : order.getShippingMethod());
							// shipping cost
							try {
								line.add(order.getShippingCost() == null ? "NONE" : nf.format(order.getShippingCost()));
							} catch(Exception e) {
								
								System.out.println("Shipping "+order.getShippingCost());
								line.add("NONE");
							}
							
						}
						// grand total
						line.add(nf.format(order.getGrandTotal()));
						// Payment Method
						line.add((order.getPaymentMethod() == null) ? "NONE" : order.getPaymentMethod());
						
						// Status
						if(order.getStatus().contains("s")){
							line.add("shipped");
						}else if(order.getStatus().contains("x")){
							line.add("cancelled");
						}else if(order.getStatus().contains("p")){
							line.add("pending");
						}else if(order.getStatus().contains("pr")){
							line.add("processing");
						}else{
							line.add("shipped");
						}
						
						// Sales Rep
						line.add(((orderSalesRep != null && orderSalesRep.getAccountNumber() != null) ? orderSalesRep.getAccountNumber() : "NONE"));
						// Shipping First Name
						line.add(((order.getShipping() != null && order.getShipping().getFirstName() != null) ? order.getShipping().getFirstName() : "NONE"));
						// Shipping Last Name
						line.add(((order.getShipping() != null && order.getShipping().getLastName() != null) ? order.getShipping().getLastName() : "NONE"));
						// Shipping Company
						line.add(((order.getShipping() != null && order.getShipping().getCompany() != null) ? order.getShipping().getCompany() : "NONE"));
						// Shipping Address 1
						line.add(((order.getShipping() != null && order.getShipping().getAddr1() != null) ? order.getShipping().getAddr1() : "NONE"));
						// Shipping Address 2
						line.add(((order.getShipping() != null && order.getShipping().getAddr2() != null) ? order.getShipping().getAddr2() : "NONE"));
						// Shipping City
						line.add(((order.getShipping() != null && order.getShipping().getCity() != null) ? order.getShipping().getCity() : "NONE"));
						// Shipping State/Province
						line.add(((order.getShipping() != null && order.getShipping().getStateProvince() != null) ? order.getShipping().getStateProvince() : "NONE"));
						// Shipping Zip/Postal Code
						line.add(((order.getShipping() != null && order.getShipping().getZip() != null) ? order.getShipping().getZip() : "NONE"));
						
						writer.writeNext(line.toArray(new String[line.size()]));
					}
				}
				
				offset = offset + limit;
			}
			
			writer.flush();			
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
   		
		exportedFiles.add(invoiceFile);
		return;
	
	}

	private OrderSearch getOrderSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		OrderSearch orderSearch = (OrderSearch) request.getSession().getAttribute("orderSearch");
		if (orderSearch == null) {
			orderSearch = new OrderSearch();
			request.getSession().setAttribute("orderSearch", orderSearch);
		}

		// order status
		if (request.getParameter("status") != null) {
			orderSearch.setStatus(ServletRequestUtils.getStringParameter(request, "status", ""));
		}

		// order num
		if (request.getParameter("orderNum") != null) {
			try {
				Integer orderNum = Integer.parseInt(request.getParameter("orderNum"));
				if (orderNum > 0) {
					orderSearch.setOrderNum(orderNum.toString());
				} else {
					orderSearch.setOrderNum("");
				}
			} catch (Exception e) {
				orderSearch.setOrderNum("");
			}
		}

		// purchase order
		if (request.getParameter("purchaseOrder") != null) {
			orderSearch.setPurchaseOrder(ServletRequestUtils.getStringParameter(request, "purchaseOrder", ""));
		}

		// billTo
		if (request.getParameter("bill_to") != null) {
			orderSearch.setBillToName(ServletRequestUtils.getStringParameter(request, "bill_to", ""));
		}

		// shipTo
		if (request.getParameter("ship_to") != null) {
			orderSearch.setShipToName(ServletRequestUtils.getStringParameter(request, "ship_to", ""));
		}

		// company Name
		if (request.getParameter("companyName") != null) {
			orderSearch.setCompanyName(ServletRequestUtils.getStringParameter(request, "companyName", ""));
		}

		// email
		if (request.getParameter("email") != null) {
			orderSearch.setEmail(ServletRequestUtils.getStringParameter(request, "email", ""));
		}

		// phone
		if (request.getParameter("phone") != null) {
			orderSearch.setPhone(ServletRequestUtils.getStringParameter(request, "phone", ""));
		}

		// zipcode
		if (request.getParameter("zipcode") != null) {
			orderSearch.setZipcode(ServletRequestUtils.getStringParameter(request, "zipcode", ""));
		}

		// trackCode
		if (request.getParameter("trackCode") != null) {
			orderSearch.setTrackCode(ServletRequestUtils.getStringParameter(request, "trackCode", ""));
		}

		// paymentMethod
		if (request.getParameter("paymentMethod") != null) {
			orderSearch.setPaymentMethod(ServletRequestUtils.getStringParameter(request, "paymentMethod", ""));
		}

		// state
		if (request.getParameter("state") != null) {
			orderSearch.setState(ServletRequestUtils.getStringParameter(request, "state", ""));
		}

		// country
		if (request.getParameter("country") != null) {
			orderSearch.setCountry(ServletRequestUtils.getStringParameter(request, "country", ""));
		}

		// sales rep
		if (request.getParameter("sales_rep_id") != null) {
			orderSearch.setSalesRepId(ServletRequestUtils.getIntParameter(request, "sales_rep_id", -1));
		}

		SimpleDateFormat df = new SimpleDateFormat(siteConfig.get("SITE_DATE_FORMAT").getValue(), RequestContextUtils.getLocale(request));
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				orderSearch.setStartDate(df.parse(request.getParameter("startDate")));
			} catch (ParseException e) {
				orderSearch.setStartDate(null);
			}
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				orderSearch.setEndDate(df.parse(request.getParameter("endDate")));
			} catch (ParseException e) {
				orderSearch.setEndDate(null);
			}
		}

		// host for multi store
		if (request.getParameter("host") != null) {
			orderSearch.setHost(ServletRequestUtils.getStringParameter(request, "host", ""));
		}

		// shipping Method
		if (request.getParameter("shippingMethod") != null) {
			orderSearch.setShippingMethod(ServletRequestUtils.getStringParameter(request, "shippingMethod", ""));
		}

		// Date Type
		if (request.getParameter("dateType") != null) {
			orderSearch.setDateType(ServletRequestUtils.getStringParameter(request, "dateType", ""));
		}

		// Sku
		if (request.getParameter("sku") != null) {
			orderSearch.setSku(ServletRequestUtils.getStringParameter(request, "sku", null));
		}

		// Backend Order
		if (request.getParameter("backendOrder") != null) {
			orderSearch.setBackendOrder(ServletRequestUtils.getStringParameter(request, "backendOrder", ""));
		}

		// Product Field
		if (request.getParameter("productField") != null) {
			orderSearch.setProductField(ServletRequestUtils.getStringParameter(request, "productField", null));
		}

		// Product Field Number
		if (request.getParameter("productFieldNumber") != null) {
			orderSearch.setProductFieldNumber(ServletRequestUtils.getStringParameter(request, "productFieldNumber", null));
		}

		// Customer Field
		if (request.getParameter("customerField") != null) {
			orderSearch.setCustomerField(ServletRequestUtils.getStringParameter(request, "customerField", null));
		}

		// Customer Field Number
		if (request.getParameter("customerFieldNumber") != null) {
			orderSearch.setCustomerFieldNumber(ServletRequestUtils.getStringParameter(request, "customerFieldNumber", null));
		}

		// Order Type
		if (request.getParameter("orderType") != null) {
			orderSearch.setOrderType(ServletRequestUtils.getStringParameter(request, "orderType", null));
		}

		// Flag1
		if (request.getParameter("flag1") != null) {
			orderSearch.setFlag1(ServletRequestUtils.getStringParameter(request, "flag1", null));
		}

		return orderSearch;
	}


}
