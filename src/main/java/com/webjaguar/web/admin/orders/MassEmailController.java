/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.28.2007
 */

package com.webjaguar.web.admin.orders;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.scheduling.quartz.CronTriggerBean;
import org.springframework.scheduling.quartz.JobDetailBean;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.MassEmailForm;
import com.webjaguar.web.quartz.MassEmailJob;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.EmailCampaign;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactSearch;

public class MassEmailController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
    private Scheduler scheduler;
	public void setScheduler(Scheduler scheduler) { this.scheduler = scheduler; }
	private String type;
	public void setType(String type) { this.type = type; }
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	public MassEmailController() {
		setSessionForm(true);
		setCommandName("form");
		setCommandClass(MassEmailForm.class);
		setFormView("admin/orders/massEmail");
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy[KK:mm a]", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, false, 20 ) );	
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		
		MassEmailForm form = (MassEmailForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
		String sessionId = ServletRequestUtils.getStringParameter( request, "_session_id", "" );
		
		if (!sessionId.equals(request.getSession().getId())) {
			model.put("scheduleDone", true );
			model.put("message", "massEmail.logout" );
			return showForm(request, response, errors, model);
		}
		if (request.getParameter("__back") != null && this.type.equalsIgnoreCase("order")) {
			return new ModelAndView(new RedirectView("../orders/"));
		}
		if (request.getParameter("__back") != null && this.type.equalsIgnoreCase("customer")) {
			return new ModelAndView(new RedirectView("../customers/"));
		}
		if (request.getParameter("__back") != null && this.type.equalsIgnoreCase("crm")) {
			return new ModelAndView(new RedirectView("../crm/crmContactList.jhtm"));
		}
		boolean sendNow = false;
		if (request.getParameter("__sendnow") != null) {
			sendNow = true;
		}
		EmailCampaign campaign = new EmailCampaign(new Date());
		this.webJaguar.insertEmailCampaign( campaign );
		form.setCampaignId(campaign.getId());
		boolean scheduleResult = scheduleCampaign(form, sendNow, siteConfig, gSiteConfig);
		
		if ( !scheduleResult ) {
			model.put("message", "massEmail.error" );
		} else {
			model.put("scheduleDone", true );
			model.put("message", "massEmail.scheduledToSend" );
		}
		//campaign = new EmailCampaign(form.getCampaignName(), form.getScheduleTime(), form.getMessage(), ServletRequestUtils.getStringParameter( request, "_backend_user", "Admin" ), form.getSubject(), form.getNumEmails());
		campaign.setCampaignName(form.getCampaignName());
		campaign.setScheduled(form.getScheduleTime());
		campaign.setMessage(form.getMessage());
		campaign.setSender(ServletRequestUtils.getStringParameter( request, "_backend_user", "Admin" ));
		
		campaign.setSubject(form.getSubject());
		campaign.setEmailToSend(form.getNumEmails());
		this.webJaguar.updateEmailCampaign( campaign );
		
		form.setNumEmailsTemp(0);
		return showForm(request, response, errors, model);
	}	
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		MassEmailForm form = new MassEmailForm();
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		
		Set<Integer> customerIdSet = new HashSet<Integer>();
		Set<Integer> crmContactIdSet = new HashSet<Integer>();
		boolean uniquePerOrder = ServletRequestUtils.getBooleanParameter(request, "uniquePerOrder", false);
		form.setType(this.type);
		form.setUniquePerOrder(uniquePerOrder);
		
		if( !siteConfig.get( "MASS_EMAIL_CONTACT" ).getValue().trim().isEmpty() ){
			form.setFrom( siteConfig.get( "MASS_EMAIL_CONTACT" ).getValue() );
		} else {
			form.setFrom( siteConfig.get( "CONTACT_EMAIL" ).getValue() );
		}
		
		if(this.type.equalsIgnoreCase("customer")) {
			if ((Boolean) gSiteConfig.get("gSALES_REP")) {
				Integer salesRepId = null;
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
				if (salesRepId != null) {
					form.setFrom(this.webJaguar.getSalesRepById(salesRepId).getEmail());
				} 
			}
			setFormView("admin/customers/massEmail");					
			CustomerSearch search = this.webJaguar.getCustomerSearch( request );
			search.setLimit( null );
			form.setCustomerSearch( search );
			
			List<Customer> customerList = this.webJaguar.getCustomerListWithDate( search );
			for(Customer customer : customerList){
				if ( customer != null && customer.getUsername() != null && !customer.getUsername().isEmpty() && !customer.isUnsubscribe() ) {
					form.setCartNotEmpty(search.getCartNotEmpty());
					customerIdSet.add(customer.getId());
				}
			}
			form.setUserIds( customerIdSet );
			form.setNumEmails( form.getUserIds().size() );
		}else if(this.type.equalsIgnoreCase("customerTextMessage")){
			if ((Boolean) gSiteConfig.get("gSALES_REP")) {
				Integer salesRepId = null;
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
				if (salesRepId != null) {
					form.setFrom(this.webJaguar.getSalesRepById(salesRepId).getEmail());
				} 
			}
			setFormView("admin/customers/massTextMessage");					
			CustomerSearch search = this.webJaguar.getCustomerSearch( request );
			search.setLimit( null );
			form.setCustomerSearch( search );
			
			List<Customer> customerList = this.webJaguar.getCustomerListWithDate( search );
			for(Customer customer : customerList){
				String cellPhone = this.webJaguar.getNumber(customer.getAddress().getCellPhone());
				Integer textMessageServerId = customer.getAddress().getMobileCarrierId();
				String textMessageServer = null;
				if( textMessageServerId != null ) textMessageServer = this.webJaguar.getTextMessageServerById(textMessageServerId);
				
				if ( customer != null && customer.getUsername() != null && !customer.getUsername().isEmpty() && customer.isTextMessageNotify() 
						&& cellPhone != null && !cellPhone.equals("") && textMessageServer != null && !textMessageServer.equals("") ) {
					form.setCartNotEmpty(search.getCartNotEmpty());
					customerIdSet.add(customer.getId());
				}
			}
			form.setUserIds( customerIdSet );
			form.setNumEmails( form.getUserIds().size() );
		} else if(this.type.equalsIgnoreCase("order")) {
			OrderSearch search = this.webJaguar.getOrderSearch( request );
			search.setLimit( null );
			form.setOrderSearch( search );
		
			List<Order> orderList = this.webJaguar.getOrdersList( search );
			form.setOrders( orderList );
		
			for ( Order order : form.getOrders()) {
				List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
				MultiStore multiStore = null;
				if (multiStores.size() > 0) {
					multiStore = multiStores.get(0);
				}
				if (multiStore != null) {
					form.setFrom(multiStore.getContactEmail());
				}
			
				Customer customer = this.webJaguar.getCustomerById( order.getUserId() );
				if ( customer != null && !customer.isUnsubscribe() && customer.getUsername() != null && !customer.getUsername().isEmpty() ) {
					if (uniquePerOrder) {
						form.getOrderIds().add(order.getOrderId());
					} else {
						customerIdSet.add( customer.getId() );
					}
				}
			}
			form.setUserIds(customerIdSet);
			if (uniquePerOrder) {
				form.setNumEmails(form.getOrderIds().size());
			} else {
				form.setNumEmails(form.getUserIds().size());
			}
		} else if(this.type.equalsIgnoreCase("crm")) {
			
			setFormView("admin/crm/contact/massEmail");
			long startTime = System.nanoTime();
			int contactId = ServletRequestUtils.getIntParameter(request, "contactId", -1);
			
			if (contactId != -1) {
				CrmContact crmContact = this.webJaguar.getCrmContactById(contactId);
				if ( crmContact != null && crmContact.getEmail1() != null && !crmContact.getEmail1().isEmpty() && !crmContact.isUnsubscribe()) {
					crmContactIdSet.add( crmContact.getId() );
				}
			} else {
				CrmContactSearch search = this.webJaguar.getCrmContactSearch(request);
				search.setLimit(null);

				startTime = System.nanoTime();
				List<CrmContact> crmContactList = this.webJaguar.getCrmContactList2(search);
				
				
				startTime = System.nanoTime();
				Map<String, Integer> emailIdMap = new HashMap<String, Integer>();
				for(CrmContact crmContact : crmContactList){
					if (crmContact != null && crmContact.getEmail1() != null && !crmContact.getEmail1().isEmpty() && !crmContact.isUnsubscribe()){
						if(emailIdMap.get(crmContact.getEmail1().trim())==null){
							crmContactIdSet.add(crmContact.getId());
						}
						emailIdMap.put(crmContact.getEmail1().trim(), crmContact.getId());
					}
				}
			
			}
			form.setCrmContactIds(crmContactIdSet);
			form.setNumEmails(form.getCrmContactIds().size());
		}
		form.setNumEmailsTemp(form.getNumEmails());
		GregorianCalendar now = new GregorianCalendar();
		now.add(Calendar.MINUTE, 1);
		form.setScheduleTime(now.getTime());
		return form;
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors) {
		MassEmailForm form = (MassEmailForm) command;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("messageGroups", this.webJaguar.getSiteMessageGroupList()) ;
		map.put("messages", this.webJaguar.getSiteMessageList());
		map.put("type", this.type);
		int emailToSend = this.webJaguar.emailInProcessCount();
		if ( Integer.parseInt( siteConfig.get( "MASS_EMAIL_POINT" ).getValue())  < form.getNumEmailsTemp() + emailToSend ) {
			map.put("massEmailPoint", Integer.parseInt( siteConfig.get( "MASS_EMAIL_POINT" ).getValue()));
			map.put("emailToSend", emailToSend);
			map.put("pointError", "massEmail.pointError");
			map.put("scheduleDone", true );
		}
		return map;
	}	
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		MassEmailForm form = (MassEmailForm) command;
		Calendar scheduledCalendar = Calendar.getInstance();
        scheduledCalendar.add(Calendar.MINUTE,1);
        
        if(this.type.equalsIgnoreCase("customerTextMessage") && form.getMessage().length() >= 255){
        	errors.rejectValue("message", "form.maxTextMessage", "Message content should be less than 255 characters");
        }
			
		boolean sendNow = false;
		if (request.getParameter("__sendnow") != null) {
			sendNow = true;
		}
		if ( !sendNow && !form.getScheduleTime().after( scheduledCalendar.getTime() ) ) {
			errors.rejectValue("scheduleTime", "form.futureDate", "Schedule time should be in future.");
		}
	}
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("__back") != null) {
			return true;			
		}
		return false;
	}
	
	private boolean scheduleCampaign(MassEmailForm massEmailForm, boolean sendNow, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) throws ParseException, SchedulerException{
		
		// get scheduled date,hour,minute
		Calendar scheduledCalendar = Calendar.getInstance();
		if ( sendNow ) {
			scheduledCalendar.add(Calendar.MINUTE,1);
		} else {
			scheduledCalendar.setTime( massEmailForm.getScheduleTime() );
		}
		
		int year = scheduledCalendar.get(Calendar.YEAR);
		int month = scheduledCalendar.get(Calendar.MONTH)+1;
		int day = scheduledCalendar.get(Calendar.DAY_OF_MONTH);
		int hour = scheduledCalendar.get(Calendar.HOUR_OF_DAY);
		int minute = scheduledCalendar.get(Calendar.MINUTE);
		
		// job detail quartz 2.x
//		JobDetail job = JobBuilder.newJob(MassEmailJob.class)
//	    .withIdentity(siteConfig.get( "MASS_EMAIL_JOB_NAME" ).getValue() + "_job_"+year+month+day+hour+minute, Scheduler.DEFAULT_GROUP)
//	    .storeDurably()
//	    .requestRecovery().build();
//		job.getJobDataMap().put("massEmailForm",massEmailForm);
//		job.getJobDataMap().put("messageContect",getMessageSourceAccessor());
		
		
		JobDetailBean job = new JobDetailBean();
		job.setJobClass(MassEmailJob.class);
		job.setName(siteConfig.get( "MASS_EMAIL_JOB_NAME" ).getValue() + "_job_"+year+month+day+hour+minute);
		job.setGroup(Scheduler.DEFAULT_GROUP);
		job.getJobDataMap().put("massEmailForm",massEmailForm);
		job.getJobDataMap().put("messageContect",getMessageSourceAccessor());

		// trigger quatrz 2.x
//		CronTrigger trigger = TriggerBuilder.newTrigger()
//	    .withIdentity(siteConfig.get( "MASS_EMAIL_JOB_NAME" ).getValue() + "_trigger_"+year+month+day+hour+minute, Scheduler.DEFAULT_GROUP)
//	    .withSchedule(CronScheduleBuilder.cronSchedule("0 "+ minute + " " + hour + " " + day + " " + month + " ? " + year))
//	    .build();
		
		CronTriggerBean trigger = new CronTriggerBean();
		trigger.setStartTime(java.util.Calendar.getInstance().getTime());
		trigger.setCronExpression("0 "+ minute + " " + hour + " " + day + " " + month + " ? " + year);
		trigger.setName(siteConfig.get( "MASS_EMAIL_JOB_NAME" ).getValue() + "_trigger_"+year+month+day+hour+minute);
		trigger.setGroup(Scheduler.DEFAULT_GROUP);
		
		massEmailForm.setCampaignName( job.getDescription() );
		try {
			// schedule a job
			scheduler.scheduleJob(job,trigger);
			return true;
		} catch (SchedulerException e) {
			return false;
		}
	}
}