/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.22.2006
 */

package com.webjaguar.web.admin.orders;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.QualifierSearch;
import com.webjaguar.model.SalesRep;

public class ListController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private String statusQuote;
	public void setStatusQuote(String statusQuote) { this.statusQuote = statusQuote; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
		
		Date d1 = new Date();
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		String orderName= "orders";	
			
		List<SalesRep> salesReps = null;	
		// For Quote View
		if( (statusQuote != null && statusQuote.equals("true")) || (request.getParameter("orderName")!= null && request.getParameter("orderName").equals("quotes")) ) {
			request.setAttribute("orderName", "quotes");
			orderName ="quotes";
		}		
		model.put("orderName", orderName);
		
		OrderSearch orderSearch = this.webJaguar.getOrderSearch( request );	
		//System.out.println("Orders List Line 54  : "+((new Date().getTime() - d1.getTime()) / 1000));
		
		// For Quote View		
		if( (statusQuote != null && statusQuote.equals("true") && siteConfig.get("PRODUCT_QUOTE") != null && siteConfig.get("PRODUCT_QUOTE").getValue().equals("true") ) ||
					(request.getParameter("orderName")!= null && request.getParameter("orderName").equals("quotes")) ) {
			orderSearch.setStatus("xq");			
		} else { 
			if((statusQuote == null || statusQuote == "") && orderSearch.getStatus() == "xq") {
				orderSearch.setStatus(ServletRequestUtils.getStringParameter( request, "status", "" ));
			}
		}
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			
			//System.out.println("Orders List Line 72  : "+((new Date().getTime() - d1.getTime()) / 1000));
			Integer salesRepId = null;
			if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
			salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
			orderSearch.setAccountManager(salesRepId);
				//if((orderSearch.getSalesRepId()==null || orderSearch.getSalesRepId()==-1) && orderSearch.getSalesRepProcessedById()!=null && orderSearch.getSalesRepProcessedById()!=-1){
				//	orderSearch.setAccountManager(orderSearch.getSalesRepProcessedById());
				//}
			}

			//System.out.println("Orders List Line 78  : "+((new Date().getTime() - d1.getTime()) / 1000));
			if (salesRepId == null) {
				salesReps = this.webJaguar.getSalesRepList();				
			} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
				salesReps = this.webJaguar.getSalesRepList(salesRepId);
			} else {
				salesReps = new ArrayList<SalesRep>();
				salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
			}
			for (SalesRep salesRep: salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);				
			}
			//System.out.println("Orders List Line 90  : "+((new Date().getTime() - d1.getTime()) / 1000));
			if (salesRepId != null) {
				// make sure access users sees only his customers
				if (orderSearch.getSalesRepId() == null && orderSearch.getSalesRepProcessedById() == null) {
					orderSearch.setSalesRepId(salesRepId);
				} else if (!(salesRepMap.containsKey(orderSearch.getSalesRepId()) || salesRepMap.containsKey(orderSearch.getSalesRepProcessedById()) || salesRepMap.containsKey(orderSearch.getQualifierId()))) {
					orderSearch.setSalesRepId(salesRepId);
				} 
				// associate reps to parents
				for (SalesRep salesRep: salesReps) {
					if (salesRepMap.containsKey(salesRep.getParent())) {
						salesRepMap.get(salesRep.getParent()).getSubAccounts().add(salesRep);					
					}
				}
				model.put("salesRepTree", salesRepMap.get(salesRepId));
			}			
			
			model.put("salesRepMap", salesRepMap);
			model.put("salesReps", salesReps);
		}
		
		//System.out.println("Orders List Line 112  : "+((new Date().getTime() - d1.getTime()) / 1000));
		// check if __batchPdf button was clicked
		if (request.getParameter("__batchPdf") != null) {
			String indexes[] = request.getParameterValues("__selected_id");
			List<Integer> orderIds = new ArrayList<Integer>();
			if (indexes != null) {
				for (int i=0; i<indexes.length; i++) {
					String orderId = ServletRequestUtils.getStringParameter(request, "__selected_order_"+indexes[i], null);
					if (orderId!=null){
						orderIds.add(Integer.parseInt(orderId));
					}
				}
			}
			if (orderIds.size() > 0) {
				if (batchPrint(request, response, orderIds)) {
					// pdf generated
					return null;
				}
			}
		}
		// check if options button was clicked
		else if (request.getParameter("__groupAssignPacher") != null) {
			// get group ids
			Set<Object> groups = new HashSet<Object>();
			String groupIdString = ServletRequestUtils.getStringParameter( request, "__group_id" );
			String batchType = ServletRequestUtils.getStringParameter( request, "__batch_type" );
			String[] groupIds = groupIdString.split( "," );
			for ( int x = 0; x < groupIds.length; x++ ) {
				if ( !("".equals( groupIds[x].trim() )) ) {
					try {
						groups.add( Integer.valueOf( groupIds[x].trim() ) );
					}catch ( Exception e ) {
						// do nothing
					}
				}
			}
			String indexes[] = request.getParameterValues("__selected_id");
			if ((indexes != null ) && ( request.getParameter("__groupAssignAll") == null)) {
				for (int i=0; i<indexes.length; i++) {
					String customerId = ServletRequestUtils.getStringParameter(request, "__selected_customer_"+indexes[i], null);
					if (customerId!=null){
						this.webJaguar.batchGroupIds( Integer.parseInt(customerId), groups, batchType);
					}
				}
			} else if( request.getParameter("__groupAssignAll") != null ) {
				for(Integer id: this.webJaguar.getCustomersWithOrderCount(orderSearch)) {
					this.webJaguar.batchGroupIds( id, groups, batchType);
				}
			}
		}
		// check if options button was clicked
		else if (request.getParameter("__batchscheduleddate") != null) {
			String subStatus = ServletRequestUtils.getStringParameter( request, "userDueDate" );
			String indexes[] = request.getParameterValues("__selected_id");
			
			DateFormat df2 = new SimpleDateFormat("MM/dd/yyyy[hh:mm a]");

			if (indexes != null) {
				for (int i=0; i<indexes.length; i++) {
					String orderId = ServletRequestUtils.getStringParameter(request, "__selected_order_"+indexes[i], null);
					String status = ServletRequestUtils.getStringParameter(request, "userDueDate", "");
					
					Date d2 = null;
					 
					try{
						d2 = df2.parse(status);
					}catch(Exception ex){
					}
					
					if (orderId!=null){
						Order order = new Order();
						order.setOrderId(Integer.parseInt(orderId));
						order.setUserDueDate(d2);
						this.webJaguar.insertOrderUserDueDate(order);
					}
				}
			}
		}
		// check if options button was clicked
		else if (request.getParameter("__batchsubstatus") != null) {
			
			String subStatus = ServletRequestUtils.getStringParameter( request, "__sub_status" );
			String indexes[] = request.getParameterValues("__selected_id");
			
			if (indexes != null) {
				OrderStatus orderStatus = new OrderStatus();
				for (int i=0; i<indexes.length; i++) {
					
					String orderId = ServletRequestUtils.getStringParameter(request, "__selected_order_"+indexes[i], null);
					
					Order order = new Order();
					if (orderId!=null){
						try{
							order.setOrderId(Integer.parseInt(orderId));
							order.setSubStatus(subStatus);
							this.webJaguar.updateOrderSubStatus(order);
						}catch(Exception ex){
						}
					}
				}
			}
		} else if (request.getParameter("__batchShippingTitle") != null) {
			String shippingTitle = ServletRequestUtils.getStringParameter( request, "__shipping_title" );
			String indexes[] = request.getParameterValues("__selected_id");
			
			if (indexes != null) {
				for (int i=0; i<indexes.length; i++) {
					String orderId = ServletRequestUtils.getStringParameter(request, "__selected_order_"+indexes[i], null);
					Order order = this.webJaguar.getOrder(Integer.valueOf(orderId), null);					
					if (order != null){
						try{
							order.setShippingMethod(shippingTitle);
							this.webJaguar.updateOrder(order);
						}catch(Exception ex){
						}
					}
				}
			}
		}else if(request.getParameter("__batchDaysToShip") != null) {
			String days = ServletRequestUtils.getStringParameter( request, "__shipping_days" );
			String indexes[] = request.getParameterValues("__selected_id");
			
			if (indexes != null) {
				for (int i=0; i<indexes.length; i++) {
					String orderId = ServletRequestUtils.getStringParameter(request, "__selected_order_"+indexes[i], null);
					Order order = this.webJaguar.getOrder(Integer.valueOf(orderId), null);					
					if (order != null){
						try{
							if(days != null) {
								order.setShippingPeriod(Integer.valueOf(days));
								this.webJaguar.updateOrder(order);
							}
						}catch(Exception ex){
						}
					}
				}
			}
		}
		//print sold lable
		else if(request.getParameter("__printSoldLabel") != null){
				String indexes[] = request.getParameterValues("__selected_id");
				String customerId = ServletRequestUtils.getStringParameter(request, "__selected_customer_"+indexes[0], null);
				Customer customer = this.webJaguar.getCustomerById(Integer.valueOf(customerId));
				//System.out.println("customerId is "+customerId);
				if(customer != null){
					//System.out.println("indexes "+indexes[0]+" cardID "+customer.getCardId());
					return new ModelAndView("../../touchPrintLabel.jhtm?__cardId="+customer.getCardId()+"&__printPdf=true");
				}	
	   } else if(request.getParameter("__batchcustomfield1") != null){

				String indexes[] = request.getParameterValues("__selected_id");
				if (indexes != null) {
					OrderStatus orderStatus = new OrderStatus();
					Order order =  new Order();
					try{
					for (int i=0; i<indexes.length; i++) {
						int orderId = ServletRequestUtils.getIntParameter(request, "__selected_order_"+indexes[i], 0);

						 order = this.webJaguar.getOrder(orderId,null);
						order.setCustomField1(request.getParameter("__customfield1"));
						this.webJaguar.updateOrder(order);

					}
					} catch(Exception e){
						
					}

				}	
			}else if(request.getParameter("__batchcustomfield2") != null){

				String indexes[] = request.getParameterValues("__selected_id");
				if (indexes != null) {
					OrderStatus orderStatus = new OrderStatus();
					Order order =  new Order();
					try{
					for (int i=0; i<indexes.length; i++) {
						int orderId = ServletRequestUtils.getIntParameter(request, "__selected_order_"+indexes[i], 0);

						 order = this.webJaguar.getOrder(orderId,null);
						order.setCustomField2(request.getParameter("__customfield2"));
						this.webJaguar.updateOrder(order);

					}
					} catch(Exception e){
						
					}

				}	
			}
    	
    	// set end date to end of day
		if (orderSearch.getEndDate() != null) {
			orderSearch.getEndDate().setTime(orderSearch.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}
		
		if (!(Boolean) gSiteConfig.get("gGROUP_CUSTOMER")) {
			orderSearch.setGroupId( null );
		} else {
			//for getting only active Customer groups
			CustomerGroupSearch customerGroupSearch = new CustomerGroupSearch();
			customerGroupSearch.setActive("1");
			model.put( "groups", this.webJaguar.getCustomerGroupList(customerGroupSearch)) ;
		}
		
		//System.out.println("Orders List Line 210  : "+((new Date().getTime() - d1.getTime()) / 1000));
		orderSearch.setAffiliateId( ServletRequestUtils.getIntParameter( request, "affId", -1 ) );
		
		int count = this.webJaguar.getOrdersListCount(orderSearch);
		int pendingCount = this.webJaguar.getOrdersCountByStatus("p");
		int beingReviewedCount =  this.webJaguar.getOrdersCountByStatus("pr");
		int waitingPaymentCount = this.webJaguar.getOrdersCountByStatus("wp");
		int releasedCount = this.webJaguar.getOrdersCountByStatus("rls");
		int shippedCount =  this.webJaguar.getOrdersCountByStatus("s");
		int cancelledCount = this.webJaguar.getOrdersCountByStatus("x");
		
		model.put("pendingCount", pendingCount);
		model.put("beingReviewedCount", beingReviewedCount);
		model.put("waitingPaymentCount", waitingPaymentCount);
		model.put("releasedCount", releasedCount);
		model.put("shippedCount", shippedCount);
		model.put("cancelledCount", cancelledCount);
		
		//System.out.println("Orders List Line 214  : "+((new Date().getTime() - d1.getTime()) / 1000));
		if (count < orderSearch.getOffset()-1) {
			orderSearch.setPage(1);
		}
		orderSearch.setLimit(orderSearch.getPageSize());
		orderSearch.setOffset((orderSearch.getPage()-1)*orderSearch.getPageSize());
		List<Order> ordersList = this.webJaguar.getOrdersList(orderSearch);	
		
//		for (Order order : ordersList) {	
//				Customer customer = this.webJaguar.getCustomerById(order.getUserId());
//				order.setCustomerField19(customer.getField19() == null ? "" : customer.getField19() );
//		}
		//System.out.println("Orders List Line 221  : "+((new Date().getTime() - d1.getTime()) / 1000));
		int pageCount = count/orderSearch.getPageSize();
		if (count%orderSearch.getPageSize() > 0) {
			pageCount++;
		}
		model.put("pageCount", pageCount);
		model.put("pageEnd", orderSearch.getOffset()+ordersList.size());
		model.put("count", count);
		
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			LinkedHashMap<Integer, SalesRep> salesRepMap = new LinkedHashMap<Integer, SalesRep>();
			
			Iterator<SalesRep> iter = this.webJaguar.getSalesRepList().iterator();
		
			Map<String, SalesRep> treeMap = new TreeMap<String, SalesRep>();
			while (iter.hasNext()) {
				SalesRep salesRep = iter.next();
				if(!salesRep.isInactive()){
					treeMap.put(salesRep.getName(), salesRep);
				}
			}
			
			Set<String> keys = treeMap.keySet();
			for(String str : keys) {
				SalesRep tmp = treeMap.get(str);
				salesRepMap.put(tmp.getId(), tmp);
			}
			model.put("salesRepMap", salesRepMap);
			
			iter = this.webJaguar.getSalesRepList().iterator();
			
			treeMap = new TreeMap<String, SalesRep>();
			while (iter.hasNext()) {
				SalesRep salesRep = iter.next();
				if(salesRep.isInactive()){
					treeMap.put(salesRep.getName(), salesRep);
				}
			}
			
			keys = treeMap.keySet();
			salesRepMap = new LinkedHashMap<Integer, SalesRep>();
			for(String str : keys) {
				SalesRep tmp = treeMap.get(str);
				salesRepMap.put(tmp.getId(), tmp);
			}
			model.put("salesRepMapInactive", salesRepMap);
		}
		
		if ((Boolean) gSiteConfig.get("gSERVICE") && siteConfig.get("CUSTOMIZE_ORDER_LIST").getValue().contains("workOrder")) {
			model.put("serviceMap", this.webJaguar.getServiceMap());
		}
		
		QualifierSearch qualifierSearch = new QualifierSearch();
		qualifierSearch.setShowActiveQualifier(true);
		model.put("activeQualifiers", salesReps);
		
		//System.out.println("Orders List Line 245  : "+((new Date().getTime() - d1.getTime()) / 1000));
		model.put( "productFieldList", this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
		model.put( "customerFieldList", this.webJaguar.getCustomerFields());
		model.put( "countries",this.webJaguar.getEnabledCountryList());
		model.put( "orders", ordersList);
		model.put( "firstOrderList", this.webJaguar.getFirstOrderAllUser());
		model.put("languageCodes", this.webJaguar.getLanguageSettings());
		model.put("customShippingRateList", this.webJaguar.getCustomShippingRateList( false, true ));
		
		/*
		 * 2021-07-29
		 * This part of logic is migrated to javascript, only load if click the "track on app" link. 
		 * As the previous logic is making too many redundant connections to the server without closing it properly.
		 * Loads the ssid only when it is needed.
		 * */
//		setPaymentUrlsforApps(ordersList);
		
		//System.out.println("Orders List Line 253  : "+((new Date().getTime() - d1.getTime()) / 1000));
		if (siteConfig.get("CUSTOMIZE_ORDER_LIST").getValue().contains("echoSign")) {
			model.put("echoSign", this.webJaguar.getLatestEchoSignMap());			
		}
		//System.out.println("Orders List Line 257  : "+((new Date().getTime() - d1.getTime()) / 1000));
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)
			return new ModelAndView("admin-responsive/orders/list", "model", model);
        return new ModelAndView("admin/orders/list", "model", model);
	}
	
	
	
	private void setPaymentUrlsforApps(List<Order> ordersList) {

		for (Order order :ordersList){
			try{
				order.setTrackUrlApps(" https://www.viatrading.com/dv/liveapps/shiptrack.php?sess_id="+this.webJaguar.paymentUrl(order.getOrderId()));
			} catch(Exception e){
				// do nothing
			}
		}
	}

	private List<Order> ordersWithRestricts(List<Order> orderList, Integer salesRepId, Integer salesRepProcessedById){
		List<Order> tempList = new LinkedList<Order>();
		Integer customerSalesRepId = null;
		Integer customerQualifierId = null;
		for(Order order : orderList){
			customerSalesRepId = -1;
			customerQualifierId = -1;
			Customer customer = this.webJaguar.getCustomerById(order.getUserId());
			if(customer!=null && customer.getSalesRepId()!=null){
				customerSalesRepId = customer.getSalesRepId();
			}
			if(customer!=null && customer.getSalesRepId()!=null){
				customerQualifierId = customer.getQualifier();
			}
			if(order!=null && (order.getSalesRepId()==salesRepId || order.getQualifier()==salesRepId || salesRepProcessedById==salesRepId || customerSalesRepId==salesRepId || customerQualifierId==salesRepId)){
				tempList.add(order);
			}
		}
		return tempList;
	}
	
	private boolean batchPrint(HttpServletRequest request, HttpServletResponse response, List<Integer> orderIds) {
		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File pdfFile = new File(tempFolder, "invoices.pdf");
		
		boolean result = false;
		
		try {
			result = this.webJaguar.createPdfInvoice(pdfFile, orderIds, request);
			
			byte[] b = this.webJaguar.getBytesFromFile(pdfFile);
			
			response.addHeader("Content-Disposition", "attachment; filename="+pdfFile.getName());
	        response.setBufferSize((int) pdfFile.length());
			response.setContentType("application/pdf");
			response.setContentLength((int) pdfFile.length());
			
			// output stream
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(b, 0,(int) pdfFile.length());
			ouputStream.flush();
			ouputStream.close();
	        
			// delete file before exit
			pdfFile.delete();		

			if (result) {
				// update 
				for (Integer orderId: orderIds) {
					Order order = new Order();
					order.setOrderId(orderId);
					order.setPrinted(true);
					this.webJaguar.updateOrderPrinted(order);
				}				
			}
		} catch (Exception e) {
			
		}
		return result;
	}
}
