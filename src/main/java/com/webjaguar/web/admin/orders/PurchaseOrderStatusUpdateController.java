package com.webjaguar.web.admin.orders;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.PurchaseOrder;
import com.webjaguar.model.PurchaseOrderLineItem;
import com.webjaguar.model.PurchaseOrderStatus;

public class PurchaseOrderStatusUpdateController implements Controller{
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private String type;
	public void setType(String type) { this.type = type; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
		
		String status = request.getParameter("status");
		String poIds = request.getParameter("poIds");
		String[] poArry = null;
		
		if(poIds!=null && poIds.length()>0){
			poArry = poIds.split(",");
		}
		
		AccessUser accessUser = (AccessUser) request.getAttribute("accessUser");
		
		for(String poId : poArry){
			
			List<PurchaseOrderStatus> list = this.webJaguar.getPurchaseOrderStatusHistory(Integer.parseInt(poId));
			Boolean staged = false;
			
			for(PurchaseOrderStatus poStatus : list){
				if(poStatus.getStatus().equalsIgnoreCase("rece") || poStatus.getStatus().equalsIgnoreCase("x")){
					staged = true;
				}
			}
			
			if(!staged){
				this.webJaguar.updatePoStatusById(Integer.parseInt(poId), status);
				this.webJaguar.addNewPurchaseOrderHistoryStatus(Integer.parseInt(poId), status, accessUser.getUsername());
				PurchaseOrder purchaseOrder = null;
				if(status.equalsIgnoreCase("x")){				
					purchaseOrder = this.webJaguar.getPurchaseOrder(Integer.parseInt(poId));			
					for (PurchaseOrderLineItem lineItem : purchaseOrder.getPoLineItems()) {
		    			lineItem.setPoId(Integer.parseInt(poId));			
		    			if(lineItem.getQty()!=null){
		    				Inventory lineInventory = new Inventory(); 	
		    				lineInventory.setSku(lineItem.getSku());
		    				lineInventory.setInventoryAFS((-1)*lineItem.getQty());
		    				
		    				InventoryActivity inventoryActivity = new InventoryActivity();
		    				

		    				inventoryActivity.setSku(lineInventory.getSku());
		    				inventoryActivity.setReference(purchaseOrder.getPoId().toString());
		    				inventoryActivity.setAccessUserId(accessUser.getId());
		    				inventoryActivity.setType("PO Cancelled");
						
		    				Inventory inventory = new Inventory();
							inventory.setSku(lineInventory.getSku());
		    				
							inventory = this.webJaguar.getInventory(inventory);
							inventoryActivity.setQuantity(lineItem.getQty());
		    				inventoryActivity.setInventory(inventory.getInventory());
		    				inventoryActivity.setInventoryAFS(inventory.getInventoryAFS());

		    			    this.webJaguar.addKitPartsHistory(inventoryActivity);
		    				this.webJaguar.updateInventory(lineInventory);
		    			}
		    		}
				}	
				else if(status.equalsIgnoreCase("rece")){				
					purchaseOrder = this.webJaguar.getPurchaseOrder(Integer.parseInt(poId));			
					for (PurchaseOrderLineItem lineItem : purchaseOrder.getPoLineItems()) {
		    			lineItem.setPoId(Integer.parseInt(poId));			
		    			if(lineItem.getQty()!=null){
		    				Inventory lineInventory = new Inventory(); 	
		    				lineInventory.setSku(lineItem.getSku());
		    				lineInventory.setInventory(lineItem.getQty());
		    				this.webJaguar.updateInventory(lineInventory);
		    			
		    				InventoryActivity inventoryActivity = new InventoryActivity();
		    				

		    				inventoryActivity.setSku(lineInventory.getSku());
		    				inventoryActivity.setReference(purchaseOrder.getPoId().toString());
		    				inventoryActivity.setAccessUserId(accessUser.getId());
		    				inventoryActivity.setType("PO Received");
						
		    				Inventory inventory = new Inventory();
							inventory.setSku(lineInventory.getSku());
		    				
							inventory = this.webJaguar.getInventory(inventory);
							inventoryActivity.setQuantity(lineItem.getQty());
		    				inventoryActivity.setInventory(inventory.getInventory());
		    				inventoryActivity.setInventoryAFS(inventory.getInventoryAFS());

		    			    this.webJaguar.addKitPartsHistory(inventoryActivity);
		    			}
		    		}
				}
			}
		}

		return null;  
	}
}
	
