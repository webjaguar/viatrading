/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.12.2008
 */

package com.webjaguar.web.admin.orders.packingList;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.PackingList;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.form.EmailMessageForm;
import com.webjaguar.web.form.PackingListForm;

public class GeneratePackingListController extends SimpleFormController {

	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;

	private WebJaguarFacade webJaguar;
    public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }

	public GeneratePackingListController() {
		setSessionForm(true);
		setCommandName("packingListForm");
		setCommandClass(PackingListForm.class);
		setFormView("admin/orders/packingList/generatePackingList");
		setSuccessView("packingList.jhtm");
	}

	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(df, true, 10));
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
		PackingListForm packingListForm = (PackingListForm) command;
		Map<String, Object> map = new HashMap<String, Object>();
		if (request.getParameter("__walkin") != null && request.getParameter("__walkin").equals("true")){
			setFormView("admin/orders/packingList/walkin");
		}else{
			setFormView("admin/orders/packingList/generatePackingList");
		}
		
		
		map.put("messages", this.webJaguar.getSiteMessageList());

		if (packingListForm.getOrder() != null) {
			packingListForm.getOrder().setLineItems(this.webJaguar.getNotProcessedLineItems(packingListForm.getOrder().getOrderId(), (packingListForm.isNewPackingList()) ? null : packingListForm.getPackingList().getPackingListNumber()));
		}
		
		map.put("order", packingListForm.getOrder());
		map.put("productFieldsHeader", this.webJaguar.getProductFieldsHeader(packingListForm.getOrder(), (Integer) gSiteConfig.get("gPRODUCT_FIELDS")));

		packingListForm.getPackingList().setSubTotal(updatePackingListLineItems(request, packingListForm));
		packingListForm.getPackingList().setGrandTotal();
		return map;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors)
			throws Exception {
		PackingListForm packingListForm = (PackingListForm) command;
		
		int orderId = ServletRequestUtils.getIntParameter(request, "orderId", -1);
		Order order = this.webJaguar.getOrder(orderId, null);
		
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()), "orderId", packingListForm.getPackingList().getOrderId());
		}
		if (packingListForm.getPackingList().getOrderId() == -1)
			return new ModelAndView(new RedirectView("invoice.jhtm"));


		Map<String, Object> model = new HashMap<String, Object>();
		
		packingListForm.getPackingList().setSubTotal(updatePackingListLineItems(request, packingListForm));
		packingListForm.getPackingList().setGrandTotal();
		if (request.getParameter("__recalculate") != null) {
			return  showForm(request, response, errors, model);
			//return new ModelAndView(new RedirectView("generatePackingList.jhtm?orderId="+packingListForm.getOrder().getOrderId()),  model);
		}
		// send email
		if (!packingListForm.getEmailMessageForm().getMessage().trim().equals("")) {
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			try {
				constructMessage(helper, packingListForm, request, getApplicationContext());
				mailSender.send(mms);
				packingListForm.getPackingList().setUserNotified(true);
			} catch (Exception e) {
				packingListForm.setErrorMessage("email.exception");
				packingListForm.getPackingList().setUserNotified(false);
			}
		}
		Integer accessUserId = null ;
		if ( request.getAttribute( "accessUser" ) != null ) {
			accessUserId = ((AccessUser) request.getAttribute( "accessUser" )).getId();
		}
		boolean inventoryHistory = false;
		if(siteConfig.get("INVENTORY_HISTORY").getValue().equals("true")) {
			inventoryHistory = true;
		}
		if (packingListForm.isNewPackingList()) {
			try {
				this.webJaguar.insertPackingList(packingListForm.getPackingList(), accessUserId, inventoryHistory, order);
			} catch (Exception e) {
			}
		} else {
			Date previousShipDate = this.webJaguar.getPackingList(packingListForm.getPackingList().getOrderId(), packingListForm.getPackingList().getPackingListNumber()).getShipped();
			if ( previousShipDate == null && packingListForm.getPackingList().getShipped() != null) {
				this.webJaguar.updateLineItemOnHand(packingListForm.getPackingList(), inventoryHistory, accessUserId);
			}
			if (previousShipDate != null && packingListForm.getPackingList().getShipped() == null ) {
				packingListForm.getPackingList().setShipped(previousShipDate);
			}
			this.webJaguar.updatePackingList(packingListForm.getPackingList());			
		}

		return new ModelAndView(new RedirectView(getSuccessView()), "orderId", packingListForm.getPackingList().getOrderId());
	}

	public Object formBackingObject(HttpServletRequest request) throws Exception {
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (request.getParameter("__walkin") != null && request.getParameter("__walkin").equals("true")){
			request.getSession().setAttribute("__walkin", "true");
		}else{
			request.getSession().setAttribute("__walkin", "false");
		}
		PackingListForm packingListForm = new PackingListForm();
		int orderId = ServletRequestUtils.getIntParameter(request, "orderId", -1);
		if (orderId != -1) {
			Order order = this.webJaguar.getOrder(new Integer(request.getParameter("orderId")), "");
			if (request.getParameter("packing_num") == null && order.isFulfilled() && request.getParameter("force_generate") == null) {
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("packingList.jhtm"), "orderId", orderId));
			}
			packingListForm.setOrder(order);
			
			if (request.getParameter("packing_num") != null) {
				packingListForm.setPackingList(this.webJaguar.getPackingList(orderId, request.getParameter("packing_num")));
				packingListForm.setNewPackingList(false);
			} else {
				PackingList pl =new PackingList();
				pl.setLineItems(order.getLineItems());
				packingListForm.setPackingList(pl);
				packingListForm.getPackingList().setPackingListNumber(orderId + "-" + this.webJaguar.packingListCount(orderId));
			}
			
			// send email
			packingListForm.setEmailMessageForm(new EmailMessageForm());
			if (order != null) {
				Customer customer = this.webJaguar.getCustomerById(order.getUserId());
				packingListForm.getEmailMessageForm().setTo(customer.getUsername());
				packingListForm.getEmailMessageForm().setCc(customer.getExtraEmail());
			}
			packingListForm.getEmailMessageForm().setFrom(siteConfig.get("CONTACT_EMAIL").getValue());
			packingListForm.getEmailMessageForm().setBcc(siteConfig.get("CONTACT_EMAIL").getValue());
			packingListForm.getPackingList().setOrderId(orderId);
		}
		return packingListForm;
	}
		
	private double updatePackingListLineItems(HttpServletRequest request, PackingListForm packingListForm) throws ServletRequestBindingException {
		double subTotal = 0;
		
		for (LineItem lineItem : packingListForm.getPackingList().getLineItems()) {
			// get quantity
			if (packingListForm.isNewPackingList()) {
				lineItem.setQuantity(ServletRequestUtils.getIntParameter(request, "__quantity_" + lineItem.getLineNumber(), lineItem.getQuantity()));
				lineItem.setToBeShipQty(ServletRequestUtils.getIntParameter(request, "__quantity_" + lineItem.getLineNumber(), 0));
				//lineItem.setToBeShipQty(lineItem.getQuantity()-lineItem.getProcessed());
			} else {
				lineItem.setToBeShipQty(lineItem.getQuantity());
			}
		}
		
		
		// to update order.lineItems toBeShipQty
		for (LineItem lineItem : packingListForm.getOrder().getLineItems()) {
			if (packingListForm.isNewPackingList()) {
				int qty = ServletRequestUtils.getIntParameter(request, "__quantity_" + lineItem.getLineNumber(), lineItem.getQuantity());
				lineItem.setToBeShipQty(ServletRequestUtils.getIntParameter(request, "__quantity_" + lineItem.getLineNumber(), lineItem.getQuantity()- lineItem.getProcessed()));
				//lineItem.setToBeShipQty(qty-lineItem.getProcessed());
			}else {
				//lineItem.setToBeShipQty(lineItem.getProcessed());
			}
			if (lineItem.getOriginalPrice() != null) {
				subTotal = lineItem.getPackingListTotalPrice() + subTotal;
			}
		}
		
		return subTotal;
	}

	private void constructMessage(MimeMessageHelper helper, PackingListForm form, HttpServletRequest request, ApplicationContext context)
			throws Exception {
		
		// to remove the qtyToBeShip is it is 0
		Iterator<LineItem> lineItemsIterator = form.getOrder().getLineItems().iterator();
		while (lineItemsIterator.hasNext()) {
			LineItem lineItem = lineItemsIterator.next();
			if (lineItem.getToBeShipQty() == 0) {
				lineItemsIterator.remove();
			}
		}
		
		SalesRep salesRep = null;
		if ((Boolean) gSiteConfig.get("gSALES_REP") && form.getOrder().getSalesRepId() != null) {
			salesRep = this.webJaguar.getSalesRepById(form.getOrder().getSalesRepId());
		}

		// to emails
		String[] emails = form.getEmailMessageForm().getTo().split("[,]"); // split by commas
		for (int x = 0; x < emails.length; x++) {
			if (emails[x].trim().length() > 0) {
				helper.addTo(emails[x].trim());
			}
		}
		// cc emails
		emails = form.getEmailMessageForm().getCc().split("[,]"); // split by commas
		for (int x = 0; x < emails.length; x++) {
			if (emails[x].trim().length() > 0) {
				helper.addCc(emails[x].trim());
			}
		}
		helper.setFrom(form.getEmailMessageForm().getFrom());
		helper.setBcc(form.getEmailMessageForm().getBcc());

		Customer customer = this.webJaguar.getCustomerById(form.getOrder().getUserId());
		
		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(form.getOrder().getHost());
		MultiStore multiStore = null;
		if (multiStores.size() > 0) {
			multiStore = multiStores.get(0);
		}

		String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));

		StringBuffer message = new StringBuffer();
		message.append(dateFormatter.format(new Date()) + "<br /><br />");

		String firstName = form.getOrder().getBilling().getFirstName();
		if (firstName == null) {
			firstName = "";
		}
		String lastname = form.getOrder().getBilling().getLastName();
		if (lastname == null) {
			lastname = "";
		}
		String email = "";
		if (customer != null) {
			email = customer.getUsername();
		}

		// subject
		form.getEmailMessageForm().setSubject(form.getEmailMessageForm().getSubject().replace("#firstname#", firstName));
		form.getEmailMessageForm().setSubject(form.getEmailMessageForm().getSubject().replace("#lastname#", lastname));
		form.getEmailMessageForm().setSubject(form.getEmailMessageForm().getSubject().replace("#email#", email));
		form.getEmailMessageForm().setSubject(
				form.getEmailMessageForm().getSubject().replace("#packinglistnumber#", form.getPackingList().getPackingListNumber()));
		form.getEmailMessageForm().setSubject(
				form.getEmailMessageForm().getSubject().replace("#order#", form.getOrder().getOrderId().toString()));
		// XXX would be replaced by OrderID-UserID Base64-encoded.
		String orig = form.getOrder().getOrderId() + "-" + form.getOrder().getUserId();
		byte[] encode_orig = Base64.encodeBase64(orig.getBytes());         
		orig = new String(encode_orig);
		if(form.getOrder().getShipping().getStateProvince().equals("CA")) {
			//If user is in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&walkin=true
			//If user is not in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&other=true
			form.getEmailMessageForm().setSubject(
					form.getEmailMessageForm().getSubject().replace("#orderlink#", secureUrl + "dv/inv/invoice.php?order_id=" + orig + "=&walkin=true"));
		} else {
			form.getEmailMessageForm().setSubject(
					form.getEmailMessageForm().getSubject().replace("#orderlink#", secureUrl + "dv/inv/invoice.php?order_id=" + orig +"=&other=true"));
		}
		
		form.getEmailMessageForm().setSubject(
				form.getEmailMessageForm().getSubject().replace("#status#",
						getMessageSourceAccessor().getMessage("orderStatus_" + form.getOrder().getStatus())));
		if (salesRep != null) {
			form.getEmailMessageForm().setSubject(form.getEmailMessageForm().getSubject().replace("#salesRepName#", salesRep.getName()));
			form.getEmailMessageForm().setSubject(form.getEmailMessageForm().getSubject().replace("#salesRepEmail#", salesRep.getEmail()));
			form.getEmailMessageForm().setSubject(
					form.getEmailMessageForm().getSubject().replace("#salesRepPhone#",
							(salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			form.getEmailMessageForm().setSubject(
					form.getEmailMessageForm().getSubject().replace("#salesRepCellPhone#",
							(salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		helper.setSubject(form.getEmailMessageForm().getSubject());

		// message
		form.getEmailMessageForm().setMessage(form.getEmailMessageForm().getMessage().replace("#firstname#", firstName));
		form.getEmailMessageForm().setMessage(form.getEmailMessageForm().getMessage().replace("#lastname#", lastname));
		form.getEmailMessageForm().setMessage(form.getEmailMessageForm().getMessage().replace("#email#", email));
		form.getEmailMessageForm().setMessage(
				form.getEmailMessageForm().getMessage().replace("#packinglistnumber#", form.getPackingList().getPackingListNumber()));
		form.getEmailMessageForm().setMessage(
				form.getEmailMessageForm().getMessage().replace("#order#", form.getOrder().getOrderId().toString()));
		if(form.getOrder().getShipping().getStateProvince().equals("CA")) {
			//If user is in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&walkin=true
			//If user is not in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&other=true
			form.getEmailMessageForm().setMessage(
					form.getEmailMessageForm().getMessage().replace("#orderlink#", secureUrl + "dv/inv/invoice.php?order_id=" + orig + "=&walkin=true"));
		} else {
			form.getEmailMessageForm().setMessage(
					form.getEmailMessageForm().getMessage().replace("#orderlink#", secureUrl + "dv/inv/invoice.php?order_id=" + orig +"=&other=true"));
		}
		form.getEmailMessageForm().setMessage(
				form.getEmailMessageForm().getMessage().replace("#status#",
						getMessageSourceAccessor().getMessage("orderStatus_" + form.getOrder().getStatus())));
		if (salesRep != null) {
			form.getEmailMessageForm().setMessage(form.getEmailMessageForm().getMessage().replace("#salesRepName#", salesRep.getName()));
			form.getEmailMessageForm().setMessage(form.getEmailMessageForm().getMessage().replace("#salesRepEmail#", salesRep.getEmail()));
			form.getEmailMessageForm().setMessage(
					form.getEmailMessageForm().getMessage().replace("#salesRepPhone#",
							(salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			form.getEmailMessageForm().setMessage(
					form.getEmailMessageForm().getMessage().replace("#salesRepCellPhone#",
							(salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}

		// html
		form.getEmailMessageForm().setMessage(
				form.getEmailMessageForm().getMessage().concat(this.webJaguar.generateHtmlLineItem(form.getOrder(), request, context)));

		message.append(form.getEmailMessageForm().getMessage());
		helper.setText(message.toString(), true);
	}
}
