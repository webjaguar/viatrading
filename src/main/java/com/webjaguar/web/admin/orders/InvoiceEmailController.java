/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.28.2007
 */

package com.webjaguar.web.admin.orders;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.thirdparty.echosign.EchoSign;
import com.webjaguar.thirdparty.echosign.EchoSignApi;
import com.webjaguar.web.form.EmailMessageForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.SalesRep;

public class InvoiceEmailController extends SimpleFormController {

	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	public InvoiceEmailController() {
		setSessionForm(true);
		setCommandName("form");
		setCommandClass(EmailMessageForm.class);
		setFormView("admin/orders/email");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {

		EmailMessageForm form = (EmailMessageForm) command;
		
		Map<String, Object> model = new HashMap<String, Object>();
		String message = "";
		
        // email invoice
		if (request.getParameter("__invoice") != null) {
			return new ModelAndView(new RedirectView("invoice.jhtm?order=" + form.getOrder().getOrderId()));
		}
		
		// construct email message
		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File pdfFile = new File(tempFolder, "order_" + form.getOrder().getOrderId() + ".pdf");
       	MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
		try {
			constructMessage(helper, form, request);
			// attach pdf invoice
			if (((Boolean) gSiteConfig.get("gPDF_INVOICE") && siteConfig.get( "ATTACHE_PDF_ON_PLACE_ORDER" ).getValue().equals( "true" ))
					&& this.webJaguar.createPdfInvoice(pdfFile, form.getOrder().getOrderId(), request)) {
				helper.addAttachment("order_" + form.getOrder().getOrderId() + ".pdf", pdfFile);
				mailSender.send(mms);
				message = message + " " +getMessageSourceAccessor().getMessage("email.sent");
				model.put("message", message);
			} else {
				message = message + " " +getMessageSourceAccessor().getMessage("pdffile.create.exception");
				model.put("message", message);
			}
		} catch (Exception e) {
			message = message + " " +getMessageSourceAccessor().getMessage("email.exception");
			model.put("message", message);
		}
		
		if(!siteConfig.get("ECHOSIGN_API_KEY").getValue().isEmpty() && !siteConfig.get("ECHOSIGN_USER_KEY").getValue().isEmpty()) {
			EchoSign echoSign = new EchoSign();
			echoSign.setDocumentName(helper.getMimeMessage().getSubject().replace(" ", "_"));			
			File baseFile = new File(getServletContext().getRealPath("/assets/EchoSign/"));
			Properties prop = new Properties();
			try
			{
				prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
				if (prop.get("site.root") != null)
				{
					baseFile = new File((String) prop.get("site.root") + "/assets/EchoSign/");
				}
			}
			catch (Exception e)
			{}
			//File echoPdfFile = new File( getServletContext().getRealPath( "/assets/EchoSign/" + echoSign.getDocumentName() +".pdf") );
			File echoPdfFile = new File(baseFile, echoSign.getDocumentName() +".pdf");
			if (echoPdfFile.exists()) {
				echoSign.setApiKey(siteConfig.get("ECHOSIGN_API_KEY").getValue().trim());
				echoSign.setUserKey(siteConfig.get("ECHOSIGN_USER_KEY").getValue().trim());	
				echoSign.setUserId(ServletRequestUtils.getIntParameter(request, "cid", -1));
		    	Customer customer = this.webJaguar.getCustomerById(form.getOrder().getUserId());
		    	echoSign.setMessage("Please review and sign this document.");
				SalesRep salesRep = null;
				if (customer != null) {
					salesRep = this.webJaguar.getSalesRepById(customer.getSalesRepId());
				}
				echoSign.setMessage(this.webJaguar.replaceDynamicElement(echoSign.getMessage(), customer, salesRep));	    
				int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		    	request.setAttribute("echoSignSentMap", this.webJaguar.getEchoSignSentMap(currentYear));
				EchoSignApi echoSignApi = new EchoSignApi();
				echoSign.setRecipient(form.getTo());
				try {
					echoSign.setDocumentKey(echoSignApi.sendDocument(echoSign.getApiKey(), echoSign.getUserKey(), 
							echoPdfFile, echoSign.getDocumentName(), echoSign.getMessage(), echoSign.getRecipient(), ""));
					this.webJaguar.nonTransactionSafeInsertEchoSign(echoSign, false);
				} catch (Exception e) {
					message = message + " " +getMessageSourceAccessor().getMessage("email.echoSign.send.exception");
					model.put("message", message);
				}
			}
			else {
				message = message + " " +getMessageSourceAccessor().getMessage("email.echoSign.pdfFile.exception");
				model.put("message", message);
			}
		}		
		if(pdfFile.exists()){
			pdfFile.delete();
		}
		return showForm(request, response, errors, model);
	}	
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("__invoice") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		EmailMessageForm form = new EmailMessageForm();
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		
		Order order = this.webJaguar.getOrder(ServletRequestUtils.getIntParameter(request, "orderId", 0), siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());
		if (order != null) {
			Customer customer = this.webJaguar.getCustomerById(order.getUserId());
			form.setTo(customer.getUsername());
			form.setCc( customer.getExtraEmail() );
			form.setOrder(order);
		} else {
			request.setAttribute("message", "order.exception.notfound" );
			return form;
		}
		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
		MultiStore multiStore = null;
		if (multiStores.size() > 0) {
			multiStore = multiStores.get(0);
		}
		if (multiStore != null) {
			form.setFrom(multiStore.getContactEmail());
			form.setBcc(multiStore.getContactEmail());			
		} else {
			form.setFrom( siteConfig.get( "CONTACT_EMAIL" ).getValue() );
			form.setBcc( siteConfig.get( "CONTACT_EMAIL" ).getValue() );						
		}
			
		return form;
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("messages", this.webJaguar.getSiteMessageList());
		return map;
	}	
	
	private void constructMessage(MimeMessageHelper helper, EmailMessageForm form, HttpServletRequest request) throws Exception {
		SalesRep salesRep = null;
		if ( (Boolean) gSiteConfig.get( "gSALES_REP" ) && form.getOrder().getSalesRepId() != null ) {
			salesRep = this.webJaguar.getSalesRepById( form.getOrder().getSalesRepId() );
		}
		
		// to emails
		String[] emails = form.getTo().split( "[,]" ); // split by commas
		for ( int x = 0; x < emails.length; x++ ) {
			if (emails[x].trim().length() > 0) {
				helper.addTo(emails[x].trim());
			}
		}
		// cc emails
		emails = form.getCc().split( "[,]" ); // split by commas
		for ( int x = 0; x < emails.length; x++ ) {
			if (emails[x].trim().length() > 0) {
				helper.addCc(emails[x].trim());
			}
		}
		helper.setFrom(form.getFrom());	
		helper.setBcc(form.getBcc());
		
		Customer customer = this.webJaguar.getCustomerById( form.getOrder().getUserId() );
		
		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(form.getOrder().getHost());
		MultiStore multiStore = null;
		if (multiStores.size() > 0) {
			multiStore = multiStores.get(0);
		}

    	String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));

    	StringBuffer message = new StringBuffer();
    	if ( form.isHtml() ) {
    		message.append( dateFormatter.format(new Date()) + "<br /><br />" );
    	} else {
    		message.append( dateFormatter.format(new Date()) + "\n\n" );
    	}
    	
    	
    	// latest order statue
		OrderStatus orderStatus = this.webJaguar.getLatestStatusHistory(form.getOrder().getOrderId());
		orderStatus.setSubject(form.getSubject());
		orderStatus.setMessage(form.getMessage());
		
		// replace dyamic elements
    	this.webJaguar.replaceDynamicElement(orderStatus, customer, salesRep, form.getOrder(), secureUrl, null);
    	
    	helper.setSubject(orderStatus.getSubject());
    	if ( form.isHtml() ) {
    		message.append( orderStatus.getMessage() + "<br /><br />" );
    	} else {
    		message.append( orderStatus.getMessage() + "\n\n" );
    	}
    	
    	
    	helper.setText( message.toString(), form.isHtml() );
	}
}