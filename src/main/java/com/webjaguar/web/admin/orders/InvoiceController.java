/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.22.2006
 */

package com.webjaguar.web.admin.orders;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.activation.DataHandler;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import merchantAtAPIs.remote.DocumentProcessingInfo;

import org.apache.axis.attachments.AttachmentPart;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.buySafe.BuySafeAPI;
import com.buySafe.BuySafeAPISoap;
import com.buySafe.SetShoppingCartCancelOrderRQ;
import com.buySafe.SetShoppingCartCancelOrderRS;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.webjaguar.dao.StagingDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Affiliate;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Contact;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerBudgetPartner;
import com.webjaguar.model.CustomerCreditHistory;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.PackingList;
import com.webjaguar.model.PackingListSearch;
import com.webjaguar.model.Payment;
import com.webjaguar.model.PaymentSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.Promo;
import com.webjaguar.model.PurchaseOrder;
import com.webjaguar.model.PurchaseOrderLineItem;
import com.webjaguar.model.PurchaseOrderSearch;
import com.webjaguar.model.QualifierSearch;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.Search;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.State;
import com.webjaguar.model.Supplier;
import com.webjaguar.model.UpsOrder;
import com.webjaguar.model.WorkOrder;
import com.webjaguar.thirdparty.payment.amazon.AmazonApi;
import com.webjaguar.thirdparty.payment.authorizenet.AuthorizeNetApi;
import com.webjaguar.thirdparty.payment.eBizCharge.EBizChargePaymentApi;
import com.webjaguar.thirdparty.payment.ebillme.EBillme;
import com.webjaguar.thirdparty.payment.ebillme.EBillmeApi;
import com.webjaguar.thirdparty.payment.ezic.EzicApi;
import com.webjaguar.thirdparty.payment.paymentech.PaymentechApi;
import com.webjaguar.thirdparty.payment.payrover.PayRoverApi;
import com.webjaguar.thirdparty.payment.yourpay.YourPayApi;
import com.webjaguar.thirdparty.payment.paypalpro.PayPalProApi;
import com.webjaguar.web.admin.customer.CreditImp;
import com.webjaguar.web.admin.customer.PaymentImp;
import org.apache.commons.codec.binary.Base64;

public class InvoiceController extends AbstractCommandController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
    private StagingDao stagingDao;
	public void setStagingDao(StagingDao stagingDao) { this.stagingDao = stagingDao; }
	
	public InvoiceController() {
		setCommandClass(OrderStatus.class);
	}

    public ModelAndView handle(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {
		
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		Order order = null;
		Customer customer = null;
		
    	try {
    		int orderId = Integer.parseInt(request.getParameter("order"));
    		order = this.webJaguar.getOrder(orderId, siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());
    		if (order == null) {
          	  	model.put("message", "order.exception.notfound");  
          	  	return new ModelAndView("admin/orders/invoice", model);
    		}
    		//set tax on shipping
    		
    		if(order!=null){
    			System.out.println("SITE_TIME_ZONE: "+siteConfig.get("SITE_TIME_ZONE").getValue());
    			/* if(siteConfig.get("SITE_TIME_ZONE").getValue().equals("PST")) {
    				//servertime+3
    				if(order.getDateOrdered() != null) {
    					java.sql.Timestamp datePST = new java.sql.Timestamp(order.getDateOrdered().getTime() + (1000 * 60 * 60 * 3));
    					order.setDateOrdered(datePST);
    				}
    			} */
    			
    			if(siteConfig.get("SITE_TIME_ZONE").getValue().equals("PST")) {
	    			boolean changeTime = false;
	    			for (OrderStatus status : order.getStatusHistory()) {
						if(status.getStatus().equalsIgnoreCase("p") && status.getDateChanged().getHours() > order.getDateOrdered().getHours()) {
							changeTime = true;
							break;
						}
	    			}
	    			for (OrderStatus status : order.getStatusHistory()) {
	    				java.sql.Timestamp newDate = new java.sql.Timestamp(status.getDateChanged().getTime() - (1000 * 60 * 60 * 3));
						if(changeTime)
							status.setDateChanged(newDate);
					} 
    			}
    			
    			String quote = this.webJaguar.getOrderQuote(order.getOrderId());
    			if(quote!=null){
        			model.put("quote", quote);
    			}
    		} 
    		
    		State state = this.webJaguar.getStateByCode(order.getShipping().getCountry(), order.getShipping().getStateProvince());
    		if (state != null) {
    			order.setTaxOnShipping(state.isApplyShippingTax());
    		}
    		
    		List<ShippingRate> customeShippingList= this.webJaguar.getCustomShippingRateList(false,true);
    		model.put("customeShippingList", customeShippingList);  
    		
    		if (order.getInvoiceNote() != null) {
        		order.setInvoiceNote(order.getInvoiceNote().replace("\n", "<br>"));
    		}
    		model.put("order", order);
    		if ((Boolean) gSiteConfig.get( "gSEARCH_ENGINE_PROSPECT" ) && order.getProspectId() != null) {
    			model.put("searchEngineProspect", this.webJaguar.getNewestProspectInformation(order.getProspectId()));
    		}
			model.put("productFieldsHeader", this.webJaguar.getProductFieldsHeader( order, (Integer) gSiteConfig.get("gPRODUCT_FIELDS") )); 		
			order.setupDueDate( order.getTurnOverday(), null );
			customer = this.webJaguar.getCustomerById( order.getUserId() );
    		List<CustomerField> customerFields = new ArrayList<CustomerField>();
    		// customer fields
			Class<Customer> c1 = Customer.class;
			Method m1 = null;
			Object arglist1[] = null;
			if ( customer != null ) {
				for ( CustomerField customerField : this.webJaguar.getCustomerFields() )
				{
					if ( customerField.isEnabled() && customerField.isShowOnInvoice() )
					{
						m1 = c1.getMethod( "getField" + customerField.getId() );
						customerField.setValue( (String) m1.invoke( customer, arglist1 ) );
						if ( customerField.getValue() != null && customerField.getValue().length() > 0 ) {
							customerFields.add( customerField );
						}
					}
				}
			}
			
			//BUDGET
			// If creditAllowed is empty get value from global
			if((Boolean) gSiteConfig.get("gBUDGET") && customer.getCreditAllowed() ==  null && siteConfig.get("BUDGET_PERCENTAGE").getValue() != null && !(siteConfig.get("BUDGET_PERCENTAGE").getValue().isEmpty())) {	
				customer.setCreditAllowed(Double.parseDouble(siteConfig.get("BUDGET_PERCENTAGE").getValue()));
			}
			// Budget Earned Credits
			if( (Boolean) gSiteConfig.get("gBUDGET") && siteConfig.get("BUDGET_ADD_PARTNER").getValue().equalsIgnoreCase("true")) {
				CustomerBudgetPartner partner = new CustomerBudgetPartner();
				partner.setOrderId(order.getOrderId());
				partner.setUserId(customer.getId());
				List<CustomerBudgetPartner> partnerList = this.webJaguar.getCustomerPartnerHistory(partner);
				if(partnerList != null && !partnerList.isEmpty()) {
					model.put("partnerList", partnerList);
				}
				Double value = null;
				if(order.getBudgetEarnedCredits() != null && customer.getBudgetPlan() != null && customer.getBudgetPlan().equalsIgnoreCase("plan-b")){				
					value= order.getSubTotal() - order.getBudgetEarnedCredits();
				} else {
					value= order.getSubTotal();
				}
				model.put("subTotalByPlan", value);
			}
			model.put("customerFields", customerFields);
    		model.put("customer", customer);
    		model.put("customerOrderInfo", this.webJaguar.getCustomerOrderReportByUserID(customer.getId()));
    		if (order.getWorkOrderNum() != null) {
    			WorkOrder workOrder = this.webJaguar.getWorkOrder(order.getWorkOrderNum());
    			model.put("product", this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(workOrder.getService().getItem().getSku()), 0, false, null));
        		model.put("workOrder", workOrder);    			
    		}
        } catch (NumberFormatException e) {
      	  	model.put("message", "order.exception.notfound");    	
      	  	return new ModelAndView("admin/orders/invoice", model);
    	}

		String gCreditCardPayment = gSiteConfig.get( "gCREDIT_CARD_PAYMENT" ).toString();

		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
		MultiStore multiStore = null;
		if (multiStores.size() > 0) {
			multiStore = multiStores.get(0);
			model.put("multiStore", multiStore);
		}
		if((Boolean) gSiteConfig.get("gPAYMENTS") && order.isPaymentAlert()) {
     	  	model.put("message", "paymentAlert");   
		}
		
		if((Boolean) gSiteConfig.get("gPAYMENTS") && order.isPaymentAlert()) {
      	  	model.put("message", "paymentAlert");   
		}
		
		// apply customer credit and  update order payment
		if(request.getParameter("applyCredit") != null && request.getParameter("applyCredit").equals("true")) {
			
			// update customer credit && insert a payment
			if(order.getAmountPaid() == null) {
				order.setAmountPaid(0.00);
			}
			Double amount = 0.00;
			
			if(request.getParameter("creditAmount") != null && request.getParameter("creditAmount") !="" ) {
				amount = Double.valueOf(request.getParameter("creditAmount"));	
			}
			
			if(amount > 0 && amount <= customer.getCredit() && amount <= order.getBalance()) {
				CreditImp credit = new CreditImp();
				CustomerCreditHistory customerCredit = new CustomerCreditHistory();
				
				PaymentImp customerPayment = new PaymentImp();	
				List<Order> orders = new ArrayList<Order>();
				orders.add(order);
				
				
				Payment payment = new Payment();
				customerCredit= credit.updateCustomerCreditByOrderPayment(order, "payment", amount, this.webJaguar.getAccessUser(request).getUsername());		
				payment=customerPayment.createPayemnts(customer, "VBA Credit", amount, "Customer used VBA Credit");				
				
				for(Order orderPayment: orders) {
					orderPayment.setPayment(amount);
					// Set credit used based on the amount.
					if(orderPayment.getCreditUsed() != null) {
						orderPayment.setCreditUsed(orderPayment.getCreditUsed() + amount);
					} else {
						orderPayment.setCreditUsed(amount);
					}
				}
				
				this.webJaguar.updateCredit(customerCredit);
				this.webJaguar.insertCustomerPayment( payment, orders);
			}
			return new ModelAndView(new RedirectView("invoice.jhtm"), "order", order.getOrderId());
		}

		// update the status
		if (request.getParameter("__status") != null) {
			OrderStatus orderStatus = (OrderStatus) command;
			
			// checking shipping title whether is valid custom shipping or not while updating status to shipped
			//if(orderStatus.getStatus() != null && orderStatus.getStatus().equals("s") && !checkShippingTitle(order.getShippingMethod())){
			//	return new ModelAndView(new RedirectView("invoice.jhtm?shippingStatus=Shipping title is not valid"), "order", order.getOrderId());
			//}
			
    		// amazon checkout
			if (order.getStatus().equals("ars")) {
	    		if (orderStatus.getStatus().equals("xaf")) {
	    			amazonCancelOrder(order, orderStatus, siteConfig);
	    		} else if (orderStatus.getStatus().equals("s")) {
	    			File errorXml = amazonShipOrder(order, orderStatus, siteConfig);
					if (errorXml != null) {
	    				// with error
	        			return new ModelAndView(new RedirectView("../../amazonCheckout/postDocument/" + errorXml.getName()));    					
					}
	    		}			
			}
			
			if (orderStatus.getStatus().equals("x") && (orderStatus.getCancelReason() == null || orderStatus.getCancelReason().length() <= 0)) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cancelReason", "payment.cancelReason");
				return new ModelAndView(new RedirectView("invoice.jhtm"), "order", order.getOrderId());
			}
			
			orderStatus.setUsername(this.webJaguar.getAccessUser(request).getUsername());
			if (orderStatus.getTrackNum() != null && orderStatus.getTrackNum().trim().length() == 0) {
				orderStatus.setTrackNum(null);
			}
			
			Contact customContact = null;
			if (siteConfig.get( "CUSTOM_SHIPPING_CONTACT_INFO" ).getValue().equals("true") && ServletRequestUtils.getIntParameter(request, "contact", 0) != 0) {
				int contactId = ServletRequestUtils.getIntParameter(request, "contact", 0);
				
				customContact = this.webJaguar.getCustomShippingContactById(contactId);
				orderStatus.setCarrier(customContact.getCompany());
			}

    		if (orderStatus.isUserNotified()) {
    			SalesRep salesRep = null;
    			if ( (Boolean) gSiteConfig.get( "gSALES_REP" ) && order.getSalesRepId() != null ) {
    				salesRep = this.webJaguar.getSalesRepById( order.getSalesRepId() );
    			}
    			    			
				
    			if (siteConfig.get( "CUSTOM_SHIPPING_CONTACT_INFO" ).getValue().equals("true") && ServletRequestUtils.getIntParameter(request, "contact", 0) != 0) {
    				int contactId = ServletRequestUtils.getIntParameter(request, "contact", 0);
    				
    				customContact = this.webJaguar.getCustomShippingContactById(contactId);
    				orderStatus.setCarrier(customContact.getCompany());
    			}else if (siteConfig.get( "CUSTOM_SHIPPING_CONTACT_INFO" ).getValue().equals("true") && ServletRequestUtils.getIntParameter(request, "contact", 0) == 0){
    				Contact contact = this.webJaguar.getCompanyContactId(orderStatus.getCarrier());
    				
    				if(contact!=null){
        				
    					customContact = this.webJaguar.getCustomShippingContactById(contact.getId());
    				}
    			}

    			// send email
    	    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	    	String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
    			
    	    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));

    	    	StringBuffer message = new StringBuffer();
    	    	if ( orderStatus.isHtmlMessage() ) {
    	    		message.append( dateFormatter.format(new Date()) + "<br /><br />" );
    	    	} else {
    	    		message.append( dateFormatter.format(new Date()) + "\n\n" );
    	    	}
    	    	
    	    	// replace dynamic elements
    			this.webJaguar.replaceDynamicElement( orderStatus, customer, salesRep, order, secureUrl, customContact);
    	    	message.append( orderStatus.getMessage() );
    	    	
    			File tempFolder = new File(getServletContext().getRealPath("temp"));
				if (!tempFolder.exists()) {
					tempFolder.mkdir();
				}
				File pdfFile = new File(tempFolder, "order_" + order.getOrderId() + ".pdf");
    			try {
        	    	// construct email message
        	       	MimeMessage mms = mailSender.createMimeMessage();
        			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
        			helper.setTo(customer.getUsername());
        			helper.setFrom(orderStatus.getFrom());
        			helper.setBcc(contactEmail);
        			// cc emails
        			String[] emails = orderStatus.getCc().split( "[,]" ); // split by commas
        			for ( int x = 0; x < emails.length; x++ ) {
        				if (emails[x].trim().length() > 0) {
        					helper.addCc(emails[x].trim());
        				}
        			}
        			helper.setSubject( orderStatus.getSubject() );
        	    	helper.setText( message.toString(), orderStatus.isHtmlMessage() );

    				if (request.getParameter("__attach") != null && (Boolean) gSiteConfig.get("gPDF_INVOICE")) {
    					// attach pdf invoice
    					if (((Boolean) gSiteConfig.get("gPDF_INVOICE") && siteConfig.get( "ATTACHE_PDF_ON_PLACE_ORDER" ).getValue().equals( "true" ))
    							&& this.webJaguar.createPdfInvoice(pdfFile, order.getOrderId(), request)) {
        					helper.addAttachment("order_" + order.getOrderId() + ".pdf", pdfFile);    						
    					}
    				}
    				mailSender.send(mms);
    			} catch (Exception ex) {
    				ex.printStackTrace();
    				orderStatus.setUserNotified( false );
    			}    			
    			if (pdfFile.exists()) {
    				pdfFile.delete();
    			}
    		}
    		
    		// update following only if status is different from the old status.
    		if( !(order.getStatus().equalsIgnoreCase(orderStatus.getStatus())) ) {    			
    			// cancelled order
    			if (orderStatus.isOrderCancel()) {    				
    				// inventory
    				if ((Boolean) gSiteConfig.get("gINVENTORY")) {
    					// Delete packing list
        				List<String> listOfPacking= this.webJaguar.getPackisListNumByOrderId(order.getOrderId());
        				if(listOfPacking != null) {
        					for (String packing : listOfPacking) {
        						this.webJaguar.deletePackingList(order.getOrderId(), packing, order.getAccessUserId(), siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"));
        					}
        				}
        				Inventory inventory = new Inventory();
        				for (LineItem lineItem : order.getLineItems()) {
    						inventory.setInventoryAFS(lineItem.getQuantity());
        					inventory.setSku(lineItem.getProduct().getSku());
        					this.webJaguar.updateInventory(inventory);    	
        				}
    				}
    				// customer credit
    				if (order.getCreditUsed() != null && order.getCreditUsed() > 0) {
    	    			CreditImp credit = new CreditImp();
    	    			CustomerCreditHistory customerCredit = credit.updateCustomerCreditByOrderPayment(order, "cancel", order.getCreditUsed(), orderStatus.getUsername());
    	    			this.webJaguar.updateCredit(customerCredit);				
    				}
    				// buy safe
    	    		if( order.getBondCost() != null && !siteConfig.get("BUY_SAFE_URL").getValue().equals("")) {
    	    			this.buySafeCancelOrder(order, siteConfig);
    	    		}    	    		
    			}     			
    			// shipped order
    			if (orderStatus.getStatus().equals( "s" )) {    				
    	    		// adds ship date to packinglist without shipdate and adjusts on hand
        			this.webJaguar.ensurePackingListShipDate(order, siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"), ((AccessUser) request.getAttribute( "accessUser" )).getId());
            		// Order status changed to ship 
    				if ( !order.isProcessed() ) { 
    					this.webJaguar.generatePackingList(order, false, new Date(), siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"));    					
    				}
    				
    				if(order.isProcessed()){
    					List<PurchaseOrderLineItem> qtyList = this.webJaguar.getPOLineItemsQuantity(order.getOrderId(), true);
    					for(PurchaseOrderLineItem lineItem: qtyList){
    			    		Inventory inventoryAFS = new Inventory();
    			    		inventoryAFS.setSku(lineItem.getSku());
    			    		inventoryAFS.setInventoryAFS(lineItem.getQty());
    			    		this.webJaguar.updateInventory(inventoryAFS);
			    		}
    				}
    				
    				// Loyalty
    				if ((Boolean) gSiteConfig.get("gLOYALTY") ) {
    	    			calculateUserPoints(order, customer, orderStatus, siteConfig.get( "POINT_TO_CREDIT" ).getValue().equals( "true" ));    					
    				} 
    			
	    			// if order is shipped send email to vendor about product.
	    			if ( (Boolean) gSiteConfig.get("gCUSTOMER_SUPPLIER")) {
	    				SiteMessage supplierSiteMessage = null;
	    				try {
	    					supplierSiteMessage = this.webJaguar.getSiteMessageById(Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_SUPPLIER_ORDERS").getValue()));
	    				} catch (NumberFormatException e) { }
	    				if (supplierSiteMessage != null) {
	    						    					  						    					    						
		    				notifySuppliers( supplierSiteMessage, request, order, siteConfig, multiStore);		    							    					
	    				}
	    			}
    			}
    		}
    		if (((Boolean) gSiteConfig.get("gWORLDSHIP") ||(Boolean) gSiteConfig.get("gFEDEXSHIPMANAAGER")) && orderStatus.getStatus().equals( "pr" ) ) {
    			if ( order.getPromo() == null )
        			order.setPromo( new Promo() );
        		if ( order.getAffiliate() == null )
        			order.setAffiliate( new Affiliate() );
    			this.stagingDao.insertStagingOrder( order );
    		}
    		
    		
    		/*
    		 * NOT in use for now ebridge

    		if ((Boolean) gSiteConfig.get("gEBRIDGE") && orderStatus.getStatus().equals( "pr" ) ) {
    			System.out.println("eBridge");
    			EBridgeConnector eBridgeConnector = new EBridgeConnector();
    			boolean reply = eBridgeConnector.sendFile(order, "WebJa88871", "85051");
    			
    			if (reply){
    				System.out.println(reply);
    				// do more
    			} else {
    				System.out.println(reply);
    				// do more
    			}
    		}
    		*/

			this.webJaguar.insertOrderStatus(orderStatus);  
			this.webJaguar.updateOrderQuote(order.getOrderId(), request.getParameter("quote"));
			
			if(siteConfig.get("WORLD_SHIP_IMPORT_ORDERS").getValue().equals("true")) {
				UpsOrder upsWorldShip = new UpsOrder();
				upsWorldShip.setTrackingNumber(orderStatus.getTrackNum());
				upsWorldShip.setOrderId(orderStatus.getOrderId());				
				OrderStatus orderStatus1 = this.webJaguar.getLatestStatusHistory(orderStatus.getOrderId());
				upsWorldShip.setOrderShipDate(orderStatus1.getDateChanged());
				this.webJaguar.updateUpsOrdersTrackAndShipDate(upsWorldShip);
			}
			
			// we don't want cancelled order to be treated as first order
			System.out.println("status:: "+orderStatus.getStatus()+" isfirstorder:: "+order.isFirstOrder());
			if (orderStatus.getStatus().equals("x") && order.isFirstOrder()) {
				order.setFirstOrder(false);
				System.out.println("in if order.isFirstOrder()::>> "+order.isFirstOrder());
				this.webJaguar.updateIsFirstOrder(order);
			}
			return new ModelAndView(new RedirectView("invoice.jhtm"), "order", order.getOrderId());
		} else {
			OrderStatus orderStatus = new OrderStatus();
			orderStatus.setOrderId( order.getOrderId() );
			orderStatus.setStatus( order.getStatus() );
			orderStatus.setSubStatus(((OrderStatus) order.getStatusHistory().get(order.getStatusHistory().size()-1)).getSubStatus());
			orderStatus.setCancelReason(((OrderStatus) order.getStatusHistory().get(order.getStatusHistory().size()-1)).getCancelReason());
			if (customer != null) {
				orderStatus.setCc(customer.getExtraEmail());				
			}
			SalesRep salesRep = null;
			if ( (Boolean) gSiteConfig.get( "gSALES_REP" ) && order.getSalesRepId() != null ) {
				salesRep = this.webJaguar.getSalesRepById( order.getSalesRepId() );
			}
			if ( salesRep != null ) {
				if (orderStatus.getCc() == null || orderStatus.getCc().trim().equals("")) {
					orderStatus.setCc(salesRep.getEmail());
				} else {
					orderStatus.setCc(orderStatus.getCc() + ", " + salesRep.getEmail());
				}
			}
			if (siteConfig.get("CUSTOMIZE_ORDER_LIST").getValue().contains("priority")) {
				orderStatus.setPriority(order.getPriority());
			}
			if (siteConfig.get("CUSTOMIZE_ORDER_LIST").getValue().contains("deliveryPerson")) {
				orderStatus.setDeliveryPerson(order.getDeliveryPerson());
			}
			// set Email from
	    	String from = siteConfig.get("CONTACT_EMAIL").getValue();
			if (multiStore != null) {
				from = multiStore.getContactEmail();
			}
			orderStatus.setFrom(from);
			model.put( "orderStatus", orderStatus);
		}
		
		// capture the transaction
		if ((request.getParameter("__charge") != null || request.getParameter("__charge_new") != null || request.getParameter("__auth_only") != null) && !gCreditCardPayment.equals( "" )) {
			
			String paymentStatus = order.getCreditCard().getPaymentStatus();
			
			// check status 
			if (paymentStatus != null && (paymentStatus.equalsIgnoreCase( "CAPTURED" ) || (paymentStatus.equalsIgnoreCase( "sale" )) )) {
	      	  	model.put("chargeMessage", "creditcard.exception.capturedalready");  
			} else {

				boolean authorizeOnly = request.getParameter("__auth_only") != null; // authorize or capture
				
				// Authorize.Net
				if (gCreditCardPayment.equals( "authorizenet" ) || gCreditCardPayment.equals( "ebizcharge" )) {
					
					boolean priorAuth = request.getParameter("__charge") != null; // prior charge
					try {
						if (gCreditCardPayment.equals( "ebizcharge" ) && siteConfig.get("PCI_FRIENDLY_EBIZCHARGE").getValue().equalsIgnoreCase("true")) {
							order.getBilling().setEmail(customer.getUsername());
							order.getShipping().setEmail(customer.getUsername());
							EBizChargePaymentApi eBizChargePayment = new EBizChargePaymentApi(siteConfig, order.getIpAddress());
							if(customer.getGatewayToken() != null && (order.getCcToken() != null && !order.getCcToken().isEmpty() && !order.getCcToken().equalsIgnoreCase("-1") && !order.getCcToken().equalsIgnoreCase("-2"))) {
								if(paymentStatus != null && paymentStatus.equalsIgnoreCase("AUTHORIZED")) {
									priorAuth = true;
								}else {
									priorAuth = false;
								}
								eBizChargePayment.CustomerBankCardTransactions(order, customer, authorizeOnly, priorAuth, siteConfig);
							} else {
								eBizChargePayment.BankCardTransactions(order,authorizeOnly ,priorAuth,siteConfig);
							}
						} else {
							AuthorizeNetApi authorizeNet = new AuthorizeNetApi();
							authorizeNet.authorize(order, authorizeOnly, priorAuth, siteConfig, gSiteConfig);	
						}
					} catch (Exception e) {
						order.getCreditCard().setPaymentNote( "Error" );
						e.printStackTrace();
					}
				}

				// YourPay
				if (gCreditCardPayment.equals( "yourpay" )) {
					
					try {
						YourPayApi yourpay = new YourPayApi();
						yourpay.process(order, authorizeOnly, siteConfig, getServletContext().getRealPath("/LinkPoint.p12"));					
					} catch (Exception e) {
						order.getCreditCard().setPaymentNote("Error");
					}
				}
				
				// Ezic
				if (gCreditCardPayment.equals("ezic")) {
					try {
						EzicApi ezic = new EzicApi();
						String custEmail = null;
						if (customer != null) {
							custEmail = customer.getUsername();
						}
						ezic.process(order, custEmail, authorizeOnly, siteConfig);					
					} catch (Exception e) {
						order.getCreditCard().setPaymentNote("Error");
					}
				}

				// PayRover
				if (gCreditCardPayment.equals("payrover")) {
					try {
						PayRoverApi payRover = new PayRoverApi();
						payRover.authorize(order, authorizeOnly, siteConfig);																		
					} catch (Exception e) {
						order.getCreditCard().setPaymentNote("Error");
					}
				}
				
				// Paymentech
				if (gCreditCardPayment.equals("paymentech")) {
					try {
						PaymentechApi paymentech = new PaymentechApi();
						paymentech.process(order, authorizeOnly, siteConfig);				
					} catch (Exception e) {
						order.getCreditCard().setPaymentNote("Error");
					}
				}
				
				//PayPalPro
				if (gCreditCardPayment.equals("paypalpro")){

					PayPalProApi payPalPro = new PayPalProApi();
					if (request.getParameter("__charge") != null) {
						siteConfig.get("CREDIT_AUTO_CHARGE").setValue("charge");
						payPalPro.DoDirectPayment(order, siteConfig, null);
					}  
					if (request.getParameter("__auth_only") != null) {
						siteConfig.get("CREDIT_AUTO_CHARGE").setValue("auth");
						payPalPro.DoDirectPayment(order, siteConfig, null);
					}
				}
				
				//add Payment
			    if ((Boolean) gSiteConfig.get("gPAYMENTS")) {
					if(order.getCreditCard().getPaymentStatus() != null && !order.getCreditCard().getPaymentStatus().isEmpty()){
						if(order.getCreditCard().getPaymentStatus().equalsIgnoreCase("sale") || order.getCreditCard().getPaymentStatus().equalsIgnoreCase("captured")){
							if(order.getPayment() == null){
								order.setPayment(0.00);							
							} else if(order.getCreditUsed() != null && order.getCreditUsed() > 0) { // to check if customer made partial payments with his credit. 
								order.setPayment(order.getCreditUsed());
							}
							PaymentImp customerPayment = new PaymentImp();	
							// insertCustomerPayment needs List<Order>
							List<Order> orders = new ArrayList<Order>();
							orders.add(order);
							Payment payment = new Payment();
							
							if(order.getCreditUsed() != null && order.getCreditUsed() > 0) { // if customer used his credit also, then deduct his credit amount form grand total and make the payment. 
								payment=customerPayment.createPayemnts(this.webJaguar.getCustomerById(order.getUserId()), "Back End", (order.getGrandTotal()-order.getCreditUsed()), "Auto generated: " +order.getOrderId()+"/Paid using: Credit Card"+"/TransactionId:"+order.getCreditCard().getTransId());
							} else {
								payment=customerPayment.createPayemnts(this.webJaguar.getCustomerById(order.getUserId()), "Back End", (order.getGrandTotal()-order.getPayment()), "Auto generated: " +order.getOrderId()+"/Paid using: Credit Card"+"/TransactionId:"+order.getCreditCard().getTransId());
							}
							
							for(Order orderPayment: orders) {
								if(orderPayment.getCreditUsed() != null && orderPayment.getCreditUsed() > 0){ // if customer already paid some amt with his credit, then deduct it form grand total and set payment. 
									orderPayment.setPayment(order.getGrandTotal()-orderPayment.getCreditUsed());
								} else {
									orderPayment.setPayment(order.getGrandTotal());
								}
							}
							
							this.webJaguar.insertCustomerPayment( payment, orders);
						}
					}
			    }
				
				this.webJaguar.updateCreditCardPayment(order);
				// redirect to show updated date
				return new ModelAndView(new RedirectView("invoice.jhtm"), "order", order.getOrderId());
			}
		}
		
        // post xml to macs
		if (request.getParameter("__macs") != null && gSiteConfig.get("gACCOUNTING").equals("MACS")) {
			sendToMACS(order, request, customer);
		}
		
        // post to evergreen
//		if (request.getParameter("__evergreen") != null && gSiteConfig.get("gACCOUNTING").equals("EVERGREEN")) {
//			boolean isAEM = false;
//			for (GrantedAuthority role: SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
//				if (role.getAuthority().equals("ROLE_AEM")) isAEM = true;
//			}
//			if (isAEM) {
//				sendToEvergreen(order, customer);
//			}
//		}
		
		// eBillme
		if (request.getParameter("__eBillme") != null && siteConfig.get("EBILLME_URL").getValue().trim().length() > 0
				&& siteConfig.get("EBILLME_MERCHANTTOKEN").getValue().trim().length() > 0) {
			try {
				EBillmeApi eBillMeApi = new EBillmeApi();
				EBillme trans = eBillMeApi.submitOrder(order, siteConfig, customer.getUsername(), request.getRemoteAddr());
				
				this.webJaguar.updateEbillmePayment(trans, null);				
				
				// redirect to show updated payment
				return new ModelAndView(new RedirectView("invoice.jhtm"), "order", order.getOrderId());
			} catch (Exception e) {
				model.put("eBillmeError", e.toString());
				e.printStackTrace();
			}
		}
		
		if (order.geteBillme() != null && order.geteBillme().getXmlFile() != null) {
			File eBillmeDir = new File(getServletContext().getRealPath("/eBillme/processed"));	
			SAXBuilder builder = new SAXBuilder(false);
	
			String xmlFile[] = order.geteBillme().getXmlFile().split(",");
			StringBuffer ebillmeXml = new StringBuffer();
			for (int xml=0; xml<xmlFile.length; xml++) {
				Document doc = builder.build(new File(eBillmeDir, xmlFile[xml]));
				Element paymentchanges = doc.getRootElement().getChild("paymentchanges", Namespace.getNamespace("http://ebillme.com/ws/v3"));
				Iterator orders = paymentchanges.getChildren().iterator();
				while (orders.hasNext()) {
					Element orderEle = (Element) orders.next();
					if (order.geteBillme().getOrderrefid().equals(orderEle.getChildText("orderrefid", Namespace.getNamespace("http://ebillme.com/ws/v3")))) {
						// matching orderrefid
	
						// show only on last payment
						if (xml == 0) {
							
							Double amountowing = Double.parseDouble(orderEle.getChildText("amountowing", Namespace.getNamespace("http://ebillme.com/ws/v3")));
							String paystatus = orderEle.getChildText("paystatus", Namespace.getNamespace("http://ebillme.com/ws/v3"));
							
							ebillmeXml.append("<tr>");
							ebillmeXml.append("<td valign=\"top\" align=\"right\">Pay Status:</td>");
							
							if (paystatus.equals("F") && amountowing < 0) {
								ebillmeXml.append("<td valign=\"top\" align=\"right\">Pay Status:</td>");
								ebillmeXml.append("<td><span style=\"color:#FF0000;font-weight:bold\">Overpayment</span> - Please note the customer has paid more than the order amount.<br/>You may wish to initiate a refund, provide a credit notice for the customer.</td>");						
							} else if (paystatus.equals("F") && amountowing == 0) {
								ebillmeXml.append("<td><span style=\"color:#FF0000;font-weight:bold\">Exact payment</span> - Please note the full payment has been made and you can fulfill the order.</td>");						
							} else if (paystatus.equals("P") && amountowing > 0) {
								ebillmeXml.append("<td><span style=\"color:#FF0000;font-weight:bold\">Underpayment</span> - Please note the full payment has not been received yet, please contact<br/>the customer and request they send the balance in order for them to receive their order.</td>");						
							} else {
								ebillmeXml.append("<td>" + paystatus + "</td>");						
							}
							ebillmeXml.append("</tr>");			
	
							ebillmeXml.append("<tr>");
							ebillmeXml.append("<td align=\"right\">Payment Date:</td>");
							ebillmeXml.append("<td>" + orderEle.getChildText("paymentdate", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
							ebillmeXml.append("</tr>");	
							
							ebillmeXml.append("<tr>");
							ebillmeXml.append("<td align=\"right\">Amount Paid to Date:</td>");
							ebillmeXml.append("<td>" + orderEle.getChildText("amountpaidtodate", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
							ebillmeXml.append("</tr>");
							
							if (amountowing != 0) {
								NumberFormat nf = new DecimalFormat("#0.00");
								ebillmeXml.append("<tr>");
								ebillmeXml.append("<td align=\"right\">Amount Owing:</td>");
								ebillmeXml.append("<td>" + nf.format(amountowing) + "</td>");
								ebillmeXml.append("</tr>");						
							}
							
							Element suspectdetails = orderEle.getChild("suspectdetails", Namespace.getNamespace("http://ebillme.com/ws/v3"));
							if (suspectdetails != null) {
								ebillmeXml.append("<tr>");
								ebillmeXml.append("<td align=\"right\">Suspect Status:</td>");
								ebillmeXml.append("<td>" + suspectdetails.getChildText("suspectstatus", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
								ebillmeXml.append("</tr>");	
								ebillmeXml.append("<tr>");
								ebillmeXml.append("<td align=\"right\">Suspect Reasons:</td>");
								ebillmeXml.append("<td>" + suspectdetails.getChild("suspectreasons", Namespace.getNamespace("http://ebillme.com/ws/v3")).getChildText("reasoncode", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
								ebillmeXml.append("</tr>");							
							}
							
							ebillmeXml.append("<tr>");
							ebillmeXml.append("<td align=\"right\" style=\"font-weight:bold\">Recent Payments</td>");				
							ebillmeXml.append("<td>&nbsp;</td>");
							ebillmeXml.append("</tr>");				
						} else if (xml == 1) {
							ebillmeXml.append("<tr>");
							ebillmeXml.append("<td align=\"right\" style=\"font-weight:bold\">Old Payments</td>");				
							ebillmeXml.append("<td>&nbsp;</td>");
							ebillmeXml.append("</tr>");									
						}
						
						Element payments = orderEle.getChild("payments", Namespace.getNamespace("http://ebillme.com/ws/v3"));
						Iterator paymentdetailsIter = payments.getChildren().iterator();
						while (paymentdetailsIter.hasNext()) {
							Element paymentdetails = (Element) paymentdetailsIter.next();
							ebillmeXml.append("<tr>");
							ebillmeXml.append("<td align=\"right\">Name:</td>");
							ebillmeXml.append("<td>" + paymentdetails.getChildText("name", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
							ebillmeXml.append("</tr>");				
							ebillmeXml.append("<tr>");
							ebillmeXml.append("<td align=\"right\">Amount Paid:</td>");
							ebillmeXml.append("<td>" + paymentdetails.getChildText("amountpaid", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
							ebillmeXml.append("</tr>");				
							ebillmeXml.append("<tr>");
							ebillmeXml.append("<td align=\"right\">Payment Date:</td>");
							ebillmeXml.append("<td>" + paymentdetails.getChildText("paymentdate", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
							ebillmeXml.append("</tr>");				
							ebillmeXml.append("<tr>");
							ebillmeXml.append("<td align=\"right\">Settlement Date:</td>");
							ebillmeXml.append("<td>" + paymentdetails.getChildText("settlementdate", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
							ebillmeXml.append("</tr>");				
							ebillmeXml.append("<tr>");
							ebillmeXml.append("<td align=\"right\">Payment Source:</td>");
							ebillmeXml.append("<td>" + paymentdetails.getChildText("paymentsource", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
							ebillmeXml.append("</tr>");								
							ebillmeXml.append("<td>&nbsp;</td>");
							ebillmeXml.append("<td>-------------------</td>");
							ebillmeXml.append("</tr>");
						}
					}
				}
			}
			model.put("ebillmeXml", ebillmeXml);
			
		}
		SalesRep salesRep = this.webJaguar.getSalesRepById( order.getSalesRepId());
		
		Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
		List<SalesRep> salesReps = null;
		salesReps = this.webJaguar.getSalesRepList();				
		for (SalesRep salesRepTemp: salesReps) {
			salesRepMap.put(salesRepTemp.getId(), salesRepTemp);				
		}
		
		QualifierSearch qualifierSearch = new QualifierSearch();
		qualifierSearch.setShowActiveQualifier(true);
		model.put("activeQualifiers", this.webJaguar.getQualifierList(qualifierSearch));
		model.put("salesRepMap", salesRepMap);
		
		model.put("salesRep", salesRep);
		model.put("salesRepProcessedBy", this.webJaguar.getSalesRepById( order.getSalesRepProcessedById() ));
		model.put("messageGroups", this.webJaguar.getSiteMessageGroupList()) ;
		model.put("messages", this.webJaguar.getSiteMessageList());
		model.put("contacts", this.webJaguar.getActiveCustomShippingContactList(new Search()));
		model.put("countries", this.webJaguar.getCountryMap());
		model.put("languageCodes", this.webJaguar.getLanguageSettings());
		
		SimpleDateFormat formatter = new SimpleDateFormat("MMMMM dd, yyyy");
		Layout layout = this.webJaguar.getSystemLayout("invoice", order.getHost(), request);
		layout.replace("#salesRepName#", ( salesRep == null ? "" : salesRep.getName() ));
		layout.replace("#salesRepNumber#", ( salesRep == null ? "" : salesRep.getPhone() ));
		layout.replace("#accountCreated#", ( customer == null ? "" : formatter.format(customer.getCreated()) ));
		layout.replace("#noOfOrders#", ( customer == null ? "0" :  customer.getOrderCount().toString() ));
		model.put("invoiceLayout", layout);
		
		//if(request.getParameter("shippingStatus")!=null && request.getParameter("shippingStatus")!=""){
		//	model.put("shippingStatus", request.getParameter("shippingStatus"));
		//}
		
		if ((Integer) gSiteConfig.get("gORDER_FILEUPLOAD") > 0) {
			File zipFolder = new File(getServletContext().getRealPath("/assets/orders_zip/"));
			Properties prop = new Properties();
			try {
				prop.load( new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
				if (prop.get("site.root") != null) {
					zipFolder = new File((String) prop.get("site.root"), "assets/orders_zip");
				}
			} catch (Exception e) {} 
			model.put("zipFile", new File(zipFolder, order.getOrderId() + ".zip").exists());
		}
		// Payments
	    if ((Boolean) gSiteConfig.get("gPAYMENTS")) {	    	
	    	// get open invoices
		    PaymentSearch search = new PaymentSearch();
		    search.setUserId(order.getUserId());
		    Map <Integer, Order> openInvoices = this.webJaguar.getInvoices(search, "open");
		    Map<Integer, Order> sortedOpenInvoices = new TreeMap<Integer, Order>();
		    for (Integer orderId: openInvoices.keySet()) {
		    	sortedOpenInvoices.put(orderId, openInvoices.get(orderId));
	    	}
		    if (sortedOpenInvoices.size() > 0) {
		    	model.put("openInvoices", sortedOpenInvoices.values());
		    }
	    }
	    
	    // Budget
	    if( (Boolean) gSiteConfig.get("gBUDGET")) {
	    	model.put("showStatusOnApproval", getOrderApprovalStatus(order, gSiteConfig, customer));
	    	//LinkedHashSet<Order> set;
	    	
			List<Order> actionOrdersList = this.webJaguar.getOrderActionHistory(order.getOrderId());
			List<Integer> ids = new ArrayList();
			for(Order id : actionOrdersList) {
				ids.add(id.getActionBy());
			}
			
			// to get all the managers IDs
			if(order.getStatus() != null && order.getStatus().equals("xq")) {
				for (Customer node: this.webJaguar.getFamilyTree( order.getUserId(), "up" )) {
					if(order.getUserId().compareTo(node.getId()) !=0 ) {
						Order orderAction = new Order();
						orderAction.setActionBy(node.getId());
						if(!(ids.contains(orderAction.getActionBy()))) {
							actionOrdersList.add(orderAction);
						}
					}
				}
			}
			
			order.setActionHistory(actionOrdersList);
			List<Integer> userIdList = new ArrayList();
			for(Order actionById: order.getActionHistory()) {				
				userIdList.add(actionById.getActionBy());
			}
			if(!userIdList.isEmpty()) {
				Map<Integer, String> nameMap = this.webJaguar.getCustomerNameMap(userIdList);
				
				for(Order actionByName:order.getActionHistory()){
					if(nameMap.containsKey((long) actionByName.getActionBy())) {
						actionByName.setActionByName(nameMap.get((long) actionByName.getActionBy()));
					}
				}
			}
	    } else {
	    	model.put("showStatusOnApproval", true);
	    }
	    
	    if (request.getParameter("__cancelBuySafe") != null) {
			SetShoppingCartCancelOrderRS cancelBuySafeBond = this.buySafeCancelOrder(order, siteConfig);
			if(cancelBuySafeBond != null){
				order.setBondCost(null);
				order.setWantsBond(false);
				order.setGrandTotal();
				
				OrderStatus orderStatus = new OrderStatus();
				orderStatus.setOrderId( order.getOrderId() );
				orderStatus.setComments("BuySafe Bond was selected but cancelled By Admin. Please check the amount charged and refund the bond cost to Customer.");
				orderStatus.setStatus(order.getStatus());
			
				this.webJaguar.cancelBuySafeBond(order, orderStatus);
			}
			return new ModelAndView(new RedirectView("invoice.jhtm"), "order", order.getOrderId());
		}
	    
	    // list of PO
	    PurchaseOrderSearch poSearch = new PurchaseOrderSearch();
	    poSearch.setOrderId(order.getOrderId());
	    poSearch.setStatus("");
	    model.put("showPOBox", ServletRequestUtils.getBooleanParameter(request, "showPOBox"));
	    model.put("poList", this.webJaguar.getPurchaseOrderList(poSearch));
	    
		if (request.getParameter("__print") != null || request.getParameter("__printDiscount") != null ||
			request.getParameter("__label") != null	|| request.getParameter("__overview") != null || request.getParameter("__walkin") != null || request.getParameter("__courierLtl") != null) {
			boolean print = true;
			if (order.isPaymentAlert()) {
				boolean isAEM=false,isADMIN=false,isOVERRIDE_PAYMENT_ALERT=false;
				for (GrantedAuthority role: SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
					if (role.getAuthority().equals("ROLE_AEM")) isAEM = true;
					if (role.getAuthority().startsWith("ROLE_ADMIN_")) isADMIN = true;
					if (role.getAuthority().equals("ROLE_OVERRIDE_PAYMENT_ALERT")) isADMIN = true;
				}
				if (!(isAEM || isADMIN || isOVERRIDE_PAYMENT_ALERT)) {
					print = false;
				}
			}
			
		
		    if (request.getParameter("__print") != null && print) {
				order.setPrinted( true );
				this.webJaguar.updateOrderPrinted( order );
	
				int type = ServletRequestUtils.getIntParameter(request, "__print", 1);
				PackingListSearch packingListSearch = new PackingListSearch();
				packingListSearch.setOrderId(order.getOrderId());
				List<PackingList> packingList = this.webJaguar.getPackingListByOrderId(packingListSearch);
				if(packingList != null && !packingList.isEmpty()) {
					model.put("shipDate", packingList.get(packingList.size() -1).getShipped());
				}
				switch (type) {
					case 2:
						layout = this.webJaguar.getSystemLayout("invoice2", order.getHost(), request);
						layout.replace("#salesRepName#", ( salesRep == null ? "" : salesRep.getName() ));
						layout.replace("#salesRepNumber#", ( salesRep == null ? "" : salesRep.getPhone() ));
						layout.replace("#accountCreated#", ( customer == null ? "" : formatter.format(customer.getCreated()) ));
						layout.replace("#noOfOrders#", ( customer == null ? "0" :  customer.getOrderCount().toString() ));
						layout.replace("#orderNo#", ""+order.getOrderId());
						model.put("invoiceLayout", layout);
						return new ModelAndView("admin/orders/invoicePrint2", model);			
					case 3:
						layout = this.webJaguar.getSystemLayout("pickingSheet", order.getHost(), request);
						layout.replace("#salesRepName#", ( salesRep == null ? "" : salesRep.getName() ));
						layout.replace("#salesRepNumber#", ( salesRep == null ? "" : salesRep.getPhone() ));
						layout.replace("#accountCreated#", ( customer == null ? "" : formatter.format(customer.getCreated()) ));
						layout.replace("#noOfOrders#", ( customer == null ? "0" :  customer.getOrderCount().toString() ));
						model.put("invoiceLayout", layout);
						return new ModelAndView("admin/orders/pickingSheet", model);			
					default:
						return new ModelAndView("admin/orders/invoicePrint", model);			
				}
			} else if (request.getParameter("__printDiscount") != null && print) {
				order.setPrinted( true );
				this.webJaguar.updateOrderPrinted( order );
				return new ModelAndView("admin/orders/invoiceShowDiscountPrint", model);
			} else if (request.getParameter("__label") != null && print) {
				return new ModelAndView("admin/orders/label", model);			
			}else if (request.getParameter("__walkin") != null) {
				
				URL url2 = new URL(request.getScheme()+"://www.viatrading.com/dv/invoice/walkin.php?InvoiceID="+order.getOrderId());
				HttpURLConnection con = (HttpURLConnection) url2.openConnection();
			    con.setRequestMethod("GET");
			    con.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			    con.addRequestProperty("User-Agent", "Mozilla");
			    con.addRequestProperty("Referer", "google.com");
			    con.setRequestProperty("Accept", "text/html, text/*");
			    con.addRequestProperty("Content-Type","application/json; charset=UTF-8"); 
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuilder sb = new StringBuilder();
				 
				while((inputLine = in.readLine()) != null)
				{
				    if(inputLine!=null && !inputLine.isEmpty()){
				    	sb.append(inputLine);
				    	
				    }
				}	
				model.put("walkinInvoice", sb.toString());
				return new ModelAndView("admin/orders/walkin", model);	
				
			}else if (request.getParameter("__courierLtl") != null) {
				
				String orig = order.getOrderId() + "-" + order.getUserId();
			    byte[] encoded = Base64.encodeBase64(orig.getBytes());         
			    orig = new String(encoded);
				
				URL url2 = new URL(request.getScheme()+"://www.viatrading.com/dv/invoice/courierinvoice.php?id=" + orig);
				HttpURLConnection con = (HttpURLConnection) url2.openConnection();
			    con.setRequestMethod("GET");
			    con.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			    con.addRequestProperty("User-Agent", "Mozilla");
			    con.addRequestProperty("Referer", "google.com");
			    con.setRequestProperty("Accept", "text/html, text/*");
			    con.addRequestProperty("Content-Type","application/json; charset=UTF-8"); 
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuilder sb = new StringBuilder();
				 
				while((inputLine = in.readLine()) != null)
				{
				    if(inputLine!=null && !inputLine.isEmpty()){
				    	sb.append(inputLine);
				    	
				    }
				}	
				model.put("courierLtlInvoice", sb.toString());
				return new ModelAndView("admin/orders/courierLtlInvoice", model);	
				
			}else if (request.getParameter("__overview") != null && print) {
				order.setPrinted( true );
				this.webJaguar.updateOrderPrinted( order );
				return new ModelAndView("admin/orders/invoiceOverview", model);			
			} 
		}
		
		String AppsLink = order.getOrderId().toString() + "-" + customer.getId().toString();
		 byte[] encode = Base64.encodeBase64(AppsLink.getBytes());         
		    String encodedId = new String(encode);
		model.put("encodedId", encodedId);
		

		String language = RequestContextUtils.getLocale(request).getLanguage();
		model.put("lang", language);
		if(order.getStatus() != null && order.getStatus().equals("xq")) {
			//Quote View
			if(request.getParameter("orderName")!= null && request.getParameter("orderName").equals("quotes")) {
				request.setAttribute("orderName", "quotes");
			}
			return new ModelAndView("admin/orders/quote", model);			
		}
		return new ModelAndView("admin/orders/invoice", model);
	}
    
	private boolean checkShippingTitle(String shippingTitle){
		List<ShippingRate> customeShippingList= this.webJaguar.getCustomShippingRateList(false,true);
		if(shippingTitle==null)
			return false;
		for(ShippingRate shipping : customeShippingList){
			if(shipping.getTitle().equalsIgnoreCase(shippingTitle)){
				return true;
			}
		}
		return false;
	}

    private void notifySuppliers(SiteMessage supplierSiteMessage,HttpServletRequest request, Order order, Map<String, Configuration> siteConfig, MultiStore multiStore) {
		// send email
		Map<Integer,HashSet<LineItem>> SupplierLineitem = new HashMap();
	
		for (LineItem lineItem : order.getLineItems()) {
			if(lineItem.isConsignmentProduct()) {
	
				Supplier s = this.webJaguar.getProductSupplierPrimaryBySku(lineItem.getProduct().getSku());
				if(s!=null){			
				HashSet<LineItem> items = new HashSet();
				if(SupplierLineitem.containsKey(s.getId())){			
					items = SupplierLineitem.get(s.getId());
				}
					items.add(lineItem);
					SupplierLineitem.put(s.getId(), items);
				}
			 }
		}	
	
		for(Integer sId: SupplierLineitem.keySet()){
			OrderStatus orderStatus = new OrderStatus(order.getOrderId(), order.getStatus(), supplierSiteMessage.getSubject(), supplierSiteMessage.getMessage() );
			orderStatus.setHtmlMessage(supplierSiteMessage.isHtml());
			Supplier supplier = this.webJaguar.getSupplierById(sId);
			
		if ( supplier != null && supplier.getAddress() != null && supplier.getAddress().getEmail()!= null) {
		
	    	DecimalFormat df = new DecimalFormat("0.00");
	    	DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
			symbols.setDecimalSeparator('.');
			DecimalFormat df2 = new DecimalFormat("#,###.##", symbols);
			String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
			String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);			
			
			StringBuffer messageBuffer = new StringBuffer();
			// subject
			orderStatus.setSubject( orderStatus.getSubject().replace( "#firstname#", (supplier.getAddress().getFirstName() != null) ? supplier.getAddress().getFirstName() : "" ) );
			orderStatus.setSubject( orderStatus.getSubject().replace( "#lastname#", (supplier.getAddress().getLastName() != null) ? supplier.getAddress().getLastName() : "" ) );
			orderStatus.setSubject( orderStatus.getSubject().replace( "#email#", (supplier.getAddress().getEmail() != null) ? supplier.getAddress().getEmail() : "" ) );
			orderStatus.setSubject( orderStatus.getSubject().replace( "#order#", order.getOrderId().toString() ) );
			orderStatus.setSubject( orderStatus.getSubject().replace( "#status#", getMessageSourceAccessor().getMessage( "orderStatus_" + orderStatus.getStatus() ) ) );
			orderStatus.setSubject( orderStatus.getSubject().replace( "#tracknum#", (orderStatus.getTrackNum() != null) ? orderStatus.getTrackNum() : "" ) );
			//orderStatus.setSubject( orderStatus.getSubject().replace( "#orderlink#", secureUrl + "supplierInvoice.jhtm?orderId=" + orderStatus.getOrderId() ) );
			String viewInvoiceHrefInSubject = secureUrl + "supplierInvoice.jhtm?orderId=" + orderStatus.getOrderId();
			StringBuilder sb_aTagInSubject = new StringBuilder();
			sb_aTagInSubject.append("<a href=").append("\"").append(viewInvoiceHrefInSubject).append("\"").append(">").append("View Invoice").append("</a>");
			orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#orderlink#", sb_aTagInSubject.toString()));
			
			StringBuilder sb_aTagInSubject_orderlinkes = new StringBuilder();
			sb_aTagInSubject_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHrefInSubject).append("\"").append(">").append("Ver Factura").append("</a>");
			orderStatus.setSubject(orderStatus.getSubject().replace("#orderlinkes#", sb_aTagInSubject_orderlinkes.toString()));
			
	
			// message
			orderStatus.setMessage( orderStatus.getMessage().replace( "#firstname#", (supplier.getAddress().getFirstName() != null) ? supplier.getAddress().getFirstName() : "" ) );
			orderStatus.setMessage( orderStatus.getMessage().replace( "#lastname#", (supplier.getAddress().getLastName() != null) ? supplier.getAddress().getLastName() : "" ) );
			orderStatus.setMessage( orderStatus.getMessage().replace( "#email#", (supplier.getAddress().getEmail() != null) ? supplier.getAddress().getEmail() : "" ) );
			orderStatus.setMessage( orderStatus.getMessage().replace( "#order#", order.getOrderId().toString() ) );
			orderStatus.setMessage( orderStatus.getMessage().replace( "#status#", getMessageSourceAccessor().getMessage( "orderStatus_" + orderStatus.getStatus() ) ) );
			orderStatus.setMessage( orderStatus.getMessage().replace( "#tracknum#", (orderStatus.getTrackNum() != null) ? orderStatus.getTrackNum() : "" ) );
			//orderStatus.setMessage( orderStatus.getMessage().replace( "#orderlink#", secureUrl + "supplierInvoice.jhtm?orderId=" + orderStatus.getOrderId() ) );
			String viewInvoiceHrefInMessage = secureUrl + "supplierInvoice.jhtm?orderId=" + orderStatus.getOrderId();
			StringBuilder sb_aTagInMessage = new StringBuilder();
			sb_aTagInMessage.append("<a href=").append("\"").append(viewInvoiceHrefInMessage).append("\"").append(">").append("View Invoice").append("</a>");
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#orderlink#", sb_aTagInMessage.toString()));
			
			StringBuilder sb_aTagInMessage_orderlinkes = new StringBuilder();
			sb_aTagInMessage_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHrefInMessage).append("\"").append(">").append("Ver Factura").append("</a>");
			orderStatus.setMessage(orderStatus.getMessage().replace("#orderlinkes#", sb_aTagInMessage_orderlinkes.toString()));
			
			messageBuffer.append( orderStatus.getMessage() );
			if (orderStatus.getMessage().contains("#wjstart#") && orderStatus.getMessage().contains("#wjend#")) {
			    messageBuffer.insert(orderStatus.getMessage().indexOf("#wjstart#"), getContent(orderStatus.getMessage(), "#wjstart#" , "#wjend#",SupplierLineitem.get(sId), siteConfig));
				messageBuffer.delete(messageBuffer.indexOf("#wjstart#"), messageBuffer.indexOf("#wjend#")+"#wjend#".length());
			}
			
			try {
		    	// construct email message
		       	MimeMessage mms = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");	
				helper.setTo(supplier.getAddress().getEmail());
	
				helper.setFrom(contactEmail);
				helper.setBcc( contactEmail );
				helper.setSubject( orderStatus.getSubject() );
				if ( orderStatus.isHtmlMessage() ) { 
		    		helper.setText( messageBuffer.toString(), true );
		    	} else {
		    		helper.setText(messageBuffer.toString());
		    	}
				
				mailSender.send(mms);
			} catch (Exception ex) {
				// do nothing
				ex.printStackTrace();
			} 
	     }
      }
  }
    

	private String getContent(String originalString, String startPoint, String endPoint, HashSet <LineItem> products, Map<String, Configuration> siteConfig) {
		
		StringBuffer sb = new StringBuffer();
		int beginIndex = originalString.indexOf(startPoint) + startPoint.length();
		int endIndex = originalString.indexOf(endPoint, beginIndex);
		String tempString = null;
		
		for(LineItem Lineitem : products) {

			tempString = originalString.substring(beginIndex, endIndex);;
			tempString = tempString.replace("#productName#",Lineitem.getProduct().getName() );
			//tempString = tempString.replace("#shortDesc#", Lineitem.getProduct().getShortDesc() == null? "" : Lineitem.getProduct().getShortDesc());
			tempString = tempString.replace("#sku#", Lineitem.getProduct().getSku());		
			tempString = tempString.replace("#qty#", Lineitem.getQuantity()+"");	
		  	DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
			symbols.setDecimalSeparator('.');
			NumberFormat df2 = new DecimalFormat("#,###.##", symbols);
			System.out.println("Lineitem id:: "+Lineitem.getProductId());
			tempString = tempString.replace("#consignmentAmount#", (Lineitem.getConsignmentValue() != null) ? df2.format(( Lineitem.getConsignmentValue()))+"" : "" );		
			
			Product product = this.webJaguar.getProductById(Lineitem.getProduct().getId(), 0, false, null);
			Double lotUnit = null;
			Double consignmentAmt = null;
			Double netRecoveryPerUnit = null;
			Long temp = 0L;
			if(Lineitem.getConsignmentValue() != null){
				try{
					temp = (long) (Lineitem.getConsignmentValue() * 100);
					consignmentAmt = (double) temp / 100;
				}catch(Exception ex){
					consignmentAmt = 0.00;
				}
			}else{
				consignmentAmt = 0.00;
			}
			if(product.getField7()!=null){
				try{
					lotUnit = Lineitem.getQuantity()* Double.valueOf(product.getField7());
				}catch(Exception ex){
					try{
						lotUnit = Double.valueOf(Lineitem.getQuantity());
					}catch(Exception e){
						lotUnit = 0.0;
					}
				}
			}else{
				try{
					lotUnit = Double.valueOf(Lineitem.getQuantity());
				}catch(Exception e){
					lotUnit = 0.0;
				}
			}
			netRecoveryPerUnit = consignmentAmt/lotUnit;
			try{
				tempString = tempString.replace("#netRecoveryPerUnit#", df2.format(netRecoveryPerUnit));
			}catch(Exception ex){
				tempString = tempString.replace("#netRecoveryPerUnit#", "");
			}
			
			sb.append(tempString);
		}
		
		return sb.toString();
	}
	
	
    private boolean getOrderApprovalStatus(Order order, Map<String, Object> gSiteConfig, Customer customer) {
    	
    	List<Integer> parentsList = new ArrayList();
    	for (Customer node: this.webJaguar.getFamilyTree( order.getUserId(), "up" )) {
			//Check if user exists in the tree. 
			if(node.getId().compareTo(customer.getId())  != 0) {
				parentsList.add(node.getId());
			}
		}
    	boolean check = true;
    	//Check if all the parents approved this order.
    	for(Integer parentId: parentsList) {
    		if(check == this.webJaguar.getApproval(order.getOrderId(), parentId, true)) {
    			continue;
    		} else {
    			check = false;
    			break;
    		}
    	}
    	return check;
    }
    
    private void sendToMACS(Order order, HttpServletRequest request, Customer customer) throws Exception {
    	
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a", RequestContextUtils.getLocale(request));
    	Date now = new Date();
    	
		URL url = new URL("http://www.yourcomputersupplies.com/cgi-bin/D3CGIServer/getGOpo.asp");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		// Setup HTTP POST parameters
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setUseCaches(false);

		// POST data
		StringBuffer xmlRequest = new StringBuffer(
				"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
				"<!DOCTYPE cXML SYSTEM \"http://xml.cXML.org/schemas/cXML/1.1.009/cXML.dtd\">" +
				"<cXML payloadID=\"" + dateFormatter.format(now) + "\" timestamp=\"" + dateFormatter.format(now) + "\" xml:lang=\"en-US\" version=\"1.1009\">" +
				"<Header>" +
				"<From>" +
				"<Credential domain = \"NetworkID\">" +
				"<Identity>15922</Identity>" +
				"</Credential>" +
				"</From>" +
				"<To>" +
				"<Credential domain=\"DUNS\">" +
				"<Identity>790101869</Identity>" +
				"</Credential>" +
				"</To>" +
				"<Sender>" +
				"<Credential domain = \"AribaNetworkUserID\">" +
				"<Identity>15922</Identity>" +
				"<SharedSecret>Shared secret - not used</SharedSecret>" +
				"</Credential>" +
				"<UserAgent>Ariba.com Network V1.1 </UserAgent>" +
				"</Sender>" +
				"</Header>" +
				"<Request deploymentMode=\"production\">" +
				"<OrderRequest>" +
				"<OrderRequestHeader orderID=\"" + order.getOrderId() + "\" ");
		
		// order date
    	dateFormatter = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
    	xmlRequest.append("orderDate=\"" + dateFormatter.format(order.getDateOrdered()) + "\" type=\"new\">");
    	
		xmlRequest.append("<Total>" +
				"<Money currency=\"USD\">" + order.getGrandTotal() + "</Money>" +
				"</Total>" +
				"<ShipTo>" +
				"<Address addressID=\"\">" +
				"<Name xml:lang=\"en\">" + order.getShipping().getFirstName() + " " + order.getShipping().getLastName() + "</Name>" +
				"<Company>" + order.getShipping().getCompany() + "</Company>" +
				"<PostalAddress name=\"default\">" +
				"<DeliverTo></DeliverTo>" +
				"<Street>" + order.getShipping().getAddr1() + " " + order.getShipping().getAddr2() + "</Street>" +
				"<City>" + order.getShipping().getCity() + "</City>" +
				"<State>" + order.getShipping().getStateProvince() + "</State>" +
				"<PostalCode>" + order.getShipping().getZip() + "</PostalCode>" +
				"<Country isoCountryCode=\"" + order.getShipping().getCountry() + "\"/>" +
				"</PostalAddress>");
		
		// email
    	String email = "";
    	if (customer != null) {
    		email = customer.getUsername();
    	}		
    	xmlRequest.append("<Email name= \"default\">" + email + "</Email>");
				
		xmlRequest.append("<Phone name = \"work\">" +
				"<TelephoneNumber>" +
				"<CountryCode isoCountryCode = \"\">" +
				"</CountryCode>" +
				"<AreaOrCityCode></AreaOrCityCode>" +
				"<Number>" + order.getShipping().getPhone() + "</Number>" +
				"</TelephoneNumber>" +
				"</Phone>" +
				"</Address>" +
				"</ShipTo>" +
				"<BillTo>" +
				"<Address>" +
				"<Name xml:lang=\"en\">" + order.getBilling().getFirstName() + " " + order.getBilling().getLastName() + "</Name>" +
				"<Company>" + order.getBilling().getCompany() + "</Company>" +
				"<PostalAddress>" +
				"<DeliverTo />" +
				"<Street>" + order.getBilling().getAddr1() + " " + order.getBilling().getAddr2() + "</Street>" +
				"<City>" + order.getBilling().getCity() + "</City>" +
				"<State>" + order.getBilling().getStateProvince() + "</State>" +
				"<PostalCode>" + order.getBilling().getZip() + "</PostalCode>" +
				"<Country isoCountryCode= \"" + order.getBilling().getCountry() + "\"/>" +
				"</PostalAddress>" +
				"</Address>" +
				"</BillTo>" +
				"<Tax>" +
				"<Money currency=\"USD\">" + order.getTax() + "</Money>" +
				"<Description xml:lang=\"en\"></Description>" +
				"</Tax>");
		
		// credit card info
		xmlRequest.append("<CreditCard>");
		if (order.getCreditCard() != null) {
			xmlRequest.append(
				"<Type>" + order.getCreditCard().getType() + "</Type>" +
				"<Number>" + order.getCreditCard().getNumber() + "</Number>" +
				"<ExpMonth>" + order.getCreditCard().getExpireMonth() + "</ExpMonth>" +
				"<ExpYear>" + order.getCreditCard().getExpireYear() + "</ExpYear>" +
				"<CardCode>" + order.getCreditCard().getCardCode() + "</CardCode>" +
				"");
			if (order.getCreditCard().getTransId() != null) {
				xmlRequest.append("<TransID>" + order.getCreditCard().getTransId() + "</TransID>");				
			} else {
				xmlRequest.append("<TransID/>");
			}
			if (order.getCreditCard().getPaymentStatus() != null) {
				xmlRequest.append("<PaymentStatus>" + order.getCreditCard().getPaymentStatus() + "</PaymentStatus>");				
			} else {
				xmlRequest.append("<PaymentStatus/>");
			}
			if (order.getCreditCard().getPaymentNote() != null) {
				xmlRequest.append("<PaymentNote>" + order.getCreditCard().getPaymentNote() + "</PaymentNote>");				
			} else {
				xmlRequest.append("<PaymentNote/>");
			}
			if (order.getCreditCard().getPaymentDate() != null) {
				xmlRequest.append("<PaymentDate>" + order.getCreditCard().getPaymentDate() + "</PaymentDate>");				
			} else {
				xmlRequest.append("<PaymentDate/>");
			}			
		} else {
			xmlRequest.append("<Type/><Number/><ExpMonth/><ExpYear/><CardCode/><TransID/><PaymentStatus/><PaymentNote/><PaymentDate/>");
		}
		xmlRequest.append("</CreditCard>");

		xmlRequest.append("</OrderRequestHeader>");
		
		// line items
    	for (LineItem lineItem: order.getLineItems()) {
    		int quantity = lineItem.getQuantity();
    		if (lineItem.getProduct().getCaseContent() != null) {
    			quantity = quantity * lineItem.getProduct().getCaseContent();
    		}
    		xmlRequest.append(
    				"<ItemOut quantity=\"" + quantity + "\">" +
    				"<ItemID>" +
    				"<SupplierPartID>" + lineItem.getProduct().getSku() + "</SupplierPartID>" +
    				"</ItemID>" +
    				"<ItemDetail>" +
    				"<UnitPrice>" +
    				"<Money currency=\"USD\">" + lineItem.getUnitPrice() + "</Money>" +
    				"</UnitPrice>" +
    				"<Description xml:lang=\"en\">" + lineItem.getProduct().getName() + "</Description>" +
    				"<UnitOfMeasure>" + lineItem.getProduct().getPacking() + "</UnitOfMeasure>" +
    				"<Classification domain=\"\"/>" +
    				"<Extrinsic name=\"InvisionID\"></Extrinsic>" +
    				"<Extrinsic name=\"ShipID\"></Extrinsic>" +
    				"</ItemDetail>" +
    				"<ShipTo>" +
    				"<Address addressID=\"\">" +
    				"<Name xml:lang=\"en\"></Name>" +
    				"<PostalAddress name=\"\">" +
    				"<Street></Street>" +
    				"<City></City>" +
    				"<State></State>" +
    				"<PostalCode></PostalCode>" +
    				"<Country isoCountryCode=\"\"></Country>" +
    				"</PostalAddress>" +
    				"</Address>" +
    				"</ShipTo>" +
    				"<Comments xml:lang=\"en-US\"></Comments>" +
    				"</ItemOut>");
    	}
    	
		xmlRequest.append("</OrderRequest>" +
				"</Request>" +
				"</cXML>");
		
		OutputStream oStream = connection.getOutputStream();
		oStream.write(xmlRequest.toString().getBytes());
		oStream.close();
		
    	File baseFile = new File(getServletContext().getRealPath("/"));
    	File ipnFile = new File(baseFile, "MACS");
    	FileOutputStream fo = new FileOutputStream(ipnFile, true);
    	PrintWriter pw = new PrintWriter(fo);
    	
    	// Get response data.
	    DataInputStream input = new DataInputStream(connection.getInputStream());
	    String str;
	    while (null != ((str = input.readLine()))) {
	    	pw.write(str  + "\n");
		}
		pw.write("--------------------------------------------\n");
		pw.close();
    	fo.close();
    }
    
    private void calculateUserPoints(Order order, Customer customer, OrderStatus orderStatus, boolean pointToCredit) {

    	if (pointToCredit && ( customer.getPointThreshold() != null && (order.getTotalLoyaltyPoint() + customer.getLoyaltyPoint()) >  customer.getPointThreshold())) {
    		CreditImp credit = new CreditImp();
    		CustomerCreditHistory customerCredit = credit.updateCustomerCreditByLoyalty(order, customer, orderStatus.getUsername());
			this.webJaguar.updateCredit(customerCredit);
			this.webJaguar.updatePointsToCustomer( customer.getId(), order.getTotalLoyaltyPoint() - getCunsumablePoint(order, customer)  );	
		} else {
			this.webJaguar.updatePointsToCustomer( customer.getId(), order.getTotalLoyaltyPoint() );	
		}
    }
    
    private Double getCunsumablePoint(Order order, Customer customer) {
    	Integer pointsByThreshold = new Integer( (int) ((order.getTotalLoyaltyPoint() + customer.getLoyaltyPoint()) / ((customer.getPointThreshold() == null) ? 1 : customer.getPointThreshold())));
    	return pointsByThreshold * ((customer.getPointThreshold() == null) ? 1.0 : customer.getPointThreshold().doubleValue());
    }
    
//    private void sendToEvergreen(Order order, Customer customer) throws Exception {
//		String export_success = this.webJaguar.getEvergreenOrderSuc(order.getOrderId());
//		
//		if (export_success == null || !export_success.equals("1")) {
//	    	// submit order to evergreen
//			EvergreenApi evergreen = new EvergreenApi();
//			
//			if (customer.getAccountNumber() == null || customer.getAccountNumber().trim().equals("")) {
//				this.webJaguar.updateEvergreenCustomerSuc(customer.getId(), evergreen.SetClient(customer, order.getShipping()));				
//			}
//			
//			for (LineItem lineItem: order.getLineItems()) {
//				evergreen.SetOrderDetail(lineItem, true);					
//			}
//			this.webJaguar.updateEvergreenOrderSuc(order.getOrderId(), evergreen.SetOrderHeader(order, customer, true));
//		}
//    }
    
    private void amazonCancelOrder(Order order, OrderStatus orderStatus, Map<String, Configuration> siteConfig) throws Exception {
    	AmazonApi amazonApi = new AmazonApi(siteConfig.get("AMAZON_MERCHANT_TOKEN").getValue(), siteConfig.get("AMAZON_MERCHANT_NAME").getValue(),
    			siteConfig.get("AMAZON_EMAIL").getValue(), siteConfig.get("AMAZON_PASSWORD").getValue());
    	
    	// create PostDocument
    	SimpleDateFormat dFormatter = new SimpleDateFormat("yyyyMMddhhmm");
    	
    	File amazonDir = new File(getServletContext().getRealPath("/amazonCheckout"));	
		if (!amazonDir.exists()) {
			amazonDir.mkdir();
		}
		File amazonPostDocumentDir = new File(amazonDir, "postDocument");
		if (!amazonPostDocumentDir.exists()) {
			amazonPostDocumentDir.mkdir();
		}
		File postDocument = new File(amazonPostDocumentDir, dFormatter.format(new Date()) + "-" + order.getOrderId() + ".xml");
    	FileOutputStream fo = new FileOutputStream(postDocument);
    	PrintWriter pw = new PrintWriter(fo);
    	pw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    	pw.write("<AmazonEnvelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"amzn-envelope.xsd\">\n");
    	pw.write("<Header>\n");
    	pw.write("<DocumentVersion>1.01</DocumentVersion>\n");
    	pw.write("<MerchantIdentifier>" + siteConfig.get("AMAZON_MERCHANT_TOKEN").getValue() + "</MerchantIdentifier>\n");
    	pw.write("</Header>\n");
    	pw.write("<MessageType>OrderAcknowledgement</MessageType>\n");
    	pw.write("<Message>\n");
    	pw.write("<MessageID>" + order.getOrderId() + "</MessageID>\n");
    	pw.write("<OrderAcknowledgement>\n");
    	pw.write("<AmazonOrderID>" + order.getAmazonIopn().getAmazonOrderID() + "</AmazonOrderID>\n");
    	pw.write("<MerchantOrderID>" + order.getOrderId() + "</MerchantOrderID>\n");
    	pw.write("<StatusCode>Failure</StatusCode>\n");
    	pw.write("</OrderAcknowledgement>\n");
    	pw.write("</Message>\n");
    	pw.write("</AmazonEnvelope>\n");
    	pw.close();
    	fo.close(); 
    	
    	orderStatus.setAmazonDocTransID(amazonApi.acknowledgeOrder(postDocument));
    }
    
    private File amazonShipOrder(Order order, OrderStatus orderStatus, Map<String, Configuration> siteConfig) throws Exception {
    	AmazonApi amazonApi = new AmazonApi(siteConfig.get("AMAZON_MERCHANT_TOKEN").getValue(), siteConfig.get("AMAZON_MERCHANT_NAME").getValue(),
    			siteConfig.get("AMAZON_EMAIL").getValue(), siteConfig.get("AMAZON_PASSWORD").getValue());
    	
    	// create PostDocument
    	SimpleDateFormat dFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	
    	File amazonDir = new File(getServletContext().getRealPath("/amazonCheckout"));	
		if (!amazonDir.exists()) {
			amazonDir.mkdir();
		}
		File amazonPostDocumentDir = new File(amazonDir, "postDocument");
		if (!amazonPostDocumentDir.exists()) {
			amazonPostDocumentDir.mkdir();
		}
		
		// acknowledge order
		File postDocument = new File(amazonPostDocumentDir, dFormatter.format(new Date()) + "-" + order.getOrderId() + ".xml");
    	FileOutputStream fo = new FileOutputStream(postDocument);
    	PrintWriter pw = new PrintWriter(fo);
    	pw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    	pw.write("<AmazonEnvelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"amzn-envelope.xsd\">\n");
    	pw.write("<Header>\n");
    	pw.write("<DocumentVersion>1.01</DocumentVersion>\n");
    	pw.write("<MerchantIdentifier>" + siteConfig.get("AMAZON_MERCHANT_TOKEN").getValue() + "</MerchantIdentifier>\n");
    	pw.write("</Header>\n");
    	pw.write("<MessageType>OrderAcknowledgement</MessageType>\n");
    	pw.write("<Message>\n");
    	pw.write("<MessageID>" + order.getOrderId() + "</MessageID>\n");
    	pw.write("<OrderAcknowledgement>\n");
    	pw.write("<AmazonOrderID>" + order.getAmazonIopn().getAmazonOrderID() + "</AmazonOrderID>\n");
    	pw.write("<MerchantOrderID>" + order.getOrderId() + "</MerchantOrderID>\n");
    	pw.write("<StatusCode>Success</StatusCode>\n");
    	pw.write("</OrderAcknowledgement>\n");
    	pw.write("</Message>\n");
    	pw.write("</AmazonEnvelope>\n");
    	pw.close();
    	fo.close(); 
		amazonApi.acknowledgeOrder(postDocument);
    	
    	// ship order
		postDocument = new File(amazonPostDocumentDir, dFormatter.format(new Date()) + "-" + order.getOrderId() + ".xml");
    	fo = new FileOutputStream(postDocument);
    	pw = new PrintWriter(fo);
    	pw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    	pw.write("<AmazonEnvelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"amzn-envelope.xsd\">\n");
    	pw.write("<Header>\n");
    	pw.write("<DocumentVersion>1.01</DocumentVersion>\n");
    	pw.write("<MerchantIdentifier>" + siteConfig.get("AMAZON_MERCHANT_TOKEN").getValue() + "</MerchantIdentifier>\n");
    	pw.write("</Header>\n");
    	pw.write("<MessageType>OrderFulfillment</MessageType>\n");
    	pw.write("<Message>\n");
    	pw.write("<MessageID>" + order.getOrderId() + "</MessageID>\n");
    	pw.write("<OrderFulfillment>\n");
    	pw.write("<AmazonOrderID>" + order.getAmazonIopn().getAmazonOrderID() + "</AmazonOrderID>\n");
    	SimpleDateFormat fulfillmentFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    	fulfillmentFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
    	pw.write("<FulfillmentDate>" + fulfillmentFormatter.format(new Date()) + "</FulfillmentDate>");
    	pw.write("</OrderFulfillment>\n");
    	pw.write("</Message>\n");
    	pw.write("</AmazonEnvelope>\n");
    	pw.close();
    	fo.close(); 
    	orderStatus.setAmazonDocTransID(amazonApi.shipOrder(postDocument));    

    	// get document
    	DocumentProcessingInfo procInfo = amazonApi.getDocumentProcessingInfo(orderStatus.getAmazonDocTransID());
    	Object[] document = amazonApi.getDocument(procInfo.getProcessingReport().getDocumentID());
    	for (int i = 0; i < document.length; i++) {
			AttachmentPart attachment = (AttachmentPart) document[i];
			DataHandler handler = attachment.getDataHandler();
			SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(handler.getInputStream());
			Element root = doc.getRootElement(); 
			Element ProcessingSummary = root.getChild("Message").getChild("ProcessingReport").getChild("ProcessingSummary");
			if (ProcessingSummary.getChildText("MessagesProcessed").equals("1") && 
				ProcessingSummary.getChildText("MessagesSuccessful").equals("1")) {
				// successful
				return null;
			} else {
				File errorXml = new File(amazonPostDocumentDir, dFormatter.format(new Date())  + "-" + order.getOrderId() + "-error.xml");
				fo = new FileOutputStream(errorXml);
				handler.writeTo(fo);
				fo.close();
				return errorXml;
			}
		}

    	return null;
    }
    
 // buy safe
	private SetShoppingCartCancelOrderRS buySafeCancelOrder(Order order, Map<String, Configuration> siteConfig ) throws MalformedURLException{
		
		BuySafeAPI buySafeService = new BuySafeAPI(new URL(siteConfig.get("BUY_SAFE_URL").getValue()+"?VER="+siteConfig.get("BUY_SAFE_VERSION").getValue()+"&Username="+siteConfig.get("BUY_SAFE_USERNAME").getValue()+"&Password="+siteConfig.get("BUY_SAFE_PASSWORD").getValue()), new QName("http://ws.buysafe.com", "BuySafeAPI"));
		BuySafeAPISoap port = buySafeService.getBuySafeAPISoap();
			
		// set header
		this.webJaguar.buySafeHeader(port);
		
		SetShoppingCartCancelOrderRQ cancelRQ = new SetShoppingCartCancelOrderRQ();
		cancelRQ.setShoppingCartId("PON::"+order.getOrderId());
		
		return port.setShoppingCartCancelOrder(cancelRQ);
	}
}
