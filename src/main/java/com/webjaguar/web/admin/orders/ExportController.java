/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.10.2007
 */

package com.webjaguar.web.admin.orders;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;

import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.domain.Constants;

public class ExportController extends SimpleFormController {
	
	boolean gSALES_REP;
	boolean gSALES_PROMOTIONS;
	boolean gADD_INVOICE;
	boolean gPURCHASE_ORDER;
	int gMULTI_STORE = 0;
	int gPRODUCT_FIELDS = 0;
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private MailSender mailSender;
    public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public ExportController() {
		setSessionForm(false);
		setCommandName("orderSearch");
		setCommandClass(OrderSearch.class);
		setFormView("admin/orders/export");
	}
	
	private File baseFile;
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		OrderSearch search = (OrderSearch) command;
		
		if (search.getFileType().equals( "" )) {
			return showForm(request, response, errors, map);
		}
		// set end date to end of day
		if (search.getEndDate() != null) {
			search.getEndDate().setTime(search.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}

    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
		if (gSALES_REP) {
			// get sales rep
			Iterator<SalesRep> iter = this.webJaguar.getSalesRepList().iterator();
			while (iter.hasNext()) {
				SalesRep salesRep = iter.next();
				salesRepMap.put(salesRep.getId(), salesRep);
			}
		}
		
		if (baseFile.canWrite()) {
			File file[] = baseFile.listFiles();
			List<Map<String, Object>> exportedFiles = new ArrayList<Map<String, Object>>();
			for (int f=0; f<file.length; f++) {
				if (file[f].getName().startsWith("invoiceExport") & file[f].getName().endsWith("."+search.getFileType())) {
					file[f].delete();
				}
			}
			map.put("exportedFiles", exportedFiles);
			
			int orderCount = this.webJaguar.invoiceCount(search);
			search.setLimit(null);
			
			if (search.getFileType().equals("xls")) {
				createExcelFiles(orderCount, search, siteConfig,  gSiteConfig, exportedFiles, salesRepMap);				
			} else if (search.getFileType().equals("vaf")){
				createVafFile(orderCount, search, siteConfig, exportedFiles);
			} else if (search.getFileType().equals("thub") && siteConfig.get( "INVOICE_EXPORT" ).getValue().toString().contains( "thub" )){
				createThubFile(orderCount, search, siteConfig, exportedFiles, salesRepMap);		
			} else if (search.getFileType().equals("csv") && siteConfig.get( "INVOICE_EXPORT" ).getValue().toString().contains( "csv" )){
				createCsvFile(orderCount, search, siteConfig, gSiteConfig, exportedFiles, salesRepMap);		
			} else if (search.getFileType().equals("packnwood") && siteConfig.get( "INVOICE_EXPORT" ).getValue().toString().contains( "packnwood" )){
				createPacknwoodCsvFile(orderCount, search, siteConfig, gSiteConfig, exportedFiles, salesRepMap);		
			} else if (search.getFileType().equals("xml") && siteConfig.get( "INVOICE_EXPORT" ).getValue().toString().contains( "xml" )){
				createXMLFile(orderCount, search, siteConfig, exportedFiles, salesRepMap);		
			}
	        
			if (orderCount > 0) {
				map.put("arguments", orderCount);
				map.put("message", "invoiceexport.success");
			} else {
				map.put("message", "invoiceexport.empty");				
			}
			notifyAdmin(siteConfig, request);
			this.webJaguar.insertImportExportHistory( new ImportExportHistory("invoice", SecurityContextHolder.getContext().getAuthentication().getName(), false ));
		}

		return showForm(request, response, errors, map);
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		OrderSearch form = getOrderSearch( request );
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
	    
		gSALES_REP = (Boolean) gSiteConfig.get("gSALES_REP");
		gSALES_PROMOTIONS = (Boolean) gSiteConfig.get("gSALES_PROMOTIONS");
		gADD_INVOICE = (Boolean) gSiteConfig.get("gADD_INVOICE");
		gPURCHASE_ORDER= (Boolean) gSiteConfig.get("gPURCHASE_ORDER");
		gMULTI_STORE = (Integer) gSiteConfig.get("gMULTI_STORE");
		gPRODUCT_FIELDS = (Integer) gSiteConfig.get("gPRODUCT_FIELDS");
		String fileType = ServletRequestUtils.getStringParameter(request, "fileType2", "xls");
		form.setFileType(fileType);
		
		baseFile = new File(getServletContext().getRealPath("/admin/excel/"));

		File file[] = baseFile.listFiles();
		List<Map<String, Object>> exportedFiles = new ArrayList<Map<String, Object>>();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith("invoiceExport") & file[f].getName().endsWith("." + form.getFileType())) {
				HashMap<String, Object> fileMap = new HashMap<String, Object>();
				fileMap.put("file", file[f]);
				fileMap.put("lastModified", new Date(file[f].lastModified()));
				exportedFiles.add( fileMap );
			}
		}
		request.setAttribute("exportedFiles", exportedFiles);
		return form;
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder( request, binder );
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, true, 10 ) );		
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		OrderSearch search = (OrderSearch) command;
		// fileType2 coming from menu
		if (request.getParameter("fileType2") != null && request.getParameter("__new") == null) {
			search.setFileType(request.getParameter("fileType2"));
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		if (!baseFile.canWrite()) {
			if (search.getFileType().equals("xls")) {
				map.put("message", "excelfile.notWritable");						
			} else {
				map.put("message", "file.notWritable");										
			}
		}

		return map;
	}
	
    private HSSFCell getCell(HSSFSheet sheet, int row, short col) {
    	return sheet.createRow(row).createCell(col);
    }
    
    private void setText(HSSFCell cell, String text) {
    	cell.setCellValue(text);
    }
    
    private void createExcelFiles(int orderCount, OrderSearch search, Map<String, Configuration> siteConfig,
    		Map<String, Object> gSiteConfig, List<Map<String, Object>> exportedFiles, Map <Integer, SalesRep> salesRepMap) throws Exception {
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	String fileName = "/invoiceExport_" + dateFormatter.format(new Date()) + "";
    	
		int limit = 3000;
		search.setLimit(limit);
    	
    	for (int offset=0, excel = 1; offset<orderCount; excel++) {
	    	int start = offset + 1;
	    	int end = (offset + limit) < orderCount ? (offset + limit) : orderCount;
	    	List<Order> orderList = this.webJaguar.getInvoiceExportList(search, siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());
	    	HSSFWorkbook wb = createWorkbook(orderList, start + " to " + end, salesRepMap, siteConfig, gSiteConfig);
	    	// Write the output to a file
	    	File newFile = new File(baseFile.getAbsolutePath() + fileName + excel + ".xls");
	        FileOutputStream fileOut = new FileOutputStream(newFile);
	        wb.write(fileOut);
	        fileOut.close();
	        HashMap<String, Object> fileMap = new HashMap<String, Object>();
	        fileMap.put("file", newFile);
			exportedFiles.add( fileMap );    
	        offset = offset + limit;
	        search.setOffset(offset);
    	}
    }
    
    private HSSFWorkbook createWorkbook(List<Order> list, String sheetName, Map <Integer, SalesRep> salesRepMap, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) throws Exception {
    	HSSFWorkbook wb = new HSSFWorkbook();
    	HSSFSheet sheet = wb.createSheet(sheetName);

		Map<String, Object> regionMap = this.webJaguar.getRegionMap();
    	Map<Integer, String> supplierNameMap = this.webJaguar.getSupplierNameMap(null);
 	
    	// product fields
		List<ProductField> productFields = null;
		if( siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue() != null && siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue().equals("true") ) {
			productFields = this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
			Iterator pfIter = productFields.iterator();
			while ( pfIter.hasNext() )
			{
				ProductField productField = (ProductField) pfIter.next();
				if ( !productField.isEnabled() || !productField.isShowOnInvoiceExport())
				{
					pfIter.remove();
				}
			}
		}
		
		// Customer Fields
		List<CustomerField> customerFields  = null;
		if( siteConfig.get("CUSTOMER_FIELDS_ON_INVOICE_EXPORT").getValue() != null && siteConfig.get("CUSTOMER_FIELDS_ON_INVOICE_EXPORT").getValue().equals("true") ) {
			customerFields  = this.webJaguar.getCustomerFields();
			Iterator<CustomerField> iter = customerFields.iterator();
			while ( iter.hasNext() ) {
				CustomerField customerField = iter.next();
				if (!customerField.isEnabled() || !customerField.isShowOnInvoiceExport()) {
					iter.remove();
				}
			}
		}
		// create the headers
		Map<String, Short> headerMap = new HashMap<String, Short>();
		short col = 0;
		headerMap.put(getMessageSourceAccessor().getMessage("orderNumber"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("orderType"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("orderDate"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("orderedMonth"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("orderedQTR"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("orderedYear"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("timeOrdered"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("email"), col++);
		headerMap.put("customerId", col++);
		headerMap.put("Type", col++);
		if(customerFields != null && customerFields.size() > 0) {
			for(CustomerField field : customerFields){
				headerMap.put("customer_field_"+field.getId(), col);	
				setText(getCell(sheet, 1, col++), field.getName());				
			}
		}
		headerMap.put("customerRating1", col++);
		headerMap.put("customerRating2", col++);
		headerMap.put("cardId", col++);
		headerMap.put(getMessageSourceAccessor().getMessage("productSku"), col++);
		if ((Boolean) gSiteConfig.get("gMASTER_SKU")) {
			headerMap.put(getMessageSourceAccessor().getMessage("parentSku"), col++);
		}
		headerMap.put("Primary Supplier", col++);
		headerMap.put("Desc", col++);
		headerMap.put("Options", col++);
		if(productFields != null && productFields.size() > 0) {
			for(ProductField field : productFields){
				headerMap.put("product_field_"+field.getId(), col);	
				setText(getCell(sheet, 1, col++), field.getName());				
			}
		}
		headerMap.put(getMessageSourceAccessor().getMessage("quantity"), col++);	
		headerMap.put(getMessageSourceAccessor().getMessage("content"), col++);	
		headerMap.put(getMessageSourceAccessor().getMessage("originalPrice"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("discount"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("dis-percentage"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("dis-amountTotal"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("productPrice"), col++);
		if(gPURCHASE_ORDER && (Boolean) siteConfig.get("SHOW_COST").getValue().equals("true")) { headerMap.put(getMessageSourceAccessor().getMessage("cost"), col++); }
		headerMap.put(getMessageSourceAccessor().getMessage("subTotal"), col++);
		if( (Boolean) gSiteConfig.get("gBUDGET") && siteConfig.get("BUDGET_ADD_PARTNER").getValue().equalsIgnoreCase("true")) {
			headerMap.put(getMessageSourceAccessor().getMessage("earnedCredits"), col++);
		}
		headerMap.put(getMessageSourceAccessor().getMessage("poNumber"), col++);
		if (gSALES_PROMOTIONS) { headerMap.put(getMessageSourceAccessor().getMessage("discountAmount"), col++); }
		if (siteConfig.get( "NO_COMMISSION_FLAG" ).getValue().equals( "true" ) ) {
			headerMap.put(getMessageSourceAccessor().getMessage("nc"), col++);
		}
		headerMap.put(getMessageSourceAccessor().getMessage("grandTotal"), col++);
		// credit card
		if (siteConfig.get( "CREDIT_CARD_ON_INVOICE_EXPORT" ).getValue().equals( "true" ) ) {
			headerMap.put(getMessageSourceAccessor().getMessage("creditCardType"), col++);
			headerMap.put(getMessageSourceAccessor().getMessage("creditCardNumber"), col++);
			headerMap.put(getMessageSourceAccessor().getMessage("creditCardCode"), col++);
			headerMap.put(getMessageSourceAccessor().getMessage("creditCardExpireDate"), col++);
		}
		// buySafe
    	if (siteConfig.get( "BUY_SAFE_URL" ).getValue().equals( "https://api.buysafe.com/BuysafeWS/CheckoutAPI.dll" ) ) {
    		headerMap.put(getMessageSourceAccessor().getMessage("buySafeBondCost"), col++);
    	}
		headerMap.put(getMessageSourceAccessor().getMessage("tax"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("taxRate"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("ccFee"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("ccFeeRate"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("shippingHandling"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("shippingTitle"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("merchandiseSalesValue"), col++);
		if (siteConfig.get( "CUSTOM_SHIPPING_FLAG" ).getValue() != "") {
			headerMap.put(getMessageSourceAccessor().getMessage("customShipping"), col++);
			headerMap.put(getMessageSourceAccessor().getMessage("customShippingTitle"), col++);
		}
		headerMap.put(getMessageSourceAccessor().getMessage("paymentMethod"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("note"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("status"), col++);
		if (!siteConfig.get( "SUB_STATUS" ).getValue().equals( "0" ) ) {
			headerMap.put(getMessageSourceAccessor().getMessage("subStatus"), col++);
		}
		if (gADD_INVOICE) { headerMap.put(getMessageSourceAccessor().getMessage("backEndOrder"), col++); }
		headerMap.put(getMessageSourceAccessor().getMessage("dateShipped"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("shippedMonth"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("shippedQTR"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("shippedYear"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("timeShipped"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("trackcode"), col++);
		headerMap.put(getMessageSourceAccessor().getMessage("qualifier"), col++);
		if (gMULTI_STORE > 0) {
			headerMap.put(getMessageSourceAccessor().getMessage("multiStore"), col++);
		}
		if (gSALES_REP) { 
			headerMap.put(getMessageSourceAccessor().getMessage("salesRep"), col++); 
			headerMap.put(getMessageSourceAccessor().getMessage("customerSalesRep"), col++); 
			headerMap.put(getMessageSourceAccessor().getMessage("processedBy"), col++); 
		}
		// credit used
		headerMap.put("creditUsed", col++);

		if (gSALES_PROMOTIONS) { headerMap.put(getMessageSourceAccessor().getMessage("promoCode"), col++); }
		headerMap.put(siteConfig.get( "ORDER_FLAG1_NAME" ).getValue(), col++);
		// billing info headers
		col = addressHeader("Billing ", headerMap, col);
		// shipping info headers
		col = addressHeader("Shipping ", headerMap, col);
		
		HSSFCellStyle dateCellStyle = wb.createCellStyle();
		dateCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yy");
		
		HSSFCellStyle timeCellStyle = wb.createCellStyle();
		timeCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("h:mm:ss AM/PM"));
		SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm:ss a");
		
		HSSFCellStyle wrapTrueStyle = wb.createCellStyle();
		wrapTrueStyle.setWrapText(true);
		
		// print headers
		for (String headerName:headerMap.keySet()){
			setText(getCell(sheet, 0, headerMap.get(headerName).shortValue()), headerName); 
		}
		
		HSSFCell cell = null;
    	
		int row = 2;
		
		// create the rows
    	for (Order order: list) { 
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("orderNumber")).shortValue()), order.getOrderId().toString());
    		Calendar orderDateCalendar = new GregorianCalendar();
    		orderDateCalendar.setTime( order.getDateOrdered() );
    		
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("orderType")).shortValue()), (order.getOrderType() == null) ? "" : order.getOrderType() );    			 
    		// date ordered
    		cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("orderDate")).shortValue());
    		cell.setCellStyle(dateCellStyle);
    		cell.setCellValue(sdfDate.format( order.getDateOrdered()));  

    		// date month 
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("orderedMonth")).shortValue()), getMonth(orderDateCalendar));    			 
    		// date QRT
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("orderedQTR")).shortValue()), getQuarter(orderDateCalendar));    			 
    		// date Year
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("orderedYear")).shortValue()), ""+orderDateCalendar.get( orderDateCalendar.YEAR ));    			  
    		
    		// time ordered
    		cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("timeOrdered")).shortValue());
    		cell.setCellStyle(timeCellStyle);
    		cell.setCellValue(sdfTime.format( order.getDateOrdered().getTime()));  
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("email")).shortValue()), order.getUserEmail());		
    		setText(getCell(sheet, row, headerMap.get("customerId").shortValue()), order.getUserId()+"");
    		Class<Customer> customer = Customer.class;
    		Method method = null;
    		Customer user = this.webJaguar.getCustomerById(order.getUserId());
    		// customer fields
    		if(customerFields != null && customerFields.size() > 0) {
				for(CustomerField field : customerFields){
					method = customer.getMethod("getField"+field.getId());
        			setText(getCell(sheet, row, headerMap.get("customer_field_"+field.getId()).shortValue()), (method.invoke(user) == null ? "" : method.invoke(user).toString()));
        		}
        	}
    		
    		setText(getCell(sheet, row, headerMap.get("Type").shortValue()), "Summary");
    		// sub total
  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("subTotal")).shortValue()); 		    
  			cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
	    	cell.setCellValue(order.getSubTotal());
	    	// Budget Earned Credits
	    	if( (Boolean) gSiteConfig.get("gBUDGET") && siteConfig.get("BUDGET_ADD_PARTNER").getValue().equalsIgnoreCase("true")) {
				cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("earnedCredits")).shortValue()); 		    
				cell.setCellValue(order.getBudgetEarnedCredits() == null ? "" : order.getBudgetEarnedCredits().toString());
			}
	    	// Po Number
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("poNumber")).shortValue()), order.getPurchaseOrder());
    		// discount Amount
	    	if (gSALES_PROMOTIONS) {
	    		cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("discountAmount")).shortValue()); 		    
	  			cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
	  			order.setPromoAmount();
		    	cell.setCellValue(order.getPromoAmount());
    		}
	    	if (siteConfig.get( "NO_COMMISSION_FLAG" ).getValue().equals( "true" ) ) {
	    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("nc")).shortValue()), ( order.getNc() ) ? "1" : null );
			}
	    	// grand total
  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("grandTotal")).shortValue()); 		    
  			cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
	    	cell.setCellValue(order.getGrandTotal());
	    	// credit Card
	    	if (siteConfig.get( "CREDIT_CARD_ON_INVOICE_EXPORT" ).getValue().equals( "true" ) && order.getCreditCard() != null ) {
		    	setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("creditCardType")).shortValue()), order.getCreditCard().getType());
		    	setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("creditCardNumber")).shortValue()), order.getCreditCard().getNumber());
		    	setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("creditCardCode")).shortValue()), order.getCreditCard().getCardCode());
		    	setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("creditCardExpireDate")).shortValue()), order.getCreditCard().getExpireMonth()+"/"+order.getCreditCard().getExpireYear());
			}
	    	// buySafe
	    	if (siteConfig.get( "BUY_SAFE_URL" ).getValue().equals( "https://api.buysafe.com/BuysafeWS/CheckoutAPI.dll" ) ) {
	    		if (order.getBondCost() != null) {
	    			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("buySafeBondCost")).shortValue()); 		    
	      			cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
	    	    	cell.setCellValue(order.getBondCost());
	    		}
	    	}
    		// tax
    		if (order.getTax() != null) {
  	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("tax")).shortValue()); 		    
  	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	    		cell.setCellValue(order.getTax());
    		}
    		// tax rate
    		if (order.getTaxRate() != null) {
  	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("taxRate")).shortValue()); 		    
  	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	    		cell.setCellValue(order.getTaxRate());
    		}
    		// ccFee
    		if (order.getCcFee() != null) {
  	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("ccFee")).shortValue()); 		    
  	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	    		cell.setCellValue(order.getCcFee());
    		}
    		// ccFee rate
    		if (order.getCcFeeRate() != null) {
  	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("ccFeeRate")).shortValue()); 		    
  	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	    		cell.setCellValue(order.getCcFeeRate());
    		}
    		// shipping & handling
    		if (order.getShippingCost() != null) {
  	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("shippingHandling")).shortValue()); 		    
  	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	    		cell.setCellValue(order.getShippingCost());
    		}
    		// Merchandise Sales Value
    		if(order.getGrandTotal() != null){
    			Double ccFee = order.getCcFee() == null ? 0 : order.getCcFee();
    			Double tax = order.getTax() == null ? 0 : order.getTax();
    			Double shippingCost = order.getShippingCost() == null ? 0 : order.getShippingCost();
    			Double merchandiseSales = (order.getGrandTotal() - tax - ccFee - shippingCost);
    			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("merchandiseSalesValue")).shortValue());
    			cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
    			cell.setCellValue(merchandiseSales);
    		}
    		
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("shippingTitle")).shortValue()), order.getShippingMethod());  
    		// custom Shipping
    		if (siteConfig.get( "CUSTOM_SHIPPING_FLAG" ).getValue() != "") {
    			if (order.getCustomShippingCost() != null) {
  	  				cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("customShipping")).shortValue()); 		    
  	  				cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	  				cell.setCellValue(order.getCustomShippingCost());
    			}
    			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("customShippingTitle")).shortValue()), "");
    		}
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("paymentMethod")).shortValue()), order.getPaymentMethod());
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("note")).shortValue()), order.getInvoiceNote());
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("status")).shortValue()), getMessageSourceAccessor().getMessage("orderStatus_" + order.getStatus()));
    		if (!siteConfig.get( "SUB_STATUS" ).getValue().equals( "0" ) ) {
    			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("subStatus")).shortValue()), getMessageSourceAccessor().getMessage("orderSubStatus_" + order.getSubStatus()));
    		}
    		if (gADD_INVOICE) {
    			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("backEndOrder")).shortValue()), ( order.isBackendOrder()  ? "1" : null ));
    		}
    		if ( order.getShipped() != null) {
    			Calendar orderShippedDateCalendar = new GregorianCalendar();
        		orderShippedDateCalendar.setTime( order.getShipped() );
        		
        		// date shipped
    			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("dateShipped")).shortValue());
        		cell.setCellStyle(dateCellStyle);
        		cell.setCellValue(sdfDate.format(order.getShipped().getTime())); 
        		
        		// shipped month
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("shippedMonth")).shortValue()), getMonth(orderShippedDateCalendar));    			 
        		// shipped QRT
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("shippedQTR")).shortValue()), getQuarter(orderShippedDateCalendar));    			 
        		// shipped Year
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("shippedYear")).shortValue()), ""+orderShippedDateCalendar.get( orderShippedDateCalendar.YEAR ));    			 
     		
        		// time shipped
        		cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("timeShipped")).shortValue());
        		cell.setCellStyle(dateCellStyle);
        		cell.setCellValue(sdfTime.format(order.getShipped().getTime()));
    		}
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("trackcode")).shortValue()), order.getTrackcode());  
    		String qualifier = "";
    		if(order.getQualifier() != null){
    			if(salesRepMap.get(order.getQualifier())!=null){
    				qualifier = salesRepMap.get(order.getQualifier()).getName();
    			}
    		}
    		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("qualifier")).shortValue()), qualifier); 
    		// multi store
    		if (gMULTI_STORE > 0) {
    			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("multiStore")).shortValue()), (order.getHost() == null) ? "" : order.getHost());
    		}
    		// Sales Rep
    		if (gSALES_REP) {
    			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("salesRep")).shortValue()), ( order.getSalesRepId() == null ) ? "" : salesRepMap.get( order.getSalesRepId() ).getName()); 
    			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("customerSalesRep")).shortValue()), ( user.getSalesRepId() == null ) ? "" : salesRepMap.get( user.getSalesRepId() ).getName()); 
				setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("processedBy")).shortValue()), ( order.getSalesRepProcessedById() == null ) ? "" : salesRepMap.get( order.getSalesRepProcessedById() ).getName()); 
    		}
  		    // Credit Used
  		    if (order.getCreditUsed() != null) {
  	  			cell = getCell(sheet, row, headerMap.get("creditUsed").shortValue()); 		    
  	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	    		cell.setCellValue(order.getCreditUsed());           	  		    	
  		    }
    		// Promotion Code
    		if (gSALES_PROMOTIONS) {
    			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("promoCode")).shortValue()), ( order.getPromo() == null ) ? "" : order.getPromo().getTitle()); 
    		}
    		setText(getCell(sheet, row, headerMap.get(siteConfig.get( "ORDER_FLAG1_NAME" ).getValue()).shortValue()), (order.getFlag1() == null) ? "" : order.getFlag1().toString());    			 
    		// billing info
    		addressInfo(order.getBilling(), "Billing ", sheet, row, headerMap, regionMap);
    		// shipping info
    		addressInfo(order.getShipping(), "Shipping ", sheet, row, headerMap, regionMap);
    		
    		row++;
    		Class<Product> product = Product.class;
    		Method m = null;  
    		// line items
    		for (LineItem lineItem : order.getLineItems()) {
    			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("orderNumber")).shortValue()), order.getOrderId().toString());
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("orderType")).shortValue()), (order.getOrderType() == null) ? "" : order.getOrderType() ); 
        		// date ordered
        		cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("orderDate")).shortValue());
        		cell.setCellStyle(dateCellStyle);
        		cell.setCellValue(sdfDate.format( order.getDateOrdered().getTime()));  
        		// date month 
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("orderedMonth")).shortValue()), getMonth(orderDateCalendar));    			 
        		// date QRT
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("orderedQTR")).shortValue()), getQuarter(orderDateCalendar));    			 
        		// date Year
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("orderedYear")).shortValue()), ""+orderDateCalendar.get( orderDateCalendar.YEAR ));
        		// time ordered
        		cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("timeOrdered")).shortValue());
        		cell.setCellStyle(timeCellStyle);
        		cell.setCellValue(sdfTime.format( order.getDateOrdered().getTime()));  
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("email")).shortValue()), order.getUserEmail());		
        		setText(getCell(sheet, row, headerMap.get("customerId").shortValue()), order.getUserId()+"");
        		setText(getCell(sheet, row, headerMap.get("Type").shortValue()), "Item");
        		setText(getCell(sheet, row, headerMap.get("customerRating1").shortValue()), user.getRating1());
        		setText(getCell(sheet, row, headerMap.get("customerRating2").shortValue()), user.getRating2());
        		setText(getCell(sheet, row, headerMap.get("cardId").shortValue()), user.getCardId());
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("productSku")).shortValue()), lineItem.getProduct().getSku());
        		if ((Boolean) gSiteConfig.get("gMASTER_SKU")) {
        			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("parentSku")).shortValue()), lineItem.getProduct().getMasterSku());
        		}
        		// Primary Supplier
    			setText(getCell(sheet, row, headerMap.get("Primary Supplier").shortValue()), lineItem.getSupplier() != null ? supplierNameMap.get((long) lineItem.getSupplier().getId()) : "");
        		setText(getCell(sheet, row, headerMap.get("Desc").shortValue()), lineItem.getProduct().getName());
        		// attributes
    			if (lineItem.getProductAttributes()!= null) {
    				StringBuffer options = new StringBuffer();
	        		for (ProductAttribute attributes : lineItem.getProductAttributes()) {
	        			options.append(attributes.getOptionName() + ": " +attributes.getValueName() + "\012");
	        		}
	        		cell = getCell(sheet, row, headerMap.get("Options").shortValue());
	        		cell.setCellStyle(wrapTrueStyle);
	        		cell.setCellValue(options.toString());
    			}	
        		// product fields
        		if(productFields != null && productFields.size() > 0) {
    				lineItem.setProductFields(productFields);
    				for(ProductField field : lineItem.getProductFields()){
            			m = product.getMethod("getField"+field.getId());
            			setText(getCell(sheet, row, headerMap.get("product_field_"+field.getId()).shortValue()), (m.invoke(lineItem.getProduct()) == null ? "" : m.invoke(lineItem.getProduct()).toString()));
            		}
            	}
        		getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("quantity")).shortValue()).setCellValue(lineItem.getQuantity());
            	if (lineItem.getProduct().getCaseContent() != null) {
            		getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("content")).shortValue()).setCellValue(lineItem.getProduct().getCaseContent());
        		}
            	// set original price
      		    if (lineItem.getOriginalPrice() != null) {
      	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("originalPrice")).shortValue()); 		    
      	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    		cell.setCellValue(lineItem.getOriginalPrice());           	  		    	
      		    }
      		    // set discount
      		    if (lineItem.getDiscount() != null) {
      	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("discount")).shortValue()); 		    
      	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    		cell.setCellValue(lineItem.getDiscount());           	  		    	
      		    }
      		    // set discount type
      		    if (lineItem.getDiscount() != null) {
      		    	setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("dis-percentage")).shortValue()), ( lineItem.isPercent() ) ? "1" : null );		    	
      		    }
      		    // set discount total
      		    if(lineItem.getDiscount() != null && lineItem.getOriginalPrice() != null && lineItem.getUnitPrice() != null){
      		    	Double disAmountTotal;
      		    	if(lineItem.isPercent()){
      		    		disAmountTotal=lineItem.getQuantity()*(lineItem.getOriginalPrice() - lineItem.getUnitPrice());
      		    	}else{
      		    		disAmountTotal=lineItem.getQuantity()*(lineItem.getOriginalPrice() - lineItem.getUnitPrice());
      		    	}
      		    	cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("dis-amountTotal")).shortValue()); 		    
      	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    		cell.setCellValue(disAmountTotal);
      		    }
      			// set final price
      		    if (lineItem.getUnitPrice() != null) {
      	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("productPrice")).shortValue()); 		    
      	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    		cell.setCellValue(lineItem.getUnitPrice());           	  		    	
      		    }
      		    
      		    //set cost
      		    if (gPURCHASE_ORDER && (Boolean) siteConfig.get("SHOW_COST").getValue().equals("true") && lineItem.getUnitCost() != null) {
      	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("cost")).shortValue()); 		    
      	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    		cell.setCellValue(lineItem.getUnitCost());           	  		    	
      		    }
      			// total price
      		    if (lineItem.getTotalPrice() != null) {
      	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("subTotal")).shortValue()); 		    
      	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    		cell.setCellValue(lineItem.getTotalPrice());           	  		    	
      		    }
      		    // Po Number
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("poNumber")).shortValue()), order.getPurchaseOrder());    			

      		    if (siteConfig.get( "NO_COMMISSION_FLAG" ).getValue().equals( "true" ) ) {
  	    		   setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("nc")).shortValue()), ( order.getNc() ) ? "1" : null );
  			    }
      		    // grand total
      			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("grandTotal")).shortValue()); 
      			cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
    	    	cell.setCellValue(order.getGrandTotal());
        		// tax 
        		if (order.getTaxRate() != null && lineItem.getProduct().isTaxable() && lineItem.getTotalPrice() != null) {
      	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("tax")).shortValue()); 		    
      	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    		cell.setCellValue(order.getTaxRate()*lineItem.getTotalPrice()/100);
      	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("taxRate")).shortValue()); 		    
      	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    		cell.setCellValue(order.getTaxRate());
        		}
        		// ccFee
        		if (order.getCcFee() != null) {
      	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("ccFee")).shortValue()); 		    
      	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    		cell.setCellValue(order.getCcFee());
        		}
        		// ccFee rate
        		if (order.getCcFeeRate() != null) {
      	  			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("ccFeeRate")).shortValue()); 		    
      	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    		cell.setCellValue(order.getCcFeeRate());
        		}
        		// custom Shipping
        		if (siteConfig.get( "CUSTOM_SHIPPING_FLAG" ).getValue() != "") {
        			if (lineItem.getCustomShippingCost() != null) {
      	  				cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("customShipping")).shortValue()); 		    
      	  				cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	  				cell.setCellValue(lineItem.getCustomShippingCost());
        			}
        			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("customShippingTitle")).shortValue()), (lineItem.getCustomShippingCost() != null) ? order.getCustomShippingTitle() : "");
        		}
      		   
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("paymentMethod")).shortValue()), order.getPaymentMethod());
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("status")).shortValue()), getMessageSourceAccessor().getMessage("orderStatus_" + order.getStatus()));
        		if (!siteConfig.get( "SUB_STATUS" ).getValue().equals( "0" ) ) {
        			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("subStatus")).shortValue()), getMessageSourceAccessor().getMessage("orderSubStatus_" + order.getSubStatus()));
        		}
        		if (gADD_INVOICE) {
        			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("backEndOrder")).shortValue()), ( order.isBackendOrder()  ? "1" : null ));
        		}
        		if ( order.getShipped() != null) {
        			Calendar orderShippedDateCalendar = new GregorianCalendar();
            		orderShippedDateCalendar.setTime( order.getShipped() );
            		
        			// date shipped
        			cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("dateShipped")).shortValue());
            		cell.setCellStyle(dateCellStyle);
            		cell.setCellValue(sdfDate.format(order.getShipped().getTime())); 
            		// shipped month
            		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("shippedMonth")).shortValue()), getMonth(orderShippedDateCalendar));    			 
            		// shipped QRT
            		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("shippedQTR")).shortValue()), getQuarter(orderShippedDateCalendar));    			 
            		// shipped Year
            		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("shippedYear")).shortValue()), ""+orderShippedDateCalendar.get( orderShippedDateCalendar.YEAR ));    			 

            		// time shipped
            		cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("timeShipped")).shortValue());
            		cell.setCellStyle(dateCellStyle);
            		cell.setCellValue(sdfTime.format(order.getShipped().getTime()));
        		}
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("trackcode")).shortValue()), order.getTrackcode()); 
        		qualifier = "";
        		if(order.getQualifier() != null){
        			if(salesRepMap.get(order.getQualifier())!=null){
        				qualifier = salesRepMap.get(order.getQualifier()).getName();
        			}
        		}
        		setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("qualifier")).shortValue()), qualifier);
        		// Sales Rep
        		if (gSALES_REP) {
        			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("salesRep")).shortValue()), ( order.getSalesRepId() == null ) ? "" : salesRepMap.get( order.getSalesRepId() ).getName()); 
        			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("customerSalesRep")).shortValue()), ( user.getSalesRepId() == null ) ? "" : salesRepMap.get( user.getSalesRepId() ).getName()); 
        			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("processedBy")).shortValue()), ( order.getSalesRepProcessedById() == null ) ? "" : salesRepMap.get( order.getSalesRepProcessedById() ).getName()); 
        		}
        		// Promotion Code
        		if (gSALES_PROMOTIONS) {
        			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("promoCode")).shortValue()), ( order.getPromo() == null ) ? "" : order.getPromo().getTitle()); 
        		}
        		setText(getCell(sheet, row, headerMap.get(siteConfig.get( "ORDER_FLAG1_NAME" ).getValue()).shortValue()), (order.getFlag1() == null) ? "" : order.getFlag1().toString());    			 			 
        		// billing info
        		addressInfo(order.getBilling(), "Billing ", sheet, row, headerMap, regionMap);
        		// shipping info
        		addressInfo(order.getShipping(), "Shipping ", sheet, row, headerMap, regionMap);
        		
        		row++;
    		}
    		
    		row++;
    	} // for	  
    	return wb;
    }
    
    private void addressInfo(Address address, String prefix, HSSFSheet sheet, int row, Map<String, Short> headerMap, Map<String, Object> regionMap) {
    	
    	Map<String, String> region = new HashMap<String, String>();    	
		if(address.getCountry() != null && !address.getCountry().isEmpty() && address.getStateProvince() != null &&  !address.getStateProvince().isEmpty()) { 
	    	region = (Map<String, String>) regionMap.get(address.getCountry() + "_" + address.getStateProvince());
    	} if (region == null && address.getCountry() != null && !address.getCountry().isEmpty()) {
	    	region = (Map<String, String>) regionMap.get(address.getCountry() + "_");
    	}
    	
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("firstName")).shortValue()), address.getFirstName());
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("lastName")).shortValue()), address.getLastName());
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("company")).shortValue()), address.getCompany());
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("nationalRegion")).shortValue()),(address.getCountry() == null || region == null) ? "" : region.get("nationalRegion")); 
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("country")).shortValue()), address.getCountry());
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("countryFullName")).shortValue()), (address.getCountry() == null || region == null) ? "" : region.get("countryName"));		
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("address")  + " 1").shortValue()), address.getAddr1());
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("address")  + " 2").shortValue()), address.getAddr2());
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("city")).shortValue()), address.getCity());
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("stateRegion")).shortValue()), (address.getCountry() == null || region == null) ? "" : region.get("stateRegion"));
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("stateProvince")).shortValue()), address.getStateProvince());
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("zipCode")).shortValue()), address.getZip());
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("phone")).shortValue()), address.getPhone());
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("cellPhone")).shortValue()), address.getCellPhone());
		setText(getCell(sheet, row, headerMap.get(prefix + getMessageSourceAccessor().getMessage("fax")).shortValue()), address.getFax());
    }
    
    private short addressHeader(String prefix, Map<String, Short> headerMap, short col) {    
    	
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("firstName"), col++);
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("lastName"), col++);
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("company"), col++);
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("nationalRegion"), col++);
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("country"), col++);	
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("countryFullName"), col++);	
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("address") + " 1", col++);
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("address") + " 2", col++);	
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("city"), col++);
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("stateRegion"), col++);		
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("stateProvince"), col++);	
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("zipCode"), col++);
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("phone"), col++);
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("cellPhone"), col++);
		headerMap.put(prefix + getMessageSourceAccessor().getMessage("fax"), col++);
		return col;
    }

    private void createVafFile(int orderCount, OrderSearch search, Map<String, Configuration> siteConfig,
    		List<Map<String, Object>> exportedFiles) throws Exception {
    	
    	if (orderCount < 1) return;
    	
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	String fileName = "/invoiceExport_" + dateFormatter.format(new Date()) + ".vaf";
    	
    	File exportFile = new File(baseFile, fileName);
    	PrintWriter pw = new PrintWriter(new FileWriter(exportFile));
    	
    	dateFormatter = new SimpleDateFormat("MM/dd/yy kk:mm");
    	pw.println("File created by: webjaguar");
    	pw.println("File started on: " + dateFormatter.format(new Date()));
    	
    	// create the rows
    	for (Order order: this.webJaguar.getInvoiceExportList(search,
    			siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue())) { 
    		
    		Customer customer = this.webJaguar.getCustomerById(order.getUserId());
    		
    		pw.print("H|");
    		if (customer != null) {
        		pw.print(customer.getAccountNumber());    			
    		}
    		pw.println("|R|" + order.getOrderId() + "|0||0");
    		// line items
    		for (LineItem lineItem : order.getLineItems()) {
    			pw.print("D|");
    			pw.print(lineItem.getProduct().getSku());
    			pw.print("|");
        		int totalQty = lineItem.getQuantity();
        		if (lineItem.getProduct().getCaseContent() != null) {
        			totalQty *= lineItem.getProduct().getCaseContent();
        		}
    			pw.print(totalQty);
    			pw.println();
    		}
    	}

    	pw.println("File ended on: " + dateFormatter.format(new Date()));
    	pw.println("End of File");
    	
    	pw.close();
    	HashMap<String, Object> fileMap = new HashMap<String, Object>();
        fileMap.put("file", exportFile);
		exportedFiles.add( fileMap );
    }
    
    private void createThubFile(int orderCount, OrderSearch search, Map<String, Configuration> siteConfig,
    		List<Map<String, Object>> exportedFiles, Map <Integer, SalesRep> salesRepMap) throws Exception {

    	if (orderCount < 1) return;
    	
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	SimpleDateFormat dateFormatter2 = new SimpleDateFormat("MM/dd/yyyy");
    	String fileName = "/invoiceExport_thub_" + dateFormatter.format(new Date()) + ".csv";
    	
    	File exportFile = new File(baseFile, fileName);
    	PrintWriter pw = new PrintWriter(new FileWriter(exportFile));
    	
    	dateFormatter = new SimpleDateFormat("MM/dd/yy kk:mm");
    	
    	pw.print("\"ORDER_NO\",");
    	pw.print("\"STORE_NAME\",");
    	pw.print("\"TXN_DT\",");
    	pw.print("\"CCY_CODE\",");
    	pw.print("\"PAYMENTMETHOD_REF\",");
    	pw.print("\"PHONE\",");
    	pw.print("\"EMAIL\",");
    	// Billing Address
    	pw.print("\"BADDR_FIRST_NM\",");
    	pw.print("\"BADDR_LAST_NM\",");
    	pw.print("\"BADDR_COMPANY\",");
    	pw.print("\"BADDR_LINE1\",");
    	pw.print("\"BADDR_LINE2\",");
    	pw.print("\"BADDR_CITY\",");
    	pw.print("\"BADDR_STATE\",");
    	pw.print("\"BADDR_ZIP\",");
    	pw.print("\"BADDR_COUNTRY\",");
    	// Shipping Address
    	pw.print("\"SADDR_FIRST_NM\",");
    	pw.print("\"SADDR_LAST_NM\",");
    	pw.print("\"SADDR_PHONE\",");
    	pw.print("\"SADDR_EMAIL\",");
    	pw.print("\"SADDR_COMPANY\",");
    	pw.print("\"SADDR_LINE1\",");
    	pw.print("\"SADDR_LINE2\",");
    	pw.print("\"SADDR_CITY\",");
    	pw.print("\"SADDR_STATE\",");
    	pw.print("\"SADDR_ZIP\",");
    	pw.print("\"SADDR_COUNTRY\",");
    	
    	pw.print("\"CreditCardType\",");
    	pw.print("\"CreditCardExpiration\",");
    	pw.print("\"CreditCardName\",");
    	pw.print("\"CreditCardNumber\",");
    	
    	pw.print("\"PO_NUMBER\",");
    	pw.print("\"SALES_REP\",");
    	pw.print("\"SHIP_METHOD_REF\",");
    	pw.print("\"MEMO\",");
    	pw.print("\"TAX1_TOTAL\",");
    	pw.print("\"TOTAL_SHIP_COST\",");
    	
    	pw.print("\"TOTAL_DISCOUNT_AMT\",");
    	pw.print("\"TOTAL_ORDER_AMT\",");
    	
    	pw.print("\"ITEM_NAME\",");
    	pw.print("\"ITEM_QUANTITY\",");
    	pw.print("\"ITEM_RATE\",");
    	pw.print("\"ITEM_AMOUNT\"");

    	pw.println("");
    	
    	// create the rows
    	for (Order order: this.webJaguar.getInvoiceExportList(search,
    			siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue())) { 
    		//Customer customer = this.webJaguar.getCustomerById(order.getUserId());

    		// line items
    		for (LineItem lineItem : order.getLineItems()) {
    			pw.print("\"" + order.getOrderId() + "\",");
        		pw.print("\"" + siteConfig.get( "SITE_NAME" ).getValue() + "\",");
        		pw.print("\"" + dateFormatter2.format( order.getDateOrdered() ) + "\",");
        		pw.print("\"USD\",");
        		pw.print("\"" + order.getPaymentMethod() + "\",");
        		pw.print("\"" + order.getBilling().getPhone() + "\",");
        		pw.print("\"" + order.getUserEmail() + "\",");
        		
        		pw.print("\"" + order.getBilling().getFirstName() + "\",");
        		pw.print("\"" + order.getBilling().getLastName() + "\",");
        		pw.print("\"" + order.getBilling().getCompany() + "\",");
        		pw.print("\"" + order.getBilling().getAddr1() + "\",");
        		pw.print("\"" + order.getBilling().getAddr2() + "\",");
        		pw.print("\"" + order.getBilling().getCity() + "\",");
        		pw.print("\"" + order.getBilling().getStateProvince() + "\",");
        		pw.print("\"" + order.getBilling().getZip() + "\",");
        		pw.print("\"" + order.getBilling().getCountry() + "\",");
        		
        		pw.print("\"" + order.getShipping().getFirstName() + "\",");
        		pw.print("\"" + order.getShipping().getLastName() + "\",");
        		pw.print("\"" + order.getShipping().getPhone() + "\",");
        		pw.print("\"" + order.getShipping().getEmail() + "\",");
        		pw.print("\"" + order.getShipping().getCompany() + "\",");
        		pw.print("\"" + order.getShipping().getAddr1() + "\",");
        		pw.print("\"" + order.getShipping().getAddr2() + "\",");
        		pw.print("\"" + order.getShipping().getCity() + "\",");
        		pw.print("\"" + order.getShipping().getStateProvince() + "\",");
        		pw.print("\"" + order.getShipping().getZip() + "\",");
        		pw.print("\"" + order.getShipping().getCountry() + "\",");
        		
        		pw.print("\"" + (( order.getCreditCard() == null ) ? "" : order.getCreditCard().getType()) + "\",");
        		if ( order.getCreditCard() != null ) {
        			pw.print("\"" + order.getCreditCard().getExpireMonth() + "/01/20" +  order.getCreditCard().getExpireYear() + "\",");
        		} else {
        			pw.print("\"\",");
        		}
        		pw.print("\"" + order.getBilling().getFirstName() + " " + order.getBilling().getLastName() + "\",");
        		pw.print("\"" + (( order.getCreditCard() == null ) ? "" : order.getCreditCard().getNumber()) + "\",");
        		
        		pw.print("\"" + order.getPurchaseOrder() + "\",");
        		pw.print("\"" + (( order.getSalesRepId() == null ) ? "" : salesRepMap.get( order.getSalesRepId() ).getName()) + "\",");
        		pw.print("\"" + order.getShippingMethod() + "\",");
        		pw.print("\"" + order.getInvoiceNote() + "\",");
        		pw.print("\"" + order.getTax() + "\",");
        		pw.print("\"" + order.getShippingCost() + "\",");
        		
        		order.setPromoAmount();
        		pw.print("\"" + order.getPromoAmount() + "\",");
        		pw.print("\"" + order.getGrandTotal() + "\",");

        		pw.print("\"" + lineItem.getProduct().getSku() + "\",");
        		pw.print("\"" + lineItem.getQuantity() + "\",");
        		pw.print("\"" + lineItem.getUnitPrice() + "\",");
        		pw.print("\"" + lineItem.getTotalPrice() + "\"");

    			pw.println();
    		}
    	}

    	pw.println();
    	pw.close();
    	HashMap<String, Object> fileMap = new HashMap<String, Object>();
        fileMap.put("file", exportFile);
		exportedFiles.add( fileMap );
    }
    
    private void createXMLFile (int orderCount, OrderSearch search, Map<String, Configuration> siteConfig,
    		List<Map<String, Object>> exportedFiles, Map <Integer, SalesRep> salesRepMap) throws Exception { 
    	
    	if(orderCount < 1) {
    		return;
    	}
    	
    	List<Order> ordersList = this.webJaguar.getInvoiceExportList(search,siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	
    	String fileName = "/invoiceExport_XML_" + dateFormatter.format(new Date()) + ".xml";
    	File file = new File(baseFile, fileName);
    	PrintWriter pw = new PrintWriter(new FileWriter(file));
    	// product fields
		List<ProductField> productFields = null;
		if( siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue() != null && siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue().equals("true") ) {
			productFields = this.webJaguar.getProductFields(null, gPRODUCT_FIELDS);
    		Iterator pfIter = productFields.iterator();
			while ( pfIter.hasNext() )
			{
				ProductField productField = (ProductField) pfIter.next();
				
	    		if ( !productField.isEnabled() || !productField.isShowOnInvoiceExport())
				{
					pfIter.remove();
				}
			}
		}
    	this.webJaguar.exportXMLInvoices(ordersList, salesRepMap, productFields, pw, null, false);
    	
    	HashMap<String, Object> fileMap = new HashMap<String, Object>();
        fileMap.put("file", file);
		exportedFiles.add( fileMap );	
    }
    
    private void createCsvFile(int orderCount, OrderSearch search, Map<String, Configuration> siteConfig,Map<String, Object> gSiteConfig,
    		List<Map<String, Object>> exportedFiles, Map <Integer, SalesRep> salesRepMap) throws Exception { 
    	
    	if (orderCount < 1) return;
    	Map<Integer, String> supplierNameMap = this.webJaguar.getSupplierNameMap(null);
		
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm:ss a");
    	
		String fileName = "/invoiceExport_" + dateFormatter.format(new Date()) + ".csv";
    	
    	File exportFile = new File(baseFile, fileName);
    	CSVWriter writer = new CSVWriter(new FileWriter(exportFile));
    	List<String> line = new ArrayList<String>();
    	
    	dateFormatter = new SimpleDateFormat("MM/dd/yy kk:mm");
    	
    	// product fields
		List<ProductField> productFields = null;
		if( siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue() != null && siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue().equals("true") ) {
			productFields = this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
			Iterator pfIter = productFields.iterator();
			while ( pfIter.hasNext() )
			{
				ProductField productField = (ProductField) pfIter.next();
				if ( !productField.isEnabled() || !productField.isShowOnInvoiceExport())
				{
					pfIter.remove();
				}
			}
		}
		// Customer Fields
		List<CustomerField> customerFields = null;
		if( siteConfig.get("CUSTOMER_FIELDS_ON_INVOICE_EXPORT").getValue() != null && siteConfig.get("CUSTOMER_FIELDS_ON_INVOICE_EXPORT").getValue().equals("true") ) {
			customerFields = this.webJaguar.getCustomerFields();
			Iterator<CustomerField> iter = customerFields.iterator();
			while ( iter.hasNext() ) {
				CustomerField customerField = iter.next();
				if (!customerField.isEnabled() || !customerField.isShowOnInvoiceExport()) {
					iter.remove();
				}
			}
		}
		
    	//header name
    	line.add(getMessageSourceAccessor().getMessage("orderNumber"));
    	line.add(getMessageSourceAccessor().getMessage("orderType"));
    	line.add(getMessageSourceAccessor().getMessage("orderDate"));
    	line.add(getMessageSourceAccessor().getMessage("orderedMonth"));
    	line.add(getMessageSourceAccessor().getMessage("orderedQTR"));
    	line.add(getMessageSourceAccessor().getMessage("orderedYear"));
    	line.add(getMessageSourceAccessor().getMessage("timeOrdered"));
    	line.add(getMessageSourceAccessor().getMessage("email"));
    	line.add("customerId");
    	// customer fields
    	if(customerFields != null && customerFields.size() > 0) {
			for(CustomerField field : customerFields){
				line.add("customer_field_"+field.getId());
			}
		}
    	line.add("customerRating1");
    	line.add("customerRating2");
    	line.add("cardId");
    	line.add(getMessageSourceAccessor().getMessage("accountCreatedMonth"));
    	line.add(getMessageSourceAccessor().getMessage("accountCreatedQTR"));
    	line.add(getMessageSourceAccessor().getMessage("accountCreatedYear"));
    	line.add(getMessageSourceAccessor().getMessage("trackcode"));
    	line.add(getMessageSourceAccessor().getMessage("qualifier"));
    	line.add(getMessageSourceAccessor().getMessage("type"));
    	line.add(getMessageSourceAccessor().getMessage("productSku"));
    	if ((Boolean) gSiteConfig.get("gMASTER_SKU")) {
    		line.add(getMessageSourceAccessor().getMessage("parentSku"));
		}
    	line.add("Primary Supplier");
    	line.add(getMessageSourceAccessor().getMessage("productShortDesc"));
    	// product fields
    	if(productFields != null && productFields.size() > 0) {
			for(ProductField field : productFields){
				line.add("product_field_"+field.getId());
			}
		}
		line.add(getMessageSourceAccessor().getMessage("quantity"));
    	line.add(getMessageSourceAccessor().getMessage("content"));
    	line.add(getMessageSourceAccessor().getMessage("originalPrice"));
    	line.add(getMessageSourceAccessor().getMessage("discount"));
    	line.add(getMessageSourceAccessor().getMessage("dis-percentage"));
    	line.add(getMessageSourceAccessor().getMessage("dis-amountTotal"));
    	line.add(getMessageSourceAccessor().getMessage("productPrice"));
    	line.add(getMessageSourceAccessor().getMessage("subTotal"));
    	if (gSALES_PROMOTIONS) {line.add(getMessageSourceAccessor().getMessage("discountAmount"));}
    	if (siteConfig.get( "NO_COMMISSION_FLAG" ).getValue().equals( "true" ) ) {
    		line.add(getMessageSourceAccessor().getMessage("nc"));
    	}/*
    	if((Boolean) gSiteConfig.get("gBUDGET") && siteConfig.get("BUDGET_ADD_PARTNER").getValue().equalsIgnoreCase("true")) {
        	line.add(getMessageSourceAccessor().getMessage("earnedCredits"));
    	}*/
    	line.add(getMessageSourceAccessor().getMessage("grandTotal"));
    	line.add(getMessageSourceAccessor().getMessage("tax"));
    	line.add(getMessageSourceAccessor().getMessage("taxRate"));
    	line.add(getMessageSourceAccessor().getMessage("ccFee"));
    	line.add(getMessageSourceAccessor().getMessage("ccFeeRate"));
    	line.add(getMessageSourceAccessor().getMessage("merchandiseSalesValue"));
    	line.add(getMessageSourceAccessor().getMessage("shippingHandling"));
    	line.add(getMessageSourceAccessor().getMessage("shippingTitle"));
    	line.add(getMessageSourceAccessor().getMessage("paymentMethod"));
    	line.add(getMessageSourceAccessor().getMessage("status"));
    	if (gADD_INVOICE) {line.add(getMessageSourceAccessor().getMessage("backEndOrder"));}
    	line.add(getMessageSourceAccessor().getMessage("dateShipped"));
    	line.add(getMessageSourceAccessor().getMessage("shippedMonth"));
    	line.add(getMessageSourceAccessor().getMessage("shippedQTR"));
    	line.add(getMessageSourceAccessor().getMessage("shippedYear"));
    	line.add(getMessageSourceAccessor().getMessage("timeShipped"));
    	line.add(getMessageSourceAccessor().getMessage("trackcode"));
    	
    	line.add(getMessageSourceAccessor().getMessage("qualifier"));
    	if (gSALES_REP) { 
    		line.add(getMessageSourceAccessor().getMessage("salesRep"));
    		line.add(getMessageSourceAccessor().getMessage("customerSalesRep"));
        	line.add(getMessageSourceAccessor().getMessage("processedBy"));
    	}
    	// credit used
    	line.add("creditUsed");
    	if (gSALES_PROMOTIONS) {line.add(getMessageSourceAccessor().getMessage("promoCode"));}
    	
    	line.add(siteConfig.get( "ORDER_FLAG1_NAME" ).getValue());
    	// billing info headers
		addressHeaderCSV("Billing ", line);
		// shipping info headers
		addressHeaderCSV("Shipping ", line);
    	
		writer.writeNext(line.toArray(new String[line.size()]));
    	
		line = new ArrayList<String>();
		
		List<String> list = Arrays.asList("","","","","","","","",""); // OrderNumber,type,date,month,qtr,year,time,email,id
    	line.addAll( list );
    	// customer fields
    	if(customerFields != null && customerFields.size() > 0) {
			for(CustomerField field : customerFields){
				line.add("(" + field.getName() + ")");
			}
		}
    	line.add( "" ); // CustomerRating 1
    	line.add( "" ); // CustomerRating 2
    	line.add( "" ); // CardId
    	line.add( "" ); // Account Created Month
    	line.add( "" ); // Account Created QTR
    	line.add( "" ); // Account Created Year
    	line.add( "" ); // Track Code
    	line.add( "" ); // Type
    	line.add( "" ); // Sku
    	line.add( "" ); // Parent Sku
    	line.add( "" ); // Primary Supplier
    	line.add( "" ); // Short Desc
    	// product fields
    	if(productFields != null && productFields.size() > 0) {
			for(ProductField field : productFields){
				line.add(field.getName());
			}
		}
    	line.add(""); // Quantity
    	line.add(""); // Content
    	line.add(""); // Original Price
    	line.add(""); // Discount
    	line.add(""); // Discount-Percentage
    	line.add(""); // Price
    	line.add(""); // Sub-Total
    	if (gSALES_PROMOTIONS) {line.add("");} // Discount-Amount
    	if (siteConfig.get( "NO_COMMISSION_FLAG" ).getValue().equals( "true" ) ) {
    		line.add(""); // No Commission
        }
    	line.add(""); // Grand Total
    	line.add(""); // Tax
    	line.add(""); // Tax Rate
    	line.add(""); // Credit Card Fee
    	line.add(""); // Credit Card Fee Rate
    	line.add("");
    	line.add("");
    	line.add("");
    	line.add("");
    	if (gADD_INVOICE) {line.add("");}
    	line.add("");
    	line.add("");
    	line.add("");
    	line.add("");
    	line.add("");
    	line.add("");
    	
    	if (gSALES_REP) { 
    		line.add("");
    		line.add("");
            line.add("");
    	}
    	if (gSALES_PROMOTIONS) {line.add("");}
    	
    	line.add("");
    	
		writer.writeNext(line.toArray(new String[line.size()]));

		Class<Customer> customer = Customer.class;
		Method method = null;
		// create the rows
		for (Order order:this.webJaguar.getInvoiceExportList(search,
    			siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue())) { 
    		line = new ArrayList<String>();
    		
    		line.add(  order.getOrderId().toString() );
    		line.add(  ((order.getOrderType() == null) ? "" : order.getOrderType()) );
    		// date ordered
    		line.add( sdfDate.format( order.getDateOrdered().getTime()) );

			Calendar orderDateCalendar = new GregorianCalendar();
			orderDateCalendar.setTime( order.getDateOrdered() );
    		// shipped month
    		line.add( getMonth(orderDateCalendar) );
    		// shipped QRT
    		line.add( getQuarter(orderDateCalendar) );
    		// shipped Year
    		line.add( orderDateCalendar.get( orderDateCalendar.YEAR ) + "" );

    		// time ordered
    		line.add( sdfTime.format( order.getDateOrdered().getTime()) );
    		line.add( order.getUserEmail() );
    		
    		line.add(order.getUserId() +"");	
    		
    		// customer fields
    		Customer user = this.webJaguar.getCustomerById(order.getUserId());
    		if(customerFields != null && customerFields.size() > 0) {
				for(CustomerField field : customerFields){
					method = customer.getMethod("getField"+field.getId());
					line.add( (method.invoke(user) == null ? "" : method.invoke(user).toString()) );
				}
        	}	    	
    		line.add ((user.getRating1() == null) ? null : user.getRating1());
    		line.add ((user.getRating2() == null) ? null : user.getRating2());
    		line.add ((user.getCardId() == null) ? null : user.getCardId());
    		
    		
    		Calendar accountCreatedDateCalendar = new GregorianCalendar();
    		accountCreatedDateCalendar.setTime( user.getCreated() );
    		
    		// month
    		line.add( Constants.getMonth(accountCreatedDateCalendar) );
    		// Qtr
    		line.add( Constants.getQuarter(accountCreatedDateCalendar) );
    		// Year
    		line.add( accountCreatedDateCalendar.get( accountCreatedDateCalendar.YEAR ) + "" );
    		
    		// trackcode
    		line.add( user.getTrackcode() );
    		
    		
    		String qualifier = "";
    		if(order.getQualifier() != null){
    			if(salesRepMap.get(order.getQualifier())!=null){
    				qualifier = salesRepMap.get(order.getQualifier()).getName();
    			}
    		}
    		line.add(qualifier);
    		
    		line.add("Summary");
    		line.add(""); //sku
    		line.add(""); //Parent Sku
    		line.add(""); //Primary Supplier
    		line.add(""); //Desc
    		// product fields
    		if(productFields != null && productFields.size() > 0) {
    			for(int i=0; i<productFields.size(); i++){
    				line.add(""); //Product Fields
    	        }
    		}
    		line.add(""); //Qty
    		line.add(""); //Content
    		line.add(""); //Original Price
    		line.add(""); //Discount
    		line.add(""); //Discount type
    		line.add(""); //Discount amount total
    		line.add(""); //Final Price
    		// sub total
    		line.add( order.getSubTotal().toString() );
	    	// discount Amount
	    	if (gSALES_PROMOTIONS) {
	    		order.setPromoAmount();
	    		line.add( order.getPromoAmount().toString() );
    		}
	    	if (siteConfig.get( "NO_COMMISSION_FLAG" ).getValue().equals( "true" ) ) {
	    		line.add(( order.getNc() ) ? "1" : "" );
			}/*
	    	if((Boolean) gSiteConfig.get("gBUDGET") && siteConfig.get("BUDGET_ADD_PARTNER").getValue().equalsIgnoreCase("true")) {
	    		line.add(( order.getBudgetEarnedCredits() == null )  ? "" : order.getBudgetEarnedCredits().toString() );
	    	}*/
	    	// grand total
	    	line.add( order.getGrandTotal().toString() );
    		// tax
	    	line.add(( order.getTax() == null )  ? "" : order.getTax().toString() );
    		// tax rate
	    	line.add(( order.getTaxRate() == null )  ? "" : order.getTaxRate().toString() );
	    	// ccFee
	    	line.add(( order.getCcFee() == null )  ? "" : order.getCcFee().toString());
    		// ccFee rate
	    	line.add(( order.getCcFeeRate() == null )  ? "" : order.getCcFeeRate().toString() );
	    	// Merchandise Sales Value
    		if(order.getGrandTotal() != null){
    			Double ccFee = order.getCcFee() == null ? 0 : order.getCcFee();
    			Double tax = order.getTax() == null ? 0 : order.getTax();
    			Double shippingCost = order.getShippingCost() == null ? 0 : order.getShippingCost();
    			Double merchandiseSales = (order.getGrandTotal() - tax - ccFee - shippingCost);
    			line.add(merchandiseSales.toString() );
    		}else{
    			line.add("");
    		}
    		// shipping & handling
	    	line.add(( order.getShippingCost() == null )  ? "" : order.getShippingCost().toString() );

    		line.add(  order.getShippingMethod() ); 			
    		line.add(  order.getPaymentMethod() );
    		line.add(  getMessageSourceAccessor().getMessage("orderStatus_" + order.getStatus()) );
    		if (gADD_INVOICE) {
    			line.add(( order.isBackendOrder()  ? "1" : "" ));
    		}
    		if ( order.getShipped() != null) {
        		// date shipped
    			line.add( sdfDate.format(order.getShipped().getTime()) );
    			Calendar orderShippedDateCalendar = new GregorianCalendar();
        		orderShippedDateCalendar.setTime( order.getShipped() );
        		// shipped month
        		line.add( getMonth(orderShippedDateCalendar) );
        		// shipped QRT
        		line.add( getQuarter(orderShippedDateCalendar) );
        		// shipped Year
        		line.add( orderShippedDateCalendar.get( orderShippedDateCalendar.YEAR ) + "" );

        		// time shipped
    			line.add( sdfTime.format(order.getShipped().getTime()) );
    		} else {
    			line.add("");
        		line.add("");
        		line.add("");
        		line.add("");
        		line.add("");
    		}
    		line.add(  ((order.getTrackcode() == null) ? "" : order.getTrackcode()) );
    		qualifier = "";
    		if(order.getQualifier() != null){
    			if(salesRepMap.get(order.getQualifier())!=null){
    				qualifier = salesRepMap.get(order.getQualifier()).getName();
    			}
    		}
    		line.add(qualifier);
    		// Sales Rep
    		if (gSALES_REP) {
    			line.add(( order.getSalesRepId() == null ) ? "" : salesRepMap.get( order.getSalesRepId() ).getName() ); 
    			line.add(( user.getSalesRepId() == null ) ? "" : salesRepMap.get( user.getSalesRepId() ).getName() ); 
    			line.add(( order.getSalesRepProcessedById() == null ) ? "" : salesRepMap.get( order.getSalesRepProcessedById() ).getName() ); 
    		}
    		// Credit Used
    		line.add((order.getCreditUsed() == null) ? "" :order.getCreditUsed().toString());
    		
    		// Promotion Code
    		if (gSALES_PROMOTIONS) {
    			line.add(( order.getPromo() == null ) ? "" : order.getPromo().getTitle() ); 
    		}
    		line.add(( order.getFlag1() == null ) ? "" : ((order.getFlag1() == null) ? "" : order.getFlag1().toString()) ); 
    		// billing info
    		addressInfoCSV(order.getBilling(), line);
    		// shipping info
    		addressInfoCSV(order.getShipping(), line);
    		
    		line.add("");
			writer.writeNext(line.toArray(new String[line.size()]));
    		// line items
			Class<Product> product = Product.class;
			Method m = null;  
			for (LineItem lineItem : order.getLineItems()) {
    			line = new ArrayList<String>();
    			line.add(  order.getOrderId().toString() );
    			line.add(  ((order.getOrderType() == null) ? "" : order.getOrderType()) );
        		// date ordered
    			line.add(  sdfDate.format( order.getDateOrdered().getTime()) );
    			orderDateCalendar.setTime( order.getDateOrdered() );
        		// shipped month
        		line.add( getMonth(orderDateCalendar) );
        		// shipped QRT
        		line.add( getQuarter(orderDateCalendar) );
        		// shipped Year
        		line.add( orderDateCalendar.get( orderDateCalendar.YEAR ) + "" );
        		// time ordered
    			line.add( sdfTime.format( order.getDateOrdered().getTime()) );  
    			line.add( order.getUserEmail() );  
    			line.add( order.getUserId()+"");
    			if(customerFields != null && customerFields.size() > 0) {
    				for(CustomerField field : customerFields){
    					line.add("");
    				}
            	}
    			line.add("");//CustomerRating-1
    			line.add("");//CustomerRating-2
    			line.add("");//cardID
    			line.add("");//Account Created Month
    			line.add("");//Account Created QTR
    			line.add("");//Account Created Year
    			line.add("");//Track Code
				line.add( "Item");
    			line.add( lineItem.getProduct().getSku() );
				if ((Boolean) gSiteConfig.get("gMASTER_SKU")) {
	    			line.add( (lineItem.getProduct().getMasterSku() == null) ? "" : lineItem.getProduct().getMasterSku() );
        		}
				line.add( (lineItem.getSupplier() != null ? (supplierNameMap.get((long) lineItem.getSupplier().getId())) : ""));
    			line.add( lineItem.getProduct().getName() );
    			// product fields
        		if(productFields != null && productFields.size() > 0) {
    				lineItem.setProductFields(productFields);
    				for(ProductField field : lineItem.getProductFields()){
            			product.getMethod("getField"+field.getId());
            			m = product.getMethod("getField"+field.getId());
            			line.add( m.invoke(lineItem.getProduct()) == null ? "" : m.invoke(lineItem.getProduct()).toString() );
            		}
            	}
        		line.add( lineItem.getQuantity() + "" );
    			
    			line.add(( lineItem.getProduct().getCaseContent() == null )  ? "" : lineItem.getProduct().getCaseContent().toString() );
    			// set original price
    			line.add(( lineItem.getOriginalPrice() == null )  ? "" : lineItem.getOriginalPrice().toString() );
      		    // set discount
    			line.add(( lineItem.getDiscount() == null )  ? "" : (lineItem.getDiscount().toString()));
    			// set discount type
    			line.add(( lineItem.getDiscount() == null )  ? "" : (lineItem.isPercent()) ? "1" : "");
    			// set discount total
      		    if(lineItem.getDiscount() != null && lineItem.getOriginalPrice() != null && lineItem.getUnitPrice() != null){
      		    	Double disAmountTotal;
      		    	if(lineItem.isPercent()){
      		    		disAmountTotal=lineItem.getQuantity()*(lineItem.getOriginalPrice() - lineItem.getUnitPrice());
      		    	}else{
      		    		disAmountTotal=lineItem.getQuantity()*(lineItem.getOriginalPrice() - lineItem.getUnitPrice());
      		    	}
      		    	line.add(disAmountTotal.toString());
      		    }else{
      		    	line.add("");
      		    }
    			// set unit price
    			line.add(( lineItem.getUnitPrice() == null )  ? "" : lineItem.getUnitPrice().toString() );
      			// total price
    			line.add(( lineItem.getTotalPrice() == null )  ? "" : lineItem.getTotalPrice().toString()  );
    			// discount Amount
    	    	if (gSALES_PROMOTIONS) {
    	    		line.add("");
        		}
    			if (siteConfig.get( "NO_COMMISSION_FLAG" ).getValue().equals( "true" ) ) {
      		    	line.add(( order.getNc() ) ? "1" : "" );
  			    }
      		    // grand total
      		    line.add( order.getGrandTotal().toString() );
        		// tax 
        		if (order.getTaxRate() != null && lineItem.getProduct().isTaxable() && lineItem.getTotalPrice() != null) {
        			line.add( order.getTaxRate()*lineItem.getTotalPrice()/100 + "" );
        			line.add( order.getTaxRate().toString() );
        		} else {
        			line.add("");
        			line.add("");
        		}
        		// ccFee
    	    	line.add(( order.getCcFee() == null )  ? "" : order.getCcFee().toString() );
        		// ccFee rate
    	    	line.add(( order.getCcFeeRate() == null )  ? "" :  order.getCcFeeRate().toString() );
    	    	line.add("");//merchandise sale
        		// shipping & handling
        		line.add("");
    			line.add("");
      		   
        		line.add(  order.getPaymentMethod() );
        		line.add(  getMessageSourceAccessor().getMessage("orderStatus_" + order.getStatus()) );
        		if (gADD_INVOICE) {
        			line.add(( order.isBackendOrder()  ? "1" : "" ));
        		}
        		if ( order.getShipped() != null) {
        			// date shipped
        			line.add( sdfDate.format(order.getShipped().getTime()) ); 
        			Calendar orderShippedDateCalendar = new GregorianCalendar();
            		orderShippedDateCalendar.setTime( order.getShipped() );
            		
            		// shipped month
            		line.add( getMonth(orderShippedDateCalendar) );
            		// shipped QRT
            		line.add( getQuarter(orderShippedDateCalendar) );
            		// shipped Year
            		line.add( orderShippedDateCalendar.get( orderShippedDateCalendar.YEAR ) + "" );

            		// time shipped
        			line.add( sdfTime.format(order.getShipped().getTime()) );
        		} else {
        			line.add("");
        			line.add("");
        			line.add("");
        			line.add("");
        			line.add("");
        		}
        		line.add( ((order.getTrackcode() == null) ? "" : order.getTrackcode()) );    
        		qualifier = "";
        		if(order.getQualifier() != null){
        			if(salesRepMap.get(order.getQualifier())!=null){
        				qualifier = salesRepMap.get(order.getQualifier()).getName();
        			}
        		}
        		line.add(qualifier);   
        		// Sales Rep
        		if (gSALES_REP) {
        			line.add(( order.getSalesRepId() == null ) ? "1" : salesRepMap.get( order.getSalesRepId() ).getName() ); 
        			line.add(( user.getSalesRepId() == null ) ? "1" : salesRepMap.get( user.getSalesRepId() ).getName() ); 
        			line.add(( order.getSalesRepProcessedById() == null ) ? "1": salesRepMap.get( order.getSalesRepProcessedById() ).getName() ); 
        		}
        		// Credit Used
        		line.add("");
        		
        		// Promotion Code
        		if (gSALES_PROMOTIONS) {
        			line.add(( order.getPromo() == null ) ? "1" : order.getPromo().getTitle() ); 
        		}
        		line.add(( order.getFlag1() == null ) ? "1" : ((order.getFlag1() == null) ? "" : order.getFlag1().toString()) ); 
        		// billing info
        		addressInfoCSV(order.getBilling(), line);
        		// shipping info
        		addressInfoCSV(order.getShipping(), line);
        		
        		line.add("");
        		writer.writeNext(line.toArray(new String[line.size()]));
    		}
    		
    	}
    	
    	writer.close();
    	HashMap<String, Object> fileMap = new HashMap<String, Object>();
        fileMap.put("file", exportFile);
		exportedFiles.add( fileMap );
    	
    }
    
    private void createPacknwoodCsvFile(int orderCount, OrderSearch search, Map<String, Configuration> siteConfig,Map<String, Object> gSiteConfig,
    		List<Map<String, Object>> exportedFiles, Map <Integer, SalesRep> salesRepMap) throws Exception { 
    	
    	if (orderCount < 1) return;
		
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm:ss a");
    	
		String fileName = "/invoiceExport_" + dateFormatter.format(new Date()) + ".csv";
    	
    	File exportFile = new File(baseFile, fileName);
    	CSVWriter writer = new CSVWriter(new FileWriter(exportFile));
    	List<String> line = new ArrayList<String>();
    	
    	dateFormatter = new SimpleDateFormat("MM/dd/yy kk:mm");
    	
    	// product fields
		List<ProductField> productFields = null;
		if( siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue() != null && siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue().equals("true") ) {
			productFields = this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
			Iterator pfIter = productFields.iterator();
			while ( pfIter.hasNext() )
			{
				ProductField productField = (ProductField) pfIter.next();
				if ( !productField.isEnabled() || !productField.isShowOnInvoiceExport())
				{
					pfIter.remove();
				}
			}
		}
		// Customer Fields
		List<CustomerField> customerFields = null;
		if( siteConfig.get("CUSTOMER_FIELDS_ON_INVOICE_EXPORT").getValue() != null && siteConfig.get("CUSTOMER_FIELDS_ON_INVOICE_EXPORT").getValue().equals("true") ) {
			customerFields = this.webJaguar.getCustomerFields();
			Iterator<CustomerField> iter = customerFields.iterator();
			while ( iter.hasNext() ) {
				CustomerField customerField = iter.next();
				if (!customerField.isEnabled() || !customerField.isShowOnInvoiceExport()) {
					iter.remove();
				}
			}
		}
				
    	//header name
    	line.add("Packing List No.");
    	line.add("SO No.");
    	line.add("Customer Name");
    	line.add("Customer Code");
    	line.add("Create Date");
    	line.add("Customer PO");
    	line.add("Start Date");
    	line.add("SKU");
    	line.add("LOT");
    	line.add("Order Quantity");
    	line.add("Unit Weight");
    	line.add("UPC CODE");
    	line.add("Cancel Date");
    	line.add("Terms of Payment");
    	line.add("Ship Via");
    	line.add("Ship To Store No.");
    	line.add("Ship To Name");
    	line.add("Ship To Address1");
    	line.add("Ship To Address2");
    	line.add("Ship To City");
    	line.add("Ship To State");
    	line.add("Ship To Zip");
    	line.add("Ship To Contact");
    	line.add("Ship To Telephone");
    	line.add("BOL Note");
    	line.add("Pick List Note");
    	
		writer.writeNext(line.toArray(new String[line.size()]));	
		
		
		Class<Customer> customer = Customer.class;
		Method method = null;
		// create the rows
		for (Order order:this.webJaguar.getInvoiceExportList(search,
    			siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue())) { 
    		line = new ArrayList<String>();
    		
    		line.add(  order.getOrderId().toString() );
    		line.add(  order.getOrderId().toString() );
    		Customer user = this.webJaguar.getCustomerById(order.getUserId());
    		line.add( user.getUsername());
    		line.add(" ");
    		line.add( sdfDate.format( order.getDateOrdered().getTime()) );
    		line.add(" ");
    		line.add( sdfDate.format( order.getDateOrdered().getTime()) );
    		line.add(" ");
    		line.add(" ");
    		line.add(" ");
    		line.add(" ");
    		line.add(" ");
    		line.add( sdfDate.format( order.getDateOrdered().getTime()) );
    		line.add(order.getPaymentMethod());
    		line.add(order.getShippingMethod());
    		line.add(" ");
    		line.add(" ");
    		line.add(order.getShipping().getAddr1());
    		line.add(" ");
    		line.add(order.getShipping().getCity());
    		line.add(order.getShipping().getStateProvince());
    		line.add(order.getShipping().getZip());
    		line.add("");
    		line.add(order.getShipping().getCellPhone());
    		line.add("");
			writer.writeNext(line.toArray(new String[line.size()]));
    		// line items
			Class<Product> product = Product.class;
			Method m = null;  
			for (LineItem lineItem : order.getLineItems()) {
    			line = new ArrayList<String>();
    			line.add(" ");
    			line.add(" ");
    			line.add(" ");
    			line.add(" ");
    			line.add(" ");
    			line.add(" ");
    			line.add(" ");    			
    			line.add( lineItem.getProduct().getSku() );
    			if(lineItem.getProduct().getCaseContent()!=null) {
        			line.add(lineItem.getProduct().getCaseContent()*lineItem.getQuantity() + "");
        		} 
        		else {
        			line.add(" ");
        		}
        		line.add( lineItem.getQuantity() + "" ); 
        		
        		writer.writeNext(line.toArray(new String[line.size()]));
    		}
    		
    	}
    	
    	writer.close();
    	HashMap<String, Object> fileMap = new HashMap<String, Object>();
        fileMap.put("file", exportFile);
		exportedFiles.add( fileMap );
    	
    }

    private void addressInfoCSV(Address address, List<String> line) {
    	Map<String, Object> regionMap = this.webJaguar.getRegionMap();
    	Map<String, String> region = new HashMap<String, String>();
		if( address.getCountry() != null && !address.getCountry().isEmpty() && address.getStateProvince() != null &&  address.getStateProvince() != "") { 
	    	region = (Map<String, String>) regionMap.get(address.getCountry() + "_" + address.getStateProvince());
    	} if (region == null && address.getCountry() != null && !address.getCountry().isEmpty()) {
	    	region = (Map<String, String>) regionMap.get(address.getCountry() + "_");
    	}

    	line.add(address.getFirstName());
    	line.add(address.getLastName());
    	line.add(address.getCompany());
    	line.add((address.getCountry() == null || region == null) ? "" : region.get("nationalRegion"));
    	line.add(address.getCountry());
    	line.add((address.getCountry() == null || region == null) ? "" : region.get("countryName"));
    	line.add(address.getAddr1());
    	line.add(address.getAddr2());
    	line.add(address.getCity());
    	line.add((address.getStateProvince() == null || region == null) ? "" : region.get("stateRegion"));
    	line.add(address.getStateProvince());
    	line.add(address.getZip());
    	line.add(address.getPhone());
    	line.add(address.getCellPhone());
    	line.add(address.getFax());
    }
    
    private void addressHeaderCSV(String prefix, List<String> line) {	
    	line.add(prefix + getMessageSourceAccessor().getMessage("firstName"));
    	line.add(prefix + getMessageSourceAccessor().getMessage("lastName"));
    	line.add(prefix + getMessageSourceAccessor().getMessage("company"));
    	line.add(prefix + getMessageSourceAccessor().getMessage("nationalRegion"));
    	line.add(prefix + getMessageSourceAccessor().getMessage("country"));
    	line.add(prefix + getMessageSourceAccessor().getMessage("countryFullName"));
    	line.add(prefix + getMessageSourceAccessor().getMessage("address") + "1");
    	line.add(prefix + getMessageSourceAccessor().getMessage("address") + "2");
    	line.add(prefix + getMessageSourceAccessor().getMessage("city"));
    	line.add(prefix + getMessageSourceAccessor().getMessage("stateRegion"));
    	line.add(prefix + getMessageSourceAccessor().getMessage("stateProvince"));
    	line.add(prefix + getMessageSourceAccessor().getMessage("zipCode"));
    	line.add(prefix + getMessageSourceAccessor().getMessage("phone"));
    	line.add(prefix + getMessageSourceAccessor().getMessage("cellPhone"));
    	line.add(prefix + getMessageSourceAccessor().getMessage("fax"));
    }

	private OrderSearch getOrderSearch(HttpServletRequest request) {
		OrderSearch orderSearch = (OrderSearch) request.getSession().getAttribute( "orderSearch" );
		if (orderSearch == null) {
			orderSearch = new OrderSearch();
			request.getSession().setAttribute( "orderSearch", orderSearch );
		}
		
		// order status
		if (request.getParameter("status") != null) {
			orderSearch.setStatus(ServletRequestUtils.getStringParameter( request, "status", "" ));
		}
		
		// order num
		if (request.getParameter("orderNum") != null) {
			try {
				Integer orderNum = Integer.parseInt( request.getParameter("orderNum") );
				if (orderNum > 0) {
					orderSearch.setOrderNum( orderNum.toString() );
				} else {
					orderSearch.setOrderNum( "" );					
				}
			} catch (Exception e) {
				orderSearch.setOrderNum( "" );
			}				
		}
		
		// purchase order
		if (request.getParameter("purchaseOrder") != null) {
			orderSearch.setPurchaseOrder(ServletRequestUtils.getStringParameter(request, "purchaseOrder", ""));				
		}		
		
		// billTo
		if (request.getParameter("bill_to") != null) {
			orderSearch.setBillToName(ServletRequestUtils.getStringParameter( request, "bill_to", "" ));
		}		
		
		// shipTo
		if (request.getParameter("ship_to") != null) {
			orderSearch.setShipToName(ServletRequestUtils.getStringParameter( request, "ship_to", "" ));
		}	
		
		// company Name
		if (request.getParameter("companyName") != null) {
			orderSearch.setCompanyName( ServletRequestUtils.getStringParameter( request, "companyName", "" ));
		}
		
		// email
		if (request.getParameter("email") != null) {
			orderSearch.setEmail(ServletRequestUtils.getStringParameter( request, "email", "" ));
		}	
		
		// phone
		if (request.getParameter("phone") != null) {
			orderSearch.setPhone(ServletRequestUtils.getStringParameter( request, "phone", "" ));
		}
		
		// zipcode
		if (request.getParameter("zipcode") != null) {
			orderSearch.setZipcode(ServletRequestUtils.getStringParameter( request, "zipcode", "" ));
		}
		
		// trackCode
		if (request.getParameter("trackCode") != null) {
			orderSearch.setTrackCode( ServletRequestUtils.getStringParameter( request, "trackCode", "" ));
		}
		
		// qualifier
		if (request.getParameter("qualifier") != null) {
			orderSearch.setQualifier( ServletRequestUtils.getStringParameter( request, "qualifier", "" ));
		}
		
		// paymentMethod
		if (request.getParameter("paymentMethod") != null) {
			orderSearch.setPaymentMethod( ServletRequestUtils.getStringParameter( request, "paymentMethod", "" ));
		}
		
		// state
		if (request.getParameter("state") != null) {
			orderSearch.setState( ServletRequestUtils.getStringParameter( request, "state", "" ));
		}
		
		// country
		if (request.getParameter("country") != null) {
			orderSearch.setCountry( ServletRequestUtils.getStringParameter( request, "country", "" ));
		}
		
		// sales rep
		if (request.getParameter("sales_rep_id") != null) {
			orderSearch.setSalesRepId(ServletRequestUtils.getIntParameter( request, "sales_rep_id", -1 ));
		}
		
		// host for multi store
		if (request.getParameter("host") != null) {
			orderSearch.setHost(ServletRequestUtils.getStringParameter( request, "host", "" ));
		}
		
		// shipping Method
		if (request.getParameter("shippingMethod") != null) {
			orderSearch.setShippingMethod( ServletRequestUtils.getStringParameter( request, "shippingMethod", "" ));
		}
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				orderSearch.setStartDate( df.parse( request.getParameter("startDate")) );
			} catch (ParseException e) {
				orderSearch.setStartDate(null);
	        }
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				orderSearch.setEndDate( df.parse( request.getParameter("endDate")) );
			} catch (ParseException e) {
				orderSearch.setEndDate(null);
	        }
		}
		
		// Date Type
		if (request.getParameter("dateType") != null) {
			orderSearch.setDateType( ServletRequestUtils.getStringParameter( request, "dateType", "" ));
		}
		
		// Sku
		if (request.getParameter("sku") != null) {
			orderSearch.setSku( ServletRequestUtils.getStringParameter( request, "sku", null ));
		}
		
		// Backend Order
		if (request.getParameter("backendOrder") != null) {
			orderSearch.setBackendOrder( ServletRequestUtils.getStringParameter( request, "backendOrder", "" ));
		}
		
		// Product Field
		if (request.getParameter("productField") != null) {
			orderSearch.setProductField( ServletRequestUtils.getStringParameter( request, "productField", null ));
		}
		
		// Product Field Number
		if (request.getParameter("productFieldNumber") != null) {
			orderSearch.setProductFieldNumber( ServletRequestUtils.getStringParameter( request, "productFieldNumber", null ));
		}
		
		// Customer Field
		if (request.getParameter("customerField") != null) {
			orderSearch.setCustomerField( ServletRequestUtils.getStringParameter( request, "customerField", null ));
		}
		
		// Customer Field Number
		if (request.getParameter("customerFieldNumber") != null) {
			orderSearch.setCustomerFieldNumber( ServletRequestUtils.getStringParameter( request, "customerFieldNumber", null ));
		}
		
		// Order Type
		if (request.getParameter("orderType") != null) {
			orderSearch.setOrderType( ServletRequestUtils.getStringParameter( request, "orderType", null ));
		}
		
		// Flag1
		if (request.getParameter("flag1") != null) {
			orderSearch.setFlag1( ServletRequestUtils.getStringParameter( request, "flag1", null ));
		}
		
		return orderSearch;
	}
	
	private String getQuarter(Calendar orderDateCalendar) {
		int month = orderDateCalendar.get(Calendar.MONTH);
		return (month >= Calendar.JANUARY && month <= Calendar.MARCH)     ? "Q1" :
		       (month >= Calendar.APRIL && month <= Calendar.JUNE)        ? "Q2" :
		       (month >= Calendar.JULY && month <= Calendar.SEPTEMBER)    ? "Q3" : "Q4";
	}
	
	private String getMonth(Calendar orderDateCalendar) {
		int month = orderDateCalendar.get(Calendar.MONTH);
		return (month == Calendar.JANUARY)  ? "JAN" :
		       (month == Calendar.FEBRUARY) ? "FEB" :
		       (month == Calendar.MARCH) ? "MAR" :
		       (month == Calendar.APRIL) ? "APR" :
		       (month == Calendar.MAY) ? "MAY" :
		       (month == Calendar.JUNE) ? "JUN" :	   
		       (month == Calendar.JULY) ? "JUL" :	   
		       (month == Calendar.AUGUST) ? "AUG" :	   
		       (month == Calendar.SEPTEMBER) ? "SEP" :	   
		       (month == Calendar.OCTOBER) ? "OCT" :	
		       (month == Calendar.NOVEMBER) ? "NOV" : "DEC";
	}
	private void notifyAdmin(Map<String, Configuration> siteConfig, HttpServletRequest request){
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	StringBuffer message = new StringBuffer();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
    	message.append("user "+accessUser.getUsername()+" has exported Order file");
    	SimpleMailMessage msg = new SimpleMailMessage();			
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Automated Export on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}

}
