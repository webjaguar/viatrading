/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.13.2008
 */

package com.webjaguar.web.admin.orders.packingList;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.PackingList;
import com.webjaguar.model.PackingListSearch;
import com.webjaguar.model.SalesRep;

public class PackingListController implements Controller {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		PackingListSearch packingListSearch = getPackingListSearch(request);
		Integer orderId = ServletRequestUtils.getIntParameter(request,"orderId", -1);

		Integer accessUserId = null ;
		if ( request.getAttribute( "accessUser" ) != null ) {
			accessUserId = ((AccessUser) request.getAttribute( "accessUser" )).getId();
		}	
		// check if delete button was clicked
		Order order = this.webJaguar.getOrder(orderId, "");
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				boolean inventoryHistory = false;
				if(siteConfig.get("INVENTORY_HISTORY").getValue().equals("true")) {
					inventoryHistory = true;
				}
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.deletePackingList(orderId, ids[i], accessUserId, inventoryHistory);
				}
			}
		}
		model.put("fullfil", order.isFulfilled());
		if (request.getParameter("printPackingList") != null) {
			PackingList packingList = this.webJaguar.getPackingList(orderId, request.getParameter("packing_num"));
			Iterator<LineItem> lineItemsIterator = order.getLineItems().iterator();
			while (lineItemsIterator.hasNext()) {
				LineItem lineItem = lineItemsIterator.next();
				boolean flag = false;
				for (LineItem l : packingList.getLineItems()) {
					if (lineItem.getLineNumber() == l.getLineNumber()) {
						lineItem.setToBeShipQty(l.getQuantity());
						flag = true;
						continue;
					}
				}
				if (!flag) {
					lineItemsIterator.remove();
				}
			}
			model.put("productFieldsHeader", this.webJaguar.getProductFieldsHeader(order, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
			model.put("trackingNumber", packingList.getTracking());
			model.put("order", order);
			// pass customer for replaceing dynamic value
			Customer customer = this.webJaguar.getCustomerById(order.getUserId());
			request.setAttribute("customerLogo", customer);
			model.put("customer", customer);
			SalesRep salesRep = null;
			if ((Boolean) gSiteConfig.get("gSALES_REP") && order.getSalesRepId() != null) {
				salesRep = this.webJaguar.getSalesRepById(order.getSalesRepId());			
				model.put("salesRep", salesRep);
				model.put("salesRepProcessedBy", this.webJaguar.getSalesRepById( order.getSalesRepProcessedById() ));
			}
			if (request.getParameter("printPackingList").equals("1")) {
				//model.put("packingLayout", this.webJaguar.getSystemLayout("packing", order.getHost(), request));
				
				String orig = order.getOrderId() + "-" + order.getUserId();
			    byte[] encoded = Base64.encodeBase64(orig.getBytes());         
			    orig = new String(encoded);
				
				packingList.setGrandTotal();
				URL url2 = new URL(request.getScheme()+"://www.viatrading.com/dv/invoice/courierpacking.php?id=" + orig);
				HttpURLConnection con = (HttpURLConnection) url2.openConnection();
			    con.setRequestMethod("GET");
			    con.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			    con.addRequestProperty("User-Agent", "Mozilla");
			    con.addRequestProperty("Referer", "google.com");
			    con.setRequestProperty("Accept", "text/html, text/*");
			    con.addRequestProperty("Content-Type","application/json; charset=UTF-8"); 
				
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuilder sb = new StringBuilder();
				while((inputLine = in.readLine()) != null)
				{
				    if(inputLine!=null && !inputLine.isEmpty()){
				    	sb.append(inputLine);
				    	
				    }
				}	
				model.put("courierLtlPackingList", sb.toString());
				return new ModelAndView("admin/orders/packingList/courierLtlPackingList", model);

			} else if (request.getParameter("printPackingList").equals("2")) {
				model.put("packingLayout", this.webJaguar.getSystemLayout("packing2", order.getHost(), request));

			} else if (request.getParameter("printPackingList").equals("3")) {
				model.put("packingLayout", this.webJaguar.getSystemLayout("packing3", order.getHost(), request));

			} else if (request.getParameter("printPackingList").equals("4")) {
				model.put("packingLayout", this.webJaguar.getSystemLayout( "packing4", order.getHost(), request));
				// grand total
				packingList.setGrandTotal();
				model.put("packingList", packingList);
				return new ModelAndView("admin/orders/packingList/packingList4", model);
			}else if (request.getParameter("printPackingList").equals("5")) {
				//model.put("packingLayout", this.webJaguar.getSystemLayout( "packing5", order.getHost(), request));
				packingList.setGrandTotal();
				
				URL url2 = new URL(request.getScheme()+"://www.viatrading.com/dv/invoice/packing.php?InvoiceID="+order.getOrderId());
				HttpURLConnection con = (HttpURLConnection) url2.openConnection();
			    con.setRequestMethod("GET");
			    con.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			    con.addRequestProperty("User-Agent", "Mozilla");
			    con.addRequestProperty("Referer", "google.com");
			    con.setRequestProperty("Accept", "text/html, text/*");
			    con.addRequestProperty("Content-Type","application/json; charset=UTF-8"); 
				
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuilder sb = new StringBuilder();
				 
				while((inputLine = in.readLine()) != null)
				{
				    if(inputLine!=null && !inputLine.isEmpty()){
				    	sb.append(inputLine);
				    	
				    }
				}	
				model.put("packingList", sb.toString());
				return new ModelAndView("admin/orders/packingList/packingList5", model);
			}
			return new ModelAndView("admin/orders/packingList/packingList", model);
		}
		
		
		String AppsLink = order.getOrderId().toString() + "-" + this.webJaguar.getCustomerById(order.getUserId()).getId().toString();
		 byte[] encode = Base64.encodeBase64(AppsLink.getBytes());         
		    String encodedId = new String(encode);
		model.put("encodedId", encodedId);

		if (request.getParameter("__generatePackingList") != null) {
			return new ModelAndView(new RedirectView(
					"generatePackingList.jhtm?orderId=" + orderId
							+ "&force_generate=true"));
		}

		packingListSearch.setOrderId(orderId);
		PagedListHolder packingList = new PagedListHolder(this.webJaguar
				.getPackingListByOrderId(packingListSearch));

		packingList.setPageSize(packingListSearch.getPageSize());
		packingList.setPage(packingListSearch.getPage() - 1);

		model.put("orderId", orderId);
		model.put("packingList", packingList);
		model.put("countries", this.webJaguar.getCountryMap());
		
		String __walkin = (String) request.getSession().getAttribute("__walkin");
		if(__walkin != null && __walkin.equalsIgnoreCase("true")){
			return new ModelAndView("admin/orders/packingList/packingLists", "model", model);
		}else{
			return new ModelAndView("admin/orders/packingList/packingLists", "model", model);
		}
	}

	private PackingListSearch getPackingListSearch(HttpServletRequest request) {
		PackingListSearch packingListSearch = (PackingListSearch) request.getSession().getAttribute("packingListSearch");
		if (packingListSearch == null) {
			packingListSearch = new PackingListSearch();
			request.getSession().setAttribute("packingListSearch",
					packingListSearch);
		}

		// sorting
		if (request.getParameter("sort") != null) {
			packingListSearch.setSort(ServletRequestUtils.getStringParameter(
					request, "sort", ""));
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				packingListSearch.setPage(1);
			} else {
				packingListSearch.setPage(page);
			}
		}

		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				packingListSearch.setPageSize(10);
			} else {
				packingListSearch.setPageSize(size);
			}
		}

		return packingListSearch;
	}
}