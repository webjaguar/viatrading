/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.29.2009
 */

package com.webjaguar.web.admin.orders;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class CustomFrameController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
    	
		Map<String, Object> model = new HashMap<String, Object>();
		Integer orderId = ServletRequestUtils.getIntParameter(request, "orderId", -1);
		Integer lineNum = ServletRequestUtils.getIntParameter(request, "lineNum", -1);
		model.put("customFrame", this.webJaguar.getCustomFrame(orderId, lineNum));
		
        return new ModelAndView("admin/orders/customFrame",  model);
	}
}
