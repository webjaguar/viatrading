/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.04.2008
 */

package com.webjaguar.web.admin.orders;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Order;

public class PdfInvoiceUploadController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	Order order = this.webJaguar.getOrder(ServletRequestUtils.getIntParameter(request, "orderId", 0), null);
    	
    	if (order == null) {
			return new ModelAndView(new RedirectView("../orders/"));    		
    	}
    	
		File baseFile = new File(getServletContext().getRealPath("/assets/pdf/"));
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				baseFile = new File((String) prop.get("site.root") + "/assets/pdf/");
			}
		} catch (Exception e) {}
    	    	
		if (!baseFile.exists()) {
			baseFile.mkdir();
		}
		
		if (baseFile.canWrite()) {
	    	// check if delete button was pressed
	    	if (request.getParameter("_delete") != null && order.getPdfUrl() != null) {
	    		File pdf = new File(baseFile, order.getPdfUrl());
	    		if (!pdf.exists() || pdf.delete()) {
	        		order.setPdfUrl(null);
	        		this.webJaguar.updatePdfOrder(order);
	    		}
	    		
	    		return new ModelAndView(new RedirectView("../orders/"));
	    	}
	    	
	    	// check if upload button was pressed
	    	if (request.getParameter("_upload") != null) {
	    		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
	        	String fileName = order.getOrderId() + "-" + dateFormatter.format(new Date()) + ".pdf";
	    		File pdf = new File(baseFile, fileName);
	    		
	    		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	    		MultipartFile file = multipartRequest.getFile( "_file" );
				if (file != null && !file.isEmpty()) {
					if (file.getContentType().equalsIgnoreCase("application/pdf")) {
						try {
							file.transferTo(pdf);
				    		order.setPdfUrl(fileName);
			        		this.webJaguar.updatePdfOrder(order);
			        		
			        		return new ModelAndView(new RedirectView("../orders/"));
						} catch (IOException e) {
							// do nothing
						}
					} else {
						map.put("message", "pdffile.invalid");
					}
				} else {
					map.put("message", "pdffile.invalid");
				}
	    	}			
		} else {
			map.put("message", "file.notWritable");
		}


    	map.put("order", order);
    	
		return new ModelAndView("admin/orders/pdf", map);
    }
}
