/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.30.2007
 */

package com.webjaguar.web.admin.orders;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.EmailMessageForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.SalesRep;

public class PackingEmailController extends SimpleFormController {

	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	public PackingEmailController() {
		setSessionForm(true);
		setCommandName("form");
		setCommandClass(EmailMessageForm.class);
		setFormView("admin/orders/emailPacking");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {

		EmailMessageForm form = (EmailMessageForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
		
        // email invoice
		if (request.getParameter("__invoice") != null) {
			return new ModelAndView(new RedirectView("invoice.jhtm?order=" + form.getOrder().getOrderId()));
		}
		
		// construct email message
		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File pdfFile = new File(tempFolder, "order_" + form.getOrder().getOrderId() + ".pdf");
       	MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
		try {
			constructMessage(helper, form, request);
			// attach pdf invoice
			if (this.webJaguar.createPdfPackingList(pdfFile, form.getOrder().getOrderId(), request, form.getSupplierId())) {
				helper.addAttachment("invoice" + form.getOrder().getOrderId() + ".pdf", pdfFile);
				mailSender.send(mms);
				model.put("message", "email.sent" );
			} else {
				model.put("message", "pdffile.create.exception" );
			}
		} catch (Exception e) {
			model.put("message", "email.exception" );
		}
		if (pdfFile.exists()) {
			pdfFile.delete();
		}
		
		return showForm(request, response, errors, model);
	}	
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("__invoice") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		EmailMessageForm form = new EmailMessageForm();
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		
		Order order = this.webJaguar.getOrder(ServletRequestUtils.getIntParameter(request, "orderId", 0), siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());
		if (order != null) {
			form.setOrder(order);
		} else {
			request.setAttribute("message", "order.exception.notfound" );
			return form;
		}
		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
		MultiStore multiStore = null;
		if (multiStores.size() > 0) {
			multiStore = multiStores.get(0);
		}
		if (multiStore != null) {
			form.setFrom(multiStore.getContactEmail());
			form.setBcc(multiStore.getContactEmail());			
		} else {
			form.setFrom( siteConfig.get( "CONTACT_EMAIL" ).getValue() );
			form.setBcc( siteConfig.get( "CONTACT_EMAIL" ).getValue() );						
		}
		
		return form;
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors) {
		EmailMessageForm form = (EmailMessageForm) command;
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("messages", this.webJaguar.getSiteMessageList());
		if (form.getOrder() != null) {
			map.put("suppliers", this.webJaguar.getProductSupplierListByOrderId( form.getOrder().getOrderId(), null ));			
		}
		return map;
	}	
	
	private void constructMessage(MimeMessageHelper helper, EmailMessageForm form, HttpServletRequest request) throws Exception {
		SalesRep salesRep = null;
		if ( (Boolean) gSiteConfig.get( "gSALES_REP" ) && form.getOrder().getSalesRepId() != null ) {
			salesRep = this.webJaguar.getSalesRepById( form.getOrder().getSalesRepId() );
		}
		
		// to emails
		String[] emails = form.getTo().split( "[,]" ); // split by commas
		for ( int x = 0; x < emails.length; x++ ) {
			if (emails[x].trim().length() > 0) {
				helper.addTo(emails[x].trim());
			}
		}
		// cc emails
		emails = form.getCc().split( "[,]" ); // split by commas
		for ( int x = 0; x < emails.length; x++ ) {
			if (emails[x].trim().length() > 0) {
				helper.addCc(emails[x].trim());
			}
		}
		helper.setFrom(form.getFrom());	
		helper.setBcc(form.getBcc());
		
		Customer customer = this.webJaguar.getCustomerById( form.getOrder().getUserId() );
		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(form.getOrder().getHost());
		MultiStore multiStore = null;
		if (multiStores.size() > 0) {
			multiStore = multiStores.get(0);
		}

    	String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));

    	StringBuffer message = new StringBuffer();
    	message.append( dateFormatter.format(new Date()) + "\n\n" );
    	
    	String firstName = form.getOrder().getBilling().getFirstName();  
    	if (firstName == null) {
    		firstName = "";
    	}
    	String lastname = form.getOrder().getBilling().getLastName();
    	if (lastname == null) {
    		lastname = "";
    	}
    	String email = "";
    	if (customer != null) {
    		email = customer.getUsername();
    	}
    	
    	// subject
    	form.setSubject( form.getSubject().replace( "#firstname#", firstName ) );
    	form.setSubject( form.getSubject().replace( "#lastname#", lastname ) );
    	form.setSubject( form.getSubject().replace( "#email#", email ) );
    	form.setSubject( form.getSubject().replace( "#order#", form.getOrder().getOrderId().toString() ) );
    	// XXX would be replaced by OrderID-UserID Base64-encoded.
    	String orig = form.getOrder().getOrderId() + "-" + form.getOrder().getUserId();
    	byte[] encode_orig = Base64.encodeBase64(orig.getBytes());         
    	orig = new String(encode_orig);
    	if(form.getOrder().getShipping().getStateProvince().equals("CA")) {
    		//If user is in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&walkin=true
    		//If user is not in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&other=true
    		//form.setSubject( form.getSubject().replace(  "#orderlink#", secureUrl + "dv/inv/invoice.php?order_id=" + orig +"=&walkin=true") );
    		String viewInvoiceHrefInSubject = secureUrl + "dv/inv/invoice.php?order_id=" + orig +"=&walkin=true";
    		StringBuilder sb_aTagInSubject = new StringBuilder();
    		sb_aTagInSubject.append("<a href=").append("\"").append(viewInvoiceHrefInSubject).append("\"").append(">").append("View Invoice").append("</a>");
    		form.setSubject(form.getSubject().replaceAll("(?i)#orderlink#", sb_aTagInSubject.toString()));    		
 
    		StringBuilder sb_aTagInSubject_orderlinkes = new StringBuilder();
    		sb_aTagInSubject_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHrefInSubject).append("\"").append(">").append("Ver Factura").append("</a>");
    		form.setSubject(form.getSubject().replace("#orderlinkes#", sb_aTagInSubject_orderlinkes.toString()));
    	} else {
    		//form.setSubject( form.getSubject().replace(  "#orderlink#", secureUrl + "dv/inv/invoice.php?order_id=" + orig +"=&other=true") );
    		String viewInvoiceHrefInSubject = secureUrl + "dv/inv/invoice.php?order_id=" + orig +"=&other=true";
    		StringBuilder sb_aTagInSubject = new StringBuilder();
    		sb_aTagInSubject.append("<a href=").append("\"").append(viewInvoiceHrefInSubject).append("\"").append(">").append("View Invoice").append("</a>");
    		form.setSubject(form.getSubject().replaceAll("(?i)#orderlink#", sb_aTagInSubject.toString()));
    		
    		StringBuilder sb_aTagInSubject_orderlinkes = new StringBuilder();
    		sb_aTagInSubject_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHrefInSubject).append("\"").append(">").append("Ver Factura").append("</a>");
    		form.setSubject(form.getSubject().replace("#orderlinkes#", sb_aTagInSubject_orderlinkes.toString()));
    	}
    	form.setSubject( form.getSubject().replace( "#status#", getMessageSourceAccessor().getMessage( "orderStatus_" + form.getOrder().getStatus() ) ) );  
    	if ( salesRep != null ) {
    		form.setSubject( form.getSubject().replace( "#salesRepName#", salesRep.getName() ) );
    		form.setSubject( form.getSubject().replace( "#salesRepEmail#", salesRep.getEmail() ) );
    		form.setSubject( form.getSubject().replace( "#salesRepPhone#", ( salesRep.getPhone() == null ) ? "" : salesRep.getPhone() ) );
    		form.setSubject( form.getSubject().replace( "#salesRepCellPhone#", ( salesRep.getCellPhone() == null ) ? "" : salesRep.getCellPhone() ) );
    	}
    	helper.setSubject(form.getSubject());
    	
    	// message
    	form.setMessage( form.getMessage().replace( "#firstname#", firstName ) );
    	form.setMessage( form.getMessage().replace( "#lastname#", lastname ) );
    	form.setMessage( form.getMessage().replace( "#email#", email ) );
    	form.setMessage( form.getMessage().replace( "#order#", form.getOrder().getOrderId().toString() ) );
    	if(form.getOrder().getShipping().getStateProvince().equals("CA")) {
    		//If user is in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&walkin=true
    		//If user is not in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&other=true
    		//form.setMessage( form.getMessage().replace( "#orderlink#", secureUrl + "dv/inv/invoice.php?order_id=" + orig +"=&walkin=true") );
			String viewInvoiceHrefInMessage = secureUrl + "dv/inv/invoice.php?order_id=" + orig +"=&walkin=true";
			StringBuilder sb_aTagInMessage = new StringBuilder();
			sb_aTagInMessage.append("<a href=").append("\"").append(viewInvoiceHrefInMessage).append("\"").append(">").append("View Invoice").append("</a>");
			form.setMessage(form.getMessage().replaceAll("(?i)#orderlink#", sb_aTagInMessage.toString()));
			
			StringBuilder sb_aTagInMessage_orderlinkes = new StringBuilder();
			sb_aTagInMessage_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHrefInMessage).append("\"").append(">").append("Ver Factura").append("</a>");
			form.setMessage(form.getMessage().replace("#orderlinkes#", sb_aTagInMessage_orderlinkes.toString()));
    	} else {
    		//form.setMessage( form.getMessage().replace( "#orderlink#", secureUrl + "dv/inv/invoice.php?order_id=" + orig +"=&other=true") );
			String viewInvoiceHrefInMessage = secureUrl + "dv/inv/invoice.php?order_id=" + orig +"=&other=true";
			StringBuilder sb_aTagInMessage = new StringBuilder();
			sb_aTagInMessage.append("<a href=").append("\"").append(viewInvoiceHrefInMessage).append("\"").append(">").append("View Invoice").append("</a>");
			form.setMessage(form.getMessage().replaceAll("(?i)#orderlink#", sb_aTagInMessage.toString()));
			
			StringBuilder sb_aTagInMessage_orderlinkes = new StringBuilder();
			sb_aTagInMessage_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHrefInMessage).append("\"").append(">").append("Ver Factura").append("</a>");
			form.setMessage(form.getMessage().replace("#orderlinkes#", sb_aTagInMessage_orderlinkes.toString()));
    	}
    	form.setMessage( form.getMessage().replace( "#status#", getMessageSourceAccessor().getMessage( "orderStatus_" + form.getOrder().getStatus() ) ) );
    	if ( salesRep != null ) {
    		form.setMessage( form.getMessage().replace( "#salesRepName#", salesRep.getName() ) );
    		form.setMessage( form.getMessage().replace( "#salesRepEmail#", salesRep.getEmail() ) );
    		form.setMessage( form.getMessage().replace( "#salesRepPhone#", ( salesRep.getPhone() == null ) ? "" : salesRep.getPhone() ) );
    		form.setMessage( form.getMessage().replace( "#salesRepCellPhone#", ( salesRep.getCellPhone() == null ) ? "" : salesRep.getCellPhone() ) );
    	}
    	message.append( form.getMessage() );
    	helper.setText(message.toString(), form.isHtml());
	}
}
