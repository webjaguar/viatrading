/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.22.2006
 */

package com.webjaguar.web.admin.orders;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.crm.CrmContact;

public class UserNotificationsImportController extends AbstractCommandController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }

    public ModelAndView handle(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView("admin/orders/invoice", model);
	}
    
    private boolean autoUserNotificationsImport(){
    	
		

    	List<Customer> userNotificationsList = this.webJaguar.getUserNotifications();
		

		Integer userId = null;
		Integer crmId = null;
		
		CrmContact crmcontact = null;
		Customer user = null;
		
    	for(Customer customer : userNotificationsList){
    		try{
    			
    			try{	
    				if(customer.getUserId()!=null){
    					userId = Integer.parseInt(customer.getUserId());
    					user = this.webJaguar.getCustomerById(userId);
    				}
    				if(customer.getCrmId()!=null){
    					crmId = Integer.parseInt(customer.getCrmId());
    					crmcontact = this.webJaguar.getCrmContactById(crmId);
    				}
    			}catch(Exception ex){
    			}
    			
    			if(user!=null && crmcontact==null){
    				crmcontact=this.webJaguar.getCrmContactById(user.getCrmContactId());
    			}
    			
    			if(user==null && crmcontact!=null){
    				user = this.webJaguar.getCustomerById(crmcontact.getUserId());
    			}
    			
    			if(user!=null){
    				user.setUnsubscribe(customer.isUnsubscribe());
    				user.setEmailNotify(customer.isEmailNotify());
    				user.setTextMessageNotify(customer.isTextMessageNotify());
    				
    				if(customer.getTaxId()!=null){
    					user.setTaxId(customer.getTaxId());
    				}
    				if(customer.getField1()!=null){
    					user.setField1(customer.getField1());
    				}
    				if(customer.getField2()!=null){
    					user.setField2(customer.getField2());
    				}
    				if(customer.getField3()!=null){
    					user.setField3(customer.getField3());
    				}
    				if(customer.getField4()!=null){
    					user.setField4(customer.getField4());
    				}
    				if(customer.getField5()!=null){
    					user.setField5(customer.getField5());
    				}
    				if(customer.getField6()!=null){
    					user.setField6(customer.getField6());
    				}
    				if(customer.getField7()!=null){
    					user.setField7(customer.getField7());
    				}
    				if(customer.getField8()!=null){
    					user.setField8(customer.getField8());
    				}
    				if(customer.getField9()!=null){
    					user.setField9(customer.getField9());
    				}
    				if(customer.getField10()!=null){
    					user.setField10(customer.getField10());
    				}
    				if(customer.getField11()!=null){
    					user.setField11(customer.getField11());
    				}
    				if(customer.getField12()!=null){
    					user.setField12(customer.getField12());
    				}
    				if(customer.getField13()!=null){
    					user.setField13(customer.getField13());
    				}
    				if(customer.getField14()!=null){
    					user.setField14(customer.getField14());
    				}
    				if(customer.getField15()!=null){
    					user.setField15(customer.getField15());
    				}
    				if(customer.getField16()!=null){
    					user.setField16(customer.getField16());
    				}
    				if(customer.getField17()!=null){
    					user.setField17(customer.getField17());
    				}
    				if(customer.getField18()!=null){
    					user.setField18(customer.getField18());
    				}
    				if(customer.getField19()!=null){
    					user.setField19(customer.getField19());
    				}
    				if(customer.getField20()!=null){
    					user.setField20(customer.getField20());
    				}
    				this.webJaguar.updateUserNotifications(user);
    			}
    			
    			if(crmcontact!=null){
    				crmcontact.setUnsubscribe(customer.isUnsubscribe());
					this.webJaguar.updateCrmNotifications(crmcontact);
    			}
			
    		}catch(Exception ex){
    			//System.out.println(ex.getStackTrace());
    		}
    	}
    	
		
    	this.webJaguar.removeUserNotifications();
		

		return false;
	}
 
}
