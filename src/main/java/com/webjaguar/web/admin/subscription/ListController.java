/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 04.01.2008
 */

package com.webjaguar.web.admin.subscription;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.SubscriptionSearch;

public class ListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
    	
		SubscriptionSearch search = getSubscriptionSearch(request);
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		
		PagedListHolder subscriptionsList = new PagedListHolder(this.webJaguar.getSubscriptions(search));	

		subscriptionsList.setPageSize(search.getPageSize());
		subscriptionsList.setPage(search.getPage()-1);

		model.put("list", subscriptionsList);
    	
        return new ModelAndView("admin/subscription/subscriptionsList", "model", model);
	}

	private SubscriptionSearch getSubscriptionSearch(HttpServletRequest request) {
		SubscriptionSearch search = (SubscriptionSearch) request.getSession().getAttribute("subscriptionSearch");
		if (search == null) {
			search = new SubscriptionSearch();
			request.getSession().setAttribute("subscriptionSearch", search);
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}			
		
		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		return search;
	}
	
}
