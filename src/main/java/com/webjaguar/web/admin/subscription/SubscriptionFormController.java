/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 04.01.2008
 */

package com.webjaguar.web.admin.subscription;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Subscription;

public class SubscriptionFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public SubscriptionFormController() {
		setSessionForm(true);
		setCommandName("subscription");
		setCommandClass(Subscription.class);
		setFormView("admin/subscription/subscriptionForm");
		setSuccessView("subscriptionsList.jhtm");
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder( request, binder );
		NumberFormat nf = NumberFormat.getInstance( RequestContextUtils.getLocale( request ) );
		nf.setMaximumFractionDigits( 2 );
		nf.setMinimumFractionDigits( 2 );
		binder.registerCustomEditor( Double.class, new CustomNumberEditor( Double.class, nf, true ) );
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor(Date.class, new CustomDateEditor(df, false, 10));		
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {

		Subscription subscription = (Subscription) command;

		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}

		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteSubscription(subscription);
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		this.webJaguar.updateSubscription(subscription);
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}	
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		Subscription subscription = this.webJaguar.getSubscription(ServletRequestUtils.getStringParameter(request, "code", ""));
		if (subscription == null) {
			// check if subscription exists
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView(getSuccessView())));
		}
		subscription.setCustomer(this.webJaguar.getCustomerById(subscription.getUserId()));
		
		return subscription;
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) 
		throws Exception {
	
		Subscription subscription = (Subscription) command;
		
		if (subscription.getNextOrder().before(new Date())) {
			// check if next order is after today
			// errors.rejectValue("nextOrder", "DATE_MUST_BE_FUTURE");
		}
	}

}
