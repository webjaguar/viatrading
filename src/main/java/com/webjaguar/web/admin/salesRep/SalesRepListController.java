/*
 * Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 06.25.2007 
 */

package com.webjaguar.web.admin.salesRep;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.SalesRepSearch;

public class SalesRepListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar( WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }	

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		// if add pushed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("salesRep.jhtm"));
		}

		SalesRepSearch salesRepSearch = getSalesRepSearch(request);
		
		PagedListHolder salesRepList = new PagedListHolder(this.webJaguar.getSalesRepList(salesRepSearch)); 
		salesRepList.setPageSize(salesRepSearch.getPageSize());
		salesRepList.setPage(salesRepSearch.getPage() - 1);
		
		java.util.Map<String, Object> myModel = new java.util.HashMap<String, Object>();
		myModel.put("salesReps", salesRepList);
		if (request.getSession().getAttribute("message") != null) 
			myModel.put("message", request.getSession().getAttribute( "message")); 
		
		if (salesRepSearch.getParent() != null) {
			myModel.put("salesRepTree", this.webJaguar.getSalesRepTree(salesRepSearch.getParent()));			
		}
		myModel.put("groupList", this.webJaguar.getSalesRepGroupList());
		return new ModelAndView("admin/salesRep/list", "model", myModel);
	}

	private SalesRepSearch getSalesRepSearch(HttpServletRequest request) {
		SalesRepSearch salesRepSearch = (SalesRepSearch) request.getSession().getAttribute("salesRepSearch");
		if (salesRepSearch == null) {
			salesRepSearch = new SalesRepSearch();
			request.getSession().setAttribute("salesRepSearch", salesRepSearch);
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			salesRepSearch.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}
		
		// name
		if (request.getParameter("name") != null) {
			salesRepSearch.setName(ServletRequestUtils.getStringParameter(request, "name", ""));
		}
				
		// email
		if (request.getParameter("email") != null) {
			salesRepSearch.setEmail(ServletRequestUtils.getStringParameter(request, "email", ""));
		}	
		
		// account number
		if (request.getParameter("accountNumber") != null) {
			salesRepSearch.setAccountNumber(ServletRequestUtils.getStringParameter(request, "accountNumber", ""));
		}
		
		//salesrepGroup
		if (request.getParameter("salesrepGroup") != null) {
			salesRepSearch.setSalesrepGroup(ServletRequestUtils.getStringParameter(request, "salesrepGroup", ""));
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				salesRepSearch.setPage(1);
			} else {
				salesRepSearch.setPage(page);				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10);
			if (size < 10) {
				salesRepSearch.setPageSize(10);
			} else {
				salesRepSearch.setPageSize(size);				
			}
		}
		
		// parent ID
		if (request.getParameter("parent") != null) {
			int parentId = ServletRequestUtils.getIntParameter(request, "parent", 0);		
			if (parentId == -1) {
				salesRepSearch.setParent(-1);				
			} else if (parentId > 0) {
				salesRepSearch.setParent(parentId);
			} else {
				salesRepSearch.setParent(null);
			}		
		}
		
		return salesRepSearch;
	}

}