/*
 * Copyright 2005, 2007 Advanced E-Media Solutions @author Shahin Naji
 * 
 * @since 06.25.2007
 */

package com.webjaguar.web.admin.salesRep;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.form.SalesRepForm;

public class SalesRepFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private Map<String, Object> gSiteConfig;
	private Map<String, Configuration> siteConfig;

	public SalesRepFormController() {
		setSessionForm(false);
		setCommandName("salesRepForm");
		setCommandClass(SalesRepForm.class);
		setFormView("admin/salesRep/form");
		setSuccessView("salesRepList.jhtm");
		this.setValidateOnBinding(true);
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {

		SalesRepForm salesRepForm = (SalesRepForm) command;
		
		// check if delete button was pressed
		String deleteSalesRep = ServletRequestUtils.getStringParameter(request, "delete");
		if (deleteSalesRep != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		// protected access
       	if (siteConfig.get( "SALESREP_PROTECTED_ACCESS" ).getValue().equals( "true" ) && salesRepForm.getProtectedAccess() != null) {
       		StringBuffer protectedAccess = new StringBuffer();
           	for (int i=0; i<salesRepForm.getProtectedAccess().length; i++) {
           		if (request.getParameter("__enabled_" + i) != null) {
           			protectedAccess.insert(0, '1');
           		} else {
           			protectedAccess.insert(0, '0');
           		}
           	}
           	salesRepForm.getSalesRep().setProtectedAccess(protectedAccess.toString());
       	}
		
		if (salesRepForm.getSalesRep().getParent() != null && !salesRepForm.isNewSalesRep()) { 
			List<SalesRep> salesRepTree = this.webJaguar.getSalesRepTree(salesRepForm.getSalesRep().getParent());
			boolean loop = false;
			for (SalesRep salesRep: salesRepTree) {
				if (salesRep.getId().compareTo(salesRepForm.getSalesRep().getId()) == 0) {
					loop = true;
				}
			}
			if (salesRepTree.size() == 0 || loop) {
				if (loop) {
					errors.rejectValue("salesRep.parent", "salesRep.exception.loop");
				} else {
					errors.rejectValue("salesRep.parent", "salesRep.exception.notfound");					
				}
				return showForm(request, response, errors);
			}
		}
		
		if (salesRepForm.isNewSalesRep()) {
			try {
				this.webJaguar.insertSalesRep(salesRepForm.getSalesRep());
			} catch (Exception e) {
				// no duplicate entry is allowed.
			}
		} else {
			this.webJaguar.updateSalesRep(salesRepForm.getSalesRep());
		}

		return new ModelAndView(new RedirectView(getSuccessView()));
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		SalesRepForm salesRepForm = (SalesRepForm) command;
	
		// check if delete button was pressed
		String deleteSalesRep = ServletRequestUtils.getStringParameter(request, "delete");
		if (deleteSalesRep != null) {
			System.out.println("salesRepForm.getSalesRep().isInactive() "+salesRepForm.getSalesRep().isInactive());
			try {
				this.webJaguar.deleteSalesRep(salesRepForm.getSalesRep().getId());
			} catch (Exception e) {
				errors.rejectValue("salesRep.name", "form.salesRep.orderUsingThisSalesRep", "One of orders or customers is assigned to this Sales Rep.");
			}
		}
		
		// set salesRep to inactive, check does salesRep have customer or not.
		if(salesRepForm.getSalesRep().isInactive() == true){
			if(this.webJaguar.salesRepHasCustomer(salesRepForm.getSalesRep().getId())){
				errors.rejectValue("salesRep.name", "form.salesRep.customerAssignedToThisSalesRep", "Customers is assigned to this Sales Rep.");
			}
		}
	}
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;
		}
		return false;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		SalesRepForm salesRepForm = new SalesRepForm();

		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		if (request.getParameter("id") != null) {
			Integer salesRepId = new Integer(request.getParameter("id"));
			SalesRep salesRep = this.webJaguar.getSalesRepById(salesRepId);
			
			// protected access
			int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
			if (protectedLevels > 0 &&  siteConfig.get( "SALESREP_PROTECTED_ACCESS" ).getValue().equals( "true" ) ) {
				boolean protectedAccess[] = new boolean[protectedLevels];
				for (int i=0; i<salesRep.getProtectedAccess().length(); i++) {
					if (salesRep.getProtectedAccess().charAt(i) == '1') {
						protectedAccess[salesRep.getProtectedAccess().length()-1-i] = true;
					}
				}
				salesRepForm.setProtectedAccess(protectedAccess);	
			}

			salesRepForm.setSalesRep(salesRep);
			salesRepForm.setNewSalesRep(false);
		}
		if (salesRepForm.getSalesRep().getAddress() == null) {
			salesRepForm.getSalesRep().setAddress(new Address());
		}

		return salesRepForm;

	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("countries",this.webJaguar.getEnabledCountryList());
		map.put("states",this.webJaguar.getStateList("US"));
		map.put("caProvinceList", this.webJaguar.getStateList("CA"));
		map.put("salesReps", this.webJaguar.getSalesRepList());
		// labels
		map.put("labels", this.webJaguar.getLabels("protected"));
		return map;
	}

}
