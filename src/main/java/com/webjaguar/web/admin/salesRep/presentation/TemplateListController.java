package com.webjaguar.web.admin.salesRep.presentation;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.PresentationTemplateSearch;

public class TemplateListController  implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		// if add pushed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("templateForm.jhtm"));
		}
		
		PresentationTemplateSearch templateSearch = getTemplateSearch(request);
		PagedListHolder templateList = new PagedListHolder(this.webJaguar.getPresentationTemplateList(templateSearch));
		templateList.setPageSize(templateSearch.getPageSize());
		templateList.setPage(templateSearch.getPage() - 1);
		
		Map<String, Object> myModel = new java.util.HashMap<String, Object>();
		
		myModel.put("templateSearch", templateSearch);
		myModel.put("templateList", templateList);
		
		return new ModelAndView("admin/salesRep/presentation/list", "model", myModel);
	}
	
	private PresentationTemplateSearch getTemplateSearch(HttpServletRequest request) {
		PresentationTemplateSearch templateSearch = (PresentationTemplateSearch) request.getSession().getAttribute("templateSearch");
		if (templateSearch == null) {
			templateSearch = new PresentationTemplateSearch();
			request.getSession().setAttribute("templateSearch", templateSearch);
		}
		

		// sorting
		if (request.getParameter("sort") != null) {
			templateSearch.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}
		
		// name
		if (request.getParameter("name") != null) {
			templateSearch.setName(ServletRequestUtils.getStringParameter(request, "name", ""));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				templateSearch.setPage(1);
			} else {
				templateSearch.setPage(page);				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10);
			if (size < 10) {
				templateSearch.setPageSize(10);
			} else {
				templateSearch.setPageSize(size);				
			}
		}
		
		return templateSearch;
	}

}
