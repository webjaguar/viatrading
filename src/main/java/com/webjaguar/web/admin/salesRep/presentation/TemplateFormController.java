package com.webjaguar.web.admin.salesRep.presentation;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Presentation;
import com.webjaguar.model.PresentationTemplate;
import com.webjaguar.web.form.TemplateForm;

public class TemplateFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public TemplateFormController() {
		setSessionForm(false);
		setCommandName("templateForm");
		setCommandClass(TemplateForm.class);
		setFormView("admin/salesRep/presentation/templateForm");
		setSuccessView("templateList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws ServletException {
		
		TemplateForm templateForm = (TemplateForm) command;
		
		if (templateForm.isNewTemplate()) {
			try {
				this.webJaguar.insertTemplate(templateForm.getTemplate());
			} catch (Exception e) {
				// no duplicate entry is allowed.
			}
		} else {
			this.webJaguar.updateTemplate(templateForm.getTemplate());
		}
		return new ModelAndView(new RedirectView(getSuccessView()));
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		TemplateForm templateForm = (TemplateForm) command;
		
		
		// check if delete button was pressed
		String deletetemplate = ServletRequestUtils.getStringParameter(request, "delete");
			if (deletetemplate != null) {
				try {
					Presentation presentation = new Presentation();
					presentation.setTemplateId(templateForm.getTemplate().getId());
					if(this.webJaguar.getPresentationList(presentation).isEmpty()) {
						this.webJaguar.deleteTemplate(templateForm.getTemplate().getId());
					} else {
						errors.rejectValue("template.name", "form.template.templateInUse", "A Presentation is using this template.");
					}
				} catch (Exception e) {
					
				}
			}
	}
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;
		}
		
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		TemplateForm templateForm = new TemplateForm();
		PresentationTemplate template = new PresentationTemplate();
		templateForm.setTemplate(template);
		if (request.getParameter("id") != null) {
			Integer templateId = new Integer(request.getParameter("id"));
			template = this.webJaguar.getTemplateById(templateId);

			templateForm.setTemplate(template);
			templateForm.setNewTemplate(false);
		} else {
			templateForm.setNewTemplate(true);
		}
		return templateForm;
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		return map;
	}
	
}
