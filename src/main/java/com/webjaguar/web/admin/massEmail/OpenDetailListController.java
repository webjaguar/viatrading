package com.webjaguar.web.admin.massEmail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ClickDetail;
import com.webjaguar.model.ClickDetailSearch;
import com.webjaguar.model.ReportFilter;

public class OpenDetailListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		ClickDetailSearch clickDetailSearch = getClickDetialSearch(request);
		
		if (request.getParameter("campaignId") != null){
			clickDetailSearch.setClickId(Integer.parseInt(request.getParameter("campaignId")));
			Integer salesRepId = null;
			//sales rep filter
			if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
			}
			clickDetailSearch.setSalesRepId(salesRepId);
			List<ClickDetail> openDetailList = this.webJaguar.getOpenDetail(clickDetailSearch);
			PagedListHolder list = new PagedListHolder(openDetailList);
			
			list.setPageSize(clickDetailSearch.getPageSize());
			list.setPage(clickDetailSearch.getPage()-1);
			myModel.put("campaignId", request.getParameter("campaignId"));
			myModel.put("openDetailsList", list);
			
			List<Integer> contactIds = new ArrayList<Integer>();
			for(ClickDetail cd : openDetailList) {
				contactIds.add(cd.getContactId());
			}
			
			// check if options button was clicked
			if (request.getParameter("__groupAssignPacher") != null) {
				// get group ids
				Set<Object> groups = new HashSet<Object>();
				String groupIdString = ServletRequestUtils.getStringParameter( request, "__group_id" );
				String batchType = ServletRequestUtils.getStringParameter( request, "__batch_type" );
				String[] groupIds = groupIdString.split( "," );
				for ( int x = 0; x < groupIds.length; x++ ) {
					if ( !("".equals( groupIds[x].trim() )) ) {
						try {
							groups.add( Integer.valueOf( groupIds[x].trim() ) );
						}catch ( Exception e ) {
							// do nothing
						}
					}
				}
				String ids[] = request.getParameterValues("__selected_id");
				if ((ids != null ) && ( request.getParameter("__groupAssignAll") == null)) {
					for (int i=0; i<ids.length; i++) {
						this.webJaguar.batchCrmContactGroupIds( Integer.parseInt(ids[i]), groups, batchType);
					}
				} else if( request.getParameter("__groupAssignAll") != null ) {
					for(Integer id: contactIds) {
						this.webJaguar.batchCrmContactGroupIds( id, groups, batchType);
					}
				}
			}
			
			return new ModelAndView("admin/massEmail/openDetailList", "model", myModel);
		} else {
			return new ModelAndView("admin/massEmail/massEmailList");
		}
	}

	private ClickDetailSearch getClickDetialSearch(HttpServletRequest request) {
		ClickDetailSearch clickDetailSearch = (ClickDetailSearch) request.getSession().getAttribute( "openMassEmailDetailSearch" );
		if (clickDetailSearch == null) {
			clickDetailSearch = new ClickDetailSearch();
			clickDetailSearch.setSort( "campaign_id DESC" );
			request.getSession().setAttribute( "openMassEmailDetailSearch", clickDetailSearch );
		}
		
		// sort
		if (request.getParameter("sort") != null) {
			clickDetailSearch.setSort( ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				clickDetailSearch.setPage( 1 );
			} else {
				clickDetailSearch.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				clickDetailSearch.setPageSize( 10 );
			} else {
				clickDetailSearch.setPageSize( size );				
			}
		}
		
		return clickDetailSearch;
	}
	
	
}
