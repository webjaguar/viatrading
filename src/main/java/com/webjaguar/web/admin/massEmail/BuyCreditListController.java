package com.webjaguar.web.admin.massEmail;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.dao.MassEmailDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ReportFilter;


public class BuyCreditListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ReportFilter buyCreditFilter = getReportFilter( request );
		Map<String, Object> myModel = new HashMap<String, Object>();

		PagedListHolder buyCreditList = new PagedListHolder(this.webJaguar.getBuyCreditList(buyCreditFilter));		
		
		buyCreditList.setPageSize(buyCreditFilter.getPageSize());
		buyCreditList.setPage(buyCreditFilter.getPage()-1);

		myModel.put( "buyCreditList", buyCreditList );
        return new ModelAndView("admin/massEmail/buyCreditList", "model", myModel);
	}

	private ReportFilter getReportFilter(HttpServletRequest request) {
		ReportFilter buyCreditFilter = (ReportFilter) request.getSession().getAttribute( "buyCreditFilter" );
		if (buyCreditFilter == null) {
			buyCreditFilter = new ReportFilter();
			buyCreditFilter.setSort( "created DESC" );
			request.getSession().setAttribute( "buyCreditFilter", buyCreditFilter );
		}
		
		// sort
		if (request.getParameter("sort") != null) {
			buyCreditFilter.setSort( ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				buyCreditFilter.setPage( 1 );
			} else {
				buyCreditFilter.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				buyCreditFilter.setPageSize( 10 );
			} else {
				buyCreditFilter.setPageSize( size );				
			}
		}

		return buyCreditFilter;
	}
}
