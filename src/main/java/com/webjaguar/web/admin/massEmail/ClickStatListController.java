package com.webjaguar.web.admin.massEmail;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Click;
import com.webjaguar.model.Search;

public class ClickStatListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request,HttpServletResponse response) throws Exception {
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		Search search = getSearch(request);
		if(request.getParameter("campaignId") != null) {
			Click click = new Click();
			click.setCampaignId(Integer.parseInt(request.getParameter("campaignId")));
			myModel.put("campaignId", click.getCampaignId());
			
			PagedListHolder list = new PagedListHolder(this.webJaguar.getClick(click));
			
			list.setPageSize(search.getPageSize());
			list.setPage(search.getPage()-1);
			
			myModel.put("clickList", list);
			
			return new ModelAndView("admin/massEmail/clickList", "model", myModel);
		} else {
			return new ModelAndView("admin/massEmail/massEmailList");
		}
	}
	
	private Search getSearch(HttpServletRequest request) {
		Search search = (Search) request.getSession().getAttribute("clickStatMassEmailSearch");
		if (search == null) {
			search = new Search();
			request.getSession().setAttribute("clickStatMassEmailSearch", search);
		}

		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);				
			}
		}			
		
		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				search.setPageSize(10);
			} else {
				search.setPageSize(size);				
			}
		}
		
		return search;
	}
}