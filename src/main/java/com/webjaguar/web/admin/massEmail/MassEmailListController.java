package com.webjaguar.web.admin.massEmail;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ReportFilter;


public class MassEmailListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ReportFilter campaignFilter = getReportFilter( request );
		Map<String, Object> myModel = new HashMap<String, Object>();

		PagedListHolder compaignList = new PagedListHolder(this.webJaguar.getEmailCampaigns( campaignFilter ));		
		
		compaignList.setPageSize(campaignFilter.getPageSize());
		compaignList.setPage(campaignFilter.getPage()-1);
		myModel.put( "compaignList", compaignList );
        return new ModelAndView("admin/massEmail/massEmailList", "model", myModel);
	}

	private ReportFilter getReportFilter(HttpServletRequest request) {
		ReportFilter campaignFilter = (ReportFilter) request.getSession().getAttribute( "campaignFilter" );
		if (campaignFilter == null) {
			campaignFilter = new ReportFilter();
			campaignFilter.setSort( "sent DESC" );
			request.getSession().setAttribute( "campaignFilter", campaignFilter );
		}
		
		// sort
		if (request.getParameter("sort") != null) {
			campaignFilter.setSort( ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				campaignFilter.setPage( 1 );
			} else {
				campaignFilter.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				campaignFilter.setPageSize( 10 );
			} else {
				campaignFilter.setPageSize( size );				
			}
		}

		return campaignFilter;
	}
}
