package com.webjaguar.web.admin.massEmail;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CreditCard;
import com.webjaguar.model.EmailCampaignBalance;
import com.webjaguar.model.Order;
import com.webjaguar.thirdparty.payment.authorizenet.AuthorizeNetApi;
import com.webjaguar.web.form.BuyCreditForm;
import com.webjaguar.web.validator.BuyCreditFormValidator;


public class BuyCreditController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	Map<String, Object> gSiteConfig;
	Map<String, Configuration> siteConfig;

	public BuyCreditController()
	{
		setSessionForm( true );
		setCommandName( "buyCreditForm" );
		setCommandClass( BuyCreditForm.class );
		setFormView( "admin/massEmail/buyCredit" );
		setSuccessView( "buyCreditList.jhtm" );
		this.setValidateOnBinding( true );
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) 
	{
		BuyCreditForm form = (BuyCreditForm) command;
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map) request.getAttribute( "siteConfig" );
		// Note: get everything from model
		java.util.Map<String, Object> model = new java.util.HashMap<String, Object>();
		java.util.Map<String, Object> model1 = new java.util.HashMap<String, Object>();
		model1.put("point", siteConfig.get( "MASS_EMAIL_POINT" ).getValue() );
		model1.put("countries",this.webJaguar.getEnabledCountryList());
		model1.put("states",this.webJaguar.getStateList("US"));
		model1.put("years",getyears());
		Collection<GrantedAuthority> auths = (Collection<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
		for ( GrantedAuthority role : auths) {
			if (role.getAuthority().equals( "ROLE_AEM") || role.getAuthority().equals( "ROLE_SITE_INFO")) {
				form.setAemUser(true);
			}
		}
		model.put( "model", model1 );
		
		return model;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception
	{
		BuyCreditForm form = (BuyCreditForm) command;
		
		if (form.isAemUser() ) {
			EmailCampaignBalance balance = new EmailCampaignBalance();
			balance.setUser( ServletRequestUtils.getStringParameter( request, "_backend_user", "Admin" ) );
			balance.setAmount( calculateCost(form.getPoint()) );
			balance.setPoint( form.getPoint() );
			balance.setNote( form.getNote() );
			balance.setPaymentMethod( "Added Manually" );
			this.webJaguar.insertEmailCampaignBalance( balance );
			this.webJaguar.addEmailCampaignPoint( form.getPoint() );
			return new ModelAndView( new RedirectView( getSuccessView() ) );
		}
		
		boolean isSuccess = false;
		EmailCampaignBalance balance = new EmailCampaignBalance();
		try {
			Order buyCreditOrder = new Order();
			init(buyCreditOrder, form);
			this.webJaguar.insertEmailCampaignBalance( balance );
			buyCreditOrder.setOrderId( balance.getId() );
			// Always Authorize.Net
			AuthorizeNetApi authorizeNet = new AuthorizeNetApi();
			isSuccess = authorizeNet.buyMassEmailCredit( buyCreditOrder, siteConfig, gSiteConfig);	
			balance.setTransId( buyCreditOrder.getCreditCard().getTransId() );
			balance.setPaymentStatus( buyCreditOrder.getCreditCard().getPaymentStatus() );
		} catch (Exception e) {
			// log it
			e.printStackTrace();
		}
		if (isSuccess) { 
			String buyer = ServletRequestUtils.getStringParameter( request, "_backend_user", "Admin" );
			balance.setUser( buyer );
			balance.setAmount( form.getAmount() );
			balance.setPoint( form.getPoint() );

			balance.setPaymentMethod( form.getCreditCard().getType() );
			balance.setNote( form.getNote() );

			// update buyCredit in database
			this.webJaguar.updateEmailCampaignBalance( balance );
			this.webJaguar.addEmailCampaignPoint( form.getPoint() );
		} else {
			this.webJaguar.deleteEmailCampaignById( balance.getId() );
			notifyAdmin("Buy Credit on " + siteConfig.get("SITE_URL").getValue(), "Credit Card can not be charge.");
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("message", "creditcard.notCharged");
			return showForm(request, response, errors, model);
		}
		return new ModelAndView( new RedirectView( getSuccessView() ) );
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		BuyCreditForm form = (BuyCreditForm) command;
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "point", "form.required", "required" );
		if (form.getPoint() < 1 ) {
			errors.rejectValue("point", "point", "Point value is out of range."); 
		}
			
		if (request.getParameter( "creditCard.number" ) != null && !form.isAemUser()) {
			BuyCreditFormValidator validator = (BuyCreditFormValidator) getValidator();
			validator.validateCC( form, errors );				
		}
	}
	
	protected boolean suppressBinding(HttpServletRequest request)
	{
		if ( request.getParameter( "_cancel" ) != null )
		{
			return true;
		}
		return false;
	}

	protected boolean suppressValidation(HttpServletRequest request, Object command) 
	{
		BuyCreditForm form = (BuyCreditForm) command;
		if ( form.isAemUser() ) {
			return true;
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception
	{
		BuyCreditForm form = new BuyCreditForm();
		return form;
	}
	
	private void init(Order buyCreditOrder, BuyCreditForm form) {
		buyCreditOrder.setCreditCard( new CreditCard() );
		buyCreditOrder.setBilling( new Address() );
		
		buyCreditOrder.setGrandTotal( calculateCost(form.getPoint()) );
		form.setAmount( buyCreditOrder.getGrandTotal() );
		buyCreditOrder.getCreditCard().setNumber( form.getCreditCard().getNumber() );
		buyCreditOrder.getCreditCard().setCardCode( form.getCreditCard().getCardCode() );
		buyCreditOrder.getCreditCard().setExpireMonth( form.getCreditCard().getExpireMonth() );
		buyCreditOrder.getCreditCard().setExpireYear( form.getCreditCard().getExpireYear() );
		buyCreditOrder.getBilling().setFirstName( form.getBilling().getFirstName() );
		buyCreditOrder.getBilling().setLastName( form.getBilling().getLastName() );
		buyCreditOrder.getBilling().setCompany( gSiteConfig.get( "gSITE_DOMAIN" ).toString() );
		buyCreditOrder.getBilling().setAddr1( form.getBilling().getAddr1() );
		buyCreditOrder.getBilling().setCity( form.getBilling().getCity() );
		buyCreditOrder.getBilling().setStateProvince( form.getBilling().getStateProvince() );
		buyCreditOrder.getBilling().setZip( form.getBilling().getZip() );
		buyCreditOrder.getBilling().setCountry( form.getBilling().getCountry() );
		buyCreditOrder.getBilling().setPhone( form.getBilling().getPhone() );
		buyCreditOrder.getBilling().setFax( "" );
	}
	
	// calculate cost for number of emails
	public double calculateCost(int emailNumber){
		NumberFormat formatter = new DecimalFormat("#0.00");

		double rate1 = 0.01;
		double rate2 = 0.0085;
		double rate3 = 0.0070;
		double rate4 = 0.0055;
		double rate5 = 0.0040;
		double cost = 0;
	    if(1 <= emailNumber && emailNumber <= 1000){
			cost = rate1 * emailNumber;
		}
		else if (1001 <= emailNumber && emailNumber <= 5000){
			cost = rate2 * emailNumber;
		}
		else if (5001 <= emailNumber && emailNumber <= 25000){
			cost = rate3 * emailNumber;
		}
		else if (25001 <= emailNumber && emailNumber <= 100000){
			cost = rate4 * emailNumber;
		}
		else if (100001 <= emailNumber){
			cost = rate5 * emailNumber;
		}
		return Double.parseDouble( formatter.format( cost ) );
	}
	
	private List<String> getyears() {
		List<String> yearList = new ArrayList<String>();
		Calendar now = Calendar.getInstance();
		int year = now.get( Calendar.YEAR );
		
		int past = year - 1;
		int future = year + 8;
		
		while (past<future) {
			yearList.add( "" +  past);
			past++;
		}
		return yearList;
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}
