package com.webjaguar.web.admin.massEmail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ClickDetail;
import com.webjaguar.model.Search;

public class CountDetailListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		Search search = getSearch(request);
		
		if (request.getParameter("clickId") != null){
			ClickDetail clickDetail = new ClickDetail();
			clickDetail.setClickId(Integer.parseInt(request.getParameter("clickId")));
			myModel.put("clickId", clickDetail.getClickId());
			myModel.put("campaignId", request.getParameter("campaignId"));
			myModel.put("campaignUrl", request.getParameter("campaignUrl"));
			
			List<Integer> contactIds = new ArrayList<Integer>();
			List<ClickDetail> clickDetailList = this.webJaguar.getCountDetail(clickDetail);
			for(ClickDetail detail: clickDetailList) {
				detail.setEmail(this.webJaguar.getUserName(detail.getUserId(), detail.getContactId()));
				contactIds.add(detail.getContactId());
				}
			
			// check if options button was clicked
			if (request.getParameter("__groupAssignPacher") != null) {
				// get group ids
				Set<Object> groups = new HashSet<Object>();
				String groupIdString = ServletRequestUtils.getStringParameter( request, "__group_id" );
				String batchType = ServletRequestUtils.getStringParameter( request, "__batch_type" );
				String[] groupIds = groupIdString.split( "," );
				for ( int x = 0; x < groupIds.length; x++ ) {
					if ( !("".equals( groupIds[x].trim() )) ) {
						try {
							groups.add( Integer.valueOf( groupIds[x].trim() ) );
						}catch ( Exception e ) {
							// do nothing
						}
					}
				}
				String ids[] = request.getParameterValues("__selected_id");
				if ((ids != null ) && ( request.getParameter("__groupAssignAll") == null)) {
					for (int i=0; i<ids.length; i++) {
						this.webJaguar.batchCrmContactGroupIds( Integer.parseInt(ids[i]), groups, batchType);
					}
				} else if( request.getParameter("__groupAssignAll") != null ) {
					for(Integer id: contactIds) {
						this.webJaguar.batchCrmContactGroupIds( id, groups, batchType);
					}
				}
			}
			PagedListHolder list = new PagedListHolder(clickDetailList);
			
			list.setPageSize(search.getPageSize());
			list.setPage(search.getPage()-1);
			
			myModel.put("countDetailList", list);
			
			return new ModelAndView("admin/massEmail/countDetailList", "model", myModel);
		} else {
			return new ModelAndView("admin/massEmail/ClickList");
		}
	}
	
	private Search getSearch(HttpServletRequest request) {
		Search search = (Search) request.getSession().getAttribute("countUrlMassEmailSearch");
		if (search == null) {
			search = new Search();
			request.getSession().setAttribute("countUrlMassEmailSearch", search);
		}

		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);				
			}
		}			
		
		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				search.setPageSize(10);
			} else {
				search.setPageSize(size);				
			}
		}
		
		return search;
	}

}
