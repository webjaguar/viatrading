package com.webjaguar.web.admin.massEmail;

import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.webjaguar.web.domain.*;

public class HTMLLinkExtrator {
	  private Pattern patternTag, patternLink;
	  private Matcher matcherTag;
	  private Matcher matcherLink;

	  private final String HTML_A_TAG_PATTERN = 
                    "(?i)<a([^>]+)>(.+?)</a>";

	  private final String HTML_A_HREF_TAG_PATTERN = 
                    "\\s*(?i)href\\s*=\\s*(\"([^\"]*\")|'[^']*'|([^'\">\\s]+))";

	  public HTMLLinkExtrator(){
		  patternTag = Pattern.compile(HTML_A_TAG_PATTERN);
		  patternLink = Pattern.compile(HTML_A_HREF_TAG_PATTERN);
	  }

	  /**
	   * Validate html with regular expression
	   * @param html html content for validation
	   * @return Vector links and link text
	   */
	  public Vector<HtmlLink> grabHTMLLinks(final String html){

		  Vector<HtmlLink> result = new Vector<HtmlLink>();

		  matcherTag = patternTag.matcher(html);

		  while(matcherTag.find()){

			  String href = matcherTag.group(1); //href
			  String linkText = matcherTag.group(2); //link text
			  matcherLink = patternLink.matcher(href);

			  while(matcherLink.find()){

				  String link = matcherLink.group(2); //link
				  HtmlLink htmlLink = new HtmlLink();
				  htmlLink.setLink(link);
				  htmlLink.setLinkText(linkText);
				  htmlLink.setStartOffset(matcherTag.start());
				  htmlLink.setEndOffset(matcherTag.end());
				  result.add(htmlLink);
			  }
		  }

		  return result;
	  }
}