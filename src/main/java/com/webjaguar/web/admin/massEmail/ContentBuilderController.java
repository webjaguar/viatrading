package com.webjaguar.web.admin.massEmail;

import java.util.Map;
import java.util.Vector;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Click;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.web.domain.HtmlLink;
import com.webjaguar.web.domain.Utilities;

public class ContentBuilderController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private final static String KEY = "AECCF9008EDBF4535CCE2345435364FA";
	/*
	 * customerId,-ContactId|clickId
	 * Customer: x,-|x Contact: -,x|x
	 */
	public String replaceClick(String oldString, Integer campaignId,  Customer customer, CrmContact crmContact) {
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		HTMLLinkExtrator html = new HTMLLinkExtrator();
		Vector<HtmlLink> hrefLinks = html.grabHTMLLinks(oldString);
		
		StringBuffer newStringBuffer = new StringBuffer();
		int pinterIndex = 0;
		for (HtmlLink link:hrefLinks) {
			try{
				Click click = createClick(link, campaignId);
				String domainName= siteConfig.get( "SITE_URL" ).getValue();
				String finalValue = null;
	
		    	String input = null;
				if(crmContact == null) {
					input= customer.getId()+",-|"+click.getId();			
				} else {
					input= "-,"+ crmContact.getId() +"|"+click.getId();								
				}
				newStringBuffer.append(oldString.substring(pinterIndex, link.getStartOffset()));
				finalValue = Utilities.AESencrypt(KEY, input);
				newStringBuffer.append("<a href=\"" + domainName + "click.jhtm?value=" +finalValue+ "&url=").append(link.getLink()).append(">");
				newStringBuffer.append(link.getLinkText() + "</a>");
				pinterIndex = link.getEndOffset();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		newStringBuffer.append(oldString.substring(pinterIndex));
		
		String newString = newStringBuffer.toString();		
		return newString;
	}
	
	public Click createClick(HtmlLink link, Integer campaignId) {
		Click click = new Click();
		try {
			click.setUrl(link.getLink().replaceAll("\"", ""));
			click.setCampaignId(campaignId);
			click.setId(this.webJaguar.insertOrGetClick(click));
		}catch(Exception e){
			e.printStackTrace();
		}
		return click;
	}
}