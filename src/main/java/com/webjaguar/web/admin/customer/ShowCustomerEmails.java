
package com.webjaguar.web.admin.customer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.stereotype.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;


@Controller
public class ShowCustomerEmails extends WebApplicationObjectSupport {

	@Autowired
	private WebJaguarFacade webJaguar;

	
	@RequestMapping(value="admin/orders/show-customer-emails.jhtm")
	public @ResponseBody ModelAndView ShowEmails(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
				
		Map<String, Object> myModel = new HashMap<String, Object>();	

		if (request.getParameter( "value" ) != null) {			
			CustomerSearch search = new CustomerSearch();			
			search.setEmail(request.getParameter( "value" ));
			List <Customer> customers = this.webJaguar.getCustomerListWithDate(search);
			myModel.put("customers", customers);
		}
		
        return new ModelAndView("admin/orders/showEmails", "model", myModel);
	}
	
	@RequestMapping(value="admin/orders/show-customer-phonenumbers.jhtm")
	public @ResponseBody ModelAndView ShowPhoneNumbers(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
				
		Map<String, Object> myModel = new HashMap<String, Object>();	

		if (request.getParameter( "value" ) != null) {			
			CustomerSearch search = new CustomerSearch();			
			search.setPhone(request.getParameter( "value" ));			
			List <Customer> customers = this.webJaguar.getCustomerListWithDate(search);
			myModel.put("customers", customers);
			//customers.get(1).getAddress().getPhone()
		}
		
        return new ModelAndView("admin/orders/showPhones", "model", myModel);
	}
	
	@RequestMapping(value="admin/orders/show-customer-cellphonenumbers.jhtm")
	public @ResponseBody ModelAndView ShowCellPhoneNumbers(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
				
		Map<String, Object> myModel = new HashMap<String, Object>();	

		if (request.getParameter( "value" ) != null) {			
			CustomerSearch search = new CustomerSearch();			
			search.setCellPhone(request.getParameter( "value" ));			
			List <Customer> customers = this.webJaguar.getCustomerListWithDate(search);
			myModel.put("customers", customers);
			//customers.get(1).getAddress().getPhone()
		}
		
        return new ModelAndView("admin/orders/showCellPhones", "model", myModel);
	}
	
}
