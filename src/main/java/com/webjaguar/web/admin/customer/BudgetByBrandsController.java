/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.12.2008
 */

package com.webjaguar.web.admin.customer;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Brand;
import com.webjaguar.model.Customer;

public class BudgetByBrandsController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
    	
		Map<String, Object> map = new HashMap<String, Object>();
		
		Customer customer = this.webJaguar.getCustomerById(ServletRequestUtils.getIntParameter(request, "id", 0));
		if (customer != null) {
			map.put("customer", customer);
		} else {
			return new ModelAndView(new RedirectView("customerList.jhtm"));			
		}
		
		// current year
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		map.put("currentYear", currentYear);
		
		List<Brand> brands = this.webJaguar.getBrands(customer.getId());
		for (Brand brand: brands) {
			brand.setTotal(this.webJaguar.getOrderTotalByBrand(brand, customer.getId(), currentYear));
		}		
		
        // check if update button was pressed
		if (request.getParameter("__update") != null) {
			for (Brand brand: brands) {
				try {
					brand.setBudget(ServletRequestUtils.getDoubleParameter(request, "__budget_" + brand.getId()));
					if (brand.getBudget() < 0) {
						brand.setBudget(0.0);
					}
				} catch (Exception e) {
					brand.setBudget(0.0);
				}				
			}
			this.webJaguar.updateBudget(customer.getId(), brands);
			map.put("message", "update.successful");
		}
		
		map.put("brands", brands);
		
		return new ModelAndView("admin/customers/budgetByBrands", map);
	}

}
