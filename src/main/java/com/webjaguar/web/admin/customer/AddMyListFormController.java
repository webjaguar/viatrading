/* Copyright 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 06.20.2007
 */

package com.webjaguar.web.admin.customer;

import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;


	public class AddMyListFormController implements Controller {

		private WebJaguarFacade webJaguar;
		public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }		
		
	    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
	    	throws Exception {	
	    	Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
	    	String message = "update.successful";
	    	
			Map<String, Object> model = new HashMap<String, Object>();    	
			Set productIds = new HashSet();
			Integer userId = org.springframework.web.bind.ServletRequestUtils.getIntParameter( request, "id", -1 );

			// check if update button was pressed
			if (request.getParameter("__add") != null) {

			    String skus = request.getParameter("skus");
			    StringTokenizer st = new StringTokenizer(skus.toString(), "[,\n]");
			    while (st.hasMoreTokens()) {
			    	String skuToken = null;
			    	try {
		            	skuToken = st.nextToken();
		            	productIds.add( Integer.parseInt( this.webJaguar.getProductIdBySku( skuToken.trim() ).toString() ) );
		            }
		            catch ( Exception e ) {
		            	// show the last wrong sku
		            	message = "addList.exception" ;
		            	model.put("skuToken", skuToken);
		            	continue;
		            }
		        }
			    if ( (Boolean) gSiteConfig.get( "gMYLIST" )) {
			        this.webJaguar.addToWishList(userId,productIds);
    		    }
				model.put("message", message);	
			}    	
			model.put("userId", userId);
	    	return new ModelAndView("admin/customers/myListForm", "model", model);
	    }
	}