/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.customer;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.Utilities;

public class LoginAsCustomerController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {

    	String token = org.springframework.web.bind.ServletRequestUtils.getStringParameter( request, "cid", null );
    	String type = org.springframework.web.bind.ServletRequestUtils.getStringParameter( request, "type", null );
    	Customer customer = this.webJaguar.getCustomerByToken( token );
    	
    	if ( customer != null )
    	{
    		//  set user session
			UserSession userSession = new UserSession();
			userSession.setUsername(customer.getUsername());
			userSession.setFirstName( customer.getAddress().getFirstName() );
			userSession.setLastName( customer.getAddress().getLastName() );
			userSession.setUserid(customer.getId());
			this.webJaguar.setUserSession(request, userSession);
			
			//cookie code
			// KeepMeLogin Module
			String path = request.getContextPath().equals("") ? "/" : request.getContextPath();
			Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
			System.out.println("path::> "+path);
			if(siteConfig.size() > 0) {
				System.out.println("in if siteConfig.size() > 0");
				if (Integer.parseInt(siteConfig.get("COOKIE_AGE_KEEP_ME_LOGIN").getValue()) > -1) {
					String token1 = this.webJaguar.createPasswordToken(customer.getId());
					// without this mod rewrite pages will save multiple cookies with different paths
					//String path = request.getContextPath().equals("") ? "/" : request.getContextPath();
					int days = Integer.parseInt(siteConfig.get("COOKIE_AGE_KEEP_ME_LOGIN").getValue());
					String cookieValue = token1;
					Utilities.addCookie(response,"UKWJ", cookieValue, Constants.SECONDS_IN_DAY * days, path);
				}
			}
			
			// Encode customer ID in cookies
			if(customer!= null && customer.getId()!=null && !customer.getId().toString().isEmpty()){
				try{
					System.out.println("in if customer!= null");
					String orig = customer.getId().toString();
				    byte[] encoded = Base64.encodeBase64(orig.getBytes());         
				    String encode = new String(encoded);   
			
				    //System.out.println("orig"+ customer.getId().toString() +"\n encoded"+ encode + "\n decoded" + decode);
				    
					Cookie CustID = new Cookie("CI", encode);
					int days = Integer.parseInt(siteConfig.get("COOKIE_AGE").getValue());
					CustID.setMaxAge(-1);
					CustID.setPath(path);
					response.addCookie(CustID);
				} catch (Exception e){
					Cookie CustID = new Cookie("CI", "ERROR");
					CustID.setPath(path);
					response.addCookie(CustID);
				}
		   }
    	}
    	if(type!=null&&type.equals("abandonedShoppingCart")){
    		return new ModelAndView(new RedirectView(request.getContextPath() + "/viewCart.jhtm")); 
    	}
        return new ModelAndView(new RedirectView(request.getContextPath() + "/account.jhtm"));    	
    }

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

}
