package com.webjaguar.web.admin.customer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.record.formula.functions.And;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Affiliate;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.VirtualBank;
import com.webjaguar.model.VirtualBankOrderSearch;

public class VirtualBankPaymentListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	private Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		Map<String, Object> myModel = new HashMap<String, Object>();
		if (siteConfig.get( "SITE_MESSAGE_ID_FOR_SUPPLIER_PAYMENT" ).getValue() == "") {
			myModel.put("message", "SiteMessageErrorMessage");			
		}
		//this.webJaguar.getCustomerByUsername(username);
		List<VirtualBank> orderList  = new ArrayList<VirtualBank>();
		
		VirtualBankOrderSearch orderSearch = getSearch(request);
		Customer customer = this.webJaguar.getCustomerByUsername(orderSearch.getUserName());
		
		
		if(customer != null) {
			orderSearch.setUserId(customer.getId().toString());
			if(customer.getSupplierId() != null) {
				orderSearch.setSupplierId(customer.getSupplierId().toString());	
			} else {
				orderSearch.setSupplierId(null);
			}
		} else {
			orderSearch.setUserId(null);
			orderSearch.setSupplierId(null);
		}
		orderSearch.setProductConsignmentNote(siteConfig.get("CONSIGNMENT_NOTE_FIELD").getValue());
		orderList = this.webJaguar.getConsignmentAndAffiliateOrders(orderSearch);
		
		// Index
		int index=0;
		for(VirtualBank vbIndex: orderList) {
			vbIndex.setIndex(index);
			++index;
		}
		
		// check if make payment is clicked
		if ( (request.getParameter( "__make_payment" ) != null && siteConfig.get( "SITE_MESSAGE_ID_FOR_SUPPLIER_PAYMENT" ).getValue() !="") || request.getParameter( "__batchUpdate" ) != null)
		{
			List<Order> vbaOrders = new ArrayList<Order>();
			List<LineItem> vbaOrdersLineItems = new ArrayList<LineItem>();
			DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm a");
			Map<Integer, Integer> users = new HashMap <Integer, Integer>();
			for(VirtualBank order: orderList) {
				
				if(order.getCommission() != 0 && ! order.getCommission().equals("") ) {
					Order vbaOrder = new Order();
					if(request.getParameter("__userId_"+order.getIndex()) != null) {
						vbaOrder.setUserId(Integer.parseInt(request.getParameter("__userId_"+order.getIndex())) );
					}
					vbaOrder.setOrderId(order.getOrderId());
					if(request.getParameter("__amountToPay_"+order.getIndex()) != null) {
						vbaOrder.setVbaAmountToPay(Double.parseDouble(request.getParameter("__amountToPay_"+order.getIndex())));
					}
					if(request.getParameter("__note_"+order.getIndex()) != null) {
						vbaOrder.setVbaNote(request.getParameter("__note_"+order.getIndex()));
					}
					if(request.getParameter("__paymentMethod_"+order.getIndex()) != null) {
						vbaOrder.setVbaPaymentMethod(request.getParameter("__paymentMethod_"+order.getIndex()));
					}
					vbaOrder.setVbaPaymentStatus(true);
					vbaOrder.setVbaTransactionId(UUID.randomUUID().toString().toUpperCase().replace( "-", "" ).substring( 0, 9 ));
					if(request.getParameter("__startDate_"+order.getIndex()) != null && request.getParameter("__startDate_"+order.getIndex()) != "") {
						vbaOrder.setVbaDateOfPay( dateFormat.parse(request.getParameter("__startDate_"+order.getIndex())));	
					}
					vbaOrder.setAffiliate(new Affiliate());
					vbaOrder.getAffiliate().setAffiliateLevel1Id(order.getAffiliateId());
					// Check if Payment Method is selected then update
					if( vbaOrder.getVbaPaymentMethod() != null && vbaOrder.getVbaPaymentMethod() !="" ) {
						vbaOrders.add(vbaOrder);
						if (siteConfig.get( "SITE_MESSAGE_ID_FOR_AFFILIATE_PAYMENT" ).getValue() != null ) {
							users.put(vbaOrder.getUserId(), Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_AFFILIATE_PAYMENT" ).getValue()));
						}
					}
				} else {
					LineItem vbaOrderLineItem = new LineItem();					
					if(request.getParameter("__userId_"+order.getIndex()+"_"+order.getProductSku()) != null) {
						vbaOrderLineItem.setSupplier(this.webJaguar.getSupplierById(Integer.parseInt(request.getParameter("__userId_"+order.getIndex()+"_"+order.getProductSku()))));
					}
					vbaOrderLineItem.setVbaProductSku(order.getProductSku());
					vbaOrderLineItem.setOrderId(order.getOrderId());
					if(request.getParameter("__amountToPay_"+order.getIndex()+"_"+order.getProductSku()) != null && !request.getParameter("__amountToPay_"+order.getIndex()+"_"+order.getProductSku()).equals("")) {
						vbaOrderLineItem.setVbaAmountToPay(Double.parseDouble(request.getParameter("__amountToPay_"+order.getIndex()+"_"+order.getProductSku())));
					}
					if(request.getParameter("__note_"+order.getIndex()) != null) {
						vbaOrderLineItem.setVbaNote(request.getParameter("__note_"+order.getIndex()));
					}
					if(request.getParameter("__paymentMethod_"+order.getIndex()) != null) {
						vbaOrderLineItem.setVbaPaymentMethod(request.getParameter("__paymentMethod_"+order.getIndex()));
					}
					vbaOrderLineItem.setVbaPaymentStatus(true);
					vbaOrderLineItem.setLineNumber(order.getOrderLineNum());
					vbaOrderLineItem.setVbaTransactionId(UUID.randomUUID().toString().toUpperCase().replace( "-", "" ).substring( 0, 9 ));
					if(request.getParameter("__startDate_"+order.getIndex()) != null && request.getParameter("__startDate_"+order.getIndex()) != "") {
						vbaOrderLineItem.setVbaDateOfPay(dateFormat.parse(request.getParameter("__startDate_"+order.getIndex())));
					}
					// Check if Payment Method is selected then update
					if( vbaOrderLineItem.getVbaPaymentMethod() != null && vbaOrderLineItem.getVbaPaymentMethod() !="" ) {
						vbaOrdersLineItems.add(vbaOrderLineItem);
						if(vbaOrderLineItem.getSupplier() != null) {
							users.put(vbaOrderLineItem.getSupplier().getUserId(), Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_SUPPLIER_PAYMENT" ).getValue()));	
						}
					}
				}				
			}
			this.webJaguar.updateVbaOrders(vbaOrders, vbaOrdersLineItems,this.webJaguar.getAccessUser(request).getUsername());			
			notifySuppliers(request, users);
			
			orderSearch.setProductConsignmentNote(siteConfig.get("CONSIGNMENT_NOTE_FIELD").getValue());
			orderList = this.webJaguar.getConsignmentAndAffiliateOrders(orderSearch);
			PagedListHolder updatedVbaPaymentList = orderList(orderSearch, orderList);
			
			myModel.put("username", request.getParameter("username"));
			myModel.put( "orderList", updatedVbaPaymentList );			
			myModel.put( "currentTime", new Date() );
			myModel.put("message", "paymentMessage");
			return new ModelAndView( "admin/customers/vbaPaymentList", "model", myModel );
		}
		
		PagedListHolder vbaPaymentList = orderList(orderSearch, orderList);
		
		myModel.put("username", request.getParameter("username"));
		myModel.put( "orderList", vbaPaymentList );
		myModel.put( "currentTime", new Date() );
		myModel.put("dateOfPay", new Date());
		return new ModelAndView( "admin/customers/vbaPaymentList", "model", myModel );
	}
	
	private void notifySuppliers(HttpServletRequest request, Map<Integer, Integer> users ) throws MessagingException
	{
		// send email
		MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
		SiteMessage siteMessage = null;
		Iterator it = users.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry userId = (Map.Entry)it.next();
			Customer customer = this.webJaguar.getCustomerById(Integer.parseInt(userId.getKey().toString()) );
			// send email
			String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
			
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));
			
	    	StringBuffer message = new StringBuffer();
			try {
				Integer messageId = Integer.parseInt(userId.getValue().toString());
				siteMessage = this.webJaguar.getSiteMessageById( messageId );
				
				//Subject
				siteMessage.setSubject(siteMessage.getSubject().replace( "#firstname#", (customer.getAddress().getFirstName() != null) ? customer.getAddress().getFirstName() : "" ) );
				siteMessage.setSubject(siteMessage.getSubject().replace( "#lastname#", (customer.getAddress().getLastName() != null) ? customer.getAddress().getLastName() : "" ) );
				
				//Message
				siteMessage.setMessage(siteMessage.getMessage().replace( "#firstname#", (customer.getAddress().getFirstName() != null) ? customer.getAddress().getFirstName() : "" ) );
				siteMessage.setMessage(siteMessage.getMessage().replace( "#lastname#", (customer.getAddress().getLastName() != null) ? customer.getAddress().getLastName() : "" ) );
								
				if (siteMessage != null) {
			    	message.append( siteMessage.getMessage() );			    	
			    	helper.setTo(customer.getUsername());				    	
			    	helper.setFrom(contactEmail);			    	
			    	helper.setBcc(contactEmail);
			    	helper.setSubject( siteMessage.getSubject());
			    	helper.setText(message.toString(), true);
					mailSender.send(mms);				
			    	
				}
				
			} catch (Exception e) {
				e.printStackTrace();				
			}
	        it.remove(); // avoids a ConcurrentModificationException
	    }
	}
	
	private PagedListHolder orderList (VirtualBankOrderSearch orderSearch, List<VirtualBank> orderList) {
				
		//user id
		List<VirtualBank> userIdList = new ArrayList<VirtualBank>();		
		for(VirtualBank userId :orderList) {
			VirtualBank thisUserId = new VirtualBank();	
			thisUserId.setUserId(userId.getUserId());
			userIdList.add(thisUserId);
		}
		if (userIdList.size() > 0) {
			Map<Integer, String> companyMap = this.webJaguar.getCustomerCompanyMap(userIdList);
			
			for(VirtualBank vb :orderList){
				if(vb.getUserId() != null && ! vb.getUserId().equals("")) {
					if(companyMap.containsKey((long) vb.getUserId() )) {
						vb.setCompanyName(companyMap.get((long) vb.getUserId()));
					}
				} else {
					vb.setCompanyName("");
				}
			}
		}
					
		PagedListHolder updatedVbaPaymentList = new PagedListHolder(orderList);			
		updatedVbaPaymentList.setPageSize(orderSearch.getPageSize());
		updatedVbaPaymentList.setPage(orderSearch.getPage()-1);
		
		return updatedVbaPaymentList;
	}
	
	private VirtualBankOrderSearch getSearch(HttpServletRequest request) {
		
		VirtualBankOrderSearch search = (VirtualBankOrderSearch) request.getSession().getAttribute("vbaOrderSearch");
		if (search == null) {
			search = new VirtualBankOrderSearch();
			request.getSession().setAttribute( "vbaOrderSearch", search );
		}		
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		// user name 
		if (request.getParameter("userName") != null) {
			search.setUserName(ServletRequestUtils.getStringParameter( request, "userName", "" ));	
		} else {
			search.setUserName(request.getParameter("username"));
		}
		
		// order Id
		if (request.getParameter("orderNum") != null) {		
			try {
				Integer orderNum = Integer.parseInt( request.getParameter("orderNum") );
				if (orderNum > 0) {
					search.setOrderId( orderNum.toString() );
				} else {
					search.setOrderId( "" );					
				}
			} catch (Exception e) {
				search.setOrderId( "" );
			}
		}
		
		// Product Sku
		if (request.getParameter("productSku") != null) {		
			search.setProductSku(request.getParameter("productSku"));
		}
		
		// Payment For
		if(request.getParameter("paymentFor") != null) {
			search.setPaymentFor(request.getParameter("paymentFor"));
		}
		
		// Order Status
		if(request.getParameter("orderStatus") != null) {
			search.setOrderStatus(ServletRequestUtils.getStringParameter( request, "orderStatus", "" ));	
		} else {
			search.setOrderStatus("s");
		}
		
		// sort
		if (request.getParameter("sort") != null) {
			search.setSort( ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}		
		
		return search;		
	}
}