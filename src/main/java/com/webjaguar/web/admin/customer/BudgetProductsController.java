/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.14.2009
 */

package com.webjaguar.web.admin.customer;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.BudgetProductSearch;

public class BudgetProductsController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
		
		// current year
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		BudgetProductSearch search = getBudgetProductSearch(request, currentYear);
		
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			if (search.getUserId() != null) {
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("cid", search.getUserId());
				params.put("productSku", search.getSku());
				return new ModelAndView(new RedirectView("budgetByProduct.jhtm"), params);
			} else {
				return new ModelAndView(new RedirectView("customerList.jhtm"));				
			}
		}
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		
		PagedListHolder list = new PagedListHolder(this.webJaguar.getBudgetProductList(search));		
		
		/*
		 Used for inserting new budget based on last year's balance
		BudgetProductSearch search2 = new BudgetProductSearch();
		search2.setYear(2010);
		search2.setUserId(43);
		for (BudgetProduct budgetProduct: this.webJaguar.getBudgetProductList(search2)) {
			if (budgetProduct.getBalance() > 0) {
				budgetProduct.setEntered(new Date());
				budgetProduct.setQty(budgetProduct.getBalance());
				this.webJaguar.insertBudgetProduct(budgetProduct);					
			}
		}
		*/
		
		list.setPageSize(search.getPageSize());
		list.setPage(search.getPage()-1);
		
		model.put("list", list);
		List<Integer> years = this.webJaguar.getBudgetProductYears(search);
		if (years.isEmpty() || years.get(0) != currentYear) {
			years.add(0, currentYear);
		}
		model.put("years", years);
		model.put("customer", this.webJaguar.getCustomerById(ServletRequestUtils.getIntParameter(request, "cid", -1)));
    	
        return new ModelAndView("admin/customers/budgetByProducts", "model", model);
	}

	private BudgetProductSearch getBudgetProductSearch(HttpServletRequest request, int currentYear) {
		BudgetProductSearch search = (BudgetProductSearch) request.getSession().getAttribute("budgetProductSearch");
		if (search == null) {
			search = new BudgetProductSearch();
			request.getSession().setAttribute("budgetProductSearch", search);
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}		
		
		// user id
		if (request.getParameter("cid") != null) {
			int cid = ServletRequestUtils.getIntParameter(request, "cid", 0);
			if (cid <= 0) {
				search.setUserId(null);
			} else {
				search.setUserId(cid);				
			}
		}		
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);				
			}
		}			
		
		// year
		if (request.getParameter("year") != null) {
			search.setYear(ServletRequestUtils.getIntParameter(request, "year", currentYear));			
		}
		
		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				search.setPageSize(10);
			} else {
				search.setPageSize(size);				
			}
		}
		
		// sku
		search.setSku(ServletRequestUtils.getStringParameter(request, "sku", ""));
		
		return search;
	}	
	
}
