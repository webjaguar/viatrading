/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.26.2007
 */

package com.webjaguar.web.admin.customer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.Order;
import com.webjaguar.model.Payment;
import com.webjaguar.model.PaymentSearch;

public class PaymentListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
    	
		PaymentSearch search = getPaymentSearch( request );
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			if (search.getUserId() != null) {
				return new ModelAndView( new RedirectView( "payment.jhtm" ), "cid", search.getUserId());
			} else {
				return new ModelAndView( new RedirectView( "customerList.jhtm" ) );				
			}
		}
		
		//cancel Payments:
		if(request.getParameter("__cancel") != null ){
			String paymentIds[] = request.getParameterValues("__selected_paymentId");
			if (paymentIds != null && paymentIds.length > 0) {
				for (int i=0; i<paymentIds.length; i++) {					
					Payment payment = this.webJaguar.getCustomerPaymentById(Integer.parseInt(paymentIds[i]));
					
					//Cancel the payment
					payment.setCancelPayment(true);
					payment.setCancelledAmt(payment.getAmount());
					payment.setCancelledBy(this.webJaguar.getAccessUser(request).getUsername());
					
					search.setId(Integer.parseInt(paymentIds[i]));					
					Map<Integer, Order> invoices =  this.webJaguar.getInvoices( search, "paid" );
					
					// Cancel the invoices which are allocated to this payment.
					for(Order invoice: invoices.values()) {
						invoice.setCancelledPayment(true);
						invoice.setPaymentCancelledAmt(this.webJaguar.getPaymentAmount(Integer.parseInt(paymentIds[i]), invoice.getOrderId()));
						invoice.setPaymentCancelledBy(this.webJaguar.getAccessUser(request).getUsername());
						invoice.setPayment(0.00);	
					}
					
					this.webJaguar.updateCustomerPayment(payment, invoices.values());
				}
			}
		}
		    	
		

		if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
			search.setSalesRepId(this.webJaguar.getAccessUser(request).getSalesRepId());
		}
		
		Map<String, Object> model = new HashMap<String, Object>();
		List<Payment> customerPaymentList = this.webJaguar.getCustomerPaymentList(search);
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		AccessUser accessUser = this.webJaguar.getAccessUser(request);
		if(siteConfig.get("SALESREP_INCOMING_PAYMENT_FILTER").getValue().equals("true") && accessUser.getId() != 0 && accessUser.getSalesRepId() != null){		
			Iterator<Payment> itor = customerPaymentList.iterator();
			int salesrepID = accessUser.getSalesRepId();
			while(itor.hasNext()){
				Payment payment = itor.next();
				if(payment.getSalesRepId() == null || payment.getSalesRepId() != salesrepID){
					itor.remove();
				}
			}
		}
		PagedListHolder paymentList = new PagedListHolder(customerPaymentList);		
		
		paymentList.setPageSize(search.getPageSize());
		paymentList.setPage(search.getPage()-1);
		
		model.put("payments", paymentList);
		//for getting only active Customer groups
		CustomerGroupSearch customerGroupSearch = new CustomerGroupSearch();
		customerGroupSearch.setActive("1");
		model.put( "groups", this.webJaguar.getCustomerGroupList(customerGroupSearch)) ;
		model.put( "search", search );
		//Responsive Admin
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/customers/payments", "model", model);
		}
        return new ModelAndView("admin/customers/payments", "model", model);
	}

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private PaymentSearch getPaymentSearch(HttpServletRequest request) {
		PaymentSearch search = (PaymentSearch) request.getSession().getAttribute( "paymentSearch" );
		if (search == null) {
			search = new PaymentSearch();
			request.getSession().setAttribute( "paymentSearch", search );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}		
		
		// user id
		if (request.getParameter("cid") != null) {
			int cid = ServletRequestUtils.getIntParameter( request, "cid", 0 );
			if (cid <= 0) {
				search.setUserId( null );
			} else {
				search.setUserId( cid );				
			}
		}		
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}			
		
		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		// email
		if (request.getParameter("email") != null) {
			search.setEmail(ServletRequestUtils.getStringParameter(request, "email", "").trim());
		}
		
		return search;
	}	
	
}
