/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.customer;

import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CustomerExtContact;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.web.form.CustomerExtContactForm;

public class CustomerExtContactFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar; 
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public CustomerExtContactFormController() {
		setSessionForm(true);
		setCommandName("customerExtContactForm");
		setCommandClass(CustomerExtContactForm.class);
		setFormView("admin/customers/customerExtContactForm");
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
     	CustomerExtContactForm customerExtContactForm = (CustomerExtContactForm) command;
     	Integer userId = new Integer(request.getParameter("uid"));
		customerExtContactForm.getCustomerExtContact().setUserId(userId);
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteCustomerExtContact(customerExtContactForm.getCustomerExtContact());
			return new ModelAndView(new RedirectView("customer.jhtm"), "id", userId);
		}     	
     	
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView("customer.jhtm"), "id", userId);
		}    	
     	
		if (customerExtContactForm.isNewCustomerExtContact()) {
			this.webJaguar.insertCustomerExtContact(customerExtContactForm.getCustomerExtContact());
		} else {
			this.webJaguar.updateCustomerExtContact(customerExtContactForm.getCustomerExtContact());
		}
					
		//return new ModelAndView(new RedirectView("addressList.jhtm?id=" + userId));  
		return new ModelAndView(new RedirectView("customer.jhtm"), "id", userId);

    }

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}    
			
    protected Map referenceData(HttpServletRequest request) 
			throws Exception {
    	//Integer userId = new Integer(request.getParameter("uid"));
		//Customer customer = this.webJaguar.getCustomerById(userId);   
		
       	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();
	    // get a list of enabled country code from database
	  //  myModel.put("countries",this.webJaguar.getEnabledCountryList());
     //   myModel.put("states",this.webJaguar.getStateList("US"));
	//	myModel.put("caProvinceList", this.webJaguar.getStateList("CA"));  
	//	myModel.put("AUstatelist", this.webJaguar.getStateList("AU"));
		//myModel.put("firstName",customer.getCustomerExtContact().getFirstName());
	//	myModel.put("lastName",customer.getCustomerExtContact().getLastName());
	    map.put("model", myModel);  
	    return map;
    }  
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		CustomerExtContactForm customerExtContactForm = new CustomerExtContactForm();
		
		if ( request.getParameter("id") != null ) {
			Integer customerExtContactId = new Integer(request.getParameter("id"));
			CustomerExtContact customerExtContact = this.webJaguar.getCustomerExtContactById(customerExtContactId);
			customerExtContactForm = new CustomerExtContactForm(customerExtContact);
		}
		else if ( request.getParameter("uid") != null ) {
			Integer userid = new Integer(request.getParameter("uid"));
			customerExtContactForm = new CustomerExtContactForm();
		}

		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			setFormView("admin-responsive/customers/customerExtContactForm");
		} else{
			setFormView("admin/customers/customerExtContactForm");
		}
		return customerExtContactForm;
	}
}
