/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.23.2007
 */

package com.webjaguar.web.admin.customer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvData;
import com.webjaguar.model.CsvIndex;
import com.webjaguar.model.CsvMap;
import com.webjaguar.model.Customer;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.web.domain.Constants;

public class ImportController extends WebApplicationObjectSupport implements Controller {
	
	int gPRICE_TABLE = 0;
	int gPROTECTED = 0;	
	boolean gTAX_EXEMPTION, gSALES_REP, gMASS_EMAIL,gSUB_ACCOUNTS,gBUDGET,gSHOPPING_CART,gPAYMENTS;
	int gCUSTOMER_FIELDS = 0;
	Object gCUSTOMER_CATEGORY;
	String gI18N;
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	private HSSFFormulaEvaluator evaluator;
	
	String formView = "admin/customers/import";
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {	
        
		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			return new ModelAndView("admin/customers/import", "message", "excelfile.notWritable");	
		}    	
    	
		// check if upload button was pressed
		if (request.getParameter("__upload") != null) {
	    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	    	MultipartFile file = multipartRequest.getFile("file");
	    	if (file != null && !file.isEmpty()) {
	    		File excel_file = new File(baseFile, "customers_import.xls");
	    		file.transferTo(excel_file);
	    		file = null; // delete to save resources
	    		
	    		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
	    		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
	    		List<Customer> importedCustomers = new ArrayList<Customer>();
	        	try {
	                List<Map<String, Object>> addedItems = new ArrayList<Map<String, Object>>();
	                List<Map<String, Object>> addedCrmItems = new ArrayList<Map<String, Object>>();
	                List<Map<String, Object>> updatedItems = new ArrayList<Map<String, Object>>();
	                List<Map<String, Object>> updatedCrmItems = new ArrayList<Map<String, Object>>();
	                List<Map<String, Object>> notAddedCrmItems = new ArrayList<Map<String, Object>>();
	                List<Map<String, Object>> notAddedCustomerItems = new ArrayList<Map<String, Object>>();
	                Map<String, Object> map = new HashMap<String, Object>();
	                
	                if (processFile(excel_file, gSiteConfig, map, importedCustomers, addedItems, updatedItems)){
	                
	        		//if (processFileWithBatchQueries(excel_file, gSiteConfig, map, importedCustomers, addedItems, updatedItems, addedCrmItems, updatedCrmItems, notAddedCrmItems, notAddedCustomerItems)) {
	    				// TODO: catch when salesrep Id does not exist 
	    				this.webJaguar.insertImportExportHistory(new ImportExportHistory("customer", SecurityContextHolder.getContext().getAuthentication().getName(), true));

	        			if (updatedItems.size() > 0) map.put("updatedItems", updatedItems);
	        			if (addedItems.size() > 0) map.put("addedItems", addedItems);
	        			if (addedCrmItems.size() > 0) map.put("addedCrmItems", addedCrmItems);
	        			if (updatedCrmItems.size() > 0) map.put("updatedCrmItems", updatedCrmItems);
	        			if (notAddedCrmItems.size() > 0) map.put("notAddedCrmItems", notAddedCrmItems);
	        			if (notAddedCustomerItems.size() > 0) map.put("notAddedCustomerItems", notAddedCustomerItems);
	        			
	        			return new ModelAndView("admin/customers/importSuccess", map);
	        		} else {
	        			return new ModelAndView(formView, map);
	        		}
	        	} catch (IOException e) {
	    			return new ModelAndView(formView, "message", "excelfile.invalid");
	    		} finally {
	    	    	// delete excel file
	    			excel_file.delete();	    			
	    		} 
	    	} else {
	        	return new ModelAndView(formView, "message", "excelfile.invalid");	    		
	    	}
		}
    	
    	return new ModelAndView(formView);
    }
    

    private boolean processFileWithBatchQueries(File excel_file, Map<String, Object> gSiteConfig, Map<String, Object> map, List<Customer> importedCustomers, List<Map<String, Object>> addedItems, List<Map<String, Object>> updatedItems, List<Map<String, Object>> addedCrmItems, List<Map<String, Object>> updatedCrmItems,List<Map<String, Object>> notAddedCrmItems ,  List<Map<String, Object>> notAddedCustomerItems) throws IOException, Exception {
    
    	POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(excel_file));
    	HSSFWorkbook wb = new HSSFWorkbook(fs); 
		HSSFSheet sheet = wb.getSheetAt(0);       	// first sheet
		HSSFRow row     = sheet.getRow(0);        	// first row
		
		evaluator = new HSSFFormulaEvaluator(sheet, wb);
		
		Map<String, Short> header = new HashMap<String, Short>();
		Set<String> emails = new HashSet<String>();
		Set<Short> category = new HashSet<Short>();

		if (wb.getNumberOfSheets() > 1) {
			// multiple sheets
			map.put("message", "excelfile.multipleSheets");
			return false;					
		}
		if (row == null) {
			// first line should be the headers
			map.put("message", "excelfile.emptyHeaders");
			return false;				
		}
		
	    Iterator iter = row.cellIterator();
	   	
		Map<String, CsvData> customerFeedMap = this.webJaguar.getCsvFeedMap("customer");
		Map<String, CsvData> addressFeedMap = this.webJaguar.getCsvFeedMap("address");
		Map<String, CsvData> crmFeedMap = this.webJaguar.getCsvFeedMap("crm");
		
		List<CsvIndex> customerCsvIndex = new ArrayList<CsvIndex>();
		List<CsvIndex> addressCsvIndex = new ArrayList<CsvIndex>();
		List<CsvIndex> crmCsvIndex = new ArrayList<CsvIndex>();	
		
		Map<String,String> dublicateUsers = new HashMap<String,String>();
		
	    // get header names
		while (iter.hasNext()) {
			HSSFCell cell = (HSSFCell) iter.next();
			String headerName = "";
			try {
				headerName = cell.getStringCellValue().trim();					
			} catch (NumberFormatException e) {
				// do nothing
			}
			if (!headerName.equals("")) {
				if (customerFeedMap.get(headerName.toLowerCase()) != null) {
					customerCsvIndex.add(new CsvIndex(customerFeedMap.get(headerName.toLowerCase()).getHeaderName(), customerFeedMap.get(headerName.toLowerCase()).getColumnName(), customerFeedMap.get(headerName.toLowerCase()).getType(),(int)cell.getCellNum()));			
				}
				if(addressFeedMap.get(headerName.toLowerCase()) != null) {
					addressCsvIndex.add(new CsvIndex(addressFeedMap.get(headerName.toLowerCase()).getHeaderName(), addressFeedMap.get(headerName.toLowerCase()).getColumnName(), addressFeedMap.get(headerName.toLowerCase()).getType(),(int)cell.getCellNum()));			
				}
				if(crmFeedMap.get(headerName.toLowerCase()) != null) {
					crmCsvIndex.add(new CsvIndex(crmFeedMap.get(headerName.toLowerCase()).getHeaderName(), crmFeedMap.get(headerName.toLowerCase()).getColumnName(), crmFeedMap.get(headerName.toLowerCase()).getType(),(int)cell.getCellNum()));			
				}
			} 
		}
		
		// check required headers
		if (customerCsvIndex.get(0)==null || customerCsvIndex.get(0).getHeaderName()==null || customerCsvIndex.get(0).getHeaderName().length()<=0) { // email address column is required
			map.put("message", "excelfile.missingEmailHeader");
			return false;
		}
		
		Integer index = sheet.getLastRowNum() / 20000;
		Integer rem = sheet.getLastRowNum() % 20000;
		
		int count = 0;
		for (count=0; count<index; count++) {
			readCustomerExcelFiles(count*20000+1, (count+1)*20000, customerCsvIndex, addressCsvIndex, crmCsvIndex, row, sheet, dublicateUsers, addedItems, updatedItems, addedCrmItems, updatedCrmItems, notAddedCrmItems ,  notAddedCustomerItems);
		}
		readCustomerExcelFiles(count*20000+1, (count)*20000+rem, customerCsvIndex, addressCsvIndex, crmCsvIndex, row, sheet, dublicateUsers, addedItems, updatedItems, addedCrmItems, updatedCrmItems, notAddedCrmItems ,  notAddedCustomerItems);
		
    	return true;
    }
    
    private void readCustomerExcelFiles(Integer startIndex, Integer endIndex, List<CsvIndex> customerCsvIndex, List<CsvIndex> addressCsvIndex ,List<CsvIndex> crmCsvIndex, HSSFRow row, HSSFSheet sheet, Map<String,String> dublicateUsers, List<Map<String, Object>> addedItems, List<Map<String, Object>> updatedItems, List<Map<String, Object>> addedCrmItems, List<Map<String, Object>> updatedCrmItems, List<Map<String, Object>> notAddedCrmItems ,  List<Map<String, Object>> notAddedCustomerItems){
    	
    	List<CsvData> customerCsvData = new ArrayList<CsvData>();
		List<CsvData> addressCsvData = new ArrayList<CsvData>();
		List<CsvData> crmCsvData = new ArrayList<CsvData>();
			
		List<String> allEmailAddressList = new ArrayList<String>();
		Map<String,CsvMap> availableEmailMap = null;
			
		Map<String,List<CsvData>> customerCsvDataList = new HashMap<String,List<CsvData>>();
		Map<String,List<CsvData>> addressCsvDataList = new HashMap<String,List<CsvData>>();
		Map<String,List<CsvData>> crmCsvDataList = new HashMap<String,List<CsvData>>();
		
		for (int i=startIndex; i<=endIndex; i++) {
			
			row = sheet.getRow(i);
			if (row == null) {
				continue;
			}
			
			customerCsvData = new ArrayList<CsvData>();
			addressCsvData = new ArrayList<CsvData>();
			crmCsvData = new ArrayList<CsvData>();
			
			for(CsvIndex csv : customerCsvIndex){
				if(csv.getType().equalsIgnoreCase("integer")){
					customerCsvData.add(new CsvData(csv.getHeaderName(),csv.getColumnName(), csv.getType(), csv.getIndex(), row.getCell(csv.getIndex())!=null?String.valueOf(row.getCell(csv.getIndex()).getNumericCellValue()):null));
				}else if(csv.getType().equalsIgnoreCase("double")){
					customerCsvData.add(new CsvData(csv.getHeaderName(),csv.getColumnName(), csv.getType(), csv.getIndex(), row.getCell(csv.getIndex())!=null?String.valueOf(row.getCell(csv.getIndex()).getNumericCellValue()):null));
				}else{
					customerCsvData.add(new CsvData(csv.getHeaderName(),csv.getColumnName(), csv.getType(), csv.getIndex(), row.getCell(csv.getIndex())!=null?removeFirst(row.getCell(csv.getIndex()).getStringCellValue().trim()):null));
				}
			}
			
			for(CsvIndex csv : addressCsvIndex){
				if(csv.getType().equalsIgnoreCase("integer")){
					addressCsvData.add(new CsvData(csv.getHeaderName(),csv.getColumnName(), csv.getType(), csv.getIndex(), row.getCell(csv.getIndex())!=null?String.valueOf(row.getCell(csv.getIndex()).getNumericCellValue()):null));
				}else if(csv.getType().equalsIgnoreCase("double")){
					addressCsvData.add(new CsvData(csv.getHeaderName(),csv.getColumnName(), csv.getType(), csv.getIndex(), row.getCell(csv.getIndex())!=null?String.valueOf(row.getCell(csv.getIndex()).getNumericCellValue()):null));
				}else{
					addressCsvData.add(new CsvData(csv.getHeaderName(),csv.getColumnName(), csv.getType(), csv.getIndex(), row.getCell(csv.getIndex())!=null?removeFirst(row.getCell(csv.getIndex()).getStringCellValue().trim()):null));
				}	
			}
			
			for(CsvIndex csv : crmCsvIndex){
				if(csv.getType().equalsIgnoreCase("integer")){
					crmCsvData.add(new CsvData(csv.getHeaderName(),csv.getColumnName(), csv.getType(), csv.getIndex(), row.getCell(csv.getIndex())!=null?String.valueOf(row.getCell(csv.getIndex()).getNumericCellValue()):null));
				}else if(csv.getType().equalsIgnoreCase("double")){
					crmCsvData.add(new CsvData(csv.getHeaderName(),csv.getColumnName(), csv.getType(), csv.getIndex(), row.getCell(csv.getIndex())!=null?String.valueOf(row.getCell(csv.getIndex()).getNumericCellValue()):null));
				}else{
					crmCsvData.add(new CsvData(csv.getHeaderName(),csv.getColumnName(), csv.getType(), csv.getIndex(), row.getCell(csv.getIndex())!=null?removeFirst(row.getCell(csv.getIndex()).getStringCellValue().trim()):null));
				}	
			}
			
			customerCsvDataList.put(removeFirst(row.getCell(customerCsvIndex.get(0).getIndex()).getStringCellValue().trim()), customerCsvData);
			addressCsvDataList.put(removeFirst(row.getCell(customerCsvIndex.get(0).getIndex()).getStringCellValue().trim()), addressCsvData);
			crmCsvDataList.put(removeFirst(row.getCell(customerCsvIndex.get(0).getIndex()).getStringCellValue().trim()), crmCsvData);
			
			if(dublicateUsers.get(removeFirst(row.getCell(customerCsvIndex.get(0).getIndex()).getStringCellValue().trim()))==null){
				allEmailAddressList.add(removeFirst(row.getCell(customerCsvIndex.get(0).getIndex()).getStringCellValue().trim()));
				dublicateUsers.put(removeFirst(row.getCell(customerCsvIndex.get(0).getIndex()).getStringCellValue().trim()),row.getCell(customerCsvIndex.get(0).getIndex()).getStringCellValue().trim());
			}
			
			}
			
			availableEmailMap = this.webJaguar.getAvailableUserList(allEmailAddressList);
			
			this.webJaguar.insertAndUpdateCustomerList(allEmailAddressList, availableEmailMap, customerCsvDataList, addressCsvDataList, crmCsvDataList, customerCsvIndex,addressCsvIndex, crmCsvIndex, addedItems, updatedItems, addedCrmItems, updatedCrmItems, notAddedCrmItems ,  notAddedCustomerItems);	
    }

	private boolean processFile(File excel_file, Map<String, Object> gSiteConfig, 
			Map<String, Object> map, List<Customer> importedCustomers, List<Map<String, Object>> addedItems, List<Map<String, Object>> updatedItems) throws IOException, Exception {
    	
    	List<Map<String, Object>> invalidItems = new ArrayList<Map<String, Object>>();
        
    	gPRICE_TABLE = (Integer) gSiteConfig.get("gPRICE_TABLE");
    	gPROTECTED = (Integer) gSiteConfig.get("gPROTECTED");
    	gTAX_EXEMPTION = (Boolean) gSiteConfig.get("gTAX_EXEMPTION");
    	gCUSTOMER_FIELDS = (Integer) gSiteConfig.get("gCUSTOMER_FIELDS");
    	gSALES_REP = (Boolean) gSiteConfig.get("gSALES_REP");
    	gMASS_EMAIL = (Boolean) gSiteConfig.get("gMASS_EMAIL");
    	gCUSTOMER_CATEGORY = gSiteConfig.get("gCUSTOMER_CATEGORY");
		gSUB_ACCOUNTS = (Boolean) gSiteConfig.get("gSUB_ACCOUNTS");
		gBUDGET = (Boolean) gSiteConfig.get("gBUDGET");
		gSHOPPING_CART = (Boolean)  gSiteConfig.get("gSHOPPING_CART");
    	gI18N = (String) gSiteConfig.get("gI18N");
    	gPAYMENTS = (Boolean)  gSiteConfig.get("gPAYMENTS");		
		POIFSFileSystem fs =
            new POIFSFileSystem(new FileInputStream(excel_file));
		HSSFWorkbook wb = new HSSFWorkbook(fs);
	    
		HSSFSheet sheet = wb.getSheetAt(0);       	// first sheet
		HSSFRow row     = sheet.getRow(0);        	// first row
		
		evaluator = new HSSFFormulaEvaluator(sheet, wb);
		
		Map<String, Short> header = new HashMap<String, Short>();
		Set<String> emails = new HashSet<String>();
		Set<Short> category = new HashSet<Short>();
		List<Integer> inactiveSalesRepList = this.webJaguar.getInactiveSalesRepIdList();
		Map<Integer,Boolean> inactiveSalesRep = new HashMap<Integer,Boolean>();
		
		for(Integer id : inactiveSalesRepList){
			inactiveSalesRep.put(id, true);
		}
		
		if (wb.getNumberOfSheets() > 1) {
			// multiple sheets
			map.put("message", "excelfile.multipleSheets");
			return false;					
		}
		if (row == null) {
			// first line should be the headers
			map.put("message", "excelfile.emptyHeaders");
			return false;				
		}
		
	    Iterator iter = row.cellIterator();
	    
	    // get header names
		while (iter.hasNext()) {
			HSSFCell cell = (HSSFCell) iter.next();
			String headerName = "";
			try {
				headerName = cell.getStringCellValue().trim();					
			} catch (NumberFormatException e) {
				// do nothing
			}
			if (!headerName.equals( "" )) {
				if (headerName.equalsIgnoreCase("category") && gCUSTOMER_CATEGORY != null) {
					category.add(cell.getCellNum());
				} else {
					if (header.put(headerName.toLowerCase(), cell.getCellNum()) != null) {
						// duplicate header
						map.put("message", "excelfile.duplicateHeaders");
						map.put("arguments", headerName);
						return false;								
					}										
				}
			} 
		}
		
		// check required headers
		if (!header.containsKey("emailaddress")) { // email address column is required
			map.put("message", "excelfile.missingEmailHeader");
			return false;
		}
		
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Integer duration = null;
		Integer amount = null;
		if(siteConfig!=null && siteConfig.get("IMPORT_WAIT_DURATION")!=null && siteConfig.get("IMPORT_WAIT_DURATION").getValue()!=null){
			try{
				duration = Integer.valueOf(siteConfig.get("IMPORT_WAIT_DURATION").getValue());
			}catch(Exception ex){
				duration = 0;
			}
		}else{
			duration = 0;
		}
		if(siteConfig!=null && siteConfig.get("IMPORT_ROW_AMOUNT")!=null && siteConfig.get("IMPORT_ROW_AMOUNT").getValue()!=null){
			try{
				amount = Integer.valueOf(siteConfig.get("IMPORT_ROW_AMOUNT").getValue());
			}catch(Exception ex){
				amount = 0;
			}
		}else{
			amount = 0;
		}
		
		Integer rowSize =  sheet.getLastRowNum();
		Integer rowAmt =   rowSize/amount;
		Integer rowRest =  rowSize-(rowSize/amount)*amount;
		int ind = 0;
		
		for(ind=0;ind<rowAmt;ind++){
			importedCustomers = new ArrayList<Customer>();
			nextRow: for (int i=ind*amount+1; i<=(ind*amount+amount); i++) {
				invalidItems = new ArrayList<Map<String, Object>>();
				row = sheet.getRow(i);
				if (row == null) {
					continue;
				}
				
				// check if new or existing customer
				String emailAddress = null;
				emailAddress = getStringValue(row, header.get("emailaddress")).trim();
				if (emailAddress.equals("")) emailAddress = null;
				Map<String, Object> thisMap = new HashMap<String, Object>();
				thisMap.put("emailAddress", emailAddress);
				
				// check for valid and duplicate emails
				if (emailAddress != null) {	
					if (!Constants.validateEmail(emailAddress, null)) {
						// email is invalid
						thisMap.put("rowNum", row.getRowNum()+1);
						thisMap.put("emailAddress", emailAddress);
						thisMap.put("reason", "Invalid Email Address.");
						invalidItems.add(thisMap);
						continue nextRow;						
					} else {
						if (!emails.add(emailAddress.toLowerCase())) {
							// email is a duplicate
							thisMap.put("rowNum", row.getRowNum()+1);
							thisMap.put("emailAddress", emailAddress);
							thisMap.put("reason", "Duplicate Email Address.");
							invalidItems.add(thisMap);
							continue nextRow;
						}
					}
				} else {
					continue nextRow;
				}
				
				// check for parent id 
				if ((Boolean) gSiteConfig.get("gSUB_ACCOUNTS") && header.containsKey("parent")) {
					Integer parentId = null;
					try {
						parentId = getIntegerValue(row, header.get("parent"));
						Customer customer = new Customer();				
							if (parentId != null && !parentId.equals("")) {
								customer = this.webJaguar.getCustomerById(parentId);
								if(customer == null) {
									// email is invalid
									thisMap.put("rowNum", row.getRowNum()+1);
									thisMap.put("parent", parentId);
									thisMap.put("reason", "Invalid Parent.");
									invalidItems.add(thisMap);
									continue nextRow;
								} 
							} 
						}
					catch(Exception e){					
					}
				}
				
				// check salesRep is inactive or not
				// Sales Rep   (-1 to unassign salesRep)
				if ( gSALES_REP ) {
					Integer salesRepId = null;
					try {
						salesRepId = getIntegerValue(row, header.get("salesrepid"));
						if (salesRepId != null && salesRepId != 0 && salesRepId != -1){
							if(inactiveSalesRep.containsKey(salesRepId)){
								thisMap.put("rowNum", row.getRowNum()+1);
								thisMap.put("salesRepId", salesRepId);
								thisMap.put("reason", "Customer cannot be assigned to Invalid SalesRep.");
								invalidItems.add(thisMap);
								continue nextRow;
							}
						} 
					} catch (Exception e) {
						// do nothing
					}		
				}
				
				if (invalidItems.size() == 0) { // no errors
					// save or update customer
					Customer customer = null;
					customer = this.webJaguar.getCustomerByUsername( emailAddress );
					if (customer != null) { 		// existing customer
						processRow(row, header, customer, category, thisMap);						
						updatedItems.add(thisMap);						
					} else {						// new customer
						customer = new Customer();
						customer.setAddress( new Address() );
						customer.setUsername( emailAddress );
						processRow(row, header, customer, category, thisMap);					
						addedItems.add(thisMap);
					}
					importedCustomers.add(customer);
				} // if no errors
			}
			this.webJaguar.importCustomers(importedCustomers, (Boolean) gSiteConfig.get("gCRM"));
			importWait(duration);
		}
		importedCustomers = new ArrayList<Customer>();
		nextRow2: for (int i=ind*amount+1; i<=(ind*amount+rowRest); i++) {
			row = sheet.getRow(i);
			if (row == null) {
				continue;
			}
			
			// check if new or existing customer
			String emailAddress = null;
			emailAddress = getStringValue(row, header.get("emailaddress")).trim();
			if (emailAddress.equals("")) emailAddress = null;
			Map<String, Object> thisMap = new HashMap<String, Object>();
			thisMap.put("emailAddress", emailAddress);
			
			// check for valid and duplicate emails
			if (emailAddress != null) {	
				if (!Constants.validateEmail(emailAddress, null)) {
					// email is invalid
					thisMap.put("rowNum", row.getRowNum()+1);
					thisMap.put("emailAddress", emailAddress);
					thisMap.put("reason", "Invalid Email Address.");
					invalidItems.add(thisMap);
					continue nextRow2;						
				} else {
					if (!emails.add(emailAddress.toLowerCase())) {
						// email is a duplicate
						thisMap.put("rowNum", row.getRowNum()+1);
						thisMap.put("emailAddress", emailAddress);
						thisMap.put("reason", "Duplicate Email Address.");
						invalidItems.add(thisMap);
						continue nextRow2;
					}
				}
			} else {
				continue nextRow2;
			}
			
			// check for parent id 
			if ((Boolean) gSiteConfig.get("gSUB_ACCOUNTS") && header.containsKey("parent")) {
				Integer parentId = null;
				try {
					parentId = getIntegerValue(row, header.get("parent"));
					Customer customer = new Customer();				
						if (parentId != null && !parentId.equals("")) {
							customer = this.webJaguar.getCustomerById(parentId);
							if(customer == null) {
								// email is invalid
								thisMap.put("rowNum", row.getRowNum()+1);
								thisMap.put("parent", parentId);
								thisMap.put("reason", "Invalid Parent.");
								invalidItems.add(thisMap);
								continue nextRow2;
							} 
						} 
					}
				catch(Exception e){					
				}
			}
			
			// check salesRep is inactive or not
			// Sales Rep   (-1 to unassign salesRep)
			if ( gSALES_REP ) {
				Integer salesRepId = null;
				try {
					salesRepId = getIntegerValue(row, header.get("salesrepid"));
					if (salesRepId != null && salesRepId != 0 && salesRepId != -1){
						if(inactiveSalesRep.containsKey(salesRepId)){
							thisMap.put("rowNum", row.getRowNum()+1);
							thisMap.put("salesRepId", salesRepId);
							thisMap.put("reason", "Customer cannot be assigned to Invalid SalesRep.");
							invalidItems.add(thisMap);
							continue nextRow2;
						}
					} 
				} catch (Exception e) {
					// do nothing
				}		
			}
			
			if (invalidItems.size() == 0) { // no errors
				// save or update customer
				Customer customer = null;
				customer = this.webJaguar.getCustomerByUsername( emailAddress );
				if (customer != null) { 		// existing customer
					processRow(row, header, customer, category, thisMap);						
					updatedItems.add(thisMap);						
				} else {						// new customer
					customer = new Customer();
					customer.setAddress( new Address() );
					customer.setUsername( emailAddress );
					processRow(row, header, customer, category, thisMap);					
					addedItems.add(thisMap);
				}
				importedCustomers.add(customer);
			} // if no errors
		}	
		this.webJaguar.importCustomers(importedCustomers, (Boolean) gSiteConfig.get("gCRM"));
		importWait(duration);
		
		if (invalidItems.size() > 0) { // has errors
			map.put("message", "importFailedTitle");
			map.put("invalidItems", invalidItems);
			return false;				
		} else if (importedCustomers.size() == 0) {	// no Customers to import
			map.put("message", "excelfile.empty");
			return false;			
		}
		
		return true;
    }
	public void importWait(Integer duration){
		long start_time = System.currentTimeMillis();
		for(int i=0;i<duration*1000;i++){
			for(int j=0;j<1000;j++){}
		}
		long end_time = System.currentTimeMillis();
		long difference = end_time-start_time;
		
	}

    private Integer getIntegerValue(HSSFRow row, short cellnum) throws Exception {
		HSSFCell cell = row.getCell(cellnum);
    	Integer value = null;
    	
    	if (cell != null) {
    		switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = new Integer(cell.getStringCellValue());										
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Double cellValue = cell.getNumericCellValue();
					if (cellValue != cellValue.intValue()) {
						throw new Exception();	
					}
					value =  cellValue.intValue();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue formulaCellValue = evaluator.evaluate(cell);					
					if (formulaCellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						cellValue =  new Double(formulaCellValue.getNumberValue());
						if (cellValue != cellValue.intValue()) {
							throw new Exception();	
						}
						value =  cellValue.intValue();
					}
					break;
    		} // switch
    	}
    	return value;
    }
    
    private String getStringValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		String value = "";

    	if (cell != null) {
			switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = cell.getStringCellValue();
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Integer intValue = null;
					try {
						intValue = getIntegerValue(row, cellnum);
					} catch (Exception e) {}
					if (intValue != null) {
						value = intValue.toString();
						break;
					}
					value = new Double(cell.getNumericCellValue()).toString();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue cellValue = evaluator.evaluate(cell);
					if (cellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						value = new Double(cellValue.getNumberValue()).toString();						
					} else if (cellValue.getCellType() == HSSFCell.CELL_TYPE_STRING) {
						value = cellValue.getStringValue();
					}
					break;
			
			} // switch		
    	}
		return value;
    }
    
    private boolean getBooleanValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		boolean value = false;

    	if (cell != null) {
    		if (getStringValue(row, cellnum).trim().equals( "1" ))  {
    			value = true;
    		}
    	}
		return value;
    } 
    
    private void processRow(HSSFRow row, Map<String, Short> header, Customer customer, Set<Short> category, Map<String, Object> thisMap) {
    	Class<Customer> c = Customer.class;
    	Class paramTypeString[] = {String.class};
    	
    	StringBuffer comments = new StringBuffer();
    	NumberFormat formatter = new DecimalFormat("#");
    	
    	if (header.containsKey("suspended")) {
        	customer.setSuspended( getBooleanValue(row, header.get("suspended")) );	    		
    	}
    	if ( gMASS_EMAIL && header.containsKey("unsubscribe") ) {
    		customer.setUnsubscribe( getBooleanValue(row, header.get("unsubscribe")) );	
    	}
		// get first name
		if (header.containsKey("firstname")) {
			customer.getAddress().setFirstName( getStringValue(row, header.get("firstname")) );
		}		

		// get last name
		if (header.containsKey("lastname")) {
			customer.getAddress().setLastName( getStringValue(row, header.get("lastname")) );
		}   
		
		// get ratings
		if (header.containsKey("rating1")) {
			customer.setRating1( getStringValue(row, header.get("rating1")) );
		}
		if (header.containsKey("rating2")) {
			customer.setRating2( getStringValue(row, header.get("rating2")) );
		}
		
		// budget
		if(gBUDGET && header.containsKey("creditallowed")) {
			try {
				customer.setCreditAllowed(Double.parseDouble(getStringValue(row, header.get("creditallowed"))));
			} catch (Exception e){
				customer.setCreditAllowed(-1.0);
			}
		}
		// admin alert
		if(gPAYMENTS && header.containsKey("paymentalert")) {
			try {
				customer.setPaymentAlert(Double.parseDouble(getStringValue(row, header.get("paymentalert"))));
			} catch (Exception e) {
				customer.setPaymentAlert(0.00);
			}
		}
		// get password
		if (header.containsKey("password")) {
			String password = getStringValue(row, header.get("password"));
			if (password.trim().equals( "" )) { // missing password
				if (customer.getId() == null) {	// new customer 
					customer.setPassword( customer.getUsername().toLowerCase() );
					comments.append("password set to \"" + customer.getPassword() + "\", ");					
				} else { // old customer
					customer.setPassword( null );	// retain old password
					comments.append("old password retained, ");					
				}
			} else {
				customer.setPassword( getStringValue(row, header.get("password")) );
				comments.append("password set to \"" + customer.getPassword() + "\", ");
			}
		} else {
			if (customer.getId() == null) {	// new customer
				customer.setPassword( customer.getUsername().toLowerCase() );
				comments.append("password set to \"" + customer.getPassword() + "\", ");	
			}
		}
		
		// get company
		if (header.containsKey("company")) {
			customer.getAddress().setCompany( getStringValue(row, header.get("company")) );
		}  

		// get country
		if (header.containsKey("country")) {
			if(getStringValue(row, header.get("country")) == null || getStringValue(row, header.get("country")).equals("")) {
				// default Country is US
				customer.getAddress().setCountry("US");
			} else {
				customer.getAddress().setCountry( getStringValue(row, header.get("country")) );
			}
		} 
		
		// get address 1
		if (header.containsKey("address1")) {
			customer.getAddress().setAddr1( getStringValue(row, header.get("address1")) );
		}		
		
		// get address 2
		if (header.containsKey("address2")) {
			customer.getAddress().setAddr2( getStringValue(row, header.get("address2")) );
		}		
		
		// get city
		if (header.containsKey("city")) {
			customer.getAddress().setCity( getStringValue(row, header.get("city")) );
		}		
		
		// get state/province
		if (header.containsKey("stateprovince")) {
			customer.getAddress().setStateProvince( getStringValue(row, header.get("stateprovince")) );
		}			

		// get zip
		if (header.containsKey("zip")) {
			customer.getAddress().setZip( getStringValue(row, header.get("zip")) );
		}	
		
		// get phone
		if (header.containsKey("phone")) {
			try {
				if(row.getCell(header.get("phone")).getNumericCellValue() != 0){
					customer.getAddress().setPhone( formatter.format(row.getCell(header.get("phone")).getNumericCellValue()) );
				} else{
					customer.getAddress().setPhone( getStringValue(row, header.get("phone")) );
				}
			} catch (Exception e) {
				customer.getAddress().setPhone( getStringValue(row, header.get("phone")) );
			}
		}
		
		// get cell phone	
		if (header.containsKey("cellphone")) {
			try {
				if(row.getCell(header.get("cellphone")).getNumericCellValue() != 0){
					customer.getAddress().setCellPhone( formatter.format(row.getCell(header.get("cellphone")).getNumericCellValue()).toString() );
				} else{
					customer.getAddress().setCellPhone( getStringValue(row, header.get("cellphone")) );
				}
			} catch (Exception e) {
				customer.getAddress().setCellPhone( getStringValue(row, header.get("cellphone")) );
			}
		}		
		
		// get fax
		if (header.containsKey("fax")) {
			customer.getAddress().setFax( getStringValue(row, header.get("fax")) );
		}					
		
		// get parent 
		if (gSUB_ACCOUNTS && header.containsKey("parent")) {
			try {
				customer.setParent( getIntegerValue(row, header.get("parent")) );				
			} catch (Exception e) {
				customer.setParent(null);
			}
		}
		
		// get account number
		if (header.containsKey("accountnumber")) {
			customer.setAccountNumber( getStringValue(row, header.get("accountnumber")) );
		}		

		// get language
		if (header.containsKey("language") && !gI18N.isEmpty()) {
			if(getStringValue(row, header.get("language")).equals("en")) {
				customer.setLanguageCode(LanguageCode.en);
			} else if(getStringValue(row, header.get("language")).equals("es")) {
				customer.setLanguageCode(LanguageCode.es);
			} else if(getStringValue(row, header.get("language")).equals("fr")) {
				customer.setLanguageCode(LanguageCode.fr);
			}
		}		

		// see hidden price
		if (header.containsKey("seehiddenprice")) {
			customer.setSeeHiddenPrice( getBooleanValue(row, header.get("seehiddenprice")) );
		}		
		
		// seller's permit
		if (header.containsKey("sellerspermit") && gTAX_EXEMPTION) {
			customer.setTaxId( getStringValue(row, header.get("sellerspermit")) );			
		}
		// payment options
		if(gSHOPPING_CART && header.containsKey("paymentoptions")) {
			customer.setPayment(getStringValue(row, header.get("paymentoptions")));
		}
		// price table
		if (header.containsKey("pricetable") && gPRICE_TABLE > 0) {
			try {
				int priceTable = Integer.parseInt(getStringValue(row, header.get("pricetable")).trim());
				if (priceTable > gPRICE_TABLE || priceTable == 0) {
					customer.setPriceTable( 0 );
				} else {
					customer.setPriceTable( priceTable );
				}
			} catch (Exception e) {
				customer.setPriceTable( 0 );
			} 
		}	
		
		// protected access
		if (gPROTECTED > 0) {
			BitSet bitSet = customer.getProtectedAccessAsBitSet();
			for (int i=0; i<gPROTECTED; i++) {
				if (header.containsKey("protectedaccess" + (i+1))) {
					bitSet.set( i, getBooleanValue(row, header.get("protectedaccess" + (i+1))));
				}
			}	
			StringBuffer protectedAccess = new StringBuffer();
			for (int i=0; i<bitSet.length(); i++) {
				protectedAccess.insert(0, bitSet.get( i ) ? "1" : "0");
			}
			customer.setProtectedAccess( protectedAccess.toString() );
		}
		
		// login success url
		if (header.containsKey("loginsuccessurl")) {
			customer.setLoginSuccessURL( getStringValue(row, header.get("loginsuccessurl")) );			
		}
		
		// Sales Rep   (-1 to unassign salesRep)
		if ( gSALES_REP ) {
			Integer salesRepId = null;
			try {
				salesRepId = getIntegerValue(row, header.get("salesrepid"));
				if (salesRepId == -1) customer.setSalesRepId(null);
				else if (salesRepId != null && salesRepId != 0) customer.setSalesRepId(salesRepId);
			} catch (Exception e) {
				// do nothing
			}		
		}
		
		// get extra customer fields, uses java reflection
		for (int ef=1; ef<=gCUSTOMER_FIELDS; ef++) {
			if (header.containsKey("field_" + ef)) {
				try {
					Method m = c.getMethod("setField" + ef, paramTypeString);
					Object arglist[] = { getStringValue(row, header.get("field_" + ef)) };
					m.invoke(customer, arglist);
				} catch (Exception e) {
					// do nothing
				}
			}
		}
		
		// get category associations
		if (!category.isEmpty()) {
			Set<Object> categories = new HashSet<Object>();
			for (Short colnum:category) {
				try {
					Integer catId = getIntegerValue(row, colnum);
					if (catId != null) categories.add(catId);										
				} catch (Exception e) {
					// do nothing
				}
			} // for
			customer.setCatIds(categories);
		}
		
		// get mainsource
		if (header.containsKey("mainsource")) {
			customer.setMainSource(getStringValue(row, header.get("mainsource")));
		}
		
		// get paid
		if (header.containsKey("paid")) {
			customer.setPaid(getStringValue(row, header.get("paid")));
		}	
		
		// get medium
		if (header.containsKey("medium")) {
			customer.setMedium(getStringValue(row, header.get("medium")));
		}
		
		// get languagefield
		if (header.containsKey("languagefield")) {
			customer.setLanguage(getStringValue(row, header.get("languagefield")));
		}

		thisMap.put("comments", comments);
    }
      
    public void autoImport() {
    	Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// UPDATE site_config SET config_value = '__server_name__,__username__,__password__,__filename__,__email__'
		// WHERE config_key = 'AUTO_IMPORT_CUSTOMER';
		
		String[] config = siteConfig.get("AUTO_IMPORT_CUSTOMER").getValue().split(",");
		if (!(config.length > 3 && (Boolean) gSiteConfig.get("gSITE_ACTIVE") && (Boolean) gSiteConfig.get("gAUTO_IMPORT_CUSTOMER"))) {
			// return if feature is disabled 
			return;
		}
		
		String contactEmail = "";
		if (config.length > 4) {
			contactEmail = config[4];
		}
		
		Date start = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		
		File tempFolder = new File(getServletContext().getRealPath("temp"));	
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File excel_file = new File(tempFolder, "customers_import.xls");
		
		List<Map<String, Object>> addedItems = new ArrayList<Map<String, Object>>();
		try {
			 FTPClient ftp = new FTPClient();
			 ftp.connect(config[0]);
		        
			 // login
			 ftp.login(config[1], config[2]);
			 ftp.enterLocalPassiveMode();
			 ftp.setFileType(FTP.BINARY_FILE_TYPE);	// for excel files
			 
			 if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
				 if (ftp.retrieveFile(config[3], new FileOutputStream(excel_file))) {
					 try {
						 
						 Map<String, Object> map = new HashMap<String, Object>();
						 List<Customer> importedCustomers = new ArrayList<Customer>();
						 List<Map<String, Object>> updatedItems = new ArrayList<Map<String, Object>>();
						 
						 if (processFile(excel_file, gSiteConfig, map, importedCustomers, addedItems, updatedItems)) {
							 this.webJaguar.importCustomers(importedCustomers, (Boolean) gSiteConfig.get("gCRM"));
							 ftp.deleteFile(config[3]);								 
							 
							 Date end = new Date();
							 if (contactEmail.trim().length() > 0) {
								 StringBuffer sbuff = new StringBuffer();
								 sbuff.append("Started : " + dateFormatter.format(start) + "\n");
								 sbuff.append("Ended : " + dateFormatter.format(end) + "\n");		
								 sbuff.append("Total time : " + (end.getTime() - start.getTime()) + " milliseconds\n");
								 sbuff.append("Total added : " + addedItems.size() + "\n");
								 sbuff.append("Email added : " + addedItemToString(addedItems, "emailAddress") + "\n");
								 sbuff.append("Total updated : " + updatedItems.size() + "\n");
								 
								 notifyAdmin(siteConfig, sbuff.toString(), contactEmail);
							 }
						 } else {
							 String message = "";
							 if (map.get("message") != null) {
								 message = map.get("message").toString();
							 }
							 if (map.get("invalidItems") != null || message.equals("")) {
								 message = "automated.import.failed";
							 }
							 notifyAdmin(siteConfig, getApplicationContext().getMessage(message, new Object[0], null), contactEmail);								 
						 } 			 
					 } catch (IOException e) {
						 notifyAdmin(siteConfig, getApplicationContext().getMessage("excelfile.invalid", new Object[0], null), contactEmail);
					 } catch (Exception e) {
						 
						 e.printStackTrace();
						 notifyAdmin(siteConfig, e.toString(), "developers@advancedemedia.com");
					 }	 
				 }
			 } else {
				 notifyAdmin(siteConfig, ftp.getReplyString(), contactEmail);
			 }
			 			 
			 // Logout from the FTP Server and disconnect
			 ftp.logout();
			 ftp.disconnect();
		} catch (UnknownHostException e) {
			notifyAdmin(siteConfig, getApplicationContext().getMessage("dataFeed.ftp.unknownHost", new Object[0], null), contactEmail);
		} catch (Exception e) {
			// do nothing
		}
		
		
		// export the customer list in csv file
		File baseFile = new File(getServletContext().getRealPath("/"));
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				baseFile = new File( (String) prop.get( "site.root" ) + "/" );
			}
		} catch ( Exception e ) {}
		
		
		StringBuffer sbuff = new StringBuffer();
		
		try {
			// create CSV file
			// EVE/customers_import.csv
			File exportFile = new File(baseFile + "/customers_added.csv");
	    	CSVWriter writer = new CSVWriter(new FileWriter(exportFile));
	    	List<String> line = new ArrayList<String>();
	    	line.add("Email");
	    	
	    	String[] lines = new String[line.size()];
	    	writer.writeNext(line.toArray(lines));
	    	
	    	for (Map<String, Object> item : addedItems) {
	    		line = new ArrayList<String>();
	        	line.add(item.get("emailAddress").toString());
	        	lines = new String[line.size()];
	        	writer.writeNext(line.toArray(lines));

	    	}
	    	writer.close();
			
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	
	 
	        if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
	            // upload file
	            FileInputStream fis = new FileInputStream(exportFile);
	            ftp.storeFile("/EVE/customers_added.csv", fis );
	            fis.close();        	
	        }
	        
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
        
		} catch (Exception e) {
			e.printStackTrace();
			notifyAdmin(siteConfig, "Evergreen autoImport() on ", contactEmail);
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		sbuff.append("Create customers_import.csv file in EVE folder. This csv contains Email." + "\n\n");
		notifyAEM("autoImport() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());

    }
    
    private String addedItemToString (List<Map<String, Object>> items, String toPrint) {
    	StringBuffer list = new StringBuffer();
    	for (Map<String, Object> addedItem : items) {
    		list.append("\n"+addedItem.get(toPrint));
    	}
    	return list.toString();
    }
    
	public String removeFirst(String input)
	{
		if(input.startsWith(" ")){
			return input.substring(1,input.length());
		}else{
			return input;
		}
	}
	private void notifyAdmin(Map<String, Configuration> siteConfig, String message, String contactEmail) {
		// send email notification
    	if (contactEmail.trim().equals("")) {
    		return;
    	}
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom("myevergreen@myevergreennoreply.com");
		msg.setBcc("developers@advancedemedia.com");
		msg.setSubject("Automated customer import on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
			ex.printStackTrace();
		}
	}
	
	private void notifyAEM(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
}