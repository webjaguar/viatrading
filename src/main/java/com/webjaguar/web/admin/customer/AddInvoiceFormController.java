/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.17.2007
 */

package com.webjaguar.web.admin.customer;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.view.RedirectView;

import com.buySafe.ShoppingCartAddUpdateRS;
import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CreditCard;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Payment;
import com.webjaguar.model.Product;
import com.webjaguar.model.Promo;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.model.Supplier;
import com.webjaguar.model.UpsOrder;
import com.webjaguar.model.WorkOrder;
import com.webjaguar.model.WorkOrderLineItem;
import com.webjaguar.web.admin.orders.InvoiceFormController;
import com.webjaguar.web.domain.Utilities;
import com.webjaguar.web.form.InvoiceForm;

public class AddInvoiceFormController extends InvoiceFormController {
	
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	private String type;
	public void setType(String type) { this.type = type; }
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		
		InvoiceForm invoiceForm = (InvoiceForm) command;
		Map<String, Object> map = new HashMap<String, Object>();
		
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView("../customers/customerList.jhtm"));
		}
		
		// check work order again
		if (invoiceForm.getOrder().getWorkOrderNum() != null) {
			WorkOrder workOrder = this.webJaguar.getWorkOrder(invoiceForm.getOrder().getWorkOrderNum());
	    	if (workOrder.getService().getStatus().equalsIgnoreCase("completed")
	    			|| workOrder.getService().getStatus().equalsIgnoreCase("canceled")) {
	    		// prevent completed/canceled work orders from generating an invoice
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("../service/workOrderPrint.jhtm"), "num", workOrder.getService().getServiceNum()));			
	    	}	 
		}
		
		Customer customer = null;
		if(invoiceForm!=null && invoiceForm.getOrder()!=null){
			customer = this.webJaguar.getCustomerById( invoiceForm.getOrder().getUserId());
		}
		try {
			customer.setPayment(ServletRequestUtils.getStringParameter( request, "order.paymentMethod",""));
		} catch(Exception e) {
			// do nothing
		}
		map.put("customer", customer);
		invoiceForm.getOrder().setSubTotal(updateLineItems(request, invoiceForm, errors));
		if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") && invoiceForm.getOrder().getInsertedLineItems().size() > 0) {
			invoiceForm.getOrder().setWantsBond(ServletRequestUtils.getBooleanParameter(request, "WantsBondField"));
		    if(request.getSession().getAttribute("buySafeCartId") == null){
				request.getSession().setAttribute("buySafeCartId", UUID.randomUUID().toString().toUpperCase().replace("-", "").substring(0, 4)+new Date().getTime());
			}
		    ShoppingCartAddUpdateRS addUpdateRS = null;
			
			try{
				addUpdateRS = this.webJaguar.addUpdateShoppingCartRequest(null, invoiceForm.getOrder(), request, invoiceForm.getOrder().getUserId(), null);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(addUpdateRS != null && addUpdateRS.getBondCostDisplayText() != null && !addUpdateRS.getBondCostDisplayText().equals("")){
				invoiceForm.getOrder().setWantsBond(true);
				invoiceForm.getOrder().setBondCost(addUpdateRS.getTotalBondCost());
			} else {
				invoiceForm.getOrder().setWantsBond(false);
				invoiceForm.getOrder().setBondCost(null);
			}
			invoiceForm.getOrder().setCartId(request.getSession().getAttribute("buySafeCartId").toString());
		}
	    
	    this.webJaguar.updateGrandTotal(gSiteConfig, invoiceForm.getOrder());
		
		boolean hasPaymentErrors = false;
		if (invoiceForm.getOrder().getPayments() != null) {
			hasPaymentErrors = checkPayments(request, invoiceForm, errors);
		}

		UpsOrder upsWorldShip = null;
		if(siteConfig.get("WORLD_SHIP_IMPORT_ORDERS").getValue().equals("true")) {			
			upsWorldShip = new UpsOrder();
			upsWorldShip.setCustomerPO2(request.getParameter("customerPO2"));
			upsWorldShip.setCarrierPhone(request.getParameter("carrierPhone"));
		}
		// add sku
		if (request.getParameter("__update") != null) {
			
			map.put("upsWorldShip", upsWorldShip);
			for (int skuIndex = 1; skuIndex <= 5; skuIndex++) {
				String sku = ServletRequestUtils.getStringParameter( request, "__addProduct" + skuIndex + "_sku", "" );
				if ( !sku.isEmpty() ) {
					Integer existingId = this.webJaguar.getProductIdBySku(sku);
					//Product product = this.webJaguar.getProductById( existingId, request );
					Product product = this.webJaguar.getProductById(existingId, 1, false, null);
					if (product == null && !siteConfig.get("ORDER_ADD_VALID_SKU").getValue().equals("true")) {
						product = new Product();
						product.setSku( sku );
					}
					if (product == null && siteConfig.get("ORDER_ADD_VALID_SKU").getValue().equals("true")) {
						errors.reject("invalidSku", "Invalid Sku");
					}
					if (product != null) {
						// check if the product is LNOW product and has cost, if doesn't has cost, cannot add to invoice
						if(sku.contains("-")){
							String[] skuContainsDash=Utilities.split(sku, "-");
							Integer supplierID = this.webJaguar.getCustomerIdBySupplierPrefix(skuContainsDash[0]);
							if(supplierID != null){
								Supplier supplier = this.webJaguar.getProductSupplierPrimaryBySku(sku);
								if(supplier == null){
									errors.reject("invalidSku", "This sku is LNOW sku but doesn't have supplier");
								}else if(supplier.getPrice() == null){
									errors.reject("invalidSku", "This sku has supplier but doesn't have cost value");
								}else{
									LineItem lineItem = new LineItem();
									this.setLineItem(lineItem, product, skuIndex, request, invoiceForm);
									invoiceForm.getOrder().getInsertedLineItems().add(lineItem);
								}
							}else{
								LineItem lineItem = new LineItem();
								this.setLineItem(lineItem, product, skuIndex, request, invoiceForm);
								invoiceForm.getOrder().getInsertedLineItems().add(lineItem);
							}
							
						}else{
							LineItem lineItem = new LineItem();
							this.setLineItem(lineItem, product, skuIndex, request, invoiceForm);
							invoiceForm.getOrder().getInsertedLineItems().add(lineItem);
						}
						
					}
				}
			}
			
			if(invoiceForm.getOrder().getStatus() != null && invoiceForm.getOrder().getStatus().equals("xq")) {
				invoiceForm.getOrder().getLineItems().addAll( invoiceForm.getOrder().getInsertedLineItems() );
				double totalWeight = 0;
				double subTotal = 0;
				for (LineItem lineItem:invoiceForm.getOrder().getLineItems()) {
					if (lineItem.getPackageWeight() != null ) {
						totalWeight = totalWeight + (lineItem.getPackageWeight().doubleValue() * lineItem.getQuantity()) ;
					}
					subTotal = subTotal + ( lineItem.getTotalPrice() != null ? lineItem.getTotalPrice() : 0.0);
				}
				invoiceForm.getOrder().setTotalWeight(totalWeight);
				invoiceForm.getOrder().setSubTotal(subTotal);
				List<ShippingRate> shippingRates = this.webJaguar.calculateShippingHandling(invoiceForm.getOrder(), null, request, gSiteConfig);
				
				
				invoiceForm.getOrder().getLineItems().removeAll( invoiceForm.getOrder().getInsertedLineItems() );
				
				
				this.webJaguar.updateGrandTotal(gSiteConfig, invoiceForm.getOrder());
				
				map.put("shippingRates", shippingRates);
			}
		}
		
		// check if save button was pressed
		if (request.getParameter("__save") != null) {	
			if(!siteConfig.get( "INVOICE_REMOVE_VALIDATION" ).getValue().equals("true")) {
				if(!(invoiceForm.getOrder().getStatus() != null && invoiceForm.getOrder().getStatus().equals("xq"))) {
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "order.paymentMethod", "payment.required");
				}
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "order.shippingMethod", "shipping.required");	
				if (siteConfig.get( "ORDER_FLAG1_NAME" ).getValue().length()>0 && !(invoiceForm.getOrder().getStatus() != null && invoiceForm.getOrder().getStatus().equals("xq"))) {
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "order.flag1", "form.required");
				}
				if (siteConfig.get( "ORDER_TYPE" ).getValue().length()>0 && !(invoiceForm.getOrder().getStatus() != null && invoiceForm.getOrder().getStatus().equals("xq"))) {
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "order.orderType", "form.required");
				}
			}
			
			//prevent adding orders for suspended cusotmers
			if (customer != null && (customer.isSuspended())) {
				errors.reject("ACCOUNT_SUSPENDED");
				return showForm(request, response, errors, map);
			}
			
			// invoice can be added without lineitem
			
			// payments
			if (hasPaymentErrors) {
				errors.reject("order.exception.payments");
			} else if (invoiceForm.getOrder().getPayments() != null && invoiceForm.getOrder().getTotalPayments() != null
					&& invoiceForm.getOrder().getTotalPayments() > Utilities.roundFactory(invoiceForm.getOrder().getGrandTotal(), 2, BigDecimal.ROUND_HALF_UP)) {
				errors.reject("order.exception.amountLessThanTotalPayments");
			}
			if (errors.hasErrors()) {
				return showForm(request, response, errors, map);				
			}
			
			
			// Consignment
			if ((Boolean) gSiteConfig.get("gCUSTOMER_SUPPLIER")) {
				for (LineItem lineItem :invoiceForm.getOrder().getInsertedLineItems()) {
					Supplier supplier = this.webJaguar.getProductSupplierPrimaryBySku(lineItem.getProduct().getSku());
					if (supplier != null) {
						lineItem.setSupplier(supplier);
						lineItem.setCost(supplier.getPrice());
						lineItem.setCostPercent(supplier.isPercent());
						lineItem.getProduct().setDefaultSupplierId(supplier.getId());
					}
				}
			}
			
			// insert order
			OrderStatus orderStatus = new OrderStatus();
			if(invoiceForm.getOrder().getStatus() != null && invoiceForm.getOrder().getStatus().equals("xq")) {
				orderStatus.setStatus( "xq" ); // quote
			} else {
				orderStatus.setStatus( "p" ); // pending
			}
			orderStatus.setUsername(this.webJaguar.getAccessUser(request).getUsername());
			// set access user id
			invoiceForm.getOrder().setAccessUserId(this.webJaguar.getAccessUser(request).getId());
			int lastLineNum = 0;
			for (LineItem lineItem: invoiceForm.getOrder().getInsertedLineItems()) {
				lineItem.setLineNumber( ++lastLineNum );
			}
			invoiceForm.getOrder().getLineItems().addAll( invoiceForm.getOrder().getInsertedLineItems() );
			// affiliate 
			int gAFFILIATE = (Integer) gSiteConfig.get( "gAFFILIATE" );
			// set ip
			invoiceForm.getOrder().setIpAddress(request.getRemoteAddr());
			
			// placed invoice using cusotmer's CREDIT
			Customer customerWithCredit = this.webJaguar.getCustomerById( ServletRequestUtils.getIntParameter( request, "cid", -1 ) );
			Double amt = null;
			
			// If payment method is 'credit' set 'credit used' to credit amount. 
			if (invoiceForm.getOrder().getPaymentMethod() != null && invoiceForm.getOrder().getPaymentMethod().equalsIgnoreCase("vba")) {
				amt = invoiceForm.getOrder().getGrandTotal();
				invoiceForm.getOrder().setCreditUsed(amt);
			} else { // in case of multiple payment methods: if one of the payment methods is 'credit' set 'credit used' to credit amount.
				if (invoiceForm.isApplyUserPoint()) {
					amt = customerWithCredit.getCredit();
					invoiceForm.getOrder().setCreditUsed(amt);
				} else if(invoiceForm.getOrder().getCreditUsed() != null) {
					amt=null;
					invoiceForm.getOrder().setCreditUsed(null);
				}
			}
			// if credit used is > 0 set the payment to credit used. 
			if(invoiceForm.getOrder().getCreditUsed() != null && invoiceForm.getOrder().getCreditUsed() > 0) {
				invoiceForm.getOrder().setPayment(amt);
			}
			
			// insert order
			this.webJaguar.insertOrder( invoiceForm.getOrder(), orderStatus, false, true, gAFFILIATE, siteConfig, gSiteConfig );	
			
			//Superior Washer World Ship Orders
			if(upsWorldShip != null) {			
				int userId = ServletRequestUtils.getIntParameter( request, "cid", -1 );
				customer = this.webJaguar.getCustomerById( userId );	
				
				upsWorldShip.setAccountNumber(customer.getAccountNumber());
				upsWorldShip.setCompanyName(customer.getAddress().getCompany());
				upsWorldShip.setAddr1(customer.getAddress().getAddr1());
				upsWorldShip.setCity(customer.getAddress().getCity());
				upsWorldShip.setStateProvince(customer.getAddress().getStateProvince());
				upsWorldShip.setCustomerPO(invoiceForm.getOrder().getPurchaseOrder());
				//Superior Washer has only one lineItems so LineItem is set to '0'
				if (!invoiceForm.getOrder().getLineItems().isEmpty() && invoiceForm.getOrder().getLineItems() != null) {
					upsWorldShip.setLineItems(invoiceForm.getOrder().getLineItems().get(0).getProduct().getSku());
					upsWorldShip.setWeight(invoiceForm.getOrder().getLineItems().get(0).getProduct().getWeight());
					upsWorldShip.setNumberPackages(invoiceForm.getOrder().getLineItems().get(0).getQuantity());
				}
				upsWorldShip.setShippingCompany(invoiceForm.getOrder().getShippingMethod());
				upsWorldShip.setOrderId(invoiceForm.getOrder().getOrderId());
				this.webJaguar.importUpsOrders(upsWorldShip);
			}
			
			// buy safe
			if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
				try {
					 this.webJaguar.setShoppingCartCheckout( invoiceForm.getOrder(), null );
						
				} catch (Exception e) { 
					e.printStackTrace();
					OrderStatus status = new OrderStatus();
					status.setOrderId( invoiceForm.getOrder().getOrderId() );
					status.setStatus( "p" ); // pending
					if( (invoiceForm.getOrder().getWantsBond() != null) && (invoiceForm.getOrder().getWantsBond() == true) ) {
						status.setComments("BuySafe Bond was selected but cancelled as system failed to connect Buysafe. Please check the amount charged and refund the bond cost to Customer.");
					} else {
						status.setComments("BuySafe Bond was not selected. System failed to connect Buysafe");
					}
					invoiceForm.getOrder().setWantsBond(false);
					invoiceForm.getOrder().setBondCost(null);
					invoiceForm.getOrder().setGrandTotal();
					
					this.webJaguar.cancelBuySafeBond(invoiceForm.getOrder(), status);
				}
				request.getSession().removeAttribute("buySafeCartId");
			}
			
			return new ModelAndView(new RedirectView("../orders/invoice.jhtm?order="+invoiceForm.getOrder().getOrderId()));
		}	
		
		return showForm(request, response, errors, map);
		
	}
	
	public Object formBackingObject(HttpServletRequest request) 
	throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
	    int userId = ServletRequestUtils.getIntParameter( request, "cid", -1 );
	    Customer customer = this.webJaguar.getCustomerById( userId );
		if(customer.isUpdateNewInformation()){
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("../customers/customer.jhtm?id="+userId+"&forwardAction=../orders/addInvoice.jhtm?cid="+userId), "type","update"));		
		}

	    InvoiceForm invoiceForm = new InvoiceForm();
	    invoiceForm.setNewInvoice( true );
	    	    
	    WorkOrder workOrder = null;
	    

	    if (request.getParameter("workOrder") != null) {
	    	workOrder = this.webJaguar.getWorkOrder(ServletRequestUtils.getIntParameter(request, "workOrder", -1));
	    	if (workOrder == null) {
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("../service/services.jhtm")));				    		
	    	} else if (workOrder.getService().getStatus().equalsIgnoreCase( "completed" )
	    			|| workOrder.getService().getStatus().equalsIgnoreCase( "canceled" )) {
	    		// prevent completed/canceled work orders from generating an invoice
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("../service/workOrderPrint.jhtm"), "num", workOrder.getService().getServiceNum()));			
	    	}	    	
	    	userId = workOrder.getService().getItem().getCustomer().getId();
	    }
	  

	    if (customer != null) {
	    	invoiceForm.getOrder().setUserId( customer.getId() );
	    	invoiceForm.getOrder().setBilling( setAddress(customer.getAddress()) );
		    invoiceForm.getOrder().setShipping( setAddress(customer.getAddress()) );
		    invoiceForm.getOrder().setSalesRepId( customer.getSalesRepId() );
		    invoiceForm.getOrder().setPromo( new Promo() );	
		    invoiceForm.getOrder().setSubTotal(0.0);
		    invoiceForm.getOrder().setLanguageCode(customer.getLanguageCode());
		} else {
    		// customer is required
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("../customers/customerList.jhtm")));	
	    }

	    invoiceForm.getOrder().setInsertedLineItems(new ArrayList<LineItem>());

	    if (workOrder != null) {
	    	invoiceForm.getOrder().setWorkOrderNum(workOrder.getService().getServiceNum());
	    	invoiceForm.getOrder().setPurchaseOrder(workOrder.getService().getPurchaseOrder());
	    	if (workOrder.getService().getItem().getAddress() != null) {
		    	invoiceForm.getOrder().setShipping(setAddress(workOrder.getService().getItem().getAddress()));	    		
	    	}
	    	
	    	invoiceForm.getOrder().setSubTotal(insertLineItems(request, workOrder, invoiceForm));
	    	
	    	// get customer's payment options
	    	if (customer.getPayment() == null || customer.getPayment().equals( "" )) {
	    		// immediate
	    		invoiceForm.getOrder().setPaymentMethod("Credit Card");
		    	invoiceForm.getOrder().setCreditCard(new CreditCard()); 		
	    	} else {
	    		invoiceForm.getOrder().setPaymentMethod(customer.getPayment());
	    	}
	    }
	    invoiceForm.getOrder().setOrderType( siteConfig.get( "DEFAULT_ORDER_TYPE" ).getValue() );
	    invoiceForm.getOrder().setCcFeeRate( Double.parseDouble( siteConfig.get( "DEFAULT_CREDITCARD_FEE_RATE" ).getValue() ));
	    invoiceForm.getOrder().setTaxExemption((customer.getTaxId() == null || customer.getTaxId().trim().equals( "" )) ? false : true);
	    this.webJaguar.updateTaxAndTotal(gSiteConfig, globalDao, invoiceForm.getOrder());
	    
	    // payments
	    if ((Boolean) gSiteConfig.get("gPAYMENTS")) {
	    	List<Payment> payments = new ArrayList<Payment>();
	    	Date now = new Date();
	    	for (int i=0; i<5; i++) payments.add(new Payment(customer, now));
	    	invoiceForm.getOrder().setPayments(payments);
	    }
	    
	    
	    if(type != null && type.equalsIgnoreCase("quote") && (Boolean) gSiteConfig.get("gSHIPPING_QUOTE")) {
	    	invoiceForm.getOrder().setStatus("xq");
	    	Address source = new Address();
	    	source.setCountry(siteConfig.get( "SOURCE_COUNTRY" ).getValue());
	    	source.setStateProvince(siteConfig.get( "SOURCE_STATEPROVINCE" ).getValue());
			source.setZip(siteConfig.get( "SOURCE_ZIP" ).getValue());
			invoiceForm.getOrder().setSource( setAddress(source) );
			  
	    	setFormView("admin/orders/quoteForm");
		}
	     
		return invoiceForm;
    }

	private Address setAddress(Address address) {
		Address newAddress = new Address();
		newAddress.setFirstName( address.getFirstName() );
		newAddress.setLastName( address.getLastName() );		
		newAddress.setCompany( address.getCompany() );
		newAddress.setAddr1( address.getAddr1() );
		newAddress.setAddr2( address.getAddr2() );
		newAddress.setCity( address.getCity() );
		newAddress.setStateProvince( address.getStateProvince() );
		newAddress.setZip( address.getZip() );
		newAddress.setCountry( address.getCountry() );
		newAddress.setPhone( address.getPhone() );
		newAddress.setCellPhone( address.getCellPhone() );
		newAddress.setFax( address.getFax() );
		newAddress.setResidential( address.isResidential() );
		newAddress.setLiftGate( address.isLiftGate() );
		newAddress.setCode(address.getCode());
		return newAddress;
	}
	
	private double insertLineItems(HttpServletRequest request, WorkOrder workOrder, InvoiceForm invoiceForm) {
		
		// subtotal
		double subTotal = 0;
		
		for (WorkOrderLineItem woLineItem:workOrder.getLineItems()) {
			LineItem lineItem = new LineItem();
			Product product = woLineItem.getProduct();
			// get taxable
			Product thisProduct = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(product.getSku()), 0, false, null);
			if (thisProduct != null) {
				product.setTaxable(thisProduct.isTaxable());
			}
			lineItem.setProduct(product);
			lineItem.setQuantity(woLineItem.getQuantity());
			lineItem.setUnitPrice(this.webJaguar.getUnitPrice(gSiteConfig, siteConfig, invoiceForm.getOrder().getUserId(), product.getSku(), lineItem.getQuantity()));
			lineItem.setOriginalPrice(lineItem.getUnitPrice());
			invoiceForm.getOrder().getInsertedLineItems().add(lineItem);
			if (woLineItem.getInSN() != null && woLineItem.getInSN().trim().length() > 0) {
				lineItem.addSerialNum(woLineItem.getInSN().trim());
			}
			
			// update sub total
			if (lineItem.getTotalPrice() != null) subTotal = lineItem.getTotalPrice() + subTotal;
		}
		
		return subTotal;
	}
	
	private boolean checkPayments(HttpServletRequest request, InvoiceForm invoiceForm, BindException errors) {
		boolean hasErrors = false;
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		int index = 0;
		for (Payment payment: invoiceForm.getOrder().getPayments()) {
			payment.setMessage(null);
			
			payment.setMemo(ServletRequestUtils.getStringParameter(request, "__paymentMemo_" + index, "").trim());
			payment.setPaymentMethod(ServletRequestUtils.getStringParameter(request, "__paymentPaymentMethod_" + index, ""));
			// get amount
			try {
				payment.setAmount(Utilities.roundFactory(ServletRequestUtils.getDoubleParameter(request, "__paymentAmount_" + index), 2, BigDecimal.ROUND_HALF_UP));
				if (payment.getAmount() <= 0) {
					payment.setMessage("Amount should be a positive amount.");
					hasErrors = true;
				}
			} catch (Exception e) {
				payment.setAmount(null);
			}
			try {
				payment.setDate(df.parse(ServletRequestUtils.getStringParameter(request, "__paymentDate_" + index, "")));				
			} catch (ParseException e) {
				payment.setDate(null);
			}
			if (payment.getMemo().length() > 0 ^ payment.getAmount() != null) {
				if (payment.getAmount() == null) {
					payment.setMessage("Amount is required.");
				} else {
					payment.setMessage("Memo is required.");
				}
				hasErrors = true;
			}
			
			index++;
		}
		
		return hasErrors;
	}
	
	// multiple browser tabs/windows session issue
	// http://forum.springframework.org/showthread.php?t=42241
	protected String getFormSessionAttributeName(HttpServletRequest request) {
		String orderId = request.getParameter("cid");
		return super.getFormSessionAttributeName(request) + orderId;	    	  
	}
	
	private LineItem setLineItem(LineItem lineItem, Product product, int skuIndex, HttpServletRequest request, InvoiceForm invoiceForm){
		lineItem.setProduct(product);
		lineItem.setProductId( product.getId() );
		lineItem.setQuantity(ServletRequestUtils.getIntParameter( request, "__addProduct" + skuIndex + "_qty", 1 ));
		lineItem.setUnitPrice(this.webJaguar.getUnitPrice(gSiteConfig, siteConfig, invoiceForm.getOrder().getUserId(), product.getSku(), lineItem.getQuantity()));
		lineItem.setOriginalPrice( lineItem.getUnitPrice() );
		
		return lineItem;
	}
}
