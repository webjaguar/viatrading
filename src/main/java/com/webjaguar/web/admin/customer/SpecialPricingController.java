/*
 * Copyright 2005, 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 * @since 07.05.2007
 */

package com.webjaguar.web.admin.customer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.SpecialPricing;
import com.webjaguar.model.SpecialPricingSearch;
import com.webjaguar.model.Customer;

public class SpecialPricingController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		SpecialPricingSearch specialPricingSearch = getSpecialPricingSearch( request ); 
		String message = null;
		Map<String, Object> myModel = new HashMap<String, Object>();
		Integer customerId = ServletRequestUtils.getIntParameter( request, "id", 0 );

		boolean success = false;
		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i=0; i<ids.length; i++) {
					this.webJaguar.deleteSpecialPricingById(Integer.parseInt(ids[i]));
				}
			}
		}
		
		// check if update active button was clicked
		if (request.getParameter("__update_active") != null) {
			String ids[] = request.getParameterValues("__id");
			if (ids != null) {
				for (int i=0; i<ids.length; i++) {
					SpecialPricing sp = new SpecialPricing();
					sp.setCustomerId( customerId );
					// set id
					Integer id = new Integer (ids[i]);
					sp.setId( id );
					// set active
					String active = request.getParameter("__active_" + id);   
					if(active!=null){
						sp.setEnable( true );
					}
					this.webJaguar.updateSpecialPricingActive( sp );
					success = true;
				}
			}
		}
		
		// check if add button was pressed 
		if (request.getParameter("__add") != null) {
			String sku = request.getParameter("__sku");
			Integer groupId = ServletRequestUtils.getIntParameter( request, "__group_id", 0 );
			if(groupId!= 0){
				if (validateForm(sku)) {
					Double price = ServletRequestUtils.getDoubleParameter( request, "__price", -999 );
					// if price is empty don't update anything
					if (price != -999) {
						ArrayList<Integer> userIdList = (ArrayList<Integer>) this.webJaguar.getCustomerIdByGroupId(groupId);
						if (!userIdList.isEmpty()) {
							success = true;
							for (Integer userId : userIdList) {
								SpecialPricing sp = new SpecialPricing( sku, userId, price , true);
								this.webJaguar.insertSpecialPricing(sp);
							}
						}
					} 
				}	
			}
			else{
				if ( validateForm(sku) ) {
					Double price = ServletRequestUtils.getDoubleParameter( request, "__price", -999 );
					SpecialPricing sp = new SpecialPricing( sku, specialPricingSearch.getCustomerId(), price , true);
					this.webJaguar.insertSpecialPricing(sp);
					success = true;
				}
			}
		}
		// check if add button was pressed 
		if (request.getParameter("__addProductOfCategory") != null) {
			Integer cid = ServletRequestUtils.getIntParameter( request, "__cid", 0 );
			if (cid != 0) {
				ArrayList<String> skuList = (ArrayList<String>) this.webJaguar.getProductSkuListByCategoryId(cid);
				Double price = ServletRequestUtils.getDoubleParameter( request, "__price_cid", -999 );
				// if price is empty don't update anything
				if (price != -999 && !skuList.isEmpty()) {
					success = true;
					for (String sku : skuList) {
						SpecialPricing sp = new SpecialPricing( sku, specialPricingSearch.getCustomerId(), price , true);
						this.webJaguar.insertSpecialPricing(sp);
					}
				} 
			}
		}
		
		// check if add button was pressed 
		if (request.getParameter("__addProductOfCategoryWithCustomerGroupId") != null) {
			Integer cid = ServletRequestUtils.getIntParameter( request, "__cid_gid", 0 );
			Integer groupId = ServletRequestUtils.getIntParameter( request, "__group_id_cid_gid", 0 );
			if (cid != 0) {
				ArrayList<String> skuList = (ArrayList<String>) this.webJaguar.getProductSkuListByCategoryId(cid);
				Double price = ServletRequestUtils.getDoubleParameter( request, "__price_cid_gid", -999 );
				// if price is empty don't update anything
				if (price != -999 && !skuList.isEmpty()) {
					ArrayList<Integer> userIdList = (ArrayList<Integer>) this.webJaguar.getCustomerIdByGroupId(groupId);
					if (!userIdList.isEmpty()) {
						success = true;
						for (Integer userId : userIdList) {
							for (String sku : skuList) {
								SpecialPricing sp = new SpecialPricing( sku, userId, price , true);
								this.webJaguar.insertSpecialPricing(sp);
							}
						}
					}
				} 
			}
		}
		
		if (success) {
			myModel.put("message", "update.successful");
		}
		
		PagedListHolder specialPriceList = null;
		if ( specialPricingSearch.getCustomerId() != null ) {
			// get customers
			specialPriceList = new PagedListHolder( this.webJaguar.getSpecialPricingByCustomerID( specialPricingSearch ) );
			specialPriceList.setPageSize(specialPricingSearch.getPageSize());
			specialPriceList.setPage(specialPricingSearch.getPage()-1);
		}

		myModel.put("spSearch", specialPricingSearch);
		myModel.put("spList", specialPriceList);
		myModel.put("customerId", specialPricingSearch.getCustomerId());
		myModel.put("customer", this.webJaguar.getCustomerById(customerId));
		//for getting only active Customer groups
		CustomerGroupSearch customerGroupSearch = new CustomerGroupSearch();
		customerGroupSearch.setActive("1");
		myModel.put( "groups", this.webJaguar.getCustomerGroupList(customerGroupSearch)) ;
		
        return new ModelAndView("admin/customers/specialPricingList", "model", myModel);
     }
	
	private SpecialPricingSearch getSpecialPricingSearch(HttpServletRequest request) {
		SpecialPricingSearch spSearch = (SpecialPricingSearch) request.getSession().getAttribute( "spSearch" );
		if (spSearch == null) {
			spSearch = new SpecialPricingSearch();
			request.getSession().setAttribute( "spSearch", spSearch );
		}
		
		
		// Customer Id
		if (request.getParameter("id") != null) {
			spSearch.setCustomerId(ServletRequestUtils.getIntParameter( request, "id", 0 ));
		}	
		
		// Product Sku
		if (request.getParameter("_sku_search") != null) {
			spSearch.setSku(ServletRequestUtils.getStringParameter( request, "_sku_search", "" ));
		}
				
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				spSearch.setPage( 1 );
			} else {
				spSearch.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				spSearch.setPageSize( 10 );
			} else {
				spSearch.setPageSize( size );				
			}
		}

		return spSearch;
	}
	
	private boolean validateForm( String sku ) {
		if ( this.webJaguar.getProductIdBySku( sku ) == null ) {
			return false;
		}
		
		return true;
	}	
}
