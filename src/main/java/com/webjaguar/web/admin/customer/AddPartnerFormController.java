package com.webjaguar.web.admin.customer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.BudgetPartner;
import com.webjaguar.web.form.CategoryForm;

public class AddPartnerFormController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }


	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		// get accessUser username
    	String accessUser = this.webJaguar.getAccessUser(request).getUsername();
    	
    	
    	if(request.getParameter("pid") != null) {
    		
			BudgetPartner partner = new BudgetPartner();
			partner.setId(Integer.parseInt(request.getParameter("pid")));
			partner = this.webJaguar.getPartnerById(partner);
			
			myModel.put("pid", request.getParameter("pid"));
			myModel.put("active", partner.isActive());
			myModel.put("partnerName", partner.getPartnerName());
    		myModel.put("update", "true");
			
			partner.setPartnerName(request.getParameter("__partnerName"));
			partner.setActive(Boolean.parseBoolean(request.getParameter("_active")));
			
			partner.setModifiedBy(accessUser);
			partner.setModifiedDate(new Date());
			if(request.getParameter("update") != null) {
				this.webJaguar.updatePartners(partner);
				return new ModelAndView(new RedirectView("partnersList.jhtm"));
			}
		}
		
		if(request.getParameter("__addName") != null && request.getParameter("__partnerName") != null ) {
			BudgetPartner partner = new BudgetPartner();
			partner.setPartnerName(request.getParameter("__partnerName"));
			partner.setCreatedBy(accessUser);
			partner.setActive(Boolean.parseBoolean(request.getParameter("_active")));
			partner.setCreatedDate(new Date());
			if(this.webJaguar.getPartnerByName(partner.getPartnerName()) == true) {
				myModel.put("active", partner.isActive());
				myModel.put("partnerName", partner.getPartnerName());
				myModel.put("message", "partnerExists");
			} else {
				this.webJaguar.insertBudgetPartners(partner);
				return new ModelAndView(new RedirectView("partnersList.jhtm"));
			}
		}
		return new ModelAndView("admin/customers/addBudgetPartners", myModel);
	}
}
