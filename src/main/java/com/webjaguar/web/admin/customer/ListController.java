/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 */

package com.webjaguar.web.admin.customer;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.asicentral.content_bridge._1_0.editorial.Search;
import com.webjaguar.listener.SessionListener;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.Location;
import com.webjaguar.model.QualifierSearch;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SalesRepSearch;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.VirtualBank;
import com.webjaguar.model.ZipCode;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.web.form.EmailMessageForm;

public class ListController implements Controller {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}
	
	private String type;

	public void setType(String type) {
		this.type = type;
	}
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Date d1 = new Date();
		//System.out.println("Customer List Line 52  : "+((new Date().getTime() - d1.getTime()) / 1000));
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("addCustomer.jhtm"));
		}

		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		CustomerSearch customerSearch = this.webJaguar.getCustomerSearch(request);

		//System.out.println("Customer List Line 63  : "+((new Date().getTime() - d1.getTime()) / 1000));
		// trackcode
		if ((type==null) || (!type.equalsIgnoreCase("list2"))) {
			customerSearch.setTrackcodeFieldDropDown(null);
		}
		Map<Integer, SalesRep> qualifierRepMap = new HashMap<Integer, SalesRep>();
		Map<Integer, String> qualifierMap = new HashMap<Integer, String>();
		List<SalesRep> qualifierList = this.webJaguar.getSalesRepList();
		for (SalesRep salesRep : qualifierList) {
			qualifierRepMap.put(salesRep.getId(), salesRep);
		}
		model.put("qualifierRepMap", qualifierRepMap);
		
		// loggedIn
		ArrayList<Integer> loggedInIds = new ArrayList<Integer>();
		loggedInIds.addAll(SessionListener.getActiveSessions().values());
		customerSearch.setLoggedIn(loggedInIds);

		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map<Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = null;

			Integer salesRepId = null;
			if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
				salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
				customerSearch.setAccountManager(salesRepId);
			}

			//System.out.println("Customer List Line 84  : "+((new Date().getTime() - d1.getTime()) / 1000));
			if (salesRepId == null) {
				salesReps = this.webJaguar.getSalesRepList();
			} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
				salesReps = this.webJaguar.getSalesRepList(salesRepId);
			} else {
				salesReps = new ArrayList<SalesRep>();
				salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
			}
			for (SalesRep salesRep : salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);
			}
			if (salesRepId != null) {
				// make sure access users sees only his customers
				if (customerSearch.getSalesRepId() == null) {
					customerSearch.setSalesRepId(salesRepId);
				} else if (!(salesRepMap.containsKey(customerSearch.getSalesRepId()) || salesRepMap.containsKey(customerSearch.getQualifierId()))) {
					customerSearch.setSalesRepId(salesRepId);
				}
				// associate reps to parents
				for (SalesRep salesRep : salesReps) {
					if (salesRepMap.containsKey(salesRep.getParent())) {
						salesRepMap.get(salesRep.getParent()).getSubAccounts().add(salesRep);
					}
				}
				model.put("salesRepTree", salesRepMap.get(salesRepId));
			}

			model.put("salesRepMap", salesRepMap);
			model.put("salesReps", salesReps);
			
			//System.out.println("Customer List Line 115  : "+((new Date().getTime() - d1.getTime()) / 1000));
			//SalesRep for search dropdown, which only show active salesRep.
			if ((Boolean) gSiteConfig.get("gSHOPPING_CART") && (Boolean) gSiteConfig.get("gSALES_REP")){
				SalesRepSearch salesRepSearch = new SalesRepSearch();
				salesRepSearch.setShowActiveSalesRep(true);
				model.put("activeSalesReps", this.webJaguar.getSalesRepList(salesRepSearch));
				salesRepSearch = new SalesRepSearch();
				salesRepSearch.setShowNonActiveSalesRep(true);
				model.put("nonActiveSalesReps", this.webJaguar.getSalesRepList(salesRepSearch));
			}
			SalesRepSearch salesRepSearch = new SalesRepSearch();
			salesRepSearch.setShowActiveSalesRep(true);
			if (salesRepId != null) {
				model.put("activeQualifiers", salesReps);
			} else {
				model.put("activeQualifiers", this.webJaguar.getSalesRepList(salesRepSearch));
			}
			//System.out.println("Customer List Line 122  : "+((new Date().getTime() - d1.getTime()) / 1000));
			
		}

		// set end date to end of day
		if (customerSearch.getEndDate() != null) {
			customerSearch.getEndDate().setTime(customerSearch.getEndDate().getTime() + 23 * 60 * 60 * 1000 // hours
					+ 59 * 60 * 1000 // minutes
					+ 59 * 1000); // seconds
		}
		if (customerSearch.getCartEndDate() != null) {
			customerSearch.getCartEndDate().setTime(customerSearch.getCartEndDate().getTime() + 23 * 60 * 60 * 1000 // hours
					+ 59 * 60 * 1000 // minutes
					+ 59 * 1000); // seconds
		}
		if (customerSearch.getLoginEndDate() != null) {
			customerSearch.getLoginEndDate().setTime(customerSearch.getLoginEndDate().getTime() + 23 * 60 * 60 * 1000 // hours
					+ 59 * 60 * 1000 // minutes
					+ 59 * 1000); // seconds
		}

		customerSearch.setAffiliate(ServletRequestUtils.getIntParameter(request, "affId", -1));

		// get customers
		//System.out.println("Customer List Line 146  : "+((new Date().getTime() - d1.getTime()) / 1000));
		List<Integer> customerIds = this.webJaguar.getCustomerIdsList(customerSearch);
		//System.out.println("Customer List Line 148  : "+((new Date().getTime() - d1.getTime()) / 1000));
		
		// check if options button was clicked
		if (request.getParameter("__groupAssignPacher") != null) {
			// get group ids
			Set<Object> groups = new HashSet<Object>();
			String groupIdString = ServletRequestUtils.getStringParameter(request, "__group_id");
			String batchType = ServletRequestUtils.getStringParameter(request, "__batch_type");
			String[] groupIds = groupIdString.split(",");
			for (int x = 0; x < groupIds.length; x++) {
				if (!("".equals(groupIds[x].trim()))) {
					try {
						groups.add(Integer.valueOf(groupIds[x].trim()));
					} catch (Exception e) {
						// do nothing
					}
				}
			}
			String ids[] = request.getParameterValues("__selected_id");
			if ((ids != null) && (request.getParameter("__groupAssignAll") == null)) {
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.batchGroupIds(Integer.parseInt(ids[i]), groups, batchType);
				}
			} else if (request.getParameter("__groupAssignAll") != null) {
				for (Integer id : customerIds) {
					this.webJaguar.batchGroupIds(id, groups, batchType);
				}
			}
		}
		//System.out.println("Customer List Line 177  : "+((new Date().getTime() - d1.getTime()) / 1000));
		if (request.getParameter("__suspendPacher") != null) {
			// get group ids
			String batchAction = ServletRequestUtils.getStringParameter(request, "__batch_action");

			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.updateSuspension(Integer.parseInt(ids[i]), batchAction);
				}
			}
		}
		
		
		//Assign access user
		if (request.getParameter("__userAssignPacher") != null) {
		
			
			String ids[] = request.getParameterValues("__selected_id");
			
			List<Integer> userIds = new ArrayList<Integer>();
			
			if ((ids != null ) && ( request.getParameter("__userAssignAll") == null)) {
				for (int i=0; i<ids.length; i++) {
					userIds.add(Integer.parseInt(ids[i]));
				}
			} else if( request.getParameter("__userAssignAll") != null ) {
				userIds = customerIds;
			}
			if ( !userIds.isEmpty() ) {
				AccessUser accUser = null;
				if(ServletRequestUtils.getIntParameter(request, "contact_assignedTo") != null) {
					accUser = this.webJaguar.getAccessUserById(ServletRequestUtils.getIntParameter(request, "contact_assignedTo"));
				}
				this.webJaguar.batchAssignUserToCustomers(userIds, accUser);
				notifySalesRep(siteConfig, userIds);
				
			}
		}
		
		//Update New Information
		if (request.getParameter("__updateNewInformation") != null) {
			String ids[] = request.getParameterValues("__selected_id");		
			List<Integer> userIds = new ArrayList<Integer>();	
			if ((ids != null ) && ( request.getParameter("__userAssignAll") == null)) {
				for (int i=0; i<ids.length; i++) {
					userIds.add(Integer.parseInt(ids[i]));
				}
			} else if( request.getParameter("__updateNewInformationAll") != null ) {
				userIds = customerIds;
			}		
			if(ServletRequestUtils.getIntParameter(request, "__batch_updateNewInformation") != null) {
				Integer batchAction = ServletRequestUtils.getIntParameter(request, "__batch_updateNewInformation");
				
				if (userIds != null) {
					for (int i = 0; i < userIds.size(); i++) {
						this.webJaguar.updateNewInformation(userIds.get(i), batchAction);
					}
				}		
			}
		}
		
		
		if (request.getParameter("__updateQualifier") != null) {
			// get group ids
			String qualifier = ServletRequestUtils.getStringParameter(request, "__qualifier_field");

			String ids[] = request.getParameterValues("__selected_id");
			if ((ids != null) && (request.getParameter("__qualifierAll") == null)){
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.updateQualifier(Integer.parseInt(ids[i]), qualifier);
					this.webJaguar.updateCrmQualifier(Integer.parseInt(ids[i]), qualifier);
				}
			}else if(request.getParameter("__qualifierAll") != null) {
				for (Integer id : customerIds) {
					try{
						this.webJaguar.updateQualifier(id, qualifier);
						this.webJaguar.updateCrmQualifier(id, qualifier);
					}catch(Exception ex){}
				}
			}
		}
		
		if (request.getParameter("__updateWholeMetrics") != null) {
			
			String ids[] = request.getParameterValues("__selected_id");
			
			String trackCode = ServletRequestUtils.getStringParameter(request, "__track_code");
			if(trackCode!=null && trackCode.length()>0){
				if ((ids != null) && (request.getParameter("__metricsAll") == null)){
					for (int i = 0; i < ids.length; i++) {
						this.webJaguar.updateTrackCode(Integer.parseInt(ids[i]), trackCode);
						this.webJaguar.updateCrmTrackCode(Integer.parseInt(ids[i]), trackCode);
					}
				}else if(request.getParameter("__metricsAll") != null) {
					for (Integer id : customerIds) {
						this.webJaguar.updateTrackCode(id, trackCode);
						this.webJaguar.updateCrmTrackCode(id, trackCode);
					}
				}
			}
			
			
			String mainSource = ServletRequestUtils.getStringParameter(request, "__main_source");
			if(mainSource!=null && mainSource.length()>0){
				if ((ids != null) && (request.getParameter("__metricsAll") == null)) {
					for (int i = 0; i < ids.length; i++) {
						this.webJaguar.updateMainSource(Integer.parseInt(ids[i]), mainSource);
					}
				} else if (request.getParameter("__metricsAll") != null) {
					for (Integer id : customerIds) {
						this.webJaguar.updateMainSource(id, mainSource);
					}
				}
			}
			
			String paid = ServletRequestUtils.getStringParameter(request, "__paid");
			if(paid!=null && paid.length()>0){
				if ((ids != null) && (request.getParameter("__metricsAll") == null)) {
					for (int i = 0; i < ids.length; i++) {
						this.webJaguar.updatePaid(Integer.parseInt(ids[i]), paid);
					}
				} else if (request.getParameter("__metricsAll") != null) {
					for (Integer id : customerIds) {
						this.webJaguar.updatePaid(id, paid);
					}
				}
			}
			
			String medium = ServletRequestUtils.getStringParameter(request, "__medium");
			if(medium!=null && medium.length()>0){
				if ((ids != null) && (request.getParameter("__metricsAll") == null)) {
					for (int i = 0; i < ids.length; i++) {
						this.webJaguar.updateMedium(Integer.parseInt(ids[i]), medium);
					}
				} else if (request.getParameter("__metricsAll") != null) {
					for (Integer id : customerIds) {
						this.webJaguar.updateMedium(id, medium);
					}
				}
			}
			
			String languageField = ServletRequestUtils.getStringParameter(request, "__language_field");
			if(languageField!=null && languageField.length()>0){
				if ((ids != null) && (request.getParameter("__metricsAll") == null)) {
					for (int i = 0; i < ids.length; i++) {
						this.webJaguar.updateLanguageField(Integer.parseInt(ids[i]), languageField);
					}
				} else if (request.getParameter("__metricsAll") != null) {
					for (Integer id : customerIds) {
						this.webJaguar.updateLanguageField(id, languageField);
					}
				}
			}
		}
		
		//System.out.println("Customer List Line 190  : "+((new Date().getTime() - d1.getTime()) / 1000));
		if (request.getParameter("__updateTrackCodePacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			String trackCode = ServletRequestUtils.getStringParameter(request, "__track_code");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.updateTrackCode(Integer.parseInt(ids[i]), trackCode);
					this.webJaguar.updateCrmTrackCode(Integer.parseInt(ids[i]), trackCode);
				}
			}
		}
		
		//System.out.println("Customer List Line 201  : "+((new Date().getTime() - d1.getTime()) / 1000));
		//update Main Source
		if (request.getParameter("__updateMainSourcePacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			String mainSource = ServletRequestUtils.getStringParameter(request, "__main_source");
			
			if ((ids != null) && (request.getParameter("__mainAssignAll") == null)) {
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.updateMainSource(Integer.parseInt(ids[i]), mainSource);
				}
			} else if (request.getParameter("__mainAssignAll") != null) {
				for (Integer id : customerIds) {
					this.webJaguar.updateMainSource(id, mainSource);
				}
			}
		}
		
		//System.out.println("Customer List Line 218  : "+((new Date().getTime() - d1.getTime()) / 1000));
		//update paid field
		if (request.getParameter("__updatePaidPacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			String paid = ServletRequestUtils.getStringParameter(request, "__paid");
			if ((ids != null) && (request.getParameter("__paidAssignAll") == null)) {
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.updatePaid(Integer.parseInt(ids[i]), paid);
				}
			} else if (request.getParameter("__paidAssignAll") != null) {
				for (Integer id : customerIds) {
					this.webJaguar.updatePaid(id, paid);
				}
			}
		}
		
		//System.out.println("Customer List Line 234  : "+((new Date().getTime() - d1.getTime()) / 1000));
		//update Medium
		if (request.getParameter("__updateMediumPacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			String medium = ServletRequestUtils.getStringParameter(request, "__medium");

			if ((ids != null) && (request.getParameter("__mediumAssignAll") == null)) {
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.updateMedium(Integer.parseInt(ids[i]), medium);
				}
			} else if (request.getParameter("__mediumAssignAll") != null) {
				for (Integer id : customerIds) {
					this.webJaguar.updateMedium(id, medium);
				}
			}
		}
		
		//System.out.println("Customer List Line 251  : "+((new Date().getTime() - d1.getTime()) / 1000));
		//update Language Field
		if (request.getParameter("__updateLanguageFieldPacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			String languageField = ServletRequestUtils.getStringParameter(request, "__language_field");

			if ((ids != null) && (request.getParameter("__languageAssignAll") == null)) {
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.updateLanguageField(Integer.parseInt(ids[i]), languageField);
				}
			} else if (request.getParameter("__languageAssignAll") != null) {
				for (Integer id : customerIds) {
					this.webJaguar.updateLanguageField(id, languageField);
				}
			}
		}
		
		
		//System.out.println("Customer List Line 269  : "+((new Date().getTime() - d1.getTime()) / 1000));
		//print sold lable
		if(request.getParameter("__printSoldLabel") != null){
			String ids[] = request.getParameterValues("__selected_id");
			//System.out.println("link "+ids[0]);
			Customer customer = this.webJaguar.getCustomerById(Integer.valueOf(ids[0]));
			if(customer != null){
				return new ModelAndView("../../touchPrintLabel.jhtm?__cardId="+customer.getCardId()+"&__printPdf=true");
			}	
		}
		
		if (!(Boolean) gSiteConfig.get("gGROUP_CUSTOMER")) {
			customerSearch.setGroupId(null);
		} else {
			// for getting only active Customer groups
			CustomerGroupSearch customerGroupSearch = new CustomerGroupSearch();
			customerGroupSearch.setActive("1");
			model.put("groups", this.webJaguar.getCustomerGroupList(customerGroupSearch));
		}
		if (customerSearch.getNumOrder() != null || customerSearch.getTotalOrder() != null) {
			customerSearch.setBaseOnOrder(true);
		}
		
		//System.out.println("Customer List Line 292  : "+((new Date().getTime() - d1.getTime()) / 1000));
		// filter by zipcode & distance		
		if(customerSearch.getLocationSearch() != null && customerSearch.getLocationSearch().getRadius() != -1 
				&& customerSearch.getLocationSearch().getZipcode() != null && customerSearch.getLocationSearch().getZipcode().length() > 0){
			ZipCode zipObject = this.webJaguar.getZipCode( customerSearch.getLocationSearch().getZipcode() );
			if ( zipObject != null ) {
				customerSearch.getLocationSearch().setZipcodeObject( zipObject );
				List<ZipCode> zipcodeList = this.webJaguar.getZipCodeList( customerSearch.getLocationSearch() );
				customerSearch.getLocationSearch().setZipcodeSearchList(zipcodeList);
		}
		}
		
		// get customers
		//System.out.println("Customer List Line 305  : "+((new Date().getTime() - d1.getTime()) / 1000));
		int count = this.webJaguar.getCustomerListWithDateCount(customerSearch);
		if (count < customerSearch.getOffset() - 1) {
			customerSearch.setPage(1);
		}
		customerSearch.setLimit(customerSearch.getPageSize());
		customerSearch.setOffset((customerSearch.getPage() - 1) * customerSearch.getPageSize());
		
		
		//System.out.println("Customer List Line 314  : "+((new Date().getTime() - d1.getTime()) / 1000));
		List<Customer> custList = this.webJaguar.getCustomerListWithDate(customerSearch);
		

		
		//System.out.println("Customer List Line 319  : "+((new Date().getTime() - d1.getTime()) / 1000));
		int pageCount = count / customerSearch.getPageSize();
		if (count % customerSearch.getPageSize() > 0) {
			pageCount++;
		}
		model.put("pageCount", pageCount);
		model.put("pageEnd", customerSearch.getOffset() + custList.size());
		model.put("count", count);
		if (customerSearch.getParent() != null) {
			model.put("familyTree", this.webJaguar.getFamilyTree(customerSearch.getParent(), "up"));
		}

		//System.out.println("Customer List Line 331  : "+((new Date().getTime() - d1.getTime()) / 1000));
		if (siteConfig.get("PRODUCT_QUOTE").getValue().equals("true") || (Boolean) gSiteConfig.get("gSHIPPING_QUOTE")) {
			Map<Integer, Customer> customerQuoteMap = new HashMap<Integer, Customer>();
			customerQuoteMap = this.webJaguar.getQuoteTotalByUserIDPerPage(custList);
			model.put("customerQuoteMap", customerQuoteMap);
		}

		if ((Boolean) gSiteConfig.get("gVIRTUAL_BANK_ACCOUNT")) {
			// Supplier Due Map
			List<VirtualBank> userIdList = new ArrayList<VirtualBank>();
			for (Customer customer : custList) {
				VirtualBank vb = new VirtualBank();
				vb.setUserId(customer.getId());
				vb.setSupplierId(customer.getSupplierId());
				userIdList.add(vb);
			}
			// CRASHED THE SITE DUE TO MYSQL BIG QUERY
			if (userIdList.size() > 0) {
				Map<Integer, Double> supplierPayMap = this.webJaguar.getPaymentsTotalToCustomerMap(userIdList);
				for (Customer customer : custList) {
					if (supplierPayMap.get((long) customer.getId()) != null) {
						customer.setSupplierPay(supplierPayMap.get((long) customer.getId()));
					}
				}
			}
		}
		//System.out.println("Customer List Line 357  : "+((new Date().getTime() - d1.getTime()) / 1000));
		
		model.put("loggedIn", customerSearch.getLoggedIn());
		model.put("customers", custList);
		model.put("customerFieldList", this.webJaguar.getCustomerFields());	
		model.put("countries", this.webJaguar.getEnabledCountryList());
		model.put("region", this.webJaguar.getRegion());
		model.put("nationalRegion", this.webJaguar.getNationalRegionList());
		model.put("userList", this.webJaguar.getUserPrivilegeList(null) );

		
		//System.out.println("Customer List Line 366  : "+((new Date().getTime() - d1.getTime()) / 1000));
		model.put("languageCodes", this.webJaguar.getLanguageSettings());	
		if (siteConfig.get("CUSTOMIZE_CUSTOMER_LIST").getValue().contains("echoSign")) {
			model.put("echoSign", this.webJaguar.getLatestEchoSignMap());
		}

		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/customers/list", "model", model);
		}
		//System.out.println("Customer List Line 372  : "+((new Date().getTime() - d1.getTime()) / 1000));
		if(type != null && type.equals("list2")){
			return new ModelAndView("admin/customers/list2", "model", model);
		}else{
			return new ModelAndView("admin/customers/list", "model", model);
		}
	}
	
	private void notifySalesRep(Map<String, Configuration> siteConfig, List<Integer> userIds) {
		
		for(Integer id: userIds){			
			Customer customer = this.webJaguar.getCustomerById(id);
			AccessUser accessUser = this.webJaguar.getAccessUserById(customer.getAccessUserId());
	    	SalesRep salesRep = this.webJaguar.getSalesRepById( accessUser.getSalesRepId() );
	    	StringBuffer message = new StringBuffer();		
	    	EmailMessageForm form = new EmailMessageForm();
	    	form.setHtml(false);
	    	form.setSubject("Account Manager Notification");
	    	int messageId;
	    	if(siteConfig.get( "SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION" ).getValue()!=null){
	    		messageId= Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION" ).getValue() );
	    	}else{
	    		messageId=0;       	
	    	}
	    	SiteMessage siteMessage = this.webJaguar.getSiteMessageById( messageId );
			if(	siteMessage!=null&&!siteMessage.equals("")){
				
		    	if(siteMessage.isHtml()==true){
		    		form.setHtml(true);
		    	}
		    	message.append(siteMessage.getMessage());
		    	form.setSubject(siteMessage.getMessageName());
			}else{
				message.append("Hello #salesRepName#");
				message.append(",\n\n");
				message.append("This is to inform you that a new Contact #firstname# is assigned to you. \n\n");
				message.append("Dialing Notes: #dialingNotes# \n");
				message.append("Thanks \n");
				
			}
			form.setMessage(message.toString());
			if(salesRep!=null){
				form.setTo(salesRep.getEmail());
			}else{
				form.setTo(accessUser.getEmail());
			}
			form.setFrom(siteConfig.get("CONTACT_EMAIL").getValue());
			try {
		    	// construct email message
		       	MimeMessage mms = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
				if(salesRep!=null){
					this.webJaguar.replaceDynamicElement(form, customer, salesRep);
				}else{
					this.webJaguar.replaceDynamicElement(form, customer, accessUser);
				}
				helper.setTo(form.getTo());
				helper.setFrom(form.getFrom());
				helper.setSubject(form.getSubject());
				if ( form.isHtml()==true ) { 
		    		helper.setText(form.getMessage(), true );
		    	} else {
		    		helper.setText(form.getMessage());
		    	}
				mailSender.send(mms);
			} catch (Exception ex) {			
					ex.printStackTrace();					
			}  		
		}
		
		
	}

}