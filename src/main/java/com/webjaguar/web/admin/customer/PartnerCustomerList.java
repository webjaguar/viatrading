package com.webjaguar.web.admin.customer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerBudgetPartner;

public class PartnerCustomerList implements Controller{
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();
		
		// check if add button was pressed 
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("partnerCustomerForm.jhtm?cId="+request.getParameter("cId")));				
		}
		
		Customer customer = this.webJaguar.getCustomerById(ServletRequestUtils.getIntParameter(request, "cId", 0));
		if (customer != null) {
			model.put("customer", customer);
		} else {
			return new ModelAndView(new RedirectView("customerList.jhtm"));			
		}
		
		if(request.getParameter("cId") != null){
			PagedListHolder partnerList = 
				new PagedListHolder(this.webJaguar.getPartnersListByCustomerId(Integer.parseInt(request.getParameter("cId")), true));
			partnerList.setPageSize(10);
			String page = request.getParameter("page");
			if (page != null) {
				partnerList.setPage(Integer.parseInt(page)-1);
			}
			
			model.put("partnerList", partnerList);
			model.put("cId", request.getParameter("cId"));
		}
		return new ModelAndView("admin/customers/partnerCustomerList", model);
	}

}
