package com.webjaguar.web.admin.customer;


import java.util.Date;
import java.util.List;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.logic.interfaces.Credit;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerCreditHistory;
import com.webjaguar.model.GiftCard;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;

public class CreditImp implements Credit{
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	// credit transactions when an order is placed using credit OR 'CREDIT' Payment made OR 'CREDIT' Payment cancelled.
	public CustomerCreditHistory updateCustomerCreditByOrderPayment(Order order, String paymentType, Double amount, String userName) {
		CustomerCreditHistory customerCredit = new CustomerCreditHistory();
		
		if(paymentType.equalsIgnoreCase("payment")) { // if payment is made
			customerCredit.setCredit((-1) * amount);
			customerCredit.setReference("Order Payment");		
		} else { // if payment is cancelled
			customerCredit.setCredit(amount);
			customerCredit.setReference("Order Payment Cancelled");
		}	
		customerCredit.setDate(new Date());
		customerCredit.setUserName(userName);
		customerCredit.setTransactionId(Integer.toString(order.getOrderId()));
		customerCredit.setUserId(order.getUserId());	
		
		return customerCredit;
	}
	
	
	// credit transaction when VBA payment is made using Payment Method as 'CREDIT' or when VBA payment and Payment type is 'CREDIT' and is cancelled.
	public CustomerCreditHistory updateCustomerCreditByVBAPayment(LineItem lineItem, String paymentType, String userName) {
		CustomerCreditHistory customerCredit = new CustomerCreditHistory();
		
		customerCredit.setCredit(lineItem.getVbaAmountToPay());
		customerCredit.setTransactionId(lineItem.getVbaTransactionId());
		if(paymentType.equalsIgnoreCase("payment")) { // if VBA payment is made
			customerCredit.setReference("VBA Payment");			
		} else { // if VBA payment is cancelled
			customerCredit.setReference("VBA Payment Cancelled");		
		}
		customerCredit.setDate(lineItem.getVbaDateOfPay());	
		customerCredit.setUserName(userName);
		customerCredit.setUserId(lineItem.getSupplier().getUserId());
		
		return customerCredit;
	}
	
	// credit transaction when Affiliate payment is made using Payment Method as 'CREDIT' or when Affiliate payment and Payment type is 'CREDIT' and is cancelled.
	public CustomerCreditHistory updateCustomerCreditByAffiliatePayment(Order order, String paymentType, String userName) {
		CustomerCreditHistory customerCredit = new CustomerCreditHistory();
		
		customerCredit.setCredit(order.getVbaAmountToPay());
		customerCredit.setTransactionId(order.getVbaTransactionId());		
		if(paymentType.equalsIgnoreCase("payment")) { // if Affiliate payment is made
			customerCredit.setReference("Affiliate Payment");
		} else { // if Affiliate payment is cancelled
			customerCredit.setReference("Affiliate Payment Cancel");
		}	
		customerCredit.setDate(order.getVbaDateOfPay());
		customerCredit.setUserName(userName);
		customerCredit.setUserId(order.getAffiliate().getAffiliateLevel1Id()); // Need to work for Affiliate2
		
		return customerCredit;
	}
	
	// When a Gift Card is redeemed credit will be updated.
	public CustomerCreditHistory updateCustomerCreditByGiftCard(GiftCard giftCard, Integer usreId) {
		CustomerCreditHistory customerCredit = new CustomerCreditHistory();
		
		customerCredit.setCredit(giftCard.getAmount());
		customerCredit.setTransactionId(giftCard.getCode());
		customerCredit.setReference("Gift Card");
		customerCredit.setUserId(usreId);	
		customerCredit.setDate(new Date());
		customerCredit.setUserName(giftCard.getRedeemedByFirstName()+" "+giftCard.getRedeemedByLastName());
		return customerCredit;
	}
	
	// When loyalty threshold is reached credit is updated. 
	public CustomerCreditHistory updateCustomerCreditByLoyalty(Order order, Customer customer, String userName) {
		CustomerCreditHistory customerCredit = new CustomerCreditHistory();
		
	    Integer pointsByThreshold = new Integer( (int) ((order.getTotalLoyaltyPoint() + customer.getLoyaltyPoint()) / ((customer.getPointThreshold() == null) ? 1 : customer.getPointThreshold())));

		customerCredit.setDate(new Date());
		customerCredit.setCredit(pointsByThreshold * ((customer.getPointThreshold() == null) ? 1.0 : customer.getPointThreshold().doubleValue()));
		customerCredit.setTransactionId(Integer.toString(order.getOrderId()));
		customerCredit.setReference("Loyalty");
		customerCredit.setUserId(customer.getId());	
		customerCredit.setUserName(userName);
		
		return customerCredit;
	}
	
}
