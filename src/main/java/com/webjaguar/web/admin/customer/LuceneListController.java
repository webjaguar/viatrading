/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 */

package com.webjaguar.web.admin.customer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TopFieldDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.LuceneCustomerSearch;

public class LuceneListController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	
	private IndexSearcher searcher;
	private QueryParser qp;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("addCustomer.jhtm"));
		}
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();

		this.getCustomersPerPage(siteConfig, gSiteConfig, request, model);
		
		return new ModelAndView("admin/customers/luceneList", "model", model);
		
	}
	
	private void getCustomersPerPage(Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig, HttpServletRequest request, Map<String, Object> model) throws Exception, IOException{
		
		
		LuceneCustomerSearch search = getCustomerSearch(request);
		
		List<Customer> results = new ArrayList<Customer>();
		

		StringBuffer keywords = new StringBuffer();
		TopFieldDocs docs = this.getTopFieldDocs(keywords, siteConfig, request);
			
		if (docs != null) {
			ScoreDoc[] hits = docs.scoreDocs;
			
			int count = hits.length;
			int start = (search.getPage() - 1) * search.getPageSize();
			if (start > count) {
				search.setPage(1);
				start = 0;
			}
			int end = start + search.getPageSize();
			if (end > count) {
				end = count;
			}

			int pageCount = count / search.getPageSize();
			if (count % search.getPageSize() > 0) {
				pageCount++;
			}

			model.put("start", start);
			model.put("pageCount", pageCount);
			model.put("count", count);
			
			for (int i = start; i < end; i++) {
				Document doc = searcher.doc(hits[i].doc);
				
				//Customer customer = this.webJaguar
				//Product product = this.webJaguar.getProductByIdForLuceneListOnAdmin(new Integer(doc.get("id")));
				
				Customer customer = this.webJaguar.getCustomerById(new Integer(doc.get("id")));
				if (customer != null) {
					
					results.add(customer);
				}
			}
			model.put("pageEnd", start + results.size());
			
			model.put("customers", results);
		}
	} 
	
	private LuceneCustomerSearch getCustomerSearch(HttpServletRequest request) {
		LuceneCustomerSearch search = (LuceneCustomerSearch) WebUtils.getSessionAttribute(request, "lCustomerSearch");
		if (search == null) {
			Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
			search = new LuceneCustomerSearch();
			search.setSort(siteConfig.get("SEARCH_SORT").getValue());
			// On Admin DEFAULT_LIST_SIZE should be used instead of NUMBER_PRODUCT_PER_PAGE_SEARCH
			search.setPageSize(new Integer(siteConfig.get("DEFAULT_LIST_SIZE").getValue()));
			request.getSession().setAttribute("lCustomerSearch", search);
		}

		if (request.getParameter("keywords") != null) {
			search.setKeywords(ServletRequestUtils.getStringParameter(request, "keywords", "").replace("*", ""));

		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);
			}
		}

		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				search.setPageSize(10);
			} else {
				search.setPageSize(size);
			}
		}		
		return search;
	}
	
	private TopFieldDocs getTopFieldDocs(StringBuffer keywords, Map<String, Configuration> siteConfig, HttpServletRequest request) throws Exception, IOException{
		
		
		LuceneCustomerSearch search = getCustomerSearch(request);

	
			DirectoryReader reader = DirectoryReader.open(FSDirectory.open(new File(getServletContext().getRealPath("/lucene_index/customerIndexing/customers"))));
			searcher = new IndexSearcher(reader);

			keywords = new StringBuffer();

			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_41);
			qp = new QueryParser(Version.LUCENE_41, "keywords", analyzer);
			

			if (search.getKeywords() != null && search.getKeywords().trim().length() > 0) {
				String[] searchPhrases = search.getKeywords().split("\\|\\|");
				
				keywords.append("("+ searchPhrases[0] +")");
				for (String searchPhrase : searchPhrases) {
					keywords.append("all_fields_data :" + searchPhrase +"*");
				}
			}

			// Use || to search multiple search phrases
			// For example, /lsearch.jhtm?keywords=Red+Pen||Red+Pencil will search for "Red Pen" OR "Red Pencil"
			/*
			if (search.getKeywords() != null && search.getKeywords().trim().length() > 0) {
				String[] searchPhrases = search.getKeywords().split("\\|\\|");

				// search fields
				if (siteConfig.get("LUCENE_GROUP_FIELDS_INTO_ONE").getValue().equals("true")) {
					keywords.append("( \"" + searchPhrases[0] + "\"~" + proximity);
					for (String searchPhrase : searchPhrases) {
						keywords.append(" OR all_fields_data :\"" + searchPhrase + "\"~" + proximity);
					}
				} else {
					keywords.append("( \"" + searchPhrases[0] + "\"~" + proximity);
					for (String searchPhrase : searchPhrases) {
						keywords.append(" OR name :\"" + searchPhrase + "\"~" + proximity);
					}
		
					String[] searchFieldsList = siteConfig.get("SEARCH_FIELDS").getValue() != null ? siteConfig.get("SEARCH_FIELDS").getValue().split(",") : null;
					if (searchFieldsList != null) {
						for (String field : searchFieldsList) {
							if (!field.isEmpty() && !(field.equalsIgnoreCase("id") || field.equalsIgnoreCase("name") || field.equalsIgnoreCase("search_rank"))) {
								for (String searchPhrase : searchPhrases) {
									keywords.append(" OR " + field + " :\"" + searchPhrase + "\"~" + proximity);
								}
							}
						}
					}
				}
				// complete query bracket
				keywords.append(")");
			}
			*/
			Query query = null;
			TopFieldDocs docs = null;
			
			try {
				if(!keywords.toString().trim().isEmpty()) {
					query = qp.parse(keywords.toString());
				} else {
					query = new MatchAllDocsQuery();
				}
				SortField sfName = new SortField("username", SortField.Type.STRING, false);
				Sort sort = new Sort(sfName);
				docs = searcher.search(query, null, Integer.MAX_VALUE, sort);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		return docs;
		
	}
}