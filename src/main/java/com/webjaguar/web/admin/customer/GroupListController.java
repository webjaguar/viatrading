/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.customer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CustomerGroup;
import com.webjaguar.model.CustomerGroupSearch;

public class GroupListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		if (!(Boolean) gSiteConfig.get("gGROUP_CUSTOMER")) {
			return new ModelAndView(new RedirectView("customerList.jhtm"));		
		}
		
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("customerGroup.jhtm"));		
		}
		
		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {					
						this.webJaguar.deleteCustomerGroupById(Integer.parseInt(ids[i]));		
				}
			}
		}
		
		// search criteria
		CustomerGroupSearch search = getCustomerGroupSearch( request );   	
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		PagedListHolder groupList = new PagedListHolder( this.webJaguar.getCustomerGroupList( search ));

		groupList.setPageSize(search.getPageSize());
		groupList.setPage(search.getPage()-1);

		myModel.put( "groups", groupList );
		myModel.put( "groupList", groupList.getPageList() );
		myModel.put( "search", search );
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null) { 
		   return new ModelAndView( "admin-responsive/customers/groups", "model", myModel );
		}
		return new ModelAndView( "admin/customers/groups", "model", myModel );
	}
    
	private CustomerGroupSearch getCustomerGroupSearch(HttpServletRequest request) {
		CustomerGroupSearch customerGroupSearch = (CustomerGroupSearch) request.getSession().getAttribute( "groupCustomerSearch" );
		if (customerGroupSearch == null) {
			customerGroupSearch = new CustomerGroupSearch();
			request.getSession().setAttribute( "groupCustomerSearch", customerGroupSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			customerGroupSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// group name
		if (request.getParameter("group") != null) {
			customerGroupSearch.setGroup(ServletRequestUtils.getStringParameter( request, "group", "" ));
		}
		
		//active
		if (request.getParameter("active") != null) {
			customerGroupSearch.setActive( ServletRequestUtils.getStringParameter( request, "active", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				customerGroupSearch.setPage( 1 );
			} else {
				customerGroupSearch.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				customerGroupSearch.setPageSize( 10 );
			} else {
				customerGroupSearch.setPageSize( size );				
			}
		}
		
		return customerGroupSearch;
	}		
}