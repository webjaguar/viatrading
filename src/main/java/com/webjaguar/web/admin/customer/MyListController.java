/*
 * Copyright 2005, 2007 Advanced E-Media Solutions
 */

package com.webjaguar.web.admin.customer;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.MyListSearch;
/*
 * 
 */
public class MyListController extends AbstractCommandController
{

	public MyListController()
	{
		setCommandClass( MyListSearch.class );
	}

	private WebJaguarFacade webJaguar;

	public ModelAndView handle( HttpServletRequest request, HttpServletResponse response, Object command, BindException errors ) throws Exception
	{
    	MyListSearch search = (MyListSearch) command;
    	String message = null;
    	PagedListHolder myList = new PagedListHolder();
		Map<String, Object> myModel = new HashMap<String, Object>();

		Integer userId = org.springframework.web.bind.ServletRequestUtils.getIntParameter( request, "id", -1 );
        
		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				this.webJaguar.removeFromList(ids, userId);
			}
		}
		
        // if add skus
		if ( request.getParameter( "__add" ) != null )
		{
			return new org.springframework.web.servlet.ModelAndView( new org.springframework.web.servlet.view.RedirectView( "addMyList.jhtm?id=" + userId ) );
		}
		
		// get my List
		if ( userId == -1 ) { 
			myModel.put( "message", "customer.exception.notfound" );
		} else
			myList = new PagedListHolder( this.webJaguar.getShoppingListByUserid( userId, 0, request ) );
		String size = request.getParameter( "size" );
		if ( size != null )
		{
			try
			{
				request.getSession().setAttribute( "myListSize", Integer.parseInt( size ) );
			}
			catch ( NumberFormatException e )
			{
				request.getSession().setAttribute( "myListSize", 10 );
			}
		}
		if ( request.getSession().getAttribute( "myListSize" ) == null )
		{
			myList.setPageSize( 10 );
		}
		else
		{
			myList.setPageSize( (Integer) request.getSession().getAttribute( "myListSize" ) );
		}
		String page = request.getParameter( "page" );
		if ( page != null )
		{
			myList.setPage( Integer.parseInt( page ) - 1 );
		}
		
		myModel.put( "products", myList );
		myModel.put( "userId", userId );

		return new ModelAndView( "admin/customers/myList", "model", myModel );
	}

	public void setWebJaguar( WebJaguarFacade webJaguar )
	{
		this.webJaguar = webJaguar;
	}

}