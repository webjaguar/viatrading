/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 */

package com.webjaguar.web.admin.customer;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.listener.SessionListener;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SalesRepSearch;
import com.webjaguar.model.UserSession;
import com.webjaguar.model.VirtualBank;
import com.webjaguar.model.ZipCode;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.web.form.AddressForm;

public class CustomerAddressListController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar; 
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	public CustomerAddressListController() {
		setSessionForm(true);
		setCommandName("addressForm");
		setCommandClass(AddressForm.class);
		setFormView("/admin/customers/customerAddressList");
		setSuccessView("customer.jhtm");
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
     	AddressForm addressForm = (AddressForm) command;  	
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			if(!addressForm.getAddress().isPrimary()){
				this.webJaguar.deleteAddress(addressForm.getAddress());
			}
			return new ModelAndView(new RedirectView(getSuccessView()+"?id="+addressForm.getAddress().getUserId()));
		}     	
     	
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()+"?id="+addressForm.getAddress().getUserId()));
		}    	
     	
		// set as primary address
		if (addressForm.isSetAsPrimary()) {
			addressForm.getAddress().setPrimary(true);
		}
		
		if(request.getParameter("userid")!=null)
			addressForm.getAddress().setUserId(Integer.valueOf(request.getParameter("userid").toString()));
			
    	if (addressForm.isNewAddress()) {
    		this.webJaguar.insertAddress(addressForm.getAddress());
    	} else {
    		this.webJaguar.updateAddress(addressForm.getAddress());
    		Customer customer = this.webJaguar.getCustomerById(addressForm.getAddress().getUserId());
    		if( (Boolean) gSiteConfig.get("gCRM") && customer.getCrmContactId() != null && addressForm.getAddress().isPrimary()) {
    			CrmContact crmContact = this.webJaguar.getCrmContactById( customer.getCrmContactId() );
    			if(crmContact != null) {
    				try {
    					this.webJaguar.customerToCrmContactSynch(crmContact, customer);
    				} catch (Exception e) { e.printStackTrace();}
    				this.webJaguar.updateCrmContact(crmContact);
    			} 
    		}
    	}
		return new ModelAndView(new RedirectView(getSuccessView()+"?id="+addressForm.getAddress().getUserId()));    	
    }

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}    
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) {	
		if (request.getParameter("setAsPrimary") == null) {
	     	AddressForm addressForm = (AddressForm) command;
	     	addressForm.setSetAsPrimary(false);
		}
	}
			
    protected Map referenceData(HttpServletRequest request) 
			throws Exception {
	
       	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();
	    myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
	    // get a list of enabled country code from database
	    myModel.put("countrylist",this.webJaguar.getEnabledCountryList());
        myModel.put("statelist",this.webJaguar.getStateList("US"));
		myModel.put("caProvinceList", this.webJaguar.getStateList("CA"));  
	   
	    
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		if (request.getParameter("type") != null && request.getParameter("type").toString().equalsIgnoreCase("edit")){
			myModel.put("type", "edit"); 
		}else{
			myModel.put("type", "add");  
		}
		
		map.put("model", myModel);  
	    return map;
    }  
    
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
    	UserSession userSession = this.webJaguar.getUserSession(request);	
		AddressForm addressForm = new AddressForm();
		
		if ( request.getParameter("id") != null ) {
			Integer addressId = new Integer(request.getParameter("id"));
			Address address = this.webJaguar.getAddressById(addressId);
			if (address == null) {
				Map<String, Object> myModel = new HashMap<String, Object>();
				myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
				myModel.put("message", "Address does not exist.");
				ModelAndView modelAndView = new ModelAndView("frontend/error");
				modelAndView.addObject("model", myModel);
				throw new ModelAndViewDefiningException(modelAndView);				
			}
			addressForm = new AddressForm(address);
			if (addressForm.getAddress()!=null && addressForm.getAddress().getStateProvince()!=null && addressForm.getAddress().getStateProvince().trim().equals("")) {
				addressForm.getAddress().setStateProvinceNA(true);
			}
		}
		if ( siteConfig.get( "CUSTOMERS_REQUIRED_COMPANY" ).getValue().equals( "true" ) ) {
			addressForm.setCompanyRequired( true );
		}
		
		return addressForm;
	}
}
