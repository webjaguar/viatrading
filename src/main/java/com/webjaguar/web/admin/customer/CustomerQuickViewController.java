/*
 * Copyright 2005, 2008 Advanced E-Media Solutions
 */

package com.webjaguar.web.admin.customer;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.Report;
import com.webjaguar.model.SalesRep;

public class CustomerQuickViewController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar( WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			Iterator<SalesRep> iter = this.webJaguar.getSalesRepList().iterator();
			while (iter.hasNext()) {
				SalesRep salesRep = iter.next();
				salesRepMap.put(salesRep.getId(), salesRep);
			}
			model.put("salesRepMap", salesRepMap);
		}
		// get customer
		Integer userId = ServletRequestUtils.getIntParameter( request, "cid", -1 );
		Customer customer = this.webJaguar.getCustomerById( userId );
		// get customers
		PagedListHolder itemsPurchasedList = new PagedListHolder( this.webJaguar.getCustomerQuickViewList( userId ) );
		if ( request.getParameter( "page" ) != null ) {
			itemsPurchasedList.setPage( Integer.parseInt( request.getParameter( "page" ) ) - 1 );
		}
		itemsPurchasedList.setPageSize( 20 );
		
		List<Report> customerPurchase = new ArrayList();
		List<Report> customerPurchaseTwoYears = new ArrayList();
		customerPurchaseTwoYears = this.webJaguar.getCustomerQuickViewPurchaseLastYear( userId );
		customerPurchase = this.webJaguar.getCustomerQuickViewPurchase( userId );
		customerPurchaseTwoYears.addAll(customerPurchase);
		Double maxValue = 0.0;
		Double minValue = Double.MAX_VALUE;
		for ( Report report : customerPurchaseTwoYears ){
			if ( report.getGrandTotal() > maxValue )
				maxValue = report.getGrandTotal();
			if ( report.getGrandTotal() < minValue )
				minValue = report.getGrandTotal();
		}
		
		// Customer Fields
		Class<Customer> c = Customer.class;
		Method m = null;
		Object arglist[] = null;
		for ( CustomerField customerField : this.webJaguar.getCustomerFields() ) {
			if ( customerField.isEnabled() ) {
				m = c.getMethod( "getField" + customerField.getId() );
				customerField.setValue( (String) m.invoke( customer, arglist ) );
				if ( customerField.getValue() != null && customerField.getValue().length() > 0 ) {
					customer.addCustomerField( customerField );
				}
			}
		}
		
		model.put( "customer", customer );
		model.put( "itemsPurchasedList", itemsPurchasedList );
		model.put( "current", new Date() );
		Date now = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy");
		String nowYear=dateFormatter.format(now);
		int lastYear=Integer.valueOf(nowYear)-1;
		model.put( "lastYear", lastYear );
		model.put( "customerPurchaseList", customerPurchaseTwoYears );
		model.put( "customerPurchase", textEncode(customerPurchaseTwoYears, maxValue, minValue) );
		
		return new ModelAndView( "admin/customers/quickView", "model", model );
	}
	
	private String[] textEncode(List<Report> valueArray, Double maxValue, Double minValue) {
		StringBuffer chartData = new StringBuffer("t:");
		String[] data = {"",""};
		int count=0;
		for ( Report report : valueArray ){
			chartData.append( getPercentages(report.getGrandTotal(), maxValue ) ); 
			if ( report.getMonth() < 12 )
		    	chartData.append( "," );
			if(report.getMonth()==12&&count==0){
				chartData.append(",");
				count++;
			}
		}		
		data[0] = chartData.toString();
		data[1] = "|0|" + maxValue;
		//data[1] = "|" + 0 + "|" + Math.rint( 100*1/4 ) + "|" + Math.rint( 100*1/2 ) + "|" + Math.rint( 100*3/4 ) + "|" + Math.rint( 100 );
		return data;
	}
	
	private String getPercentages(Double value, Double maxValue) {
		return  "" + Math.rint( 100 * value / maxValue );
	}

}