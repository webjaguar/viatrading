/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.customer;

import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.web.form.AddressForm;

public class AddressFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar; 
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public AddressFormController() {
		setSessionForm(true);
		setCommandName("addressForm");
		setCommandClass(AddressForm.class);
		setFormView("admin/customers/addressForm");
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
     	AddressForm addressForm = (AddressForm) command;
     	Integer userId = new Integer(request.getParameter("uid"));
		addressForm.getAddress().setUserId(userId);
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteAddress(addressForm.getAddress());
			return new ModelAndView(new RedirectView(getSuccessView()));
		}     	
     	
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}    	
     	
		// set as primary address
		if (addressForm.isSetAsPrimary()) {
			addressForm.getAddress().setPrimary(true);
		}
	
		if (addressForm.isNewAddress()) {
			this.webJaguar.insertAddress(addressForm.getAddress());
		} else {
			this.webJaguar.updateAddress(addressForm.getAddress());
		}
		
		return new ModelAndView(new RedirectView("addressList.jhtm?id=" + userId));  
		
    }

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}    
			
    protected Map referenceData(HttpServletRequest request) 
			throws Exception {
    	Integer userId = new Integer(request.getParameter("uid"));
		Customer customer = this.webJaguar.getCustomerById(userId);   
		
       	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();
	    // get a list of enabled country code from database
	    myModel.put("countries",this.webJaguar.getEnabledCountryList());
        myModel.put("states",this.webJaguar.getStateList("US"));
		myModel.put("caProvinceList", this.webJaguar.getStateList("CA"));  
		myModel.put("firstName",customer.getAddress().getFirstName());
		myModel.put("lastName",customer.getAddress().getLastName());
	    map.put("model", myModel);  
	    return map;
    }  
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		AddressForm addressForm = new AddressForm();
		
		if ( request.getParameter("id") != null ) {
			Integer addressId = new Integer(request.getParameter("id"));
			Address address = this.webJaguar.getAddressById(addressId);
			addressForm = new AddressForm(address);
			if (addressForm.getAddress().getStateProvince().trim().equals("")) {
				addressForm.getAddress().setStateProvinceNA(true);
			}
		}
		if ( siteConfig.get( "CUSTOMERS_REQUIRED_COMPANY" ).getValue().equals( "true" ) ) {
			addressForm.setCompanyRequired( true );
		}
		return addressForm;
	}
}
