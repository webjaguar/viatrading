/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.customer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;

public class AddressListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	
    	Integer userId = new Integer(request.getParameter("id"));
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("addressForm.jhtm"), "uid", userId);		
		}		
		
		Customer customer = this.webJaguar.getCustomerById(userId);    	
		Map<String, Object> myModel = new HashMap<String, Object>();
    	
		
		if ( customer != null ) {
			// default address
			myModel.put("defaultAddress", this.webJaguar.getDefaultAddressByUserid(customer.getId()));
			// addresses
			PagedListHolder addressList = 
				new PagedListHolder(this.webJaguar.getAddressListByUserid( customer.getId() ));
			addressList.setPageSize(10);
			String page = request.getParameter("page");
			if (page != null) {
				addressList.setPage(Integer.parseInt(page)-1);
			}
			myModel.put("addresses", addressList);	
			myModel.put("firstName",customer.getAddress().getFirstName());
			myModel.put("lastName",customer.getAddress().getLastName());
			myModel.put("id", userId);	
		}
		return new ModelAndView("admin/customers/addresses", "model", myModel);    	
    }

}
