/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 06.08.2007
 */

package com.webjaguar.web.admin.customer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;

import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.Language;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.domain.Constants;

public class ExportController extends SimpleFormController {

	int gPRICE_TABLE = 0;
	int gPROTECTED = 0;
	boolean gTAX_EXEMPTION, gSALES_REP, gMASS_EMAIL,gSUB_ACCOUNTS,gBUDGET,gSHOPPING_CART,gPAYMENTS;
	int gAFFILIATE = 0, gMULTI_STORE = 0;
	Object gCUSTOMER_CATEGORY;
	List<Language> gI18N;
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	private MailSender mailSender;
    public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public ExportController() {
		setSessionForm(false);
		setCommandName("customerSearch");
		setCommandClass(CustomerSearch.class);
		setFormView("admin/customers/export");
	}
	
	private File baseFile;
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		CustomerSearch search = (CustomerSearch) command;
		if (search.getFileType().equals( "" )) {
			return showForm(request, response, errors, map);
		}
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	
		// check if new button was pressed
		
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			if (gSALES_REP) {
				// get sales rep
				Iterator<SalesRep> salesRepIter = this.webJaguar.getSalesRepList().iterator();
				while (salesRepIter.hasNext()) {
					SalesRep salesRep = salesRepIter.next();
					salesRepMap.put(salesRep.getId(), salesRep);
				}
			}
			Map <Integer, Customer> affiliateMap = null;
			if (gAFFILIATE > 0) {
				affiliateMap = new HashMap<Integer, Customer>();
				for (Customer affiliate : this.webJaguar.getAffiliateNameList()) {
					affiliateMap.put(affiliate.getId(), affiliate);
				}
			}
			if (baseFile.canWrite()) {
				File file[] = baseFile.listFiles();
				List<Map<String, Object>> exportedFiles = new ArrayList<Map<String, Object>>();
				for (int f=0; f<file.length; f++) {
					if (file[f].getName().startsWith("invoiceExport") & file[f].getName().endsWith(search.getFileType())) {
						file[f].delete();
					}
				}
				map.put("exportedFiles", exportedFiles);
				
				// set end date to end of day
				if (search.getEndDate() != null) {
					search.getEndDate().setTime(search.getEndDate().getTime() 
							+ 23*60*60*1000 // hours
							+ 59*60*1000 	// minutes
							+ 59*1000);		// seconds	
				}				
				
				int customerCount = this.webJaguar.customerCount(search);
				search.setLimit(null);
				if (search.getFileType().equals("xls")) {
					createExcelFiles(customerCount, search, siteConfig, exportedFiles, salesRepMap, affiliateMap);				
				} else if (search.getFileType().equals("csv") ){
					createCsvFile(customerCount, search, siteConfig, exportedFiles, salesRepMap, affiliateMap);		
				}
		        
				if (customerCount > 0) {
					map.put("arguments", customerCount);
					map.put("message", "customerexport.success");
					this.webJaguar.insertImportExportHistory( new ImportExportHistory("customer", SecurityContextHolder.getContext().getAuthentication().getName(), false ));
				} else {
					map.put("message", "customerexport.empty");				
				}
				
				notifyAdmin(siteConfig, request);
		}

		return showForm(request, response, errors, map);
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		CustomerSearch customerSearch = this.webJaguar.getCustomerSearch( request );
		customerSearch.setLimit( null );
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		gPRICE_TABLE = (Integer) gSiteConfig.get("gPRICE_TABLE");
		gPROTECTED = (Integer) gSiteConfig.get("gPROTECTED");
		gTAX_EXEMPTION = (Boolean) gSiteConfig.get("gTAX_EXEMPTION");
		gSALES_REP = (Boolean) gSiteConfig.get("gSALES_REP");
		gMASS_EMAIL = (Boolean) gSiteConfig.get("gMASS_EMAIL");
		gCUSTOMER_CATEGORY = gSiteConfig.get("gCUSTOMER_CATEGORY");
		gAFFILIATE = (Integer) gSiteConfig.get("gAFFILIATE");
		gMULTI_STORE = (Integer) gSiteConfig.get("gMULTI_STORE");
		gSUB_ACCOUNTS = (Boolean) gSiteConfig.get("gSUB_ACCOUNTS");
		gI18N = this.webJaguar.getLanguageSettings();
		gBUDGET = (Boolean) gSiteConfig.get("gBUDGET");
		gSHOPPING_CART = (Boolean) gSiteConfig.get("gSHOPPING_CART");
		gPAYMENTS = (Boolean) gSiteConfig.get("gPAYMENTS");
		String fileType = ServletRequestUtils.getStringParameter(request, "fileType2", "xls");
		customerSearch.setFileType(fileType);
		
		Map<String, Object> map = new HashMap<String, Object>();
		baseFile = new File(getServletContext().getRealPath("/admin/excel/"));

		File file[] = baseFile.listFiles();
		List<Map> exportedFiles = new ArrayList<Map>();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith("customerExport") & file[f].getName().endsWith("." + customerSearch.getFileType())) {
				if (request.getParameter("__new") != null) { // check if new button was pressed
					file[f].delete();
				} else {
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					exportedFiles.add( fileMap );
				}
			}
		}
		request.setAttribute("exportedFiles", exportedFiles);
		return customerSearch;
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder( request, binder );
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, true, 10 ) );		
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		CustomerSearch search = (CustomerSearch) command;
		// fileType2 coming from menu
		if (request.getParameter("fileType2") != null && request.getParameter("__new") == null) {
			search.setFileType(request.getParameter("fileType2"));
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> model1 = new HashMap<String, Object>();
		model1.put("countries",this.webJaguar.getEnabledCountryList());
		model1.put("customerFieldList", this.webJaguar.getCustomerFields());
		
		if (!baseFile.canWrite()) {
			if (search.getFileType().equals("xls")) {
				map.put("message", "excelfile.notWritable");						
			} else {
				map.put("message", "file.notWritable");										
			}
		}
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			map.put("salesRepList", this.webJaguar.getSalesRepList());
		}
		map.put( "model", model1 );
		return map;
	}
    
    private HSSFCell getCell(HSSFSheet sheet, int row, short col) {
    	return sheet.createRow(row).createCell(col);
    }
    
    private void setText(HSSFCell cell, String text) {
    	cell.setCellValue(text);
    }
    
    private void createExcelFiles(int customerCount, CustomerSearch search, Map<String, Configuration> siteConfig,
    		List<Map<String, Object>> exportedFiles, Map <Integer, SalesRep> salesRepMap, Map <Integer, Customer> affiliateMap) throws Exception {
		int limit = 3000;
		search.setLimit( limit );
		
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	String fileName = "/customerExport_" + dateFormatter.format(new Date()) + "";

    	for (int offset=0, excel = 1; offset<customerCount; excel++) {
	    	List<Customer> customerList = this.webJaguar.getCustomerExportList(search);
	    	int start = offset + 1;
	    	int end = (offset + limit) < customerCount ? (offset + limit) : customerCount;
	    	HSSFWorkbook wb = createWorkbook(customerList, start + " to " + end, salesRepMap, affiliateMap);
	    	// Write the output to a file
	    	File newFile = new File(baseFile.getAbsolutePath() + fileName + excel + ".xls");
	        FileOutputStream fileOut = new FileOutputStream(newFile);
	        wb.write(fileOut);
	        fileOut.close();
	        HashMap<String, Object> fileMap = new HashMap<String, Object>();
	        fileMap.put("file", newFile);
			exportedFiles.add( fileMap );    
	        offset = offset + limit;
	        search.setOffset(offset);
    	}
    }
    
    private HSSFWorkbook createWorkbook(List<Customer> customerList, String sheetName,Map <Integer, SalesRep> salesRepMap, Map <Integer, Customer> affiliateMap) throws Exception {
    	int numOfCategories = 0;
    	if (gCUSTOMER_CATEGORY != null) {
        	for (Customer customer: customerList) {
        		Set<Object> categories = new HashSet<Object>();
    			for (Object id: this.webJaguar.getCategoryIdsByUser(customer.getId())) { 
                    categories.add(new Integer(id.toString()));                                        
        		}
        		customer.setCatIds(categories);
        		if (customer.getCatIds() != null) {
            		int catIdSize = customer.getCatIds().size();
            		numOfCategories = (catIdSize > numOfCategories) ? catIdSize : numOfCategories;         			
        		}
        	}    		
    	}

		Map<String, Object> regionMap = this.webJaguar.getRegionMap();
		
    	Iterator iter = customerList.iterator();
    	
    	HSSFWorkbook wb = new HSSFWorkbook();
    	HSSFSheet sheet = wb.createSheet(sheetName);
    	
    	// create the headers
		Map<String, Short> headerMap = new HashMap<String, Short>();
		short col = 0;
		headerMap.put("CustomerId", col++);
		headerMap.put("EmailAddress", col++);
		if (gSUB_ACCOUNTS) {
			headerMap.put("Parent", col++);
		}
		headerMap.put("Suspended", col++);
		if (gMASS_EMAIL) {
			headerMap.put("Unsubscribe", col++);			
		}
		headerMap.put("FirstName", col++);
		headerMap.put("LastName", col++);
		headerMap.put("Rating1", col++);
    	headerMap.put("Rating2", col++);	
    	if(gBUDGET){
    		headerMap.put("CreditAllowed", col++);
    	}
    	headerMap.put("CardId", col++);	
    	if(gPAYMENTS) {
    		headerMap.put("paymentAlert", col++);
    	}
		headerMap.put("Company", col++);
		headerMap.put("Qualifier", col++);
		headerMap.put("NationalRegion", col++);
		headerMap.put("Country", col);
		setText(getCell(sheet, 1, col++), "2-letter country code");
		headerMap.put("CountryFullName", col++);
		headerMap.put("StateRegion", col++);
		headerMap.put("Address1", col++);	
		headerMap.put("Address2", col++);	
		headerMap.put("City", col++);
		headerMap.put("StateRegion", col++);
		headerMap.put("StateProvince", col);
		setText(getCell(sheet, 1, col++), "for US: 2-letter state code");	
		headerMap.put("Zip", col++);
		headerMap.put("Phone", col++);
		headerMap.put("CellPhone", col++);	
		headerMap.put("Fax", col++);
		if (!gI18N.isEmpty()) {
			headerMap.put("Language", col++);			
		}
		headerMap.put("AccountNumber", col++);
		// see hidden price
		headerMap.put("SeeHiddenPrice", col++);
		// seller's permit
		if (gTAX_EXEMPTION) {
			headerMap.put("SellersPermit", col++);			
		}
		// payment options
		if(gSHOPPING_CART) {
			headerMap.put("PaymentOptions", col++);	
		}
		// price table
		if (gPRICE_TABLE > 0) {
			headerMap.put("PriceTable", col++);						
		}
		// protected access
		for (int i=1; i<=gPROTECTED; i++) {
			headerMap.put("ProtectedAccess" + i, col++);				
		}
		// Login Success URL
		headerMap.put("loginSuccessURL", col++);		
		// Sales Rep
		if ( gSALES_REP ) {
			headerMap.put("salesRep", col++);		
		}
		// Affiliate
		if ( gAFFILIATE > 0 ) {
			headerMap.put("AffiliateOwner", col++);		
		}
		// multi store
		if (gMULTI_STORE > 0) {
			headerMap.put(getMessageSourceAccessor().getMessage("multiStore"), col++);
		}
		
		// read only dates
		HSSFCellStyle dateCellStyle = wb.createCellStyle();
		dateCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yy");
		// read only time
		HSSFCellStyle timeCellStyle = wb.createCellStyle();
		timeCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("h:mm:ss AM/PM"));
		SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm:ss a");
		
		headerMap.put("AccountCreatedDate", col);
		setText(getCell(sheet, 1, col++), "Read Only");
		
		headerMap.put("AccountCreatedMonth", col);
		setText(getCell(sheet, 1, col++), "Read Only");
		headerMap.put("AccountCreatedQTR", col);
		setText(getCell(sheet, 1, col++), "Read Only");
		headerMap.put("AccountCreatedYear", col);
		setText(getCell(sheet, 1, col++), "Read Only");

		headerMap.put("AccountCreatedTime", col);
		setText(getCell(sheet, 1, col++), "Read Only");
		headerMap.put("LastLogin", col);
		setText(getCell(sheet, 1, col++), "Read Only");
		
		// trackcode (read only)
		headerMap.put("TrackCode", col);
		setText(getCell(sheet, 1, col++), "Read Only");
		// CustomerCreatedBy (read only)
		headerMap.put("CustomerCreatedBy", col);
		setText(getCell(sheet, 1, col++), "Read Only");
		// ordersGrandTotal (read only)
		headerMap.put("OrdersGrandTotal", col);
		setText(getCell(sheet, 1, col++), "Read Only");
		// orderCount (read only)
		headerMap.put("OrderCount", col);
		setText(getCell(sheet, 1, col++), "Read Only");
		// LastDateOrder (read only)
		headerMap.put("LastDateOrdered", col);
		setText(getCell(sheet, 1, col++), "Read Only");
		
    	// extra customer fields
		List customerFields = this.webJaguar.getCustomerFields();
		Iterator customerFieldsIter = customerFields.iterator();
		while (customerFieldsIter.hasNext()) {
			CustomerField customerField = (CustomerField) customerFieldsIter.next();
			if (customerField.isEnabled()) {
				headerMap.put("Field_" + customerField.getId(), col);
				setText(getCell(sheet, 1, col++), customerField.getName());				
			}
		}
		
		// print headers
		for (String headerName:headerMap.keySet()){
			setText(getCell(sheet, 0, headerMap.get(headerName).shortValue()), headerName); 
		}
		
		// category associations
		short[] category = new short[numOfCategories];
		for (int i=0; i<numOfCategories; i++) {
			Short categoryColumn = col++;
			category[i] = categoryColumn;
			setText(getCell(sheet, 0, categoryColumn), "Category"); 
		}
		
    	iter = customerList.iterator();
		
    	Class c = Customer.class;
    	Method m = null;
		HSSFCell cell = null;
		Object arglist[] = null;
		
		// create the rows
    	for (int row=2; iter.hasNext(); row++) {    		
    		Customer customer = (Customer) iter.next();
    		setText(getCell(sheet, row, headerMap.get("CustomerId").shortValue()), customer.getId().toString());
    		setText(getCell(sheet, row, headerMap.get("EmailAddress").shortValue()), customer.getUsername());
    		if (gSUB_ACCOUNTS) {
    			setText(getCell(sheet, row, headerMap.get("Parent").shortValue()), ( customer.getParent() == null ) ? "" : customer.getParent().toString());
    		}
    		setText(getCell(sheet, row, headerMap.get("Suspended").shortValue()), customer.isSuspended() ? "1" : null); 
    		if (gMASS_EMAIL) {
    			setText(getCell(sheet, row, headerMap.get("Unsubscribe").shortValue()), customer.isUnsubscribe() ? "1" : null);  		
    		}
        	setText(getCell(sheet, row, headerMap.get("FirstName").shortValue()), customer.getAddress().getFirstName());
        	setText(getCell(sheet, row, headerMap.get("LastName").shortValue()), customer.getAddress().getLastName());
        	setText(getCell(sheet, row, headerMap.get("Rating1").shortValue()), customer.getRating1() == null ? "" : customer.getRating1());
            setText(getCell(sheet, row, headerMap.get("Rating2").shortValue()), customer.getRating2() == null ? "" : customer.getRating2());
        	if(gBUDGET){
        		setText(getCell(sheet, row, headerMap.get("CreditAllowed").shortValue()), customer.getCreditAllowed() == null ? "": customer.getCreditAllowed().toString());
        	}
        	setText(getCell(sheet, row, headerMap.get("CardId").shortValue()), customer.getCardId() == null ? "" :customer.getCardId().toString());
        	if (gPAYMENTS) {
        		setText(getCell(sheet, row, headerMap.get("paymentAlert").shortValue()), customer.getPaymentAlert() == null ? "" :customer.getPaymentAlert().toString());
        	}
        	setText(getCell(sheet, row, headerMap.get("Company").shortValue()), customer.getAddress().getCompany());   
        	String qualifier = "";
    		if(customer.getQualifier() != null){
    			if(salesRepMap.get(customer.getQualifier())!=null){
    				qualifier = salesRepMap.get(customer.getQualifier()).getName();
    			}
    		}
        	setText(getCell(sheet, row, headerMap.get("Qualifier").shortValue()), qualifier);	
        	Map<String, String> region = new HashMap<String, String>();
    		if( customer.getAddress().getCountry() != null && !customer.getAddress().getCountry().isEmpty() && customer.getAddress().getStateProvince() != null &&  customer.getAddress().getStateProvince() != "") { 
        		region = (Map<String, String>) regionMap.get(customer.getAddress().getCountry() +"_" +customer.getAddress().getStateProvince());
        	} if (region == null && customer.getAddress().getCountry() != null && !customer.getAddress().getCountry().isEmpty()) {
        		region = (Map<String, String>) regionMap.get(customer.getAddress().getCountry() +"_");
        	}

        	setText(getCell(sheet, row, headerMap.get("NationalRegion").shortValue()), customer.getAddress().getCountry() == null || region == null ? "" : region.get("nationalRegion"));
        	setText(getCell(sheet, row, headerMap.get("Country").shortValue()), customer.getAddress().getCountry());
        	setText(getCell(sheet, row, headerMap.get("CountryFullName").shortValue()), customer.getAddress().getCountry() == null || region == null  ? "" : region.get("countryName"));
        	setText(getCell(sheet, row, headerMap.get("Address1").shortValue()), customer.getAddress().getAddr1());
        	setText(getCell(sheet, row, headerMap.get("Address2").shortValue()), customer.getAddress().getAddr2());
        	setText(getCell(sheet, row, headerMap.get("City").shortValue()), customer.getAddress().getCity());
        	setText(getCell(sheet, row, headerMap.get("StateRegion").shortValue()), customer.getAddress().getCountry() == null || region == null ? "" : region.get("stateRegion"));
        	setText(getCell(sheet, row, headerMap.get("StateProvince").shortValue()), customer.getAddress().getStateProvince());
        	setText(getCell(sheet, row, headerMap.get("Zip").shortValue()), customer.getAddress().getZip());
        	setText(getCell(sheet, row, headerMap.get("Phone").shortValue()), customer.getAddress().getPhone());
        	setText(getCell(sheet, row, headerMap.get("CellPhone").shortValue()), customer.getAddress().getCellPhone());
        	setText(getCell(sheet, row, headerMap.get("Fax").shortValue()), customer.getAddress().getFax());
        	//language
        	if (!gI18N.isEmpty()) {
        		setText(getCell(sheet, row, headerMap.get("Language").shortValue()), customer.getLanguageCode().toString());    			
        	}
    		setText(getCell(sheet, row, headerMap.get("AccountNumber").shortValue()), customer.getAccountNumber());
    		// see hidden price
        	setText(getCell(sheet, row, headerMap.get("SeeHiddenPrice").shortValue()), customer.isSeeHiddenPrice() ? "1" : null);     			
    		// seller's permit
    		if (gTAX_EXEMPTION) {
            	setText(getCell(sheet, row, headerMap.get("SellersPermit").shortValue()), customer.getTaxId());
    		}   
    		// payment Options
    		if(gSHOPPING_CART) {
    			setText(getCell(sheet, row, headerMap.get("PaymentOptions").shortValue()), (customer.getPayment() == null) ? "" : customer.getPayment());  
    		}
    		// price table
    		if (gPRICE_TABLE > 0) {
 	    		cell = getCell(sheet, row, headerMap.get("PriceTable").shortValue()); 	
      	    	if (customer.getPriceTable() == 0 || customer.getPriceTable() > gPRICE_TABLE) {
          	    	cell.setCellType( HSSFCell.CELL_TYPE_BLANK );            	    		
      	    	} else {
          	    	cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
          	    	cell.setCellValue(customer.getPriceTable());      	    		
      	    	}
    		}
    		// protected access
    		if (gPROTECTED > 0) {
    			BitSet bitSet = customer.getProtectedAccessAsBitSet();
        		for (int i=1; i<=gPROTECTED; i++) {        
                	setText(getCell(sheet, row, headerMap.get("ProtectedAccess" + i).shortValue()), bitSet.get(i-1) ? "1" : null);
        		}
    		}  
    		// Login Success URL
    		setText(getCell(sheet, row, headerMap.get("loginSuccessURL").shortValue()), customer.getLoginSuccessURL());
    		// Sales Rep
    		if ( gSALES_REP ) {
    			setText(getCell(sheet, row, headerMap.get("salesRep").shortValue()), ( customer.getSalesRepId() == null ) ? "" : salesRepMap.get( customer.getSalesRepId() ).getName()); 
    		}
    		// Affiliate
    		if ( gAFFILIATE > 0 ) {
    			try {
    				setText(getCell(sheet, row, headerMap.get("AffiliateOwner").shortValue()), ( customer.getAffiliateParent() == 0 || customer.getAffiliateParent() == null ) ? "" : (affiliateMap.get( customer.getAffiliateParent() ).getAddress().getFirstName() + " " + affiliateMap.get( customer.getAffiliateParent() ).getAddress().getLastName())); 
    			} catch (Exception e) {
    				setText(getCell(sheet, row, headerMap.get("AffiliateOwner").shortValue()), "error"); 
    			}
    		}
    		// multi store
    		if (gMULTI_STORE > 0) {
    			setText(getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("multiStore")).shortValue()), (customer.getHost() == null) ? "" : customer.getHost());    			
    		}
    		// read only dates
    		cell = getCell(sheet, row, headerMap.get("AccountCreatedDate").shortValue());
    		cell.setCellStyle(dateCellStyle);
    		cell.setCellValue(sdfDate.format(customer.getCreated().getTime()));
    		
    		Calendar accountCreatedDateCalendar = new GregorianCalendar();
    		accountCreatedDateCalendar.setTime( customer.getCreated() );
    		
    		// date month 
    		setText(getCell(sheet, row, headerMap.get("AccountCreatedMonth")), Constants.getMonth(accountCreatedDateCalendar));    			 
    		// date QRT
    		setText(getCell(sheet, row, headerMap.get("AccountCreatedQTR")), Constants.getQuarter(accountCreatedDateCalendar));    			 
    		// date Year
    		setText(getCell(sheet, row, headerMap.get("AccountCreatedYear")), ""+accountCreatedDateCalendar.get( accountCreatedDateCalendar.YEAR ));
    		
    		cell = getCell(sheet, row, headerMap.get("AccountCreatedTime").shortValue());
    		cell.setCellStyle(timeCellStyle);
    		cell.setCellValue(sdfTime.format(customer.getCreated().getTime()));
    		
    		if (customer.getLastLogin() != null) {
        		cell = getCell(sheet, row, headerMap.get("LastLogin").shortValue());    
        		cell.setCellValue( customer.getLastLogin() );
       			cell.setCellStyle(dateCellStyle);
    		}
    		
    		// trackcode (read only)
    		setText(getCell(sheet, row, headerMap.get("TrackCode").shortValue()), customer.getTrackcode());
    		
    		// CustomerCreatedBy
    		setText(getCell(sheet, row, headerMap.get("CustomerCreatedBy").shortValue()), customer.getRegisteredBy());
    		
    		// ordersGrandTotal
    		cell = getCell(sheet, row, headerMap.get("OrdersGrandTotal").shortValue()); 	
  	    	if (customer.getOrdersGrandTotal() == null) {
      	    	cell.setCellType( HSSFCell.CELL_TYPE_BLANK );            	    		
  	    	} else {
      	    	cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    	cell.setCellValue(customer.getOrdersGrandTotal());      	    		
  	    	}
    		// CustomerCreatedBy
    		cell = getCell(sheet, row, headerMap.get("OrderCount").shortValue()); 	
  	    	if (customer.getOrderCount() == null) {
      	    	cell.setCellType( HSSFCell.CELL_TYPE_BLANK );            	    		
  	    	} else {
      	    	cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    	cell.setCellValue(customer.getOrderCount());      	    		
  	    	}
  	    	// read only lastDateOrder
    		cell = getCell(sheet, row, headerMap.get("LastDateOrdered").shortValue());
    		if (customer.getLastOrderDate() == null) {
      	    	cell.setCellType( HSSFCell.CELL_TYPE_BLANK );            	    		
  	    	} else {
  	    		cell.setCellStyle(dateCellStyle);
  	    		cell.setCellValue(sdfDate.format(customer.getLastOrderDate().getTime()));  	    		
  	    	}
    		
        	// set extra customer fields
    		customerFieldsIter = customerFields.iterator();
    		while (customerFieldsIter.hasNext()) {
    			CustomerField customerField = (CustomerField) customerFieldsIter.next();
    			if (customerField.isEnabled()) {
            		m = c.getMethod("getField" + customerField.getId());
            		setText(getCell(sheet, row, headerMap.get("Field_"+customerField.getId()).shortValue()), (String) m.invoke(customer, arglist));    				
    			}
        	}
    		
    		// category associations
    		if (customer.getCatIds() != null) {
        		int index = 0;
    			for (Object catId: customer.getCatIds()) {
    	    		setText(getCell(sheet, row, category[index++]), catId.toString());
    			}    			
    		}

    	} // for	  
    	return wb;
    }
    
    private void createCsvFile(int customerCount, CustomerSearch search, Map<String, Configuration> siteConfig,
    		List<Map<String, Object>> exportedFiles, Map <Integer, SalesRep> salesRepMap, Map <Integer, Customer> affiliateMap) throws Exception {
    	if (customerCount < 1) return;
    	
    	Map<String, Object> regionMap = this.webJaguar.getRegionMap();
    	
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm:ss a");
    	
		String fileName = "/customerExport_" + dateFormatter.format(new Date()) + ".csv";
    	
    	File exportFile = new File(baseFile, fileName);
    	
    	dateFormatter = new SimpleDateFormat("MM/dd/yy kk:mm");  
    	CSVWriter writer = new CSVWriter(new FileWriter(exportFile));
    	List<String> line = new ArrayList<String>();
    	//header name
    	line.add("CustomerId");
    	line.add("EmailAddress");
    	if (gSUB_ACCOUNTS) {
    		line.add("Parent");
    	}
    	line.add("Suspended");
    	if (gMASS_EMAIL) {
    		line.add("Unsubscribe");
    	}
    	line.add("FirstName");
    	line.add("LastName");
    	line.add("Rating1");
    	line.add("Rating2");
    	if(gBUDGET){
    		line.add("CreditAllowed");
    	}
    	line.add("cardId");
    	if(gPAYMENTS) {
    		line.add("paymentAlert");
    	}
    	line.add("Company");
    	line.add("Qualifier");
    	line.add("NationalRegion");
    	line.add("Country");
    	line.add("countryFullName");
    	line.add("Address1");
    	line.add("Address2");
    	line.add("City");
    	line.add("StateRegion");
    	line.add("StateProvince");
    	line.add("Zip");
    	line.add("Phone");
    	line.add("CellPhone");
    	line.add("Fax");
    	line.add("AccountNumber");
    	line.add("SeeHiddenPrice");
    	//language
    	if (!gI18N.isEmpty()) {
    		line.add("Language");
        }
		if (gTAX_EXEMPTION) {
    		line.add("SellersPermit");
    	}
		//payment options
		if(gSHOPPING_CART) {
			line.add("PaymentOptions");
		}
		if (gPRICE_TABLE > 0) {
    		line.add("PriceTable");
    	}
    	for (int i=1; i<=gPROTECTED; i++) {
    		line.add("ProtectedAccess"+i);				
		}
    	line.add("loginSuccessURL");
    	if ( gSALES_REP ) {	
			line.add("salesRep");
		}
		if ( gAFFILIATE > 0 ) {
			line.add("AffiliateOwner");	
		}
    	line.add("AccountCreatedDate");
    	line.add("AccountCreatedMonth");
    	line.add("AccountCreatedQTR");
    	line.add("AccountCreatedYear");
    	line.add("AccountCreatedTime");
    	line.add("LastLogin");
    	line.add("TrackCode");
    	line.add("CustomerCreatedBy");
    	line.add("OrdersGrandTotal");
    	line.add("OrderCount");
    	line.add("LastOrderedMonth");
    	line.add("LastOrderedQTR");
    	line.add("LastOrderedYear");

    	// extra customer fields
		List<CustomerField> customerFields = this.webJaguar.getCustomerFields();
		for (CustomerField customerField:customerFields) {
			if (customerField.isEnabled()) {
				line.add("Field_" + customerField.getId());			
			}
		}
    	
		writer.writeNext(line.toArray(new String[line.size()]));
		
		line = new ArrayList<String>();
		line.add("");
		line.add("");
    	if (gMASS_EMAIL) { line.add(""); }
    	List<String> list = Arrays.asList("","","","","","","","","","","","","","");
    	line.addAll( list );
    	if (!gI18N.isEmpty()) { line.add(""); }
		if (gTAX_EXEMPTION) { line.add(""); }
    	if (gPRICE_TABLE > 0) { line.add(""); }
    	for (int i=1; i<=gPROTECTED; i++) { line.add(""); }
    	line.add("");
    	if ( gSALES_REP ) {	 line.add(""); }
    	if ( gAFFILIATE > 0 ) {	 line.add(""); }
    	list = Arrays.asList("","","","","","","","","","","","","");
    	line.addAll( list );
    	// extra customer fields
		for (CustomerField customerField:customerFields) {
			if (customerField.isEnabled()) {
				line.add("(" + customerField.getName() + ")");			
			}
		}
    	writer.writeNext(line.toArray(new String[line.size()]));
		
		Class c = Customer.class;
    	Method m = null;
		HSSFCell cell = null;
		Object arglist[] = null;
    	// create the rows
    	for (Customer customer: this.webJaguar.getCustomerExportList( search )) {
			line = new ArrayList<String>();
			line.add(customer.getId().toString());
			line.add(customer.getUsername().toString());
			if (gSUB_ACCOUNTS) {
				line.add(( customer.getParent() == null ) ? null : customer.getParent().toString());
			}
			line.add(customer.isSuspended() ? "1" : null);
			if (gMASS_EMAIL) {
				line.add(customer.isUnsubscribe() ? "1" : null);
			}
			line.add(( customer.getAddress().getFirstName() == null ) ? null : customer.getAddress().getFirstName().toString());
			line.add(( customer.getAddress().getLastName() == null ) ? null : customer.getAddress().getLastName().toString());

	    	line.add((customer.getRating1() == null) ? null : customer.getRating1().toString());
	    	line.add((customer.getRating2() == null) ? null : customer.getRating2().toString());
	    	if(gBUDGET){
	    		line.add((customer.getCreditAllowed() == null) ? null :customer.getCreditAllowed().toString());
	    	}
	    	
	    	line.add((customer.getCardId() == null) ? null : customer.getCardId());
	    	
	    	Map<String, String> region = new HashMap<String, String>();
    		if( customer.getAddress().getCountry() != null && !customer.getAddress().getCountry().isEmpty() && customer.getAddress().getStateProvince() != null &&  customer.getAddress().getStateProvince() != "") { 
		    	region = (Map<String, String>) regionMap.get(customer.getAddress().getCountry() + "_" +customer.getAddress().getStateProvince());
        	} if (region == null && customer.getAddress().getCountry() != null && !customer.getAddress().getCountry().isEmpty()) {
		    	region = (Map<String, String>) regionMap.get(customer.getAddress().getCountry() + "_");
        	}
        	if (gPAYMENTS){
        		line.add((customer.getPaymentAlert() == null) ? null: customer.getPaymentAlert().toString());
        	}
        	line.add(( customer.getAddress().getCompany() == null ) ? null : customer.getAddress().getCompany().toString());
        	line.add(( customer.getQualifier() == null ) ? null : customer.getQualifier().toString());
			line.add(( customer.getAddress().getCountry() == null || region == null) ? null : region.get("nationalRegion"));
			line.add(( customer.getAddress().getCountry() == null ) ? null : customer.getAddress().getCountry().toString());
			line.add(( customer.getAddress().getCountry() == null || region == null) ? null : region.get("countryName"));
			line.add(( customer.getAddress().getAddr1() == null ) ? null : customer.getAddress().getAddr1().toString());
			line.add(( customer.getAddress().getAddr2() == null ) ? null : customer.getAddress().getAddr2().toString());
			line.add(( customer.getAddress().getCity() == null ) ? null : customer.getAddress().getCity().toString());
			line.add(( customer.getAddress().getCountry() == null || region == null) ? null : region.get("stateRegion"));
			line.add(( customer.getAddress().getStateProvince() == null ) ? null : customer.getAddress().getStateProvince().toString());
			line.add(( customer.getAddress().getZip() == null ) ? null : customer.getAddress().getZip().toString());
			line.add(( customer.getAddress().getPhone() == null ) ? null : customer.getAddress().getPhone().toString());
			line.add(( customer.getAddress().getCellPhone() == null ) ? null : customer.getAddress().getCellPhone().toString());
			line.add(( customer.getAddress().getFax() == null ) ? null : customer.getAddress().getFax().toString());
			//language
	    	if (!gI18N.isEmpty()) {
	    		line.add(customer.getLanguageCode().toString());
			}
			line.add(( customer.getAccountNumber() == null ) ? null : customer.getAccountNumber().toString());
			line.add(customer.isSeeHiddenPrice() ? "1" : null);
			if (gTAX_EXEMPTION) {
				line.add(customer.getTaxId());
			}
			// payment options
			if (gSHOPPING_CART) {
				line.add((customer.getPayment() == null) ? "" : customer.getPayment().toString());
			}
			// price table
    		if (gPRICE_TABLE > 0) {
    			if (customer.getPriceTable() == 0 || customer.getPriceTable() > gPRICE_TABLE) {
    				line.add(null);        	    		
      	    	} else {
      	    		line.add(customer.getPriceTable().toString());    	    		
      	    	}
    		}
    		// protected access
    		if (gPROTECTED > 0) {
    			BitSet bitSet = customer.getProtectedAccessAsBitSet();
        		for (int i=1; i<=gPROTECTED; i++) {  
        			line.add(bitSet.get(i-1) ? "1" : null);
        		}
    		}  
    		// Login Success URL
    		line.add(customer.getLoginSuccessURL());
    		// Sales Rep
    		if ( gSALES_REP ) {
    			line.add(( customer.getSalesRepId() == null ) ? "" : salesRepMap.get( customer.getSalesRepId() ).getName().toString());
    		}
    		// Affiliate
    		if ( gAFFILIATE > 0 ) {
    			try {
    				line.add(( customer.getAffiliateParent() == 0 || customer.getAffiliateParent() == null ) ? "" : (affiliateMap.get( customer.getAffiliateParent() ).getAddress().getFirstName() + " " + affiliateMap.get( customer.getAffiliateParent() ).getAddress().getLastName()));
    			} catch (Exception e) {
    				line.add("error");
    			}
    		}
    		
    		// date
    		line.add(sdfDate.format(customer.getCreated().getTime()));
    		
    		Calendar accountCreatedDateCalendar = new GregorianCalendar();
    		accountCreatedDateCalendar.setTime( customer.getCreated() );
    		
    		// shipped month
    		line.add( Constants.getMonth(accountCreatedDateCalendar) );
    		// shipped QRT
    		line.add( Constants.getQuarter(accountCreatedDateCalendar) );
    		// shipped Year
    		line.add( accountCreatedDateCalendar.get( accountCreatedDateCalendar.YEAR ) + "" );
    		
    		// time
    		line.add(sdfTime.format(customer.getCreated().getTime()));
    		
    		if (customer.getLastLogin() != null) {
    			line.add(( customer.getLastLogin() == null ) ? null : customer.getLastLogin().toString());
    		} else {
    			line.add(null);
    		}
    		
    		// trackcode (read only)
    		line.add(( customer.getTrackcode() == null ) ? null : customer.getTrackcode().toString());
    		
    		// CustomerCreatedBy
    		line.add(( customer.getRegisteredBy() == null ) ? null : customer.getRegisteredBy().toString());
    		
    		// ordersGrandTotal	
    		line.add(( customer.getOrdersGrandTotal() == null ) ? null : customer.getOrdersGrandTotal().toString());
  	    	
    		// Order count
    		line.add(( customer.getOrderCount() == null ) ? null : customer.getOrderCount().toString());
    		
    		Calendar lastOrderDateDateCalendar = new GregorianCalendar();
    		try {lastOrderDateDateCalendar.setTime( customer.getLastOrderDate() );} catch (NullPointerException e) {}
    		
    		// Last Date Ordered month
    		line.add( (customer.getLastOrderDate()==null) ? "" : Constants.getMonth(lastOrderDateDateCalendar) );
    		// Last Date Ordered QRT
    		line.add( (customer.getLastOrderDate()==null) ? "" : Constants.getQuarter(lastOrderDateDateCalendar) );
    		// Last Date Ordered Year
    		line.add( (customer.getLastOrderDate()==null) ? "" : lastOrderDateDateCalendar.get( lastOrderDateDateCalendar.YEAR ) + "" );
    		
        	// set extra customer fields
    		for (CustomerField customerField:customerFields) {
    			if (customerField.isEnabled()) {
            		m = c.getMethod("getField" + customerField.getId());
            		line.add((String) m.invoke(customer, arglist));
    			}
        	}
    		
    		// category associations
    		if (customer.getCatIds() != null) {
        		int index = 0;
    			for (Object catId: customer.getCatIds()) {
    				line.add(catId.toString());
    			}    			
    		}

			line.add("");
			writer.writeNext(line.toArray(new String[line.size()]));
    	}
    	
    	writer.close();
    	
    	HashMap<String, Object> fileMap = new HashMap<String, Object>();
        fileMap.put("file", exportFile);
		exportedFiles.add( fileMap );
    }
    
    private void notifyAdmin(Map<String, Configuration> siteConfig, HttpServletRequest request){
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	StringBuffer message = new StringBuffer();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
    	message.append("user "+accessUser.getUsername()+" has exported Customer file");
    	SimpleMailMessage msg = new SimpleMailMessage();			
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Automated Export on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}