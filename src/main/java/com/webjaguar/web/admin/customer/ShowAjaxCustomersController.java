/* Copyright 2005, 2008 Advanced E-Media Solutions
 * author: Shahin Naji
 */

package com.webjaguar.web.admin.customer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;


public class ShowAjaxCustomersController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
		CustomerSearch customerSearch = getCustomerSearch( request );
		Map<String, Object> myModel = new HashMap<String, Object>();	
		List<Customer> customers = this.webJaguar.getCustomersAjax( customerSearch );
		myModel.put("search", customerSearch);
		myModel.put("customers", customers);
		
        return new ModelAndView("admin/customers/ajax/showCustomers", "model", myModel);
	}
	
	private CustomerSearch getCustomerSearch(HttpServletRequest request) {
		CustomerSearch customerSearch = new CustomerSearch();

		// first name
		if (request.getParameter("value") != null && request.getParameter("search") != null && request.getParameter("search").equals( "firstname" )) {
			customerSearch.setFirstName(ServletRequestUtils.getStringParameter( request, "value", "" ));
			customerSearch.setAjaxSelect( "firstname" );
		}
		
		// last name
		if (request.getParameter("value") != null && request.getParameter("search") != null && request.getParameter("search").equals( "lastname" )) {
			customerSearch.setLastName(ServletRequestUtils.getStringParameter( request, "value", "" ));
			customerSearch.setAjaxSelect( "lastname" );
		}	
		
		return customerSearch;
	}
}
