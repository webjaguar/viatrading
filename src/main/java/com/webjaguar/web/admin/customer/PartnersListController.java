package com.webjaguar.web.admin.customer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.BudgetPartner;
import com.webjaguar.model.Configuration;

public class PartnersListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		
		if(request.getParameter("__add") != null){
			return new ModelAndView(new RedirectView("addPartners.jhtm"));		
		}
		
		PagedListHolder partnersList = 
				new PagedListHolder( this.webJaguar.getPartnersList(null));
		partnersList.setPageSize(10);
		String page = request.getParameter("page");
		if (page != null) {
			partnersList.setPage(Integer.parseInt(page)-1);
		}
		model.put("partnersList", partnersList);
		
		return new ModelAndView("admin/customers/partnersList", "model", model);
	}

}
