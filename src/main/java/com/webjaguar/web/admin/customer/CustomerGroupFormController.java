/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.customer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataAccessException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.dao.PredictiveDialingDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerGroup;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.web.form.CustomerGroupForm;

public class CustomerGroupFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar; 
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private PredictiveDialingDao predictiveDialingDao;
	public void setPredictiveDialingDao(PredictiveDialingDao predictiveDialingDao) { this.predictiveDialingDao = predictiveDialingDao; }
	
	public CustomerGroupFormController() {
		setSessionForm(true);
		setCommandName("customerGroupForm");
		setCommandClass(CustomerGroupForm.class);
		setFormView("admin/customers/groupForm");
		setSuccessView( "customerGroupList.jhtm" );
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
    	
    	CustomerGroupForm groupForm = (CustomerGroupForm) command;
    	Map<String, Object> model = new HashMap<String, Object>();
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			if (this.webJaguar.isGroupCustomerExist(groupForm.getCustomerGroup().getId())) {
				model.put("message", "groupHasCustomer");
				return showForm(request, response, errors, model);	
			} else {
				this.webJaguar.deleteCustomerGroupById( groupForm.getCustomerGroup().getId() );
				return new ModelAndView(new RedirectView(getSuccessView()));
			}
		}     	
     	
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		// Push To Predictive Dialing
		if (request.getParameter("_pushToPredictiveDialing") != null) {
			CustomerSearch search = new CustomerSearch();
			search.setGroupId(groupForm.getCustomerGroup().getId());
			List<Customer> customerList = this.webJaguar.getCustomerListWithDate(search);
			
			this.predictiveDialingDao.insertCustomerToPredictiveDialing(customerList);
			return new ModelAndView(new RedirectView( getSuccessView()));
		}
		
		try {
			if (groupForm.isNewCustomerGroup()) {
				AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
				groupForm.getCustomerGroup().setCreatedBy(accessUser.getUsername());
				this.webJaguar.insertCustomerGroup(groupForm.getCustomerGroup());
			} else {
				this.webJaguar.updateCustomerGroup(groupForm.getCustomerGroup());
			}
		} catch (DataAccessException e) {
			model.put("message", "form.hasDuplicate");
			return showForm(request, response, errors, model);	
		}
		return new ModelAndView(new RedirectView( getSuccessView()));  
    }

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}    
			
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors)
	{
		CustomerGroupForm groupForm = (CustomerGroupForm) command;
 
       	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();
	   
	    map.put("model", myModel);  
	    return map;
    }  
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		CustomerGroupForm groupForm;
		Integer groupId = ServletRequestUtils.getIntParameter( request, "id", -1 );
		if ( groupId != -1 ) {
			CustomerGroup group = this.webJaguar.getCustomerGroupById( groupId );
			groupForm = new CustomerGroupForm(group);
		} else {
			groupForm = new CustomerGroupForm();
		}
		return groupForm;
	}
}
