package com.webjaguar.web.admin.customer;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;


import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.BudgetPartner;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerBudgetPartner;

public class PartnerCustomerForm implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> model = new HashMap<String, Object>();
		List<BudgetPartner> partnerList= this.webJaguar.getPartnersList(true);
		
		
		Customer customer = this.webJaguar.getCustomerById(ServletRequestUtils.getIntParameter(request, "cId", 0));
		if (customer != null) {
			model.put("customer", customer);
		} else {
			return new ModelAndView(new RedirectView("customerList.jhtm"));			
		}
		model.put("partnerList", partnerList);
		// get accessUser username
    	String accessUser = this.webJaguar.getAccessUser(request).getUsername();
    	
    	if(request.getParameter("cId") != null && request.getParameter("pId") != null){
    		model.put("update", "true");
    		model.put("pId", Integer.parseInt(request.getParameter("pId")));
    	}
    	
    	
		if(request.getParameter("cId") != null && (request.getParameter("__assignPartner") != null || (request.getParameter("__updatePartner") != null))) {

			CustomerBudgetPartner partner = new CustomerBudgetPartner();
			Double amount = 0.00;
			try {
				amount = ServletRequestUtils.getDoubleParameter(request, "__partnerAmount", 0.00);
				partner.setAmount(amount);
				partner.setDate(new Date());
				partner.setCreatedBy(accessUser);
				partner.setModifiedBy(accessUser);
				partner.setUserId(Integer.parseInt(request.getParameter("cId")));
				partner.setPartnerId(Integer.parseInt(request.getParameter("partnerId")));
				if(partnerList != null) {
					for(BudgetPartner p: partnerList){
						if(p.getId().compareTo(partner.getPartnerId()) == 0){
							partner.setPartnerName(p.getPartnerName());
						}
					}
				}
				if(request.getParameter("__updatePartner") != null) {
					this.webJaguar.updatePartnerCustomer(partner, false);
				} else {
					this.webJaguar.insertPartnerCustomer(partner);
				}
				return new ModelAndView(new RedirectView("partnerCustomerList.jhtm?cId="+request.getParameter("cId")));
			} catch (Exception e){
				model.put("message", "onlyNumbers");
				model.put("partnerAmt", request.getParameter("__partnerAmount"));
				return new ModelAndView("admin/customers/partnerCustomerForm", model);
			}
			
		}
		return new ModelAndView("admin/customers/partnerCustomerForm", model);
	}

}
