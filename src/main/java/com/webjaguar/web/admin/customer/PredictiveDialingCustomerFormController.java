package com.webjaguar.web.admin.customer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;

public class PredictiveDialingCustomerFormController implements Controller {
	
	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		int customerId = ServletRequestUtils.getIntParameter(request, "customerId", -1);
		int crmId = ServletRequestUtils.getIntParameter(request, "crmId", -1);
		if(customerId != -1){
			return new ModelAndView(new RedirectView("customerDialingForm.jhtm?id="+customerId));
		}else{
			return new ModelAndView(new RedirectView("../crm/crmContactDialingForm.jhtm?id="+crmId));
		}
	}
	
}