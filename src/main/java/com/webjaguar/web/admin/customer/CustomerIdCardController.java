/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.customer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.SalesRep;

public class CustomerIdCardController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	if ( !(Boolean) gSiteConfig.get( "gREGISTRATION_TOUCH_SCREEN" ) ) {
    		return new ModelAndView( new RedirectView( "customerList.jhtm" ));
    	}
    	Map<String, Object> model = new HashMap<String, Object>();
    	Integer userId = null;
    	
    	//Tow paths : 1.from backend  2.from frontend
    	String path = request.getServletPath();		// pattern /customerIdCard_*.pdf
		try {
			int startIndex = 0;
			for(int i = path.length()-4; i>=0; i--){
				if(path.charAt(i) == '_'){
					startIndex = i+1;
					break;
				}
			}
			userId = new Integer(path.substring(startIndex, path.length()-4));			
		} catch (NumberFormatException e) {
			userId = -1;
		}
    	if ( userId != -1 ) {
    		Customer customer = this.webJaguar.getCustomerById( userId );
    		this.webJaguar.updateCardIdCount(customer.getId());
    		SalesRep salesRep = this.webJaguar.getSalesRepById( customer.getSalesRepId() );
    		model.put("customer", customer);
    		model.put("salesRep", salesRep);
    	}
    	model.put("context", getApplicationContext());	
		return new ModelAndView("customerIDPdfView", model);
	}

}
