/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.customer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.dao.DataAccessException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.support.DefaultMultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.cete.dynamicpdf.imaging.tiff.a;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Affiliate;
import com.webjaguar.model.BrokerImage;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.DialingNote;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SalesRepSearch;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmQualifierSearch;
import com.webjaguar.thirdparty.echosign.EchoSign;
import com.webjaguar.thirdparty.echosign.EchoSignApi;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.Utilities;
import com.webjaguar.web.form.CustomerForm;
import com.webjaguar.web.form.EmailMessageForm;

import echosign.api.dto.DocumentHistoryEvent;
import echosign.api.dto.DocumentInfo;

public class CustomerFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	private String formType;
	public void setFormType(String formType) { this.formType = formType; }
	
	private Properties site;

	public void setSite(Properties site) {
		this.site = site;
	}
	
	public CustomerFormController() {
		setSessionForm(true);
		setCommandName("customerForm");
		setCommandClass(CustomerForm.class);
		setFormView("admin/customers/form");
		setSuccessView("customerList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {

		CustomerForm customerForm = (CustomerForm) command;
		int thumbnail = Integer.parseInt( siteConfig.get( "THUMBNAIL_RESIZE" ).getValue() );
	
		if (request.getParameter("__addContact") != null) {
			//setFormView("admin/customers/customerExtContactForm");
			return new ModelAndView(new RedirectView("customerExtContactForm.jhtm"), "uid", customerForm.getCustomer().getId());	
		}
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			try {
				this.webJaguar.deleteCustomer(customerForm.getCustomer());				
			} catch (DataAccessException e) {
		    	Map<String, Object> model = new HashMap<String, Object>();
				model.put("message", "customer.exception.hasOrders");
				return showForm(request, response, errors, model);	
			}
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		//upload Broker Logo
		if(customerForm.getCustomer().getCustomerType() != null && customerForm.getCustomer().getCustomerType() == 1){
			//Save Broker Image
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile image_file = multipartRequest.getFile( "image_file_1" );
			String root = site.getProperty("site.root");
			File baseFile = new File( getServletContext().getRealPath( "/assets/Image/Customer" ) );
			if (root != null && !root.isEmpty()) {
				baseFile = new File(root + "/assets/Image/Customer");
			}
			BrokerImage image = new BrokerImage();
			if(image_file != null && !image_file.isEmpty()){
				File uploadFile = new File(baseFile.getAbsolutePath(), image_file.getOriginalFilename());
				try {
					image_file.transferTo(uploadFile);
					image.setImageUrl(image_file.getOriginalFilename());
					customerForm.getCustomer().setBrokerImage(image);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
			
			
			//remove Broker Image
			if( request.getParameter("__clear_image_1") != null){
				if (customerForm.getCustomer().getBrokerImage().getImageUrl() != null && customerForm.getCustomer().getBrokerImage().getImageUrl() != "") {
					File deleteFile = new File(baseFile.getAbsolutePath(), customerForm.getCustomer().getBrokerImage().getImageUrl());
					if(deleteFile.exists()) {
						deleteFile.delete();
					}
					customerForm.getCustomer().setRemoveBrokerImage(true);
				}
			}
		}
		
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile image_file = multipartRequest.getFile("image_file_2");
		
		String root = site.getProperty("site.root");
		File baseFile = new File( getServletContext().getRealPath("/assets/Image/Product"));
		File thumbDir = new File( baseFile, "thumb" );
		File tempDir = new File( baseFile, "temp" );
		if (root != null && !root.isEmpty()) {
			baseFile = new File(root + "/assets/Image/Product");
		}
		
		if (!baseFile.exists()){
            baseFile.mkdir();
        }
		
		BrokerImage image = new BrokerImage();
		if(image_file != null && !image_file.isEmpty()){
			File uploadFile = new File(baseFile.getAbsolutePath(), "Customer_" + customerForm.getCustomer().getId() + ".jpg");
			try {
				if(uploadFile.exists()) {
					uploadFile.delete();
				}
				image_file.transferTo(uploadFile);
				image.setImageUrl(image_file.getOriginalFilename());
				customerForm.getCustomer().setBrokerImage(image);
				
				//copy( temp, thumb );
				
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		
		/*Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				baseFile = new File( (String) prop.get( "site.root" ) + "/assets/Image/Product" );
			}
		} catch ( Exception e ) {}
		
		File detailsbigDir = new File( baseFile, "detailsbig" );
		File thumbDir = new File( baseFile, "thumb" );
		File tempDir = new File( baseFile, "temp" ); // temporary folder

		String fileName = image_file.getOriginalFilename();
		File detailsbig = new File( baseFile.getAbsolutePath() + "/" + fileName );
		File thumb = new File( thumbDir.getAbsolutePath() + "/" + fileName );
		File temp = new File( tempDir.getAbsolutePath() + "/" + fileName );
		try {
			// resize image in temporary folder
			image_file.transferTo( temp );
			boolean success = this.webJaguar.resizeImage( temp.getAbsolutePath(), temp.getAbsolutePath(), 800, 600, image_file.getContentType() );
			// if success, copy resized image to detailsbig folder, and then create thumbnail
			if ( success ) {
				BufferedImage img = ImageIO.read( temp );
				if ( img.getHeight() <= thumbnail && img.getWidth() <= thumbnail ) {
					copy( temp, thumb );
				} else {
					this.webJaguar.resizeImage( temp.getAbsolutePath(), thumb.getAbsolutePath(), thumbnail, thumbnail, image_file.getContentType() );
				}
				if ( (Boolean) gSiteConfig.get( "gWATERMARK" ) )
				{ // check if watermark is needed
					this.webJaguar.watermarkImage( temp.getAbsolutePath(), temp.getAbsolutePath(), siteConfig.get( "WATERMARK_TEXT" ).getValue(), Integer
							.parseInt( siteConfig.get( "WATERMARK_FONT_COLOR_R" ).getValue() ), Integer.parseInt( siteConfig.get( "WATERMARK_FONT_COLOR_G" )
							.getValue() ), Integer.parseInt( siteConfig.get( "WATERMARK_FONT_COLOR_B" ).getValue() ), Integer.parseInt( siteConfig.get(
							"WATERMARK_FONT_SIZE" ).getValue() ), image_file.getContentType() );
				}
				copy( temp, detailsbig );
				//image.setImageUrl( fileName );
			}
			// delete the images in temporary folder
			temp.delete();
		} catch ( IOException e ) {
			// do nothing
		}*/
		
		// protected access
       	if (customerForm.getProtectedAccess() != null) {
       		StringBuffer protectedAccess = new StringBuffer();
           	for (int i=0; i<customerForm.getProtectedAccess().length; i++) {
           		if (request.getParameter("__enabled_" + i) != null) {
           			protectedAccess.insert(0, '1');
           		} else {
           			protectedAccess.insert(0, '0');
           		}
           	}       		
           	customerForm.getCustomer().setProtectedAccess(protectedAccess.toString());
       	}
       	
       	if ((Boolean) gSiteConfig.get("gBUDGET_BRAND")) {
    		StringBuffer brandReport = new StringBuffer(",");
    		for (String skuPrefix: ServletRequestUtils.getStringParameters(request, "BRAND_REPORT")) {
    			brandReport.append(skuPrefix + ",");
    		}
    		if (brandReport.length() > 1) {
    			customerForm.getCustomer().setBrandReport(brandReport.toString());			
    		} else {
    			customerForm.getCustomer().setBrandReport(null);
    		}       		
       	}
       	
		if( (Boolean) gSiteConfig.get("gCRM") && customerForm.getCustomer().getCrmContactId() != null) {
			//Original : 
			//CrmContact crmContact = this.webJaguar.getCrmContactById( customerForm.getCustomer().getCrmContactId() );
			List<CrmContact> crmContacts = this.webJaguar.getCrmContactByEmail1( customerForm.getCustomer().getUsername() );
			
			if(crmContacts != null && !crmContacts.isEmpty()) {
				for(CrmContact crmContact : crmContacts){
					this.webJaguar.customerToCrmContactSynch(crmContact, customerForm.getCustomer());
					this.webJaguar.updateCrmContact(crmContact);
				}
	
			}else if(crmContacts.size() == 0) {
				CrmContact crmContact = this.webJaguar.getCrmContactById( customerForm.getCustomer().getCrmContactId() );
				if(crmContact != null) {
					this.webJaguar.customerToCrmContactSynch(crmContact, customerForm.getCustomer());
					this.webJaguar.updateCrmContact(crmContact);
				}
			} else {
				customerForm.getCustomer().setCrmAccountId(null);
				customerForm.getCustomer().setCrmContactId(null);
			}
		}		
		
		//Consignment - CompanyName
		if (customerForm.getCustomer().isConvertToSupplier()){
			//If customer is converted to supplier, company should not be empty.
			if (customerForm.getCustomer().getAddress().getCompany().equals(null) || customerForm.getCustomer().getAddress().getCompany().equals("")) {
				customerForm.getCustomer().getAddress().setCompany(customerForm.getCustomer().getAddress().getFirstName().concat(customerForm.getCustomer().getAddress().getLastName()));
			} else if (customerForm.getCustomer().getSupplierId() == null) {				
				customerForm.getCustomer().getAddress().setCompany(customerForm.getCustomer().getAddress().getCompany() + "-" +customerForm.getCustomer().getAddress().getFirstName());		
			}
		}
		if(customerForm.getCustomer().getId()!=null){
			int customerId=customerForm.getCustomer().getId();
			Customer customer = this.webJaguar.getCustomerById(customerId);
			if(customerForm.getCustomer().getSalesRepId()!=null &&(customer.getSalesRepId()==null||!customer.getSalesRepId().equals(customerForm.getCustomer().getSalesRepId()))){
				notifySalesRep(siteConfig, customerForm.getCustomer());
			}
		}else{
			if(customerForm.getCustomer().getSalesRepId()!=null ){
				notifySalesRep(siteConfig, customerForm.getCustomer());
			}
		}
		String type = request.getParameter("type");
		if(type != null && type.equals("update")){
			customerForm.getCustomer().setUpdateNewInformation(false);	
		}
		
		Class<Customer> c = Customer.class;
		
		for (int i = 1; i <= 20; i++) {
			String[] valueSet = request.getParameterValues("customer.field" + i + "Set");
			StringBuilder sb= new StringBuilder();

			if (valueSet != null && valueSet.length > 0) {	
				
				for(String fieldVal : valueSet) {
					sb.append(fieldVal).append(",");
				}
				
				Method m =c.getMethod("setField"+i,String.class);
				m.invoke(customerForm.getCustomer(), sb.toString().substring(0, sb.length() - 1));
			}
		}		
		
		
		this.webJaguar.updateCustomer(customerForm.getCustomer(), !customerForm.getCustomer().getUsername().equalsIgnoreCase(customerForm.getExistingUsername()), customerForm.getCustomer().getPassword().length() > 0);
		
		//Dialing Note
		if(customerForm.getCustomer().getNewDialingNote() != null && customerForm.getCustomer().getNewDialingNote().getNote() != null && !customerForm.getCustomer().getNewDialingNote().getNote().equals("")){
			customerForm.getCustomer().getNewDialingNote().setCustomerId(customerForm.getCustomer().getId());
			customerForm.getCustomer().getNewDialingNote().setCrmId(customerForm.getCustomer().getCrmContactId());
			this.webJaguar.insertDialingNoteHistory(customerForm.getCustomer().getNewDialingNote());
		}
		
		if ( (Integer) gSiteConfig.get( "gAFFILIATE" ) > 0 ) {
			if (!customerForm.getCustomer().getIsAffiliate() && customerForm.isCurrentIsAfiliate()) {
				// force to delete all children
				customerForm.getCustomer().setAffiliateParent(-1);
			}
			this.webJaguar.updateEdge(customerForm.getCustomer().getId(), customerForm.getCustomer().getAffiliateParent() );
		}
		
		if(request.getParameter("customer.qualifier") != null && !request.getParameter("customer.qualifier").equals(""))
			customerForm.getCustomer().setQualifier(Integer.valueOf(request.getParameter("customer.qualifier")));
		
		String forwardAction = request.getParameter("forwardAction");
		if(forwardAction == null){
			return new ModelAndView(new RedirectView(getSuccessView()));
		}else{
			return new ModelAndView(new RedirectView(forwardAction));
		}
		
	}
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		Integer userId = new Integer(request.getParameter("id"));
		
		Customer customer = this.webJaguar.getCustomerById(userId);
		CustomerForm customerForm = new CustomerForm(customer);
		if(!customerForm.getCustomer().getIsAffiliate()) {
			customerForm.getCustomer().setAffiliate( new Affiliate() );
		} else {
			customerForm.setCurrentIsAfiliate(true);
		}
		// protected access
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels > 0) {
			boolean protectedAccess[] = new boolean[protectedLevels];
			for (int i=0; i<customer.getProtectedAccess().length(); i++) {
				if (customer.getProtectedAccess().charAt(i) == '1') {
					protectedAccess[customer.getProtectedAccess().length()-1-i] = true;
				}
			}
			customerForm.setProtectedAccess(protectedAccess);			
		}
		if ( siteConfig.get( "CUSTOMERS_REQUIRED_COMPANY" ).getValue().equals( "true" ) ) {
			customerForm.setCompanyRequired( true );
		}
		
		if (customerForm.getCustomer().getAddress().getStateProvince() == null || customerForm.getCustomer().getAddress().getStateProvince().trim().equals("")) {
			customerForm.getCustomer().getAddress().setStateProvinceNA(true);
		}
		
		// Customer Fields
		List<CustomerField> customerFields = this.webJaguar.getCustomerFields();
		Iterator<CustomerField> iter = customerFields.iterator();
		while ( iter.hasNext() ) {
			CustomerField customerField = iter.next();
			if (!customerField.isEnabled()) {
				iter.remove();
			}
		}
		customerForm.setCustomerFields( customerFields );
		
		// supplier
		customer.setConvertToSupplier(customer.getSupplierId() != null);
		
		//predictiveDialing form
		if(formType != null && formType.equals("predictiveDialing")){
			setFormView("admin/customers/predictiveDialingForm");	
		}
		customerForm.getCustomer().setDialingNoteHistory(this.webJaguar.getDialingNoteHistoryById(userId, true));
		if(customerForm.getCustomer().getDialingNoteHistory() != null && !customerForm.getCustomer().getDialingNoteHistory().isEmpty()) {
			customerForm.getCustomer().setLastDialingNote(customerForm.getCustomer().getDialingNoteHistory().get(customerForm.getCustomer().getDialingNoteHistory().size() -1).getNote());
		}
		return customerForm;
		
	}
	
	protected void onBindAndValidate(HttpServletRequest request,
            Object command, BindException errors) throws Exception {
		CustomerForm customerForm = (CustomerForm) command;

       	if (customerForm.getProtectedAccess() != null) {
           	for (int i=0; i<customerForm.getProtectedAccess().length; i++) {
           		if (request.getParameter("__enabled_" + i) != null) {
           			customerForm.getProtectedAccess()[i] = true;
           		} else {
           			customerForm.getProtectedAccess()[i] = false;       			
           		}
           	}       		
       	}
       	if ( (Boolean) gSiteConfig.get( "gLOYALTY" ) ) {
       		if ( customerForm.getCustomer().getPointValue() == null || customerForm.getCustomer().getPointValue().equals( "" ) || 
       			 customerForm.getCustomer().getPointValue() <= 0.0 || customerForm.getCustomer().getPointValue() > 1.0) {
       			errors.rejectValue("customer.pointValue", "customer.pointValue.outofrange", "Point value is out of range.");    					
       		}
       	}
		
		if (!customerForm.getCustomer().getUsername().equalsIgnoreCase(customerForm.getExistingUsername()) &&
				this.webJaguar.getCustomerByUsername(customerForm.getCustomer().getUsername()) != null) {
			errors.rejectValue("customer.username", "USER_ID_ALREADY_EXISTS", "An account using this email address already exists.");    					
		}
		if ( customerForm.getCustomer().getTaxId() != null && customerForm.getCustomer().getTaxId().trim().equals( "" ) )
		{
			customerForm.getCustomer().setTaxId( null );
		}
		if ( !customerForm.getCustomer().getIsAffiliate() )
		{
			customerForm.getCustomer().getAffiliate().setPromoTitle( null );
			customerForm.getCustomer().getAffiliate().setCategoryId( null );
			customerForm.getCustomer().getAffiliate().setDomainName( null );
		}
		//	check if there is a promoCode
		if ( (Integer) gSiteConfig.get( "gAFFILIATE" ) > 0 ) {
			if ( customerForm.getCustomer().getAffiliate().getPromoTitle() != null && !"".equals( customerForm.getCustomer().getAffiliate().getPromoTitle() ) )
			{
				customerForm.getCustomer().getAffiliate().setPromoTitle( customerForm.getCustomer().getAffiliate().getPromoTitle().trim() );
				try {
					Integer id = this.webJaguar.getUserIdByPromoCode( customerForm.getCustomer().getAffiliate().getPromoTitle() );
					if ( id != null )
					{ // sku exists
						if ( customerForm.isNewCustomer() )
						{
						errors.rejectValue( "customer.affiliate.promoTitle", "PROMO_ALREADY_EXISTS", "Another Customer using this Promo code." );
						}
						else if ( id.compareTo( customerForm.getCustomer().getId() ) != 0 )
						{ // not the same item
							errors.rejectValue( "customer.affiliate.promoTitle", "PROMO_ALREADY_EXISTS", "Another Customer using this Promo code." );
						}
					}
				} catch (Exception e) {
					// do nothing
				}
			}
		
			if ( customerForm.getCustomer().getAffiliate().getCategoryId() != null ) {
				String protectedAccess = "0";
				// check if protected feature is enabled
				int protectedLevels = (Integer) gSiteConfig.get( "gPROTECTED" );
				if ( protectedLevels == 0 ) {
					protectedAccess = Constants.FULL_PROTECTED_ACCESS;
				} else {
					if ( customerForm.getCustomer() != null ) {
						protectedAccess = customerForm.getCustomer().getProtectedAccess();
					}
				}
				Category category = this.webJaguar.getCategoryById( customerForm.getCustomer().getAffiliate().getCategoryId(), protectedAccess );
				if ( category == null ) {
					errors.rejectValue( "customer.affiliate.categoryId", "CATEGORY_NOT_EXISTS", "Category ID does not exist." );
				}
			}
		}
		
		//	supplier
		if (customerForm.getCustomer().isConvertToSupplier()) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.supplierPrefix", "required");
			if (customerForm.getCustomer().getSupplierId() == null) {
				// new supplier
				customerForm.getCustomer().setSupplierPrefix(customerForm.getCustomer().getSupplierPrefix().trim());
				// check if existing already
				if(Utilities.containsDash(customerForm.getCustomer().getSupplierPrefix())){
					errors.rejectValue("customer.supplierPrefix", "SUPPLIER_PREFIX_CONTAINS_DASH"); 
				}else if (customerForm.getCustomer().getSupplierPrefix().length() < 4) {
					Object[] errorArgs = { new Integer(4) };
					errors.rejectValue("customer.supplierPrefix", "form.charLessMin", errorArgs, "should be at least 4 chars");
				} else if (this.webJaguar.getCustomerIdBySupplierPrefix(customerForm.getCustomer().getSupplierPrefix()) != null) {
					// existing already
					errors.rejectValue("customer.supplierPrefix", "SUPPLIER_PREFIX_ALREADY_EXISTS"); 
				}
			}
		} else {
			customerForm.getCustomer().setSupplierPrefix(null);
		}
	}
	
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
    	CustomerForm customerForm = (CustomerForm) command;
    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	String type = request.getParameter("type");
		if(type != null && type.equals("update")){
			map.put("message", "customer.updateNewInformation");
		}
	
    	List<Affiliate> affiliateOwner = new Vector<Affiliate>();
    	if ( (Integer) gSiteConfig.get( "gAFFILIATE" ) > 0 ) {
    		// add root with id 0
    		affiliateOwner.add( new Affiliate(0) );
    		if ( customerForm.getCustomer().getIsAffiliate() )
    		{
    			List<Affiliate> child = new Vector<Affiliate>();
    			affiliateOwner.addAll(this.webJaguar.getAffiliates( 0, 0 ) );
    			child.addAll(this.webJaguar.getAffiliates( customerForm.getCustomer().getId(), 1 ) );

    			List child1  = new Vector();
    			for (int x=child.size()-1; x>=0; x--) {
    				Affiliate ch = (Affiliate) child.get(x);
    				child1.add( ch.getNodeId() );
    			}
    			Iterator<Affiliate> it = affiliateOwner.iterator(); 
    			while ( it.hasNext() ) {
    				Affiliate af = it.next();
    				if ( child1.contains( af.getNodeId() )) {
    					it.remove();
    				}
    			}
    		}
    		else
    		{
    			customerForm.getCustomer().setAffiliate( new Affiliate() );
    			affiliateOwner.addAll( this.webJaguar.getAffiliates( 0, 0 ) );
    		}
    		map.put("affiliates", affiliateOwner);
    		map.put("numAffiliateInUse", this.webJaguar.affiliateCount());
    	}
    	
		if ((Boolean) gSiteConfig.get("gSUB_ACCOUNTS") && 
				this.webJaguar.getFamilyTree(customerForm.getCustomer().getId(), "down").size() > 1) {
			map.put("hasSubAccounts", true);
		}		
		
		List<SalesRep> salesReps = null;
		Integer salesRepId = null;
		
		if (this.webJaguar.getAccessUser(request).isSalesRepFilter()) {
			salesRepId = this.webJaguar.getAccessUser(request).getSalesRepId();
		}
		if (salesRepId == null) {
			salesReps = this.webJaguar.getSalesRepList();
		} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
			salesReps = this.webJaguar.getSalesRepList(salesRepId);
		} else {
			salesReps = new ArrayList<SalesRep>();
			salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
		}
		
		SalesRepSearch salesRepSearch1 = new SalesRepSearch();
		salesRepSearch1.setShowActiveSalesRep(true);
		if (salesRepId != null) {
			map.put("activeQualifiers", salesReps);
		} else {
			map.put("activeQualifiers", this.webJaguar.getSalesRepList(salesRepSearch1));
		}
		
		map.put("countries",this.webJaguar.getEnabledCountryList());
		map.put("mobileCarriers",this.webJaguar.getMobileCarrierList());
		map.put("states",this.webJaguar.getStateList("US"));
		map.put("caProvinceList", this.webJaguar.getStateList("CA"));
		map.put("paymentList", this.webJaguar.getCustomPaymentMethodList(null));
		map.put("accessUsers", this.webJaguar.getUserPrivilegeList(null));	

		if ((Boolean) gSiteConfig.get("gSHOPPING_CART") && (Boolean) gSiteConfig.get("gSALES_REP")){
			SalesRepSearch salesRepSearch = new SalesRepSearch();
			salesRepSearch.setShowActiveSalesRep(true);
			map.put("salesReps", this.webJaguar.getSalesRepList(salesRepSearch));
		}
		
		CrmQualifierSearch crmQualifierSearch = new CrmQualifierSearch();
		crmQualifierSearch.setSort("name");
		map.put("crmQualifierList", this.webJaguar.getCrmQualifierList(crmQualifierSearch));
		
		if ((Boolean) gSiteConfig.get("gBUDGET_BRAND")) {
			map.put("brands", this.webJaguar.getBrandMap().values());
		}
		
		// labels
		map.put("labels", this.webJaguar.getLabels("priceTable", "protected"));
		if(((String) gSiteConfig.get("gI18N")).length()>0){
			map.put("languageCodes", this.webJaguar.getLanguageSettings());
		}
		
		if (!customerForm.isNewCustomer()) {
			List<EchoSign> echoSigns = this.webJaguar.getEchoSignListByUserid(customerForm.getCustomer().getId());
			if (echoSigns.size() > 0) {
				for (EchoSign echoSign: echoSigns) {
					if (echoSign.getAgreementStatus() == null || echoSign.getAgreementStatus().equals("OUT_FOR_SIGNATURE")) {
						// get DocumentInfo
						try {
					    	EchoSignApi echoSignApi = new EchoSignApi();
					    	DocumentInfo documentInfo = echoSignApi.getDocumentInfo(echoSign.getApiKey(), echoSign.getDocumentKey());
					    	echoSign.setAgreementStatus(documentInfo.getStatus().toString());
					    	String versionKey = null;
					    	StringBuffer history = new StringBuffer();
					    	for (DocumentHistoryEvent event: documentInfo.getEvents()) {
					    	  versionKey = event.getDocumentVersionKey();
					    	  history.append(event.getDescription() + " on " + event.getDate() +
					    	    (versionKey == null ? "" : " (versionKey: " + versionKey + ")") + "\n");
					    	}
					    	echoSign.setHistory(history.toString());
					    	echoSign.setDocumentUrl(echoSignApi.getLatestDocumentUrl(echoSign.getApiKey(), echoSign.getDocumentKey()));
					    	this.webJaguar.updateEchoSign(echoSign);
						} catch (Exception e) {
							// do nothing
						}
					}
				}
			}
			map.put("echoSigns", echoSigns);
		}
		
		if ((Boolean) gSiteConfig.get( "gSEARCH_ENGINE_PROSPECT" ) && customerForm.getCustomer().getProspectId() != null) {
			map.put("searchEngineProspect", this.webJaguar.getNewestProspectInformation(customerForm.getCustomer().getProspectId()));
		}
		
		map.put( "randomNum", new Random().nextInt( 1000 ) );
		
		map.put("contactFields", this.webJaguar.getCrmContactFieldList(null));
		//order report
		map.put("customerOrderInfo", this.webJaguar.getCustomerOrderReportByUserID(customerForm.getCustomer().getId()));
		
		List<Address> addressList = this.webJaguar.getAllAddressList(customerForm.getCustomer().getId());
		map.put("addresses", addressList);
		
		PagedListHolder customerExtContactList = 
				new PagedListHolder(this.webJaguar.getCustomerExtContactListByUserid(customerForm.getCustomer().getId()));
			customerExtContactList.setPageSize(10);
			String page = request.getParameter("page");
			if (page != null) {
				customerExtContactList.setPage(Integer.parseInt(page)-1);
			}
			map.put("customerExtContacts", customerExtContactList);	
			//myModel.put("id", userId);	
		
		return map;
    }
    
    private void notifySalesRep(Map<String, Configuration> siteConfig, Customer customer) {
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	SalesRep salesRep = this.webJaguar.getSalesRepById( customer.getSalesRepId() );
    	StringBuffer message = new StringBuffer();
    	boolean isHtml=false;
    	String emailSubject=null;
    	EmailMessageForm form = new EmailMessageForm();
    	form.setHtml(false);
    	form.setSubject("Account Manager Notification");
    	int messageId;
    	if(siteConfig.get( "SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION" ).getValue()!=null){
    		messageId= Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION" ).getValue() );
    	}else{
    		messageId=0;       	
    	}
    	SiteMessage siteMessage = this.webJaguar.getSiteMessageById( messageId );
		if(	siteMessage!=null&&!siteMessage.equals("")){
			
	    	if(siteMessage.isHtml()==true){
	    		form.setHtml(true);
	    	}
	    	message.append(siteMessage.getMessage());
	    	form.setSubject(siteMessage.getMessageName());
		}else{
			message.append("Hello #salesRepName#");
			message.append(",\n\n");
			message.append("This is to inform you that a new Customer #firstname# is assigned to you. \n\n");
			message.append("Dialing Notes: #dialingNotes# \n");
			message.append("Thanks \n");
			
		}
		form.setMessage(message.toString());
		form.setTo(salesRep.getEmail());
		form.setFrom(siteConfig.get("CONTACT_EMAIL").getValue());
		try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			this.webJaguar.replaceDynamicElement(form, customer, salesRep);
			helper.setTo(form.getTo());
			helper.setFrom(form.getFrom());
			helper.setSubject(form.getSubject());
			
			if ( form.isHtml()==true ) { 
	    		helper.setText(form.getMessage(), true );
	    	} else {
	    		helper.setText(form.getMessage());
	    	}

			mailSender.send(mms);
		} catch (Exception ex) {
			if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
				ex.printStackTrace();				
			}
		}    				
		
	}
    
    private void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream( src );
		OutputStream out = new FileOutputStream( dst );

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ( (len = in.read( buf )) > 0 ) {
			out.write( buf, 0, len );
		}
		in.close();
		out.close();
	}
}