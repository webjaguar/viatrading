package com.webjaguar.web.admin.customer;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.CustomerCreditHistory;
import com.webjaguar.model.Order;
import com.webjaguar.model.Payment;
import com.webjaguar.model.PaymentSearch;

public class CancelPaymentController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> model = new HashMap<String, Object>();	
		
		PaymentSearch search = getPaymentSearch( request );
		
		if(request.getParameter("orderId") != null) {
			search.setOrderId( Integer.parseInt(request.getParameter("orderId")));
			
			
			if(request.getParameter("__cancel") != null ){
				String paymentIds[] = request.getParameterValues("__selected_paymentId");
				

				if (paymentIds != null && paymentIds.length > 0) {
					for (int i=0; i<paymentIds.length; i++) {
						Payment payment = this.webJaguar.getCustomerPaymentById(Integer.parseInt(paymentIds[i]));
						
						Map<Integer, Order> invoices = new HashMap<Integer, Order>();
						Order invoice = this.webJaguar.getOrder(Integer.parseInt(request.getParameter("orderId")), null);
						
						Boolean previousPaid = false;
						if(payment!=null && invoice!=null){
							if(this.webJaguar.checkPreviousPaid(payment.getId(), invoice.getOrderId())==1){
								previousPaid = true;
							}
						}
						
						// set invoice payment to cancelled
						invoice.setCancelledPayment(true);
						invoice.setPaymentCancelledAmt(this.webJaguar.getPaymentAmount(Integer.parseInt(paymentIds[i]), Integer.parseInt(request.getParameter("orderId"))));
						invoice.setPaymentCancelledBy(this.webJaguar.getAccessUser(request).getUsername());
						invoice.setPayment(0.00);	
						
						if(!previousPaid){
						
							//Update customer's credit
							if(payment.getPaymentMethod().equalsIgnoreCase("vba credit")) {
								CreditImp credit = new CreditImp();
								
								
								CustomerCreditHistory customerCredit= credit.updateCustomerCreditByOrderPayment(invoice, "cancel", payment.getAmount(), this.webJaguar.getAccessUser(request).getUsername());
								this.webJaguar.updateCredit(customerCredit);
								
								
								//Update credit used in orders to old credit used - cancelled amount.
								invoice.setCreditUsed(invoice.getCreditUsed() - invoice.getPaymentCancelledAmt());
								
								
								payment.setCancelPayment(true);
								payment.setCancelledAmt(payment.getAmount());
								payment.setCancelledBy(this.webJaguar.getAccessUser(request).getUsername());
								
							}
							
							//invoices MAP to updateCustomerPayment
							invoices.put( Integer.parseInt(request.getParameter("orderId")), invoice);
							this.webJaguar.updateCustomerPayment(payment, invoices.values());
						}
					}
				}
			}

			PagedListHolder paymentList = new PagedListHolder(this.webJaguar.getCustomerPaymentListByOrder(search));
			
			paymentList.setPageSize(search.getPageSize());
			paymentList.setPage(search.getPage()-1);
			
			model.put("orderId", Integer.parseInt(request.getParameter("orderId")));
			model.put("paymentOrdersList" , paymentList);
		} else {
			return new ModelAndView(new RedirectView("payments.jhtm"));
		}
		return new ModelAndView("admin/customers/cancelPayments", "model", model);
	}
	
	private PaymentSearch getPaymentSearch(HttpServletRequest request) {

		PaymentSearch search = (PaymentSearch) request.getSession().getAttribute( "paymentSearch" );
		if (search == null) {
			search = new PaymentSearch();
			request.getSession().setAttribute( "paymentSearch", search );
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}			
		
		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		return search;		
	}
}