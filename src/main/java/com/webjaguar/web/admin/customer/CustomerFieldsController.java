/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.12.2007
 */

package com.webjaguar.web.admin.customer;

import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.webjaguar.web.form.CustomerFieldsForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CustomerField;

public class CustomerFieldsController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public CustomerFieldsController() {
		setSessionForm(true);
		setCommandName("customerFieldsForm");
		setCommandClass(CustomerFieldsForm.class);
		setFormView("admin/customers/fieldsForm");
		setSuccessView("admin/customers/fieldsForm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {

		CustomerFieldsForm form = (CustomerFieldsForm) command;

		for (CustomerField customerField:form.getCustomerFields()) {
			customerField.setName( ServletRequestUtils.getStringParameter( request, "__name_" + customerField.getId() ) );
			customerField.setNameEs( ServletRequestUtils.getStringParameter( request, "__nameEs_" + customerField.getId() ) );
			customerField.setPreValue( ServletRequestUtils.getStringParameter( request, "__preValue_" + customerField.getId() ) );
			customerField.setEnabled( ServletRequestUtils.getBooleanParameter( request, "__enabled_" + customerField.getId(), false ) );
			customerField.setRequired( ServletRequestUtils.getBooleanParameter( request, "__required_" + customerField.getId(), false ) );
			customerField.setPublicField( ServletRequestUtils.getBooleanParameter( request, "__publicField_" + customerField.getId(), false ) );
			customerField.setShowOnInvoice( ServletRequestUtils.getBooleanParameter( request, "__showOnInvoice_" + customerField.getId(), false ) );
			customerField.setShowOnInvoiceExport( ServletRequestUtils.getBooleanParameter( request, "__showOnInvoiceExport_" + customerField.getId(), false ) );
			customerField.setMultiSelecOnAdmin( ServletRequestUtils.getBooleanParameter( request, "__multiSelecOnAdmin_" + customerField.getId(), false ) );
		}
		
		this.webJaguar.updateCustomerFields( form.getCustomerFields() );

    	Map<String, Object> model = new HashMap<String, Object>();		  	
		model.put(getCommandName(), form);
		model.put("message", "update.successful");
		
		return new ModelAndView(getSuccessView(), model);
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		CustomerFieldsForm form = new CustomerFieldsForm();	
		form.setCustomerFields( this.webJaguar.getCustomerFields() );
        return form;		
	}

}
