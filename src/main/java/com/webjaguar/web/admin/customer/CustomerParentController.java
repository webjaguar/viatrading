/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 08.28.2007
 */

package com.webjaguar.web.admin.customer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;

public class CustomerParentController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
		
		CustomerSearch customerSearch = getCustomerSearch(request);
		Map<String, Object> model = new HashMap<String, Object>();

		// check if cancel button was pressed
		if (request.getParameter("__cancel") != null) {
			return new ModelAndView(new RedirectView("customerList.jhtm"));
		}
		
		Customer customer = this.webJaguar.getCustomerById(ServletRequestUtils.getIntParameter(request, "cid", 0));
		if (customer != null) {
			model.put("customer", customer);
			model.put("parent", this.webJaguar.getCustomerById(customer.getParent()));
		} else {
			return new ModelAndView(new RedirectView("customerList.jhtm"));			
		}
		
		// check if update button was pressed
		if (request.getParameter("__update") != null) {
			int parentId = ServletRequestUtils.getIntParameter(request, "pid", 0);
			if (parentId == 0 || parentId == customer.getId()) {
				customer.setParent(null);									
			} else {
				// check if parent id will cause a loop
				customer.setParent(parentId);
				for (Customer node: this.webJaguar.getFamilyTree(customer.getId(), "down")) {
					if (parentId == node.getId()) {
						customer.setParent(null);
						break;
					}
				}
			}
			this.webJaguar.updateParent(customer);
			return new ModelAndView(new RedirectView("customerList.jhtm"));
		}
		
		if (request.getParameter("begin") != null) {
			if (customer.getParent() != null) {
				List <Customer> familyTree = this.webJaguar.getFamilyTree(customer.getParent(), "up");
				if (familyTree.size() > 1) {
					customerSearch.setParent(familyTree.get(familyTree.size()-2).getId());					
				} else {
					customerSearch.setParent(-1);					
				}
			} else {
				customerSearch.setParent(-1);				
			}
		}		

		int count = this.webJaguar.getCustomerListByParentCount(customerSearch);
		if (count < customerSearch.getOffset()-1) {
			customerSearch.setPage(1);
		}
		customerSearch.setLimit(customerSearch.getPageSize());
		customerSearch.setOffset((customerSearch.getPage()-1)*customerSearch.getPageSize());
		List<Customer> custList = this.webJaguar.getCustomerListByParent(customerSearch);
		int pageCount = count/customerSearch.getPageSize();
		if (count%customerSearch.getPageSize() > 0) {
			pageCount++;
		}
		model.put("pageCount", pageCount);
		model.put("pageEnd", customerSearch.getOffset()+custList.size());
		model.put("count", count);
		
		if (customerSearch.getParent() != null) {
			model.put("familyTree", this.webJaguar.getFamilyTree(customerSearch.getParent(), "up"));			
		}
		model.put("customers", custList);
    	
        return new ModelAndView("admin/customers/parentForm", "model", model);
	}

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private CustomerSearch getCustomerSearch(HttpServletRequest request) {
		CustomerSearch search = (CustomerSearch) request.getSession().getAttribute("customerParentSearch");
		if (search == null) {
			search = new CustomerSearch();
			request.getSession().setAttribute("customerParentSearch", search);
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}
		
		// parent ID
		if (request.getParameter("parent") != null) {
			int parentId = ServletRequestUtils.getIntParameter(request, "parent", 0);
			if (parentId == -1) {
				search.setParent(-1);				
			} else if (parentId > 0) {
				search.setParent(parentId);
			} else {
				search.setParent(null);
			}
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);				
			}
		}			
		
		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				search.setPageSize(10);
			} else {
				search.setPageSize(size);				
			}
		}
		
		return search;
	}	
	
}
