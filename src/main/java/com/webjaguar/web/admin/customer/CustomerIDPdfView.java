/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 05.18.2009
 */

package com.webjaguar.web.admin.customer;


import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Rectangle;
import com.webjaguar.model.Customer;
import com.webjaguar.model.SalesRep;

public class CustomerIDPdfView extends AbstractPdfView{
	
	protected void buildPdfDocument(Map model,
			Document document,
			PdfWriter writer,
			HttpServletRequest request,
			HttpServletResponse response)
			throws Exception {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
		Font font2 = new Font(Font.HELVETICA, 8, Font.NORMAL);
    	Font font3 = new Font(Font.HELVETICA, 8, Font.BOLD);
    	Font font5 = new Font(Font.HELVETICA, 13, Font.BOLD);
    	Rectangle pageSize = new Rectangle(234,144);
    	document.setPageSize(pageSize);
    	document.setMargins(0f, 0f, 1f, 0f);
    	//(Rectangle pageSize, float marginLeft, float marginRight, float marginTop, float marginBottom) 
    	
    	ApplicationContext context = (ApplicationContext) model.get( "context" );
		Customer customer = (Customer) model.get( "customer" );
		SalesRep salesRep = (SalesRep) model.get( "salesRep" );
		/*
		File baseFile = new File( getServletContext().getRealPath( "/assets/Image/" ) );

		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				baseFile = new File( (String) prop.get( "site.root" ) + "/assets/Image/" );
			}
		}
		catch ( Exception e )
		{}
		Image logo = null;
		try {
			logo = Image.getInstance( baseFile + "/Layout/idCard_logo.jpg" );
		} catch ( FileNotFoundException e )
		{}
		*/
		PdfContentByte cb = writer.getDirectContent();
		PdfPTable mainTable = new PdfPTable(2);

		mainTable.setTotalWidth(395f);
		mainTable.setLockedWidth(true);
		mainTable.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable leftTable = new PdfPTable(3);
		leftTable.setTotalWidth(190f);
		leftTable.setLockedWidth(true);
		leftTable.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		PdfPTable rightTable = new PdfPTable(1);
		rightTable.setTotalWidth(55f);
		rightTable.setLockedWidth(true);
		rightTable.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		
		PdfPCell cell = new PdfPCell();
    	/*cell.setHorizontalAlignment(Element.ALIGN_JUSTIFIED_ALL);
    	cell.setBorderWidth( 0 );
    	cell.setPaddingLeft( 57 );
    	cell.setPaddingRight( 20 );
    	//cell.addElement( logo );
    	cell.setFixedHeight( 60f );
    	cell.setColspan( 4 );
    	//table.addCell(cell);
    	 */
    	
    	
    	cell = new PdfPCell(new Phrase("Via Trading Customer Card", font5));
    	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    	cell.setBorderWidth( 0 );
    	cell.setColspan( 3 );
    	leftTable.addCell(cell);
    	
    	cell = new PdfPCell(new Phrase("Tarjeta De Cliente", font3));
    	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    	cell.setBorderWidth( 0 );
    	cell.setColspan( 3 );
    	leftTable.addCell(cell);
    	
    	cell = new PdfPCell(new Phrase("", font3));
    	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    	cell.setBorderWidth( 0 );
    	cell.setFixedHeight(6f);
    	cell.setColspan( 3 );
    	leftTable.addCell(cell);
    	
    	cell = new PdfPCell(new Phrase(context.getMessage("Name", new Object[0], RequestContextUtils.getLocale(request)) + ": ", font3));
    	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    	cell.setBorderWidth( 0 );
    	cell.setPaddingLeft(10f);
    	leftTable.addCell(cell);
    	cell = new PdfPCell(new Phrase(customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName(), font3));
    	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    	cell.setBorderWidth( 0 );
    	cell.setPaddingLeft(15f);
    	cell.setColspan(2);
    	leftTable.addCell(cell);
    	
    	cell = new PdfPCell(new Phrase(context.getMessage("company", new Object[0], RequestContextUtils.getLocale(request)) + ": ", font3));
    	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    	cell.setBorderWidth( 0 );
    	cell.setPaddingLeft(10f);
    	leftTable.addCell(cell);
    	cell = new PdfPCell(new Phrase(customer.getAddress().getCompany(), font3));
    	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    	cell.setBorderWidth( 0 );
    	cell.setPaddingLeft(15f);
    	cell.setColspan(2);
    	leftTable.addCell(cell);
    	
    	cell = new PdfPCell(new Phrase(context.getMessage("salesRep", new Object[0], RequestContextUtils.getLocale(request)) + ": ", font3));
    	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    	cell.setBorderWidth( 0 );
    	cell.setPaddingLeft(10f);
    	leftTable.addCell(cell);
    	cell = new PdfPCell(new Phrase(( customer.getSalesRepId() == null ) ? "" : salesRep.getName().toString(), font3));
    	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    	cell.setBorderWidth( 0 );
    	cell.setPaddingLeft(15f);
    	cell.setColspan(2);
    	leftTable.addCell(cell);
    	
    	cell = new PdfPCell(new Phrase("Tax Status: ", font3));
    	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    	cell.setBorderWidth( 0 );
    	cell.setPaddingLeft(10f);
    	leftTable.addCell(cell);
    	if (customer.getField20() != null && customer.getField20().trim().length() > 0) {
        	cell = new PdfPCell(new Phrase(customer.getField20().trim(), font3));    		
    	} else {
        	cell = new PdfPCell(new Phrase("", font3));
    	}
    	//cell = new PdfPCell(new Phrase(( customer.getTaxId() == null || customer.getTaxId().trim().length() < 1 ) ? "Taxable" : "Not Taxable", font3));
    	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    	cell.setBorderWidth( 0 );
    	cell.setPaddingLeft(15f);
    	cell.setColspan(2);
    	leftTable.addCell(cell);
    	
    	cell = new PdfPCell(new Phrase("", font3));
    	cell.setHorizontalAlignment(Element.ALIGN_CENTER);
    	cell.setBorderWidth( 0 );
    	cell.setColspan( 3 );
    	cell.setFixedHeight(6f);
    	leftTable.addCell(cell);
    	
    	cell = new PdfPCell(new Paragraph("Member Since:\n" + dateFormatter.format(customer.getCreated()).toString(), font2));
    	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    	cell.setBorderWidth( 0 );
    	cell.setPaddingLeft(10f);
    	leftTable.addCell(cell);
    	cell = new PdfPCell();
    	cell.setHorizontalAlignment(Element.ALIGN_LEFT);
    	cell.setBorderWidth( 0 );
    	cell.setPaddingLeft(19f);
    	cell.setColspan(2);
    	cell.setFixedHeight(34f);
    	cell.addElement(generateBarcode(customer).createImageWithBarcode( cb, null, null ));
    	leftTable.addCell(cell);
    	
    	cell = new PdfPCell();
    	Image barcodeImage = generateBarcode(customer).createImageWithBarcode( cb, null, null );
		barcodeImage.setRotation((float)Math.PI / 2);
		cell.addElement( barcodeImage );
		cell.setFixedHeight(135f);
		cell.setPaddingBottom(20f);
		cell.setPaddingTop(25f);
		cell.setPaddingLeft(1f);
		cell.setBorderWidth( 0 );
    	rightTable.addCell(cell);
    	
    	
    	PdfPCell mainCell = new PdfPCell();
    	mainCell.addElement(leftTable);
    	mainCell.setBorderWidth(0);
    	mainCell.setFixedHeight(132f);
    	mainCell.setPaddingLeft(1f);
    	mainCell.setPaddingTop(10f);
    	mainCell.setTop(1f);
    	mainTable.addCell(mainCell);
		
    	mainCell = new PdfPCell();
    	mainCell.addElement(rightTable);
     	mainCell.setBorderWidth(0);
    	mainCell.setFixedHeight(140f);
    	mainCell.setPaddingLeft(1f);
    	mainCell.setPaddingTop(10f);
    	mainTable.addCell(mainCell);
    	document.open();
		document.add(mainTable);
		document.close();
	}
	
	private Barcode128 generateBarcode(Customer customer) {
		Barcode128 code128 = new Barcode128(); 
		code128.setBarHeight( code128.getSize() * 3.1f );
		if (customer.getCardId() == null) {
			code128.setCode( "0000000000000" );
		} else {
			code128.setCode( customer.getCardId() );
		}
		code128.setBaseline( 12f );
		code128.setSize( 12f );
		return code128;
	}
}