/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.14.2009
 */

package com.webjaguar.web.admin.customer;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.BudgetProduct;
import com.webjaguar.model.BudgetProductSearch;
import com.webjaguar.model.Product;

public class BudgetProductFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public BudgetProductFormController() {
		setCommandName("budgetProduct");
		setCommandClass(BudgetProduct.class);
		setFormView("admin/customers/budgetProductForm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		BudgetProduct budgetProduct = (BudgetProduct) command;
		
		if (request.getParameter("_delete") != null) {
			// check if delete button was pressed
			this.webJaguar.deleteBudgetProduct(budgetProduct.getId());
		} else if (request.getParameter("_cancel") != null) {
			// check if cancel button was pressed
		} else {
			if (budgetProduct.getId() != null) {
				this.webJaguar.updateBudgetProduct(budgetProduct);
			} else {
				this.webJaguar.insertBudgetProduct(budgetProduct);
			}			
		}
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("cid", budgetProduct.getUserId());
		params.put("sku", budgetProduct.getProduct().getSku());
		
		BudgetProductSearch search = (BudgetProductSearch) request.getSession().getAttribute("budgetProductSearch");
		if (search != null && budgetProduct.getEntered() != null) {
			Calendar entered = new GregorianCalendar();
			entered.setTime(budgetProduct.getEntered());
			search.setYear(entered.get(Calendar.YEAR));
		}
		
		return new ModelAndView(new RedirectView("budgetByProducts.jhtm"), params);
	}

	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(df, false, 10));
	}
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}
	
	 protected Map referenceData(HttpServletRequest request) throws Exception {
		 Map<String, Object> map = new HashMap<String, Object>();
		 map.put("customer", this.webJaguar.getCustomerById(ServletRequestUtils.getIntParameter(request, "cid", -1)));
		 
		 return map;
	 }  
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		BudgetProduct form = this.webJaguar.getBudgetProduct(ServletRequestUtils.getIntParameter(request, "id", -1));
		if (form == null) {
			form = new BudgetProduct();
			form.setUserId(ServletRequestUtils.getIntParameter(request, "cid", -1));
			if (this.webJaguar.getCustomerById(form.getUserId()) == null) {
				// invalid user
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("customerList.jhtm")));
			}
			form.setProduct(new Product(ServletRequestUtils.getStringParameter(request, "productSku", ""), this.webJaguar.getAccessUser(request).getUsername()));
		}
		
		return form;
	}
	
}
