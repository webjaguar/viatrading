/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.06.2007
 */

package com.webjaguar.web.admin.customer;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.dao.ibatis.IbatisConfigDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Affiliate;
import com.webjaguar.model.BrokerImage;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SalesRepSearch;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmQualifierSearch;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.CustomerForm;
import com.webjaguar.web.form.EmailMessageForm;

public class AddCustomerFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private Map<String, Configuration> siteConfig;
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }

	public AddCustomerFormController() {
		setSessionForm(true);
		setCommandName("customerForm");
		setCommandClass(CustomerForm.class);
		setFormView("admin/customers/form");
		setSuccessView("customerList.jhtm");
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception{

		CustomerForm customerForm = (CustomerForm) command;

		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		//upload Broker Logo
		if(customerForm.getCustomer().getCustomerType() != null && customerForm.getCustomer().getCustomerType() == 1){
			//Save Broker Image
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile image_file = multipartRequest.getFile( "image_file_1" );
			File baseFile = new File( getServletContext().getRealPath( "/assets/Image/Customer" ) );
			BrokerImage image = new BrokerImage();
			if(image_file != null && !image_file.isEmpty()){
				File uploadFile = new File(baseFile.getAbsolutePath(), image_file.getOriginalFilename());
				try {
					image_file.transferTo(uploadFile);
					image.setImageUrl(image_file.getOriginalFilename());
					customerForm.getCustomer().setBrokerImage(image);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
			
			
			//remove Broker Image
			if( request.getParameter("__clear_image_1") != null){
				if (customerForm.getCustomer().getBrokerImage().getImageUrl() != null && customerForm.getCustomer().getBrokerImage().getImageUrl() != "") {
					File deleteFile = new File(baseFile.getAbsolutePath(), customerForm.getCustomer().getBrokerImage().getImageUrl());
					if(deleteFile.exists()) {
						deleteFile.delete();
					}
					customerForm.getCustomer().setRemoveBrokerImage(true);
				}
			}
		}	
		
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile image_file = multipartRequest.getFile( "image_file_1" );
		File baseFile = new File( getServletContext().getRealPath( "/assets/Image/CustomerImage" ) );
		BrokerImage image = new BrokerImage();
		if(image_file != null && !image_file.isEmpty()){
			File uploadFile = new File(baseFile.getAbsolutePath(), image_file.getOriginalFilename());
			try {
				image_file.transferTo(uploadFile);
				image.setImageUrl(image_file.getOriginalFilename());
				customerForm.getCustomer().setBrokerImage(image);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}

		// protected access
		if (customerForm.getProtectedAccess() != null) {
			StringBuffer protectedAccess = new StringBuffer();
			for (int i = 0; i < customerForm.getProtectedAccess().length; i++) {
				if (request.getParameter("__enabled_" + i) != null) {
					protectedAccess.insert(0, '1');
				} else {
					protectedAccess.insert(0, '0');
				}
			}
			customerForm.getCustomer().setProtectedAccess(protectedAccess.toString());
		}
		if (customerForm.getCustomer().getAffiliateParent() == null)
			customerForm.getCustomer().setAffiliateParent(0);
		if (customerForm.getCustomer().getRegisteredBy() == null) {
			if ((request.getAttribute("accessUser") != null)) {
				AccessUser accessUser = (AccessUser) request.getAttribute("accessUser");
				customerForm.getCustomer().setRegisteredBy(accessUser.getUsername());
			} else {
				customerForm.getCustomer().setRegisteredBy(getApplicationContext().getMessage("admin", new Object[0], RequestContextUtils.getLocale(request)));
			}
		}
		// generate random cardID
		generateRandomCardID(customerForm.getCustomer());
		if (siteConfig.get("CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER").getValue().equals("true")) {
			customerForm.getCustomer().setAutoGenerateAccountNumber(true);
		} else {
			customerForm.getCustomer().setAutoGenerateAccountNumber(false);
		}
		
		if(customerForm.getCustomer().getId()!=null){
			int customerId=customerForm.getCustomer().getId();
			Customer customer = this.webJaguar.getCustomerById(customerId);
			if(customerForm.getCustomer().getSalesRepId()!=null &&(customer.getSalesRepId()==null||!customer.getSalesRepId().equals(customerForm.getCustomer().getSalesRepId()))){
				notifySalesRep(siteConfig, customerForm.getCustomer());
			}
		}else{
			if(customerForm.getCustomer().getSalesRepId()!=null ){
				notifySalesRep(siteConfig, customerForm.getCustomer());
			}
		}
		
		if (ServletRequestUtils.getIntParameter(request, "contactId") != null) {
			CrmContact crmContact = this.webJaguar.getCrmContactById(ServletRequestUtils.getIntParameter(request, "contactId"));
			customerForm.getCustomer().setQualifier(crmContact.getQualifier());
			if (crmContact != null) {
				if("liquidate now".equals(crmContact.getLeadSource())){
					customerForm.getCustomer().setRegisteredBy("LiquidateNowConv");
				}else if("LandingPage".equals(crmContact.getLeadSource())){
					customerForm.getCustomer().setRegisteredBy("LandingPageConv");
				}
			}
		}
		
		
		Class<Customer> c = Customer.class;
		
		for (int i = 1; i <= 20; i++) {
			String[] valueSet = request.getParameterValues("customer.field" + i + "Set");
			StringBuilder sb= new StringBuilder();

			if (valueSet != null && valueSet.length > 0) {	
				
				for(String fieldVal : valueSet) {
					sb.append(fieldVal).append(",");
				}
				
				Method m =c.getMethod("setField"+i,String.class);
				m.invoke(customerForm.getCustomer(), sb.toString().substring(0, sb.length() - 1));
			}
		}		
		
		
		this.webJaguar.insertCustomer(customerForm.getCustomer(), null, ((Boolean) gSiteConfig.get("gCRM") && request.getParameter("contactId") == null));
		
		
		if (ServletRequestUtils.getIntParameter(request, "contactId") != null) {
			if(customerForm.getCustomer().getId()!=null){
				this.webJaguar.updateDialingNotesCrmToCustomer(ServletRequestUtils.getIntParameter(request,"contactId"), customerForm.getCustomer().getId());
			}
			this.webJaguar.convertCrmContactToCustomer(ServletRequestUtils.getIntParameter(request, "contactId"), customerForm.getCustomer().getId(), null);
			
			CrmContact crmContact = this.webJaguar.getCrmContactById(ServletRequestUtils.getIntParameter(request, "contactId"));
			if (crmContact != null) {
				if("liquidate now".equals(crmContact.getLeadSource())){
					
					crmContact.setLeadSource("LiquidateNowConv");
					customerForm.getCustomer().setRegisteredBy("LiquidateNowConv");
					this.webJaguar.updateCrmContact(crmContact);
				}else if("LandingPage".equals(crmContact.getLeadSource())){
					
					crmContact.setLeadSource("LandingPageConv");
					customerForm.getCustomer().setRegisteredBy("LandingPageConv");
					this.webJaguar.updateCrmContact(crmContact);
				}
			}
		}else{
			customerForm.getCustomer().getNewDialingNote().setCustomerId(customerForm.getCustomer().getId());
			customerForm.getCustomer().getNewDialingNote().setCrmId(customerForm.getCustomer().getCrmContactId());
			this.webJaguar.insertDialingNoteHistory(customerForm.getCustomer().getNewDialingNote());
		}
		
		SiteMessage siteMessage = null;
		try {
			Integer messageId = null;
			Integer newRegistrationId;
			String languageCode = customerForm.getCustomer().getLanguageCode().toString();
			try {
				newRegistrationId = this.webJaguar.getLanguageSetting(languageCode).getNewRegistrationId();
			} catch (Exception e) {
				newRegistrationId = null;
			}
			if (languageCode.equalsIgnoreCase((LanguageCode.en).toString()) || newRegistrationId == null) {
				messageId = Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_NEW_REGISTRATION").getValue());
			} else {
				messageId = Integer.parseInt(newRegistrationId.toString());
			}
			siteMessage = this.webJaguar.getSiteMessageById(messageId);
			if (siteMessage != null) {
				// send email
				sendEmail(siteMessage, request, customerForm.getCustomer());
			}
		} catch (NumberFormatException e) {
			// do nothing
			e.printStackTrace();
		}
		return new ModelAndView(new RedirectView(getSuccessView()));
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;
		}
		return false;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		CustomerForm customerForm = new CustomerForm();
		customerForm.setNewCustomer(true);
		if (!customerForm.getCustomer().getIsAffiliate()) {
			customerForm.getCustomer().setAffiliate(new Affiliate());
		}
		// protected access
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels > 0) {
			boolean protectedAccess[] = new boolean[protectedLevels];
			for (int i = 0; i < protectedLevels; i++) {
				protectedAccess[i] = false;
			}

			String defaultProtectedAccess = siteConfig.get("PROTECTED_ACCESS_NEW_REGISTRANT").getValue();
			// Default protected access
			for (int i = 0; i < defaultProtectedAccess.length(); i++) {
				if (defaultProtectedAccess.charAt(i) == '1') {
					protectedAccess[defaultProtectedAccess.length() - 1 - i] = true;
				}
			}
			customerForm.setProtectedAccess(protectedAccess);
		}
		if (((Configuration) siteConfig.get("CUSTOMERS_REQUIRED_COMPANY")).getValue().equals("true")) {
			customerForm.setCompanyRequired(true);
		}

		if (ServletRequestUtils.getIntParameter(request, "contactId") != null) {
			CrmContact crmContact = this.webJaguar.getCrmContactById(ServletRequestUtils.getIntParameter(request, "contactId"));
			if (crmContact != null) {
				this.webJaguar.crmContactToCustomerSynch(crmContact, customerForm.getCustomer(), false);
			}
		}
		// Customer Fields
		List<CustomerField> customerFields = this.webJaguar.getCustomerFields();
		Iterator iter = customerFields.iterator();
		while (iter.hasNext()) {
			CustomerField customerField = (CustomerField) iter.next();
			if (!customerField.isEnabled()) {
				iter.remove();
			}
		}
		customerForm.setCustomerFields(customerFields);

		if (siteConfig.get("ADDRESS_RESIDENTIAL_COMMERCIAL").getValue().equals("1") || siteConfig.get("ADDRESS_RESIDENTIAL_COMMERCIAL").getValue().equals("3")) {
			customerForm.getCustomer().getAddress().setResidential(true);
		}
		// remove this code whemn viatading is moved to its own branch
		if (siteConfig.get("SITE_URL").getValue().contains("viatrading") || siteConfig.get("SITE_URL").getValue().contains("test.wjserver180.com") || siteConfig.get("SITE_URL").getValue().contains("localhost")) {
			customerForm.getCustomer().getAddress().setLiftGate(true);
		}

		return customerForm;

	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		CustomerForm customerForm = (CustomerForm) command;
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		if (customerForm.getProtectedAccess() != null) {
			for (int i = 0; i < customerForm.getProtectedAccess().length; i++) {
				if (request.getParameter("__enabled_" + i) != null) {
					customerForm.getProtectedAccess()[i] = true;
				} else {
					customerForm.getProtectedAccess()[i] = false;
				}
			}
		}
		// password
		if (customerForm.getCustomer().getPassword().length() < 5) {
			Object[] errorArgs = { new Integer(5) };
			errors.rejectValue("customer.password", "form.charLessMin", errorArgs, "should be at least 5 chars");
		}

		if (this.webJaguar.getCustomerByUsername(customerForm.getCustomer().getUsername()) != null) {
			errors.rejectValue("customer.username", "USER_ID_ALREADY_EXISTS", "An account using this email address already exists.");
		}
		// check if there is a promoCode
		if ((Integer) gSiteConfig.get("gAFFILIATE") > 0) {
			if (customerForm.getCustomer().getAffiliate().getPromoTitle() != null && !"".equals(customerForm.getCustomer().getAffiliate().getPromoTitle())) {
				customerForm.getCustomer().getAffiliate().setPromoTitle(customerForm.getCustomer().getAffiliate().getPromoTitle().trim());
				try {
					Integer id = this.webJaguar.getUserIdByPromoCode(customerForm.getCustomer().getAffiliate().getPromoTitle());
					if (id != null) { // sku exists
						if (customerForm.isNewCustomer()) {
							errors.rejectValue("customer.affiliate.promoTitle", "PROMO_ALREADY_EXISTS", "Another Customer using this Promo code.");
						} else if (id.compareTo(customerForm.getCustomer().getId()) != 0) { // not the same item
							errors.rejectValue("customer.affiliate.promoTitle", "PROMO_ALREADY_EXISTS", "Another Customer using this Promo code.");
						}
					}
				} catch (Exception e) {
					// do nothing
				}
			}
		}

		if (customerForm.getCustomerFields() != null) {
			for (CustomerField customerField : customerForm.getCustomerFields()) {
				if (customerField.isRequired())
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.field" + customerField.getId() + "Set", "form.required", "required");
			}
		}

		// supplier
		if (customerForm.getCustomer().isConvertToSupplier()) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.supplierPrefix", "required");
			// If customer is converted to supplier, company should not be empty.
			if (customerForm.getCustomer().getAddress().getCompany().equals(null) || customerForm.getCustomer().getAddress().getCompany().equals("")) {
				customerForm.getCustomer().getAddress().setCompany(customerForm.getCustomer().getAddress().getFirstName().concat(customerForm.getCustomer().getAddress().getLastName()));
			} else if (!customerForm.getCustomer().getAddress().getCompany().equals(null)) {
				customerForm.getCustomer().getAddress().setCompany(customerForm.getCustomer().getAddress().getCompany() + "-" + customerForm.getCustomer().getAddress().getFirstName());
			}
			if (customerForm.getCustomer().getSupplierId() == null) {
				// new supplier
				customerForm.getCustomer().setSupplierPrefix(customerForm.getCustomer().getSupplierPrefix().trim());
				// check if existing already
				if (customerForm.getCustomer().getSupplierPrefix().length() < 4) {
					Object[] errorArgs = { new Integer(4) };
					errors.rejectValue("customer.supplierPrefix", "form.charLessMin", errorArgs, "should be more than 4 chars");
				} else if (this.webJaguar.getCustomerIdBySupplierPrefix(customerForm.getCustomer().getSupplierPrefix()) != null) {
					// existing already
					errors.rejectValue("customer.supplierPrefix", "SUPPLIER_PREFIX_ALREADY_EXISTS");
				}
			}
		} else {
			customerForm.getCustomer().setSupplierPrefix(null);
		}

		// check if CRM contact already exist
		if ((Boolean) gSiteConfig.get("gCRM")) {
			if (ServletRequestUtils.getIntParameter(request, "contactId") == null) {
				List<CrmContact> crmContactList = this.webJaguar.getCrmContactByEmail1(customerForm.getCustomer().getUsername());
				if (crmContactList != null && !crmContactList.isEmpty()) {
					errors.rejectValue("customer.username", "USER_ALREADY_EXISTS_ON_CRM", "Account exists on CRM. Convert contact to customer from CRM");
				}
			}
		}
	}

	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		IbatisConfigDao nextIndex = new IbatisConfigDao();
		Map<String, Object> map = new HashMap<String, Object>();
		List<Affiliate> affiliateOwner = new Vector();
		if ((Integer) gSiteConfig.get("gAFFILIATE") > 0) {
			// add root with id = 0
			affiliateOwner.add(new Affiliate(0));
			affiliateOwner.addAll(this.webJaguar.getAffiliates(0, 0));
			map.put("affiliates", affiliateOwner);
			map.put("numAffiliateInUse", this.webJaguar.affiliateCount());
		}
		Constants constant = new Constants();
		if (siteConfig.get("CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER").getValue().equals("true")) {
			map.put("accountNumber", this.webJaguar.getNextIndexByName(constant.nextCustomerAccountName));
		}
		map.put("mobileCarriers",this.webJaguar.getMobileCarrierList());
		map.put("countries", this.webJaguar.getEnabledCountryList());
		map.put("states", this.webJaguar.getStateList("US"));
		map.put("caProvinceList", this.webJaguar.getStateList("CA"));
		map.put("labels", this.webJaguar.getLabels("priceTable"));
		map.put("paymentList", this.webJaguar.getCustomPaymentMethodList(null));
		if(((String) gSiteConfig.get("gI18N")).length()>0){
			map.put("languageCodes", this.webJaguar.getLanguageSettings());
		}
		if ((Boolean) gSiteConfig.get("gSHOPPING_CART") && (Boolean) gSiteConfig.get("gSALES_REP")){
			SalesRepSearch salesRepSearch = new SalesRepSearch();
			salesRepSearch.setShowActiveSalesRep(true);
			map.put("salesReps", this.webJaguar.getSalesRepList(salesRepSearch));
		}
		
		CrmQualifierSearch crmQualifierSearch = new CrmQualifierSearch();
		crmQualifierSearch.setSort("name");
		map.put("crmQualifierList", this.webJaguar.getCrmQualifierList(crmQualifierSearch));
		
		map.put("contactFields", this.webJaguar.getCrmContactFieldList(null));

		return map;
	}

	private void generateRandomCardID(Customer customer) {
		String cardID = UUID.randomUUID().toString().toUpperCase().replace("-", "").substring(0, 9);
		if (this.webJaguar.getCustomerByCardID(cardID) == null) {
			customer.setCardId(cardID);
		} else
			generateRandomCardID(customer);
	}
	
	 private void notifySalesRep(Map<String, Configuration> siteConfig, Customer customer) {
			
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
	    	SalesRep salesRep = this.webJaguar.getSalesRepById( customer.getSalesRepId() );
	    	StringBuffer message = new StringBuffer();
	    	boolean isHtml=false;
	    	String emailSubject=null;
	    	EmailMessageForm form = new EmailMessageForm();
	    	form.setHtml(false);
	    	form.setSubject("Account Manager Notification");
	    	int messageId;
	    	if(siteConfig.get( "SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION" ).getValue()!=null){
	    		messageId= Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION" ).getValue() );
	    	}else{
	    		messageId=0;       	
	    	}
	    	SiteMessage siteMessage = this.webJaguar.getSiteMessageById( messageId );
			if(	siteMessage!=null&&!siteMessage.equals("")){
				
		    	if(siteMessage.isHtml()==true){
		    		form.setHtml(true);
		    	}
		    	message.append(siteMessage.getMessage());
		    	form.setSubject(siteMessage.getMessageName());
			}else{
				message.append("Hello #salesRepName#");
				message.append(",\n\n");
				message.append("This is to inform you that a new Customer #firstname# is assigned to you. \n\n");
				message.append("Dialing Notes: #dialingNotes# \n");
				message.append("Thanks \n");
				
			}
			form.setMessage(message.toString());
			form.setTo(salesRep.getEmail());
			form.setFrom(siteConfig.get("CONTACT_EMAIL").getValue());
			try {
		    	// construct email message
		       	MimeMessage mms = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
				this.webJaguar.replaceDynamicElement(form, customer, salesRep);
				helper.setTo(form.getTo());
				helper.setFrom(form.getFrom());
				helper.setSubject(form.getSubject());
				
				if ( form.isHtml()==true ) { 
		    		helper.setText(form.getMessage(), true );
		    	} else {
		    		helper.setText(form.getMessage());
		    	}

				mailSender.send(mms);
			} catch (Exception ex) {
				
					ex.printStackTrace();				
		
			}    				
			
		}
	 private void sendEmail(SiteMessage siteMessage, HttpServletRequest request, Customer customer) {

			// send email
			String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
			Format formatter2 = new SimpleDateFormat("MM/dd/yyyy");
			SalesRep salesRep = this.webJaguar.getSalesRepById( customer.getSalesRepId() );
			StringBuffer messageBuffer = new StringBuffer();

			String firstName = customer.getAddress().getFirstName();
			if (firstName == null) {
				firstName = "";
			}
			String lastname = customer.getAddress().getLastName();
			if (lastname == null) {
				lastname = "";
			}
			String note = customer.getNote();
			if (note == null) {
				note = "";
			}

			// subject
			siteMessage.setSubject(siteMessage.getSubject().replace("#email#", customer.getUsername()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#password#", customer.getPassword()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#firstname#", firstName));
			siteMessage.setSubject(siteMessage.getSubject().replace("#lastname#", lastname));
			siteMessage.setSubject(siteMessage.getSubject().replace("#note#", note));
			siteMessage.setSubject(siteMessage.getSubject().replace("#date#", formatter2.format(new Date()).toString()));

			if (salesRep != null) {
				siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepName#", salesRep.getName()));
				siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepEmail#", salesRep.getEmail()));
				siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
				siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
			}
			// message
			siteMessage.setMessage(siteMessage.getMessage().replace("#email#", customer.getUsername()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#password#", customer.getPassword()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#firstname#", firstName));
			siteMessage.setMessage(siteMessage.getMessage().replace("#lastname#", lastname));
			siteMessage.setMessage(siteMessage.getMessage().replace("#note#", note));
			siteMessage.setMessage(siteMessage.getMessage().replace("#date#", formatter2.format(new Date()).toString()));

			if (salesRep != null) {
				siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepName#", salesRep.getName()));
				siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepEmail#", salesRep.getEmail()));
				siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
				siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
			}
			messageBuffer.append(siteMessage.getMessage());

			try {
				// construct email message
				MimeMessage mms = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
				helper.setTo(customer.getUsername());
				helper.setFrom(contactEmail);
				helper.addBcc(contactEmail);
				helper.setSubject(siteMessage.getSubject());
				helper.setText(messageBuffer.toString(), siteMessage.isHtml());
				mailSender.send(mms);
			} catch (Exception ex) {
				// do nothing
			}
			// salesRep is not null is TERRITORY_ZIPCODE is ON
			// TODO if message is not HTML we send html message to SalesRep
			if (salesRep != null) {
				messageBuffer.append("<br /><br />New Customer Info:<br />Name:<b>" + customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName() + "</b>");
				messageBuffer.append("<br />Company Name:<b>" + customer.getAddress().getCompany() + "</b>");
				messageBuffer.append("<br />Address1:<b>" + customer.getAddress().getAddr1() + "</b>");
				messageBuffer.append("<br />Address2:<b>" + ((customer.getAddress().getAddr2() == null) ? "" : customer.getAddress().getAddr2()) + "</b>");
				messageBuffer.append("<br />City:<b>" + customer.getAddress().getCity() + "</b> State:<b> " + customer.getAddress().getStateProvince() + "</b> Zipcode:<b> "
						+ customer.getAddress().getZip() + "</b>");
				messageBuffer.append("<br />Phone:<b>" + customer.getAddress().getPhone() + "</b>");
				messageBuffer.append("<br />Email:<b>" + customer.getUsername() + "</b>");
				messageBuffer.append("<br />Note:<b>" + customer.getNote() + "</b>");
				try {
					// construct email message
					MimeMessage mms = mailSender.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
					helper.setTo(salesRep.getEmail());
					helper.setFrom(contactEmail);
					helper.setCc(contactEmail);

					helper.setSubject(siteMessage.getSubject());
					helper.setText(messageBuffer.toString(), siteMessage.isHtml());
					mailSender.send(mms);
				} catch (Exception ex) {
					// do nothing
				}
			}
		}
}
