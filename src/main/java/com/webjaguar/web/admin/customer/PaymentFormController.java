/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.31.2007
 */

package com.webjaguar.web.admin.customer;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.domain.Utilities;
import com.webjaguar.web.form.PaymentForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerCreditHistory;
import com.webjaguar.model.Order;
import com.webjaguar.model.Payment;
import com.webjaguar.model.PaymentMethod;
import com.webjaguar.model.PaymentSearch;

public class PaymentFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public PaymentFormController() {
		setSessionForm(true);
		setCommandName("paymentForm");
		setCommandClass(PaymentForm.class);
		setFormView("admin/customers/paymentForm");
		setSuccessView("payments.jhtm");
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception
	{
		super.initBinder( request, binder );
		NumberFormat nf = NumberFormat.getInstance( RequestContextUtils.getLocale( request ) );
		nf.setMaximumFractionDigits( 2 );
		nf.setMinimumFractionDigits( 2 );
		binder.registerCustomEditor( Double.class, new CustomNumberEditor( Double.class, nf, true ) );
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, false, 10 ) );		
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		
		PaymentForm form = (PaymentForm) command;

		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}		
		if (form.getPayment().getAmount()>0 && updateInvoices(request, form.getInvoices(), errors) > form.getPayment().getAmount()) {
			errors.rejectValue("payment.amount", "form.amountLessThanTotalPayments");
		}
		if (errors.hasErrors()) {
			return showForm(request, response, errors);				
		}
		if (form.getPayment().getAddedBy() == null) {
			if ((request.getAttribute("accessUser") != null)) {
				AccessUser accessUser = (AccessUser) request.getAttribute("accessUser");
				form.getPayment().setAddedBy(accessUser.getUsername());
			} else {
				form.getPayment().setAddedBy(getApplicationContext().getMessage("admin", new Object[0], RequestContextUtils.getLocale(request)));
			}
		}
		if (form.isNewPayment()) {
			// If Payment is made through invoice and if Payment in second step is null then set the value to Amount value from step 1. 
			if(request.getParameter("orderId") != null) {
				for(Order order: form.getInvoiceList()) {
					if(order.getPayment() == null) {
						if(form.getPayment().getAmount() <= order.getGrandTotal()) {
							order.setPayment(form.getPayment().getAmount());							
						} else if(form.getPayment().getAmount() > order.getGrandTotal()) {
							order.setPayment(order.getGrandTotal());
						}
						/* order.setPayment(form.getPayment().getAmount());
						if (form.getPayment().getAmount() > order.getGrandTotal() ) {
							errors.reject( "form.overPayment" );
							return showForm(request, response, errors);	
						}*/
					}
				}
			}
			this.webJaguar.insertCustomerPayment( form.getPayment(), form.getInvoiceList() );
		} else {
			this.webJaguar.updateCustomerPayment( form.getPayment(), form.getInvoiceList() );			
		}
		
		if(request.getParameter("orderId") != null) {
			return new ModelAndView(new RedirectView("../orders/invoice.jhtm?order="+request.getParameter("orderId")));
		}
		return new ModelAndView(new RedirectView(getSuccessView()));
	}	
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		PaymentForm form = new PaymentForm();
		PaymentSearch search = new PaymentSearch();
		Map <Integer, Order> invoices = new HashMap<Integer, Order>();
		
		int id = ServletRequestUtils.getIntParameter( request, "id", 0 );
		if (id > 0) {
			// get payment
			form.setPayment( this.webJaguar.getCustomerPaymentById( id ) );			
				    	
	    	// get open invoices
	    	search.setUserId(form.getPayment().getCustomer().getId());
	    	invoices = this.webJaguar.getInvoices( search, "open" );
	    	
	    	// get paid invoices
	    	search.setId(form.getPayment().getId());
	    	invoices.putAll( this.webJaguar.getInvoices( search, "paid" ) );
	    		    	
		} else {
			form = new PaymentForm(this.webJaguar.getCustomerById( ServletRequestUtils.getIntParameter( request, "cid", 0 ) ));	    	
	    	
			// get open invoices
			if (form.getPayment().getCustomer() != null) {
		    	search.setUserId(form.getPayment().getCustomer().getId());

			}
	    	invoices = this.webJaguar.getInvoices( search, "open" );			
		}
		
    	// sort invoices
    	form.setInvoices( new TreeMap<Integer, Order>() );
    	for (Integer orderId: invoices.keySet()) {
    		// if orderId is not null show only that invoice in the payment's list. 
    		if(request.getParameter("orderId") != null){
        		Order order = this.webJaguar.getOrder(Integer.parseInt(request.getParameter("orderId")), null);
        		if(order.getAmountPaid() == null){
        			order.setAmountPaid(0.00);
        		}
        		if(order.getAmountPaid() < order.getGrandTotal()){
            		form.getInvoices().put(Integer.parseInt(request.getParameter("orderId")), order);        			
        		} else {
            		form.getInvoices().put( orderId, invoices.get( orderId ) );        			
        		}
        	} else {
        		form.getInvoices().put( orderId, invoices.get( orderId ) );
        	}
    	}
    	
		return form;
	}
	// we don't need negative payment, we have Cancel Payment now.
	protected void onBindAndValidate(HttpServletRequest request,
            Object command, BindException errors) throws Exception {
		PaymentForm form = (PaymentForm) command;
		if (form.getPayment().getAmount() != null && form.getPayment().getAmount() <= 0) {
			//errors.rejectValue("payment.amount", "form.negAmount");
		}
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
		PaymentForm form = (PaymentForm) command;
		Map<String, Object> map = new HashMap<String, Object>();
		Map<Integer, Boolean> firstPaymentMap = new HashMap<Integer, Boolean>();
		
		List<PaymentMethod> paymentMethods = this.webJaguar.getCustomPaymentMethodList(null);
		
		// adding payment methods.
		if(!paymentMethods.contains(form.getPayment().getPaymentMethod())) {
			List<String> title = new ArrayList<String>();
			// When a Payment is made form Front-end and when order placed form Admin with 'CREDIT/CHARGED CREDIT CARD' then set the Payment-Method
			if(form.getPayment().getPaymentMethod() != null) {
				title.add(form.getPayment().getPaymentMethod());
			}
			if(title.size() > 0){
				for(String paymentMethodTitle: title){
					PaymentMethod paymentMethod = new PaymentMethod();
					
					paymentMethod.setEnabled(true);
					paymentMethod.setTitle(paymentMethodTitle);
					paymentMethods.add(paymentMethod);
				}
			}
		}
		// If payment is a VBA or a Cancelled payment don't allow to update
		if(form.getPayment().getPaymentMethod() != null && !(form.getPayment().getPaymentMethod().equalsIgnoreCase("VBA Credit")) && !form.getPayment().isCancelPayment() ) {
			map.put("paymentUpdate", true);
		}

		map.put("customPayments", paymentMethods);

		// get first payment 
    	for (Order orderId: form.getInvoiceList()) {
    		Integer firstPaymentId = this.webJaguar.getFirstPaymentId(orderId.getOrderId());
    		if (form.getPayment().getId()!= null && form.getPayment().getId().equals(firstPaymentId)) {
    			firstPaymentMap.put(orderId.getOrderId(), true);
    		} else {
    			firstPaymentMap.put(orderId.getOrderId(), false);
    		} 
    	}
		
    	map.put("firstPaymentMap", firstPaymentMap);
   		return map;
    }
	
	private double updateInvoices(HttpServletRequest request, Map <Integer, Order> invoices, BindException errors) throws ServletRequestBindingException {		

		// subtotal
		double subTotal = 0;
		
		for (Order order: invoices.values()) {
			
			Double payment = 
				Utilities.roundFactory(ServletRequestUtils.getDoubleParameter(request, "__payment_" + order.getOrderId(), 0), 2, BigDecimal.ROUND_HALF_UP);
			
			//if payment to order is cancelled don't change the payment. 
			if(!order.isCancelledPayment()) {
				if (payment != null && payment > 0) {
					if (payment > Utilities.roundFactory(order.getBalance(), 2, BigDecimal.ROUND_HALF_UP)) {
						errors.reject( "form.overPayment" );
					}
					order.setPayment( payment );
				} else {
					order.setPayment( null );
				}
			}
			
			// update sub total
			if (order.getPayment() != null) subTotal = order.getPayment() + subTotal;
		}	
		return Utilities.roundFactory(subTotal, 2, BigDecimal.ROUND_HALF_UP);
	}
	
	// multiple browser tabs/windows session issue
	// http://forum.springframework.org/showthread.php?t=42241
	protected String getFormSessionAttributeName(HttpServletRequest request) {
		String id = request.getParameter("id");
		return super.getFormSessionAttributeName(request) + id;	    	  
	}
}