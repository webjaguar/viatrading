package com.webjaguar.web.admin.customer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;

import com.webjaguar.model.AccessUser;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.VbaPaymentReport;
import com.webjaguar.model.VirtualBank;

public class VbaPaymentReportController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Configuration> siteConfig =  (Map) request.getAttribute( "siteConfig" );
		Map<String, Object> myModel = new HashMap<String, Object>();		
		VbaPaymentReport reportFilter = getVbaPaymentReportFilter(request);
		
		// set end date to end of day
		if (reportFilter.getEndDate() != null) {
			reportFilter.getEndDate().setTime(reportFilter.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}
		reportFilter.setProductConsignmentNote(siteConfig.get("CONSIGNMENT_NOTE_FIELD").getValue());
		
		// cancel payments
		if(request.getParameter("__cancel") != null){
			String transactionIds[] = request.getParameterValues("__selected_transactionId");
			if (transactionIds != null) {
				for (int i=0; i<transactionIds.length; i++) {
					VbaPaymentReport canceledPayment = new VbaPaymentReport();
					canceledPayment.setTransactionId(transactionIds[i]);
					canceledPayment.setReportView(reportFilter.getReportView());
					canceledPayment.setProductConsignmentNote(reportFilter.getProductConsignmentNote());
					
					List<VbaPaymentReport> canceledPaymentsList = this.webJaguar.getVbaPaymentReport(canceledPayment);
					
					for(VbaPaymentReport cancelPayment: canceledPaymentsList) {
						if(cancelPayment.getTransactionId().equals(transactionIds[i])){
							
							List<Order> vbaOrders = new ArrayList<Order>();
							List<LineItem> vbaOrdersLineItems = new ArrayList<LineItem>();
							Order order = this.webJaguar.getOrder(cancelPayment.getOrderId(), null);
								
							// update payment status for affiliate 
							if(reportFilter.getReportView().equalsIgnoreCase("affiliate")) {
								order.setVbaAmountToPay((-1 * cancelPayment.getAmountToPay()));
								order.setVbaPaymentStatus(false);
								vbaOrders.add(order);
							} else {
								// update payment status for consignement
								for(LineItem lineItem: order.getLineItems()) {
									if( lineItem.getVbaTransactionId() != null && lineItem.getVbaTransactionId().equals(cancelPayment.getTransactionId())){
										lineItem.getSupplier().setUserId(cancelPayment.getUserId());
										if(cancelPayment.getAmountToPay() != null) {
											lineItem.setVbaAmountToPay((-1 * cancelPayment.getAmountToPay()));
										}
										lineItem.setVbaPaymentStatus(false);
										lineItem.setVbaProductSku(cancelPayment.getProductSku());
										lineItem.setLineNumber(cancelPayment.getOrderLineNum());
										vbaOrdersLineItems.add(lineItem);
									}
								}
							}
							if(!vbaOrders.isEmpty() || !vbaOrdersLineItems.isEmpty()) {
								this.webJaguar.updateVbaOrders(vbaOrders, vbaOrdersLineItems, this.webJaguar.getAccessUser(request).getUsername());
							}
						}						
					}
				}
			}
		}
		
		List<VbaPaymentReport> reportList = this.webJaguar.getVbaPaymentReport(reportFilter);
		
		//Company Name
		List<VirtualBank> userIdList = new ArrayList<VirtualBank>();		
		for(VbaPaymentReport userId :reportList) {
			VirtualBank thisUserId = new VirtualBank();	
			thisUserId.setUserId(userId.getUserId());
			userIdList.add(thisUserId);
		}
		Map<Integer, String> companyMap = this.webJaguar.getCustomerCompanyMap(userIdList);
		
		for(VbaPaymentReport vb :reportList){
			if(companyMap.containsKey((long) vb.getUserId() )) {
				vb.setCompanyName(companyMap.get((long) vb.getUserId()));
			}
		}
		
		PagedListHolder vbaPaymentReports = new PagedListHolder(reportList);		
		vbaPaymentReports.setPageSize(reportFilter.getPageSize());
		vbaPaymentReports.setPage(reportFilter.getPage()-1);
		
		if(request.getParameter("userName")!= null) {
			myModel.put("userName",request.getParameter("userName"));
		}
		myModel.put("reportView", reportFilter.getReportView());
		myModel.put("vbaPaymentReportList", vbaPaymentReports);
		return new ModelAndView("admin/customers/vbaPaymentReport", "model", myModel);
	}

	private VbaPaymentReport getVbaPaymentReportFilter(HttpServletRequest request) throws ParseException {
		VbaPaymentReport vbaPaymentReportFilter = (VbaPaymentReport)  request.getSession().getAttribute( "vbaPaymentReportFilter" );
		if (vbaPaymentReportFilter == null) {
			vbaPaymentReportFilter = new VbaPaymentReport();
			request.getSession().setAttribute( "vbaPaymentReportFilter", vbaPaymentReportFilter );
		}
		
		// Date Type
		if (request.getParameter("dateType") != null) {
			vbaPaymentReportFilter.setDateType( ServletRequestUtils.getStringParameter( request, "dateType", "orderDate" ));
		}
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				vbaPaymentReportFilter.setStartDate( df.parse( request.getParameter("startDate")) );
			} catch (ParseException e) {
				vbaPaymentReportFilter.setStartDate(null);
	        }
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				vbaPaymentReportFilter.setEndDate( df.parse( request.getParameter("endDate")) );
			} catch (ParseException e) {
				vbaPaymentReportFilter.setEndDate(null);
	        }
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				vbaPaymentReportFilter.setPage( 1 );
			} else {
				vbaPaymentReportFilter.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				vbaPaymentReportFilter.setPageSize( 10 );
			} else {
				vbaPaymentReportFilter.setPageSize( size );				
			}
		}
		
		//reportView
		if(request.getParameter("reportView") != null) {
			vbaPaymentReportFilter.setReportView(ServletRequestUtils.getStringParameter( request, "reportView", "consignment" ));
		}
		
		//Pay Dates
		SimpleDateFormat formatter2 = new SimpleDateFormat( "yyyy/MM/dd", RequestContextUtils.getLocale( request ) );

		// Set default start date to 30 days before current date
		long date30 = new Date().getTime()- (  20*24*60*60*1000);
		date30 = date30 - (  10*24*60*60*1000);
		
		System.out.println("Start Date 1"+  formatter2.format(date30));

		
		if (request.getParameter("startPayDate") != null) {
			try {
				vbaPaymentReportFilter.setDateOfPay(( formatter2.parse( request.getParameter("startPayDate"))));
			} catch (ParseException e) {
				vbaPaymentReportFilter.setDateOfPay(formatter2.parse(formatter2.format(date30)));
	        }
		} else {
			vbaPaymentReportFilter.setDateOfPay(formatter2.parse(formatter2.format(date30)));
		}
		
		System.out.println("Start Date AFTER"+  vbaPaymentReportFilter.getDateOfPay());

		
			//if(request.getParameter("EndPayDate") != null) {
	
		if (request.getParameter("EndPayDate") != null) {
			try {
				vbaPaymentReportFilter.setEndDateOfPay(( formatter2.parse( request.getParameter("EndPayDate"))));
			} catch (ParseException e) {
				vbaPaymentReportFilter.setEndDateOfPay(formatter2.parse(formatter2.format(new Date())));
	        }
		} else {
			vbaPaymentReportFilter.setEndDateOfPay(formatter2.parse(formatter2.format(new Date())));
		}
		
		System.out.println("END Date AFTER"+  vbaPaymentReportFilter.getEndDateOfPay());

		
		//reportView
		if(request.getParameter("paymentMethod") != null) {
			vbaPaymentReportFilter.setPaymentMethod(ServletRequestUtils.getStringParameter( request, "paymentMethod", "" ));
		}
		
		//transaction Id
		if(request.getParameter("transactionId") != null) {
			vbaPaymentReportFilter.setTransactionId(ServletRequestUtils.getStringParameter( request, "transactionId", "" ));
		}
		
		//affiliateId/supplierId
		if(request.getParameter("userName") != null) {
			Customer user = this.webJaguar.getCustomerByUsername(request.getParameter("userName"));
			if(request.getParameter("userName") != "") {
				if(vbaPaymentReportFilter.getReportView() != "" && vbaPaymentReportFilter.getReportView().equalsIgnoreCase("consignment")) {
					if(user.getSupplierId()!= null && !user.getSupplierId().equals("")) {
						vbaPaymentReportFilter.setSupplierId(user.getSupplierId());
					} else {
						vbaPaymentReportFilter.setSupplierId(0);
					}
				} else {
					vbaPaymentReportFilter.setUserId(user.getId());
				}
			} else {
				vbaPaymentReportFilter.setSupplierId(null);
				vbaPaymentReportFilter.setUserId(null);
			}
		}

		// Product Sku
		if (request.getParameter("productSku") != null) {		
			vbaPaymentReportFilter.setProductSku(request.getParameter("productSku"));
		}
		
		return vbaPaymentReportFilter;		
	}
}
