package com.webjaguar.web.admin.customer;

import java.util.Date;

import com.webjaguar.logic.interfaces.Payment;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Order;

public class PaymentImp implements Payment{	

	public com.webjaguar.model.Payment createPayemnts(Customer customer,  String paymenttMethod, Double amt, String memo) {
		com.webjaguar.model.Payment customerPayment = new com.webjaguar.model.Payment();
		
		customerPayment.setUserId(customer.getId());
		customerPayment.setAmount(amt);
		customerPayment.setDate(new Date());
		customerPayment.setPaymentMethod(paymenttMethod);
		customerPayment.setMemo(memo);
		customerPayment.setCustomer(customer);
		
		return customerPayment;		
	}
}

