package com.webjaguar.web.admin.faq;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.web.domain.Constants;

public class FaqGroupListController implements Controller {
	
	private WebJaguarFacade webJaguar;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
        // check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("faqGroup.jhtm"));
		}
		
		// check if update ranking button was pressed
		if (request.getParameter("__update_ranking") != null) {
			updateRanking(request);
		}
		
		Map<String, PagedListHolder> myModel = new HashMap<String, PagedListHolder>();
		
		PagedListHolder faqGroupList = new PagedListHolder(this.webJaguar.getFaqGroupList(Constants.FULL_PROTECTED_ACCESS));
		faqGroupList.setPageSize(25);
		String page = request.getParameter("page");
		if (page != null) {
			faqGroupList.setPage(Integer.parseInt(page)-1);
		}
		
		myModel.put("faqGroups", faqGroupList);
    	
        return new ModelAndView("admin/faq/grouplist", "model", myModel);
	}
	
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}
	
	public void updateRanking(HttpServletRequest request){
		Enumeration paramNames = request.getParameterNames();
		List<Map> data = new ArrayList<Map>();
		while (paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement().toString();
			if (paramName.startsWith("__rank_")) {
				HashMap<String, Integer> rankingMap = new HashMap<String, Integer>();
				try {
					Integer id = new Integer(paramName.substring(7));
					if (request.getParameter(paramName).equals("")) {
						rankingMap.put("id", id);					
						rankingMap.put("rank", null);						
					} else {	
						Integer rank = new Integer(request.getParameter(paramName));
						if (rank.intValue() >= 0) {
							rankingMap.put("id", id);					
							rankingMap.put("rank", rank);					
						}
					}
					data.add(rankingMap);
				} catch (NumberFormatException e) {}
			}
		}
		this.webJaguar.updateFaqGroupRanking(data);
	}

}
