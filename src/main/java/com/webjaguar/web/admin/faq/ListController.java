/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.faq;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.FaqSearch;
import com.webjaguar.web.domain.Constants;

public class ListController extends AbstractCommandController {
	
	public ListController() {
		setCommandClass(FaqSearch.class);
	}    

	private WebJaguarFacade webJaguar;
	
    public ModelAndView handle(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {
    	
        // search criteria
    	FaqSearch search = (FaqSearch) command;
    	search.setProtectedAccess( Constants.FULL_PROTECTED_ACCESS );
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			if (search.getGroupId() != null) {
				return new ModelAndView(new RedirectView("faq.jhtm"), "groupId", search.getGroupId());
			} else {
				return new ModelAndView(new RedirectView("faq.jhtm"));				
			}			
		}
		
		// check if update ranking button was pressed
		if (request.getParameter("__update_ranking") != null) {
			updateRanking(request);
		}
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		PagedListHolder faqList = new PagedListHolder(this.webJaguar.getFaqList(search));
		String size = request.getParameter("size");
		if (size != null) {
			faqList.setPageSize(Integer.parseInt(size));
		} else {		
			faqList.setPageSize(25);
		}		
		String page = request.getParameter("page");
		if (page != null) {
			faqList.setPage(Integer.parseInt(page)-1);
		}
		
		map.put("faqs", faqList);
		// get faq groups
		map.put("groupList", this.webJaguar.getFaqGroupList(Constants.FULL_PROTECTED_ACCESS));		
		map.put("search", search);
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		//Responsive Admin
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null) { 
			return new ModelAndView("admin-responsive/faq/list", "model", map);
		}
        return new ModelAndView("admin/faq/list", "model", map);
	}
	
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}
	
	private void updateRanking(HttpServletRequest request) {
		Enumeration paramNames = request.getParameterNames();
		List<Map> data = new ArrayList<Map>();
		while (paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement().toString();
			if (paramName.startsWith("__rank_")) {
				HashMap<String, Integer> rankingMap = new HashMap<String, Integer>();
				try {
					Integer id = new Integer(paramName.substring(7));
					if (request.getParameter(paramName).equals("")) {
						rankingMap.put("id", id);					
						rankingMap.put("rank", null);						
					} else {	
						Integer rank = new Integer(request.getParameter(paramName));
						if (rank.intValue() >= 0) {
							rankingMap.put("id", id);					
							rankingMap.put("rank", rank);					
						}
					}
					data.add(rankingMap);
				} catch (NumberFormatException e) {}
			}
		}
		this.webJaguar.updateFaqRanking(data);					
	}
}
