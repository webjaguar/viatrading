/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.faq;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.FaqForm;

public class FaqFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public FaqFormController() {
		setSessionForm(true);
		setCommandName("faqForm");
		setCommandClass(FaqForm.class);
		setFormView("admin/faq/form");
		setSuccessView("faqList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
		
		FaqForm faqForm = (FaqForm) command;
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteFaq(faqForm.getFaq().getId());	
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		if (faqForm.isNewFaq()) {
			this.webJaguar.insertFaq(faqForm.getFaq());
		} else {
			this.webJaguar.updateFaq(faqForm.getFaq());
		}
		
		ModelAndView modelAndView = new ModelAndView(new RedirectView(getSuccessView()));	
		return modelAndView;
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null) {
			return true;			
		}
		return false;
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
		// allows null faq group (Integer)
		binder.registerCustomEditor(Integer.class, new CustomNumberEditor(Integer.class, true));
	}	
	
    protected Map referenceData(HttpServletRequest request) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    
//		String protectedLevels[] = new String[10];
//		for (int i = 0; i < protectedLevels.length; i++) {
//			StringBuffer protectedLevel = new StringBuffer("1");
//			for (int y = 0; y < i; y++)
//				protectedLevel.append("0");
//			protectedLevels[i] = protectedLevel.toString();
//		}
		map.put("protectedLevels", Constants.protectedLevels());
    	
    	// get faq groups
    	map.put("groupList", this.webJaguar.getFaqGroupList(Constants.FULL_PROTECTED_ACCESS));
    	
    	// labels
		map.put("labels", this.webJaguar.getLabels("protected"));
		
		return map;
    }  	
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
		FaqForm faqForm = new FaqForm();
		if ( request.getParameter("id") != null ) {
			faqForm = new FaqForm(this.webJaguar.getFaqById(new Integer(request.getParameter("id"))));
		} 
		return faqForm;
	} 
}