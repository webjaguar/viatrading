package com.webjaguar.web.admin.faq;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.FaqGroupForm;

public class FaqGroupFormController extends SimpleFormController{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public FaqGroupFormController() {
		setSessionForm(true);
		setCommandName("faqGroupForm");
		setCommandClass(FaqGroupForm.class);
		setFormView("admin/faq/groupform");
		setSuccessView("faqGroupList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
		
		FaqGroupForm faqGroupForm = (FaqGroupForm) command;
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteFaqGroup(faqGroupForm.getFaqGroup().getId());			
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		if (faqGroupForm.isNewFaqGroup()) {
			this.webJaguar.insertFaqGroup(faqGroupForm.getFaqGroup());
		} else {
			this.webJaguar.updateFaqGroup(faqGroupForm.getFaqGroup());
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
	        throws Exception {
		FaqGroupForm faqGroupForm = new FaqGroupForm();
	    if ( request.getParameter("id") != null ) {
		    faqGroupForm = new FaqGroupForm(this.webJaguar.getFaqGroupById(new Integer(request.getParameter("id"))));
	    }
	    return faqGroupForm;
    }
	
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors)
	{
		FaqGroupForm faqGroupForm = (FaqGroupForm) command;
		Map<String, Object> map = new HashMap<String, Object>();

//		String protectedLevels[] = new String[10];
//		for ( int i = 0; i < protectedLevels.length; i++ )
//		{
//			StringBuffer protectedLevel = new StringBuffer( "1" );
//			for ( int y = 0; y < i; y++ )
//				protectedLevel.append( "0" );
//			protectedLevels[i] = protectedLevel.toString();
//		}
		map.put( "protectedLevels", Constants.protectedLevels() );

		// labels
		map.put("labels", this.webJaguar.getLabels("protected"));
		
		return map;
	}
}