/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.09.2008
 */

package com.webjaguar.web.admin.multiStore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.MultiStore;

public class ConfigurationController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }		
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    	throws Exception {	

    	Map<String, Object> map = new HashMap<String, Object>();
    	List<MultiStore> multiStores = this.webJaguar.getMultiStore("all");
    	
    	// check if update button was pressed
    	if (request.getParameter("__update") != null) {
    		if (updateMultiStoreConfig(request, multiStores, map)) {
        		map.put("message", "update.successful");    			
    		}
		}
    	
		map.put("multiStores", multiStores);
		map.put("siteMessages", this.webJaguar.getSiteMessageList());
		
    	return new ModelAndView("admin/multiStore/config", map);
    }
    
    private boolean updateMultiStoreConfig(HttpServletRequest request, List<MultiStore> multiStores, Map<String, Object> map) {
    	StringBuffer multiStoreList = new StringBuffer();
    	
		for (MultiStore multiStore: multiStores) {
			multiStore.setContactEmail(ServletRequestUtils.getStringParameter(request, "__contactEmail_" + multiStore.getHost(), "").trim());
			if (multiStore.getContactEmail().length() > 0) {
				// check if email is valid
				try {
					new InternetAddress(multiStore.getContactEmail());
					if (!multiStore.getContactEmail().contains( "@" )) {
						multiStoreList.append(multiStore.getHost() + ", ");				
					}
				} catch (AddressException ex) {
					multiStoreList.append(multiStore.getHost() + ", ");
				}				
			}
			try {
				multiStore.setMidNewRegistration(ServletRequestUtils.getIntParameter(request, "__midNewRegistration_" + multiStore.getHost()));
			} catch(Exception e) {
				multiStore.setMidNewRegistration(null);
			}
			multiStore.setRegistrationSuccessUrl(ServletRequestUtils.getStringParameter(request, "__registrationSuccessUrl_" + multiStore.getHost(), null));
			try {
				multiStore.setMidNewOrders(ServletRequestUtils.getIntParameter(request, "__midNewOrders_" + multiStore.getHost()));
			} catch(Exception e) {
				multiStore.setMidNewOrders(null);
			}
			try {
				multiStore.setMidShippedOrders(ServletRequestUtils.getIntParameter(request, "__midShippedOrders_" + multiStore.getHost()));
			} catch(Exception e) {
				multiStore.setMidShippedOrders(null);
			}
			try {
				multiStore.setMinOrderAmt(ServletRequestUtils.getDoubleParameter(request, "__minOrderAmt_" + multiStore.getHost()));
			} catch(Exception e) {
				multiStore.setMinOrderAmt(null);
			}
			try {
				multiStore.setMinOrderAmtWholesale(ServletRequestUtils.getDoubleParameter(request, "__minOrderAmtWholesale_" + multiStore.getHost()));
			} catch(Exception e) {
				multiStore.setMinOrderAmtWholesale(null);
			}
		}
				
		if (multiStoreList.length() > 0) {
			// delete extra ", "
			multiStoreList.delete(multiStoreList.length()-2, multiStoreList.length());
			map.put("message", "multistore.exception.contactEmail");
			map.put("multiStoreList", multiStoreList);
			return false;
		}
		
		this.webJaguar.updateMultiStoreConfig(multiStores);
		return true;
    }
}
