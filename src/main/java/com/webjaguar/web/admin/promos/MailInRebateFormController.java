/*
 * Copyright 2005, 2007 Advanced E-Media Solutions @author Jwalant Patel
 * 
 * @since 01.23.2007
 */

package com.webjaguar.web.admin.promos;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.model.MailInRebate;
import com.webjaguar.web.form.MailInRebateForm;

public class MailInRebateFormController extends org.springframework.web.servlet.mvc.SimpleFormController {

	private com.webjaguar.logic.WebJaguarFacade webJaguar;

	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	public MailInRebateFormController() {
		setSessionForm(true);
		setCommandName("mailInRebateForm");
		setCommandClass(com.webjaguar.web.form.MailInRebateForm.class);
		setFormView("admin/promos/mailInRebates/form");
		setSuccessView("mailInRebateList.jhtm");
		this.setValidateOnBinding(true);
	}

	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);

		SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy hh:mm a", RequestContextUtils.getLocale(request));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(df, false, 19));
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		// Note: get everything from model
		java.util.Map<String, Object> model = new java.util.HashMap<String, Object>();
		java.util.Map<String, Object> model1 = new java.util.HashMap<String, Object>();
		model1.put("currentTime", new Date());
		model.put("model", model1);

		return model;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws ServletException {
		MailInRebateForm mailInRebateForm = (MailInRebateForm) command;

		// check if delete button was pressed
		String deleteMailInRebate = ServletRequestUtils.getStringParameter(request, "delete");
		if (deleteMailInRebate != null) {
			this.webJaguar.deleteMailInRebateById(mailInRebateForm.getMailInRebate().getRebateId());
			return new ModelAndView(new RedirectView(getSuccessView()));
		}

		if (mailInRebateForm.isNewMailInRebate()) {
			try {
				this.webJaguar.insertMailInRebate(mailInRebateForm.getMailInRebate());
			} catch (Exception e) {
				// no duplicate entry is allowed.
				// mailInRebate title is UNIQUE
			}
		} else {
			this.webJaguar.updateMailInRebate(mailInRebateForm.getMailInRebate());
		}

		return new ModelAndView(new RedirectView(getSuccessView()));
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		MailInRebateForm mailInRebateForm = (MailInRebateForm) command;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mailInRebate.title", "form.required", "Title is required.");

		MailInRebate mailInRebate = this.webJaguar.getMailInRebateByName(mailInRebateForm.getMailInRebate().getTitle());
		if (mailInRebate != null) { // if exists
			if (mailInRebateForm.isNewMailInRebate()) {
				errors.rejectValue("mailInRebate.title", "MAIL_IN_REBATE_ALREADY_EXISTS", "A Mail In Reabte using this title already exists.");
			} else if (mailInRebate.getRebateId().compareTo(mailInRebateForm.getMailInRebate().getRebateId()) != 0) { // not
																														// the
																														// same
																														// reabte
				errors.rejectValue("mailInRebate.title", "MAIL_IN_REBATE_ALREADY_EXISTS", "A Mail In Reabte using this title already exists.");
			}
		}
		// check if discount is double
		if (mailInRebateForm.getMailInRebate().getDiscount() != null && !mailInRebateForm.getMailInRebate().getDiscount().equals("")) {
			try {
				Double.valueOf(mailInRebateForm.getMailInRebate().getDiscount());
			} catch (Exception e) {
				errors.rejectValue("mailInRebate.discount", "checkDiscount");
			}
		}
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;
		}
		return false;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		MailInRebateForm mailInRebateForm = new MailInRebateForm();

		if (request.getParameter("id") != null) {
			Integer mailInRebateId = new Integer(request.getParameter("id"));
			MailInRebate mailInRebate = this.webJaguar.getMailInRebateById(mailInRebateId);

			mailInRebateForm.setMailInRebate(mailInRebate);
			mailInRebateForm.setNewMailInRebate(false);
		}
		return mailInRebateForm;
	}
}
