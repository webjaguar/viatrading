/*
 * Copyright 2005, 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 * @since 03.25.2008
 */

package com.webjaguar.web.admin.promos;

import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.model.AccessUser;
import com.webjaguar.model.GiftCard;
import com.webjaguar.model.GiftCardStatus;
import com.webjaguar.web.form.GiftCardForm;

public class GiftCardFormController extends org.springframework.web.servlet.mvc.SimpleFormController
{
	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public GiftCardFormController()
	{
		setSessionForm( true );
		setCommandName( "giftCardForm" );
		setCommandClass( GiftCardForm.class );
		setFormView( "admin/promos/giftCards/form" );
		setSuccessView( "giftCardList.jhtm" );
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) 
	{
		GiftCardForm giftCardForm = (GiftCardForm) command;
		Integer giftCardOrderId = giftCardForm.getGiftCard().getGiftCardOrderId();
		
		List<GiftCardStatus> giftCardStatusList = this.webJaguar.getGiftCardStatusHistory(giftCardOrderId);		
		// Note: get everything from model
		Map<String, Object> model = new HashMap<String, Object>();	
		model.put("giftCardStatusList", giftCardStatusList);
		model.put( "giftCardOrderId", giftCardOrderId );
		
		return model;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws ServletException {
		
		GiftCardForm giftCardForm = (GiftCardForm) command;
		
		//check for updateStatus 
		if (request.getParameter("__status") != null) {
			
			GiftCardStatus giftCardStatus = new GiftCardStatus();	
			giftCardStatus.setGiftCardOrderId(Integer.parseInt(request.getParameter("giftCardOrderId")));
			giftCardStatus.setStatus(request.getParameter("giftCardStatus"));
			giftCardStatus.setComments(request.getParameter("giftCardComments"));
			giftCardStatus.setUsername(this.webJaguar.getAccessUser(request).getUsername());
			this.webJaguar.insertGiftCardStatus(giftCardStatus);
			
			return new ModelAndView(new RedirectView("giftCardForm.jhtm?id="+giftCardForm.getGiftCard().getCode()));
		}	

		// check if update button was pressed
		String updateGiftCard = ServletRequestUtils.getStringParameter( request, "_update" );
		if ( updateGiftCard != null )
		{
			this.webJaguar.updateGiftCard( giftCardForm.getGiftCard() );
		}

		return new ModelAndView( new RedirectView( getSuccessView() ) );
	}

	protected Object formBackingObject(javax.servlet.http.HttpServletRequest request) throws Exception
	{
		GiftCardForm giftCardForm = new GiftCardForm();

		Map gSiteConfig = (java.util.Map) request.getAttribute( "gSiteConfig" );
		if ( !(Boolean) gSiteConfig.get("gGIFTCARD") )
        	return new ModelAndView(new RedirectView("../"));
		if ( request.getParameter( "id" ) != null )
		{
			String giftCardId = ServletRequestUtils.getStringParameter( request, "id" );
			GiftCard giftCard = this.webJaguar.getGiftCardByCode( giftCardId, null );

			giftCardForm.setGiftCard( giftCard );
		}
		return giftCardForm;
	}
}
