/*
 * Copyright 2005, 2007 Advanced E-Media Solutions @author Jwalant Patel
 * 
 * @since 01.23.2007
 */

package com.webjaguar.web.admin.promos;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.Deal;
import com.webjaguar.model.ViaDeal;
import com.webjaguar.web.form.ViaDealForm;

public class ViaDealFormController extends org.springframework.web.servlet.mvc.SimpleFormController
{

	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private String dealName;
	public void setDealName(String dealName) { this.dealName = dealName; }
	
	public ViaDealFormController()
	{
//		setSessionForm( true );
		setCommandName( "viaDealForm" );
		setCommandClass( com.webjaguar.web.form.ViaDealForm.class );
		setSuccessView( "viaDealList.jhtm" );
		this.setValidateOnBinding( true );
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception
	{
		super.initBinder( request, binder );
		
		SimpleDateFormat df = new SimpleDateFormat( "MM-dd-yyyy hh:mm a", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, false, 19 ) );		
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) 
	{
		// Note: get everything from model
		java.util.Map<String, Object> model = new java.util.HashMap<String, Object>();
		java.util.Map<String, Object> model1 = new java.util.HashMap<String, Object>();
		model1.put( "currentTime", new Date() );
		model.put( "model", model1 );

		return model;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws ServletException
	{
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView( new RedirectView( getSuccessView() ) );
		}
		ViaDealForm viadDealForm = (ViaDealForm) command;

		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();

		// check if delete button was pressed
		String deleteDeal = ServletRequestUtils.getStringParameter( request, "delete" );
		if ( deleteDeal != null ) {
			this.webJaguar.deleteViaDealById( viadDealForm.getViaDeal().getDealId() );
			return new ModelAndView( new RedirectView( getSuccessView() ) );
		}
		
		if ( viadDealForm.isNewDeal() ) {
			try {
				this.webJaguar.insertViaDeal( viadDealForm.getViaDeal() );
			}
			catch ( Exception e ) {
				// no duplicate entry is allowed.
				// deal title is UNIQUE
			}
		}
		else {
			this.webJaguar.updateViaDeal( viadDealForm.getViaDeal() );
		}

		return new ModelAndView( new RedirectView( getSuccessView() ) );
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		ViaDealForm viadDealForm = (ViaDealForm) command;

		// check if one deal is already active for one sku
		ViaDeal viaDeal = this.webJaguar.getViaDealBySku(viadDealForm.getViaDeal().getParentSku());
		if(viaDeal != null && !viaDeal.getDealId().equals(viadDealForm.getViaDeal().getDealId())){
			errors.rejectValue( "viaDeal.parentSku", "DEAL_ALREADY_EXISTS", "A deal on this Parent Sku already exists." );
		}
	}
	
	protected boolean suppressBinding(HttpServletRequest request)
	{
		if ( request.getParameter( "_cancel" ) != null )
		{
			return true;
		}
		return false;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception
	{
		ViaDealForm viaDealForm = new ViaDealForm();

		if ( request.getParameter( "id" ) != null ){
			Integer dealId = new Integer( request.getParameter( "id" ) );
			//Deal deal = this.webJaguar.getDealById( dealId );
			ViaDeal viaDeal = this.webJaguar.getViaDealById( dealId );

			viaDealForm.setViaDeal( viaDeal );
			viaDealForm.setNewDeal( false );
			setFormView("admin/promos/deals/form");
		}
		
		if( request.getParameter( "id" ) == null ) {
				setFormView("admin/promos/deals/form");
		} 
		
		return viaDealForm;
	}
}
