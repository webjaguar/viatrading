/*
 * Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.23.2007 
 */

package com.webjaguar.web.admin.promos;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.PromoSearch;

public class PromoListController implements Controller
{
	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar( com.webjaguar.logic.WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
    		throws Exception {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		PromoSearch promoSearch = getPromoSearch( request );
		// search criteria

		// if add promo pushed
		if ( request.getParameter( "__add" ) != null ) {
			return new org.springframework.web.servlet.ModelAndView( new org.springframework.web.servlet.view.RedirectView( "promo.jhtm" ) );
		}

		// check if update ranking button was pressed
		if ( request.getParameter( "__update_ranking" ) != null )
		{
			updateRanking( request );
		}
		
		promoSearch.setActive(false);
		PagedListHolder promoList = new PagedListHolder( this.webJaguar.getPromoList( promoSearch) ); 
		promoList.setPageSize( promoSearch.getPageSize() );
		promoList.setPage( promoSearch.getPage() - 1 );
		
		java.util.Map<String, Object> myModel = new java.util.HashMap<String, Object>();
		myModel.put( "promotions", promoList );
		myModel.put( "nOfpromotions", ( promoList == null ) ? 0 : promoList.getNrOfElements() );
		
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)
			return new ModelAndView("admin-responsive/promos/promotions/list", "model", myModel);
		return new org.springframework.web.servlet.ModelAndView( "admin/promos/promotions/list", "model", myModel );
	}

	private void updateRanking(HttpServletRequest request)
	{
		Enumeration paramNames = request.getParameterNames();
		List<Map <String, Integer>> data = new ArrayList<Map <String, Integer>>();
		while ( paramNames.hasMoreElements() )
		{
			String paramName = paramNames.nextElement().toString();
			if ( paramName.startsWith( "__rank_" ) )
			{
				HashMap<String, Integer> rankingMap = new HashMap<String, Integer>();
				try
				{
					Integer id = new Integer( paramName.substring( 7 ) );
					if ( request.getParameter( paramName ).equals( "" ) )
					{
						rankingMap.put( "id", id );
						rankingMap.put( "rank", null );
					}
					else
					{
						Integer rank = new Integer( request.getParameter( paramName ) );
						if ( rank.intValue() >= 0 )
						{
							rankingMap.put( "id", id );
							rankingMap.put( "rank", rank );
						}
					}
					data.add( rankingMap );
				}
				catch ( NumberFormatException e )
				{}
			}
		}
		this.webJaguar.updatePromoRanking( data );
	}
	
	private PromoSearch getPromoSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		PromoSearch promoSearch = (PromoSearch) request.getSession().getAttribute( "promoSearch" );
		if (promoSearch == null) {
			promoSearch = new PromoSearch();
			promoSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "promoSearch", promoSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			promoSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				promoSearch.setPage( 1 );
			} else {
				promoSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				promoSearch.setPageSize( 10 );
			} else {
				promoSearch.setPageSize( size );				
			}
		}
		
		return promoSearch;
	}
}