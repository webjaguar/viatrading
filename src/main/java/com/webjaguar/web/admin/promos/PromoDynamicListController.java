/*
 * Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.23.2007 
 */

package com.webjaguar.web.admin.promos;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.PromoSearch;

public class PromoDynamicListController implements Controller
{
	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar( com.webjaguar.logic.WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
    		throws Exception {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		PromoSearch promoSearch = getPromoSearch( request );
//		System.out.println(promoSearch.getPrefix() + "????");
		// search criteria

		// if add promo pushed
		if ( request.getParameter( "__add" ) != null ) {
			return new org.springframework.web.servlet.ModelAndView( new org.springframework.web.servlet.view.RedirectView( "promo.jhtm" ) );
		}

		// check if update ranking button was pressed
		if ( request.getParameter( "__update_ranking" ) != null )
		{
			updateRanking( request );
		}
		
		// check if delete button was pressed
		if ( request.getParameter( "__delete" ) != null )
		{
			delete( request );
		}
		
		promoSearch.setActive(true);
		java.util.Map<String, Object> myModel = new java.util.HashMap<String, Object>();
//		myModel.put("promoSearch", promoSearch);
		PagedListHolder promoList = new PagedListHolder( this.webJaguar.getDynamicPromoList(promoSearch) ); 
		promoList.setPageSize( promoSearch.getPageSize() );
		promoList.setPage( promoSearch.getPage() - 1 );
		
		
		myModel.put( "promotions", promoList );
		myModel.put( "nOfpromotions", ( promoList == null ) ? 0 : promoList.getNrOfElements() );
		
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)
			return new ModelAndView("admin-responsive/promos/promotions/Dynamiclist", "model", myModel);
		return new org.springframework.web.servlet.ModelAndView( "admin/promos/promotions/DynamicList", "model", myModel );
	}
	
	private void delete(HttpServletRequest request) {
		
		String[] promoIds = request.getParameterValues("__selected_id");
		if(promoIds.length == 1) {
			int promoId = Integer.parseInt(promoIds[0]);
			this.webJaguar.deletePromoById(promoId);
		}else {
			int[] promoDynamicIds = new int[promoIds.length];
			for(int i = 0; i < promoIds.length ; i++) {
				promoDynamicIds[i] = Integer.parseInt(promoIds[i]);
			} 
			
			this.webJaguar.deletePromoByIds(promoDynamicIds);
		}
		
		
	}

	private void updateRanking(HttpServletRequest request)
	{
		Enumeration paramNames = request.getParameterNames();
		List<Map <String, Integer>> data = new ArrayList<Map <String, Integer>>();
		while ( paramNames.hasMoreElements() )
		{
			String paramName = paramNames.nextElement().toString();
			if ( paramName.startsWith( "__rank_" ) )
			{
				HashMap<String, Integer> rankingMap = new HashMap<String, Integer>();
				try
				{
					Integer id = new Integer( paramName.substring( 7 ) );
					if ( request.getParameter( paramName ).equals( "" ) )
					{
						rankingMap.put( "id", id );
						rankingMap.put( "rank", null );
					}
					else
					{
						Integer rank = new Integer( request.getParameter( paramName ) );
						if ( rank.intValue() >= 0 )
						{
							rankingMap.put( "id", id );
							rankingMap.put( "rank", rank );
						}
					}
					data.add( rankingMap );
				}
				catch ( NumberFormatException e )
				{}
			}
		}
		this.webJaguar.updatePromoRanking( data );
	}
	
	private PromoSearch getPromoSearch(HttpServletRequest request) {
		PromoSearch promoSearch = (PromoSearch) request.getSession().getAttribute( "promoSearch" );
		
		
		if (promoSearch == null) {
			promoSearch = new PromoSearch();
//			promoSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "promoSearch", promoSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			promoSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		// promo type
		if (request.getParameter("_promotype") != null) {
		promoSearch.setPromoType(ServletRequestUtils.getStringParameter( request, "_promotype", "" ));
		}
		// prefix
		if(request.getParameter("_prefix") != null) {
			promoSearch.setPrefix(request.getParameter("_prefix"));
		} 
		
		
		
		
		// userID
		if(request.getParameter("_userId") != null) {
			promoSearch.setUserId( ServletRequestUtils.getIntParameter(request, "_userId", -1));
		}
		
		
		// product sku
		if(request.getParameter("_sku") != null) {
		promoSearch.setSku(ServletRequestUtils.getStringParameter( request, "_sku", "" ));
		}
		
		// startDate and endDate

//		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy[hh:mm aaa]");

		// startDate
		
				if (request.getParameter("_startDate") != null) {
//					System.out.println(request.getParameter("_startDate"));
					try {
						promoSearch.setStartDate(df.parse(request.getParameter("_startDate")));
					} catch (ParseException e) {
						System.out.println(e);
						promoSearch.setStartDate(null);
					}
				}
		
				// endDate
				if (request.getParameter("_endDate") != null) {
					try {
						promoSearch.setEndDate(df.parse(request.getParameter("_endDate")));
					} catch (ParseException e) {
						promoSearch.setEndDate(null);
					}
				}
		
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				promoSearch.setPage( 1 );
			} else {
				promoSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				promoSearch.setPageSize( 10 );
			} else {
				promoSearch.setPageSize( size );				
			}
		}

		return promoSearch;
	}
}