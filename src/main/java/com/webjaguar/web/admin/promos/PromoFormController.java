/*
 * Copyright 2005, 2007 Advanced E-Media Solutions @author Shahin Naji
 * 
 * @since 01.23.2007
 */

package com.webjaguar.web.admin.promos;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.Promo;
import com.webjaguar.web.form.PromoForm;

public class PromoFormController extends org.springframework.web.servlet.mvc.SimpleFormController
{

	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public PromoFormController()
	{
		setSessionForm( true );
		setCommandName( "promoForm" );
		setCommandClass( com.webjaguar.web.form.PromoForm.class );
		setFormView( "admin/promos/promotions/form" );
		setSuccessView( "promoList.jhtm" );
		this.setValidateOnBinding( true );
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception
	{
		super.initBinder( request, binder );
		
		SimpleDateFormat df = new SimpleDateFormat( "MM-dd-yyyy hh:mm a", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, false, 19 ) );		
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) 
	{
		
		
		// Note: get everything from model
		java.util.Map<String, Object> model = new java.util.HashMap<String, Object>();
		java.util.Map<String, Object> model1 = new java.util.HashMap<String, Object>();
		
		model1.put( "currentTime", new Date() );
		if(request.getParameter("dyn") != null) {
			model1.put("isDynamic", "true");
		    setSuccessView( "promoDynamicList.jhtm" );
			
//			System.out.println(request.getParameter("dyn"));
		 }else {
			 setSuccessView( "promoList.jhtm" );
		 }
		model.put( "model", model1 );
		
		model.put( "shippingList", this.webJaguar.getCustomShippingRateList(true, false) );

		// check if current m or n year is in the array
		PromoForm promoForm = (PromoForm) command;
		
		return model;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws ServletException
	{

		PromoForm promoForm = (PromoForm) command;


		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();

		// check if delete button was pressed
		String deletePromo = ServletRequestUtils.getStringParameter( request, "delete" );
		if ( deletePromo != null ) {
			this.webJaguar.deletePromoById( promoForm.getPromo().getPromoId() );
			
			return new ModelAndView( new RedirectView( getSuccessView() ) );
		}
		
		if(!promoForm.getPromo().getDiscountType().equalsIgnoreCase("product")) {
			promoForm.getPromo().setBrands(null);
			promoForm.getPromo().setSkus(null);
			promoForm.getPromo().setMinOrderPerBrand(null);
		}
		
		// get group ids
		Set<Object> groups = new HashSet<Object>();
		if (promoForm.getPromo().getGroupIds() != null) {
			String[] groupIds = promoForm.getPromo().getGroupIds().split( "," );
			for ( int x = 0; x < groupIds.length; x++ ) {
				if ( !("".equals( groupIds[x].trim() )) ) {
					groups.add( Integer.valueOf( groupIds[x].trim() ) );
				}
			}
			promoForm.getPromo().setGroupIdSet( groups );
		}	
		
		
		// get skus
		Set<Object> skus = null;
		if (promoForm.getPromo().getSkus() != null) {
			skus = new HashSet<Object>();
			String[] sku = promoForm.getPromo().getSkus().split( "," );
			for ( int x = 0; x < sku.length; x++ ) {
				if ( !("".equals( sku[x].trim() )) ) {
					skus.add( sku[x].trim() );
				}
			}
		}
		promoForm.getPromo().setSkuSet( skus );

		
		//get parent skus
      Set<Object> parentSkus = null;
		
		if (promoForm.getPromo().getParentSkus() != null) {
			parentSkus = new HashSet<Object>();
			String[] parents = promoForm.getPromo().getParentSkus().split( "," );
			for ( int x = 0; x < parents.length; x++ ) {
				if ( !("".equals( parents[x].trim() )) ) {
					parentSkus.add( parents[x].trim() );
				}
			}
		}
		
		promoForm.getPromo().setParentSkuSet(parentSkus);
		
		//get category Ids
		Set<Object> categoryIdSet = null;
		if (promoForm.getPromo().getCategoryIds() != null) {
			categoryIdSet = new HashSet<Object>();
			String[] categories = promoForm.getPromo().getCategoryIds().split( "," );
			for ( int x = 0; x < categories.length; x++ ) {
				if ( !("".equals( categories[x].trim() )) ) {
					categoryIdSet.add( categories[x].trim() );
				}
			}
		}	
		promoForm.getPromo().setCategoryIdSet(categoryIdSet);
		
		
		// get brands
		Set<Object> brands = null;
		if (promoForm.getPromo().getBrands() != null) {
			brands = new HashSet<Object>();
			String[] brand = promoForm.getPromo().getBrands().split( "," );
			for ( int x = 0; x < brand.length; x++ ) {
				if ( !("".equals( brand[x].trim() )) ) {
					brands.add( brand[x].trim() );
				}
			}
		}
		promoForm.getPromo().setBrandSet( brands );
		
		
		

		if ( promoForm.isNewPromo() ) {
			try {
				this.webJaguar.insertPromo( promoForm.getPromo() );
			}
			catch ( Exception e ) {
				// no duplicate entry is allowed.
				// promo title is UNIQUE
			}
		}
		else {
			this.webJaguar.updatePromo( promoForm.getPromo() );
		}

		return new ModelAndView( new RedirectView( getSuccessView() ) );
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		PromoForm promoForm = (PromoForm) command;

		// check if there is a promo code
		Integer promoId = this.webJaguar.getPromoIdByName( promoForm.getPromo().getTitle() );
		if ( promoId != null )
		{ // promo	 exists
			if ( promoForm.isNewPromo() )
			{
				errors.rejectValue( "promo.title", "PROMO_ALREADY_EXISTS", "A promo using this code already exists." );
			}
			else if ( promoId.compareTo( promoForm.getPromo().getPromoId() ) != 0 )
			{ // not the same promo
				errors.rejectValue( "promo.title", "PROMO_ALREADY_EXISTS", "A promo using this code already exists." );
			}
		}
		// check if group id is integer
		if(promoForm.getPromo().getGroupIds() != null && !promoForm.getPromo().getGroupIds().equals("")){
			String[] groupIds = promoForm.getPromo().getGroupIds().split( "," );
			for ( int x = 0; x < groupIds.length; x++ ) {
				try{
					Integer.valueOf( groupIds[x].trim() ) ;
				} catch(Exception e){
					errors.rejectValue("promo.groupIds", "checkGroupIds");
					break;
				}	
			}
		}	
	}
	
	protected boolean suppressBinding(HttpServletRequest request)
	{
		if ( request.getParameter( "_cancel" ) != null )
		{
			return true;
		}
		return false;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception
	{
		PromoForm promoForm = new PromoForm();

		if ( request.getParameter( "id" ) != null )
		{
			Integer promoId = new Integer( request.getParameter( "id" ) );
			Promo promo = this.webJaguar.getPromoById( promoId );
			if (promo==null) {
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("../promos")));			
			}
			promoForm.setPromo( promo );
			promoForm.setNewPromo( false );
			
			if (promo.getGroupIdSet() != null && promo.getGroupIdSet().size() != 0) {
				Set groupIdSet = promo.getGroupIdSet();

				Iterator iter = groupIdSet.iterator();
				StringBuffer groupIds = new StringBuffer();
				if ( iter.hasNext() ) {
					groupIds.append( iter.next() );
				}
				while ( iter.hasNext() ) {
					groupIds.append( ", " );
					groupIds.append( iter.next() );
				}
				promoForm.getPromo().setGroupIds( groupIds.toString() );
			}
			
			//Skus
			if (promo.getSkuSet() != null && promo.getSkuSet().size() != 0) {
				Set skuSet = promo.getSkuSet();
			
				Iterator itr = skuSet.iterator();
				StringBuffer skus = new StringBuffer();
				if ( itr.hasNext() ) {
					skus.append( itr.next() );
				}
				while ( itr.hasNext() ) {
					skus.append( ", " );
					skus.append( itr.next() );
				}
				promoForm.getPromo().setSkus( skus.toString() );
			}
			
			//Parent Skus
			if (promo.getParentSkuSet() != null && promo.getParentSkuSet().size() != 0) {
				Set parentskuSet = promo.getParentSkuSet();
			
				Iterator itr = parentskuSet.iterator();
				StringBuffer parentSkus = new StringBuffer();
				if ( itr.hasNext() ) {
					parentSkus.append( itr.next() );
				}
				while ( itr.hasNext() ) {
					parentSkus.append( ", " );
					parentSkus.append( itr.next() );
				}
				promoForm.getPromo().setParentSkus( parentSkus.toString() );
			}
			
			//CategoryIds
			if (promo.getCategoryIdSet() != null && promo.getCategoryIdSet().size() != 0) {
				Set categoryIdSet = promo.getCategoryIdSet();
			
				Iterator itr = categoryIdSet.iterator();
				StringBuffer categories = new StringBuffer();
				if ( itr.hasNext() ) {
					categories.append( itr.next() );
				}
				while ( itr.hasNext() ) {
					categories.append( ", " );
					categories.append( itr.next() );
				}
				promoForm.getPromo().setCategoryIds( categories.toString() );
			}
			
			
			//Brands
			if (promo.getBrandSet() != null && promo.getBrandSet().size() != 0) {
				Set brandSet = promo.getBrandSet();
			
				Iterator itr = brandSet.iterator();
				StringBuffer brands = new StringBuffer();
				if ( itr.hasNext() ) {
					brands.append( itr.next() );
				}
				while ( itr.hasNext() ) {
					brands.append( ", " );
					brands.append( itr.next() );
				}
				promoForm.getPromo().setBrands( brands.toString() );
			}
		}
		return promoForm;
	}
}
