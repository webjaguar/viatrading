/*
 * Copyright 2005, 2009 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 01.05.2009 
 */

package com.webjaguar.web.admin.promos;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.DealSearch;

public class DealListController implements Controller
{
	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar( com.webjaguar.logic.WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
    		throws Exception {
		
		DealSearch dealSearch = getDealSearch( request );
		// search criteria

		// if add deal pushed
		if ( request.getParameter( "__addOnSubTotal" ) != null ) {
			return new org.springframework.web.servlet.ModelAndView( new org.springframework.web.servlet.view.RedirectView( "dealOnSubTotal.jhtm" ) ) ;
		}
		if ( request.getParameter( "__addOnSku" ) != null ) {
			return new org.springframework.web.servlet.ModelAndView( new org.springframework.web.servlet.view.RedirectView( "dealOnSku.jhtm" ) ) ;
		}

		PagedListHolder dealList = new PagedListHolder( this.webJaguar.getDealList( dealSearch ) ); 
		dealList.setPageSize( dealSearch.getPageSize() );
		dealList.setPage( dealSearch.getPage() - 1 );
		
		java.util.Map<String, Object> myModel = new java.util.HashMap<String, Object>();
		myModel.put( "deals", dealList );
		myModel.put( "nOfdeals", ( dealList == null ) ? 0 : dealList.getNrOfElements() );

		return new org.springframework.web.servlet.ModelAndView( "admin/promos/deals/list", "model", myModel );
	}

	private DealSearch getDealSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		DealSearch dealSearch = (DealSearch) request.getSession().getAttribute( "dealSearch" );
		if (dealSearch == null) {
			dealSearch = new DealSearch();
			dealSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "dealSearch", dealSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			dealSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// view type
		dealSearch.setViewType(ServletRequestUtils.getStringParameter( request, "_viewtype", "" ));
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				dealSearch.setPage( 1 );
			} else {
				dealSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				dealSearch.setPageSize( 10 );
			} else {
				dealSearch.setPageSize( size );				
			}
		}
		
		return dealSearch;
	}
}