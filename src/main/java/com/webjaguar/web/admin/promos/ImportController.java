package com.webjaguar.web.admin.promos;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.mail.MailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.Promo;

public class ImportController  extends WebApplicationObjectSupport implements Controller{
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private HSSFFormulaEvaluator evaluator;
	
	String formView = "admin/promos/promotions/import";
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			return new ModelAndView("admin/promos/import", "message", "excelfile.notWritable");	
		} 
		
		// check if upload button was pressed
		if (request.getParameter("__upload") != null) {
	    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	    	MultipartFile file = multipartRequest.getFile("file");
	    	if (file != null && !file.isEmpty()) {
	    		File excel_file = new File(baseFile, "promos_import.xls");
	    		file.transferTo(excel_file);
	    		file = null; // delete to save resources
	    		
	    		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
	    		List<Promo> importedPromos = new ArrayList<Promo>();
	        	try {
	                List<Map<String, Object>> addedItems = new ArrayList<Map<String, Object>>();
	                List<Map<String, Object>> updatedItems = new ArrayList<Map<String, Object>>();
	                Map<String, Object> map = new HashMap<String, Object>();
	        		if (processFile(excel_file, gSiteConfig, map, importedPromos, addedItems, updatedItems)) {
	        			this.webJaguar.insertImportExportHistory( new ImportExportHistory("promotion", SecurityContextHolder.getContext().getAuthentication().getName(), true ));
        				for (Promo promo : importedPromos) {            				
        					try {
        						// new promo
                    			if (promo.getPromoId() == null) {
                    				this.webJaguar.insertPromo(promo);                    				
                    			} else { // old promo
                    				this.webJaguar.updatePromo(promo);
                    			}        					
            				} catch (Exception e) {
            					//System.out.println("ex: " +e);
            				}
        				}

	        			if (updatedItems.size() > 0) map.put("updatedItems", updatedItems);
	        			if (addedItems.size() > 0) map.put("addedItems", addedItems);
	        			
	        			return new ModelAndView("admin/promos/promotions/importSuccess", map);
	        		} else {
	        			return new ModelAndView(formView, map);
	        		}
	        	} catch (IOException e) {
	    			return new ModelAndView(formView, "message", "excelfile.invalid");
	    		} finally {
	    	    	// delete excel file
	    			excel_file.delete();	    			
	    		} 
	    	} else {
	        	return new ModelAndView(formView, "message", "excelfile.invalid");	    		
	    	}
		}    	
    	return new ModelAndView(formView);
	}
	
	private boolean processFile(File excel_file, Map<String, Object> gSiteConfig, 
		Map<String, Object> map, List<Promo> importedPromos, List<Map<String, Object>> addedItems, List<Map<String, Object>> updatedItems) throws IOException, Exception {
    	
    	List<Map<String, Object>> invalidItems = new ArrayList<Map<String, Object>>();
        	    						
		POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(excel_file));
		HSSFWorkbook wb = new HSSFWorkbook(fs);
	    
		HSSFSheet sheet = wb.getSheetAt(0);       	// first sheet
		HSSFRow row     = sheet.getRow(0);        	// first row
		
		evaluator = new HSSFFormulaEvaluator(sheet, wb);
		
		Map<String, Short> header = new HashMap<String, Short>();
		Set<String> promos = new HashSet<String>();
		//Set<Short> promoSet = new HashSet<Short>();
		
		if (wb.getNumberOfSheets() > 1) {
			// multiple sheets
			map.put("message", "excelfile.multipleSheets");
			return false;					
		}
		if (row == null) {
			// first line should be the headers
			map.put("message", "excelfile.emptyHeaders");
			return false;				
		}
		
	    Iterator iter = row.cellIterator();
	    
	    // get header names
		while (iter.hasNext()) {
			HSSFCell cell = (HSSFCell) iter.next();
			String headerName = "";
			try {
				headerName = cell.getStringCellValue().trim();					
			} catch (NumberFormatException e) {
				// do nothing
			}
			if (!headerName.equals( "" )) {					
				if (header.put(headerName.toLowerCase(), cell.getCellNum()) != null) {
					// duplicate header
					map.put("message", "excelfile.duplicateHeaders");
					map.put("arguments", headerName);
					return false;								
				}
			} 
		}
		
		// check required headers
		if (!header.containsKey("promocode") || !header.containsKey("discount") || !header.containsKey("minimumorder") || !header.containsKey("startdate") || !header.containsKey("enddate") || !header.containsKey("discounttype")) {
			map.put("message", "excelfile.missingHeaders");
			return false;
		}

		int promoCount = this.webJaguar.getPromoCount(null);
		boolean promoLimit = false;
		
		nextRow: for (int i=1; i<=sheet.getLastRowNum(); i++) {
			row = sheet.getRow(i);
			if (row == null) {
				continue;
			}
			
			// check if new or existing promo
			String promoCode = null;
			promoCode = getStringValue(row, header.get("promocode")).trim();
			if (promoCode.equals("")) promoCode = null;
			Map<String, Object> thisMap = new HashMap<String, Object>();
			thisMap.put("promoCode", promoCode);
			
			
			// check for valid and duplicate promo
			if (promoCode != null) {
				if (!promos.add(promoCode.toLowerCase())) {
					// promo is a duplicate
					thisMap.put("rowNum", row.getRowNum()+1);
					thisMap.put("promoCode", promoCode);
					thisMap.put("reason", "Duplicate Promo Code.");
					invalidItems.add(thisMap);
					continue nextRow;
				}
			} else {
				continue nextRow;
			}
			if (invalidItems.size() == 0) { // no errors
				// save or update promo
				Promo promo = null;
				promo = this.webJaguar.getPromo(promoCode);
				if (promo != null) { 		
					// existing promo
					processRow(row, header, promo, thisMap);						
					updatedItems.add(thisMap);
								
				} else if(Integer.parseInt(gSiteConfig.get("gNUMBER_PROMOS").toString()) == -1 || promoCount <= Integer.parseInt(gSiteConfig.get("gNUMBER_PROMOS").toString())) {
					if(promoCount == Integer.parseInt(gSiteConfig.get("gNUMBER_PROMOS").toString())) {
						promoLimit= true;
					}
					// new Promo
					promo = new Promo();
					promo.setTitle(promoCode);
					processRow(row, header, promo, thisMap);		
					if(!promoLimit) {
						addedItems.add(thisMap);						
					}
					++promoCount;
				}
				if(! promoLimit) {
					importedPromos.add(promo);
				}
			}			
		}	
		if (Integer.parseInt(gSiteConfig.get("gNUMBER_PROMOS").toString()) != -1 && promoCount > Integer.parseInt(gSiteConfig.get("gNUMBER_PROMOS").toString())) {
			map.put("limit", "Sorry, you reached the limit for Promos. Please contact ADMIN for more PROMOS");
		}
		
		if (invalidItems.size() > 0) { // has errors
			map.put("message", "importFailedTitle");
			map.put("invalidItems", invalidItems);
			return false;				
		} else if (importedPromos.size() == 0) {	// no promos to import
			map.put("message", "excelfile.empty");
			return false;			
		}
		
		return true;
    }
	 
	 private Integer getIntegerValue(HSSFRow row, short cellnum) throws Exception {
		HSSFCell cell = row.getCell(cellnum);
    	Integer value = null;
    	
    	if (cell != null) {
    		switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = new Integer(cell.getStringCellValue());										
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Double cellValue = cell.getNumericCellValue();
					if (cellValue != cellValue.intValue()) {
						throw new Exception();	
					}
					value =  cellValue.intValue();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue formulaCellValue = evaluator.evaluate(cell);					
					if (formulaCellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						cellValue =  new Double(formulaCellValue.getNumberValue());
						if (cellValue != cellValue.intValue()) {
							throw new Exception();	
						}
						value =  cellValue.intValue();
					}
					break;
    		} // switch
    	}
    	return value;
    }
	 
	private String getStringValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		String value = "";

    	if (cell != null) {
			switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = cell.getStringCellValue();
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Integer intValue = null;
					try {
						intValue = getIntegerValue(row, cellnum);
					} catch (Exception e) {}
					if (intValue != null) {
						value = intValue.toString();
						break;
					}
					value = new Double(cell.getNumericCellValue()).toString();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue cellValue = evaluator.evaluate(cell);
					if (cellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						value = new Double(cellValue.getNumberValue()).toString();						
					} else if (cellValue.getCellType() == HSSFCell.CELL_TYPE_STRING) {
						value = cellValue.getStringValue();
					}
					break;
			
			} // switch		
    	}
		return value;
    }
	 
	private boolean getBooleanValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		boolean value = false;

    	if (cell != null) {
    		if (getStringValue(row, cellnum).trim().equals( "1" ))  {
    			value = true;
    		}
    	}
		return value;
    } 
	
	private Date getDateValue(HSSFRow row, short cellnum) throws ParseException {

		HSSFCell cell = row.getCell(cellnum);		
		Date value= new Date();
		
		if(cell != null) {
			switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
			    	SimpleDateFormat df = new SimpleDateFormat( "MM-dd-yyyy hh:mm a" );
					String date = cell.getRichStringCellValue().getString();
					try {
						value = (Date)df.parse(date); 
					} catch (Exception e) {
						value = new Date();
					}
				    break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					if(HSSFDateUtil.isCellDateFormatted(cell)) {
						value = cell.getDateCellValue();
					}
				    break;
			}
		}
		return value;		
	}
		
	private void processRow(HSSFRow row, Map<String, Short> header, Promo promo, Map<String, Object> thisMap) throws Exception {
       	
    	// promo name/Title
    	if (header.containsKey("promocode")) {
    		promo.setTitle( getStringValue(row, header.get("promocode")) );	    		
    	}
    	
    	
    	// one time use per customer
    	if (header.containsKey("onetimeuseonly")) {
    		promo.setOnetimeUseOnly( getBooleanValue(row, header.get("onetimeuseonly")) );	    		
    	}
    	
    	// one time use per customer
    	if (header.containsKey("onetimeusepercustomer")) {
    		promo.setOnetimeUsePerCustomer( getBooleanValue(row, header.get("onetimeusepercustomer")) );	    		
    	}
    	
    	// discount
    	if (header.containsKey("discount")) {
    		if(getIntegerValue(row, header.get("minimumorder")) != null && !getIntegerValue(row, header.get("minimumorder")).equals("")) {
        		promo.setDiscount( (double) getIntegerValue(row, header.get("discount")) );	     			
    		} else {
    			promo.setDiscount(0.00);
    		}
    	}
    	
    	// discount type
    	if (header.containsKey("discounttype")) { 
    		if(getStringValue(row, header.get("discounttype")) != null &&! getStringValue(row, header.get("discounttype")).equals("")) {
    			promo.setDiscountType( getStringValue(row, header.get("discounttype")) );	    
    		}else {
    			promo.setDiscountType("order");
    		}
    	}
    	
    	// percent
    	if (header.containsKey("percent")) {
    		promo.setPercent( getBooleanValue(row, header.get("percent")) );	    		
    	}
    	
    	// minimum order
    	if (header.containsKey("minimumorder")) {
    		if(getIntegerValue(row, header.get("minimumorder")) != null && !getIntegerValue(row, header.get("minimumorder")).equals("")) {
    			promo.setMinOrder( (double) getIntegerValue(row, header.get("minimumorder")) );	    	
    		} else {
    			promo.setMinOrder(0.00);
    		}
    	}
    	
    	// minimum order per brand
    	if (header.containsKey("minimumorderperorder")) {
    		if(getIntegerValue(row, header.get("minimumorderperorder")) != null && !getIntegerValue(row, header.get("minimumorderperorder")).equals("")) {
    			promo.setMinOrderPerBrand( (double) getIntegerValue(row, header.get("minimumorderperorder")) );	    		
    		}
    	}
    	
    	// start date
    	if (header.containsKey("startdate")) {
    		promo.setStartDate(getDateValue(row, header.get("startdate")));
    	}
    	
    	// end date
    	if (header.containsKey("enddate")) {
    		
    		Date endDate = getDateValue(row, header.get("enddate"));    		
    		
    		// check for end date: if end date and start date are same then set the end date time to end of day. 
    		int end = endDate.compareTo( promo.getStartDate() );
    		if(end == 0) {
    			Calendar now = Calendar.getInstance();
    			now.set( Calendar.HOUR, 23 );
    			promo.setEndDate(now.getTime());
    		}   else {
    			promo.setEndDate(endDate);
    		}
    	}
    	
    	//rank
    	if(header.containsKey("rank")) {
    		promo.setRank(getIntegerValue(row, header.get("rank")) );	     		
    	}
    	
    	// product availability
    	if(header.containsKey("productavailability")) {
    		promo.setProductAvailability(getBooleanValue(row, header.get("productavailability")));
    	}
    	
    	// product availability
    	if(header.containsKey("htmlcode")) {
    		promo.setHtmlCode(getStringValue(row, header.get("htmlcode")));
    	}
	}
}
