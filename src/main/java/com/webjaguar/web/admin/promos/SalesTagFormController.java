/*
 * Copyright 2005, 2007 Advanced E-Media Solutions @author Shahin Naji
 * 
 * @since 01.23.2007
 */

package com.webjaguar.web.admin.promos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.SalesTag;
import com.webjaguar.web.form.salesTagForm;

public class SalesTagFormController extends org.springframework.web.servlet.mvc.SimpleFormController
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private File baseFile;

	public SalesTagFormController()
	{
		setSessionForm( true );
		setCommandName( "salesTagForm" );
		setCommandClass( com.webjaguar.web.form.salesTagForm.class );
		setFormView( "admin/promos/salesTags/form" );
		setSuccessView( "salesTagList.jhtm" );
		this.setValidateOnBinding( true );
	}

	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception
	{
		super.initBinder( request, binder );
		
		SimpleDateFormat df = new SimpleDateFormat( "MM-dd-yyyy hh:mm a", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, false, 19 ) );		
	}
	
	protected Map referenceData(javax.servlet.http.HttpServletRequest request, Object command, org.springframework.validation.Errors errors)
	{
		salesTagForm salesTagForm = (salesTagForm) command;
		
		baseFile = new File( getServletContext().getRealPath( "/assets/Image/" ) );

		Properties prop = new Properties();
		try
		{
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null )
			{
				baseFile = new File( (String) prop.get( "site.root" ) + "/assets/Image/" );
			}
		}
		catch ( Exception e )
		{}
		File salesTagImageFile = new File( baseFile, "PromoSalesTag/salestag_" + salesTagForm.getSalesTag().getTagId() + ".gif" );
		
		// Note: get everything from model
		java.util.Map<String, Object> model = new java.util.HashMap<String, Object>();
		java.util.Map<String, Object> model1 = new java.util.HashMap<String, Object>();
		
		model1.put( "imageExist", (salesTagImageFile.exists() && !salesTagImageFile.getName().equals( "salestag_null.gif" )) ? true : false );
		if ( !baseFile.canWrite()  ) {
			model1.put( "message", "images.notWritable" );
		}
		model1.put( "randomNum", new Random().nextInt( 1000 ) );
		model1.put( "currentTime", new Date() );
		
		model.put( "model", model1 );

		return model;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws ServletException
	{

		// check if cancel button was pressed
		if ( request.getParameter( "_cancel" ) != null ) {
			return new ModelAndView( new RedirectView( getSuccessView() ) );
		}
		
		salesTagForm salesTagForm = (salesTagForm) command;

		java.util.Map<String, com.webjaguar.model.Configuration> siteConfig = webJaguar.getSiteConfig();
		File salesTagImageFile = new File( baseFile, "PromoSalesTag" );

		// check if delete button was pressed
		String deleteSalesTag = ServletRequestUtils.getStringParameter( request, "delete" );
		String deleteSalesTagImage = ServletRequestUtils.getStringParameter( request, "delete_salesTagImage" );
		boolean uploadSalesTag = ServletRequestUtils.getBooleanParameter( request, "upload_salesTag", false );

		boolean uploadedImage = false;
		boolean getImageImage = false;
		org.springframework.web.multipart.MultipartFile salesTag_imageName = null;

		if ( deleteSalesTag != null ) {
			this.webJaguar.deleteSalesTagById( salesTagForm.getSalesTag().getTagId() );
			return new org.springframework.web.servlet.ModelAndView( new org.springframework.web.servlet.view.RedirectView( getSuccessView() ) );
		}
		else if ( uploadSalesTag ) {
			String getImageSalesTag = ServletRequestUtils.getStringParameter( request, "getImage_salesTag" );
			if ( salesTagImageFile.canWrite() )
			{
				org.springframework.web.multipart.MultipartHttpServletRequest multipartRequest = (org.springframework.web.multipart.MultipartHttpServletRequest) request;
				salesTag_imageName = multipartRequest.getFile( "salesTag_imageName" );
				if ( salesTag_imageName != null && !salesTag_imageName.isEmpty() )
				{

					if ( salesTag_imageName.getContentType().equalsIgnoreCase( "image/gif" ) ) {
						// do something for gif
						uploadedImage = true;
					}
					else if ( salesTag_imageName.getContentType().equalsIgnoreCase( "image/jpeg" ) ) {
						// do something for jpeg
						uploadedImage = true;
					}
				}
				else if ( getImageSalesTag != null ) {
					getImageImage = true;
				}
			}
		}
		else if ( deleteSalesTagImage != null ) {
			if ( salesTagImageFile.exists() ) {
				new File( baseFile,  "PromoSalesTag/salestag_" + salesTagForm.getSalesTag().getTagId() + ".gif" ).delete();
			}
		}
		Integer salesTagId = null;
		if ( salesTagForm.isNewSalesTag() ){
			salesTagId = this.webJaguar.insertSalesTag( salesTagForm.getSalesTag() );
		}
		else{
			this.webJaguar.updateSalesTag( salesTagForm.getSalesTag() );
		}
		// save images
		if ( uploadedImage ) {
			File newFile = new File( baseFile, "PromoSalesTag/salestag_" + salesTagForm.getSalesTag().getTagId() + ".gif" );
			try{
				salesTag_imageName.transferTo( newFile );
			}
			catch ( IOException e ){

			}
		}
		else if ( getImageImage ) {// get pre Image
			Integer imageSalesTagId = org.springframework.web.bind.ServletRequestUtils.getIntParameter( request, "getImage_name" );
			File preImageFile = new File( baseFile, "SalesTagImages/" + imageSalesTagId + "percent.gif" );
			File newFile;
			if ( salesTagId != null )
				newFile = new File( baseFile, "PromoSalesTag/salestag_" + salesTagId + ".gif" );
			else
				newFile = new File( baseFile, "PromoSalesTag/salestag_" + salesTagForm.getSalesTag().getTagId() + ".gif" );
			try{
				copy( preImageFile, newFile );
			}
			catch ( IOException e ){
			}
		}

		return new org.springframework.web.servlet.ModelAndView( new org.springframework.web.servlet.view.RedirectView( getSuccessView() ) );
	}

	protected boolean suppressBinding(HttpServletRequest request)
	{
		if (  request.getParameter( "_delete" ) != null || request.getParameter( "_cancel" ) != null ) {
			return true;
		}
		return false;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception
	{
		salesTagForm salesTagForm = new salesTagForm();

		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );

		if ( request.getParameter( "id" ) != null )
		{
			Integer salesTagId = new Integer( request.getParameter( "id" ) );
			SalesTag salesTag = this.webJaguar.getSalesTagById( salesTagId );

			salesTagForm.setSalesTag( salesTag );
			salesTagForm.setNewSalesTag( false );
		}

		return salesTagForm;
	}

	// Copies src file to dst file.
	// If the dst file does not exist, it is created
	private void copy(File src, File dst) throws IOException
	{
		InputStream in = new FileInputStream( src );
		OutputStream out = new FileOutputStream( dst );

		// Transfer bytes from in to out
		byte[] buf = new byte[4096];
		int len;
		while ( (len = in.read( buf )) > 0 )
		{
			out.write( buf, 0, len );
		}
		in.close();
		out.close();
	}
}
