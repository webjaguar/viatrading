/*
 * Copyright 2005, 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 03.18.2008 
 */

package com.webjaguar.web.admin.promos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.GiftCard;
import com.webjaguar.model.GiftCardOrder;
import com.webjaguar.model.GiftCardSearch;

public class GiftCardListController extends org.springframework.web.servlet.mvc.AbstractCommandController
{

	public GiftCardListController()
	{
		setCommandClass( com.webjaguar.model.CategorySearch.class );
	}

	private WebJaguarFacade webJaguar;
	public void setWebJaguar( WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }
	
	public ModelAndView handle( HttpServletRequest request, HttpServletResponse response, Object command, BindException errors ) throws Exception
	{
		GiftCardSearch search = getSearch( request );
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		if ( !(Boolean) gSiteConfig.get("gGIFTCARD") )
        	return new ModelAndView(new RedirectView("../"));
		
		if ( !search.getViewType().equals( "order" ) ) {
			List<GiftCard> giftCards = this.webJaguar.getGiftCardList( search, null );

			PagedListHolder giftCardList = new PagedListHolder( giftCards ); 
			giftCardList.setPageSize(search.getPageSize());
			giftCardList.setPage(search.getPage()-1);
			myModel.put( "giftCards", giftCardList );
		} else {
			List<GiftCardOrder> giftCardOrders = this.webJaguar.getGiftCardOrderList( search, null );

			PagedListHolder giftCardOrderList = new PagedListHolder( giftCardOrders ); 
			giftCardOrderList.setPageSize(search.getPageSize());
			giftCardOrderList.setPage(search.getPage()-1);
			myModel.put( "giftCardOrders", giftCardOrderList );
		}
		return new ModelAndView( "admin/promos/giftCards/list", "model", myModel );
	}
	
	private GiftCardSearch getSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		GiftCardSearch search = (GiftCardSearch) request.getSession().getAttribute( "giftCardSearch" );
		if (search == null) {
			search = new GiftCardSearch();
			search.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "giftCardSearch", search );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}				
		
		// view type
		search.setViewType(ServletRequestUtils.getStringParameter( request, "_viewtype", "order" ));
		if ( search.getViewType().equals( "giftcard" ))
			search.setGiftcardOrderId( null );
		
		// item id
		if (request.getParameter("claim_code") != null) {
			search.setClaimCode(ServletRequestUtils.getStringParameter( request, "claim_code", "" ));
		}
		
		// giftcardOrderId
		if (request.getParameter("giftcardOrderId") != null) {
			try {
				search.setGiftcardOrderId(ServletRequestUtils.getIntParameter( request, "giftcardOrderId" ));
				search.setViewType( "giftcard" );
			} catch (Exception e) {
				search.setGiftcardOrderId( null );
			}
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		return search;
	}	
}