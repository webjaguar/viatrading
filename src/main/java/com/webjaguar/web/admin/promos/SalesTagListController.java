/*
 * Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.23.2007 
 */

package com.webjaguar.web.admin.promos;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.SalesTag;
import com.webjaguar.model.Search;

public class SalesTagListController extends org.springframework.web.servlet.mvc.AbstractCommandController
{
	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar( com.webjaguar.logic.WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }
	
	public SalesTagListController()
	{
		setCommandClass( com.webjaguar.model.CategorySearch.class );
	}

	public ModelAndView handle( javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, Object command, org.springframework.validation.BindException errors ) throws Exception
	{
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		Search salesTagSearch = getSalesTagSearch( request );

		// if add promo pushed
		if ( request.getParameter( "__add" ) != null ) {
			return new ModelAndView( new RedirectView( "salesTag.jhtm" ) );
		}

		PagedListHolder salesTagList = new PagedListHolder( this.webJaguar.getSalesTagList() ); 
		salesTagList.setPageSize( salesTagSearch.getPageSize() );
		salesTagList.setPage( salesTagSearch.getPage() - 1 );
		
		Iterator  iterator = salesTagList.getPageList().iterator();
		while ( iterator.hasNext() )
		{
			SalesTag salesTag = ( SalesTag ) iterator.next();
			File salesTagImageFile = new File( getServletContext().getRealPath( "/assets/Image/PromoSalesTag/salestag_" + salesTag.getTagId() + ".gif" ) );
			Properties prop = new Properties();
			try
			{
				prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
				if ( prop.get( "site.root" ) != null )
				{
					salesTagImageFile = new File( (String) prop.get( "site.root" ) + "/assets/Image/PromoSalesTag/salestag_" + salesTag.getTagId() + ".gif" );
				}
			}
			catch ( Exception e )
			{}
			salesTag.setImage( salesTagImageFile.exists() );
		}

		java.util.Map<String, Object> myModel = new java.util.HashMap<String, Object>();
		myModel.put( "salesTags", salesTagList );
		myModel.put( "nOfsalesTags", salesTagList.getNrOfElements() );
		myModel.put( "randomNum", new Random().nextInt( 1000 ) );

		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			myModel.put("_pageinfo", "salestaglist");
			return new ModelAndView("admin-responsive/promos/salesTags/list", "model", myModel);
		}
		
		return new org.springframework.web.servlet.ModelAndView( "admin/promos/salesTags/list", "model", myModel );
	}
	
	private Search getSalesTagSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		Search salesTagSearch = (Search) request.getSession().getAttribute( "salesTagSearch" );
		if (salesTagSearch == null) {
			salesTagSearch = new Search();
			salesTagSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "salesTagSearch", salesTagSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			salesTagSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				salesTagSearch.setPage( 1 );
			} else {
				salesTagSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				salesTagSearch.setPageSize( 10 );
			} else {
				salesTagSearch.setPageSize( size );				
			}
		}
		
		return salesTagSearch;
	}
}