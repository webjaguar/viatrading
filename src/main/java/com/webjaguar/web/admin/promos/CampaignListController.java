package com.webjaguar.web.admin.promos;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.PromoSearch;

public class CampaignListController implements Controller {
	
	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar( com.webjaguar.logic.WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }
	
	

	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		PromoSearch promoSearch = getPromoSearch(request);
		
		promoSearch.setActive(false);
		PagedListHolder promoList = new PagedListHolder( this.webJaguar.getCampaignList( promoSearch) ); 
		promoList.setPageSize( promoSearch.getPageSize() );
		promoList.setPage( promoSearch.getPage() - 1 );
		
		java.util.Map<String, Object> myModel = new java.util.HashMap<String, Object>();
//		myModel.put("name", "AllamChaeib");
		myModel.put( "promotions", promoList );
		myModel.put( "nOfpromotions", ( promoList == null ) ? 0 : promoList.getNrOfElements() );
		
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)
			return new ModelAndView("admin-responsive/promos/promotions/campaignList", "model", myModel);
		return new org.springframework.web.servlet.ModelAndView( "admin/promos/promotions/campaignList", "model", myModel );
		
	}




	private PromoSearch getPromoSearch(HttpServletRequest request) {
		
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		PromoSearch promoSearch = (PromoSearch) request.getSession().getAttribute( "promoSearch" );
		if (promoSearch == null) {
			promoSearch = new PromoSearch();
			promoSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "promoSearch", promoSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			promoSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				promoSearch.setPage( 1 );
			} else {
				promoSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				promoSearch.setPageSize( 10 );
			} else {
				promoSearch.setPageSize( size );				
			}
		}
		
		return promoSearch;
	
	}

}
