/*
 * Copyright 2005, 2009 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 01.05.2009 
 */

package com.webjaguar.web.admin.promos;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.MailInRebateSearch;

public class MailInRebateListController implements Controller
{
	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar( com.webjaguar.logic.WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
    		throws Exception {
		
		MailInRebateSearch mailInRebateSearch = getMailInRebateSearch( request );
		// search criteria

		// if add mailInRebate pushed
		if ( request.getParameter( "__add" ) != null ) {
			return new org.springframework.web.servlet.ModelAndView( new org.springframework.web.servlet.view.RedirectView( "mailInRebate.jhtm" ) ) ;
		}
		
		PagedListHolder mailInRebateList = new PagedListHolder( this.webJaguar.getMailInRebateList( mailInRebateSearch ) ); 
		mailInRebateList.setPageSize( mailInRebateSearch.getPageSize() );
		mailInRebateList.setPage( mailInRebateSearch.getPage() - 1 );
		
		java.util.Map<String, Object> myModel = new java.util.HashMap<String, Object>();
		myModel.put( "mailInRebates", mailInRebateList );
		myModel.put( "nOfmailInRebates", ( mailInRebateList == null ) ? 0 : mailInRebateList.getNrOfElements() );

		return new org.springframework.web.servlet.ModelAndView( "admin/promos/mailInRebates/list", "model", myModel );
	}

	private MailInRebateSearch getMailInRebateSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		MailInRebateSearch mailInRebateSearch = (MailInRebateSearch) request.getSession().getAttribute( "mailInRebateSearch" );
		if (mailInRebateSearch == null) {
			mailInRebateSearch = new MailInRebateSearch();
			mailInRebateSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "mailInRebateSearch", mailInRebateSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			mailInRebateSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				mailInRebateSearch.setPage( 1 );
			} else {
				mailInRebateSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				mailInRebateSearch.setPageSize( 10 );
			} else {
				mailInRebateSearch.setPageSize( size );				
			}
		}
		
		return mailInRebateSearch;
	}
}