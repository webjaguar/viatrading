/* Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 11.16.2007
 */

package com.webjaguar.web.admin.ticket;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessPrivilegeSearch;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.form.TicketForm;

public class TicketUpdateController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
    
    private final String updateTicketAdmin = "admin/ticket/ticketUpdate.jhtm?id=";
    private final String updateTicketUser = "account_ticketUpdate.jhtm?id=";
    private Map<String, Object> gSiteConfig;
    
    private File baseFile;
	
    public TicketUpdateController() {
		setSessionForm(true);
		setCommandName("ticketForm");
		setCommandClass(TicketForm.class);
		setFormView("admin/ticket/ticketUpdate");
		setSuccessView("ticketUpdate.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		
		TicketForm ticketForm = (TicketForm) command;
		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(ticketForm.getTicket().getHost());
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);
		}
		// check if close button was pressed
		if (request.getParameter("__close_ticket") != null) {
			ticketForm.getTicket().setStatus( "close" );
			this.webJaguar.updateTicketStatus( ticketForm.getTicket() );
		}
		// check if update button was pressed
		if (request.getParameter("__update_ticket") != null) {
			if (ticketForm.getTicketVersion().getAccessUserId() != null) {
				ticketForm.getTicketVersion().setCreatedByType( "AdminUser" );
			} else {
				ticketForm.getTicketVersion().setCreatedByType( "Admin" );
			}
			
			ticketForm.getTicketVersion().setOwnerId( ticketForm.getTicket().getTicketId() );
			this.webJaguar.insertTicketVersion( ticketForm.getTicketVersion() );
			this.webJaguar.updateTicketLastModified( ticketForm.getTicketVersion().getOwnerId(), ticketForm.getTicketVersion().getCreatedByType() );
			
			// save attachement
			if ( baseFile.canWrite() ) {
				boolean uploadedAttachment = false;
				MultipartFile attachment = null;
				
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				attachment = multipartRequest.getFile( "attachment" );
				if ( attachment != null && !attachment.isEmpty() ) {
					uploadedAttachment = true;
					if ( uploadedAttachment ) {
						File newFile = new File( baseFile, ticketForm.getTicket().getTicketId()+"_"+ ticketForm.getTicketVersion().getTicketVersionId()+"_"+ attachment.getOriginalFilename() );
						try {
							attachment.transferTo( newFile );
						} catch ( IOException e ) {
							e.printStackTrace();
						}
					}
				}
			}

			// construct admin email message
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, false, "UTF-8");
			try {
				constructMessage(helper, ticketForm, request, ticketForm.getTicket().getUserId(), true, multiStore);
				mailSender.send(mms);
			} catch (Exception e) {
			}
			if ( ticketForm.getTicketVersion().getAccessUserId() == null ) {
				// construct customer email message
				MimeMessageHelper helper2 = new MimeMessageHelper(mms, false, "UTF-8");
				try {
					constructMessage(helper2, ticketForm, request, ticketForm.getTicket().getUserId(), false, multiStore);
					mailSender.send(mms);
				} catch (Exception e) {
				}
			}
		}
		return new ModelAndView(new RedirectView( getSuccessView() ), "id", ticketForm.getTicket().getTicketId() );
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("__close_ticket") != null) {
			return true;			
		}
		return false;
	}
	
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
    	TicketForm ticketForm = (TicketForm) command;
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		List<TicketForm> ticketList  = this.webJaguar.getTicketVersionListById(ticketForm.getTicket().getTicketId(), null);
    		for(TicketForm tForm : ticketList){
    			// check ticket has attachment
    			File[] listOfFiles = baseFile.listFiles();
    			if(listOfFiles != null) {
    				for(int i=0; i<listOfFiles.length; i++){
    					if(listOfFiles[i].isFile() && listOfFiles[i].getName().startsWith(tForm.getTicket().getTicketId()+"_"+tForm.getTicketVersion().getTicketVersionId() + "_")){
    						tForm.getTicketVersion().setAttachment(listOfFiles[i].getName());
    					}
    				}	
    			}
    		}
    		map.put( "questionAnswerList", ticketList);
    		map.put( "user", this.webJaguar.getCustomerById( ticketForm.getTicket().getUserId() ));
    	} catch (Exception e) {}
    	if ((Boolean) gSiteConfig.get("gACCESS_PRIVILEGE")) {
			Map <Integer, AccessUser> accessUserMap = new HashMap<Integer, AccessUser>();
			AccessPrivilegeSearch search = new AccessPrivilegeSearch();
			search.setEnable(true);
			List<AccessUser> accessUserList = this.webJaguar.getUserPrivilegeList(search);
			for (AccessUser accessUser : accessUserList) {
				accessUserMap.put(accessUser.getId(), accessUser);
			}
			map.put("accessUserMap", accessUserMap);
			map.put("accessUserList", accessUserList);
		}
    	return map;
    }  	
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		TicketForm ticketForm = new TicketForm();
		if ( request.getParameter("id") != null ) {
			ticketForm = new TicketForm(this.webJaguar.getTicketById(new Integer(request.getParameter("id"))));
			String userName = SecurityContextHolder.getContext().getAuthentication().getName();
			AccessUser user = this.webJaguar.getUserByUserName(userName);
			if (user!=null && user.getFirstName()!=null && user.getLastName()!=null) {
				ticketForm.getTicketVersion().setCreatedBy( user.getFirstName() + " " + user.getLastName() );
			} else {
				ticketForm.getTicketVersion().setCreatedBy( userName );
			}
			baseFile = new File( getServletContext().getRealPath("temp"));
			try {
				if(!baseFile.exists()){
					baseFile.mkdir();
				}
				baseFile = new File( baseFile, "/Ticket/" );
				if(!baseFile.exists()){
					baseFile.mkdir();
				}
				baseFile = new File( baseFile, "/customer_"+ticketForm.getTicket().getUserId()+"/" );
				if(!baseFile.exists()){
					baseFile.mkdir();
				}
			} catch ( Exception e ){ e.printStackTrace(); }
			
			//check ticket has attachment
			File[] listOfFiles = baseFile.listFiles();
			if(listOfFiles != null) {
				for(int i=0; i<listOfFiles.length; i++){
					if(listOfFiles[i].isFile() && listOfFiles[i].getName().startsWith(ticketForm.getTicket().getTicketId()+"_"+ticketForm.getTicketVersion().getTicketVersionId() + "_")){
						ticketForm.getTicketVersion().setAttachment(listOfFiles[i].getName());
					}
				}	
			}

		} 
		return ticketForm;
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		TicketForm ticketForm = (TicketForm) command;
		if (ticketForm.getTicketVersion().getQuestionDescription() == null || ticketForm.getTicketVersion().getQuestionDescription().equals( "" )) {
			errors.rejectValue("ticketVersion.questionDescription", "form.required", "required");    					
		}
	}
	
	private void constructMessage(MimeMessageHelper helper, TicketForm ticketForm, HttpServletRequest request, int userId, boolean admin, MultiStore multiStore) throws Exception {
		Customer customer = this.webJaguar.getCustomerById( userId );
		// send email
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		if (!((Configuration) siteConfig.get("TICKET_MESSAGE_ON_EMAIL")).getValue().equals( "true" ) || !ticketForm.isOnEmail()) {
			ticketForm.getTicketVersion().setQuestionDescription( "" );
		}
		String contactEmail = ((Configuration) siteConfig.get("CONTACT_EMAIL")).getValue();
		String siteURL = ((Configuration) siteConfig.get("SECURE_URL")).getValue();
		if (multiStore != null){
			contactEmail = multiStore.getContactEmail();
			siteURL = request.getScheme()+"://" + ticketForm.getTicket().getHost() + "/";
		}
    	
    	String ticketContactEmail = ((Configuration) siteConfig.get("TICKET_CONTACT_EMAIL")).getValue();
    	String siteName = ((Configuration) siteConfig.get("SITE_NAME")).getValue();
    	    	
    	String firstName = (customer.getAddress().getFirstName() == null) ? "" : customer.getAddress().getFirstName();  
    	String path = updateTicketAdmin;
    	
    	if (admin) {
    		helper.setFrom(ticketContactEmail);
    		AccessUser user = null;
    		if (ticketForm.getTicketVersion().getAccessUserId() != null) {
    			user = this.webJaguar.getAccessUserById(ticketForm.getTicketVersion().getAccessUserId());
    		}
    		if (user != null && !user.getEmail().isEmpty()) {
    			helper.setTo(user.getEmail());
    		} else {
    			helper.setTo(ticketContactEmail);
    		}
    		firstName = ( user == null ) ? "Admin" : user.getFirstName();
    		helper.setBcc( contactEmail );
    		siteURL = ((Configuration) siteConfig.get("SECURE_URL")).getValue();
    	} else {
    		helper.setFrom(ticketContactEmail);
    		helper.setTo(customer.getUsername());
    		path = updateTicketUser;
    		// cc emails
    		String[] extraEmails = customer.getExtraEmail().split( "[,]" ); // split by commas
    		for ( int x = 0; x < extraEmails.length; x++ ) {
    			if (extraEmails[x].trim().length() > 0) {
    				helper.addCc(extraEmails[x].trim());
    			}
    		}
    		// Bcc to SalesRep
    		if ( (Boolean) gSiteConfig.get("gSHOPPING_CART") && (Boolean) gSiteConfig.get("gSALES_REP")){
    			SalesRep salesRep = this.webJaguar.getSalesRepByUserId( userId );
    			if ( salesRep != null ) {
    				helper.setBcc(  new String[] { contactEmail, salesRep.getEmail() } );
    			}
    		}
    	}
		
		helper.setSubject("Updated " + siteName + " Ticket #" + ticketForm.getTicket().getTicketId());
		StringBuffer message = new StringBuffer();
		message.append("Dear "+firstName+",<br/><br/>The following support ticket has been updated:<br /><br />A comment has been added to the following support ticket.<br /><br />" +
				"Ticket#:  "+ticketForm.getTicket().getTicketId()+"<br />Subject: " + ticketForm.getTicket().getSubject() + "<br /><br />" +
				"You can view this ticket here:<br /><a href=\"" + siteURL + path + ticketForm.getTicket().getTicketId() + "\">" + siteURL + path + ticketForm.getTicket().getTicketId() + "</a><br /><br />" +
				"Thank you,<br />  " + siteName + "<br />" + "<a href=\"" +siteURL + "\" >" +siteURL+ "</a><br /><br />"+
				ticketForm.getTicketVersion().getQuestionDescription() );
		
		helper.setText(message.toString(), true);
 	}
}