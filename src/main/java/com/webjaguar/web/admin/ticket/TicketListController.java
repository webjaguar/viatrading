/* Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 11.16.2007
 */

package com.webjaguar.web.admin.ticket;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.TicketSearch;

public class TicketListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		TicketSearch ticketSearch = getTicketSearch( request );
		Map<String, Object> model = new HashMap<String, Object>();
		
		// get tickets
		PagedListHolder ticketList = new PagedListHolder( this.webJaguar.getTicketsList( ticketSearch ) );
		ticketList.setPageSize( ticketSearch.getPageSize() );
		ticketList.setPage( ticketSearch.getPage() - 1 );
		
		model.put( "tickets", ticketList );
		
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)
			return new ModelAndView("admin-responsive/ticket/ticketList", "model", model);
		return new ModelAndView( "admin/ticket/ticketList", "model", model );
	}
	
	private TicketSearch getTicketSearch(HttpServletRequest request) {
		TicketSearch ticketSearch = (TicketSearch) request.getSession().getAttribute( "ticketSearch" );
		if (ticketSearch == null) {
			ticketSearch = new TicketSearch();
			request.getSession().setAttribute( "ticketSearch", ticketSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			ticketSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}	

		// subject
		if (request.getParameter("subject") != null) {
			ticketSearch.setSubject(ServletRequestUtils.getStringParameter( request, "subject", "" ));
		}
		
		// company
		if (request.getParameter("company") != null) {
			ticketSearch.setCompany(ServletRequestUtils.getStringParameter( request, "company", "" ));
		}
		
		// ticket Id
		if (request.getParameter("ticket_id") != null) {
			ticketSearch.setTicketId(ServletRequestUtils.getStringParameter( request, "ticket_id", "" ));
		}		
		
		// status
		if (request.getParameter("status") != null) {
			ticketSearch.setStatus(ServletRequestUtils.getStringParameter( request, "status", "" ));
		}
		
		// user Id
		if (request.getParameter("user_id") != null) {
			ticketSearch.setUserId(ServletRequestUtils.getIntParameter( request, "user_id", -1 ));
		}
		
		// version created by type
		if (request.getParameter("version_createdby_type") != null) {
			ticketSearch.setVersionCreatedByType(ServletRequestUtils.getStringParameter( request, "version_createdby_type", "" ));
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				ticketSearch.setPage( 1 );
			} else {
				ticketSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				ticketSearch.setPageSize( 10 );
			} else {
				ticketSearch.setPageSize( size );				
			}
		}	
		return ticketSearch;
	}
}
