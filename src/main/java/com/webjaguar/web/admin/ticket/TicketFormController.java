/* Copyright 2005, 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.04.2010
 */

package com.webjaguar.web.admin.ticket;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.form.TicketForm;

public class TicketFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
    private final String updateTicketAdmin = "admin/ticket/ticketUpdate.jhtm?id=";
    private final String updateTicketUser = "account_ticketUpdate.jhtm?id=";
    private Map<String, Object> gSiteConfig;
    private File baseFile;
	
	public TicketFormController() {
		setSessionForm(true);
		setCommandName("ticketForm");
		setCommandClass(TicketForm.class);
		setFormView("admin/ticket/ticketForm");
		setSuccessView("ticketList.jhtm");
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
    	TicketForm ticketForm = (TicketForm) command;
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("ticketList.jhtm"));
    	}
    	Customer customer = this.webJaguar.getCustomerById(ticketForm.getTicket().getUserId());
    	
    	// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);
			ticketForm.getTicket().setHost(request.getHeader("host"));		
		}
		ticketForm.getTicket().setStatus( "open" );
		int ticketNumber = this.webJaguar.insertTicket( ticketForm.getTicket() );

		// save attachement
		boolean uploadedAttachment = false;
		MultipartFile attachment = null;
		if ( baseFile.canWrite() ) {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			attachment = multipartRequest.getFile( "attachment" );
			if ( attachment != null && !attachment.isEmpty() ) {
				uploadedAttachment = true;
			}
		}
		
		if ( uploadedAttachment ) {
			File newFile = new File( baseFile, ticketNumber+"_0_"+attachment.getOriginalFilename() );
			try {
				attachment.transferTo( newFile );
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
		
		// construct email message
		MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms, false, "UTF-8");
		try {
			constructMessage(helper, ticketForm, request, customer, true, multiStore);
			mailSender.send(mms);
		} catch (Exception e) {
		}
		// construct user email message
		MimeMessageHelper helper2 = new MimeMessageHelper(mms, false, "UTF-8");
		try {
			constructMessage(helper2, ticketForm, request, customer, false, multiStore);
			mailSender.send(mms);
		} catch (Exception e) {
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));    	
    }
    
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		TicketForm ticketForm = new TicketForm();
		Integer userId = ServletRequestUtils.getIntParameter(request, "cid", -1);
    	if (userId == -1) {
    		throw new ModelAndViewDefiningException( new ModelAndView( new RedirectView("customers.jhtm") ) );
    	}
    	ticketForm.getTicket().setUserId(userId);
    	
		String userName = SecurityContextHolder.getContext().getAuthentication().getName();
		AccessUser user = this.webJaguar.getUserByUserName(userName);
		if (user!=null && user.getFirstName()!=null && user.getLastName()!=null) {
			ticketForm.getTicket().setCreatedBy( user.getFirstName() + " " + user.getLastName() );
		} else {
			ticketForm.getTicket().setCreatedBy( userName );
		}
		
		baseFile = new File( getServletContext().getRealPath("temp"));
		try {
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
			baseFile = new File( baseFile, "/Ticket/" );
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
			baseFile = new File( baseFile, "/customer_"+userId+"/" );
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
		} catch ( Exception e ){ e.printStackTrace(); }
		
		return ticketForm;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		TicketForm ticketForm = (TicketForm) command;
		if (ticketForm.getTicket().getSubject() == null || ticketForm.getTicket().getSubject().equals( "" )) {
			errors.rejectValue("ticket.subject", "form.required", "required");    					
		}
		if (ticketForm.getTicket().getQuestion() == null || ticketForm.getTicket().getQuestion().equals( "" )) {
			errors.rejectValue("ticket.question", "form.required", "required");    					
		}
	}
	
	private void constructMessage(MimeMessageHelper helper, TicketForm ticketForm, HttpServletRequest request, Customer customer, boolean admin, MultiStore multiStore) throws Exception {
		// send email
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		if (!((Configuration) siteConfig.get("TICKET_MESSAGE_ON_EMAIL")).getValue().equals( "true" ) || !ticketForm.isOnEmail()) {
			ticketForm.getTicket().setQuestion( "" );
		}
    	String contactEmail = ((Configuration) siteConfig.get("CONTACT_EMAIL")).getValue();
    	String ticketContactEmail = ((Configuration) siteConfig.get("TICKET_CONTACT_EMAIL")).getValue();
    	String siteName = ((Configuration) siteConfig.get("SITE_NAME")).getValue();
    	String siteURL = ((Configuration) siteConfig.get("SECURE_URL")).getValue();
    	if (multiStore != null){
			contactEmail = multiStore.getContactEmail();
			siteURL = request.getScheme()+"://" + ticketForm.getTicket().getHost() + "/";
		}
    	
    	String firstName = customer.getAddress().getFirstName();  
    	if (firstName == null) {
    		firstName = "";
    	}
    	String path = updateTicketAdmin;
    	
    	if (admin) {
    		helper.setFrom(ticketContactEmail);
    		helper.setTo(ticketContactEmail);
    		helper.setBcc(ticketContactEmail);
    		siteURL = ((Configuration) siteConfig.get("SECURE_URL")).getValue();
    		firstName = "Admin";
    	} else {
    		helper.setFrom(ticketContactEmail);
    		helper.setTo(customer.getUsername());
    		path = updateTicketUser;
    		// cc emails
    		String[] extraEmails = customer.getExtraEmail().split( "[,]" ); // split by commas
    		for ( int x = 0; x < extraEmails.length; x++ ) {
    			if (extraEmails[x].trim().length() > 0) {
    				helper.addCc(extraEmails[x].trim());
    			}
    		}
    		// Bcc to SalesRep
    		if ( (Boolean) gSiteConfig.get("gSHOPPING_CART") && (Boolean) gSiteConfig.get("gSALES_REP")){
    			SalesRep salesRep = this.webJaguar.getSalesRepByUserId( customer.getId() );
    			if ( salesRep != null ) {
    				helper.setBcc(  new String[] { contactEmail, salesRep.getEmail() } );
    			}
    		}
    	}

		helper.setSubject("Created " + siteName + " Ticket #" + ticketForm.getTicket().getTicketId());
		StringBuffer message = new StringBuffer();
		message.append("Dear "+firstName+",<br/><br/>The following support ticket has been created:<br /><br />A comment has been added to the following support ticket.<br /><br />" +
				"Ticket#:  "+ticketForm.getTicket().getTicketId()+"<br />Subject: " + ticketForm.getTicket().getSubject() + "<br /><br />" +
				"You can view this ticket here:<br /><a href=\"" + siteURL + path + ticketForm.getTicket().getTicketId() + "\">" + siteURL + path + ticketForm.getTicket().getTicketId() + "</a><br /><br />" +
				"Thank you,<br />  " + siteName + "<br />" + siteURL + "<br /><br />"+
				ticketForm.getTicket().getQuestion().toString());
		
		
		helper.setText(message.toString(), true);
 	}
}
