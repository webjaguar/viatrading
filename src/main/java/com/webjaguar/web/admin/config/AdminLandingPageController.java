/* Copyright 2006 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 07.10.2009
 */

package com.webjaguar.web.admin.config;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller; 
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.model.Configuration;


public class AdminLandingPageController implements Controller {
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	
    	Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
    	
    	if (siteConfig.get("ADMIN_LANDING_PAGE").getValue().equals("catalog/productList.jhtm")) { 
			return new ModelAndView(new RedirectView("../catalog/productList.jhtm"));
    	}
    	else if (siteConfig.get("ADMIN_LANDING_PAGE").getValue().equals("customers/customerList.jhtm")) {
			return new ModelAndView(new RedirectView("../customers/customerList.jhtm"));
    	} 
    	else if (siteConfig.get("ADMIN_LANDING_PAGE").getValue().equals("orders/ordersList.jhtm")) {
			return new ModelAndView(new RedirectView("../orders/ordersList.jhtm"));
    	}
    	else if (siteConfig.get("ADMIN_LANDING_PAGE").getValue().equals("reports/")) {
			return new ModelAndView(new RedirectView("../reports/"));
    	}
    	else if (siteConfig.get("ADMIN_LANDING_PAGE").getValue().equals("message/")) {
			return new ModelAndView(new RedirectView("../message/"));
    	}
    	else  {
			return new ModelAndView(new RedirectView("../catalog/index.jsp"));
    	}
    }
}
