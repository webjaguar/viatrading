/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 10.20.2006
 */

package com.webjaguar.web.admin.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ShippingMethod;

public class ShippingMethodListController implements Controller{
	
	private WebJaguarFacade webJaguar;
	
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
	        throws Exception {

		// construct map model
		Map<String, Object> myModel = new HashMap<String, Object>();
		
        // check if update button was pressed
		if (request.getParameter("__update") != null) {
			updateSiteConfig(request);
			updateShippingMethod(request);
			myModel.put("message", "update.successful");
		}
		myModel.put("siteConfig", webJaguar.getSiteConfig());
		myModel.put("shippingmethodlist", this.webJaguar.getShippingMethodList(null));    
		myModel.put("countrylist", this.webJaguar.getCountryList(false)); 
		myModel.put("statelist", this.webJaguar.getStateList("US")); 
		myModel.put("caProvinceList", this.webJaguar.getStateList("CA"));
		
		// return ModelAndView
        return new ModelAndView("admin/config/shippingList", "model", myModel);
		
	}
	
    private void updateShippingMethod(HttpServletRequest request) {
    	Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		// set a List contains countries
		List<ShippingMethod> data = new ArrayList<ShippingMethod>();
		
		// get a list of ShippingMethod ids from http request
		String ids [] = request.getParameterValues("__id");
		
		// create ShippingMethod objects based on ids
		for (int i=0;i<ids.length;i++){
			try {
				    ShippingMethod shippingMethod = new ShippingMethod();
                    // set id
					Integer id = new Integer (ids[i]);
					shippingMethod.setId(id);
					// set rank
					Integer rank = new Integer(request.getParameter("__rank_" + id));
					shippingMethod.setShippingRank(rank);
					// set active
					String active = request.getParameter("__active_" + id);   
					if(active!=null){
						shippingMethod.setShippingActive(true);   
					}
					// set title
					String title = request.getParameter("__title_" + id);
					shippingMethod.setShippingTitle(title);
					// set temperature
					if ( (Boolean) gSiteConfig.get( "gWEATHER_CHANNEL" ) ) {	
						String temperature = request.getParameter("__weather_temp_" + id);
						if ( temperature != "" ) {
							shippingMethod.setWeatherTemp(new Integer(temperature));
						}
					}
					// get min and max price
					try {
						shippingMethod.setMinPrice(ServletRequestUtils.getDoubleParameter(request, "__minPrice_" + id));
					} catch (Exception e) {
						shippingMethod.setMinPrice(null);
					}
					try {
						shippingMethod.setMaxPrice(ServletRequestUtils.getDoubleParameter(request, "__maxPrice_" + id));
					} catch (Exception e) {
						shippingMethod.setMaxPrice(null);
					}
					
					data.add(shippingMethod);
				} catch (NumberFormatException e) {}
		}
			
		this.webJaguar.updateShippingMethodList(data);	
			
	}
    
	private void updateSiteConfig(HttpServletRequest request) {

		List<Configuration> data = new ArrayList<Configuration>();

		String keys[] = request.getParameterValues("__key");
		
		for (int i=0; i<keys.length; i++){
			Configuration config = new Configuration();
			config.setKey(keys[i]);
			if ( keys[i].equals( "PACKAGING_DIMENSION_LENGTH" ) || keys[i].equals( "PACKAGING_DIMENSION_WIDTH" ) || keys[i].equals( "PACKAGING_DIMENSION_HEIGHT" ) ) {
				try {
					Integer intValue = Integer.parseInt( request.getParameter(keys[i]) );
					config.setValue( intValue.toString() );	
				}
				catch ( NumberFormatException e ) {
					config.setValue("");
				}
			}
			else 
				config.setValue(request.getParameter(keys[i]));
			data.add(config); 
		}
			
		this.webJaguar.updateSiteConfig(data);	
	}      

}
