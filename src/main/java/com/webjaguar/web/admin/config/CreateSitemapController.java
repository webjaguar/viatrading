/* Copyright 2005, 2006 Advanced E-Media Solutions
 * @author Jwalant Patel
 */

package com.webjaguar.web.admin.config;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.CategorySearch;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductSearch;

public class CreateSitemapController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	private Map<String, Object> gSiteConfig;
	private Map<String, Configuration> siteConfig;
	
	private File baseFile;
	private final int MAX_URL = 49000;
	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	
	public CreateSitemapController() {
		setSessionForm(false);
		setCommandName("createSitemap");
		setCommandClass(ProductSearch.class);
		setFormView("admin/config/createSitemap");
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		
		ProductSearch search = (ProductSearch) command;
    	
		Map<String, Object> map = new HashMap<String, Object>();
		
		// run this on server
		// chgrp psacln httpdocs and chmod 775 httpdocs
		if (baseFile.canWrite()) {
			File file[] = baseFile.listFiles();
			List<Map> exportedFiles = new ArrayList<Map>();
			for (int f=0; f<file.length; f++) {
				if (file[f].getName().startsWith("siteMap_") & file[f].getName().endsWith(".xml")) {
					file[f].delete();
				}
			}
			map.put("exportedFiles", exportedFiles);
			
			int productCount = generateProductSiteMap(search, exportedFiles);

			
			int categoryCount = generateCategorySiteMap(new CategorySearch(), exportedFiles);

			
			// we might add UI to select what pages appear on sitemap
			int fixUrlCount = generateFixUrlSiteMap(search, exportedFiles);
			
			// create sitemap index
			if(exportedFiles.size()>1){
				for(int i=0;i<this.createIndexFile(exportedFiles,siteConfig).size();i++){
					String fileName = "/sitemap";
					Writer output = null;
					File newFile = new File(baseFile.getAbsolutePath()+fileName+".xml");
					output = new BufferedWriter(new FileWriter(newFile));
					output.write(this.createIndexFile(exportedFiles,siteConfig).get(i));
					output.close();
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", newFile);
					exportedFiles.add( fileMap );
				}
			}
		}  else {
			map.put("message", "FolderNotFound");
		}
		
		return showForm(request, response, errors, map);
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		siteConfig = this.webJaguar.getSiteConfig();
		 
		ProductSearch search = new ProductSearch();
		search.setSearchable(true);
		if ((Boolean) gSiteConfig.get("gMASTER_SKU") && Integer.parseInt(siteConfig.get("PARENT_CHILDERN_DISPLAY").getValue()) == 0) {
			search.setMasterSku(true);
		}
		
		//baseFile = new File(getServletContext().getRealPath("/assets/"));
		baseFile = new File(getServletContext().getRealPath("/"));
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				//baseFile = new File( (String) prop.get( "site.root" ) + "/assets/" );
				baseFile = new File( (String) prop.get( "site.root" ) + "/" );
			}
		} catch ( Exception e ) {}
		File file[] = baseFile.listFiles();
		List<Map> exportedFiles = new ArrayList<Map>();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith("siteMap_") & file[f].getName().endsWith(".xml")) {
				HashMap<String, Object> fileMap = new HashMap<String, Object>();
				fileMap.put("file", file[f]);
				fileMap.put("lastModified", new Date(file[f].lastModified()));
				exportedFiles.add( fileMap );				
			}
		}
		request.setAttribute("exportedFiles", exportedFiles);		
		return search;
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		Map<String, Object> map = new HashMap<String, Object>();
		if (!baseFile.canWrite()){
			map.put("message", "Please contact admin to enable Sitemap feature.");		
		}
		return map;
	}
	
    private ArrayList<String> createProductXML(List<Product> productList) throws Exception {
    	int j = 0;
    	int count=0;

    	ArrayList<String> content = new ArrayList<String>();
    	String xmlContentHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
								  "<urlset xmlns = \"http://www.sitemaps.org/schemas/sitemap/0.9\">";
	  	StringBuffer xmlContent = new StringBuffer();
	  	
	  	//for Product
		for(int i=j;i<productList.size();i++){
			count=count+1;
			
			if(i == j){
				xmlContent = new StringBuffer();
				xmlContent.append(xmlContentHeader);
				if(i == 0){
					xmlContent.append(
							"<url>" +
							"<loc>"+siteConfig.get("SITE_URL").getValue()+"</loc>" +
							"<lastmod>"+df.format(new Timestamp(new Date().getTime()))+"</lastmod>" +
							"<changefreq>"+"monthly"+"</changefreq>" + 
							"<priority>"+productList.get(i).getSiteMapPriority()+"</priority>" +
							"</url>"
							);
				}
			}
			String productLink = null;
			if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && productList.get(i).getSku() != null) {
				productLink = siteConfig.get("MOD_REWRITE_PRODUCT").getValue() + "/" + productList.get(i).getEncodedSku() + "/" + productList.get(i).getEncodedName() + ".html";
			} else {
				productLink = "product.jhtm?id=" + productList.get(i).getId();
			}
			xmlContent.append(
				"<url>" +
				"<loc>"+siteConfig.get("SITE_URL").getValue()+productLink+"</loc>" +
				"<lastmod>"+((productList.get(i).getLastModified() != null) ? df.format(productList.get(i).getLastModified()) : df.format(productList.get(i).getCreated()))+"</lastmod>" +
				"<changefreq>"+"monthly"+"</changefreq>" + 
				"<priority>"+productList.get(i).getSiteMapPriority()+"</priority>" +
				"</url>"
				);
			if((xmlContent.length()>9999500)||(i==(productList.size()-1))||count>MAX_URL){
				xmlContent.append("</urlset>");
				if(i!=(productList.size()-1)){ j = i+1; }
				else{ j=0; }
				count=0;
				content.add(xmlContent.toString());
			}
		}
		
		return content;
    }
    
    private int generateProductSiteMap(ProductSearch search, List<Map> exportedFiles) throws Exception {
    	int productCount = this.webJaguar.productCount(search);
		int limit = MAX_URL;
		int numFile = 0;
		search.setLimit(limit);

    	String fileName = "/siteMapProduct_";

    	for (int offset=0, excel = 1; offset<productCount; excel++) {
	    	int start = offset + 1;
	    	int end = (offset + limit) < productCount ? (offset + limit) : productCount;
	    	ArrayList<String> xmlContent = createProductXML(this.webJaguar.getSiteMapProductList(search));
	    	if ( !xmlContent.isEmpty() ) {
	    		Writer output = null;
			  	File newFile = new File(baseFile.getAbsolutePath()+fileName+numFile++ +".xml");
			  	output = new BufferedWriter(new FileWriter(newFile));
		    	output.write(xmlContent.get(0));
		    	output.close();
		    	HashMap<String, Object> fileMap = new HashMap<String, Object>();
				fileMap.put("file", newFile);
				exportedFiles.add( fileMap );  		
		        offset = offset + limit;
		        search.setOffset(offset);
	    	}
    	}
    	
    	return productCount;
    }
	
    private int generateCategorySiteMap(CategorySearch search, List<Map> exportedFiles) throws Exception {
    	search.setProtectedAccess("0");
    	List<Category> categoryList = this.webJaguar.getSiteMapCategoryList( search );
    	String fileName = "/siteMapCategory";
    	
    	List<String> xmlContent = createCategoryXML(categoryList);
    	if ( !xmlContent.isEmpty() ) {
	    	Writer output = null;
		  	File newFile = new File(baseFile.getAbsolutePath()+fileName+".xml");
		  	output = new BufferedWriter(new FileWriter(newFile));
	    	output.write(xmlContent.get(0));
	    	output.close();
	    	HashMap<String, Object> fileMap = new HashMap<String, Object>();
			fileMap.put("file", newFile);
			exportedFiles.add( fileMap );
    	}
		
    	return categoryList.size();
    }
    
    private ArrayList<String> createCategoryXML(List<Category> categoryList) throws Exception {
    	ArrayList<String> content = new ArrayList<String>();
    	String xmlContentHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
								  "<urlset xmlns = \"http://www.sitemaps.org/schemas/sitemap/0.9\">";
	  	StringBuffer xmlContent = new StringBuffer();
    	
	  	xmlContent = new StringBuffer();
		xmlContent.append(xmlContentHeader);
	  	
    	for (Category category: categoryList) {
    		String categoryLink = null;
			if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1")) {
				categoryLink = siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/" + category.getId() + "/" + category.getEncodedName() + ".html" ;
			} else {
				categoryLink = "category.jhtm?"+"cid="+category.getId();
			}
			xmlContent.append(
    				"<url>" +
    				"<loc>"+siteConfig.get("SITE_URL").getValue()+categoryLink+"</loc>" +
    				"<lastmod>"+((category.getLastModified() != null) ? df.format(category.getLastModified()) : df.format(category.getCreated()))+"</lastmod>" +
    				"<changefreq>"+"monthly"+"</changefreq>" + 
    				"<priority>"+category.getSiteMapPriority()+"</priority>" +
    				"</url>"
    				);
    	}
    	content.add(xmlContent.append("</urlset>").toString());
    	
    	return content;
    }
    
    private int generateFixUrlSiteMap(ProductSearch search, List<Map> exportedFiles) throws Exception {
    	List<String> fixUrlList = getfixUrl();
    	String fileName = "/siteMapFixUrl";
    	
    	List<String> xmlContent = createFixUrlXML(fixUrlList);
    	
    	Writer output = null;
	  	File newFile = new File(baseFile.getAbsolutePath()+fileName+".xml");
	  	output = new BufferedWriter(new FileWriter(newFile));
    	output.write(xmlContent.get(0));
    	output.close();
    	HashMap<String, Object> fileMap = new HashMap<String, Object>();
		fileMap.put("file", newFile);
		exportedFiles.add( fileMap );  
		
    	return fixUrlList.size();
    }
    
    private ArrayList<String> createFixUrlXML(List<String> fixUrlList) throws Exception {
    	ArrayList<String> content = new ArrayList<String>();
    	String xmlContentHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
								  "<urlset xmlns = \"http://www.sitemaps.org/schemas/sitemap/0.9\">";
	  	StringBuffer xmlContent = new StringBuffer();
    	
	  	for(int i=0;i<fixUrlList.size();i++){
			if(i == 0){
				xmlContent = new StringBuffer();
				xmlContent.append(xmlContentHeader);
			}
			xmlContent.append(
				"<url>" +
				"<loc>"+siteConfig.get("SITE_URL").getValue()+fixUrlList.get(i)+"</loc>" +
				"<lastmod>"+df.format(new Date().getTime())+"</lastmod>" +
				"<changefreq>"+"monthly"+"</changefreq>" + 
				"<priority>"+0.5+"</priority>" +
				"</url>"
				);
			if((xmlContent.length()>9999500)||(i==(fixUrlList.size()-1))){
				xmlContent.append("</urlset>");
				content.add(xmlContent.toString());
			}
		}
    	
    	return content;
    }
    
    private List getfixUrl() {
		// for fix urls
		ArrayList<String> urlTags = new ArrayList<String>();
		
		urlTags.add("register.jhtm");
		urlTags.add("contactus.jhtm");
		urlTags.add("aboutus.jhtm");
		urlTags.add("faq.jhtm");
		urlTags.add("policy.jhtm");
		
		return urlTags;
    }
    
	private ArrayList<String> createIndexFile(List<Map> exportedFiles,Map<String, Configuration> siteConfig) throws Exception {
    	int j = 0;
    	int count=0;
    	ArrayList<String> content = new ArrayList<String>();
    	String xmlContentHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
								  "<sitemapindex xmlns = \"http://www.sitemaps.org/schemas/sitemap/0.9\">";
	  	StringBuffer xmlContent = new StringBuffer();
	  	for(int i=j;i<exportedFiles.size();i++){
			count=count+1;
			if(i == j){
				xmlContent = new StringBuffer();
				xmlContent.append(xmlContentHeader);
			}
			xmlContent.append(
				"<sitemap>" +
				"<loc>"+siteConfig.get("SITE_URL").getValue()+((File) exportedFiles.get(i).get("file")).getName() +"</loc>" +
				"<lastmod>"+df.format((new Date(((File) exportedFiles.get(i).get("file")).lastModified())))+"</lastmod>" +
				"</sitemap>"
				);
			if((xmlContent.length()>9999500)||(i==(exportedFiles.size()-1))||count>49999){
				xmlContent.append("</sitemapindex>");
				if(i!=(exportedFiles.size()-1)){ j = i+1; }
				else{ j=0; }
				count=0;
				content.add(xmlContent.toString());
			}
		}
		return content;
	}
}