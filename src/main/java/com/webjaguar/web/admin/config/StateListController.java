/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 10.10.2006
 */

package com.webjaguar.web.admin.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.State;

public class StateListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        // check if update ranking button was pressed
		if (request.getParameter("__update") != null) {
			updateState(request);
		}

		Map<String, Object> myModel = new HashMap<String, Object>();
		List stateList = this.webJaguar.getStateList("US");
		myModel.put("statelist", stateList);    
		List caStateList = this.webJaguar.getStateList("CA");
		myModel.put("caStateList", caStateList);  
		      
        return new ModelAndView("admin/config/stateList", "model", myModel);
    }
	
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private void updateState(HttpServletRequest request) {
		
		// get a list of state codes from http request
		String codes [] = request.getParameterValues("__code");
		
		// update tax rate
		List<State> data = new ArrayList<State>();
		// create country objects based on ids
		for (int i=0;i<codes.length;i++){
			try {
				State state = new State();
				// set code
				state.setCode(codes[i]);
				state.setTaxRate(new Double(ServletRequestUtils.getDoubleParameter(request, "__taxrate_" + codes[i], 0.0)));
				state.setApplyShippingTax(ServletRequestUtils.getBooleanParameter(request, "__applyShippingTax_" + codes[i], false));
				state.setRegion(ServletRequestUtils.getStringParameter(request, "__region_" + codes[i], ""));
				data.add(state);
			} catch (NumberFormatException e) {}
		}
		this.webJaguar.updateStateList(data);	
			
	}
	
}
