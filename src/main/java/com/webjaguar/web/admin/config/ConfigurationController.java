/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.31.2006
 */

package com.webjaguar.web.admin.config;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.listener.SessionListener;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;

public class ConfigurationController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }		
	
	private GlobalDao globalDao;	
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    	throws Exception {	

		Map<String, Object> model = new HashMap<String, Object>();    	

    	// check if update button was pressed
		if (request.getParameter("__update") != null) {
			updateSiteConfig(request);
			updateGlobalSiteConfig(request);
			if( ServletRequestUtils.getBooleanParameter( request, "access_user", false )) {
				AccessUser accessUser = globalDao.getAdminInfo();
				accessUser.setEnable( ServletRequestUtils.getBooleanParameter( request, "access_user_enable", false ) );
				globalDao.updateAdminInfo( accessUser );
			}
			model.put("message", "update.successful");
		}
		
		Set<Integer> ids = new HashSet<Integer>();
		ids.addAll(SessionListener.getActiveSessions().values());
		int loggedIn = ids.size();
		if (ids.contains(null)) loggedIn--;
		
		model.put("loggedIn", loggedIn);
		model.put("activeSessions", SessionListener.getActiveSessions().size());
		model.put("countrylist", this.webJaguar.getCountryList(false)); 
		model.put("statelist", this.webJaguar.getStateList("US")); 
		model.put("caProvinceList", this.webJaguar.getStateList("CA"));
    	model.put("siteConfig", webJaguar.getSiteConfig());
      	model.put("gSiteConfig", globalDao.getGlobalSiteConfig());  
      	setProtectedAccess(model);
		model.put("siteMessages", this.webJaguar.getSiteMessageList());
		model.put( "accessUser", globalDao.getAdminInfo() );
		
		// labels
		model.put("labels", this.webJaguar.getLabels("protected"));
    	
    	return new ModelAndView("admin/config/config", model);
    }
    
	private void updateSiteConfig(HttpServletRequest request) {

		List<Configuration> data = new ArrayList<Configuration>();

		String keys[] = request.getParameterValues("__key");
		
		for (int i=0; i<keys.length; i++){
			Configuration config = new Configuration();
			config.setKey(keys[i]);
			String value = request.getParameter(keys[i]);
			config.setValue(value);
			data.add(config); 
		}
		
		String mkeys[] = request.getParameterValues("__mkey");
		if (mkeys != null) {
			for (int i=0; i<mkeys.length; i++){
				Configuration config = new Configuration();
				config.setKey(mkeys[i]);
				StringBuffer sbuff = new StringBuffer();
				String values[] = request.getParameterValues(mkeys[i]);
				if (values != null) {
					for (int x=0; x<values.length; x++) {
						sbuff.append( values[x] + "," );
					}			
				}
				config.setValue(sbuff.toString());
				data.add(config); 
			}			
		}
		
   		int gPROTECTED = 10;	// old code only works if logged in as AEM
		if (gPROTECTED > 0) {
			String protectedKeys[] = {"PROTECTED_ACCESS_ANONYMOUS", "PROTECTED_ACCESS_NEW_REGISTRANT", "TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT"};
			for (int i=0; i<protectedKeys.length; i++) {
				Configuration config = new Configuration();
				config.setKey(protectedKeys[i]);
	       		StringBuffer protectedAccess = new StringBuffer();
	           	for (int p=0; p<gPROTECTED; p++) {
	           		if (request.getParameter(protectedKeys[i] + "_" + p) != null) {
	           			protectedAccess.insert(0, '1');
	           		} else {
	           			protectedAccess.insert(0, '0');
	           		}
	           	}
	           	// remove leading 0's
	           	while (protectedAccess.length() > 1) {
		           	if (protectedAccess.charAt(0) == '0') {
		           		protectedAccess.deleteCharAt(0);
		           	} else {
		           		break;
		           	}
	           	}
				config.setValue(protectedAccess.toString());
				data.add(config); 
			}
		}
			
		this.webJaguar.updateSiteConfig(data);	
	}   
	
	private void updateGlobalSiteConfig(HttpServletRequest request) {

		Map<String, Object> data = new HashMap<String, Object>();

		String gkeys[] = request.getParameterValues("__gkey");
		if (gkeys != null) {
			for (int i=0; i<gkeys.length; i++){
				data.put(gkeys[i], request.getParameter(gkeys[i]));	
				if ( gkeys[i].equals( "gACCESS_PRIVILEGE" ) && request.getParameter(gkeys[i]) == null ) {
					this.webJaguar.disableAccessPrivilege();
				}
			}
		}
		String gkeyArrays[] = request.getParameterValues("__gkeyArray");
		if (gkeyArrays != null) {
			for (int i=0; i<gkeyArrays.length; i++) {
				StringBuffer sbuff = new StringBuffer("");
				String gValues[] = request.getParameterValues( gkeyArrays[i] );
				if (gValues != null) {
					for (int x=0; x<gValues.length; x++) {
						sbuff.append( gValues[x] + "," );
					}						
				}
				data.put(gkeyArrays[i], sbuff.toString());	
			}
		}	
		if (!data.isEmpty()) {
			this.globalDao.updateGlobalSiteConfig(data);			
		}
	}  
	
	private void setProtectedAccess(Map<String, Object> model) {
		int gPROTECTED = (Integer) ((Map) model.get("gSiteConfig")).get("gPROTECTED");
		
		// protected access for anonymous users (not logged in)
		boolean protectedAccessAnonymous[] = new boolean[gPROTECTED];
		StringBuffer PROTECTED_ACCESS_ANONYMOUS = new StringBuffer(((Map<String, Configuration>) model.get("siteConfig")).get("PROTECTED_ACCESS_ANONYMOUS").getValue());
		PROTECTED_ACCESS_ANONYMOUS.reverse();
		for (int i=0; i<gPROTECTED; i++) {
			if (PROTECTED_ACCESS_ANONYMOUS.length() > i && PROTECTED_ACCESS_ANONYMOUS.charAt(i) == '1') {
				protectedAccessAnonymous[i] = true;
			}
		}	
		model.put("protectedAccessAnonymous", protectedAccessAnonymous);

		// protected access for new registrants
		boolean protectedAccessNewRegistrant[] = new boolean[gPROTECTED];
		StringBuffer PROTECTED_ACCESS_NEW_REGISTRANT = new StringBuffer(((Map<String, Configuration>) model.get("siteConfig")).get("PROTECTED_ACCESS_NEW_REGISTRANT").getValue());
		PROTECTED_ACCESS_NEW_REGISTRANT.reverse();
		for (int i=0; i<gPROTECTED; i++) {
			if (PROTECTED_ACCESS_NEW_REGISTRANT.length() > i && PROTECTED_ACCESS_NEW_REGISTRANT.charAt(i) == '1') {
				protectedAccessNewRegistrant[i] = true;
			}
		}
		model.put("protectedAccessNewRegistrant", protectedAccessNewRegistrant);
		
		// protected access for new registrants w/ tracking code 1
		boolean tcProtectedAccessNewRegistrant[] = new boolean[gPROTECTED];
		StringBuffer TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT = new StringBuffer(((Map<String, Configuration>) model.get("siteConfig")).get("TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT").getValue());
		TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT.reverse();
		for (int i=0; i<gPROTECTED; i++) {
			if (TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT.length() > i && TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT.charAt(i) == '1') {
				tcProtectedAccessNewRegistrant[i] = true;
			}
		}
		model.put("tcProtectedAccessNewRegistrant", tcProtectedAccessNewRegistrant);
	}
}
