/*
 * Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.23.2007 
 */

package com.webjaguar.web.admin.config;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.Search;

public class CustomShippingListController implements Controller
{
	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar( com.webjaguar.logic.WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
    		throws Exception {
		
		Search customShippingSearch = getCustomShippingSearch( request );
		
		// add Custom Shipping
		if ( request.getParameter( "__add" ) != null ) {
			return new org.springframework.web.servlet.ModelAndView( new org.springframework.web.servlet.view.RedirectView( "customShipping.jhtm" ) );
		}

		PagedListHolder customShippingList = new PagedListHolder( this.webJaguar.getCustomShippingRateList(true, true) ); 
			customShippingList.setPageSize( customShippingSearch.getPageSize() );
			customShippingList.setPage( customShippingSearch.getPage() - 1 );
		
		java.util.Map<String, Object> myModel = new java.util.HashMap<String, Object>();
		myModel.put( "customShipping", customShippingList );
		myModel.put( "nOfcustomShipping", ( customShippingList == null ) ? 0 : customShippingList.getNrOfElements() );
		
		return new org.springframework.web.servlet.ModelAndView( "admin/config/customShippingList", "model", myModel );
	}
	
	private Search getCustomShippingSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		Search customShippingSearch = (Search) request.getSession().getAttribute( "customShippingSearch" );
		if (customShippingSearch == null) {
			customShippingSearch = new Search();
			customShippingSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "customShippingSearch", customShippingSearch );
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				customShippingSearch.setPage( 1 );
			} else {
				customShippingSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				customShippingSearch.setPageSize( 10 );
			} else {
				customShippingSearch.setPageSize( size );				
			}
		}
		
		return customShippingSearch;
	}
}