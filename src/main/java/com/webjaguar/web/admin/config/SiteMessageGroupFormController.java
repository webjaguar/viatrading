package com.webjaguar.web.admin.config;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataAccessException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.SiteMessageGroup;
import com.webjaguar.web.form.SiteMessageGroupForm;

public class SiteMessageGroupFormController  extends SimpleFormController {
	
	private WebJaguarFacade webJaguar; 
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public SiteMessageGroupFormController() {
		
		setSessionForm( true );
		setCommandName( "siteMessageGroupForm" );
		setCommandClass( com.webjaguar.web.form.SiteMessageGroupForm.class );
		setFormView( "admin/config/siteMessageGroupForm" );
		setSuccessView( "siteMessageGroup.jhtm" );
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
    	
    	SiteMessageGroupForm groupForm = (SiteMessageGroupForm) command;
    	Map<String, Object> model = new HashMap<String, Object>();
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			if (this.webJaguar.getSiteMessageGroupById(groupForm.getSiteMessageGroup().getId()).getNumMessage()>0) {
				model.put("message", "groupHasMessage");
				return showForm(request, response, errors, model);	
			} else {
				this.webJaguar.deleteSiteMessageGroupById( groupForm.getSiteMessageGroup().getId() );
				return new ModelAndView(new RedirectView(getSuccessView()));
			}
		}     	
     	
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}    	
		try {
			if (groupForm.isNewSiteMessageGroup()) {
				this.webJaguar.insertSiteMessageGroup(groupForm.getSiteMessageGroup());
			} else {
				this.webJaguar.updateSiteMessageGroup(groupForm.getSiteMessageGroup());
			}
		} catch (DataAccessException e) {
			model.put("message", "form.hasDuplicate");
			return showForm(request, response, errors, model);	
		}
		return new ModelAndView(new RedirectView( getSuccessView()));  
    }

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}    
			
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors)
	{
		SiteMessageGroupForm groupForm = (SiteMessageGroupForm) command;
 
       	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();
	   
	    map.put("model", myModel);  
	    return map;
    }  
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		SiteMessageGroupForm groupForm;
		Integer groupId = ServletRequestUtils.getIntParameter( request, "id", -1 );
		if ( groupId != -1 ) {
			SiteMessageGroup group = this.webJaguar.getSiteMessageGroupById( groupId );
			groupForm = new SiteMessageGroupForm(group);
		} else {
			groupForm = new SiteMessageGroupForm();
		}
		return groupForm;
	}
}