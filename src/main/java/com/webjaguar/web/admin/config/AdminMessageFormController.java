/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.20.2008
 */

package com.webjaguar.web.admin.config;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.SiteMessage;

public class AdminMessageFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	public AdminMessageFormController() {
		setCommandName("adminMessage");
		setCommandClass(SiteMessage.class);
		setFormView("admin/config/adminMessageForm");
		setSuccessView("admin/config/adminMessageForm");
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,	BindException errors) throws Exception {
		SiteMessage adminMessage = (SiteMessage) command;
		
		this.webJaguar.updateAdminMessage(adminMessage);

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("message", "update.successful");
		map.put("adminMessage", adminMessage);  		
		
		return new ModelAndView(getSuccessView(), map);

	}

	protected Object formBackingObject(javax.servlet.http.HttpServletRequest request) throws Exception {
		return this.webJaguar.getAdminMessageById(1);
	}

}
