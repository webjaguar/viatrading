/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.01.2007
 */

package com.webjaguar.web.admin.config;

import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.webjaguar.web.form.PaymentMethodForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.PaymentMethod;

public class PaymentMethodController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public PaymentMethodController() {
		setSessionForm(false);
		setCommandName("paymentMethodForm");
		setCommandClass(PaymentMethodForm.class);
		setFormView("admin/config/paymentForm");
		setSuccessView("admin/config/paymentForm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {

		PaymentMethodForm form = (PaymentMethodForm) command;
		
		// get custom payment methods
		for (PaymentMethod customPayment: form.getCustomPaymentMethods()) {
			customPayment.setTitle(ServletRequestUtils.getStringParameter(request, "__customTitle_" + customPayment.getId()));
			customPayment.setEnabled(ServletRequestUtils.getBooleanParameter(request, "__customEnabled_" + customPayment.getId(), false));
			customPayment.setInternal(ServletRequestUtils.getBooleanParameter(request, "__customInternal_" + customPayment.getId(), false));
			customPayment.setCode(ServletRequestUtils.getStringParameter(request, "__customCode_" + customPayment.getId()));
			try {
				customPayment.setRank(ServletRequestUtils.getIntParameter(request, "__customRank_" + customPayment.getId()));
			} catch (Exception e) {
				customPayment.setRank(null);
			}
			try {
				customPayment.setGroupId(ServletRequestUtils.getIntParameter(request, "__customGroupId_" + customPayment.getId()));
			} catch (Exception e) {
				customPayment.setGroupId(null);
			}
		}
		this.webJaguar.updateCustomPaymentMethods(form.getCustomPaymentMethods());
		
		// get credit cards
		List<Configuration> data = new ArrayList<Configuration>();
		Configuration config = new Configuration();
		config.setKey("CREDIT_CARD_PAYMENT");
		StringBuffer sbuff = new StringBuffer("");
		String values[] = request.getParameterValues("CREDIT_CARD_PAYMENT");
		if (values != null) {
			for (int x=0; x<values.length; x++) {
				sbuff.append(values[x] + ",");
			}			
		}
		config.setValue(sbuff.toString());
		data.add(config); 	
		this.webJaguar.updateSiteConfig(data);	

    	Map<String, Object> model = new HashMap<String, Object>();		  	
		model.put(getCommandName(), form);
		model.put("message", "update.successful");
		model.put("siteConfig", this.webJaguar.getSiteConfig());
		
		return new ModelAndView(getSuccessView(), model);
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
		PaymentMethodForm form = new PaymentMethodForm();	
		form.setCustomPaymentMethods(this.webJaguar.getCustomPaymentMethodList(null));
		//for getting only active Customer groups
		CustomerGroupSearch customerGroupSearch = new CustomerGroupSearch();
		customerGroupSearch.setActive("1");
		request.setAttribute("groups", this.webJaguar.getCustomerGroupList(customerGroupSearch));
		
		return form;		
	}    
}
