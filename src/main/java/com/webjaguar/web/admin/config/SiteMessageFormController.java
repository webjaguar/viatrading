package com.webjaguar.web.admin.config;

/*
 * Copyright 2005, 2007 Advanced E-Media Solutions @author Shahin Naji
 * 
 * @since 01.23.2007
 */

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.model.SiteMessage;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.ProductForm;

public class SiteMessageFormController extends org.springframework.web.servlet.mvc.SimpleFormController
{

	private com.webjaguar.logic.WebJaguarFacade webJaguar;

	public SiteMessageFormController()
	{
		setSessionForm( true );
		setCommandName( "siteMessageForm" );
		setCommandClass( com.webjaguar.web.form.SiteMessageForm.class );
		setFormView( "admin/config/siteMessageForm" );
		setSuccessView( "siteMessage.jhtm" );
		this.setValidateOnBinding( true );
	}

	public org.springframework.web.servlet.ModelAndView onSubmit(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, Object command,
			org.springframework.validation.BindException errors) throws javax.servlet.ServletException
	{
		com.webjaguar.web.form.SiteMessageForm siteMessageForm = (com.webjaguar.web.form.SiteMessageForm) command;

		// check if cancel button was pressed
		if ( request.getParameter( "_cancel" ) != null )
		{
			return new ModelAndView( new org.springframework.web.servlet.view.RedirectView( getSuccessView() ) );
		}else if ( request.getParameter( "delete_siteMessage" ) != null )
		{
			Integer siteMessageId = siteMessageForm.getSiteMessage().getMessageId();
			if ( siteMessageId != -1 )
				this.webJaguar.deleteSiteMessage( siteMessageId );
			return new org.springframework.web.servlet.ModelAndView( new org.springframework.web.servlet.view.RedirectView( "siteMessage.jhtm" ) );
		}
		
		if ( siteMessageForm.isNewSiteMessage() )
		{
				this.webJaguar.insertSiteMessage( siteMessageForm.getSiteMessage() );
		
		}
		else
		{
			this.webJaguar.updateSiteMessage( siteMessageForm.getSiteMessage() );
		}

		return new org.springframework.web.servlet.ModelAndView( new org.springframework.web.servlet.view.RedirectView( getSuccessView() ) );
	}
	
	protected boolean suppressBinding(HttpServletRequest request)
	{
		if ( request.getParameter( "_cancel" ) != null )
		{
			return true;
		}
		return false;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		com.webjaguar.web.form.SiteMessageForm siteMessageForm = (com.webjaguar.web.form.SiteMessageForm) command;

		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "siteMessage.messageName", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "siteMessage.subject", "form.required", "required" );
		ValidationUtils.rejectIfEmptyOrWhitespace( errors, "siteMessage.message", "form.required", "required" );

		siteMessageForm.getSiteMessage().setMessageName( siteMessageForm.getSiteMessage().getMessageName().trim() );

		// check if there is a siteMessage code
		SiteMessage siteMessage = this.webJaguar.getSiteMessageByName( siteMessageForm.getSiteMessage().getMessageName() );
		if ( siteMessage != null )
		{ // siteMessage exists
			if ( siteMessageForm.isNewSiteMessage() )
			{
				errors.rejectValue( "siteMessage.messageName", "SITEMESSAGE_ALREADY_EXISTS", "A site message using this name already exists." );
			}
			else if ( siteMessage.getMessageId().compareTo( siteMessageForm.getSiteMessage().getMessageId() ) != 0 )
			{ // not the same siteMessage
				errors.rejectValue( "siteMessage.messageName", "SITEMESSAGE_ALREADY_EXISTS", "A site message using this name already exists." );
			}
		}

	}

	protected Object formBackingObject(javax.servlet.http.HttpServletRequest request) throws Exception
	{
		com.webjaguar.web.form.SiteMessageForm siteMessageForm = new com.webjaguar.web.form.SiteMessageForm();

		if ( request.getParameter( "id" ) != null )
		{
			Integer siteMessageId = new Integer( request.getParameter( "id" ) );
			com.webjaguar.model.SiteMessage siteMessage = this.webJaguar.getSiteMessageById( siteMessageId );

			siteMessageForm.setSiteMessage( siteMessage );
			siteMessageForm.setNewSiteMessage( false );

		}

		return siteMessageForm;

	}
	
	 protected Map referenceData(HttpServletRequest request) throws Exception {
	    	Map<String, Object> map = new HashMap<String, Object>();

			map.put("protectedLevels", Constants.protectedLevels());
	    	
	    	map.put("groupList", this.webJaguar.getSiteMessageGroupList());
	    	
			map.put("labels", this.webJaguar.getLabels("protected"));
			
			return map;
	    }  	

	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar)
	{
		this.webJaguar = webJaguar;
	}

}
