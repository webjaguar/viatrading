package com.webjaguar.web.admin.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;



import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Language;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.web.form.CategoryForm;


public class LanguageController implements Controller{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }		
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		if(request.getParameter("delete") != null) { 
			if(request.getParameter("languageCode")!= null) {
			this.webJaguar.deleteLanguage(request.getParameter("languageCode"));
			return new ModelAndView(new RedirectView("languageList.jhtm"));
			}
		}
		
		if(request.getParameter("languageCode") !=null) {
			Language language = this.webJaguar.getLanguageSetting(request.getParameter("languageCode"));
			// check if update button was pressed
			if(request.getParameter("_update")!= null) {			
				if(updateSiteMessageLanguage(request, language, map )) {
					map.put("message", "update.successful"); 				 	
				}	
			}
			map.put("language",language);
			map.put("siteMessages", this.webJaguar.getSiteMessageList());
			
			return new ModelAndView("admin/config/language", map);
		} else {
			return new ModelAndView("admin/config/languageList", map);
		}				
	}
	
	 private boolean updateSiteMessageLanguage(HttpServletRequest request, Language languages, Map<String, Object> map){		 		
		 		
		 	if(request.getParameter("languageCode")!=null) {
		 		try {
	 				languages.setNewRegistrationId(Integer.parseInt(request.getParameter("newRegistration")));	 				
	 			} catch (Exception e) {
	 				languages.setNewRegistrationId(null);		 				
	 			}
	 			try {
	 				languages.setNewOrderId(Integer.parseInt(request.getParameter("newOrders")));		 				
	 			} catch (Exception e) {
	 				languages.setNewOrderId(null);
	 			}		 		
		 	}			
			this.webJaguar.updateSiteMessageLanguage(languages);
			return true;		 
	 }	
	
}