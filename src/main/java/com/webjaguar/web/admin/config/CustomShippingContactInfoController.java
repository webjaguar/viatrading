/*
 * Copyright 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 */

package com.webjaguar.web.admin.config;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataAccessException;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.web.form.ContactForm;

public class CustomShippingContactInfoController extends SimpleFormController
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	public CustomShippingContactInfoController()
	{
		setSessionForm( true );
		setCommandName( "contactForm" );
		setCommandClass( ContactForm.class );
		setFormView( "admin/config/customShippingContact" );       
		setSuccessView( "customShippingContactInfoList.jhtm" );
		this.setValidateOnBinding( false );
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	return map;
    }
	
	protected Object formBackingObject(javax.servlet.http.HttpServletRequest request) throws Exception
	{
		ContactForm form = new ContactForm();
		Integer contactId = ServletRequestUtils.getIntParameter(request, "id" , -1);
		if ( contactId != -1 ) {
			form.setContact(this.webJaguar.getCustomShippingContactById(contactId));
			form.setNewContact(false);
		} 
		return form;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,	BindException errors) throws Exception 
	{
		ContactForm form = (ContactForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
		
		// check if delete button was pressed
		String delete = ServletRequestUtils.getStringParameter( request, "_delete" );
		if ( delete != null ) {
			try {
				this.webJaguar.deleteCustomShippingContact(form.getContact().getId());		
			} catch (DataAccessException e) {
				model.put("message", "contact.exception.hasPO");
				return showForm(request, response, errors, model);	
			}
			return new ModelAndView( new RedirectView( getSuccessView() ) );
		}
		
		if (form.isNewContact()) {
			this.webJaguar.insertCustomShippingContact(form.getContact());
		} else {
			this.webJaguar.updateCustomShippingContact(form.getContact());
		}
		
		return new ModelAndView( new RedirectView( getSuccessView() ) );

	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		ContactForm contactForm = (ContactForm) command;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "contact.contactName", "form.required", "required");
	}
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if ( request.getParameter( "_cancel" ) != null ) {
			return true;
		}
		return false;
	}
}
