/*
 * Copyright 2005, 2007 Advanced E-Media Solutions @author Jwalant Patel
 * 
 * @since 12.10.2009
 */

package com.webjaguar.web.admin.config;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.model.MultiStore;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.web.form.CustomShippingForm;

public class CustomShippingFormController extends org.springframework.web.servlet.mvc.SimpleFormController
{

	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public CustomShippingFormController()
	{
		setSessionForm( true );
		setCommandName( "customShippingForm" );
		setCommandClass( CustomShippingForm.class );
		setFormView( "admin/config/customShippingForm" );
		setSuccessView( "customShippingList.jhtm" );
		this.setValidateOnBinding( true );
	}
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) 
	{
		// Note: get everything from model
		java.util.Map<String, Object> model = new java.util.HashMap<String, Object>();
		model.put("countrylist", this.webJaguar.getCountryList(false));
		model.put("carriers", this.webJaguar.getShippingMethodList(true));

		//multistore
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		if((Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			List<MultiStore> multiStores = this.webJaguar.getMultiStore("all");
			MultiStore multiStore = new MultiStore();
			multiStore.setHost(request.getHeader("host"));
			multiStores.add(multiStore);
			model.put("multiStores",multiStores);
		}
	   	
		return model;
	}

	   
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws ServletException {

		CustomShippingForm customShipping = (CustomShippingForm) command;
		
		if (customShipping.getShippingRate().getPrice().trim().equals("")) {
			customShipping.getShippingRate().setPrice(null);
		}
		
		// remove association to USPS, UPS or FedEx, if ignoreCarriers is enabled.
		if(customShipping.getShippingRate().isIgnoreCarriers()) {
			customShipping.getShippingRate().setCarrierId(null);
		}
		
		// get group ids
		Set<Object> groups = new HashSet<Object>();
		if(customShipping.getShippingRate().getGroupIds() != null){
			String[] groupIds = customShipping.getShippingRate().getGroupIds().split( "," );
			for ( int x = 0; x < groupIds.length; x++ ) {
				if ( !("".equals( groupIds[x].trim() )) ) {
					groups.add( Integer.valueOf( groupIds[x].trim() ) );
				}
			}
			customShipping.getShippingRate().setGroupIdSet( groups );
		}
		
		if ( customShipping.isNewCustomShipping() ) {
			try {
				if(customShipping.getShippingRate().getHost() == null){
					customShipping.getShippingRate().setHost("");
				}
				this.webJaguar.insertCustomShipping( customShipping.getShippingRate() );
			}
			catch ( Exception e ) {
				// no duplicate entry is allowed.
				// custom shipping title is UNIQUE
			}
		}
		else {
			this.webJaguar.updateCustomShipping( customShipping.getShippingRate() );
		}

		return new ModelAndView( new RedirectView( getSuccessView() ) );
	}


	protected Object formBackingObject(HttpServletRequest request) throws Exception
	{
		CustomShippingForm customShippingForm = new CustomShippingForm();

		if ( request.getParameter( "id" ) != null )
		{
			Integer customShippingId = new Integer( request.getParameter( "id" ) );
			ShippingRate shippingRate = this.webJaguar.getCustomShippingById( customShippingId );
			
			customShippingForm.setShippingRate( shippingRate );
			customShippingForm.setNewCustomShipping( false );
			
			Set groupIdSet = shippingRate.getGroupIdSet();

			Iterator iter = groupIdSet.iterator();
			StringBuffer groupIds = new StringBuffer();
			if ( iter.hasNext() ) {
				groupIds.append( iter.next() );
			}
			while ( iter.hasNext() ) {
				groupIds.append( ", " );
				groupIds.append( iter.next() );
			}
			customShippingForm.getShippingRate().setGroupIds( groupIds.toString() );
		}
		return customShippingForm;
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		CustomShippingForm customShippingForm = (CustomShippingForm) command;
    	if(customShippingForm.getShippingRate().getPercent() > 1 && customShippingForm.getShippingRate().getCarrierId() != null){
    		errors.rejectValue("shippingRate.carrierId", "check carrier", "Do not select carrier OR change discount type");
    	}
    	if(customShippingForm.getShippingRate().getPercent() == 1 && customShippingForm.getShippingRate().getCarrierId() == null){
    		errors.rejectValue("shippingRate.carrierId", "check carrier", "Select carrier");
    	}
    	if(customShippingForm.getShippingRate().getCategoryId() != null ){
    		if(this.webJaguar.getCategoryName(customShippingForm.getShippingRate().getCategoryId()) == null) {
    			errors.rejectValue("shippingRate.categoryId", "check category", "Category does not exist");
    	    }
    	}
	}
    
    
}