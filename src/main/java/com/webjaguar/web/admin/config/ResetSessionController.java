/* Copyright 2005, 2008 Advanced E-Media Solutions
 * author: Shahin Naji
 */

package com.webjaguar.web.admin.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.servlet.view.RedirectView;

import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Product;

public class ResetSessionController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		//if ( ServletRequestUtils.getBooleanParameter( request, "reset", false ) )
			//request.setAttribute("resetSession", true);
        return null;
	}
	
}
