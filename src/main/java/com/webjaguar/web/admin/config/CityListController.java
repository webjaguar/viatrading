/* Copyright 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 10.29.2007
 */

package com.webjaguar.web.admin.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.City;

public class CityListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	String country = "US";
		String state = ServletRequestUtils.getStringParameter( request, "state", "" );
		String county = ServletRequestUtils.getStringParameter( request, "county", "" );
        // check if update button was pressed
		if (request.getParameter("__update") != null) {
			updateCity(request, country, state, county);
		}
		
		Map<Object, Object> myModel = new HashMap<Object, Object>();
		City aCity = new City(state, county);
		myModel.put("cityList", this.webJaguar.getCities(aCity) );  
		myModel.put("country", country ); 
		myModel.put("county", county ); 
		myModel.put("state", state );
		      
        return new ModelAndView("admin/config/cityList", "model", myModel);
    }
	
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private void updateCity(HttpServletRequest request, String country, String state, String county) {
		
		// get a list of city from http request
		String cities [] = request.getParameterValues("__cities");
		
		// update tax rate
		List<City> data = new ArrayList<City>();
		// create county objects
		for (int i=0;i<cities.length;i++){
			City city = new City();
			// set code
			city.setCountry( country );
			city.setState( state );
			city.setCounty( county );
			city.setCity( cities[i] );
			try {
				city.setTaxRate(new Double(request.getParameter("__taxrate_" + cities[i])));
			} catch (NumberFormatException e) {
				city.setTaxRate( null );
			}
			data.add(city);
		}
		this.webJaguar.updateCityList(data);		
	}
	
}
