/*
 * Copyright 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 */

package com.webjaguar.web.admin.config;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.web.form.ShippingHandlingForm;

public class TbdController extends SimpleFormController
{

	private WebJaguarFacade webJaguar;
	
	public TbdController()
	{
		setSessionForm( true );
		setCommandName( "shippingHandlingForm" );
		setCommandClass( ShippingHandlingForm.class );
		setFormView( "admin/config/shippingHandling" );
		setSuccessView( "admin/config/shippingHandling" );
		this.setValidateOnBinding( false );
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,	BindException errors) throws javax.servlet.ServletException
	{
		ShippingHandlingForm shippingHandlingForm = (ShippingHandlingForm) command;
		this.webJaguar.updateShippingHandling( shippingHandlingForm.getShippingHandling() );
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("message", "update.successful");
		myModel.put("view", "tbd");    
		myModel.put("shippingHandlingForm", shippingHandlingForm);   
	      
		return new org.springframework.web.servlet.ModelAndView( getSuccessView() , myModel );

	}

	protected Object formBackingObject(javax.servlet.http.HttpServletRequest request) throws Exception
	{
		ShippingHandlingForm form = new ShippingHandlingForm();
		form.setShippingHandling( this.webJaguar.getShippingHandling() );

		return form;
	}
	
	protected Map referenceData(HttpServletRequest request) throws Exception 
	{
		Map<String, String> map = new HashMap<String, String>();
		map.put("view", "tbd");
 	
		return map;
	}  
	
	public void setWebJaguar(WebJaguarFacade webJaguar)
	{
		this.webJaguar = webJaguar;
	}	


}
