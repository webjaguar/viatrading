/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.20.2008
 */

package com.webjaguar.web.admin.config;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller; 
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;


public class AdminMessageController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	
    	Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
    	
    	if (siteConfig.get("ADMIN_MESSAGE").getValue().equals("")) {
    		// redirect 
			return new ModelAndView(new RedirectView("../catalog/index.jsp"));
    	}

    	String message = this.webJaguar.getAdminMessageById(1).getMessage();
    	if (message == null || message.trim().equals("")) {
    		// redirect 
			return new ModelAndView(new RedirectView("../catalog/index.jsp"));
    	}
    	Map<String, String> map = new HashMap<String, String>();
		map.put("message", message);    		

        return new ModelAndView("admin/message/message", map);
    }
}
