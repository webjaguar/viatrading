/* Copyright 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 10.29.2007
 */

package com.webjaguar.web.admin.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.thirdparty.shipping.fedex.registration.FedexRegisterationService;
import com.webjaguar.thirdparty.shipping.fedex.registration.FedexSubscriptionRequest;
import com.webjaguar.thirdparty.shipping.fedex.registration.FedexVersionCaptureRequest;
import com.webjaguar.thirdparty.shipping.fedex.registration.WebAuthenticationCredential;

public class FedexSubscriberController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();
    	String meterNumber = "";
    	WebAuthenticationCredential credential = null;
    	String versionId = "";
    	List<Configuration> data = new ArrayList<Configuration>();
    	Map<Object, Object> model = new HashMap<Object, Object>();
    	
		if (request.getParameter("__getMeterNumber") != null) {
			meterNumber = getMeterNumber(siteConfig);
			versionId = getVersionCapture(siteConfig,meterNumber);
			if ( meterNumber == null ) {
				model.put("message", "errorMeterNumber" );
			} else {
				model.put("message", "meterNumberGranted" );
				Configuration config = new Configuration();
				config.setKey( "FEDEX_USER_METERNUMBER" );
				config.setValue( meterNumber );
				data.add( config );
			}
			if ( versionId == null ) {
				model.put("message", "versionIdNotGranted" );
			}
		} else if ( request.getParameter("__getkeyPassword") != null) {
			credential = getKeyPassword(siteConfig);
			if ( credential == null ) {
				model.put("message", "errorUserCredential" );
			} else {
				model.put("message", "userCredentialSets" );
				Configuration configKey = new Configuration();
				configKey.setKey( "FEDEX_USER_KEY" );
				configKey.setValue( credential.getKey() );
				data.add( configKey );
				Configuration configPas = new Configuration();
				configPas.setKey( "FEDEX_USER_PASSWORD" );
				configPas.setValue( credential.getPassword() );
				data.add( configPas );
			}
		}
		this.webJaguar.updateSiteConfig(data);	
        return new ModelAndView(new RedirectView( "shippingMethodList.jhtm" ) );
    }

	private String getMeterNumber(Map<String, Configuration> siteConfig) {
		
		String meterNumber = "";
		String contactPerson = siteConfig.get( "COMPANY_CONTACT_FIRSTNAME" ).getValue().toString() + " " + siteConfig.get( "COMPANY_CONTACT_LASTNAME" ).getValue().toString();
		String companyName = siteConfig.get( "COMPANY_NAME" ).getValue().toString();
		String companyPhone = siteConfig.get( "COMPANY_PHONE" ).getValue().toString();
		String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue().toString();
		// user Key and password
		String userKey = siteConfig.get( "FEDEX_USER_KEY" ).getValue().toString();
		String userPassword = siteConfig.get( "FEDEX_USER_PASSWORD" ).getValue().toString();
		FedexSubscriptionRequest subscriber = new FedexSubscriptionRequest();
		meterNumber = subscriber.getMeterNumber( contactPerson, companyName, companyPhone, contactEmail, userKey, userPassword );
		return meterNumber;
	}
	
	private WebAuthenticationCredential getKeyPassword(Map<String, Configuration> siteConfig) {
		
		WebAuthenticationCredential credential;
		String contactFirstName = siteConfig.get( "COMPANY_CONTACT_FIRSTNAME" ).getValue().toString();
		String contactLastName = siteConfig.get( "COMPANY_CONTACT_LASTNAME" ).getValue().toString();
		String companyName = siteConfig.get( "COMPANY_NAME" ).getValue().toString();
		String companyPhone = siteConfig.get( "COMPANY_PHONE" ).getValue().toString();
		String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue().toString();
		FedexRegisterationService registrationCSP = new FedexRegisterationService();
		credential = registrationCSP.getCredential( contactFirstName, contactLastName, companyName, companyPhone, contactEmail);
		return credential;		
	}
	
	private String getVersionCapture(Map<String, Configuration> siteConfig, String meterNumber) {
		
		String version = "";
		String userKey = siteConfig.get( "FEDEX_USER_KEY" ).getValue().toString();
		String userPassword = siteConfig.get( "FEDEX_USER_PASSWORD" ).getValue().toString();
		FedexVersionCaptureRequest versionCaptureRequest = new FedexVersionCaptureRequest();
		version = versionCaptureRequest.getVersionCapture( userKey, userPassword, meterNumber );
		return version;		
	}
}
