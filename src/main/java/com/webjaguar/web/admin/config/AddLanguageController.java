package com.webjaguar.web.admin.config;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;

public class AddLanguageController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		if(request.getParameter("add") != null) { 
			if(!((request.getParameter("language")).contentEquals("0"))) {
				String languageCode = request.getParameter("language");
				if (this.webJaguar.getLanguageSetting(languageCode) == null) {
					this.webJaguar.insertLanguage(languageCode);
				} 
			}
			else {
				myModel.put("message", "Please select a language");
			}
			return new ModelAndView(new RedirectView("languageList.jhtm"));	
		}
		return new ModelAndView("admin/config/addLanguage", myModel);
	}
}


