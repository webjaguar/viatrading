/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.21.2009
 */

package com.webjaguar.web.admin.config;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class LabelsController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    	throws Exception {	

		Map<String, Object> model = new HashMap<String, Object>();    	
		
    	// check if update button was pressed
		if (request.getParameter("__update") != null) {
			List<Map<String, String>> labels = new ArrayList<Map<String, String>>();
			for (String label: ServletRequestUtils.getStringParameters(request, "__labels")) {
				Map<String, String> map = new HashMap<String, String>();
				map.put("label_type", label);
				map.put("label_title", ServletRequestUtils.getStringParameter(request, label, "").trim());
				labels.add(map);
			}
			this.webJaguar.updateLabels(labels);
			model.put("message", "update.successful");
		}
    	
		model.put("labels", this.webJaguar.getLabels());
		
    	return new ModelAndView("admin/config/labels", model);
    }
}
