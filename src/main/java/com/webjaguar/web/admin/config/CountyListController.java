/* Copyright 2006 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 8.30.2007
 */

package com.webjaguar.web.admin.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.County;

public class CountyListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	String country = "US";
		String state = ServletRequestUtils.getStringParameter( request, "state", "CA" );
        // check if update button was pressed
		if (request.getParameter("__update") != null) {
			updateCounty(request, country, state);
		}
		
		Map<Object, Object> myModel = new HashMap<Object, Object>();
		myModel.put("countyList", this.webJaguar.getCounties(country, state) );  
		myModel.put("country", country ); 
		myModel.put("state", state );
		      
        return new ModelAndView("admin/config/countyList", "model", myModel);
    }
	
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private void updateCounty(HttpServletRequest request, String country, String state) {
		
		// get a list of state codes from http request
		String counties [] = request.getParameterValues("__counties");
		
		// update tax rate
		List<County> data = new ArrayList<County>();
		// create county objects
		for (int i=0;i<counties.length;i++){
			County county = new County();
			// set code
			county.setCountry( country );
			county.setCounty( counties[i] );
			county.setState( state );
			try {
				county.setTaxRate(new Double(request.getParameter("__taxrate_" + counties[i])));
			} catch (NumberFormatException e) {
				county.setTaxRate( null );
			}
			data.add(county);
		}
		this.webJaguar.updateCountyList(data);		
	}
	
}
