/*
 * Copyright 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 */

package com.webjaguar.web.admin.config;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.web.form.ChangePasswordForm;


public class ChangePasswordController extends SimpleFormController
{
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao)  {  this.globalDao = globalDao;  }
	
	public ChangePasswordController() {
		setSessionForm(false);
		setCommandName("changePasswordForm");
		setCommandClass(ChangePasswordForm.class);
		setFormView("admin/config/changePassword");
		setSuccessView("admin/config/changePassword");
		this.setValidateOnBinding(true);
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,	BindException errors) throws javax.servlet.ServletException {
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView("./"));
		}
		
		ChangePasswordForm changePasswordForm = (ChangePasswordForm) command;
		Map<String, Object> model = new HashMap<String, Object>();		  	
		model.put(getCommandName(), changePasswordForm);
		
		if (globalDao.getAdminInfoByUsernamePassword(changePasswordForm.getUser().getUsername(), changePasswordForm.getCurrentPassword()) != null) {
			globalDao.updateAdminInfo(changePasswordForm.getUser());
			
			model.put("message", "update.successful");			
		} else {
			model.put("message", "PASSWORD_NO_MATCH");			
		}
	      
		return new ModelAndView(getSuccessView(), model);
	}
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;
		}
		return false;
	}
	
	protected Object formBackingObject(javax.servlet.http.HttpServletRequest request) throws Exception {
		ChangePasswordForm changePasswordForm = new ChangePasswordForm();
		changePasswordForm.setUser(globalDao.getAdminInfoByUserName(SecurityContextHolder.getContext().getAuthentication().getName()));
		
		return changePasswordForm;
	}
}
