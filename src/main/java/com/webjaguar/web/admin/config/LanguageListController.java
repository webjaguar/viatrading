package com.webjaguar.web.admin.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Language;
import com.webjaguar.model.SiteMessage;

public class LanguageListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("languagelist", this.webJaguar.getLanguageSettings());	
		
		if(request.getParameter("add") != null) {			
			return new ModelAndView(new RedirectView("addLanguage.jhtm"));			
		}
		
        return new ModelAndView("admin/config/languageList", "model", myModel);
	}
}
