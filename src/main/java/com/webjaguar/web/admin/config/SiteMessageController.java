package com.webjaguar.web.admin.config;

import org.springframework.beans.support.PagedListHolder;

import com.webjaguar.model.SiteMessageGroupSearch;
import com.webjaguar.model.SiteMessageSearch;

public class SiteMessageController extends org.springframework.web.servlet.mvc.AbstractCommandController
{

	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	
	public SiteMessageController()
	{
		setCommandClass( com.webjaguar.model.CategorySearch.class );
	}
	
	public org.springframework.web.servlet.ModelAndView handle( javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, Object command, org.springframework.validation.BindException errors ) throws Exception
	{	
		if ( request.getParameter( "add_siteMessage" ) != null )
		{
			return new org.springframework.web.servlet.ModelAndView( new org.springframework.web.servlet.view.RedirectView( "siteMessageForm.jhtm" ) );
		}
	
		SiteMessageSearch siteMessageSearch = this.webJaguar.getSiteMessageSearch(request);
		PagedListHolder siteMessageList = new PagedListHolder(this.webJaguar.getSiteMessageList(siteMessageSearch));
		siteMessageList.setPageSize( siteMessageSearch.getPageSize() );
		siteMessageList.setPage( siteMessageSearch.getPage() - 1 );
		
		java.util.Map<String, Object> model = new java.util.HashMap<String, Object>();
		model.put( "siteMessages",siteMessageList  );	
		
		SiteMessageGroupSearch siteMessageGroupSearch = new SiteMessageGroupSearch();
		siteMessageGroupSearch.setActive(true);
		model.put("siteMessageSearch", siteMessageSearch);
		model.put("groups", this.webJaguar.getSiteMessageGroupList(siteMessageGroupSearch));
		model.put("siteMessagesList", this.webJaguar.getSiteMessageList());
		
		return new org.springframework.web.servlet.ModelAndView( "admin/config/siteMessage", "model", model );
	}



	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar)
	{
		this.webJaguar = webJaguar;
	}

}