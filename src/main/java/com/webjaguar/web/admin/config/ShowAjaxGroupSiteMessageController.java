package com.webjaguar.web.admin.config;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.SiteMessageSearch;

public class ShowAjaxGroupSiteMessageController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		Map<String, Object> model = new HashMap<String, Object>();	
		
		SiteMessageSearch siteMessageSearch = this.webJaguar.getSiteMessageSearch(request);
		
		model.put("siteMessagesList", this.webJaguar.getSiteMessageList(siteMessageSearch));
		
		return new ModelAndView("admin/config/showAjaxGroupSiteMessage", "model", model);
	}
	
}
