/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 10.10.2006
 */

package com.webjaguar.web.admin.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller; 

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Country;

public class CountryListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        // check if update ranking button was pressed
		if (request.getParameter("__update") != null) {
			updateCountry(request);
		}

		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("countrylist", this.webJaguar.getCountryList(true));    
		      
        return new ModelAndView("admin/config/countryList", "model", myModel);
    }
	
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private void updateCountry(HttpServletRequest request) {
		
		// set a List contains countries
		List<Country> data = new ArrayList<Country>();
		
		// get a list of country ids from http request
		String codes [] = request.getParameterValues("__code");
		
		// create country objects based on ids
		for (int i=0;i<codes.length;i++){
			try {
				    Country country = new Country();
                    // set code
					String code = codes[i];
					country.setCode(code);
					// set rank
					Integer rank = new Integer(request.getParameter("__rank_" + code));
					country.setRank(rank);
					// set enabled
					String enabled = request.getParameter("__enabled_" + code);   
					if(enabled!=null){
					    country.setEnabled(true);   
					}
					// set region
					String region = request.getParameter("__region_" + code);
					if(region != null) {
						country.setRegion(region);
					}
					// set tax
					Double taxRate = ServletRequestUtils.getDoubleParameter(request, "__taxrate_" + code, 0.0);
					if(taxRate!=null){
					    country.setTaxRate(taxRate); 
					}
					data.add(country);
				} catch (NumberFormatException e) {}
		}
			
		this.webJaguar.updateCountryList(data);	
			
	}
	
}
