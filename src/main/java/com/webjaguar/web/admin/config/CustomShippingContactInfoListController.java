/* Copyright 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 10.08.2009
 */

package com.webjaguar.web.admin.config;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Search;
import com.webjaguar.model.TicketSearch;

public class CustomShippingContactInfoListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

    	Search contactInfoSearch = getContactInfoSearch( request );
		Map<Object, Object> myModel = new HashMap<Object, Object>();
		
		// if add pushed
		if ( request.getParameter( "_add" ) != null ) {
			return new ModelAndView( new RedirectView( "customShippingContactInfo.jhtm" ) );
		}
		
		PagedListHolder contactList = new PagedListHolder( this.webJaguar.getCustomShippingContactList( contactInfoSearch ) );
		
		contactList.setPageSize( contactInfoSearch.getPageSize() );
		contactList.setPage( contactInfoSearch.getPage() - 1 );
		
		myModel.put("list", contactList );  
		      
        return new ModelAndView("admin/config/customShippingContactList", "model", myModel);
    }
    
	private Search getContactInfoSearch(HttpServletRequest request) {
		Search contactInfoSearch = (Search) request.getSession().getAttribute( "contactInfoSearch" );
		if (contactInfoSearch == null) {
			contactInfoSearch = new Search();
			request.getSession().setAttribute( "contactInfoSearch", contactInfoSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			contactInfoSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				contactInfoSearch.setPage( 1 );
			} else {
				contactInfoSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				contactInfoSearch.setPageSize( 10 );
			} else {
				contactInfoSearch.setPageSize( size );				
			}
		}	
		
		//
		if (request.getParameter("size") == null) {
			
				contactInfoSearch.setPageSize(500 );				
			
		}
		return contactInfoSearch;
	}
}
