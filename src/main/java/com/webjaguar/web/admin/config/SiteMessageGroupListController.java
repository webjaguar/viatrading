package com.webjaguar.web.admin.config;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.Search;
import com.webjaguar.model.SiteMessageGroupSearch;

public class SiteMessageGroupListController extends org.springframework.web.servlet.mvc.AbstractCommandController {
	
	private WebJaguarFacade webJaguar;
	
	public SiteMessageGroupListController()
	{
		setCommandClass( com.webjaguar.model.CategorySearch.class );
	}

	@Override
	protected ModelAndView handle(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		// TODO Auto-generated method stub
		if (request.getParameter("add_siteMessageGroup") != null) {
			return new ModelAndView(new RedirectView("siteMessageGroupForm.jhtm"));
		}
		
		
		SiteMessageGroupSearch siteMessageGroupSearch = getSiteMessageGroupSearch( request );
		
		PagedListHolder siteMessageGroupList = new PagedListHolder(this.webJaguar.getSiteMessageGroupList());
		siteMessageGroupList.setPageSize( siteMessageGroupSearch.getPageSize() );
		siteMessageGroupList.setPage( siteMessageGroupSearch.getPage() - 1 );
		
		Map<String, PagedListHolder> myModel = new HashMap<String, PagedListHolder>();
		
		
		myModel.put("siteMessageGroups", siteMessageGroupList);
    	
        return new ModelAndView("admin/config/siteMessageGroupList", "model", myModel);
	}
	
	private SiteMessageGroupSearch getSiteMessageGroupSearch(HttpServletRequest request) {
		SiteMessageGroupSearch siteMessageGroupSearch = (SiteMessageGroupSearch) request.getSession().getAttribute( "siteMessageGroupSearch" );
		if (siteMessageGroupSearch == null) {
			siteMessageGroupSearch = new SiteMessageGroupSearch();
			request.getSession().setAttribute( "siteMessageGroupSearch", siteMessageGroupSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			siteMessageGroupSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// group name
		if (request.getParameter("group") != null) {
			siteMessageGroupSearch.setName(ServletRequestUtils.getStringParameter( request, "group", "" ));
		}
		
		//active
		if (request.getParameter("active") != null) {
			siteMessageGroupSearch.setActive( ServletRequestUtils.getBooleanParameter( request, "active", false ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				siteMessageGroupSearch.setPage( 1 );
			} else {
				siteMessageGroupSearch.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				siteMessageGroupSearch.setPageSize( 10 );
			} else {
				siteMessageGroupSearch.setPageSize( size );				
			}
		}
		
		return siteMessageGroupSearch;
	}		

	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar)
	{
		this.webJaguar = webJaguar;
	}




	
}
