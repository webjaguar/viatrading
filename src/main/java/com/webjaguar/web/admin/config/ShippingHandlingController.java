/*
 * Copyright 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 */

package com.webjaguar.web.admin.config;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.web.form.ShippingHandlingForm;

public class ShippingHandlingController extends SimpleFormController
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	public ShippingHandlingController()
	{
		setSessionForm( true );
		setCommandName( "shippingHandlingForm" );
		setCommandClass( ShippingHandlingForm.class );
		setFormView( "admin/config/shippingHandling" );
		setSuccessView( "admin/config/shippingHandling" );
		this.setValidateOnBinding( false );
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,	BindException errors) throws javax.servlet.ServletException
	{
		ShippingHandlingForm form = (ShippingHandlingForm) command;
		this.webJaguar.updateShippingHandling( form.getShippingHandling() );
		form.setCustomShippingRates( this.webJaguar.getCustomShippingRateList( true, true ) );
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("message", "update.successful");
		model.put("shippingHandlingForm", form);    
	   	model.put("countrylist", this.webJaguar.getCountryList(false));
	   	model.put("multiStores", this.webJaguar.getMultiStore("all"));
	   	model.put("carriers", this.webJaguar.getShippingMethodList(null));
		return new ModelAndView( getSuccessView() , model );

	}
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		ShippingHandlingForm shippingHandlingForm = (ShippingHandlingForm) command;
		if ( shippingHandlingForm.getShippingHandling().getShippingMethod().equals( "flt" ) || shippingHandlingForm.getShippingHandling().getShippingMethod().equals( "wgt" ) || shippingHandlingForm.getShippingHandling().getShippingMethod().equals( "prc" ) )
		{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shippingHandling.shippingBase", "shippingMethod", "required");
		}
		if ( !shippingHandlingForm.getShippingHandling().getShippingMethod().equals( "det" ) )
		{
			if ( shippingHandlingForm.getShippingHandling().getPriceLimit() != null )
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shippingHandling.newPriceCost", "form.required", "required");
			if ( shippingHandlingForm.getShippingHandling().getNewPriceCost() != null )
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shippingHandling.priceLimit", "form.required", "required");
		}
		if (shippingHandlingForm.getShippingHandling().getShippingMethod().equals("crr") && shippingHandlingForm.getShippingHandling().getZeroWeightNewPriceCost() != null) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shippingHandling.zeroWeightNewShippingTitle", "form.required", "required");
		}

	}

	protected Object formBackingObject(javax.servlet.http.HttpServletRequest request) throws Exception
	{
		ShippingHandlingForm form = new ShippingHandlingForm();
		form.setShippingHandling( this.webJaguar.getShippingHandling() );
		form.setCustomShippingRates( this.webJaguar.getCustomShippingRateList( true, true ) );
		return form;
	}

    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("countrylist", this.webJaguar.getCountryList(false));
    	map.put("multiStores", this.webJaguar.getMultiStore("all"));
    	map.put("carriers", this.webJaguar.getShippingMethodList(null));
    	return map;
    }
}
