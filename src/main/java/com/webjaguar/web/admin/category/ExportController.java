/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.22.2008
 */

package com.webjaguar.web.admin.category;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;

public class ExportController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	private MailSender mailSender;
    public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }

	private int row;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
	throws Exception {

		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		Map<String, Object> map = new HashMap<String, Object>();

		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			map.put("message", "excelfile.notWritable");
			return new ModelAndView("admin/catalog/category/export", map);			
		}
		File file[] = baseFile.listFiles();
		List<Map<String, Object>> exportedFiles = new ArrayList<Map<String, Object>>();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith("categoryExport") & file[f].getName().endsWith(".xls")) {
				if (request.getParameter("__new") != null) { // check if new button was pressed
					file[f].delete();
				} else {
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					exportedFiles.add(fileMap);				
				}
			}
		}
		map.put("exportedFiles", exportedFiles);

		// check if new button was pressed
		if (request.getParameter("__new") != null) {

			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
			String fileName = "/categoryExport_" + dateFormatter.format(new Date()) + "";
			
			HSSFWorkbook wb = createWorkbook(this.webJaguar.getCategoryTree(null, null, true, null, null, null), siteConfig.get("SITE_URL").getValue(), (String) gSiteConfig.get("gMOD_REWRITE"), siteConfig.get("MOD_REWRITE_CATEGORY").getValue());
			// Write the output to a file
			File newFile = new File(baseFile.getAbsolutePath() + fileName + ".xls");
			FileOutputStream fileOut = new FileOutputStream(newFile);
			wb.write(fileOut);
			fileOut.close();
			HashMap<String, Object> fileMap = new HashMap<String, Object>();
			fileMap.put("file", newFile);
			exportedFiles.add(fileMap);  		
			notifyAdmin(siteConfig,request);
			map.put("message", "export.successful");				
		} // if    		
		return new ModelAndView("admin/catalog/category/export", map);
	}

	private HSSFCell getCell(HSSFSheet sheet, int row, int col) {
		return sheet.createRow(row).createCell((short) col);
	}

	private void setText(HSSFCell cell, String text) {
		cell.setCellValue(text);
	}

	private HSSFWorkbook createWorkbook(List<Category> categories, String siteUrl, String gMOD_REWRITE, String MOD_REWRITE_CATEGORY) throws Exception {

		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet();
					
		setText(getCell(sheet, 0, 0), "Category ID");
		if (gMOD_REWRITE != null && gMOD_REWRITE.equals("1")) {
			setText(getCell(sheet, 0, 1), "Old Category Link");
			setText(getCell(sheet, 0, 2), "New Category Link");			
		} else {
			setText(getCell(sheet, 0, 1), "Category Link");			
		}
		
		row = 1;
		
		// create the rows
		buildTree(sheet, categories, 1, siteUrl, gMOD_REWRITE, MOD_REWRITE_CATEGORY);
  
		return wb;
	}
	
	private void buildTree(HSSFSheet sheet, List<Category> categories, int level, String siteUrl, String gMOD_REWRITE, String MOD_REWRITE_CATEGORY) throws Exception {
		for (Category category: categories) {
			setText(getCell(sheet, row, 0), category.getId().toString());
			setText(getCell(sheet, row, 1), "<a href=\"" + siteUrl + "category.jhtm?cid=" + category.getId() + "\">" + category.getName() + "</a>");
			if (gMOD_REWRITE != null && gMOD_REWRITE.equals("1")) {
				setText(getCell(sheet, row, 2), "<a href=\"" + siteUrl + MOD_REWRITE_CATEGORY + "/" + category.getId() + "/" + category.getEncodedName() + ".html\">" + category.getName() + "</a>");				
			}
			setText(getCell(sheet, row, 3), " ");				
			setText(getCell(sheet, row++, level+4), category.getName());

			buildTree(sheet, category.getSubCategories(), level+1, siteUrl, gMOD_REWRITE, MOD_REWRITE_CATEGORY);				
		}
	}
	
	private void notifyAdmin(Map<String, Configuration> siteConfig, HttpServletRequest request){
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	StringBuffer message = new StringBuffer();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
    	message.append("user "+accessUser.getUsername()+" has exported category file");
    	SimpleMailMessage msg = new SimpleMailMessage();			
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Automated Export on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}