/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.web.admin.category;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.CategoryForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.CategoryLinkType;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Language;

public class CategoryFormController extends SimpleFormController
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private Map<String, Object> gSiteConfig;	
	private Map<String, Configuration> siteConfig;

	
	private File baseFile;
	
	List<Language> gI18N;

	public CategoryFormController()
	{
		setSessionForm( true );
		setCommandName( "categoryForm" );
		setCommandClass( CategoryForm.class );
		setFormView( "admin/catalog/category/form" );
		setSuccessView( "categoryList.jhtm" );
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws ServletException
	{
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		CategoryForm categoryForm = (CategoryForm) command;
		
		// check if delete button was pressed
		if ( request.getParameter( "_delete" ) != null )
		{
			this.webJaguar.deleteCategory( categoryForm.getCategory() );
			return new ModelAndView( new RedirectView( getSuccessView() ), "parent", categoryForm.getOldParent() );
		}
		// check if cancel button was pressed
		if ( request.getParameter( "_cancel" ) != null )
		{
			return new ModelAndView( new RedirectView( getSuccessView() ), "parent", categoryForm.getOldParent() );
		}

		if (categoryForm.getCategory().getParent() != null && !categoryForm.isNewCategory()) 
		{ // check only if category is old and has a parent	
			List<Category> categoryTree = this.webJaguar.getCategoryTree(categoryForm.getCategory().getParent(), true, null);
			boolean loop = false;
			for (Category category: categoryTree) {
				if (category.getId().compareTo(categoryForm.getCategory().getId()) == 0) {
					loop = true;
				}
			}
			if (categoryTree.size() == 0 || loop) { 
				// check if parent is invalid or creates an endless loop
				categoryForm.getCategory().setParent( categoryForm.getOldParent() );
			}
		}

		Integer catId = categoryForm.getCategory().getId();
		if ( request.getParameter( "__delete_linkImage" ) != null )
		{
			// delete image
			File oldFile = new File( baseFile, "catlink_" + catId + ".gif" );
			oldFile.delete();
		}
		if ( request.getParameter( "__delete_linkImageOver" ) != null )
		{
			// delete image
			File oldFile = new File( baseFile, "catlink_over_" + catId + ".gif" );
			oldFile.delete();
		}
		if ( request.getParameter( "__delete_image" ) != null )
		{
			// delete image
			File oldFile = null;
			if(categoryForm.getCategory().getImageUrl() != null) {
				if(! categoryForm.getCategory().isAbsolute()) {
					oldFile = new File( baseFile, categoryForm.getCategory().getImageUrl());
					oldFile.delete();
				}
				categoryForm.getCategory().setImageUrl(null);
			} else {
				oldFile = new File( baseFile, "cat_" + catId + ".gif" );
				oldFile.delete();
			}
		}
		if ( request.getParameter( "__delete_image_big" ) != null )
		{
			// delete image
			File oldFile = new File( baseFile, "cat_" + catId + "_big.gif" );
			oldFile.delete();
		}

		boolean uploadedLinkImage = false;
		boolean uploadedLinkImageOver = false;
		boolean uploadedImage = false;
		boolean uploadedAbsoluteImage = false;
		boolean uploadedImageBig = false;
		String file_name = null;
		MultipartFile link_image = null;
		MultipartFile link_image_over = null;
		MultipartFile image = null;
		MultipartFile imageBig = null;
		if ( baseFile.canWrite() )
		{
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			link_image = multipartRequest.getFile( "link_image" );
			if ( link_image != null && !link_image.isEmpty() )
			{
				if ( link_image.getContentType().equalsIgnoreCase( "image/gif" ) || link_image.getContentType().equalsIgnoreCase( "image/jpeg" )
						|| link_image.getContentType().equalsIgnoreCase( "image/pjpeg" ) )
				{
					uploadedLinkImage = true;
				}
			}
			link_image_over = multipartRequest.getFile( "link_image_over" );
			if ( link_image_over != null && !link_image_over.isEmpty() )
			{
				if ( link_image_over.getContentType().equalsIgnoreCase( "image/gif" ) || link_image_over.getContentType().equalsIgnoreCase( "image/jpeg" )
						|| link_image_over.getContentType().equalsIgnoreCase( "image/pjpeg" ) )
				{
					uploadedLinkImageOver = true;
				}
			}
			image = multipartRequest.getFile( "_image" );
			// for absolute image on just Thumbnail
			if (request.getParameter("_imageWithName") != null) {
				if ( image != null && !image.isEmpty() )
				{				
					file_name = image.getOriginalFilename();

					if ( image.getContentType().equalsIgnoreCase( "image/gif" ) || image.getContentType().equalsIgnoreCase( "image/jpeg" )
							|| image.getContentType().equalsIgnoreCase( "image/pjpeg" ) )
					{
						uploadedAbsoluteImage = true;
						categoryForm.getCategory().setImageUrl(file_name);
					}
				}
			} else if ( image != null && !image.isEmpty() )
			{
				if ( image.getContentType().equalsIgnoreCase( "image/gif" ) || image.getContentType().equalsIgnoreCase( "image/jpeg" )
						|| image.getContentType().equalsIgnoreCase( "image/pjpeg" ) )
				{
					uploadedImage = true;
				}
			}
			imageBig = multipartRequest.getFile( "_image_big" );
			if ( imageBig != null && !imageBig.isEmpty() )
			{
				if ( imageBig.getContentType().equalsIgnoreCase( "image/gif" ) || imageBig.getContentType().equalsIgnoreCase( "image/jpeg" )
						|| imageBig.getContentType().equalsIgnoreCase( "image/pjpeg" ) )
				{
					uploadedImageBig = true;
				}
			}

		}
		if(categoryForm.getCategory().isProductFieldSearch() && Integer.parseInt(siteConfig.get("LEFTBAR_DEFAULT_TYPE").getValue()) > 2 && categoryForm.getCategory().getProductFieldSearchPosition() == "") {
			categoryForm.getCategory().setProductFieldSearchPosition("top");
		}
		
		if ( categoryForm.isNewCategory() )
		{
			catId = this.webJaguar.insertCategory( categoryForm.getCategory() );
		}
		else
		{
			this.webJaguar.updateCategory( categoryForm.getCategory() );
		}
		
		// i18n
		if (!(gI18N.isEmpty())) {
			List<Language> languageSettings = gI18N;
			categoryForm.setI18nCategory(null);		// prevents old i18n categories from being updated		
			List<Language> i18n = languageSettings;
			for(Language language: i18n) {
				Category i18nCategory = new Category();
				i18nCategory.setId(catId);
				i18nCategory.setLang(language.getLanguageCode());
				i18nCategory.setI18nName(ServletRequestUtils.getStringParameter(request, "__i18nName_" + language.getLanguageCode(), ""));
				String htmlCode = ServletRequestUtils.getStringParameter(request, "__i18nHtmlCode_" + language.getLanguageCode(), "");
				if (htmlCode.length() > 1000000) {
					htmlCode = htmlCode.substring(0,1000000);
				}
				String protectedHtmlCode = ServletRequestUtils.getStringParameter(request, "__i18nProtectedHtmlCode_" + language.getLanguageCode(), "");			
				if (protectedHtmlCode.length() > 1000000) {
					protectedHtmlCode = protectedHtmlCode.substring(0,1000000);
				}
				i18nCategory.setI18nHtmlCode(htmlCode);
				i18nCategory.setI18nProtectedHtmlCode(protectedHtmlCode);
				i18nCategory.setI18nHeadTag(ServletRequestUtils.getStringParameter(request, "__i18nHeadTag_" + language.getLanguageCode(), ""));
				categoryForm.addI18nCategory(language.getLanguageCode(), i18nCategory);
			}
			this.webJaguar.updateI18nCategory(categoryForm.getI18nCategory().values());
		}		

		// save images
		if ( uploadedLinkImage )
		{
			File newFile = new File( baseFile, "catlink_" + catId + ".gif" );
			try
			{
				link_image.transferTo( newFile );
			}
			catch ( IOException e )
			{
				// do nothing
			}
		}
		if ( uploadedLinkImageOver )
		{
			File newFile = new File( baseFile, "catlink_over_" + catId + ".gif" );
			try
			{
				link_image_over.transferTo( newFile );
			}
			catch ( IOException e )
			{
				// do nothing
			}
		}
		if ( uploadedImage )
		{
			File newFile = new File( baseFile, "cat_" + catId + ".gif" );
			try
			{
				image.transferTo( newFile );
			}
			catch ( IOException e )
			{
				// do nothing
			}
		}
		if ( uploadedAbsoluteImage ) 
		{
			File newFile = new File( baseFile, file_name );
			try
			{
				image.transferTo( newFile );
			}
			catch ( IOException e )
			{
				// do nothing
			}
		}
		if ( uploadedImageBig )
		{
			File newFile = new File( baseFile, "cat_" + catId + "_big.gif" );
			try
			{
				imageBig.transferTo( newFile );
			}
			catch ( IOException e )
			{
				// do nothing
			}
		}


		return new ModelAndView( new RedirectView( getSuccessView() ), "parent", categoryForm.getCategory().getParent() );
	}

	protected boolean suppressBinding(HttpServletRequest request)
	{
		if ( request.getParameter( "_delete" ) != null || request.getParameter( "_cancel" ) != null )
		{
			return true;
		}
		return false;
	}

	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception
	{
		super.initBinder( request, binder );
		// allows null Integer
		binder.registerCustomEditor( Integer.class, new CustomNumberEditor( Integer.class, true ) );
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, true, 10 ) );
	}

	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors)
	{
		CategoryForm categoryForm = (CategoryForm) command;
		
		Map<String, Object> map = new HashMap<String, Object>();
		ArrayList<CategoryLinkType> linkTypes = new ArrayList<CategoryLinkType>();
		for ( CategoryLinkType type : CategoryLinkType.values() )
		{
			linkTypes.add( type );
		}
		map.put( "linkTypes", linkTypes );
		map.put( "layoutList", this.webJaguar.getLayoutList() );
		if ( (Boolean) gSiteConfig.get( "gSALES_PROMOTIONS" ) ) {
			map.put( "salesTagCodes", this.webJaguar.getSalesTagList() );
		}

		File imageBig = new File( baseFile, "/cat_" + categoryForm.getCategory().getId() + "_big.gif" );
		if ( !baseFile.canWrite() )
		{
			map.put( "message", "images.notWritable" );
		}
		map.put( "imageBigExist", (imageBig.exists() && !imageBig.getName().equals( "cat_null_big.gif" )) ? true : false );

//		String protectedLevels[] = new String[10];
//		for ( int i = 0; i < protectedLevels.length; i++ )
//		{
//			StringBuffer protectedLevel = new StringBuffer( "1" );
//			for ( int y = 0; y < i; y++ )
//				protectedLevel.append( "0" );
//			protectedLevels[i] = protectedLevel.toString();
//		}
		map.put( "protectedLevels", Constants.protectedLevels() );

		// labels
		map.put("labels", this.webJaguar.getLabels("protected"));
		
		if(((String) gSiteConfig.get("gI18N")).length()>0){
			map.put("languageCodes", gI18N);
		}
			
		return map;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception
	{
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		gI18N = this.webJaguar.getLanguageSettings();
		CategoryForm categoryForm = new CategoryForm();
		Category parentCategory = new Category();
		if ( request.getParameter( "parent" ) != null )
		{
			try
			{
				categoryForm.getCategory().setParent( new Integer( request.getParameter( "parent" ) ) );
			}
			catch ( NumberFormatException e )
			{}
		}
		if ( request.getParameter( "id" ) != null )
		{
			categoryForm = new CategoryForm( );
			Category category = this.webJaguar.getCategoryById( new Integer( request.getParameter( "id" ) ), "" ) ;
			categoryForm.setNewCategory( false );
			categoryForm.setCategory( category );		
			// i18n
			if (!(gI18N.isEmpty())) {
				categoryForm.setI18nCategory(this.webJaguar.getI18nCategory(category.getId(), null));				
			}
		}
		categoryForm.setOldParent( categoryForm.getCategory().getParent() );
		parentCategory.setName( this.webJaguar.getCategoryName( categoryForm.getCategory().getParent() ) );
		categoryForm.getCategory().setParentCategory( parentCategory );

		baseFile = new File( getServletContext().getRealPath( "/assets/Image/Category/" ) );
		Properties prop = new Properties();
		try
		{
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null )
			{
				baseFile = new File( (String) prop.get( "site.root" ) + "/assets/Image/Category/" );
			}
		}
		catch ( Exception e )
		{}
		
		// check category link images
		if ( !categoryForm.isNewCategory() )
		{
			File linkImage = new File( baseFile, "catlink_" + categoryForm.getCategory().getId() + ".gif" );
			categoryForm.getCategory().setHasLinkImage( linkImage.exists() );
			File linkImageOver = new File( baseFile, "catlink_over_" + categoryForm.getCategory().getId() + ".gif" );
			categoryForm.getCategory().setHasLinkImageOver( linkImageOver.exists() );
			File image = new File( baseFile, "cat_" + categoryForm.getCategory().getId() + ".gif" );
			categoryForm.getCategory().setHasImage( image.exists() );
		}

		return categoryForm;
	}

}
