/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 */

package com.webjaguar.web.admin.category;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CategorySearch;
import com.webjaguar.web.domain.Constants;

public class ListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		// search criteria
		CategorySearch search = getCategorySearch( request ); 

		// check if add button was pressed
		if ( request.getParameter( "__add" ) != null )
		{
			if ( search.getParent() != null )
			{
				return new ModelAndView( new RedirectView( "category.jhtm" ), "parent", search.getParent() );
			}
			else
			{
				return new ModelAndView( new RedirectView( "category.jhtm" ) );
			}
		}

		// check if update ranking button was pressed
		if ( request.getParameter( "__update_ranking" ) != null )
		{
			updateRanking( request );
		}

		Map<String, Object> myModel = new HashMap<String, Object>();
		search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		PagedListHolder categoryList = new PagedListHolder(this.webJaguar.getCategoryList( search ));

		categoryList.setPageSize(search.getPageSize());
		categoryList.setPage(search.getPage()-1);

		myModel.put( "categories", categoryList );
		myModel.put( "categoryTree", this.webJaguar.getCategoryTree(search.getParent(), true, null ));
		myModel.put( "search", search );
		
		//Go to New ADmin Layout
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/catalog/category/list", "model", myModel);
		}
		
		return new ModelAndView( "admin/catalog/category/list", "model", myModel );
	}
	
	private CategorySearch getCategorySearch(HttpServletRequest request) {
		CategorySearch categorySearch = (CategorySearch) request.getSession().getAttribute( "categorySearch" );
		if (categorySearch == null) {
			categorySearch = new CategorySearch();
			request.getSession().setAttribute( "categorySearch", categorySearch );
		}
		
		// parent ID
		if (request.getParameter("parent") != null) {
			if (request.getParameter("parent").equals( "home" )) {
				// home page
				categorySearch.setHomePage( true );
				categorySearch.setMultiStore(false);
				categorySearch.setParent(null);
			} else if (request.getParameter("parent").equals( "multi" )) {
				// home page
				categorySearch.setMultiStore(true);
				categorySearch.setHomePage(false);
				categorySearch.setParent(null);
			} else {
				int parentId = ServletRequestUtils.getIntParameter( request, "parent", -1 );		
				if (parentId > -1) {
					categorySearch.setParent(parentId);
				} else {
					categorySearch.setParent( null );
				}				
				categorySearch.setHomePage( false );
				categorySearch.setMultiStore(false);
			}
		}
						
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				categorySearch.setPage( 1 );
			} else {
				categorySearch.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				categorySearch.setPageSize( 10 );
			} else {
				categorySearch.setPageSize( size );				
			}
		}
		
		return categorySearch;
	}		

	private void updateRanking(HttpServletRequest request)
	{
		Enumeration paramNames = request.getParameterNames();
		List<Map <String, Integer>> data = new ArrayList<Map <String, Integer>>();
		while ( paramNames.hasMoreElements() )
		{
			String paramName = paramNames.nextElement().toString();
			if ( paramName.startsWith( "__rank_" ) )
			{
				HashMap<String, Integer> rankingMap = new HashMap<String, Integer>();
				try
				{
					Integer id = new Integer( paramName.substring( 7 ) );
					if ( request.getParameter( paramName ).equals( "" ) )
					{
						rankingMap.put( "id", id );
						rankingMap.put( "rank", null );
					}
					else
					{
						Integer rank = new Integer( request.getParameter( paramName ) );
						if ( rank.intValue() >= 0 )
						{
							rankingMap.put( "id", id );
							rankingMap.put( "rank", rank );
						}
					}
					data.add( rankingMap );
				}
				catch ( NumberFormatException e )
				{}
			}
		}
		this.webJaguar.updateCategoryRanking( data );
	}
}