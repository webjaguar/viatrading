/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 02.24.2008
 */

package com.webjaguar.web.admin.category;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.CategoryLinkType;

public class CategoryTreeImportController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {	
        
    	String formView = "admin/catalog/category/import";
    	
		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			return new ModelAndView(formView, "message", "excelfile.notWritable");	
		}    	
    	
		// check if upload button was pressed
		if (request.getParameter("__upload") != null) {
			
			// check category id first
			String parent = ServletRequestUtils.getStringParameter(request, "parent");
			Integer parentId = null;
			if (parent.trim().length() > 0) {
				try {
					parentId = ServletRequestUtils.getIntParameter(request, "parent");
					if (this.webJaguar.getCategoryName(parentId) == null) {
						return new ModelAndView(formView, "message", "typeMismatch.exportProductSearch.category");
					}
				} catch (Exception e) {
					return new ModelAndView(formView, "message", "typeMismatch.exportProductSearch.category");
				}				
			}
			
	    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	    	MultipartFile file = multipartRequest.getFile("file");
//	    	if (file != null && !file.isEmpty() && file.getContentType().equals("application/vnd.ms-excel")) {
	    	if (file != null && !file.isEmpty()) {
	    		File excel_file = new File(baseFile, "categoryTree_import.xls");
	    		file.transferTo(excel_file);
	    		file = null; // delete to save resources
	        	try {
	        		Map<String, Object> map = new HashMap<String, Object>();
	        		List<Category> categories = processFile(excel_file);
	        		if (categories.isEmpty()) {
	        			return new ModelAndView(formView, "message", "excelfile.empty");
	        		} else {
		        		this.webJaguar.insertCategoryTree(parentId, categories);
		        		map.put("categories", categories);
	        			return new ModelAndView("admin/catalog/category/importSuccess", map);	        			
	        		}
	        	} catch (IOException e) {
	    			return new ModelAndView(formView, "message", "excelfile.invalid");
	    		} finally {
	    	    	// delete excel file
	    			excel_file.delete();	    			
	    		}    			
	    	} else {
	        	return new ModelAndView(formView, "message", "excelfile.invalid");	    		
	    	}
		}
    	
    	return new ModelAndView(formView);
    }
	
    private List<Category> processFile(File excel_file) throws IOException, Exception {

    	POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(excel_file));
    	HSSFWorkbook wb = new HSSFWorkbook(fs);

    	HSSFSheet sheet = wb.getSheetAt(0);
    	Map<String, Short> header = new HashMap<String, Short>();
    	HSSFRow row = sheet.getRow(0);        	// first row

    	Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
    	List<Category> categories = new ArrayList<Category>();

    	
    	Iterator iter = row.cellIterator();
    	// get header names
		while (iter.hasNext()) {
			HSSFCell cell = (HSSFCell) iter.next();
			String headerName = "";
			try {
				headerName = cell.getStringCellValue().trim();					
			} catch (NumberFormatException e) {
				// do nothing
			}

			if (!headerName.equals( "" )) {
				if (header.put(headerName.toLowerCase(), cell.getCellNum()) != null) {
						// duplicate header
						//map.put("message", "excelfile.duplicateHeaders");
						//map.put("arguments", headerName);
						//return false;													
				}					
			} 
		}
		
    	for (int i=1; i<=sheet.getLastRowNum(); i++) {
			row = sheet.getRow(i);
			if (row == null) {
				continue;
			}
			Category category = null;
			for (int col=0; col<=row.getLastCellNum(); col++) {
				if (row.getCell(col) != null && row.getCell(col).getStringCellValue().trim().length() > 0) {	
					// all category columns have to have Category as header name
					if (sheet.getRow(0).getCell(col).getStringCellValue().equalsIgnoreCase("category")) {
						category = new Category();
						category.setName(row.getCell(col).getStringCellValue());
						
						if (categoryMap.containsKey(col-1)) {
							categoryMap.get(col-1).getSubCategories().add(category);
						} else {
							categories.add(category);
						}
						categoryMap.put(col, category);
					}
					
					if (category != null)
						processRow(row, header, category);	
				}
			}
    	}	
    	return categories;
    }  
    
    private void processRow(HSSFRow row, Map<String, Short> header, Category category) {
   	
		// get alternative link
		if (header.containsKey( "alternativelink" )) {
			category.setUrl(getStringValue(row, header.get("alternativelink")) );	
		}
		// get SEO Field 1
		if (header.containsKey( "field1" )) {
			category.setField1(getStringValue(row, header.get("field1")) );	
		}
		// get SEO Field 2
		if (header.containsKey( "field2" )) {
			category.setField2(getStringValue(row, header.get("field2")) );	
		}
		// get SEO Field 3
		if (header.containsKey( "field3" )) {
			category.setField3(getStringValue(row, header.get("field3")) );	
		}
		// get SEO Field 4
		if (header.containsKey( "field4" )) {
			category.setField4(getStringValue(row, header.get("field4")) );	
		}
		// get SEO Field 5
		if (header.containsKey( "field5" )) {
			category.setField5(getStringValue(row, header.get("field5")) );	
		}
		// get link type
		if (header.containsKey( "linktype" )) {
			try {
				category.setLinkType( getEnumValue(row, header.get("linktype")) );
			} catch (Exception e) {
				category.setLinkType(CategoryLinkType.VISIBLE);
			}				
		}
    }
    
    private Integer getIntegerValue(HSSFRow row, short cellnum) throws Exception {
		HSSFCell cell = row.getCell(cellnum);
    	Integer value = null;
    	
    	if (cell != null) {
    		switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = new Integer(cell.getStringCellValue());										
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Double cellValue = cell.getNumericCellValue();
					if (cellValue != cellValue.intValue()) {
						throw new Exception();	
					}
					value =  cellValue.intValue();
					break;
			} // switch
    	}
    	return value;
    }
    
    private String getStringValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		String value = "";

    	if (cell != null) {
			switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = cell.getStringCellValue();
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Integer intValue = null;
					try {
						intValue = getIntegerValue(row, cellnum);
					} catch (Exception e) {}
					if (intValue != null) {
						value = intValue.toString();
						break;
					}
					value = new Double(cell.getNumericCellValue()).toString();
					break;
			} // switch		
    	}
		return value;
    }
    
    private CategoryLinkType getEnumValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		CategoryLinkType value = null;
    	
    	if (cell != null) {
    		switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					if (cell.getStringCellValue().equalsIgnoreCase("HIDDEN")) {
						value = CategoryLinkType.HIDDEN;
					} else if (cell.getStringCellValue().equalsIgnoreCase("NONLINK")) {
						value = CategoryLinkType.NONLINK;
					} else {
						value = CategoryLinkType.VISIBLE;
					}
					break;
			} // switch
    	} else {
    		value = CategoryLinkType.VISIBLE;
    	}
    	return value;
    }
}