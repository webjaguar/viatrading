/* Copyright 2013 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 04.03.2013
 */

package com.webjaguar.web.admin.datafeed;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailSender;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.thirdparty.jgoodin.Jgoodin;

public class JgoodinProductsController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
			
		Map<String, Object> model = new HashMap<String, Object>();
		
		Jgoodin jgoodin = new Jgoodin(siteConfig, gSiteConfig, mailSender, webJaguar);
		
		if(jgoodin.getJgoodinProducts()){
			model.put("success", true);
		} else {
			model.put("success", false);
		}
		
		
		return new ModelAndView("admin/vendors/jgoodin/products", "model", model);

	}
}