/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.04.2007
 */

package com.webjaguar.web.admin.datafeed;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.DataFeed;
import com.webjaguar.model.DataFeedSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.ProductXML;
import com.webjaguar.model.WebjaguarDataFeed;

public class WebjaguarFeedController extends WebApplicationObjectSupport implements Controller {
	
	private static WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	private String feed = "webjaguar";
	private static JAXBContext context;
	private static File productFile;
	private static String content;
	private static int gPRODUCT_FIELDS;
	private static List<ProductField> productFields;
	private static Map <String, WebjaguarCategory> categoryMap;
	private Map<String, Configuration> siteConfig;
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
    	
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gPRODUCT_FIELDS = (Integer) gSiteConfig.get("gPRODUCT_FIELDS");
		productFields = webJaguar.getProductFields(null, gPRODUCT_FIELDS);
		context  = JAXBContext.newInstance(ProductXML.class);
		context.createUnmarshaller();
		
		if (!((String) gSiteConfig.get( "gDATA_FEED" )).contains( feed + ",")) {
			return new ModelAndView( new RedirectView( "index.jhtm") );
		}

		List<WebjaguarDataFeed> dataFeedConfigList = webJaguar.getWebjaguarDataFeedList(null);
	
		DataFeedSearch search = getDateFeedSearch(request);
		if ( (Boolean) gSiteConfig.get("gMASTER_SKU") ) {
			search.setMasterSkuImage("masterSKU");
		}
		
		// get generated data feed file
    	File baseFile = new File(getServletContext().getRealPath("/admin/feed/"));
    	if (!baseFile.exists()) {
    		baseFile.mkdir();
    	}
		
		HashMap<String, Object> fileMap = new HashMap<String, Object>();
		File file[] = baseFile.listFiles();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith(feed) & file[f].getName().endsWith(".xml")) {
				if (request.getParameter("__ftp") != null) { // check if new button was pressed
					file[f].delete();
				} else if(request.getParameter("__download") != null) {
					context  = JAXBContext.newInstance(Product.class);
					productFile = file[f];
					parseProductXmlFile(file[f]);
				} else {
					fileMap.put("file"+(f+1), file[f]);
					fileMap.put("lastModified"+(f+1), new Date(file[f].lastModified()));
					map.put("generatedFile", fileMap);
				}
			}
		}
    	
		if (request.getParameter("__ftpDownload") != null) {
			try {
				context  = JAXBContext.newInstance(Product.class);
				SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
		    	String fileName = feed + "_" + dateFormatter.format(new Date()) + ".xml";
		    	productFile = new File(baseFile, fileName);
				map.put("ftpMessage" , ftpDownloadDataFeed(productFile, dataFeedConfigList.get(0)));
				parseProductXmlFile(productFile);
			} catch (UnknownHostException e) {
				map.put("message1", "dataFeed.ftp.unknownHost");					
			} catch (Exception e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.missingFileName");					
			}
		}
		
		// check if new button was pressed
		if (request.getParameter("__ftp") != null ) {
			
			for(WebjaguarDataFeed dataFeedConfig : dataFeedConfigList) {
				if(dataFeedConfig.isActive()) {
					this.uploadFeed(baseFile, search, gSiteConfig, map, fileMap, dataFeedConfig);
				}
			}
		}
		// check if update button was pressed
		if (request.getParameter("__update") != null) {
			updateDataFeedConfig(request, new ArrayList<DataFeed>(), dataFeedConfigList);
			map.put("message2", "update.successful");
		}
		
        map.put("dataFeedConfigList", dataFeedConfigList);
		map.put("search", search);
		
		return new ModelAndView("admin/dataFeed/webjaguarDataFeed", map);
	}

    
    private void uploadFeed(File baseFile, DataFeedSearch search, Map<String, Object> gSiteConfig, Map<String, Object> map, HashMap<String, Object> fileMap, WebjaguarDataFeed webjaguarDataFeed) throws JAXBException {
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	String fileName = feed + "_" + dateFormatter.format(new Date()) + ".xml";
    	File dataFeedFile = new File(baseFile, fileName);
    	
		ProductXML pxml = new ProductXML();
		
		int productCount = webJaguar.webjaguarDataFeedProductListCount(search);
		System.out.println("Count "+productCount);
		int limit = 10;
		search.setLimit(limit);
		boolean hasHeaders = true;
		//String imagePath = "http://www."+gSiteConfig.get("gSITE_DOMAIN").toString()+"/assets/Image/Product/detailsbig/";
		String imagePath = siteConfig.get("SITE_URL").getValue()+"assets/Image/Product/detailsbig/";
		for (int offset=0, excel = 1; offset<productCount; excel++) {
	    	List<Product> productList = webJaguar.getWebjaguarDataFeedProductList(search, 5, imagePath, true);
	    	pxml.addToList(productList);
	    	
	    	offset = offset + limit;
	        search.setOffset(offset);
		}
		for (Product product : pxml.getProduct()) {
	    	String breadCrumb = null;
	    	for(Object catId : product.getCatIds()) {
	    		breadCrumb = this.getBreadCrumb( Integer.parseInt(catId.toString()));
	    		
	    		if(breadCrumb != null) {
	    			product.getBreadCrumb().add(breadCrumb);
	    		}
	    	}
	    	
	    	if(webjaguarDataFeed.getPrefix() != null && !webjaguarDataFeed.getPrefix().isEmpty()) {
	    		product.setSku(webjaguarDataFeed.getPrefix()+product.getSku());
	    	}
		}

		context.createMarshaller().marshal(pxml, dataFeedFile);
		
		fileMap.put("file"+webjaguarDataFeed.getId(), dataFeedFile);
		map.put("generatedFile", fileMap);
		// ftp datafeed file
		if (dataFeedFile != null && dataFeedFile.exists()) {
			try {
				map.put("ftpMessage" , ftpDataFeed(dataFeedFile, webjaguarDataFeed));
			} catch (UnknownHostException e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.unknownHost");					
			} catch (Exception e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.missingFileName");					
			}
		} else {
			map.put("message1", "dataFeed.ftp.missingDataFeed");
		}

		if (hasHeaders) {
			map.put("message1", "dataFeed.generate.success");
		} else {
			map.put("message1", "dataFeed.generate.noFieldsEnabled");
		}
	
    }
    
    private void updateDataFeedConfig(HttpServletRequest request, List<DataFeed> dataFeedList, List<WebjaguarDataFeed> dataFeedConfigList) {
		for (DataFeed dataFeed: dataFeedList) {
			dataFeed.setFeed( feed );
			dataFeed.setMethodName( ServletRequestUtils.getStringParameter( request, "__methodName_" + dataFeed.getFieldName(), "" ) );
			dataFeed.setEnabled( ServletRequestUtils.getBooleanParameter( request, "__enabled_" + dataFeed.getFieldName(), false ) );
			dataFeed.setDefaultValue(ServletRequestUtils.getStringParameter(request, "__defaultValue_" + dataFeed.getFieldName(), ""));
		}
		
		for(WebjaguarDataFeed webjaguarDataFeed : dataFeedConfigList) {
			
			webjaguarDataFeed.setServer(ServletRequestUtils.getStringParameter( request, "server_"+webjaguarDataFeed.getId(), "" ));
			webjaguarDataFeed.setUsername(ServletRequestUtils.getStringParameter( request, "username_"+webjaguarDataFeed.getId(), "" ));
			webjaguarDataFeed.setPassword(ServletRequestUtils.getStringParameter( request, "password_"+webjaguarDataFeed.getId(), "" ));
			webjaguarDataFeed.setFilename(ServletRequestUtils.getStringParameter( request, "filename_"+webjaguarDataFeed.getId(), "" ));
			webjaguarDataFeed.setPrefix(ServletRequestUtils.getStringParameter( request, "prefix_"+webjaguarDataFeed.getId(), "" ));
		//	webjaguarDataFeed.setClientId(ServletRequestUtils.getIntParameter( request, "clientId_"+webjaguarDataFeed.getId(), 1 ));
			webjaguarDataFeed.setActive(ServletRequestUtils.getBooleanParameter(request, "active_"+webjaguarDataFeed.getId(), false));
			
			try {
				webjaguarDataFeed.setPrice(ServletRequestUtils.getDoubleParameter( request, "price_"+webjaguarDataFeed.getId() ));
			} catch (ServletRequestBindingException e) {
				webjaguarDataFeed.setPrice(null);
			}
			try {
				webjaguarDataFeed.setInventory(ServletRequestUtils.getIntParameter( request, "username_"+webjaguarDataFeed.getId() ));
			} catch (ServletRequestBindingException e) {
				webjaguarDataFeed.setInventory(null);
			}
			try {
				webjaguarDataFeed.setCategory(ServletRequestUtils.getIntParameter( request, "category_"+webjaguarDataFeed.getId() ));
			} catch (ServletRequestBindingException e) {
				webjaguarDataFeed.setCategory(null);
			}
		}
		webJaguar.updateDataFeedList( dataFeedList, dataFeedConfigList );
	    
	}
    
    private String ftpDataFeed(File file, WebjaguarDataFeed webjaguarDataFeed) throws UnknownHostException, Exception {
    	System.out.println("0.0");
        
    	StringBuffer sbuff = new StringBuffer();
    	if (webjaguarDataFeed.getServer().trim().equals( "" )) {
    		throw new UnknownHostException();
    	}
    	System.out.println("1.0 "+webjaguarDataFeed.getFilename());
        
    	if (webjaguarDataFeed.getFilename().trim().equals( "" )) {
    		throw new Exception();
    	}
    	System.out.println("2.0");
        

    	// Connect and logon to FTP Server
        FTPClient ftp = new FTPClient();
        ftp.connect( webjaguarDataFeed.getServer() );
        
        // login
        ftp.login( webjaguarDataFeed.getUsername(), webjaguarDataFeed.getPassword() );
        System.out.println("Uploading");
        
        if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
        	  System.out.println("Start");
               // upload file
            FileInputStream fis = new FileInputStream(file);
            ftp.storeFile(webjaguarDataFeed.getFilename(), fis );
            sbuff.append("Uploading file: " + webjaguarDataFeed.getFilename() + "compeleted.");
            fis.close();        	
        }
     	  System.out.println("End");
     	 
        // Logout from the FTP Server and disconnect
        ftp.logout();
        ftp.disconnect();
        
        return sbuff.toString();
    }
    
    private String ftpDownloadDataFeed(File file, WebjaguarDataFeed webjaguarDatafeed) throws UnknownHostException, Exception {
    	StringBuffer sbuff = new StringBuffer();
    	if (webjaguarDatafeed.getServer().trim().equals( "" )) {
    		throw new UnknownHostException();
    	}
    	if (webjaguarDatafeed.getFilename().trim().equals( "" )) {
    		throw new Exception();
    	}
    	
    	// Connect and logon to FTP Server
        FTPClient ftp = new FTPClient();
        ftp.connect( webjaguarDatafeed.getServer() );
        
        // login
        ftp.login( webjaguarDatafeed.getUsername(), webjaguarDatafeed.getPassword() );
        
        if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
            // download file
            FileOutputStream fos = new FileOutputStream(file);
            ftp.retrieveFile(webjaguarDatafeed.getFilename(), fos);
            sbuff.append("Downloading file: " + webjaguarDatafeed.getFilename() + "Compeleted.");
            fos.close();        	
        }
        
        // Logout from the FTP Server and disconnect
        ftp.logout();
        ftp.disconnect();
        
        return sbuff.toString();
    }
    
    private DataFeedSearch getDateFeedSearch(HttpServletRequest request) {
    	DataFeedSearch search = new DataFeedSearch();
    	
    	try {
    		search.setPrice( Double.parseDouble( request.getParameter("price") ) );
    	} catch (Exception e) {
    		search.setPrice( null );
    	}
    	
    	try {
    		search.setInventory( Integer.parseInt( request.getParameter("inventory") ) );
    	} catch (Exception e) {
    		search.setInventory( null );
    	}
    	
    	try {
    		search.setCategory( Integer.parseInt( request.getParameter("category") ) );
    	} catch (Exception e) {
    		search.setCategory( null );
    	}
    	
    	return search;
    }
    
    private String getBreadCrumb(Integer catId) {
    	List<Category> breadCrumb = webJaguar.getCategoryTree(catId, true, null);
    	String categoryTree = "";
    	for (Category category : breadCrumb) {
    		categoryTree += category.getName() + "-->" ;
    	}
    	return categoryTree.substring(0, categoryTree.lastIndexOf('-')-1);
    }
    
    
 	public void parseProductXmlFile(File xml_file) {
 		categoryMap = webJaguar.getWebjaguarCategoryMap();
		try {
			SAXParserFactory f = SAXParserFactory.newInstance();
	        SAXParser parser = f.newSAXParser();
	        DefaultHandler handler = new MyContentHandler();
	        content = FileUtils.readFileToString(productFile, "UTF-8");
	        parser.parse(xml_file,handler);
	    } catch (ParserConfigurationException e) {System.out.println("error 1 ");
	    } catch (SAXException e) {System.out.println("error 2 ");
	    } catch (IOException e) {System.out.println("error 3 ");
	    	e.printStackTrace();
	    }
	}
	
	public static class MyContentHandler extends DefaultHandler {
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		List<String> dataDeleted = new ArrayList<String>();
		List<Map <String, Object>> categoryData = new ArrayList<Map <String, Object>>();
		HashMap<String, Object> map;
		Set<String> breadCrumbs = new TreeSet<String>();
		StringBuffer xml = new StringBuffer();
		
		boolean product;
		// for DIFF
		boolean modified, added, deleted;
		
		public void startDocument() throws SAXException {
			xml = new StringBuffer();
		//	webJaguar.markAndInactiveOldProducts("Webjaguar", datfalse);
	   }
	    
	    public void endDocument() throws SAXException {
	       // for files that has less than 500 Product
	       if (data.size() > 0) {
				// products
				updateProduct(data);
				data.clear();
				map.clear();
	       }
	       if (breadCrumbs.size() > 0) {
				// categories
	    	    updateCategories(breadCrumbs);
				breadCrumbs.clear();
		   }
	       if (dataDeleted.size() > 0) {
				// products
				deleteProduct(dataDeleted);
				dataDeleted.clear();
	       }
	       if (categoryData.size() > 0) {
	    	    System.out.println("Category Mapping Size "+categoryData.size());
				// product category
				updateProductCategory(categoryData);
				categoryData.clear();
		   }
	  //     webJaguar.markAndInactiveOldProducts("Webjaguar", true);
	    }
	    
	    public void startElement(String nsURI, String strippedName, String tagName, Attributes attributes) throws SAXException {
	    	if (tagName.equalsIgnoreCase("Product")) { 
	    		map = new HashMap<String, Object>();
	    		
	    		// read and save from file directly
				int start = content.indexOf("<product id=\""+attributes.getValue("id")+"\"");
	    		int end = content.indexOf("</product>", start) + 10;
	    		xml.append(content.substring(start, end));
	    		
	    		// convert to Product object
	    		JAXBContext context;
	    		Unmarshaller unmarshaller = null;
	    		ByteArrayInputStream bis;
	    		Product prod = null;
				try {
					context = JAXBContext.newInstance(Product.class);
					unmarshaller = context.createUnmarshaller();
					StringBuffer sb = new StringBuffer(xml);
					bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
					prod = (Product) unmarshaller.unmarshal(bis);
				} catch (Exception e) { e.printStackTrace(); }
				
				if(prod != null) {
					Class<Product> c = Product.class;
					Method m = null;
					Object arglist[] = null;
					
					map.put("name", prod.getName());
					map.put("sku", prod.getSku());
					map.put("master_sku", prod.getMasterSku());
					map.put("feed_freeze", prod.isFeedFreeze());
					map.put("short_desc", prod.getShortDesc());
					map.put("long_desc", prod.getLongDesc());
					map.put("keywords", prod.getKeywords());
					map.put("weight", prod.getWeight());
					map.put("msrp", prod.getMsrp());
					map.put("hide_msrp", prod.isHideMsrp());
					map.put("price_by_customer", prod.isPriceByCustomer());
					map.put("price_1", prod.getPrice1());
					map.put("price_2", prod.getPrice2());
					map.put("price_3", prod.getPrice3());
					map.put("price_4", prod.getPrice4());
					map.put("price_5", prod.getPrice5());
					map.put("price_6", prod.getPrice6());
					map.put("price_7", prod.getPrice7());
					map.put("price_8", prod.getPrice8());
					map.put("price_9", prod.getPrice9());
					map.put("price_10", prod.getPrice10());
					map.put("qty_break_1", prod.getQtyBreak1());
					map.put("qty_break_2", prod.getQtyBreak2());
					map.put("qty_break_3", prod.getQtyBreak3());
					map.put("qty_break_4", prod.getQtyBreak4());
					map.put("qty_break_5", prod.getQtyBreak5());
					map.put("qty_break_6", prod.getQtyBreak6());
					map.put("qty_break_7", prod.getQtyBreak7());
					map.put("qty_break_8", prod.getQtyBreak8());
					map.put("qty_break_9", prod.getQtyBreak9());
					map.put("quote", prod.isQuote());
					for (ProductField productField: productFields) {
		    			if (productField.isEnabled() && productField.isProductExport()) {
		            		try {
								m = c.getMethod("getField" + productField.getId());
								map.put("field_"+productField.getId(), (String) m.invoke(prod, arglist));
							} catch (Exception e) { e.printStackTrace(); }
		            	}
		        	}
		        	map.put("minimum_qty", prod.getMinimumQty());
					map.put("protected_level", prod.getProtectedLevel());
					map.put("packing", prod.getPacking());
					map.put("login_require", prod.isLoginRequire());
					map.put("hide_price", prod.isHidePrice());
					map.put("hide_header", prod.isHideHeader());
					map.put("hide_topbar", prod.isHideTopBar());
					map.put("hide_leftbar", prod.isHideLeftBar());
					map.put("hide_rightbar", prod.isHideRightBar());
					map.put("hide_footer", prod.isHideFooter());
					map.put("hide_breadcrumbs", prod.isHideBreadCrumbs());
					map.put("add_to_list", prod.isAddToList());
					map.put("compare", prod.isCompare());
					map.put("viewed", prod.getViewed());
					map.put("taxable", prod.isTaxable());
					map.put("searchable", prod.isSearchable());
					map.put("inventory", prod.getInventory());
					map.put("neg_inventory", prod.isNegInventory());
					map.put("show_neg_inventory", prod.isShowNegInventory());
					map.put("custom_shipping_enabled", prod.isCustomShippingEnabled());
					map.put("subscription", prod.isSubscription());
					map.put("active", prod.isActive());
					map.put("enable_rate", prod.isEnableRate());
					map.put("sitemap_priority", prod.getSiteMapPriority());
					map.put("feed", "Webjaguar");				
					map.put("feed_new", true);
					
					if (prod.getImages() != null && !prod.getImages().isEmpty()) {
			    		int imageIndex = 1;
			    		for (ProductImage image : prod.getImages()) {
			    			map.put("image"+imageIndex++, image.getImageUrl());
			    		}
			    	}
					map.put("productId", webJaguar.getProductIdBySku(prod.getSku()));
					breadCrumbs.addAll(prod.getBreadCrumb());
					
					// categories
					List<Integer> catIds = new ArrayList<Integer>();
					boolean productAdded = false;
					for(String breadCrumb : prod.getBreadCrumb()) {
						
						if(categoryMap.containsKey(breadCrumb)) {
							if(!productAdded) {
								data.add(map);
								if (map.get("productId") == null) {
									// new item
									Product newProduct = new Product(prod.getSku(), "Feed");
									webJaguar.insertProduct(newProduct, null);
									map.put("productId", newProduct.getId());
								}
								productAdded = true;
							}
							catIds.add(categoryMap.get(breadCrumb).getCatId());
						}	
					}
					// category mapping
					for (Integer catId: catIds) {
						HashMap<String, Object> mapCategory = new HashMap<String, Object>();
						mapCategory.put("productId", map.get("productId"));
						mapCategory.put("categoryId", catId);
						categoryData.add(mapCategory);
					}
				}
			}
	    }
	    
	    public void endElement(String nsURI, String strippedName, String tagName) throws SAXException {
    		
	    	if (tagName.equalsIgnoreCase("product")) {	
	    		product = false;
	    		xml = new StringBuffer();
	     		if (data.size() == 500) {
					// products
					updateProduct(data);
					data.clear();
					map.clear();
				}
	     		if (categoryData.size() == 500) {
					// product category
					updateProductCategory(categoryData);
					categoryData.clear();
				}
	    	}
	    }
	    
	    public void warning(SAXParseException e) throws SAXException {
	        System.out.println("Warning: " + e.getMessage());
	    }

	    private void updateProduct(List<Map <String, Object>> data) {
	    	// update Products
	    	webJaguar.nonTransactionSafeUpdateWebjaguarProduct(getCsvFeedList(), data);
	    }
	    
	    private void updateProductCategory(List<Map <String, Object>> categoryData) {
	    	// update Product Category
	    	webJaguar.nonTransactionSafeInsertCategories(categoryData);		
		}
	    
	    private void updateCategories(Set<String> breadCrumb) {
	    	//get all possible breadcrumb
	    	getAllBreadCrumbs(breadCrumb);
	    	
	    	// update Categories
	    	List<WebjaguarCategory> wjCategoryList = new ArrayList<WebjaguarCategory>();
			WebjaguarCategory wjCategory = null;
	    	for(String category : breadCrumb) {
	    		String[] categories = category.split("-->");
		    	if(categories.length > 0) {
					wjCategory = new WebjaguarCategory();
		    		wjCategory.setBreadCrumb(category.trim());
		    		wjCategory.setCat(categories[categories.length - 1].trim());
		    
		    		wjCategoryList.add(wjCategory);
				}
	    	}
	    	webJaguar.nonTransactionSafeInsertWebjaguarCategories(wjCategoryList);
	    }
	    
	    private void getAllBreadCrumbs(Set<String> breadCrumb) {
	    	Set<String> breadCrumbTemp = new TreeSet<String>();
	    	for(String category : breadCrumb) {
	    		String[] categories = category.split("-->");
		    	if(categories.length > 0) {
		    	  StringBuffer sb = new StringBuffer();
		    	  sb.append(categories[0].trim());
		    	  breadCrumbTemp.add(sb.toString());
				  for(int i=0; i< categories.length-1; i++) {
					  breadCrumbTemp.add(sb.append("-->"+categories[i+1].trim()).toString());
				  }
		    	}
	    	}
	    	
	    	breadCrumb.clear();
	    	breadCrumb.addAll(breadCrumbTemp);
	    }
	    
	    private void deleteProduct(List<String> data) {
	    	// inactive Products
	        // webJaguar.nonTransactionSafeInactiveASIFileFeedProduct(data, "asi_unique_id");
	    }
	    
	    private List<CsvFeed> getCsvFeedList() {
			List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
			
			csvFeedList.add(new CsvFeed("name"));
			csvFeedList.add(new CsvFeed("sku"));
			csvFeedList.add(new CsvFeed("master_sku"));
			csvFeedList.add(new CsvFeed("feed_freeze"));
			csvFeedList.add(new CsvFeed("short_desc"));
			csvFeedList.add(new CsvFeed("long_desc"));
			csvFeedList.add(new CsvFeed("keywords"));
			csvFeedList.add(new CsvFeed("weight"));
			csvFeedList.add(new CsvFeed("msrp"));
			csvFeedList.add(new CsvFeed("hide_msrp"));
			csvFeedList.add(new CsvFeed("price_by_customer"));
			csvFeedList.add(new CsvFeed("price_1"));
			csvFeedList.add(new CsvFeed("price_2"));
			csvFeedList.add(new CsvFeed("price_3"));
			csvFeedList.add(new CsvFeed("price_4"));
			csvFeedList.add(new CsvFeed("price_5"));
			csvFeedList.add(new CsvFeed("price_6"));
			csvFeedList.add(new CsvFeed("price_7"));
			csvFeedList.add(new CsvFeed("price_8"));
			csvFeedList.add(new CsvFeed("price_9"));
			csvFeedList.add(new CsvFeed("price_10"));
			csvFeedList.add(new CsvFeed("qty_break_1"));
			csvFeedList.add(new CsvFeed("qty_break_2"));
			csvFeedList.add(new CsvFeed("qty_break_3"));
			csvFeedList.add(new CsvFeed("qty_break_4"));
			csvFeedList.add(new CsvFeed("qty_break_5"));
			csvFeedList.add(new CsvFeed("qty_break_6"));
			csvFeedList.add(new CsvFeed("qty_break_7"));
			csvFeedList.add(new CsvFeed("qty_break_8"));
			csvFeedList.add(new CsvFeed("qty_break_9"));
			csvFeedList.add(new CsvFeed("quote"));
			for(ProductField field : productFields) {
				if(field.isEnabled()) {
					csvFeedList.add(new CsvFeed("field_"+field.getId()));
				}
			}
			csvFeedList.add(new CsvFeed("minimum_qty"));
			csvFeedList.add(new CsvFeed("protected_level"));
			csvFeedList.add(new CsvFeed("packing"));
			csvFeedList.add(new CsvFeed("login_require"));
			csvFeedList.add(new CsvFeed("hide_price"));
			csvFeedList.add(new CsvFeed("hide_header"));
			csvFeedList.add(new CsvFeed("hide_topbar"));
			csvFeedList.add(new CsvFeed("hide_leftbar"));
			csvFeedList.add(new CsvFeed("hide_rightbar"));
			csvFeedList.add(new CsvFeed("hide_footer"));
			csvFeedList.add(new CsvFeed("hide_breadcrumbs"));
			csvFeedList.add(new CsvFeed("add_to_list"));
			csvFeedList.add(new CsvFeed("compare"));
			csvFeedList.add(new CsvFeed("viewed"));
			csvFeedList.add(new CsvFeed("taxable"));
			csvFeedList.add(new CsvFeed("searchable"));
			csvFeedList.add(new CsvFeed("inventory"));
			csvFeedList.add(new CsvFeed("neg_inventory"));
			csvFeedList.add(new CsvFeed("show_neg_inventory"));
			csvFeedList.add(new CsvFeed("custom_shipping_enabled"));
			csvFeedList.add(new CsvFeed("subscription"));
			csvFeedList.add(new CsvFeed("active"));
			csvFeedList.add(new CsvFeed("enable_rate"));
			csvFeedList.add(new CsvFeed("sitemap_priority"));
			csvFeedList.add(new CsvFeed("feed"));
			csvFeedList.add(new CsvFeed("feed_new"));
			
			return csvFeedList;
		}
	 }	
}