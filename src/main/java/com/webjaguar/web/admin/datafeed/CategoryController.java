/* Copyright 2011 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 07.08.2011
 */

package com.webjaguar.web.admin.datafeed;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class CategoryController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		List<WebjaguarCategory> categories = new ArrayList<WebjaguarCategory>();
		
		if (request.getMethod().equals("POST")) {
			for (String breadCrumb: request.getParameterValues("breadCrumb")) {
				WebjaguarCategory category = new WebjaguarCategory();
				category.setBreadCrumb(breadCrumb);
				try {
					category.setCatId(ServletRequestUtils.getIntParameter(request, breadCrumb + "_catId"));
				} catch (Exception e) {
					category.setCatId(null);
				}
				categories.add(category);
			}
			this.webJaguar.nonTransactionSafeUpdateWebjaguarCategories(categories);
		}
		
		categories = this.webJaguar.getWebjguarCategories();
		
		//assign child categories
		this.assignChildCategories(categories, map);
		
		Map<String, WebjaguarCategory> categoryMap = new HashMap<String, WebjaguarCategory>();
		Iterator<WebjaguarCategory> iter = categories.iterator();
		while (iter.hasNext()) {
			WebjaguarCategory category = iter.next();
			categoryMap.put(category.getCat().toLowerCase(), category);
		}

		return new ModelAndView("admin/vendors/webjaguar/category", map);
	}
	
	
	private void assignChildCategories(List<WebjaguarCategory> categories, Map<String, Object> map){
		Map<Integer, Map<String, WebjaguarCategory>> categoryLevelMap = new HashMap<Integer, Map<String, WebjaguarCategory>>();
		for(WebjaguarCategory category : categories) {
			String[] breadcrumbCategories = category.getBreadCrumb().split("-->");
			if(categoryLevelMap.get(breadcrumbCategories.length) == null) {
				categoryLevelMap.put(breadcrumbCategories.length, new HashMap<String, WebjaguarCategory>());
			}
			categoryLevelMap.get(breadcrumbCategories.length).put(category.getBreadCrumb(), category);
		}
		
		map.put("level", categoryLevelMap.keySet().size());
		for(int level=1; level<=categoryLevelMap.keySet().size(); level++){
			if(level < categoryLevelMap.keySet().size()) {
				Map<String, WebjaguarCategory> categoryMapAtLevel = categoryLevelMap.get(level+1);
				Map<String, WebjaguarCategory> categoryMapAtParentLevel = categoryLevelMap.get(level);
				for(String breadCrumb : categoryMapAtLevel.keySet()){
					String parentCategoryBreadCrumb = breadCrumb.substring(0, breadCrumb.lastIndexOf("-->"));
					if(categoryMapAtParentLevel.get(parentCategoryBreadCrumb.trim()) != null && categoryMapAtLevel.get(breadCrumb.trim()) != null) {
						categoryMapAtParentLevel.get(parentCategoryBreadCrumb.trim()).getSubCat().add(categoryMapAtLevel.get(breadCrumb.trim()));
					}
				}
			}
		}
		List<WebjaguarCategory> mainCategories = new ArrayList<WebjaguarCategory>();
		if(categoryLevelMap.get(1) != null) {
			for(String breadCrumb : categoryLevelMap.get(1).keySet()) {
				mainCategories.add(categoryLevelMap.get(1).get(breadCrumb));
			}
		}
		map.put("categories", arrangeCategories(mainCategories, new ArrayList<WebjaguarCategory>()));
	}
	
	
	public List<WebjaguarCategory> arrangeCategories(List<WebjaguarCategory> mainCategories, List<WebjaguarCategory> allCategoryList) {
		
		for(WebjaguarCategory category : mainCategories) {
			allCategoryList.add(category);
			if(category.getSubCat() != null && !category.getSubCat().isEmpty()) {
				arrangeCategories(category.getSubCat(), allCategoryList);
			}
		}
		return allCategoryList;
	}
}
