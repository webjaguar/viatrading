/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.04.2007
 */

package com.webjaguar.web.admin.datafeed;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.DataFeed;
import com.webjaguar.model.DataFeedSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;

public class DataFeedController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	private String feed = "";
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
    	
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		// get feed 
		feed = ServletRequestUtils.getStringParameter( request, "feed", "" );
		if (feed.equals( "" ) || feed.contains( "," ) || !((String) gSiteConfig.get( "gDATA_FEED" )).contains( feed + ",")) {
			return new ModelAndView( new RedirectView( "index.jhtm") );
		}
		if (feed.equals( "webjaguar" )) {
			return new ModelAndView( new RedirectView( "myselfDataFeed.jhtm") );
		}

		Map<String, Object> dataFeedConfig = this.webJaguar.getDataFeedConfig( feed );
		List<DataFeed> dataFeedList = this.webJaguar.getDataFeedList(feed);
		List<ProductField> productFields = this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));

		DataFeedSearch search = getDateFeedSearch(request);
		if ( (Boolean) gSiteConfig.get("gMASTER_SKU") ) {
			search.setMasterSkuImage("masterSKU");
		}
		
		// get generated data feed file
    	File baseFile = new File(getServletContext().getRealPath("/admin/feed/"));
    	if (!baseFile.exists()) {
    		baseFile.mkdir();
    	}
		HashMap<String, Object> fileMap = new HashMap<String, Object>();
		File file[] = baseFile.listFiles();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith(feed) & file[f].getName().endsWith(".txt")) {
				if (request.getParameter("__new") != null) { // check if new button was pressed
					file[f].delete();
				} else {
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					map.put("generatedFile", fileMap);
				}
			}
		}
    	
		// check if new button was pressed
		if (request.getParameter("__new") != null) {
	    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
	    	String fileName = feed + "_" + dateFormatter.format(new Date()) + ".txt";
	    	File dataFeedFile = new File(baseFile, fileName);
			PrintWriter pw = new PrintWriter(new FileWriter(dataFeedFile));
			
			List<DataFeed> headers = new ArrayList<DataFeed>();
	    	
			// filter the headers
			for (DataFeed dataFeed: dataFeedList) {
			 	if (dataFeed.isEnabled() || dataFeed.isRequired()) {
			 		headers.add( dataFeed );
				}
			}
			// create the headers, if feed is toher than equiteam
			if (!feed.equals("equiteam")) {
	    		for(int i=0; i< headers.size(); i++) {
	    			DataFeed dataFeed = headers.get(i);
					pw.print( dataFeed.getFieldName());
		    		if( i+1 < headers.size()) {
		    			pw.print("\t");
		    	    }	
	    		}
	    		pw.println();
			}
			
	    	
			int productCount = this.webJaguar.productDataFeedListCount(search);
			int limit = 3000;
			search.setLimit(limit);
			boolean hasHeaders = true;
			
			for (int offset=0, excel = 1; offset<productCount; excel++) {
		    	int start = offset + 1;
		    	int end = (offset + limit) < productCount ? (offset + limit) : productCount;
		    	if (feed.equals("equiteam")) {
			    	createEquiteamDataFeed( this.webJaguar.getProductDataFeedList(search), pw, headers, productFields, siteConfig,  gSiteConfig);
		    	} else {
			    	hasHeaders = createDataFeed( this.webJaguar.getProductDataFeedList(search), pw, headers, productFields, siteConfig,  gSiteConfig);
		    	}

		    	offset = offset + limit;
		        search.setOffset(offset);
			}
			pw.close();
			fileMap.put("file", dataFeedFile);
			map.put("generatedFile", fileMap);
			if (hasHeaders) {
				map.put("message1", "dataFeed.generate.success");
			} else {
				map.put("message1", "dataFeed.generate.noFieldsEnabled");
			}
		}
		
        // check if update button was pressed
		if (request.getParameter("__update") != null) {
			updateDataFeedConfig(request, dataFeedList, dataFeedConfig);
			map.put("message2", "update.successful");
		}
		
        // check if ftp button was pressed
		if (request.getParameter("__ftp") != null) {
			File generatedFile = (File) fileMap.get( "file" );
			if (generatedFile != null && generatedFile.exists()) {
				try {
					map.put("ftpMessage" , ftpDataFeed(generatedFile, dataFeedConfig));
				} catch (UnknownHostException e) {
					map.put("message1", "dataFeed.ftp.unknownHost");					
				} catch (Exception e) {
					e.printStackTrace();
					map.put("message1", "dataFeed.ftp.missingFileName");					
				}
			} else {
				map.put("message1", "dataFeed.ftp.missingDataFeed");
			}
		}
		
		map.put("search", search);
		map.put("dataFeedConfig", dataFeedConfig);
		map.put("dataFeedList", dataFeedList);
		map.put("productFields", productFields);
		
		return new ModelAndView("admin/dataFeed/dataFeed", map);
	}
    
    private void createEquiteamDataFeed(List<Product> productList, PrintWriter pw, 
    		List<DataFeed> headers, List<ProductField> productFields, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) throws Exception {

    	Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		
		NumberFormat nf = new DecimalFormat("#0.00");

		// create the rows
    	for (Product product: productList) {
        	for (DataFeed dataFeed: headers) {
        		Object value = null;
        		if (!dataFeed.getMethodName().equals( "" )) {
            		if (dataFeed.getMethodName().startsWith( "getField" )) {
                		// check if product field is enabled or valid
            			try {
            				if (productFields.get(Integer.parseInt(dataFeed.getMethodName().substring( 8 ))-1).isEnabled()) {
                        		m = c.getMethod(dataFeed.getMethodName());
            					value = (Object) m.invoke(product, arglist);
            				}
            			} catch (Exception e) {
            				// do nothing
            			}
            		} else if (dataFeed.getMethodName().equals( "url" )) {
            			if ( product.getMasterSku() != null && product.getMasterSku().length() > 0) {
            				value = siteConfig.get( "SITE_URL" ).getValue() 
							+ "product.jhtm?sku=" + product.getMasterSku() + "&trackcode=" + feed;

            			} else {
            				if (gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null && !product.getSku().contains("/")) {
            					value = siteConfig.get( "SITE_URL" ).getValue() 
            					+ siteConfig.get("MOD_REWRITE_PRODUCT").getValue() + "/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html?trackcode=" + feed;
            				} else {
            					value = siteConfig.get( "SITE_URL" ).getValue() 
            					+ "product.jhtm?id=" + product.getId() + "&trackcode=" + feed;
            				}
            			}
            		} else if (dataFeed.getMethodName().equals( "detailsbig" ) || dataFeed.getMethodName().equals( "thumb" )) {
            			if (product.getThumbnail() != null) {
            				if (product.getThumbnail().isAbsolute()) {
            					value = product.getThumbnail().getImageUrl();
            				} else {
            					value = siteConfig.get( "SITE_URL" ).getValue() 
            							+ "assets/Image/Product/" + dataFeed.getMethodName() + "/" + product.getThumbnail().getImageUrl();
            				}
            			}
            		} else if (dataFeed.getMethodName().equals("nontaxable")) {
            			value = !product.isTaxable(); 
            		} else if (dataFeed.getMethodName().equals("getSku")) {
        				value = (product.getSku() != null) ? product.getEncodedSku() : ""; 
        			} else if (dataFeed.getMethodName().equals("getName")) {
        				value = (product.getName() != null) ? product.getName().trim() : ""; 
        			} else if (dataFeed.getMethodName().equals("getShortDesc")) {
        				value = (product.getShortDesc() != null) ? product.getShortDesc().trim() : ""; 
        			}else if (dataFeed.getMethodName().equalsIgnoreCase("getBreadCrumb")) {
            			if ( product.getCatIds() != null && !product.getCatIds().isEmpty() ) {        					
        					// get breadCrumb of the first category Id. 
        					value = getBreadCrumb(Integer.parseInt(product.getCatIds().toArray()[0].toString()));
        				}
            		} else if (dataFeed.getMethodName().equalsIgnoreCase("getInventory") && (Boolean) gSiteConfig.get("gINVENTORY")) {
            			if ( product.getInventory() != null && product.getInventory() > 0 ) {
        					value = "Y";
        				} else { value = "N"; }
            		} else if (dataFeed.getMethodName().equalsIgnoreCase("getMsrp")) {
            			value = (product.getMsrp() != null) ? product.getMsrp() : "" ;
            		} else if(dataFeed.getMethodName().equalsIgnoreCase("getVarient")) {
            			if (dataFeed.getDefaultValue() != null && !dataFeed.getDefaultValue().equals("")) {
            				value = "#"+dataFeed.getDefaultValue()+"*" + ((product.getPriceTable5() != null) ? product.getPriceTable5() : " ") + "|";
            			} else {
            				value = "#20*" + ((product.getPriceTable5() != null) ? product.getPriceTable5() : " ") + "|";
            			}
            		} else {
                		m = c.getMethod(dataFeed.getMethodName());
            			value = (Object) m.invoke(product, arglist);
            		}
        		}
        		if (value != null) {
        			if (value.getClass() == String.class) {
        				value = ((String) value).replace("\n", " ");
        				value = ((String) value).replace("\r", " ");
        				value = ((String) value).replace("\t", " ");
        			}
        			if (value.getClass() == String.class && ((String) value).trim().equals("") && dataFeed.getDefaultValue() != null) {
            			pw.print(dataFeed.getDefaultValue());        				
        			} else {
                		pw.print(value);   	  				
        			}
        		} else if (dataFeed.getDefaultValue() != null) {
        			pw.print(dataFeed.getDefaultValue());
        		}
        		if (!dataFeed.getFieldName().equals("Category")) {
        			pw.print( "|");    
    			}			
        	} // for datafeed
        	pw.println();
    	} // for product
    }
    private boolean createDataFeed(List<Product> productList, PrintWriter pw, 
    		List<DataFeed> headers, List<ProductField> productFields, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) throws Exception {

    	if (headers.isEmpty()) {
    		return false;
    	}
		
    	Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		
		NumberFormat nf = new DecimalFormat("#0.00");

		// create the rows
		Iterator itr = productList.iterator();
    	while (itr.hasNext()) {
    		Product product = (Product) itr.next();
    		boolean remove =false;
    		
    		//To remove the Product for empty rule
    		for (DataFeed dataFeed: headers) {
        		Object value = null;
    			if (dataFeed.getMethodName().startsWith( "getField" )) {
            		// check if product field is enabled or valid
        			try {
        				if (productFields.get(Integer.parseInt(dataFeed.getMethodName().substring( 8 ))-1).isEnabled()) {
                    		m = c.getMethod(dataFeed.getMethodName());
        					value = (Object) m.invoke(product, arglist);
        				}
        			} catch (Exception e) {
        				// do nothing
        			}
        			if((value == null || value.toString().isEmpty()) && (dataFeed.getDefaultValue() == null || dataFeed.getDefaultValue().isEmpty())){
        				if(dataFeed.getExcludeRule().equalsIgnoreCase("empty")) {
            				remove = true;
        				}
        			}
        		} 
    		}
			if(remove) {
				continue;
			}
    		for(int i=0; i< headers.size(); i++) {
    			DataFeed dataFeed = headers.get(i);
    			Object value = null;
        		if (!dataFeed.getMethodName().equals( "" )) {
            		if (dataFeed.getMethodName().startsWith( "getField" )) {
                		// check if product field is enabled or valid
            			try {
            				if (productFields.get(Integer.parseInt(dataFeed.getMethodName().substring( 8 ))-1).isEnabled()) {
                        		m = c.getMethod(dataFeed.getMethodName());
            					value = (Object) m.invoke(product, arglist);
            				}
            			} catch (Exception e) {
            				// do nothing
            			}
            		} else if (dataFeed.getMethodName().equals( "url" )) {
            			if ( product.getMasterSku() != null && product.getMasterSku().length() > 0) {
            				value = siteConfig.get( "SITE_URL" ).getValue() 
							+ "product.jhtm?sku=" + product.getMasterSku() + "&trackcode=" + feed;

            			} else {
            				if (gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null && !product.getSku().contains("/")) {
            					value = siteConfig.get( "SITE_URL" ).getValue() 
            					+ siteConfig.get("MOD_REWRITE_PRODUCT").getValue() + "/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html?trackcode=" + feed;
            				} else {
            					value = siteConfig.get( "SITE_URL" ).getValue() 
            					+ "product.jhtm?id=" + product.getId() + "&trackcode=" + feed;
            				}
            			}
            		} else if (dataFeed.getMethodName().equals( "detailsbig" ) || dataFeed.getMethodName().equals( "thumb" )) {
            			if (product.getThumbnail() != null) {
            				if (product.getThumbnail().isAbsolute()) {
            					value = product.getThumbnail().getImageUrl();
            					if (dataFeed.getMethodName().equals("thumb") && gSiteConfig.get("gACCOUNTING").equals("CONCORD")) {
            						// concord thumbnails
            						value = product.getThumbnail().getImageUrl().replace("http://www.dollaritem.com/wms/images/catalog/", 
            								"http://www.dollaritem.com/wms/images/thumbs/");
            					}
            				} else {
            					value = siteConfig.get( "SITE_URL" ).getValue() 
            							+ "assets/Image/Product/" + dataFeed.getMethodName() + "/" + product.getThumbnail().getImageUrl();
            				}
            			}
            		} else if (dataFeed.getMethodName().equals("nontaxable")) {
            			value = !product.isTaxable(); 
            		} else if (dataFeed.getMethodName().equalsIgnoreCase("getBreadCrumb")) {
            			if ( product.getCatIds() != null && !product.getCatIds().isEmpty() ) {        					
        					// get breadCrumb of the first category Id. 
        					value = getBreadCrumb(Integer.parseInt(product.getCatIds().toArray()[0].toString()));
        				}
            		} else if (dataFeed.getMethodName().equalsIgnoreCase("getInventory") && (Boolean) gSiteConfig.get("gINVENTORY")) {
            			if ( product.getInventory() != null && product.getInventory() > 0 ) {
        					value = "Y";
        				} else { value = "N"; }
            		} else if (dataFeed.getMethodName().equalsIgnoreCase("getMsrp") ) {
            			value = product.getMsrp();
            		} else if (dataFeed.getMethodName().equals("getAvailability")) {
            			if (!dataFeed.getDefaultValue().isEmpty()) {
            				value = dataFeed.getDefaultValue();
            			} else {
            				value = product.getInventoryAFS() != null ? (product.getInventoryAFS() > 0 ? "in stock" : product.isShowNegInventory() ? "in stock" : "out of stock") : "in stock";
            			}
            		} else {
                		m = c.getMethod(dataFeed.getMethodName());
            			value = (Object) m.invoke(product, arglist);
            			if (feed.equals("newEggMall") && value != null && product.getCaseContent() != null) {
            				if (dataFeed.getMethodName().startsWith("getPrice") || dataFeed.getMethodName().equals("getWeight")) {
            					value = nf.format((Double) value * product.getCaseContent());
            				} else if (dataFeed.getFieldName().equals("TitleDescription") || dataFeed.getFieldName().equals("LineDescription") || dataFeed.getFieldName().equals("DetailDescription")) {
            					value = value + " (" + product.getCaseContent() + " " + siteConfig.get("CASE_UNIT_TITLE").getValue() + "/" + product.getPacking() + ")";
            				}
            			}
            			if (feed.equals("googleBase") && (Boolean) gSiteConfig.get("gMASTER_SKU") && ( dataFeed.getMethodName().equals("getShortDesc") || dataFeed.getMethodName().equals("getWeight") ) ) {
            				if ( dataFeed.getMethodName().equals("getShortDesc") && (product.getShortDesc() == null || product.getShortDesc().equals( "" )) ) {
            					Product masterSKU = new Product(product.getMasterSku(), null);
            					this.webJaguar.setMasterSku( masterSKU );
            					value = masterSKU.getShortDesc();
            				}
            				if ( dataFeed.getMethodName().equals("getWeight") && (product.getWeight() == null || product.getWeight() == 0) ) {
            					Product masterSKU = new Product(product.getMasterSku(), null);
            					this.webJaguar.setMasterSku( masterSKU );
            					value = masterSKU.getWeight();
            				}
            			}
            		}
        		}
        		if (value != null) {
        			if (value.getClass() == String.class) {
        				value = ((String) value).replace("\n", " ");
        				value = ((String) value).replace("\r", " ");
        				value = ((String) value).replace("\t", " ");
        			}
        			if (value.getClass() == String.class && ((String) value).trim().equals("") && dataFeed.getDefaultValue() != null) {
            			pw.print(dataFeed.getDefaultValue());        				
        			} else {
                		pw.print(value);        				
        			}
        		} else if (dataFeed.getDefaultValue() != null) {
        			pw.print(dataFeed.getDefaultValue());
        		}
        		if(i+1 < headers.size()) {
        			pw.print( "\t");        			
                }
        	} // for datafeed
        	pw.println();
    	} // for product
    	
    	return true;
    }

    private void updateDataFeedConfig(HttpServletRequest request, List<DataFeed> dataFeedList, Map<String, Object> dataFeedConfig) {
		for (DataFeed dataFeed: dataFeedList) {
			dataFeed.setFeed( feed );
			dataFeed.setMethodName( ServletRequestUtils.getStringParameter( request, "__methodName_" + dataFeed.getFieldName(), "" ) );
			dataFeed.setEnabled( ServletRequestUtils.getBooleanParameter( request, "__enabled_" + dataFeed.getFieldName(), false ) );
			dataFeed.setDefaultValue(ServletRequestUtils.getStringParameter(request, "__defaultValue_" + dataFeed.getFieldName(), ""));
		}
		
		dataFeedConfig.put( "server", ServletRequestUtils.getStringParameter( request, "server", "" ) );
		dataFeedConfig.put( "username", ServletRequestUtils.getStringParameter( request, "username", "" ) );
		dataFeedConfig.put( "password", ServletRequestUtils.getStringParameter( request, "password", "" ) );
		dataFeedConfig.put( "filename", ServletRequestUtils.getStringParameter( request, "filename", "" ) );
		dataFeedConfig.put( "price", ServletRequestUtils.getStringParameter( request, "price", null ) );
		dataFeedConfig.put( "inventory", ServletRequestUtils.getStringParameter( request, "inventory", null ) );
		
		//temp fix for caegory id and prefix
		if(request.getParameter("prefix") != null && request.getParameter("prefix") == "") {
			dataFeedConfig.put( "prefix", null );
		} else {
			dataFeedConfig.put( "prefix", request.getParameter("prefix") );
		}
		if(request.getParameter("category") != null && request.getParameter("category") == "") {
			dataFeedConfig.put( "category", null );
		} else {
			dataFeedConfig.put( "category", request.getParameter("category") );
		}
		
		this.webJaguar.updateDataFeed( dataFeedList, dataFeedConfig );
    }
    
    private String ftpDataFeed(File file, Map<String, Object> dataFeedConfig) throws UnknownHostException, Exception {
    	StringBuffer sbuff = new StringBuffer();
    	
    	if (((String) dataFeedConfig.get( "server" )).trim().equals( "" )) {
    		throw new UnknownHostException();
    	}
    	if (((String) dataFeedConfig.get( "filename" )).trim().equals( "" )) {
    		throw new Exception();
    	}

    	// Connect and logon to FTP Server
        FTPClient ftp = new FTPClient();
        ftp.connect( InetAddress.getByName(dataFeedConfig.get( "server" ).toString()) );
        sbuff.append("Connected to " + dataFeedConfig.get( "server" ) + ".<br>");

        // login
        ftp.login( (String) dataFeedConfig.get( "username" ), (String) dataFeedConfig.get( "password" ) );
        sbuff.append(ftp.getReplyString() + "<br>");
 
        if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
            // upload file
            sbuff.append("Uploading file: " + dataFeedConfig.get("filename") + "<br>");
            FileInputStream fis = new FileInputStream(file);
            ftp.storeFile((String) dataFeedConfig.get("filename"), fis );
            sbuff.append(ftp.getReplyString() + "<br>");
            fis.close();        	
        }
        
        // Logout from the FTP Server and disconnect
        ftp.logout();
        ftp.disconnect();
        
        return sbuff.toString();
    }
    
    private DataFeedSearch getDateFeedSearch(HttpServletRequest request) {
    	DataFeedSearch search = new DataFeedSearch();
    	
    	try {
    		search.setPrice( Double.parseDouble( request.getParameter("price") ) );
    	} catch (Exception e) {
    		search.setPrice( null );
    	}
    	
    	try {
    		search.setInventory( Integer.parseInt( request.getParameter("inventory") ) );
    	} catch (Exception e) {
    		search.setInventory( null );
    	}
    	
    	return search;
    }
    
    private String getBreadCrumb(Integer catId) {
    	List<Category> breadCrumb = this.webJaguar.getCategoryTree(catId, true, null);
    	String categoryTree = "";
    	for (Category category : breadCrumb) {
    		categoryTree += category.getName() + " > " ;
    	}
    	return categoryTree.substring(0, categoryTree.lastIndexOf('>'));
    }
}
