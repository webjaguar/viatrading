package com.webjaguar.web.admin.datafeed;

import java.util.ArrayList;
import java.util.List;

public class WebjaguarCategory {

	private String cat;
	private String breadCrumb;
	private Integer catId;
	private List<WebjaguarCategory> subCat = new ArrayList<WebjaguarCategory>();
	public String getCat() {
		return cat;
	}
	public void setCat(String cat) {
		this.cat = cat;
	}
	public String getBreadCrumb() {
		return breadCrumb;
	}
	public void setBreadCrumb(String breadCrumb) {
		this.breadCrumb = breadCrumb;
	}
	public Integer getCatId() {
		return catId;
	}
	public void setCatId(Integer catId) {
		this.catId = catId;
	}
	public void setSubCat(List<WebjaguarCategory> subCat) {
		this.subCat = subCat;
	}
	public List<WebjaguarCategory> getSubCat() {
		return subCat;
	}
	
	
	
}
