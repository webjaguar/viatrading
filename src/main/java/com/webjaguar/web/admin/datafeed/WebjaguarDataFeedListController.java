/* Copyright 2011 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 08.02.2011
 */

package com.webjaguar.web.admin.datafeed;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.DataFeedSearch;
import com.webjaguar.model.WebjaguarDataFeed;


public class WebjaguarDataFeedListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		DataFeedSearch dataFeedSearch = getDateFeedSearch(request);
		Map<String, Object> myModel = new HashMap<String, Object>();
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		   
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("webjaguarDataFeedForm.jhtm") , myModel);
		}
		
		int count = this.webJaguar.getWebjaguarDataFeedListCount(dataFeedSearch);
		if (count < dataFeedSearch.getOffset()-1) {
			dataFeedSearch.setPage(1);
		}
		dataFeedSearch.setLimit(dataFeedSearch.getPageSize());
		dataFeedSearch.setOffset((dataFeedSearch.getPage()-1)*dataFeedSearch.getPageSize());
		List<WebjaguarDataFeed> webjagaurDataFeedList = this.webJaguar.getWebjaguarDataFeedList(dataFeedSearch);
		myModel.put("webjagaurDataFeedList", webjagaurDataFeedList);
		
		int pageCount = count/dataFeedSearch.getPageSize();
		if (count%dataFeedSearch.getPageSize() > 0) {
			pageCount++;
		}
		
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", dataFeedSearch.getOffset()+webjagaurDataFeedList.size());
		myModel.put("count", count);
		   

		return new ModelAndView("admin/dataFeed/list", "model", myModel);
	}
	
    private DataFeedSearch getDateFeedSearch(HttpServletRequest request) {
    	Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
    	DataFeedSearch dataFeedSearch = (DataFeedSearch) request.getSession().getAttribute( "dataFeedSearch" );
		if (dataFeedSearch == null) {
			dataFeedSearch = new DataFeedSearch();
			dataFeedSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "dataFeedSearch", dataFeedSearch );
		}
		
		try {
			dataFeedSearch.setPrice( Double.parseDouble( request.getParameter("price") ) );
    	} catch (Exception e) {
    		dataFeedSearch.setPrice( null );
    	}
    	
    	try {
    		dataFeedSearch.setInventory( Integer.parseInt( request.getParameter("inventory") ) );
    	} catch (Exception e) {
    		dataFeedSearch.setInventory( null );
    	}
    	
    	return dataFeedSearch;
    }
    

}