/* Copyright 2011 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 08.02.2011
 */

package com.webjaguar.web.admin.datafeed;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.WebjaguarDataFeedForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Category;
import com.webjaguar.model.CategoryLinkType;
import com.webjaguar.model.CategorySearch;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.DataFeed;
import com.webjaguar.model.DataFeedSearch;
import com.webjaguar.model.Option;
import com.webjaguar.model.OptionSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductOptionValue;
import com.webjaguar.model.ProductXML;
import com.webjaguar.model.Search;
import com.webjaguar.model.Supplier;
import com.webjaguar.model.WebjaguarDataFeed;

public class WebjaguarDataFeedFormController extends SimpleFormController {

	private static WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private Map<String, Configuration> siteConfig;
	
	private static JAXBContext context;
	private File productFile;
	private static String content;
	private static List<ProductField> productFields;
	private static Map <String, WebjaguarCategory> categoryMap;
	private static int gPRODUCT_FIELDS;
	List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
	 
	public WebjaguarDataFeedFormController() {
		setSessionForm(true);
		setCommandName("webjaguarDataFeedForm");
		setCommandClass(WebjaguarDataFeedForm.class);
		setFormView("admin/dataFeed/form");
		setSuccessView("webjaguarDataFeedList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
		WebjaguarDataFeedForm webjaguarDataFeedForm = (WebjaguarDataFeedForm) command;
		
		
		String fName = webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename();
		if(fName != null && !fName.isEmpty()) {
			webjaguarDataFeedForm.getWebjaguarDataFeed().setFilename(fName.split("\\.")[0]);
		}
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			webJaguar.deleteWebjaguarDataFeed(webjaguarDataFeedForm.getWebjaguarDataFeed().getId());	
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		// check if ftp button was pressed
		if (request.getParameter("_ftp") != null) {
			Map<String, Object> map = new HashMap<String, Object>();
			if(webjaguarDataFeedForm.getWebjaguarDataFeed().getFeedType().equalsIgnoreCase("premier")) {
				this.generateFeedForPremier(webjaguarDataFeedForm.getWebjaguarDataFeed(), request, map);
			} else {
				this.uploadFeed(webjaguarDataFeedForm.getWebjaguarDataFeed(), request, map);
			}
			return new ModelAndView(new RedirectView("webjaguarDataFeedForm.jhtm?id="+webjaguarDataFeedForm.getWebjaguarDataFeed().getId()) ,map);
		}
		
		if (request.getParameter("_ftpDownload") != null) {
			
			Map<String, Object> map = new HashMap<String, Object>();
			// get generated data feed file
	    	File baseFile = new File(getServletContext().getRealPath("/admin/feed/"));
	    	if (!baseFile.exists()) {
	    		baseFile.mkdir();
	    	}
			if(webjaguarDataFeedForm.getWebjaguarDataFeed().getFeedType().equalsIgnoreCase("premier")) {
	    		/******option file starts*****/
	    		try {
	    			
	    			String fileName = webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_option"+"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype();
					productFile = new File(baseFile, fileName);
			    	map.put("ftpMessage" , ftpDownloadDataFeed(productFile, webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_option" +"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype(), webjaguarDataFeedForm.getWebjaguarDataFeed(), map));
			    	parseOptionsTextFile(productFile);
			    	
				} catch (UnknownHostException e) {
					map.put("message1", "dataFeed.ftp.unknownHost");					
				} catch (Exception e) {
					e.printStackTrace();
					map.put("message1", "dataFeed.ftp.missingFileName");					
				}
	    		/******option file ends*****/
	    	
	    		
	    		/******category file starts*****/
	    		try {
					String fileName = webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_category"+"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype();
					File categoryFile = new File(baseFile, fileName);
			    	map.put("ftpMessage" , ftpDownloadDataFeed(categoryFile, webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_category" +"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype(), webjaguarDataFeedForm.getWebjaguarDataFeed(), map));
			    	System.out.println("Category Download Starts");
				    parseCategoryTextFile(categoryFile);
				    System.out.println("Category Download Completed");
				} catch (UnknownHostException e) {
					map.put("message1", "dataFeed.ftp.unknownHost");					
				} catch (Exception e) {
					e.printStackTrace();
					map.put("message1", "dataFeed.ftp.missingFileName");					
				}
	    		/******category file ends*****/

	    		/******supplier file starts*****/
	    		try {
	    			System.out.println("Supplier Download Starts");
				    String fileName = webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_supplier"+"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype();
					File supplierFile = new File(baseFile, fileName);
			    	map.put("ftpMessage" , ftpDownloadDataFeed(supplierFile, webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_supplier" +"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype(), webjaguarDataFeedForm.getWebjaguarDataFeed(), map));
					parseSupplierTextFile(supplierFile);
					System.out.println("Supplier Download Completed");
				} catch (UnknownHostException e) {
					map.put("message1", "dataFeed.ftp.unknownHost");					
				} catch (Exception e) {
					e.printStackTrace();
					map.put("message1", "dataFeed.ftp.missingFileName");					
				}
	    		/******suplier file ends*****/

	    		/******product file starts*****/
	    		try {
	    			System.out.println("Product Download Starts");
				    String fileName = webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_product"+"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype();
					productFile = new File(baseFile, fileName);
			    	map.put("ftpMessage" , ftpDownloadDataFeed(productFile, webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_product" +"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype(), webjaguarDataFeedForm.getWebjaguarDataFeed(), map));
					parseProductTextFile(productFile);
					System.out.println("Product Download Completed");
				} catch (UnknownHostException e) {
					map.put("message1", "dataFeed.ftp.unknownHost");					
				} catch (Exception e) {
					e.printStackTrace();
					map.put("message1", "dataFeed.ftp.missingFileName");					
				}
	    		/******product file ends*****/
	    		
	    		/******product supplier file starts*****/
	    		try {
	    			System.out.println("Product Supplier Download Starts");
				    String fileName = webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_product_supplier"+"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype();
					productFile = new File(baseFile, fileName);
			    	map.put("ftpMessage" , ftpDownloadDataFeed(productFile, webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_product_supplier" +"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype(), webjaguarDataFeedForm.getWebjaguarDataFeed(), map));
					parseProductSupplierTextFile(productFile);
					System.out.println("Product Supplier Download Completed");
				    
				} catch (UnknownHostException e) {
					map.put("message1", "dataFeed.ftp.unknownHost");					
				} catch (Exception e) {
					e.printStackTrace();
					map.put("message1", "dataFeed.ftp.missingFileName");					
				}
	    		/******product file ends*****/
	    		
	    		/******product fields file starts*****/
	    		try {
	    			System.out.println("Product Field Download Starts");
				    String fileName = webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_product_fields"+"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype();
					productFile = new File(baseFile, fileName);
			    	map.put("ftpMessage" , ftpDownloadDataFeed(productFile, webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_product_fields" +"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype(), webjaguarDataFeedForm.getWebjaguarDataFeed(), map));
					parseProductFieldTextFile(productFile);
					System.out.println("Product Field Download Completed");
				} catch (UnknownHostException e) {
					map.put("message1", "dataFeed.ftp.unknownHost");					
				} catch (Exception e) {
					e.printStackTrace();
					map.put("message1", "dataFeed.ftp.missingFileName");					
				}
	    		/******product file ends*****/
	    	
			} else {
				try {
					context  = JAXBContext.newInstance(Product.class);
					Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
					gPRODUCT_FIELDS = (Integer) gSiteConfig.get("gPRODUCT_FIELDS");
					productFields = webJaguar.getProductFields(null, gPRODUCT_FIELDS);
					SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
			    	String fileName = webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() + "_" + dateFormatter.format(new Date()) +"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype();
			    	productFile = new File(baseFile, fileName);
					map.put("ftpMessage" , ftpDownloadDataFeed(productFile, webjaguarDataFeedForm.getWebjaguarDataFeed().getFilename() +"."+webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype(), webjaguarDataFeedForm.getWebjaguarDataFeed(), map));
					
					if(webjaguarDataFeedForm.getWebjaguarDataFeed().getFiletype().equals("txt")) {
						parseProductTextFile(productFile);
					} else {
						parseProductXmlFile(productFile);
					}
				} catch (UnknownHostException e) {
					map.put("message1", "dataFeed.ftp.unknownHost");					
				} catch (Exception e) {
					e.printStackTrace();
					map.put("message1", "dataFeed.ftp.missingFileName");					
				}
			}

			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		if (webjaguarDataFeedForm.isNewWebjaguarDataFeed()) {
			webJaguar.insertWebjaguarDataFeed(webjaguarDataFeedForm.getWebjaguarDataFeed());
		} else {
			webJaguar.updateWebjaguarDataFeed(webjaguarDataFeedForm.getWebjaguarDataFeed());
		}
		
		if(webjaguarDataFeedForm.getWebjaguarDataFeed().getFeedType().equals("mtz")) {
			updateDataFeedConfig(request, webJaguar.getDataFeedList("myTradeZone"));
			return new ModelAndView(new RedirectView("webjaguarDataFeedForm.jhtm?feedType=mtz"));
		}
		return new ModelAndView(new RedirectView(getSuccessView()));
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null) {
			return true;			
		}
		return false;
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);		
	}	
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		WebjaguarDataFeedForm webjaguarDataFeedForm = (WebjaguarDataFeedForm) command;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "webjaguarDataFeed.token", "form.required", "Token is required.");    	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "webjaguarDataFeed.name", "form.required", "Name is required.");    	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "webjaguarDataFeed.server", "form.required", "Server is required.");    	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "webjaguarDataFeed.username", "form.required", "Username is required.");    	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "webjaguarDataFeed.filename", "form.required", "File Name is required."); 
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
	   WebjaguarDataFeedForm webjaguarDataFeedForm = new WebjaguarDataFeedForm();
	   int id = ServletRequestUtils.getIntParameter(request, "id", -1);
	   
	   if ( id != -1 ) {
		   webjaguarDataFeedForm = new WebjaguarDataFeedForm(webJaguar.getWebjaguarDataFeed(id, null, "webjaguar"));
	   }
	   
	   if(id == -1 && ServletRequestUtils.getStringParameter(request, "feedType") != null) {
		   WebjaguarDataFeed dataFeed = webJaguar.getWebjaguarDataFeed(null, null, ServletRequestUtils.getStringParameter(request, "feedType"));
		   if(dataFeed != null) {
			   webjaguarDataFeedForm = new WebjaguarDataFeedForm(dataFeed);
		   } else {
			//   webjaguarDataFeedForm.getWebjaguarDataFeed().setFeedType("mtz");
		   }
	   }
	   
	   
	   
	   siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
	   // if site config is publisher, the feed consumer is a subscriber
	   if(siteConfig.get("DATA_FEED_MEMBER_TYPE").getValue().equals("PUBLISHER")) {
		   
		   if(webjaguarDataFeedForm.isNewWebjaguarDataFeed()){
				String token = null;
				WebjaguarDataFeed dataFeed = null;
				do{
					token = UUID.randomUUID().toString();
					dataFeed = webJaguar.getWebjaguarDataFeed(null, token, "webjaguar");
				}while(dataFeed != null);
				webjaguarDataFeedForm.getWebjaguarDataFeed().setToken(token);
				
				webjaguarDataFeedForm.getWebjaguarDataFeed().setSubscriber(true);
			}
	   }
	   
	    return webjaguarDataFeedForm;
	} 
   
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
	   WebjaguarDataFeedForm webjaguarDataFeedForm = (WebjaguarDataFeedForm) command;
	   siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
	   Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
	   Map<String, Object> map = new HashMap<String, Object>();
	   // on ftp datafeed, receive message
	   if(ServletRequestUtils.getStringParameter(request, "ftpMessage") != null) {
		   map.put("ftpMessage" , ServletRequestUtils.getStringParameter(request, "ftpMessage"));
	   }
	   if(ServletRequestUtils.getStringParameter(request, "message1") != null) {
		   map.put("message1" , ServletRequestUtils.getStringParameter(request, "message1"));
	   }
	   map.put("count", webJaguar.getWebjaguarDataFeedListCount(null));
	 
	   if(webjaguarDataFeedForm.getWebjaguarDataFeed().getFeedType() != null && webjaguarDataFeedForm.getWebjaguarDataFeed().getFeedType().equals("mtz")) {
		  map.put("dataFeedList", webJaguar.getDataFeedList("myTradeZone"));
		  map.put("productFields", webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
	   }
	   
	   return map;
   }
   
	private void updateDataFeedConfig(HttpServletRequest request, List<DataFeed> dataFeedList) {
		for (DataFeed dataFeed: dataFeedList) {
			dataFeed.setFeed( "myTradeZone" );
			dataFeed.setMethodName( ServletRequestUtils.getStringParameter( request, "__methodName_" + dataFeed.getFieldName(), "" ) );
			dataFeed.setEnabled( ServletRequestUtils.getBooleanParameter( request, "__enabled_" + dataFeed.getFieldName(), false ) );
			dataFeed.setDefaultValue(ServletRequestUtils.getStringParameter(request, "__defaultValue_" + dataFeed.getFieldName(), ""));
		}
		
		webJaguar.updateDataFeed( dataFeedList, null );
    }
    
    public void uploadFeed(WebjaguarDataFeed dataFeed, HttpServletRequest request, Map<String, Object> map) {
	  
	    // get generated data feed file
   		File baseFile = new File(getServletContext().getRealPath("/admin/feed/"));
   		if (!baseFile.exists()) {
   			baseFile.mkdir();
   		}
   	
   		HashMap<String, Object> fileMap = new HashMap<String, Object>();
   		File file[] = baseFile.listFiles();
   		for (int f=0; f<file.length; f++) {
   			if (file[f].getName().startsWith(dataFeed.getFilename()) & file[f].getName().endsWith("."+dataFeed.getFiletype())) {
   				file[f].delete();
   			}
   		}
   		
   		DataFeedSearch search = getDateFeedSearch(request);
   		// set category
   		search.setCategory( dataFeed.getCategory() );
   		
   		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		if ( (Boolean) gSiteConfig.get("gMASTER_SKU") ) {
			search.setMasterSkuImage("masterSKU");
		}
		int productCount = webJaguar.webjaguarDataFeedProductListCount(search);
		int limit = 3000;
		search.setLimit(limit);
		//String imagePath = "http://www."+gSiteConfig.get("gSITE_DOMAIN").toString()+"/assets/Image/Product/detailsbig/";
		String imagePath = siteConfig.get("SITE_URL").getValue()+"assets/Image/Product/detailsbig/";
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
   	   	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
		
		String fileName = dataFeed.getFilename() + "."+dataFeed.getFiletype();
    	File dataFeedFile = new File(baseFile, fileName);
    	
    	ProductXML pxml = null;
    	PrintWriter pw = null;
    	if(dataFeed.getFiletype().equalsIgnoreCase("xml")) {
    		pxml = new ProductXML();
    		pxml.setToken(dataFeed.getToken());
    	} else if(dataFeed.getFiletype().equalsIgnoreCase("txt")){
    		try {
    			pw = new PrintWriter(new FileWriter(dataFeedFile));
    			pw.print(dataFeed.getToken()+"\n"+"|newRecord|");
    		} catch (IOException e1) { e1.printStackTrace();}
    	}
    	
		
		for (int offset=0; offset<productCount;) {
			List<Product> productList = webJaguar.getWebjaguarDataFeedProductList(search, 5, imagePath, true);
	    	
	    	if(dataFeed.getFeedType().equals("mtz")){
	    		this.updateFields(productList, webJaguar.getDataFeedList("myTradeZone"));
	    	}
	    	
	    	if(dataFeed.getFiletype().equalsIgnoreCase("xml")) {
	    		pxml.addToList(productList);
		    } else if(dataFeed.getFiletype().equalsIgnoreCase("txt") && pw != null){
		    	if(!dataFeed.getFeedType().equalsIgnoreCase("premier")) {
		    		// Generate Feed for webjaguar (besthandbag) and mytradezone
		    		this.writeTextFile(pw, productList, dataFeed.getPrefix(), siteConfig.get("FEED_DELIMITER").getValue());
				}
		    }
	    	offset = offset + limit;
	        search.setOffset(offset);
		}
		if(pw != null){
			pw.close();
		}
		if(dataFeed.getFiletype().equalsIgnoreCase("xml")) {
    		this.writeXMLFile(dataFeedFile, pxml, dataFeed);
	    } 
		fileMap.put("file"+dataFeed.getId(), dataFeedFile);
		map.put("generatedFile", fileMap);
		
		// ftp datafeed file
		if (dataFeedFile != null && dataFeedFile.exists()) {
			try {
				map.put("ftpMessage" , ftpDataFeed(dataFeedFile, dataFeed, map));
			} catch (UnknownHostException e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.unknownHost");					
			} catch (Exception e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.missingFileName");					
			}
		} else {
			map.put("message1", "dataFeed.ftp.missingDataFeed");
		}
   }

    private String getBreadCrumb(Integer catId) {
    	List<Category> breadCrumb = webJaguar.getCategoryTree(catId, true, null);
    	String categoryTree = "";
    	for (Category category : breadCrumb) {
    		categoryTree += category.getName() + "-->" ;
    	}
    	if(categoryTree.isEmpty()) {
    		return null;
    	}
    	return categoryTree.substring(0, categoryTree.lastIndexOf('-')-1);
    }
    
    private void writeXMLFile(File dataFeedFile, ProductXML pxml, WebjaguarDataFeed dataFeed) {
    	
    	for (Product product : pxml.getProduct()) {
	    	String breadCrumb = null;
	    	for(Object catId : product.getCatIds()) {
	    		breadCrumb = this.getBreadCrumb( Integer.parseInt(catId.toString()));
	    		
	    		if(breadCrumb != null) {
	    			product.getBreadCrumb().add(breadCrumb);
	    		}
	    	}
	    	
	    	if(dataFeed.getPrefix() != null && !dataFeed.getPrefix().isEmpty()) {
	    		product.setSku(dataFeed.getPrefix()+product.getSku());
	    	}
	    	
	    	// trim long desc as it creates problem
	    	if(product.getLongDesc() != null && product.getLongDesc().length() > 0){
	    		product.setLongDesc(product.getLongDesc().trim());
	    	}
	    }

		try {
			context = JAXBContext.newInstance(ProductXML.class);
			context.createMarshaller().marshal(pxml, dataFeedFile);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
    }
   
	
    private void writeTextFile(PrintWriter pw, List<Product> productList, String prefix, String delimiter) {
    	Method m = null;
		Object arglist[] = null;
		Class<Product> c = Product.class;
		for(Product product : productList){
    		if(prefix != null && !prefix.isEmpty()) {
	    		product.setSku(prefix+product.getSku());
	    	}
			pw.print(product.getName()+"\t"+product.getSku()+"\t"+product.getShortDesc()+"\t"+product.getLongDesc()+"\t");
    		pw.print(product.getWeight()+"\t"+product.getMsrp()+"\t"+product.isHideMsrp()+"\t");
    		
    		Iterator<String> itr = null;
    		if(product.getBreadCrumb() != null) {
    			itr = product.getBreadCrumb().iterator();
    		}
    		// One product can belong to max 3 categories
    		for(int i=1; i<=3; i++){
    			try {
    				pw.print(itr.next()+"\t");
    			}catch(Exception e) {
                	pw.print("" +"\t");
                }
        	}
    		pw.print(product.getPrice1()+"\t");
    		// One product can have max 5 images
    		for(int i=0; i<5; i++){
    			try {
    				pw.print(product.getImages().get(i).getImageUrl()+"\t");
    			}catch(Exception e) {
                	pw.print("" +"\t");
                }
        	}
    		// product field (Mytradezone has 30 field)
    		for(int i=1; i<=30; i++){
    			try {
    				m = c.getMethod("getField"+i);
        			pw.print((m.invoke(product, arglist) != null ? m.invoke(product, arglist) : "") +"\t");
                }catch(Exception e) {
                	pw.print("" +"\t");
                }
    		}
    		pw.print(product.getKeywords()+"\t"+product.getProtectedLevel()+"\t"+product.getPacking()+"\t"+product.isLoginRequire()+"\t");
    		pw.print(product.isHidePrice()+"\t"+product.getRecommendedList()+"\t"+product.getRecommendedListTitle()+"\t"+product.getRecommendedListDisplay()+"\t");
    		pw.print(product.getAlsoConsider()+"\t"+product.isTaxable()+"\t"+product.getOptionCode()+"\t");
    		pw.print(product.isNegInventory()+"\t"+product.isShowNegInventory()+"\t"+product.getInventoryAFS()+"\t");
    		pw.print(product.getTab1()+"\t"+product.getTab1Content()+"\t"+product.getTab2()+"\t"+product.getTab2Content()+"\t");
    		pw.print(product.getTab3()+"\t"+product.getTab3Content()+"\t"+product.getTab4()+"\t"+product.getTab4Content()+"\t");
    		pw.print(product.getManufactureName()+"\t"+product.isEnableRate()+"\t"+product.getSiteMapPriority()+"\t");
    		pw.print("\n");
    		pw.flush();
      }
    }
   
    
    private void generateFeedForPremier(WebjaguarDataFeed dataFeed, HttpServletRequest request, Map<String, Object> map) {
    	// get generated data feed file
   		File baseFile = new File(getServletContext().getRealPath("/admin/feed/"));
   		if (!baseFile.exists()) {
   			baseFile.mkdir();
   		}
   	
   		HashMap<String, Object> fileMap = new HashMap<String, Object>();
   		File file[] = baseFile.listFiles();
   		for (int f=0; f<file.length; f++) {
   			if (file[f].getName().startsWith(dataFeed.getFilename()) & file[f].getName().endsWith("."+dataFeed.getFiletype())) {
   				file[f].delete();
   			}
   		}
   		
   		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
   		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
   		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
		
		/*********product file starts***********/
   		System.out.println("Product Upload Starts");
	    String fileName = dataFeed.getFilename() + "_product" + "."+dataFeed.getFiletype();
    	File dataFeedFile = new File(baseFile, fileName);
    	
    	PrintWriter pw = null;
    	try {
    		pw = new PrintWriter(new FileWriter(dataFeedFile));
    		pw.print(dataFeed.getToken()+"\n"+"|newRecord|");
    	} catch (IOException e1) { e1.printStackTrace(); }
    	
    	DataFeedSearch search = getDateFeedSearch(request);
   		// set category
   		search.setCategory( dataFeed.getCategory() );
   		
   		int productCount = webJaguar.webjaguarDataFeedProductListCount(search);
   		//int productCount = 00;
   		
   		int limit = 200;
   		search.setLimit(limit);
   		Date d1 = new Date();
   		String imagePath = siteConfig.get("SITE_URL").getValue()+"assets/Image/Product/detailsbig/";
		for (int offset=0; offset<productCount;) {
			if(pw != null){
				
				if(offset == 0) {
					this.writeProductHeader(pw, Constants.getSpecialChar().get("premierDelimiter"));
				}
				
				List<Product> productList = webJaguar.getWebjaguarDataFeedProductList(search, 5, imagePath, false);
				// Generate Feed for premier group
	    		this.writeProductTextFileForPremier(pw, productList, dataFeed.getPrefix(), Constants.getSpecialChar().get("premierDelimiter"));
			}
	    	offset = offset + limit;
	        search.setOffset(offset);
	        System.out.println(offset+" Product Uploaded");
		}
		if(pw != null){
			pw.close();
		}
		fileMap.put("file"+dataFeed.getId(), dataFeedFile);
		map.put("generatedFile", fileMap);
		
		// ftp datafeed file
		if (dataFeedFile != null && dataFeedFile.exists()) {
			try {
				map.put("ftpMessage" , ftpDataFeed(dataFeedFile, dataFeed, map));
			} catch (UnknownHostException e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.unknownHost");					
			} catch (Exception e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.missingFileName");					
			}
		} else {
			map.put("message1", "dataFeed.ftp.missingDataFeed");
		}
		System.out.println("Product Upload Completed");
	    /*********product file ends***********/
		
		

		/*********supplier file starts***********/
		System.out.println("Supplier Upload Starts");
	    fileName = dataFeed.getFilename() + "_supplier" + "."+dataFeed.getFiletype();
    	dataFeedFile = new File(baseFile, fileName);
    	
    	try {
    		pw = new PrintWriter(new FileWriter(dataFeedFile));
    		pw.print(dataFeed.getToken()+"\n"+"|newRecord|");
    	} catch (IOException e1) { e1.printStackTrace(); }
    	
    	if(pw != null){
	    	// Generate Feed for premier groupimagePath
	       this.writeSupplierTextFileForPremier(pw, webJaguar.getSuppliers(new Search()), Constants.getSpecialChar().get("premierDelimiter") );
		}
	    if(pw != null){
			pw.close();
		}
		fileMap.put("file"+dataFeed.getId(), dataFeedFile);
		map.put("generatedFile", fileMap);
		
		// ftp datafeed file
		if (dataFeedFile != null && dataFeedFile.exists()) {
			try {
				map.put("ftpMessage" , ftpDataFeed(dataFeedFile, dataFeed, map));
			} catch (UnknownHostException e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.unknownHost");					
			} catch (Exception e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.missingFileName");					
			}
		} else {
			map.put("message1", "dataFeed.ftp.missingDataFeed");
		}
		System.out.println("Supplier Upload Completed");
	    /*********supplier file ends***********/
		
		/*********product_supplier file starts***********/
		System.out.println("Product Supplier Upload Starts");
	    fileName = dataFeed.getFilename() + "_product_supplier" + "."+dataFeed.getFiletype();
    	dataFeedFile = new File(baseFile, fileName);
    	
    	try {
    		pw = new PrintWriter(new FileWriter(dataFeedFile));
    		pw.print(dataFeed.getToken()+"\n"+"|newRecord|");
    	} catch (IOException e1) { e1.printStackTrace(); }
    	
    	if(pw != null){
	    	// Generate Feed for premier groupimagePath
	        this.writeProductSupplierTextFileForPremier(pw, Constants.getSpecialChar().get("premierDelimiter"), dataFeed.getPrefix() );
		}
	    if(pw != null){
			pw.close();
		}
		fileMap.put("file"+dataFeed.getId(), dataFeedFile);
		map.put("generatedFile", fileMap);
		
		// ftp datafeed file
		if (dataFeedFile != null && dataFeedFile.exists()) {
			try {
				map.put("ftpMessage" , ftpDataFeed(dataFeedFile, dataFeed, map));
			} catch (UnknownHostException e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.unknownHost");					
			} catch (Exception e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.missingFileName");					
			}
		} else {
			map.put("message1", "dataFeed.ftp.missingDataFeed");
		}
		System.out.println("Sipplier Upload Completed");
	    /*********supplier file ends***********/
		
		/*********category file starts***********/
		System.out.println("Category Upload Starts");
	    fileName = dataFeed.getFilename() + "_category" + "."+dataFeed.getFiletype();
    	dataFeedFile = new File(baseFile, fileName);
    	
    	try {
    		pw = new PrintWriter(new FileWriter(dataFeedFile));
    		pw.print(dataFeed.getToken()+"\n"+"|newRecord|");
    	} catch (IOException e1) { e1.printStackTrace(); }
    	
    	if(pw != null){
	    	// Generate Feed for premier
    		this.writeCategoryTextFileForPremier(pw, webJaguar.getCategoryList( new CategorySearch() ), Constants.getSpecialChar().get("premierDelimiter") );
		}
	    if(pw != null){
			pw.close();
		}
		fileMap.put("file"+dataFeed.getId(), dataFeedFile);
		map.put("generatedFile", fileMap);
		
		// ftp datafeed file
		if (dataFeedFile != null && dataFeedFile.exists()) {
			try {
				map.put("ftpMessage" , ftpDataFeed(dataFeedFile, dataFeed, map));
			} catch (UnknownHostException e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.unknownHost");					
			} catch (Exception e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.missingFileName");					
			}
		} else {
			map.put("message1", "dataFeed.ftp.missingDataFeed");
		}
		System.out.println("Category Upload Completed");
	    /*********supplier file ends***********/
		
		/*********options file starts***********/
		System.out.println("Options Upload Starts");
	    fileName = dataFeed.getFilename() + "_option" + "."+dataFeed.getFiletype();
    	dataFeedFile = new File(baseFile, fileName);
    	
    	try {
    		pw = new PrintWriter(new FileWriter(dataFeedFile));
    		pw.print(dataFeed.getToken()+"\n"+"|newRecord|");
    	} catch (IOException e1) { e1.printStackTrace(); }
    	
    	List<Option> optionsList =  webJaguar.getOptionsList(new OptionSearch());
			
	    if(pw != null){
	    	// Generate Feed for premier groupimagePath
	        this.writeOptionsTextFileForPremier(pw, optionsList, Constants.getSpecialChar().get("premierDelimiter") );
		}
	    if(pw != null){
			pw.close();
		}
		fileMap.put("file"+dataFeed.getId(), dataFeedFile);
		map.put("generatedFile", fileMap);
		
		// ftp datafeed file
		if (dataFeedFile != null && dataFeedFile.exists()) {
			try {
				map.put("ftpMessage" , ftpDataFeed(dataFeedFile, dataFeed, map));
			} catch (UnknownHostException e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.unknownHost");					
			} catch (Exception e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.missingFileName");					
			}
		} else {
			map.put("message1", "dataFeed.ftp.missingDataFeed");
		}
		System.out.println("Options Upload Completed");
	    /*********options file ends***********/
		
		
		
		/*********product fields file starts***********/
		System.out.println("Product Fields Upload Starts");
	    fileName = dataFeed.getFilename() + "_product_fields" + "."+dataFeed.getFiletype();
    	dataFeedFile = new File(baseFile, fileName);
    	
    	try {
    		pw = new PrintWriter(new FileWriter(dataFeedFile));
    		pw.print(dataFeed.getToken()+"\n"+"|newRecord|");
    	} catch (IOException e1) { e1.printStackTrace(); }
    	
    		
	    if(pw != null){
	    	// Generate Feed for premier groupimagePath
	    	this.writeProducFieldTextFileForPremier(pw, webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")),  Constants.getSpecialChar().get("premierDelimiter") );
		}
	    if(pw != null){
			pw.close();
		}
		fileMap.put("file"+dataFeed.getId(), dataFeedFile);
		map.put("generatedFile", fileMap);
		
		// ftp datafeed file
		if (dataFeedFile != null && dataFeedFile.exists()) {
			try {
				map.put("ftpMessage" , ftpDataFeed(dataFeedFile, dataFeed, map));
			} catch (UnknownHostException e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.unknownHost");					
			} catch (Exception e) {
				e.printStackTrace();
				map.put("message1", "dataFeed.ftp.missingFileName");					
			}
		} else {
			map.put("message1", "dataFeed.ftp.missingDataFeed");
		}
		System.out.println("Product Fields Upload Completed");
	    /*********product fields file ends***********/
		
		
		
	}
    private void writeProductHeader(PrintWriter pw, String delimiter){
    	/*
    	 * Create Header
    	 * 
    	 * */
    	pw.print("name"+delimiter);
    	pw.print("sku"+delimiter);
    	pw.print("masterSku"+delimiter);
    	pw.print("shortDescription"+delimiter);
    	pw.print("longDescription"+delimiter);
    	pw.print("defaultSupplier"+delimiter);
    	pw.print("manufactureName"+delimiter);
    	pw.print("keywords"+delimiter);
    	pw.print("msrp"+delimiter);
    	// prices
		for(int i=1; i<=10; i++){
			pw.print("price"+i+delimiter);
	    }
		// cost
		for(int i=1; i<=10; i++){
			pw.print("cost"+i+delimiter);
		}
		// qty break
		for(int i=1; i<=9; i++){
			pw.print("qtyBreak"+i+delimiter);
		}
		// price tables
		for(int i=1; i<=10; i++){
			pw.print("priceTable"+i+delimiter);
		}
		// price tables
		for(int i=1; i<=10; i++){
			pw.print("priceCasePack"+i+delimiter);
		}
		pw.print("quote"+delimiter);
    	pw.print("weight"+delimiter);
    	pw.print("packageLength"+delimiter);
    	pw.print("packageWidth"+delimiter);
    	pw.print("packageHeight"+delimiter);
    	pw.print("uspsMaxItemsInPackage"+delimiter);
    	pw.print("upsMaxItemsInPackage"+delimiter);
    	pw.print("loginRequire"+delimiter);
    	pw.print("hidePrice"+delimiter);
    	pw.print("hideMsrp"+delimiter);
    	pw.print("recommendedList"+delimiter);
		pw.print("recommendedListTitle"+delimiter);
		pw.print("recommendedListDisplay"+delimiter);
		pw.print("alsoConsider"+delimiter);
		pw.print("caseContent"+delimiter);
		pw.print("packing"+delimiter);
		pw.print("hideHeader"+delimiter);
		pw.print("hideTopBar"+delimiter);
		pw.print("hideLeftBar"+delimiter);
		pw.print("hideRightBar"+delimiter);
		pw.print("hideFooter"+delimiter);
		pw.print("hideBreadCrumb"+delimiter);
		pw.print("productLayout"+delimiter);
		pw.print("headTag"+delimiter);
		pw.print("htmlAddToCart"+delimiter);
		pw.print("addToList"+delimiter);
		pw.print("boxSize"+delimiter);
		pw.print("boxExtraAmount"+delimiter);
		pw.print("temperature"+delimiter);
		pw.print("compare"+delimiter);
		pw.print("imageLayout"+delimiter);
		pw.print("priceByCustomer"+delimiter);
		pw.print("taxable"+delimiter);
		pw.print("searchable"+delimiter);
		pw.print("optionCode"+delimiter);
		pw.print("inventoryAFS"+delimiter);
		pw.print("inventory"+delimiter);
		pw.print("lowInventory"+delimiter);
		pw.print("negInventory"+delimiter);
		pw.print("showNegInventory"+delimiter);
		pw.print("customShippingEnabled"+delimiter);
		pw.print("crossItemCode"+delimiter);
		pw.print("active"+delimiter);
		pw.print("tab1"+delimiter);
		pw.print("tab1Content"+delimiter);
		pw.print("tab2"+delimiter);
		pw.print("tab2Content"+delimiter);
		pw.print("tab3"+delimiter);
		pw.print("tab3Content"+delimiter);
		pw.print("tab4"+delimiter);
		pw.print("tab4Content"+delimiter);
		pw.print("tab5"+delimiter);
		pw.print("tab5Content"+delimiter);
		pw.print("tab6"+delimiter);
		pw.print("tab6Content"+delimiter);
		pw.print("tab7"+delimiter);
		pw.print("tab7Content"+delimiter);
		pw.print("tab8"+delimiter);
		pw.print("tab8Content"+delimiter);
		pw.print("tab9"+delimiter);
		pw.print("tab9Content"+delimiter);
		pw.print("tab10"+delimiter);
		pw.print("tab10Content"+delimiter);
		pw.print("minQuantity"+delimiter);
		pw.print("incrementalQuantity"+delimiter);
		pw.print("enableRate"+delimiter);
		pw.print("note"+delimiter);
		pw.print("searchRank"+delimiter);
		// One product can have max 5 images
		for(int i=1; i<=5; i++){
			pw.print("images"+i+delimiter);
		}
		// product field (ignore ASI fields)
		for(int i=1; i<=100; i++){
			if( i >33 && i < 54) {
				// DO NOT ADD ASI fields.
				continue;
			}
			pw.print("field"+i+delimiter);
        }
		pw.print("\n"+"|newRecord|");
		pw.flush();
	    
    }
    
    private void writeProductTextFileForPremier(PrintWriter pw, List<Product> productList, String prefix, String delimiter) {
    	
    	/*
    	 * Write values
    	 * 
    	 * */
    	Method m = null;
		Object arglist[] = null;
		Class<Product> c = Product.class;
		
		//Pattern to indetify decorative skus
		Pattern pattern1 = Pattern.compile("([0-9]{3})-d-", Pattern.CASE_INSENSITIVE);
		Pattern pattern2 = Pattern.compile("([0-9]{3})[a-zA-Z]", Pattern.CASE_INSENSITIVE);
		
		for(Product product : productList){
    		pw.print( product.getName() + delimiter );
    		if(prefix != null && !prefix.isEmpty() && !pattern1.matcher(product.getSku()).find() && !pattern2.matcher(product.getSku()).find() ) {
	    		pw.print( prefix+product.getSku() + delimiter);
	    		if(product.getMasterSku() != null && !product.getMasterSku().isEmpty()) {
	    			pw.print( prefix+product.getMasterSku() + delimiter);
	    		} else {
	    			pw.print(""+ delimiter);
		    	}
			} else {
				pw.print( product.getSku() + delimiter);
				if(product.getMasterSku() != null && !product.getMasterSku().isEmpty()) {
		    		pw.print( product.getMasterSku() + delimiter);
				} else {
	    			pw.print(""+ delimiter);
		    	}
			}
			pw.print( (product.getShortDesc() != null ? product.getShortDesc() : "" )+ delimiter);
			pw.print( (product.getLongDesc() != null ? product.getLongDesc() : "" )+ delimiter);
			//get default supplier by id and use account number
			Supplier supplier = webJaguar.getSupplierById(product.getDefaultSupplierId());
			pw.print( (supplier != null ? supplier.getAccountNumber() : "") + delimiter);
			pw.print( (product.getManufactureName() != null ? product.getManufactureName() : "" )+ delimiter );
			pw.print( (product.getKeywords() != null ? product.getKeywords() : "")+ delimiter );
			pw.print( (product.getMsrp() != null ? product.getMsrp() : "" )+ delimiter);
			
			// prices
    		for(int i=1; i<=10; i++){
    			try {
    				m = c.getMethod("getPrice"+i);
        			pw.print( (m.invoke(product, arglist) != null ? m.invoke(product, arglist) : "") + delimiter );
                }catch(Exception e) {
                	pw.print("" +delimiter);
                }
    		}
    		// cost
    		for(int i=1; i<=10; i++){
    			try {
    				m = c.getMethod("getCost"+i);
        			pw.print( (m.invoke(product, arglist) != null ? m.invoke(product, arglist) : "") + delimiter );
                }catch(Exception e) {
                	pw.print("" +delimiter);
                }
    		}
    		// qty break
    		for(int i=1; i<=9; i++){
    			try {
    				m = c.getMethod("getQtyBreak"+i);
        			pw.print( (m.invoke(product, arglist) != null ? m.invoke(product, arglist) : "") + delimiter );
                }catch(Exception e) {
                	pw.print("" +delimiter);
                }
    		}
    		// price tables
    		for(int i=1; i<=10; i++){
    			try {
    				m = c.getMethod("getPriceTable"+i);
        			pw.print( (m.invoke(product, arglist) != null ? m.invoke(product, arglist) : "") + delimiter );
                }catch(Exception e) {
                	pw.print("" +delimiter);
                }
    		}
    		// prices case pack
    		for(int i=1; i<=10; i++){
    			try {
    				m = c.getMethod("getPriceCasePack"+i);
        			pw.print( (m.invoke(product, arglist) != null ? m.invoke(product, arglist) : "") + delimiter );
                }catch(Exception e) {
                	pw.print("" +delimiter);
                }
    		}
    		pw.print( (product.isQuote() ? "1" : "0" )+ delimiter);
			pw.print( (product.getWeight() != null ? product.getWeight() : "0.00" )+ delimiter);
			pw.print( (product.getPackageL() != null ? product.getPackageL() : "" )+ delimiter);
			pw.print( (product.getPackageW() != null ? product.getPackageW() : "" )+ delimiter);
			pw.print( (product.getPackageH() != null ? product.getPackageH() : "" )+ delimiter);
			pw.print( (product.getUspsMaxItemsInPackage() != null ? product.getUspsMaxItemsInPackage() : "" )+ delimiter);
			pw.print( (product.getUpsMaxItemsInPackage() != null ? product.getUpsMaxItemsInPackage() : "" )+ delimiter);
			pw.print( (product.isLoginRequire() ? "1" : "0") + delimiter);
			pw.print( (product.isHidePrice() ? "1" : "0") + delimiter);
			pw.print( (product.isHideMsrp() ? "1" : "0")  + delimiter);
			pw.print( (product.getRecommendedList() != null ? product.getRecommendedList() : "" )+ delimiter );
    		pw.print( (product.getRecommendedListTitle() != null ? product.getRecommendedListTitle() : "" )+ delimiter );
    		pw.print( (product.getRecommendedListDisplay() != null ? product.getRecommendedListDisplay() : "" )+ delimiter );
    		pw.print( (product.getAlsoConsider() != null ? product.getAlsoConsider() : "" )+ delimiter );
    		pw.print( (product.getCaseContent() != null ? product.getCaseContent() : "" )+ delimiter );
    		pw.print( (product.getPacking() != null ? product.getPacking() : "" )+ delimiter );
    		pw.print( (product.isHideHeader() ? "1" : "0") + delimiter );
    		pw.print( (product.isHideTopBar() ? "1" : "0") + delimiter );
    		pw.print( (product.isHideLeftBar() ? "1" : "0") + delimiter );
    		pw.print( (product.isHideRightBar() ? "1" : "0") + delimiter );
    		pw.print( (product.isHideFooter() ? "1" : "0") + delimiter );
    		pw.print( (product.isHideBreadCrumbs() ? "1" : "0") + delimiter );
    		pw.print( (product.getProductLayout() != null ? product.getProductLayout() : "" )+ delimiter );
    		pw.print( (product.getHeadTag() != null ? product.getHeadTag() : "" )+ delimiter );
    		pw.print( (product.getHtmlAddToCart() != null ? product.getHtmlAddToCart() : "" )+ delimiter );
    		pw.print( (product.isAddToList() ? "1" : "0") + delimiter );
    		pw.print( (product.getBoxSize() != null ? product.getBoxSize() : "" )+ delimiter );
    		pw.print( (product.getBoxExtraAmt() != null ? product.getBoxExtraAmt() : "" )+ delimiter );
    		pw.print( (product.getTemperature() != null ? product.getTemperature() : "" )+ delimiter );
    		pw.print( (product.isCompare() ? "1" : "0") + delimiter );
    		pw.print( (product.getImageLayout() != null ? product.getImageLayout() : "" )+ delimiter );
    		pw.print( (product.isPriceByCustomer() ? "1" : "0") + delimiter );
    		pw.print( (product.isTaxable() ? "1" : "0") + delimiter );
    		pw.print( (product.isSearchable() ? "1" : "0") + delimiter );
    		pw.print( (product.getOptionCode() != null ? product.getOptionCode() : "" )+ delimiter );
    		pw.print( (product.getInventory() != null ? product.getInventory() : "" )+ delimiter );
    		pw.print( (product.getInventoryAFS() != null ? product.getInventoryAFS() : "" )+ delimiter );
    		pw.print( (product.getLowInventory() != null ? product.getLowInventory() : "" )+ delimiter );
    		pw.print( (product.isNegInventory() ? "1" : "0") + delimiter );
    		pw.print( (product.isShowNegInventory() ? "1" : "0") + delimiter );
    		pw.print( (product.isCustomShippingEnabled() ? "1" : "0") + delimiter );
    		if(product.getCrossItemsCode() != null) {
    			if(prefix != null && !prefix.isEmpty() && !pattern1.matcher(product.getCrossItemsCode()).find() && !pattern2.matcher(product.getCrossItemsCode()).find() ) {
    	    		pw.print( prefix+product.getCrossItemsCode() + delimiter);
    	    	} else {
    				pw.print( product.getCrossItemsCode() + delimiter);
    			}
        	} else {
        		pw.print(""+ delimiter );
        	}
    		pw.print( (product.isActive() ? "1" : "0") + delimiter );
    		pw.print( (product.getTab1() != null ? product.getTab1() : "" )+ delimiter );
    		pw.print( (product.getTab1Content() != null ? product.getTab1Content() : "" )+ delimiter );
    		pw.print( (product.getTab2() != null ? product.getTab2() : "" )+ delimiter );
    		pw.print( (product.getTab2Content() != null ? product.getTab2Content() : "" )+ delimiter );
    		pw.print( (product.getTab3() != null ? product.getTab3() : "" )+ delimiter );
    		pw.print( (product.getTab3Content() != null ? product.getTab3Content() : "" )+ delimiter );
    		pw.print( (product.getTab4() != null ? product.getTab4() : "" )+ delimiter );
    		pw.print( (product.getTab4Content() != null ? product.getTab4Content() : "" )+ delimiter );
    		pw.print( (product.getTab5() != null ? product.getTab5() : "" )+ delimiter );
    		pw.print( (product.getTab5Content() != null ? product.getTab5Content() : "" )+ delimiter );
    		pw.print( (product.getTab6() != null ? product.getTab6() : "" )+ delimiter );
    		pw.print( (product.getTab6Content() != null ? product.getTab6Content() : "" )+ delimiter );
    		pw.print( (product.getTab7() != null ? product.getTab7() : "" )+ delimiter );
    		pw.print( (product.getTab7Content() != null ? product.getTab7Content() : "" )+ delimiter );
    		pw.print( (product.getTab8() != null ? product.getTab8() : "" )+ delimiter );
    		pw.print( (product.getTab8Content() != null ? product.getTab8Content() : "" )+ delimiter );
    		pw.print( (product.getTab9() != null ? product.getTab9() : "" )+ delimiter );
    		pw.print( (product.getTab9Content() != null ? product.getTab9Content() : "" )+ delimiter );
    		pw.print( (product.getTab10() != null ? product.getTab10() : "" )+ delimiter );
    		pw.print( (product.getTab10Content() != null ? product.getTab10Content() : "" )+ delimiter );
    		
    		pw.print( (product.getMinimumQty() != null ? product.getMinimumQty() : "" )+ delimiter );
    		pw.print( (product.getIncrementalQty() != null ? product.getIncrementalQty() : "" )+ delimiter );
    		pw.print( (product.isEnableRate() ? "1" : "0") + delimiter );
    		pw.print( (product.getNote() != null ? product.getNote() : "" )+ delimiter );
    		pw.print( (product.getSearchRank() != null ? product.getSearchRank() : "10") + delimiter );
    		// One product can have max 5 images
    		
    		for(int i=0; i<5; i++){
    			try {
    				pw.print( product.getImages().get(i).getImageUrl() + delimiter );
    			}catch(Exception e) {
                	pw.print( "" + delimiter );
                }
        	}
    		// product field (ignore ASI fields)
    		for(int i=1; i<=100; i++){
    			if( i >33 && i < 54) {
    				// DO NOT ADD ASI fields.
    				continue;
    			}
    			try {
    				m = c.getMethod("getField"+i);
        			pw.print( (m.invoke(product, arglist) != null ? m.invoke(product, arglist) : "") + delimiter );
                }catch(Exception e) {
                	pw.print(" " +delimiter);
                }
    		}
    		// Since long desc may have new line, we are u
    		pw.print("\n"+"|newRecord|");
    		pw.flush();
		}
	}
   
    private void writeSupplierTextFileForPremier(PrintWriter pw, List<Supplier> supplierList, String delimiter) {
    	
    	
    	/*
    	 * Create Header
    	 * 
    	 * */
    	pw.print("accountNumber"+delimiter);
    	pw.print("company"+delimiter);
    	pw.print("firstName"+delimiter);
    	pw.print("lastName"+delimiter);
    	pw.print("addr1"+delimiter);
    	pw.print("addr2"+delimiter);
    	pw.print("city"+delimiter);
    	pw.print("state_province"+delimiter);
    	pw.print("zip"+delimiter);
    	pw.print("country"+delimiter);
    	pw.print("phone"+delimiter);
    	pw.print("fax"+delimiter);
    	pw.print("email"+delimiter);
    	pw.print("note"+delimiter);
    	pw.print("active"+delimiter);
    	pw.print("\n"+"|newRecord|");
		pw.flush();
	    
    	
    	/*
    	 * Write values
    	 * 
    	 * */
    	for(Supplier supplier : supplierList){
    		
    		// Do not transfr supplier, if it is not active
    		if(!supplier.isActive()) {
    			continue;
    		}
    		pw.print( (supplier.getAccountNumber() != null ? supplier.getAccountNumber() : "" )+ delimiter);
    		pw.print( (supplier.getAddress().getCompany() != null ? supplier.getAddress().getCompany() : "" )+ delimiter);
    		pw.print( (supplier.getAddress().getFirstName() != null ? supplier.getAddress().getFirstName() : "" )+ delimiter);
    		pw.print( (supplier.getAddress().getLastName() != null ? supplier.getAddress().getLastName() : "" )+ delimiter);
    		pw.print( (supplier.getAddress().getAddr1() != null ? supplier.getAddress().getAddr1() : "" )+ delimiter);
    		pw.print( (supplier.getAddress().getAddr2() != null ? supplier.getAddress().getAddr2() : "" )+ delimiter);
    		pw.print( (supplier.getAddress().getCity() != null ? supplier.getAddress().getCity() : "" )+ delimiter);
    		pw.print( (supplier.getAddress().getStateProvince() != null ? supplier.getAddress().getStateProvince() : "" )+ delimiter);
			pw.print( (supplier.getAddress().getZip() != null ? supplier.getAddress().getZip() : "" )+ delimiter);
    		pw.print( (supplier.getAddress().getCountry() != null ? supplier.getAddress().getCountry() : "" )+ delimiter);
    		pw.print( (supplier.getAddress().getPhone() != null ? supplier.getAddress().getPhone() : "" )+ delimiter);
			pw.print( (supplier.getAddress().getFax() != null ? supplier.getAddress().getFax() : "" )+ delimiter);
    		pw.print( (supplier.getAddress().getEmail() != null ? supplier.getAddress().getEmail() : "" )+ delimiter);
    		pw.print( (supplier.getNote() != null ? supplier.getNote() : " " )+ delimiter);
    		pw.print( supplier.isActive() + delimiter);
    		
    		pw.print("\n"+"|newRecord|");
    		pw.flush();
      }
    }
    
    private void writeProductSupplierTextFileForPremier(PrintWriter pw, String delimiter, String prefix) {
    	
    	
    	int productSupplierCount = webJaguar.getProductSupplierMapCount();
    	
   		int limit = 3000;
   		Date d1 = new Date();
   		for (int offset=0; offset<productSupplierCount;) {
			if(pw != null){
				
				if(offset == 0) {
					/*
			    	 * Create Header
			    	 * 
			    	 * */
			    	pw.print("accountNumber"+delimiter);
			    	pw.print("sku"+delimiter);
			    	pw.print("supplierSku"+delimiter);
			    	pw.print("price"+delimiter);
			    	pw.print("percent"+delimiter);
			    	pw.print("\n"+"|newRecord|");
					pw.flush();
				}
				
				/*
		    	 * Write values
		    	 * 
		    	 * */
				//Pattern to indetify decorative skus
				Pattern pattern1 = Pattern.compile("([0-9]{3})-d-", Pattern.CASE_INSENSITIVE);
				Pattern pattern2 = Pattern.compile("([0-9]{3})[a-zA-Z]", Pattern.CASE_INSENSITIVE);
				for(Map<String, Object> map : webJaguar.getProductSupplierMap(limit, offset)){
		    		
		    		pw.print( map.get("supplier_account_number")+ delimiter);
		    		if(prefix != null && !prefix.isEmpty() && !pattern1.matcher(map.get("sku").toString()).find() && !pattern2.matcher(map.get("sku").toString()).find() ) {
			    		pw.print( prefix+map.get("sku") + delimiter);
					} else {
						pw.print( map.get("sku") + delimiter);
					}
		    		pw.print( map.get("supplier_sku")+ delimiter);
		    		pw.print( (map.get("price") != null ? map.get("price") : "")+ delimiter);
		    		pw.print( (Boolean) map.get("percent") + delimiter);
		    		
		    		pw.print("\n"+"|newRecord|");
		    		pw.flush();
		    	}
			}
	    	offset = offset + limit;
   		}
    }
    
    private void writeCategoryTextFileForPremier(PrintWriter pw, List<Category> categoryList, String delimiter) {
    	
    	
    	/*
    	 * Create Header
    	 * 
    	 * */
    	pw.print("id"+delimiter);
    	pw.print("name"+delimiter);
    	pw.print("parent"+delimiter);
    	pw.print("rank"+delimiter);
    	pw.print("htmlCode"+delimiter);
    	pw.print("footerHtmlCode"+delimiter);
    	pw.print("rightBarTopHtmlCode"+delimiter);
    	pw.print("rightBarBottomHtmlCode"+delimiter);
    	pw.print("leftBarTopHtmlCode"+delimiter);
    	pw.print("leftBarBottomHtmlCode"+delimiter);
    	pw.print("protectedHtmlCode"+delimiter);
    	pw.print("url"+delimiter);
    	pw.print("urlTarget"+delimiter);
    	pw.print("redirect301"+delimiter);
    	pw.print("subcatCols"+delimiter);
    	pw.print("subcatLevels"+delimiter);
    	pw.print("subcatLocation"+delimiter);
    	pw.print("showSubCats"+delimiter);
    	pw.print("linkType"+delimiter);
    	pw.print("displayMode"+delimiter);
    	pw.print("hideHeader"+delimiter);
    	pw.print("hideTopBar"+delimiter);
    	pw.print("hideLeftBar"+delimiter);
    	pw.print("hideRightBar"+delimiter);
    	pw.print("hideFooter"+delimiter);
    	pw.print("hideBreadCrumbs"+delimiter);
    	pw.print("headTag"+delimiter);
    	pw.print("productPerPage"+delimiter);
    	pw.print("layoutId"+delimiter);
    	pw.print("catIds"+delimiter);
    	pw.print("sortBy"+delimiter);
    	pw.print("productFieldSearch"+delimiter);
    	pw.print("productFieldSearchType"+delimiter);
    	pw.print("field1"+delimiter);
    	pw.print("field2"+delimiter);
    	pw.print("field3"+delimiter);
    	pw.print("field4"+delimiter);
    	pw.print("field5"+delimiter);
    	pw.print("salesTagTitle"+delimiter);
    	pw.print("showOnSearch"+delimiter);
    	pw.print("shared"+delimiter);
    	pw.print("setType"+delimiter);
    	pw.print("sitemapPriority"+delimiter);
    	pw.print("productFieldSearchPosition"+delimiter);
    	pw.print("imageUrl"+delimiter);
    	pw.print("imageUrlAltName"+delimiter);
    	pw.print("\n"+"|newRecord|");
		pw.flush();
	    
    	
    	/*
    	 * Write values
    	 * 
    	 * */
		
		
		List<Integer> catIds = new ArrayList<Integer>();
		
    	for(Category category : webJaguar.getSiteMapCategoryList(null)) {
    		catIds.add(category.getId());
    	}
		
		Category category = null;
    	for(Integer catId : catIds){
    		category = webJaguar.getCategoryById(catId, "0");
    		
    		//Do not include category, if it is set as home page.
    		if(category.isHomePage()) {
    			continue;
    		}
    		pw.print( category.getId() + delimiter);
    		pw.print( category.getName() + delimiter);
    		pw.print( (category.getParent() != null ? category.getParent() : "" )+ delimiter);
    		pw.print( (category.getRank() != null ? category.getRank() : "" )+ delimiter);
    		pw.print( (category.getHtmlCode() != null ? category.getHtmlCode() : "" )+ delimiter);
    		pw.print( (category.getFooterHtmlCode() != null ? category.getFooterHtmlCode() : "" )+ delimiter);
    		pw.print( (category.getRightBarTopHtmlCode() != null ? category.getRightBarTopHtmlCode() : "" )+ delimiter);
    		pw.print( (category.getRightBarBottomHtmlCode() != null ? category.getRightBarBottomHtmlCode() : "" )+ delimiter);
    		pw.print( (category.getLeftBarTopHtmlCode() != null ? category.getLeftBarTopHtmlCode() : "" )+ delimiter);
			pw.print( (category.getLeftBarBottomHtmlCode() != null ? category.getLeftBarBottomHtmlCode() : "" )+ delimiter);
    		pw.print( (category.getProtectedHtmlCode() != null ? category.getProtectedHtmlCode() : "" )+ delimiter);
    		pw.print( (category.getUrl() != null ? category.getUrl() : "" )+ delimiter);
    		pw.print( (category.getUrlTarget() != null ? category.getUrlTarget() : "" )+ delimiter);
    		pw.print( (category.isRedirect301() ? "1" : "0" )+ delimiter);
    		pw.print( (category.getSubcatCols() != null ? category.getSubcatCols() : "" )+ delimiter);
    		pw.print( (category.getSubcatLevels() != null ? category.getSubcatLevels() : " " )+ delimiter);
    		pw.print( (category.getSubcatLocation() != null ? category.getSubcatLocation() : " " )+ delimiter);
    		pw.print( (category.isShowSubcats() ? "1" : "0" )+ delimiter);
    		pw.print( (category.getLinkType() != null ? category.getLinkType() : " " )+ delimiter);
    		pw.print( (category.getDisplayMode() != null ? category.getDisplayMode() : " " )+ delimiter);
    		pw.print( (category.isHideHeader() ? "1" : "0" )+ delimiter);
    		pw.print( (category.isHideTopBar() ? "1" : "0" )+ delimiter);
    		pw.print( (category.isHideLeftBar() ? "1" : "0" )+ delimiter);
    		pw.print( (category.isHideRightBar() ? "1" : "0" )+ delimiter);
    		pw.print( (category.isHideFooter() ? "1" : "0" )+ delimiter);
    		pw.print( (category.isHideBreadCrumbs() ? "1" : "0" )+ delimiter);
    		pw.print( (category.getHeadTag() != null ? category.getHeadTag() : "" )+ delimiter);
    		pw.print( (category.getProductPerPage() != null ? category.getProductPerPage() : "" )+ delimiter);
    		pw.print( category.getLayoutId() + delimiter);
    		pw.print( (category.getCategoryIds() != null ? category.getCategoryIds() : "" )+ delimiter);
    		pw.print( (category.getSortBy() != null ? category.getSortBy() : "" )+ delimiter);
    		pw.print( (category.isProductFieldSearch() ? "1" : "0" )+ delimiter);
    		pw.print( (category.getProductFieldSearchType() != null ? category.getProductFieldSearchType() : "" )+ delimiter);
    		pw.print( (category.getField1() != null ? category.getField1() : "" )+ delimiter);
    		pw.print( (category.getField2() != null ? category.getField2() : "" )+ delimiter);
    		pw.print( (category.getField3() != null ? category.getField3() : "" )+ delimiter);
    		pw.print( (category.getField4() != null ? category.getField4() : "" )+ delimiter);
    		pw.print( (category.getField5() != null ? category.getField5() : "" )+ delimiter);
    		pw.print( (category.getSalesTagTitle() != null ? category.getSalesTagTitle() : "" )+ delimiter);
			pw.print( (category.isShowOnSearch() ? "1" : "0" )+ delimiter);
			pw.print( (category.getShared() != null ? category.getShared() : "" )+ delimiter);
			pw.print( (category.getSetType() != null ? category.getSetType() : "" )+ delimiter);
			pw.print( (category.getSiteMapPriority() != null ? category.getSiteMapPriority() : "0.5" )+ delimiter);
			pw.print( (category.getProductFieldSearchPosition() != null ? category.getProductFieldSearchPosition() : "" )+ delimiter);
			
			if(category.getImageUrl() != null &&  !category.isAbsolute()) {
				category.setImageUrl(siteConfig.get( "SITE_URL" ).getValue() + "assets/Image/Category/"+category.getImageUrl());
			}
			
			pw.print( (category.getImageUrl() != null ? category.getImageUrl() : "" )+ delimiter);
			pw.print( (category.getImageUrlAltName() != null ? category.getImageUrlAltName() : "" )+ delimiter);
			pw.print("\n"+"|newRecord|");
    		pw.flush();
      }
    }
    
    
    private void writeOptionsTextFileForPremier(PrintWriter pw, List<Option> optionList, String delimiter) {
    	
    	Map<Integer, String> optionIdCodeMap = new HashMap<Integer, String>();
    	for(Option option : optionList){
    		optionIdCodeMap.put(option.getId(), option.getCode());
    	}
    	/*
    	 * Create Header
    	 * 
    	 * */
    	pw.print("optionCode");
    		/*Iterate for number of product options*/
    		pw.print("|newProductOption|");
			pw.print("optionName"+delimiter);
	    	pw.print("optionType"+delimiter);
	    	pw.print("helpText"+delimiter);
	    	pw.print("noOfColors"+delimiter);
	    	
	    		/*Iterate for number of product option values for each product option*/
		    	pw.print("|newProductOptionValue|");
				pw.print("valueName"+delimiter);
				pw.print("valueIndex"+delimiter);
		    	pw.print("optionPrice"+delimiter);
		    	pw.print("oneTimePrice"+delimiter);
		    	pw.print("optionWeight"+delimiter);
		    	pw.print("imageUrl"+delimiter);
		    	pw.print("dependingOptionIds"+delimiter);
		    	pw.print("description"+delimiter);
		    	pw.print("assignedProductSku"+delimiter);
		    	pw.print("assignedSkuAtachment"+delimiter);
		    	pw.print("includedProducts"+delimiter);
	    
		pw.print("\n"+"|newRecord|");
		pw.flush();
	    
    	
    	/*
    	 * Write values
    	 * 
    	 * */
    	for(Option option : optionList){
    		if(option.getCode() == null || option.getCode().trim().isEmpty()) {
    			continue;
    		}
    		pw.print( option.getCode());
    		List<ProductOption> optionsList = webJaguar.getProductOptionsByOptionCode(option.getCode().trim(), true, null,null);
    		if(optionsList == null || optionsList.isEmpty()) {
    			continue;
    		}
    		for(ProductOption productOption : optionsList) {
    			if(productOption.getName() == null || productOption.getName().trim().isEmpty()) {
        			continue;
        		}
    			pw.print("|newProductOption|");
    			pw.print( productOption.getName() + delimiter);
    			pw.print( (productOption.getType() != null ? productOption.getType() : "reg")+ delimiter);
    			pw.print( (productOption.getHelpText() != null ? productOption.getHelpText() : " ") + delimiter);
    			pw.print( productOption.getNumberOfColors() + delimiter);
    			
    			int index = 0;
    			for(ProductOptionValue productOptionValue : productOption.getValues()) {
    				if(productOptionValue.getName() == null || productOptionValue.getName().trim().isEmpty()) {
            			continue;
            		}
        			pw.print("|newProductOptionValue|");
        			pw.print( productOptionValue.getName() + delimiter);
        			pw.print( index + delimiter);
        			pw.print( (productOptionValue.getOptionPrice() != null ? productOptionValue.getOptionPrice() : " ")+ delimiter);
        			pw.print( (productOptionValue.isOneTimePrice() ? "1" : "0") + delimiter);
        			pw.print( (productOptionValue.getOptionWeight() != null ? productOptionValue.getOptionWeight() : " ")+ delimiter);
        			pw.print( (productOptionValue.getImageUrl() != null ? productOptionValue.getImageUrl() : " ")+ delimiter);
        			
        			StringBuffer sb = new StringBuffer();
    				if(productOptionValue.getDependingOptionIds() != null) {
        				String ids[] = productOptionValue.getDependingOptionIds().split(",");
        				for(String id : ids) {
        					try {
        						//Option tempOption = webJaguar.getOption(Integer.parseInt(id), null);
        						sb.append(optionIdCodeMap.get(Integer.parseInt(id))+",");
        					}catch(Exception e) { }
        				}
        			}
    				pw.print( (sb.toString().trim().isEmpty()  ?  " " : sb.toString().trim())+ delimiter);
            		
        			pw.print( (productOptionValue.getDescription() != null ? productOptionValue.getDescription() : " ")+ delimiter);
        			pw.print( (productOptionValue.getAssignedProductSku() != null ? productOptionValue.getAssignedProductSku() : " ")+ delimiter);
        			pw.print( (productOptionValue.isAssignedSkuAttachment() ? "1" : "0")+ delimiter);
        			pw.print( (productOptionValue.getIncludedProducts() != null ? productOptionValue.getIncludedProducts() : " ")+ delimiter);
        			index++;
    			}
            }
    		pw.print("\n"+"|newRecord|");
    		pw.flush();
    	}
    	optionIdCodeMap.clear();
    }

    private void writeProducFieldTextFileForPremier(PrintWriter pw, List<ProductField> productFields, String delimiter) {
    	
    	
    	/*
    	 * Create Header
    	 * 
    	 * */
    	pw.print("fieldId"+delimiter);
    	pw.print("rank"+delimiter);
		pw.print("name"+delimiter);
	    pw.print("enabled"+delimiter);
	    pw.print("quickModeField"+delimiter);
	    pw.print("quickMode2Field"+delimiter);
	    pw.print("preValue"+delimiter);
		pw.print("comparisonField"+delimiter);
		pw.print("showOnInvoice"+delimiter);
		pw.print("showOnInvoiceBackEnd"+delimiter);
		pw.print("showOnInvoiceExport"+delimiter);
		pw.print("search"+delimiter);
		pw.print("search2"+delimiter);
		pw.print("serviceField"+delimiter);
		pw.print("detailsField"+delimiter);
		pw.print("packingField"+delimiter);
		pw.print("productExport"+delimiter);
		pw.print("type"+delimiter);
	    pw.print("showOnMyList"+delimiter);
		pw.print("showOnDropDown"+delimiter);
		
		pw.print("\n"+"|newRecord|");
		pw.flush();
	    
    	
    	/*
    	 * Write values
    	 * 
    	 * */
		for(ProductField productField : productFields){
			//For ASI fields
			if(productField.getId() > 33 && productField.getId() < 54) {
    			continue;
    		}
			//If name of the field is empty or null, than continue
			if(productField.getName() == null || productField.getName().trim().isEmpty()) {
    			continue;
    		}
    		pw.print(productField.getId() + delimiter);
    		pw.print(productField.getRank() + delimiter);
    		pw.print(productField.getName() + delimiter);
    		pw.print((productField.isEnabled() ? "1" : "0") + delimiter);
    		pw.print((productField.isQuickModeField() ? "1" : "0")+ delimiter);
    		pw.print((productField.isQuickMode2Field() ? "1" : "0")+ delimiter);
    		pw.print(productField.getPreValue() + delimiter);
    		pw.print((productField.isComparisonField() ? "1" : "0")+ delimiter);
    		pw.print((productField.isShowOnInvoice() ? "1" : "0")+ delimiter);
    		pw.print((productField.isShowOnInvoiceBackend() ? "1" : "0")+ delimiter);
    		pw.print((productField.isShowOnInvoiceExport() ? "1" : "0")+ delimiter);
    		pw.print((productField.isSearch() ? "1" : "0")+ delimiter);
    		pw.print((productField.isSearch2() ? "1" : "0")+ delimiter);
    		pw.print((productField.isServiceField() ? "1" : "0")+ delimiter);
    		pw.print((productField.isDetailsField() ? "1" : "0")+ delimiter);
    		pw.print((productField.isPackingField() ? "1" : "0")+ delimiter);
    		pw.print((productField.isProductExport() ? "1" : "0")+ delimiter);
    		pw.print(productField.getFieldType() + delimiter);
    		pw.print((productField.isShowOnMyList() ? "1" : "0")+ delimiter);
    		pw.print((productField.isShowOnDropDown() ? "1" : "0")+ delimiter);
    		
           pw.print("\n"+"|newRecord|");
    	   pw.flush();
    	}
    }

    private void updateFields(List<Product> productList, List<DataFeed> dataFeedList){
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		Class<String> parameterType = String.class;
		Set<Integer> updatedFields = new HashSet<Integer>();
		Map<String, Object> oldValues;
		for (Product product: productList) {
			oldValues = new HashMap<String, Object>();
			for (DataFeed dataFeed: dataFeedList) {
        		Object value = null;
        		Object oldValue = null;
        		if (dataFeed.getMethodName().startsWith( "getField" ) && dataFeed.isEnabled()) {
        			// check if product field is enabled or valid
            		try {
            			
            			if(oldValues.get(dataFeed.getMethodName()) != null) {
            				value = oldValues.get(dataFeed.getMethodName());
                		} else {
            				m = c.getMethod(dataFeed.getMethodName());
                			value = (Object) m.invoke(product, arglist);
                		}
            			
            			m = c.getMethod("getField"+Constants.getMyTradeZoneFields().get(dataFeed.getFieldName()));
            			oldValue = (Object) m.invoke(product, arglist);
            			oldValues.put("getField"+Constants.getMyTradeZoneFields().get(dataFeed.getFieldName()), oldValue);
            			
            			m = c.getMethod("setField"+Constants.getMyTradeZoneFields().get(dataFeed.getFieldName()), parameterType);
            			m.invoke(product, value);
            			
            			updatedFields.add(Constants.getMyTradeZoneFields().get(dataFeed.getFieldName()));
            		} catch (Exception e) { e.printStackTrace();
            			// do nothing
            		}
            	} 
        	}
			
			//set values to null for fields not updated
			for(int i=1; i<=100; i++) {
				if(!updatedFields.contains(i)) {
					try {
						m = c.getMethod("setField"+i, parameterType);
						m.invoke(product, (Object) null);
					} catch (Exception e) {  }
        			
				}
			}
    	}
	}
	
	
	private DataFeedSearch getDateFeedSearch(HttpServletRequest request) {
   		DataFeedSearch search = new DataFeedSearch();
   	
   		try {
   			search.setPrice( Double.parseDouble( request.getParameter("price") ) );
   		} catch (Exception e) {
   			search.setPrice( null );
   		}
   	
   		try {
   			search.setInventory( Integer.parseInt( request.getParameter("inventory") ) );
   		} catch (Exception e) {
   			search.setInventory( null );
   		}
   	
   		return search;
   }
   
	private String ftpDataFeed(File file, WebjaguarDataFeed webjaguarDataFeed, Map<String, Object> map) throws UnknownHostException, Exception {
   	   
   		StringBuffer sbuff = new StringBuffer();
   		if (webjaguarDataFeed.getServer().trim().equals( "" )) {
   			throw new UnknownHostException();
   		}
   	   
   		if (webjaguarDataFeed.getFilename().trim().equals( "" )) {
   			throw new Exception();
   		}
   	   
   		// Connect and logon to FTP Server
   		FTPClient ftp = new FTPClient();
   		ftp.connect( webjaguarDataFeed.getServer() );
       
   		// login
   		ftp.login( webjaguarDataFeed.getUsername(), webjaguarDataFeed.getPassword() );
   		
   		if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
       	   // upload file
		   FileInputStream fis = new FileInputStream(file);
           ftp.storeFile(file.getName(), fis );
           sbuff.append("Uploading file: " + webjaguarDataFeed.getFilename() + " compeleted.");
           map.put("message1", "dataFeed.generate.success");
           fis.close();   
        } else {
        //	map.put("message1", "dataFeed.generate.failure");
        }
		 
   		// Logout from the FTP Server and disconnect
   		ftp.logout();
   		ftp.disconnect();
       
   		return sbuff.toString();
   	}
   
	private String ftpDownloadDataFeed(File file, String sourcefileName, WebjaguarDataFeed webjaguarDatafeed, Map<String, Object> map) throws UnknownHostException, Exception {
   	
	   
	   StringBuffer sbuff = new StringBuffer();
	   if (webjaguarDatafeed.getServer().trim().equals( "" )) {
   			throw new UnknownHostException();
   		}
   		if (webjaguarDatafeed.getFilename().trim().equals( "" )) {
   			throw new Exception();
   		}
   	
   		// Connect and logon to FTP Server
   		FTPClient ftp = new FTPClient();
   		ftp.connect( webjaguarDatafeed.getServer() );
   		
   		// login
   		ftp.login( webjaguarDatafeed.getUsername(), webjaguarDatafeed.getPassword() );
   		
   		if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
   			// download file
   			FileOutputStream fos = new FileOutputStream(file);
   			ftp.retrieveFile(sourcefileName, fos);
   			sbuff.append("Downloading file: " + webjaguarDatafeed.getFilename() + " Compeleted.");
   			fos.close();        	
   		}
       
   		// Logout from the FTP Server and disconnect
   		ftp.logout();
   		ftp.disconnect();
       
   		return sbuff.toString();
   }
   
	public void parseProductTextFile(File text_file) {
		try {
			
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
			StringBuffer sbuff = new StringBuffer();
			Date start = new Date();
			
			HashMap<String, Object> map = null;
 			BufferedReader bReader = new BufferedReader( new FileReader(text_file));
 			StringBuilder sb = new StringBuilder();
 			String line;
 			while ((line = bReader.readLine()) != null) {
 				sb.append(line);
 			}
 			bReader.close();	
 			
 			// Mark all products as old products 
 			webJaguar.markAndInactiveOldProducts("Premier", null, false);
			
 			
 			/**
 			  * Splitting the content of |newProduct| separated line
 			  */
 			String records[] = sb.toString().split("\\|newRecord\\|");
 			for(int l=0; l< records.length; l++) {
				
				if(l < 2 || records[l].trim().isEmpty()) {
					continue;
				}
				
				String datavalue[] = records[l].split("&\\|\\|&");
				
				if(datavalue[0] != null && !datavalue[0].isEmpty()) {
 			    	map = new HashMap<String, Object>();
 				
 					try {
 						map.put("name", datavalue[0] );
 					} catch (Exception e) { map.put("name", null); }
 					try {
 						map.put("sku", datavalue[1]);
 					} catch (Exception e) { map.put("sku", null); }
 					try {
 						map.put("master_sku", datavalue[2] );
 					} catch (Exception e) { map.put("master_sku", null); }
 					try {
 						map.put("short_desc", datavalue[3] );
 					} catch (Exception e) { map.put("short_desc", null); }
 					try {
 						map.put("long_desc", datavalue[4] );
 					} catch (Exception e) { map.put("long_desc", null); }
 					try {
 						Supplier supplier = webJaguar.getSupplierByAccountNumber(datavalue[5]);
 						map.put("default_supplier_id", supplier.getId() );
 					} catch (Exception e) { map.put("default_supplier_id", 0); }
 					try {
 						map.put("manufacture_name", datavalue[6] );
 					} catch (Exception e) { map.put("manufacture_name", null); }
 					try {
 						map.put("keywords", datavalue[7] );
 					} catch (Exception e) { map.put("keywords", null); }
 					try {
 						map.put("msrp", Double.parseDouble(datavalue[8]) );
 					} catch (Exception e) { map.put("msrp", null); }
 					
 					for(int i=1; i<=10; i++){
 						try {
 	 						map.put("price_"+i, Double.parseDouble(datavalue[8+i]) );
 	 					} catch (Exception e) { map.put("price_"+i, null); }
 	 				}
 					
 					for(int i=1; i<=10; i++){
 						try {
 	 						map.put("cost_"+i, Double.parseDouble(datavalue[18+i]) );
 	 					} catch (Exception e) { map.put("cost_"+i, null); }
 	 				}
 					
 					for(int i=1; i<=9; i++){
 						try {
 	 						map.put("qty_break_"+i, Integer.parseInt(datavalue[28+i]) );
 	 					} catch (Exception e) { map.put("qty_break_"+i, null); }
 	 				}
 					
 					for(int i=1; i<=10; i++){
 						try {
 	 						map.put("price_table_"+i, Double.parseDouble(datavalue[37+i]) );
 	 					} catch (Exception e) { map.put("price_table_"+i, null); }
 	 				}
 					
 					for(int i=1; i<=10; i++){
 						try {
 	 						map.put("price_case_pack_"+i, Double.parseDouble(datavalue[47+i]) );
 	 					} catch (Exception e) { map.put("price_case_pack_"+i, null); }
 	 				}
 					
 					try {
 						map.put("quote", datavalue[58] );
 					} catch (Exception e) { map.put("quote", 0); }
 					try {
 						map.put("weight", Double.parseDouble(datavalue[59]) );
 					} catch (Exception e) { map.put("weight", 0.0); }
 					try {
 						map.put("package_length", Double.parseDouble(datavalue[60]) );
 					} catch (Exception e) { map.put("package_length", null); }
 					try {
 						map.put("package_width", Double.parseDouble(datavalue[61]) );
 					} catch (Exception e) { map.put("package_width", null); }
 					try {
 						map.put("package_height", Double.parseDouble(datavalue[62]) );
 					} catch (Exception e) { map.put("quote", null); }
 					try {
 						map.put("usps_max_items_in_package", Double.parseDouble(datavalue[63]) );
 					} catch (Exception e) { map.put("usps_max_items_in_package", null); }
 					try {
 						map.put("ups_max_items_in_package", Double.parseDouble(datavalue[64]) );
 					} catch (Exception e) { map.put("ups_max_items_in_package", null); }
 					try {
 						map.put("login_require", Integer.parseInt( datavalue[65] ));
 					} catch (Exception e) { map.put("login_require", 0); }
 					try {
 						map.put("hide_price", Integer.parseInt( datavalue[66] ));
 					} catch (Exception e) { map.put("hide_price", 0); }
 					try {
 						map.put("hide_msrp", Integer.parseInt( datavalue[67] ));
 					} catch (Exception e) { map.put("hide_msrp", 0); }
 					try {
 						map.put("recommended_list", datavalue[68]);
 					} catch (Exception e) { map.put("recommendedList", null); }
 					try {
 						map.put("recommended_list_title", datavalue[69]);
 					} catch (Exception e) { map.put("recommended_list_title", null); }
 					try {
 						map.put("recommended_list_display", datavalue[70]);
 					} catch (Exception e) { map.put("recommended_list_display", null); }
 					try {
 						map.put("also_consider", datavalue[71]);
 					} catch (Exception e) { map.put("also_consider", null); }
 					try {
 						map.put("case_content", Integer.parseInt( datavalue[72] ));
 					} catch (Exception e) { map.put("case_content", null); }
 					try {
 						map.put("packing", datavalue[73]);
 					} catch (Exception e) { map.put("packing", null); }
 					try {
 						map.put("hide_header", Integer.parseInt( datavalue[74] ));
 					} catch (Exception e) { map.put("hide_header", 0); }
 					try {
 						map.put("hide_topbar", Integer.parseInt( datavalue[75] ));
 					} catch (Exception e) { map.put("hide_topbar", 0); }
 					try {
 						map.put("hide_leftbar", Integer.parseInt( datavalue[76] ));
 					} catch (Exception e) { map.put("hide_leftbar", 0); }
 					try {
 						map.put("hide_rightbar", Integer.parseInt( datavalue[77] ));
 					} catch (Exception e) { map.put("hide_rightbar", 0); }
 					try {
 						map.put("hide_footer", Integer.parseInt( datavalue[78] ));
 					} catch (Exception e) { map.put("hide_footer", 0); }
 					try {
 						map.put("hide_breadcrumbs", Integer.parseInt( datavalue[79] ));
 					} catch (Exception e) { map.put("hide_breadcrumbs", 0); }
 					try {
 						map.put("product_layout", datavalue[80]);
 					} catch (Exception e) { map.put("product_layout", ""); }
 					try {
 						map.put("head_tag", ((datavalue[81] != null && !datavalue[81].trim().isEmpty()) ? datavalue[81] : null ));
 					} catch (Exception e) { map.put("head_tag", null); }
 					try {
 						map.put("html_add_to_cart", ((datavalue[82] != null && !datavalue[82].trim().isEmpty()) ? datavalue[82] : null));
 					} catch (Exception e) { map.put("html_add_to_cart", null); }
 					try {
 						map.put("add_to_list", Integer.parseInt( datavalue[83] ));
 					} catch (Exception e) { map.put("add_to_list", 0); }
 					try {
 						map.put("box_size", Integer.parseInt( datavalue[84] ));
 					} catch (Exception e) { map.put("box_size", null); }
 					try {
 						map.put("box_extra_amt", Integer.parseInt( datavalue[85] ));
 					} catch (Exception e) { map.put("box_extra_amt", null); }
 					try {
 						map.put("temperature", Integer.parseInt( datavalue[86] ));
 					} catch (Exception e) { map.put("temperature", null); }
 					try {
 						map.put("compare", Integer.parseInt( datavalue[87] ));
 					} catch (Exception e) { map.put("compare", 0); }
 					try {
 						map.put("image_layout", Integer.parseInt( datavalue[88] ));
 					} catch (Exception e) { map.put("image_layout", null); }
 					try {
 						map.put("price_by_customer", Integer.parseInt( datavalue[89] ));
 					} catch (Exception e) { map.put("price_by_customer", 0); }
 					try {
 						map.put("taxable", Integer.parseInt( datavalue[90] ));
 					} catch (Exception e) { map.put("taxable", 0); }
 					try {
 						map.put("searchable", Integer.parseInt( datavalue[91] ));
 					} catch (Exception e) { map.put("searchable", 0); }
 					try {
 						map.put("option_code", datavalue[92]);
 					} catch (Exception e) { map.put("option_code", null); }
 					try {
 						map.put("inventory", Integer.parseInt( datavalue[93] ));
 					} catch (Exception e) { map.put("inventory", null); }
 					try {
 						map.put("inventory_afs", Integer.parseInt( datavalue[94] ));
 					} catch (Exception e) { map.put("inventory_afs", null); }
 					try {
 						map.put("low_inventory", Integer.parseInt( datavalue[95] ));
 					} catch (Exception e) { map.put("low_inventory", null); }
 					try {
 						map.put("neg_inventory", Integer.parseInt( datavalue[96] ));
 					} catch (Exception e) { map.put("neg_inventory", 0); }
 					try {
 						map.put("show_neg_inventory", Integer.parseInt( datavalue[97] ));
 					} catch (Exception e) { map.put("show_neg_inventory", 0); }
 					try {
 						map.put("custom_shipping_enabled", Integer.parseInt( datavalue[98] ));
 					} catch (Exception e) { map.put("custom_shipping_enabled", 0); }
 					try {
 						map.put("cross_items_code", datavalue[99]);
 					} catch (Exception e) { map.put("cross_items_code", null); }
 					try {
 						map.put("active", Integer.parseInt(datavalue[100]));
 					} catch (Exception e) { map.put("active", 1); }
 					try {
 						map.put("tab_1", ((datavalue[101] != null && !datavalue[101].trim().isEmpty()) ? datavalue[101] : null));
 					} catch (Exception e) { map.put("tab_1", null); }
 					try {
 						map.put("tab_1_content", ((datavalue[102] != null && !datavalue[102].trim().isEmpty()) ? datavalue[102] : null));
 					} catch (Exception e) { map.put("tab_1_content", null); }
 					try {
 						map.put("tab_2", ((datavalue[103] != null && !datavalue[103].trim().isEmpty()) ? datavalue[103] : null));
 					} catch (Exception e) { map.put("tab_2", null); }
 					try {
 						map.put("tab_2_content", ((datavalue[104] != null && !datavalue[104].trim().isEmpty()) ? datavalue[104] : null));
 					} catch (Exception e) { map.put("tab_2_content", null); }
 					try {
 						map.put("tab_3", ((datavalue[105] != null && !datavalue[105].trim().isEmpty()) ? datavalue[105] : null));
 					} catch (Exception e) { map.put("tab_3", null); }
 					try {
 						map.put("tab_3_content", ((datavalue[106] != null && !datavalue[106].trim().isEmpty()) ? datavalue[106] : null));
 					} catch (Exception e) { map.put("tab_3_content", null); }
 					try {
 						map.put("tab_4", ((datavalue[107] != null && !datavalue[107].trim().isEmpty()) ? datavalue[107] : null));
 					} catch (Exception e) { map.put("tab_4", null); }
 					try {
 						map.put("tab_4_content", ((datavalue[108] != null && !datavalue[108].trim().isEmpty()) ? datavalue[108] : null));
 					} catch (Exception e) { map.put("tab_4_content", null); }
 					try {
 						map.put("tab_5", ((datavalue[109] != null && !datavalue[109].trim().isEmpty()) ? datavalue[109] : null));
 					} catch (Exception e) { map.put("tab_5", null); }
 					try {
 						map.put("tab_5_content", ((datavalue[110] != null && !datavalue[110].trim().isEmpty()) ? datavalue[110] : null));
 					} catch (Exception e) { map.put("tab_5_content", null); }
 					try {
 						map.put("tab_6", ((datavalue[111] != null && !datavalue[111].trim().isEmpty()) ? datavalue[111] : null));
 					} catch (Exception e) { map.put("tab_6", null); }
 					try {
 						map.put("tab_6_content", ((datavalue[112] != null && !datavalue[112].trim().isEmpty()) ? datavalue[112] : null));
 					} catch (Exception e) { map.put("tab_6_content", null); }
 					try {
 						map.put("tab_7", ((datavalue[113] != null && !datavalue[113].trim().isEmpty()) ? datavalue[113] : null));
 					} catch (Exception e) { map.put("tab_7", null); }
 					try {
 						map.put("tab_7_content", ((datavalue[114] != null && !datavalue[114].trim().isEmpty()) ? datavalue[114] : null));
 					} catch (Exception e) { map.put("tab_7_content", null); }
 					try {
 						map.put("tab_8", ((datavalue[115] != null && !datavalue[115].trim().isEmpty()) ? datavalue[115] : null));
 					} catch (Exception e) { map.put("tab_8", null); }
 					try {
 						map.put("tab_8_content", ((datavalue[116] != null && !datavalue[116].trim().isEmpty()) ? datavalue[116] : null));
 					} catch (Exception e) { map.put("tab_8_content", null); }
 					try {
 						map.put("tab_9", ((datavalue[117] != null && !datavalue[117].trim().isEmpty()) ? datavalue[117] : null));
 					} catch (Exception e) { map.put("tab_9", null); }
 					try {
 						map.put("tab_9_content", ((datavalue[118] != null && !datavalue[118].trim().isEmpty()) ? datavalue[118] : null));
 					} catch (Exception e) { map.put("tab_9_content", null); }
 					try {
 						map.put("tab_10", ((datavalue[119] != null && !datavalue[119].trim().isEmpty()) ? datavalue[119] : null));
 					} catch (Exception e) { map.put("tab_10", null); }
 					try {
 						map.put("tab_10_content",((datavalue[120] != null && !datavalue[120].trim().isEmpty()) ? datavalue[120] : null));
 					} catch (Exception e) { map.put("tab_10_content", null); }
 					try {
 						map.put("minimum_qty", Integer.parseInt(datavalue[121]));
 					} catch (Exception e) { map.put("minimum_qty", null); }
 					try {
 						map.put("incremental_qty", Integer.parseInt(datavalue[122]));
 					} catch (Exception e) { map.put("incremental_qty", null); }
 					try {
 						map.put("enable_rate", Integer.parseInt(datavalue[123]));
 					} catch (Exception e) { map.put("enable_rate", 0); }
 					try {
 						map.put("note",  ((datavalue[124] != null && !datavalue[124].trim().isEmpty()) ? datavalue[124] : null));
 					} catch (Exception e) { map.put("note", 0); }
 					try {
 						map.put("search_rank", Integer.parseInt(datavalue[125]));
 					} catch (Exception e) { map.put("search_rank", 10); }
 					
 					for(int i=1; i<=5; i++){
 						try {
 	 						map.put("image"+i, datavalue[125+i] );
 	 					} catch (Exception e) { map.put("image"+i, null); }
 	 				}
 					for(int i=1, j=1; i<=100; i++){
 						if( i >33 && i < 54) {
 							// DO NOT ADD ASI fields.
 							continue;
 						}
 						try {
 	 						map.put("field_"+i, (datavalue[130+j] != null && !datavalue[130+j].trim().isEmpty()) ? datavalue[130+j] : null );
 	 					} catch (Exception e) { map.put("field_"+i, null); }
 						j++;
 	 				}
 					
 					map.put("feed", "premier");				
					map.put("feed_new", true);
					data.add(map);
 				}
				if (data.size() == 3000) {
					// products
					webJaguar.nonTransactionSafeUpdatePremierProduct(getCsvFeedList(), data);
				    //updateProduct(data);
					data.clear();
					map.clear();
				}
			}
 			    
 			// update
 			if (!data.isEmpty()) {
 				webJaguar.nonTransactionSafeUpdatePremierProduct(getCsvFeedList(), data);
		 	    //updateProduct(data);
				data.clear();
				map.clear();
			}
 			
 			// Mark all products as inactive where feed_new is false
 			webJaguar.markAndInactiveOldProducts("Premier", null, true);
			
 			Date end = new Date();
 			sbuff.append("Started : " + dateFormatter.format(start) + "\n");
 			sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
 			sbuff.append("Downloaded Products." + "\n\n");
 		//	notifyAdmin("Successful downloadProducts() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());

 		} catch (Exception e) {
 		//	notifyAdmin("CdsAndDvds Datafeed downloadProducts() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
	    }
	}
	
	public void parseSupplierTextFile(File text_file) {
		try {
			
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
			Date start = new Date();
			BufferedReader bReader = new BufferedReader( new FileReader(text_file));
 			StringBuilder sb = new StringBuilder();
 			String line;
 			while ((line = bReader.readLine()) != null) {
 				sb.append(line);
 			}
 			bReader.close();
 			
 			Supplier supplier  = null;
			Address supplierAddress = null;
			
			/**
 			  * Splitting the content of |newProduct| separated line
 			  */
 			String records[] = sb.toString().split("\\|newRecord\\|");
 			for(int l=0; l< records.length; l++) {
				
				if(l < 2 || records[l].trim().isEmpty()) {
					continue;
				}
				
				String datavalue[] = records[l].split("&\\|\\|&");
				if(datavalue[0] == null || datavalue[0].isEmpty() || datavalue[1] == null || datavalue[1].isEmpty()) {
					continue;
				}
				
				supplierAddress = new Address();
				supplier = new Supplier();
				
				try {
 					supplier.setAccountNumber(datavalue[0]);
 	 	 		} catch (Exception e) { 
 	 	 			continue;
 				}
 				
				try {
 					supplierAddress.setCompany(datavalue[1]);
 	 	 	 	} catch (Exception e) {
 	 	 	 		continue;
 				}
 					
				try {
 					supplierAddress.setFirstName(datavalue[2]);
 				} catch (Exception e) { 
 	 	 			supplierAddress.setFirstName(null);
 	 	 		}
 					
 				try {
 					supplierAddress.setLastName(datavalue[3]);
 	 	 	 	} catch (Exception e) { 
 	 	 	 		supplierAddress.setLastName(null);
 	 	 	 	}
 					
 				try {
 					supplierAddress.setAddr1(datavalue[4]);
 	 	 	 	} catch (Exception e) {
 					supplierAddress.setAddr1(null);
 	 	 	 	}
 				
 				try {
 	 				supplierAddress.setAddr2(datavalue[5]);
 	 	 	 	} catch (Exception e) {
 	 				supplierAddress.setAddr2(null);
 	 	 	 	}
 				
 				try {
 	 				supplierAddress.setCity(datavalue[6]);
 	 	 	 	} catch (Exception e) { 
 	 				supplierAddress.setCity(null);
 	 	 	 	}
 				
 				try {
 	 				supplierAddress.setStateProvince(datavalue[7]);
 	 	 	 	} catch (Exception e) { 
 	 				supplierAddress.setStateProvince(null);
 	 	 	 	}
 				
 				try {
 	 				supplierAddress.setZip(datavalue[8]);
 	 	 	 	} catch (Exception e) { 
 	 				supplierAddress.setZip(null);
 	 	 	 	}
 				
 				try {
 	 				supplierAddress.setCountry(datavalue[9]);
 	 	 	 	} catch (Exception e) { 
 	 				supplierAddress.setCountry(null);
 	 	 	 	}
 				
 				try {
 	 				supplierAddress.setPhone(datavalue[10]);
 	 	 	 	} catch (Exception e) { 
 	 				supplierAddress.setPhone(null);
 	 	 	 	}
 				
 				try {
 	 				supplierAddress.setFax(datavalue[11]);
 	 	 	 	} catch (Exception e) { 
 	 				supplierAddress.setFax(null);
 	 	 	 	}
 				
 				try {
 	 				supplierAddress.setEmail(datavalue[12]);
 	 	 	 	} catch (Exception e) { 
 	 				supplierAddress.setEmail(null);
 	 	 	 	}
 				
 				try {
 					supplier.setNote(datavalue[13]);
 	 	 	 	} catch (Exception e) { 
 					supplier.setNote(null);
 	 	 	 	}
 				
 				try {
 	 				supplier.setActive(Boolean.parseBoolean(datavalue[14]));
 	 	 	 	} catch (Exception e) { 
 	 				supplier.setActive(true);
 	 	 	 	}
 	 				
 				supplierAddress.setPrimary(true);
 				supplier.setAddress(supplierAddress);
 				Integer supplierId = webJaguar.getSupplierIdByAccountNumber(supplier.getAccountNumber());
				if (supplierId != null) {
					 supplier.setId(supplierId);
					 webJaguar.updateSupplierById(supplier);
				 } else {
					 webJaguar.insertSupplier(supplier);
				 }	
 			}
		} catch (Exception e) {
			e.printStackTrace();
			// do nothing
 		}
	}
	
	public void parseProductSupplierTextFile(File text_file) {
		try {
		
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
			Date start = new Date();
			BufferedReader bReader = new BufferedReader( new FileReader(text_file));
 			StringBuilder sb = new StringBuilder();
 			String line;
 			while ((line = bReader.readLine()) != null) {
 				sb.append(line);
 			}
 			bReader.close();
 			Supplier supplier  = null;
 			Map<String, Supplier> supplierAccountMap = new HashMap<String, Supplier>();
 			List<Supplier> suppliersList = webJaguar.getSuppliers(new Search());
 			for(Supplier sup : suppliersList) {
 				supplierAccountMap.put(sup.getAccountNumber(), sup);
 			}
			
			/**
 			  * Splitting the content of |newProduct| separated line
 			  */
 			String records[] = sb.toString().split("\\|newRecord\\|");
 			
 			for(int l=0; l< records.length; l++) {
				if(l < 2 || records[l].trim().isEmpty()) {
					continue;
				}
				
				String datavalue[] = records[l].split("&\\|\\|&");
				if(datavalue[0] == null || datavalue[0].isEmpty() || datavalue[1] == null || datavalue[1].isEmpty()) {
					continue;
				}
				supplier = supplierAccountMap.get(datavalue[0].toString());
				if (supplier != null) {
					System.out.println("Supplier Product Insert : "+l);
					supplier.setSku(datavalue[1].toString());
 					supplier.setSupplierSku(datavalue[2].toString());
 					if (datavalue[3] != null && !datavalue[3].isEmpty()) {
 						supplier.setPrice(Double.parseDouble(datavalue[3].toString()));
 					}
 					if (datavalue[4] != null && !datavalue[4].isEmpty()) {
 						supplier.setPercent(Boolean.parseBoolean(datavalue[4].toString()));
 					}
 					try {
 						webJaguar.insertProductSupplier(supplier);
 			 		} catch(Exception e){ }
 				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			// do nothing
 		}
	}
	
	public void parseProductFieldTextFile(File text_file) {
		try {
			
			BufferedReader bReader = new BufferedReader( new FileReader(text_file));
 			StringBuilder sb = new StringBuilder();
 			String line;
 			while ((line = bReader.readLine()) != null) {
 				sb.append(line);
 			}
 			bReader.close();
 			
			/**
 			  * Splitting the content of |newProduct| separated line
 			  */
 			String records[] = sb.toString().split("\\|newRecord\\|");
 			ProductField productField;
 			List<ProductField> productFieldList = new ArrayList<ProductField>();
 			for(int l=0; l< records.length; l++) {
				
				if(l < 2 || records[l].trim().isEmpty()) {
					continue;
				}
				
				String datavalue[] = records[l].split("&\\|\\|&");
				if(datavalue[0] == null || datavalue[0].isEmpty() || datavalue[2] == null || datavalue[2].isEmpty()) {
					continue;
				}
				
				productField = new ProductField();
				
				productField.setId( Integer.parseInt(datavalue[0]) );
				productField.setRank( datavalue[1] != null ? Integer.parseInt(datavalue[1]) : 0 );
				productField.setName( datavalue[2].toString() );			
				productField.setEnabled( datavalue[3].toString().equals("1") ? true : false );
				productField.setQuickModeField( datavalue[4].toString().equals("1") ? true : false );
				productField.setQuickMode2Field( datavalue[5].toString().equals("1") ? true : false );
				productField.setPreValue( datavalue[6] != null? datavalue[6].toString() : "" );
				productField.setComparisonField( datavalue[7].toString().equals("1") ? true : false );
				productField.setShowOnInvoice( datavalue[8].toString().equals("1") ? true : false );
				productField.setShowOnInvoiceBackend( datavalue[9].toString().equals("1") ? true : false );
				productField.setShowOnInvoiceExport( datavalue[10].toString().equals("1") ? true : false );
				productField.setSearch( datavalue[11].toString().equals("1") ? true : false );
				productField.setSearch2( datavalue[12].toString().equals("1") ? true : false );
				productField.setServiceField( datavalue[13].toString().equals("1") ? true : false );
				productField.setDetailsField( datavalue[14].toString().equals("1") ? true : false );
				productField.setPackingField( datavalue[15].toString().equals("1") ? true : false );
				productField.setProductExport( datavalue[16].toString().equals("1") ? true : false );
				productField.setFieldType( datavalue[17] != null? datavalue[17].toString() : "string" );
				productField.setShowOnMyList( datavalue[18].toString().equals("1") ? true : false );
				productField.setShowOnDropDown( datavalue[19].toString().equals("1") ? true : false );
				productFieldList.add(productField);
			}

			webJaguar.updateProductFields(productFieldList);
			
		} catch(Exception e){ }
	
	}
	
	public void parseCategoryTextFile(File text_file) {
		try {
			
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
			Date start = new Date();
			BufferedReader bReader = new BufferedReader( new FileReader(text_file));
 			StringBuilder sb = new StringBuilder();
 			String line;
 			while ((line = bReader.readLine()) != null) {
 				sb.append(line);
 			}
 			
 			Category category  = null;
			
			/**
 			  * Splitting the content of |newRecord| separated line
 			  */
 			String records[] = sb.toString().split("\\|newRecord\\|");
 			HashMap<String, Integer> rankingMap = null;
 			List<Map <String, Integer>> data = new ArrayList<Map <String, Integer>>();
 			for(int l=0; l< records.length; l++) {
 				
				if(l < 2 || records[l].trim().isEmpty()) {
					continue;
				}
				
				String datavalue[] = records[l].split("&\\|\\|&");
				if(datavalue[0] == null || datavalue[0].isEmpty() || datavalue[1] == null || datavalue[1].isEmpty()) {
					continue;
				}
				
				category = new Category();
				
				try {
					category.setId(Integer.parseInt(datavalue[0]));
 	 	 		} catch (Exception e) { 
 	 	 			continue;
 				}
 				
				try {
					category.setName(datavalue[1]);
 	 	 	 	} catch (Exception e) {
 	 	 	 		continue;
 				}
 					
				try {
 					category.setParent(Integer.parseInt(datavalue[2]));
 				} catch (Exception e) { 
 					category.setParent(null);
 	 			}
 					
				rankingMap = new HashMap<String, Integer>();
				rankingMap.put("id", Integer.parseInt(datavalue[0]));
 	 	 	 	try {
 					category.setRank(Integer.parseInt(datavalue[3]));
 					rankingMap.put("rank", Integer.parseInt(datavalue[3]));
 				} catch (Exception e) { 
 	 	 	 		category.setRank(null);
 	 	 	 		rankingMap.put("rank", null);
 				}
 	 	 	 	data.add( rankingMap );	
				
 	 	 	 	try {
 					category.setHtmlCode(datavalue[4]);
 	 	 	 	} catch (Exception e) {
 					category.setHtmlCode(null);
 	 	 	 	}
 				
 				try {
 	 				category.setFooterHtmlCode(datavalue[5]);
 	 	 	 	} catch (Exception e) {
 	 				category.setFooterHtmlCode(null);
 	 	 	 	}
 				
 				try {
 	 				category.setRightBarTopHtmlCode(datavalue[6]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setRightBarTopHtmlCode(null);
 	 	 	 	}
 				
 				try {
 	 				category.setRightBarBottomHtmlCode(datavalue[7]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setRightBarBottomHtmlCode(null);
 	 	 	 	}
 				
 				try {
 	 				category.setLeftBarTopHtmlCode(datavalue[8]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setLeftBarTopHtmlCode(null);
 	 	 	 	}
 				
 				try {
 	 				category.setLeftBarBottomHtmlCode(datavalue[9]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setLeftBarBottomHtmlCode(null);
 	 	 	 	}
 				
 				try {
 	 				category.setProtectedHtmlCode(datavalue[10]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setProtectedHtmlCode(null);
 	 	 	 	}
 				
 				try {
 	 				category.setUrl(datavalue[11]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setUrl(null);
 	 	 	 	}
 				
 				try {
 	 				category.setUrlTarget(datavalue[12]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setUrlTarget(null);
 	 	 	 	}
 				
 				try {
 	 				category.setRedirect301(Boolean.parseBoolean(datavalue[13]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setRedirect301(false);
 	 	 	 	}
 				
 				try {
 	 				category.setSubcatCols(Integer.parseInt(datavalue[14]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setSubcatCols(1);
 	 	 	 	}
 				
 				try {
 	 				category.setSubcatLevels(Integer.parseInt(datavalue[15]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setSubcatLevels(null);
 	 	 	 	}
 				
 				try {
 	 				category.setSubcatLocation(datavalue[16]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setSubcatLocation(null);
 	 	 	 	}
 				
 				try {
 	 				category.setShowSubcats(Boolean.parseBoolean(datavalue[17]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setShowSubcats(false);
 	 	 	 	}
 				
 				try {
 	 				category.setLinkType(CategoryLinkType.valueOf(datavalue[18]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setLinkType(CategoryLinkType.VISIBLE);
 	 	 	 	}
 				
 				try {
 	 				category.setDisplayMode(datavalue[19]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setDisplayMode(null);
 	 	 	 	}
 				
 				try {
 	 				category.setHideHeader(Boolean.parseBoolean(datavalue[20]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setHideHeader(false);
 	 	 	 	}
 				
 				try {
 	 				category.setHideTopBar(Boolean.parseBoolean(datavalue[21]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setHideTopBar(false);
 	 	 	 	}
 				
 				try {
 	 				category.setHideLeftBar(Boolean.parseBoolean(datavalue[22]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setHideLeftBar(false);
 	 	 	 	}
 				
 				try {
 	 				category.setHideRightBar(Boolean.parseBoolean(datavalue[23]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setHideRightBar(false);
 	 	 	 	}
 				
 				try {
 	 				category.setHideFooter(Boolean.parseBoolean(datavalue[24]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setHideFooter(false);
 	 	 	 	}
 				
 				try {
 	 				category.setHideBreadCrumbs(Boolean.parseBoolean(datavalue[25]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setHideBreadCrumbs(false);
 	 	 	 	}
 				
 				try {
 	 				category.setHeadTag(datavalue[26]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setHeadTag(null);
 	 	 	 	}
 				
 				try {
 	 				category.setProductPerPage(Integer.parseInt(datavalue[27]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setProductPerPage(null);
 	 	 	 	}
 				
 				try {
 	 				category.setLayoutId(Integer.parseInt(datavalue[28]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setLayoutId(0);
 	 	 	 	}
 				
 				try {
 	 				category.setCategoryIds(datavalue[29]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setCategoryIds(null);
 	 	 	 	}
 				
 				try {
 	 				category.setSortBy(datavalue[30]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setSortBy(null);
 	 	 	 	}
 				
 				try {
 	 				category.setProductFieldSearch(Boolean.parseBoolean(datavalue[31]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setProductFieldSearch(false);
 	 	 	 	}
 				
 				try {
 	 				category.setProductFieldSearchType(datavalue[32]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setProductFieldSearchType(null);
 	 	 	 	}
 				
 				try {
 	 				category.setField1(datavalue[33]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setField1(null);
 	 	 	 	}
 				
 				try {
 	 				category.setField2(datavalue[34]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setField2(null);
 	 	 	 	}
 				
 				try {
 	 				category.setField3(datavalue[35]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setField3(null);
 	 	 	 	}
 				
 				try {
 	 				category.setField4(datavalue[36]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setField4(null);
 	 	 	 	}
 				
 				try {
 	 				category.setField5(datavalue[37]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setField5(null);
 	 	 	 	}
 				
 				try {
 	 				category.setSalesTagTitle(datavalue[38]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setSalesTagTitle(null);
 	 	 	 	}
 				
 				try {
 	 				category.setShowOnSearch(Boolean.parseBoolean(datavalue[39]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setShowOnSearch(false);
 	 	 	 	}
 				
 				try {
 	 				category.setShared(Boolean.parseBoolean(datavalue[40]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setShared(false);
 	 	 	 	}
 				try {
 	 				category.setSetType(datavalue[41]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setSetType(null);
 	 	 	 	}
 				try {
 	 				category.setSiteMapPriority(Double.parseDouble(datavalue[42]));
 	 	 	 	} catch (Exception e) { 
 	 				category.setSiteMapPriority(0.5);
 	 	 	 	}
 				try {
 	 				category.setProductFieldSearchPosition(datavalue[43]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setProductFieldSearchPosition(null);
 	 	 	 	}
 				try {
 	 				category.setImageUrl(datavalue[44]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setImageUrl(null);
 	 	 	 	}
 				try {
 	 				category.setImageUrlAltName(datavalue[45]);
 	 	 	 	} catch (Exception e) { 
 	 				category.setImageUrlAltName(null);
 	 	 	 	}
 				
 				//set the category feed value so that no one can delete it
 				category.setFeed("Webjaguar");
 						
 				Category tempCategory = webJaguar.getCategoryById(category.getId(), "0");
 				try {
 	 				if(tempCategory == null) {
 	 					webJaguar.insertCategory(category);
 	 				} else {
 	 					webJaguar.updateCategory(category);
 	 				}
 				}catch(Exception e) {
						
				}
 			}
 			//update ranking
 			webJaguar.updateCategoryRanking( data );
 		} catch (Exception e) {
			e.printStackTrace();
			// do nothing
 		}
	}
	
	
	public void parseOptionsTextFile(File text_file) {
		Map<String, Integer> optionIdCodeMap = new HashMap<String, Integer>();
		List<Option> optionsList =  webJaguar.getOptionsList(new OptionSearch());
		for(Option option : optionsList){
    		optionIdCodeMap.put(option.getCode(), option.getId());
    	}
    	try {
			
			BufferedReader bReader = new BufferedReader( new FileReader(text_file));
 			StringBuilder sb = new StringBuilder();
 			String line;
 			while ((line = bReader.readLine()) != null) {
 				sb.append(line);
 			}
 			
 			/**
 			  * Splitting the content of |newProduct| separated line
 			  */
 			String records[] = sb.toString().split("\\|newRecord\\|");
 			System.out.println("Options Size "+records.length);
 			for(int l=0; l< records.length; l++) {
 				System.out.println("Option "+l);
 	 				
				if(l < 2 || records[l].trim().isEmpty()) {
					continue;
				}
				
				String options[] = records[l].split("\\|newProductOption\\|");
				
				Option option = new Option();
				option.setCode(options[0].trim());
				if(optionIdCodeMap.get(options[0].trim()) != null) {
					option.setId(optionIdCodeMap.get(options[0].trim()));
				}
				ProductOption po =  null;
				List<ProductOption> productOptionList = new ArrayList<ProductOption>();
				for(int k=1; k<options.length; k++){
					
					String optionValues[] = options[k].split("\\|newProductOptionValue\\|");
					
					// First part is about product option, rest are for product option values
					String productOptionAttr[] = optionValues[0].split("&\\|\\|&");
					po = new ProductOption();
					try {
						po.setName(productOptionAttr[0].trim());
					} catch (Exception e) { 
	 	 	 			continue;
	 				}
					
					try {
						po.setType((productOptionAttr[1] != null && !productOptionAttr[1].trim().isEmpty()) ? productOptionAttr[1].trim() : "reg");
					} catch (Exception e) { 
						po.setType("reg");
					}
					
					try {
						po.setHelpText((productOptionAttr[2] != null && !productOptionAttr[2].trim().isEmpty())? productOptionAttr[2].trim() : null);
					} catch (Exception e) { 
						po.setHelpText(null);
					}
					
					try {
						po.setNumberOfColors((productOptionAttr[3] != null && !productOptionAttr[3].trim().isEmpty()) ? Integer.parseInt(productOptionAttr[3].trim()) : 0);
					} catch (Exception e) { 
						po.setNumberOfColors(0);
					}
					
					// iterate over the rest of the parts to parse product option values
					for(int m=1; m<optionValues.length; m++){
						
						String productOptionValueAttr[] = optionValues[m].split("&\\|\\|&");
						ProductOptionValue pov = null;
						try {
							pov = new ProductOptionValue(Integer.parseInt(productOptionValueAttr[1].trim()));
							pov.setName(productOptionValueAttr[0].trim());
						} catch (Exception e) { 
							e.printStackTrace();
							continue;
						}
						
						try {
							pov.setOptionPrice((productOptionValueAttr[2] != null && !productOptionValueAttr[2].trim().isEmpty()) ? Double.parseDouble(productOptionValueAttr[2]) : null);
						} catch (Exception e) { 
							pov.setOptionPrice(null);
						}
						
						try {
							pov.setOneTimePrice(productOptionValueAttr[3] != null ? Boolean.parseBoolean(productOptionValueAttr[3]) : false);
						} catch (Exception e) { 
							pov.setOneTimePrice(false);
						}

						try {
							pov.setOptionWeight(productOptionValueAttr[4] != null ? Double.parseDouble(productOptionValueAttr[4]) : null);
						} catch (Exception e) { 
							pov.setOptionWeight(null);
						}
						
						try {
							pov.setImageUrl((productOptionValueAttr[5] != null && !productOptionValueAttr[5].trim().isEmpty()) ? productOptionValueAttr[5] : null);
						} catch (Exception e) { 
							pov.setImageUrl(null);
						}
						
						if(productOptionValueAttr[6] != null && !productOptionValueAttr[6].isEmpty()) {
							String optionsCodes[] = productOptionValueAttr[6].split(",");
							StringBuffer optionIds = new StringBuffer();
							for(String optionCode : optionsCodes) {
								if(optionCode != null && !optionCode.trim().isEmpty() && optionIdCodeMap.get(optionCode.trim()) != null) {
									optionIds.append(optionIdCodeMap.get(optionCode.trim())+",");
								}
							}
							try {
								pov.setDependingOptionIds(optionIds.toString().substring(0, optionIds.lastIndexOf(",")));
							} catch (Exception e) { 
								pov.setDependingOptionIds(null);
							}
						}
						
						
						try {
							pov.setDescription((productOptionValueAttr[7] != null && !productOptionValueAttr[7].trim().isEmpty()) ? productOptionValueAttr[7] : null);
						} catch (Exception e) { 
							pov.setDescription(null);
						}
						
						try {
							pov.setAssignedProductSku((productOptionValueAttr[8] != null && !productOptionValueAttr[8].trim().isEmpty()) ? productOptionValueAttr[8] : null);
						} catch (Exception e) { 
							pov.setAssignedProductSku(null);
						}
						
						try {
							pov.setAssignedSkuAttachment(productOptionValueAttr[9] != null ? Boolean.parseBoolean(productOptionValueAttr[9]) : false);
						} catch (Exception e) { 
							pov.setAssignedSkuAttachment(false);
						}

						try {
							pov.setIncludedProducts((productOptionValueAttr[10] != null && !productOptionValueAttr[10].trim().isEmpty()) ? productOptionValueAttr[10] : null);
						} catch (Exception e) { 
							e.printStackTrace();
							pov.setIncludedProducts(null);
						}
						
						// add product option value to the list
						po.addValue(pov);
					}
					// add product option to the list
					productOptionList.add(po);
				}
				
				option.setProductOptions(productOptionList);
				
				webJaguar.updateProductOptions(option);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// do nothing
 		}
	}
	
	private List<CsvFeed> getCsvFeedList() {
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		
		csvFeedList.add(new CsvFeed("name"));
		csvFeedList.add(new CsvFeed("sku"));
		csvFeedList.add(new CsvFeed("master_sku"));
		csvFeedList.add(new CsvFeed("short_desc"));
		csvFeedList.add(new CsvFeed("long_desc"));
		csvFeedList.add(new CsvFeed("default_supplier_id"));
		csvFeedList.add(new CsvFeed("manufacture_name"));
		csvFeedList.add(new CsvFeed("keywords"));
		csvFeedList.add(new CsvFeed("msrp"));
		
		for(int i=1; i<=10; i++){
			csvFeedList.add(new CsvFeed("price_"+i));
		}
		for(int i=1; i<=10; i++){
			csvFeedList.add(new CsvFeed("cost_"+i));
		}
		for(int i=1; i<=9; i++){
			csvFeedList.add(new CsvFeed("qty_break_"+i));
		}
		for(int i=1; i<=9; i++){
			csvFeedList.add(new CsvFeed("price_table_"+i));
		}
		for(int i=1; i<=9; i++){
			csvFeedList.add(new CsvFeed("price_case_pack_"+i));
		}
			
		csvFeedList.add(new CsvFeed("quote"));
		csvFeedList.add(new CsvFeed("weight"));
		csvFeedList.add(new CsvFeed("package_length"));
		csvFeedList.add(new CsvFeed("package_width"));
		csvFeedList.add(new CsvFeed("package_height"));
		csvFeedList.add(new CsvFeed("usps_max_items_in_package"));
		csvFeedList.add(new CsvFeed("ups_max_items_in_package"));
		csvFeedList.add(new CsvFeed("login_require"));
		csvFeedList.add(new CsvFeed("hide_price"));
		csvFeedList.add(new CsvFeed("hide_msrp"));
		csvFeedList.add(new CsvFeed("recommended_list"));
		csvFeedList.add(new CsvFeed("recommended_list_title"));
		csvFeedList.add(new CsvFeed("recommended_list_display"));
		csvFeedList.add(new CsvFeed("also_consider"));
		csvFeedList.add(new CsvFeed("case_content"));
		csvFeedList.add(new CsvFeed("packing"));
		csvFeedList.add(new CsvFeed("hide_header"));
		csvFeedList.add(new CsvFeed("hide_topbar"));
		csvFeedList.add(new CsvFeed("hide_leftbar"));
		csvFeedList.add(new CsvFeed("hide_rightbar"));
		csvFeedList.add(new CsvFeed("hide_footer"));
		csvFeedList.add(new CsvFeed("hide_breadcrumbs"));
		csvFeedList.add(new CsvFeed("product_layout"));
		csvFeedList.add(new CsvFeed("head_tag"));
		csvFeedList.add(new CsvFeed("html_add_to_cart"));
		csvFeedList.add(new CsvFeed("add_to_list"));
		csvFeedList.add(new CsvFeed("box_size"));
		csvFeedList.add(new CsvFeed("box_extra_amt"));
		csvFeedList.add(new CsvFeed("temperature"));
		csvFeedList.add(new CsvFeed("compare"));
		csvFeedList.add(new CsvFeed("image_layout"));
		csvFeedList.add(new CsvFeed("price_by_customer"));
		csvFeedList.add(new CsvFeed("taxable"));
		csvFeedList.add(new CsvFeed("searchable"));
		csvFeedList.add(new CsvFeed("option_code"));
		csvFeedList.add(new CsvFeed("inventory"));
		csvFeedList.add(new CsvFeed("inventory_afs"));
		csvFeedList.add(new CsvFeed("low_inventory"));
		csvFeedList.add(new CsvFeed("neg_inventory"));
		csvFeedList.add(new CsvFeed("show_neg_inventory"));
		csvFeedList.add(new CsvFeed("custom_shipping_enabled"));
		csvFeedList.add(new CsvFeed("cross_items_code"));
		csvFeedList.add(new CsvFeed("active"));
		csvFeedList.add(new CsvFeed("tab_1"));
		csvFeedList.add(new CsvFeed("tab_1_content"));
		csvFeedList.add(new CsvFeed("tab_2"));
		csvFeedList.add(new CsvFeed("tab_2_content"));
		csvFeedList.add(new CsvFeed("tab_3"));
		csvFeedList.add(new CsvFeed("tab_3_content"));
		csvFeedList.add(new CsvFeed("tab_4"));
		csvFeedList.add(new CsvFeed("tab_4_content"));
		csvFeedList.add(new CsvFeed("tab_5"));
		csvFeedList.add(new CsvFeed("tab_5_content"));
		csvFeedList.add(new CsvFeed("tab_6"));
		csvFeedList.add(new CsvFeed("tab_6_content"));
		csvFeedList.add(new CsvFeed("tab_7"));
		csvFeedList.add(new CsvFeed("tab_7_content"));
		csvFeedList.add(new CsvFeed("tab_8"));
		csvFeedList.add(new CsvFeed("tab_8_content"));
		csvFeedList.add(new CsvFeed("tab_9"));
		csvFeedList.add(new CsvFeed("tab_9_content"));
		csvFeedList.add(new CsvFeed("tab_10"));
		csvFeedList.add(new CsvFeed("tab_10_content"));
		csvFeedList.add(new CsvFeed("minimum_qty"));
		csvFeedList.add(new CsvFeed("incremental_qty"));
		csvFeedList.add(new CsvFeed("enable_rate"));
		csvFeedList.add(new CsvFeed("note"));
		csvFeedList.add(new CsvFeed("search_rank"));
		
		for(int i=1; i<=100; i++){
			if( i >33 && i < 54) {
				// DO NOT ADD ASI fields.
				continue;
			}
			csvFeedList.add(new CsvFeed("field_"+i));
		}
			
		csvFeedList.add(new CsvFeed("feed"));
		csvFeedList.add(new CsvFeed("feed_new"));
		
		return csvFeedList;
	}
	public void parseProductXmlFile(File xml_file) {
	   categoryMap = webJaguar.getWebjaguarCategoryMap();
	   try {
			SAXParserFactory f = SAXParserFactory.newInstance();
	        SAXParser parser = f.newSAXParser();
	        DefaultHandler handler = new MyContentHandler();
	        content = FileUtils.readFileToString(productFile, "UTF-8");
	        parser.parse(xml_file,handler);
	    } catch (ParserConfigurationException e) {System.out.println("error 1 ");
	    } catch (SAXException e) {e.printStackTrace();  System.out.println("error 2 ");
	    } catch (IOException e) {System.out.println("error 3 ");
	    	e.printStackTrace();
	    }
	}
	
	public static class MyContentHandler extends DefaultHandler {
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		List<String> dataDeleted = new ArrayList<String>();
		List<Map <String, Object>> categoryData = new ArrayList<Map <String, Object>>();
		HashMap<String, Object> map;
		Set<String> breadCrumbs = new TreeSet<String>();
		StringBuffer xml = new StringBuffer();
		boolean authorizedFeed = false;
		WebjaguarDataFeed dataFeed = null;
		
		boolean product;
		// for DIFF
		boolean modified, added, deleted;
		
		public void startDocument() throws SAXException {
			xml = new StringBuffer();
		}
	    
	    public void endDocument() throws SAXException {
	       // for files that has less than 500 Product
	       if (data.size() > 0) {
				// products
				updateProduct(data);
				data.clear();
				map.clear();
	       }
	       if (breadCrumbs.size() > 0) {
				// categories
	    	    updateCategories(breadCrumbs);
				breadCrumbs.clear();
		   }
	       if (dataDeleted.size() > 0) {
				// products
				deleteProduct(dataDeleted);
				dataDeleted.clear();
	       }
	       if (categoryData.size() > 0) {
	    	   	// product category
				updateProductCategory(categoryData);
				categoryData.clear();
		   }
	       if(dataFeed != null) {
	    	   webJaguar.markAndInactiveOldProducts("Webjaguar", dataFeed.getId(), true);
	       }
	    }
	    
	    public void startElement(String nsURI, String strippedName, String tagName, Attributes attributes) throws SAXException {
	    	if(tagName.equalsIgnoreCase("Token")){
	    		// read and save from file directly
				int start = content.indexOf("<token>");
	    		int end = content.indexOf("</token>", start);
	    		String token = content.substring(start+7, end);
	    		
	    		dataFeed = webJaguar.getWebjaguarDataFeed(null, token.trim(), "webjaguar");
	    		if(dataFeed != null) {
	    			authorizedFeed = true;
	    			webJaguar.markAndInactiveOldProducts("Webjaguar", dataFeed.getId(), false);
	    	    } 
	    	}
 	    	if(!tagName.equalsIgnoreCase("Products") && !authorizedFeed) {
    	    	throw new SAXException();
    	    }
	    	
	    	if (tagName.equalsIgnoreCase("Product") && authorizedFeed) { 
 	    		map = new HashMap<String, Object>();
	    		
	    		// read and save from file directly
				int start = content.indexOf("<product id=\""+attributes.getValue("id")+"\"");
	    		int end = content.indexOf("</product>", start) + 10;
	    		xml.append(content.substring(start, end));
	    		
	    		// convert to Product object
	    		JAXBContext context;
	    		Unmarshaller unmarshaller = null;
	    		ByteArrayInputStream bis;
	    		Product prod = null;
				try {
					context = JAXBContext.newInstance(Product.class);
					unmarshaller = context.createUnmarshaller();
					StringBuffer sb = new StringBuffer(xml);
					bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
					prod = (Product) unmarshaller.unmarshal(bis);
				} catch (Exception e) { e.printStackTrace(); }
				
				if(prod != null) {
					Class<Product> c = Product.class;
					Method m = null;
					Object arglist[] = null;
					
					map.put("name", prod.getName());
					map.put("sku", prod.getSku());
					map.put("master_sku", prod.getMasterSku());
					map.put("feed_freeze", prod.isFeedFreeze());
					map.put("short_desc", prod.getShortDesc());
					map.put("long_desc", prod.getLongDesc());
					map.put("keywords", prod.getKeywords());
					map.put("weight", prod.getWeight());
					map.put("msrp", prod.getMsrp());
					map.put("hide_msrp", prod.isHideMsrp());
					map.put("price_by_customer", prod.isPriceByCustomer());
					map.put("price_1", prod.getPrice1());
					map.put("price_2", prod.getPrice2());
					map.put("price_3", prod.getPrice3());
					map.put("price_4", prod.getPrice4());
					map.put("price_5", prod.getPrice5());
					map.put("price_6", prod.getPrice6());
					map.put("price_7", prod.getPrice7());
					map.put("price_8", prod.getPrice8());
					map.put("price_9", prod.getPrice9());
					map.put("price_10", prod.getPrice10());
					map.put("qty_break_1", prod.getQtyBreak1());
					map.put("qty_break_2", prod.getQtyBreak2());
					map.put("qty_break_3", prod.getQtyBreak3());
					map.put("qty_break_4", prod.getQtyBreak4());
					map.put("qty_break_5", prod.getQtyBreak5());
					map.put("qty_break_6", prod.getQtyBreak6());
					map.put("qty_break_7", prod.getQtyBreak7());
					map.put("qty_break_8", prod.getQtyBreak8());
					map.put("qty_break_9", prod.getQtyBreak9());
					map.put("quote", prod.isQuote());
					for (ProductField productField: productFields) {
		    			try {
							m = c.getMethod("getField" + productField.getId());
							map.put("field_"+productField.getId(), (String) m.invoke(prod, arglist));
						} catch (Exception e) {
							map.put("field_"+productField.getId(), null);
						}
		            }
		        	map.put("minimum_qty", prod.getMinimumQty());
					map.put("protected_level", prod.getProtectedLevel());
					map.put("packing", prod.getPacking());
					map.put("login_require", prod.isLoginRequire());
					map.put("hide_price", prod.isHidePrice());
					map.put("hide_header", prod.isHideHeader());
					map.put("hide_topbar", prod.isHideTopBar());
					map.put("hide_leftbar", prod.isHideLeftBar());
					map.put("hide_rightbar", prod.isHideRightBar());
					map.put("hide_footer", prod.isHideFooter());
					map.put("hide_breadcrumbs", prod.isHideBreadCrumbs());
					map.put("add_to_list", prod.isAddToList());
					map.put("compare", prod.isCompare());
					map.put("viewed", prod.getViewed());
					map.put("taxable", prod.isTaxable());
					map.put("searchable", prod.isSearchable());
					map.put("inventory", prod.getInventory());
					map.put("inventory_afs", prod.getInventoryAFS());
					map.put("neg_inventory", prod.isNegInventory());
					map.put("show_neg_inventory", prod.isShowNegInventory());
					map.put("custom_shipping_enabled", prod.isCustomShippingEnabled());
					map.put("subscription", prod.isSubscription());
					map.put("active", prod.isActive());
					map.put("enable_rate", prod.isEnableRate());
					map.put("sitemap_priority", prod.getSiteMapPriority());
					map.put("recommended_list", prod.getRecommendedList());
					map.put("recommended_list_title", prod.getRecommendedListTitle());
					map.put("recommended_list_display", prod.getRecommendedListDisplay());
					map.put("option_code", prod.getOptionCode());
					map.put("cross_items_code", prod.getCrossItemsCode());
					map.put("feed", "Webjaguar");				
					map.put("feed_new", true);
					map.put("feed_id",dataFeed.getId());
					
					if (prod.getImages() != null && !prod.getImages().isEmpty()) {
			    		int imageIndex = 1;
			    		for (ProductImage image : prod.getImages()) {
			    			map.put("image"+imageIndex++, image.getImageUrl());
			    		}
			    	}
					map.put("productId", webJaguar.getProductIdBySku(prod.getSku()));
					breadCrumbs.addAll(prod.getBreadCrumb());
					
					// categories
					List<Integer> catIds = new ArrayList<Integer>();
					boolean productAdded = false;
					for(String breadCrumb : prod.getBreadCrumb()) {
						
						if(categoryMap.containsKey(breadCrumb)) {
							if(!productAdded) {
								data.add(map);
								if (map.get("productId") == null) {
									// new item
									Product newProduct = new Product(prod.getSku(), "Feed");
									webJaguar.insertProduct(newProduct, null);
									map.put("productId", newProduct.getId());
								}
								productAdded = true;
							}
							catIds.add(categoryMap.get(breadCrumb).getCatId());
						}	
					}
					// category mapping
					for (Integer catId: catIds) {
						HashMap<String, Object> mapCategory = new HashMap<String, Object>();
						mapCategory.put("productId", map.get("productId"));
						mapCategory.put("categoryId", catId);
						categoryData.add(mapCategory);
					}
				}
			}
	    }
	    
	    public void endElement(String nsURI, String strippedName, String tagName) throws SAXException {
   		
	    	if (tagName.equalsIgnoreCase("product") && authorizedFeed) {	
				product = false;
	    		xml = new StringBuffer();
	     		if (data.size() == 500) {
					// products
					updateProduct(data);
					data.clear();
					map.clear();
				}
	     		if (categoryData.size() == 500) {
					// product category
					updateProductCategory(categoryData);
					categoryData.clear();
				}
	    	}
	    }
	    
	    public void warning(SAXParseException e) throws SAXException {
	        System.out.println("Warning: " + e.getMessage());
	    }

	    private void updateProduct(List<Map <String, Object>> data) {
	    	// update Products
	    	webJaguar.nonTransactionSafeUpdateWebjaguarProduct(getCsvFeedList(), data);
	    }
	    
	    private void updateProductCategory(List<Map <String, Object>> categoryData) {
	    	// update Product Category
	    	webJaguar.nonTransactionSafeInsertCategories(categoryData);		
		}
	    
	    private void updateCategories(Set<String> breadCrumb) {
	    	//get all possible breadcrumb
	    	getAllBreadCrumbs(breadCrumb);
	    	
	    	// update Categories
	    	List<WebjaguarCategory> wjCategoryList = new ArrayList<WebjaguarCategory>();
			WebjaguarCategory wjCategory = null;
	    	for(String category : breadCrumb) {
	    		String[] categories = category.split("-->");
		    	if(categories.length > 0) {
					wjCategory = new WebjaguarCategory();
		    		wjCategory.setBreadCrumb(category.trim());
		    		wjCategory.setCat(categories[categories.length - 1].trim());
		    
		    		wjCategoryList.add(wjCategory);
				}
	    	}
	    	webJaguar.nonTransactionSafeInsertWebjaguarCategories(wjCategoryList);
	    }
	    
	    private void getAllBreadCrumbs(Set<String> breadCrumb) {
	    	Set<String> breadCrumbTemp = new TreeSet<String>();
	    	for(String category : breadCrumb) {
	    		String[] categories = category.split("-->");
		    	if(categories.length > 0) {
		    	  StringBuffer sb = new StringBuffer();
		    	  sb.append(categories[0].trim());
		    	  breadCrumbTemp.add(sb.toString());
				  for(int i=0; i< categories.length-1; i++) {
					  breadCrumbTemp.add(sb.append("-->"+categories[i+1].trim()).toString());
				  }
		    	}
	    	}
	    	
	    	breadCrumb.clear();
	    	breadCrumb.addAll(breadCrumbTemp);
	    }
	    
	    private void deleteProduct(List<String> data) {
	    	// inactive Products
	        // webJaguar.nonTransactionSafeInactiveASIFileFeedProduct(data, "asi_unique_id");
	    }
	    
	    private List<CsvFeed> getCsvFeedList() {
			List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
			
			csvFeedList.add(new CsvFeed("name"));
			csvFeedList.add(new CsvFeed("sku"));
			csvFeedList.add(new CsvFeed("master_sku"));
			csvFeedList.add(new CsvFeed("feed_freeze"));
			csvFeedList.add(new CsvFeed("short_desc"));
			csvFeedList.add(new CsvFeed("long_desc"));
			csvFeedList.add(new CsvFeed("keywords"));
			csvFeedList.add(new CsvFeed("weight"));
			csvFeedList.add(new CsvFeed("msrp"));
			csvFeedList.add(new CsvFeed("hide_msrp"));
			csvFeedList.add(new CsvFeed("price_by_customer"));
			csvFeedList.add(new CsvFeed("price_1"));
			csvFeedList.add(new CsvFeed("price_2"));
			csvFeedList.add(new CsvFeed("price_3"));
			csvFeedList.add(new CsvFeed("price_4"));
			csvFeedList.add(new CsvFeed("price_5"));
			csvFeedList.add(new CsvFeed("price_6"));
			csvFeedList.add(new CsvFeed("price_7"));
			csvFeedList.add(new CsvFeed("price_8"));
			csvFeedList.add(new CsvFeed("price_9"));
			csvFeedList.add(new CsvFeed("price_10"));
			csvFeedList.add(new CsvFeed("qty_break_1"));
			csvFeedList.add(new CsvFeed("qty_break_2"));
			csvFeedList.add(new CsvFeed("qty_break_3"));
			csvFeedList.add(new CsvFeed("qty_break_4"));
			csvFeedList.add(new CsvFeed("qty_break_5"));
			csvFeedList.add(new CsvFeed("qty_break_6"));
			csvFeedList.add(new CsvFeed("qty_break_7"));
			csvFeedList.add(new CsvFeed("qty_break_8"));
			csvFeedList.add(new CsvFeed("qty_break_9"));
			csvFeedList.add(new CsvFeed("quote"));
			for(ProductField field : productFields) {
				csvFeedList.add(new CsvFeed("field_"+field.getId()));
			}
			csvFeedList.add(new CsvFeed("minimum_qty"));
			csvFeedList.add(new CsvFeed("protected_level"));
			csvFeedList.add(new CsvFeed("packing"));
			csvFeedList.add(new CsvFeed("login_require"));
			csvFeedList.add(new CsvFeed("hide_price"));
			csvFeedList.add(new CsvFeed("hide_header"));
			csvFeedList.add(new CsvFeed("hide_topbar"));
			csvFeedList.add(new CsvFeed("hide_leftbar"));
			csvFeedList.add(new CsvFeed("hide_rightbar"));
			csvFeedList.add(new CsvFeed("hide_footer"));
			csvFeedList.add(new CsvFeed("hide_breadcrumbs"));
			csvFeedList.add(new CsvFeed("add_to_list"));
			csvFeedList.add(new CsvFeed("compare"));
			csvFeedList.add(new CsvFeed("viewed"));
			csvFeedList.add(new CsvFeed("taxable"));
			csvFeedList.add(new CsvFeed("searchable"));
			csvFeedList.add(new CsvFeed("inventory"));
			csvFeedList.add(new CsvFeed("inventory_afs"));
			csvFeedList.add(new CsvFeed("neg_inventory"));
			csvFeedList.add(new CsvFeed("show_neg_inventory"));
			csvFeedList.add(new CsvFeed("custom_shipping_enabled"));
			csvFeedList.add(new CsvFeed("subscription"));
			csvFeedList.add(new CsvFeed("active"));
			csvFeedList.add(new CsvFeed("enable_rate"));
			csvFeedList.add(new CsvFeed("sitemap_priority"));
			csvFeedList.add(new CsvFeed("recommended_list"));
			csvFeedList.add(new CsvFeed("recommended_list_title"));
			csvFeedList.add(new CsvFeed("recommended_list_display"));
			csvFeedList.add(new CsvFeed("option_code"));
			csvFeedList.add(new CsvFeed("cross_items_code"));
			csvFeedList.add(new CsvFeed("feed"));
			csvFeedList.add(new CsvFeed("feed_new"));
			csvFeedList.add(new CsvFeed("feed_id"));
			
			return csvFeedList;
		}
	 }
}