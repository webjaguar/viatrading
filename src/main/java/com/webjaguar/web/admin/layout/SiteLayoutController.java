/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.layout;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.dao.LayoutDao;
import com.webjaguar.model.Language;
import com.webjaguar.model.Layout;

public class SiteLayoutController extends MultiActionController {

	private LayoutDao layoutDao;
	public void setLayoutDao(LayoutDao layoutDao) { this.layoutDao = layoutDao; }
	
    public ModelAndView siteLayout(HttpServletRequest request, HttpServletResponse response,
    		Layout layout) {
		Map<String, Object> map = new HashMap<String, Object>();
    	
    	// check if update button was pressed
		if (request.getParameter("__update") != null) {	
			layoutDao.updateLayout(layout);
		}
		
		if (request.getParameter("layout") != null) {
			String value = request.getParameter("layout");
			if (value.startsWith("1,")) {
				layout.setId(1);
				layout.setHost(value.substring(2));					
			} else {
				layout.setId(ServletRequestUtils.getIntParameter(request, "layout", 1));
			}
		}
		layout = this.layoutDao.getLayout(layout);
		if (layout == null) {
			layout = this.layoutDao.getLayout(new Layout(1));
		}	
		
		map.put("layout", layout);
		map.put("layoutList", layoutDao.getLayoutList());
		
    	return new ModelAndView("admin/layout/site", map);
    }	
    
    public ModelAndView siteHeadTag(HttpServletRequest request, HttpServletResponse response, Layout layout) { 
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	List<Language> gI18N =layoutDao.getLanguageSettings(); 
    	
    	// check if cancel button was pressed
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("siteLayout.jhtm"), "id", layout.getId());
    	}
    	
    	layout.setLang("");
    	
		// check if update button was pressed
		if (request.getParameter("_update") != null) {
			this.layoutDao.updateHeadTag(layout);
			map.put("layout", this.layoutDao.getLayout(layout));
			map.put("updated", "HeadTag");
			map.put("layoutList", layoutDao.getLayoutList());
			// i18n
			if (!(gI18N.isEmpty())) { 
				List<Language> i18n = gI18N;
				for(Language language: i18n) {
					layout.setLang(language.getLanguageCode());
					layout.setHeadTag(ServletRequestUtils.getStringParameter(request, "__i18nHeadTag_" + language.getLanguageCode(), ""));
					layout.setHeadTagMobile(ServletRequestUtils.getStringParameter(request, "__i18nHeadTagMobile_" + language.getLanguageCode(), ""));
					this.layoutDao.updateHeadTag(layout);					
				}
			}
			return new ModelAndView("admin/layout/site", map);
		}
    	    	
    	layout = this.layoutDao.getLayout(layout);
		if (layout == null) {
			return new ModelAndView(new RedirectView("siteLayout.jhtm"));
		} else {
			// i18n
			if(((String) gSiteConfig.get("gI18N")).length()>0) {
				if (!(gI18N.isEmpty())) {
					layout.setLang(null);
					map.put("i18nMap", this.layoutDao.getI18nLayout(layout));				
					map.put("languageCodes", gI18N);
				}
			}
		}
		
		map.put("layout", layout);
				
		return new ModelAndView("admin/layout/headTagForm", map);
    }
    
    public ModelAndView siteHeader(HttpServletRequest request, HttpServletResponse response, Layout layout) { 
    	Map<String, Object> map = new HashMap<String, Object>();
    	List<Language> gI18N =layoutDao.getLanguageSettings(); 
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
    	// check if cancel button was pressed
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("siteLayout.jhtm"), "id", layout.getId());
    	}
    	
    	layout.setLang("");
    	
		// check if update button was pressed
		if (request.getParameter("_update") != null) {
			this.layoutDao.updateHeaderHtml(layout);
			map.put("layout", this.layoutDao.getLayout(layout));
			map.put("updated", "Header");
			map.put("layoutList", layoutDao.getLayoutList());
			// i18n
			if (!(gI18N.isEmpty())) {
				List<Language> i18n =gI18N;
				for(Language language: i18n) {
					layout.setLang(language.getLanguageCode());
					layout.setHeaderHtml(ServletRequestUtils.getStringParameter(request, "__i18nHeaderHtml_" + language.getLanguageCode(), ""));
					layout.setHeaderHtmlLogin(ServletRequestUtils.getStringParameter(request, "__i18nHeaderHtmlLogin_" + language.getLanguageCode(), ""));
					layout.setHeaderHtmlMobile(ServletRequestUtils.getStringParameter(request, "__i18nHeaderHtmlMobile_" + language.getLanguageCode(), ""));
					layout.setHeaderHtmlLoginMobile(ServletRequestUtils.getStringParameter(request, "__i18nHeaderHtmlLoginMobile_" + language.getLanguageCode(), ""));
					this.layoutDao.updateHeaderHtml(layout);					
				}
			}
			return new ModelAndView("admin/layout/site", map);
		}
    	    	
    	layout = this.layoutDao.getLayout(layout);
		if (layout == null) {
			return new ModelAndView(new RedirectView("siteLayout.jhtm"));
		} else {
			// i18n
			if(((String) gSiteConfig.get("gI18N")).length()>0) {
				if (!(gI18N.isEmpty())) {
					layout.setLang(null);
					map.put("i18nMap", this.layoutDao.getI18nLayout(layout));
					map.put("languageCodes", gI18N);
				}
			}
		}
		
		map.put("layout", layout);
				
		return new ModelAndView("admin/layout/headerForm", map);
    }
    
    public ModelAndView siteTopBar(HttpServletRequest request, HttpServletResponse response, Layout layout) { 
    	Map<String, Object> map = new HashMap<String, Object>();
    	List<Language> gI18N =layoutDao.getLanguageSettings(); 
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
    	// check if cancel button was pressed
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("siteLayout.jhtm"), "id", layout.getId());
    	}
    	
    	layout.setLang("");
    	
		// check if update button was pressed
		if (request.getParameter("_update") != null) {
			this.layoutDao.updateTopBarHtml(layout);
			map.put("layout", this.layoutDao.getLayout(layout));
			map.put("updated", "TopBar");
			map.put("layoutList", layoutDao.getLayoutList());
			// i18n
			if (!(gI18N.isEmpty())) {
				List<Language> i18n = gI18N;
				for(Language language: i18n) {
					layout.setLang(language.getLanguageCode());
					layout.setTopBarHtml(ServletRequestUtils.getStringParameter(request, "__i18nTopBarHtml_" + language.getLanguageCode(), ""));
					layout.setTopBarHtmlLogin(ServletRequestUtils.getStringParameter(request, "__i18nTopBarHtmlLogin_" + language.getLanguageCode(), ""));
					layout.setTopBarHtmlMobile(ServletRequestUtils.getStringParameter(request, "__i18nTopBarHtmlMobile_" + language.getLanguageCode(), ""));
					layout.setTopBarHtmlLoginMobile(ServletRequestUtils.getStringParameter(request, "__i18nTopBarHtmlLoginMobile_" + language.getLanguageCode(), ""));
					this.layoutDao.updateTopBarHtml(layout);					
				}
			}
			return new ModelAndView("admin/layout/site", map);
		}
    	    	
    	layout = this.layoutDao.getLayout(layout);
		if (layout == null) {
			return new ModelAndView(new RedirectView("siteLayout.jhtm"));
		} else {
			// i18n
			if(((String) gSiteConfig.get("gI18N")).length()>0){				
				if (!(gI18N.isEmpty())) {
					layout.setLang(null);
					map.put("i18nMap", this.layoutDao.getI18nLayout(layout));
					map.put("languageCodes", gI18N);
				}
			}
		}
		
		map.put("layout", layout);
				
		return new ModelAndView("admin/layout/topBarForm", map);
    }    
    
    public ModelAndView siteFooter(HttpServletRequest request, HttpServletResponse response, Layout layout) { 
    	Map<String, Object> map = new HashMap<String, Object>();
    	List<Language> gI18N =layoutDao.getLanguageSettings(); 
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
    	// check if cancel button was pressed
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("siteLayout.jhtm"), "id", layout.getId());
    	}
    	
    	layout.setLang("");
    	
		// check if update button was pressed
		if (request.getParameter("_update") != null) {
			this.layoutDao.updateFooterHtml(layout);
			map.put("layout", this.layoutDao.getLayout(layout));
			map.put("updated", "Footer");
			map.put("layoutList", layoutDao.getLayoutList());
			// i18n
			if (!(gI18N.isEmpty())) {
				List<Language> i18n = gI18N;
				for(Language language: i18n) {
					layout.setLang(language.getLanguageCode());
					layout.setFooterHtml(ServletRequestUtils.getStringParameter(request, "__i18nFooterHtml_" + language.getLanguageCode(), ""));
					layout.setFooterHtmlLogin(ServletRequestUtils.getStringParameter(request, "__i18nFooterHtmlLogin_" + language.getLanguageCode(), ""));
					layout.setFooterHtmlMobile(ServletRequestUtils.getStringParameter(request, "__i18nFooterHtmlMobile_" + language.getLanguageCode(), ""));
					layout.setFooterHtmlLoginMobile(ServletRequestUtils.getStringParameter(request, "__i18nFooterHtmlLoginMobile_" + language.getLanguageCode(), ""));
					this.layoutDao.updateFooterHtml(layout);					
				}
			}
			return new ModelAndView("admin/layout/site", map);
		}
    	    	
    	layout = this.layoutDao.getLayout(layout);
		if (layout == null) {
			return new ModelAndView(new RedirectView("siteLayout.jhtm"));
		} else {
			// i18n
			if(((String) gSiteConfig.get("gI18N")).length()>0){				
				if (!(gI18N.isEmpty())) {
					layout.setLang(null);
					map.put("i18nMap", this.layoutDao.getI18nLayout(layout));	
					map.put("languageCodes", gI18N);
				}
			}
		}
		
		map.put("layout", layout);
				
		return new ModelAndView("admin/layout/footerForm", map);
	}
    
    public ModelAndView siteAemFooter(HttpServletRequest request, HttpServletResponse response, Layout layout) { 
    	Map<String, Object> map = new HashMap<String, Object>();
    	List<Language> gI18N =layoutDao.getLanguageSettings(); 
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
    	// check if cancel button was pressed
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("siteLayout.jhtm"), "id", layout.getId());
    	}
    	
    	layout.setLang("");
    	
		// check if update button was pressed
		if (request.getParameter("_update") != null) {
			this.layoutDao.updateAemFooterHtml(layout);
			map.put("layout", this.layoutDao.getLayout(layout));
			map.put("updated", "AemFooter");
			map.put("layoutList", layoutDao.getLayoutList());
			// i18n
			if (!(gI18N.isEmpty())) {
				List<Language> i18n = gI18N;
				for(Language language: i18n) {
					layout.setLang(language.getLanguageCode());
					layout.setAemFooter(ServletRequestUtils.getStringParameter(request, "__i18nAemFooter_" + language.getLanguageCode(), ""));
					this.layoutDao.updateAemFooterHtml(layout);					
				}
			}
			return new ModelAndView("admin/layout/site", map);
		}
    	    	
    	layout = this.layoutDao.getLayout(layout);
		if (layout == null) {
			return new ModelAndView(new RedirectView("siteLayout.jhtm"));
		} else {
			// i18n
			if(((String) gSiteConfig.get("gI18N")).length()>0){
				if (!(gI18N.isEmpty())) {
					layout.setLang(null);
					map.put("i18nMap", this.layoutDao.getI18nLayout(layout));
					map.put("languageCodes", gI18N);
				}
			}
		}
		
		map.put("layout", layout);
				
		return new ModelAndView("admin/layout/aemFooterForm", map);
	}
    
    public ModelAndView siteLeftBarTop(HttpServletRequest request, HttpServletResponse response, Layout layout) { 
    	Map<String, Object> map = new HashMap<String, Object>();
    	List<Language> gI18N =layoutDao.getLanguageSettings(); 
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
    	// check if cancel button was pressed
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("siteLayout.jhtm"), "id", layout.getId());
    	}
    	
    	layout.setLang("");
    	
		// check if update button was pressed
		if (request.getParameter("_update") != null) {
			this.layoutDao.updateLeftBarTopHtml(layout);
			map.put("layout", this.layoutDao.getLayout(layout));
			map.put("updated", "LeftBarTop");
			map.put("layoutList", layoutDao.getLayoutList());
			// i18n
			if (!(gI18N.isEmpty())) {
				List<Language> i18n =gI18N;
				for(Language language: i18n) {
					layout.setLang(language.getLanguageCode());
					layout.setLeftBarTopHtml(ServletRequestUtils.getStringParameter(request, "__i18nLeftBarTopHtml_" + language.getLanguageCode(), ""));
					layout.setLeftBarTopHtmlLogin(ServletRequestUtils.getStringParameter(request, "__i18nLeftBarTopHtmlLogin_" + language.getLanguageCode(), ""));
					layout.setLeftBarTopHtmlMobile(ServletRequestUtils.getStringParameter(request, "__i18nLeftBarTopHtmlMobile_" + language.getLanguageCode(), ""));
					layout.setLeftBarTopHtmlLoginMobile(ServletRequestUtils.getStringParameter(request, "__i18nLeftBarTopHtmlLoginMobile_" + language.getLanguageCode(), ""));
					this.layoutDao.updateLeftBarTopHtml(layout);					
				}
			}
			return new ModelAndView("admin/layout/site", map);
		}
    	    	
    	layout = this.layoutDao.getLayout(layout);
		if (layout == null) {
			return new ModelAndView(new RedirectView("siteLayout.jhtm"));
		} else {
			// i18n
			if(((String) gSiteConfig.get("gI18N")).length()>0){				
				if (!(gI18N.isEmpty())) {
					layout.setLang(null);
					map.put("i18nMap", this.layoutDao.getI18nLayout(layout));
					map.put("languageCodes", gI18N);
				}
			}
		}
		
		map.put("layout", layout);
				
		return new ModelAndView("admin/layout/leftBarTopForm", map);
    }

    public ModelAndView siteLeftBarBottom(HttpServletRequest request, HttpServletResponse response, Layout layout) { 
    	Map<String, Object> map = new HashMap<String, Object>();
    	List<Language> gI18N =layoutDao.getLanguageSettings(); 
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
    	// check if cancel button was pressed
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("siteLayout.jhtm"), "id", layout.getId());
    	}
    	
    	layout.setLang("");
    	
		// check if update button was pressed
		if (request.getParameter("_update") != null) {
			this.layoutDao.updateLeftBarBottomHtml(layout);
			map.put("layout", this.layoutDao.getLayout(layout));
			map.put("updated", "LeftBarBottom");
			map.put("layoutList", layoutDao.getLayoutList());
			// i18n
			if (!(gI18N.isEmpty())) {
				List<Language> i18n = gI18N;
				for(Language language: i18n) {
					layout.setLang(language.getLanguageCode());
					layout.setLeftBarBottomHtml(ServletRequestUtils.getStringParameter(request, "__i18nLeftBarBottomHtml_" + language.getLanguageCode(), ""));
					layout.setLeftBarBottomHtmlLogin(ServletRequestUtils.getStringParameter(request, "__i18nLeftBarBottomHtmlLogin_" +language.getLanguageCode(), ""));
					layout.setLeftBarBottomHtmlMobile(ServletRequestUtils.getStringParameter(request, "__i18nLeftBarBottomHtmlMobile_" + language.getLanguageCode(), ""));
					layout.setLeftBarBottomHtmlLoginMobile(ServletRequestUtils.getStringParameter(request, "__i18nLeftBarBottomHtmlLoginMobile_" + language.getLanguageCode(), ""));
					this.layoutDao.updateLeftBarBottomHtml(layout);					
				}
			}
			return new ModelAndView("admin/layout/site", map);
		}
    	    	
    	layout = this.layoutDao.getLayout(layout);
		if (layout == null) {
			return new ModelAndView(new RedirectView("siteLayout.jhtm"));
		} else {
			// i18n
			if(((String) gSiteConfig.get("gI18N")).length()>0){				
				if (!(gI18N.isEmpty())) {
					layout.setLang(null);
					map.put("i18nMap", this.layoutDao.getI18nLayout(layout));
					map.put("languageCodes", gI18N);
				}
			}
		}
		
		map.put("layout", layout);
				
		return new ModelAndView("admin/layout/leftBarBottomForm", map);
    }
    
    public ModelAndView siteRightBarTop(HttpServletRequest request, HttpServletResponse response, Layout layout) { 
    	Map<String, Object> map = new HashMap<String, Object>();
    	List<Language> gI18N =layoutDao.getLanguageSettings(); 
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
    	// check if cancel button was pressed
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("siteLayout.jhtm"), "id", layout.getId());
    	}
    	
    	layout.setLang("");
    	
		// check if update button was pressed
		if (request.getParameter("_update") != null) {
			this.layoutDao.updateRightBarTopHtml(layout);
			map.put("layout", this.layoutDao.getLayout(layout));
			map.put("updated", "RightBarTop");
			map.put("layoutList", layoutDao.getLayoutList());
			// i18n
			if (!(gI18N.isEmpty())) {
				List<Language> i18n = gI18N;
				for(Language language: i18n) {
					layout.setLang(language.getLanguageCode());
					layout.setRightBarTopHtml(ServletRequestUtils.getStringParameter(request, "__i18nRightBarTopHtml_" + language.getLanguageCode(), ""));
					layout.setRightBarTopHtmlLogin(ServletRequestUtils.getStringParameter(request, "__i18nRightBarTopHtmlLogin_" + language.getLanguageCode(), ""));
					layout.setRightBarTopHtmlMobile(ServletRequestUtils.getStringParameter(request, "__i18nRightBarTopHtmlMobile_" + language.getLanguageCode(), ""));
					layout.setRightBarTopHtmlLoginMobile(ServletRequestUtils.getStringParameter(request, "__i18nRightBarTopHtmlLoginMobile_" + language.getLanguageCode(), ""));
					this.layoutDao.updateRightBarTopHtml(layout);					
				}
			}
			return new ModelAndView("admin/layout/site", map);
		}
    	    	
    	layout = this.layoutDao.getLayout(layout);
		if (layout == null) {
			return new ModelAndView(new RedirectView("siteLayout.jhtm"));
		} else {
			// i18n
			if(((String) gSiteConfig.get("gI18N")).length()>0){
				if (!(gI18N.isEmpty())) {
					layout.setLang(null);
					map.put("i18nMap", this.layoutDao.getI18nLayout(layout));
					map.put("languageCodes", gI18N);
				}
			}
		}
		
		map.put("layout", layout);
				
		return new ModelAndView("admin/layout/rightBarTopForm", map);
    }
    
    public ModelAndView siteRightBarBottom(HttpServletRequest request, HttpServletResponse response, Layout layout) { 
    	Map<String, Object> map = new HashMap<String, Object>();
    	List<Language> gI18N =layoutDao.getLanguageSettings(); 
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
    	// check if cancel button was pressed
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("siteLayout.jhtm"), "id", layout.getId());
    	}
    	
    	layout.setLang("");
    	
		// check if update button was pressed
		if (request.getParameter("_update") != null) {
			this.layoutDao.updateRightBarBottomHtml(layout);
			map.put("layout", this.layoutDao.getLayout(layout));
			map.put("updated", "RightBarBottom");
			map.put("layoutList", layoutDao.getLayoutList());
			// i18n
			if (!(gI18N.isEmpty())) {
				List<Language> i18n = gI18N;
				for(Language language: i18n) {
					layout.setLang(language.getLanguageCode());
					layout.setRightBarBottomHtml(ServletRequestUtils.getStringParameter(request, "__i18nRightBarBottomHtml_" + language.getLanguageCode(), ""));
					layout.setRightBarBottomHtmlLogin(ServletRequestUtils.getStringParameter(request, "__i18nRightBarBottomHtmlLogin_" + language.getLanguageCode(), ""));
					layout.setRightBarBottomHtmlMobile(ServletRequestUtils.getStringParameter(request, "__i18nRightBarBottomHtmlMobile_" + language.getLanguageCode(), ""));
					layout.setRightBarBottomHtmlLoginMobile(ServletRequestUtils.getStringParameter(request, "__i18nRightBarBottomHtmlLoginMobile_" + language.getLanguageCode(), ""));
					this.layoutDao.updateRightBarBottomHtml(layout);					
				}
			}
			return new ModelAndView("admin/layout/site", map);
		}
    	    	
    	layout = this.layoutDao.getLayout(layout);
		if (layout == null) {
			return new ModelAndView(new RedirectView("siteLayout.jhtm"));
		} else {
			// i18n
			if(((String) gSiteConfig.get("gI18N")).length()>0){				
				if (!(gI18N.isEmpty())) {
					layout.setLang(null);
					map.put("i18nMap", this.layoutDao.getI18nLayout(layout));	
					map.put("languageCodes", gI18N);
				}
			}
		}
		
		map.put("layout", layout);
				
		return new ModelAndView("admin/layout/rightBarBottomForm", map);
    }
    
    public ModelAndView systemPage(HttpServletRequest request, HttpServletResponse response, Layout layout) { 
    	Map<String, Object> map = new HashMap<String, Object>();
    	List<Language> gI18N =layoutDao.getLanguageSettings(); 

		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

    	// check if cancel button was pressed
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("../layout/"));
    	}
    	
    	layout.setLang("");
    	
    	if(request.getParameter("type") != null && !request.getParameter("type").equals("")) {
    		if(request.getParameter("type").equals("forgetPassword")) {
    			// for forget password page, client requested dynamic head tag only, so setting others to blank
    			layout.setHeaderHtml("");
				layout.setFooterHtml("");
				layout.setHeaderHtmlMobile("");
				layout.setFooterHtmlMobile("");
				layout.setHeadTagMobile("");
				layout.setLeftBarTopHtmlMobile("");
				layout.setRightBarTopHtmlMobile("");
    		}
    	}
		// check if update button was pressed
		if (request.getParameter("_update") != null) {
			this.layoutDao.updateSystemLayout(layout);
			// i18n
			if (!(gI18N.isEmpty())) {
				List<Language> i18n = gI18N;
				for(Language language: i18n) {
					layout.setLang(language.getLanguageCode());
					layout.setHeaderHtml(ServletRequestUtils.getStringParameter(request, "__i18nHeaderHtml_" + language.getLanguageCode(), ""));
					layout.setFooterHtml(ServletRequestUtils.getStringParameter(request, "__i18nFooterHtml_" + language.getLanguageCode(), ""));
					layout.setHeadTag(ServletRequestUtils.getStringParameter(request, "__i18nHeadTag_" + language.getLanguageCode(), ""));
					layout.setHeaderHtmlMobile(ServletRequestUtils.getStringParameter(request, "__i18nHeaderHtmlMobile_" + language.getLanguageCode(), ""));
					layout.setFooterHtmlMobile(ServletRequestUtils.getStringParameter(request, "__i18nFooterHtmlMobile_" + language.getLanguageCode(), ""));
					layout.setHeadTagMobile(ServletRequestUtils.getStringParameter(request, "__i18nHeadTagMobile_" + language.getLanguageCode(), ""));
					this.layoutDao.updateSystemLayout(layout);					
				}
				layout.setLang("");
			}			
			map.put("message", "update.successful");
		}
    			
    	layout = this.layoutDao.getSystemLayout(layout);
		if (layout == null) {
			return new ModelAndView(new RedirectView("../layout/"));
		} else {
			// i18n
			if(((String) gSiteConfig.get("gI18N")).length()>0){				
				if (!(gI18N.isEmpty())) {
					layout.setLang(null);
					map.put("i18nMap", this.layoutDao.getI18nSystemLayout(layout));
					map.put("languageCodes",gI18N);
				}
			}
		}
		
		// multi store
		List<Layout> layoutList = layoutDao.getSystemLayoutList(layout.getType());
		if (layoutList.size() > 1) {
			map.put("layoutList", layoutList);			
		}
		map.put("layout", layout);
		
		return new ModelAndView("admin/layout/systemPage", map);
    }
    
    public ModelAndView pdf(HttpServletRequest request, HttpServletResponse response, Layout layout) { 
    	Map<String, Object> map = new HashMap<String, Object>();
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
    	// check if cancel button was pressed
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("../layout/"));
    	}
    	
    	if (layout.getType() == null || !(layout.getType().equals("invoice") || layout.getType().equals("packing") || layout.getType().equals("po")|| layout.getType().equals("presentation"))) {
			return new ModelAndView(new RedirectView("../layout/"));    		
    	}
    	
		if(!(Boolean) gSiteConfig.get("gPDF_INVOICE") && !layout.getType().equals("po")){
			return new ModelAndView(new RedirectView("../layout/"));
		}
		File baseFile = new File( getServletContext().getRealPath( "/assets/Image/Layout/" ) );
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				baseFile = new File( (String) prop.get( "site.root" ) + "/assets/Image/Layout/" );
			}
		} catch ( Exception e ) {}
		File headerImage = new File(baseFile, "pdf_" + layout.getType() + "_header.gif");
		File footerImage1 = new File(baseFile, "pdf_" + layout.getType() + "_footer_1.gif");
		File footerImage2 = new File(baseFile, "pdf_" + layout.getType() + "_footer_2.gif");
		
		// check if update button was pressed
    	if (request.getParameter("_update") != null) {
    		if ( request.getParameter( "__delete_header" ) != null ) {
    			// delete header image
    			headerImage.delete();
    		}
    		if ( request.getParameter( "__delete_footer_1" ) != null ) {
    			// delete footer image 1
    			footerImage1.delete();
    		}
    		if ( request.getParameter( "__delete_footer_2" ) != null ) {
    			// delete footer image 2
    			footerImage2.delete();
    		}
    		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
    		MultipartFile imageHeader = multipartRequest.getFile( "_image_header" );
			if ( imageHeader != null && !imageHeader.isEmpty() ) {
				if ( imageHeader.getContentType().equalsIgnoreCase( "image/gif" ) || imageHeader.getContentType().equalsIgnoreCase( "image/jpeg" )
						|| imageHeader.getContentType().equalsIgnoreCase( "image/pjpeg" ) ) {
					try {
						imageHeader.transferTo(headerImage);
					} catch ( IOException e ) {
						// do nothing
					}
				}
			}	
    		MultipartFile imageFooter1 = multipartRequest.getFile( "_image_footer_1" );
			if ( imageFooter1 != null && !imageFooter1.isEmpty() ) {
				if ( imageFooter1.getContentType().equalsIgnoreCase( "image/gif" ) || imageFooter1.getContentType().equalsIgnoreCase( "image/jpeg" )
						|| imageFooter1.getContentType().equalsIgnoreCase( "image/pjpeg" ) ) {
					try {
						imageFooter1.transferTo(footerImage1);
					} catch ( IOException e ) {
						// do nothing
					}
				}
			}
    		MultipartFile imageFooter2 = multipartRequest.getFile( "_image_footer_2" );
			if ( imageFooter2 != null && !imageFooter2.isEmpty() ) {
				if ( imageFooter2.getContentType().equalsIgnoreCase( "image/gif" ) || imageFooter2.getContentType().equalsIgnoreCase( "image/jpeg" )
						|| imageFooter2.getContentType().equalsIgnoreCase( "image/pjpeg" ) ) {
					try {
						imageFooter2.transferTo(footerImage2);
					} catch ( IOException e ) {
						// do nothing
					}
				}
			}
    	}
		map.put("hasHeaderImage", headerImage.exists());
		map.put("hasFooterImage1", footerImage1.exists());
		map.put("hasFooterImage2", footerImage2.exists());
		
		return new ModelAndView("admin/layout/pdf", map);
    }
}
