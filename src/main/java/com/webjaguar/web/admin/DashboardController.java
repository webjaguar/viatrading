package com.webjaguar.web.admin;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AbandonedShoppingCart;
import com.webjaguar.model.AbandonedShoppingCartSearch;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.CityReport;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.ProductReport;
import com.webjaguar.model.RangeReport;
import com.webjaguar.model.Report;
import com.webjaguar.model.ReportFilter;
import com.webjaguar.model.SalesRepReport;
import com.webjaguar.model.State;
import com.webjaguar.model.StateReport;
import com.webjaguar.model.TicketReport;
import com.webjaguar.model.ZipCode;
import com.webjaguar.model.crm.CrmContactSearch;
import com.webjaguar.model.crm.CrmTaskSearch;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ServiceWork;
import com.webjaguar.web.domain.Constants;


public class DashboardController implements Controller{
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		SimpleDateFormat df = new SimpleDateFormat(siteConfig.get("SITE_DATE_FORMAT").getValue(), RequestContextUtils.getLocale(request));
		// Convert Browser/client time zone to Application server time
		// Make sure DB server time and Application server times are same.
		if(!siteConfig.get("SITE_TIME_ZONE").getValue().equals("EST5EDT")) {
			df.setTimeZone(TimeZone.getTimeZone(siteConfig.get("SITE_TIME_ZONE").getValue()));
		}
		
		Calendar start = Calendar.getInstance();
	    start.set(Calendar.HOUR_OF_DAY, 0);
	    start.set(Calendar.MINUTE, 0);
	    start.set(Calendar.SECOND, 0);
	    start.set(Calendar.MILLISECOND, 0);
		
		Calendar end = Calendar.getInstance();
	    end.set(Calendar.HOUR_OF_DAY, 23);
	    end.set(Calendar.MINUTE, 59);
	    end.set(Calendar.SECOND, 59);
	    end.set(Calendar.MILLISECOND, 999);
	    
	    String reportingPeriod = ServletRequestUtils.getStringParameter(request, "reportingPeriod", "last_12_months");
	    
	    //last_12_months
	    String reportingPeriodGroup = "m";
		Calendar startDateCal = (Calendar)start.clone();
		startDateCal.add(Calendar.MONTH, -11);
	    startDateCal.set(Calendar.DATE, 1);
		
	    Calendar endDateCal = (Calendar)end.clone();
	    
	    if(reportingPeriod.equals("last_year")) {
			reportingPeriodGroup = "m";
			
			startDateCal = (Calendar)start.clone();
			startDateCal.add(Calendar.YEAR, -1);
			startDateCal.set(Calendar.MONTH, 0);
			startDateCal.set(Calendar.DATE, 1);
			
			endDateCal = (Calendar)end.clone();
			endDateCal.add(Calendar.YEAR, -1);
			endDateCal.set(Calendar.MONTH, 11);
			endDateCal.set(Calendar.DATE, endDateCal.getActualMaximum(Calendar.DATE));
		}

	    if(reportingPeriod.equals("this_year")) {
			reportingPeriodGroup = "m";
			
			startDateCal = (Calendar)start.clone();
			startDateCal.set(Calendar.MONTH, 0);
			startDateCal.set(Calendar.DATE, 1);
			
			endDateCal = (Calendar)end.clone();
			endDateCal.set(Calendar.MONTH, 11);
			endDateCal.set(Calendar.DATE, endDateCal.getActualMaximum(Calendar.DATE));
		}
		
		if(reportingPeriod.equals("last_7_days")) {
			reportingPeriodGroup = "d";
			startDateCal = (Calendar)start.clone();
			startDateCal.add(Calendar.DATE, -6);
			endDateCal = (Calendar)end.clone();
		}
		
		if(reportingPeriod.equals("last_30_days")) {
			reportingPeriodGroup = "w";
			startDateCal = (Calendar)start.clone();
			startDateCal.add(Calendar.DATE, -29);
			endDateCal = (Calendar)end.clone();
		}
		
		if(reportingPeriod.equals("last_month")) {
			reportingPeriodGroup = "w";
			startDateCal = (Calendar)start.clone();
			startDateCal.add(Calendar.MONTH, -1);
			startDateCal.set(Calendar.DATE, 1);
			endDateCal = (Calendar)end.clone();
			endDateCal.add(Calendar.MONTH, -1);
			endDateCal.set(Calendar.DATE, endDateCal.getMaximum(Calendar.DATE));
		}

		Date reportStartDate = startDateCal.getTime();
		Date reportEndDate = endDateCal.getTime();
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put( "reportingPeriod",  reportingPeriod );
		
		/*ORDER REPORT*/
		
		OrderSearch orderSearch = new OrderSearch();
		orderSearch.setStartDate(reportStartDate);
		orderSearch.setEndDate(reportEndDate);
		orderSearch.setLimit(Integer.MAX_VALUE);
		orderSearch.setOffset(0);
		orderSearch.setStatus("pprpss");
		List<Order> orders = this.webJaguar.getOrdersList(orderSearch);
		Double orderTotal = 0.0;
		List<RangeReport> orderReports = this.allocateReports(reportStartDate,reportEndDate,reportingPeriodGroup);
		Map<String, ZipCode> cityZipcodeObjectMap = new HashMap<String, ZipCode>();

		for(Order order: orders) {
			for(RangeReport rr : orderReports) {
				if(order.getDateOrdered().compareTo(rr.getStartDate())>=0 && order.getDateOrdered().compareTo(rr.getEndDate())<=0) {
					rr.setCount(rr.getCount() + 1);
					rr.setTotal(rr.getTotal() + order.getGrandTotal());
					break;
				}
			}
			orderTotal = orderTotal + order.getGrandTotal();
		}

		model.put("orderReports", orderReports);
		model.put("nOrders", orders.size());
		model.put("orderTotal", orderTotal);
		model.put("cityZipcodeObjectMap", cityZipcodeObjectMap);
		
		/*QUOTE REPORT*/
		
		OrderSearch quoteSearch = new OrderSearch();
		quoteSearch.setStartDate(reportStartDate);
		quoteSearch.setEndDate(reportEndDate);
		quoteSearch.setLimit(Integer.MAX_VALUE);
		quoteSearch.setOffset(0);
		quoteSearch.setStatus("xq");
		List<Order> quotes = this.webJaguar.getOrdersList(quoteSearch);
		Double quoteTotal = 0.0;
		List<RangeReport> quoteReports = this.allocateReports(reportStartDate,reportEndDate,reportingPeriodGroup);
		
		for(Order quote: quotes) {
			for(RangeReport rr : quoteReports) {
				if(quote.getDateOrdered().compareTo(rr.getStartDate())>=0 && quote.getDateOrdered().compareTo(rr.getEndDate())<=0) {
					rr.setCount(rr.getCount() + 1);
					rr.setTotal(rr.getTotal() + quote.getGrandTotal());
					break;
				}
			}
			quoteTotal = quoteTotal + quote.getGrandTotal();
		}
		
		model.put("quoteReports", quoteReports);
		model.put("nQuotes", quotes.size());
		model.put("quoteTotal", quoteTotal);
		
		/*TICKET REPORT*/
		
		ReportFilter reportFilter = new ReportFilter();
		reportFilter.setStartDate(reportStartDate);
		reportFilter.setEndDate(reportEndDate);
		List<TicketReport> ticketReports = this.webJaguar.getTicketReport(reportFilter);
		Integer nTickets = 0;
		List<RangeReport> ticketRangeReports = this.allocateReports(reportStartDate,reportEndDate,reportingPeriodGroup);
		
		for(TicketReport ticketReport: ticketReports) {
			for(RangeReport rr : ticketRangeReports) {
				if(ticketReport.getCreatedDate().compareTo(rr.getStartDate())>=0 && ticketReport.getCreatedDate().compareTo(rr.getEndDate())<=0) {
					rr.setCount(rr.getCount() + ticketReport.getCount());
					break;
				}
			}
			nTickets += ticketReport.getCount();
		}
		
		model.put("ticketReports", ticketRangeReports);
		model.put("nTickets", nTickets);
		
		/*CUSTOMER REPORT*/
		
		CustomerSearch customerSearch = new CustomerSearch();
		customerSearch.setStartDate(reportStartDate);
		customerSearch.setEndDate(reportEndDate);
		List<Report> customerReports = this.webJaguar.getCustomerReportDaily(reportFilter);
		Integer nCustomers = 0;
		List<RangeReport> customerRangeReports = this.allocateReports(reportStartDate,reportEndDate,reportingPeriodGroup);
		
		for(Report customerReport: customerReports) {
			for(RangeReport rr : customerRangeReports) {
				if(customerReport.getDate().compareTo(rr.getStartDate())>=0 && customerReport.getDate().compareTo(rr.getEndDate())<=0) {
					rr.setCount(rr.getCount() + customerReport.getNewUser());
					break;
				}
			}
			nCustomers += customerReport.getNewUser();
		}

		model.put("customerReports", customerRangeReports);
		model.put("nCustomers", nCustomers);
		
		/*SHIPPED ORDER REPORT*/
		
		OrderSearch orderShippedSearch = new OrderSearch();
		orderShippedSearch.setStartDate(reportStartDate);
		orderShippedSearch.setEndDate(reportEndDate);
		orderShippedSearch.setLimit(Integer.MAX_VALUE);
		orderShippedSearch.setStatus("s");
		List<Order> shippedOrders = this.webJaguar.getOrdersList(orderShippedSearch);
		Double shippedOrderTotal = 0.0;
		List<RangeReport> shippedOrderReports = this.allocateReports(reportStartDate,reportEndDate,reportingPeriodGroup);
		for(Order order: shippedOrders) {
			for(RangeReport rr : shippedOrderReports) {
				if(order.getDateOrdered().compareTo(rr.getStartDate())>=0 && order.getDateOrdered().compareTo(rr.getEndDate())<=0) {
					rr.setCount(rr.getCount() + 1);
					rr.setTotal(rr.getTotal() + order.getGrandTotal());
					break;
				}
			}
			shippedOrderTotal = shippedOrderTotal + order.getGrandTotal();
		}
		
		model.put("shippedOrderReports", shippedOrderReports);
		model.put("nShippedOrders", shippedOrders.size());
		model.put("shippedOrderTotal", shippedOrderTotal);
		
		/*PROCESSING ORDER REPORT*/
		
		OrderSearch orderPrsSearch = new OrderSearch();
		orderPrsSearch.setStartDate(reportStartDate);
		orderPrsSearch.setEndDate(reportEndDate);
		orderPrsSearch.setLimit(Integer.MAX_VALUE);
		orderPrsSearch.setStatus("pr");
		List<Order> processingOrders = this.webJaguar.getOrdersList(orderPrsSearch);
		Double processingOrderTotal = 0.0;
		List<RangeReport> processingOrderReports = this.allocateReports(reportStartDate,reportEndDate,reportingPeriodGroup);
		for(Order order: processingOrders) {
			for(RangeReport rr : processingOrderReports) {
				if(order.getDateOrdered().compareTo(rr.getStartDate())>=0 && order.getDateOrdered().compareTo(rr.getEndDate())<=0) {
					rr.setCount(rr.getCount() + 1);
					rr.setTotal(rr.getTotal() + order.getGrandTotal());
					break;
				}
			}
			processingOrderTotal = processingOrderTotal + order.getGrandTotal();
		}
		
		model.put("processingOrderReports", processingOrderReports);
		model.put("nProcessingOrders", processingOrders.size());
		model.put("processingOrderTotal", processingOrderTotal);
		
		/*PENDING ORDER REPORT*/
		
		OrderSearch orderReqAttnSearch = new OrderSearch();
		orderReqAttnSearch.setStartDate(reportStartDate);
		orderReqAttnSearch.setEndDate(reportEndDate);
		orderReqAttnSearch.setLimit(Integer.MAX_VALUE);
		orderReqAttnSearch.setStatus("p");
		List<Order> pendingOrders = this.webJaguar.getOrdersList(orderReqAttnSearch);
		Double pendingOrderTotal = 0.0;
		List<RangeReport> pendingOrderReports = this.allocateReports(reportStartDate,reportEndDate,reportingPeriodGroup);
		for(Order order: pendingOrders) {
			for(RangeReport rr : pendingOrderReports) {
				if(order.getDateOrdered().compareTo(rr.getStartDate())>=0 && order.getDateOrdered().compareTo(rr.getEndDate())<=0) {
					rr.setCount(rr.getCount() + 1);
					rr.setTotal(rr.getTotal() + order.getGrandTotal());
					break;
				}
			}			
			pendingOrderTotal = pendingOrderTotal + order.getGrandTotal();
		}
		
		model.put("pendingOrderReports", pendingOrderReports);
		model.put("nPendingOrders", pendingOrders.size());
		model.put("pendingOrderTotal", pendingOrderTotal);
		
		/*ABANDONED CART REPORT*/
		
		AbandonedShoppingCartSearch abandonedShoppingCartSearch = new AbandonedShoppingCartSearch();
		abandonedShoppingCartSearch.setStartDate(reportStartDate);
		abandonedShoppingCartSearch.setEndDate(reportEndDate);
		List<AbandonedShoppingCart> abandonedCarts = this.webJaguar.getAbandonedShoppingCartList(abandonedShoppingCartSearch);
		Double abandonedCartTotal = 0.0;
		List<RangeReport> abandonedCartReports = this.allocateReports(reportStartDate,reportEndDate,reportingPeriodGroup);
		for(AbandonedShoppingCart cart : abandonedCarts) {
			for(RangeReport rr : abandonedCartReports) {
				if(cart.getCreatedDate().compareTo(rr.getStartDate())>=0 && cart.getCreatedDate().compareTo(rr.getEndDate())<=0) {
					rr.setCount(rr.getCount() + 1);
					rr.setTotal(rr.getTotal() + cart.getTotalAmount());
					break;
				}
			}
			abandonedCartTotal = abandonedCartTotal + cart.getTotalAmount();
		}
		
		model.put("abandonedCartReports", abandonedCartReports);
		model.put("nAbandonedCarts", abandonedCarts.size());
		model.put("abandonedCartTotal", abandonedCartTotal);
		
		/*BEST SELLING PRODUCTS*/
		
		ReportFilter bestSellingProductFilter = new ReportFilter();
		bestSellingProductFilter.setStartDate(reportStartDate);
		bestSellingProductFilter.setEndDate(reportEndDate);
		bestSellingProductFilter.setLimit(5);
		List<ProductReport> bestSellingProducts = this.webJaguar.getBestSellingProducts(bestSellingProductFilter);
		model.put("bestSellingProducts", bestSellingProducts);
		
		/*TOP ABANDONED PRODUCTS*/
		
		ReportFilter topAbandonedProductFilter = new ReportFilter();
		topAbandonedProductFilter.setStartDate(reportStartDate);
		topAbandonedProductFilter.setEndDate(reportEndDate);
		topAbandonedProductFilter.setLimit(5);
		List<ProductReport> topAbandonedProducts = this.webJaguar.getTopAbandonedProducts(topAbandonedProductFilter);
		model.put("topAbandonedProducts", topAbandonedProducts);
		
		/* TOP SHIPPED CITIES */
		
		ReportFilter topShippedCitiesFilter = new ReportFilter();
		topShippedCitiesFilter.setStartDate(reportStartDate);
		topShippedCitiesFilter.setEndDate(reportEndDate);
		topShippedCitiesFilter.setLimit(5);
		List<CityReport> topShippedCities = this.webJaguar.getTopShippedCities(topShippedCitiesFilter);
		model.put("topShippedCities", topShippedCities);
		
		/* TOP SHIPPED STATES */
		
		ReportFilter topShippedStatesFilter = new ReportFilter();
		topShippedStatesFilter.setStartDate(reportStartDate);
		topShippedStatesFilter.setEndDate(reportEndDate);
		topShippedStatesFilter.setLimit(5);
		List<StateReport> topShippedStates = this.webJaguar.getTopShippedStates(topShippedStatesFilter);
		model.put("topShippedStates", topShippedStates);
		
		/* TOP SALE REPS */
		
		ReportFilter topSalesRepsFilter = new ReportFilter();
		topSalesRepsFilter.setStartDate(reportStartDate);
		topSalesRepsFilter.setEndDate(reportEndDate);
		topSalesRepsFilter.setLimit(5);
		List<SalesRepReport> topSalesReps = this.webJaguar.getTopSalesReps(topSalesRepsFilter);
		model.put("topSalesReps", topSalesReps);

		/* EXTRA COMPARISON REPORT */
		if(reportingPeriod.equals("this_year")) {
			OrderSearch lastYearOrderSearch = new OrderSearch();
			
			Calendar lastYearStartDateCal = (Calendar)startDateCal.clone();
			lastYearStartDateCal.add(Calendar.YEAR, -1);
			Date lastYearReportStartDate = lastYearStartDateCal.getTime();
			lastYearOrderSearch.setStartDate(lastYearReportStartDate);

			Calendar lastYearEndDateCal = (Calendar)endDateCal.clone();
			lastYearEndDateCal.add(Calendar.YEAR, -1);
			Date lastYearReportEndDate = lastYearEndDateCal.getTime();
			lastYearOrderSearch.setEndDate(lastYearReportEndDate);
			
			lastYearOrderSearch.setLimit(Integer.MAX_VALUE);
			lastYearOrderSearch.setOffset(0);
			lastYearOrderSearch.setStatus("pprpss");
			List<Order> lastYearOrders = this.webJaguar.getOrdersList(lastYearOrderSearch);

			List<RangeReport> lastYearOrderReports = this.allocateReports(lastYearReportStartDate,lastYearReportEndDate,reportingPeriodGroup);
	
			for(Order order: lastYearOrders) {
				for(RangeReport rr : lastYearOrderReports) {
					if(order.getDateOrdered().compareTo(rr.getStartDate())>=0 && order.getDateOrdered().compareTo(rr.getEndDate())<=0) {
						rr.setCount(rr.getCount() + 1);
						rr.setTotal(rr.getTotal() + order.getGrandTotal());
						break;
					}
				}
				orderTotal = orderTotal + order.getGrandTotal();
			}

			model.put("lastYearOrderReports", lastYearOrderReports);
		}
		return new ModelAndView("admin-responsive/dashboard", "model", model);
	}
	
	private List<RangeReport> allocateReports(Date startDate, Date endDate,String reportingPeriodGroup){
		Calendar startDateCal = Calendar.getInstance();
		startDateCal.setTime(startDate);
		startDateCal.set(Calendar.HOUR_OF_DAY, 0);
		startDateCal.set(Calendar.MINUTE, 0);
		startDateCal.set(Calendar.SECOND, 0);
		startDateCal.set(Calendar.MILLISECOND, 0);
		
		Calendar endDateCal = Calendar.getInstance();
		endDateCal.setTime(endDate);
		endDateCal.set(Calendar.HOUR_OF_DAY, 0);
		endDateCal.set(Calendar.MINUTE, 0);
		endDateCal.set(Calendar.SECOND, 0);
		endDateCal.set(Calendar.MILLISECOND, 0);
		
		List<RangeReport> ret = new ArrayList<RangeReport>();

		if(reportingPeriodGroup.equals("d")) {
			Calendar cal = startDateCal;
			while(cal.compareTo(endDateCal)<=0) {
				RangeReport rr = new RangeReport();
				rr.setReportingPeriodGroup(reportingPeriodGroup);
				rr.setStartDate(cal.getTime());
				cal.add(Calendar.DATE, 1);
				Calendar end = (Calendar)cal.clone();
				end.add(Calendar.MILLISECOND, -1);
				rr.setEndDate(end.getTime());
				ret.add(rr);
			}	
		}
		if(reportingPeriodGroup.equals("w")) {
			Calendar cal = startDateCal;
			while(cal.compareTo(endDateCal)<=0) {
				int day_of_week = cal.get(Calendar.DAY_OF_WEEK);
				int delta = Calendar.SATURDAY - day_of_week;
				Calendar end = (Calendar)cal.clone();
				end.add(Calendar.DATE, delta);
				
				if(end.compareTo(endDateCal)<=0) {
					RangeReport rr = new RangeReport();
					rr.setReportingPeriodGroup(reportingPeriodGroup);
					rr.setStartDate(cal.getTime());
					rr.setEndDate(end.getTime());
					cal.add(Calendar.DATE,delta+1);
					ret.add(rr);
					continue;
				}
				
				RangeReport rr = new RangeReport();
				rr.setReportingPeriodGroup(reportingPeriodGroup);
				rr.setStartDate(cal.getTime());
				rr.setEndDate(endDateCal.getTime());
				ret.add(rr);
				break;
			}
		}
		if(reportingPeriodGroup.equals("m")) {
			Calendar cal = startDateCal;
			while(cal.compareTo(endDateCal)<=0) {
				RangeReport rr = new RangeReport();
				rr.setReportingPeriodGroup(reportingPeriodGroup);
				rr.setStartDate(cal.getTime());
				cal.add(Calendar.MONTH, 1);
				Calendar end = (Calendar)cal.clone();
				end.add(Calendar.MILLISECOND, -1);
				rr.setEndDate(end.getTime());
				ret.add(rr);
			}
		}
		return ret;
	}

}