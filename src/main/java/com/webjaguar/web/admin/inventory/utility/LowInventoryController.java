package com.webjaguar.web.admin.inventory.utility;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;

public class LowInventoryController implements Controller {
	
	private static WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		
		if ((Boolean) gSiteConfig.get("gINVENTORY")) {
			InventoryUtility inventory = new InventoryUtility(siteConfig);
			inventory.getLowInventory();
		}
		
		return new ModelAndView("admin/catalog/category/list", "model", myModel);
	}
}