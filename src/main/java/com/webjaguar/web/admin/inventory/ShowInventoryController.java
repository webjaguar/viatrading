/* Copyright 2005, 2008 Advanced E-Media Solutions
 * author: Shahin Naji
 */

package com.webjaguar.web.admin.inventory;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Product;

public class ShowInventoryController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		Product product = this.webJaguar.getProductById( this.webJaguar.getProductIdBySku( request.getParameter("sku") ), 0, false, null );

		if ( ServletRequestUtils.getBooleanParameter( request, "check", false ) ) {
			myModel.put("message", "In stock: " + product.getInventory());
		} else {
			if ( product != null && !product.isNegInventory() ) {
				if ( ServletRequestUtils.getIntParameter( request, "qty", -1 ) == -1 ) {
					myModel.put("message", "Invalid Quantity");
				} else if ( product.getInventory() != null && product.getInventory() <  Integer.parseInt( request.getParameter("qty")) ) {
					myModel.put("message", "In stock: " + product.getInventory());
				}		
			}
		}

        return new ModelAndView("admin/inventory/showInventory", "model", myModel);
	}
}