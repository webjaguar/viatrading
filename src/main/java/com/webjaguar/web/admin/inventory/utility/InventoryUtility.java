package com.webjaguar.web.admin.inventory.utility;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductSearch;

public class InventoryUtility {

	private static WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private static MailSender mailSender;
    public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }

	static Map<String, Configuration> siteConfig;
	static Map<String, Object> gSiteConfig;

	public InventoryUtility() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InventoryUtility(Map<String, Configuration> siteConfig) {
		this.siteConfig = siteConfig;
	}

	public List getLowInventory() {
		// TO get the list of low inventory
		ProductSearch productSearch = new ProductSearch();
		productSearch.setInventory("1");
		List<Product> productList = this.webJaguar.getProductList(productSearch);
		if (productList != null && productList.size() > 0) {
			notifyAdmin(productList);
		}
		return productList;
	}

	private void notifyAdmin(List<Product> productList) {
		// String subject, String message,
		// send email notification
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Low Inventory on " + siteConfig.get("SITE_URL").getValue());
		StringBuffer message = new StringBuffer();
		message.append("You have a low inventory of the following products:\n");
		message.append("\nProduct Sku\t Product Name\t Product ID\t On Hand/Available for Sale\n");
		for (Product product : productList) {
			message.append("\n" + StringUtils.rightPad(product.getSku(), 15) + "\t\t " + StringUtils.rightPad(product.getName(), 20) + "\t\t" + StringUtils.rightPad(product.getId().toString(), 10) + "\t\t" + StringUtils.rightPad(product.getInventory() + "/" + product.getInventoryAFS(), 15));
		}
		msg.setText("\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}