/* Copyright 2005, 2008 Advanced E-Media Solutions
 * author Shahin Naji
 */

package com.webjaguar.web.admin.inventory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.Product;


public class ExportController extends WebApplicationObjectSupport implements Controller {

	boolean gINVENTORY;
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	private MailSender mailSender;
    public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {

    	Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	gINVENTORY = (Boolean) gSiteConfig.get("gINVENTORY");
    	
		Map<String, Object> map = new HashMap<String, Object>();
		
		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			map.put("message", "excelfile.notWritable");
			return new ModelAndView("admin/inventory/export", map);			
		}
		File file[] = baseFile.listFiles();
		List<Map> exportedFiles = new ArrayList<Map>();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith("inventoryExport") & file[f].getName().endsWith(".xls")) {
				if (request.getParameter("__new") != null) { // check if new button was pressed
					file[f].delete();
				} else {
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					exportedFiles.add( fileMap );				
				}
			}
		}
		map.put("exportedFiles", exportedFiles);
    	
		// check if new button was pressed
		if (request.getParameter("__new") != null) {
			
			int productCount = this.webJaguar.productCount();
			int limit = 3000;
			
	    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
	    	String fileName = "/inventoryExport_" + dateFormatter.format(new Date()) + "";

	    	for (int offset=0, excel = 1; offset<productCount; excel++) {
		    	List productList = this.webJaguar.getProductExportList(limit, offset);
		    	int start = offset + 1;
		    	int end = (offset + limit) < productCount ? (offset + limit) : productCount;
		    	HSSFWorkbook wb = createWorkbook(productList, start + " to " + end);
		    	// Write the output to a file
		    	File newFile = new File(baseFile.getAbsolutePath() + fileName + excel + ".xls");
		        FileOutputStream fileOut = new FileOutputStream(newFile);
		        wb.write(fileOut);
		        fileOut.close();
		        HashMap<String, Object> fileMap = new HashMap<String, Object>();
		        fileMap.put("file", newFile);
				exportedFiles.add( fileMap );  		
		        offset = offset + limit;
	    	}
	        
			if (productCount > 0) {
				map.put("arguments", productCount);
				map.put("message", "inventoryexport.success");
				this.webJaguar.insertImportExportHistory( new ImportExportHistory("inventory", SecurityContextHolder.getContext().getAuthentication().getName(), false ));
			} else {
				map.put("message", "inventoryexport.empty");				
			}
			notifyAdmin( siteConfig, request);
		} // if    
		
		return new ModelAndView("admin/inventory/export", map);
	}
    
    private HSSFCell getCell(HSSFSheet sheet, int row, short col) {
    	return sheet.createRow(row).createCell(col);
    }
    
    private void setText(HSSFCell cell, String text) {
    	cell.setCellValue(text);
    }
    
    private HSSFWorkbook createWorkbook(List productList, String sheetName) throws Exception {
    	int numOfCategories = 1;
    	int numOfSupplieres = 1;
    	Iterator iter = productList.iterator();
    	while (iter.hasNext()) {
    		Product product = (Product) iter.next();
    		int catIdSize = product.getCatIds().size();
    		numOfCategories = (catIdSize > numOfCategories) ? catIdSize : numOfCategories; 
    		
    		int supplierIdSize = product.getSupplierIds().size();
    		numOfSupplieres = (supplierIdSize > numOfSupplieres) ? supplierIdSize : numOfSupplieres;
    	}
    	
    	HSSFWorkbook wb = new HSSFWorkbook();
    	HSSFSheet sheet = wb.createSheet(sheetName);
    	
    	// create the headers
		Map<String, Short> headerMap = new HashMap<String, Short>();
		short col = 0;		
		// sku
		headerMap.put("Sku", col++);
		// inventory
		if ( gINVENTORY ) headerMap.put("On PO", col++);
		if ( gINVENTORY ) headerMap.put("On SaleOrder", col++);
		if ( gINVENTORY ) headerMap.put("OnHand", col++);
		if ( gINVENTORY ) headerMap.put("Available For Sale", col++);
		
		// inventory extra information
		headerMap.put("AllowNegativeInventory", col++);
		headerMap.put("ShowNegativeInventory", col++);
		headerMap.put("LowInventoryAlert", col++);
		
		// print headers
		for (String headerName:headerMap.keySet()){
			setText(getCell(sheet, 0, headerMap.get(headerName).shortValue()), headerName); 
		}
		
		// suppliers
		short[] supplier = new short[numOfSupplieres];
		for (int i=0; i<numOfSupplieres; i++) {
			Short supplierColumn = col++;
			supplier[i] = supplierColumn;
			setText(getCell(sheet, 0, supplierColumn), "Supplier"); 
		}
		productList = this.webJaguar.getQuantityPending(productList);
    	iter = productList.iterator();
		HSSFCell cell = null;
		Object arglist[] = null;
		
		// create the rows
    	for (int row=2; iter.hasNext(); row++) {    		
    		Product product = (Product) iter.next();
        	setText(getCell(sheet, row, headerMap.get("Sku").shortValue()), product.getSku());
        	// set inventory
  		    if (product.getInventory() != null) {  		    
  		    	cell = getCell(sheet, row, headerMap.get("On PO").shortValue());
  		    	cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  		    	if(product.getPurchasePendingQty() != null) {
  	  		    	cell.setCellValue(product.getPurchasePendingQty());
  		    	}
  	  			cell = getCell(sheet, row, headerMap.get("OnHand").shortValue()); 
  	    		cell.setCellValue(product.getInventory());  
  		    	cell = getCell(sheet, row, headerMap.get("On SaleOrder").shortValue());
  		    	cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  		    	if(product.getOrderPendingQty() != null) {
  	  		    	cell.setCellValue(product.getOrderPendingQty());
  		    	}  	    		
  		    }
  		cell = getCell(sheet, row, headerMap.get("Available For Sale").shortValue());
    	if(product.getInventoryAFS() != null) {
  	    		cell.setCellValue(product.getInventoryAFS());
    		}
        	
        	setText(getCell(sheet, row, headerMap.get("AllowNegativeInventory").shortValue()), product.isNegInventory() ? "1" : null);
        	setText(getCell(sheet, row, headerMap.get("ShowNegativeInventory").shortValue()), product.isShowNegInventory() ? "1" : null);

        	// set low inventory
  		    if (product.getLowInventory() != null) {
  	  			cell = getCell(sheet, row, headerMap.get("LowInventoryAlert").shortValue()); 		    
  	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	    		cell.setCellValue(product.getLowInventory());           	  		    	
  		    }
  		    
  		    // set suppliers
  		    Iterator supplierIdIter = product.getSupplierIds().iterator();
    		int index = 0;
			while (supplierIdIter.hasNext()) {
	    		setText(getCell(sheet, row, supplier[index++]), supplierIdIter.next().toString());
			}
  		}	  
    	return wb;
    }
    
    private void notifyAdmin(Map<String, Configuration> siteConfig, HttpServletRequest request){
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	StringBuffer message = new StringBuffer();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
    	message.append("user "+accessUser.getUsername()+" has exported Inventory file");
    	SimpleMailMessage msg = new SimpleMailMessage();			
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Automated Export on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}