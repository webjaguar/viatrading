package com.webjaguar.web.admin.inventory.utility;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailSender;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.web.quartz.InventoryUpdate;

public class InventoryUpdateController implements Controller {

	private static WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private static MailSender mailSender;

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	static Map<String, Configuration> siteConfig;
	static Map<String, Object> gSiteConfig;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();

		InventoryUpdate invUpdate = new InventoryUpdate(siteConfig, webJaguar, mailSender);
		invUpdate.updateInventory();

		return null;
	}
}
