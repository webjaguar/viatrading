package com.webjaguar.web.admin.inventory;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.PurchaseOrder;
import com.webjaguar.model.PurchaseOrderLineItem;

@Controller
public class ShowPOSkus extends WebApplicationObjectSupport{
	@Autowired
	private WebJaguarFacade webJaguar;
	@RequestMapping(value="/admin/inventory/POskuList.jhtm", method = RequestMethod.GET)
	public ModelAndView skuList(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Map<String, Object> model = new HashMap<String, Object>();
		Integer poId = ServletRequestUtils.getIntParameter(request, "poId");
		
		PurchaseOrder PO = this.webJaguar.getPurchaseOrder(poId);
		
		if(PO != null){
			model.put("PONumber", PO.getPoNumber());
		    model.put("items", PO.getPoLineItems());
		}
		
		return new ModelAndView("admin/inventory/poSkuList", "model", model);
	}

}
