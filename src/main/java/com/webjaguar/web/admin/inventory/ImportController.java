/* Copyright 2005, 2008 Advanced E-Media Solutions
 * author Shahin Naji
 */

package com.webjaguar.web.admin.inventory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.Product;

public class ImportController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	String formView = "admin/inventory/import";
	boolean gINVENTORY;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {	
        
		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			return new ModelAndView("admin/inventory/import", "message", "excelfile.notWritable");	
		}    	
    	
		// check if upload button was pressed
		if (request.getParameter("__upload") != null) {
	    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	    	MultipartFile file = multipartRequest.getFile("file");
//	    	if (file != null && !file.isEmpty() && file.getContentType().equals("application/vnd.ms-excel")) {
	    	if (file != null && !file.isEmpty()) {
	    		File excel_file = new File(baseFile, "inventory_import.xls");
	    		file.transferTo(excel_file);
	    		file = null; // delete to save resources
		    	return processFile(excel_file, request);	    			
	    	} else {
	        	return new ModelAndView(formView, "message", "excelfile.invalid");	    		
	    	}
		}
    	
    	return new ModelAndView(formView);
    }
    
    private ModelAndView processFile(File excel_file, HttpServletRequest request) {
        List<Map> addedItems = new ArrayList<Map>();
        List<Map> updatedItems = new ArrayList<Map>();
        List<Map> invalidItems = new ArrayList<Map>();
		List<Product> importedProductsInventory = new ArrayList<Product>();	
        
    	Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
    	gINVENTORY = (Boolean) gSiteConfig.get( "gINVENTORY" );
        
		Map<String, Object> map = new HashMap<String, Object>();
		
		try {
			POIFSFileSystem fs =
	            new POIFSFileSystem(new FileInputStream(excel_file));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
		    
			HSSFSheet sheet = wb.getSheetAt(0);       	// first sheet
			HSSFRow row     = sheet.getRow(0);        	// first row
			Map<String, Short> header = new HashMap<String, Short>();

			
			if (wb.getNumberOfSheets() > 1) {
				// multiple sheets
				map.put("message", "excelfile.multipleSheets");
				return new ModelAndView(formView, map);					
			}
			if (row == null) {
				// first line should be the headers
				map.put("message", "excelfile.emptyHeaders");
				return new ModelAndView(formView, map);					
			}
			
		    Iterator iter = row.cellIterator();
		    
		    // get header names
			while (iter.hasNext()) {
				HSSFCell cell = (HSSFCell) iter.next();
				String headerName = "";
				try {
					headerName = cell.getStringCellValue().trim();					
				} catch (NumberFormatException e) {
					// do nothing
				}
				if (!headerName.equals( "" )) {
					if (header.put(headerName.toLowerCase(), cell.getCellNum()) != null) {
						// duplicate header
						//will be improved later for multiple supplier import
//						map.put("message", "excelfile.duplicateHeaders");
//						map.put("arguments", headerName);
//						return new ModelAndView(formView, map);													
					}					
				} 
			}
			
			// check required headers
			if (!header.containsKey("sku") || !header.containsKey("onhand")) { // sku, inventory columns are required
				return new ModelAndView(formView, "message", "excelfile.missingHeaders");
			}
			
			
			nextRow: for (int i=1; i<=sheet.getLastRowNum(); i++) {
				row = sheet.getRow(i);
				if (row == null) {
					continue;
				}
				
				// check if new or existing item
				String sku = null;
				sku = getStringValue(row, header.get("sku")).trim();
				if (sku.equals("")) sku = null;
				Map<String, Object> thisMap = new HashMap<String, Object>();
				thisMap.put("sku", sku);
				Integer existingId = this.webJaguar.getProductIdBySku(sku);
				if ( existingId == null ) {
					thisMap.put("rowNum", row.getRowNum()+1);
					thisMap.put("sku", sku);
					thisMap.put("reason", "SKU does not exist.");
					invalidItems.add(thisMap);
					continue nextRow;
				}

				
				if (invalidItems.size() == 0) { // no errors
					// save or update product
					Product product = this.webJaguar.getProductById(existingId, 0, false, null);
					if (product.getInventory() != null) { 				// existing item
						thisMap.put("id", existingId);
						product.setSku(sku);
						processRow(row, header, product, thisMap);						
						updatedItems.add(thisMap);						
					} else if (sku != null) {		// existing item
						product.setSku(sku);
						processRow(row, header, product, thisMap);					
						addedItems.add(thisMap);	
					} else { // no name
						continue nextRow;
					}
					importedProductsInventory.add(product);
				} // if no errors
			}					
		} catch (IOException e) {
			return new ModelAndView(formView, "message", "excelfile.invalid");
		} finally {
	    	// delete excel file
			excel_file.delete();	    			
		}
		
		if (invalidItems.size() > 0) { // has errors
			map.put("invalidItems", invalidItems);
			return new ModelAndView(formView, map);					
		} else {
			if (importedProductsInventory.size() == 0) { // no items to import
				return new ModelAndView(formView, "message", "excelfile.empty");
			} else {
				for (Product product : importedProductsInventory) {
					Inventory inventory = new Inventory();
					inventory.setSku(product.getSku());
					inventory = this.webJaguar.getInventory(inventory);
					InventoryActivity inventoryActivity = new InventoryActivity();	
					inventoryActivity.setSku(product.getSku());
					inventoryActivity.setInventory(product.getInventory());
					inventoryActivity.setinventoryAFS(product.getInventoryAFS());
					inventoryActivity.setBackToNull(product.isBackToNull());
					if( !inventoryActivity.isBackToNull() && inventoryActivity.getInventory() == null && inventory.getInventory() == null) {
						inventoryActivity.setBackToNull(true);
					}
					inventoryActivity.setAccessUserId(this.webJaguar.getAccessUser(request).getId());
					inventoryActivity.setNegInventory(product.isNegInventory());
					inventoryActivity.setShowNegInventory(product.isShowNegInventory());
					inventoryActivity.setLowInventory(product.getLowInventory());
					if(product.isBackToNull()) {
						inventoryActivity.setType("Changed to Null");
					}
					else {
						inventoryActivity.setType("Imported");
					}
    				if(product.getInventoryAFS() == null && inventoryActivity.getType().equals("Imported") && inventoryActivity.getInventory() != null) {
    					map.put("message", "excelfile.invalidOnHand");
						map.put("arguments", product.getSku());
						return new ModelAndView(formView, map);
    				}
					this.webJaguar.importInventory(inventoryActivity);
				}
				this.webJaguar.insertImportExportHistory(new ImportExportHistory("inventory", SecurityContextHolder.getContext().getAuthentication().getName(), true));
			}
		}
		
		map.put("updatedItems", updatedItems);
		map.put("addedItems", addedItems);
		return new ModelAndView("admin/inventory/importSuccess", map);
		
    }

    private Integer getIntegerValue(HSSFRow row, short cellnum) throws Exception {
		HSSFCell cell = row.getCell(cellnum);
    	Integer value = null;
    	
    	if (cell != null) {
    		switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = new Integer(cell.getStringCellValue());										
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Double cellValue = cell.getNumericCellValue();
					if (cellValue != cellValue.intValue()) {
						throw new Exception();	
					}
					value =  cellValue.intValue();
					break;
			} // switch
    	}
    	return value;
    }
    
    private String getStringValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		String value = "";

    	if (cell != null) {
			switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = cell.getStringCellValue();
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Integer intValue = null;
					try {
						intValue = getIntegerValue(row, cellnum);
					} catch (Exception e) {}
					if (intValue != null) {
						value = intValue.toString();
						break;
					}
					value = new Double(cell.getNumericCellValue()).toString();
					break;
			} // switch		
    	}
		return value;
    }
    
    private boolean getBooleanValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		boolean value = false;

    	if (cell != null) {
    		if (getStringValue(row, cellnum).trim().equals( "1" ))  {
    			value = true;
    		}
    	}
		return value;
    }    
    
    private void processRow(HSSFRow row, Map<String, Short> header, Product product, Map<String, Object> thisMap) {

    	StringBuffer comments = new StringBuffer();
	
		// get inventory
    	if (header.containsKey( "onhand" )) {
  		  try {
  			Integer inventory = Integer.parseInt(getStringValue(row, header.get("onhand")).trim());
  			if (inventory == null) {
  				product.setInventory( null);
  			} else {
  				product.setInventory( inventory);
  				product.setBackToNull(false);
  			}				
  		  } catch ( NumberFormatException e) {
  			  product.setInventory( null);
  			  product.setBackToNull(false);
  			  if(getStringValue(row, header.get("onhand")).trim().isEmpty())  {
    				product.setBackToNull(true);
  			  }
  			  comments.append("No OnHand ");
  		  }
  	    }
    	
    	if (header.containsKey( "available for sale" )) {
    		try {
    			Integer inventory = Integer.parseInt(getStringValue(row, header.get("available for sale")).trim());
    			if (inventory == null) {
    				product.setInventoryAFS( null);
    			} else {
    				product.setInventoryAFS( inventory);
    			}				
    		} catch ( NumberFormatException e) {
    			product.setInventoryAFS( null);
    			comments.append(" No Available For Sale");
    		}
    	}
    	
		// get negative inventory
		if (header.containsKey( "allownegativeinventory" )) {
			product.setNegInventory( getBooleanValue(row, header.get("allownegativeinventory")) );	
		}		
		
		// show negative inventory
		if (header.containsKey( "shownegativeinventory" )) {
			product.setShowNegInventory( getBooleanValue(row, header.get("shownegativeinventory")) );				
		}	
		

		// low inventory alert
		if (header.containsKey( "lowinventoryalert" )) {
		  try {
			Integer lowInventory = Integer.parseInt(getStringValue(row, header.get("lowinventoryalert")).trim());
			if (lowInventory == null || lowInventory <= 0) {
				product.setLowInventory( null);
			} else {
				product.setLowInventory( lowInventory);
			}				
		  } catch ( Exception e) {
			  product.setLowInventory( null);
		  }
	    }
        
		thisMap.put("comments", comments);
    }
}