/* Copyright 2005, 2007 Advanced E-Media Solutions
 * author: Shahin Naji
 */

package com.webjaguar.web.admin.inventory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.PurchaseOrder;
import com.webjaguar.model.PurchaseOrderSearch;
import com.webjaguar.model.PurchaseOrderStatus;
import com.webjaguar.model.Search;
import com.webjaguar.web.form.PurchaseOrderForm;

public class PurchaseOrderListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		PurchaseOrderSearch purchaseOrderSearch = getPurchaseOrderSearch( request ); 
		System.out.println("dt:: "+request.getParameter("dateType"));
		System.out.println("date:: "+request.getParameter("startDate"));
		System.out.println("dt obj:"+purchaseOrderSearch.getDateType());
		// set end date to end of day
		if (purchaseOrderSearch.getEndDate() != null) {
			purchaseOrderSearch.getEndDate().setTime(purchaseOrderSearch.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds	
		}
		
		// check if add button was pressed 
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("purchaseOrderForm.jhtm"));				
		}
		
		// check if options button was clicked
		if (request.getParameter("__poPtBacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					String pt = ServletRequestUtils.getStringParameter(request, "__po_pt");
					this.webJaguar.updatePoPtById(Integer.parseInt(ids[i]),pt);
				}
			}
		}		
		// check if options button was clicked
		if (request.getParameter("__statusPtBacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			String broadcast=ServletRequestUtils.getStringParameter(request,"__selected_id","Y");
			
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					String status = ServletRequestUtils.getStringParameter(request, "__po_status");
					this.webJaguar.updatePoStatusById(Integer.parseInt(ids[i]),status);
				}
			}
		}
		

		if (request.getParameter("__batchDueDate") != null) {
			int[] ids = ServletRequestUtils.getIntParameters(request, "__selected_id");
			DateFormat df2 = new SimpleDateFormat("MM/dd/yyyy[hh:mm a]");
			//Date date = null;
			if (ids != null) {
				PurchaseOrderForm purchaseOrderForm = new PurchaseOrderForm();
				for (int i = 0; i < ids.length; i++) {					
					PurchaseOrder PO = this.webJaguar.getPurchaseOrder(ids[i]);					
					PO.setDueDate(df2.parse(request.getParameter("PO_dueDate")));
					purchaseOrderForm.setPurchaseOrder(PO);
					this.webJaguar.updatePurchaseOrder( purchaseOrderForm ); 
				}
			}
		}
		
		if (request.getParameter("__batchUpdateDocks") != null) {
			int[] ids = ServletRequestUtils.getIntParameters(request, "__selected_id");
			
			if (ids != null) {
				PurchaseOrderForm purchaseOrderForm = new PurchaseOrderForm();

				for (int i = 0; i < ids.length; i++) {
					PurchaseOrder PO = this.webJaguar.getPurchaseOrder(ids[i]);
					PO.setDocks(request.getParameter("__docks"));
					purchaseOrderForm.setPurchaseOrder(PO);
					this.webJaguar.updatePurchaseOrder( purchaseOrderForm ); 
				}
			}
		}
		
		if (request.getParameter("__batchUpdateResc") != null) {
			int[] ids = ServletRequestUtils.getIntParameters(request, "__selected_id");
			
			if (ids != null) {
				PurchaseOrderForm purchaseOrderForm = new PurchaseOrderForm();

				for (int i = 0; i < ids.length; i++) {
					PurchaseOrder PO = this.webJaguar.getPurchaseOrder(ids[i]);
					PO.setResources(request.getParameter("__resources"));				
					purchaseOrderForm.setPurchaseOrder(PO);
					this.webJaguar.updatePurchaseOrder( purchaseOrderForm ); 
					System.out.println("updated res");

				}
			}
		}
		
		
		ServletContext context = request.getSession().getServletContext(); 
		HttpSession session = request.getSession();
		Map<String, Object> myModel = new HashMap<String, Object>();
				
		PagedListHolder purchaseOrderList = new PagedListHolder(this.webJaguar.getPurchaseOrderList( purchaseOrderSearch ));		
		
		purchaseOrderList.setPageSize(purchaseOrderSearch.getPageSize());
		purchaseOrderList.setPage(purchaseOrderSearch.getPage()-1);
		
		myModel.put("search", purchaseOrderSearch);
		myModel.put("purchaseOrders", purchaseOrderList);
		myModel.put("customShippingRateList", this.webJaguar.getCustomShippingRateList( false, true ));
		if (siteConfig.get("CUSTOM_SHIPPING_CONTACT_INFO").getValue().endsWith("true")) {
			myModel.put("customShippingContact", this.webJaguar.getCustomShippingContactList(new Search()));
		}
		myModel.put("poPTList", this.webJaguar.getPoPTList());
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null) 
			return new ModelAndView("admin-responsive/inventory/purchaseOrderList", "model", myModel);
        return new ModelAndView("admin/inventory/purchaseOrderList", "model", myModel);
	}
	
	private PurchaseOrderSearch getPurchaseOrderSearch(HttpServletRequest request) {
		PurchaseOrderSearch purchaseOrderSearch = (PurchaseOrderSearch) request.getSession().getAttribute( "purchaseOrderSearch" );
		if (purchaseOrderSearch == null) {
			purchaseOrderSearch = new PurchaseOrderSearch();
			request.getSession().setAttribute( "purchaseOrderSearch", purchaseOrderSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			purchaseOrderSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}	
		
		// PurchaseOrder Number
		if (request.getParameter("po_number") != null) {
			purchaseOrderSearch.setPoNumber(ServletRequestUtils.getStringParameter( request, "po_number", "" ));
		}
		
		// order By
		if (request.getParameter("order_by") != null) {
			purchaseOrderSearch.setOrderBy(ServletRequestUtils.getStringParameter( request, "order_by", "" ));
		}
		
		// supplier
		if (request.getParameter("supplier") != null) {
			purchaseOrderSearch.setSupplier(ServletRequestUtils.getStringParameter( request, "supplier", "" ));
		}		
		
		// status
		if (request.getParameter("status") != null) {
			purchaseOrderSearch.setStatus(ServletRequestUtils.getStringParameter( request, "status", "" ));
		}	
		
		// Sku
		if (request.getParameter("sku") != null) {
			purchaseOrderSearch.setSku( ServletRequestUtils.getStringParameter( request, "sku", null ));
		}
		
		// shipVia
		if (request.getParameter("shipVia") != null) {
			purchaseOrderSearch.setShipVia( ServletRequestUtils.getStringParameter( request, "shipVia", null ));
		}
		
		// contactInfo
		if (request.getParameter("contactInfo") != null) {
			purchaseOrderSearch.setContactInfo( ServletRequestUtils.getStringParameter( request, "contactInfo", null ));
		}
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				purchaseOrderSearch.setStartDate( df.parse( request.getParameter("startDate")) );
			} catch (ParseException e) {
				purchaseOrderSearch.setStartDate(null);
	        }
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				purchaseOrderSearch.setEndDate( df.parse( request.getParameter("endDate")) );
			} catch (ParseException e) {
				purchaseOrderSearch.setEndDate(null);
	        }
		}
		
		// Date Type
		if (request.getParameter("dateType") != null 
				&& (purchaseOrderSearch.getStartDate() != null || purchaseOrderSearch.getEndDate() != null)) {
			purchaseOrderSearch.setDateType(ServletRequestUtils.getStringParameter(request, "dateType", ""));
		}
				
		//PT
		if (request.getParameter("pt") != null) {
			purchaseOrderSearch.setPt( ServletRequestUtils.getStringParameter( request, "pt", "" ));
		}
				
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				purchaseOrderSearch.setPage( 1 );
			} else {
				purchaseOrderSearch.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				purchaseOrderSearch.setPageSize( 10 );
			} else {
				purchaseOrderSearch.setPageSize( size );				
			}
		}
		
		return purchaseOrderSearch;
	}	

}
