/* Copyright 2006 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 12.19.2007
 */

package com.webjaguar.web.admin.inventory;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Layout;
import com.webjaguar.model.PurchaseOrder;
import com.webjaguar.model.Supplier;
import com.webjaguar.web.form.PurchaseOrderForm;

public class PurchaseOrderController extends AbstractCommandController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public PurchaseOrderController() {
		setCommandClass(PurchaseOrderForm.class);
	}

    public ModelAndView handle(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors)
        throws Exception {
    	Map<String, Object> myModel = new HashMap<String, Object>();
    	Integer poId = org.springframework.web.bind.ServletRequestUtils.getIntParameter( request, "poId", -1 );
    	PurchaseOrder purchaseOrder = this.webJaguar.getPurchaseOrder( poId );
    	
    	if ( purchaseOrder != null ) {
    		Supplier supplier = this.webJaguar.getSupplierById( purchaseOrder.getSupplierId() );
    		purchaseOrder.setSupplier( supplier );
        	myModel.put("purchaseOrder", purchaseOrder);
        	
        	Layout layout = this.webJaguar.getSystemLayout("purchaseOrder", "", request);
        	myModel.put("poLayout", layout);
    		
    	}
    	
		return new ModelAndView("admin/inventory/poPrint", myModel);
    }
}