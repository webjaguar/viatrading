/* Copyright 2005, 2007 Advanced E-Media Solutions
 * Author: Shahin Naji
 */

package com.webjaguar.web.admin.inventory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;


import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.dao.DataAccessException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import asiProduct.PriceGrid;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.Price;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.PurchaseOrder;
import com.webjaguar.model.PurchaseOrderLineItem;
import com.webjaguar.model.Search;
import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.asi.asiAPI.Product.Variants.Variant;
import com.webjaguar.web.domain.Utilities;
import com.webjaguar.web.form.PurchaseOrderForm;

public class PurchaseOrderWizardController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
    Map<String, Configuration> siteConfig;
    
	public PurchaseOrderWizardController() {
		setCommandName("purchaseOrderForm");
		setCommandClass(PurchaseOrderForm.class);
		setPages(new String[] {"admin/inventory/poFormStep1", "admin/inventory/poFormStep2"});
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
		
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy[hh:mm a]", RequestContextUtils.getLocale( request ) );
		// does not allow empty date and 20 characters required
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, false, 20 ) );	
	
		//allow empty DUE DATE
		binder.registerCustomEditor( Date.class, "purchaseOrder.dueDate", new CustomDateEditor( df, true, 20 ) );	
	}
	
    public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {	
    	Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );	
    	Map siteConfig = (Map) request.getAttribute( "siteConfig" );
    	PurchaseOrderForm purchaseOrderForm = (PurchaseOrderForm) command;
    	purchaseOrderForm.getPurchaseOrder().setSubTotal(updateLineItems(request, purchaseOrderForm));
    	if ( purchaseOrderForm.isNewPurchaseOrder() ){
    		this.webJaguar.insertPurchaseOrder( purchaseOrderForm );
    	} else {
    		for ( PurchaseOrderLineItem lineItem : purchaseOrderForm.getPurchaseOrder().getPoLineItems()) {
				lineItem.setPoId( purchaseOrderForm.getPurchaseOrder().getPoId() );			
				PurchaseOrderLineItem oldLineItem = this.webJaguar.getLineItemByPOId(lineItem);
				if(lineItem.getQty() != oldLineItem.getQty()){
					Inventory lineInventory = new Inventory(); 	
					lineInventory.setSku(lineItem.getSku());
					lineInventory.setInventoryAFS(lineItem.getQty()-oldLineItem.getQty());
					this.webJaguar.updateInventory(lineInventory);
				}
    		}
    		this.webJaguar.updatePurchaseOrder( purchaseOrderForm );    		
    	}

    	return new ModelAndView(new RedirectView("../inventory"));
    }
 
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
    	throws Exception {    
    	PurchaseOrderForm purchaseOrderForm = (PurchaseOrderForm) command;
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		if ( request.getAttribute( "accessUser" ) != null ) {
			AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
			myModel.put("accessUser", accessUser.getUsername() );
			myModel.put("userList", this.webJaguar.getUserPrivilegeList(null) );
			
			if ( purchaseOrderForm.isNewPurchaseOrder() )
				purchaseOrderForm.getPurchaseOrder().setOrderBy( accessUser.getUsername() );
		} 
		
		// get list of suppliers
		Search search = null;
		if(page == 0){
			if ( purchaseOrderForm.getPurchaseOrder().getOrderId() != null && purchaseOrderForm.isNewPurchaseOrder() )
				myModel.put("supplierlist",this.webJaguar.getProductSupplierListByOrderId(purchaseOrderForm.getPurchaseOrder().getOrderId(), "filter"));
			else
				myModel.put("supplierlist",this.webJaguar.getSuppliers(search));
			myModel.put("customShippingRateList", this.webJaguar.getCustomShippingRateList( false, true ));
			if (siteConfig.get("CUSTOM_SHIPPING_CONTACT_INFO").getValue().endsWith("true")) {
				myModel.put("customShippingContact", this.webJaguar.getCustomShippingContactList(new Search()));
			}
		}
		if (page == 1){
			myModel.put("now",new Date());
			if ( purchaseOrderForm.getPurchaseOrder().getOrderId() == null )
				myModel.put("skus",this.webJaguar.getProductSkuBySId( purchaseOrderForm.getPurchaseOrder().getSupplierId() ));
			myModel.put("messages", this.webJaguar.getSiteMessageList());
			myModel.put("customShippingContact", this.webJaguar.getCustomShippingContactList(new Search()));
			if ( !purchaseOrderForm.isNewPurchaseOrder() ) {
				myModel.put("poStatusHistory", this.webJaguar.getPurchaseOrderStatusHistory( purchaseOrderForm.getPurchaseOrder().getPoId() ));
			}
			// update lineItem
			purchaseOrderForm.getPurchaseOrder().setSubTotal(updateLineItems(request, purchaseOrderForm));
			
			if ( request.getParameter( "__sku" ) != null && !request.getParameter( "__sku" ).equals( "" ) && purchaseOrderForm.getOrder() == null ) {
				Supplier supplier = this.webJaguar.getProductSupplierBySIdSku( request.getParameter( "__sku" ) ,purchaseOrderForm.getPurchaseOrder().getSupplierId() );
				PurchaseOrderLineItem p = new PurchaseOrderLineItem();
				p.setSku( request.getParameter( "__sku" ) );
				if ( supplier != null ) {
					p.setCost( supplier.getPrice() );
					p.setSupplierSku( supplier.getSupplierSku() );
				}
				if(siteConfig.get("COST_TIERS").getValue().equals("true")) {
					if(p.getCost() == null) {
						Integer productId = this.webJaguar.getProductIdBySku(p.getSku());
						Product product = this.webJaguar.getProductById(productId, request);
						p.setCost(product.getCost1());
						p.setProductCostTier(true);
					}
	
					if(p.getCost() !=null){
						purchaseOrderForm.getPurchaseOrder().addPOInsertedLineItem( p );
					} else {
						purchaseOrderForm.setErrorMessage( "purchaseOrderForm.noCost" );
					}
				} else {				
					purchaseOrderForm.getPurchaseOrder().addPOInsertedLineItem( p );
				}
			}
			if ( request.getParameter( "__nonSupplierSKU" ) != null && !request.getParameter( "__nonSupplierSKU" ).equals( "" ) && purchaseOrderForm.getOrder() == null ) {
				List<Supplier> supplier = this.webJaguar.getProductSupplierListBySku(request.getParameter( "__nonSupplierSKU" ));
				Supplier supplierInProcess = this.webJaguar.getSupplierById(purchaseOrderForm.getPurchaseOrder().getSupplierId());
				if ( supplierInProcess != null && supplier.isEmpty()) {
					PurchaseOrderLineItem p = new PurchaseOrderLineItem();
					p.setSku( request.getParameter( "__nonSupplierSKU" ) );
					p.setCost( 0.00 );
					p.setSupplierSku( supplierInProcess.getSupplierSku() );
					purchaseOrderForm.getPurchaseOrder().addPOInsertedLineItem( p );
				} else {
					purchaseOrderForm.setErrorMessage( "purchaseOrderForm.otherSupplierError" );
				}
			}
			
			if (siteConfig.get("SUPPLIER_MULTI_ADDRESS").getValue().endsWith("true")) {
				myModel.put("supplierAddressList", this.webJaguar.getAddressListBySupplierId(purchaseOrderForm.getPurchaseOrder().getSupplierId()));
			}
			
			// display multiple shipping address, if decorative sku is available
			Set<String> itemGroups = null;
			for(LineItem lineItem : purchaseOrderForm.getPurchaseOrder().getPoLineItems()){
				if(lineItem.getItemGroup() != null && lineItem.isItemGroupMainItem()) {
					if(itemGroups == null) {
						itemGroups = new HashSet<String>();
					}
					itemGroups.add(lineItem.getItemGroup());
				}
			}
			if(itemGroups != null) {
				List<Address> shipToAddressList = new ArrayList<Address>();
				
				Address defaultShippingAddress = new Address();
				defaultShippingAddress.setId(-1);
				defaultShippingAddress.setCompany(((Configuration) siteConfig.get("COMPANY_NAME")).getValue());
				defaultShippingAddress.setAddr1(((Configuration) siteConfig.get("COMPANY_ADDRESS1")).getValue());
				defaultShippingAddress.setAddr2(((Configuration) siteConfig.get("COMPANY_ADDRESS2")).getValue());
				defaultShippingAddress.setCity(((Configuration) siteConfig.get("COMPANY_CITY")).getValue());
				defaultShippingAddress.setStateProvince(((Configuration) siteConfig.get("COMPANY_STATE_PROVINCE")).getValue());
				defaultShippingAddress.setCountry(((Configuration) siteConfig.get("COMPANY_COUNTRY")).getValue());
				defaultShippingAddress.setZip(((Configuration) siteConfig.get("COMPANY_ZIPCODE")).getValue());
				defaultShippingAddress.setPhone(((Configuration) siteConfig.get("COMPANY_PHONE")).getValue());
				defaultShippingAddress.setFax(((Configuration) siteConfig.get("COMPANY_FAX")).getValue());
				defaultShippingAddress.setEmail(((Configuration) siteConfig.get("CONTACT_EMAIL")).getValue());
				shipToAddressList.add(defaultShippingAddress);
				
				// supplier for decorative skus
				if(purchaseOrderForm.getOrder() != null && purchaseOrderForm.getOrder().getLineItems() != null) {
					for(LineItem lineItem : purchaseOrderForm.getOrder().getLineItems()){
						if(lineItem.getItemGroup() != null && !lineItem.isItemGroupMainItem() && itemGroups.contains(lineItem.getItemGroup())) {
							List<Address> addressList = this.webJaguar.getAddressListBySupplierId(lineItem.getSupplier().getId());
							if(addressList != null){
								shipToAddressList.addAll(addressList);
							}
						}
					}
				}
				myModel.put("shipToAddressList", shipToAddressList);
			}
		}
		
		map.put( "model", myModel );
		return map;
    }
    
    protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception
	{
    	PurchaseOrderForm purchaseOrderForm = (PurchaseOrderForm) command;	
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
    	
		switch ( page )
		{
		case 0:
			if ( purchaseOrderForm.getPurchaseOrder().getSupplierId() != null ) {
				Supplier supplier = this.webJaguar.getSupplierById( purchaseOrderForm.getPurchaseOrder().getSupplierId() );
				if ( purchaseOrderForm.isNewPurchaseOrder() ) {	
					if ( ServletRequestUtils.getBooleanParameter(request, "dropship" , false )) {
						// customer address
						purchaseOrderForm.getPurchaseOrder().setShippingAddress( getDropShipAddress(purchaseOrderForm.getOrder()) );
					} else {
						// company address
						purchaseOrderForm.getPurchaseOrder().setShippingAddress( getCompanyAddress() );
					}		
					// supplier address
					purchaseOrderForm.getPurchaseOrder().setBillingAddress( supplier.getFullAddressToTextArea() );
				}
				purchaseOrderForm.getPurchaseOrder().setSupplier( supplier );
				purchaseOrderForm.getPurchaseOrder().setTo(purchaseOrderForm.getPurchaseOrder().getSupplier().getAddress().getEmail());
				purchaseOrderForm.getPurchaseOrder().setFrom(((Configuration) siteConfig.get("CONTACT_EMAIL")).getValue());
			}
			if ( purchaseOrderForm.getOrder() != null ) {
				// populate lineItems
				initPurchaseOrderForm(purchaseOrderForm, siteConfig, request);
			}
			break;
		case 1:
			if ( request.getParameter( "__update_status" ) != null ) {
				PurchaseOrder updatedPO = this.webJaguar.getPurchaseOrder( ServletRequestUtils.getIntParameter(request, "poId" , -1 ) );
	    		if(updatedPO.getStatus() == null || !updatedPO.getStatus().equals("rece")){
	    			purchaseOrderForm.getPurchaseOrder().setStatus( purchaseOrderForm.getPurchaseOrderStatus().getStatus() );
					purchaseOrderForm.getPurchaseOrderStatus().setComments( ( request.getParameter( "__status_comment" ) == null ) ? null : request.getParameter( "__status_comment" ) );
					if(request.getAttribute( "accessUser" ) != null) {
						purchaseOrderForm.getPurchaseOrder().setAccessUserId( ((AccessUser) request.getAttribute( "accessUser" )).getId()  );
						purchaseOrderForm.getPurchaseOrderStatus().setUpdatedBy( ((AccessUser) request.getAttribute( "accessUser" )).getUsername() );
					}
					if ( purchaseOrderForm.getPurchaseOrderStatus().isSendEmail()) {
						try {
							sendEmail(siteConfig, request, purchaseOrderForm.getPurchaseOrder());
						} catch (Exception e) {
							purchaseOrderForm.setErrorMessage( "email.exception" );
							purchaseOrderForm.getPurchaseOrderStatus().setComments(getApplicationContext().getMessage("email.exception", new Object[0], RequestContextUtils.getLocale(request)));
						}
					}
					try {
						boolean inventoryHistory = false;
						if(siteConfig.get("INVENTORY_HISTORY").getValue().equals("true")) {
							inventoryHistory = true;
						}
						this.webJaguar.insertPurchaseOrderStatus( purchaseOrderForm, inventoryHistory );
					} catch (DataAccessException e) {
						purchaseOrderForm.setErrorMessage("inventory.error");
					}
	    		}				
	    	}
			break;	
		}
	}
    
    protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors)
     throws Exception {
    	return new ModelAndView(new RedirectView("../inventory"));
    }

    protected void validatePage(Object command, Errors errors, int page, boolean finish) {
    	PurchaseOrderForm purchaseOrderForm = (PurchaseOrderForm) command;
		switch (page) {
		case 0:
			// check if there is duplicate purchase order
			Integer poId = this.webJaguar.getPurchaseOrderIdByName( purchaseOrderForm.getPurchaseOrder().getPoNumber() );
			if ( poId != null )
			{ // purchase order exists
				if ( purchaseOrderForm.isNewPurchaseOrder() )
				{
					errors.rejectValue( "purchaseOrder.poNumber", "PURCHASE_ORDER_ALREADY_EXISTS", "A purchase order using this number already exists." );
				}
				else if ( poId.compareTo( purchaseOrderForm.getPurchaseOrder().getPoId() ) != 0 )
				{ // not the same purchase order
					errors.rejectValue( "purchaseOrder.poNumber", "PURCHASE_ORDER_ALREADY_EXISTS", "A purchase order using this number already exists." );
				}
			}
			if (purchaseOrderForm.getPurchaseOrder().getSupplierId() == null || purchaseOrderForm.getPurchaseOrder().getSupplierId().equals( "-" )) {
				errors.rejectValue("purchaseOrder.supplierId", "form.required", "required");
			}	
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "purchaseOrder.poNumber", "form.required", "required");	
		}
    }
    
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );	
		PurchaseOrderForm purchaseOrderForm = new PurchaseOrderForm();
		if ( request.getParameter( "poId" ) != null) {
			purchaseOrderForm.setPurchaseOrder( this.webJaguar.getPurchaseOrder( ServletRequestUtils.getIntParameter(request, "poId" , -1 ) ) );
			purchaseOrderForm.getPurchaseOrderStatus().setPoId( purchaseOrderForm.getPurchaseOrder().getPoId() );
			purchaseOrderForm.setNewPurchaseOrder( false );
		}
		else if ( request.getParameter( "orderId" ) != null) {
			Order order = this.webJaguar.getOrder( ServletRequestUtils.getIntParameter(request, "orderId" , -1 ), "" );
			if ( order != null ) {
				if ( order.isFulfilled() ) {
					throw new ModelAndViewDefiningException( new ModelAndView( new RedirectView("purchaseOrders.jhtm"), "po_number", order.getOrderId() ));
				}
				purchaseOrderForm.setOrder( order );
				purchaseOrderForm.getPurchaseOrder().setOrderId( order.getOrderId() );
				purchaseOrderForm.getPurchaseOrder().setPoNumber( order.getOrderId() + "-" + this.webJaguar.purchaseOrderCount( order.getOrderId() ) );
				if ( ServletRequestUtils.getBooleanParameter(request, "dropship" , false ))
					purchaseOrderForm.getPurchaseOrder().setDropShip( true );
			}
		}
		return purchaseOrderForm;
	}

	private double updateLineItems(HttpServletRequest request, PurchaseOrderForm purchaseOrderForm) throws ServletRequestBindingException{		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		// subtotal
		double subTotal = 0;
		
		Iterator iter = purchaseOrderForm.getPurchaseOrder().getPoLineItems().iterator();
		while (iter.hasNext()) {
			PurchaseOrderLineItem lineItem = (PurchaseOrderLineItem) iter.next();
			// check if removed
			if (ServletRequestUtils.getBooleanParameter( request, "__remove_" + lineItem.getLineNumber(), false )) {
				purchaseOrderForm.getPurchaseOrder().getPoDeletedLineItems().add( lineItem );
				iter.remove();
			}			
		}
		
		for (PurchaseOrderLineItem lineItem:purchaseOrderForm.getPurchaseOrder().getPoLineItems()) {
			// get quantity
			int quant = lineItem.getQty();
			lineItem.setQty( ServletRequestUtils.getIntParameter( request, "__quantity_" + lineItem.getLineNumber(), lineItem.getQty() ) );
			
			/*
			 * TO BE DELETED
			 * 
			 * if(siteConfig.get("COST_TIERS").getValue().equals("true")) {
				lineItem.setCost(this.webJaguar.getProductCost(lineItem.getSku(), lineItem.getQty(), request));
			} else {
				if(lineItem.getCost() != null) {
					if(lineItem.getCost() != ServletRequestUtils.getDoubleParameter( request, "__unitPrice_" + lineItem.getLineNumber(), lineItem.getCost()) ) {
						lineItem.setCost( ServletRequestUtils.getDoubleParameter( request, "__unitPrice_" + lineItem.getLineNumber(), lineItem.getCost()));		
					} else if( quant == ServletRequestUtils.getIntParameter( request, "__quantity_" + lineItem.getLineNumber(), lineItem.getQty() ) && lineItem.getTotalCost() != ServletRequestUtils.getDoubleParameter( request, "__totalCost_" + lineItem.getLineNumber(), lineItem.getTotalCost())){
						lineItem.setCost(ServletRequestUtils.getDoubleParameter( request, "__totalCost_" + lineItem.getLineNumber(), lineItem.getTotalCost()) / lineItem.getQty());
					}
				} else {
					if(ServletRequestUtils.getDoubleParameter( request, "__totalCost_" + lineItem.getLineNumber()) != null){
						lineItem.setCost(ServletRequestUtils.getDoubleParameter( request, "__totalCost_" + lineItem.getLineNumber()) / lineItem.getQty());
					}
				}
			}*/
			
			// get unit price 
			if(lineItem.getCost() != null) {
				if(lineItem.getCost() != ServletRequestUtils.getDoubleParameter( request, "__unitPrice_" + lineItem.getLineNumber(), lineItem.getCost()) ) {
					lineItem.setCost( ServletRequestUtils.getDoubleParameter( request, "__unitPrice_" + lineItem.getLineNumber(), lineItem.getCost()));		
				} else if( quant == ServletRequestUtils.getIntParameter( request, "__quantity_" + lineItem.getLineNumber(), lineItem.getQty() ) && lineItem.getTotalCost() != ServletRequestUtils.getDoubleParameter( request, "__totalCost_" + lineItem.getLineNumber(), lineItem.getTotalCost())){
					lineItem.setCost(ServletRequestUtils.getDoubleParameter( request, "__totalCost_" + lineItem.getLineNumber(), lineItem.getTotalCost()) / lineItem.getQty());
				}
			} else {
				if(ServletRequestUtils.getDoubleParameter( request, "__totalCost_" + lineItem.getLineNumber()) != null){
					lineItem.setCost(ServletRequestUtils.getDoubleParameter( request, "__totalCost_" + lineItem.getLineNumber()) / lineItem.getQty());
				}
			}
			
			// get product name
			lineItem.setProductName( ServletRequestUtils.getStringParameter( request, "__productName_" + lineItem.getLineNumber(), lineItem.getProductName() ) );
			
			// update sub total
			if (lineItem.getTotalCost() != null) subTotal = lineItem.getTotalCost() + subTotal;
		}
		
		// inserted line items
		if (purchaseOrderForm.getPurchaseOrder().getPoInsertedLineItems() != null) {
			Iterator iterInserted = purchaseOrderForm.getPurchaseOrder().getPoInsertedLineItems().iterator();
			int index = 0;
			while (iterInserted.hasNext()) {
				PurchaseOrderLineItem lineItem = (PurchaseOrderLineItem) iterInserted.next();
				// check if removed
				if (ServletRequestUtils.getBooleanParameter( request, "__remove_new_" + index, false )) {
					iterInserted.remove();
				} else {
					int quant = lineItem.getQty();
					// get quantity
					lineItem.setQty( ServletRequestUtils.getIntParameter( request, "__quantity_new_" + index, lineItem.getQty() ) );
					
					// get unit price
					if(siteConfig.get("COST_TIERS").getValue().equals("true")) {
						lineItem.setCost(this.webJaguar.getProductCost(lineItem.getSku(), lineItem.getQty(), request));
					} else {
						if((lineItem.getCost() != null) && (lineItem.getCost() != ServletRequestUtils.getDoubleParameter( request, "__unitPrice_new_" + index, lineItem.getCost()) )) {
							lineItem.setCost( ServletRequestUtils.getDoubleParameter( request, "__unitPrice_new_" + index, lineItem.getCost()));		
						} else if( quant == ServletRequestUtils.getIntParameter( request, "__quantity_new_" + index, lineItem.getQty() ) && lineItem.getTotalCost() != ServletRequestUtils.getDoubleParameter( request, "__totalCost_new_" + index, lineItem.getTotalCost())){
							lineItem.setCost(ServletRequestUtils.getDoubleParameter( request, "__totalCost_new_" + index, lineItem.getTotalCost()) / lineItem.getQty());
						} 
					}
					
					// get product name
					lineItem.setProductName( ServletRequestUtils.getStringParameter( request, "__productName_new_" + index, lineItem.getProductName() ) );
					
					// update sub total
					if (lineItem.getTotalCost() != null) subTotal = lineItem.getTotalCost() + subTotal;
				}
				index++;
			}	
		}
		
    	if ((purchaseOrderForm!=null) && (!purchaseOrderForm.isNewPurchaseOrder())){
    		for ( PurchaseOrderLineItem lineItem : purchaseOrderForm.getPurchaseOrder().getPoLineItems()) {
				lineItem.setPoId( purchaseOrderForm.getPurchaseOrder().getPoId() );			
				PurchaseOrderLineItem oldLineItem = this.webJaguar.getLineItemByPOId(lineItem);
				if(lineItem.getQty() != oldLineItem.getQty()){
					Inventory lineInventory = new Inventory(); 	
					lineInventory.setSku(lineItem.getSku());
					lineInventory.setInventoryAFS(lineItem.getQty()-oldLineItem.getQty());
					lineInventory.setInventory(lineItem.getQty()-oldLineItem.getQty());
					this.webJaguar.updateInventory(lineInventory);
				}
    		}
    		this.webJaguar.updatePurchaseOrder( purchaseOrderForm );    		
    	}

		return subTotal;
	}
	
	private void sendEmail(Map siteConfig, HttpServletRequest request, PurchaseOrder purchaseOrder) {
		
		// send email
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));

    	StringBuffer messageBuffer = new StringBuffer();
    	messageBuffer.append( dateFormatter.format(new Date()) + "\n\n" );
    	
    	String firstName = purchaseOrder.getSupplier().getAddress().getFirstName();  
    	if (firstName == null) {
    		firstName = "";
    	}
    	String lastname = purchaseOrder.getSupplier().getAddress().getLastName();
    	if (lastname == null) {
    		lastname = "";
    	}
    
    	// subject
    	purchaseOrder.setSubject( purchaseOrder.getSubject().replace( "#email#", purchaseOrder.getSupplier().getAddress().getEmail() ) );
    	purchaseOrder.setSubject( purchaseOrder.getSubject().replace( "#firstname#", firstName ) );
    	purchaseOrder.setSubject( purchaseOrder.getSubject().replace( "#lastname#", lastname ) );
    	purchaseOrder.setSubject( purchaseOrder.getSubject().replace( "#ponumber#", purchaseOrder.getPoNumber() ) );
    	
    	// message
    	purchaseOrder.setMessage( purchaseOrder.getMessage().replace( "#email#", purchaseOrder.getSupplier().getAddress().getEmail() ) );
    	purchaseOrder.setMessage( purchaseOrder.getMessage().replace( "#firstname#", firstName ) );
    	purchaseOrder.setMessage( purchaseOrder.getMessage().replace( "#lastname#", lastname ) );
    	purchaseOrder.setMessage( purchaseOrder.getMessage().replace( "#ponumber#", purchaseOrder.getPoNumber() ) );
    	
    	messageBuffer.append( purchaseOrder.getMessage() );
    	
    	File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File pdfFile = new File(tempFolder, "purchaseOrder_" + purchaseOrder.getPoNumber() + ".pdf");
		try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(purchaseOrder.getTo());
			helper.setFrom(purchaseOrder.getFrom());
			if(purchaseOrder.getCc() != null && !purchaseOrder.getCc().trim().isEmpty()) {
				helper.setCc(purchaseOrder.getCc());
			}
			//helper.setBcc(purchaseOrder.getBcc());
			helper.setSubject( purchaseOrder.getSubject() );
			if ( purchaseOrder.isHtml() ) { 
	    		helper.setText( messageBuffer.toString(), true );
	    	} else {
	    		helper.setText(messageBuffer.toString());
	    	}
			
			if (this.webJaguar.createPdfPO(pdfFile, purchaseOrder, request)) {
				helper.addAttachment("purchaseOrder_" + purchaseOrder.getPoNumber() + ".pdf", pdfFile);    						
			}
			
			
			// save attachement
		    File baseFile;
		    baseFile = new File( getServletContext().getRealPath("temp"));
			try {
				if(!baseFile.exists()){
					baseFile.mkdir();
				}
				baseFile = new File( baseFile, "/POAttachments/" );
				if(!baseFile.exists()){
					baseFile.mkdir();
				}
			} catch ( Exception e ){ e.printStackTrace(); }
			boolean uploadedAttachment = false;
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile attachment = null;
			if ( baseFile.canWrite() ) {
				attachment = multipartRequest.getFile( "attachment" );
				if ( attachment != null && !attachment.isEmpty() ) {
					uploadedAttachment = true;
				}
			}
			
			if ( uploadedAttachment ) {
				File newFile = new File( baseFile, purchaseOrder.getPoId()+"_0_"+attachment.getOriginalFilename() );
				try {
					attachment.transferTo( newFile );
					helper.addAttachment(purchaseOrder.getPoId()+"_0_"+attachment.getOriginalFilename(), newFile);
				} catch ( IOException e ) {
					e.printStackTrace();
				}
			}
			
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing
			ex.printStackTrace();
		}    			
		if (pdfFile.exists()) {
			pdfFile.delete();
		}
	}
	
	private String getCompanyAddress() {
		StringBuffer sbuff = new StringBuffer();
		String COMPANY_NAME = siteConfig.get("COMPANY_NAME").getValue();
		String COMPANY_ADDRESS1 = siteConfig.get("COMPANY_ADDRESS1").getValue();
		String COMPANY_ADDRESS2 = siteConfig.get("COMPANY_ADDRESS2").getValue();
		String COMPANY_CITY = siteConfig.get("COMPANY_CITY").getValue();
		String COMPANY_STATE = siteConfig.get("COMPANY_STATE_PROVINCE").getValue();
		String COMPANY_COUNTRY = siteConfig.get("COMPANY_COUNTRY").getValue();
		String COMPANY_ZIPCODE = siteConfig.get("COMPANY_ZIPCODE").getValue();
		String COMPANY_PHONE = siteConfig.get("COMPANY_PHONE").getValue();
		String COMPANY_FAX = siteConfig.get("COMPANY_FAX").getValue();
		String CONTACT_EMAIL = siteConfig.get("CONTACT_EMAIL").getValue();
		
		sbuff.append(  COMPANY_NAME + "\n\n" + COMPANY_ADDRESS1 + " " + COMPANY_ADDRESS2 + "\n" + COMPANY_CITY + " " + COMPANY_STATE + " " + COMPANY_ZIPCODE + " " + COMPANY_COUNTRY + " ");
		if ( !siteConfig.get("PO_REMOVE_CUSTOMER_CONTACT").getValue().equals("true")) {
			sbuff.append("\nTel: " + COMPANY_PHONE + "\nFax: " + COMPANY_FAX + "\n" + CONTACT_EMAIL);
		}
		return sbuff.toString();
	}
	
	private String getDropShipAddress( Order order ) {
		StringBuffer sbuff = new StringBuffer();
		String firstName = order.getShipping().getFirstName();
		String lastName = order.getShipping().getLastName();
		String companyName = order.getShipping().getCompany();
		String add1 = order.getShipping().getAddr1();
		String add2 = order.getShipping().getAddr2();
		String city = order.getShipping().getCity();
		String state = order.getShipping().getStateProvince();
		String zip = order.getShipping().getZip();
		String country = order.getShipping().getCountry();
		String tel = order.getShipping().getPhone();
		String fax = order.getShipping().getFax();
		String username = this.webJaguar.getCustomerById( order.getUserId() ).getUsername();
		
		sbuff.append(  firstName + " " + lastName + "\n\n"+ companyName + "\n" + add1 + " " + add2 + "\n" + city + " " + state + " " + zip + " " + country + " ");
		if (!siteConfig.get("PO_REMOVE_CUSTOMER_CONTACT").getValue().equals("true")) {
			sbuff.append("\nTel: " + tel + "\nFax: " + fax + "\n" + username);
		}
		return sbuff.toString();
	}
	
	private void initPurchaseOrderForm(PurchaseOrderForm purchaseOrderForm, Map<String, Configuration> siteConfig, HttpServletRequest request) {
		purchaseOrderForm.getOrder().setLineItems( this.webJaguar.getNotProcessedLineItems( purchaseOrderForm.getOrder().getOrderId(), null ));

    	Map<String, Integer> crossItems = new HashMap<String, Integer>();
		for ( LineItem lineItem : purchaseOrderForm.getOrder().getLineItems() ) {
			Integer productId = this.webJaguar.getProductIdBySku(lineItem.getProduct().getSku());
			if(productId != null) {
				Product product = this.webJaguar.getProductById(productId, 0, false, null);
				if (product.getCrossItemsCode() != null && product.getCrossItemsCode().trim().length() > 0) {
					if (crossItems.containsKey(product.getCrossItemsCode().toLowerCase().trim())) {
						crossItems.put(product.getCrossItemsCode().toLowerCase().trim(), crossItems.get(product.getCrossItemsCode().toLowerCase().trim()) + lineItem.getQuantity());
					} else {
						crossItems.put(product.getCrossItemsCode().toLowerCase().trim(), lineItem.getQuantity());
					}
				}	
			}
		}
		for ( LineItem lineItem : purchaseOrderForm.getOrder().getLineItems() ) {
			Supplier supplier = this.webJaguar.getProductSupplierBySIdSku( lineItem.getProduct().getSku() ,purchaseOrderForm.getPurchaseOrder().getSupplierId() );
			if ( supplier != null ) {
				PurchaseOrderLineItem poLineItem = new PurchaseOrderLineItem();
				poLineItem.setLineNumber( lineItem.getLineNumber() );
				poLineItem.setQty( lineItem.getQuantity() );
				poLineItem.setSku( lineItem.getProduct().getSku() );
				poLineItem.setSupplierSku( supplier.getSupplierSku() );
				//setting cost
				String asiXML = this.webJaguar.getProductAsiXMLById(lineItem.getProduct().getSku());
				//ASI
				if(asiXML != null && !asiXML.trim().isEmpty()) {
					Double cost = setAsiPricing(lineItem, asiXML, supplier.isEndQtyPricing(), siteConfig);
					if (cost != null) {
						poLineItem.setCost(cost);
					}					
				} else if(supplier.getPrice() != null) {				
					//Default cost from supplier
					poLineItem.setCost( supplier.getPrice() );
					//display cost tiers on supplier sku column
					Product product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(lineItem.getProduct().getSku()), 0, true, null);
					lineItem.getProduct().setPrice(product.getPrice());
				} else if(siteConfig.get("COST_TIERS").getValue().equals("true")) {
					//Cost per from supplier
					Product product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(lineItem.getProduct().getSku()), 0, true, null);
					int quantity = lineItem.getQuantity();
					if (product.getCrossItemsCode() != null && !product.getCrossItemsCode().equalsIgnoreCase("")) {
						quantity = crossItems.get(product.getCrossItemsCode().toLowerCase().trim());
					}
					poLineItem.setCost( this.webJaguar.getProductCost(poLineItem.getSku(), quantity, request) );
					
					//display cost tiers on supplier sku column
					lineItem.getProduct().setPrice(product.getPrice());
				}
				poLineItem.setProduct( lineItem.getProduct() );
				poLineItem.setProductAttributes( lineItem.getProductAttributes() );
				poLineItem.setItemGroup(lineItem.getItemGroup());
				poLineItem.setItemGroupMainItem(lineItem.isItemGroupMainItem());
				purchaseOrderForm.getPurchaseOrder().addPOLineItem( poLineItem );
			}	
		}
	}
    
	private Double setAsiPricing(LineItem lineItem, String asiXml, boolean endQtyPricing, Map<String, Configuration> siteConfig) {
		List<Price> prices = null;
		Double unitCost = null;
		
		JAXBContext context;
		Unmarshaller unmarshaller = null;
		ByteArrayInputStream bis;
		
		asiProduct.Product oldAsiProd = null;
		com.webjaguar.thirdparty.asi.asiAPI.Product newAsiProd = null;
		
		// OLD ASI Product
		if(siteConfig.get("ASI_FEED_PRODUCT_TOKEN").getValue() != null && !siteConfig.get("ASI_FEED_PRODUCT_TOKEN").getValue().trim().isEmpty()) {
			try {
				context = JAXBContext.newInstance( asiProduct.Product.class);
				unmarshaller = context.createUnmarshaller();
				bis = new ByteArrayInputStream(asiXml.getBytes("UTF-8"));
				oldAsiProd = (asiProduct.Product) unmarshaller.unmarshal(bis);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		// NEW ASI Product
		if(siteConfig.get("ASI_KEY").getValue() != null && !siteConfig.get("ASI_KEY").getValue().trim().isEmpty()) {
			try {
				context = JAXBContext.newInstance(com.webjaguar.thirdparty.asi.asiAPI.Product.class);
				unmarshaller = context.createUnmarshaller();
				StringBuffer sb = new StringBuffer(asiXml.toString());
				bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
				newAsiProd = (com.webjaguar.thirdparty.asi.asiAPI.Product) unmarshaller.unmarshal(bis);
			} catch (Exception e) { 
				e.printStackTrace(); 
			}
		}
		
		if(oldAsiProd != null && oldAsiProd.getPricing() != null && oldAsiProd.getPricing().getPriceGrid() != null) {
			String priceGridId = null;
			for(PriceGrid priceGrid : oldAsiProd.getPricing().getPriceGrid()) { 
				if(priceGrid.isBase() && !priceGrid.getId().equals("pricegrid-99")) {
					for(int i=0 ; i<priceGrid.getPrice().size(); i++) {
						asiProduct.PriceGrid.Price asiPrice = priceGrid.getPrice().get(i);
						
						int quantMin = 0;
						if(i != 0) {
							quantMin = Integer.parseInt(priceGrid.getPrice().get(i).getQuantity().toString());
						}
						Double newCost = Double.parseDouble(asiPrice.getCost());
						if(unitCost == null || (unitCost != null && unitCost > newCost)){
							if(endQtyPricing) {
								unitCost = newCost;
								priceGridId = priceGrid.getId();
							} else if(lineItem.getQuantity() >= quantMin) {
								unitCost = newCost;
								priceGridId = priceGrid.getId();
							}
						}
					}
				}
			}
			
			prices = new ArrayList<Price>();
			for(PriceGrid priceGrid : oldAsiProd.getPricing().getPriceGrid()) { 
				if(priceGrid.getId().equals(priceGridId)) {
					for(int i=0 ; i<priceGrid.getPrice().size(); i++) {
						asiProduct.PriceGrid.Price asiPrice = priceGrid.getPrice().get(i);
						int quantMin = Integer.parseInt(priceGrid.getPrice().get(i).getQuantity().toString());
						Integer quantMax = 0;
						
						if(i == priceGrid.getPrice().size()-1) {
							quantMax = null;
						} else {
							quantMax = Integer.parseInt(priceGrid.getPrice().get(i+1).getQuantity().toString());
						}
						Price price = new Price(Double.parseDouble(asiPrice.getValue()), null, null, quantMin, quantMax, Double.parseDouble(asiPrice.getCost()));
						prices.add(price);
					}
				}
			}
			lineItem.getProduct().setPrice(prices);
		}
		
		if(newAsiProd != null) {
			if(newAsiProd.getPrices() != null) {
				for (com.webjaguar.thirdparty.asi.asiAPI.Price asiPrice : newAsiProd.getPrices().getPrice()) {
			    	if(asiPrice.getIsUndefined() != null && asiPrice.getIsUndefined()){
			    		continue;
			    	}
			    	int quantMin = asiPrice.getQuantity().getRange().getFrom();
					
			    	if(unitCost == null || (unitCost != null && unitCost > asiPrice.getCost().doubleValue())){
						if(endQtyPricing) {
							unitCost = asiPrice.getCost().doubleValue();
						} else if(lineItem.getQuantity() >= quantMin) {
							unitCost = asiPrice.getCost().doubleValue();
						}
					}
			    }
			    prices = new ArrayList<Price>();
			    for (com.webjaguar.thirdparty.asi.asiAPI.Price asiPrice : newAsiProd.getPrices().getPrice()) {
			    	int quantMin = asiPrice.getQuantity().getRange().getFrom();
			    	int quantMax = asiPrice.getQuantity().getRange().getTo();
					Price price = new Price(asiPrice.getPrice().doubleValue(), null, null, quantMin, quantMax, asiPrice.getCost().doubleValue());
					prices.add(price);
				}
				lineItem.getProduct().setPrice(prices);
			} else if (newAsiProd.getVariants() != null && newAsiProd.getVariants().getVariant() != null && !newAsiProd.getVariants().getVariant().isEmpty()) {
				Variant lineItemVariant = null;
				
				for(Variant variant : newAsiProd.getVariants().getVariant()) {
					String variantDesc = variant.getDescription();
					for(ProductAttribute  attr : lineItem.getProductAttributes()){
						if(attr.getOptionName().equals("Variant") && attr.getValueName().equalsIgnoreCase(variantDesc)){
							lineItemVariant = variant;
							break;
						}
					}
				}
				
				if(lineItemVariant == null) {
					lineItemVariant = newAsiProd.getVariants().getVariant().get(0);
				}
				for (com.webjaguar.thirdparty.asi.asiAPI.Price asiPrice : lineItemVariant.getPrices().getPrice()) {
			    	if(asiPrice.getIsUndefined() != null && asiPrice.getIsUndefined()){
			    		continue;
			    	}
			    	int quantMin = asiPrice.getQuantity().getRange().getFrom();
					
			    	if(unitCost == null || (unitCost != null && unitCost > asiPrice.getCost().doubleValue())){
						if(endQtyPricing) {
							unitCost = asiPrice.getCost().doubleValue();
						} else if(lineItem.getQuantity() >= quantMin) {
							unitCost = asiPrice.getCost().doubleValue();
						}
					}
			    }
			    prices = new ArrayList<Price>();
			    for (com.webjaguar.thirdparty.asi.asiAPI.Price asiPrice : lineItemVariant.getPrices().getPrice()) {
			    	int quantMin = asiPrice.getQuantity().getRange().getFrom();
			    	int quantMax = asiPrice.getQuantity().getRange().getTo();
					Price price = new Price(asiPrice.getPrice().doubleValue(), null, null, quantMin, quantMax, asiPrice.getCost().doubleValue());
					prices.add(price);
				}
				lineItem.getProduct().setPrice(prices);
		    }
		}
		return unitCost;
	}
	
	// multiple browser tabs/windows session issue
	// http://forum.springframework.org/showthread.php?t=42241
	protected String getFormSessionAttributeName(HttpServletRequest request) {
		String poId = request.getParameter("poId");
		return super.getFormSessionAttributeName(request) + poId;	    	  
	}
}