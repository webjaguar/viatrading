/* Copyright 2005, 2007 Advanced E-Media Solutions
 * Author: Shahin Naji
 */

package com.webjaguar.web.admin.inventory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;


import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.Product;
import com.webjaguar.model.PurchaseOrderLineItem;
import com.webjaguar.model.Supplier;
import com.webjaguar.web.form.PurchaseOrderForm;

public class AjaxShowOrderLineItemsController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	Map<String, Object> model = new HashMap<String, Object>();
    	
		Order order = this.webJaguar.getOrder( ServletRequestUtils.getIntParameter(request, "orderId" , -1 ), "" );
		if ( order != null ) {
			if ( order.isFulfilled() ) {
			//	throw new ModelAndViewDefiningException( new ModelAndView( new RedirectView("purchaseOrders.jhtm"), "po_number", order.getOrderId() ));
			}
			if(ServletRequestUtils.getIntParameter(request, "orderId") != null) {
				model.put("supplierList",this.webJaguar.getProductSupplierListByOrderId(ServletRequestUtils.getIntParameter(request, "orderId"), "filter"));
			}
			model.put("order", order);
			
			List<LineItem> lineItems =  this.webJaguar.getNotProcessedLineItems( ServletRequestUtils.getIntParameter(request, "orderId", -1), null );
			if(lineItems != null) {
				Iterator<LineItem> itr = lineItems.iterator();
				while(itr.hasNext()){
					LineItem currentItem = itr.next();
					Supplier supp = this.webJaguar.getProductSupplierBySIdSku( currentItem.getProduct().getSku() ,ServletRequestUtils.getIntParameter(request, "supplierId", -1) );
					if ( supp == null ) {
						itr.remove();
						continue;
				   	}
					// need to check for less than or zero as drop ship can be for more items than actually ordered
					if (currentItem.getQuantity() <= currentItem.getProcessed()) {
						itr.remove();
			   			continue;
			   		}
					
				}
				model.put("lineItems", lineItems);
			}
			model.put("supplierId",  ServletRequestUtils.getIntParameter(request, "supplierId", -1) );
		}
		return new ModelAndView( "admin/inventory/ajaxSupplierLineItems", "model", model );
	}
}