package com.webjaguar.web.admin.inventory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.mail.MailSender;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.PurchaseOrder;
import com.webjaguar.model.PurchaseOrderLineItem;
import com.webjaguar.web.form.PurchaseOrderForm;

public class PurchaseOrderAPIController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	
	public void setWebJaguar(WebJaguarFacade webJaguar) 
	{ 
		this.webJaguar = webJaguar; 
	}
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	System.out.println("-----PurchaseOrderAPIController------");
    	
    	PurchaseOrderForm purchaseOrderForm = new PurchaseOrderForm();
    	
    	purchaseOrderForm.setPurchaseOrder(getPurchaseOrderObject(purchaseOrderForm, request));
    	
    	this.webJaguar.insertPurchaseOrder(purchaseOrderForm);
    	

    	return null;
    }
    
	private PurchaseOrder getPurchaseOrderObject(PurchaseOrderForm purchaseOrderForm, HttpServletRequest request) {
		
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		
		List<PurchaseOrderLineItem> purchaseOrderLineItemList = new LinkedList<PurchaseOrderLineItem>();
		
		purchaseOrder.setStatus("pend");
		
		Date now = new Date();
		
		if (request.getParameter("poNumber") != null) {
			String poNumber = ServletRequestUtils.getStringParameter(request, "poNumber", "");
			if (poNumber != null) {
				purchaseOrder.setPoNumber(poNumber);
			} 
		}
		
		if (request.getParameter("createdDate") != null) {
			
			String createdDate = ServletRequestUtils.getStringParameter(request, "createdDate", "");
			DateFormat df = new SimpleDateFormat("mm/dd/yyyy");
			
			try{
				Date date = df.parse(createdDate);
				if (date != null) {
					purchaseOrder.setCreated(date);
				} 
			}
			catch(Exception ex){
					purchaseOrder.setCreated(now);
			}
		}
		
		if (request.getParameter("supplierId") != null) {
			Integer supplierId = ServletRequestUtils.getIntParameter(request, "supplierId", 0);
			if (supplierId != null) {
				purchaseOrder.setSupplierId(supplierId);
			} 
		}
		
		if (request.getParameter("dueDate") != null) {
			
			String dueDate = ServletRequestUtils.getStringParameter(request, "dueDate", "");
			DateFormat df = new SimpleDateFormat("mm/dd/yyyy");
			
			try{
				Date date = df.parse(dueDate);
				if (date != null) {
					purchaseOrder.setDueDate(date);
				} 
			}
			catch(Exception ex){
					purchaseOrder.setDueDate(now);
			}
		}
		
		if (request.getParameter("subTotal") != null) {
			Double subTotal = ServletRequestUtils.getDoubleParameter(request, "subTotal", 0.0);
			if (subTotal != null) {
				purchaseOrder.setSubTotal(subTotal);
			} 
		}
		
		if (request.getParameter("shipVia") != null) {
			String shipVia = ServletRequestUtils.getStringParameter(request, "shipVia", "");
			if (shipVia != null) {
				purchaseOrder.setShipVia(shipVia);
			} 
		}
		
		if (request.getParameter("shipViaContactId") != null) {
			Integer shipViaContactId = ServletRequestUtils.getIntParameter(request, "shipViaContactId", 0);
			if (shipViaContactId != null) {
				purchaseOrder.setShipViaContactId(shipViaContactId);
			} 
		}
		
		if (request.getParameter("orderBy") != null) {
			String orderBy = ServletRequestUtils.getStringParameter(request, "orderBy", "");
			if (orderBy != null) {
				purchaseOrder.setOrderBy(orderBy);
			} 
		}
		
		if (request.getParameter("orderId") != null) {
			Integer orderId = ServletRequestUtils.getIntParameter(request, "orderId", 0);
			if (orderId != null) {
				purchaseOrder.setOrderId(orderId);
			} 
		}
		
		if (request.getParameter("billingAddress") != null) {
			String billingAddress = ServletRequestUtils.getStringParameter(request, "billingAddress", "");
			if (billingAddress != null) {
				purchaseOrder.setBillingAddress(billingAddress);
			} 
		}
		
		if (request.getParameter("shippingAddress") != null) {
			String shippingAddress = ServletRequestUtils.getStringParameter(request, "shippingAddress", "");
			if (shippingAddress != null) {
				purchaseOrder.setShippingAddress(shippingAddress);
			} 
		}
		
		if (request.getParameter("note") != null) {
			String note = ServletRequestUtils.getStringParameter(request, "note", "");
			if (note != null) {
				purchaseOrder.setNote(note);
			} 
		}
		
		if (request.getParameter("dropShip") != null) {
			Integer dropShip = ServletRequestUtils.getIntParameter(request, "dropShip", 0);
			if (dropShip != null) {
				purchaseOrder.setDropShip((dropShip == 1) ? true : false);
			} 
		}
		
		if (request.getParameter("supplierInvoiceNumber") != null) {
			String supplierInvoiceNumber = ServletRequestUtils.getStringParameter(request, "supplierInvoiceNumber", "");
			if (supplierInvoiceNumber != null) {
				purchaseOrder.setSupplierInvoiceNumber(supplierInvoiceNumber);
			} 
		}
		
		if (request.getParameter("shippingQuote") != null) {
			Double shippingQuote = ServletRequestUtils.getDoubleParameter(request, "shippingQuote", 0.0);
			if (shippingQuote != null) {
				purchaseOrder.setShippingQuote(shippingQuote);
			} 
		}
		
		if (request.getParameter("flag1") != null) {
			String flag1 = ServletRequestUtils.getStringParameter(request, "flag1", "");
			if (flag1 != null) {
				purchaseOrder.setFlag1(flag1);
			} 
		}
		
		if (request.getParameter("specialInstruction") != null) {
			String specialInstruction = ServletRequestUtils.getStringParameter(request, "specialInstruction", "");
			if (specialInstruction != null) {
				purchaseOrder.setSpecialInstruction(specialInstruction);
			} 
		}
		
		if (request.getParameter("pt") != null) {
			String pt = ServletRequestUtils.getStringParameter(request, "pt", "");
			if (pt != null) {
				purchaseOrder.setPt(pt);
			} 
		}
		
		Integer count = 0;
		
		if (request.getParameter("count") != null) {
			count = ServletRequestUtils.getIntParameter(request, "count", 0);
		}
		
		if(count != null){
			
			for(int i = 1; i < count + 1; i++){
				
				PurchaseOrderLineItem purchaseOrderLineItem = new PurchaseOrderLineItem();
				
				if (request.getParameter("sku" + i) != null) {
					String sku = ServletRequestUtils.getStringParameter(request, "sku" + i, "");
					if (sku != null) {
						purchaseOrderLineItem.setSku(sku);
					} 
				}
				
				if (request.getParameter("productName" + i) != null) {
					String productName = ServletRequestUtils.getStringParameter(request, "productName" + i, "");
					if (productName != null) {
						purchaseOrderLineItem.setProductName(productName);
					} 
				}
				
				if (request.getParameter("supplierSku" + i) != null) {
					String supplierSku = ServletRequestUtils.getStringParameter(request, "supplierSku" + i, "");
					if (supplierSku != null) {
						purchaseOrderLineItem.setSupplierSku(supplierSku);
					} 
				}
				
				if (request.getParameter("qty" + i) != null) {
					Integer qty = ServletRequestUtils.getIntParameter(request, "qty" + i, 0);
					if (qty != null) {
						purchaseOrderLineItem.setQty(qty);
					} 
				}
				
				if (request.getParameter("cost" + i) != null) {
					Double cost = ServletRequestUtils.getDoubleParameter(request, "cost" + i, 0);
					if (cost != null) {
						purchaseOrderLineItem.setUnitCost(cost);
						purchaseOrderLineItem.setUnitPrice(cost);
						purchaseOrderLineItem.setCost(cost);
					} 
				}
				
				purchaseOrderLineItem.setLineNumber(i);
				
				purchaseOrderLineItemList.add(purchaseOrderLineItem);
			}
			
		}
		
		purchaseOrder.setPoLineItems(purchaseOrderLineItemList);
		return purchaseOrder;
	}

}
