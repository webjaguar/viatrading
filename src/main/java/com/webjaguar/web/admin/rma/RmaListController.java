/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.16.2008
 */

package com.webjaguar.web.admin.rma;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.RmaSearch;

public class RmaListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
    	
		RmaSearch search = getSearch(request);
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		
		PagedListHolder list = new PagedListHolder(this.webJaguar.getRmaList(search));		

		list.setPageSize(search.getPageSize());
		list.setPage(search.getPage()-1);
		
		model.put("list", list);
    	
        return new ModelAndView("admin/rma/rmas", "model", model);
	}

	private RmaSearch getSearch(HttpServletRequest request) {
		RmaSearch search = (RmaSearch) request.getSession().getAttribute("rmaSearch");
		if (search == null) {
			search = new RmaSearch();
			request.getSession().setAttribute("rmaSearch", search);
		}
		
		// rma number
		if (request.getParameter("rmaNum") != null) {
			search.setRmaNum(ServletRequestUtils.getStringParameter( request, "rmaNum", "" ));
		}

		// order number
		if (request.getParameter("orderId") != null) {
			search.setOrderId(ServletRequestUtils.getStringParameter( request, "orderId", "" ));
		}
		
		// serial num
		if (request.getParameter("serialNum") != null) {
			search.setSerialNum(ServletRequestUtils.getStringParameter( request, "serialNum", "" ));
		}
		
		// sku
		if (request.getParameter("sku") != null) {
			search.setSku(ServletRequestUtils.getStringParameter( request, "sku", "" ));
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		return search;
	}	
	
}
