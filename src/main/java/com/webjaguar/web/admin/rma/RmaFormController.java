/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.16.2008
 */

package com.webjaguar.web.admin.rma;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Rma;

public class RmaFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public RmaFormController() {
		setSessionForm(true);
		setCommandName("rmaForm");
		setCommandClass(Rma.class);
		setFormView("admin/rma/rmaForm");
		setSuccessView("rmas.jhtm");
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(df, true, 10));		
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		
		Rma rma = (Rma) command;
		
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		this.webJaguar.updateRma(rma);
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}	
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		return this.webJaguar.getRma(new Rma(ServletRequestUtils.getIntParameter(request, "num", -1)));
	}

}
