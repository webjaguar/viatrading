/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.event;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.EventForm;
import com.webjaguar.logic.WebJaguarFacade;

public class EventFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public EventFormController() {
		setSessionForm(true);
		setCommandName("eventForm");
		setCommandClass(EventForm.class);
		setFormView("admin/event/form");
		setSuccessView("eventList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
		EventForm eventForm = (EventForm) command;
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteEvent(eventForm.getEvent().getId());	
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		if (eventForm.isNewEvent()) {
			this.webJaguar.insertEvent(eventForm.getEvent());
		} else {
			this.webJaguar.updateEvent(eventForm.getEvent());
		}
		
		ModelAndView modelAndView = new ModelAndView(new RedirectView(getSuccessView()));	
		return modelAndView;
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null) {
			return true;			
		}
		return false;
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
	
		SimpleDateFormat df = new SimpleDateFormat( "MM-dd-yyyy hh:mm a", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, false, 19 ) );		
	}	
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		EventForm eventForm = (EventForm) command;
		eventForm.getEvent().setEventName(eventForm.getEvent().getEventName().trim());
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "event.eventName", "form.required", "Event Name is required.");    	
	}
	
   protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
		EventForm eventForm = new EventForm();
		if ( request.getParameter("id") != null ) {
			eventForm = new EventForm(this.webJaguar.getEventById(new Integer(request.getParameter("id"))));
		} 
		return eventForm;
	} 
}