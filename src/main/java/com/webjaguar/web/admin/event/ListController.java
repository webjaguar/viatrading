/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.event;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Event;
import com.webjaguar.model.EventSearch;


public class ListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
    	
    	EventSearch eventSearch = getEventSearch( request );
		Map<String, Object> myModel = new HashMap<String, Object>();

		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("event.jhtm"));
		}
		int count = this.webJaguar.getEventList(eventSearch).size();
		if (count < eventSearch.getOffset()-1) {
			eventSearch.setPage(1);
		}
		eventSearch.setLimit(eventSearch.getPageSize());
		eventSearch.setOffset((eventSearch.getPage()-1)*eventSearch.getPageSize());
		List<Event> eventList = this.webJaguar.getEventList(eventSearch);
		myModel.put("eventList", eventList);
		
		int pageCount = count/eventSearch.getPageSize();
		if (count%eventSearch.getPageSize() > 0) {
			pageCount++;
		}
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", eventSearch.getOffset()+eventList.size());
		myModel.put("count", count);

		return new ModelAndView("admin/event/list", "model", myModel);
	}
	
	private EventSearch getEventSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		EventSearch eventSearch = (EventSearch) request.getSession().getAttribute( "eventSearch" );
		if (eventSearch == null) {
			eventSearch = new EventSearch();
			eventSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "eventSearch", eventSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			eventSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// event name
		if (request.getParameter("eventName") != null) {
			eventSearch.setEventName(ServletRequestUtils.getStringParameter( request, "eventName", "" ));
		}
		
		// event active
		if (request.getParameter("_active") != null) {
			eventSearch.setActive( ServletRequestUtils.getStringParameter( request, "_active", "" ));
		}
		
		// event inEffect
		if (request.getParameter("_inEffect") != null) {
			eventSearch.setInEffect( ServletRequestUtils.getStringParameter( request, "_inEffect", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				eventSearch.setPage( 1 );
			} else {
				eventSearch.setPage( page );				
			}
		}			
		
		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				eventSearch.setPageSize( 10 );
			} else {
				eventSearch.setPageSize( size );				
			}
		}
		
		return eventSearch;
	}	
}
