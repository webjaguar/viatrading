/* Copyright 2005, 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.09.2009
 */

package com.webjaguar.web.admin.event;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.EventMemberForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.EventMember;

public class EventMemberFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public EventMemberFormController() {
		setSessionForm(true);
		setCommandName("eventMemberForm");
		setCommandClass(EventMemberForm.class);
		setFormView("admin/event/memberForm");
		setSuccessView("eventMemberList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
		EventMemberForm eventMemberForm = (EventMemberForm) command;
		
		// check if delete button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		this.webJaguar.updateEventMember(eventMemberForm.getEventMember());

		return new ModelAndView(new RedirectView(getSuccessView() ), "id", eventMemberForm.getEventMember().getEventId());	
	}
	
   protected Object formBackingObject(HttpServletRequest request) 
   	   throws Exception {
		
	   EventMemberForm eventMemberForm = new EventMemberForm();
	   int eventId = ServletRequestUtils.getIntParameter( request, "id", -1 );
	   int userId = ServletRequestUtils.getIntParameter( request, "cid", -1 );
		if ( eventId != -1 && userId != -1 ) {
			EventMember eventMember = this.webJaguar.getEventMemberByEventIdUserId(eventId, userId);
			eventMemberForm.setEventMember(eventMember);
		} 
		return eventMemberForm;
	} 
}