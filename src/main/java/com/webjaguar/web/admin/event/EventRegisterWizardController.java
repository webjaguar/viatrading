/* Copyright 2005, 2010 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.event;

import java.lang.reflect.Method;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.EventMember;
import com.webjaguar.model.EventSearch;
import com.webjaguar.web.form.EventForm;

public class EventRegisterWizardController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	public EventRegisterWizardController() {
		setCommandName("eventForm");
		setCommandClass(EventForm.class);
	}
	
    public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {

    	EventForm eventForm = (EventForm) command;
    	StringBuffer message = new StringBuffer();
    	
    	String ids[] = request.getParameterValues("eventId");
		if (ids != null) {
			for (int i=0; i<ids.length; i++) {
				eventForm.setEvent(this.webJaguar.getEventById(Integer.parseInt(ids[i])));
		    	if (eventForm.getCustomer() != null) {
		    		if (eventForm.getEvent().getHtmlCode() != null) {
		    			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#cardid#", eventForm.getCustomer().getCardId()!=null ? eventForm.getCustomer().getCardId() : ""));
		    			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#firstname#", eventForm.getCustomer().getAddress().getFirstName()));
		    			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#lastname#", eventForm.getCustomer().getAddress().getLastName()));
		    			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#accountNumber#", (eventForm.getCustomer().getAccountNumber()==null) ? "" : eventForm.getCustomer().getAccountNumber()));
		    			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#eventname#", eventForm.getEvent().getEventName()));
		    			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#eventdate#", eventForm.getEvent().getStartDate().toString()));
		    			try {
		    				eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#salesRepName#", this.webJaguar.getSalesRepById(eventForm.getCustomer().getSalesRepId()).getName()));
		    			} catch (Exception e) {
		    				eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#salesRepName#", ""));
		    			}
		    			// customer fields
		    			Class<Customer> c1 = Customer.class;
		    			Method m1 = null;
		    			Object arglist1[] = null;
		    			for ( CustomerField customerField : this.webJaguar.getCustomerFields() ) {
		    				if ( customerField.isEnabled() ) {
		    					m1 = c1.getMethod( "getField" + customerField.getId() );
		    					customerField.setValue( (String) m1.invoke( eventForm.getCustomer(), arglist1 ) );
		    					eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#customerfield"+ customerField.getId() +"#", ( customerField.getValue() == null ) ? "" :  customerField.getValue() ));
		    				}
		    			}
		    		}
				}
		    	
		    		
				try {
		    		EventMember member = new EventMember();
		    		member.setUserId(eventForm.getCustomer().getId());
		    		member.setEventId(eventForm.getEvent().getId());
		    		member.setUsername(eventForm.getCustomer().getUsername());
		    		member.setFirstName(eventForm.getCustomer().getAddress().getFirstName());
		    		member.setLastName(eventForm.getCustomer().getAddress().getLastName());
		    		member.setCardId(eventForm.getCustomer().getCardId());
		    		member.setSalesRepId(eventForm.getCustomer().getSalesRepId());
		    		member.setHtmlCode(eventForm.getEvent().getHtmlCode());
					this.webJaguar.insertEventMember(member);
					
					// insert queue
					if (eventForm.getEvent().getHtmlCode() != null) {
						eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#queue#", member.getUserIndex().toString()));
						member.setHtmlCode(eventForm.getEvent().getHtmlCode());
						this.webJaguar.updateEventMember(member);
					}
					
				} catch (DataIntegrityViolationException ex) {
					message.append(eventForm.getEvent().getEventName() + ": in the list<br />");
				}				
			}
		}

		Map<String, Object> model = new HashMap<String, Object>();
		if (message.length()<1) {
			message.append("User registered to the Events sucessfully.");
		}

		model.put( "eventForm", eventForm );
		model.put( "message", message );
		return new ModelAndView( "admin/event/register/step1", "model", model );
    }
 
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
    	throws Exception {
    	EventForm eventForm = (EventForm) command;
    	
       	Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> myModel = new HashMap<String, Object>();
				
		switch (page) {
			case 0:
				EventSearch search = new EventSearch();
				search.setActive("1");
				//search.setInEffect("1");
				myModel.put("events", this.webJaguar.getEventList(search));
				break;
		}	
		map.put("model", myModel);
		
		return map;
    }
    
    protected void validatePage(Object command, Errors errors, int page) {
    	EventForm eventForm = (EventForm) command;
		
		switch (page) {
		case 1:
		}
    }
    
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );	
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		setPages( new String[] {"admin/event/register/step0", "admin/event/register/step1"});
		EventForm eventForm = new EventForm();
		Integer userId = ServletRequestUtils.getIntParameter(request, "id", -1);
		if (userId != -1){
			Customer customer = this.webJaguar.getCustomerById(userId);
			eventForm.setCustomer(customer);
		}
		
		return eventForm;
	}
	
	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		EventForm eventForm = (EventForm) command;

		switch (page) {
			case 0:
				break;
		}		
	}
}