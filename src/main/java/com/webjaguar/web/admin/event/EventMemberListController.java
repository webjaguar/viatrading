/* Copyright 2005, 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 12.04.2009
 */

package com.webjaguar.web.admin.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.EventMember;
import com.webjaguar.model.EventSearch;
import com.webjaguar.model.SalesRep;


public class EventMemberListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	EventSearch eventSearch = getEventSearch( request );
		Map<String, Object> myModel = new HashMap<String, Object>();
		int eventId = ServletRequestUtils.getIntParameter( request, "id", -1 );
		if (eventId == -1) {
			return new ModelAndView( new RedirectView( "eventList.jhtm" ) );
		}
		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i=0; i<ids.length; i++) {
					this.webJaguar.deleteEventMemberById(eventId, Integer.parseInt(ids[i]));
				}
			}
		}
		
		if (request.getParameter("__print") != null) {
			int userId = ServletRequestUtils.getIntParameter( request, "cid", -1 );
			myModel.put("eventMember", this.webJaguar.getEventMemberByEventIdUserId(eventId,userId));
			return new ModelAndView("admin/event/eventPrint", "model", myModel);
		}
		if(request.getParameter("__printEventMember") != null){
			String ids[] = request.getParameterValues("__selected_id");
			List<EventMember> eventMemberList = new ArrayList<EventMember>();
			Map<Integer, String> userMap = new HashMap<Integer, String>();
			for(String id : ids){
				EventMember eventMember = this.webJaguar.getEventMemberByEventIdUserId(eventId, Integer.valueOf(id));
				Customer customer = this.webJaguar.getCustomerById(eventMember.getUserId());
				userMap.put(eventMember.getUserId(), customer.getField20());
				if(eventMember != null){
					eventMemberList.add(eventMember);
					
				}
			}
			myModel.put("userMap", userMap);
			myModel.put("event", this.webJaguar.getEventById(eventId));
			myModel.put("eventMemberList", eventMemberList);
			return new ModelAndView("admin/event/eventPrint", "model", myModel);
		}
		
		//arrange customer group
		if (request.getParameter("__groupAssignPacher") != null) {
			// get group ids
			Set<Object> groups = new HashSet<Object>();
			String groupIdString = ServletRequestUtils.getStringParameter(request, "__group_id");
			String batchType = ServletRequestUtils.getStringParameter(request, "__batch_type");
			String[] groupIds = groupIdString.split(",");
			for (int x = 0; x < groupIds.length; x++) {
				if (!("".equals(groupIds[x].trim()))) {
					try {
						groups.add(Integer.valueOf(groupIds[x].trim()));
					} catch (Exception e) {
						// do nothing
					}
				}
			}
			String ids[] = request.getParameterValues("__selected_id");
			if ((ids != null)) {
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.batchGroupIds(Integer.parseInt(ids[i]), groups, batchType);
				}
			} 
		}		
		
		eventSearch.setEventId("" + eventId);
		
		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
			// get sales rep
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = this.webJaguar.getSalesRepList();
			for (SalesRep salesRep: salesReps) {
				salesRepMap.put(salesRep.getId(), salesRep);				
			}
			myModel.put("salesRepMap", salesRepMap);
			myModel.put("salesReps", salesReps);
		}
		
		int count = this.webJaguar.getEventMemberListCount(eventSearch);
		if (count < eventSearch.getOffset()-1) {
			eventSearch.setPage(1);
		}
		eventSearch.setLimit(eventSearch.getPageSize());
		eventSearch.setOffset((eventSearch.getPage()-1)*eventSearch.getPageSize());

		List<EventMember> eventMemberList = this.webJaguar.getEventMemberList(eventSearch);
		myModel.put("eventMemberList", eventMemberList);
		
		int pageCount = count/eventSearch.getPageSize();
		if (count%eventSearch.getPageSize() > 0) {
			pageCount++;
		}
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", eventSearch.getOffset()+eventMemberList.size());
		myModel.put("count", count);
		
		myModel.put("event", this.webJaguar.getEventById(eventId));
						
		return new ModelAndView("admin/event/memberList", "model", myModel);
	}
	
	private EventSearch getEventSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		EventSearch eventMemberSearch = (EventSearch) request.getSession().getAttribute( "eventMemberSearch" );
		if (eventMemberSearch == null) {
			eventMemberSearch = new EventSearch();
			eventMemberSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "eventMemberSearch", eventMemberSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			eventMemberSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "username DESC" ));
		}
		
//		// event name
//		if (request.getParameter("eventName") != null) {
//			eventMemberSearch.setEventName(ServletRequestUtils.getStringParameter( request, "eventName", "" ));
//		}
		
		// Card ID
		if (request.getParameter("cardId") != null) {
			eventMemberSearch.setCardId( ServletRequestUtils.getStringParameter( request, "cardId", "" ));
		}
		
		// first name
		if (request.getParameter("firstName") != null) {
			eventMemberSearch.setFirstName(ServletRequestUtils.getStringParameter( request, "firstName", "" ));
		}
		
		// last name
		if (request.getParameter("lastName") != null) {
			eventMemberSearch.setLastName(ServletRequestUtils.getStringParameter( request, "lastName", "" ));
		}	
		
		// sales rep
		if (request.getParameter("salesRepId") != null) {
			eventMemberSearch.setSalesRepId(ServletRequestUtils.getIntParameter( request, "salesRepId", -1 ));
		}
		
		// email
		if (request.getParameter("email") != null) {
			eventMemberSearch.setEmail( ServletRequestUtils.getStringParameter( request, "email", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				eventMemberSearch.setPage( 1 );
			} else {
				eventMemberSearch.setPage( page );				
			}
		}			
		
		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				eventMemberSearch.setPageSize( 10 );
			} else {
				eventMemberSearch.setPageSize( size );				
			}
		}
		
		return eventMemberSearch;
	}	
}
