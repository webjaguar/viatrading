package com.webjaguar.web.admin.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
//import org.springframework.security.core.GrantedAuthorityImpl;
//import org.springframework.security.providers.AuthenticationProvider;
//import org.springframework.security.providers.UsernamePasswordAuthenticationToken;
//import org.springframework.security.ui.WebAuthenticationDetails;
//import org.springframework.security.userdetails.User;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;

public class CustomAuthenticationProvider implements AuthenticationProvider {

	protected WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private GlobalDao globalDao;	
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	public Authentication authenticate(Authentication authentication)  {
		String username = String.valueOf(authentication.getPrincipal());
		String password = String.valueOf(authentication.getCredentials());
		String ipAddress = ((WebAuthenticationDetails) authentication.getDetails()).getRemoteAddress();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Object principal = authentication.getPrincipal();
	    if (principal instanceof String) {
    		AccessUser adminUser = this.globalDao.getAdminInfoByUsernamePassword(username, password);
    		if ( adminUser != null && (adminUser.getIpAddress()==null || adminUser.getIpAddress().isEmpty() || adminUser.getIpAddress().equals(ipAddress))) {
    			User user = new User(adminUser.getUsername(), adminUser.getPassword(), true, true, true, true, (Collection<? extends GrantedAuthority>) getAuthorities(adminUser));
    			Authentication usernamePasswordAuthToken = new UsernamePasswordAuthenticationToken(user, adminUser.getPassword(), (Collection<? extends GrantedAuthority>) getAuthorities(adminUser));
	    		return usernamePasswordAuthToken;
    		}
    		if (!siteConfig.get("GOOGLE_CHECKOUT_URL").getValue().isEmpty() && !siteConfig.get("GOOGLE_MERCHANT_ID").getValue().isEmpty() && !siteConfig.get("GOOGLE_MERCHANT_KEY").getValue().isEmpty()) {
    			if (siteConfig.get("GOOGLE_MERCHANT_ID").getValue().trim().equals(username) && siteConfig.get("GOOGLE_MERCHANT_KEY").getValue().trim().equals(password)) {
    				AccessUser googleCheckoutUser = new AccessUser();
    				googleCheckoutUser.setUsername(username);
    				googleCheckoutUser.setRoles(Arrays.asList("ROLE_GOOGLE_CHECKOUT"));
    				Authentication usernamePasswordAuthToken = new UsernamePasswordAuthenticationToken(adminUser, googleCheckoutUser, (Collection<? extends GrantedAuthority>) getAuthorities(googleCheckoutUser));
    	    		return usernamePasswordAuthToken;
        		}
    		}
	    }
	    return null;
	}
	
	public boolean supports(Class<? extends Object> authentication) {
	    return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}
	
	public Collection<? extends GrantedAuthority> getAuthorities(AccessUser user)
	{
		List<GrantedAuthority> result = new ArrayList<GrantedAuthority>(user.getRoles().size());
		for (String role : user.getRoles()) {
			result.add(new SimpleGrantedAuthority(role));
		}
		return result;
	}
}
