/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.policy;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.PolicyForm;
import com.webjaguar.logic.WebJaguarFacade;

public class PolicyFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public PolicyFormController() {
		setSessionForm(true);
		setCommandName("policyForm");
		setCommandClass(PolicyForm.class);
		setFormView("admin/policy/form");
		setSuccessView("policyList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {

		PolicyForm policyForm = (PolicyForm) command;
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deletePolicy(policyForm.getPolicy().getId());			
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		if (policyForm.isNewPolicy()) {
			this.webJaguar.insertPolicy(policyForm.getPolicy());
		} else {
			this.webJaguar.updatePolicy(policyForm.getPolicy());
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		PolicyForm policyForm = new PolicyForm();
		if ( request.getParameter("id") != null ) {
			policyForm = new PolicyForm(this.webJaguar.getPolicyById(new Integer(request.getParameter("id"))));
		}
		
		return policyForm;
	}

}
