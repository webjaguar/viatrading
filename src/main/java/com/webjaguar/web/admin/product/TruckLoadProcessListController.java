package com.webjaguar.web.admin.product;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.TruckLoadProcessSearch;

public class TruckLoadProcessListController implements Controller {
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		TruckLoadProcessSearch truckLoadProcessSearch = getTruckLoadProcessSearch(request);
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("truckLoadProcess.jhtm"));
		}
		
		
		// gets list of truckLoad from database.
		PagedListHolder truckLoadProcessList = new PagedListHolder(this.webJaguar.getTruckLoadProcessListReport(truckLoadProcessSearch));
		
		truckLoadProcessList.setPageSize(truckLoadProcessSearch.getPageSize());
		truckLoadProcessList.setPage(truckLoadProcessSearch.getPage()-1);
		
		myModel.put("truckLoadProcessList", truckLoadProcessList);
		myModel.put("truckLoadProcessPTList", this.webJaguar.getTruckLoadProcessPTList());
		
		return new ModelAndView("admin/catalog/truckLoadProcessing/list", "model", myModel);
	}
	
	private TruckLoadProcessSearch getTruckLoadProcessSearch(HttpServletRequest request) throws ParseException {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		TruckLoadProcessSearch truckLoadProcessSearch = (TruckLoadProcessSearch) request.getSession().getAttribute( "truckLoadProcessSearch" );
			if (truckLoadProcessSearch == null) {
				truckLoadProcessSearch = new TruckLoadProcessSearch();
			request.getSession().setAttribute( "truckLoadProcessSearch", truckLoadProcessSearch );
		}
			SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
			
		    //  Set default start date to 90 days before current date
	
			
			long date90 = new Date().getTime()- (  20*24*60*60*1000);
			date90 = date90 - (  20*24*60*60*1000);
			date90 = date90 - (  20*24*60*60*1000);
			date90 = date90 - (  20*24*60*60*1000);
			date90 = date90 - (  10*24*60*60*1000);
			Long nowdate = new Date().getTime()+23*60*60*1000;
			
			System.out.println(df.parse(df.format(date90)) + "----------date90");

			// startDate
			if (request.getParameter("startDate") != null ) {
				try {
					truckLoadProcessSearch.setStartDate( df.parse( request.getParameter("startDate")) );
				} catch (ParseException e) {
		        }
			} else {
				truckLoadProcessSearch.setStartDate(df.parse(df.format(date90)));
			}

			// endDate
			if (request.getParameter("endDate") != null) {
				try {
					truckLoadProcessSearch.setEndDate( df.parse( request.getParameter("endDate")) );
				} catch (ParseException e) {
		        }
			} else {
				truckLoadProcessSearch.setEndDate(df.parse(df.format(nowdate)));
			}
			
			// name
			if (request.getParameter("name") != null) {
				truckLoadProcessSearch.setName( ServletRequestUtils.getStringParameter( request, "name", "" ));
			}
			
			// originalSku
			if (request.getParameter("originalSku") != null) {
				truckLoadProcessSearch.setOriginalSku( ServletRequestUtils.getStringParameter( request, "originalSku", "" ));
			}
			
			// producedSku
			if (request.getParameter("producedSku") != null) {
				truckLoadProcessSearch.setProducedSku( ServletRequestUtils.getStringParameter( request, "producedSku", "" ));
			}			

			// custom1
			if (request.getParameter("custom1") != null) {
				truckLoadProcessSearch.setCustom1( ServletRequestUtils.getStringParameter( request, "custom1", "" ));
			}
			
			// custom2
			if (request.getParameter("custom2") != null) {
				truckLoadProcessSearch.setCustom2( ServletRequestUtils.getStringParameter( request, "custom2", "" ));
			}
			
			// custom3
			if (request.getParameter("custom3") != null) {
				truckLoadProcessSearch.setCustom3( ServletRequestUtils.getStringParameter( request, "custom3", "" ));
			}
			
			// custom4
			if (request.getParameter("custom4") != null) {
				truckLoadProcessSearch.setCustom4( ServletRequestUtils.getStringParameter( request, "custom4", "" ));
			}
			
			//PT
			if (request.getParameter("pt") != null) {
				truckLoadProcessSearch.setPt( ServletRequestUtils.getStringParameter( request, "pt", "" ));
			}
			
			// status
			if (request.getParameter("status") != null) {
				truckLoadProcessSearch.setStatus( ServletRequestUtils.getStringParameter( request, "status", "" ));
			}
			
			// sort
			if (request.getParameter("sort") != null) {
				truckLoadProcessSearch.setSort( ServletRequestUtils.getStringParameter( request, "sort", "" ));
				}
			
			// page
			if (request.getParameter("page") != null) {
				int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
				if (page < 1) {
					truckLoadProcessSearch.setPage( 1 );
				} else {
					truckLoadProcessSearch.setPage( page );				
				}
			}			
		
			// size
			if (request.getParameter("size") != null) {
				int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
				if (size < 10) {
					truckLoadProcessSearch.setPageSize( 10 );
				} else {
					truckLoadProcessSearch.setPageSize( size );				
				}
			}
		return truckLoadProcessSearch;
	}
}
