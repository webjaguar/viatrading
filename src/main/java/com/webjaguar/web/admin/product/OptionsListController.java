/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.16.2007
 */

package com.webjaguar.web.admin.product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Option;
import com.webjaguar.model.OptionSearch;
import com.webjaguar.model.Product;

public class OptionsListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
		OptionSearch search = getSearch(request);
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		search.setProductCount(!siteConfig.get("PRODUCT_MULTI_OPTION").getValue().equals("true"));
		
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("productOptions.jhtm"));
		}
		
		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i=0; i<ids.length; i++) {
					Option option = new Option();
					option.setId(Integer.parseInt(ids[i]));
					this.webJaguar.deleteOption(option);
				}
			}
		}
		Map<String, Object> model = new HashMap<String, Object>();
		
		int count = this.webJaguar.getOptionsListCount();
		if (count < search.getOffset()-1) {
			search.setPage(1);
		}
		search.setLimit(search.getPageSize());
		search.setOffset((search.getPage()-1)*search.getPageSize());		
		
		List<Option> optionsList = this.webJaguar.getOptionsList(search);
		
		int pageCount =count/search.getPageSize();
		if (count%search.getPageSize() > 0) {
			pageCount++;
		}
		
		model.put("pageCount", pageCount);
		model.put("list", optionsList);
		model.put("pageEnd",search.getOffset()+optionsList.size());
		model.put("count", count);

		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/catalog/product/options", "model", model);
		}
		
        return new ModelAndView("admin/catalog/product/options", "model", model);
	}

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private OptionSearch getSearch(HttpServletRequest request) {
		OptionSearch search = (OptionSearch) request.getSession().getAttribute("optionSearch");
		if (search == null) {
			search = new OptionSearch();
			request.getSession().setAttribute("optionSearch", search);
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}				
		
		// item id
		if (request.getParameter("option_code") != null) {
			search.setOptionCode(ServletRequestUtils.getStringParameter(request, "option_code", ""));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		return search;
	}	
	
}
