/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.29.2006
 */

package com.webjaguar.web.admin.product;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.CategoryForm;
import com.webjaguar.web.form.ProductOptionsForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Option;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductOptionValue;

public class ProductOptionsController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private Map<String, Object> gSiteConfig;

	public ProductOptionsController() {
		setSessionForm(true);
		setCommandName("form");
		setCommandClass(ProductOptionsForm.class);
		setFormView("admin/catalog/product/optionsForm");
		setSuccessView("options.jhtm");
	}   
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {

		ProductOptionsForm form = (ProductOptionsForm) command;
		
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteOption( form.getOption() );
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		try {
			// delete
			int delete = ServletRequestUtils.getIntParameter(request, "delete", -1);
			if (delete > -1) {
				form.getOption().getProductOptions().remove(delete);
			}
			// up
			int up = ServletRequestUtils.getIntParameter(request, "up", -1);
			if (up > -1) {
				form.getOption().getProductOptions().add(up-1, form.getOption().getProductOptions().get(up));
				form.getOption().getProductOptions().remove(up+1);
			}
			// down
			int down = ServletRequestUtils.getIntParameter(request, "down", -1);
			if (down > -1) {
				form.getOption().getProductOptions().add(down+2, form.getOption().getProductOptions().get(down));
				form.getOption().getProductOptions().remove(down);
			}			
		} catch (Exception e) {
			// do nothing
		}
		
		boolean priceValueError = false;

		for (ProductOption po:form.getOption().getProductOptions()) {
			// get option name
			po.setName(ServletRequestUtils.getStringParameter(request, "optionName_" + po.getIndex(), "").trim());
			
			// get option help text
			if(!ServletRequestUtils.getStringParameter(request, "optionHelpText_" + po.getIndex()).trim().isEmpty()) {
				po.setHelpText(ServletRequestUtils.getStringParameter(request, "optionHelpText_" + po.getIndex()));
			} else {
				po.setHelpText(null);
			}
			
			// get number of colors ( for color swatch )
			po.setNumberOfColors(ServletRequestUtils.getIntParameter(request, "optionNumberOfColors_" + po.getIndex(), 0));
			
			// get option values
			po.setValues(null);
			String[] values = ServletRequestUtils.getStringParameter(request, "optionValue_" + po.getIndex(), "").split("\n");
			int index = 0;			
			for (int x=0; x<values.length; x++) {
				if ( !("".equals(values[x].trim())) ) {
					ProductOptionValue pov = new ProductOptionValue(index++);
					priceValueError = checkPrice(values[x].trim());
					
					// description
					String description = null;
					if(values[x].trim().indexOf("<txt>") != -1 && values[x].trim().indexOf("</txt>") != -1) {
						description = values[x].trim().substring(values[x].trim().indexOf("<txt>")+5, values[x].trim().indexOf("</txt>"));
					}
					
					//assigned prooduct id
					Integer assignedProductId = null;
					if(values[x].trim().indexOf("<id>") != -1 && values[x].trim().indexOf("</id>") != -1) {
						try {
							assignedProductId = Integer.parseInt( values[x].trim().substring(values[x].trim().indexOf("<id>")+4, values[x].trim().indexOf("</id>")) );
						} catch(Exception e) { }
					}
					
					//assigned products
					String includedProducts = null;
					if(values[x].trim().indexOf("<products>") != -1 && values[x].trim().indexOf("</products>") != -1) {
						try {
							includedProducts = values[x].trim().substring(values[x].trim().indexOf("<products>"), values[x].trim().indexOf("</products>")+11);
						} catch(Exception e) { }
					}
					StringTokenizer st = new StringTokenizer(values[x].trim(), " ", false);
					Double optionPrice = null;
					Double optionWeight = null;
					String imageUrl = null;
					while (st.hasMoreTokens()) {
						
						String tempToken = st.nextToken();
						if ( tempToken.startsWith( "$" )) {
							try {
								optionPrice = Double.parseDouble( tempToken.substring( 1 ) );
							} catch (NumberFormatException e) {
								optionPrice = null;
							}
							continue;
						}
						if ( tempToken.startsWith( "|$" )) {
							try {
								optionPrice = Double.parseDouble( tempToken.substring( 2 ) );
								pov.setOneTimePrice(true);
							} catch (NumberFormatException e) {
								optionPrice = null;
							}
							continue;
						}
						if ( tempToken.startsWith( "@" )) {
							try {
								imageUrl = tempToken.substring( 1 );
							} catch (NumberFormatException e) {
								imageUrl = null;
							}
							continue;
						}
						if ( tempToken.startsWith( "#" )) {
							try {
								optionWeight = Double.parseDouble( tempToken.substring( 1 ) );
							} catch (NumberFormatException e) {
								optionWeight = null;
							}
							continue;
						}
						if ( tempToken.startsWith( "^" )) {
							try {
								pov.setDependingOptionIds(tempToken.substring( 1 ));
							} catch (NumberFormatException e) {
								pov.setDependingOptionIds(null);
							}
							continue;
						}
					 }
					// to remove price on database name field
					StringBuffer searchBuffer = new StringBuffer(values[x].trim());
					for ( int i=0; i < searchBuffer.length(); i++ ) {
						if ( searchBuffer.charAt( i ) == '$') {
							searchBuffer.delete( i, searchBuffer.length() );
							break;
						}
					}
					for ( int i=0; i < searchBuffer.length(); i++ ) {
						if ( searchBuffer.charAt( i ) == '|') {
							searchBuffer.delete( i, searchBuffer.length() );
							break;
						}
					}
					// to remove @ on database name field
					for ( int i=0; i < searchBuffer.length(); i++ ) {
						if ( searchBuffer.charAt( i ) == '@') {
							searchBuffer.delete( i, searchBuffer.length() );
							break;
						}
					}
					// to remove # on database name field
					for ( int i=0; i < searchBuffer.length(); i++ ) {
						if ( searchBuffer.charAt( i ) == '#') {
							searchBuffer.delete( i, searchBuffer.length() );
							break;
						}
					}
					// to remove * on database name field
					for ( int i=0; i < searchBuffer.length(); i++ ) {
						if ( searchBuffer.charAt( i ) == '^') {
							searchBuffer.delete( i, searchBuffer.length() );
							break;
						}
					}
					// to remove <txt> and </txt> on database name field
					if ( searchBuffer.indexOf("<txt>") != -1) {
						searchBuffer.delete( searchBuffer.indexOf("<txt>"), searchBuffer.length() );
					}

					// to remove <id> and </id> on database name field
					if ( searchBuffer.indexOf("<id>") != -1) {
						searchBuffer.delete( searchBuffer.indexOf("<id>"), searchBuffer.length() );
					}
					// to remove <products> and </products> on database name field
					if ( searchBuffer.indexOf("<products>") != -1) {
						searchBuffer.delete( searchBuffer.indexOf("<products>"), searchBuffer.length() );
					}
					pov.setName(searchBuffer.toString().trim());
					pov.setOptionPrice( optionPrice );
					pov.setOptionWeight( optionWeight );
					pov.setImageUrl( imageUrl );
					pov.setDescription( description );
					pov.setAssignedProductId( assignedProductId );
					pov.setIncludedProducts( includedProducts );
					po.addValue(pov);
				}
			}
		}
		
    	Map<String, Object> model = new HashMap<String, Object>();		  	

		// check if add / update button was pressed
		if (request.getParameter("_update") != null || request.getParameter("_add") != null) {

			// check option names
			boolean hasErrors = false;
			String skuNotExist = null;
			boolean duplicate = false;
			Set<String> toCheckDuplicate = new HashSet<String>();
			for (ProductOption po:form.getOption().getProductOptions()) {
				po.setMessage(null);
				po.setValueMessage(null);
				if ("".equals(po.getName().trim())) {
					po.setMessage("form.required");
					hasErrors = true;
				} else if ( !toCheckDuplicate.add(po.getName().toLowerCase().trim()) ) {
					duplicate = true;
					po.setMessage("*");
				}
				if ( po.getType().equals( "box" ) && this.webJaguar.getProductIdBySku( po.getName() ) == null ) {
					skuNotExist = po.getName();
				}
				if (po.getType() == null || po.getType().equals("reg")) {
					// check option values
					if (po.getValues() == null) {
						po.setValueMessage("form.required");
						hasErrors = true;
					}
				}
			}
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "option.code", "form.required");
			
			// trim option code
			form.getOption().setCode(form.getOption().getCode().trim());
			
			if (!errors.hasErrors()) {
				// check option code if duplicate
				Option option = this.webJaguar.getOption(null, form.getOption().getCode());
				if (option != null && (form.isNewOption() || option.getId().compareTo(form.getOption().getId()) != 0)) {
					errors.rejectValue( "option.code", "duplicate" );			
				}
			}			
			
			model.put("var", "");
			// update
			boolean canUpdate = true;
			if (hasErrors) {
				model.put("message", "form.hasErrors");
				canUpdate = false;
			} 
			if (duplicate) {
				model.put("message", "form.hasDuplicate");
				canUpdate = false;
			} 
			if (skuNotExist != null) {
				model.put("message", "form.skuNotExist");
				model.put("var", skuNotExist);	
				canUpdate = false;
			} 
			if (priceValueError) {
				model.put("message", "form.fixPrice");	
				canUpdate = false;
			}
			if (canUpdate && !errors.hasErrors()) {
				this.webJaguar.updateProductOptions(form.getOption());				
				return new ModelAndView( new RedirectView(getSuccessView()) );					
			}
			
		} else {
			int index = 0;
			int size = form.getOption().getProductOptions().size();
			if (size > 0) {
				// get last index
				index = form.getOption().getProductOptions().get(size-1).getIndex() + 1;
			}
			ProductOption productOption = new ProductOption(index);
			if ( request.getParameter("__add") != null ) {
				form.getOption().getProductOptions().add(productOption);
				productOption.setType( "reg" );
			} else if ( request.getParameter("__add_custom_text") != null ) {
				productOption.setType( "cus" );
				form.getOption().getProductOptions().add(productOption);
			} else if ( request.getParameter("__add_box") != null ) {
				productOption.setType( "box" );
				form.getOption().getProductOptions().add(productOption);
			} else if ( request.getParameter("__add_color_swatch") != null ) {
				productOption.setType( "colorSwatch" );
				form.getOption().getProductOptions().add(productOption);
			}
		}
		
		return showForm(request, response, errors, model);
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors)
	{
		Map<String, Object> map = new HashMap<String, Object>();
		
		// labels
		map.put("labels", this.webJaguar.getLabels("protected"));
		map.put( "protectedLevels", Constants.protectedLevels() );
		return map;
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		ProductOptionsForm form = new ProductOptionsForm();
		form.getOption().setProductOptions(new ArrayList<ProductOption>());
		if (request.getParameter("optionCode") != null) {
			Option loadOption = this.webJaguar.getOption(null, ServletRequestUtils.getStringParameter(request, "optionCode", ""));
			if (loadOption != null) {
				form.setOption(loadOption);
				form.getOption().setId(null);
			}
		}
		
		Option option = this.webJaguar.getOption(ServletRequestUtils.getIntParameter( request, "id", -1 ), null);
		if (option != null) {
			option.setOldCode( option.getCode() );
			form = new ProductOptionsForm(option);
		}		
        return form;
		
	}
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}
	
	private boolean checkPrice(String valueWithPrice)
	{
		StringTokenizer st = new StringTokenizer(valueWithPrice, " ", false);
		while (st.hasMoreTokens()) {
			String tempToken = st.nextToken();
			if ( tempToken.startsWith( "$" )) {
				try {
					Double.parseDouble( tempToken.substring( 1 ) );
				} catch (NumberFormatException e) {
					return true;
				}
			}
	     }
		return false;
	}
	
}
