/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.15.2008
 */

package com.webjaguar.web.admin.product;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.Supplier;

public class ProductSupplierImportController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private HSSFFormulaEvaluator evaluator;
	
	String formView = "admin/catalog/product/supplier/import";
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {	
    	
    	Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			return new ModelAndView("admin/catalog/product/supplier/import", "message", "excelfile.notWritable");	
		}    	
    	
		// check if upload button was pressed
		if (request.getParameter("__upload") != null) {
	    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	    	MultipartFile file = multipartRequest.getFile("file");
	    	if (file != null && !file.isEmpty()) {
	    		File excel_file = new File(baseFile, "product_suppliers_import.xls");
	    		file.transferTo(excel_file);
	    		file = null; // delete to save resources
		    	return processFile(excel_file, request, gSiteConfig);	    			
	    	} else {
	        	return new ModelAndView(formView, "message", "excelfile.invalid");	    		
	    	}
		}
    	
    	return new ModelAndView(formView);
    }
    
    private ModelAndView processFile(File excel_file, HttpServletRequest request, Map gSiteConfig) {
        List<Map> addedItems = new ArrayList<Map>();
        List<Map> updatedItems = new ArrayList<Map>();
        List<Map> invalidItems = new ArrayList<Map>();
		List<Supplier> importedSupplier = new ArrayList<Supplier>();	
    	
		Map<String, Object> map = new HashMap<String, Object>();
		
		try {
			POIFSFileSystem fs =
	            new POIFSFileSystem(new FileInputStream(excel_file));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
		    
			HSSFSheet sheet = wb.getSheetAt(0);       	// first sheet
			HSSFRow row     = sheet.getRow(0);        	// first row
			
			evaluator = new HSSFFormulaEvaluator(sheet, wb);
			
			Map<String, Short> header = new HashMap<String, Short>();
			
			if (wb.getNumberOfSheets() > 1) {
				// multiple sheets
				map.put("message", "excelfile.multipleSheets");
				return new ModelAndView(formView, map);					
			}
			if (row == null) {
				// first line should be the headers
				map.put("message", "excelfile.emptyHeaders");
				return new ModelAndView(formView, map);					
			}
			
		    Iterator iter = row.cellIterator();
		    
		    // get header names
			while (iter.hasNext()) {
				HSSFCell cell = (HSSFCell) iter.next();
				String headerName = "";
				try {
					headerName = cell.getStringCellValue().trim();					
				} catch (NumberFormatException e) {
					// do nothing
				}
				if (!headerName.equals( "" )) {
					if (header.put(headerName.toLowerCase(), cell.getCellNum()) != null) {
						// duplicate header
						map.put("message", "excelfile.duplicateHeaders");
						map.put("arguments", headerName);
						return new ModelAndView(formView, map);								
					}				
				} 
			}
			
			// check required headers
			if (!header.containsKey("supplierid") || !header.containsKey("sku") || !header.containsKey("cost")) { // supplierid, productsku and cost columns are required
				if ((Boolean) gSiteConfig.get("gCUSTOMER_SUPPLIER") && !header.containsKey("percentage")) { // if consignment module is on then column percentage is required
					return new ModelAndView(formView, "message", "excelfile.missingHeaders");
				}
				return new ModelAndView(formView, "message", "excelfile.missingHeaders");
			}
			
			nextRow: for (int i=1; i<=sheet.getLastRowNum(); i++) {
				row = sheet.getRow(i);
				if (row == null) {
					continue;
				}
				
				Map<String, Object> thisMap = new HashMap<String, Object>();
				String sku = null;
				sku = getStringValue(row, header.get("sku")).trim();
				thisMap.put("sku", sku);
				if (sku.equals("")) {
					continue nextRow;
				}
				if (this.webJaguar.getProductIdBySku(sku) == null) { // sku not valid
					thisMap.put("rowNum", row.getRowNum()+1);
					thisMap.put("reason", "Invalid SKU");
					invalidItems.add(thisMap);
					continue nextRow;
				}	
				
				Integer supplierId = null;
				try {
					supplierId = getIntegerValue(row, header.get("supplierid"));
				} catch (Exception e) {
					thisMap.put("id", getStringValue(row, header.get("supplierid")));
					thisMap.put("rowNum", row.getRowNum()+1);
					thisMap.put("reason", "Invalid Supplier ID");
					invalidItems.add(thisMap);
					continue nextRow;
				}	
				if (supplierId == null) {
					// missing supplier id
					thisMap.put("id", getStringValue(row, header.get("supplierid")));
					thisMap.put("rowNum", row.getRowNum()+1);
					thisMap.put("reason", "Missing Supplier ID");
					invalidItems.add(thisMap);
					continue nextRow;					
				} else if (!this.webJaguar.isValidSupplierId(supplierId)) { // id not valid
					thisMap.put("rowNum", row.getRowNum()+1);
					thisMap.put("id", supplierId);
					thisMap.put("reason", "Invalid Supplier ID");
					invalidItems.add(thisMap);
					continue nextRow;							
				}
				
				if (invalidItems.size() == 0) { // no errors
					// save or update customer
					Supplier supplier = null;
					supplier = this.webJaguar.getProductSupplierBySIdSku( sku, supplierId );
					thisMap.put("sku", sku);
					thisMap.put("id", supplierId);
					if (supplier != null) { 		// existing supplier
						supplier.setSku( sku );
						processRow(row, header, supplier, thisMap, gSiteConfig);						
						updatedItems.add(thisMap);						
					} else {						// new supplier
						supplier = new Supplier();
						supplier.setSku( sku );
						supplier.setId( supplierId );
						supplier.setNewSupplier( true );
						processRow(row, header, supplier, thisMap, gSiteConfig);					
						addedItems.add(thisMap);
					}
					importedSupplier.add(supplier);
				} // if no errors
			}					
		} catch (IOException e) {
			return new ModelAndView(formView, "message", "excelfile.invalid");
		} catch (Exception e) {
        	return new ModelAndView(formView, "message", "excelfile.version");
		} finally {
	    	// delete excel file
			excel_file.delete();	    			
		}
		
		if (invalidItems.size() > 0) { // has errors
			map.put("invalidItems", invalidItems);
			return new ModelAndView(formView, map);					
		} else {
			if (importedSupplier.size() == 0) {	// no Customers to import
				return new ModelAndView(formView, "message", "excelfile.empty");			
			} else {
				this.webJaguar.importProductSuppliers(importedSupplier);
				this.webJaguar.insertImportExportHistory( new ImportExportHistory("prodsupp", SecurityContextHolder.getContext().getAuthentication().getName(), true ));
			}
		}
		
		map.put("updatedItems", updatedItems);
		map.put("addedItems", addedItems);
		return new ModelAndView("admin/catalog/product/supplier/importSuccess", map);
		
    }

    private Integer getIntegerValue(HSSFRow row, short cellnum) throws Exception {
		HSSFCell cell = row.getCell(cellnum);
    	Integer value = null;
    	
    	if (cell != null) {
    		switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = new Integer(cell.getStringCellValue());										
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Double cellValue = cell.getNumericCellValue();
					if (cellValue != cellValue.intValue()) {
						throw new Exception();	
					}
					value =  cellValue.intValue();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue formulaCellValue = evaluator.evaluate(cell);					
					if (formulaCellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						cellValue =  new Double(formulaCellValue.getNumberValue());
						if (cellValue != cellValue.intValue()) {
							throw new Exception();	
						}
						value =  cellValue.intValue();
					}
					break;
			} // switch
    	}
    	return value;
    }
    
    private String getStringValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		String value = "";

    	if (cell != null) {
			switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = cell.getStringCellValue();
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Integer intValue = null;
					try {
						intValue = getIntegerValue(row, cellnum);
					} catch (Exception e) {}
					if (intValue != null) {
						value = intValue.toString();
						break;
					}
					value = new Double(cell.getNumericCellValue()).toString();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue cellValue = evaluator.evaluate(cell);
					if (cellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						value = new Double(cellValue.getNumberValue()).toString();						
					}
					break;
			} // switch		
    	}
		return value;
    }
    
    private boolean getBooleanValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		boolean value = false;

    	if (cell != null) {
    		if (getStringValue(row, cellnum).trim().equals( "1" ))  {
    			value = true;
    		}
    	}
		return value;
    } 
    
    private void processRow(HSSFRow row, Map<String, Short> header, Supplier supplier, Map<String, Object> thisMap,Map gSiteConfig) {
    	StringBuffer comments = new StringBuffer();		

		// get sku
		if (header.containsKey("sku")) {
			supplier.setSku( getStringValue(row, header.get("sku")) );
		}    

		// get supplier sku
		if (header.containsKey("suppliersku")) {
			supplier.setSupplierSku( getStringValue(row, header.get("suppliersku")) );
		} 
		
		// get price
		if (header.containsKey("cost")) {
			String price = getStringValue(row, header.get("cost")).trim();
			if (price.equals("")) {
				supplier.setPrice( 0.00 );
			} else {
				try {
					supplier.setPrice( new Double(price) );
				} catch (NumberFormatException e) { // invalid price
					comments.append("invalid price, " + price + " set to 0.00" );
					supplier.setPrice( 0.00 );
				}					
			}
		}
		
		// get percentage
		if ((Boolean) gSiteConfig.get("gCUSTOMER_SUPPLIER") && header.containsKey("percentage")) {
			supplier.setPercent(getBooleanValue(row, header.get("percentage")));
		}
 		// get primary supplier
		if (header.containsKey("primarysupplier")) {
			supplier.setPrimary( getBooleanValue(row, header.get("primarysupplier")) );
		}
		thisMap.put("comments", comments);
    }
    
}