/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.product;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.Language;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAffiliateCommission;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.KeyBean;
import com.webjaguar.web.domain.Utilities;

public class ImportController extends SimpleFormController {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private GlobalDao globalDao;

	public void setGlobalDao(GlobalDao globalDao) {
		this.globalDao = globalDao;
	}

	private MailSender mailSender;

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	int gPRODUCT_IMAGES = 0;
	int gPRICE_TIERS = 0;
	int gPRICE_TABLE = 0;
	int gPRODUCT_FIELDS = 0;
	int gPROTECTED = 0;
	int gTAB = 0;
	int gAFFILIATE = 0;
	boolean PRODUCT_RATE;
	int NUMBER_PRODUCT_COMMISSION_TABLE = 0;
	int gMAIL_IN_REBATES = 0;
	boolean gALSO_CONSIDER, gCASE_CONTENT, gMYLIST, gPRESENTATION, gRECOMMENDED_LIST, gCOMPARISON, gINVENTORY, gLOYALTY, gSHOPPING_CART, gCUSTOM_LINES, gSUBSCRIPTION, gMINIMUM_INCREMENTAL_QTY,
			gMASTER_SKU, gSALES_PROMOTIONS, gMANUFACTURER, gPRODUCT_REVIEW, gPRICE_CASEPACK;
	String gASI;
	List<Language> gI18N;
	private HSSFFormulaEvaluator evaluator;

	public ImportController() {
		setSessionForm(false);
		setCommandName("importProductSearch");
		setCommandClass(ProductSearch.class);
		setFormView("admin/catalog/product/import");
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {

		
		ProductSearch search = (ProductSearch) command;

		Map<String, Object> map = new HashMap<String, Object>();

		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()) {
			map.put("message", "excelfile.notWritable");
			return showForm(request, response, errors, map);
		}
		Customer customer = null;
		if (search.getDefaultSupplierId() != null) {
			try {
				customer = this.webJaguar.getCustomerById(this.webJaguar.getSupplierById(search.getDefaultSupplierId()).getUserId());
			} catch (Exception e) {
				errors.rejectValue("defaultSupplierId", "typeMismatch.importProductSearch.supplierId");
				return showForm(request, response, errors, map);
			}
		}

		Date start = new Date();
		boolean uploadedFile = false, ftpFile = false;

		File excel_file = null;

		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile file = multipartRequest.getFile("file");
		if (file != null && !file.isEmpty()) {
			uploadedFile = true;
			excel_file = new File(baseFile, "products_import.xls");
		} else if (search.getImportFileMap() != null && ((File) search.getImportFileMap().get("file")).exists()) {
			// use file located in ftp
			ftpFile = true;
			excel_file = (File) search.getImportFileMap().get("file");
		}
		if (uploadedFile || ftpFile) {
			if (uploadedFile) {
				file.transferTo(excel_file);
				file = null; // delete to save resources
			}
			Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
			Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
			List<Product> importedProducts = new ArrayList<Product>();
			try {
				List<Map<String, Object>> addedItems = new ArrayList<Map<String, Object>>();
				List<Map<String, Object>> updatedItems = new ArrayList<Map<String, Object>>();
				if (processFile(excel_file, siteConfig, gSiteConfig, map, importedProducts, customer, addedItems, updatedItems, request)) {
					this.webJaguar.insertImportExportHistory(new ImportExportHistory("product", SecurityContextHolder.getContext().getAuthentication().getName(), true));

					int added = -1;
					int updated = -1;
					int totalAdded = 0;
					int totalUpdated = 0;
					int totalErrors = 0;

					StringBuffer sbuff = new StringBuffer();
					for (Product product : importedProducts) {
						boolean add = true;
						try {
							if (product.getId() == null) { // new product
								added++;
								this.webJaguar.insertProduct(product, request);

								totalAdded++;
								sbuff.append("Line " + addedItems.get(added).get("rowNum") + ". (added) " + product.getSku() + "\n");
							} else { // old product
								updated++;
								add = false;
								this.webJaguar.updateProduct(product, request);

								totalUpdated++;
								sbuff.append("Line " + updatedItems.get(updated).get("rowNum") + ". (updated) " + product.getSku() + "\n");
							}
						} catch (Exception e) {
							totalErrors++;
							String error = e.toString();
							if (error.contains("Caused by:")) {
								error = error.substring(error.lastIndexOf("Caused by:"));
							}
							sbuff.append("Line ");
							if (add) {
								addedItems.get(added).put("sqlError", error);

								sbuff.append(addedItems.get(added).get("rowNum"));
							} else {
								updatedItems.get(updated).put("sqlError", error);

								sbuff.append(updatedItems.get(updated).get("rowNum"));
							}
							sbuff.append(". (error) " + product.getSku() + " -- " + error + "\n");
						}
					}
					if (updatedItems.size() > 0)
						map.put("updatedItems", updatedItems);
					if (addedItems.size() > 0)
						map.put("addedItems", addedItems);

					if (siteConfig.get("PRODUCT_IMPORT_EMAIL").getValue().length() > 0) {
						emailResults(siteConfig, sbuff.toString(), start, new Date(), totalAdded, totalUpdated, totalErrors);
					}
					return new ModelAndView("admin/catalog/product/importSuccess", map);
				}
			} catch (IOException e) {
				map.put("message", "excelfile.invalid");
				return showForm(request, response, errors, map);
			} catch (Exception e) {
				e.printStackTrace();
				map.put("message", "excelfile.version");
				return showForm(request, response, errors, map);
			} finally {
				// delete excel file
				excel_file.delete();
			}
		} else {
			map.put("message", "excelfile.invalid");
			return showForm(request, response, errors, map);
		}

		return showForm(request, response, errors, map);
	}

	private boolean processFile(File excel_file, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig, Map<String, Object> map, List<Product> importedProducts, Customer customer,
			List<Map<String, Object>> addedItems, List<Map<String, Object>> updatedItems, HttpServletRequest request) throws IOException, Exception {

		List<Map<String, Object>> invalidItems = new ArrayList<Map<String, Object>>();
		boolean hasNameCol = false;

		gPRODUCT_IMAGES = (Integer) gSiteConfig.get("gPRODUCT_IMAGES");
		gPRICE_TIERS = (Integer) gSiteConfig.get("gPRICE_TIERS");
		gPRICE_TABLE = (Integer) gSiteConfig.get("gPRICE_TABLE");
		gPRODUCT_FIELDS = (Integer) gSiteConfig.get("gPRODUCT_FIELDS");
		gALSO_CONSIDER = (Boolean) gSiteConfig.get("gALSO_CONSIDER");
		gCASE_CONTENT = (Boolean) gSiteConfig.get("gCASE_CONTENT");
		gMYLIST = (Boolean) gSiteConfig.get("gMYLIST");
		gPRESENTATION = (Boolean) gSiteConfig.get("gPRESENTATION");
		gRECOMMENDED_LIST = (Boolean) gSiteConfig.get("gRECOMMENDED_LIST");
		gPROTECTED = (Integer) gSiteConfig.get("gPROTECTED");
		gCOMPARISON = (Boolean) gSiteConfig.get("gCOMPARISON");
		gINVENTORY = (Boolean) gSiteConfig.get("gINVENTORY");
		gLOYALTY = (Boolean) gSiteConfig.get("gLOYALTY");
		gSHOPPING_CART = (Boolean) gSiteConfig.get("gSHOPPING_CART");
		gCUSTOM_LINES = (Boolean) gSiteConfig.get("gCUSTOM_LINES");
		gSUBSCRIPTION = (Boolean) gSiteConfig.get("gSUBSCRIPTION");
		gTAB = (Integer) gSiteConfig.get("gTAB");
		gMINIMUM_INCREMENTAL_QTY = (Boolean) gSiteConfig.get("gMINIMUM_INCREMENTAL_QTY");
		gAFFILIATE = (Integer) gSiteConfig.get("gAFFILIATE");
		NUMBER_PRODUCT_COMMISSION_TABLE = Integer.parseInt(siteConfig.get("NUMBER_PRODUCT_COMMISSION_TABLE").getValue());
		gMASTER_SKU = (Boolean) gSiteConfig.get("gMASTER_SKU");
		gSALES_PROMOTIONS = (Boolean) gSiteConfig.get("gSALES_PROMOTIONS");
		gMAIL_IN_REBATES = (Integer) gSiteConfig.get("gMAIL_IN_REBATES");
		gMANUFACTURER = (Boolean) gSiteConfig.get("gMANUFACTURER");
		gPRODUCT_REVIEW = (Boolean) gSiteConfig.get("gPRODUCT_REVIEW");
		PRODUCT_RATE = Boolean.parseBoolean(siteConfig.get("PRODUCT_RATE").getValue());
		gI18N = this.webJaguar.getLanguageSettings();
		gASI = (String) gSiteConfig.get("gASI");
		gPRICE_CASEPACK = (Boolean) gSiteConfig.get("gPRICE_CASEPACK");

		// get accessUser username
		String accessUser = this.webJaguar.getAccessUser(request).getUsername();

		POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(excel_file));
		HSSFWorkbook wb = new HSSFWorkbook(fs);

		HSSFSheet sheet = wb.getSheetAt(0); // first sheet
		HSSFRow row = sheet.getRow(0); // first row

		evaluator = new HSSFFormulaEvaluator(sheet, wb);

		Map<String, Short> header = new HashMap<String, Short>();
		Map<String, String> skus = new HashMap<String, String>();
		Set<Short> category = new HashSet<Short>();
		Set<Short> images = new TreeSet<Short>(); // sort images

		if (wb.getNumberOfSheets() > 1) {
			// multiple sheets
			map.put("message", "excelfile.multipleSheets");
			return false;
		}
		if (row == null) {
			// first line should be the headers
			map.put("message", "excelfile.emptyHeaders");
			return false;
		}

		Iterator iter = row.cellIterator();
		while (iter.hasNext()) {
			HSSFCell cell = (HSSFCell) iter.next();
			String headerName = "";
			try {
				headerName = cell.getStringCellValue().trim();
			} catch (NumberFormatException e) {
				// do nothing
			}
			if (!headerName.equals("")) {
				if (headerName.equalsIgnoreCase("category")) {
					category.add(cell.getCellNum());
				} else if (headerName.equalsIgnoreCase("image")) {
					images.add(cell.getCellNum());
				} else {
					if (header.put(headerName.toLowerCase(), cell.getCellNum()) != null) {
						// duplicate header
						map.put("message", "excelfile.duplicateHeaders");
						map.put("arguments", headerName);
						return false;
					}
				}
			}
		}

		if (images.isEmpty()) {
			// images is no longer needed
			gPRODUCT_IMAGES = 0;
		}

		// i18n
		boolean getI18n = false;
		if (!gI18N.isEmpty()) {
			for (Language language : gI18N) {
				if (header.containsKey("name_" + language.getLanguageCode()))
					getI18n = true;
				if (header.containsKey("shortdescription_" + language.getLanguageCode()))
					getI18n = true;
				if (header.containsKey("longdescription_" + language.getLanguageCode()))
					getI18n = true;
				// extra product fields
				for (int ef = 1; ef <= gPRODUCT_FIELDS; ef++) {
					if (header.containsKey("field_" + ef + "_" + language.getLanguageCode()))
						getI18n = true;
				}
			}
		}

		// check required headers
		if (!header.containsKey("sku")) { // sku column is required
			map.put("message", "excelfile.missingHeaders");
			return false;
		}

		if (header.containsKey("name"))
			hasNameCol = true;

		nextRow: for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			row = sheet.getRow(i);
			if (row == null) {
				continue;
			}

			// check if new or existing item
			String sku = null;
			sku = getStringValue(row, header.get("sku")).trim();
			if (sku.equals("")) {
				sku = null;
			} else if (customer != null && !sku.startsWith(customer.getSupplierPrefix() + "-")) {
				// add supplier's prefix
				sku = customer.getSupplierPrefix() + "-" + sku;
			}
			Map<String, Object> thisMap = new HashMap<String, Object>();
			thisMap.put("sku", sku);
			thisMap.put("rowNum", row.getRowNum() + 1);

			Integer id = null;
			if (header.containsKey("id")) {
				try {
					id = getIntegerValue(row, header.get("id"));
				} catch (Exception e) {
					thisMap.put("id", getStringValue(row, header.get("id")));
					thisMap.put("reason", "Invalid Product ID");
					invalidItems.add(thisMap);
					continue nextRow;
				}
				if (id != null && !this.webJaguar.isValidProductId(id)) { // id not valid
					thisMap.put("id", id);
					thisMap.put("reason", "Invalid Product ID");
					invalidItems.add(thisMap);
					continue nextRow;
				}
			}
			Integer existingId = this.webJaguar.getProductIdBySku(sku);
			if (id != null) {
				if (sku != null) {
					if (existingId != null) { // sku exists
						if (existingId.compareTo(id) != 0) { // not the same item
							thisMap.put("id", id);
							thisMap.put("reason", "An item using this SKU already exists.");
							invalidItems.add(thisMap);
							continue nextRow;
						}
					}
					String exsitingSku = this.webJaguar.getProductSkuById(id);
					if(exsitingSku != null){
						if(exsitingSku.compareTo(sku) != 0){
							thisMap.put("id", id);
							thisMap.put("reason", "Cannot change SKU.");
							invalidItems.add(thisMap);
							continue nextRow;
						}
					}
				
				} else {
					// require sku
					thisMap.put("id", id);
					thisMap.put("reason", "Sku required.");
					invalidItems.add(thisMap);
					continue nextRow;
				}
			}
			if (existingId != null)
				id = existingId;

			// check for duplicate skus
			if (sku != null) {
				if (id == null) {
						if (Utilities.containsSpace(sku)) {
							thisMap.put("id", id);
							thisMap.put("reason", "SKU cannot have space");
							invalidItems.add(thisMap);
							continue nextRow;
						}
				}
				String idString = "";
				if (id != null) {
					idString = id.toString();
				}
				String oldValue = skus.put(sku.toLowerCase(), idString);
				if (oldValue != null) {
					if (!oldValue.equals(idString) || oldValue.equals("")) {
						thisMap.put("id", id);
						thisMap.put("reason", "Duplicate SKU.");
						invalidItems.add(thisMap);
						continue nextRow;
					}
				}
			}

			// check name
			String name = null;
			if (hasNameCol) {
				name = getStringValue(row, header.get("name"));
				if (name.trim().equals("") && (id != null || sku != null)) {
					thisMap.put("id", id);
					thisMap.put("reason", "Name is required.");
					invalidItems.add(thisMap);
					continue nextRow;
				}
				thisMap.put("name", name);
			}

			if (invalidItems.size() == 0) { // no errors
				// save or update product
				Product product = new Product();
				// existing item
				if (id != null) {
					product = this.webJaguar.getProductById(id, gPRODUCT_IMAGES, false, null);
					product.setSku(sku);
					product.setLastModifiedBy(accessUser);
					if (hasNameCol) {
						product.setName(name);
					}
					thisMap.put("id", id);
					if (getI18n) { // i18n
						product.setI18nProduct(this.webJaguar.getI18nProduct(id));
					}
					processRow(row, header, product, images, category, thisMap, siteConfig, customer);
					updatedItems.add(thisMap);
				} // new item
				else if (sku != null) {
					product.setSku(sku);
					product.setCreatedBy(accessUser);
					if (!hasNameCol) {
						product.setName(sku);
					} else {
						product.setName(name);
					}
					if (getI18n) { // i18n
						product.setI18nProduct(new HashMap<String, Product>());
					}
					processRow(row, header, product, images, category, thisMap, siteConfig, customer);
					addedItems.add(thisMap);
				} else { // no sku
					continue nextRow;
				}
				importedProducts.add(product);
			} // if no errors
		}

		if (invalidItems.size() > 0) { // has errors
			map.put("message", "importFailedTitle");
			map.put("invalidItems", invalidItems);
			return false;
		} else if (importedProducts.size() == 0) { // no items to import
			map.put("message", "excelfile.empty");
			return false;
		}

		map.put("hasNameCol", hasNameCol);

		return true;
	}

	private Integer getIntegerValue(HSSFRow row, short cellnum) throws Exception {
		HSSFCell cell = row.getCell(cellnum);
		Integer value = null;

		if (cell != null) {
			Double cellValue = null;
			switch (cell.getCellType()) {
			case HSSFCell.CELL_TYPE_STRING:
				value = new Integer(cell.getStringCellValue());
				break;
			case HSSFCell.CELL_TYPE_NUMERIC:
				cellValue = cell.getNumericCellValue();
				if (cellValue != cellValue.intValue()) {
					throw new Exception();
				}
				value = cellValue.intValue();
				break;
			case HSSFCell.CELL_TYPE_FORMULA:
				evaluator.setCurrentRow(row);
				HSSFFormulaEvaluator.CellValue formulaCellValue = evaluator.evaluate(cell);
				if (formulaCellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
					cellValue = new Double(formulaCellValue.getNumberValue());
					if (cellValue != cellValue.intValue()) {
						throw new Exception();
					}
					value = cellValue.intValue();
				}
				break;
			} // switch
		}
		return value;
	}

	private String getStringValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		String value = "";

		if (cell != null) {
			switch (cell.getCellType()) {
			case HSSFCell.CELL_TYPE_STRING:
				value = cell.getStringCellValue();
				break;
			case HSSFCell.CELL_TYPE_NUMERIC:
				Integer intValue = null;
				try {
					intValue = getIntegerValue(row, cellnum);
				} catch (Exception e) {
				}
				if (intValue != null) {
					value = intValue.toString();
					break;
				}
				value = new Double(cell.getNumericCellValue()).toString();
				break;
			case HSSFCell.CELL_TYPE_FORMULA:
				evaluator.setCurrentRow(row);
				HSSFFormulaEvaluator.CellValue cellValue = evaluator.evaluate(cell);
				if (cellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
					value = new Double(cellValue.getNumberValue()).toString();
				} else if (cellValue.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					value = cellValue.getStringValue();
				}
				break;
			} // switch
		}
		return value;
	}

	private boolean getBooleanValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		boolean value = false;

		if (cell != null) {
			if (getStringValue(row, cellnum).trim().equals("1")) {
				value = true;
			}
		}
		return value;
	}

	private void processRow(HSSFRow row, Map<String, Short> header, Product product, Set<Short> images, Set<Short> category, Map<String, Object> thisMap, Map<String, Configuration> siteConfig,
			Customer customer) {
		Class<Product> c = Product.class;
		Class paramTypeString[] = { String.class };
		Class paramTypeDouble[] = { Double.class };
		Class paramTypeInteger[] = { Integer.class };

		StringBuffer comments = new StringBuffer();

		// supplier id
		if (customer != null) {
			product.setDefaultSupplierId(customer.getSupplierId());
		}

		// get Manufacture
		if (gMANUFACTURER && header.containsKey("manufacture")) {
			product.setManufactureName(getStringValue(row, header.get("manufacture")));
		}

		// get short description
		if (header.containsKey("shortdescription")) {
			product.setShortDesc(getStringValue(row, header.get("shortdescription")));
		}

		// get long description
		if (header.containsKey("longdescription")) {
			product.setLongDesc(getStringValue(row, header.get("longdescription")));
		}

		// get HTML Add To Cart
		if (header.containsKey("htmladdtocart")) {
			product.setHtmlAddToCart(getStringValue(row, header.get("htmladdtocart")));
		}

		// get extra product fields, uses java reflection
		for (int ef = 1; ef <= gPRODUCT_FIELDS; ef++) {
			if (header.containsKey("field_" + ef)) {
				try {
					Method m = c.getMethod("setField" + ef, paramTypeString);
					Object arglist[] = { getStringValue(row, header.get("field_" + ef)) };
					m.invoke(product, arglist);
				} catch (Exception e) {
					// do nothing
				}
			}
		}

		// get images
		if (!images.isEmpty()) {
			List<ProductImage> productImages = new ArrayList<ProductImage>();
			byte index = 1;
			for (Short colnum : images) {
				String imageUrl = getStringValue(row, colnum);
				if (!imageUrl.trim().equals("") && gPRODUCT_IMAGES > productImages.size()) {
					ProductImage image = new ProductImage();
					image.setIndex(index++);
					image.setImageUrl(imageUrl);
					if (!image.isAbsolute() && customer != null && !image.getImageUrl().startsWith(customer.getSupplierId() + "/")) {
						// add supplier's folder
						image.setImageUrl(customer.getSupplierId() + "/" + imageUrl);
					}
					productImages.add(image);
				}
			}
			product.setImages(productImages);
		} else {
			// set null to bypass update/insert to make import faster
			product.setImages(null);
		}

		// get image layout
		if (header.containsKey("imagelayout")) {
			String imageLayout = getStringValue(row, header.get("imagelayout")).trim();
			if (imageLayout.length() > 2) {
				imageLayout = "";
			}
			product.setImageLayout(imageLayout);
		}

		// get category associations
		//StringBuffer categoryIdString = new StringBuffer();
		
		if (!category.isEmpty()) {
			Set<Object> categories = new HashSet<Object>();
			for (Short colnum : category) {
				try {
					Integer catId = getIntegerValue(row, colnum);
					if (catId != null)
						categories.add(catId);
					    //categoryIdString.append(catId.toString() + ",");
				} catch (Exception e) {
					// do nothing
				}
			} // for
			product.setCatIds(categories);
			
			//String of category ids
			//product.setCategoryIds(categoryIdString.toString());
		} else {
			// set null to bypass update/insert
			product.setCatIds(null);
		}
		
		//

		// get keywords
		if (header.containsKey("keywords")) {
			product.setKeywords(getStringValue(row, header.get("keywords")));
		}
		
		// redirect url English
		if (header.containsKey("redirecturlen")){
			product.setRedirectUrlEn(getStringValue(row, header.get("redirecturlen")));
		}
		
		// redirect url Spanish

		if (header.containsKey("redirecturles")){
			product.setRedirectUrlEs(getStringValue(row, header.get("redirecturles")));
		}
		
		// Category ids string
		if (header.containsKey("catidstring")){
			 product.setCategoryIds(getStringValue(row, header.get("catidstring")));
		}

		// get image folder
		if (header.containsKey("imagefolder")) {
			String imageFolder = getStringValue(row, header.get("imagefolder")).trim();
			if (imageFolder.equals("") || imageFolder.startsWith(".")) {
				product.setImageFolder(null);
			} else
				product.setImageFolder(imageFolder);
		}

		// get Product layout
		if (header.containsKey("productlayout")) {
			String layout = getStringValue(row, header.get("productlayout"));
			if (layout.equals("")) {
				product.setProductLayout("");
			} else {
				for (KeyBean productLayout : Constants.PRODUCT_LAYOUT) {
					if (layout != null && layout.equalsIgnoreCase(productLayout.getValue().toString())) {
						product.setProductLayout(productLayout.getName().toString());
					}
				}
			}
		}

		// get weight
		if (header.containsKey("weight")) {
			String weight = getStringValue(row, header.get("weight")).trim();
			if (weight.equals("")) {
				product.setWeight(null);
			} else {
				try {
					product.setWeight(new Double(weight));
				} catch (NumberFormatException e) {
					comments.append("invalid weight, ");
				}
			}
		}

		// upsMaxItemsInPackage
		if (header.containsKey("upsmaxitemsinpackage")) {
			try {
				Integer upsMaxItemsInPackage = getIntegerValue(row, header.get("upsmaxitemsinpackage"));
				if (!(upsMaxItemsInPackage.equals(null))) {
					product.setUpsMaxItemsInPackage(upsMaxItemsInPackage.doubleValue());
				}
			} catch (Exception e) {
			}
		}

		// uspsMaxItemsInPackage
		if (header.containsKey("uspsmaxitemsinpackage")) {
			try {
				Integer uspsMaxItemsInPackage = getIntegerValue(row, header.get("uspsmaxitemsinpackage"));
				if (!(uspsMaxItemsInPackage.equals(null))) {
					product.setUspsMaxItemsInPackage(uspsMaxItemsInPackage.doubleValue());
				}
			} catch (Exception e) {
			}
		}

		// dimension
		if (header.containsKey("packagelength")) {
			String packageLength = getStringValue(row, header.get("packagelength")).trim();
			if (packageLength.equals("")) {
				product.setPackageL(null);
			} else {
				try {
					product.setPackageL(new Double(packageLength));
				} catch (NumberFormatException e) {
					comments.append("invalid package length, ");
				}
			}
		}
		if (header.containsKey("packagewidth")) {
			String packageWidth = getStringValue(row, header.get("packagewidth")).trim();
			if (packageWidth.equals("")) {
				product.setPackageW(null);
			} else {
				try {
					product.setPackageW(new Double(packageWidth));
				} catch (NumberFormatException e) {
					comments.append("invalid package width, ");
				}
			}
		}
		if (header.containsKey("packageheight")) {
			String packageHeight = getStringValue(row, header.get("packageheight")).trim();
			if (packageHeight.equals("")) {
				product.setPackageH(null);
			} else {
				try {
					product.setPackageH(new Double(packageHeight));
				} catch (NumberFormatException e) {
					comments.append("invalid package height, ");
				}
			}
		}

		// get msrp
		if (header.containsKey("msrp")) {
			String msrp = getStringValue(row, header.get("msrp")).trim();
			if (msrp.equals("")) {
				product.setMsrp(null);
			} else {
				try {
					product.setMsrp(new Double(msrp));
				} catch (NumberFormatException e) { // invalid msrp
					comments.append("invalid MSRP, ");
				}
			}
		}

		// get salesTag ID
		if (gSALES_PROMOTIONS && header.containsKey("salestagid")) {
			String salesTagIs = getStringValue(row, header.get("salestagid")).trim();
			if (salesTagIs.equals("")) {
				product.setSalesTag(null);
			} else {
				try {
					product.setSalesTagId(new Integer(salesTagIs));
				} catch (NumberFormatException e) { // invalid salesTagId
					comments.append("invalid salesTag Id, ");
				}
			}
		}

		// get Mail In Rebate ID
		if (gMAIL_IN_REBATES > 0 && header.containsKey("mailinrebateid")) {
			String mailInRebateId = getStringValue(row, header.get("mailinrebateid")).trim();
			if (mailInRebateId.equals("")) {
				product.setMailInRebate(null);
			} else {
				try {
					product.setRebateId(new Integer(mailInRebateId));
				} catch (NumberFormatException e) { // invalid salesTagId
					comments.append("invalid mailInRebate Id, ");
				}
			}
		}

		// get prices
		for (int i = 1; i <= gPRICE_TIERS; i++) {
			if (header.containsKey("price" + i)) {
				String price = getStringValue(row, header.get("price" + i)).trim();
				try {
					Method m = c.getMethod("setPrice" + i, paramTypeDouble);
					Object arglist[] = new Object[1];
					if (price.equals("")) {
						arglist[0] = null;
						m.invoke(product, arglist);
					} else {
						try {
							arglist[0] = new Double(price);
							m.invoke(product, arglist);
						} catch (NumberFormatException e) { // invalid price
							comments.append("invalid Price" + i + ", ");
						}
					}
				} catch (Exception e) {
					// do nothing
				}
			}
		}

		// get qty breaks
		for (int i = 1; i < gPRICE_TIERS; i++) {
			if (header.containsKey("qtybreak" + i)) {
				try {
					Method m = c.getMethod("setQtyBreak" + i, paramTypeInteger);
					Object arglist[] = { getIntegerValue(row, header.get("qtybreak" + i)) };
					m.invoke(product, arglist);
				} catch (Exception e) {
					comments.append("invalid QtyBreak" + i + ", ");
				}
			}
		}

		// get cost
		if (siteConfig.get("COST_TIERS").getValue().equals("true")) {
			for (int i = 1; i < gPRICE_TIERS; i++) {
				if (header.containsKey("cost" + i)) {
					String cost = getStringValue(row, header.get("cost" + i)).trim();
					try {
						Method m = c.getMethod("setCost" + i, paramTypeDouble);
						Object arglist[] = new Object[1];
						if (cost.equals("")) {
							arglist[0] = null;
							m.invoke(product, arglist);
						} else {
							try {
								arglist[0] = new Double(cost);
								m.invoke(product, arglist);
							} catch (NumberFormatException e) { // invalid price
								comments.append("invalid Cost" + i + ", ");
							}
						}
					} catch (Exception e) {
						// do nothing
					}

				}
			}
		}

		// get Price Tables
		for (int i = 1; i <= gPRICE_TABLE; i++) {
			if (header.containsKey("pricetable" + i)) {
				String price = getStringValue(row, header.get("pricetable" + i)).trim();
				try {
					Method m = c.getMethod("setPriceTable" + i, paramTypeDouble);
					Object arglist[] = new Object[1];
					if (price.equals("")) {
						arglist[0] = null;
						m.invoke(product, arglist);
					} else {
						try {
							arglist[0] = new Double(price);
							m.invoke(product, arglist);
						} catch (NumberFormatException e) { // invalid price
							comments.append("invalid PriceTable" + i + ", ");
						}
					}
				} catch (Exception e) {
					// do nothing
				}
			}
		}

		// get Price Case Pack
		if (gPRICE_CASEPACK) {

			// PriceCasePack Qty
			if (header.containsKey("pricecasepackqty")) {
				try {
					Integer priceCasePackQty = Integer.parseInt(getStringValue(row, header.get("pricecasepackqty")).trim());
					if (priceCasePackQty == null || priceCasePackQty <= 0) {
						product.setPriceCasePackQty(null);
					} else {
						product.setPriceCasePackQty(priceCasePackQty);
					}
				} catch (Exception e) {
					product.setPriceCasePackQty(null);
				}
			}

			for (int i = 1; i <= 10; i++) {
				if (header.containsKey("pricecasepack" + i)) {
					String price = getStringValue(row, header.get("pricecasepack" + i)).trim();
					try {
						Method m = c.getMethod("setPriceCasePack" + i, paramTypeDouble);
						Object arglist[] = new Object[1];
						if (price.equals("")) {
							arglist[0] = null;
							m.invoke(product, arglist);
						} else {
							try {
								arglist[0] = new Double(price);
								m.invoke(product, arglist);
							} catch (NumberFormatException e) { // invalid price
								comments.append("invalid PriceCasePack" + i + ", ");
							}
						}
					} catch (Exception e) {
						// do nothing
					}
				}
			}
		}

		// price quote
		if (siteConfig.get("PRODUCT_QUOTE").getValue().equals("true") && header.containsKey("pricequote")) {
			product.setQuote(getBooleanValue(row, header.get("pricequote")));
		}

		// commission
		if (gAFFILIATE > 0) {
			Class<ProductAffiliateCommission> commissionClass = ProductAffiliateCommission.class;
			for (int i = 1; i <= NUMBER_PRODUCT_COMMISSION_TABLE; i++) {
				if (header.containsKey("commissiontable" + i)) {
					String commissionTable = getStringValue(row, header.get("commissiontable" + i)).trim();
					try {
						Method m = commissionClass.getMethod("setCommissionTable" + i, paramTypeDouble);
						Object arglist[] = new Object[1];
						if (commissionTable.equals("")) {
							arglist[0] = null;
							m.invoke(product.getCommissionTables(), arglist);
						} else {
							try {
								arglist[0] = new Double(commissionTable);
								m.invoke(product.getCommissionTables(), arglist);
							} catch (NumberFormatException e) { // invalid price
								comments.append("invalid CommissionTable" + i + ", ");
							}
						}
					} catch (Exception e) {
						comments.append("invalid CommissionTable" + i + ", ");
					}
				}
			}
		}

		// get prices
		for (int i = 1; i <= gTAB; i++) {
			if (header.containsKey("tab" + i)) {
				String tabName = getStringValue(row, header.get("tab" + i)).trim();
				try {
					Method m = c.getMethod("setTab" + i, paramTypeString);
					Object arglist[] = new Object[1];
					if (tabName.equals("")) {
						arglist[0] = null;
						m.invoke(product, arglist);
					} else {
						arglist[0] = tabName;
						m.invoke(product, arglist);
					}
				} catch (Exception e) {
					// do nothing
				}
			}
			if (header.containsKey("tabcontent" + i)) {
				String tabContent = getStringValue(row, header.get("tabcontent" + i)).trim();
				try {
					Method m = c.getMethod("setTab" + i + "Content", paramTypeString);
					Object arglist[] = new Object[1];
					if (tabContent.equals("")) {
						arglist[0] = null;
						m.invoke(product, arglist);
					} else {
						arglist[0] = tabContent;
						m.invoke(product, arglist);
					}
				} catch (Exception e) {
					// do nothing
				}
			}
		}

		// get login require
		if (header.containsKey("loginrequired")) {
			product.setLoginRequire(getBooleanValue(row, header.get("loginrequired")));
		}

		// hide price
		if (header.containsKey("hideprice")) {
			product.setHidePrice(getBooleanValue(row, header.get("hideprice")));
		}

		// hide msrp
		if (header.containsKey("hidemsrp")) {
			product.setHideMsrp(getBooleanValue(row, header.get("hidemsrp")));
		}

		// hide left bar
		if (header.containsKey("hideleftbar")) {
			product.setHideLeftBar(getBooleanValue(row, header.get("hideleftbar")));
		}

		// hide right bar
		if (header.containsKey("hiderightbar")) {
			product.setHideRightBar(getBooleanValue(row, header.get("hiderightbar")));
		}

		// taxable
		if (header.containsKey("taxable")) {
			product.setTaxable(getBooleanValue(row, header.get("taxable")));
		}

		// searchable
		if (header.containsKey("searchable")) {
			product.setSearchable(getBooleanValue(row, header.get("searchable")));
		}

		// active
		if (header.containsKey("active")) {
			product.setActive(getBooleanValue(row, header.get("active")));
		}

		
		// No price override
		if (header.containsKey("no-price override")) {
			product.setRestrictPriceChange(getBooleanValue(row, header.get("no-price override")));
		}
				
		// Freeze
		if ((gASI.equals("API") || gASI.equals("File")) && header.containsKey("freeze")) {
			product.setFeedFreeze(getBooleanValue(row, header.get("freeze")));
		}

		// Product Review
		if ((gPRODUCT_REVIEW || PRODUCT_RATE) && header.containsKey("productreview")) {
			product.setEnableRate(getBooleanValue(row, header.get("productreview")));
		}

		if (header.containsKey("fob")) {
			product.setFobZipCode(getStringValue(row, header.get("fob")));
		}
		
		if (header.containsKey("classfield")) {
			System.out.println(getStringValue(row, header.get("classfield")));
			product.setClassField(getStringValue(row, header.get("classfield")));
		}
		
		if (header.containsKey("leaveoncloud")) {
			product.setSoftLink(getBooleanValue(row, header.get("leaveoncloud")));
			
		}
		// custom shipping enabled
		if (gSHOPPING_CART && header.containsKey("customshippingenabled")) {
			product.setCustomShippingEnabled(getBooleanValue(row, header.get("customshippingenabled")));
		}

		// option code
		if (header.containsKey("optioncode")) {
			product.setOptionCode(getStringValue(row, header.get("optioncode")));
		}

		// cross items
		if (header.containsKey("crossitemscode") && gPRICE_TIERS > 1) {
			product.setCrossItemsCode(getStringValue(row, header.get("crossitemscode")));
		}

		// subscription
		if (gSUBSCRIPTION) {
			if (header.containsKey("autoship")) {
				product.setSubscription(getBooleanValue(row, header.get("autoship")));
			}
			if (header.containsKey("autoshipdiscount")) {
				try {
					Double discount = new Double(getStringValue(row, header.get("autoshipdiscount")).trim());
					if (discount < 0) {
						product.setSubscriptionDiscount(0.0);
					} else if (discount > 100) {
						product.setSubscriptionDiscount(100.0);
					} else {
						product.setSubscriptionDiscount(discount);
					}
				} catch (NumberFormatException e) { // invalid discount
					product.setSubscriptionDiscount(null);
				}
			}
		}

		// also consider
		if (header.containsKey("alsoconsider") && gALSO_CONSIDER) {
			product.setAlsoConsider(getStringValue(row, header.get("alsoconsider")));
		}

		// case content
		if (gCASE_CONTENT && header.containsKey("casecontent")) {
			try {
				Integer caseContent = Integer.parseInt(getStringValue(row, header.get("casecontent")).trim());
				if (caseContent == null || caseContent <= 0) {
					product.setCaseContent(null);
				} else {
					product.setCaseContent(caseContent);
				}
			} catch (Exception e) {
				product.setCaseContent(null);
			}
		}

		// minimum / incremental
		if (gMINIMUM_INCREMENTAL_QTY && header.containsKey("minimumqty")) {
			try {
				Integer minimumQty = Integer.parseInt(getStringValue(row, header.get("minimumqty")).trim());
				if (minimumQty == null || minimumQty <= 0) {
					product.setMinimumQty(null);
				} else {
					product.setMinimumQty(minimumQty);
				}
			} catch (Exception e) {
				product.setMinimumQty(null);
			}
		}
		if (gMINIMUM_INCREMENTAL_QTY && header.containsKey("incrementalqty")) {
			try {
				Integer incrementalQty = Integer.parseInt(getStringValue(row, header.get("incrementalqty")).trim());
				if (incrementalQty == null || incrementalQty <= 0) {
					product.setIncrementalQty(null);
				} else {
					product.setIncrementalQty(incrementalQty);
				}
			} catch (Exception e) {
				product.setIncrementalQty(null);
			}
		}

		// packing
		if (header.containsKey("packing")) {
			product.setPacking(getStringValue(row, header.get("packing")));
		}

		// add to presentation
		if (gPRESENTATION && header.containsKey("addtopresentation")) {
			product.setAddToPresentation(getBooleanValue(row, header.get("addtopresentation")));
		}

		// add To List
		if (gMYLIST && header.containsKey("addtolist")) {
			product.setAddToList(getBooleanValue(row, header.get("addtolist")));
		}

		if (gRECOMMENDED_LIST) {
			// recommended list
			if (header.containsKey("recommendedlist")) {
				product.setRecommendedList(getStringValue(row, header.get("recommendedlist")));
			}

			// recommended list title
			if (header.containsKey("recommendedlisttitle")) {
				product.setRecommendedListTitle(getStringValue(row, header.get("recommendedlisttitle")));
				if (product.getRecommendedListTitle().length() > 50) {
					comments.append("invalid recommended list title, ");
					product.setRecommendedListTitle("");
				}
			}

			// recommended list display
			if (header.containsKey("recommendedlistdisplay")) {
				product.setRecommendedListDisplay(getStringValue(row, header.get("recommendedlistdisplay")));
				if (product.getRecommendedListDisplay().length() > 6) {
					comments.append("invalid recommended list display, ");
					product.setRecommendedListDisplay("");
				}
			}
		}

		// protected level
		if (header.containsKey("protectedlevel") && gPROTECTED > 0) {
			try {
				int protectedlevel = Integer.parseInt(getStringValue(row, header.get("protectedlevel")).trim());
				if (protectedlevel > gPROTECTED || protectedlevel == 0) {
					product.setProtectedLevel("0");
				} else {
					StringBuffer protectedLevel = new StringBuffer("1");
					for (int i = 1; i < protectedlevel; i++) {
						protectedLevel.append("0");
					}
					product.setProtectedLevel(protectedLevel.toString());
				}
			} catch (Exception e) {
				product.setProtectedLevel("0");
			}
		}

		// comparison
		if (gCOMPARISON && header.containsKey("compare")) {
			product.setCompare(getBooleanValue(row, header.get("compare")));
		}

		// get inventory
		if (gINVENTORY && siteConfig.get("INVENTORY_ON_PRODUCT_IMPORT_EXPORT").getValue().equals("true")) {
			if (header.containsKey("onhand") && !siteConfig.get("INVENTORY_HISTORY").getValue().equals("true")) {
				Integer inventory = null;
				Integer inventoryAFS = null;
				try {
					inventory = Integer.parseInt(getStringValue(row, header.get("onhand")).trim());
				} catch (NumberFormatException e) {
					product.setInventory(null);
					comments.append("No inventory");
				}
				try {
					inventoryAFS = Integer.parseInt(getStringValue(row, header.get("availableforsale")).trim());
				} catch (NumberFormatException e) {
				}
				if (inventoryAFS == null) {
					if (inventory == null) {
						product.setInventoryAFS(null);
					} else {
						product.setInventoryAFS(inventory);
					}
				} else {
					product.setInventoryAFS(inventoryAFS);
				}
				if (inventory == null) {
					product.setInventory(null);
				} else {
					product.setInventory(inventory);
				}
			}

			// get negative inventory
			if (header.containsKey("allownegativeinventory")) {
				product.setNegInventory(getBooleanValue(row, header.get("allownegativeinventory")));
			}

			// show negative inventory
			if (header.containsKey("shownegativeinventory")) {
				product.setShowNegInventory(getBooleanValue(row, header.get("shownegativeinventory")));
			}

			// low inventory alert
			if (header.containsKey("lowinventoryalert")) {
				try {
					Integer lowInventory = Integer.parseInt(getStringValue(row, header.get("lowinventoryalert")).trim());
					if (lowInventory == null || lowInventory <= 0) {
						product.setLowInventory(null);
					} else {
						product.setLowInventory(lowInventory);
					}
				} catch (Exception e) {
					product.setLowInventory(null);
				}
			}
		}

		// loyalty point
		if (gLOYALTY && header.containsKey("loyaltypoint")) {
			String point = getStringValue(row, header.get("loyaltypoint")).trim();
			if (point.equals("")) {
				product.setLoyaltyPoint(null);
			} else {
				try {
					product.setLoyaltyPoint(new Double(point));
				} catch (NumberFormatException e) { // invalid point
					comments.append("invalid point, ");
				}
			}
		}

		// custom lines
		if (gCUSTOM_LINES && header.containsKey("numberofcustomline")) {
			try {
				String numCustomLine = getStringValue(row, header.get("numberofcustomline")).trim();
				if (numCustomLine.equals("") || numCustomLine == null || new Integer(numCustomLine) <= 0 || new Integer(numCustomLine) > 10) {
					product.setNumCustomLines(null);
				} else {
					product.setNumCustomLines(new Integer(numCustomLine));
				}
			} catch (Exception e) {
				comments.append("invalid Number of custom Line, ");
			}

			// number of character
			if (header.containsKey("customlinecharacter")) {
				try {
					Integer numChar = Integer.parseInt(getStringValue(row, header.get("customlinecharacter")).trim());
					if (numChar == null || numChar < 10 || numChar > 80) {
						product.setCustomLineCharacter(null);
					} else {
						product.setCustomLineCharacter(numChar);
					}
				} catch (Exception e) {
					product.setCustomLineCharacter(null);
				}
			}
		}

		// Parent sku
		if (header.containsKey("parentsku") && gMASTER_SKU) {
			product.setMasterSku(getStringValue(row, header.get("parentsku")));
		}

		// UPC
		if (header.containsKey("upc")) {
			product.setUpc(getStringValue(row, header.get("upc")));
		}

		// Etilize ID
		if (header.containsKey("etilizeid")) {
			try {
				product.setEtilizeId(getIntegerValue(row, header.get("etilizeid")));
			} catch (Exception e) {
			}
		}

		// i18n
		if (!gI18N.isEmpty() && product.getI18nProduct() != null) {
			for (Language language : gI18N) {
				boolean addI18n = false;
				Product i18nProduct = product.getI18nProduct().get(language.getLanguageCode());
				if (i18nProduct == null) {
					i18nProduct = new Product();
					i18nProduct.setLang(language.getLanguageCode());
				}
				if (header.containsKey("name_" + language.getLanguageCode())) {
					i18nProduct.setI18nName(getStringValue(row, header.get("name_" + language.getLanguageCode())));
					addI18n = true;
				}
				if (header.containsKey("shortdescription_" + language.getLanguageCode())) {
					i18nProduct.setI18nShortDesc(getStringValue(row, header.get("shortdescription_" + language.getLanguageCode())));
					addI18n = true;
				}
				if (header.containsKey("longdescription_" + language.getLanguageCode())) {
					i18nProduct.setI18nLongDesc(getStringValue(row, header.get("longdescription_" + language.getLanguageCode())));
					addI18n = true;
				}
				// get extra product fields, uses java reflection
				for (int ef = 1; ef <= gPRODUCT_FIELDS; ef++) {
					if (header.containsKey("field_" + ef + "_" + language.getLanguageCode())) {
						try {
							Method m = c.getMethod("setField" + ef, paramTypeString);
							Object arglist[] = { getStringValue(row, header.get("field_" + ef + "_" + language.getLanguageCode())) };
							m.invoke(i18nProduct, arglist);
							addI18n = true;
						} catch (Exception e) {
							// do nothing
						}
					}
				}
				if (addI18n) {
					product.addI18nProduct(language.getLanguageCode(), i18nProduct);
				} else {
					// remove to bypass step of updating on database
					product.getI18nProduct().remove(language.getLanguageCode());
				}
			}
		}

		// retain on cart
		if (header.containsKey("retainoncart")) {
			product.setRetainOnCart(getBooleanValue(row, header.get("retainoncart")));
		}

		// sitemap priority
		if (siteConfig.get("SITEMAP").getValue().equals("true") && header.containsKey("sitemappriority")) {
			try {
				product.setSiteMapPriority(Double.parseDouble(getStringValue(row, header.get("sitemappriority")).trim()));
			} catch (Exception e) {
				product.setSiteMapPriority(0.5);
			}
		}

		thisMap.put("comments", comments);
	}

	public void autoImport() {
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();

		if (!(Boolean) gSiteConfig.get("gAUTO_IMPORT") || !(Boolean) gSiteConfig.get("gSITE_ACTIVE") || siteConfig.get("AUTO_IMPORT_SERVER").getValue().trim().equals("")
				|| siteConfig.get("AUTO_IMPORT_FILENAME").getValue().trim().equals("") || siteConfig.get("AUTO_IMPORT_USERNAME").getValue().trim().equals("")
				|| siteConfig.get("AUTO_IMPORT_PASSWORD").getValue().trim().equals("")) {
			// return if this feature is disabled
			return;
		}

		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File excel_file = new File(tempFolder, "products_import.xls");

		try {
			FTPClient ftp = new FTPClient();
			ftp.connect(siteConfig.get("AUTO_IMPORT_SERVER").getValue());

			// login
			ftp.login(siteConfig.get("AUTO_IMPORT_USERNAME").getValue(), siteConfig.get("AUTO_IMPORT_PASSWORD").getValue());
			ftp.enterLocalPassiveMode();
			ftp.setFileType(FTP.BINARY_FILE_TYPE); // for excel files

			if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
				if (ftp.retrieveFile(siteConfig.get("AUTO_IMPORT_FILENAME").getValue(), new FileOutputStream(excel_file))) {
					try {
						List<Product> importedProducts = new ArrayList<Product>();
						Map<String, Object> map = new HashMap<String, Object>();

						if (processFile(excel_file, siteConfig, gSiteConfig, map, importedProducts, null, new ArrayList<Map<String, Object>>(), new ArrayList<Map<String, Object>>(), null)) {
							this.webJaguar.importProducts(importedProducts);
							if (siteConfig.get("AUTO_IMPORT_DELETE").getValue().equals("true")) {
								ftp.deleteFile(siteConfig.get("AUTO_IMPORT_FILENAME").getValue());
							}
						} else {
							String message = "";
							if (map.get("message") != null) {
								message = map.get("message").toString();
							}
							if (map.get("invalidItems") != null || message.equals("")) {
								message = "automated.import.failed";
							}
							notifyAdmin(siteConfig, getApplicationContext().getMessage(message, new Object[0], null), false);
						}
					} catch (IOException e) {
						notifyAdmin(siteConfig, getApplicationContext().getMessage("excelfile.invalid", new Object[0], null), false);
					} catch (Exception e) {
						notifyAdmin(siteConfig, e.toString(), true);
					}
				}
			} else {
				notifyAdmin(siteConfig, ftp.getReplyString(), false);
			}

			// Logout from the FTP Server and disconnect
			ftp.logout();
			ftp.disconnect();
		} catch (UnknownHostException e) {
			notifyAdmin(siteConfig, getApplicationContext().getMessage("dataFeed.ftp.unknownHost", new Object[0], null), false);
		} catch (Exception e) {
			// do nothing
		}
	}

	private void notifyAdmin(Map<String, Configuration> siteConfig, String message, boolean toEduardo) {
		// send email notification
		String contactEmail = siteConfig.get("AUTO_IMPORT_EMAIL").getValue();
		if (toEduardo) {
			contactEmail = "developers@advancedemedia.com";
		}
		if (contactEmail.trim().equals("")) {
			return;
		}
		String siteURL = siteConfig.get("SITE_URL").getValue();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Automated upload on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}

	private void emailResults(Map<String, Configuration> siteConfig, String message, Date start, Date end, int totalAdded, int totalUpdated, int totalErrors) {
		try {
			// send email notification
			String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
			String siteURL = siteConfig.get("SITE_URL").getValue();

			SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

			SimpleMailMessage msg = new SimpleMailMessage();
			// to emails
			String[] emails = siteConfig.get("PRODUCT_IMPORT_EMAIL").getValue().replace(" ", "").split("[,]"); // split by commas
			msg.setTo(emails);
			msg.setFrom(contactEmail);
			msg.setSubject("Product excel import on " + siteURL);

			StringBuffer sbuff = new StringBuffer();

			sbuff.append("Started : " + dateFormatter.format(start) + "\n");
			sbuff.append("Ended : " + dateFormatter.format(end) + "\n");
			sbuff.append("Total time : " + (end.getTime() - start.getTime()) + " milliseconds\n");
			sbuff.append("Total added : " + totalAdded + "\n");
			sbuff.append("Total updated : " + totalUpdated + "\n");
			sbuff.append("Total errors : " + totalErrors + "\n");

			msg.setText(sbuff + "\n\n" + message);
			mailSender.send(msg);
		} catch (Exception e) {
			// do nothing
		}

	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {

		ProductSearch form = new ProductSearch();

		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		if (siteConfig.get("PRODUCT_IMPORT_FILENAME").getValue().length() > 0) {
			File baseDir = new File(getServletContext().getRealPath("/"));
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
				if (prop.get("site.root") != null) {
					baseDir = new File((String) prop.get("site.root"));
				}
			} catch (Exception e) {
			}

			// check import file if present
			File importFile = new File(baseDir, siteConfig.get("PRODUCT_IMPORT_FILENAME").getValue());
			if (importFile.exists()) {
				HashMap<String, Object> fileMap = new HashMap<String, Object>();
				fileMap.put("file", importFile);
				fileMap.put("lastModified", new Date(importFile.lastModified()));
				fileMap.put("size", importFile.length());
				form.setImportFileMap(fileMap);
			}
		}

		return form;
	}
}