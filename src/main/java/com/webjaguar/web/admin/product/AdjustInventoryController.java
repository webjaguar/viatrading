/* Copyright 2010 Advanced E-Media Solutions
 * @author Jwalant
 * @since 09.09.2010
 */

package com.webjaguar.web.admin.product;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.KitParts;

public class AdjustInventoryController implements Controller{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
		
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
		String sku = ServletRequestUtils.getStringParameter( request, "sku" );
		String adjustQtyString = ServletRequestUtils.getStringParameter( request, "adjustQty" );
		Integer adjustQty = 0;
		if ( !("".equals( adjustQtyString.trim() )) )
		{
			try {
				adjustQty = Integer.valueOf( adjustQtyString.trim()  );
			}catch ( Exception e ) {System.out.println("Exception "+adjustQtyString);}
		}
		Integer currentQty;
		String note = ServletRequestUtils.getStringParameter( request, "adjustNote" );
		String adjustAvForSale = ServletRequestUtils.getStringParameter( request, "adjustAvForSale" );
		if( sku != null) {
			Inventory inventory = new Inventory();
			inventory.setSku(sku);
			inventory = this.webJaguar.getInventory(inventory);
			InventoryActivity inventoryActivity = new InventoryActivity();
			inventoryActivity.setSku(sku);
			if(inventory.getInventory() != null) {
				inventoryActivity.setType("adjustment");
				currentQty = ServletRequestUtils.getIntParameter( request, "inventory" );
			}
			else {
				inventoryActivity.setType("initialization");
				currentQty = 0;
			}
			if(adjustAvForSale != null && !adjustAvForSale.isEmpty() && adjustAvForSale.equalsIgnoreCase("true")) {
				inventoryActivity.setAdjustAvForSaleOnly(true);
				inventoryActivity.setType("AFS adjustment");
			}else {
				inventoryActivity.setAdjustAvForSaleOnly(false);				
			}
			inventoryActivity.setQuantity(adjustQty);
			if (inventoryActivity.isAdjustAvForSaleOnly()) {
				inventoryActivity.setInventory(currentQty);

				
			} else {
				inventoryActivity.setInventory(currentQty + (adjustQty));
			}			

			
			if(request.getAttribute( "accessUser" ) != null) {
				inventoryActivity.setAccessUserId(((AccessUser) request.getAttribute( "accessUser" )).getId());
			}
			inventoryActivity.setNote(note);
			// adjust inventory and inventory on hand
			Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
			if ((Boolean) gSiteConfig.get( "gKIT" )) {
				List<KitParts> kitPart = this.webJaguar.getKitPartsByKitSku(sku);
				if(kitPart != null) {
					for (KitParts kit : kitPart) {
						Inventory kiInventory = new Inventory();
						kiInventory.setSku(kit.getKitPartsSku());
						kiInventory = this.webJaguar.getInventory(kiInventory);
						if ( kiInventory.getInventory()!=null) {
							InventoryActivity kitActivity = new InventoryActivity();
							kiInventory.setSku(kit.getKitPartsSku());
							kiInventory.setInventoryAFS(-kit.getQuantity()*adjustQty);
							kiInventory.setInventory(-kit.getQuantity()*adjustQty);
							kitActivity.setSku(kit.getKitPartsSku());
							kitActivity.setType("adjustment");
							kitActivity.setNote("Kit " + kit.getKitSku());
							kitActivity.setQuantity(-kit.getQuantity()*adjustQty);
							this.webJaguar.updateInventory(kiInventory);
							kiInventory.setSku(kit.getKitPartsSku());
							kiInventory = this.webJaguar.getInventory(kiInventory);
							kitActivity.setInventory(kiInventory.getInventory() );
							if(request.getAttribute( "accessUser" ) != null) {
								kitActivity.setAccessUserId(((AccessUser) request.getAttribute( "accessUser" )).getId());
							}
							this.webJaguar.addKitPartsHistory(kitActivity);						
						}					
					}
				}
			}
			this.webJaguar.adjustInventoryBySku(inventoryActivity);
		}
		return null;
	}
}