/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.26.2009
 */

package com.webjaguar.web.admin.product;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import au.com.bytecode.opencsv.CSVReader;

import com.webjaguar.logic.WebJaguarFacade;

public class ImportCsvController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
    
	String formView = "admin/catalog/product/importCsv";
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {	  	
    	
		// check if upload button was pressed
		if (request.getParameter("__upload") != null) {
	    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	    	MultipartFile file = multipartRequest.getFile("file");
	    	if (file != null && !file.isEmpty()) {
	    		Map<String, Object> map = new HashMap<String, Object>();
	    		if (processFile(file.getInputStream(), map)) {
	    			return new ModelAndView("admin/catalog/product/importCsvSuccess");
	    		}
	    	} else {
	        	return new ModelAndView(formView, "message", "csvfile.invalid");	    		
	    	}
		}
    	
    	return new ModelAndView(formView);
    } 	

    private boolean processFile(InputStream inStream, Map<String, Object> map) throws Exception {
    	
    	CSVReader reader = new CSVReader(new InputStreamReader(inStream));
    	
    	// get headers
		String[] headers = reader.readNext();
		for (int i=0; i<headers.length; i++) {
			String headerName = headers[i].trim();
    	}
    	
		return true;
    }
}
	

