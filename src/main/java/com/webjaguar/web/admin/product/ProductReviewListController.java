/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ProductReview;
import com.webjaguar.model.ReviewSearch;


public class ProductReviewListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ReviewSearch productReviewSearch = getProductSearch( request ); 
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		String productSku = ServletRequestUtils.getStringParameter( request, "sku", null );
		if (productSku == null) {
			productReviewSearch.setProductSku( "" );
		}
		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i=0; i<ids.length; i++) {
					this.webJaguar.deleteProductReviewById(Integer.parseInt(ids[i]));
				}
			}
		}
		
		int count = this.webJaguar.getProductReviewListCount(productReviewSearch);
		if (count < productReviewSearch.getOffset()-1) {
			productReviewSearch.setPage(1);
		}
		
		productReviewSearch.setLimit(productReviewSearch.getPageSize());
		productReviewSearch.setOffset((productReviewSearch.getPage()-1)*productReviewSearch.getPageSize());
		
		List<ProductReview> productReviewList = this.webJaguar.getProductReviewList(productReviewSearch);
		int pageCount = count/productReviewSearch.getPageSize();
		if (count%productReviewSearch.getPageSize() > 0) {
			pageCount++;
		}
		
		myModel.put("productReviewList", productReviewList);
		myModel.put("product", this.webJaguar.getProductById( this.webJaguar.getProductIdBySku(productSku), 1, false, null ));
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", productReviewSearch.getOffset()+productReviewList.size());
		myModel.put("count", count);
		
        return new ModelAndView("admin/catalog/product/reviewList", "model", myModel);
	}
	
	private ReviewSearch getProductSearch(HttpServletRequest request) {
		ReviewSearch productReviewSearch = (ReviewSearch) request.getSession().getAttribute( "productReviewSearch" );
		if (productReviewSearch == null) {
			productReviewSearch = new ReviewSearch();
			request.getSession().setAttribute( "productReviewSearch", productReviewSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			productReviewSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "id DESC" ));
		}
		
		// active
		if (request.getParameter("active") != null) {
			productReviewSearch.setActive(ServletRequestUtils.getStringParameter( request, "active", null ));
		}
		
		// product id
		if (request.getParameter("sku") != null) {
			productReviewSearch.setProductSku(ServletRequestUtils.getStringParameter( request, "sku", null ));
		}
				
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				productReviewSearch.setPage( 1 );
			} else {
				productReviewSearch.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				productReviewSearch.setPageSize( 10 );
			} else {
				productReviewSearch.setPageSize( size );				
			}
		}
		
		return productReviewSearch;
	}
}
