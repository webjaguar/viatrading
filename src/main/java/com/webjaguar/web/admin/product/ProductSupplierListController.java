/*
 * Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 07.13.2007 
 */

package com.webjaguar.web.admin.product;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CategorySearch;
import com.webjaguar.model.Product;

public class ProductSupplierListController extends org.springframework.web.servlet.mvc.AbstractCommandController
{

	public ProductSupplierListController()
	{
		setCommandClass( CategorySearch.class );
	}

	private WebJaguarFacade webJaguar;
	public void setWebJaguar( com.webjaguar.logic.WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }

	public ModelAndView handle( HttpServletRequest request, HttpServletResponse response, Object command, org.springframework.validation.BindException errors ) throws Exception
	{
		String sku = ServletRequestUtils.getStringParameter( request, "sku", "" );
		
		// if add pushed
		if ( request.getParameter( "__add" ) != null )
		{
			return new ModelAndView( new RedirectView( "productSupplierForm.jhtm" ), "sku", sku );
		}

		PagedListHolder supplierList = new PagedListHolder( this.webJaguar.getProductSupplierListBySku( sku ) ); 
		
		Integer size = ServletRequestUtils.getIntParameter( request, "size", 10 );
		supplierList.setPageSize(  size );

		String page = request.getParameter( "page" );
		if ( page != null )
		{
			supplierList.setPage( Integer.parseInt( page ) - 1 );
		}
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put( "suppliers", supplierList );
		myModel.put( "sku", sku );

		// check if valid sku
		Product product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(sku), 0, false, null);
		if (product == null) {
			return new ModelAndView(new RedirectView("productList.jhtm"));
		} 
		/*
		 * MTZ
		 * else { 
		 * myModel.put("productSupplierId", product.getDefaultSupplierId());
		 * (Need to fix: When product added form front-end we shouldn't allow ADMIN to add supplier to that Product.)
		 * }
		 */
		
		return new ModelAndView( "admin/catalog/product/productSupplierList", "model", myModel );
	}



}