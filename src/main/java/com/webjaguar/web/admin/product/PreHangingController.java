/* Copyright 2011 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 01.03.2011
 */

package com.webjaguar.web.admin.product;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class PreHangingController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {	
    	
    	Map<String, Object> map = new HashMap<String, Object>();

		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i=0; i<ids.length; i++) {
					this.webJaguar.deletePreHanging(Integer.parseInt(ids[i]));
				}
			}
		}
    	
    	// check if update button was pressed
    	if (request.getParameter("__update") != null) {
    		if (updatePreHanging(request)) {
        		map.put("message", "update.successful");    			
    		}
		}
    	
    	map.put("preHanging", this.webJaguar.getPreHanging(null, null, null, null));

    	return new ModelAndView("admin/catalog/product/preHanging", map);
	}
    
    private boolean updatePreHanging(HttpServletRequest request) {
    	List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
    	
		for (String id: ServletRequestUtils.getStringParameters(request, "_id")) {
			Map<String, Object> prehang = new HashMap<String, Object>();
			prehang.put("id", Integer.parseInt(id));
			Double price = null;
			try {
				price = ServletRequestUtils.getDoubleParameter(request, "__price_" + id);				
			} catch (Exception e) {}
			prehang.put("price", price);
			prehang.put("dimensions", ServletRequestUtils.getStringParameter(request, "__dimensions_" + id, "").trim());
			data.add(prehang);
		}
		
		this.webJaguar.updatePreHanging(data);
		
		data.clear();

		String doorConfig[] = ServletRequestUtils.getStringParameters(request, "__prehang_doorConfig");
		String species[] = ServletRequestUtils.getStringParameters(request, "__prehang_species");
		String widthHeight[] = ServletRequestUtils.getStringParameters(request, "__prehang_widthHeight");
		String price[] = ServletRequestUtils.getStringParameters(request, "__prehang_price");
		String dimensions[] = ServletRequestUtils.getStringParameters(request, "__prehang_dimensions");
		for (int i=0; i<doorConfig.length; i++) {
			if (doorConfig[i].trim().length() > 0 
					&& species[i].trim().length() > 0 && widthHeight[i].trim().length() > 0) {

				Map<String, Object> prehang = new HashMap<String, Object>();
				
				prehang.put("doorConfig", doorConfig[i]);
				prehang.put("species", species[i]);
				prehang.put("widthHeight", widthHeight[i]);
				prehang.put("dimensions", dimensions[i]);
				try {
					prehang.put("price", (Double.parseDouble(price[i].trim())));
				} catch (NumberFormatException e) {}
				
				data.add(prehang);
			}
		}
		this.webJaguar.insertPreHanging(data);
		
		return true;
    }
    
/*
CREATE TABLE pre_hanging (
  id INT unsigned NOT NULL auto_increment,
  door_config varchar(50),
  species varchar(50),
  width_height varchar(50),
  price double(10,2),
  dimensions varchar(50),
  PRIMARY KEY (id),
  UNIQUE KEY (door_config, species, width_height)  
) ENGINE = MyISAM;
*/
    
}