/*
 * Copyright 2005, 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 * 
 * @since 04.21.2009
 */

package com.webjaguar.web.admin.product;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Product;
import com.webjaguar.web.form.ReviewForm;


public class ProductReviewFormController extends SimpleFormController
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ProductReviewFormController()
	{
		setSessionForm( true );
		setCommandName( "productReviewForm" );
		setCommandClass( ReviewForm.class );
		setFormView( "admin/catalog/product/productReviewform" );
		setSuccessView( "productReviewList.jhtm" );
		this.setValidateOnBinding( true );
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception
	{
		ReviewForm reviewForm = (ReviewForm) command;

		Map<String, Object> map = new HashMap<String, Object>();
        
		// check if cancel button was pressed
		if ( request.getParameter( "_cancel" ) != null ) {
			return new ModelAndView( new RedirectView( getSuccessView() ), "id", reviewForm.getProductReview().getProductId() );
		}
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null)
		{
			this.webJaguar.deleteProductReviewById( reviewForm.getProductReview().getId() );
			return new ModelAndView( new RedirectView( getSuccessView() ), "id", reviewForm.getProductReview().getProductId() );
		}

		this.webJaguar.updateProductReview( reviewForm.getProductReview() ); 
		return new ModelAndView( new RedirectView( getSuccessView() ), "id", reviewForm.getProductReview().getProductId() );
	}
	
	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
 	
		return map;
    }

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productReview.rate", "form.required", "required");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productReview.tile", "form.required", "required");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productReview.text", "form.required", "required");
	}
	
	protected boolean suppressBinding(HttpServletRequest request)
	{
		if (request.getParameter( "_cancel" ) != null || request.getParameter("_delete") != null) {
			return true;
		}
		return false;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception
	{
		ReviewForm reviewForm = new ReviewForm();
		
		int productReviewId = ServletRequestUtils.getIntParameter(request, "id", -1);
		String productSku = ServletRequestUtils.getStringParameter(request, "sku", null);
		Integer productId = this.webJaguar.getProductIdBySku(productSku);
		Product product = this.webJaguar.getProductById( productId, 0, false, null );

		if ( product != null ) {
			reviewForm.getProductReview().setId( productReviewId );
			reviewForm.getProductReview().setProductId( productId );
			reviewForm.setProductReview( this.webJaguar.getProductReviewById( productReviewId ) );
		} else {
			// product not valid
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("productList.jhtm")));			
		}
		
		return reviewForm;
	}
}
