/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Language;
import com.webjaguar.model.ProductField;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.ProductFieldsForm;

public class ProductFieldsController extends SimpleFormController {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private List<Language> gI18N;

	public ProductFieldsController() {
		setSessionForm(true);
		setCommandName("productFieldsForm");
		setCommandClass(ProductFieldsForm.class);
		setFormView("admin/catalog/product/fieldsForm");
		setSuccessView("admin/catalog/product/fieldsForm");
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws ServletException {

		ProductFieldsForm form = (ProductFieldsForm) command;

		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		for (ProductField productField : form.getProductFields()) {
			productField.setFieldType(ServletRequestUtils.getStringParameter(request, "__fieldType_" + productField.getId(), ""));
			productField.setRank(ServletRequestUtils.getIntParameter(request, "__rank_" + productField.getId(), 0));
			productField.setName(ServletRequestUtils.getStringParameter(request, "__name_" + productField.getId(), ""));
			productField.setGroupName(ServletRequestUtils.getStringParameter(request, "__group_name_" + productField.getId(), ""));
			productField.setFormatType(ServletRequestUtils.getStringParameter(request, "__formatType_" + productField.getId(), ""));
			productField.setEnabled(ServletRequestUtils.getBooleanParameter(request, "__enabled_" + productField.getId(), false));
			productField.setQuickModeField(ServletRequestUtils.getBooleanParameter(request, "__quickModeField_" + productField.getId(), false));
			productField.setQuickMode2Field(ServletRequestUtils.getBooleanParameter(request, "__quickMode2Field_" + productField.getId(), false));
			productField.setShowOnInvoice(ServletRequestUtils.getBooleanParameter(request, "__showOnInvoice_" + productField.getId(), false));
			productField.setShowOnInvoiceBackend(ServletRequestUtils.getBooleanParameter(request, "__showOnInvoice_backend_" + productField.getId(), false));
			productField.setShowOnInvoiceExport(ServletRequestUtils.getBooleanParameter(request, "__showOnInvoice_export_" + productField.getId(), false));
			productField.setShowOnPresentation(ServletRequestUtils.getBooleanParameter(request, "__showOnPresentation_" + productField.getId(), false));
			productField.setShowOnProductFormFrontend(ServletRequestUtils.getBooleanParameter(request, "__showOnProductForm_frontend_" + productField.getId(), false));
			productField.setPackingField(ServletRequestUtils.getBooleanParameter(request, "__packingField_" + productField.getId(), false));
			productField.setSearch(ServletRequestUtils.getBooleanParameter(request, "__search_" + productField.getId(), false));
			productField.setSearch2(ServletRequestUtils.getBooleanParameter(request, "__search2_" + productField.getId(), false));
			productField.setProtectedLevel(ServletRequestUtils.getStringParameter(request, "__protected_" + productField.getId(), productField.getProtectedLevel()));
			productField.setComparisonField(ServletRequestUtils.getBooleanParameter(request, "__comparisonField_" + productField.getId(), false));
			productField.setServiceField(ServletRequestUtils.getBooleanParameter(request, "__serviceField_" + productField.getId(), false));
			productField.setDetailsField(ServletRequestUtils.getBooleanParameter(request, "__detailsField_" + productField.getId(), false));
			productField.setPreValue(ServletRequestUtils.getStringParameter(request, "__preValue_" + productField.getId()));
			productField.setProductExport(ServletRequestUtils.getBooleanParameter(request, "__productExport_" + productField.getId(), false));
			productField.setShowOnMyList(ServletRequestUtils.getBooleanParameter(request, "__showOnMyList_" + productField.getId(), false));
			productField.setShowOnDropDown(ServletRequestUtils.getBooleanParameter(request, "__showOnDropDown_" + productField.getId(), false));
			productField.setIndexForSearch(ServletRequestUtils.getBooleanParameter(request, "__indexForSearch_" + productField.getId(), false));
			productField.setIndexForFilter(ServletRequestUtils.getBooleanParameter(request, "__indexForFilter_" + productField.getId(), false));
			productField.setIndexForPredictiveSearch(ServletRequestUtils.getBooleanParameter(request, "__indexForPredictiveSearch_" + productField.getId(), false));
			productField.setRangeFilter(ServletRequestUtils.getBooleanParameter(request, "__rangeFilter_" + productField.getId(), false));
			productField.setCheckboxFilter(ServletRequestUtils.getBooleanParameter(request, "__checkboxFilter_" + productField.getId(), false));
			
			if (false && (Boolean) gSiteConfig.get("gCUSTOMER_SUPPLIER")) {
				Set<Integer> catIds = new TreeSet<Integer>();
				for (String id : ServletRequestUtils.getStringParameter(request, "__catIds_" + productField.getId()).split(",")) {
					try {
						catIds.add(new Integer(id.trim()));
					} catch (NumberFormatException e) {
					}
				}
				StringBuffer categoryIds = new StringBuffer();
				for (Integer catId : catIds)
					categoryIds.append(catId + ",");
				if (categoryIds.length() > 0)
					categoryIds.insert(0, ",");
				productField.setCategoryIds(categoryIds.toString());
			}
		}

		this.webJaguar.updateProductFields(form.getProductFields());

		// i18n
		if (!(gI18N.isEmpty())) {
			List<ProductField> productFields = new ArrayList<ProductField>();

			List<Language> i18n = gI18N;
			for (Language language : i18n) {
				for (ProductField productField : form.getProductFields()) {
					ProductField newProductField = new ProductField();
					newProductField.setLang(language.getLanguageCode());
					newProductField.setId(productField.getId());
					newProductField.setName(ServletRequestUtils.getStringParameter(request, "__i18n_name_" + productField.getId() + "_" + language.getLanguageCode(), ""));
					newProductField.setPreValue(ServletRequestUtils.getStringParameter(request, "__i18n_preValue_" + productField.getId() + "_" + language.getLanguageCode(), ""));

					productFields.add(newProductField);
				}
			}
			this.webJaguar.updateI18nProductFields(productFields);
		}

		Map<String, Object> model = new HashMap<String, Object>();
		model.put(getCommandName(), form);
		model.put("message", "update.successful");
		model.put("protectedLevels", Constants.protectedLevels());
		// labels
		model.put("labels", this.webJaguar.getLabels("protected"));
		// i18n
		if (!(gI18N.isEmpty())) {
			model.put("languageCodes", gI18N);
			model.put("i18nProductFields", this.webJaguar.getI18nProductFields((Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
		}

		return new ModelAndView(getSuccessView(), model);
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		ProductFieldsForm form = new ProductFieldsForm();
		form.setProductFields(this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
		return form;
	}

	protected Map referenceData(HttpServletRequest request) {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		gI18N = this.webJaguar.getLanguageSettings();

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("protectedLevels", Constants.protectedLevels());
		// labels
		map.put("labels", this.webJaguar.getLabels("protected"));

		// i18n
		if (!(gI18N.isEmpty())) {
			map.put("i18nProductFields", this.webJaguar.getI18nProductFields((Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
			if (((String) gSiteConfig.get("gI18N")).length() > 0) {
				map.put("languageCodes", gI18N);
			}
		}

		return map;
	}
}