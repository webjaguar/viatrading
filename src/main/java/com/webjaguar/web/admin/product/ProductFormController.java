/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.product;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.ProductForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.KitParts;
import com.webjaguar.model.Language;
import com.webjaguar.model.MailInRebateSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAffiliateCommission;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.Search;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.Utilities;
import com.webjaguar.web.form.ProductForm;

public class ProductFormController extends SimpleFormController
{

	private WebJaguarFacade webJaguar;
	private List<Language> gI18N ;

	public void setWebJaguar(WebJaguarFacade webJaguar)
	{
		this.webJaguar = webJaguar;
	}

	public ProductFormController()
	{
		setSessionForm( true );
		setCommandName( "productForm" );
		setCommandClass( ProductForm.class );
		setFormView( "admin/catalog/product/form" );
		setSuccessView( "productList.jhtm" );
	}

	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception
	{
		super.initBinder( request, binder );
		NumberFormat nf = NumberFormat.getInstance( RequestContextUtils.getLocale( request ) );
		nf.setMaximumFractionDigits( 3 );
		nf.setMinimumFractionDigits( 2 );
		binder.registerCustomEditor( Double.class, new CustomNumberEditor( Double.class, nf, true ) );
		// for qty breaks
		binder.registerCustomEditor( Integer.class, new CustomNumberEditor( Integer.class, true ) );
	}

	protected Map referenceData(HttpServletRequest request)
	{
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		Map<String, Object> map = new HashMap<String, Object>();
//		String protectedLevels[] = new String[64];
//		for ( int i = 0; i < protectedLevels.length; i++ ) {
//			StringBuffer protectedLevel = new StringBuffer( "1" );
//			for ( int y = 0; y < i; y++ )
//				protectedLevel.append( "0" );
//			protectedLevels[i] = protectedLevel.toString();
//		}
		map.put( "protectedLevels", Constants.protectedLevels() );

		File baseFile = new File( getServletContext().getRealPath( "/assets/Image/Product" ) );
		Properties prop = new Properties();
		try
		{
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null )
			{
				baseFile = new File( (String) prop.get( "site.root" ) + "/assets/Image/Product" );
			}
		}
		catch ( Exception e )
		{}
		File detailsbig = new File( baseFile, "detailsbig" );
		File thumb = new File( baseFile, "thumb" );
		File temp = new File( baseFile, "temp" ); // temporary folder
		if ( !detailsbig.canWrite() || !thumb.canWrite() || !temp.canWrite() )
		{
			map.put( "message", "images.notWritable" );
		}

		map.put( "randomNum", new Random().nextInt( 1000 ) );
		if ( (Boolean) gSiteConfig.get( "gSALES_PROMOTIONS" ) ) {
			map.put( "salesTagCodes", this.webJaguar.getSalesTagList() );
		}
		if((Integer) gSiteConfig.get("gMAIL_IN_REBATES") > 0) {
			map.put( "mailInRebates", this.webJaguar.getMailInRebateList(new MailInRebateSearch()) );
		}
		Search search = null;
		if ( (Boolean) gSiteConfig.get("gSUPPLIER"))
			map.put("suppliers",this.webJaguar.getSuppliers(search));
		if ( (Boolean) gSiteConfig.get("gMANUFACTURER"))
			map.put("manufacturers",this.webJaguar.getManufacturerList(null));
		
		// labels
		map.put("labels", this.webJaguar.getLabels("priceTable", "protected"));
		
		// i18n	
		if (!(gI18N.isEmpty())) {
			map.put("i18nProductFields", this.webJaguar.getI18nProductFields((Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
			if(((String) gSiteConfig.get("gI18N")).length()>0){
				map.put("languageCodes", gI18N);
			}
		}
		//Consignment
		if (request.getParameter("supId") != null && request.getParameter("prefix") != null){
			map.put("supplierPrefix", request.getParameter("prefix"));
		} 
		
		return map;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws ServletException
	{
		ProductForm productForm = (ProductForm) command;
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		int thumbnail = Integer.parseInt( siteConfig.get( "THUMBNAIL_RESIZE" ).getValue() );

		// check if delete button was pressed
		if ( request.getParameter( "_delete" ) != null ) {			
			if ((Boolean) gSiteConfig.get( "gKIT" ) && (Boolean) gSiteConfig.get( "gINVENTORY" )) {
				if ( productForm.getProduct().getKitParts() != null ) {
					Inventory inventory = new Inventory();
					for( KitParts parts : productForm.getProduct().getKitParts()) {
						inventory.setSku(parts.getKitPartsSku());
						inventory = this.webJaguar.getInventory(inventory);
						if ( inventory.getInventory() != null) {
							inventory.setInventory(productForm.getProduct().getInventoryAFS() * parts.getQuantity());
							inventory.setInventoryAFS(productForm.getProduct().getInventoryAFS() * parts.getQuantity());
							inventory.setSku(parts.getKitPartsSku());
							this.webJaguar.updateInventory(inventory);
							InventoryActivity inventoryActivity = new InventoryActivity();
							inventoryActivity.setSku(inventory.getSku());
							inventoryActivity.setType("kit");
							inventoryActivity.setNote("kit Disassembeled " + parts.getKitSku());
							inventoryActivity.setQuantity(productForm.getProduct().getInventoryAFS() * parts.getQuantity());
							inventory = this.webJaguar.getInventory(inventory);
							inventoryActivity.setInventory(inventory.getInventory());
							if(request.getAttribute( "accessUser" ) != null) {
							inventoryActivity.setAccessUserId(((AccessUser) request.getAttribute( "accessUser" )).getId());
						}
							this.webJaguar.addKitPartsHistory(inventoryActivity);
					}
				}
			}
		}
			// Check if sku exists in orders
			   String LineItemSku = this.webJaguar.getLineItemSku(productForm.getProduct().getSku());
               if(LineItemSku!=null){
			       Map<String, Object> model = new HashMap<String, Object>();
				   model.put("message", "product.delete.failed");
				   try {
					    return showForm(request, response, errors, model);
					} catch (Exception e) {
						e.printStackTrace();
					}
				
				} else {	
				     this.webJaguar.deleteProductById( productForm.getProduct().getId() );
				     return new ModelAndView( new RedirectView( getSuccessView() ), "category", productForm.getCategory() );
				}
		}
		// check if cancel button was pressed
		if ( request.getParameter( "_cancel" ) != null ) {
			return new ModelAndView( new RedirectView( getSuccessView() ), "category", productForm.getCategory() );
		}

		File baseFile = new File( getServletContext().getRealPath( "/assets/Image/Product" ) );
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				baseFile = new File( (String) prop.get( "site.root" ) + "/assets/Image/Product" );
			}
		} catch ( Exception e ) {}
		File detailsbigDir = new File( baseFile, "detailsbig" );
		File thumbDir = new File( baseFile, "thumb" );
		File tempDir = new File( baseFile, "temp" ); // temporary folder

		if ( detailsbigDir.canWrite() && thumbDir.canWrite() && tempDir.canWrite() ) {
		  
			try {	
				  MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;	
				  for ( ProductImage image : productForm.getProductImages() ) {
					MultipartFile image_file = multipartRequest.getFile( "image_file_" + image.getIndex() );
					if ( image_file != null && !image_file.isEmpty() ) {
						if ( image_file.getContentType().equalsIgnoreCase( "image/gif" ) | image_file.getContentType().equalsIgnoreCase( "image/pjpeg" )
								| image_file.getContentType().equalsIgnoreCase( "image/jpeg" ) )
						{
							String fileName = image_file.getOriginalFilename();
							File detailsbig = new File( detailsbigDir.getAbsolutePath() + "/" + fileName );
							File thumb = new File( thumbDir.getAbsolutePath() + "/" + fileName );
							File temp = new File( tempDir.getAbsolutePath() + "/" + fileName );
							try {
								// resize image in temporary folder
								image_file.transferTo( temp );
								boolean success = this.webJaguar.resizeImage( temp.getAbsolutePath(), temp.getAbsolutePath(), 800, 600, image_file.getContentType() );
								// if success, copy resized image to detailsbig folder, and then create thumbnail
								if ( success ) {
									BufferedImage img = ImageIO.read( temp );
									if ( img.getHeight() <= thumbnail && img.getWidth() <= thumbnail ) {
										copy( temp, thumb );
									} else {
										this.webJaguar.resizeImage( temp.getAbsolutePath(), thumb.getAbsolutePath(), thumbnail, thumbnail, image_file.getContentType() );
									}
									if ( (Boolean) gSiteConfig.get( "gWATERMARK" ) )
									{ // check if watermark is needed
										this.webJaguar.watermarkImage( temp.getAbsolutePath(), temp.getAbsolutePath(), siteConfig.get( "WATERMARK_TEXT" ).getValue(), Integer
												.parseInt( siteConfig.get( "WATERMARK_FONT_COLOR_R" ).getValue() ), Integer.parseInt( siteConfig.get( "WATERMARK_FONT_COLOR_G" )
												.getValue() ), Integer.parseInt( siteConfig.get( "WATERMARK_FONT_COLOR_B" ).getValue() ), Integer.parseInt( siteConfig.get(
												"WATERMARK_FONT_SIZE" ).getValue() ), image_file.getContentType() );
									}
									copy( temp, detailsbig );
									image.setImageUrl( fileName );
									productForm.getProduct().addImage( image );
								}
								// delete the images in temporary folder
								temp.delete();
							} catch ( IOException e ) {
								// do nothing
							}
						}
					}
				}
			} catch(Exception e){
				System.out.println("-Error while editing product at ProductFormController:225");
				//e.printStackTrace();
			}	
		}
		if ( productForm.getProduct().getImages() != null ) {
			Iterator prodImages = productForm.getProduct().getImages().iterator();
			while ( prodImages.hasNext() ) {
				ProductImage image = (ProductImage) prodImages.next();
				if ( request.getParameter( "__clear_image_" + image.getIndex() ) != null ) {
					prodImages.remove();
				}
			}
		}

		// get category ids
		Set<Object> categories = new HashSet<Object>();
		String[] categoryIds = productForm.getProduct().getCategoryIds().split( "," );
		for ( int x = 0; x < categoryIds.length; x++ ) {
			if ( !("".equals( categoryIds[x].trim() )) ) {
				categories.add( Integer.valueOf( categoryIds[x].trim() ) );
			}
		}
		productForm.getProduct().setCatIds( categories );

		// check if there is a sku
		productForm.getProduct().setSku( productForm.getProduct().getSku().trim() );
		if ( "".equals( productForm.getProduct().getSku() ) ) {
			productForm.getProduct().setSku( null );
		}
		
		if ( productForm.isNewProduct() ) {
			// consignment Product
			if (request.getParameter("prefix") != null) {
				productForm.getProduct().setSku(request.getParameter("prefix") + "-" +productForm.getProduct().getSku());
			}				
			
			// Viatrading requested Set default value of allow neg inv & show neg inv to true
			if(!productForm.getProduct().isNegInventory() || !productForm.getProduct().isShowNegInventory()){	
				 productForm.getProduct().setNegInventory(true);
				 productForm.getProduct().setShowNegInventory(true);
			  }
				
			this.webJaguar.insertProduct( productForm.getProduct(), request );
			if ((Boolean) gSiteConfig.get( "gKIT" ) && (Boolean) gSiteConfig.get( "gINVENTORY" )) {
				if ( productForm.getProduct().getKitParts() != null && productForm.getProduct().getInventory() != null) {
					Inventory inventory = new Inventory();
					for( KitParts parts : productForm.getProduct().getKitParts()) {
						inventory.setSku(parts.getKitPartsSku());
						inventory = this.webJaguar.getInventory(inventory);
						if ( (inventory.getInventory() !=null && productForm.getProduct().getInventory() != null && inventory.getInventory() > productForm.getProduct().getInventory() * parts.getQuantity() ) || (inventory.getInventory() !=null && inventory.isNegInventory()) ) {
							inventory.setInventory(productForm.getProduct().getInventory() * -parts.getQuantity());
							inventory.setInventoryAFS(productForm.getProduct().getInventory() * -parts.getQuantity());
							inventory.setSku(parts.getKitPartsSku());
							this.webJaguar.updateInventory(inventory);
							InventoryActivity inventoryActivity = new InventoryActivity();
							inventoryActivity.setSku(inventory.getSku());
							inventoryActivity.setType("kit");
							inventoryActivity.setNote("Kit " + productForm.getProduct().getSku());
							inventoryActivity.setQuantity(productForm.getProduct().getInventory() * -parts.getQuantity());
							inventory = this.webJaguar.getInventory(inventory);
							inventoryActivity.setInventory(inventory.getInventory());
							if(request.getAttribute( "accessUser" ) != null) {
								inventoryActivity.setAccessUserId(((AccessUser) request.getAttribute( "accessUser" )).getId());
							}
							this.webJaguar.addKitPartsHistory(inventoryActivity);
						}
					}
				}
			}
		} else {			
			if ((Boolean) gSiteConfig.get( "gKIT" ) && (Boolean) gSiteConfig.get( "gINVENTORY" )) {
				if ( productForm.getProduct().getKitParts() != null && productForm.getProduct().getInventory() != null) {
					Inventory inventory = new Inventory();
					for (KitParts kit : productForm.getProduct().getKitParts()) {
						inventory.setSku(kit.getKitPartsSku());
						inventory = this.webJaguar.getInventory(inventory);
						if (kit.isTemp()){
							inventory.setSku(kit.getKitPartsSku());
							inventory.setInventory(-productForm.getProduct().getInventory() * kit.getQuantity());
							inventory.setInventoryAFS(-productForm.getProduct().getInventory() * kit.getQuantity());
							this.webJaguar.updateInventory(inventory);
							InventoryActivity inventoryActivity = new InventoryActivity();
							inventoryActivity.setSku(inventory.getSku());
							inventoryActivity.setType("kit");
							inventoryActivity.setNote("Kit " + productForm.getProduct().getSku());
							inventoryActivity.setQuantity(-productForm.getProduct().getInventory() * kit.getQuantity());
							inventory = this.webJaguar.getInventory(inventory);
							inventoryActivity.setInventory(inventory.getInventory());
							if(request.getAttribute( "accessUser" ) != null) {
								inventoryActivity.setAccessUserId(((AccessUser) request.getAttribute( "accessUser" )).getId());
							}
							this.webJaguar.addKitPartsHistory(inventoryActivity);
						}
						else {
							for (KitParts oldKit : this.webJaguar.getKitPartsByKitSku(kit.getKitSku())) {
								if (kit.getKitPartsSku().equals(oldKit.getKitPartsSku())) {
									inventory.setSku(kit.getKitPartsSku());
									inventory.setInventory(-productForm.getProduct().getInventory() * (kit.getQuantity()-oldKit.getQuantity()));
									inventory.setInventoryAFS(-productForm.getProduct().getInventory() * (kit.getQuantity()-oldKit.getQuantity()));
									this.webJaguar.updateInventory(inventory);
									InventoryActivity inventoryActivity = new InventoryActivity();
									inventoryActivity.setSku(inventory.getSku());
									inventoryActivity.setType("kit");
									inventoryActivity.setNote("Kit " + productForm.getProduct().getSku());
									inventoryActivity.setQuantity(-productForm.getProduct().getInventory() * (kit.getQuantity()-oldKit.getQuantity()));
									inventory = this.webJaguar.getInventory(inventory);
									inventoryActivity.setInventory(inventory.getInventory() );
									if(request.getAttribute( "accessUser" ) != null) {
										inventoryActivity.setAccessUserId(((AccessUser) request.getAttribute( "accessUser" )).getId());
									}
									this.webJaguar.addKitPartsHistory(inventoryActivity);
								}
							}
						}
					}
				}
			}
			
			this.webJaguar.updateProduct(productForm.getProduct(), request);
		}
		return new ModelAndView( new RedirectView( getSuccessView() ), "category", productForm.getCategory() );
	}

	protected boolean suppressBinding(HttpServletRequest request)
	{
		if ( request.getParameter( "_delete" ) != null || request.getParameter( "_cancel" ) != null )
		{
			return true;
		}
		return false;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		ProductForm productForm = (ProductForm) command;
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		
		// check if there is a sku
		if(productForm.getProduct() != null && productForm.getProduct().getSku() != null && !productForm.getProduct().getSku().equals("")){
			productForm.getProduct().setSku( productForm.getProduct().getSku().trim() );

		}
		
		boolean skuCheck;
		skuCheck = checkSKU(productForm);
			Integer existingId = this.webJaguar.getProductIdBySku(productForm.getProduct().getSku());
			if(existingId==null){
				if (Utilities.containsSpace(productForm.getProduct().getSku())) {
					errors.rejectValue("product.sku", "SKU_HAS_SPACE", "SKU cannot have space");
				}
			}			

		/*
		 * Need to check if product sku prefix is in supplier data base then shouldn't be able to change the 'SKU' 
		 * Consignment Product if (!skuCheck && productForm.getProduct().getConsignmentSupplierId() != null) { Supplier supplier = this.webJaguar.getSupplierById(productForm
		 * .getProduct().getConsignmentSupplierId()); boolean sku = productForm.getProduct ().getSku().startsWith(supplier.getSupplierPrefix() + "-"); if (supplier.getSupplierPrefix() != null && sku == false) { productForm.getProduct().setSku(supplier.getSupplierPrefix() + "-" +
		 * productForm.getProduct().getSku()); errors.rejectValue( "product.sku", "CONSIGNMENT_SKU", " This is a consignment product, it should start with a prefix." ); } }
		 */

		if (skuCheck) {
			errors.rejectValue("product.sku", "SKU_ALREADY_EXISTS", "An item using this SKU already exists.");
		}

		if (productForm.getProduct().getCaseContent() != null && productForm.getProduct().getCaseContent() <= 0) {
			errors.rejectValue("product.caseContent", "UNIT_POSITIVE", "select a positive number");
		}
		if (productForm.getProduct().getBoxExtraAmt() != null && productForm.getProduct().getBoxSize() == null) {
			errors.rejectValue("product.boxSize", "form.required", "required");
		}
		// UPS
		if (productForm.getProduct().getUpsMaxItemsInPackage() != null) {
			if (productForm.getProduct().getUpsMaxItemsInPackage() <= 0) {
				errors.rejectValue("product.upsMaxItemsInPackage", "UNIT_POSITIVE", "select a positive number");
			}

			if (productForm.getProduct().getWeight() != null && productForm.getProduct().getWeight() > 0) {
				if (productForm.getProduct().getWeight() * productForm.getProduct().getUpsMaxItemsInPackage() > 150) {
					errors.rejectValue("product.upsMaxItemsInPackage", "WEIGHT_LIMIT_EXCEED", "Total weight of a package exceeds 150lb");
				}
			}
		}
		//USPS
		if ( productForm.getProduct().getUspsMaxItemsInPackage() != null ){
			if( productForm.getProduct().getUspsMaxItemsInPackage() <= 0 ) {
				errors.rejectValue( "product.uspsMaxItemsInPackage", "UNIT_POSITIVE", "select a positive number" );
			}
			
			if(productForm.getProduct().getWeight() != null && productForm.getProduct().getWeight() > 0) {
				if(productForm.getProduct().getWeight() * productForm.getProduct().getUspsMaxItemsInPackage() > 70) {
					errors.rejectValue( "product.uspsMaxItemsInPackage", "WEIGHT_LIMIT_EXCEED", "Total weight of a package exceeds 70lb" );
				}
			}
		}
		if (productForm.getProduct().getImageFolder() != null) {
			String imageFolder = productForm.getProduct().getImageFolder().trim();
			if ( imageFolder.equals( "" ) || imageFolder.startsWith( "." ) )
				productForm.getProduct().setImageFolder( null );			
		}
		if ( (Boolean) gSiteConfig.get( "gINVENTORY" ) && productForm.getProduct().getInventory() != null && productForm.getProduct().getInventory() < 0 && !productForm.getProduct().isNegInventory() ) {
			errors.rejectValue( "product.negInventory", "NEG_INVENTORY_NOT_ALLOWED", "Please Check Allow Negative Inventory." );
		}
		if ( (Boolean) gSiteConfig.get( "gLOYALTY" ) && productForm.getProduct().getLoyaltyPoint() != null && productForm.getProduct().getLoyaltyPoint() < 0 ) {
			errors.rejectValue( "product.loyaltyPoint", "NEG_LOYALTY_NOT_ALLOWED", "select a positive number" );
		}
		
		// i18n
		Class paramTypeString[] = { String.class };
        Class<Product> c = Product.class;       
        if (!(gI18N.isEmpty())) {
			List<Language> i18n = gI18N;
			for(Language language: i18n) {
				Product i18nProduct = new Product();
				i18nProduct.setLang(language.getLanguageCode());
				i18nProduct.setI18nName(ServletRequestUtils.getStringParameter(request, "__i18nName_" + language.getLanguageCode(), ""));
				i18nProduct.setI18nShortDesc(ServletRequestUtils.getStringParameter(request, "__i18nShortDesc_" + language.getLanguageCode(), ""));
				String longDesc = ServletRequestUtils.getStringParameter(request, "__i18nLongDesc_" + language.getLanguageCode(), "");			
				if (longDesc.length() > 65535) {
					// TEXT is limited to 65,535 characters. MEDIUMTEXT is 16,777,215
					longDesc = longDesc.substring(0, 65535);
				}
				i18nProduct.setI18nLongDesc(longDesc);
				for (ProductField productField: productForm.getProductFields()) {
					Method m = c.getMethod("setField" + productField.getId(), paramTypeString);
					Object arglist[] = { ServletRequestUtils.getStringParameter(request, "__i18nProductField" + productField.getId() + "_" + language.getLanguageCode(), "") };
					m.invoke(i18nProduct, arglist);
				}
				productForm.getProduct().addI18nProduct(language.getLanguageCode(), i18nProduct);			
			}
		}
		
		// product variant
		if ((Boolean) gSiteConfig.get("gPRODUCT_VARIANTS")) {
			Set<String> skus = new HashSet<String>();
			
			List<Product> variants = new ArrayList<Product>();
			String variantSku[] = ServletRequestUtils.getStringParameters(request, "__variant_sku");
			String variantName[] = ServletRequestUtils.getStringParameters(request, "__variant_name");
			String variantShortDesc[] = ServletRequestUtils.getStringParameters(request, "__variant_shortDesc");
			String variantPrice[] = ServletRequestUtils.getStringParameters(request, "__variant_price");
			for (int i=0; i<variantSku.length; i++) {
				if (variantSku[i].trim().length() > 0 && skus.add(variantSku[i].trim().toLowerCase())) {
					// sku is required and unique
					Product variant = new Product(variantSku[i].trim(), this.webJaguar.getAccessUser(request).getUsername());
					
					if (variantName[i].trim().length() > 0) {
						variant.setName(variantName[i].trim());
					} else {
						variant.setName(null);
					}

					if (variantShortDesc[i].trim().length() > 0) {
						variant.setShortDesc(variantShortDesc[i].trim());
					} else {
						variant.setShortDesc(null);
					}
					
					try {
						variant.setPrice1(Double.parseDouble(variantPrice[i].trim()));
					} catch (NumberFormatException e) {
						variant.setPrice1(null);
					}
					
					variants.add(variant);
				}
			}
			productForm.getProduct().setVariants(variants);
		}
		
		// KIT
		if ((Boolean) gSiteConfig.get("gKIT")) {
			List<KitParts> parts = new ArrayList<KitParts>();
			String partSKU[] = ServletRequestUtils.getStringParameters(request, "__kitParts_sku");
			String partQuantity[] = ServletRequestUtils.getStringParameters(request, "__kitParts_quantity");
			Set<String> skus = new HashSet<String>();
			String invalidError = "";
			for(int i=0;i<partSKU.length;i++) {
				boolean addPartsDup = skus.add(partSKU[i].trim().toLowerCase());
				if (!addPartsDup) {
					invalidError = "duplicateSku";
				}
				if(partSKU[i].trim().length() > 0 && addPartsDup) {
					KitParts kitpart = new KitParts();	
					kitpart.setKitPartsSku(partSKU[i].trim().toLowerCase());
					kitpart.setKitSku(productForm.getProduct().getSku());
					if (this.webJaguar.getProductIdBySku(kitpart.getKitPartsSku()) == null){
						invalidError = "invalidSku";
					}
					kitpart.setTemp(true);
					try {
						kitpart.setQuantity(Integer.parseInt(partQuantity[i].trim()));
						if (kitpart.getQuantity() == null) {
							invalidError = "invalid";
						}
					} catch (NumberFormatException e ) {
						invalidError = "invalid";
					}
					parts.add(kitpart);
				}				
			}
			if ( productForm.getProduct().getKitParts() != null && invalidError != "invalid" && invalidError != "duplicateSku" && invalidError != "invalidSku") {
				for (KitParts kit : productForm.getProduct().getKitParts()){
					for (KitParts part : parts) {
						Inventory inventory = new Inventory();
						inventory.setSku(part.getKitPartsSku());
						inventory = this.webJaguar.getInventory(inventory);
						if ( kit.getKitPartsSku().equals(part.getKitPartsSku())) {
							part.setTemp(false);
							if ( productForm.getProduct().getInventory() != null && inventory.getInventory() != null && inventory.getInventoryAFS() < productForm.getProduct().getInventory() * (part.getQuantity()-kit.getQuantity()) && !inventory.isNegInventory() ) {
								invalidError = "low";
							}
						}
					}
				}
				
			}
			if (!parts.isEmpty() &&invalidError != "invalid" && invalidError != "duplicateSku" && invalidError != "invalidSku") {
				for(KitParts part : parts) {
					if ( part.isTemp() ) {
						Inventory inventory = new Inventory();
						inventory.setSku(part.getKitPartsSku());
						inventory = this.webJaguar.getInventory(inventory);
						if ( productForm.getProduct().getInventory() != null && inventory.getInventory() != null && inventory.getInventoryAFS() < productForm.getProduct().getInventory() * part.getQuantity() && !inventory.isNegInventory() ) {
							invalidError = "low";	
						}
					}
				}
			}
			if (invalidError=="invalidSku") {
				errors.rejectValue( "product.kitParts", "", "Invalid SKU, Product Dose Not Exists" );
			}
			productForm.getProduct().setKitParts(parts);
			if (invalidError=="low") {
				errors.rejectValue( "product.kitParts", "", "SKU Selected For Kit Dont Have Enough Inventory." );
			}
			if (invalidError=="invalid") {
				errors.rejectValue( "product.kitParts", "", "Invalid Quantity." );
			}
			if (invalidError=="duplicateSku") {
				errors.rejectValue( "product.kitParts", "", "Duplicate SKU Deleted" );
			}			
		}		
	}

	private boolean checkSKU(ProductForm productForm) {
		if ( !"".equals( productForm.getProduct().getSku() ) )
		{
			Integer id = this.webJaguar.getProductIdBySku( productForm.getProduct().getSku() );
			if ( id != null )
			{ // sku exists
				if ( productForm.isNewProduct() )
				{
					return true;
				}
				else if ( id.compareTo( productForm.getProduct().getId() ) != 0 )
				{ // not the same item
					return true;
				}
			}
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		ProductForm productForm = new ProductForm();
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();
		Integer productId = null;
		gI18N = this.webJaguar.getLanguageSettings();
		// check if load sku is pushed.
		if ( request.getParameter( "__loadSku" ) != null && request.getParameter( "sku" ) != null ) {
			productForm.getProduct().setSku( request.getParameter( "sku" ).trim() );
			if ( checkSKU(productForm)) {
				productId = this.webJaguar.getProductIdBySku( request.getParameter( "sku" ) );
			} else {
				productForm.setMessage( "sku does not exist." );
				productForm.getProduct().setCommissionTables(new ProductAffiliateCommission());
				return productForm;
			}
		}
		if ( request.getParameter( "id" ) != null || productId != null ) {
			boolean loadSku = true;
			if ( request.getParameter( "id" ) != null ) {
				productId = new Integer( request.getParameter( "id" ) );
				loadSku = false;
			} 

			Product product = this.webJaguar.getProductById( productId, (Integer) gSiteConfig.get("gPRODUCT_IMAGES"), false, null);
			productForm = new ProductForm( product );
			if ( loadSku ) {
				productForm.setNewProduct( true );
				//productForm.getProduct().setDefaultSupplierId( null ); /* MTZ
				productForm.getProduct().setInventory(null);
				productForm.getProduct().setAsiId(null);
				productForm.getProduct().setAsiXML(null);
				if(product.getFeed() != null && ! product.getFeed().isEmpty()) {
					productForm.getProduct().setFeed(product.getFeed()+"_duplicate");
				}
			}
			Set catIds = product.getCatIds();

			Iterator iter = catIds.iterator();
			StringBuffer categoryIds = new StringBuffer();
			if ( iter.hasNext() ) {
				categoryIds.append( iter.next() );
			}
			while ( iter.hasNext() ) {
				categoryIds.append( ", " );
				categoryIds.append( iter.next() );
			}
			
			/* Viatrading wants to display cat ids not in sequence
			if(productForm.getProduct().getCategoryIds() == null){
				productForm.getProduct().setCategoryIds( categoryIds.toString() );
			}*/

			productForm.getProduct().setCategoryIds( categoryIds.toString() );

			//KIT
			if ((Boolean) gSiteConfig.get("gKIT")) {
				List<KitParts> kitPartsList = this.webJaguar.getKitPartsByKitSku(product.getSku() );
				if( kitPartsList != null) {
					product.setKitParts(kitPartsList);
				}
			}
			
			
			// i18n
			 if (!(gI18N.isEmpty()))  {
				productForm.getProduct().setI18nProduct(this.webJaguar.getI18nProduct(productId));				
			}
			
			// product variant
			if ((Boolean) gSiteConfig.get("gPRODUCT_VARIANTS")) {
				productForm.getProduct().setVariants(this.webJaguar.getProductVariant(productId, null, true));
			}
		}
		else {
			productForm.getProduct().setSearchable( siteConfig.get("PRODUCT_SEARCHABLE_DEFAULT").getValue().equals("true"));
			productForm.getProduct().setEnableRate( siteConfig.get("PRODUCT_REVIEW_DEFAULT").getValue().equals("true"));
		}

		try {
			productForm.setCategory( new Integer( request.getParameter( "category" ) ) );
			if ( productForm.isNewProduct() ) {
				productForm.getProduct().setCategoryIds( productForm.getCategory().toString() );
			}
		}
		catch ( NumberFormatException e ) {}

		// product fields
		List<ProductField> productFields = this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
		Iterator pfIter = productFields.iterator();
		while ( pfIter.hasNext() ) {
			ProductField productField = (ProductField) pfIter.next();
			if ( !productField.isEnabled() ) {
				pfIter.remove();
			}
		}
		productForm.setProductFields( productFields );

		// product images
		List<ProductImage> productImages = new ArrayList<ProductImage>();
		byte index = 1;
		if ( productForm.getProduct().getImages() != null ) {
			for ( ProductImage image : productForm.getProduct().getImages() ) {
				image.setIndex( index++ );
				productImages.add( image );
			}
		}
		int numImages = (Integer) gSiteConfig.get( "gPRODUCT_IMAGES" );
		for ( ; index <= numImages; index++ ) {
			ProductImage image = new ProductImage();
			image.setIndex( index );
			productImages.add( image );
		}
		productForm.setProductImages( productImages );

		if ( (Integer) gSiteConfig.get("gAFFILIATE") >= 1 ) {
			if (productForm.getProduct().getCommissionTables() == null ) {
				productForm.getProduct().setCommissionTables( new ProductAffiliateCommission() ) ;
			}
		}

		// price tiers
		productForm.setPriceTiers( (Integer) gSiteConfig.get( "gPRICE_TIERS" ) );
		
		//set created and modified date PST
		//saving EST to DB, EST-3 = PST
		if(productForm.getProduct() != null && siteConfig.get("SITE_TIME_ZONE").getValue().equals("PST")) {
			Product p = productForm.getProduct();
			if(p.getCreated() != null)
				p.setCreated(new java.sql.Timestamp(p.getCreated().getTime() - (1000 * 60 * 60 * 3)));
			if(p.getLastModified() != null)
				p.setLastModified(new java.sql.Timestamp(p.getLastModified().getTime() - (1000 * 60 * 60 * 3)));
		}
		
		return productForm;

	}

	// Copies src file to dst file.
	// If the dst file does not exist, it is created
	private void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream( src );
		OutputStream out = new FileOutputStream( dst );

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ( (len = in.read( buf )) > 0 ) {
			out.write( buf, 0, len );
		}
		in.close();
		out.close();
	}

}