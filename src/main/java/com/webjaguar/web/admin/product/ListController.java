/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.product;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.KitParts;
import com.webjaguar.model.Presentation;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductLabelTemplateSearch;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.Search;
import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.itext.ITextApi;
import com.webjaguar.web.domain.Utilities;
import com.webjaguar.web.form.ProductSupplierForm;

public class ListController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ProductSearch productSearch = getProductSearch(request);
		Map<String, Object> myModel = new HashMap<String, Object>();
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			if (productSearch.getCategory() != null) {
				return new ModelAndView(new RedirectView("product.jhtm"), "category", productSearch.getCategory());
			} else {
				return new ModelAndView(new RedirectView("product.jhtm"));
			}
		}

		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					Product product = this.webJaguar.getProductById(Integer.parseInt(ids[i]), (Integer) gSiteConfig.get("gPRODUCT_IMAGES"), false, null);
					if (product != null) {
						List<KitParts> kitPartsList = this.webJaguar.getKitPartsByKitSku(product.getSku());
						if (kitPartsList.isEmpty()) {
							this.webJaguar.deleteProductById(Integer.parseInt(ids[i]));
						}
					}
				}
			}
		}

		// check if update ranking button was pressed
		if (request.getParameter("__update_ranking") != null) {
			updateRanking(request, productSearch);
		}

		// check if options button was clicked
		if (request.getParameter("__salesTagBatcher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					Integer salesTagId = ServletRequestUtils.getIntParameter(request, "__salesTag_id");
					this.webJaguar.updateSalesTagToProductId(Integer.parseInt(ids[i]), (salesTagId == -999) ? null : salesTagId);
				}
			}
		}

		// check if options button was clicked
		if (request.getParameter("__manufacturerBacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					Integer manufactuereId = ServletRequestUtils.getIntParameter(request, "__manufacturer_id");
					this.webJaguar.updateManufacturerToProductId(Integer.parseInt(ids[i]), (manufactuereId == -999) ? null : manufactuereId);
				}
			}
		}

		// check if options button was clicked
		if (request.getParameter("__activeBacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					boolean activeMode = ServletRequestUtils.getBooleanParameter(request, "__active_mode");
					if(this.webJaguar.getAccessUser(request)!=null && this.webJaguar.getAccessUser(request).getUsername()!=null){
						String last_modified_by = this.webJaguar.getAccessUser(request).getUsername();
						this.webJaguar.updateProduct("last_modified_by = " + "'" + last_modified_by + "'" + " , active = " + activeMode, " id = " + Integer.parseInt(ids[i]));
					}else{
						this.webJaguar.updateProduct(" active = " + activeMode, " id = " + Integer.parseInt(ids[i]));	
					}
				}
			}
		}
		
		// check if options button was clicked
		if (request.getParameter("__updateParentSKUBacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					String parentSku = ServletRequestUtils.getStringParameter(request, "__parent_sku");
					this.webJaguar.updateParentProduct(parentSku,Integer.valueOf(ids[i]));
				}
			}
		}

		// check if weight update button was clicked
		if (request.getParameter("__updateWeightBacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					String weight = ServletRequestUtils.getStringParameter(request, "__product_weight");
					this.webJaguar.updateProduct("weight = " + "'" + weight + "'", " id = " + Integer.parseInt(ids[i]));
				}
			}
		}
		
		// check if program update button was clicked
		if (request.getParameter("__updateProgramBacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					String program = ServletRequestUtils.getStringParameter(request, "__product_program");
					this.webJaguar.updateProduct("program = " + "'" + program + "'", " id = " + Integer.parseInt(ids[i]));
				}
			}
		}
				
		// check if update ranking button was clicked
		if (request.getParameter("__updateRankingBacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {

				List<Integer> productIds = new ArrayList<Integer>();
				if (request.getParameter("checkAll") != null) {
					for (Integer productId : this.webJaguar.getProductIdListBySearch(productSearch)) {
						productIds.add(productId);
					}
				} else if (ids != null && request.getParameter("checkAll") == null) {
					for (int i = 0; i < ids.length; i++) {
						String productId = ids[i];
						if (productId != null) {
							productIds.add(Integer.parseInt(productId));
						}
					}
				}
				StringBuffer idString = new StringBuffer();
				for (int i = 0; i < productIds.size(); i++) {
					idString.append(productIds.get(i));
					if (i + 1 < productIds.size()) {
						idString.append(",");
					}
				}
				int rank = ServletRequestUtils.getIntParameter(request, "__rank", 10);
				this.webJaguar.updateProduct(" search_rank = " + rank, " id IN (" + idString.toString() + ")");
			}
		}

		String message = "";
		// check if options button was clicked
		if (request.getParameter("__duplicateSkuBacher") != null && !request.getParameter("__duplicate_sku_prefix").isEmpty()) {
			boolean skuExist = false;
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					String skuPrefix = ServletRequestUtils.getStringParameter(request, "__duplicate_sku_prefix");
					Product product = this.webJaguar.getProductById(Integer.parseInt(ids[i]), request);
					if (product != null) {
						product.setInventory(null);
						product.setAsiId(null);
						product.setAsiXML(null);
						if (product.getFeed() != null && !product.getFeed().isEmpty()) {
							product.setFeed(product.getFeed() + "_duplicate");
						}
						product.setSku(skuPrefix + product.getSku());
					}
					if (this.webJaguar.getProductIdBySku(product.getSku()) == null) {
						this.webJaguar.insertProduct(product, request);
						// move to category
						if (request.getParameter("__category_association") != null) {
							Set<Object> categories = new HashSet<Object>();
							String categoryIdString = ServletRequestUtils.getStringParameter(request, "__category_association");
							String batchType = "move";
							String[] categoryIds = categoryIdString.split(",");
							for (int x = 0; x < categoryIds.length; x++) {
								if (!("".equals(categoryIds[x].trim()))) {
									try {
										categories.add(Integer.valueOf(categoryIds[x].trim()));
									} catch (Exception e) {
										// do nothing
									}

								}
							}
							this.webJaguar.batchCategoryIds(product.getId(), categories, batchType);
						}
					} else {
						skuExist = true;
					}
					if (skuExist) {
						message = "duplicatePrefixSku";
					}
				}
			}
		}
		
		// check if options button was clicked
		if (request.getParameter("__fobBacher") != null && !request.getParameter("__fobBacher").isEmpty()) {
			String ids[] = request.getParameterValues("__selected_id");
			String fob = ServletRequestUtils.getStringParameter(request, "__fob");
			List<Integer> productIds = new ArrayList<Integer>();
			if (request.getParameter("checkAll") != null) {
				for (Integer productId : this.webJaguar.getProductIdListBySearch(productSearch)) {
					productIds.add(productId);
				}
			}
			else if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					productIds.add(Integer.valueOf(ids[i]));
				}
			}
			if(productIds.size()>0){
				for(Integer i : productIds){
					this.webJaguar.updateProduct("fob_zip_code='"+fob + "'","id="+i);
				}
			}	
		}

		// check if options button was clicked
		if (request.getParameter("__categoryAssignBacher") != null) {
			// get category ids
			Set<Object> categories = new HashSet<Object>();
			String categoryIdString = ServletRequestUtils.getStringParameter(request, "__category_id");
			String batchType = ServletRequestUtils.getStringParameter(request, "__batch_type");
			String[] categoryIds = categoryIdString.split(",");
			for (int x = 0; x < categoryIds.length; x++) {
				if (!("".equals(categoryIds[x].trim()))) {
					try {
						categories.add(Integer.valueOf(categoryIds[x].trim()));
					} catch (Exception e) {
						// do nothing
					}

				}
			}
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i = 0; i < ids.length; i++) {
					this.webJaguar.batchCategoryIds(Integer.parseInt(ids[i]), categories, batchType);
				}
			}
		}
		// check if __presentationPdf button was clicked
		if (request.getParameter("__presentationPdf") != null) {
			String indexes[] = request.getParameterValues("__selected_id");
			List<Integer> productIds = new ArrayList<Integer>();
			Presentation presentation = new Presentation();
			SalesRep salesrep = new SalesRep();
			// to check whether the call is from the admin
			boolean admin = true;
			Integer noOfProduct = ServletRequestUtils.getIntParameter(request, "__productPerPage", 0);
			Map<Integer, Double> presentationProductPrice = new HashMap<Integer, Double>();
			if (request.getParameter("checkAll") != null) {
				for (Integer productId : this.webJaguar.getProductIdListBySearch(productSearch)) {
					productIds.add(productId);
				}
			} else if (indexes != null && request.getParameter("checkAll") == null) {
				for (int i = 0; i < indexes.length; i++) {
					String productId = indexes[i];
					if (productId != null) {
						productIds.add(Integer.parseInt(productId));
					}
				}
			}
			if (productIds.size() > 0) {
				if (this.webJaguar.createPdfPresentation(request, response, productIds, noOfProduct, presentation, salesrep, gSiteConfig, presentationProductPrice, admin)) {
					// pdf generated
					return null;
				}
			}
		}
		// check if __batchPdf button was clicked
		if (request.getParameter("__batchPdf") != null) {
			String indexes[] = request.getParameterValues("__selected_id");
			List<Integer> productIds = new ArrayList<Integer>();
			Integer quantity = ServletRequestUtils.getIntParameter(request, "__qty_sku", 0);
			Integer template = ServletRequestUtils.getIntParameter(request, "__label_layout_id", 0);

			if (request.getParameter("checkAll") != null) {
				for (Integer productId : this.webJaguar.getProductIdListBySearch(productSearch)) {
					productIds.add(productId);
				}
			} else if (indexes != null && request.getParameter("checkAll") == null) {
				for (int i = 0; i < indexes.length; i++) {
					String productId = indexes[i];
					if (productId != null) {
						productIds.add(Integer.parseInt(productId));
					}
				}
			}
			if (productIds.size() > 0) {
				if (batchPrint(request, response, productIds, quantity, template)) {
					// pdf generated
					return null;
				}
			}
		}
		// check if __fieldEditBatcher button was clicked
		if (request.getParameter("__fieldEditBatcher") != null) {
			String indexes[] = request.getParameterValues("__selected_id");
			Set<Integer> productIds = new HashSet<Integer>();
			Integer fieldId = ServletRequestUtils.getIntParameter(request, "__field_id", 0);
			String fieldValue = ServletRequestUtils.getStringParameter(request, "__field_value", null);

			if (request.getParameter("checkAll") != null) {
				for (Integer productId : this.webJaguar.getProductIdListBySearch(productSearch)) {
					productIds.add(productId);
				}
			} else if (indexes != null && request.getParameter("checkAll") == null) {
				for (int i = 0; i < indexes.length; i++) {
					String productId = indexes[i];
					if (productId != null) {
						productIds.add(Integer.parseInt(productId));
					}
				}
			}
			if (productIds.size() > 0) {
				this.webJaguar.batchFieldUpdate(productIds, fieldId, fieldValue);
			}
		}
		
		// check if __supplierEditBatcher button was clicked
		if (request.getParameter("__supplierEditBatcher") != null) {
			String indexes[] = request.getParameterValues("__selected_id");
			Set<Integer> productIds = new HashSet<Integer>();
			Integer supplierId = ServletRequestUtils.getIntParameter(request, "__supplier_id", 0);
			String supplierSKU = ServletRequestUtils.getStringParameter(request, "__supplier_sku", null);
			Double supplierCostValue = Utilities.roundFactory(ServletRequestUtils.getDoubleParameter(request, "__cost_value",0.0), 2, BigDecimal.ROUND_HALF_UP);
			Boolean isPercent = ServletRequestUtils.getBooleanParameter(request, "__cost_percent", false);
			if (request.getParameter("checkAll") != null) {
				for (Integer productId : this.webJaguar.getProductIdListBySearch(productSearch)) {
					productIds.add(productId);
				}
			} else if (indexes != null && request.getParameter("checkAll") == null) {
				for (int i = 0; i < indexes.length; i++) {
					String productId = indexes[i];
					if (productId != null) {
						productIds.add(Integer.parseInt(productId));
					}
				}
			}
			if (productIds.size() > 0) {
				for(Integer id : productIds){
					ProductSupplierForm productSupplierForm = new ProductSupplierForm();
					Product product = this.webJaguar.getProductById(id, 0, false, null);
					if ( product != null ) {
						Supplier supplier = new Supplier();
						supplier.setSku(product.getSku());
						supplier.setSupplierSku(supplierSKU);
						supplier.setPrice(supplierCostValue);
						supplier.setPercent(isPercent);
						supplier.setId(supplierId);
						this.webJaguar.insertProductSupplier(supplier);
						this.webJaguar.updateDefaultSupplierId(supplierId, product.getSku());
					}else{
						continue;
					}
					
				}
			}
		}

		if (productSearch.getCategory() != null) {
			myModel.put("categoryName", this.webJaguar.getCategoryName(productSearch.getCategory()));
		}

		int count = this.webJaguar.getProductListCount(productSearch);
		if (count < productSearch.getOffset() - 1) {
			productSearch.setPage(1);
		}

		productSearch.setLimit(productSearch.getPageSize());
		productSearch.setOffset((productSearch.getPage() - 1) * productSearch.getPageSize());

		List<Product> productList = this.webJaguar.getProductList(productSearch);

		myModel.put("search", productSearch);
		myModel.put("products", productList);
		if ((Boolean) gSiteConfig.get("gSALES_PROMOTIONS")) {
			myModel.put("salesTagCodes", this.webJaguar.getSalesTagList());
		}
		if ((Boolean) gSiteConfig.get("gMANUFACTURER")) {
			myModel.put("manufactuereCodes", this.webJaguar.getManufacturerList(null));
		}
		int pageCount = count / productSearch.getPageSize();
		if (count % productSearch.getPageSize() > 0) {
			pageCount++;
		}

		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", productSearch.getOffset() + productList.size());
		myModel.put("count", count);
		myModel.put("manufacturers", this.webJaguar.getManufacturerList(null));
		if ( (Boolean) gSiteConfig.get("gSUPPLIER")){
			Search searchSupplier = null;
			myModel.put("suppliersList", this.webJaguar.getSuppliers(searchSupplier));
		}
			
		myModel.put("productFieldList", this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
		if (siteConfig.get("BATCH_PRINT_PRODUCT_LABEL") != null && siteConfig.get("BATCH_PRINT_PRODUCT_LABEL").getValue().equals("true")) {
			ProductLabelTemplateSearch labelTemplateSearch = new ProductLabelTemplateSearch();
			labelTemplateSearch.setActive("1");
			myModel.put("labelTemplateList", this.webJaguar.getProductLabelTemplates(labelTemplateSearch));
		}
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/catalog/product/list", "model", myModel);
		}
		return new ModelAndView("admin/catalog/product/list", "model", myModel);
	}

	private ProductSearch getProductSearch(HttpServletRequest request) {
		ProductSearch productSearch = (ProductSearch) request.getSession().getAttribute("productSearch");
		if (productSearch == null) {
			productSearch = new ProductSearch();
			request.getSession().setAttribute("productSearch", productSearch);
		}

		// sorting
		if (request.getParameter("sort") != null) {
			productSearch.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}

		// Category Id
		if (request.getParameter("category") != null) {
			int categoryId = ServletRequestUtils.getIntParameter(request, "category", -1);
			if (categoryId > -1) {
				productSearch.setCategory(categoryId);
			} else {
				productSearch.setCategory(null);
			}
		}

		// Product Id
		if (request.getParameter("product_id") != null) {
			productSearch.setProductId(ServletRequestUtils.getStringParameter(request, "product_id", "").trim());
		}

		// Product Name
		if (request.getParameter("product_name") != null) {
			productSearch.setProductName(ServletRequestUtils.getStringParameter(request, "product_name", "").trim());
		}

		// Product Sku
		if (request.getParameter("product_sku") != null) {
			productSearch.setSku(ServletRequestUtils.getStringParameter(request, "product_sku", "").trim());
			productSearch.setSkuOperator(ServletRequestUtils.getIntParameter(request, "product_skuOperator", 0));
		}
		
		// Product Parent Sku
		if (request.getParameter("product_parent_sku") != null) {
			productSearch.setParentSku(ServletRequestUtils.getStringParameter(request, "product_parent_sku", "").trim());
			productSearch.setParentSkuOperator(ServletRequestUtils.getIntParameter(request, "product_parent_skuOperator", 0));
		}

		// Product type
		if (request.getParameter("_type") != null) {
			productSearch.setType(ServletRequestUtils.getStringParameter(request, "_type", ""));
		}

		// Product SalesTag title
		if (request.getParameter("salesTag_title") != null) {
			productSearch.setSalesTagTitle(ServletRequestUtils.getStringParameter(request, "salesTag_title", ""));
		}

		// Product Inventory
		if (request.getParameter("_inventory") != null) {
			productSearch.setInventory(ServletRequestUtils.getStringParameter(request, "_inventory", ""));
		}

		// Consignment Product
		if (request.getParameter("_consignmentProducts") != null) {
			if (request.getParameter("_consignmentProducts") != "") {
				productSearch.setConsignment(ServletRequestUtils.getStringParameter(request, "_consignmentProducts", ""));
			} else {
				productSearch.setConsignment(null);
			}
		}

		// Product active
		if (request.getParameter("_active") != null) {
			productSearch.setActive(ServletRequestUtils.getStringParameter(request, "_active", ""));
		}
		
		// has Parent SKU
		if (request.getParameter("_has_parent_sku") != null) {
			productSearch.setHasParentSku(ServletRequestUtils.getStringParameter(request, "_has_parent_sku", ""));
		}

		// Product Supplier Company
		if (request.getParameter("supplier_company") != null) {
			productSearch.setSupplierCompany(ServletRequestUtils.getStringParameter(request, "supplier_company", "").trim());
		}

		// Product Supplier Account Number
		if (request.getParameter("supplier_account_number") != null) {
			productSearch.setSupplierAccountNumber(ServletRequestUtils.getStringParameter(request, "supplier_account_number", "").trim());
		}

		// Product Manufacturer
		if (request.getParameter("manufacturer") != null) {
			productSearch.setManufacturer(ServletRequestUtils.getStringParameter(request, "manufacturer", ""));
		}
		// Product Field
		if (request.getParameter("productField") != null && request.getParameter("productFieldNumber") != null) {
			List<ProductField> list = new ArrayList<ProductField>();
			ProductField field = new ProductField();
			field.setId(Integer.parseInt(ServletRequestUtils.getStringParameter(request, "productFieldNumber", null)));
			field.setValue(ServletRequestUtils.getStringParameter(request, "productField", null));
			list.add(field);

			productSearch.setProductField(list);
		}

		// short description
		if (request.getParameter("short_desc") != null) {
			productSearch.setShortDesc(ServletRequestUtils.getStringParameter(request, "short_desc", "").trim());
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				productSearch.setPage(1);
			} else {
				productSearch.setPage(page);
			}
		}

		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				productSearch.setPageSize(10);
			} else {
				productSearch.setPageSize(size);
			}
		}
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				productSearch.setStartDate(df.parse(request.getParameter("startDate")));
			} catch (ParseException e) {
				productSearch.setStartDate(null);
			}
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				productSearch.setEndDate(df.parse(request.getParameter("endDate")));
			} catch (ParseException e) {
				productSearch.setEndDate(null);
			}
		}
		
		// set end date to end of day
		if (productSearch.getEndDate() != null) {
			productSearch.getEndDate().setTime(productSearch.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}
		// hasPrimarySupplier
		if(request.getParameter("_hasPrimarySupplier") != null){
			productSearch.setHasPrimarySupplier(ServletRequestUtils.getStringParameter(request, "_hasPrimarySupplier", ""));
		}

		return productSearch;
	}

	// Product rank for a specific category
	private void updateRanking(HttpServletRequest request, ProductSearch search) {
		Enumeration paramNames = request.getParameterNames();
		List<Map<String, Integer>> data = new ArrayList<Map<String, Integer>>();
		while (paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement().toString();
			if (paramName.startsWith("__rank_")) {
				HashMap<String, Integer> rankingMap = new HashMap<String, Integer>();
				try {
					Integer productId = new Integer(paramName.substring(7));
					if (request.getParameter(paramName).equals("")) {
						rankingMap.put("productId", productId);
						rankingMap.put("categoryId", search.getCategory());
						rankingMap.put("rank", null);
					} else {
						Integer rank = new Integer(request.getParameter(paramName));
						if (rank.intValue() >= 0) {
							rankingMap.put("productId", productId);
							rankingMap.put("categoryId", search.getCategory());
							rankingMap.put("rank", rank);
						}
					}
					data.add(rankingMap);
				} catch (NumberFormatException e) {
				}
			}
		}
		this.webJaguar.updateCategoryProductRanking(data);
	}

	private boolean batchPrint(HttpServletRequest request, HttpServletResponse response, List<Integer> productIds, Integer quantity, Integer templateId) {
		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File pdfFile = new File(tempFolder, "labels.pdf");

		boolean result = false;

		List<Product> products = new ArrayList<Product>();

		try {
			ITextApi iText = new ITextApi();
			for (Integer productId : productIds) {
				Product product = this.webJaguar.getProductById(productId, request);
				if (product != null) {
					products.add(product);
				}
			}

			File baseImageDir = new File(getServletContext().getRealPath("/assets/Image"));
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
				if (prop.get("site.root") != null) {
					baseImageDir = new File((String) prop.get("site.root"), "/assets/Image");
				}
			} catch (Exception e) {
			}

			iText.createPdfProductLabels(pdfFile, products, request, getApplicationContext(), quantity, this.webJaguar.getProductLabelTemplate(templateId));

			result = true;

			byte[] b = this.webJaguar.getBytesFromFile(pdfFile);

			response.addHeader("Content-Disposition", "attachment; filename=" + pdfFile.getName());
			response.setBufferSize((int) pdfFile.length());
			response.setContentType("application/pdf");
			response.setContentLength((int) pdfFile.length());

			// output stream
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(b, 0, (int) pdfFile.length());
			ouputStream.flush();
			ouputStream.close();

			// delete file before exit
			pdfFile.delete();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
