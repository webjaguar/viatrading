/*
 * Copyright 2005, 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 * 
 * @since 07.16.2007
 */

package com.webjaguar.web.admin.product;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.Search;
import com.webjaguar.model.Supplier;
import com.webjaguar.web.form.ProductSupplierForm;


public class ProductSupplierFormController extends SimpleFormController
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ProductSupplierFormController()
	{
		setSessionForm( true );
		setCommandName( "productSupplierForm" );
		setCommandClass( com.webjaguar.web.form.ProductSupplierForm.class );
		setFormView( "admin/catalog/product/productSupplierForm" );
		setSuccessView( "productSupplierList.jhtm" );
		this.setValidateOnBinding( true );
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception
	{
		ProductSupplierForm productSupplierForm = (ProductSupplierForm) command;

		Map<String, Object> map = new HashMap<String, Object>();
        
		// check if cancel button was pressed
		if ( request.getParameter( "_cancel" ) != null ) {
			return new ModelAndView( new RedirectView( getSuccessView() ), "sku", productSupplierForm.getSku() );
		}
		
		// check if delete button was pressed
		if (request.getParameter("delete") != null) {
			this.webJaguar.deleteProductSupplier( productSupplierForm.getSku(), productSupplierForm.getSupplierId() ); 
			return new ModelAndView( new RedirectView( getSuccessView() ), "sku", productSupplierForm.getSku() );
		}
		if ( productSupplierForm.isNewSupplier() ) {
			try {
				this.webJaguar.insertProductSupplier( productSupplierForm.getSupplier() );
			}
			catch ( Exception e ) {
				// no duplicate entry is allowed.
				map.put( "message", "form.hasDuplicate" );
				return showForm(request, response, errors, map);	
			}
		}
		else {
			this.webJaguar.updateProductSupplier( productSupplierForm.getSupplier()); 
		}
		
		// set as primary supplier
		if (productSupplierForm.isSetAsPrimary()) {
			productSupplierForm.getSupplier().setPrimary(true);
			this.webJaguar.updateDefaultSupplierId(productSupplierForm.getSupplier().getId(), productSupplierForm.getSku());  
		}
	
		return new ModelAndView( new RedirectView( getSuccessView() ), "sku", productSupplierForm.getSku() );
	}
	
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		if(!siteConfig.get("COST_TIERS").getValue().equals("true")) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "supplier.price", "form.required", "required");
		}
	}
	
	protected boolean suppressBinding(HttpServletRequest request)
	{
		if (request.getParameter( "_cancel" ) != null || request.getParameter("delete") != null) {
			return true;
		}
		return false;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception
	{
		ProductSupplierForm productSupplierForm = new ProductSupplierForm();
		
		String sku = ServletRequestUtils.getStringParameter(request, "sku", "");
		Product product = this.webJaguar.getProductById( this.webJaguar.getProductIdBySku( sku ), 0 , false, null);
		
		productSupplierForm.setSku( sku );
		productSupplierForm.getSupplier().setSku( sku );

		if ( product != null ) {
		/*	if (product.getDefaultSupplierId() != 0 && request.getParameter( "sid" ) == null) { // If product is added form front-end, then admin should not be able to add more supplier's for that product. Need to generate a flag which will differentiate product from ADMIN and FRONTEND. 			
				// missing supplier
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("productSupplierList.jhtm"), "sku", sku));				
			} */
			productSupplierForm.setSku( product.getSku() );
			productSupplierForm.getSupplier().setSku( product.getSku() );
		} else {
			// sku not valid
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("productList.jhtm")));			
		}
		if ( request.getParameter( "sid" ) != null )
		{
			Integer supplierId = new Integer( request.getParameter( "sid" ) );
			productSupplierForm.setSupplierId( supplierId );
			Supplier supplier = this.webJaguar.getProductSupplierBySIdSku( sku ,supplierId );
			if (supplier == null) {
				// invalid supplier
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("productSupplierList.jhtm"), "sku", sku));
			}
			productSupplierForm.setSupplier( supplier );
			productSupplierForm.setNewSupplier( false );
		}
		//
		// Please work on this
		//if (product.getDefaultSupplierId() != 0 && productSupplierForm.getSupplierId() != null && product.getDefaultSupplierId().compareTo(productSupplierForm.getSupplierId()) == 0) {
		//	productSupplierForm.setCanDelete(false);
		//}
		
		return productSupplierForm;
	}
	
    protected Map referenceData(HttpServletRequest request) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		Search search = null;
		if ( (Boolean) gSiteConfig.get("gSUPPLIER"))
			map.put("suppliers",this.webJaguar.getSuppliers(search));
 	
		return map;
    }
}