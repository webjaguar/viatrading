/* Copyright 2005, 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 */

package com.webjaguar.web.admin.product;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.Supplier;

public class ProductSupplierExportController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }		
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {

    	Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Object> map = new HashMap<String, Object>();
		
		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			map.put("message", "excelfile.notWritable");
			return new ModelAndView("admin/catalog/product/supplier/export", map);			
		}
		File file[] = baseFile.listFiles();
		List<Map> exportedFiles = new ArrayList<Map>();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith("productSupplierExport") & file[f].getName().endsWith(".xls")) {
				if (request.getParameter("__new") != null) { // check if new button was pressed
					file[f].delete();
				} else {
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					exportedFiles.add( fileMap );				
				}
			}
		}
		map.put("exportedFiles", exportedFiles);
    	
		// check if new button was pressed
		if (request.getParameter("__new") != null) {
			
			int productSupplierCount = this.webJaguar.productSupplierCount();
			int limit = 3000;
			
	    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
	    	String fileName = "/productSupplierExport_" + dateFormatter.format(new Date()) + "";

	    	for (int offset=0, excel = 1; offset<productSupplierCount; excel++) {
		    	List productSupplierList = this.webJaguar.getProductSupplierExportList(limit, offset);
		    	int start = offset + 1;
		    	int end = (offset + limit) < productSupplierCount ? (offset + limit) : productSupplierCount;
		    	HSSFWorkbook wb = createWorkbook(productSupplierList,gSiteConfig, start + " to " + end);
		    	// Write the output to a file
		    	File newFile = new File(baseFile.getAbsolutePath() + fileName + excel + ".xls");
		        FileOutputStream fileOut = new FileOutputStream(newFile);
		        wb.write(fileOut);
		        fileOut.close();
		        HashMap<String, Object> fileMap = new HashMap<String, Object>();
		        fileMap.put("file", newFile);
				exportedFiles.add( fileMap );  		
		        offset = offset + limit;
	    	}
	        
			if (productSupplierCount > 0) {
				map.put("arguments", productSupplierCount);
				map.put("message", "supplierexport.success");
				this.webJaguar.insertImportExportHistory( new ImportExportHistory("prodsupp", SecurityContextHolder.getContext().getAuthentication().getName(), false ));
			} else {
				map.put("message", "supplierexport.empty");				
			}
		} // if    
		
		return new ModelAndView("admin/catalog/product/supplier/export", map);
	}
    
    private HSSFCell getCell(HSSFSheet sheet, int row, short col) {
    	return sheet.createRow(row).createCell(col);
    }
    
    private void setText(HSSFCell cell, String text) {
    	cell.setCellValue(text);
    }
    
    private HSSFWorkbook createWorkbook(List productSupplierList,Map gSiteConfig , String sheetName) throws Exception {
    	Iterator iter = productSupplierList.iterator();

    	HSSFWorkbook wb = new HSSFWorkbook();
    	HSSFSheet sheet = wb.createSheet(sheetName);
    	
    	// create the headers
		Map<String, Short> headerMap = new HashMap<String, Short>();
		short col = 0;
		
		headerMap.put("Sku", col++);
		headerMap.put("SupplierId", col++);
		headerMap.put("SupplierSku", col++);
		headerMap.put("Cost", col++);
		// Consignment
		if ((Boolean) gSiteConfig.get("gCUSTOMER_SUPPLIER")) {
			headerMap.put("Percentage", col++);
		}
		headerMap.put("PrimarySupplier", col++);
		// print headers
		for (String headerName:headerMap.keySet()){
			setText(getCell(sheet, 0, headerMap.get(headerName).shortValue()), headerName); 
		}
	
    	iter = productSupplierList.iterator();
    	
    	HSSFCell cell = null;
		
		// create the rows
    	for (int row=2; iter.hasNext(); row++) {    		
    		Supplier supplier = (Supplier) iter.next();
        	setText(getCell(sheet, row, headerMap.get("Sku").shortValue()), supplier.getSku());
        	setText(getCell(sheet, row, headerMap.get("SupplierId").shortValue()), supplier.getId().toString());
        	setText(getCell(sheet, row, headerMap.get("SupplierSku").shortValue()), supplier.getSupplierSku());
        	if (supplier.getPrice() != null) {
        		cell = getCell(sheet, row, headerMap.get("Cost").shortValue());
        		cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
        		cell.setCellValue(supplier.getPrice().doubleValue()); 
        	}
        	if ((Boolean) gSiteConfig.get("gCUSTOMER_SUPPLIER")) {
        		if(supplier.isPercent()) {
        			setText(getCell(sheet, row, headerMap.get("Percentage").shortValue()), "1");
        		} else{
        			setText(getCell(sheet, row, headerMap.get("Percentage").shortValue()), "0");
        		}
        	}
        	setText(getCell(sheet, row, headerMap.get("PrimarySupplier").shortValue()), supplier.isPrimary() ? "1" : "0");
    	}	  
    	return wb;
    }
}
