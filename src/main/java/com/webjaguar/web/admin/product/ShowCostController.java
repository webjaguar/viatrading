/* Copyright 2006 Advanced E-Media Solutions
 * @author Jwalant
 * @since 10.22.2009
 */

package com.webjaguar.web.admin.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class ShowCostController implements Controller{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
		
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();
		List<Map<String, Object>> costList = new ArrayList<Map<String, Object>>();	
		Integer productId = ServletRequestUtils.getIntParameter( request, "id", -1 );
		if (productId != -1) {
			costList = this.webJaguar.getCostListByProductId( productId );
			model.put("costList", costList);
		}
        return new ModelAndView("admin/catalog/product/showUnitCost", "model", model);
	}
}