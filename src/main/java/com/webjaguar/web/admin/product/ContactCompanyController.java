/* Copyright 2010 Advanced E-Media Solutions
 * @author Jwalant
 * @since 09.09.2010
 */

package com.webjaguar.web.admin.product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Contact;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.KitParts;

public class ContactCompanyController implements Controller{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
		
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
		
		Map<String, Object> model = new HashMap<String, Object>();
		String contactCompany = ServletRequestUtils.getStringParameter( request, "contactcompany");
		System.out.println("contactCompany-------" + contactCompany);
		
		Contact contact = this.webJaguar.getCompanyContactId(contactCompany);
		
		Integer id = null;
		if(contact!=null){
			id = contact.getId();
		}
		
		model.put("id", id);
		return new ModelAndView( "admin/inventory/companyContactInfo", "model", model );		
	}
}