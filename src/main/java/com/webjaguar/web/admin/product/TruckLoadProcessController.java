/* Copyright 2011 Advanced E-Media Solutions
 *
 */
package com.webjaguar.web.admin.product;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;


import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.Product;
import com.webjaguar.model.TruckLoadProcess;
import com.webjaguar.model.TruckLoadProcessProducts;
import com.webjaguar.thirdparty.itext.ITextApi;
import com.webjaguar.web.form.TruckLoadProcessForm;

public class TruckLoadProcessController extends SimpleFormController { 
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public TruckLoadProcessController() {
		setSessionForm(true);
		setCommandName("truckLoadProcessForm");
		setCommandClass(TruckLoadProcessForm.class);
		setFormView("admin/catalog/truckLoadProcessing/form");
		setSuccessView("truckLoadProcessList.jhtm");		
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		TruckLoadProcessForm truckLoadProcessForm = (TruckLoadProcessForm) command;
		
		// when add button is clicked. Insert new truckload to database
		
		
		if(request.getParameter("add")!=null) {
			truckLoadProcessForm.getTruckLoadProcess().setCreatedBy(((AccessUser) request.getAttribute( "accessUser" )).getId());
			this.webJaguar.insertTruckLoadProcess(truckLoadProcessForm.getTruckLoadProcess());
		}
		if (request.getParameter("id") != null) {
			Integer id = (Integer) Integer.parseInt(request.getParameter("id"));
			truckLoadProcessForm.getTruckLoadProcess().setId(id);
		}
		
		// when update button is clicked. 
		if(request.getParameter("update")!=null) {
			truckLoadProcessForm.getTruckLoadProcess().setStatus("pr");
			this.webJaguar.updateTruckLoadProcess(truckLoadProcessForm.getTruckLoadProcess());
		}
		
		// when cancel button is clicked 
		if(request.getParameter("cancel")!=null) {
			truckLoadProcessForm.getTruckLoadProcess().setStatus("x");
			this.webJaguar.updateTruckLoadProcess(truckLoadProcessForm.getTruckLoadProcess());
		}
		
		TruckLoadProcess truckload = this.webJaguar.getTruckLoadProcessById(truckLoadProcessForm.getTruckLoadProcess().getId());

		// when complete button is clicked. 
		// All inventory operation take place
		if(request.getParameter("complete")!=null) {
			truckLoadProcessForm.getTruckLoadProcess().setStatus("c");
			this.webJaguar.updateTruckLoadProcess(truckLoadProcessForm.getTruckLoadProcess());
			originalInventoryAdjust(truckload, request);
			producedInventoryAdjust(truckload , request);
		}
		
		// When processing is complete, we can only update PT
		if(request.getParameter("updatePT")!=null) {
			this.webJaguar.updateTruckLoadPT(truckLoadProcessForm.getTruckLoadProcess());
		}
		
		
		
		if (request.getParameter("__PmPdf") != null) {
			List<Product> products = new ArrayList<Product>();

			int[] indexes = ServletRequestUtils.getIntParameters(request, "__selected_id");
			
			Integer quantity = ServletRequestUtils.getIntParameter(request, "labelQty", 0);
			Integer template = 2;
			Map <String,Integer>prodQty =  new HashMap<String,Integer>();


		 if (indexes != null) {

				for (int i = 0; i < indexes.length; i++) {

                    	TruckLoadProcessProducts derivedProd = this.webJaguar.getTruckLoadDerivedProductById(indexes[i]);
                    	
        					Integer productId  = this.webJaguar.getProductIdBySku(derivedProd.getProduct().getSku());
        					//new
        					Product product = this.webJaguar.getProductById(productId, 0, false, null);
        					
        					if (product != null) {
	                            //replace field24 by condition :- top right
	        					product.setField24((derivedProd.getCondition() !=null) ? derivedProd.getCondition() : "");
	        					
	        					//replace field7 by #Units
	        				    product.setField7((derivedProd.getNo_Of_Units() !=null && !derivedProd.getNo_Of_Units().equals(1)) ? derivedProd.getNo_Of_Units().toString() : null);

	        				    //replace field8 by price/unit
	        					DecimalFormat df = new DecimalFormat("##.##");   							        					

	        				    if(product.getField7()!=null){
		        				    try{
			        					Double pricePerUnit = derivedProd.getUnitPrice()/derivedProd.getNo_Of_Units();
			        					product.setField8("$"+df.format(pricePerUnit).toString()+"/Unit");
		        				    } catch (Exception e){
			        					product.setField8(null);	        				    	
		        				    }
        					     } else {
        					    	 product.setField8(null);
        					     }
	        				    

	        					//replace price1 with new unitPrice
	                            product.setPrice1(derivedProd.getUnitPrice());    	        				
        				        
	        					//replace name by desc
	        					product.setName(derivedProd.getDescription());
	        					
	        					//set Qty
        				        prodQty.put(product.getName(), derivedProd.getQuantity());
        						products.add(product);
        						
        					}      					
				}
		}
			if (products.size() > 0) {
				if (batchPrintLables(request, response, products, quantity, template, prodQty)) {
					// pdf generated
					return null;
				}
			}
		}
		return new ModelAndView(new RedirectView(getSuccessView()));
	}
	private boolean batchPrintLables(HttpServletRequest request, HttpServletResponse response, List products, Integer quantity, Integer templateId, Map<String, Integer> prodQty) {
		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File pdfFile = new File(tempFolder, "labels.pdf");

		boolean result = false;


		try {
			ITextApi iText = new ITextApi();

			File baseImageDir = new File(getServletContext().getRealPath("/assets/Image"));
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
				if (prop.get("site.root") != null) {
					baseImageDir = new File((String) prop.get("site.root"), "/assets/Image");
				}
			} catch (Exception e) {
			}			

			iText.createProcessingListLabels(pdfFile, products, request, getApplicationContext(), quantity, this.webJaguar.getProductLabelTemplate(templateId), prodQty);

			result = true;

			byte[] b = this.webJaguar.getBytesFromFile(pdfFile);

			response.addHeader("Content-Disposition", "attachment; filename=" + pdfFile.getName());
			response.setBufferSize((int) pdfFile.length());
			response.setContentType("application/pdf");
			response.setContentLength((int) pdfFile.length());

			// output stream
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(b, 0, (int) pdfFile.length());
			ouputStream.flush();
			ouputStream.close();

			// delete file before exit
			pdfFile.delete();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}


	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		TruckLoadProcessForm truckLoadProcessForm = (TruckLoadProcessForm) command;
		
		// get new original product added and new produced product added and their quantity
		String originalSKU[] = ServletRequestUtils.getStringParameters(request, "__truckLoadOriginal");
		String originalQuantity[] = ServletRequestUtils.getStringParameters(request, "__truckLoadOriginal_qty");
		String originalValue[] = ServletRequestUtils.getStringParameters(request, "__truckLoadOriginal_value");
		String originalValCG[] = ServletRequestUtils.getStringParameters(request, "__truckLoadOriginal_ValCG");
		String originalPacking[] = ServletRequestUtils.getStringParameters(request, "__truckLoadOriginal_packing");
		String originalNumOfPallets[] = ServletRequestUtils.getStringParameters(request, "__truckLoadOriginal_numOfPallets");

		String producedSKU[] = ServletRequestUtils.getStringParameters(request, "__truckLoadProduced_sku");
		String producedQuantity[] = ServletRequestUtils.getStringParameters(request, "__truckLoadProduced_qty");
		String producedUnitPrice[] = ServletRequestUtils.getStringParameters(request, "__truckLoadProduced_unitPrice");
		String producedDesc[] = ServletRequestUtils.getStringParameters(request, "__truckLoadProduced_Desc");
		String producedCondition[] = ServletRequestUtils.getStringParameters(request, "__truckLoadProduced_condition");
		String producedUnits[] = ServletRequestUtils.getStringParameters(request, "__truckLoadProduced_#units");
		String producedPacking[] = ServletRequestUtils.getStringParameters(request, "__truckLoadProduced_packing");
		String producedNumOfPallets[] = ServletRequestUtils.getStringParameters(request, "__truckLoadProduced_numOfPallets");

		

		TruckLoadProcess truckLoadProcess = new TruckLoadProcess();
		List<Product> originalProduct = new ArrayList<Product>();
		String invalidError = "";
		for(int i=0;i<originalSKU.length;i++) {
			Product product = new Product();
			product.setSku(originalSKU[i].trim());
			Integer id = this.webJaguar.getProductIdBySku(originalSKU[i].trim());
			if (id == null) {
				invalidError = "invalidOriginal";
			}
			else {
			try {
				product.setInventory((Integer) Integer.parseInt(originalQuantity[i].trim()));
				//they requested to add field called Value :- it is mapped to product.price1 only for reference
				product.setPrice1(originalValue[i].trim() == "" ? 0.00 : (Double) Double.parseDouble(originalValue[i].trim()));
				
				//they requested to add field called ValCG :- it is mapped to product.price2 only for reference
				product.setPrice2(originalValCG[i].trim() == ""? 0.00 : (Double) Double.parseDouble(originalValCG[i].trim()));
				
				//set packing
				if(originalPacking!=null && !originalPacking.equals("")) {
					product.setPacking(originalPacking[i].trim());
					
				}
				
				//set #of Pallets
				if(originalNumOfPallets!=null && !originalNumOfPallets.equals("")) {
					product.setField15(originalNumOfPallets[i].trim());		
				}
				
				
			} catch (NumberFormatException e ) {
				invalidError = "invalidOriginal";
			}
			originalProduct.add(product);
			}
		}
		List<TruckLoadProcessProducts> producedProduct = new ArrayList<TruckLoadProcessProducts>();
		for(int i=0;i<producedSKU.length;i++) {
			TruckLoadProcessProducts productProduced = new TruckLoadProcessProducts();
			Integer id = this.webJaguar.getProductIdBySku(producedSKU[i].trim());
			if (id == null) {
				invalidError = "invalid";
			}
			else {
				productProduced.setProduct(this.webJaguar.getProductById( id, 0, false, null));
				try {
					productProduced.setQuantity((Integer) Integer.parseInt(producedQuantity[i].trim()));
					productProduced.setUnitPrice((Double) Double.parseDouble(producedUnitPrice[i].trim()));
					productProduced.setDescription(producedDesc[i].trim());
					productProduced.setCondition(producedCondition[i].trim());
					productProduced.setNo_Of_Units(Integer.parseInt(producedUnits[i].trim()));
					if (producedPacking!=null) {
						productProduced.setPacking(producedPacking[i].trim());

					}
					if(producedNumOfPallets!=null) {
						productProduced.setNumOfPallets((producedNumOfPallets[i].trim()));

					}
				} catch (NumberFormatException e ) {
					invalidError = "invalid";
				}
				producedProduct.add(productProduced);
			}
		}
		truckLoadProcess.setName(truckLoadProcessForm.getTruckLoadProcess().getName());
		truckLoadProcess.setCustom1(truckLoadProcessForm.getTruckLoadProcess().getCustom1());
		truckLoadProcess.setCustom2(truckLoadProcessForm.getTruckLoadProcess().getCustom2());
		truckLoadProcess.setCustom3(truckLoadProcessForm.getTruckLoadProcess().getCustom3());
		truckLoadProcess.setCustom4(truckLoadProcessForm.getTruckLoadProcess().getCustom4());
		truckLoadProcess.setPt(truckLoadProcessForm.getTruckLoadProcess().getPt());
		truckLoadProcess.setType(truckLoadProcessForm.getTruckLoadProcess().getType());
		truckLoadProcess.setTeamHours(truckLoadProcessForm.getTruckLoadProcess().getTeamHours());
		truckLoadProcess.setTeamCost(truckLoadProcessForm.getTruckLoadProcess().getTeamCost());
		truckLoadProcess.setNumOfTeamMembers(truckLoadProcessForm.getTruckLoadProcess().getNumOfTeamMembers());
		truckLoadProcess.setDateStarted(truckLoadProcessForm.getTruckLoadProcess().getDateStarted());
		truckLoadProcess.setDateEnded(truckLoadProcessForm.getTruckLoadProcess().getDateEnded());

		
		if(invalidError == "invalidOriginal") {
			errors.rejectValue( "truckLoadProcess.originalProduct", "dfv", "Truck Load Process SKU is invalid" );
		} else {
			truckLoadProcess.setDerivedProduct(producedProduct);
			truckLoadProcess.setOriginalProduct(originalProduct);
			truckLoadProcess.setLastDate(new Date());
			truckLoadProcessForm.setTruckLoadProcess(truckLoadProcess);
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "truckLoadProcess.name", "form.required", "Truck Load Process Name is required");
		ValidationUtils.rejectIfEmpty(errors, "truckLoadProcess.originalProduct", "Truck Load Process one original SKU is required");
		
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		TruckLoadProcessForm truckLoadProcessForm = new TruckLoadProcessForm();
		TruckLoadProcess truckLoadProcess = new TruckLoadProcess();
		truckLoadProcessForm.setNewTruckLoadProcess(true);
		truckLoadProcessForm.setTruckLoadProcess(truckLoadProcess);
		if (request.getParameter("id") != null) {
			//getProcess By id
			Integer id = (Integer) Integer.parseInt(request.getParameter("id"));
			truckLoadProcessForm.setNewTruckLoadProcess(false);
			truckLoadProcessForm.setTruckLoadProcess(this.webJaguar.getTruckLoadProcessById(id));
			truckLoadProcessForm.getTruckLoadProcess().setId(id);
		}
		return truckLoadProcessForm;
	}
	
	// produced sku inventory adjustment and inventory history
	private void producedInventoryAdjust(TruckLoadProcess truckLoadprocess, HttpServletRequest request) {
		for(TruckLoadProcessProducts truckLoad : truckLoadprocess.getDerivedProduct()) {
			InventoryActivity inventoryActivity = new InventoryActivity();
			inventoryActivity.setSku(truckLoad.getProduct().getSku());
			inventoryActivity.setQuantity(truckLoad.getQuantity());
			inventoryActivity.setType("process");
			inventoryActivity.setReference(truckLoadprocess.getId().toString());
			Inventory inventory = new Inventory();
			inventory.setSku(truckLoad.getProduct().getSku());
			if(this.webJaguar.getInventory(inventory).getInventory() == null) {
				inventoryActivity.setInventory(0 + truckLoad.getQuantity());
			} else {
				inventoryActivity.setInventory(this.webJaguar.getInventory(inventory).getInventory() + truckLoad.getQuantity());
			}
			if(request.getAttribute( "accessUser" ) != null) {
				inventoryActivity.setAccessUserId(((AccessUser) request.getAttribute( "accessUser" )).getId());
			}
			this.webJaguar.adjustInventoryBySku(inventoryActivity);
		}
	}
	
	
	// original sku inventory adjustment and inventory history
	private void originalInventoryAdjust(TruckLoadProcess truckload, HttpServletRequest request) {
		for(Product original : truckload.getOriginalProduct()) {
			InventoryActivity inventoryActivity = new InventoryActivity();
			Inventory inventory = new Inventory();
			inventory.setSku(original.getSku());
			inventory =	this.webJaguar.getInventory(inventory);
			if(inventory.getInventory() != null){
				inventoryActivity.setSku(original.getSku());
				inventoryActivity.setInventory(inventory.getInventory()-original.getInventory());
				inventoryActivity.setInventoryAFS(inventory.getInventoryAFS());
				inventoryActivity.setQuantity(-original.getInventory());
				inventoryActivity.setType("process");
				inventoryActivity.setReference(truckload.getId().toString());
				if(request.getAttribute( "accessUser" ) != null) {
					inventoryActivity.setAccessUserId(((AccessUser) request.getAttribute( "accessUser" )).getId());
				}
				this.webJaguar.adjustInventoryBySku(inventoryActivity);
			}
		}
	}
}
