/* Copyright 2005, 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 */

package com.webjaguar.web.admin.product;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;

import spexlive.etilize.com.Catalog;
import spexlive.etilize.com.CatalogServiceIntf;
import spexlive.etilize.com.CategoryId;
import spexlive.etilize.com.ProductSummary;
import spexlive.etilize.com.Search;
import spexlive.etilize.com.SearchCriteria;
import spexlive.etilize.com.SearchResult;
import spexlive.etilize.com.SelectProductFields;
import spexlive.etilize.com.Sku;
import spexlive.etilize.com.SubcatalogFilter;

import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;

import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Customer;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.Language;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAffiliateCommission;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.KeyBean;

public class ExportController extends SimpleFormController {

	int gPRICE_TABLE = 0;
	int gPRODUCT_IMAGES = 5;
	int gPRICE_TIERS = 1;
	int gPROTECTED = 0;
	int gTAB = 0;
	int gAFFILIATE = 0;
	int NUMBER_PRODUCT_COMMISSION_TABLE = 0;
	boolean PRODUCT_RATE;
	int gPRODUCT_FIELDS = 10;
	int gMAIL_IN_REBATES = 0;
	boolean gALSO_CONSIDER, gRECOMMENDED_LIST, gCASE_CONTENT, gMYLIST, gPRESENTATION, gCOMPARISON, gINVENTORY, gLOYALTY, gSHOPPING_CART
	        ,gCUSTOM_LINES, gSUBSCRIPTION, gMINIMUM_INCREMENTAL_QTY, gMASTER_SKU, gSALES_PROMOTIONS, gMANUFACTURER, gPRODUCT_REVIEW
	        ,gPRICE_CASEPACK;
	List<Language> gI18N;
	String gMOD_REWRITE, gASI;
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }	
	
	private MailSender mailSender;
    public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public ExportController() {
		setSessionForm(false);
		setCommandName("exportProductSearch");
		setCommandClass(ProductSearch.class);
		setFormView("admin/catalog/product/export");
	}
	
	private File baseFile;
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors)  throws Exception {
				
		ProductSearch search = (ProductSearch) command;
    	
		Map<String, Object> map = new HashMap<String, Object>();
		
		if (search.getFileType().equals( "" )) {
			return showForm(request, response, errors, map);
		}
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> gSiteConfig = (Map<String, Object>) request .getAttribute("gSiteConfig");
		
		if(request.getParameter("__new") != null) {
			if (search.getCategory() != null) {
				List<Category> categories = this.webJaguar.getCategoryTree(search.getCategory(), null, true, null, null, null);
				if (categories.isEmpty()) {
					errors.rejectValue("category", "typeMismatch.exportProductSearch.category");
					return showForm(request, response, errors, map);				
				} else {
					List<Integer> categoryIds = new ArrayList<Integer>();
					categoryIds.add(categories.get(0).getId());
					// get subcategory id's
					traverseTree(categoryIds, categories.get(0));
					search.setCategoryIds(categoryIds);
				}
			}
			
			// etilize
			String appId = siteConfig.get("ETILIZE").getValue().trim();
			if (appId.length() > 0) {
				String etilizeCategoryFilter = ServletRequestUtils.getStringParameter(request, "etilizeCategoryFilter", "").trim();
				if (etilizeCategoryFilter.length() > 0) {
					search.setEtilizeSearchCriteria(new SearchCriteria());
					
					String[] etilizeCategoryIds = etilizeCategoryFilter.split("[,]"); // split by commas
					for (int i=0; i<etilizeCategoryIds.length; i++) {
						if (etilizeCategoryIds[i].trim().length() > 0) {
							try {
								CategoryId categoryFilter = new CategoryId();
			        			categoryFilter.setId(Integer.parseInt(etilizeCategoryIds[i].trim()));
			        			search.getEtilizeSearchCriteria().getCategoryFilter().add(categoryFilter);                    						
							} catch (Exception e) {
								// do nothing
							}
						}
					}
					
					if (search.getEtilizeSearchCriteria().getCategoryFilter().size() > 0) {
						search.setEtilizeSkus(getEtilizeProducts(appId, search.getEtilizeSearchCriteria(), request.getParameter("debug") != null));
					}				
				}	
			}
			
			if (baseFile.canWrite()) {
				File file[] = baseFile.listFiles();
				List<Map<String, Object>> exportedFiles = new ArrayList<Map<String, Object>>();
				for (int f=0; f<file.length; f++) {
					if (file[f].getName().startsWith("productExport") & file[f].getName().endsWith("."+search.getFileType())) {
						file[f].delete();
					}
				}
				map.put("exportedFiles", exportedFiles);
				search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("siteConfig", siteConfig);
				params.put("search", search);
				params.put("productFields", this.webJaguar.getProductFields(null, gPRODUCT_FIELDS));
				params.put("labels", this.webJaguar.getLabels());
				
				if(! siteConfig.get("EXCLUDE_FEEDTYPE_FROM_PRODUCT_EXPORT").getValue().isEmpty()) {
					search.setProductFeedType(siteConfig.get("EXCLUDE_FEEDTYPE_FROM_PRODUCT_EXPORT").getValue().split(","));
				}
				int productCount = 0;
				if (search.getEtilizeSkus() != null && search.getEtilizeSkus().size() == 0) {
					// no skus returned by etilize
					productCount = 0;
				} else {
					productCount = this.webJaguar.productCount(search);
				}
				search.setLimit(null);
				if (search.getFileType().equals("xls")) {
					createExcelFiles(productCount, search, exportedFiles, params);
				} else if (search.getFileType().equals("csv")) {
					createCsvFile(productCount, search, siteConfig, gSiteConfig, exportedFiles, params);
				}
		        
				if (productCount > 0) {
					map.put("arguments", productCount);
					map.put("message", "fileexport.success");
					this.webJaguar.insertImportExportHistory( new ImportExportHistory("product", SecurityContextHolder.getContext().getAuthentication().getName(), false ));
				} else {
					map.put("message", "fileexport.empty");				
				}
			}
			// send email
			notifyAdmin(siteConfig, request);
			
		}
		
		
		return showForm(request, response, errors, map);
	}
	
	private void traverseTree(List<Integer> categoryIds, Category category) {
		for (Category thisCategory: category.getSubCategories()) {
			categoryIds.add(thisCategory.getId());
			traverseTree(categoryIds, thisCategory);				
		}
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
    	Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
    	Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
    	gPRICE_TABLE = (Integer) gSiteConfig.get("gPRICE_TABLE");
    	gPRODUCT_IMAGES = (Integer) gSiteConfig.get("gPRODUCT_IMAGES");
    	gPRICE_TIERS = (Integer) gSiteConfig.get("gPRICE_TIERS");
    	gALSO_CONSIDER = (Boolean) gSiteConfig.get("gALSO_CONSIDER");
    	gRECOMMENDED_LIST = (Boolean) gSiteConfig.get("gRECOMMENDED_LIST");
    	gPROTECTED = (Integer) gSiteConfig.get("gPROTECTED");
    	gCASE_CONTENT = (Boolean) gSiteConfig.get("gCASE_CONTENT");
    	gMYLIST = (Boolean) gSiteConfig.get( "gMYLIST" );
    	gPRESENTATION = (Boolean) gSiteConfig.get( "gPRESENTATION" );
    	gCOMPARISON = (Boolean) gSiteConfig.get( "gCOMPARISON" );
    	gINVENTORY = (Boolean) gSiteConfig.get( "gINVENTORY" );
    	gLOYALTY = (Boolean) gSiteConfig.get( "gLOYALTY" );
    	gSHOPPING_CART = (Boolean) gSiteConfig.get("gSHOPPING_CART");
    	gCUSTOM_LINES = (Boolean) gSiteConfig.get("gCUSTOM_LINES");
    	gSUBSCRIPTION = (Boolean) gSiteConfig.get("gSUBSCRIPTION");
    	gTAB = (Integer) gSiteConfig.get("gTAB");
    	gMINIMUM_INCREMENTAL_QTY = (Boolean) gSiteConfig.get("gMINIMUM_INCREMENTAL_QTY"); 
    	gAFFILIATE = (Integer) gSiteConfig.get("gAFFILIATE");
    	NUMBER_PRODUCT_COMMISSION_TABLE = Integer.parseInt( siteConfig.get( "NUMBER_PRODUCT_COMMISSION_TABLE" ).getValue() );
    	gMASTER_SKU = (Boolean) gSiteConfig.get("gMASTER_SKU");
    	gSALES_PROMOTIONS = (Boolean) gSiteConfig.get( "gSALES_PROMOTIONS" );
    	gMAIL_IN_REBATES = (Integer) gSiteConfig.get("gMAIL_IN_REBATES");
    	gMANUFACTURER = (Boolean) gSiteConfig.get( "gMANUFACTURER" );
    	gPRODUCT_REVIEW = (Boolean) gSiteConfig.get( "gPRODUCT_REVIEW" );
    	PRODUCT_RATE = Boolean.parseBoolean(siteConfig.get("PRODUCT_RATE").getValue());
    	gPRODUCT_FIELDS = (Integer) gSiteConfig.get("gPRODUCT_FIELDS");
    	gI18N = this.webJaguar.getLanguageSettings();
    	gMOD_REWRITE = (String) gSiteConfig.get("gMOD_REWRITE");
    	gASI = (String) gSiteConfig.get("gASI");
    	gPRICE_CASEPACK = (Boolean) gSiteConfig.get( "gPRICE_CASEPACK" );
		String fileType = ServletRequestUtils.getStringParameter(request, "fileType2", "xls");
    	
		ProductSearch form = new ProductSearch();

		form.setFileType(fileType);
		baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		
		File file[] = baseFile.listFiles();
		List<Map<String, Object>> exportedFiles = new ArrayList<Map<String, Object>>();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith("productExport") & file[f].getName().endsWith("." + form.getFileType())) {
				HashMap<String, Object> fileMap = new HashMap<String, Object>();
				fileMap.put("file", file[f]);
				fileMap.put("lastModified", new Date(file[f].lastModified()));
				exportedFiles.add( fileMap );				
			}
		}
		request.setAttribute("exportedFiles", exportedFiles);		
		return form;
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		ProductSearch search = (ProductSearch) command;
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		// fileType2 coming from menu
		if (request.getParameter("fileType2") != null && request.getParameter("__new") == null) {
			search.setFileType(request.getParameter("fileType2"));
		}
		
		if(!(gI18N.isEmpty())) {
			map.put("languageCodes", gI18N);
		}
		
		if (!baseFile.canWrite()) {
			if (search.getFileType().equals("xls")) {
				map.put("message", "excelfile.notWritable");						
			} else {
				map.put("message", "file.notWritable");										
			}
		}
		return map;
	}
    
    private HSSFCell getCell(HSSFSheet sheet, int row, short col) {
    	return sheet.createRow(row).createCell(col);
    }
    
    private void setText(HSSFCell cell, String text) {
    	cell.setCellValue(text);
    }
    
    private void createExcelFiles(int productCount, ProductSearch search, List<Map<String, Object>> exportedFiles, Map<String, Object> params) throws Exception {
		int limit = 3000;
		search.setLimit(limit);

    	Map<String, Configuration> siteConfig = (Map) params.get( "siteConfig" );
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
		String fileName = "/productExport_" + dateFormatter.format(new Date())
				+ "";

		for (int offset = 0, excel = 1; offset < productCount; excel++) {
			int start = offset + 1;
			int end = (offset + limit) < productCount ? (offset + limit) : productCount;
			HSSFWorkbook wb = createWorkbook( this.webJaguar.getProductExportList(search, gPRODUCT_IMAGES),start + " to " + end, params);
			
			// Write the output to a file
			File newFile = new File(baseFile.getAbsolutePath() + fileName + excel + ".xls");
			FileOutputStream fileOut = new FileOutputStream(newFile);
			wb.write(fileOut);
			fileOut.close();
			HashMap<String, Object> fileMap = new HashMap<String, Object>();
			fileMap.put("file", newFile);
			exportedFiles.add(fileMap);
			offset = offset + limit;
			search.setOffset(offset);
		}
	}
    
    private HSSFWorkbook createWorkbook(List<Product> productList, String sheetName, Map<String, Object> params) throws Exception {
				
    	Map<Integer, String> supplierNameMap = this.webJaguar.getSupplierNameMap(null);
				
		int numOfCategories = 1;

    	Iterator<Product> iter = productList.iterator();
    	while (iter.hasNext()) {
    		Product product = iter.next();
    		int catIdSize = product.getCatIds().size();
    		numOfCategories = (catIdSize > numOfCategories) ? catIdSize : numOfCategories; 
    	}

    	HSSFWorkbook wb = new HSSFWorkbook();
    	HSSFSheet sheet = wb.createSheet(sheetName);
    	
    	ProductSearch search = (ProductSearch) params.get("search");
    	Map<String, Configuration> siteConfig = (Map) params.get( "siteConfig" );
    	
    	// create the headers
		Map<String, Short> headerMap = new HashMap<String, Short>();
		short col = 0;
		if (search.isWithProductId()) {
			headerMap.put("ID", col++);			
		}
		// sku
		headerMap.put("Sku", col++);
		
		//date created
		headerMap.put("Date Created", col++);
		
		//last modified
		headerMap.put("Last Modified", col++);
		
		//created by
		headerMap.put("Created By", col++);
		
		//last modified by
		headerMap.put("Last Modified By", col++);
		
		// master sku
		if (gMASTER_SKU) {
			headerMap.put("ParentSku", col++);
		}
		
		// primary supplier
		headerMap.put("Primary Supplier", col++);
		
		// UPC 
		headerMap.put("UPC", col++);
		// Etilize
		if (siteConfig.get("ETILIZE").getValue().trim().length() > 0) {
//			headerMap.put("UPC", col++);
			headerMap.put("EtilizeID", col++);
		}
		
		if (search.isWithProductAndImageUrl()) {
			headerMap.put("Product URL", col);
			setText(getCell(sheet, 1, col++), "Read Only");
		}

		if (search.isWithProductName()) {
			headerMap.put("Name", col++);		
			if (search.isWithI18n() && (!(gI18N.isEmpty()))) { // i18n
				  for(Language language: gI18N) {
					  headerMap.put("Name_" + language.getLanguageCode().toUpperCase(), col);
					  setText(getCell(sheet, 1, col++), getApplicationContext().getMessage("language_" + language.getLanguageCode(), new Object[0], null));
				}
			}
		}
		
		headerMap.put("Active", col++);
		
		//Restrict Price Override
		headerMap.put("NO-Price Override", col++);

		
		//Product Review
		if(gPRODUCT_REVIEW || PRODUCT_RATE){
			headerMap.put("ProductReview", col++);	
		}

		headerMap.put("Fob", col++);	
		
		headerMap.put("classField", col++);
		headerMap.put("LeaveOnCloud", col++);
		
		//Freeze
		if(gASI.equals("API") || gASI.equals("File")){
			headerMap.put("Freeze", col++);	
		}
			
		// Brand
		if (gMANUFACTURER) {
			headerMap.put("Manufacture", col++);
		}
		
		// Short/Long Description
		headerMap.put("ShortDescription", col++);
		if (search.isWithI18n() && (!(gI18N.isEmpty()))) { // i18n
			for(Language language: gI18N) {
				headerMap.put("ShortDescription_" + language.getLanguageCode().toUpperCase(), col);	
				setText(getCell(sheet, 1, col++), getApplicationContext().getMessage("language_" + language.getLanguageCode(), new Object[0], null));
			}
		}
		if (search.isWithLongDesc()) {
			headerMap.put("LongDescription", col++);
			if (search.isWithI18n() && (!(gI18N.isEmpty()))) { // i18n
				for(Language language: gI18N) {
					headerMap.put("LongDescription_" + language.getLanguageCode().toUpperCase(), col);
					setText(getCell(sheet, 1, col++), getApplicationContext().getMessage("language_" + language.getLanguageCode(), new Object[0], null));
				}
			}
		}
		
		headerMap.put("HtmlAddToCart", col++);
		
		if (search.isWithProductAndImageUrl()) {
			headerMap.put("Small Image URL", col);
			setText(getCell(sheet, 1, col++), "Read Only");
			headerMap.put("Large Image URL", col);
			setText(getCell(sheet, 1, col++), "Read Only");
		}
		
		// Images
		short[] images = new short[gPRODUCT_IMAGES];
		for (int i=0; i<gPRODUCT_IMAGES; i++) {
			Short imageColumn = col++;
			images[i] = imageColumn;
			setText(getCell(sheet, 0, imageColumn), "Image"); 
		}
		
		// Tab
		for (int i=1; i<=gTAB; i++) {
			headerMap.put("Tab" + i, col++);
			headerMap.put("TabContent" + i, col++);
		}
		// image layout
		headerMap.put( "ImageLayout", col++ );
		
		// image folder
		headerMap.put( "ImageFolder", col++ );
		
		// Layout
		headerMap.put("ProductLayout", col++);
		
		// weight
		headerMap.put("Weight", col++);
		
		// upsMaxItemsInPackage
		headerMap.put("UpsMaxItemsInPackage", col++);
		
		// uspsMaxItemsInPackage
		headerMap.put("UspsMaxItemsInPackage", col++);
		
		// dimension
		headerMap.put("PackageLength", col++);
		headerMap.put("PackageWidth", col++);
		headerMap.put("PackageHeight", col++);
		
		// msrp
		headerMap.put("MSRP", col++);
		
		// SalesTag ID
		if (gSALES_PROMOTIONS) {
			headerMap.put("SalesTagId", col++);
		}
		
		// Mail In Rebate
		if (gMAIL_IN_REBATES > 0) {
			headerMap.put("MailInRebateId", col++);
		}
		
    	// price and quantity breaks
		for (int i=1; i<=gPRICE_TIERS; i++) {
			headerMap.put("Price" + i, col++);
			if (i < gPRICE_TIERS) headerMap.put("QtyBreak" + i, col++);
		}
		// cost tier
		if(siteConfig.get("COST_TIERS").getValue().equals("true")){
			for (int i=1; i<=gPRICE_TIERS; i++) {
				headerMap.put("Cost" + i, col++);
			}
		}
    	// price table
		for (int i=1; i<=gPRICE_TABLE; i++) {
			headerMap.put("PriceTable" + i, col);
			setText(getCell(sheet, 1, col++), ((Map<String, String>) params.get("labels")).get("priceTable" + i));
		}
		// price case pack
		if (gPRICE_CASEPACK) {
			headerMap.put("PriceCasePackQty", col++);
			for (int i=1; i<=10; i++) {
				headerMap.put("PriceCasePack" + i, col++);
			}
		}
    	
		// price quote
		if( siteConfig.get("PRODUCT_QUOTE").getValue().equals("true")) {
			headerMap.put("PriceQuote", col++);
		}
		// commission
		if ( gAFFILIATE > 0 ) {
			for (int i=1; i<=NUMBER_PRODUCT_COMMISSION_TABLE; i++) {
				headerMap.put("CommissionTable" + i, col++);
			}
		}
		
    	// extra product fields
		for (ProductField productField: ((List<ProductField>) params.get("productFields"))) {
			if (productField.isEnabled() && productField.isProductExport()) {
				headerMap.put("Field_" + productField.getId(), col);
				setText(getCell(sheet, 1, col++), productField.getName());				
				if (search.isWithI18n() && (!(gI18N.isEmpty()))) { // i18n
					for(Language language: gI18N) {
						headerMap.put("Field_" + productField.getId() + "_" + language.getLanguageCode().toUpperCase(), col);
						setText(getCell(sheet, 1, col++), getApplicationContext().getMessage("language_" + language.getLanguageCode(), new Object[0], null));
					}
				}
			}
		}
		
		// keywords
		headerMap.put("Keywords", col++);
		
		// login require
		headerMap.put("LoginRequired", col++);
		
		// hide price
		headerMap.put("HidePrice", col++);
		
		// hide msrp
		headerMap.put("HideMsrp", col++);
		
		// hide leftbar
		headerMap.put("HideLeftBar", col++);
		
		// hide rightbar
		headerMap.put("HideRightBar", col++);
		
		// taxable
		headerMap.put("Taxable", col++);
		
		// searchable
		headerMap.put("Searchable", col++);
		
		// custom shipping enabled
		if (gSHOPPING_CART) headerMap.put("CustomShippingEnabled", col++);
		
		// option code
		headerMap.put("OptionCode", col++);
		
		// cross items
		if (gPRICE_TIERS > 1) headerMap.put("CrossItemsCode", col++);
		
		// also consider
		if (gALSO_CONSIDER) headerMap.put("AlsoConsider", col++);		

		// recommended list
		if (gRECOMMENDED_LIST && search.isWithRecommendedList()) {
			headerMap.put("RecommendedList", col++);	
			headerMap.put("RecommendedListTitle", col++);	
			headerMap.put("RecommendedListDisplay", col++);	
		}
		
		// protected level
		if (gPROTECTED > 0) headerMap.put("ProtectedLevel", col++);
    	
		// packing
		headerMap.put("Packing", col++);

		// case content
		if ( gCASE_CONTENT )  {
			 headerMap.put("CaseContent", col++);
		}
		
		// minimum / incremental
		if ( gMINIMUM_INCREMENTAL_QTY ) {
			headerMap.put("MinimumQty", col++);
			headerMap.put("IncrementalQty", col++);
		}
		
		//add to presentation
		if( gPRESENTATION ) headerMap.put("AddToPresentation", col++);
		
		// add to List
		if ( gMYLIST ) headerMap.put("AddToList", col++);

		// product comparison
		if ( gCOMPARISON ) headerMap.put("Compare", col++);
		
		// inventory
		if ( gINVENTORY && siteConfig.get( "INVENTORY_ON_PRODUCT_IMPORT_EXPORT" ).getValue().equals( "true" )) {
			headerMap.put("OnHand", col++);		
			headerMap.put("AvailableForSale", col++);
			headerMap.put("AllowNegativeInventory", col++);
			headerMap.put("ShowNegativeInventory", col++);
			headerMap.put("LowInventoryAlert", col++);
		}
		
		// loyalty point
		if ( gLOYALTY ) headerMap.put("LoyaltyPoint", col++);
		
		// custom lines
		if ( gCUSTOM_LINES ) {
			headerMap.put("NumberOfCustomLine", col++);
			headerMap.put("CustomLineCharacter", col++);
		}
         
		//redirect url English
		 headerMap.put("RedirectUrlEn", col++);
			
		 //redirect url Spanish
        headerMap.put("RedirectUrlEs", col++);

        //Category ids string
        headerMap.put("CatIdString", col++);
		 
		// subscription
		if (gSUBSCRIPTION) {
			headerMap.put("AutoShip", col++);
			headerMap.put("AutoShipDiscount", col++);			
		}
		
		// sitemap
		if(siteConfig.get("SITEMAP").getValue().equals( "true" )) {
			headerMap.put("SitemapPriority", col++);
		}
		
		// print headers
		for (String headerName:headerMap.keySet()){
			setText(getCell(sheet, 0, headerMap.get(headerName).shortValue()), headerName); 
		}

		// category associations
		short[] category = new short[numOfCategories];
		for (int i=0; i<numOfCategories; i++) {
			Short categoryColumn = col++;
			category[i] = categoryColumn;
			setText(getCell(sheet, 0, categoryColumn), "Category"); 
		}

		

		
	
		
		HSSFCellStyle dateCellStyle = wb.createCellStyle();
		dateCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yy");
		
    	iter = productList.iterator();
    	Class<Product> c = Product.class;
		Method m = null;
		HSSFCell cell = null;
		Object arglist[] = null;
    	
		// create the rows
    	for (int row=2; iter.hasNext(); row++) {    		
    		Product product = (Product) iter.next();
    		if (search.isWithProductId()) {
        		setText(getCell(sheet, row, headerMap.get("ID").shortValue()), product.getId().toString());    			
    		}
    		if (search.isWithProductName()) {
    			setText(getCell(sheet, row, headerMap.get("Name").shortValue()), product.getName());
    		}
        	setText(getCell(sheet, row, headerMap.get("Active").shortValue()), product.isActive() ? "1" : null);
        	
        	setText(getCell(sheet, row, headerMap.get("NO-Price Override").shortValue()), product.isRestrictPriceChange() ? "1" : null);

        	if(gPRODUCT_REVIEW || PRODUCT_RATE){
        		setText(getCell(sheet, row, headerMap.get("ProductReview").shortValue()), product.isEnableRate()  ? "1" : null);	
        	}
        	setText(getCell(sheet, row, headerMap.get("Fob").shortValue()), product.getFobZipCode());	
        	if(gASI.equals("API") || gASI.equals("File")){
        		setText(getCell(sheet, row, headerMap.get("Freeze").shortValue()), product.isFeedFreeze()  ? "1" : null);	
        	}
        	setText(getCell(sheet, row, headerMap.get("Sku").shortValue()), product.getSku());
        	
        	if(product.getClassField() != null ){
        		System.out.println("product: "+product.getName()+ " "+product.getId());
        		setText(getCell(sheet, row, headerMap.get("classField").shortValue()), product.getClassField());
        	}       	
        	
        	//leave on cloud
        		setText(getCell(sheet, row, headerMap.get("LeaveOnCloud").shortValue()), product.isSoftLink()? "1" : null);
        
        	
        	// Date Created
        	cell = getCell(sheet, row, headerMap.get("Date Created").shortValue());
        	cell.setCellStyle(dateCellStyle);
    		cell.setCellValue(sdfDate.format( product.getCreated()));
        	
    		// Last Modified
    		cell = getCell(sheet, row, headerMap.get("Last Modified").shortValue());
        	cell.setCellStyle(dateCellStyle);
        	if(product.getLastModified() != null) {
        		cell.setCellValue(sdfDate.format( product.getLastModified()));
        	} else {
        		cell.setCellValue("");
        	}
        	
        	// Created By
        	setText(getCell(sheet, row, headerMap.get("Created By").shortValue()), product.getCreatedBy() != null ? product.getCreatedBy() : "");
        	// Last Modified By
        	setText(getCell(sheet, row, headerMap.get("Last Modified By").shortValue()), product.getLastModifiedBy() != null ? product.getLastModifiedBy() : "");
        	
        	
        	// Parent sku
    		if (gMASTER_SKU) {
    			setText(getCell(sheet, row, headerMap.get("ParentSku").shortValue()), product.getMasterSku());
    		}    		
    		// Primary Supplier
			setText(getCell(sheet, row, headerMap.get("Primary Supplier").shortValue()), product.getDefaultSupplierId() != 0 ? supplierNameMap.get((long) product.getDefaultSupplierId()) : "");
		// UPC	
			setText(getCell(sheet, row, headerMap.get("UPC").shortValue()), product.getUpc());
    		// Etilize
    		if (siteConfig.get("ETILIZE").getValue().trim().length() > 0) {
//    			setText(getCell(sheet, row, headerMap.get("UPC").shortValue()), product.getUpc());
    			setText(getCell(sheet, row, headerMap.get("EtilizeID").shortValue()), (product.getEtilizeId() == null) ? null : product.getEtilizeId().toString());   
    		}
    		
    		// Redirect url English
        	setText(getCell(sheet, row, headerMap.get("RedirectUrlEn").shortValue()), (product.getRedirectUrlEn() == null) ? "" : product.getRedirectUrlEn());
        	//Spanish
        	setText(getCell(sheet, row, headerMap.get("RedirectUrlEs").shortValue()), (product.getRedirectUrlEs() == null) ? "" : product.getRedirectUrlEs());

        	//Cat id string
        	setText(getCell(sheet, row, headerMap.get("CatIdString").shortValue()), (product.getCategoryIds() == null) ? "" : product.getCategoryIds());

    		// Manufacture
    		if (gMANUFACTURER) {
    			setText(getCell(sheet, row, headerMap.get("Manufacture").shortValue()), product.getManufactureName());
    		}
        	setText(getCell(sheet, row, headerMap.get("ShortDescription").shortValue()), product.getShortDesc());
    		if (search.isWithLongDesc()) {
            	setText(getCell(sheet, row, headerMap.get("LongDescription").shortValue()), product.getLongDesc());    			
    		}
    		setText(getCell(sheet, row, headerMap.get("HtmlAddToCart").shortValue()), product.getHtmlAddToCart());
        	setText(getCell(sheet, row, headerMap.get("ImageLayout").shortValue()), product.getImageLayout());
        	setText(getCell(sheet, row, headerMap.get("ImageFolder").shortValue()), product.getImageFolder());
    		
    		if (search.isWithProductAndImageUrl()) {
    			String productLink = "product.jhtm?id=" + product.getId() + "&";
    			if (gMOD_REWRITE != null && gMOD_REWRITE.equals("1") && product.getSku() != null && !product.getSku().contains("/")) {
    				productLink = siteConfig.get("MOD_REWRITE_PRODUCT").getValue() + "/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html?";
    			}
    			setText(getCell(sheet, row, headerMap.get("Product URL").shortValue()), 
    					siteConfig.get( "SITE_URL" ).getValue() + productLink + "tc=replacethistrackcode" );
    		}
        	
        	// Images
    		int index = 0;
  		    if (product.getImages() != null) {
  	    		for (ProductImage image:product.getImages()) {
  	    			if (index == 0 && search.isWithProductAndImageUrl()) {
  	    				if (image.getImageUrl().contains("//") || image.getImageUrl().startsWith("/")) {
  	    	    			setText(getCell(sheet, row, headerMap.get("Small Image URL").shortValue()), image.getImageUrl());
  	    	    			setText(getCell(sheet, row, headerMap.get("Large Image URL").shortValue()), image.getImageUrl());
  	    				} else {
  	    	    			setText(getCell(sheet, row, headerMap.get("Small Image URL").shortValue()), 
  	    	    					siteConfig.get( "SITE_URL" ).getValue() + "assets/Image/Product/thumb/" + image.getImageUrl() );
  	    	    			setText(getCell(sheet, row, headerMap.get("Large Image URL").shortValue()), 
  	    	    					siteConfig.get( "SITE_URL" ).getValue() + "assets/Image/Product/detailsbig/" + image.getImageUrl() );
  	    				}
  	    			}
  	    			setText(getCell(sheet, row, images[index++]), image.getImageUrl());
  	    		}
  		    }

  		    // set tab
	  		for (int i=1; i<=gTAB; i++) {
	  			cell = getCell(sheet, row, headerMap.get("Tab"+i).shortValue());
	  			m = c.getMethod("getTab" + i);
				String tabName = (String) m.invoke(product, arglist);
				cell.setCellType( HSSFCell.CELL_TYPE_STRING );
        		cell.setCellValue(tabName);  
        		cell = getCell(sheet, row, headerMap.get("TabContent"+i).shortValue());
	  			m = c.getMethod("getTab" + i + "Content");
				String tabContent = (String) m.invoke(product, arglist);
				cell.setCellType( HSSFCell.CELL_TYPE_STRING );
        		cell.setCellValue(tabContent);  
	  		}

			// Layout
	  		for(KeyBean productLayout : Constants.PRODUCT_LAYOUT) {
	  			if(product.getProductLayout() != null && product.getProductLayout().toString().equalsIgnoreCase(productLayout.getName().toString())) {
	  	  			setText(getCell(sheet, row, headerMap.get("ProductLayout").shortValue()), ( product.getProductLayout() == null ) ? "" : productLayout.getValue().toString());	  				
	  			}
	  		}
  			
    		
  			// set weight
  	  		cell = getCell(sheet, row, headerMap.get("Weight").shortValue()); 		    
  	    	cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	    	cell.setCellValue(product.getWeight());   
  	    	
  	    	
  	        // upsMaxItemsInPackage
  			if(product.getUpsMaxItemsInPackage() != null) {			
  			cell = getCell(sheet, row, headerMap.get("UpsMaxItemsInPackage").shortValue());
  			cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	    	cell.setCellValue(product.getUpsMaxItemsInPackage()); 
  			}
  	    	
  			// uspsMaxItemsInPackage
  			if(product.getUspsMaxItemsInPackage() != null) {			
  			cell = getCell(sheet, row, headerMap.get("UspsMaxItemsInPackage").shortValue());
  			cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	    	cell.setCellValue(product.getUspsMaxItemsInPackage()); 
  			}
  	    	
  	    	// dimension
  	    	if (product.getPackageL() != null) {
  	  	    	cell = getCell(sheet, row, headerMap.get("PackageLength").shortValue());
  	  	    	cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
  	  	    	cell.setCellValue(product.getPackageL());     	    		
  	    	}
  	    	if (product.getPackageW() != null) {
  	  	    	cell = getCell(sheet, row, headerMap.get("PackageWidth").shortValue());
  	  	    	cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
  	  	    	cell.setCellValue(product.getPackageW());     	    		
  	    	}
  	    	if (product.getPackageH() != null) {
  	  	    	cell = getCell(sheet, row, headerMap.get("PackageHeight").shortValue());
  	  	    	cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
  	  	    	cell.setCellValue(product.getPackageH());     	    		
  	    	}
  		    
  			// set msrp
  		    if (product.getMsrp() != null) {
  	  			cell = getCell(sheet, row, headerMap.get("MSRP").shortValue()); 		    
  	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	    		cell.setCellValue(product.getMsrp());           	  		    	
  		    }
  		    
  		    // SalesTag ID
  			if (gSALES_PROMOTIONS) {
  				setText(getCell(sheet, row, headerMap.get("SalesTagId").shortValue()), ( product.getSalesTagId() == null ) ? "" : product.getSalesTagId().toString());
  			}
  			
  			// Mail In Rebate
  			if (gMAIL_IN_REBATES > 0) {
  				setText(getCell(sheet, row, headerMap.get("MailInRebateId").shortValue()), ( product.getRebateId() == null ) ? "" : product.getRebateId().toString());
  		  	}
  			
  			// set price & price break
    		for (int i=1; i<=gPRICE_TIERS; i++) {
    			cell = getCell(sheet, row, headerMap.get("Price"+i).shortValue());
				m = c.getMethod("getPrice" + i);
				Double price = (Double) m.invoke(product, arglist);
				if (price != null) {
	        		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
	        		cell.setCellValue(price.doubleValue());    								
				}
				if (i < gPRICE_TIERS) {
	    			cell = getCell(sheet, row, headerMap.get("QtyBreak"+i).shortValue());
					m = c.getMethod("getQtyBreak" + i);
					Integer qtyBreak = (Integer) m.invoke(product, arglist);
					if (qtyBreak != null) {
		        		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
		        		cell.setCellValue(qtyBreak.doubleValue());    								
					}					
				}
    		}
    		
    		// set cost
    		if(siteConfig.get("COST_TIERS").getValue().equals("true")) {
    			for (int i=1; i<=gPRICE_TIERS; i++) {
        			cell = getCell(sheet, row, headerMap.get("Cost"+i).shortValue());
    				m = c.getMethod("getCost" + i);
    				Double cost = (Double) m.invoke(product, arglist);
    				if (cost != null) {
    	        		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
    	        		cell.setCellValue(cost.doubleValue());    								
    				}
    			}
    		}
        	
        	// set extra product fields
    		for (ProductField productField: ((List<ProductField>) params.get("productFields"))) {
    			if (productField.isEnabled() && productField.isProductExport()) {
            		m = c.getMethod("getField" + productField.getId());
            		setText(getCell(sheet, row, headerMap.get("Field_"+productField.getId()).shortValue()), (String) m.invoke(product, arglist));    				
    			}
        	}
        	
        	// price table
    		for (int i=1; i<=gPRICE_TABLE; i++) {
    			cell = getCell(sheet, row, headerMap.get("PriceTable"+i).shortValue());
				m = c.getMethod("getPriceTable" + i);
        		Double priceTable = (Double) m.invoke(product, arglist);
				if (priceTable != null) {
	        		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
	        		cell.setCellValue(priceTable.doubleValue());    								
				}
    		}
    		
    		// price case pack
    		if (gPRICE_CASEPACK) {
    			setText(getCell(sheet, row, headerMap.get("PriceCasePackQty").shortValue()), (product.getPriceCasePackQty() == null) ? null : product.getPriceCasePackQty().toString());   
    			
    			for (int i=1; i<=10; i++) {
    				cell = getCell(sheet, row, headerMap.get("PriceCasePack"+i).shortValue());
    				m = c.getMethod("getPriceCasePack" + i);
            		Double priceCasePack = (Double) m.invoke(product, arglist);
    				if (priceCasePack != null) {
    	        		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
    	        		cell.setCellValue(priceCasePack.doubleValue());    								
    				}
    			}
    		}
    		
    		// price quote
    		if( siteConfig.get("PRODUCT_QUOTE").getValue().equals("true")) {
    			setText(getCell(sheet, row, headerMap.get("PriceQuote").shortValue()), product.isQuote() ? "1" : null);
            }
    		
    		// commission
    		if ( gAFFILIATE > 0 && product.getCommissionTables() != null){
    			Class<ProductAffiliateCommission> commission = ProductAffiliateCommission.class;
    			for (int i=1; i<=NUMBER_PRODUCT_COMMISSION_TABLE; i++) {
        			cell = getCell(sheet, row, headerMap.get("CommissionTable"+i).shortValue());
    				m = commission.getMethod("getCommissionTable" + i);
            		Double commissionTable = (Double) m.invoke(product.getCommissionTables(), arglist);
    				if (commissionTable != null) {
    	        		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
    	        		cell.setCellValue(commissionTable.doubleValue());    								
    				}
        		}
    		}
    		
    		// keywords
        	setText(getCell(sheet, row, headerMap.get("Keywords").shortValue()), product.getKeywords());
        	
    		// login require
        	setText(getCell(sheet, row, headerMap.get("LoginRequired").shortValue()), product.isLoginRequire() ? "1" : null);

    		// hide price
        	setText(getCell(sheet, row, headerMap.get("HidePrice").shortValue()), product.isHidePrice() ? "1" : null);  
        	
        	// hide msrp
        	setText(getCell(sheet, row, headerMap.get("HideMsrp").shortValue()), product.isHideMsrp() ? "1" : null);  
        	
        	// hide left bar
        	setText(getCell(sheet, row, headerMap.get("HideLeftBar").shortValue()), product.isHideLeftBar() ? "1" : null);  
        	
        	// hide right bar
        	setText(getCell(sheet, row, headerMap.get("HideRightBar").shortValue()), product.isHideRightBar() ? "1" : null);  
        	
        	// taxable
        	setText(getCell(sheet, row, headerMap.get("Taxable").shortValue()), product.isTaxable() ? "1" : null);
        	
        	// searchable
        	setText(getCell(sheet, row, headerMap.get("Searchable").shortValue()), product.isSearchable() ? "1" : null);
        	
    		// custom shipping enabled
    		if (gSHOPPING_CART) {
            	setText(getCell(sheet, row, headerMap.get("CustomShippingEnabled").shortValue()), product.isCustomShippingEnabled() ? "1" : null);
    		}
        	
    		// option code
        	setText(getCell(sheet, row, headerMap.get("OptionCode").shortValue()), product.getOptionCode());
        	
    		// cross items
    		if (gPRICE_TIERS > 1) {
            	setText(getCell(sheet, row, headerMap.get("CrossItemsCode").shortValue()), product.getCrossItemsCode());
    		}
    		
    		// also consider
    		if (gALSO_CONSIDER) {
            	setText(getCell(sheet, row, headerMap.get("AlsoConsider").shortValue()), product.getAlsoConsider());    			
    		}

    		if (gRECOMMENDED_LIST && search.isWithRecommendedList()) {
            	setText(getCell(sheet, row, headerMap.get("RecommendedList").shortValue()), product.getRecommendedList());
            	setText(getCell(sheet, row, headerMap.get("RecommendedListTitle").shortValue()), product.getRecommendedListTitle());
            	setText(getCell(sheet, row, headerMap.get("RecommendedListDisplay").shortValue()), product.getRecommendedListDisplay());
    		}
        	
    		// protected level
    		if (gPROTECTED > 0) {
            	setText(getCell(sheet, row, headerMap.get("ProtectedLevel").shortValue()), product.getProtectedLevelAsNumber().toString());    			
    		}
    		
    		// packing
			setText(getCell(sheet, row, headerMap.get("Packing").shortValue()), product.getPacking() );
			
    		// case content
    		if ( gCASE_CONTENT ) {
    			setText(getCell(sheet, row, headerMap.get("CaseContent").shortValue()), (product.getCaseContent() == null) ? null : product.getCaseContent().toString());   
    		}
    		
    		// minimum / incremental
    		if ( gMINIMUM_INCREMENTAL_QTY ) {
    			setText(getCell(sheet, row, headerMap.get("MinimumQty").shortValue()), (product.getMinimumQty() == null) ? null : product.getMinimumQty().toString());
    			setText(getCell(sheet, row, headerMap.get("IncrementalQty").shortValue()), (product.getIncrementalQty() == null) ? null : product.getIncrementalQty().toString());
    		}
    		
    		// add to pressentation
    		if (gPRESENTATION) {
    			setText(getCell(sheet, row, headerMap.get("AddToPresentation").shortValue()), product.isAddToPresentation()? "1" : null);    			
    		}
    		
    		// addToList
    		if ( gMYLIST ) {
    			setText(getCell(sheet, row, headerMap.get("AddToList").shortValue()), product.isAddToList() ? "1" : null);
    		}
    		
    		// product comparison
    		if ( gCOMPARISON ) {
    			setText(getCell(sheet, row, headerMap.get("Compare").shortValue()), product.isCompare() ? "1" : null);
    		}
    		
    		// set inventory
    		if ( gINVENTORY && siteConfig.get( "INVENTORY_ON_PRODUCT_IMPORT_EXPORT" ).getValue().equals( "true" )) {
    			// set inventory
      		    if (product.getInventory() != null) {
      	  			cell = getCell(sheet, row, headerMap.get("OnHand").shortValue()); 		    
      	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    		cell.setCellValue(product.getInventory());           	  		    	
      		    }
      		    
      		  if (product.getInventoryAFS() != null) {
    	  			cell = getCell(sheet, row, headerMap.get("AvailableForSale").shortValue()); 		    
    	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
    	    		cell.setCellValue(product.getInventoryAFS());           	  		    	
    		    }
            	
            	setText(getCell(sheet, row, headerMap.get("AllowNegativeInventory").shortValue()), product.isNegInventory() ? "1" : null);
            	setText(getCell(sheet, row, headerMap.get("ShowNegativeInventory").shortValue()), product.isShowNegInventory() ? "1" : null);

            	// set low inventory
      		    if (product.getLowInventory() != null) {
      	  			cell = getCell(sheet, row, headerMap.get("LowInventoryAlert").shortValue()); 		    
      	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
      	    		cell.setCellValue(product.getLowInventory());           	  		    	
      		    }
    		}
    		
    		// loyalty point
    		if ( gLOYALTY ) {
    			setText(getCell(sheet, row, headerMap.get("LoyaltyPoint").shortValue()), (product.getLoyaltyPoint() == null) ? null : product.getLoyaltyPoint().toString() );
    		}
    		
    		// custom lines
			if (gCUSTOM_LINES && product.getNumCustomLines() != null) {
  	  			cell = getCell(sheet, row, headerMap.get("NumberOfCustomLine").shortValue()); 		    
  	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	    		cell.setCellValue(product.getNumCustomLines()); 
  	    		
  	    		if ( product.getCustomLineCharacter() != null ) {
  	    			cell = getCell(sheet, row, headerMap.get("CustomLineCharacter").shortValue()); 		    
  	  	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	  	    		cell.setCellValue(product.getCustomLineCharacter()); 
  	    		}
  		    }
			
			// subscription
			if (gSUBSCRIPTION) {
    			setText(getCell(sheet, row, headerMap.get("AutoShip").shortValue()), product.isSubscription() ? "1" : null);

  	    		if (product.getSubscriptionDiscount() != null) {
  	    			cell = getCell(sheet, row, headerMap.get("AutoShipDiscount").shortValue()); 		    
  	  	    		cell.setCellType( HSSFCell.CELL_TYPE_NUMERIC );
  	  	    		cell.setCellValue(product.getSubscriptionDiscount()); 
  	    		}
			}
			
			if(siteConfig.get("SITEMAP").getValue().equals( "true" )) {
				setText(getCell(sheet, row, headerMap.get("SitemapPriority").shortValue()), (product.getSiteMapPriority() != null ? product.getSiteMapPriority().toString() : "0.5"));
				headerMap.put("", col++);
			}
			
			// i18n
        	if (search.isWithI18n() && (!(gI18N.isEmpty())) && !product.getI18nProduct().isEmpty()) {
        		for(Language language: gI18N) {
					if (product.getI18nProduct().containsKey(language.getLanguageCode())) {
						if (search.isWithProductName()) {
							setText(getCell(sheet, row, headerMap.get("Name_" + language.getLanguageCode().toUpperCase()).shortValue()), product.getI18nProduct().get(language.getLanguageCode()).getI18nName());
						}
    					setText(getCell(sheet, row, headerMap.get("ShortDescription_" + language.getLanguageCode().toUpperCase()).shortValue()), product.getI18nProduct().get(language.getLanguageCode()).getI18nShortDesc());
    					if (search.isWithLongDesc()) {
        					setText(getCell(sheet, row, headerMap.get("LongDescription_" + language.getLanguageCode().toUpperCase()).shortValue()), product.getI18nProduct().get(language.getLanguageCode()).getI18nLongDesc());    						
    					}
    		        	// extra product fields
    		    		for (ProductField productField: ((List<ProductField>) params.get("productFields"))) {
    		    			if (productField.isEnabled() && productField.isProductExport()) {
    		            		m = c.getMethod("getField" + productField.getId());
    		            		setText(getCell(sheet, row, headerMap.get("Field_" + productField.getId() + "_" + language.getLanguageCode().toUpperCase()).shortValue()), (String) m.invoke(product.getI18nProduct().get(language.getLanguageCode()), arglist));
    		    			}
    		        	}
					}
				}
        	}
     		
    		// category associations
    		Iterator catIdIter = product.getCatIds().iterator();
    		index = 0;
			while (catIdIter.hasNext()) {
	    		setText(getCell(sheet, row, category[index++]), catIdIter.next().toString());
			}	
    	} // for	  
    	return wb;
    }
    
    private void createCsvFile(int productCount, ProductSearch search, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig, List<Map<String, Object>> exportedFiles, Map<String, Object> params) throws Exception {
    	
    	int numOfCategories = 1;
    	Map<Integer, String> supplierNameMap = this.webJaguar.getSupplierNameMap(null);

		List productList = this.webJaguar.getProductExportList(search, gPRODUCT_IMAGES);
		Iterator<Product> iter = productList.iterator();
		while (iter.hasNext()) {
			Product product = iter.next();
			int catIdSize = product.getCatIds().size();
			numOfCategories = (catIdSize > numOfCategories) ? catIdSize : numOfCategories;
		}

		if (productCount < 1)
			return;
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");

		String fileName = "/productExport_" + dateFormatter.format(new Date())+ ".csv";

		File exportFile = new File(baseFile, fileName);
		CSVWriter writer = new CSVWriter(new FileWriter(exportFile));
		List<String> line = new ArrayList<String>();

		dateFormatter = new SimpleDateFormat("MM/dd/yy kk:mm");

		// product fields
		List<ProductField> productFields = null;
		if (siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue() != null && siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue().equals("true")) {
			productFields = this.webJaguar.getProductFields(null,(Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
			Iterator pfIter = productFields.iterator();
			while (pfIter.hasNext()) {
				ProductField productField = (ProductField) pfIter.next();
				if (!productField.isEnabled() || !productField.isShowOnInvoiceExport()) {
					pfIter.remove();
				}
			}
		}
		// header name
		if (search.isWithProductId()) {
			line.add(getMessageSourceAccessor().getMessage("id"));
		}

		line.add(getMessageSourceAccessor().getMessage("sku"));
		line.add(getMessageSourceAccessor().getMessage("dateCreated"));
		line.add(getMessageSourceAccessor().getMessage("lastModified"));
		line.add(getMessageSourceAccessor().getMessage("createdBy"));
		line.add(getMessageSourceAccessor().getMessage("lastModifiedBy"));

		if (gMASTER_SKU) {
			line.add(getMessageSourceAccessor().getMessage("masterSku"));
		}
		// Primary Supplier
		line.add("Primary Supplier");
		// UPC
		line.add("UPC");
		if (siteConfig.get("ETILIZE").getValue().trim().length() > 0) {
//			line.add("UPC");
			line.add("EtilizeID");
		}
		if (search.isWithProductAndImageUrl()) {
			line.add("Product URL");
		}
		// Product Name
		if (search.isWithProductName()) {
			line.add(getMessageSourceAccessor().getMessage("productName"));
			if (search.isWithI18n() && (!(gI18N.isEmpty()))) { // i18n
				for (Language language : gI18N) {
					line.add("Name_" + language.getLanguageCode().toUpperCase());
				}
			}
		}
		line.add(getMessageSourceAccessor().getMessage("active"));
		if (gPRODUCT_REVIEW || PRODUCT_RATE) {
			line.add(getMessageSourceAccessor().getMessage("productReview"));
		}

			line.add(getMessageSourceAccessor().getMessage("fob"));

		
		if (gASI.equals("API") || gASI.equals("File")) {
			line.add(getMessageSourceAccessor().getMessage("feedFreeze"));
		}
		
		line.add(getMessageSourceAccessor().getMessage("classField"));
		
		if (gMANUFACTURER) {
			line.add(getMessageSourceAccessor().getMessage("manufacturer"));
		}
		// Short Description
		line.add("ShortDescription");		
		if (search.isWithI18n() && (!(gI18N.isEmpty()))) { // i18n
			for(Language language: gI18N) {
				line.add(language.getLanguageCode().toUpperCase());	
			}
		}
		// Short/Long Description can not be added on CSV format
		
		if (search.isWithProductAndImageUrl()) {
			line.add("Small Image URL");
			line.add("Large Image URL");
		}
		
		// Images
		for (int i=0; i<gPRODUCT_IMAGES; i++) {
			line.add("Image");
		}	
		
		// Tab can not be added on CSV format
		
		// image layout
		line.add("ImageLayout");
		// image folder
		line.add("ImageFolder");
		// Layout
		line.add("ProductLayout");
		// weight
		line.add("Weight");
		// upsMaxItemsInPackage
		line.add("UpsMaxItemsInPackage");
		// uspsMaxItemsInPackage
		line.add("UspsMaxItemsInPackage");
		// dimension
		line.add("PackageLength");
		line.add("PackageWidth");
		line.add("PackageHeight");
		// msrp
		line.add("MSRP");
		// SalesTag ID
		if (gSALES_PROMOTIONS) {
			line.add("SalesTagId");
		}
		// Mail In Rebate
		if (gMAIL_IN_REBATES > 0) {
			line.add("MailInRebateId");
		}
		//Price
		for (int i = 1; i <= gPRICE_TIERS; i++) {
			line.add("Price" + i);
			if (i < gPRICE_TIERS)
				line.add("QtyBreak" + i);
		}
		// price table
		for (int i = 1; i <= gPRICE_TABLE; i++) {
			line.add("PriceTable" + i);
		}
		// price case pack
		if (gPRICE_CASEPACK) {
			line.add("PriceCasePackQty");
			for (int i = 1; i <= 10; i++) {
				line.add("PriceCasePack" + i);
			}
		}
		// price quote
		if (siteConfig.get("PRODUCT_QUOTE").getValue().equals("true")) {
			line.add("PriceQuote");
		}
		// commission
		if (gAFFILIATE > 0) {
			for (int i = 1; i <= NUMBER_PRODUCT_COMMISSION_TABLE; i++) {
				line.add("CommissionTable" + i);
			}
		}
		// extra product fields
		for (ProductField productField : ((List<ProductField>) params.get("productFields"))) {
			if (productField.isEnabled() && productField.isProductExport()) {
				line.add("Field_" + productField.getId());
				if (search.isWithI18n() && (!(gI18N.isEmpty()))) { // i18n
					for (Language language : gI18N) {
						line.add("Field_" + productField.getId() + "_" + language.getLanguageCode().toUpperCase());
					}
				}
			}
		}
		
		// keywords
		line.add("Keywords");
		// login require
		line.add("LoginRequired");
		// hide price
		line.add("HidePrice");
		// hide msrp
		line.add("HideMsrp");
		// hide leftbar
		line.add("HideLeftBar");
		// hide rightbar
		line.add("HideRightBar");
		// taxable
		line.add("Taxable");
		// searchable
		line.add("Searchable");
		// custom shipping enabled
		if (gSHOPPING_CART)
			line.add("CustomShippingEnabled");
		// option code
		line.add("OptionCode");
		// cross items
		if (gPRICE_TIERS > 1)
			line.add("CrossItemsCode");
		// also consider
		if (gALSO_CONSIDER)
			line.add("AlsoConsider");
		if (gRECOMMENDED_LIST && search.isWithRecommendedList()) {
			line.add("RecommendedList");
			line.add("RecommendedListTitle");
			line.add("RecommendedListDisplay");
		}
		// protected level
		if (gPROTECTED > 0)
			line.add("ProtectedLevel");
		// packing
		line.add("Packing");

		// case content
		if (gCASE_CONTENT) {
			line.add("CaseContent");
		}
		// minimum / incremental
		if (gMINIMUM_INCREMENTAL_QTY) {
			line.add("MinimumQty");
			line.add("IncrementalQty");
		}
		// add to presentation
		if(gPRESENTATION)
			line.add("AddToPresentation");			
		// add to List
		if (gMYLIST)
			line.add("AddToList");

		// product comparison
		if (gCOMPARISON)
			line.add("Compare");
		// inventory
		if (gINVENTORY && siteConfig.get("INVENTORY_ON_PRODUCT_IMPORT_EXPORT").getValue().equals("true")) {
			line.add("OnHand");
			line.add("AvailableForSale");
			line.add("AllowNegativeInventory");
			line.add("ShowNegativeInventory");
			line.add("LowInventoryAlert");
		}
		// loyalty point
		if (gLOYALTY)
			line.add("LoyaltyPoint");
		// custom lines
		if (gCUSTOM_LINES) {
			line.add("NumberOfCustomLine");
			line.add("CustomLineCharacter");
		}

		// subscription
		if (gSUBSCRIPTION) {
			line.add("AutoShip");
			line.add("AutoShipDiscount");
		}
		// sitemap
		if (siteConfig.get("SITEMAP").getValue().equals("true")) {
			line.add("SitemapPriority");
		} 
	
		// category associations
		for (int i=0; i< numOfCategories; i++) {
			line.add("Category");
		}
		
		line.add("");
		writer.writeNext(line.toArray(new String[line.size()]));
		
		
		

		iter = productList.iterator();
		Class<Product> c = Product.class;
		Method m = null;
		HSSFCell cell = null;
		Object arglist[] = null;
		
		// create the rows
		for (int row = 2; iter.hasNext(); row++) {
			Product product = (Product) iter.next();
			line = new ArrayList<String>();
			// Id
			if (search.isWithProductId()) {
				line.add(product.getId().toString());
			}
			//sku
			line.add(product.getSku());
			// date created
			line.add((product.getCreated() == null) ? "" :product.getCreated().toString());
			// last modified
			line.add((product.getLastModified() == null) ? "" :product.getLastModified().toString());
			// created by
			line.add((product.getCreatedBy() == null) ? "" :product.getCreatedBy().toString());
			// last modified by
			line.add((product.getLastModifiedBy() == null) ? "" :product.getLastModifiedBy().toString());

			// parent sku
			if (gMASTER_SKU) {
				line.add((product.getMasterSku() == null) ? "" : product.getMasterSku().toString());
			}
			// primary supplier
			line.add(product.getDefaultSupplierId() != 0 ? (supplierNameMap.get((long) product.getDefaultSupplierId())) : "");
			
			// UPC
			line.add((product.getUpc() == null) ? "" : product.getUpc().toString());
			// Etilize
			if (siteConfig.get("ETILIZE").getValue().trim().length() > 0) {
//				line.add((product.getUpc() == null) ? "" : product.getUpc().toString());
				line.add((product.getEtilizeId() == null) ? "" : product.getEtilizeId().toString());
			}
			//product URL
			if (search.isWithProductAndImageUrl()) {
				String productLink = "product.jhtm?id=" + product.getId() + "&"; 
				if (gMOD_REWRITE != null && gMOD_REWRITE.equals("1") && product.getSku() != null && !product.getSku().contains("/")) {
					productLink = siteConfig.get("MOD_REWRITE_PRODUCT").getValue()+ "/"+ product.getEncodedSku()+ "/"+ product.getEncodedName() + ".html?";
				}
				line.add(siteConfig.get("SITE_URL").getValue() + productLink + "tc=replacethistrackcode");
			}
			// product Name
			if (search.isWithProductName()) {
				line.add(product.getName());
				if (search.isWithI18n() && (!(gI18N.isEmpty()))) {					
					for (Language language : gI18N) {
						if ( !product.getI18nProduct().isEmpty() && product.getI18nProduct().containsKey(language.getLanguageCode())) {
							line.add((product.getI18nProduct().get(language.getLanguageCode()).getI18nName() == null)? "" : product.getI18nProduct().get(language.getLanguageCode()).getI18nName());
						} else {
							line.add("");
						}
					}
				}					
			} 
			
			// active
			line.add(product.isActive() ? "1" : null);
			//product review
			if (gPRODUCT_REVIEW || PRODUCT_RATE) {
				line.add(product.isEnableRate() ? "1" : null);
			}
			//asi
			if (gASI.equals("API") || gASI.equals("File")) {
				line.add(product.isFeedFreeze() ? "1" : null);
			}
			// Manufacture
			if (gMANUFACTURER) {
				line.add((product.getManufactureName() == null) ? "" : product.getManufactureName().toString());
			}
			// Short		
			line.add((product.getShortDesc() == null) ? "" : product.getShortDesc());
			
			if (search.isWithI18n() && (!(gI18N.isEmpty()))) {
				for (Language language : gI18N) {
					if (!product.getI18nProduct().isEmpty() && product.getI18nProduct().containsKey(language.getLanguageCode())) {
						if (search.isWithProductName()) {
							line.add((product.getI18nProduct().get(language.getLanguageCode()).getI18nShortDesc() == null) ? "" : product.getI18nProduct().get(language.getLanguageCode()).getI18nShortDesc());
						} 
					} else {
						line.add("");
					}						
				}
			}			
			
			//Long Description can not be added on CSV format	
			
			// Images
    		int index = 0;
  		    if (product.getImages() != null) {  		    	
  	    		for (ProductImage image: product.getImages()) {
  	    			if (index == 0 && search.isWithProductAndImageUrl()) {
  	    				if (image.getImageUrl().contains("//") || image.getImageUrl().startsWith("/")) {
  	    					line.add(image.getImageUrl());
  	    					line.add(image.getImageUrl());
  	    				} else {
  	    					line.add(siteConfig.get("SITE_URL").getValue() + "assets/Image/Product/thumb/" + image.getImageUrl());
  	    					line.add(siteConfig.get("SITE_URL").getValue() + "assets/Image/Product/detailsbig/" + image.getImageUrl());
  	    				}
  	    			}
  	    			line.add(image.getImageUrl());
  	    			index++;
  	    		}
  	    		// small & large image url
  	    		if (search.isWithProductAndImageUrl() && !(product.getImages().size() > 0)) {
  	    			line.add("");
  	    			line.add("");
  	    		}
  	    		// images
  	    		if(product.getImages().size() < gPRODUCT_IMAGES) {
	  	    		for (int i = product.getImages().size(); i < gPRODUCT_IMAGES ; i++) {
  	  	    				line.add("");
  	    			}
  	    		}
  		    }		
						
			// Tab can not be added on CSV format
			
			//Image Layout
			line.add((product.getImageLayout() == null) ? "" : product.getImageLayout());
			
			//Image Folder
			line.add((product.getImageFolder() == null) ? "" : product.getImageFolder());
			
			// Layout
			if(product.getProductLayout() == null || product.getProductLayout() == "") {
				line.add("");
			} else {
				String layout = null;
				for(KeyBean productLayout : Constants.PRODUCT_LAYOUT) {
		  			if(product.getProductLayout().toString().equalsIgnoreCase(productLayout.getName().toString())) {
		  				layout = productLayout.getValue().toString();
		  				break;
		  			}
				}
				line.add(layout);
			}
	  		
			
			// Weight
			line.add(product.getWeight().toString());

			// upsMaxItemsInPackage
			line.add((product.getUpsMaxItemsInPackage() == null) ? "" : product.getUpsMaxItemsInPackage().toString());
			// uspsMaxItemsInPackage
			line.add((product.getUspsMaxItemsInPackage() == null) ? "" : product.getUspsMaxItemsInPackage().toString());

			// dimension
			line.add((product.getPackageL() == null) ? "" : product.getPackageL().toString());
			// Package weight
			line.add((product.getPackageW() == null) ? "" : product.getPackageW().toString());
			// Package height
			line.add((product.getPackageH() == null) ? "" : product.getPackageH().toString());

			// set msrp
			line.add((product.getMsrp() == null) ? "" : product.getMsrp().toString());

			// SalesTag ID
			if (gSALES_PROMOTIONS) {
				line.add((product.getSalesTagId() == null) ? "" : product.getSalesTagId().toString());
			}

			// Mail In Rebate
			if (gMAIL_IN_REBATES > 0) {
				line.add((product.getRebateId() == null) ? "" : product.getRebateId().toString());
			}
			// set price & price break
			for (int i = 1; i <= gPRICE_TIERS; i++) {			
				m = c.getMethod("getPrice" + i);
				Double price = (Double) m.invoke(product, arglist);
				line.add((price == null) ? "" : Double.toString(price.doubleValue()));
				if (i < gPRICE_TIERS) {
					m = c.getMethod("getQtyBreak" + i);
					Integer qtyBreak = (Integer) m.invoke(product, arglist);
					line.add((qtyBreak == null) ? "" : Double.toString(qtyBreak.doubleValue()));
				}
			}			

			// price table
			for (int i = 1; i <= gPRICE_TABLE; i++) {
				m = c.getMethod("getPriceTable" + i);
				Double priceTable = (Double) m.invoke(product, arglist);
				line.add((priceTable == null) ? "" : Double.toString(priceTable.doubleValue()));
			}

			// price case pack
			if (gPRICE_CASEPACK) {
				line.add((product.getPriceCasePackQty() == null) ? null: product.getPriceCasePackQty().toString());
				for (int i = 1; i <= 10; i++) {
					m = c.getMethod("getPriceCasePack" + i);
					Double priceCasePack = (Double) m.invoke(product, arglist);
					line.add((priceCasePack == null) ? "" : Double.toString(priceCasePack.doubleValue()));
				}
			}

			// price quote
			if (siteConfig.get("PRODUCT_QUOTE").getValue().equals("true")) {
				line.add(product.isQuote() ? "1" : null);
			}

			// commission
			if (gAFFILIATE > 0 && product.getCommissionTables() != null) {
				Class<ProductAffiliateCommission> commission = ProductAffiliateCommission.class;
				for (int i = 1; i <= NUMBER_PRODUCT_COMMISSION_TABLE; i++) {
					m = commission.getMethod("getCommissionTable" + i);
					Double commissionTable = (Double) m.invoke(product.getCommissionTables(), arglist);
					line.add((commissionTable == null) ? "" : Double.toString(commissionTable.doubleValue()));
				}
			}
			//Set extra product fields
			for (ProductField productField : ((List<ProductField>) params.get("productFields"))) {
				if (productField.isEnabled() && productField.isProductExport()) {
					m = c.getMethod("getField" + productField.getId());
					line.add((String) m.invoke(product,arglist));
					if (search.isWithI18n() && (!(gI18N.isEmpty()))) { // i18n
						for (Language language : gI18N) {
							if (!product.getI18nProduct().isEmpty() && productField.isEnabled() && productField.isProductExport()) {
								m = c.getMethod("getField"+ productField.getId());		
								line.add((product.getI18nProduct().get(language.getLanguageCode()) == null)? "" : (String) m.invoke(product.getI18nProduct().get(language.getLanguageCode()),arglist));								
							} else {
								line.add("");
							}
						}						
					} 
				}
			}
			
			// keywords
			line.add(product.getKeywords());

			// login require
			line.add(product.isLoginRequire() ? "1" : null);

			// hide price
			line.add(product.isHidePrice() ? "1" : null);

			// hide msrp
			line.add(product.isHideMsrp() ? "1" : null);

			// hide left bar
			line.add(product.isHideLeftBar() ? "1" : null);

			// hide right bar
			line.add(product.isHideRightBar() ? "1" : null);

			// taxable
			line.add(product.isTaxable() ? "1" : null);

			// searchable
			line.add(product.isSearchable() ? "1" : null);

			// custom shipping enabled
			if (gSHOPPING_CART) {
				line.add(product.isCustomShippingEnabled() ? "1" : null);
			}

			// option code
			line.add(product.getOptionCode());

			// cross items
			if (gPRICE_TIERS > 1) {
				line.add(product.getCrossItemsCode());
			}

			// also consider
			if (gALSO_CONSIDER) {
				line.add(product.getAlsoConsider());
			}

			if (gRECOMMENDED_LIST && search.isWithRecommendedList()) {
				line.add(product.getRecommendedList());
				line.add(product.getRecommendedListTitle());
				line.add(product.getRecommendedListDisplay());
			}

			// protected level
			if (gPROTECTED > 0) {
				line.add(product.getProtectedLevelAsNumber().toString());
			}

			// packing
			line.add(product.getPacking());
			// case content
			if (gCASE_CONTENT) {
				line.add((product.getCaseContent() == null) ? null : product.getCaseContent().toString());
			}

			// minimum / incremental
			if (gMINIMUM_INCREMENTAL_QTY) {
				line.add((product.getMinimumQty() == null) ? null : product.getMinimumQty().toString());
				line.add((product.getIncrementalQty() == null) ? null : product.getIncrementalQty().toString());
			}
			
			// add to presentation
			if(gPRESENTATION) {
				line.add(product.isAddToPresentation() ? "1" : null);
			}

			// addToList
			if (gMYLIST) {
				line.add(product.isAddToList() ? "1" : null);
			}

			// product comparison
			if (gCOMPARISON) {
				line.add(product.isCompare() ? "1" : null);
			}

			// set inventory
			if (gINVENTORY && siteConfig.get("INVENTORY_ON_PRODUCT_IMPORT_EXPORT").getValue().equals("true")) {
				
				// set inventory
				line.add((product.getInventory() == null) ? "" :product.getInventory().toString());				
				// inventory on hand
				line.add((product.getInventoryAFS() == null) ? "" :product.getInventoryAFS().toString());
								
				line.add(product.isNegInventory() ? "1" : null);
				line.add(product.isShowNegInventory() ? "1" : null);

				// set low inventory
				line.add((product.getLowInventory() == null) ? "" :product.getLowInventory().toString());
			}

			// loyalty point
			if (gLOYALTY) {
				line.add((product.getLoyaltyPoint() == null) ? null : product.getLoyaltyPoint().toString());
			}

			// custom lines
			if (gCUSTOM_LINES) {
				if(product.getNumCustomLines() != null) {
					line.add(product.getNumCustomLines().toString());	
					line.add((product.getCustomLineCharacter() == null) ? "" : product.getCustomLineCharacter().toString());
				} else {
					line.add("");
				}

				
			}
			// subscription
			if (gSUBSCRIPTION) {
				line.add(product.isSubscription() ? "1" : null);
				line.add((product.getSubscriptionDiscount() == null) ? "" :product.getSubscriptionDiscount().toString());
			}
			//site map
			if (siteConfig.get("SITEMAP").getValue().equals("true")) {
				line.add((product.getSiteMapPriority() != null ? product.getSiteMapPriority().toString() : "0.5"));
			}			
			
			// category associations
			Iterator catIdIter = product.getCatIds().iterator();
			index = 0;
			while (catIdIter.hasNext()) {
				line.add(catIdIter.next().toString());
			}
			
			line.add("");
			writer.writeNext(line.toArray(new String[line.size()]));
		}
		
		writer.close();
		HashMap<String, Object> fileMap = new HashMap<String, Object>();
		fileMap.put("file", exportFile);
		exportedFiles.add(fileMap);
	}
	private List<Product> getEtilizeProducts(String appId, SearchCriteria searchCriteria, boolean debug) {
		
		// skus
		List<Product> skuList = new ArrayList<Product>();
		
    	try {
    		String wsdlLocation = "http://ws.spexlive.net/service/soap/catalog?appId=" + appId + "&wsdl";
    		Catalog service = new Catalog(new URL(wsdlLocation), new QName("http://com.etilize.spexlive", "catalog"));
    		CatalogServiceIntf port = service.getCatalogHttpPort();
    		
    		Search search = new Search();
    		search.setCatalog("na");
    		search.setSiteId(0);
    		search.setPageNo(1);
    		search.setPageSize(50);
    		search.setCriteria(searchCriteria);
    		
    		// subcatalog
    		SubcatalogFilter subcatalogFilter = new SubcatalogFilter();
    		subcatalogFilter.setContent("allitems");
    		searchCriteria.setSubcatalogFilter(subcatalogFilter);
  
       		// SelectProductFields
    		SelectProductFields selectProductFields = new SelectProductFields();	
    		selectProductFields.getSkuType().add("all");
    		search.setSelectProductFields(selectProductFields);
    		
    		SearchResult searchResult = new SearchResult();
    		searchResult = port.search(search);
    		
    		int numOfPages = searchResult.getCount()/search.getPageSize();
    		if (searchResult.getCount()%search.getPageSize() > 0) {
    			numOfPages++;
    		}
    		
    		for (int pageNo=1; pageNo<=numOfPages; pageNo++) {
    			if (pageNo > 1) {
    				search.setPageNo(pageNo);
    				searchResult = port.search(search);
    				if (debug) {
        				//System.out.println(pageNo + " of " + numOfPages);    					
    				}
    			}
    			
    			for (ProductSummary productSummary: searchResult.getProducts().getProductSummary()) {
        			
        			if (productSummary.getSkus() != null) {
                		for (Sku sku: productSummary.getSkus().getSku()) {

                			if (sku.getType().equalsIgnoreCase("Ingram Micro USA")) {
                				Product thisSku = new Product();
                				thisSku.setSku("IM-" + sku.getNumber());
                				thisSku.setFeed("INGRAM");
                				skuList.add(thisSku);
                			} else if (sku.getType().equalsIgnoreCase("Synnex")) {
                				Product thisSku = new Product();
                				thisSku.setSku("SYN-" + sku.getNumber());
                				thisSku.setFeed("SYNNEX");
                				skuList.add(thisSku);
                			} else if (sku.getType().equalsIgnoreCase("Tech Data")) {
                				Product thisSku = new Product();
                				thisSku.setSku("TD-" + sku.getNumber());
                				thisSku.setFeed("TECHDATA");
                				skuList.add(thisSku);
                			} else if (sku.getType().equalsIgnoreCase("DSI")) {
                				Product thisSku = new Product();
                				thisSku.setSku("DSI-" + sku.getNumber());
                				thisSku.setFeed("DSI");
                				skuList.add(thisSku);
                			}
                		}        				
        			}
        		}
    		}
    		
    	} catch (Exception e) {
    		if (debug) {
        		e.printStackTrace();   			
    		}
    	} 
    	return skuList;
	}
	private void notifyAdmin(Map<String, Configuration> siteConfig, HttpServletRequest request){
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	StringBuffer message = new StringBuffer();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
    	message.append("user "+accessUser.getUsername()+" has exported product file");
    	SimpleMailMessage msg = new SimpleMailMessage();			
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Automated Export on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}
