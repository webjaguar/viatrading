/* Copyright 2005, 2008 Advanced E-Media Solutions
 * author: Shahin Naji
 */

package com.webjaguar.web.admin.product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductSearch;


public class ShowSkusController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
		
		Map<String, Object> myModel = new HashMap<String, Object>();	

		if (request.getParameter( "value" ) != null) {
			ProductSearch productSearch = new ProductSearch();
			productSearch.setSku(request.getParameter( "value" ));
			Integer supplierId = null;
			try {
				supplierId = Integer.parseInt(request.getParameter( "supplierId" ));
			} catch( Exception e) {}
			productSearch.setDefaultSupplierId(supplierId);
			if(request.getParameter( "inactive" )==null){
			   productSearch.setActive("1");
			}
			
			List<Product> products = this.webJaguar.getProductAjax( productSearch );
			myModel.put("products", products);
		}
		
        return new ModelAndView("admin/orders/showSkus", "model", myModel);
	}
	
}
