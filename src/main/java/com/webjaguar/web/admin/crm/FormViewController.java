package com.webjaguar.web.admin.crm;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.crm.CrmForm;
import com.webjaguar.web.form.crm.CrmFormBuilderForm;

public class FormViewController implements Controller{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private String type;
	public void setType(String type) { this.type = type; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		CrmForm crmForm = this.webJaguar.getCrmFormById(ServletRequestUtils.getIntParameter(request, "id"));
		CrmFormBuilderForm crmFormBuilderForm = new CrmFormBuilderForm();
		crmFormBuilderForm.setNewCrmForm(false);
		crmFormBuilderForm.setCrmForm(crmForm);
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		String form =  this.webJaguar.createForm(crmFormBuilderForm, request);
		myModel.put("form", form);
		
		if(this.type.equalsIgnoreCase("code")){
			String htmlCode = form.replaceAll("<", "&lt;");
			htmlCode = htmlCode.replaceAll(">", "&gt;") ;
			myModel.put("htmlCode", htmlCode);
		}
		return new ModelAndView("admin/crm/form/view", "model", myModel);
	}

}
