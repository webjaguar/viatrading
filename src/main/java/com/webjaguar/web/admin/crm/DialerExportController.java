package com.webjaguar.web.admin.crm;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactField;
import com.webjaguar.model.crm.CrmContactGroup;
import com.webjaguar.model.crm.CrmContactSearch;
import com.webjaguar.web.domain.Constants;

public class DialerExportController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private MailSender mailSender;
    public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	private File baseFile;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		CrmContactSearch search = this.webJaguar.getCrmContactSearch( request );
		search.setIsDialingExport(true);
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		search.setFileType(ServletRequestUtils.getStringParameter(request, "fileType", "xls"));
		baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			map.put("message", "excelfile.notWritable");
			return new ModelAndView("admin/crm/contact/export", map);			
		}
		
		File file[] = baseFile.listFiles();
		List<Map> exportedFiles = new ArrayList<Map>();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith("crmDialerExport") & file[f].getName().endsWith(".csv")) {
				if (request.getParameter("__new") != null) { // check if new button was pressed
					file[f].delete();
				} else {
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					exportedFiles.add( fileMap );				
				}
			}
		}
		map.put("exportedFiles", exportedFiles);		
		
		System.out.println("dialer crm ");
		
		// check if new button was pressed
		if (request.getParameter("__new") != null) {
			
			int count = this.webJaguar.getCrmContactListCount(search);
			createCsvFile(count, search, exportedFiles, request);		
	       
			if (count > 0) {
				map.put("arguments", count);
				map.put("message", "crmcontactexport.success");
				this.webJaguar.insertImportExportHistory( new ImportExportHistory("crmcontact", SecurityContextHolder.getContext().getAuthentication().getName(), false ));
				notifyAdmin(siteConfig, request);
			} else {
				map.put("message", "crmcontactexport.empty");				
			}
		} // if 
		search.setIsDialingExport(null);		
		return new ModelAndView("admin/crm/contact/export", map);
	}
	
	private void createCsvFile(int orderCount, CrmContactSearch search,List<Map> exportedFiles, HttpServletRequest request) throws Exception { 
    	
    	search.setLimit(null);
    	List<CrmContact> contacts = this.webJaguar.getCrmContactExport(search);
    	if (orderCount < 1) return;
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm:ss a");
		AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
		Integer groupId = search.getGroupId();
		System.out.println("id "+search.getGroupId());
		String fileName = "/crmDialerExport_" + accessUser.getUsername() + "_" + dateFormatter.format(new Date()) + ".csv";
		if(groupId != null && groupId != -1){
			CrmContactGroup group = this.webJaguar.getContactGroupById( groupId );
			if(group != null && group.getName() != null && group.getName() != ""){
				fileName = "/crmDialerExport_" + accessUser.getUsername() + "_"+ group.getName() + "_" + dateFormatter.format(new Date()) + ".csv";
			}		
		}
    	
    	File exportFile = new File(baseFile, fileName);
    	CSVWriter writer = new CSVWriter(new FileWriter(exportFile));
    	List<String> line = new ArrayList<String>();
    	
    	dateFormatter = new SimpleDateFormat("MM/dd/yy kk:mm");
    	//header name
    	
    	line.add("accountname");
    	line.add("firstname");
    	line.add("lastname");
    	line.add("crmContactId");
    	line.add("customerId");
    	line.add("title");
    	line.add("email1");
    	line.add("email2");
    	line.add("phone1");
    	line.add("phone2");
    	line.add("Street");
    	line.add("City");
    	line.add("State");
    	line.add("Zip");
    	line.add("Country");
    	line.add("CreatedDate");
    	line.add("AssignedTo");
    	
		writer.writeNext(line.toArray(new String[line.size()]));
    	
		line = new ArrayList<String>();
		
		List<String> list = Arrays.asList("","","","","","","","","","","","","","","","","");
    	line.addAll( list );
    	// customer fields
    	writer.writeNext(line.toArray(new String[line.size()]));
    	
    	// create the rows
		List<AccessUser> users = this.webJaguar.getUserPrivilegeList(null);
		Map<Integer, String> userMap = new HashMap<Integer, String>();
		for(AccessUser user: users){
			userMap.put(user.getId(), user.getUsername());
		}
		for (CrmContact contact: contacts) { 
    		line = new ArrayList<String>();
    		
    		line.add(  contact.getAccountName().toString() );
    		line.add( (contact.getFirstName() == null) ? "" : contact.getFirstName().toString() );
    		line.add( (contact.getLastName() == null) ? "" : contact.getLastName().toString() );
    		line.add(  contact.getId().toString() );
    		line.add( (contact.getUserId() == null) ? null : contact.getUserId().toString() );
    		line.add( (contact.getTitle() == null) ? "" : contact.getTitle().toString() );
    		line.add( (contact.getEmail1() == null) ? "" : contact.getEmail1().toString() );
    		line.add( (contact.getEmail2() == null) ? "" : contact.getEmail2().toString() );

	    	line.add( (contact.getPhone1() == null) ? "" : contact.getPhone1().toString() );
    		line.add( (contact.getPhone2() == null) ? "" : contact.getPhone2().toString() );
	    	line.add( (contact.getStreet() == null) ? "" : contact.getStreet().toString() );
    		line.add( (contact.getCity() == null) ? "" : contact.getCity().toString() );
    		line.add( (contact.getStateProvince() == null) ? "" : contact.getStateProvince().toString() );
    		line.add(  (contact.getZip() == null) ? "" : contact.getZip().toString() ); 			
    		line.add(  (contact.getCountry() == null) ? "" : contact.getCountry().toString() );
    		
        	line.add(sdfDate.format( contact.getCreated() ));
        	line.add( (contact.getAccessUserId() == null) ? "" : contact.getAccessUserId().toString() );
        	
        	writer.writeNext(line.toArray(new String[line.size()]));
    		
		}
    	
    	writer.close();
    	HashMap<String, Object> fileMap = new HashMap<String, Object>();
        fileMap.put("file", exportFile);
		exportedFiles.add( fileMap );
    	
    }
	
	
	private void notifyAdmin(Map<String, Configuration> siteConfig, HttpServletRequest request){
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	StringBuffer message = new StringBuffer();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
    	message.append("user "+accessUser.getUsername()+" has exported Dialing CrmContacts file");
    	SimpleMailMessage msg = new SimpleMailMessage();			
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Automated Export on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}

}
