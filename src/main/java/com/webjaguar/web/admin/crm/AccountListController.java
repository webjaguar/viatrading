/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.01.2010
 */

package com.webjaguar.web.admin.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.crm.CrmAccount;
import com.webjaguar.model.crm.CrmAccountSearch;


public class AccountListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
		CrmAccountSearch crmAccountSearch = getCrmAccountSearch( request );
		Map<String, Object> myModel = new HashMap<String, Object>();
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("crmAccountForm.jhtm"));
		}
		Map<Integer, AccessUser> accessUserMap = new HashMap<Integer, AccessUser>();
		
		AccessUser accessUser = this.webJaguar.getAccessUser(request);
		myModel.put("accessUser", accessUser);
		
		List<AccessUser> accessUserList = this.webJaguar.getUserPrivilegeList(null);
		for(AccessUser accUser : accessUserList) {
			accessUserMap.put(accUser.getId(), accUser);
		}
		myModel.put("accessUserMap", accessUserMap);
		
		// get sales rep
		List<SalesRep> salesReps = null;
		
		Integer salesRepId = null;
		if (accessUser.isSalesRepFilter()) {
			salesRepId = accessUser.getSalesRepId();
		}

		if (salesRepId == null) {
			salesReps = this.webJaguar.getSalesRepList();				
		} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
			salesReps = this.webJaguar.getSalesRepList(salesRepId);
		} else {
			salesReps = new ArrayList<SalesRep>();
			salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
		}
		
		if (salesRepId != null) {
			// associate subaccount
			for (SalesRep sRep: salesReps) {
				if (sRep.getParent() != null && sRep.getParent().equals(salesRepId)) {
					AccessUser accUser = this.webJaguar.getAccessUserBySalesRepId(sRep.getId());
					if(accUser != null) {
						accessUser.getSubAccounts().add(accUser);
					}
				}
			}
			myModel.put("accessUserTree", accessUser);
		}	
		
		int count = this.webJaguar.getCrmAccountListCount(crmAccountSearch);
		if (count < crmAccountSearch.getOffset()-1) {
			crmAccountSearch.setPage(1);
		}
		crmAccountSearch.setLimit(crmAccountSearch.getPageSize());
		crmAccountSearch.setOffset((crmAccountSearch.getPage()-1)*crmAccountSearch.getPageSize());
		List<CrmAccount> crmAccountList = this.webJaguar.getCrmAccountList(crmAccountSearch);
		myModel.put("crmAccountList", crmAccountList);
		
		int pageCount = count/crmAccountSearch.getPageSize();
		if (count%crmAccountSearch.getPageSize() > 0) {
			pageCount++;
		}
		
	//	myModel.put("accounts", this.webJaguar.getCrmAccountNameList());
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", crmAccountSearch.getOffset()+crmAccountList.size());
		myModel.put("count", count);

		//Go to New Admin Layout
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/crm/account/list", "model", myModel);
		}
		
		return new ModelAndView("admin/crm/account/list", "model", myModel);
	}
	
	private CrmAccountSearch getCrmAccountSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		CrmAccountSearch crmAccountSearch = (CrmAccountSearch) request.getSession().getAttribute( "crmAccountSearch" );
		if (crmAccountSearch == null) {
			crmAccountSearch = new CrmAccountSearch();
			if ( request.getAttribute( "accessUser" ) != null ) {
				AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
				crmAccountSearch.setAccessUserId(accessUser.getId());
			}
			crmAccountSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "crmAccountSearch", crmAccountSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			crmAccountSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "id" ));
		}

		 // account name
		if (request.getParameter("crmAccountName") != null) {
			crmAccountSearch.setAccountName( ServletRequestUtils.getStringParameter( request, "crmAccountName", "" ) );
		}
		
		// account Id
		if (request.getParameter("crmAccountId") != null) {
			int accountId = ServletRequestUtils.getIntParameter( request, "crmAccountId", -1 );
			if (accountId == -1) {
				crmAccountSearch.setAccountId(null);	
			} else {
				crmAccountSearch.setAccountId(accountId);	
			}
		}
		
		// access user
		if (request.getParameter("accessUserId") != null) {
			int accessUserId = ServletRequestUtils.getIntParameter( request, "accessUserId", -1 );		
			if (accessUserId == -1) {
				crmAccountSearch.setAccessUserId( null );			
			} else {
				crmAccountSearch.setAccessUserId( accessUserId );
			}		
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				crmAccountSearch.setPage( 1 );
			} else {
				crmAccountSearch.setPage( page );				
			}
		}			
		
		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				crmAccountSearch.setPageSize( 10 );
			} else {
				crmAccountSearch.setPageSize( size );				
			}
		}
		
		return crmAccountSearch;
	}	
}