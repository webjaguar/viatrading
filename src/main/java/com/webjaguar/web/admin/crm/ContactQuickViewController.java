/*
 * Copyright 2005, 2010 Advanced E-Media Solutions
 */

package com.webjaguar.web.admin.crm;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmTask;
import com.webjaguar.model.crm.CrmTaskSearch;

public class ContactQuickViewController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar( WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		
		if ((Boolean) gSiteConfig.get("gACCESS_PRIVILEGE")) {
			// get access user
			Map <Integer, AccessUser> accessUserMap = new HashMap<Integer, AccessUser>();
			Iterator<AccessUser> iter = this.webJaguar.getUserPrivilegeList(null).iterator();
			while (iter.hasNext()) {
				AccessUser accessUser = iter.next();
				accessUserMap.put(accessUser.getId(), accessUser);
			}
			model.put("accessUserMap", accessUserMap);
		}
		model.put( "current", new Date() );
		
		// get contact
		Integer contactId = ServletRequestUtils.getIntParameter( request, "id", 0 );
		CrmContact contact = this.webJaguar.getCrmContactById( contactId );
		model.put( "contact", contact );
		
		if(contact == null) {
			return new ModelAndView( "admin/crm/contact/quickView", "model", model );
		}
		// get task history
		CrmTaskSearch search = new CrmTaskSearch();
		search.setAccountId(contact.getAccountId());
		search.setSort("due_date desc");
		search.setLimit(100);
		
		// all tasks for account
		search.setContactId(-1);
		List<CrmTask> accountTaskList = this.webJaguar.getCrmTaskList(search);
		model.put( "accountTaskList", accountTaskList );
		
		// all task for contact
		search.setContactId(contactId);
		List<CrmTask> contactTaskList = this.webJaguar.getCrmTaskList(search);
		
		model.put( "contactTaskList", contactTaskList );
		
		
		return new ModelAndView( "admin/crm/contact/quickView", "model", model );
	}
}