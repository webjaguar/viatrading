/* Copyright 2008 Advanced E-Media Solutions
	 * @author Shahin Naji
	 * @since 03.10.2010
	 */

package com.webjaguar.web.admin.crm;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.crm.CrmAccount;

public class AccountImportController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
		
	private HSSFFormulaEvaluator evaluator;
	
	String formView = "admin/crm/account/import";
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {	
        
		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			return new ModelAndView("admin/crm/account/import", "message", "excelfile.notWritable");	
		}    	
    	
		// check if upload button was pressed
		if (request.getParameter("__upload") != null) {
	    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	    	MultipartFile file = multipartRequest.getFile("file");
	    	if (file != null && !file.isEmpty()) {
	    		File excel_file = new File(baseFile, "crm_account_import.xls");
	    		file.transferTo(excel_file);
	    		file = null; // delete to save resources
		    	return processFile(excel_file, request);	    			
	    	} else {
	        	return new ModelAndView(formView, "message", "excelfile.invalid");	    		
	    	}
		}
    	
    	return new ModelAndView(formView);
    }
    
    private ModelAndView processFile(File excel_file, HttpServletRequest request) {
        List<Map> addedItems = new ArrayList<Map>();
        List<Map> invalidItems = new ArrayList<Map>();
        Set<String> allItems = new HashSet<String>();
        
		List<CrmAccount> importedCrmAccounts = new ArrayList<CrmAccount>();	
		List<CrmAccount> updatedCrmAccounts = new ArrayList<CrmAccount>();	
    	
		Map<String, Object> map = new HashMap<String, Object>();
		
		try {
			POIFSFileSystem fs =
	            new POIFSFileSystem(new FileInputStream(excel_file));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
		    
			HSSFSheet sheet = wb.getSheetAt(0);       	// first sheet
			HSSFRow row     = sheet.getRow(0);        	// first row
			
			evaluator = new HSSFFormulaEvaluator(sheet, wb);
			
			Map<String, Short> header = new HashMap<String, Short>();
			
			if (wb.getNumberOfSheets() > 1) {
				// multiple sheets
				map.put("message", "excelfile.multipleSheets");
				return new ModelAndView(formView, map);					
			}
			if (row == null) {
				// first line should be the headers
				map.put("message", "excelfile.emptyHeaders");
				return new ModelAndView(formView, map);					
			}
			
		    Iterator iter = row.cellIterator();
		    
		    // get header names
			while (iter.hasNext()) {
				HSSFCell cell = (HSSFCell) iter.next();
				String headerName = "";
				try {
					headerName = cell.getStringCellValue().trim();					
				} catch (NumberFormatException e) {
					// do nothing
				}
				if (!headerName.equals( "" )) {
					if (header.put(headerName.toLowerCase(), cell.getCellNum()) != null) {
						// duplicate header
						map.put("message", "excelfile.duplicateHeaders");
						map.put("arguments", headerName);
						return new ModelAndView(formView, map);								
					}				
				} 
			}
			
			// check required headers
			if (!header.containsKey("accountname")) { // account name is required
				return new ModelAndView(formView, "message", "excelfile.missingHeaders");
			}
			
			nextRow: for (int i=1; i<=sheet.getLastRowNum(); i++) {
				row = sheet.getRow(i);
				if (row == null) {
					continue;
				}
				
				String accountName = null;
				accountName = getStringValue(row, header.get("accountname")).trim();
				
				if (accountName == null || accountName.isEmpty()) {
					continue nextRow;
				}
				Map<String, Object> thisMap = new HashMap<String, Object>();
				thisMap.put("accountName", accountName);
				
				//boolean accountExist = this.webJaguar.isCrmAccountExist(accountName);
				if (allItems.contains(accountName)) { // account name exist
					thisMap.put("rowNum", row.getRowNum()+1);
					thisMap.put("reason", "Dupliacte Account in Excel");
					invalidItems.add(thisMap);
					continue nextRow;
				} else {
					allItems.add(accountName);
				}
				
				if (invalidItems.size() == 0) { // no errors
					// save or update contact
					CrmAccount crmAccount = this.webJaguar.getCrmAccountByName(accountName);
					if(crmAccount == null){
						crmAccount = new CrmAccount();
						crmAccount.setAccountName(accountName);
						importedCrmAccounts.add(crmAccount);
					} else {
						updatedCrmAccounts.add(crmAccount);
					}
					processRow(row, header, crmAccount, thisMap);					
					addedItems.add(thisMap);

				} // if no errors
			}					
		} catch (IOException e) {
			return new ModelAndView(formView, "message", "excelfile.invalid");
		} finally {
	    	// delete excel file
			excel_file.delete();	    			
		}
		
		if (invalidItems.size() > 0) { // has errors
			map.put("invalidItems", invalidItems);
			return new ModelAndView(formView, map);					
		} else {
			if (importedCrmAccounts.isEmpty() && updatedCrmAccounts.isEmpty()) {	// no Accounts to import
				return new ModelAndView(formView, "message", "excelfile.empty");			
			} else {
				if(!importedCrmAccounts.isEmpty()) {
					this.webJaguar.importCrmAccounts(importedCrmAccounts);
				}
				if(!updatedCrmAccounts.isEmpty()) {
					for(CrmAccount crmAccount : updatedCrmAccounts){
						this.webJaguar.updateCrmAccount(crmAccount);
					}
				}
				this.webJaguar.insertImportExportHistory( new ImportExportHistory("crmaccount", SecurityContextHolder.getContext().getAuthentication().getName(), true ));
			}
		}
		
		map.put("addedItems", addedItems);
		return new ModelAndView("admin/crm/account/importSuccess", map);
		
    }

    private Integer getIntegerValue(HSSFRow row, short cellnum) throws Exception {
		HSSFCell cell = row.getCell(cellnum);
    	Integer value = null;
    	
    	if (cell != null) {
    		switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = new Integer(cell.getStringCellValue());										
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Double cellValue = cell.getNumericCellValue();
					if (cellValue != cellValue.intValue()) {
						throw new Exception();	
					}
					value =  cellValue.intValue();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue formulaCellValue = evaluator.evaluate(cell);					
					if (formulaCellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						cellValue =  new Double(formulaCellValue.getNumberValue());
						if (cellValue != cellValue.intValue()) {
							throw new Exception();	
						}
						value =  cellValue.intValue();
					}
					break;
			} // switch
    	}
    	return value;
    }
    
    private String getStringValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		String value = "";

    	if (cell != null) {
			switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = cell.getStringCellValue();
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Integer intValue = null;
					try {
						intValue = getIntegerValue(row, cellnum);
					} catch (Exception e) {}
					if (intValue != null) {
						value = intValue.toString();
						break;
					}
					value = new Double(cell.getNumericCellValue()).toString();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue cellValue = evaluator.evaluate(cell);
					if (cellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						value = new Double(cellValue.getNumberValue()).toString();						
					}
					break;
			} // switch		
    	}
		return value;
    }
 
    
    private void processRow(HSSFRow row, Map<String, Short> header, CrmAccount account, Map<String, Object> thisMap) {
    	NumberFormat formatter = new DecimalFormat("#");
    	StringBuffer comments = new StringBuffer();		

    	// get account name
		if (header.containsKey("accountname")) {
			account.setAccountName( getStringValue(row, header.get("accountname")) );
		}

		// get account owner
		if (header.containsKey("accountowner")) {
			account.setAccountOwner( getStringValue(row, header.get("accountowner")) );
		}

		// get user
		if (header.containsKey("assignedto") && !getStringValue(row, header.get("assignedto")).equals("")) {
			AccessUser user = this.webJaguar.getUserByUserName( getStringValue(row, header.get("assignedto")).trim() );
			account.setAccessUserId( user != null ? user.getId() : null );
		}
		
		// get type
		if (header.containsKey("type")) {
			account.setAccountType( getStringValue(row, header.get("type")) );
		}
		
		// get phone
		if (header.containsKey("phone")) {
			try {
				if(row.getCell(header.get("phone")).getNumericCellValue() != 0){
					account.setPhone( formatter.format(row.getCell(header.get("phone")).getNumericCellValue()) );
				} else{
					account.setPhone( getStringValue(row, header.get("phone")) );
				}
			} catch (Exception e) {
				account.setPhone( getStringValue(row, header.get("phone")) );
			}
		}
		
		// get fax
		if (header.containsKey("fax")) {
			account.setFax( getStringValue(row, header.get("fax")) );
		}
		
		// get description
		if (header.containsKey("description")) {
			account.setDescription( getStringValue(row, header.get("description")) );
		}
		
		// get industry
		if (header.containsKey("industry")) {
			account.setIndustry( getStringValue(row, header.get("industry")) );
		}

		
		// get employees
		if (header.containsKey("employees")) {
			try {
				Integer numEmployees = getIntegerValue(row, header.get("employees"));
				if (numEmployees != null) {
					account.setEmployees(numEmployees);
				}
			} catch (Exception e) {
				// do nothing
			}
		}

		
		// get website
		if (header.containsKey("website")) {
			account.setWebsite( getStringValue(row, header.get("website")) );
		}
		
		// get rating
		if (header.containsKey("rating")) {
			account.setRating( getStringValue(row, header.get("rating")) );
		}
		
		// get annual revenue
		if (header.containsKey("annualrevenue")) {
			String annualrevenue = getStringValue(row, header.get("annualrevenue")).trim();
			if (annualrevenue.equals("")) {
				account.setAnnualRevenue( null );
			} else {
				try {
					account.setAnnualRevenue( new Integer(annualrevenue) );
				} catch (NumberFormatException e) {
					comments.append("invalid annual revenue, ");
				}					
			}
		}
		
		// get street
		if (header.containsKey("shippingstreet")) {
			account.setShippingStreet( getStringValue(row, header.get("shippingstreet")) );
		}	
		
		// get city
		if (header.containsKey("shippingcity")) {
			account.setShippingCity( getStringValue(row, header.get("shippingcity")) );
		}		
		
		// get state/province
		if (header.containsKey("shippingstateprovince")) {
			account.setShippingStateProvince( getStringValue(row, header.get("shippingstateprovince")) );
		}			

		// get zip
		if (header.containsKey("shippingzip")) {
			account.setShippingZip( getStringValue(row, header.get("shippingzip")) );
		}
		
		// get country
		if (header.containsKey("shippingcountry")) {
			account.setShippingCountry( getStringValue(row, header.get("shippingcountry")) );
		}
		
		// get billing street
		if (header.containsKey("billingstreet")) {
			account.setBillingStreet( getStringValue(row, header.get("billingstreet")) );
		}	
		
		// get billing city
		if (header.containsKey("billingcity")) {
			account.setBillingCity( getStringValue(row, header.get("billingcity")) );
		}		
		
		// get billing state/province
		if (header.containsKey("billingstateprovince")) {
			account.setBillingStateProvince( getStringValue(row, header.get("billingstateprovince")) );
		}			

		// get billing zip
		if (header.containsKey("billingzip")) {
			account.setBillingZip( getStringValue(row, header.get("billingzip")) );
		}
		
		// get billing country
		if (header.containsKey("billingcountry")) {
			account.setBillingCountry( getStringValue(row, header.get("billingcountry")) );
		}
		
		thisMap.put("comments", comments);
    }
}