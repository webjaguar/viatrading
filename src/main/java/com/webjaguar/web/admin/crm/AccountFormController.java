/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.01.2010
 */

package com.webjaguar.web.admin.crm;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.crm.CrmAccountForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.crm.CrmAccount;

public class AccountFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private Map<String, Configuration> siteConfig;
	
	public AccountFormController() {
		setSessionForm(true);
		setCommandName("crmAccountForm");
		setCommandClass(CrmAccountForm.class);
		setFormView("admin/crm/account/form");
		setSuccessView("crmAccountList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
		CrmAccountForm crmAccountForm = (CrmAccountForm) command;
		
		// check if delete button was pressed
		// Leads account can not be deleted (Leads ID = 1)
		if (request.getParameter("_delete") != null && crmAccountForm.getCrmAccount().getId() != 1) {
			this.webJaguar.deleteCrmAccount(crmAccountForm.getCrmAccount().getId());	
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		if (crmAccountForm.isNewCrmAccount()) {
			this.webJaguar.insertCrmAccount(crmAccountForm.getCrmAccount());
		} else {
			this.webJaguar.updateCrmAccount(crmAccountForm.getCrmAccount());
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null) {
			return true;			
		}
		return false;
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);		
	}	
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		CrmAccountForm crmAccountForm = (CrmAccountForm) command;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmAccount.accountName", "form.required", "Account Name is required.");    	
		CrmAccount crmAccount = this.webJaguar.getCrmAccountByName(crmAccountForm.getCrmAccount().getAccountName());
		if(crmAccountForm.isNewCrmAccount() && crmAccount != null){
			errors.rejectValue("crmAccount.accountName", "ACCOUNT_ALREADY_EXIST", "Account Name already exist."); 
		}
		if ( !crmAccountForm.isNewCrmAccount() && crmAccount != null && !crmAccountForm.getCrmAccount().getId().equals(crmAccount.getId()) ) {
			errors.rejectValue("crmAccount.accountName", "ACCOUNT_ALREADY_EXIST", "Account Name already exist."); 
		}
	}
	
   protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
	   CrmAccountForm crmAccountForm = new CrmAccountForm();
		if ( request.getParameter("id") != null ) {
			crmAccountForm = new CrmAccountForm(this.webJaguar.getCrmAccountById(new Integer(request.getParameter("id"))));
		} else {
			if ( request.getAttribute( "accessUser" ) != null ) {
				AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
				crmAccountForm.getCrmAccount().setAccountOwner( accessUser.getUsername() );
				crmAccountForm.getCrmAccount().setAccessUserId(accessUser.getId());
		   }
		}
		return crmAccountForm;
	} 
   
   protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
	   CrmAccountForm crmAccountForm = (CrmAccountForm) command;
	   siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
	   Map<String, Object> map = new HashMap<String, Object>();
   	
	   map.put( "countries",this.webJaguar.getEnabledCountryList());
   	   map.put("accessUsers", this.webJaguar.getUserPrivilegeList(null));	
   	   map.put("rating", siteConfig.get("CRM_RATING").getValue());	
   	   map.put("types", siteConfig.get("CRM_ACCOUNT_CONTACT_TYPE").getValue());	
	   map.put("industry", siteConfig.get("CRM_ACCOUNT_INDUSTRY").getValue());	
	   return map;
   }
}