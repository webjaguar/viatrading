/* Copyright 2005, 2012 Advanced E-Media Solutions
 * author: Jwalant Patel
 */

package com.webjaguar.web.admin.crm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.crm.CrmAccount;


public class AjaxShowCrmAccountsController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
		Map<String, Object> myModel = new HashMap<String, Object>();	
		if (request.getParameter("value") != null && request.getParameter("search") != null && request.getParameter("search").equals( "crmAccountName" )) {
			List<CrmAccount> crmAccounts = this.webJaguar.getCrmAccountAjax( ServletRequestUtils.getStringParameter( request, "value", "" ) );
			myModel.put("crmAccounts", crmAccounts);
		}
		
		return new ModelAndView("admin/crm/account/showAccounts", "model", myModel);
	}
}