/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.01.2010
 */

package com.webjaguar.web.admin.crm;



import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;

import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.crm.CrmContactFieldForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.crm.CrmContactField;


public class ContactFieldFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ContactFieldFormController() {
		setSessionForm(true);
		setCommandName("crmContactFieldForm");
		setCommandClass(CrmContactFieldForm.class);
		setFormView("admin/crm/contact/fields/form");
		setSuccessView("crmContactFieldList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
		CrmContactFieldForm crmContactFieldForm = (CrmContactFieldForm) command;
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteCrmContactField(crmContactFieldForm.getCrmContactField().getId());	
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		if (crmContactFieldForm.isNewCrmContactField()) {
			this.webJaguar.insertCrmContactField(crmContactFieldForm.getCrmContactField());
		} else {
			this.webJaguar.updateCrmContactField(crmContactFieldForm.getCrmContactField());
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null) {
			return true;			
		}
		return false;
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);		
	}	
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		CrmContactFieldForm crmContactFieldForm = (CrmContactFieldForm) command;
		CrmContactField crmContactField = this.webJaguar.getCrmContactFieldByGlobalName(crmContactFieldForm.getCrmContactField().getGlobalName());
		
		if(crmContactField != null && !crmContactField.getId().equals(crmContactFieldForm.getCrmContactField().getId())) {
			errors.rejectValue("crmContactField.globalName",  "FIELD_ALREADY_EXISTS", "Field using this email name already exists.");    					
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContactField.globalName", "form.required", "Name is required.");    	
		
	}
	
   protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
	   CrmContactFieldForm crmContactFieldForm = new CrmContactFieldForm();
	   int fieldId = ServletRequestUtils.getIntParameter(request, "id", -1);
	   
	   if ( fieldId != -1 ) {
			crmContactFieldForm = new CrmContactFieldForm(this.webJaguar.getCrmContactFieldById(fieldId));
	   }
	   
	   
	   return crmContactFieldForm;
	} 

}