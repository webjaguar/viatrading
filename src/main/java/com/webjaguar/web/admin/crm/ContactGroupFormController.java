/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 03.24.2010
 */

package com.webjaguar.web.admin.crm;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataAccessException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.crm.CrmContactGroup;
import com.webjaguar.web.form.crm.CrmContactGroupForm;

public class ContactGroupFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar; 
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ContactGroupFormController() {
		setSessionForm(true);
		setCommandName("crmContactGroupForm");
		setCommandClass(CrmContactGroupForm.class);
		setFormView("admin/crm/contact/groupForm");
		setSuccessView( "crmContactGroupList.jhtm" );
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
    	CrmContactGroupForm groupForm = (CrmContactGroupForm) command;
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteCrmContactGroupById( groupForm.getCrmContactGroup().getId() );
			return new ModelAndView(new RedirectView(getSuccessView()));
		}     	
     	
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}    	
		try {
			if(ServletRequestUtils.getStringParameter(request, "messageSelected") == null || ServletRequestUtils.getStringParameter(request, "messageSelected") == ""){
				groupForm.getCrmContactGroup().setMessageId(null);
			} else {
				groupForm.getCrmContactGroup().setMessageId(ServletRequestUtils.getIntParameter(request, "messageSelected"));
			}
			if (groupForm.isNewCrmContactGroup()) {
				this.webJaguar.insertCrmContactGroup(groupForm.getCrmContactGroup());
			} else {
				this.webJaguar.updateCrmContactGroup(groupForm.getCrmContactGroup());
			}
		} catch (DataAccessException e) {
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("message", "form.hasDuplicate");
			return showForm(request, response, errors, model);	
		}
		
		return new ModelAndView(new RedirectView( getSuccessView()));  
		
    }

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}    
			
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors)
	{
		CrmContactGroupForm groupForm = (CrmContactGroupForm) command;
 
       	Map<String, Object> map = new HashMap<String, Object>();
		map.put("messages", this.webJaguar.getSiteMessageList());
		return map;
    }  
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		CrmContactGroupForm groupForm;
		Integer groupId = ServletRequestUtils.getIntParameter( request, "id", -1 );
		if ( groupId != -1 ) {
			CrmContactGroup group = this.webJaguar.getContactGroupById( groupId );
			groupForm = new CrmContactGroupForm(group);
		} else {
			groupForm = new CrmContactGroupForm();
		}
		return groupForm;
	}
}