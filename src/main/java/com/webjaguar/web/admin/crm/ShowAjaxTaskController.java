package com.webjaguar.web.admin.crm;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.crm.CrmTask;


public class ShowAjaxTaskController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
		CrmTask crmTask = this.webJaguar.getCrmTaskById( Integer.parseInt(request.getParameter("id")));
		Map<String, Object> myModel = new HashMap<String, Object>();	
		myModel.put("crmTask", crmTask);
		
        return new ModelAndView("admin/crm/task/ajaxTask", "model", myModel);
	}
	
	
}
