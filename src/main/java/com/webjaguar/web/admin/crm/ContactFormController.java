/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.01.2010
 */

package com.webjaguar.web.admin.crm;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.EmailMessageForm;
import com.webjaguar.web.form.crm.CrmContactForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.DialingNote;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SalesRepSearch;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.crm.CrmAccount;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactField;
import com.webjaguar.model.crm.CrmQualifierSearch;

public class ContactFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private Map<String, Configuration> siteConfig;
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	private String formType;
	public void setFormType(String formType) { this.formType = formType; }
	
	private File baseFile;
	
	public ContactFormController() {
		setSessionForm(true);
		setCommandName("crmContactForm");
		setCommandClass(CrmContactForm.class);
		setFormView("admin/crm/contact/form");
		setSuccessView("crmContactList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
		CrmContactForm crmContactForm = (CrmContactForm) command;
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		// check if delete button was pressed
		
		if (request.getParameter("customerUpdate")!=null){ 
			if(crmContactForm.getCustomer()!=null && this.webJaguar.getCustomerByUsername(crmContactForm.getCustomer().getUsername())==null){
				if (siteConfig!=null && siteConfig.get("CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER")!=null && siteConfig.get("CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER").getValue()!=null && siteConfig.get("CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER").getValue().equals("true")) {
					crmContactForm.getCustomer().setAutoGenerateAccountNumber(true);
				} else {
					crmContactForm.getCustomer().setAutoGenerateAccountNumber(false);
				}
				if (crmContactForm.getCrmContact().getId() != null) {
					CrmContact crmContact = this.webJaguar.getCrmContactById(crmContactForm.getCrmContact().getId());
					if (crmContact != null) {
						if("liquidate now".equals(crmContact.getLeadSource())){
							//System.out.println("convertCrmContactToCustomer 170");
							crmContact.setLeadSource("LiquidateNowConv");
							crmContactForm.getCustomer().setRegisteredBy("LiquidateNowConv");
							this.webJaguar.updateCrmContact(crmContact);
						}else if("LandingPage".equals(crmContact.getLeadSource())){
							//System.out.println("convertCrmContactToCustomer 174");
							crmContact.setLeadSource("LandingPageConv");
							crmContactForm.getCustomer().setRegisteredBy("LandingPageConv");
							this.webJaguar.updateCrmContact(crmContact);
						}
						crmContactForm.getCustomer().setQualifier(crmContact.getQualifier());
					}
				}	
				if(crmContactForm!=null && crmContactForm.getCustomer()!=null && (crmContactForm.getCustomer().getCardId()==null || crmContactForm.getCustomer().getCardId().length()<=0)){
					generateRandomCardID(crmContactForm.getCustomer());
				}
				if(crmContactForm!=null && crmContactForm.getCrmContact()!=null && crmContactForm!=null && crmContactForm.getCustomer()!=null){
					crmContactForm.getCustomer().setUnsubscribe(crmContactForm.getCrmContact().isUnsubscribe());
				}
				this.webJaguar.insertCustomer(crmContactForm.getCustomer(), null, ((Boolean) gSiteConfig.get("gCRM") && request.getParameter("contactId") == null));
				if (crmContactForm.getCrmContact().getId() != null) {
					//Dialing Note
					if( crmContactForm.getCustomer().getNewDialingNote() != null && crmContactForm.getCustomer().getNewDialingNote().getNote() != null && !crmContactForm.getCustomer().getNewDialingNote().getNote().equals("")){
						crmContactForm.getCustomer().getNewDialingNote().setCrmId(crmContactForm.getCrmContact().getId());
						crmContactForm.getCustomer().getNewDialingNote().setCustomerId(crmContactForm.getCustomer().getId());
						this.webJaguar.insertDialingNoteHistory(crmContactForm.getCustomer().getNewDialingNote());
					}	
					this.webJaguar.convertCrmContactToCustomer(crmContactForm.getCrmContact().getId(), crmContactForm.getCustomer().getId(), null);
				}
				CrmContact crmContact = this.webJaguar.getCrmContactById(crmContactForm.getCrmContact().getId());
    			if(crmContact != null) {
    				try {
    					this.webJaguar.customerToCrmContactSynch(crmContact, crmContactForm.getCustomer());
    				} catch (Exception e) { e.printStackTrace();}
    				this.webJaguar.updateCrmContact(crmContact);
    			} 
			}
		}else{
			if (request.getParameter("_delete") != null) {
				this.webJaguar.deleteCrmContact(crmContactForm.getCrmContact().getId());	
				return new ModelAndView(new RedirectView(getSuccessView()));
			}
			
			if (crmContactForm.isNewCrmContact()) {
				if ( request.getAttribute( "accessUser" ) != null ) {
					AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
					crmContactForm.getCrmContact().setLeadSource( accessUser.getUsername() );
				} else {
					crmContactForm.getCrmContact().setLeadSource( getApplicationContext().getMessage("admin", new Object[0], RequestContextUtils.getLocale(request)) );
				}
				
				if(crmContactForm.getCrmContact().getAccountId() == null) {
					this.assignNewAccount(crmContactForm.getCrmContact());
				}
				if((crmContactForm.getCrmContact().getAccessUserId() != null ) && (!crmContactForm.getCrmContact().getAccessUserId().equals(request.getSession().getAttribute("accessUserId")))) {
					notifyAccessUser(siteConfig, crmContactForm.getCrmContact());				
					request.getSession().removeAttribute("accessUserId");
				}
				if(crmContactForm.getCrmContact().getId()!=null){
					int crmContanctID= crmContactForm.getCrmContact().getId();
					CrmContact crmContact = this.webJaguar.getCrmContactById(crmContanctID);
					if(crmContactForm.getCrmContact().getAccessUserId() != null&&(crmContact.getAccessUserId()==null||!crmContact.getAccessUserId().equals(crmContactForm.getCrmContact().getAccessUserId()))){
						notifySalesRep(siteConfig, crmContactForm.getCrmContact());	
					}
				}else{
					if(crmContactForm.getCrmContact().getAccessUserId() != null){
						notifySalesRep(siteConfig, crmContactForm.getCrmContact());	
					}
				}	
				
				
				
				this.webJaguar.insertCrmContact(crmContactForm.getCrmContact());
			} else {
				if((crmContactForm.getCrmContact().getAccountId()!=null) && (crmContactForm.getCrmContact().getAccountId() == 1) && (crmContactForm.getCrmContact().getAccountName()!=null) && (!crmContactForm.getCrmContact().getAccountName().equalsIgnoreCase("Leads"))) {
					this.assignNewAccount(crmContactForm.getCrmContact());
				}
				if( crmContactForm.getCrmContact().getUserId() != null ) {
					Customer customer = this.webJaguar.getCustomerById(crmContactForm.getCrmContact().getUserId());
					if(customer != null) {
						try {
							this.webJaguar.crmContactToCustomerSynch(crmContactForm.getCrmContact(), customer, false);
							
							if(crmContactForm.getCrmContact().getDescription()!=null && crmContactForm.getCrmContact().getDescription().length()>0){
								customer.setNote(crmContactForm.getCrmContact().getDescription());
							}
							
						} catch (Exception e) {}
						this.webJaguar.updateCustomer(customer, false, false);
					} else {
						crmContactForm.getCrmContact().setUserId(null);
					}
				}
				if((crmContactForm.getCrmContact().getAccessUserId() != null ) && (!crmContactForm.getCrmContact().getAccessUserId().equals(request.getSession().getAttribute("accessUserId")))) {
					notifyAccessUser(siteConfig, crmContactForm.getCrmContact());				
					request.getSession().removeAttribute("accessUserId");
				}
				int crmContanctID= crmContactForm.getCrmContact().getId();
				CrmContact crmContact = this.webJaguar.getCrmContactById(crmContanctID);
				if(crmContactForm.getCrmContact().getAccessUserId() != null&&(crmContact.getAccessUserId()==null||!crmContact.getAccessUserId().equals(crmContactForm.getCrmContact().getAccessUserId()))){
					notifySalesRep(siteConfig, crmContactForm.getCrmContact());	
				}			
				this.webJaguar.updateCrmContact(crmContactForm.getCrmContact());
				
				//Dialing Note
				if(crmContactForm.getCrmContact().getNewDialingNote() != null && crmContactForm.getCrmContact().getNewDialingNote().getNote() != null && !crmContactForm.getCrmContact().getNewDialingNote().getNote().equals("")){
					if(crmContactForm.getCustomer()!=null && crmContactForm.getCustomer().getNewDialingNote()!=null && crmContactForm.getCrmContact()!=null){
						crmContactForm.getCustomer().getNewDialingNote().setCrmId(crmContactForm.getCrmContact().getId());
					}
					if(crmContactForm!=null && crmContactForm.getCrmContact()!=null && crmContactForm.getCrmContact().getId()!=null){
						crmContactForm.getCrmContact().getNewDialingNote().setCrmId(crmContactForm.getCrmContact().getId());
					}
					
					Customer customer = this.webJaguar.getCustomerById(crmContactForm.getCrmContact().getUserId());
					if(customer!=null && customer.getId()!=null){
						crmContactForm.getCrmContact().getNewDialingNote().setCustomerId(customer.getId());
					}
					this.webJaguar.insertDialingNoteHistory(crmContactForm.getCrmContact().getNewDialingNote());
				}		
			}
			
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}
	
	private void generateRandomCardID(Customer customer) {
		String cardID = UUID.randomUUID().toString().toUpperCase().replace("-", "").substring(0, 9);
		if (this.webJaguar.getCustomerByCardID(cardID) == null) {
			customer.setCardId(cardID);
		} else
			generateRandomCardID(customer);
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null) {
			return true;			
		}
		return false;
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);		
	}	
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		CrmContactForm crmContactForm = (CrmContactForm) command;
		if (request.getParameter("customerUpdate")!=null){ 
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.username", "form.required", "Username is required.");    	
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.password", "form.required", "Password is required.");    	
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.firstName", "form.required", "Firstname is required."); 
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.lastName", "form.required", "Lastname is required.");    	
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.addr1", "form.required", "Address is required.");    	
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.country", "form.required", "Country is required."); 
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.zip", "form.required", "Zip is required.");    	
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.city", "form.required", "City is required.");    	
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.stateProvince", "form.required", "State is required."); 
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.phone", "form.required", "Phone is required.");
			if(this.webJaguar.getCustomerByUsername(crmContactForm.getCustomer().getUsername())!=null){
				errors.rejectValue("customer.username", "USER_ID_ALREADY_EXISTS", "An account using this email address already exists.");
			}
		}else{
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.firstName", "form.required", "First Name is required.");    	
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.lastName", "form.required", "Last Name is required.");    	
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.accountName", "form.required", "Account Name is required."); 
			if (crmContactForm!=null && (!crmContactForm.isNewCrmContact()) && crmContactForm.getCrmContact()!=null && crmContactForm.getCrmContact().getAccountId()!=null && crmContactForm.getCrmContact().getAccountId() > 1) {
				CrmAccount crmAccount = this.webJaguar.getCrmAccountByName(crmContactForm.getCrmContact().getAccountName());
				if (crmAccount==null) {
					errors.rejectValue("crmContact.accountName", "crm.noAccount", "Account Name does not exist."); 
				} else {
					crmContactForm.getCrmContact().setAccountId(crmAccount.getId());
					crmContactForm.getCrmContact().setAccountName(crmAccount.getAccountName());
				}
				if( crmContactForm.getCrmContact().getUserId() != null ) {
					Customer customer = this.webJaguar.getCustomerById(crmContactForm.getCrmContact().getUserId());
					if(customer != null && !customer.getUsername().equalsIgnoreCase(crmContactForm.getCrmContact().getEmail1())) {
						errors.rejectValue("crmContact.email1", "NOT_ALLOWED_TO_CHANGE", "User Name can not be changed.");    					
					}
				}
			}
		} 
		if(errors!=null && (request.getParameter("customerUpdate")!=null || request.getParameter("crmUpdate")!=null)){
			setFormView("admin/crm/contact/predictiveDialingForm2");
		}else{
			setFormView("admin/crm/contact/form");
		}
	}
	
   protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
	   CrmContactForm crmContactForm = new CrmContactForm();
	   int accountId = ServletRequestUtils.getIntParameter(request, "accountId", -1);
	   int contactId = ServletRequestUtils.getIntParameter(request, "id", -1);
	   Customer customer = null;
	   Address address = null;
	   
	   if ( contactId != -1 ) {
			crmContactForm = new CrmContactForm(this.webJaguar.getCrmContactById(contactId));
			customer = new Customer();
			address = new Address();
			crmContactForm.setCustomer(customer);
			crmContactForm.getCustomer().setAddress(address);
			if(crmContactForm!=null && crmContactForm.getCrmContact()!=null){
				crmContactForm.getCustomer().setUsername(crmContactForm.getCrmContact().getEmail1());
				crmContactForm.getCustomer().setRating1(crmContactForm.getCrmContact().getRating1());
				crmContactForm.getCustomer().setRating2(crmContactForm.getCrmContact().getRating2());
				crmContactForm.getCustomer().getAddress().setFirstName(crmContactForm.getCrmContact().getFirstName());
				crmContactForm.getCustomer().getAddress().setLastName(crmContactForm.getCrmContact().getLastName()); 
				crmContactForm.getCustomer().getAddress().setAddr1(crmContactForm.getCrmContact().getStreet());
				crmContactForm.getCustomer().getAddress().setCity(crmContactForm.getCrmContact().getCity());
				crmContactForm.getCustomer().getAddress().setCountry(crmContactForm.getCrmContact().getCountry());
				crmContactForm.getCustomer().getAddress().setZip(crmContactForm.getCrmContact().getZip());
				crmContactForm.getCustomer().getAddress().setStateProvince(crmContactForm.getCrmContact().getStateProvince());
				crmContactForm.getCustomer().getAddress().setPhone(crmContactForm.getCrmContact().getPhone1());
				crmContactForm.getCustomer().getAddress().setCellPhone(crmContactForm.getCrmContact().getPhone2());
				crmContactForm.getCustomer().getAddress().setFax(crmContactForm.getCrmContact().getFax());
				crmContactForm.getCustomer().setRating1(crmContactForm.getCrmContact().getRating1());
				crmContactForm.getCustomer().setRating2(crmContactForm.getCrmContact().getRating2());
			}
			
			if(crmContactForm.getCustomer()!=null){
				if(crmContactForm.getCustomer().getDialingNoteHistory()==null){
					crmContactForm.getCustomer().setDialingNoteHistory(new LinkedList<DialingNote>());
				}
				crmContactForm.getCustomer().setDialingNoteHistory(this.webJaguar.getDialingNoteHistoryById(contactId, false));
				if(crmContactForm.getCustomer().getDialingNoteHistory() != null && !crmContactForm.getCustomer().getDialingNoteHistory().isEmpty()) {
					crmContactForm.getCustomer().setLastDialingNote(crmContactForm.getCustomer().getDialingNoteHistory().get(crmContactForm.getCustomer().getDialingNoteHistory().size() -1).getNote());
				}
			}
	   }
	   
	   if( contactId == -1 ) {
		   List<CrmContactField> crmContactFields = this.webJaguar.getCrmContactFieldList(null);
		   Iterator<CrmContactField> iter = crmContactFields.iterator();
		   while ( iter.hasNext() ) {
			   if (!iter.next().isEnabled()) {
				   iter.remove();
			   }
		   }
		  crmContactForm.getCrmContact().setCrmContactFields(crmContactFields);
	   }
	   
	   if ( accountId != -1 ) {
		   CrmAccount crmAccount = this.webJaguar.getCrmAccountById(accountId);
		   crmContactForm.getCrmContact().setAccountId(accountId);
		   crmContactForm.getCrmContact().setAccountName(crmAccount.getAccountName());
		   if( crmContactForm.getCrmContact().getAccessUserId() == null) {
			   crmContactForm.getCrmContact().setAccessUserId(crmAccount.getAccessUserId());
		   }
		   
		   //auto populate some information from account like contact phone, fax and address
		   if(contactId == -1) {
			   crmContactForm.getCrmContact().setFax(crmAccount.getFax());
			   crmContactForm.getCrmContact().setPhone1(crmAccount.getPhone());
			   crmContactForm.getCrmContact().setPhone2(crmAccount.getPhone());
			   crmContactForm.getCrmContact().setWebsite(crmAccount.getWebsite());
			   
			   crmContactForm.getCrmContact().setStreet(crmAccount.getBillingStreet());
			   crmContactForm.getCrmContact().setCity(crmAccount.getBillingCity());
			   crmContactForm.getCrmContact().setStateProvince(crmAccount.getBillingStateProvince());
			   crmContactForm.getCrmContact().setCountry(crmAccount.getBillingCountry());
			   crmContactForm.getCrmContact().setZip(crmAccount.getBillingZip());
			  
			   crmContactForm.getCrmContact().setOtherStreet(crmAccount.getShippingStreet());
			   crmContactForm.getCrmContact().setOtherCity(crmAccount.getShippingCity());
			   crmContactForm.getCrmContact().setOtherStateProvince(crmAccount.getShippingStateProvince());
			   crmContactForm.getCrmContact().setOtherCountry(crmAccount.getShippingCountry());
			   crmContactForm.getCrmContact().setOtherZip(crmAccount.getShippingZip());
		   }
		}
	   
	    if ( request.getAttribute( "accessUser" ) != null && crmContactForm.isNewCrmContact()) {
		   AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
		   crmContactForm.getCrmContact().setCreatedBy( accessUser.getUsername() );
	    }
	    request.getSession().setAttribute("accessUserId", crmContactForm.getCrmContact().getAccessUserId());
	   
	   
	    //Get list of files from the folder of the contact
   	    // For example, ROOT Directory/temp/CRM/contact_CONTACTID
	    if(crmContactForm.getCrmContact().getId() != null) {
	    	baseFile = new File( getServletContext().getRealPath( "/assets/CRM/" ) );
			Properties prop = new Properties();
			try {
				prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
				if ( prop.get( "site.root" ) != null ) {
					baseFile = new File( (String) prop.get( "site.root" ) + "/assets/CRM/" );
				}
				if(!baseFile.exists()) {
					baseFile.mkdir();
				}
			} catch ( Exception e ){
				
			}
			try {
   				baseFile = new File( baseFile, "/contact_"+crmContactForm.getCrmContact().getId()+"/" );
   			} catch ( Exception e ){ e.printStackTrace(); }
   			
   			if(baseFile != null && baseFile.listFiles() != null){
   				File[] list = baseFile.listFiles();
   				List<String> fileNames = new ArrayList<String>();
   		        for ( File f : list ) {
   		        	fileNames.add(f.getName());
   		        }
   		        crmContactForm.getCrmContact().setAttachmentList(fileNames);
   			}
   		}
		if(formType != null && formType.equals("predictiveDialing")){
			setFormView("admin/crm/contact/predictiveDialingForm2");		
		}else{
			setFormView("admin/crm/contact/form");
		}
		if( contactId != -1 ){
			crmContactForm.getCrmContact().setDialingNoteHistory(this.webJaguar.getDialingNoteHistoryById(crmContactForm.getCrmContact().getId(), false));
		}
   		return crmContactForm;
	} 
   
   protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
	   CrmContactForm crmContactForm = (CrmContactForm) command;
	   siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
	   Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
	   Map<String, Object> map = new HashMap<String, Object>();
   	
	   map.put( "countries",this.webJaguar.getEnabledCountryList());
	   map.put("accessUsers", this.webJaguar.getUserPrivilegeList(null));	
	   map.put("rating", siteConfig.get("CRM_RATING").getValue());	
	   map.put("types", siteConfig.get("CRM_ACCOUNT_CONTACT_TYPE").getValue());	
	   
	   map.put("mobileCarriers",this.webJaguar.getMobileCarrierList());
   	   map.put("states",this.webJaguar.getStateList("US"));
	   map.put("caProvinceList", this.webJaguar.getStateList("CA"));
	   CrmQualifierSearch crmQualifierSearch = new CrmQualifierSearch();
	   crmQualifierSearch.setSort("name");
	   map.put("crmQualifierList", this.webJaguar.getCrmQualifierList(crmQualifierSearch));
	   map.put("contactFields", this.webJaguar.getCrmContactFieldList(null));
		
	   if(((String) gSiteConfig.get("gI18N")).length()>0){
		   map.put("languageCodes", this.webJaguar.getLanguageSettings());
	   }
	   if ((Boolean) gSiteConfig.get("gSHOPPING_CART") && (Boolean) gSiteConfig.get("gSALES_REP")){
			SalesRepSearch salesRepSearch = new SalesRepSearch();
			salesRepSearch.setShowActiveSalesRep(true);
			map.put("salesReps", this.webJaguar.getSalesRepList(salesRepSearch));
	   }
	   return map;
   }
   
   private void assignNewAccount( CrmContact crmContact ){
	   CrmAccount crmAccount = this.webJaguar.getCrmAccountByName(crmContact.getAccountName());
		if(crmAccount == null){
			crmAccount = new CrmAccount();
			crmAccount.setAccountName(crmContact.getAccountName());
			crmAccount.setAccountOwner(crmContact.getLeadSource());
			this.webJaguar.insertCrmAccount(crmAccount);
		}
		crmContact.setAccountId(crmAccount.getId());
   }
   
	private void notifyAccessUser(Map<String, Configuration> siteConfig, CrmContact contact) {
		// send email notification
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	AccessUser accessUser = this.webJaguar.getAccessUserById(contact.getAccessUserId());
    	
    	StringBuffer message = new StringBuffer();
		
		message.append("Hello "+accessUser.getFirstName()+" "+accessUser.getLastName());
		message.append(",\n\n");
		message.append("This is to inform you that a new Contact "+contact.getFirstName()+" "+contact.getLastName()+" is assigned to you. \n\n");
		message.append("Thanks \n");
		
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(accessUser.getEmail());			
		msg.setFrom(siteConfig.get("CONTACT_EMAIL").getValue());
		msg.setSubject("Contact Assignment ");

		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			long startTime = System.nanoTime();
			mailSender.send(msg);
			long endTime = System.nanoTime();
			//System.out.println("Took #############################################################"+(endTime - startTime) + " ns"); 
		} catch (MailException ex) { }  		
	} 	
	
	private void notifySalesRep(Map<String, Configuration> siteConfig, CrmContact contact) {		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	AccessUser accessUser = this.webJaguar.getAccessUserById(contact.getAccessUserId());
    	SalesRep salesRep = this.webJaguar.getSalesRepById( accessUser.getSalesRepId() );
    	StringBuffer message = new StringBuffer();		
    	EmailMessageForm form = new EmailMessageForm();
    	form.setHtml(false);
    	form.setSubject("Account Manager Notification");
    	int messageId;
    	if(siteConfig.get( "SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION_CONTACT" ).getValue()!=null){
    		messageId= Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION_CONTACT" ).getValue() );
    	}else{
    		messageId=0;       	
    	}
    	SiteMessage siteMessage = this.webJaguar.getSiteMessageById( messageId );
		if(	siteMessage!=null&&!siteMessage.equals("")){
			
	    	if(siteMessage.isHtml()==true){
	    		form.setHtml(true);
	    	}
	    	message.append(siteMessage.getMessage());
	    	form.setSubject(siteMessage.getMessageName());
		}else{
			message.append("Hello #salesRepName#");
			message.append(",\n\n");
			message.append("This is to inform you that a new Contact #firstname# is assigned to you. \n\n");
			message.append("Dialing Notes: #dialingNotes# \n");
			message.append("Thanks \n");
		}
		form.setMessage(message.toString());
		if(salesRep!=null){
			form.setTo(salesRep.getEmail());
			// add the #diallingAddress#
			
		}else{
			form.setTo(accessUser.getEmail());
		}
		
		form.setFrom(siteConfig.get("CONTACT_EMAIL").getValue());
		try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			if(salesRep!=null){
				this.webJaguar.replaceDynamicElement(form, contact, salesRep);
			}else{
				this.webJaguar.replaceDynamicElement(form, contact, accessUser);
			}
			
			helper.setTo(form.getTo());
			helper.setFrom(form.getFrom());
			helper.setSubject(form.getSubject());
			if ( form.isHtml()==true ) { 
	    		helper.setText(form.getMessage(), true );
	    	} else {
	    		helper.setText(form.getMessage());
	    	}
			mailSender.send(mms);
		} catch (Exception ex) {			
				ex.printStackTrace();					
		}  				
	}
}