package com.webjaguar.web.admin.crm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.DialingNote;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

public class AjaxDialingNoteController implements Controller{
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		DialingNote note = new DialingNote();
		Integer contactId = ServletRequestUtils.getIntParameter( request, "contactId", -1);
		note.setCrmId(contactId);
		Integer customerId = null;
		List<Integer> crmList = new LinkedList<Integer>();
		List<Integer> customerList = null;
		if(contactId > -1){ 
			crmList.add(contactId);
			customerList = this.webJaguar.getUserIdByCrmId(crmList);
			if(customerList!=null && (customerList.size()>0)){
				customerId = customerList.get(0);
			}
		}
		note.setCustomerId(customerId);
		note.setNote(ServletRequestUtils.getStringParameter(request, "note", ""));
		this.webJaguar.insertDialingNoteHistory(note);
		return null;
	}
}
