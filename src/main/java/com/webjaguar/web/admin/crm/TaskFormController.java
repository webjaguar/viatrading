/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.01.2010
 */

package com.webjaguar.web.admin.crm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.simpl.SimpleJobFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.scheduling.quartz.CronTriggerBean;
import org.springframework.scheduling.quartz.JobDetailBean;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.crm.CrmTaskForm;
import com.webjaguar.web.quartz.CrmTaskReminderJob;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.crm.CrmContact;

public class TaskFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private Scheduler scheduler;
	public void setScheduler(Scheduler scheduler) { this.scheduler = scheduler; }
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	public TaskFormController() {
		setSessionForm(true);
		setCommandName("crmTaskForm");
		setCommandClass(CrmTaskForm.class);
		setFormView("admin/crm/task/form");
		setSuccessView("crmTaskList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		CrmTaskForm crmTaskForm = (CrmTaskForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
		
		if (request.getParameter("__back") != null) {
			return new ModelAndView(new RedirectView(getSuccessView() + "?contactId=" + crmTaskForm.getCrmTask().getContactId()));
		}
		
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteCrmTask(crmTaskForm.getCrmTask().getId());	
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		AccessUser user = this.webJaguar.getAccessUserById(crmTaskForm.getCrmTask().getAssignedToId());
		if (user != null){
			crmTaskForm.getCrmTask().setAssignedTo(user.getName());
		} else {
			// Admin id = 0
			crmTaskForm.getCrmTask().setAssignedTo("Admin");
		}
		
		if (crmTaskForm.getCrmTask().getReminder() != null) {
			crmTaskForm.getCrmTask().setupReminder();
		}
		
		
		if (crmTaskForm.isNewCrmTask()) {
			this.webJaguar.insertCrmTask(crmTaskForm.getCrmTask());
		} else {
			this.webJaguar.updateCrmTask(crmTaskForm.getCrmTask());
		}
		
		// setup reminder
		int scheduleResult = 0;
		if (crmTaskForm.getCrmTask().getReminder() != null) {
			try{
				scheduleResult = scheduleTaskReminder(crmTaskForm);
			} catch (Exception e) {}
		} else {
			// check if job exist remove it
			// quartz 2.x
//			JobKey jobKey = new JobKey(siteConfig.get("MASS_EMAIL_JOB_NAME" ).getValue() + "_trigger_"+crmTaskForm.getCrmTask().getContactId()+"_"+crmTaskForm.getCrmTask().getId(), Scheduler.DEFAULT_GROUP); 
//			this.scheduler.deleteJob(jobKey);
			this.scheduler.deleteJob("_task_job_"+crmTaskForm.getCrmTask().getContactId()+"_"+crmTaskForm.getCrmTask().getId(), Scheduler.DEFAULT_GROUP);
			
		}
		
		if ( scheduleResult == 3 ) {
			model.put("message", "reminder.error" );
		} else if ( scheduleResult == 1 ) {
			model.put("scheduleDone", true );
			model.put("message", "reminder.scheduled" );
		} else if ( scheduleResult == 2 ) {
			model.put("scheduleDone", true );
			model.put("message", "reminder.rescheduled" );
		}
		
		if (crmTaskForm.getCrmTask().getReminder() != null && scheduleResult == 3) {
			return showForm(request, response, errors, model);
		} else if(request.getParameter("_followUp") != null){
			return new ModelAndView(new RedirectView("crmTaskForm.jhtm?taskId=" + crmTaskForm.getCrmTask().getId()+"&fupt=followup"));
		} else {
			return new ModelAndView(new RedirectView(getSuccessView() + "?contactId=" + crmTaskForm.getCrmTask().getContactId()));
		}
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, true, 10 ) );		
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		CrmTaskForm crmTaskForm = (CrmTaskForm) command;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmTask.title", "form.required", "First Name is required.");  
		if (crmTaskForm.getCrmTask().getReminder() != null) { 
			crmTaskForm.getCrmTask().setupReminder();
			if (crmTaskForm.getCrmTask().getReminder().before(Calendar.getInstance().getTime())) {
				errors.rejectValue("crmTask.reminder", "form.futureDate", "Schedule time should be in future.");
			}	
		}
	}
	
   protected Object formBackingObject(HttpServletRequest request)  throws Exception 
   {	
	   siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
	   gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		
	   CrmTaskForm crmTaskForm = new CrmTaskForm();
	   int taskId = ServletRequestUtils.getIntParameter(request, "taskId", -1);
	   int contactId = ServletRequestUtils.getIntParameter(request, "contactId", -1);
	   String followup = ServletRequestUtils.getStringParameter(request, "fupt", "");
	   
	   if (taskId != -1) {
		   crmTaskForm.setCrmTask(this.webJaguar.getCrmTaskById(taskId));
		   crmTaskForm.setNewCrmTask(false);
		   if (!followup.isEmpty()) {
			   crmTaskForm.setNewCrmTask(true);
			   // clear data
			   crmTaskForm.getCrmTask().setDescription(null);
			   crmTaskForm.getCrmTask().setReminder(null);
			   crmTaskForm.getCrmTask().setStatus(null);
			}
	   } else { 
		   CrmContact crmContact = this.webJaguar.getCrmContactById(contactId);
		   crmTaskForm.getCrmTask().setAccountId(crmContact.getAccountId());
		   crmTaskForm.getCrmTask().setAccountName(crmContact.getAccountName());
		   crmTaskForm.getCrmTask().setContactId(contactId);
		   crmTaskForm.getCrmTask().setContactName(crmContact.getContactName());
		   crmTaskForm.getCrmTask().setContactPhone(crmContact.getPhone1());
		   crmTaskForm.getCrmTask().setContactEmail(crmContact.getEmail1());
		   crmTaskForm.getCrmTask().setContactName(crmContact.getContactName());
		   crmTaskForm.getCrmTask().setAssignedToId(crmContact.getAccessUserId());
			
	   }
	   return crmTaskForm;
	}
   
   protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {   
	   CrmTaskForm crmTaskForm = (CrmTaskForm) command;
	   Map<String, Object> myModel = new HashMap<String, Object>();
	   myModel.put("userList", this.webJaguar.getUserPrivilegeList(null) );

	   Map<String, Map> map = new HashMap<String, Map>();
	   myModel.put( "currentTime", new Date() );
	   myModel.put("taskTypes", siteConfig.get("CRM_TASK_TYPE").getValue());
	   myModel.put("taskRanks", siteConfig.get("CRM_TASK_RANK").getValue());
	   myModel.put("taskActions", siteConfig.get("CRM_TASK_ACTION").getValue());
	   map.put( "model", myModel );
	   return map;
   }
   
   private int scheduleTaskReminder(CrmTaskForm crmTaskForm) throws ParseException, SchedulerException {
	   //http://forums.terracotta.org/forums/posts/list/5274.page
	   //http://vageeshhoskere.wordpress.com/2011/05/09/jdbc-job-store-quartz-scheduler/
	   //SchedulerFactory schdFact = new StdSchedulerFactory("quartz.properties");
	   //Scheduler schd = schdFact.getScheduler();
	   //schd.start();
	   
		// get scheduled date,hour,minute
		Calendar scheduledCalendar = Calendar.getInstance();

		scheduledCalendar.setTime( crmTaskForm.getCrmTask().getReminder() );
		
		int year = scheduledCalendar.get(Calendar.YEAR);
		int month = scheduledCalendar.get(Calendar.MONTH)+1;
		int day = scheduledCalendar.get(Calendar.DAY_OF_MONTH);
		int hour = scheduledCalendar.get(Calendar.HOUR_OF_DAY);
		int minute = scheduledCalendar.get(Calendar.MINUTE);
		
		// job detail quartz 2.x
//		JobDetail job = JobBuilder.newJob(CrmTaskReminderJob.class)
//	    .withIdentity(siteConfig.get( "MASS_EMAIL_JOB_NAME" ).getValue() + "_task_job_"+crmTaskForm.getCrmTask().getContactId()+"_"+crmTaskForm.getCrmTask().getId(), Scheduler.DEFAULT_GROUP)
//	    .storeDurably()
//	    .requestRecovery().build();
//		job.getJobDataMap().put("crmTaskForm",crmTaskForm);
		
		
		JobDetailBean job = new JobDetailBean();
		job.setJobClass(CrmTaskReminderJob.class);
		job.setName( "_task_job_"+crmTaskForm.getCrmTask().getContactId()+"_"+crmTaskForm.getCrmTask().getId());
		job.setGroup(Scheduler.DEFAULT_GROUP);
		job.getJobDataMap().put("crmTaskForm",crmTaskForm);
		
		
		// trigger quartz 2.x
//		CronTrigger trigger = TriggerBuilder.newTrigger()
//	    .withIdentity(siteConfig.get("MASS_EMAIL_JOB_NAME" ).getValue() + "_trigger_"+crmTaskForm.getCrmTask().getContactId()+"_"+crmTaskForm.getCrmTask().getId(), Scheduler.DEFAULT_GROUP)
//	    .withSchedule(CronScheduleBuilder.cronSchedule("0 "+ minute + " " + hour + " " + day + " " + month + " ? " + year))
//	    .build();
		
		// trigger
		CronTriggerBean trigger = new CronTriggerBean();
		trigger.setStartTime(java.util.Calendar.getInstance().getTime());
		trigger.setCronExpression("0 "+ minute + " " + hour + " " + day + " " + month + " ? " + year);
		trigger.setName("_trigger_"+crmTaskForm.getCrmTask().getContactId()+"_"+crmTaskForm.getCrmTask().getId());
		trigger.setGroup(Scheduler.DEFAULT_GROUP);

		try {
			// schedule a job
			scheduler.scheduleJob(job,trigger);
			return 1;
		} catch (SchedulerException e) {
			// reschedule the same task
			// quartz 2.x
			//JobKey jobKey = new JobKey(siteConfig.get("MASS_EMAIL_JOB_NAME" ).getValue() + "_trigger_"+crmTaskForm.getCrmTask().getContactId()+"_"+crmTaskForm.getCrmTask().getId(), Scheduler.DEFAULT_GROUP);
			//scheduler.deleteJob(jobKey);
			
			scheduler.deleteJob("_task_job_"+crmTaskForm.getCrmTask().getContactId()+"_"+crmTaskForm.getCrmTask().getId(), Scheduler.DEFAULT_GROUP);
			scheduler.scheduleJob(job,trigger);
			return 2;
		} catch (Exception e) {
			return 3;
		}
	}
}