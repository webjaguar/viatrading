/* Copyright 2005, 2010 Advanced E-Media Solutions
 * @author Jwalant Patel
 */

package com.webjaguar.web.admin.crm;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.crm.CrmTask;
import com.webjaguar.model.crm.CrmTaskSearch;

public class TaskExportController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }		
	
	private File baseFile;
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
    	CrmTaskSearch search = this.webJaguar.getCrmTaskSearch( request );
    	
    	Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Object> map = new HashMap<String, Object>();
		
		search.setFileType(ServletRequestUtils.getStringParameter(request, "fileType", "xls"));
		baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			map.put("message", "excelfile.notWritable");
			return new ModelAndView("admin/crm/task/export", map);			
		}
		File file[] = baseFile.listFiles();
		List<Map> exportedFiles = new ArrayList<Map>();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith("crmTaskExport") & file[f].getName().endsWith(".xls")) {
				if (request.getParameter("__new") != null) { // check if new button was pressed
					file[f].delete();
				} else {
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					exportedFiles.add( fileMap );				
				}
			}
		}
		map.put("exportedFiles", exportedFiles);
    	
		// check if new button was pressed
		if (request.getParameter("__new") != null) {
			
			int count = this.webJaguar.getCrmTaskListCount(search);
			if (search.getFileType().equals("xls")) {
				createExcelFiles(count, search, exportedFiles);				
			} else if (search.getFileType().equals("csv")){
				//createCsvFile(count, search, exportedFiles);		
			}
	        
			if (count > 0) {
				map.put("arguments", count);
				map.put("message", "crmtaskexport.success");
				this.webJaguar.insertImportExportHistory( new ImportExportHistory("crmtask", SecurityContextHolder.getContext().getAuthentication().getName(), false ));
			} else {
				map.put("message", "crmtaskexport.empty");				
			}
		} // if    
		
		return new ModelAndView("admin/crm/task/export", map);
	}
    
   
	private void createExcelFiles( int count, CrmTaskSearch search, List<Map> exportedFiles) throws Exception {
    	int limit = 3000;
    	search.setLimit(limit);
    	search.setOffset(0);

    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	String fileName = "/crmTaskExport_" + dateFormatter.format(new Date()) + "";

    	for (int offset=0, excel = 1; offset<count; excel++) {
    		List<CrmTask> tasks = this.webJaguar.getCrmTaskExport(search);
	    	int start = offset + 1;
	    	int end = (offset + limit) < count ? (offset + limit) : count;
	    	HSSFWorkbook wb = createWorkbook(tasks, start + " to " + end);
	    	// Write the output to a file
	    	File newFile = new File(baseFile.getAbsolutePath() + fileName + excel + ".xls");
	        FileOutputStream fileOut = new FileOutputStream(newFile);
	        wb.write(fileOut);
	        fileOut.close();
	        HashMap<String, Object> fileMap = new HashMap<String, Object>();
	        fileMap.put("file", newFile);
			exportedFiles.add( fileMap );  		
	        offset = offset + limit;
	        search.setOffset(offset);
    	}
        
    }
    
    private HSSFCell getCell(HSSFSheet sheet, int row, short col) {
    	return sheet.createRow(row).createCell(col);
    }
    
    private void setText(HSSFCell cell, String text) {
    	cell.setCellValue(text);
    }
    
    private HSSFWorkbook createWorkbook(List<CrmTask> tasks, String sheetName) throws Exception {
    	Iterator<CrmTask> iter = tasks.iterator();

    	HSSFWorkbook wb = new HSSFWorkbook();
    	HSSFSheet sheet = wb.createSheet(sheetName);
    	
    	// create the headers
		Map<String, Short> headerMap = new HashMap<String, Short>();
		short col = 0;
		
		headerMap.put("Id", col++);
		headerMap.put("AccountName", col++);
		headerMap.put("ContactName", col++);
		headerMap.put("Title", col++);
		headerMap.put("Type", col++);
		headerMap.put("AssignedTo", col++);
		headerMap.put("Created", col++);
		headerMap.put("Description", col++);
		headerMap.put("DueDate", col++);
		headerMap.put("Rank", col++);
		headerMap.put("Status", col++);
		headerMap.put("Action", col++);
		headerMap.put("Email", col++);
		headerMap.put("Phone", col++);
		
		// print headers
		for (String headerName:headerMap.keySet()){
			setText(getCell(sheet, 0, headerMap.get(headerName).shortValue()), headerName); 
		}
    	iter = tasks.iterator();
    	
    	HSSFCellStyle dateCellStyle = wb.createCellStyle();
		dateCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yy");
		
    	HSSFCell cell = null;
		// create the rows
    	for (int row=2; iter.hasNext(); row++) {    		
    		CrmTask task = (CrmTask) iter.next();
    		setText(getCell(sheet, row, headerMap.get("Id").shortValue()), task.getId().toString());
        	setText(getCell(sheet, row, headerMap.get("AccountName").shortValue()), task.getAccountName().toString());
        	setText(getCell(sheet, row, headerMap.get("ContactName").shortValue()), (task.getContactName()  == null) ? "" : task.getContactName().toString());
        	setText(getCell(sheet, row, headerMap.get("Title").shortValue()), (task.getTitle() == null) ? "" : task.getTitle().toString());
        	
        	setText(getCell(sheet, row, headerMap.get("Type").shortValue()), (task.getType() == null) ? "" : task.getType().toString());
    		
        	// Assigned To
    		setText(getCell(sheet, row, headerMap.get("AssignedTo").shortValue()), (task.getAssignedTo() == null) ? "" : task.getAssignedTo());
    		
        	setText(getCell(sheet, row, headerMap.get("Description").shortValue()), (task.getDescription() == null) ? "" : task.getDescription().toString());
        	setText(getCell(sheet, row, headerMap.get("DueDate").shortValue()), (task.getDueDate() == null) ? "" : task.getDueDate().toString());
        	
        	// date created
        	if(task.getCreated() != null) {
        		cell = getCell(sheet, row, headerMap.get("Created").shortValue());
        		cell.setCellStyle(dateCellStyle);
        		cell.setCellValue(sdfDate.format( task.getCreated() ));  
        	}
    		// date duedate
    		if(task.getDueDate() != null) {
    			cell = getCell(sheet, row, headerMap.get("DueDate").shortValue());
        		cell.setCellStyle(dateCellStyle);
        		cell.setCellValue(sdfDate.format( task.getDueDate() ));
            }
    		
        	setText(getCell(sheet, row, headerMap.get("Rank").shortValue()), (task.getPriority() == null) ? "" : task.getPriority().toString());
    		setText(getCell(sheet, row, headerMap.get("Status").shortValue()), (task.getStatus() == null) ? "" : task.getStatus().toString());
    		setText(getCell(sheet, row, headerMap.get("Action").shortValue()), (task.getAction() == null) ? "" : task.getAction().toString());
    		setText(getCell(sheet, row, headerMap.get("Email").shortValue()), (task.getContactEmail() == null) ? "" : task.getContactEmail().toString());
    		setText(getCell(sheet, row, headerMap.get("Phone").shortValue()), (task.getContactPhone() == null) ? "" : task.getContactPhone().toString());
    		
    	}	  
    	return wb;
    }
}