/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 03.24.2010
 */

package com.webjaguar.web.admin.crm;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.crm.CrmContactGroupSearch;

public class ContactGroupListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("crmContactGroupForm.jhtm"));		
		}	
		
		// search criteria
		CrmContactGroupSearch search = getCustomerGroupSearch( request );   	
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		PagedListHolder groupList = new PagedListHolder( this.webJaguar.getContactGroupList( search ));

		groupList.setPageSize(search.getPageSize());
		groupList.setPage(search.getPage()-1);

		myModel.put( "groups", groupList );
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)
			return new ModelAndView("admin-responsive/crm/contact/groups", "model", myModel);
		return new ModelAndView( "admin/crm/contact/groups", "model", myModel );
	}
    
	private CrmContactGroupSearch getCustomerGroupSearch(HttpServletRequest request) {
		CrmContactGroupSearch crmContactGroupSearch = (CrmContactGroupSearch) request.getSession().getAttribute( "groupCrmContactSearch" );
		if (crmContactGroupSearch == null) {
			crmContactGroupSearch = new CrmContactGroupSearch();
			request.getSession().setAttribute( "groupCrmContactSearch", crmContactGroupSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			crmContactGroupSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// group name
		if (request.getParameter("group") != null) {
			crmContactGroupSearch.setGroup(ServletRequestUtils.getStringParameter( request, "group", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				crmContactGroupSearch.setPage( 1 );
			} else {
				crmContactGroupSearch.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				crmContactGroupSearch.setPageSize( 10 );
			} else {
				crmContactGroupSearch.setPageSize( size );				
			}
		}
		
		return crmContactGroupSearch;
	}		
}