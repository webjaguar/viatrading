package com.webjaguar.web.admin.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.crm.CrmFormField;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.crm.CrmFormBuilderForm;

public class FormController  extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public FormController()
	{
		setCommandName( "crmFormBuilderForm" );
		setCommandClass( CrmFormBuilderForm.class );
		setPages( new String[] { "admin/crm/form/step0", "admin/crm/form/step1", "admin/crm/form/step2", "admin/crm/form/step3",
				"admin/crm/form/step4", "../../admin/crm/index" } );
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
	throws Exception {    	
	
	CrmFormBuilderForm crmFormBuilderForm = (CrmFormBuilderForm) command;
	
   	Map<String, Map> map = new HashMap<String, Map>();
	Map<String, Object> myModel = new HashMap<String, Object>();
			
	switch (page) {
		case 1:
			List<CrmFormField> list = crmFormBuilderForm.getCrmForm().getCrmFields();
			if( list == null ) {
				list = new ArrayList<CrmFormField>();
				for(int i=0; i< crmFormBuilderForm.getCrmForm().getNumberOfFields(); i++){
					list.add(new CrmFormField());
				}
				crmFormBuilderForm.getCrmForm().setCrmFields(list);
			} else if( list.size() < crmFormBuilderForm.getCrmForm().getNumberOfFields() ){
				int extraFields = crmFormBuilderForm.getCrmForm().getNumberOfFields() - list.size();
				for(int i=0; i< extraFields; i++){
					list.add(new CrmFormField());
				}
			} 
			myModel.put("nrOfFileds", crmFormBuilderForm.getCrmForm().getNumberOfFields());
			myModel.put("formFields", list);
			myModel.put("crmContactFields", this.webJaguar.getCrmContactFieldList(null));
			break;
		case 4:
			String form =  this.webJaguar.createForm(crmFormBuilderForm, request);
			myModel.put("form", form);
			
			String htmlCode = form.replaceAll("<", "&lt;");
			htmlCode = htmlCode.replaceAll(">", "&gt;") ;
			myModel.put("htmlCode", htmlCode);
			break;
		case 5:
			this.webJaguar.deleteCrmForm(crmFormBuilderForm.getCrmForm().getFormId());
			break;
		
	}	
	
	return myModel;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		CrmFormBuilderForm crmFormBuilderForm = new CrmFormBuilderForm();
		
		if ( request.getParameter( "id" ) != null) {
			crmFormBuilderForm.setCrmForm( this.webJaguar.getCrmFormById( ServletRequestUtils.getIntParameter(request, "id" , -1 ) ) );
			crmFormBuilderForm.setNewCrmForm( false );
		}
		return crmFormBuilderForm;
	}
	
	
	@Override
	protected ModelAndView processFinish(HttpServletRequest request,
			HttpServletResponse response, Object command, BindException eroors)
			throws Exception {
		CrmFormBuilderForm crmFormBuilderForm = (CrmFormBuilderForm) command;
		
		if(crmFormBuilderForm.isNewCrmForm()){
			this.webJaguar.insertCrmForm(crmFormBuilderForm.getCrmForm());
		} else {
			this.webJaguar.updateCrmForm(crmFormBuilderForm.getCrmForm());
		}
		return new ModelAndView( new RedirectView("crmFormList.jhtm") );
		
	}
	
	protected void validatePage(Object command, Errors errors, int page)  {
		CrmFormBuilderForm crmFormBuilderForm = (CrmFormBuilderForm) command;
		
		switch (page) {
			case 0:
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmForm.formName", "form.required", "required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmForm.recipientEmail", "form.required", "required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmForm.returnUrl", "form.required", "required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmForm.numberOfFields", "form.required", "required");
				if (crmFormBuilderForm.getCrmForm().getRecipientEmail() != null && !Constants.validateEmail(crmFormBuilderForm.getCrmForm().getRecipientEmail(), null)) {
					errors.rejectValue("crmForm.recipientEmail", "form.invalidEmail", "invalid email");
				}
				if (crmFormBuilderForm.getCrmForm().getRecipientCcEmail() != null && !crmFormBuilderForm.getCrmForm().getRecipientCcEmail().trim().isEmpty()) {
					for(String ccEmail : crmFormBuilderForm.getCrmForm().getRecipientCcEmail().split(",")) {
						if(!Constants.validateEmail(ccEmail.trim(), null)) {
							errors.rejectValue("crmForm.recipientCcEmail", "form.invalidEmail", "invalid email");
							break;
						}
					} 
				}
				if (crmFormBuilderForm.getCrmForm().getRecipientBccEmail() != null && !crmFormBuilderForm.getCrmForm().getRecipientBccEmail().trim().isEmpty()) {
					for(String bccEmail : crmFormBuilderForm.getCrmForm().getRecipientBccEmail().split(",")) {
						if(!Constants.validateEmail(bccEmail.trim(), null)) {
							errors.rejectValue("crmForm.recipientBccEmail", "form.invalidEmail", "invalid email");
							break;
						}
					} 
				}
				
				if (crmFormBuilderForm.getCrmForm().getReturnUrl() != null && !Constants.validateUrl(crmFormBuilderForm.getCrmForm().getReturnUrl())) {
					errors.rejectValue("crmForm.returnUrl", "form.invalidUrl", "invalid Url");
				}
				if(crmFormBuilderForm.getCrmForm().getNumberOfFields() != null && !crmFormBuilderForm.getCrmForm().getNumberOfFields().toString().matches("^[0-9]*$")) {
					errors.rejectValue("crmForm.numberOfFields", "form.crmFormBuilder.number", "Number Required");
				}
				break;
			case 1:
				List<Integer> contactFieldIds = new ArrayList<Integer>(); 
				for(int i=0;i<crmFormBuilderForm.getCrmForm().getNumberOfFields();i++){
					if(contactFieldIds.contains(crmFormBuilderForm.getCrmForm().getCrmFields().get(i).getContactFieldId())){
						errors.rejectValue("crmForm.crmFields["+i+"].contactFieldId", "ALREADY_MAPPED", "Already mapped" );
					} else {
						contactFieldIds.add(crmFormBuilderForm.getCrmForm().getCrmFields().get(i).getContactFieldId());
					}
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmForm.crmFields["+i+"].fieldName", "form.required", "required");
				}
				break;
			case 2:
				for(int i=0;i<crmFormBuilderForm.getCrmForm().getNumberOfFields();i++){
					if(crmFormBuilderForm.getCrmForm().getCrmFields().get(i).getType() > 2 && crmFormBuilderForm.getCrmForm().getCrmFields().get(i).getType() < 6) {
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmForm.crmFields["+i+"].options", "form.required", "required");
					}
				}
				break;
			case 3:
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmForm.sendButton", "form.required", "required");
				if( crmFormBuilderForm.getCrmForm().isDoubleOptIn() ){
					boolean mailAddress = false;
					for(int i=0;i<crmFormBuilderForm.getCrmForm().getNumberOfFields();i++){
						if( crmFormBuilderForm.getCrmForm().getCrmFields().get(i).getContactFieldId() == 1030 ){
							mailAddress = true;
							break;
						} 
					}
					if(!mailAddress){
						errors.rejectValue("crmForm.doubleOptIn", "EMAIL_REQUIRED", "Email field is required." );
					}
				}
				break;
				
		}
    }
	
}
