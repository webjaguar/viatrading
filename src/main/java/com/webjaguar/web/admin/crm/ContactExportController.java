/* Copyright 2005, 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 */

package com.webjaguar.web.admin.crm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactField;
import com.webjaguar.model.crm.CrmContactSearch;
import com.webjaguar.web.domain.Constants;

public class ContactExportController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private MailSender mailSender;
    public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	private File baseFile;
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
    	CrmContactSearch search = this.webJaguar.getCrmContactSearch( request );
    	
    	Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		search.setFileType(ServletRequestUtils.getStringParameter(request, "fileType", "xls"));
		baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			map.put("message", "excelfile.notWritable");
			return new ModelAndView("admin/crm/contact/export", map);			
		}
		File file[] = baseFile.listFiles();
		List<Map> exportedFiles = new ArrayList<Map>();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith("crmContactExport") & file[f].getName().endsWith(".xls")) {
				if (request.getParameter("__new") != null) { // check if new button was pressed
					file[f].delete();
				} else {
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					exportedFiles.add( fileMap );				
				}
			}
		}
		map.put("exportedFiles", exportedFiles);
    	
		// check if new button was pressed
		if (request.getParameter("__new") != null) {
			
			int count = this.webJaguar.getCrmContactListCount(search);
			if (search.getFileType().equals("xls")) {
				createExcelFiles(count, search, exportedFiles);				
			} else if (search.getFileType().equals("csv")){
				createCsvFile(count, search, exportedFiles);		
			}
	        
			if (count > 0) {
				map.put("arguments", count);
				map.put("message", "crmcontactexport.success");
				this.webJaguar.insertImportExportHistory( new ImportExportHistory("crmcontact", SecurityContextHolder.getContext().getAuthentication().getName(), false ));
				notifyAdmin(siteConfig, request);
			} else {
				map.put("message", "crmcontactexport.empty");				
			}
		} // if    
		
		return new ModelAndView("admin/crm/contact/export", map);
	}
    
   
	private void createExcelFiles( int count, CrmContactSearch search, List<Map> exportedFiles) throws Exception {
    	int limit = 3000;
		search.setLimit(limit);
		 search.setOffset(0);
		
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	String fileName = "/crmContactExport_" + dateFormatter.format(new Date()) + "";

    	for (int offset=0, excel = 1; offset<count; excel++) {
    		List<CrmContact> contacts = this.webJaguar.getCrmContactExport(search);
	    	int start = offset + 1;
	    	int end = (offset + limit) < count ? (offset + limit) : count;
	    	HSSFWorkbook wb = createWorkbook(contacts, start + " to " + end);
	    	// Write the output to a file
	    	File newFile = new File(baseFile.getAbsolutePath() + fileName + excel + ".xls");
	        FileOutputStream fileOut = new FileOutputStream(newFile);
	        wb.write(fileOut);
	        fileOut.close();
	        HashMap<String, Object> fileMap = new HashMap<String, Object>();
	        fileMap.put("file", newFile);
			exportedFiles.add( fileMap );  		
	        offset = offset + limit;
	        search.setOffset(offset);
	    }
    }
    
    private HSSFCell getCell(HSSFSheet sheet, int row, short col) {
    	return sheet.createRow(row).createCell(col);
    }
    
    private void setText(HSSFCell cell, String text) {
    	cell.setCellValue(text);
    }
    
    private HSSFWorkbook createWorkbook(List<CrmContact> contacts, String sheetName) throws Exception {
    	Iterator<CrmContact> iter = contacts.iterator();

    	HSSFWorkbook wb = new HSSFWorkbook();
    	HSSFSheet sheet = wb.createSheet(sheetName);
    	
    	Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
		Iterator<SalesRep> salesRepIter = this.webJaguar.getSalesRepList().iterator();
		while (salesRepIter.hasNext()) {
			SalesRep salesRep = salesRepIter.next();
			salesRepMap.put(salesRep.getId(), salesRep);
		}
    	
    	// create the headers
		Map<String, Short> headerMap = new HashMap<String, Short>();
		short col = 0;
		
		headerMap.put("Id", col++);
		headerMap.put("AccountName", col++);
		headerMap.put("FirstName", col++);
		headerMap.put("LastName", col++);
		headerMap.put("Title", col++);
		headerMap.put("Converted", col++);
		headerMap.put("CreatedBy", col++);
		headerMap.put("Created", col++);
		headerMap.put("ShortDescription", col++);
		headerMap.put("Description", col++);
		headerMap.put("Type", col++);
		headerMap.put("Rating", col++);
		headerMap.put("LeadSource", col++);
		headerMap.put("Qualifier", col++);
		headerMap.put("Trackcode", col++);
		headerMap.put("Department", col++);
		headerMap.put("Email1", col++);
		headerMap.put("Email2", col++);
		headerMap.put("Unsubscribe", col++);
		headerMap.put("Phone1", col++);
		headerMap.put("Phone2", col++);
		headerMap.put("Fax", col++);
		headerMap.put("Street", col++);
		headerMap.put("City", col++);
		headerMap.put("State", col++);
		headerMap.put("Zip", col++);
		headerMap.put("Country", col++);
		headerMap.put("OtherStreet", col++);
		headerMap.put("OtherCity", col++);
		headerMap.put("OtherState", col++);
		headerMap.put("OtherZip", col++);
		headerMap.put("OtherCountry", col++);
		
		if(contacts.size() != 0) {
			for(CrmContactField contactField : contacts.get(0).getCrmContactFields()){
				headerMap.put("field_"+contactField.getId(), col++);
			}
		}
		headerMap.put("AssignedTo", col++);
		
		HSSFCellStyle dateCellStyle = wb.createCellStyle();
		dateCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yy");
		
		HSSFCellStyle timeCellStyle = wb.createCellStyle();
		timeCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("h:mm:ss AM/PM"));
		SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm:ss a");
		
		// print headers
		for (String headerName:headerMap.keySet()){
			setText(getCell(sheet, 0, headerMap.get(headerName).shortValue()), headerName); 
		}
	
    	iter = contacts.iterator();
    	
    	HSSFCell cell = null;
		
		// create the rows
    	for (int row=2; iter.hasNext(); row++) {    		
    		CrmContact contact = (CrmContact) iter.next();
    		setText(getCell(sheet, row, headerMap.get("Id").shortValue()), contact.getId().toString());
        	setText(getCell(sheet, row, headerMap.get("AccountName").shortValue()), contact.getAccountName().toString());
        	setText(getCell(sheet, row, headerMap.get("FirstName").shortValue()), (contact.getFirstName() == null) ? "" : contact.getFirstName().toString());
        	setText(getCell(sheet, row, headerMap.get("LastName").shortValue()), (contact.getLastName() == null) ? "" : contact.getLastName().toString());
        	setText(getCell(sheet, row, headerMap.get("Title").shortValue()), (contact.getTitle() == null) ? "" : contact.getTitle().toString());
        	setText(getCell(sheet, row, headerMap.get("Converted").shortValue()), (contact.getUserId() == null) ? null : contact.getUserId().toString());
        	
        	setText(getCell(sheet, row, headerMap.get("Created").shortValue()), (contact.getCreated() == null) ? "" : contact.getCreated().toString());
        	setText(getCell(sheet, row, headerMap.get("CreatedBy").shortValue()), (contact.getCreatedBy() == null) ? "" : contact.getCreatedBy().toString());
        	// date ordered
    		cell = getCell(sheet, row, headerMap.get(getMessageSourceAccessor().getMessage("created")).shortValue());
    		cell.setCellStyle(dateCellStyle);
    		cell.setCellValue((contact.getCreated() != null) ? (sdfDate.format( contact.getCreated())) : "");  
        	
    		setText(getCell(sheet, row, headerMap.get("Type").shortValue()), (contact.getContactType() == null) ? "" : contact.getContactType().toString());
    		setText(getCell(sheet, row, headerMap.get("Rating").shortValue()), (contact.getRating() == null) ? "" : contact.getRating().toString());
    		setText(getCell(sheet, row, headerMap.get("LeadSource").shortValue()), (contact.getLeadSource() == null) ? "" : contact.getLeadSource().toString());
    		
    		String qualifier = "";
    		if(contact.getQualifier() != null){
    			if(salesRepMap.get(contact.getQualifier())!=null){
    				qualifier = salesRepMap.get(contact.getQualifier()).getName();
    			}
    		}
    		setText(getCell(sheet, row, headerMap.get("Qualifier").shortValue()), qualifier);
    		setText(getCell(sheet, row, headerMap.get("Trackcode").shortValue()), (contact.getTrackcode() == null) ? "" : contact.getTrackcode().toString());
    		setText(getCell(sheet, row, headerMap.get("ShortDescription").shortValue()), (contact.getShortDesc() == null) ? "" : contact.getShortDesc().toString());
    		setText(getCell(sheet, row, headerMap.get("Description").shortValue()), (contact.getDescription() == null) ? "" : contact.getDescription().toString());
    		setText(getCell(sheet, row, headerMap.get("Department").shortValue()), (contact.getDepartment() == null) ? "" : contact.getDepartment().toString());
    		setText(getCell(sheet, row, headerMap.get("Email1").shortValue()), (contact.getEmail1() == null) ? "" : contact.getEmail1().toString());
        	setText(getCell(sheet, row, headerMap.get("Email2").shortValue()), (contact.getEmail2() == null) ? "" : contact.getEmail2().toString());
        	setText(getCell(sheet, row, headerMap.get("Unsubscribe").shortValue()), contact.isUnsubscribe() ? "1" : null);
        	setText(getCell(sheet, row, headerMap.get("Phone1").shortValue()), (contact.getPhone1() == null) ? "" : contact.getPhone1().toString());
        	setText(getCell(sheet, row, headerMap.get("Phone2").shortValue()), (contact.getPhone2() == null) ? "" : contact.getPhone2().toString());
        	setText(getCell(sheet, row, headerMap.get("Fax").shortValue()), (contact.getFax() == null) ? "" : contact.getFax().toString());
        	// address
    		setText(getCell(sheet, row, headerMap.get("Street").shortValue()), (contact.getStreet() == null) ? "" : contact.getStreet().toString());
    		setText(getCell(sheet, row, headerMap.get("City").shortValue()), (contact.getCity() == null) ? "" : contact.getCity().toString());
    		setText(getCell(sheet, row, headerMap.get("State").shortValue()), (contact.getStateProvince() == null) ? "" : contact.getStateProvince().toString());
    		setText(getCell(sheet, row, headerMap.get("Zip").shortValue()), (contact.getZip() == null) ? "" : contact.getZip().toString());
    		setText(getCell(sheet, row, headerMap.get("Country").shortValue()), (contact.getCountry() == null) ? "" : contact.getCountry().toString());
    		// other address
    		setText(getCell(sheet, row, headerMap.get("OtherStreet").shortValue()), (contact.getOtherStreet() == null) ? "" : contact.getOtherStreet().toString());
    		setText(getCell(sheet, row, headerMap.get("OtherCity").shortValue()), (contact.getOtherCity() == null) ? "" : contact.getOtherCity().toString());
    		setText(getCell(sheet, row, headerMap.get("OtherState").shortValue()), (contact.getOtherStateProvince() == null) ? "" : contact.getOtherStateProvince().toString());
    		setText(getCell(sheet, row, headerMap.get("OtherZip").shortValue()), (contact.getOtherZip() == null) ? "" : contact.getOtherZip().toString());
    		setText(getCell(sheet, row, headerMap.get("OtherCountry").shortValue()), (contact.getOtherCountry() == null) ? "" : contact.getOtherCountry().toString());
    		
    		// contact fields
    		for(CrmContactField contactField : contact.getCrmContactFields()){
    			setText(getCell(sheet, row, headerMap.get("field_"+contactField.getId()).shortValue()), (contactField.getFieldValue() == null) ? "" : contactField.getFieldValue());
        	}
    		
    		// Assigned To
    		List<AccessUser> users = this.webJaguar.getUserPrivilegeList(null);
    		Map<Integer, String> userMap = new HashMap<Integer, String>();
    		for(AccessUser user: users){
    			userMap.put(user.getId(), user.getUsername());
    		}
    		setText(getCell(sheet, row, headerMap.get("AssignedTo").shortValue()), (userMap.get(contact.getAccessUserId()) == null) ? "" : userMap.get(contact.getAccessUserId()));
    		
        	
    	}	  
    	return wb;
    }
    
    private void createCsvFile(int orderCount, CrmContactSearch search,List<Map> exportedFiles) throws Exception { 
    	
    	search.setLimit(null);
    	List<CrmContact> contacts = this.webJaguar.getCrmContactExport(search);
    	if (orderCount < 1) return;
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    	SimpleDateFormat sdfDate = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm:ss a");
    	
		String fileName = "/crmContactExport_" + dateFormatter.format(new Date()) + ".csv";
    	
    	File exportFile = new File(baseFile, fileName);
    	CSVWriter writer = new CSVWriter(new FileWriter(exportFile));
    	List<String> line = new ArrayList<String>();
    	
    	dateFormatter = new SimpleDateFormat("MM/dd/yy kk:mm");
    	//header name
    	line.add("id");
    	line.add("accountname");
    	line.add("firstname");
    	line.add("lastname");
    	line.add("title");
    	line.add("converted");
    	line.add("createdby");
    	line.add("created");
    	line.add("accountCreatedMonth");
    	line.add("accountCreatedQTR");
    	line.add("accountCreatedYear");
    	line.add("shortDescription");
    	line.add("description");
    	line.add("type");
    	line.add("rating");
    	line.add("leadSource");
    	line.add("qualifier");
    	line.add("trackcode");
    	line.add("department");
    	line.add("email1");
    	line.add("email2");
    	line.add("unsubscribe");
    	line.add("phone1");
    	line.add("phone2");
    	line.add("Fax");
    	line.add("Street");
    	line.add("City");
    	line.add("State");
    	line.add("Zip");
    	line.add("Country");
    	line.add("OtherStreet");
    	line.add("OtherCity");
    	line.add("OtherState");
    	line.add("OtherZip");
    	line.add("OtherCountry");
    	if(contacts != null) {
   			for(CrmContactField contactField : contacts.get(0).getCrmContactFields()){
				line.add("field_"+contactField.getId());
			}
		}
		line.add("AssignedTo");
    	
		writer.writeNext(line.toArray(new String[line.size()]));
    	
		line = new ArrayList<String>();
		
		List<String> list = Arrays.asList("","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","");
    	line.addAll( list );
    	// customer fields
    	if(contacts != null) {
			for(CrmContactField contactField : contacts.get(0).getCrmContactFields()){
				line.add("(" + contactField.getFieldName() + ")");
			}
		}
    	writer.writeNext(line.toArray(new String[line.size()]));
    	
    	// create the rows
		List<AccessUser> users = this.webJaguar.getUserPrivilegeList(null);
		Map<Integer, String> userMap = new HashMap<Integer, String>();
		for(AccessUser user: users){
			userMap.put(user.getId(), user.getUsername());
		}
		for (CrmContact contact: contacts) { 
    		line = new ArrayList<String>();
    		
    		line.add(  contact.getId().toString() );
    		line.add(  contact.getAccountName().toString() );
    		line.add( (contact.getFirstName() == null) ? "" : contact.getFirstName().toString() );
    		line.add( (contact.getLastName() == null) ? "" : contact.getLastName().toString() );
    		line.add( (contact.getTitle() == null) ? "" : contact.getTitle().toString() );
    		line.add( (contact.getUserId() == null) ? null : contact.getUserId().toString() );
    		line.add( (contact.getCreatedBy() == null) ? "" : contact.getCreatedBy().toString() );	
    		line.add(sdfDate.format( contact.getCreated() ));
    		
    		Calendar contactCreatedDateCalendar = new GregorianCalendar();
    		contactCreatedDateCalendar.setTime( contact.getCreated() );
    		
    		line.add( Constants.getMonth(contactCreatedDateCalendar) );
    		line.add( Constants.getQuarter(contactCreatedDateCalendar) );
    		line.add( contactCreatedDateCalendar.get( contactCreatedDateCalendar.YEAR ) + "" );
    		line.add((contact.getShortDesc() == null) ? "" : contact.getShortDesc());
    		line.add((contact.getDescription() == null) ? "" : contact.getDescription());
    		line.add((contact.getContactType() == null) ? "" : contact.getContactType().toString());
    		line.add( (contact.getRating() == null) ? "" : contact.getRating().toString() );
    		line.add( (contact.getLeadSource() == null) ? "" : contact.getLeadSource().toString() );
    		line.add( (contact.getQualifier() == null) ? "" : contact.getQualifier().toString());
    		line.add( (contact.getTrackcode() == null) ? "" : contact.getTrackcode().toString() ); 
    		line.add( (contact.getDepartment() == null) ? "" : contact.getDepartment().toString() );
    		line.add( (contact.getEmail1() == null) ? "" : contact.getEmail1().toString() );
    		line.add( (contact.getEmail2() == null) ? "" : contact.getEmail2().toString() );
    		line.add( contact.isUnsubscribe() ? "1" : null );
	    	line.add( (contact.getPhone1() == null) ? "" : contact.getPhone1().toString() );
    		line.add( (contact.getPhone2() == null) ? "" : contact.getPhone2().toString() );
    		line.add( (contact.getFax() == null) ? "" : contact.getFax().toString() );
	    	line.add( (contact.getStreet() == null) ? "" : contact.getStreet().toString() );
    		line.add( (contact.getCity() == null) ? "" : contact.getCity().toString() );
    		line.add( (contact.getStateProvince() == null) ? "" : contact.getStateProvince().toString() );
    		line.add(  (contact.getZip() == null) ? "" : contact.getZip().toString() ); 			
    		line.add(  (contact.getCountry() == null) ? "" : contact.getCountry().toString() );
    		
    		line.add(  (contact.getOtherStreet() == null) ? "" : contact.getOtherStreet().toString() );
    		line.add( (contact.getOtherCity() == null) ? "" : contact.getOtherCity().toString() );
    		line.add( (contact.getOtherStateProvince() == null) ? "" : contact.getOtherStateProvince().toString() );
    		line.add( (contact.getOtherZip() == null) ? "" : contact.getOtherZip().toString() );
        	line.add( (contact.getOtherCountry() == null) ? "" : contact.getOtherCountry().toString() );
        	
        	for(CrmContactField contactField : contact.getCrmContactFields()){
        		line.add( (contactField.getFieldValue() == null) ? "" : contactField.getFieldValue() );
        	}
    		
        	line.add( (userMap.get(contact.getAccessUserId()) == null) ? "" : userMap.get(contact.getAccessUserId()) );
        	writer.writeNext(line.toArray(new String[line.size()]));
    		
		}
    	
    	writer.close();
    	HashMap<String, Object> fileMap = new HashMap<String, Object>();
        fileMap.put("file", exportFile);
		exportedFiles.add( fileMap );
    	
    }
    
    private void notifyAdmin(Map<String, Configuration> siteConfig, HttpServletRequest request){
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	StringBuffer message = new StringBuffer();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	AccessUser accessUser = (AccessUser) request.getAttribute( "accessUser" );
    	message.append("user "+accessUser.getUsername()+" has exported CrmContact file");
    	SimpleMailMessage msg = new SimpleMailMessage();			
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Automated Export on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
	
}