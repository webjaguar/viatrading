/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.01.2010
 */

package com.webjaguar.web.admin.crm;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.crm.CrmAccountForm;
import com.webjaguar.web.form.crm.CrmQualifierForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.crm.CrmAccount;
import com.webjaguar.model.crm.CrmQualifier;

public class QualifierFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private Map<String, Configuration> siteConfig;
	
	public QualifierFormController() {
		setSessionForm(true);
		setCommandName("crmQualifierForm");
		setCommandClass(CrmQualifierForm.class);
		setFormView("admin/crm/qualifier/form");
		setSuccessView("crmQualifierList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
		
		CrmQualifierForm crmQualifierForm = (CrmQualifierForm) command;

		if (request.getParameter("_delete") != null && crmQualifierForm.getCrmQualifier().getId() != 1) {
			this.webJaguar.deleteCrmQualifier(crmQualifierForm.getCrmQualifier().getId());	
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		if (crmQualifierForm.isNewCrmQualifier()) {
			this.webJaguar.insertCrmQualifier(crmQualifierForm.getCrmQualifier());
		} else {
			this.webJaguar.updateCrmQualifier(crmQualifierForm.getCrmQualifier());
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null) {
			return true;			
		}
		return false;
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);		
	}	
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		CrmQualifierForm crmQualifierForm = (CrmQualifierForm) command;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmQualifier.name", "form.required", "Qualifier Name is required.");    	
		//CrmQualifier crmQualifier = this.webJaguar.getCrmQualifierByName(crmQualifierForm.getCrmQualifier().getQualifierName());
		//if(crmQualifierForm.isNewCrmQualifier() && crmQualifier != null){
		//	errors.rejectValue("crmQualifier.qualifierName", "QUALIFIER_ALREADY_EXIST", "Qualifier Name already exist."); 
		//}
		//if ( !crmQualifierForm.isNewCrmQualifier() && crmQualifier != null && !crmQualifierForm.getCrmQualifier().getId().equals(crmQualifier.getId()) ) {
		//	errors.rejectValue("crmQualifier.qualifierName", "QUALIFIER_ALREADY_EXIST", "Qualifier Name already exist."); 
		//}
	}
	
   protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
	   CrmQualifierForm crmQualifierForm = new CrmQualifierForm();
		if ( request.getParameter("id") != null ) {
			crmQualifierForm = new CrmQualifierForm(this.webJaguar.getCrmQualifierById(new Integer(request.getParameter("id"))));
		} 

		return crmQualifierForm;
	} 
   
   protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
	   CrmQualifierForm crmQualifierForm = (CrmQualifierForm) command;
	   siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
	   Map<String, Object> map = new HashMap<String, Object>();
		map.put("states",this.webJaguar.getStateList("US"));
		map.put("caProvinceList", this.webJaguar.getStateList("CA"));
   	
	   map.put( "countries",this.webJaguar.getEnabledCountryList());
   	   map.put("accessUsers", this.webJaguar.getUserPrivilegeList(null));	
   	   map.put("rating", siteConfig.get("CRM_RATING").getValue());	
   	   map.put("types", siteConfig.get("CRM_ACCOUNT_CONTACT_TYPE").getValue());	
	   map.put("industry", siteConfig.get("CRM_ACCOUNT_INDUSTRY").getValue());	
	   return map;
   }
}