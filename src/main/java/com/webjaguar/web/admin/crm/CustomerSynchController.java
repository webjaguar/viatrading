package com.webjaguar.web.admin.crm;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;

public class CustomerSynchController implements Controller{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		if ((Boolean) gSiteConfig.get("gCRM") && !siteConfig.get("CRM_CUSTOMER_SYNCH").getValue().equals("true")) {
			int count = this.webJaguar.getCustomersCount();
			
			CustomerSearch search = new CustomerSearch();
			search.setLimit(3000);
			
			for (int offset=0; offset<count; ) {
		    	
				search.setOffset(offset);
		    	List<Customer> list = this.webJaguar.getCustomers(search);
		    	
		    	for(int i=0;i<list.size();i++){
		    		this.webJaguar.insertCrmAccountContact(list.get(i), null);
		    	}
				offset = offset + 3000;
		    }
		}
		this.webJaguar.updateSiteConfig("CRM_CUSTOMER_SYNCH", "true");
		return new ModelAndView(new RedirectView( "crmAccountList.jhtm" ));
	}

}
