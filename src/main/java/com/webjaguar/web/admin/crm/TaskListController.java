/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.01.2010
 */

package com.webjaguar.web.admin.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.crm.CrmTask;
import com.webjaguar.model.crm.CrmTaskSearch;


public class TaskListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
		CrmTaskSearch crmTaskSearch = this.webJaguar.getCrmTaskSearch( request );
		
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		Map<String, Object> myModel = new HashMap<String, Object>();
		Map<Integer, AccessUser> accessUserMap = new HashMap<Integer, AccessUser>();
		AccessUser accessUser = this.webJaguar.getAccessUser(request);
		myModel.put("accessUser", accessUser);
		
		List<AccessUser> accessUserList = this.webJaguar.getUserPrivilegeList(null);
		for(AccessUser accUser : accessUserList) {
			accessUserMap.put(accUser.getId(), accUser);
		}
		myModel.put("accessUserMap", accessUserMap);
		
		// get sales rep
		List<SalesRep> salesReps = null;
		
		Integer salesRepId = null;
		if (accessUser.isSalesRepFilter()) {
			salesRepId = accessUser.getSalesRepId();
		}

		if (salesRepId == null) {
			salesReps = this.webJaguar.getSalesRepList();				
		} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
			salesReps = this.webJaguar.getSalesRepList(salesRepId);
		} else {
			salesReps = new ArrayList<SalesRep>();
			salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
		}
		
		if (salesRepId != null) {
			// associate subaccount
			for (SalesRep sRep: salesReps) {
				if (sRep.getParent() != null && sRep.getParent().equals(salesRepId)) {
					AccessUser accUser = this.webJaguar.getAccessUserBySalesRepId(sRep.getId());
					if(accUser != null) {
						accessUser.getSubAccounts().add(accUser);
					}
				}
			}
			myModel.put("accessUserTree", accessUser);
		}	
	
		
		
		
		// check if options button was clicked
		if (request.getParameter("__delete") != null) {
			// get task ids
			Set<Integer> taskIds = new HashSet<Integer>();
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null ) {
				for (int i=0; i<ids.length; i++) {
					taskIds.add(Integer.parseInt(ids[i]));
				}
				this.webJaguar.batchCrmTaskDelete( taskIds);
			}
		}
		
		// set end date to end of day
		if (crmTaskSearch.getEndDate() != null) {
			crmTaskSearch.getEndDate().setTime(crmTaskSearch.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds	
		}
		
		int count = this.webJaguar.getCrmTaskListCount(crmTaskSearch);
		if (count < crmTaskSearch.getOffset()-1) {
			crmTaskSearch.setPage(1);
		}
		crmTaskSearch.setLimit(crmTaskSearch.getPageSize());
		crmTaskSearch.setOffset((crmTaskSearch.getPage()-1)*crmTaskSearch.getPageSize());
		List<CrmTask> crmTaskList = this.webJaguar.getCrmTaskList(crmTaskSearch);
		myModel.put("crmTaskList", crmTaskList);
		
		int pageCount = count/crmTaskSearch.getPageSize();
		if (count%crmTaskSearch.getPageSize() > 0) {
			pageCount++;
		}
		
		myModel.put("userList", this.webJaguar.getUserPrivilegeList(null) );
	//	myModel.put("accounts", this.webJaguar.getCrmAccountNameList());
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", crmTaskSearch.getOffset()+crmTaskList.size());
		myModel.put("count", count);
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			myModel.put("_pageinfo", "crmContactListpage");
			return new ModelAndView("admin-responsive/crm/task/list", "model", myModel);
		}
		return new ModelAndView("admin/crm/task/list", "model", myModel);
	}
}