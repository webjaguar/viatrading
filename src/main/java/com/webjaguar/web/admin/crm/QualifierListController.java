/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.01.2010
 */

package com.webjaguar.web.admin.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.crm.CrmAccount;
import com.webjaguar.model.crm.CrmAccountSearch;
import com.webjaguar.model.crm.CrmQualifier;
import com.webjaguar.model.crm.CrmQualifierSearch;


public class QualifierListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
    	
		CrmQualifierSearch crmQualifierSearch = getCrmQualifierSearch(request);
		Map<String, Object> myModel = new HashMap<String, Object>();
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("crmQualifierForm.jhtm"));
		}
		
		int count = this.webJaguar.getCrmQualifierListCount(crmQualifierSearch);
		if (count < crmQualifierSearch.getOffset()-1) {
			crmQualifierSearch.setPage(1);
		}
		crmQualifierSearch.setLimit(crmQualifierSearch.getPageSize());
		crmQualifierSearch.setOffset((crmQualifierSearch.getPage()-1)*crmQualifierSearch.getPageSize());
		List<CrmQualifier> crmQualifierList = this.webJaguar.getCrmQualifierList(crmQualifierSearch);
		myModel.put("crmQualifierList", crmQualifierList);
		
		int pageCount = count/crmQualifierSearch.getPageSize();
		if (count%crmQualifierSearch.getPageSize() > 0) {
			pageCount++;
		}
		
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", crmQualifierSearch.getOffset()+crmQualifierList.size());
		myModel.put("count", count);

		return new ModelAndView("admin/crm/qualifier/list", "model", myModel);
	}
	
	private CrmQualifierSearch getCrmQualifierSearch(HttpServletRequest request) {
		
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		CrmQualifierSearch crmQualifierSearch = (CrmQualifierSearch) request.getSession().getAttribute( "crmQualifierSearch" );
		if (crmQualifierSearch == null) {
			crmQualifierSearch = new CrmQualifierSearch();
			crmQualifierSearch.setPageSize( Integer.parseInt( siteConfig.get( "DEFAULT_LIST_SIZE" ).getValue()) );
			request.getSession().setAttribute( "crmAccountSearch", crmQualifierSearch );
		}
		// sorting
		if (request.getParameter("sort") != null) {
			crmQualifierSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "id" ));
		}

		 // account name
		if (request.getParameter("crmName") != null) {
			crmQualifierSearch.setName( ServletRequestUtils.getStringParameter( request, "crmName", "" ) );
		}
		
		// account Id
		if (request.getParameter("crmQualifierId") != null) {
			int accountId = ServletRequestUtils.getIntParameter( request, "crmQualifierId", -1 );
			if (accountId == -1) {
				crmQualifierSearch.setQualifierId(null);	
			} else {
				crmQualifierSearch.setQualifierId(accountId);	
			}
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				crmQualifierSearch.setPage( 1 );
			} else {
				crmQualifierSearch.setPage( page );				
			}
		}			
		
		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				crmQualifierSearch.setPageSize( 10 );
			} else {
				crmQualifierSearch.setPageSize( size );				
			}
		}
		
		return crmQualifierSearch;
	}	
}