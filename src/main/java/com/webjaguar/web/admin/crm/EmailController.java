/* Copyright 2010 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 07.16.2010
 */

package com.webjaguar.web.admin.crm;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.EmailMessageForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Promo;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmTask;

public class EmailController extends SimpleFormController {

	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	public EmailController() {
		setSessionForm(true);
		setCommandName("form");
		setCommandClass(EmailMessageForm.class);
		setFormView("admin/crm/contact/email");
		setSuccessView("crmContactList.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {

		EmailMessageForm form = (EmailMessageForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
		
        // cancel
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		if(siteConfig.get("DYNAMIC_PROMO").getValue().equals("true")){
			if(form!=null&&form.getMessage()!=null){
				Promo promo = new Promo();
				promo.setPercent(false);
				String promoCode = null;
				// Dynamic Elements For Promos 
				//random promocode generator 
				char[] chars = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();
				StringBuilder sb = new StringBuilder();
				Random random = new Random();
				for (int i = 0; i < 10; i++) {
					char c = chars[random.nextInt(chars.length)];
					sb.append(c);
				}
				String output = sb.toString();
				promoCode = output;
				form.setMessage(form.getMessage().replaceAll("#dynamicPromo#",promoCode));
				//percentage dynamic tag
    	
				StringBuffer sb2 = new StringBuffer(form.getMessage());
				Pattern pattern = Pattern.compile("#percentage.*");
				Matcher matcher = pattern.matcher(sb2);
				String tempPercentageNum = null;
				int percentageNum=0;
				while (matcher.find()){
					promo.setPercent(true);
					String temp = matcher.group().toString();
					tempPercentageNum = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
					percentageNum = Integer.parseInt(tempPercentageNum);
					form.setMessage(form.getMessage().replaceAll("#percentage,"+percentageNum+"#",""));
				}    
				//fixed dynamic tag
				Pattern pattern2 = Pattern.compile("#fixed.*");
				Matcher matcher2 = pattern2.matcher(sb2);
				int fixedNum=0;
				while(matcher2.find()){
					String temp = matcher2.group().toString();
					String fixedAmount = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
					fixedNum = Integer.parseInt(fixedAmount);
					form.setMessage(form.getMessage().replaceAll("#fixed,"+fixedNum+"#",""));
				}	
				// Replacing amount with percentage or fixed
				Pattern pattern3 = Pattern.compile("#amount.*");
				Matcher matcher3 = pattern3.matcher(sb2);
				String helperText = "";
				if(promo.isPercent()){
					helperText="%";
				}
				else{
					helperText="\\$";
				}
				int amountNum=0;
				while(matcher3.find()){
					String temp = matcher3.group().toString();
					String amount = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
					amountNum = Integer.parseInt(amount);
					form.setMessage(form.getMessage().replaceAll("#amount,"+amountNum+"#",""+amountNum+""+helperText+"")); 
				}
				// replacing delta tag
				Pattern pattern4 = Pattern.compile("#delta.*");
				Matcher matcher4 = pattern4.matcher(sb2);
				int daysNum=0;
				Date date = null;
				Date endDate = null;
				while(matcher4.find()){
					String temp = matcher4.group().toString();
					//System.out.println("temp is---" +temp);
					String days = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
					daysNum = Integer.parseInt(days);
					//System.out.println(daysNum+"---number of dats"); 
					// setting start and end dates to the promo
					Calendar cal = Calendar.getInstance();
					date = new Date();
					cal.setTime(date);
					cal.add(Calendar.DATE,daysNum);
					endDate = cal.getTime();
					form.setMessage(form.getMessage().replaceAll("#delta,"+daysNum+"#",""+endDate+""));
				}
				//replacing hours tag
				Pattern pattern5 = Pattern.compile("hours.*");
				Matcher matcher5 = pattern5.matcher(sb2);
				while(matcher5.find()){
					String temp = matcher5.group().toString();
					//System.out.println("temp is---" +temp);
					String hours = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
					Calendar cal = Calendar.getInstance();
					date = new Date();
					cal.setTime(date);
					cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(hours));
					endDate = cal.getTime();
					form.setMessage(form.getMessage().replaceAll("#hours,"+hours+"#",""+endDate+"")); 
				}
				// Writing into promo table in the database
				if(amountNum != 0) {
					promo.setStartDate(date);
					promo.setEndDate(endDate);
					promo.setTitle(promoCode);
					promo.setDiscount((double)amountNum);
					promo.setOnetimeUseOnly(false);
					promo.setOnetimeUsePerCustomer(false);
					promo.setMinOrder(0.00);
					this.webJaguar.insertPromo(promo);
				}

			}
		}
		
		// construct email message
		MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
		try {
			CrmTask task = new CrmTask();
			constructMessage(helper, form, request, task);
			mailSender.send(mms);
			model.put("message", "email.sent" );
			if(siteConfig.get("CREATE_TASK_ON_EMAIL").getValue().equalsIgnoreCase("true")) {
				this.webJaguar.insertCrmTask(task);
			}
		} catch (Exception e) {
			model.put("message", "email.exception" );
		}
		
		return showForm(request, response, errors, model);
	}	
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		EmailMessageForm form = new EmailMessageForm();
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		
		int contactId = ServletRequestUtils.getIntParameter(request, "contactId", 0);
		
		CrmContact crmContact = this.webJaguar.getCrmContactById(contactId);
		if (crmContact != null) {
			form.setRecipientId(crmContact.getId());
			form.setUsername(crmContact.getEmail1());
			form.setTo(crmContact.getEmail1());
		} else {
			request.setAttribute("message", "customer.exception.notfound" );
		}
		AccessUser user = this.webJaguar.getAccessUserById(((AccessUser) request.getAttribute( "accessUser" )).getId());
		if (user!= null && user.getEmail()!=null) {
			form.setFrom(user.getEmail());
			form.setBcc( user.getEmail() );
		} else {
			form.setFrom( siteConfig.get( "CONTACT_EMAIL" ).getValue() );
			form.setBcc( siteConfig.get( "CONTACT_EMAIL" ).getValue() );
		}
		
		return form;
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors) {
		EmailMessageForm form = (EmailMessageForm) command;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("messages", this.webJaguar.getSiteMessageList());
		map.put("messageGroups", this.webJaguar.getSiteMessageGroupList()) ;
		CrmContact crmContact = this.webJaguar.getCrmContactById(form.getRecipientId());
		map.put("crmContact", crmContact);
		return map;
	}	
	
	private void constructMessage(MimeMessageHelper helper, EmailMessageForm form, HttpServletRequest request, CrmTask crmTask) throws Exception {
		CrmContact crmContact = this.webJaguar.getCrmContactById(form.getRecipientId());
		SalesRep salesRep = null;
		if ( (Boolean) gSiteConfig.get( "gSALES_REP" ) && crmContact.getAccessUserId() != null ) {
			AccessUser accessUser = this.webJaguar.getAccessUserById(crmContact.getAccessUserId());
			if(accessUser.getSalesRepId() != null){
				salesRep = this.webJaguar.getSalesRepById(accessUser.getSalesRepId());
			}
		}
		helper.setTo(form.getTo());
		// cc emails
		String[] emails = form.getCc().split( "[,]" ); // split by commas
		for ( int x = 0; x < emails.length; x++ ) {
			if (emails[x].trim().length() > 0) {
				helper.addCc(emails[x].trim());
			}
		}
		helper.setFrom(form.getFrom());	
		helper.setBcc(form.getBcc());
		
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));

    	StringBuffer message = new StringBuffer();
    	message.append( dateFormatter.format(new Date()) + "\n\n" );
    	
    	// replace dyamic elements
    	this.webJaguar.replaceDynamicElement(form, crmContact, salesRep);

    	helper.setSubject(form.getSubject());

    	message.append( form.getMessage() );
    	if ( form.isHtml() ) { 
    		helper.setText( form.getMessage(), form.isHtml() );
    	} else {
    		helper.setText(message.toString());
    	}
    	if(crmContact != null && siteConfig.get("CREATE_TASK_ON_EMAIL").getValue().equalsIgnoreCase("true")) {
    		crmTask.setType("Email");
    		crmTask.setCreated(new Date());
			crmTask.setContactId(crmContact.getId());
			crmTask.setContactEmail(crmContact.getEmail1());
			crmTask.setAccountId(crmContact.getAccountId());
			crmTask.setAccountName(crmContact.getAccountName());
			crmTask.setContactPhone(crmContact.getPhone1());
			crmTask.setAssignedToId(crmContact.getAccessUserId() != null ? crmContact.getAccessUserId() : 0 );
			
			crmTask.setTitle("Email :"+form.getSubject());
			crmTask.setDescription(form.getMessage());
		}
	}
}
