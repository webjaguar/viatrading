package com.webjaguar.web.admin.crm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.crm.CrmContactField;



public class ContactCustomerMapController extends SimpleFormController{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }		
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    	throws Exception {	

		Map<String, Object> model = new HashMap<String, Object>();    	

    	// check if update button was pressed
		if (request.getParameter("__update") != null) {
			updateSiteConfig(request);
			model.put("message", "update.successful");
		}
    	
		model.put("siteConfig", webJaguar.getSiteConfig());
      	model.put("data", this.webJaguar.getContcatCustomerMap("customer"));
      	model.put("fields", getContactProperties());
      	
		// Customer Fields
		List<CustomerField> customerFields = this.webJaguar.getCustomerFields();
		model.put("customerFields", customerFields);
		
    	return new ModelAndView("admin/crm/contact/mapCustomer", model);
    }
    
	private void updateSiteConfig(HttpServletRequest request) {

		List<Configuration> data = new ArrayList<Configuration>();

		String keys[] = request.getParameterValues("__key");
		
		for (int i=0; i<keys.length; i++){
			Configuration config = new Configuration();
			config.setKey(keys[i]);
			String value = request.getParameter(keys[i]);
			config.setValue(value);
			data.add(config); 
		}
		this.webJaguar.updateCustomerMap(data);	
	}
	
	private Map<String, String> getContactProperties() {
		Map<String, String> properties = new LinkedHashMap<String, String>();
		
		properties.put("First Name", "getFirstName");
		properties.put("Last Name", "getLastName");
		properties.put("Email1", "getEmail1");
		properties.put("Email2", "getEmail2");
		properties.put("Phone1", "getPhone1");
		properties.put("Phone2", "getPhone2");
		properties.put("Fax", "getFax");
		properties.put("Description", "getDescription");
		properties.put("LeadSource", "getLeadSource");
		properties.put("Department", "getDepartment");
		properties.put("Street", "getStreet");
		properties.put("City", "getCity");
		properties.put("StateProvince", "getStateProvince");
		properties.put("Zip", "getZip");
		properties.put("Country", "getCountry");
		properties.put("OtherStreet", "getOtherStreet");
		properties.put("OtherCity", "getOtherCity");
		properties.put("OtherStateProvince", "getOtherStateProvince");
		properties.put("OtherZip", "getOtherZip");
		properties.put("OtherCountry", "getOtherCountry");
		properties.put("Trackcode", "getTrackcode");
		properties.put("AddressType", "getAddressType");
		properties.put("OtherAddressType", "getOtherAddressType");
		properties.put("Qualifier", "getQualifier");
		properties.put("Language", "getLanguage");
		for(CrmContactField field : this.webJaguar.getCrmContactFieldList(null)) {
			properties.put(field.getFieldName(), "getCrmContactFields_"+field.getId());
		}
		
		return properties;
	}
	
}
