/* Copyright 2010 Advanced E-Media Solutions
	 * @author Shahin Naji
	 * @since 03.01.2010
	 */

package com.webjaguar.web.admin.crm;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.crm.CrmAccount;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.web.domain.Constants;

public class ContactImportController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
		
	private HSSFFormulaEvaluator evaluator;
	
	String formView = "admin/crm/contact/import";
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {	
    	
		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			return new ModelAndView("admin/crm/contact/import", "message", "excelfile.notWritable");	
		}    	
    	
		// check if upload button was pressed
		if (request.getParameter("__upload") != null) {
	    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	    	MultipartFile file = multipartRequest.getFile("file");
	    	if (file != null && !file.isEmpty()) {
	    		File excel_file = new File(baseFile, "crm_contact_import.xls");
	    		file.transferTo(excel_file);
	    		file = null; // delete to save resources
	            return processFile(excel_file, request);	    			
	    	} else {
	        	return new ModelAndView(formView, "message", "excelfile.invalid");	    		
	    	}
		}
		//System.out.println("ContactImportController");
    	return new ModelAndView(formView);
    }
    
    private ModelAndView processFile(File excel_file, HttpServletRequest request) {
        List<Map> addedItems = new ArrayList<Map>();
        List<Map> updatedItems = new ArrayList<Map>();
        List<Map> invalidItems = new ArrayList<Map>();
		List<CrmContact> importedCrmContacts = new ArrayList<CrmContact>();	
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		try {
			POIFSFileSystem fs =
	            new POIFSFileSystem(new FileInputStream(excel_file));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
		    
			HSSFSheet sheet = wb.getSheetAt(0);       	// first sheet
			HSSFRow row     = sheet.getRow(0);        	// first row
			
			evaluator = new HSSFFormulaEvaluator(sheet, wb);
			
			Map<String, Short> header = new HashMap<String, Short>();
			
			if (wb.getNumberOfSheets() > 1) {
				// multiple sheets
				map.put("message", "excelfile.multipleSheets");
				return new ModelAndView(formView, map);					
			}
			if (row == null) {
				// first line should be the headers
				map.put("message", "excelfile.emptyHeaders");
				return new ModelAndView(formView, map);					
			}
			
		    Iterator iter = row.cellIterator();
		    
		    // get header names
			while (iter.hasNext()) {
				HSSFCell cell = (HSSFCell) iter.next();
				String headerName = "";
				try {
					headerName = cell.getStringCellValue().trim();					
				} catch (NumberFormatException e) {
					// do nothing
				}
				if (!headerName.equals( "" )) {
					if (header.put(headerName.toLowerCase(), cell.getCellNum()) != null) {
						// duplicate header
						map.put("message", "excelfile.duplicateHeaders");
						map.put("arguments", headerName);
						return new ModelAndView(formView, map);								
					}				
				} 
			}
		    
			// check required headers
			if (!header.containsKey("accountname") || !header.containsKey("firstname") || !header.containsKey("lastname") ) { // account name is required
				return new ModelAndView(formView, "message", "excelfile.missingHeaders");
			}
			
			nextRow: for (int i=1; i<=sheet.getLastRowNum(); i++) {
				row = sheet.getRow(i);
				if (row == null) {
					continue;
				}
				
				String accountName = null;
				accountName = getStringValue(row, header.get("accountname")).trim();
				
				if (accountName.equals("")) accountName = null;
				Map<String, Object> thisMap = new HashMap<String, Object>();
				thisMap.put("accountName", accountName);
				
				if (accountName != null && !accountName.equals("")) {

				} else {
					continue nextRow;
				}
				
				
				Integer crmAccountId = null;
				boolean accountExist = this.webJaguar.isCrmAccountExist(accountName);
				if (!accountExist) { // account name does not exist
					thisMap.put("rowNum", row.getRowNum()+1);
					thisMap.put("reason", "Account Name Does not Exist");
					invalidItems.add(thisMap);
					continue nextRow;
				} else {
					// check required headers
					if (!header.containsKey("accountname") ) { // account number
						return new ModelAndView(formView, "message", "excelfile.missingAccountNameHeaders");
					}
					
					if(getStringValue(row, header.get("accountname")).trim() != null) {
						CrmAccount account = this.webJaguar.getCrmAccountByName(getStringValue(row, header.get("accountname")).trim());
						crmAccountId = account.getId();
					}
				}
				
				if (invalidItems.size() == 0) { // no errors
					// save or update contact
					CrmContact crmContact = null;
					//String firstName = getStringValue(row, header.get("firstname")).trim();
					//String lastName = getStringValue(row, header.get("lastname")).trim();
					if( getStringValue(row, header.get("id")).trim() != null && !getStringValue(row, header.get("id")).equals("") ){
						crmContact = this.webJaguar.getCrmContactById(Integer.parseInt(getStringValue(row, header.get("id")).trim()));
					}
					
					thisMap.put("accountname", accountName);
					thisMap.put("firstname", getStringValue(row, header.get("firstname")).trim());
					thisMap.put("lastname", getStringValue(row, header.get("lastname")).trim());
					if (crmContact != null) { 		// existing contacts
						crmContact.setCrmContactFields(this.webJaguar.getCrmContactFieldList(null));
						processRow(row, header, crmContact, thisMap);						
						updatedItems.add(thisMap);						
					} else {						// new contacts
						crmContact = new CrmContact();
						crmContact.setAccountId(crmAccountId);
						crmContact.setCrmContactFields(this.webJaguar.getCrmContactFieldList(null));
						processRow(row, header, crmContact, thisMap);					
						addedItems.add(thisMap);
					}
					importedCrmContacts.add(crmContact);
				} // if no errors
			}					
		} catch (IOException e) {
			return new ModelAndView(formView, "message", "excelfile.invalid");
		} finally {
	    	// delete excel file
			excel_file.delete();	    			
		}
		
		if (invalidItems.size() > 0) { // has errors
			map.put("invalidItems", invalidItems);
			return new ModelAndView(formView, map);					
		} else {
			if (importedCrmContacts.size() == 0) {	// no Customers to import
				return new ModelAndView(formView, "message", "excelfile.empty");			
			} else {
		    	this.webJaguar.importCrmContacts(importedCrmContacts);
				this.webJaguar.insertImportExportHistory( new ImportExportHistory("crmcontact", SecurityContextHolder.getContext().getAuthentication().getName(), true ));
			}
		}
		
		map.put("updatedItems", updatedItems);
		map.put("addedItems", addedItems);
		return new ModelAndView("admin/crm/contact/importSuccess", map);
		
    }

    private Integer getIntegerValue(HSSFRow row, short cellnum) throws Exception {
		HSSFCell cell = row.getCell(cellnum);
    	Integer value = null;
    	
    	if (cell != null) {
    		switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = new Integer(cell.getStringCellValue());										
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Double cellValue = cell.getNumericCellValue();
					if (cellValue != cellValue.intValue()) {
						throw new Exception();	
					}
					value =  cellValue.intValue();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue formulaCellValue = evaluator.evaluate(cell);					
					if (formulaCellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						cellValue =  new Double(formulaCellValue.getNumberValue());
						if (cellValue != cellValue.intValue()) {
							throw new Exception();	
						}
						value =  cellValue.intValue();
					}
					break;
			} // switch
    	}
    	return value;
    }
    
    private String getStringValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		String value = "";

    	if (cell != null) {
			switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = cell.getStringCellValue();
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Integer intValue = null;
					try {
						intValue = getIntegerValue(row, cellnum);
					} catch (Exception e) {}
					if (intValue != null) {
						value = intValue.toString();
						break;
					}
					value = new Double(cell.getNumericCellValue()).toString();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue cellValue = evaluator.evaluate(cell);
					if (cellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						value = new Double(cellValue.getNumberValue()).toString();						
					}
					break;
			} // switch		
    	}
		return value;
    }
    
    private boolean getBooleanValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		boolean value = false;

    	if (cell != null) {
    		if (getStringValue(row, cellnum).trim().equals( "1" ))  {
    			value = true;
    		}
    	}
		return value;
    } 
 
    
    private void processRow(HSSFRow row, Map<String, Short> header, CrmContact contact, Map<String, Object> thisMap) {
    	NumberFormat formatter = new DecimalFormat("#");
    	StringBuffer comments = new StringBuffer();		

		// get account name
		if (header.containsKey("accountname")) {
			contact.setAccountName( getStringValue(row, header.get("accountname")) );
		}
		
		// get title
		if (header.containsKey("title")) {
			contact.setTitle( getStringValue(row, header.get("title")) );
		}
		
		// get firstname
		if (header.containsKey("firstname")) {
			contact.setFirstName( getStringValue(row, header.get("firstname")) );
		}
		
		// get lastname
		if (header.containsKey("lastname")) {
			contact.setLastName( getStringValue(row, header.get("lastname")) );
		}
		
		// get email1
		if (header.containsKey("email1")) {
			String email1 = getStringValue(row, header.get("email1")).trim();
			//System.out.println("processRow 326" + email1);
			if (!email1.equals( "" )) { // missing email1
				if (!Constants.validateEmail(email1, null)) {
					comments.append("email1 is not valid \"" + email1 + "\", ");
				} else {
					contact.setEmail1( email1 );
				}
			}
		}
		
		// get email2
		if (header.containsKey("email2")) {
			String email2 = getStringValue(row, header.get("email2")).trim();
			if (!email2.equals( "" )) { // missing email2
				if (!Constants.validateEmail(email2, null)) {
					comments.append("email2 is not valid \"" + email2 + "\", ");
				} else {
					contact.setEmail2( email2 );
				}
			}
		}
		
		// get unsubscribe
		if (header.containsKey("unsubscribe")) {
    		contact.setUnsubscribe( getBooleanValue(row, header.get("unsubscribe")) );	
    	}
	
		// get customer id
		if (header.containsKey("converted")) {
    		try {
				contact.setUserId( getIntegerValue(row, header.get("converted")) );
			} catch (Exception e) {}
		}
		
		// get phone1
		if (header.containsKey("phone1")) {
			try {
				if(row.getCell(header.get("phone1")).getNumericCellValue() != 0){
					contact.setPhone1( formatter.format(row.getCell(header.get("phone1")).getNumericCellValue()) );
				} else{
					contact.setPhone1( getStringValue(row, header.get("phone1")) );
				}
			} catch (Exception e) {
				contact.setPhone1( getStringValue(row, header.get("phone1")) );
			}
		}
		
		// get phone2
		if (header.containsKey("phone2")) {
			try {
				if(row.getCell(header.get("phone2")).getNumericCellValue() != 0){
					contact.setPhone2( formatter.format(row.getCell(header.get("phone2")).getNumericCellValue()) );
				} else{
					contact.setPhone2( getStringValue(row, header.get("phone2")) );
				}
			} catch (Exception e) {
				contact.setPhone2( getStringValue(row, header.get("phone2")) );
			}
		}
		
		// get fax
		if (header.containsKey("fax")) {
			contact.setFax( getStringValue(row, header.get("fax")) );
		}
		
		// get created by
		if (header.containsKey("createdby")) {
			contact.setCreatedBy( getStringValue(row, header.get("createdby")) );
		}
		
		// get created
		if (header.containsKey("created")) {
			if(getStringValue(row, header.get("created")) != null && !getStringValue(row, header.get("created")).equals("")){
				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				try {
					contact.setCreated(df.parse(getStringValue(row, header.get("created"))));
				} catch (ParseException e) {}
			}
    	}
	
		// get leadsource
		if (header.containsKey("leadsource")) {
			//System.out.println("processRow 408" + getStringValue(row, header.get("leadsource")));
			contact.setLeadSource( getStringValue(row, header.get("leadsource")) );
		}
		
		// get trackcode
		if (header.containsKey("trackcode")) {
			contact.setTrackcode( getStringValue(row, header.get("trackcode")) );
		}
		
		// get department
		if (header.containsKey("department")) {
			contact.setDepartment( getStringValue(row, header.get("department")) );
		}
		
		// get type
		if (header.containsKey("type")) {
			contact.setContactType( getStringValue(row, header.get("type")) );
		}
		
		// get rating
		if (header.containsKey("rating")) {
			contact.setRating( getStringValue(row, header.get("rating")) );
		}
		
		// get description
		if (header.containsKey("description")) {
		    contact.setDescription( getStringValue(row, header.get("description")) );
		}
		
		// get short description
		if (header.containsKey("shortdescription")) {
		    contact.setShortDesc( getStringValue(row, header.get("shortdescription")) );
		}
		   		
		// get street
		if (header.containsKey("street")) {
			contact.setStreet( getStringValue(row, header.get("street")) );
		}	
		
		// get city
		if (header.containsKey("city")) {
			contact.setCity( getStringValue(row, header.get("city")) );
		}		
		
		// get state/province
		if (header.containsKey("stateprovince")) {
			contact.setStateProvince( getStringValue(row, header.get("stateprovince")) );
		}			

		// get zip
		if (header.containsKey("zip")) {
			contact.setZip( getStringValue(row, header.get("zip")) );
		}
		
		// get country
		if (header.containsKey("country")) {
			contact.setCountry( getStringValue(row, header.get("country")) );
		}
		
		// get other street
		if (header.containsKey("otherstreet")) {
			contact.setOtherStreet( getStringValue(row, header.get("otherstreet")) );
		}	
		
		// get city
		if (header.containsKey("othercity")) {
			contact.setOtherCity( getStringValue(row, header.get("othercity")) );
		}		
		
		// get state/province
		if (header.containsKey("otherstateprovince")) {
			contact.setOtherStateProvince( getStringValue(row, header.get("otherstateprovince")) );
		}			

		// get zip
		if (header.containsKey("otherzip")) {
			contact.setOtherZip( getStringValue(row, header.get("otherzip")) );
		}
		
		// get country
		if (header.containsKey("othercountry")) {
			contact.setOtherCountry( getStringValue(row, header.get("othercountry")) );
		}

		// get user
		if (header.containsKey("assignedto") && !getStringValue(row, header.get("assignedto")).equals("")) {
			AccessUser user = this.webJaguar.getUserByUserName( getStringValue(row, header.get("assignedto")).trim() );
			contact.setAccessUserId( user != null ? user.getId() : null );
		}
		
		// get extra fields
		for (int ef=1; ef<=contact.getCrmContactFields().size(); ef++) {
			if (header.containsKey("field_" + contact.getCrmContactFields().get(ef-1).getId())) {
				contact.getCrmContactFields().get(ef-1).setFieldValue(getStringValue(row, header.get("field_" + contact.getCrmContactFields().get(ef-1).getId())) );
			}
		}
		
		thisMap.put("comments", comments);
    }
    
}