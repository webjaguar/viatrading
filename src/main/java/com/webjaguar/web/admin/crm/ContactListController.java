/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.01.2010
 */

package com.webjaguar.web.admin.crm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerGroupSearch;
import com.webjaguar.model.Language;
import com.webjaguar.model.QualifierSearch;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactSearch;
import com.webjaguar.model.crm.CrmTask;
import com.webjaguar.web.form.EmailMessageForm;


public class ContactListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private Map<String, Configuration> siteConfig;
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		CrmContactSearch crmContactSearch = this.webJaguar.getCrmContactSearch(request);
		// set end date to end of day
		if (crmContactSearch.getEndDate() != null) {
			crmContactSearch.getEndDate().setTime(crmContactSearch.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds	
		}
		Map<String, Object> myModel = new HashMap<String, Object>();
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		   
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			myModel.put("type", "withAccount");
			return new ModelAndView(new RedirectView("crmContactForm.jhtm") , myModel);
		}
		List<Integer> contactIds = this.webJaguar.getCrmContactIdsList(crmContactSearch);
		Map<Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
		Map<Integer, SalesRep> qualifierRepMap = new HashMap<Integer, SalesRep>();

		Map<Integer, AccessUser> accessUserMap = new LinkedHashMap<Integer, AccessUser>();
		Map<String, AccessUser> treeMap = new TreeMap<String, AccessUser>();
		
		AccessUser accessUser = this.webJaguar.getAccessUser(request);
		myModel.put("accessUser", accessUser);
		
		List<AccessUser> accessUserList = this.webJaguar.getUserPrivilegeList(null);
		
		for(AccessUser accUser : accessUserList) {
			treeMap.put(accUser.getUsername().toLowerCase(), accUser);
		}
		
		Set<String> keys = treeMap.keySet();
		for(String str : keys) {
			AccessUser accUser = treeMap.get(str);
			accessUserMap.put(accUser.getId(), accUser);	
		}
		myModel.put("accessUserMap", accessUserMap);
		
		// get sales rep
		List<SalesRep> salesReps = null;
		Map<Integer, String> qualifierMap = new HashMap<Integer, String>();

		List<SalesRep> qualifierList = this.webJaguar.getSalesRepList();
		
		for (SalesRep salesRep : qualifierList) {
			qualifierRepMap.put(salesRep.getId(), salesRep);
		}
		myModel.put("qualifierRepMap", qualifierRepMap);

		Integer salesRepId = null;
		if (accessUser.isSalesRepFilter()) {
			salesRepId = accessUser.getSalesRepId();
			crmContactSearch.setAccountManager(salesRepId);
		}

		if (salesRepId == null) {
			salesReps = this.webJaguar.getSalesRepList();				
		} else if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
			salesReps = this.webJaguar.getSalesRepList(salesRepId);
		} else {
			salesReps = new ArrayList<SalesRep>();
			salesReps.add(this.webJaguar.getSalesRepById(salesRepId));
		}
		
		if (salesRepId != null) {
			// associate subaccount
			for (SalesRep sRep: salesReps) {
				if (sRep.getParent() != null && sRep.getParent().equals(salesRepId)) {
					AccessUser accUser = this.webJaguar.getAccessUserBySalesRepId(sRep.getId());
					if(accUser != null) {
						accessUser.getSubAccounts().add(accUser);
					}
				}
			}
			myModel.put("accessUserTree", accessUser);
		}	
		
		for (SalesRep salesRep : salesReps) {
			salesRepMap.put(salesRep.getId(), salesRep);
		}
		myModel.put("salesRepMap", salesRepMap);
		
		QualifierSearch qualifierSearch = new QualifierSearch();
		qualifierSearch.setShowActiveQualifier(true);
		if (salesRepId != null) {
			myModel.put("activeQualifiers", salesReps);
		} else {
			myModel.put("activeQualifiers", this.webJaguar.getQualifierList(qualifierSearch));
		}
	
		// check if options button was clicked
		if (request.getParameter("__groupAssignPacher") != null) {
			// get group ids
			Set<Object> groups = new HashSet<Object>();
			String groupIdString = ServletRequestUtils.getStringParameter( request, "__group_id" );
			String batchType = ServletRequestUtils.getStringParameter( request, "__batch_type" );
			String[] groupIds = groupIdString.split( "," );
			for ( int x = 0; x < groupIds.length; x++ ) {
				if ( !("".equals( groupIds[x].trim() )) ) {
					try {
						groups.add( Integer.valueOf( groupIds[x].trim() ) );
					}catch ( Exception e ) {
						// do nothing
					}
				}
			}
			String ids[] = request.getParameterValues("__selected_id");
			if ((ids != null ) && ( request.getParameter("__groupAssignAll") == null)) {
				for (int i=0; i<ids.length; i++) {
					this.webJaguar.batchCrmContactGroupIds( Integer.parseInt(ids[i]), groups, batchType);
				}
			} else if( request.getParameter("__groupAssignAll") != null ) {
				for(Integer id: contactIds) {
					this.webJaguar.batchCrmContactGroupIds( id, groups, batchType);
				}
			}
		}
		// check if task is assigned to contacts
		if (request.getParameter("__taskAssignPacher") != null) {
			// get group ids
			CrmTask task;
			
			String ids[] = request.getParameterValues("__selected_id");
			
			List<Integer> selectedIds = new ArrayList<Integer>();
			if ((ids != null ) && ( request.getParameter("__taskAssignAll") == null)) {
				for (int i=0; i<ids.length; i++) {
					selectedIds.add(Integer.parseInt(ids[i]));
				}
			} else if( request.getParameter("__taskAssignAll") != null ) {
				selectedIds = contactIds;
			}
			
			for(Integer id : selectedIds) {
				task = new CrmTask();
				task.setType(ServletRequestUtils.getStringParameter(request, "task_type"));
				task.setTitle(ServletRequestUtils.getStringParameter(request, "task_title"));
				if(ServletRequestUtils.getIntParameter(request, "task_assignedTo") != -1) {
					task.setAssignedToId(ServletRequestUtils.getIntParameter(request, "task_assignedTo"));
				}
				task.setCreated(new Date());
				CrmContact contact = this.webJaguar.getCrmContactById(id);
				if(contact != null) {
					task.setContactId(id);
					task.setContactEmail(contact.getEmail1());
					task.setAccountId(contact.getAccountId());
					task.setAccountName(contact.getAccountName());
					task.setContactPhone(contact.getPhone1());
					if(ServletRequestUtils.getIntParameter(request, "task_assignedTo") == -1) {
						task.setAssignedToId(contact.getAccessUserId() != null ? contact.getAccessUserId() : 0 );
					}	
					this.webJaguar.insertCrmTask(task);
				}
			}
		}
		
		// check if user is assigned to contacts
		if (request.getParameter("__userAssignPacher") != null) {
			// get group ids
			CrmTask task;
			
			String ids[] = request.getParameterValues("__selected_id");
			List<Integer> selectedIds = new ArrayList<Integer>();
			if ((ids != null ) && ( request.getParameter("__userAssignAll") == null)) {
				for (int i=0; i<ids.length; i++) {
					selectedIds.add(Integer.parseInt(ids[i]));
				}
			} else if( request.getParameter("__userAssignAll") != null ) {
				selectedIds = contactIds;
			}
			if ( !selectedIds.isEmpty() ) {
				AccessUser accUser = null;
				if(ServletRequestUtils.getIntParameter(request, "contact_assignedTo") != null) {
					accUser = this.webJaguar.getAccessUserById(ServletRequestUtils.getIntParameter(request, "contact_assignedTo"));
				}
				this.webJaguar.batchAssignUserToContacts(selectedIds, accUser);
				notifySalesRep(siteConfig, selectedIds);
				
			}
		}
		
		// Viatrading not use it
		// check if rating is assigned to contacts
//		if (request.getParameter("__ratingAssignPacher") != null) {
//			String ids[] = request.getParameterValues("__selected_id");
//			List<Integer> selectedIds = new ArrayList<Integer>();
//			
//			if ((ids != null )&& ( request.getParameter("__ratingAssignAll") == null)) {
//				for (int i=0; i<ids.length; i++) {
//					selectedIds.add(Integer.parseInt(ids[i]));
//				}
//			} else if( request.getParameter("__ratingAssignAll") != null ) {
//					selectedIds = contactIds;
//			}
//			if ( !selectedIds.isEmpty() ) {
//					String rating =null;
//					if(ServletRequestUtils.getStringParameter(request, "__rating") != null) {
//						rating = ServletRequestUtils.getStringParameter(request, "__rating");
//					}
//					this.webJaguar.batchCrmRating(selectedIds, rating);
//			}
//		}
		
		// check if rating1 is assigned to contacts
		if (request.getParameter("__rating1AssignPacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			List<Integer> selectedIds = new ArrayList<Integer>();
			
			if ((ids != null )&& ( request.getParameter("__ratingAssignAll") == null)) {
				for (int i=0; i<ids.length; i++) {
					selectedIds.add(Integer.parseInt(ids[i]));
				}
			} else if( request.getParameter("__ratingAssignAll") != null ) {
					selectedIds = contactIds;
			}
			if ( !selectedIds.isEmpty() ) {
					String rating =null;
					if(ServletRequestUtils.getStringParameter(request, "__rating1") != null) {
						rating = ServletRequestUtils.getStringParameter(request, "__rating1");
					}
					this.webJaguar.batchCrmRating1(selectedIds, rating);
					this.webJaguar.batchCustomerRating1(this.webJaguar.getUserIdByCrmId(selectedIds),rating);
			}
		}
		
		// check if rating2 is assigned to contacts
		if (request.getParameter("__rating2AssignPacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			List<Integer> selectedIds = new ArrayList<Integer>();
			
			if ((ids != null )&& ( request.getParameter("__ratingAssignAll") == null)) {
				for (int i=0; i<ids.length; i++) {
					selectedIds.add(Integer.parseInt(ids[i]));
				}
			} else if( request.getParameter("__ratingAssignAll") != null ) {
					selectedIds = contactIds;
			}
			if ( !selectedIds.isEmpty() ) {
					String rating =null;
					if(ServletRequestUtils.getStringParameter(request, "__rating2") != null) {
						rating = ServletRequestUtils.getStringParameter(request, "__rating2");
					}
					this.webJaguar.batchCrmRating2(selectedIds, rating);
					this.webJaguar.batchCustomerRating2(this.webJaguar.getUserIdByCrmId(selectedIds),rating);
			}
		}
		
		// check if rating1 is assigned to contacts
		if (request.getParameter("__qualifierAssignPacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			List<Integer> selectedIds = new ArrayList<Integer>();
			
			if ((ids != null) && (request.getParameter("__qualifierAll") == null)) {
				for (int i=0; i<ids.length; i++) {
					selectedIds.add(Integer.parseInt(ids[i]));
				}
			} else if(request.getParameter("__qualifierAll") != null) {
					selectedIds = contactIds;
			}
			if (!selectedIds.isEmpty()) {
					String qualifier =null;
					if(ServletRequestUtils.getStringParameter(request, "__qualifier_field") != null) {
						qualifier = ServletRequestUtils.getStringParameter(request, "__qualifier_field");
					}
					try{
						this.webJaguar.batchCrmQualifier(selectedIds, qualifier);
						this.webJaguar.batchCustomerQualifier(selectedIds, qualifier);
					}catch(Exception ex){}
			}
		}
		
		// check if language is assigned to contacts
		if (request.getParameter("__languageAssignPacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			List<Integer> selectedIds = new ArrayList<Integer>();
			
			if ((ids != null) && (request.getParameter("__languageAll") == null)) {
				for (int i=0; i<ids.length; i++) {
					selectedIds.add(Integer.parseInt(ids[i]));
				}
			} else if(request.getParameter("__languageAll") != null) {
					selectedIds = contactIds;
			}
			if (!selectedIds.isEmpty()) {
					String language =null;
					if(ServletRequestUtils.getStringParameter(request, "__language_field") != null) {
						language = ServletRequestUtils.getStringParameter(request, "__language_field");
					}
					try{
						this.webJaguar.batchCrmLanguage(selectedIds, language);
						if(language==null || language.isEmpty()){
							this.webJaguar.batchCustomerLanguage(selectedIds, "en");
						}else{
							this.webJaguar.batchCustomerLanguage(selectedIds, language);
						}
					}catch(Exception ex)
					{
					}
			}
		}
				
		if (request.getParameter("__delete") != null) {
			// get task ids
			Set<Integer> contacts = new HashSet<Integer>();
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null ) {
				for (int i=0; i<ids.length; i++) {
					contacts.add(Integer.parseInt(ids[i]));
				}
				this.webJaguar.batchCrmContactDelete(contacts);
			}
		}
		
		if(request.getParameter("__unassign_user") != null){
			List<Integer> contacts = new ArrayList<Integer>();
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null ) {
				for (int i=0; i<ids.length; i++) {
					contacts.add(Integer.parseInt(ids[i]));
				}
			}
			this.webJaguar.removeUserFromContacts(contacts);
		}
		
		int count = this.webJaguar.getCrmContactListCount(crmContactSearch);
		if (count < crmContactSearch.getOffset()-1) {
			crmContactSearch.setPage(1);
		}
		crmContactSearch.setLimit(crmContactSearch.getPageSize());
		crmContactSearch.setOffset((crmContactSearch.getPage()-1)*crmContactSearch.getPageSize());
		List<CrmContact> crmContactList = this.webJaguar.getCrmContactList(crmContactSearch);
		myModel.put("crmContactList", crmContactList);
		
		int pageCount = count/crmContactSearch.getPageSize();
		if (count%crmContactSearch.getPageSize() > 0) {
			pageCount++;
		}
		
		myModel.put("groups", this.webJaguar.getContactGroupNameList());
		if ((Boolean) gSiteConfig.get("gGROUP_CUSTOMER")) {
			//for getting only active Customer groups
			CustomerGroupSearch customerGroupSearch = new CustomerGroupSearch();
			customerGroupSearch.setActive("1");
			myModel.put( "customerGroups", this.webJaguar.getCustomerGroupList(customerGroupSearch)) ;
			
		}
		myModel.put("countries",this.webJaguar.getEnabledCountryList());
	//	myModel.put("accounts", this.webJaguar.getCrmAccountNameList());
		myModel.put("contactFields", this.webJaguar.getCrmContactFieldList(null));
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", crmContactSearch.getOffset()+crmContactList.size());
		myModel.put("count", count);
		myModel.put("userList", this.webJaguar.getUserPrivilegeList(null) );
		myModel.put("taskTypes", siteConfig.get("CRM_TASK_TYPE").getValue());
		myModel.put("contactFields", this.webJaguar.getCrmContactFieldList(null));
		
	    if(((String) gSiteConfig.get("gI18N")).length()>0){
	    	myModel.put("languageCodes", this.webJaguar.getLanguageSettings());
	    }
		
		for(CrmContact crm : crmContactList){
			 if("fr".equalsIgnoreCase(crm.getLanguage())){
				 crm.setLanguage("French");
			 }
			 else if("de".equalsIgnoreCase(crm.getLanguage())){
				 crm.setLanguage("German");
			 }
			 else if("pt".equalsIgnoreCase(crm.getLanguage())){
				 crm.setLanguage("Portuguese");
			 }
			 else if("es".equalsIgnoreCase(crm.getLanguage())){
				 crm.setLanguage("Spanish");
			 }
			 else if("ar".equalsIgnoreCase(crm.getLanguage())){
				 crm.setLanguage("Arabic");
			 }
			 else if("en".equalsIgnoreCase(crm.getLanguage())){
				 crm.setLanguage("English");
			 }
		 }
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)
			return new ModelAndView("admin-responsive/crm/contact/list", "model", myModel);
		return new ModelAndView("admin/crm/contact/list", "model", myModel);
	}
	

	private void notifySalesRep(Map<String, Configuration> siteConfig, List<Integer> selectedIds) {
		for(Integer id: selectedIds){			
			CrmContact contact = this.webJaguar.getCrmContactById(id);
			AccessUser accessUser = this.webJaguar.getAccessUserById(contact.getAccessUserId());
	    	SalesRep salesRep = this.webJaguar.getSalesRepById( accessUser.getSalesRepId() );
	    	StringBuffer message = new StringBuffer();		
	    	EmailMessageForm form = new EmailMessageForm();
	    	form.setHtml(false);
	    	form.setSubject("Account Manager Notification");
	    	int messageId;
	    	if(siteConfig.get( "SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION_CONTACT" ).getValue()!=null){
	    		messageId= Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION_CONTACT" ).getValue() );
	    	}else{
	    		messageId=0;       	
	    	}
	    	SiteMessage siteMessage = this.webJaguar.getSiteMessageById( messageId );
			if(	siteMessage!=null&&!siteMessage.equals("")){
				
		    	if(siteMessage.isHtml()==true){
		    		form.setHtml(true);
		    	}
		    	message.append(siteMessage.getMessage());
		    	form.setSubject(siteMessage.getMessageName());
			}else{
				message.append("Hello #salesRepName#");
				message.append(",\n\n");
				message.append("This is to inform you that a new Contact #firstname# is assigned to you. \n\n");
				message.append("Dialing Notes: #dialingNotes# \n");
				message.append("Thanks \n");
				
			}
			form.setMessage(message.toString());
			if(salesRep!=null){
				form.setTo(salesRep.getEmail());
			}else{
				form.setTo(accessUser.getEmail());
			}
			form.setFrom(siteConfig.get("CONTACT_EMAIL").getValue());
			try {
		    	// construct email message
		       	MimeMessage mms = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
				if(salesRep!=null){
					this.webJaguar.replaceDynamicElement(form, contact, salesRep);
				}else{
					this.webJaguar.replaceDynamicElement(form, contact, accessUser);
				}
				helper.setTo(form.getTo());
				helper.setFrom(form.getFrom());
				helper.setSubject(form.getSubject());
				if ( form.isHtml()==true ) { 
		    		helper.setText(form.getMessage(), true );
		    	} else {
		    		helper.setText(form.getMessage());
		    	}
				mailSender.send(mms);
			} catch (Exception ex) {			
					ex.printStackTrace();					
			}  		
		}
		
		
	}
}