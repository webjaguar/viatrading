/* Copyright 2011 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 02.28.2011
 */

package com.webjaguar.web.admin.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ServiceSearch;
import com.webjaguar.web.domain.Constants;

public class IncompleteWorkOrderListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
    	
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		ServiceSearch search = getSearch(request);
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		
		PagedListHolder list = new PagedListHolder(this.webJaguar.getIncompleteWorkOrders(siteConfig.get("WO_ALERT_INTERVAL_CUSTOMER_FIELD").getValue(), Integer.parseInt(siteConfig.get("WO_ALERT_INTERVAL_DEFAULT").getValue())));		

		list.setPageSize(search.getPageSize());
		list.setPage(search.getPage()-1);
		
		model.put("list", list);
		
		model.put("countries", this.webJaguar.getCountryMap());
		model.put("statuses", Constants.getWorkOrderStatuses());
		
        return new ModelAndView("admin/service/incompleteWorkOrders", "model", model);
	}

	private ServiceSearch getSearch(HttpServletRequest request) {
		ServiceSearch search = (ServiceSearch) request.getSession().getAttribute("workOrderSearch");
		if (search == null) {
			search = new ServiceSearch();
			request.getSession().setAttribute("workOrderSearch", search);
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);				
			}
		}			
		
		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				search.setPageSize(10);
			} else {
				search.setPageSize(size);				
			}
		}
		
		return search;
	}	
	
}
