/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 01.15.2008
 */

package com.webjaguar.web.admin.service;

import java.lang.reflect.Method;
import java.sql.Time;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ServiceWork;
import com.webjaguar.model.WorkOrder;
import com.webjaguar.model.WorkOrderLineItem;
import com.webjaguar.web.domain.Constants;

public class WorkOrderFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public WorkOrderFormController() {
		setSessionForm(true);
		setCommandName("workOrder");
		setCommandClass(WorkOrder.class);
		setFormView("admin/service/workOrderForm");
		setSuccessView("services.jhtm");
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
		NumberFormat nf = NumberFormat.getInstance(RequestContextUtils.getLocale(request));
		binder.registerCustomEditor(Integer.class, new CustomNumberEditor(Integer.class, nf, true));
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(df, true, 10));		
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		
		WorkOrder workOrder = (WorkOrder) command;
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		// update line items
		updateLineItems(request, workOrder);
		
		// add sku
		if (request.getParameter("__sku") != null) {
			String sku = request.getParameter("__sku").trim();
			if (!sku.equals("")) {
				Integer existingId = this.webJaguar.getProductIdBySku(sku);
				Product product = this.webJaguar.getProductById(existingId, 0, false, null);
				if (product == null) {
					product = new Product();
					product.setSku(sku);
				}
				workOrder.insertLineItem(product);				
			}
		}
		
		// check if save or complete button was pressed
		if (request.getParameter("__save") != null || request.getParameter("__complete") != null) {			
			if (errors.hasErrors()) {
				return showForm(request, response, errors, map);				
			}
			
			// save work order
			this.webJaguar.updateWorkOrder(workOrder);	
			
			if (request.getParameter("__save") != null) {
				return new ModelAndView(new RedirectView(getSuccessView()));				
			} else {
				if (workOrder.getService().getStatus().equalsIgnoreCase("Canceled")) {
					// check if status is Canceled
					map.put("message",  "workorder.exception.canceled");
					return showForm(request, response, errors, map);					
				}
				if (workOrder.getService().getItem().getCustomer() == null) {
					// check if item belongs to a customer
					map.put("message",  "workorder.exception.nocustomer");
					return showForm(request, response, errors, map);					
				}
				
				return new ModelAndView(new RedirectView("../orders/addInvoice.jhtm"), "workOrder", workOrder.getService().getServiceNum());
			}
		}
		
		return showForm(request, response, errors, map);
	}	
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		WorkOrder workOrder = this.webJaguar.getWorkOrder(ServletRequestUtils.getIntParameter(request, "num", -1));
		if (workOrder == null) {
			ServiceWork service = this.webJaguar.getService(ServletRequestUtils.getIntParameter(request, "num", -1));
			if (service != null) {
				workOrder = new WorkOrder();
				workOrder.setNewWorkOrder(true);
				workOrder.setService(service);
			}
		}
		if (workOrder == null) {
			// check if work order/service is valid
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("services.jhtm")));
		} else if (workOrder.getService().getStatus().equalsIgnoreCase("completed")) {
			// check if work order is completed
			if (this.webJaguar.getAccessUser(request) != null && this.webJaguar.getAccessUser(request).getRoles() != null) {
				// accessUser with roles is neither ROLE_AEM nor ROLE_ADMIN_? (AdminInterceptor) 
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("workOrderPrint.jhtm"), "num", workOrder.getService().getServiceNum()));			
			}
		}
		
		if (workOrder.getService().getItem().getCustomer() != null) {
			workOrder.getService().getItem().setCustomer(this.webJaguar.getCustomerById(workOrder.getService().getItem().getCustomer().getId()));			
		}
		// get start time
		if (workOrder.getStartTime() != null) {
			workOrder.setStartHour(workOrder.getStartTime().toString().substring(0, 2));
			workOrder.setStartMin(workOrder.getStartTime().toString().substring(3, 5));
		}
		// get stop time
		if (workOrder.getStopTime() != null) {
			workOrder.setStopHour(workOrder.getStopTime().toString().substring(0, 2));
			workOrder.setStopMin(workOrder.getStopTime().toString().substring(3, 5));
		}
		workOrder.setDeletedLineItems(new HashSet<Integer>());
		
		return workOrder;
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors) {
		Map<String, Object> map = new HashMap<String, Object>();
		WorkOrder form = (WorkOrder) command;
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		Product product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(form.getService().getItem().getSku()), 0, false, null);
		if (product == null) {
			product = new Product();
		}
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		for (ProductField productField : this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"))) {
			if (productField.isEnabled() && productField.isServiceField()) {
				try {
					m = c.getMethod("getField" + productField.getId());
					productField.setValue((String) m.invoke(product, arglist));						
				} catch (Exception e) {
					// do nothing
				}
				product.addProductField(productField);
			}
		}			
		map.put("product", product);
		map.put("countries", this.webJaguar.getCountryMap());
		map.put("statuses", Constants.getWorkOrderStatuses());
		map.put("serviceTypes", Constants.getWorkOrderServiceTypes());
		
		// sub contractors
		List<Layout> subContractors = this.webJaguar.getSystemLayoutList("workorder");
		if (subContractors.size() > 1) {
			map.put("subContractors", subContractors);			
		}
		
		return map;
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) 
		throws Exception {
		
		WorkOrder workOrder = (WorkOrder) command;
		
		// get start time
		if (workOrder.getStartHour().length() > 0) {
			workOrder.setStartTime(Time.valueOf(workOrder.getStartHour() + ":" + workOrder.getStartMin() + ":00"));
		} else {
			workOrder.setStartTime(null);
		}

		// get stop time
		if (workOrder.getStopHour().length() > 0) {
			workOrder.setStopTime(Time.valueOf(workOrder.getStopHour() + ":" + workOrder.getStopMin() + ":00"));
		} else {
			workOrder.setStopTime(null);
		}
	}
	
	private void updateLineItems(HttpServletRequest request, WorkOrder workOrder) {
		Iterator<WorkOrderLineItem> iter = workOrder.getLineItems().iterator();
		while (iter.hasNext()) {
			WorkOrderLineItem lineItem = iter.next();
			// check if removed
			if (ServletRequestUtils.getBooleanParameter(request, "__remove_" + lineItem.getLineNumber(), false)) {
				workOrder.getDeletedLineItems().add(lineItem.getLineNumber());
				iter.remove();
			}			
		}
		
		for (WorkOrderLineItem lineItem:workOrder.getLineItems()) {
			// get quantity
			lineItem.setQuantity(ServletRequestUtils.getIntParameter(request, "__quantity_" + lineItem.getLineNumber(), lineItem.getQuantity()));

			// get notes
			lineItem.setNotes(ServletRequestUtils.getStringParameter(request, "__notes_" + lineItem.getLineNumber(), ""));
			
			// get In SN and Out SN
			lineItem.setInSN(ServletRequestUtils.getStringParameter(request, "__inSN_" + lineItem.getLineNumber(), ""));
			lineItem.setOutSN(ServletRequestUtils.getStringParameter(request, "__outSN_" + lineItem.getLineNumber(), ""));			
		}
		
		// inserted line items
		if (workOrder.getInsertedLineItems() != null) {
			Iterator<WorkOrderLineItem> iterInserted = workOrder.getInsertedLineItems().iterator();
			int index = 0;
			while (iterInserted.hasNext()) {
				WorkOrderLineItem lineItem = iterInserted.next();
				// check if removed
				if (ServletRequestUtils.getBooleanParameter(request, "__remove_new_" + index, false)) {
					iterInserted.remove();
				} else {
					// get quantity
					lineItem.setQuantity(ServletRequestUtils.getIntParameter(request, "__quantity_new_" + index, lineItem.getQuantity()));
					
					// get title
					if (request.getParameter("__name_new_" + index) != null) {
						lineItem.getProduct().setName(request.getParameter("__name_new_" + index));
					}

					// get notes
					lineItem.setNotes(ServletRequestUtils.getStringParameter(request, "__notes_new_" + index, ""));

					// get In SN and Out SN
					lineItem.setInSN(ServletRequestUtils.getStringParameter(request, "__inSN_new_" + index, ""));
					lineItem.setOutSN(ServletRequestUtils.getStringParameter(request, "__outSN_new_" + index, ""));					
				}
				index++;
			}	
		}
	}
}
