/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.03.2010
 */

package com.webjaguar.web.admin.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ServiceableItem;
import com.webjaguar.web.domain.Constants;

public class ServiceHistoryController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {
		
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
				
		// get serviceable item
		ServiceableItem item = this.webJaguar.getServiceableItemById(ServletRequestUtils.getIntParameter(request, "id", -1));
		
		if (item != null) {
			this.webJaguar.getServiceHistory(item, model, webJaguar, gSiteConfig);			
		}
		model.put("statuses", Constants.getWorkOrderStatuses());
		
		return new ModelAndView("admin/service/serviceHistory", "model", model);    	
    }
	
}
