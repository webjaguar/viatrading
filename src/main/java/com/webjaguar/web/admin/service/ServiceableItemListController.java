/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.21.2007
 */

package com.webjaguar.web.admin.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ServiceableItem;
import com.webjaguar.model.ServiceableItemSearch;
import com.webjaguar.web.domain.Constants;

public class ServiceableItemListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
		
		Map<String, Object> model = new HashMap<String, Object>();
    	
		ServiceableItemSearch search = getSearch( request );
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("item.jhtm"));
		}
		
		if (request.getParameter("__update") != null) {
			List<ServiceableItem> items = new ArrayList<ServiceableItem>();
			for (String item_id: ServletRequestUtils.getStringParameters(request, "item_ids")) {
				ServiceableItem item = new ServiceableItem();
				item.setId(Integer.parseInt(item_id));
				item.setActive(request.getParameter("__active_" + item_id) != null);
				items.add(item);
			}
			if (items.size() > 0) {
				this.webJaguar.updateServiceableItems(items);
				model.put("message", "update.successful");
			}
		}
		
		PagedListHolder list = new PagedListHolder(this.webJaguar.getServiceableItemList(search));		

		list.setPageSize(search.getPageSize());
		list.setPage(search.getPage()-1);
		
		model.put("list", list);
		model.put("statuses", Constants.getWorkOrderStatuses());
    	
        return new ModelAndView("admin/service/items", "model", model);
	}

	private ServiceableItemSearch getSearch(HttpServletRequest request) {
		ServiceableItemSearch search = (ServiceableItemSearch) request.getSession().getAttribute( "serviceableItemSearch" );
		if (search == null) {
			search = new ServiceableItemSearch();
			request.getSession().setAttribute( "serviceableItemSearch", search );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}				
		
		// item id
		if (request.getParameter("item_id") != null) {
			search.setItemId(ServletRequestUtils.getStringParameter( request, "item_id", "" ));
		}
		
		// serial number
		if (request.getParameter("s/n") != null) {
			search.setSerialNum(ServletRequestUtils.getStringParameter( request, "s/n", "" ));
		}		
		
		// sku
		if (request.getParameter("sku") != null) {
			search.setSku( ServletRequestUtils.getStringParameter( request, "sku", "" ));
		}
		
		// company Name
		if (request.getParameter("companyName") != null) {
			search.setCompanyName(ServletRequestUtils.getStringParameter(request, "companyName", ""));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}			
		
		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		return search;
	}	
	
}
