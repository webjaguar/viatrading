/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 01.15.2008
 */

package com.webjaguar.web.admin.service;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;

import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.WorkOrder;
import com.webjaguar.web.domain.Constants;

public class WorkOrderController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
        
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
		Map<String, Object> model = new HashMap<String, Object>();
		
		WorkOrder workOrder = this.webJaguar.getWorkOrder(ServletRequestUtils.getIntParameter(request, "num", -1));
    	if (workOrder != null) {

    		if (workOrder.getService().getItem().getCustomer() != null) {
    			workOrder.getService().getItem().setCustomer( this.webJaguar.getCustomerById( workOrder.getService().getItem().getCustomer().getId() ) );			
    		}
    		
    		model.put("workOrder", workOrder);
    		
    		Product product = this.webJaguar.getProductById( this.webJaguar.getProductIdBySku( workOrder.getService().getItem().getSku() ), 0, false, null );
    		if (product == null) {
    			product = new Product();
    		}
    		Class<Product> c = Product.class;
    		Method m = null;
    		Object arglist[] = null;
    		for ( ProductField productField : this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")) ) {
    			if ( productField.isEnabled() && productField.isServiceField() ) {
    				try {
    					m = c.getMethod( "getField" + productField.getId() );
    					productField.setValue( (String) m.invoke( product, arglist ) );						
    				} catch (Exception e) {
    					// do nothing
    				}
    				product.addProductField( productField );
    			}
    		}			
    		model.put("product", product);
    		model.put("countries", this.webJaguar.getCountryMap());
    		model.put("statuses", Constants.getWorkOrderStatuses());
    		
    		// multi store / sub contractor
    		model.put("workOrderLayout", this.webJaguar.getSystemLayout("workorder", workOrder.getService().getHost(), request));
    		
    		this.webJaguar.updateWorkOrderPrinted(workOrder.getService().getServiceNum());
    	} else {
    		model.put("message", "workorder.exception.notfound");	
    	}
		
		return new ModelAndView("admin/service/workOrderPrint", model);
	}
}
