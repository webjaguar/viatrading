/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.27.2007
 */

package com.webjaguar.web.admin.service;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.ServiceForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ServiceableItemSearch;

public class ServiceFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ServiceFormController() {
		setSessionForm(true);
		setCommandName("serviceForm");
		setCommandClass(ServiceForm.class);
		setFormView("admin/service/serviceForm");
		setSuccessView("services.jhtm");
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(df, true, 10));		
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		
		ServiceForm form = (ServiceForm) command;
		
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		if (form.getService().getNotesTemp() != null && form.getService().getNotesTemp().trim().length() > 0) {
			String notes = form.getService().getNotes();
			if (notes == null || notes.trim().length() == 0) {
				notes = "";
			} else {
				notes += "\n";
			}
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yy-h:mma");
			notes = notes + dateFormatter.format(new Date()) + " (" 
					+ SecurityContextHolder.getContext().getAuthentication().getName() + ") -- " 
					+ form.getService().getNotesTemp();
			form.getService().setNotes(notes);					
		}
		
		this.webJaguar.updateService(form.getService());
		
		// check if work order button was pressed
		if (request.getParameter("_workorder") != null) {
			return new ModelAndView(new RedirectView("workOrder.jhtm?num=" + form.getService().getServiceNum()));
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}	
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		ServiceForm form = new ServiceForm();
		form.setService(this.webJaguar.getService(ServletRequestUtils.getIntParameter(request, "num", -1)));
		if (form.getService().getItem().getCustomer() != null) {
			form.getService().getItem().setCustomer(this.webJaguar.getCustomerById(form.getService().getItem().getCustomer().getId()));			
		}
		
		return form;
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors) {
		Map<String, Object> map = new HashMap<String, Object>();
		ServiceForm form = (ServiceForm) command;
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		Product product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(form.getService().getItem().getSku()), 0, false, null);
		if (product == null) {
			product = new Product();
		}
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		for (ProductField productField : this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"))) {
			if (productField.isEnabled() && productField.isServiceField()) {
				try {
					m = c.getMethod("getField" + productField.getId());
					productField.setValue((String) m.invoke(product, arglist));						
				} catch (Exception e) {
					// do nothing
				}
				product.addProductField(productField);
			}
		}			
		map.put("product", product);
		map.put("countries", this.webJaguar.getCountryMap());
		map.put("statuses", Constants.getWorkOrderStatuses());
		
		if (form.getService().getItem().getCustomer() != null) {
			// get serviceable items
			ServiceableItemSearch search = new ServiceableItemSearch();
			for (Customer customer:this.webJaguar.getFamilyTree(form.getService().getItem().getCustomer().getId(), "down")) {
				search.addUserId(customer.getId());
			}
			map.put("serviceItems", this.webJaguar.getServiceableItemList(search));			
		}
		
		if (form.getService().getItem().getAlternateContacts() != null && form.getService().getItem().getAlternateContacts().trim().length() > 0) {
			String[] contacts = form.getService().getItem().getAlternateContacts().split("[\n]"); // split by commas
			List<String> alternateContacts = new ArrayList<String>();
			boolean noMatch = true;
			for (int i=0; i<contacts.length; i++) {
				if (contacts[i].trim().length() > 0) {
					alternateContacts.add(contacts[i].trim());
					if (contacts[i].trim().equals(form.getService().getAlternateContact())) {
						noMatch = false;
					}
				}
			}
			if (noMatch && form.getService().getAlternateContact() != null && form.getService().getAlternateContact().trim().length() > 0) {
				alternateContacts.add(form.getService().getAlternateContact());
			}
			if (alternateContacts.size() > 0) {
				map.put("alternateContacts", alternateContacts);
			}
		}
		
		return map;
	}

}
