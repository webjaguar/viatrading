/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.26.2007
 */

package com.webjaguar.web.admin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.ServiceableItem;
import com.webjaguar.web.domain.Constants;

public class ServiceableItemOwnerController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
		
		CustomerSearch search = getCustomerSearch( request );
		Map<String, Object> model = new HashMap<String, Object>();

		// check if cancel button was pressed
		if (request.getParameter("__cancel") != null) {
			return new ModelAndView( new RedirectView( "items.jhtm") );
		}
		
		ServiceableItem item = this.webJaguar.getServiceableItemById( ServletRequestUtils.getIntParameter( request, "id", 0 ) );
		if (item.getCustomer() == null) {
			item.setCustomer( new Customer() );
		}
		if (item != null) {
			model.put("item", item);
			model.put("customer", this.webJaguar.getCustomerById(item.getCustomer().getId()));				
		} else {
			return new ModelAndView( new RedirectView( "items.jhtm") );			
		}
		
		// check if update button was pressed
		if (request.getParameter("__update") != null) {
			int userId = ServletRequestUtils.getIntParameter( request, "uid", 0 );
			if (userId > 0) {
				item.getCustomer().setId( userId );
			} else {
				item.getCustomer().setId( null );
			}
			try {
				this.webJaguar.updateServiceableItem(item);								
			} catch (Exception e) {
				// do nothing
			}
			return new ModelAndView( new RedirectView( "items.jhtm") );
		}
		
		if (request.getParameter("begin") != null) {
			if (item.getCustomer().getId() != null) {
				List <Customer> familyTree = this.webJaguar.getFamilyTree(item.getCustomer().getId(), "up");
				if (familyTree.size() > 1) {
					search.setParent(familyTree.get( familyTree.size()-2 ).getId());					
				} else {
					search.setParent(-1);					
				}
			} else {
				search.setParent(-1);				
			}
		}
		PagedListHolder custList = new PagedListHolder(this.webJaguar.getCustomerListByParent(search));		

		custList.setPageSize(search.getPageSize());
		custList.setPage(search.getPage()-1);
		
		if (search.getParent() != null) {
			model.put( "familyTree", this.webJaguar.getFamilyTree( search.getParent(), "up" ) );			
		}
		model.put("customers", custList);
		model.put("statuses", Constants.getWorkOrderStatuses());
    	
        return new ModelAndView("admin/service/ownerForm", "model", model);
	}

	private CustomerSearch getCustomerSearch(HttpServletRequest request) {
		CustomerSearch search = (CustomerSearch) request.getSession().getAttribute( "serviceableItemOwnerSearch" );
		if (search == null) {
			search = new CustomerSearch();
			request.getSession().setAttribute( "serviceableItemOwnerSearch", search );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// parent ID
		if (request.getParameter("parent") != null) {
			int parentId = ServletRequestUtils.getIntParameter( request, "parent", 0 );
			if (parentId == -1) {
				search.setParent(-1);				
			} else if (parentId > 0) {
				search.setParent(parentId);
			} else {
				search.setParent( null );
			}
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}			
		
		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		return search;
	}	
	
}
