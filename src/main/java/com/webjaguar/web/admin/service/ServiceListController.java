/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.27.2007
 */

package com.webjaguar.web.admin.service;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ServiceSearch;
import com.webjaguar.web.domain.Constants;

public class ServiceListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
    	
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		ServiceSearch search = getSearch( request );
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		
		PagedListHolder list = new PagedListHolder(this.webJaguar.getServiceList(search));		

		list.setPageSize(search.getPageSize());
		list.setPage(search.getPage()-1);
		
		model.put("list", list);
		model.put("statuses", Constants.getWorkOrderStatuses());
		
		Cookie incompleteWO = WebUtils.getCookie(request, "incompleteWO");
		if ((incompleteWO == null || !incompleteWO.getValue().equals(this.webJaguar.getAccessUser(request).getUsername()))
				&& this.webJaguar.getIncompleteWorkOrders(siteConfig.get("WO_ALERT_INTERVAL_CUSTOMER_FIELD").getValue(), Integer.parseInt(siteConfig.get("WO_ALERT_INTERVAL_DEFAULT").getValue())).size() > 0) {
			// create cookie
			GregorianCalendar now = new GregorianCalendar();
			now.add(Calendar.HOUR, 4);
			Long cookieAge = (now.getTimeInMillis() - (new Date()).getTime())/1000; // 4 hours
			incompleteWO = new Cookie("incompleteWO", this.webJaguar.getAccessUser(request).getUsername());
			incompleteWO.setPath(request.getContextPath().equals("") ? "/" : request.getContextPath());
			incompleteWO.setMaxAge(cookieAge.intValue());			
			response.addCookie(incompleteWO);
			
			model.put("incompleteWO", true);
		}
    	
        return new ModelAndView("admin/service/services", model);
	}

	private ServiceSearch getSearch(HttpServletRequest request) {
		ServiceSearch search = (ServiceSearch) request.getSession().getAttribute( "serviceSearch" );
		if (search == null) {
			search = new ServiceSearch();
			request.getSession().setAttribute( "serviceSearch", search );
		}
		
		// service number
		if (request.getParameter("serviceNum") != null) {
			search.setServiceNum(ServletRequestUtils.getStringParameter( request, "serviceNum", "" ));
		}
		
		// item id
		if (request.getParameter("item_id") != null) {
			search.setItemId(ServletRequestUtils.getStringParameter( request, "item_id", "" ));
		}
		
		// item id like
		if (request.getParameter("item_id_like") != null) {
			search.setItemIdLike(ServletRequestUtils.getStringParameter(request, "item_id_like", ""));
		}

		// purchase order
		if (request.getParameter("purchaseOrder") != null) {
			search.setPurchaseOrder(ServletRequestUtils.getStringParameter(request, "purchaseOrder", ""));				
		}
		
		// company Name
		if (request.getParameter("companyName") != null) {
			search.setCompanyName(ServletRequestUtils.getStringParameter(request, "companyName", ""));
		}
		
		// work order type
		if (request.getParameter("workOrderType") != null) {
			search.setWorkOrderType(ServletRequestUtils.getStringParameter(request, "workOrderType", ""));
		}
		
		// technician
		if (request.getParameter("technician") != null) {
			search.setTechnician(ServletRequestUtils.getStringParameter(request, "technician", ""));
		}

		// 3rd party work order
		if (request.getParameter("wo3rd") != null) {
			search.setWo3rd(ServletRequestUtils.getStringParameter(request, "wo3rd", ""));
		}
		
		// supplier po
		if (request.getParameter("poSupplier") != null) {
			search.setPoSupplier(ServletRequestUtils.getStringParameter(request, "poSupplier", ""));
		}
		
		// reference #
		if (request.getParameter("refNum") != null) {
			search.setRefNum(ServletRequestUtils.getStringParameter(request, "refNum", ""));
		}
		
		// manuf warranty
		if (request.getParameter("manufWarranty") != null) {
			search.setManufWarranty(ServletRequestUtils.getStringParameter(request, "manufWarranty", ""));
		}
		
		// status
		if (request.getParameter("status") != null) {
			search.setStatus(ServletRequestUtils.getStringParameter( request, "status", "" ));
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			search.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}			
		
		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		return search;
	}	
	
}
