/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.24.2007
 */

package com.webjaguar.web.admin.service;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.springframework.dao.DataAccessException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.ServiceableItemForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;

public class ServiceableItemFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ServiceableItemFormController() {
		setSessionForm(true);
		setCommandName("serviceableItemForm");
		setCommandClass(ServiceableItemForm.class);
		setFormView("admin/service/itemForm");
		setSuccessView("items.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		
		ServiceableItemForm form = (ServiceableItemForm) command;
		
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			try {
				this.webJaguar.deleteServiceableItem(form.getItem());				
			} catch (DataAccessException e) {
		    	Map<String, Object> model = new HashMap<String, Object>();
				model.put("message", "service.exception.itemHasService");
				return showForm(request, response, errors, model);	
			}
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		if (form.isNewItem()) {
			this.webJaguar.insertServiceableItem(form.getItem());	
		} else {
			this.webJaguar.updateServiceableItem(form.getItem());				
		}
		
		// check if there is an owner
		if (form.getItem().getCustomer() == null) {
			return new ModelAndView(new RedirectView("setOwner.jhtm"), "id", form.getItem().getId());
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}	
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		ServiceableItemForm form = new ServiceableItemForm(this.webJaguar.getServiceableItemById(ServletRequestUtils.getIntParameter(request, "id", -1)));
		if (form.getItem() != null) {
			if (form.getItem().getCustomer() != null) {
				form.getItem().setCustomer(this.webJaguar.getCustomerById(form.getItem().getCustomer().getId()));
			}
			if (form.getItem().getAddress() == null) {
				form.getItem().setAddress(new Address());
			}
		} else {
			form = new ServiceableItemForm();
		}
		
		return form;
	}

	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors) {
		Map<String, Object> map = new HashMap<String, Object>();
		ServiceableItemForm form = (ServiceableItemForm) command;
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		Product product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(form.getItem().getSku()), 0, false, null);
		if (product == null) {
			product = new Product();
		}
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		for (ProductField productField : this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"))) {
			if (productField.isEnabled() && productField.isServiceField()) {
				try {
					m = c.getMethod("getField" + productField.getId());
					productField.setValue((String) m.invoke(product, arglist));						
				} catch (Exception e) {
					// do nothing
				}
				product.addProductField(productField);
			}
		}			
		map.put("product", product);
		
		map.put("countries",this.webJaguar.getEnabledCountryList());
		map.put("states",this.webJaguar.getStateList("US"));
		map.put("caProvinceList", this.webJaguar.getStateList("CA"));
		map.put("statuses", Constants.getWorkOrderStatuses());

		return map;
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		ServiceableItemForm form = (ServiceableItemForm) command;

		// check item id if duplicate
		Integer id = this.webJaguar.getServiceableItemId(form.getItem().getItemId(), null);
		if (id != null && (form.isNewItem() || id.compareTo(form.getItem().getId()) != 0)) {
			errors.rejectValue("item.itemId", "duplicate");			
		}
		
		// check s/n if duplicate
		id = this.webJaguar.getServiceableItemId(null, form.getItem().getSerialNum());
		if (id != null && (form.isNewItem() || id.compareTo(form.getItem().getId()) != 0)) {
			errors.rejectValue("item.serialNum", "duplicate");			
		}
	}
}
