package com.webjaguar.web.admin.supplier;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.cete.dynamicpdf.Resource;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.Supplier;

@Controller
public class SupplierImportController extends WebApplicationObjectSupport {

	@Autowired
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	String formView = "admin/supplier/import";
	private HSSFFormulaEvaluator evaluator;
	
	@RequestMapping(value="/admin/supplier/ImportSupplier.jhtm")
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {	
    	
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
    	
		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			return new ModelAndView("admin/supplier/import", "message", "excelfile.notWritable");	
		}    	
    	
		// check if upload button was pressed
		if (request.getParameter("__upload") != null) {
	    	MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request; 
	    	MultipartFile file = multipartRequest.getFile("file");
	    	if (file != null && !file.isEmpty()) {
	    		File excel_file = new File(baseFile, "suppliers_import.xls");
	    		file.transferTo(excel_file);
	    		file = null; // delete to save resources
		    	return processFile(excel_file, request, gSiteConfig);	    			
	    	} else {
	        	return new ModelAndView(formView, "message", "excelfile.invalid");	    		
	    	}
		}
		return new ModelAndView(formView);
    }
	
    private ModelAndView processFile(File excel_file, HttpServletRequest request, Map gSiteConfig) {
        List<Map> addedItems = new ArrayList<Map>();
        List<Map> updatedItems = new ArrayList<Map>();
        List<Map> invalidItems = new ArrayList<Map>();
		List<Supplier> importedSupplier = new ArrayList<Supplier>();	
    	
		Map<String, Object> map = new HashMap<String, Object>();
		
		try {
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(excel_file));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
		    
			HSSFSheet sheet = wb.getSheetAt(0);       	// first sheet
			HSSFRow row     = sheet.getRow(0);        	// first row
			
			evaluator = new HSSFFormulaEvaluator(sheet, wb);
			
			Map<String, Short> header = new HashMap<String, Short>();
			
			if (wb.getNumberOfSheets() > 1) {
				// multiple sheets
				map.put("message", "excelfile.multipleSheets");
				return new ModelAndView(formView, map);					
			}
			if (row == null) {
				// first line should be the headers
				map.put("message", "excelfile.emptyHeaders");
				return new ModelAndView(formView, map);					
			}
			
		    Iterator iter = row.cellIterator();
		    
		    // get header names
			while (iter.hasNext()) {
				HSSFCell cell = (HSSFCell) iter.next();
				String headerName = "";
				try {
					headerName = cell.getStringCellValue().trim();					
				} catch (NumberFormatException e) {
					// do nothing
				}
				if (!headerName.equals( "" )) {
					if (header.put(headerName.toLowerCase(), cell.getCellNum()) != null) {
						// duplicate header
						map.put("message", "excelfile.duplicateHeaders");
						map.put("arguments", headerName);
						return new ModelAndView(formView, map);								
					}				
				} 
			}
			
//			System.out.println(header.toString() + "====header ");
			
			// check required headers
			if (!header.containsKey("supplier id")) { // supplierid, productsku and cost columns are required
				return new ModelAndView(formView, "message", "excelfile.missingHeaders");
			}
			
			nextRow: for (int i=1; i<=sheet.getLastRowNum(); i++) {
				row = sheet.getRow(i);
				if (row == null) {
					continue;
				}
				
				// skip the row if supplier id is empty
				Map<String, Object> thisMap = new HashMap<String, Object>();
				Integer supplierId = null;
				try {
					supplierId = getIntegerValue(row, header.get("supplier id"));
				} catch (Exception e) {
					thisMap.put("id", getStringValue(row, header.get("supplier id")));
					thisMap.put("rowNum", row.getRowNum()+1);
					thisMap.put("reason", "Invalid Supplier ID");
					invalidItems.add(thisMap);
					continue nextRow;
				}	
				if (supplierId == null) {
					// missing supplier id
					thisMap.put("id", getStringValue(row, header.get("supplier id")));
					thisMap.put("rowNum", row.getRowNum()+1);
					thisMap.put("reason", "Missing Supplier ID");
					invalidItems.add(thisMap);
					continue nextRow;					
				} else if (!this.webJaguar.isValidSupplierId(supplierId)) { // id not valid
					thisMap.put("rowNum", row.getRowNum()+1);
					thisMap.put("id", supplierId);
					thisMap.put("reason", "Invalid Supplier ID");
					invalidItems.add(thisMap);
					continue nextRow;							
				}
				
				if (invalidItems.size() == 0) { // no errors
					// save or update customer
					Supplier supplier = null;
					supplier = this.webJaguar.getSupplierById(supplierId);
					thisMap.put("id", supplierId);
					if (supplier != null) { 		// existing supplier
						processRow(row, header, supplier);		
						updatedItems.add(thisMap);
					} else {						// doesn't exist
						continue nextRow;
					}
					importedSupplier.add(supplier);
				} 
			}					
		} catch (IOException e) {
			return new ModelAndView(formView, "message", "excelfile.invalid");
		} catch (Exception e) {
        	return new ModelAndView(formView, "message", "excelfile.version");
		} finally {
	    	// delete excel file
			excel_file.delete();	    			
		}
		
		if (invalidItems.size() > 0) { // has errors
			map.put("invalidItems", invalidItems);
			return new ModelAndView(formView, map);					
		} else {
			if (importedSupplier.size() == 0) {	// no Customers to import
				return new ModelAndView(formView, "message", "excelfile.empty");			
			} else {
				this.webJaguar.importSuppliers(importedSupplier);
				this.webJaguar.insertImportExportHistory( new ImportExportHistory("supplierImport", SecurityContextHolder.getContext().getAuthentication().getName(), true ));
			}
		}		
		map.put("updatedItems", updatedItems);
		return new ModelAndView("admin/supplier/importSuccess", map);		
    }
    
    private Integer getIntegerValue(HSSFRow row, short cellnum) throws Exception {
		HSSFCell cell = row.getCell(cellnum);
    	Integer value = null;
    	
    	if (cell != null) {
    		switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = new Integer(cell.getStringCellValue());										
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Double cellValue = cell.getNumericCellValue();
					if (cellValue != cellValue.intValue()) {
						throw new Exception();	
					}
					value =  cellValue.intValue();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue formulaCellValue = evaluator.evaluate(cell);					
					if (formulaCellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						cellValue =  new Double(formulaCellValue.getNumberValue());
						if (cellValue != cellValue.intValue()) {
							throw new Exception();	
						}
						value =  cellValue.intValue();
					}
					break;
			} // switch
    	}
    	return value;
    }
    
    private String getStringValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		String value = "";

    	if (cell != null) {
			switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_STRING:
					value = cell.getStringCellValue();
					break;
				case HSSFCell.CELL_TYPE_NUMERIC:
					Integer intValue = null;
					try {
						intValue = getIntegerValue(row, cellnum);
					} catch (Exception e) {}
					if (intValue != null) {
						value = intValue.toString();
						break;
					}
					value = new Double(cell.getNumericCellValue()).toString();
					break;
				case HSSFCell.CELL_TYPE_FORMULA:
					evaluator.setCurrentRow(row);
					HSSFFormulaEvaluator.CellValue cellValue = evaluator.evaluate(cell);
					if (cellValue.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
						value = new Double(cellValue.getNumberValue()).toString();						
					}
					break;
			} // switch		
    	}
		return value;
    }
    
    private boolean getBooleanValue(HSSFRow row, short cellnum) {
		HSSFCell cell = row.getCell(cellnum);
		boolean value = false;

    	if (cell != null) {
    		if (getStringValue(row, cellnum).trim().equals( "1" ))  {
    			value = true;
    		}
    	}
		return value;
    } 
    
    private void processRow(HSSFRow row, Map<String, Short> header, Supplier supplier) {
		
		Address address = supplier.getAddress();
		
		// get company
		if (header.containsKey("company name")) {
			if (getStringValue(row, header.get("company name")) != null && !getStringValue(row, header.get("company name")).isEmpty()) {
				address.setCompany( getStringValue(row, header.get("company name")) );
			}
		} 
    	
		// get account number
		if (header.containsKey("account number")) {
			if (getStringValue(row, header.get("account number")) != null && !getStringValue(row, header.get("account number")).isEmpty()) {
				supplier.setAccountNumber( getStringValue(row, header.get("account number")) );
			}
		}    

		// get vendor group
		if (header.containsKey("vendor group")) {
			if (getStringValue(row, header.get("vendor group")) != null && !getStringValue(row, header.get("vendor group")).isEmpty()) {
				supplier.setGroup( getStringValue(row, header.get("vendor group")) );
			}		
		} 
		
		// get vendor DBA
		if (header.containsKey("vendor dba")) {
			if (getStringValue(row, header.get("vendor dba")) != null && !getStringValue(row, header.get("vendor dba")).isEmpty()) {
				supplier.setDba( getStringValue(row, header.get("vendor dba")) );
			}		
		} 
		
		// get supplier prefix
		if (header.containsKey("supplier prefix")) {
			if (getStringValue(row, header.get("supplier prefix")) != null && !getStringValue(row, header.get("supplier prefix")).isEmpty()) {
				supplier.setSupplierPrefix( getStringValue(row, header.get("supplier prefix")));
			}		
		} 
		
		// get first name
		if (header.containsKey("first name")) {
			if (getStringValue(row, header.get("first name")) != null && !getStringValue(row, header.get("first name")).isEmpty()) {
				address.setFirstName( getStringValue(row, header.get("first name")) );
			}		
		} 
		
		// get last name
		if (header.containsKey("last name")) {
			if (getStringValue(row, header.get("last name")) != null && !getStringValue(row, header.get("last name")).isEmpty()) {
				address.setLastName( getStringValue(row, header.get("last name")) );
			}		
		} 
		
		// get address 1
		if (header.containsKey("address 1")) {
			if (getStringValue(row, header.get("address 1")) != null && !getStringValue(row, header.get("address 1")).isEmpty()) {
				address.setAddr1(getStringValue(row, header.get("address 1")) );
			}		
		} 
		
		// get address 2
		if (header.containsKey("address 2")) {
			if (getStringValue(row, header.get("address 2")) != null && !getStringValue(row, header.get("address 2")).isEmpty()) {
				address.setAddr2(getStringValue(row, header.get("address 2")) );
			}		
		} 
		
		// get country
		if (header.containsKey("country")) {
			if (getStringValue(row, header.get("country")) != null && !getStringValue(row, header.get("country")).isEmpty()) {
				address.setCountry(getStringValue(row, header.get("country")) );
			}		
		} 
		
		// get City
		if (header.containsKey("city")) {
			if (getStringValue(row, header.get("city")) != null && !getStringValue(row, header.get("city")).isEmpty()) {
				address.setCity(getStringValue(row, header.get("city")) );
			}		
		} 
		
		// get state/province
		if (header.containsKey("state/province")) {
			if (getStringValue(row, header.get("state/province")) != null && !getStringValue(row, header.get("state/province")).isEmpty()) {
				address.setStateProvince(getStringValue(row, header.get("state/province")) );
			}		
		} 
		
		// get zip/postal code
		if (header.containsKey("zip/postal code")) {
			if (getStringValue(row, header.get("zip/postal code")) != null && !getStringValue(row, header.get("zip/postal code")).isEmpty()) {
				address.setZip(getStringValue(row, header.get("zip/postal code")) );
			}		
		} 
		
		// get phone
		if (header.containsKey("phone")) {
			if (getStringValue(row, header.get("phone")) != null && !getStringValue(row, header.get("phone")).isEmpty()) {
				address.setPhone(getStringValue(row, header.get("phone")) );
			}		
		} 
		
		// get fax
		if (header.containsKey("fax")) {
			if (getStringValue(row, header.get("fax")) != null && !getStringValue(row, header.get("fax")).isEmpty()) {
				address.setFax(getStringValue(row, header.get("fax")) );
			}		
		} 
		
		// get email
		if (header.containsKey("email")) {
			if (getStringValue(row, header.get("email")) != null && !getStringValue(row, header.get("email")).isEmpty()) {
				address.setEmail(getStringValue(row, header.get("email")) );
			}		
		} 
		supplier.setAddress(address);
		
		// get note
		if (header.containsKey("note")) {
			if (getStringValue(row, header.get("note")) != null && !getStringValue(row, header.get("note")).isEmpty()) {
				supplier.setNote(getStringValue(row, header.get("note")) );
			}		
		} 
		
		// payment preference
		if (header.containsKey("payment preference")) {
			if (getStringValue(row, header.get("payment preference")) != null && !getStringValue(row, header.get("payment preference")).isEmpty()) {
				supplier.setPaymentPreference(getStringValue(row, header.get("payment preference")) );
			}		
		} 
		
		// mail check to
		if (header.containsKey("mail check to")) {
			if (getStringValue(row, header.get("mail check to")) != null && !getStringValue(row, header.get("mail check to")).isEmpty()) {
				supplier.setMailCheckTo(getStringValue(row, header.get("mail check to")) );
			}		
		} 
		
		// transfer money to
		if (header.containsKey("transfer money to")) {
			if (getStringValue(row, header.get("transfer money to")) != null && !getStringValue(row, header.get("transfer money to")).isEmpty()) {
				supplier.setTransferMoneyTo(getStringValue(row, header.get("transfer money to")) );
			}		
		} 
		
    }
}
