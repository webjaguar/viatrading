package com.webjaguar.web.admin.supplier;

	import java.util.HashMap;
	import java.util.List;
	import java.util.Map;

	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

	import org.springframework.web.servlet.ModelAndView;

	import org.springframework.web.servlet.mvc.Controller;
	import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Supplier;


	public class ShowAjaxSupplierController implements Controller{

		private WebJaguarFacade webJaguar;
		public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
		
		public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
		{		
			
			Map<String, Object> myModel = new HashMap<String, Object>();	
			
			if (request.getParameter( "value" ) != null) {
				List<Supplier> supplier = this.webJaguar.getSupplierAjax( request.getParameter( "value" ) );
				myModel.put("supplier", supplier);
				
			}
			
	        return new ModelAndView("admin/supplier/showSuppliers", "model", myModel);
		}
		
	}

	
	
	