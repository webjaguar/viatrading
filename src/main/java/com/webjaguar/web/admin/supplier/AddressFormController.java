/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.supplier;

import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Supplier;
import com.webjaguar.web.form.AddressForm;

public class AddressFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar; 
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public AddressFormController() {
		setSessionForm(true);
		setCommandName("addressForm");
		setCommandClass(AddressForm.class);
		setSuccessView("addressList.jhtm" );
		setFormView("admin/supplier/addressForm");
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
     	AddressForm addressForm = (AddressForm) command;
     	Integer supplierId = new Integer(request.getParameter("sid"));
		addressForm.getAddress().setSupplierId(supplierId);
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null) {
			this.webJaguar.deleteSupplierAddress(addressForm.getAddress());
			return new ModelAndView(new RedirectView(getSuccessView()) , "sid" , supplierId);
		}     	
     	
		// check if cancel button was pressed
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()) , "sid" , supplierId);
		}    	
     	
		// set as primary address
		if (addressForm.isSetAsPrimary()) {
			addressForm.getAddress().setPrimary(true);
		} 

	
		if (addressForm.isNewAddress()) {
			this.webJaguar.insertSupplierAddress(addressForm.getAddress());
		} else {
			this.webJaguar.updateSupplierAddress(addressForm.getAddress());
		}
		// if primary address changed then change the address of supplier in database supplier.
		if (addressForm.getAddress().isPrimary()) {
			this.webJaguar.updateSupplierPrimeAddressBySupplierId(addressForm.getAddress());
		}
		
		return new ModelAndView(new RedirectView("addressList.jhtm?sid=" + supplierId));  
		
    }

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}    
			
    protected Map referenceData(HttpServletRequest request) 
			throws Exception {
    	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();
		
    	Integer supplierId = new Integer(request.getParameter("sid"));
    	Supplier supplier = this.webJaguar.getSupplierById(supplierId); 
    	myModel.put("company",supplier.getAddress().getCompany());
       	
	    // get a list of enabled country code from database
	    myModel.put("countries",this.webJaguar.getEnabledCountryList());
        myModel.put("states",this.webJaguar.getStateList("US"));
		myModel.put("caProvinceList", this.webJaguar.getStateList("CA"));  
		
	    map.put("model", myModel);  
	    return map;
    }  
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		AddressForm addressForm = new AddressForm();
		
		if (siteConfig.get("SUPPLIER_MULTI_ADDRESS").getValue().endsWith("false")) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("supplierList.jhtm")));
		}
		
		Integer supplierId = new Integer(request.getParameter("sid"));
    	Supplier supplier = this.webJaguar.getSupplierById(supplierId);
    	if (supplier != null) {
    		addressForm.getAddress().setCompany(supplier.getAddress().getCompany());
    	}
		
		if ( request.getParameter("aid") != null ) {
			Integer addressId = new Integer(request.getParameter("aid"));
			Address address = this.webJaguar.getSupplierAddressById(addressId);
			addressForm = new AddressForm(address);
			if (addressForm.getAddress().getStateProvince().trim().equals("")) {
				addressForm.getAddress().setStateProvinceNA(true);
			}
		}
		return addressForm;
	}
}