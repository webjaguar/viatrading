/* 
 * Copyright 2020 Advanced E-Media Solutions
 * @author Jerry Zhang
 * @since 11.20.2020
 */

package com.webjaguar.web.admin.supplier;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ImportExportHistory;
import com.webjaguar.model.Supplier;

@Controller
public class SupplierExportController extends WebApplicationObjectSupport {
	
	@Autowired
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }		
	
	@RequestMapping(value="/admin/supplier/ExportSupplier.jhtm")
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {

    	Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Object> map = new HashMap<String, Object>();
		
		File baseFile = new File(getServletContext().getRealPath("/admin/excel/"));
		if (!baseFile.canWrite()){
			map.put("message", "excelfile.notWritable");
			return new ModelAndView("admin/supplier/export", map);			
		}
		File file[] = baseFile.listFiles();
		List<Map> exportedFiles = new ArrayList<Map>();
		for (int f=0; f<file.length; f++) {
			if (file[f].getName().startsWith("supplierExport") & file[f].getName().endsWith(".xls")) {
				if (request.getParameter("__new") != null) { // check if new button was pressed
					file[f].delete();
				} else {
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					exportedFiles.add( fileMap );				
				}
			}
		}
		map.put("exportedFiles", exportedFiles);
    	
		// check if new button was pressed
		if (request.getParameter("__new") != null) {
			
			int supplierCount = this.webJaguar.supplierCount();
			int limit = 3000;
			
	    	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
	    	String fileName = "/supplierExport_" + dateFormatter.format(new Date()) + "";

	    	for (int offset=0, excel = 1; offset<supplierCount; excel++) {
		    	List<Supplier> supplierList = this.webJaguar.getSupplierExportList(limit, offset);
		    	int start = offset + 1;
		    	int end = (offset + limit) < supplierCount ? (offset + limit) : supplierCount;
		    	HSSFWorkbook wb = createWorkbook(supplierList,gSiteConfig, start + " to " + end);
		    	// Write the output to a file
		    	File newFile = new File(baseFile.getAbsolutePath() + fileName + excel + ".xls");
		        FileOutputStream fileOut = new FileOutputStream(newFile);
		        wb.write(fileOut);
		        fileOut.close();
		        HashMap<String, Object> fileMap = new HashMap<String, Object>();
		        fileMap.put("file", newFile);
				exportedFiles.add( fileMap );  		
		        offset = offset + limit;
	    	}	        
			if (supplierCount > 0) {
				map.put("arguments", supplierCount);
				map.put("message", "supplierexport.success");
				this.webJaguar.insertImportExportHistory( new ImportExportHistory("supplierExport", SecurityContextHolder.getContext().getAuthentication().getName(), false ));
			} else {
				map.put("message", "supplierexport.empty");				
			}
		}
		return new ModelAndView("admin/supplier/export", map);
	}
    
    private HSSFCell getCell(HSSFSheet sheet, int row, short col) {
    	return sheet.createRow(row).createCell(col);
    }
    
    private void setText(HSSFCell cell, String text) {
    	cell.setCellValue(text);
    }
    
    private HSSFWorkbook createWorkbook(List supplierList,Map gSiteConfig , String sheetName) throws Exception {
    	Iterator iter = supplierList.iterator();

    	HSSFWorkbook wb = new HSSFWorkbook();
    	HSSFSheet sheet = wb.createSheet(sheetName);
    	
    	// create the headers
		Map<String, Short> headerMap = new HashMap<String, Short>();
		short col = 0;
		headerMap.put("Supplier Id", col++);
		headerMap.put("Company Name", col++);
		headerMap.put("Account Number", col++);
		headerMap.put("Vendor Group", col++);
		headerMap.put("Vendor DBA", col++);
		headerMap.put("Supplier Prefix", col++);
		
		headerMap.put("First Name", col++);
		headerMap.put("Last Name", col++);
		headerMap.put("Address 1", col++);
		headerMap.put("Address 2", col++);
		headerMap.put("Country", col++);
		headerMap.put("City", col++);
		headerMap.put("State/Province", col++);
		headerMap.put("Zip/Postal Code", col++);
		headerMap.put("Phone", col++);		
		headerMap.put("Fax", col++);
		headerMap.put("Email", col++);
		headerMap.put("Note", col++);
		headerMap.put("Payment Preference", col++);
		headerMap.put("Mail Check to", col++);
		headerMap.put("Transfer Money to", col++);
		
		// print headers
		for (String headerName:headerMap.keySet()){
			setText(getCell(sheet, 0, headerMap.get(headerName).shortValue()), headerName); 
		}
	
    	iter = supplierList.iterator();   	
    	HSSFCell cell = null;
		
		// create the rows
    	for (int row=2; iter.hasNext(); row++) {    		
    		Supplier supplier = (Supplier) iter.next();
        	setText(getCell(sheet, row, headerMap.get("Supplier Id").shortValue()), supplier.getId().toString());	
        	setText(getCell(sheet, row, headerMap.get("Company Name").shortValue()), supplier.getAddress().getCompany());
        	setText(getCell(sheet, row, headerMap.get("Account Number").shortValue()), supplier.getAccountNumber());
        	setText(getCell(sheet, row, headerMap.get("Vendor Group").shortValue()), supplier.getGroup());
        	setText(getCell(sheet, row, headerMap.get("Vendor DBA").shortValue()), supplier.getDba());
        	
        	setText(getCell(sheet, row, headerMap.get("Supplier Prefix").shortValue()), supplier.getSupplierPrefix());
        	
        	setText(getCell(sheet, row, headerMap.get("First Name").shortValue()), supplier.getAddress().getFirstName());
        	setText(getCell(sheet, row, headerMap.get("Last Name").shortValue()), supplier.getAddress().getLastName());
        	setText(getCell(sheet, row, headerMap.get("Address 1").shortValue()), supplier.getAddress().getAddr1());
        	setText(getCell(sheet, row, headerMap.get("Address 2").shortValue()), supplier.getAddress().getAddr2());        	
        	setText(getCell(sheet, row, headerMap.get("Country").shortValue()), supplier.getAddress().getCountry());
        	setText(getCell(sheet, row, headerMap.get("City").shortValue()), supplier.getAddress().getCity());
        	setText(getCell(sheet, row, headerMap.get("State/Province").shortValue()), supplier.getAddress().getStateProvince());
        	setText(getCell(sheet, row, headerMap.get("Zip/Postal Code").shortValue()), supplier.getAddress().getZip());
        	setText(getCell(sheet, row, headerMap.get("Phone").shortValue()), supplier.getAddress().getPhone());
        	setText(getCell(sheet, row, headerMap.get("Fax").shortValue()), supplier.getAddress().getFax());
        	setText(getCell(sheet, row, headerMap.get("Email").shortValue()), supplier.getAddress().getEmail());
        	
        	setText(getCell(sheet, row, headerMap.get("Note").shortValue()), supplier.getNote());
        	setText(getCell(sheet, row, headerMap.get("Payment Preference").shortValue()), supplier.getPaymentPreference());
        	setText(getCell(sheet, row, headerMap.get("Mail Check to").shortValue()), supplier.getMailCheckTo());
        	setText(getCell(sheet, row, headerMap.get("Transfer Money to").shortValue()), supplier.getTransferMoneyTo());
    	}	  
    	return wb;
    }
}
