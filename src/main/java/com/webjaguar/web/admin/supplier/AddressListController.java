/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.supplier;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Supplier;

public class AddressListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	
    	if (siteConfig.get("SUPPLIER_MULTI_ADDRESS").getValue().endsWith("false")) {
    		return new ModelAndView(new RedirectView("supplierList.jhtm"));
    	}
    	
    	Integer supplierId = new Integer(request.getParameter("sid"));
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("addressForm.jhtm"), "sid", supplierId);		
		}		
		
		Supplier supplier = this.webJaguar.getSupplierById(supplierId); 	
		Map<String, Object> myModel = new HashMap<String, Object>();
    	
		
		if ( supplier != null ) {
			// default address
			myModel.put("defaultAddress", this.webJaguar.getDefaultAddressBySupplierId(supplier.getId()));
			// addresses
			PagedListHolder addressList = 
				new PagedListHolder(this.webJaguar.getAddressListBySupplierId(supplier.getId()));
			addressList.setPageSize(10);
			String page = request.getParameter("page");
			if (page != null) {
				addressList.setPage(Integer.parseInt(page)-1);
			}
			myModel.put("addresses", addressList);	
			myModel.put("company",supplier.getAddress().getCompany());
			myModel.put("sid", supplierId);	
		}
		return new ModelAndView("admin/supplier/addresses", "model", myModel);    	
    }
}
