/*
 * Copyright 2005, 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 * 
 * @since 04.21.2009
 */

package com.webjaguar.web.admin.supplier;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CompanyReview;
import com.webjaguar.model.Product;
import com.webjaguar.model.Supplier;
import com.webjaguar.web.form.ReviewForm;


public class CompanyReviewFormController extends SimpleFormController
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public CompanyReviewFormController()
	{
		setSessionForm( true );
		setCommandName( "companyReviewForm" );
		setCommandClass( ReviewForm.class );
		setFormView( "admin/supplier/companyReviewform" );
		setSuccessView( "companyReviewList.jhtm" );
		this.setValidateOnBinding( true );
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception
	{
		ReviewForm reviewForm = (ReviewForm) command;

		Map<String, Object> map = new HashMap<String, Object>();
        
		// check if cancel button was pressed
		if ( request.getParameter( "_cancel" ) != null ) {
			return new ModelAndView( new RedirectView( getSuccessView() ), "id", reviewForm.getCompanyReview().getCompanyId() );
		}
		
		// check if delete button was pressed
		if (request.getParameter("_delete") != null)
		{
			this.webJaguar.deleteCompanyReviewById( reviewForm.getProductReview().getId() );
			return new ModelAndView( new RedirectView( getSuccessView() ), "id", reviewForm.getCompanyReview().getCompanyId() );
		}

		this.webJaguar.updateCompanyReview( reviewForm.getCompanyReview()); 
		return new ModelAndView( new RedirectView( getSuccessView() ), "id", reviewForm.getCompanyReview().getCompanyId() );
	}
	
	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
 	
		return map;
    }

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productReview.rate", "form.required", "required");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productReview.tile", "form.required", "required");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productReview.text", "form.required", "required");
	}
	
	protected boolean suppressBinding(HttpServletRequest request)
	{
		if (request.getParameter( "_cancel" ) != null || request.getParameter("_delete") != null) {
			return true;
		}
		return false;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception
	{
		ReviewForm reviewForm = new ReviewForm();
		CompanyReview companyReview = new CompanyReview();
		reviewForm.setCompanyReview( companyReview );
		
		int companyReviewId = ServletRequestUtils.getIntParameter(request, "id", -1);
		int companyId = ServletRequestUtils.getIntParameter(request, "companyId", -1);
		Supplier company = this.webJaguar.getSupplierById( companyId );

		if ( company != null ) {
			reviewForm.getCompanyReview().setId( companyReviewId );
			reviewForm.getCompanyReview().setCompanyId( companyId );
			reviewForm.setCompanyReview( this.webJaguar.getCompanyReviewById( companyReviewId ) );
		} else {
			// product not valid
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("supplerList.jhtm")));			
		}
		
		return reviewForm;
	}
}
