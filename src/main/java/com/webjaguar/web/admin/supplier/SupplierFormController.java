/*
 * Copyright 2005, 2007 Advanced E-Media Solutions @author Shahin Naji
 * 
 * @since 07.16.2007
 */

package com.webjaguar.web.admin.supplier;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.Supplier;
import com.webjaguar.web.form.SupplierForm;


public class SupplierFormController extends org.springframework.web.servlet.mvc.SimpleFormController
{

	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar(com.webjaguar.logic.WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public SupplierFormController()
	{
		setSessionForm( true );
		setCommandName( "supplierForm" );
		setCommandClass( com.webjaguar.web.form.SupplierForm.class );
		setFormView( "admin/supplier/form" );
		setSuccessView( "supplierList.jhtm" );
		this.setValidateOnBinding( true );
	}

	public ModelAndView onSubmit(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, Object command,
			org.springframework.validation.BindException errors) throws javax.servlet.ServletException
	{
		SupplierForm supplierForm = (SupplierForm) command;
		
		// check if cancel button was pressed
		if ( ServletRequestUtils.getStringParameter( request, "_cancel" ) != null ) {
			return new ModelAndView( new RedirectView( getSuccessView() ) );
		}
		
		// check if delete button was pressed
		String deleteSupplier = ServletRequestUtils.getStringParameter( request, "delete" );
		if ( deleteSupplier != null ) {
			this.webJaguar.deleteSupplierById(supplierForm.getSupplier().getId());
			return new ModelAndView( new RedirectView( getSuccessView()));
		}
		
		if (supplierForm.isNewSupplier()) {
			// duplicates are allowed. Just id is prime.
			this.webJaguar.insertSupplier(supplierForm.getSupplier());
		} else {
			
			if(supplierForm.isActiveStatus() != supplierForm.getSupplier().isActive()) {
				this.webJaguar.updateProduct(" active = " + supplierForm.getSupplier().isActive(), " sku IN ( SELECT sku FROM product_supplier WHERE supplier_id =" + supplierForm.getSupplier().getId()+" )");
			}
			this.webJaguar.updateSupplierByIdWithMarkup(supplierForm.getSupplier());
		}

		return new ModelAndView(new RedirectView(getSuccessView()));
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		SupplierForm supplierForm = (SupplierForm) command;
        
		// check if delete button was pressed
		String deleteSupplier = org.springframework.web.bind.ServletRequestUtils.getStringParameter( request, "delete" );
		if ( deleteSupplier != null ) {
			try {
				this.webJaguar.deleteSupplierById( supplierForm.getSupplier().getId() );
			} catch ( Exception e ) {
				errors.rejectValue("supplier.address.company", "form.supplier.productAssociatedWithThisSupplier", "A Product Associated to This Sales Rep.");
			}
		}
	}
	protected boolean suppressBinding(HttpServletRequest request)
	{
		if ( request.getParameter( "_cancel" ) != null || request.getParameter( "delete" ) != null ) {
			return true;
		}
		return false;
	}

	protected Object formBackingObject(javax.servlet.http.HttpServletRequest request) throws Exception
	{
		SupplierForm supplierForm = new SupplierForm();

		if ( request.getParameter( "sid" ) != null ) {
			Integer supplierId = new Integer( request.getParameter( "sid" ) );
			Supplier supplier = this.webJaguar.getSupplierById( supplierId );

			supplierForm.setSupplier( supplier );
			supplierForm.setNewSupplier( false );
			supplierForm.setActiveStatus(supplier.isActive());
		} else {
			supplierForm.getSupplier().getAddress().setPrimary(true);
		}

		return supplierForm;
	}
	
    protected Map referenceData(HttpServletRequest request) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		map.put("countries",this.webJaguar.getEnabledCountryList());
		map.put("countryMap", this.webJaguar.getCountryMap());
		map.put("states",this.webJaguar.getStateList("US"));
		map.put("caProvinceList", this.webJaguar.getStateList("CA"));
 	
		return map;
    }
}
