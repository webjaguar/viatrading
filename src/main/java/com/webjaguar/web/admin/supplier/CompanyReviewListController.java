/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.admin.supplier;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CompanyReview;
import com.webjaguar.model.ProductReview;
import com.webjaguar.model.ReviewSearch;


public class CompanyReviewListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		ReviewSearch companyReviewSearch = getProductSearch( request ); 
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		int companyId = ServletRequestUtils.getIntParameter( request, "id", -1 );
		if (companyId == -1) {
			companyReviewSearch.setCompanyId( "" );
		}
		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i=0; i<ids.length; i++) {
					this.webJaguar.deleteCompanyReviewById(Integer.parseInt(ids[i]));
				}
			}
		}
		
		int count = this.webJaguar.getCompanyReviewListCount(companyReviewSearch);
		if (count < companyReviewSearch.getOffset()-1) {
			companyReviewSearch.setPage(1);
		}
		
		companyReviewSearch.setLimit(companyReviewSearch.getPageSize());
		companyReviewSearch.setOffset((companyReviewSearch.getPage()-1)*companyReviewSearch.getPageSize());
		
		List<CompanyReview> companyReviewList = this.webJaguar.getcompanyReviewList(companyReviewSearch);
		int pageCount = count/companyReviewSearch.getPageSize();
		if (count%companyReviewSearch.getPageSize() > 0) {
			pageCount++;
		}
		
		myModel.put("companyReviewList", companyReviewList);
		myModel.put("companyId", companyId);
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", companyReviewSearch.getOffset()+companyReviewList.size());
		myModel.put("count", count);
		
        return new ModelAndView("admin/supplier/companyReviewList", "model", myModel);
	}
	
	private ReviewSearch getProductSearch(HttpServletRequest request) {
		ReviewSearch companyReviewSearch = (ReviewSearch) request.getSession().getAttribute( "companyReviewSearch" );
		if (companyReviewSearch == null) {
			companyReviewSearch = new ReviewSearch();
			request.getSession().setAttribute( "companyReviewSearch", companyReviewSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			companyReviewSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "id" ));
		}
		
		// active
		if (request.getParameter("active") != null) {
			companyReviewSearch.setActive(ServletRequestUtils.getStringParameter( request, "active", null ));
		}
		
		// product id
		if (request.getParameter("id") != null) {
			companyReviewSearch.setCompanyId(ServletRequestUtils.getStringParameter( request, "id", "" ));
		}
				
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				companyReviewSearch.setPage( 1 );
			} else {
				companyReviewSearch.setPage( page );				
			}
		}			
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				companyReviewSearch.setPageSize( 10 );
			} else {
				companyReviewSearch.setPageSize( size );				
			}
		}
		
		return companyReviewSearch;
	}
}
