/*
 * Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 07.13.2007 
 */

package com.webjaguar.web.admin.supplier;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.model.Search;
import com.webjaguar.model.Supplier;

public class SupplierListController extends org.springframework.web.servlet.mvc.AbstractCommandController
{

	public SupplierListController() {
		setCommandClass( com.webjaguar.model.CategorySearch.class );
	}

	private com.webjaguar.logic.WebJaguarFacade webJaguar;
	public void setWebJaguar( com.webjaguar.logic.WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }	

	
	public org.springframework.web.servlet.ModelAndView handle( javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response, Object command, org.springframework.validation.BindException errors ) throws Exception
	{
		Search supplierSearch = getSupplierSearch( request );
		Map<String, Object> myModel = new HashMap<String, Object>();
		Map<String, Object> gSiteConfig =  (Map) request.getAttribute( "gSiteConfig" );
		
		// if add pushed
		if ( request.getParameter( "__add" ) != null ) {
			return new ModelAndView( new RedirectView( "supplier.jhtm" ) );
		}
		// check if options button was clicked
		if (request.getParameter("__activeBacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i=0; i<ids.length; i++) {
					boolean activeMode = ServletRequestUtils.getBooleanParameter( request, "__active_mode" );
					System.out.println("Products");
					this.webJaguar.updateProduct(" active = " + activeMode, " sku IN ( SELECT sku FROM product_supplier WHERE supplier_id =" + ids[i]+" )");
					this.webJaguar.updateSupplierStatusById(Integer.parseInt(ids[i]), activeMode);
				}
			}
		}
		
		// check if options button was clicked
		if (request.getParameter("__updateRankingBacher") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				for (int i=0; i<ids.length; i++) {
					int rank = ServletRequestUtils.getIntParameter( request, "__rank", 10);
					this.webJaguar.updateProduct(" search_rank = " + rank, " sku IN ( SELECT sku FROM product_supplier WHERE supplier_id =" + ids[i]+" )");
				}
			}
		}
				
		List<Supplier> suppliers =  this.webJaguar.getSuppliers(supplierSearch) ;
		Boolean prefix = false;
		
		Map<Integer, Integer> supplierProductCount = new HashMap<Integer, Integer>();
		for (Supplier supplier : suppliers) {
			supplierProductCount.put(supplier.getId(), this.webJaguar.getProductsCountBySupplierId(supplier.getId()));
		}
		myModel.put( "supplierProductCount", supplierProductCount );
		if ((Boolean) gSiteConfig.get("gCUSTOMER_SUPPLIER")) {
			for (Supplier supplier : suppliers) {
				if (supplier.getSupplierPrefix() != null) {
					prefix = true;
					break;
				}
			}
		}
		
		PagedListHolder supplierList = new PagedListHolder(suppliers); 
		supplierList.setPageSize( supplierSearch.getPageSize() );
		supplierList.setPage( supplierSearch.getPage() - 1 );
		
		myModel.put( "prefix", prefix );
		myModel.put( "suppliers", supplierList );
		if (request.getSession().getAttribute( "message" ) != null) 
			myModel.put("message", request.getSession().getAttribute( "message" )); 
		if((Boolean) gSiteConfig.get("gNEW_ADMIN") && request.getAttribute("newAdmin") != null)  {
			return new ModelAndView("admin-responsive/supplier/list", "model", myModel);
		}
		return new org.springframework.web.servlet.ModelAndView( "admin/supplier/list", "model", myModel );
	}
	
	private Search getSupplierSearch(HttpServletRequest request) {
		Search supplierSearch = (Search) request.getSession().getAttribute( "supplierSearch" );
		if (supplierSearch == null) {
			supplierSearch = new Search();
			request.getSession().setAttribute( "supplierSearch", supplierSearch );
		}		
		// company name		
		if (request.getParameter("company") != null) {
			supplierSearch.setCompany(request.getParameter("company"));
		}			

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				supplierSearch.setPage( 1 );
			} else {
				supplierSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				supplierSearch.setPageSize( 10 );
			} else {
				supplierSearch.setPageSize( size );				
			}
		}	
		return supplierSearch;
	}
}