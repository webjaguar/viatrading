package com.webjaguar.web.admin.manufacturer;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.web.form.ManufacturerForm;

public class ManufacturerFormController extends SimpleFormController { 
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ManufacturerFormController() {
		setSessionForm(true);
		setCommandName("manufacturerForm");
		setCommandClass(ManufacturerForm.class);
		setFormView("admin/catalog/manufacturer/form");
		setSuccessView("manufacturerList.jhtm");		
	}
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
			
		ManufacturerForm manufacturerForm = (ManufacturerForm) command;
		
		Map<String, Object> model = new HashMap<String, Object>();
		if(request.getParameter("_delete")!=null) {
			if (this.webJaguar.isProductExist(manufacturerForm.getManufacturer().getName())) {
				model.put("message", "manufacturerHasProduct");
				return showForm(request, response, errors, model);	
			} else {
				this.webJaguar.deleteManufacturer(manufacturerForm.getManufacturer().getId());
				return new ModelAndView(new RedirectView(getSuccessView()));
			}
		}
		
		if(manufacturerForm.isNewManufacturer()) {
			this.webJaguar.insertManufacturer(manufacturerForm.getManufacturer());
		} else {
			String oldManufacturerName = this.webJaguar.getManufacturerById(manufacturerForm.getManufacturer().getId()).getName();
			if(oldManufacturerName != null) {
				this.webJaguar.updateProductManufacturerName(oldManufacturerName, manufacturerForm.getManufacturer().getName());
				this.webJaguar.updateShoppingcartManufacturerName(oldManufacturerName, manufacturerForm.getManufacturer().getName());
			}
			this.webJaguar.updateManufacturer(manufacturerForm.getManufacturer());
			
		}		
		
		ModelAndView modelAndView = new ModelAndView(new RedirectView(getSuccessView()));	
		return modelAndView;
	}	

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		ManufacturerForm manufacturerForm = (ManufacturerForm) command;
		manufacturerForm.getManufacturer().setName(manufacturerForm.getManufacturer().getName().trim());		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "manufacturer.name", "form.required", "Manufacturer Name is required");
		
		if(manufacturerForm.isNewManufacturer() && this.webJaguar.getManufacturerByName(manufacturerForm.getManufacturer().getName()) !=null) {
			errors.rejectValue("manufacturer.name", "MANUFACTURER_ALREADY_EXISTS", "Manufacturer with this name already exists.");
		}
	} 	
	
	protected Object formBackingObject(HttpServletRequest request) 
	throws Exception {
		
		ManufacturerForm manufacturerForm = new ManufacturerForm();
		if (request.getParameter("id") != null) {
			manufacturerForm = new ManufacturerForm(this.webJaguar.getManufacturerById(new Integer (request.getParameter("id"))));
		}
		return manufacturerForm;
    }
}
