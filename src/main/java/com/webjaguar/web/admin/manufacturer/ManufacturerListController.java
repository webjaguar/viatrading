package com.webjaguar.web.admin.manufacturer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Manufacturer;
import com.webjaguar.model.ManufacturerSearch;


public class ManufacturerListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		ManufacturerSearch manufacturerSearch = getManufacturerSearch(request);
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("manufacturer.jhtm"));
		}
		
		int count = this.webJaguar.getManufacturerList(null).size();
		if (count < manufacturerSearch.getOffset()-1) {
			manufacturerSearch.setPage(1);
		}
		manufacturerSearch.setLimit(manufacturerSearch.getPageSize());
		manufacturerSearch.setOffset((manufacturerSearch.getPage()-1)*manufacturerSearch.getPageSize());
		
		
		List<Manufacturer> manufacturerlist = this.webJaguar.getManufacturerList(manufacturerSearch);
		myModel.put("manufacturerlist", manufacturerlist);
		
		int pageCount = count/manufacturerSearch.getPageSize();
		if (count%manufacturerSearch.getPageSize() > 0) {
			pageCount++;
		}
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", manufacturerSearch.getOffset()+manufacturerlist.size());
		myModel.put("count", count);
		
		return new ModelAndView("admin/catalog/manufacturer/list", "model", myModel);
	}

	private ManufacturerSearch getManufacturerSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		ManufacturerSearch manufacturerSearch = (ManufacturerSearch) request.getSession().getAttribute( "manufacturerSearch" );
			if (manufacturerSearch == null) {
			manufacturerSearch = new ManufacturerSearch();
			request.getSession().setAttribute( "manufacturerSearch", manufacturerSearch );
		}
		
	/*	// manufacturer name
		if (request.getParameter("name") != null) {
			manufacturerSearch.setName(ServletRequestUtils.getStringParameter( request, "name", "" )) ;
		} */
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				manufacturerSearch.setPage( 1 );
			} else {
				manufacturerSearch.setPage( page );				
			}
		}			
		
		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				manufacturerSearch.setPageSize( 10 );
			} else {
				manufacturerSearch.setPageSize( size );				
			}
		}
		return manufacturerSearch;
	}
}