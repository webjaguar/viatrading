/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 10.13.2010
 */

package com.webjaguar.web.quartz;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import powerReview.ProductType;
import powerReview.ProductWithQuestions;
import powerReview.ProductWithReviews;
import powerReview.ProductWithReviewsAndQuestions;
import powerReview.Products;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;

public class PowerReviewJob extends WebApplicationObjectSupport {
	
	private static WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	Map<String, Configuration> siteConfig;

	static StringBuffer xml;
	
	public void getPowerReview() throws Exception {
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();	
		
		String powerReviewXML_URI = siteConfig.get("POWER_REVIEW_XML").getValue();
		if (powerReviewXML_URI.isEmpty()) {
			return;
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		// parse PowerReview
		parsePowerReviewXML(powerReviewXML_URI, siteConfig);
		

		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		sbuff.append("Read review_data_summary.xml file in " + powerReviewXML_URI + "  folder." + "\n\n");
		notifyAEM("getPowerReview() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
	
	public void parsePowerReviewXML(String uri, Map<String, Configuration> siteConfig) {
		try {
			SAXParserFactory f = SAXParserFactory.newInstance();
	        SAXParser parser = f.newSAXParser();
	        DefaultHandler handler = new MyContentHandler();
	        
	        xml = new StringBuffer();
			URL url = new URL(uri);
	    	BufferedReader in = new BufferedReader( new InputStreamReader(url.openStream()));
	    	String inputLine;
	    	while ((inputLine = in.readLine()) != null) {
	    		xml.append(inputLine);
			}
			in.close();
			parser.parse(url.openStream(),handler);	        

		} catch (ParserConfigurationException e) {System.out.println("error 1 ");
	        notifyAdmin("parseXmlFile ApplianceSpec on " + siteConfig.get("SITE_URL").getValue(), e.toString());
	    } catch (SAXException e) {System.out.println("error 2 ");
	    	notifyAdmin("parseXmlFile ApplianceSpec on " + siteConfig.get("SITE_URL").getValue(), e.toString());
	    } catch (IOException e) {System.out.println("error 3 ");
	    	notifyAdmin("parseXmlFile ApplianceSpec on " + siteConfig.get("SITE_URL").getValue(), e.toString());
	    }
	}
	
    public static class MyContentHandler extends DefaultHandler {
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		HashMap<String, Object> map;
		
		boolean sku = false;
		boolean inlinequestionfiles = false;
		boolean inlinefiles = false;
		boolean inlinefile = false;
	    
		public void startDocument() throws SAXException {
			List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
			HashMap<String, Object> map;
			
			JAXBContext context;
	    	Unmarshaller unmarshaller = null;
	    	ByteArrayInputStream bis;
	    	Products products = null;
	    	
	    	try {
	    		context = JAXBContext.newInstance(Products.class);
	    		unmarshaller = context.createUnmarshaller();
	    		bis = new ByteArrayInputStream(xml.toString().getBytes("UTF-8"));
	    		products = (Products) unmarshaller.unmarshal(bis);
	    		
	    	}catch (Exception e){ e.printStackTrace(); }

    		if(products != null) {
    			for(ProductType pt : products.getProductOrDeletedProduct()) {
    				ProductWithReviews productWR = null ;
    	    		ProductWithQuestions productWQ = null ;
    	    		ProductWithReviewsAndQuestions productWRQ = null ;
    	    		map = new HashMap<String, Object>();
    	    		
    				if(pt.getClass().equals(ProductWithReviews.class)){
    					productWR = (ProductWithReviews) pt;
    					if(productWR != null) {
    						map.put("sku", productWR.getPageid());
    						// In future we need to save all files but for now save only first file 
	    					// as multiple file does not work on tab
	    					StringBuffer reviews = new StringBuffer();
	    					if(productWR.getInlinefiles() != null && productWR.getInlinefiles().getInlinefile() != null) {
	    						reviews.append(productWR.getInlinefiles().getInlinefile().get(0).getValue());
	    					}
	    					map.put("tab_3_content", reviews.toString());
	    				}
    					
    	    		} else if(pt.getClass().equals(ProductWithQuestions.class)) {
    					productWQ = (ProductWithQuestions) pt;
    					
    					if(productWQ != null) {
	    					map.put("sku", productWQ.getPageid());
	    					
	    					// In future we need to save all files but for now save only first file 
	    					// as multiple file does not work on tab
	    					StringBuffer reviews = new StringBuffer();
	    					if(productWQ.getInlinequestionfiles() != null && productWQ.getInlinequestionfiles().getInlinefile() != null) {
	    						reviews.append(productWQ.getInlinequestionfiles().getInlinefile().get(0).getValue());
	    					}
	    					map.put("tab_6_content", reviews.toString());
	    	    		}
	   					
	   	    		} else if(pt.getClass().equals(ProductWithReviewsAndQuestions.class)) {
	       				productWRQ = (ProductWithReviewsAndQuestions) pt;
	       				
	       				if(productWRQ != null) {
	    					map.put("sku", productWRQ.getPageid());
	    					
	    					// In future we need to save all files but for now save only first file 
	    					// as multiple file does not work on tab
	    					StringBuffer reviews = new StringBuffer();
	    					if(productWRQ.getInlinefiles() != null && productWRQ.getInlinefiles().getInlinefile() != null) {
	    						reviews.append(productWRQ.getInlinefiles().getInlinefile().get(0).getValue());
	    					}
	    					map.put("tab_3_content", reviews.toString());
	        	    		
	    					StringBuffer qReviews = new StringBuffer();
	    					if(productWRQ.getInlinequestionfiles() != null && productWRQ.getInlinequestionfiles().getInlinefile() != null) {
	    						qReviews.append(productWRQ.getInlinequestionfiles().getInlinefile().get(0).getValue());
	    					}
	    					map.put("tab_6_content", qReviews.toString());
	    	    		}
	       				System.out.println("789 "+map);
	   	    			
	       			}
	   				
	   				data.add(map);
	   				if (data.size() == 500) {
	   					// products
	   					updateProduct(data);
	   					data.clear();
	   					map.clear();
	   				}
	   			}
	   		}
	   }
	   
	   public void endDocument() throws SAXException {
	       // for files that has less than 500 Product
	       if (data.size() > 0) {
				// products
				updateProduct(data);
				data.clear();
				map.clear();
			}
	    }
	    
	    public void ignorableWhitespace(char buf[], int offset, int len) throws SAXException {

	    }
	    
	    public void skippedEntity(String name)  throws SAXException {
	    	System.out.println("skippedEntity " + name);
	    }
	    
	    public void warning(SAXParseException e) throws SAXException {
	        System.out.println("Warning: " + e.getMessage());
	    }

	    private void updateProduct(List<Map <String, Object>> data) {
	    	// update Products
			webJaguar.updatePowerReview(data);
	    }
	 }

	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
	
	private void notifyAEM(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
}