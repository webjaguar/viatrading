/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.26.2008
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;

public class EvergreenJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	// ftp.wjserver50.com,ftp_myevergreen.com,myevergreen822,EVE/PRODUCT.csv,EVE/REPINFO.csv,EVE/REPZIP.csv,EVE/CUSTOMER.csv,EVE/CUSTOMER_EXPORT.csv
	
	/*
	 * updating/adding products 
	 * look at the csv_feed for the fields need to be updated.
	 * 
	 * Triggers the update everyday at 3:30 AM (server is at EST)
	 * 
	 * Log on to ftp using following credentials on configuration: 
	 * URL : 1st value
	 * Username : 2nd Value
	 * Password : 3rd Value
	 * 
	 * Read PRODUCT.csv file in EVE folder. Location is specified as 4th value on Configuration
	 * 
	 * CSV file is tab delimited.
	 * If sku does not exist on database, it will be added.
	 * If the value of PROMOPRICE is greater than or equal to 0 and price_1 is not empty, MSRP will be price_1 and price_1 will be PROMOPRICE 
	 * If exception is thrown during updating products, an email will be sent to Shahin.
	 * 
	 */
	public void updateInventory() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map<String, CsvFeed> csvFeedMap = this.webJaguar.getCsvFeedMapping("evergreen-inventory");
		Map <String, Long> salesTagCodeMap = this.webJaguar.getSalesTagCodeMap();
		
		// EVERGREEN configuration
		String[] config = siteConfig.get("EVERGREEN").getValue().split(",");
		
		if (!(gSiteConfig.get("gACCOUNTING").equals("EVERGREEN") && config.length > 3 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if evergreen feature is disabled 
			return;
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		int gPROTECTED = (Integer) gSiteConfig.get("gPROTECTED");
		int gPRODUCT_IMAGES = (Integer) gSiteConfig.get("gPRODUCT_IMAGES");
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		int promoPrice = -1;
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	        	        
	        InputStream inStream = ftp.retrieveFileStream(config[3]);

	        if (inStream != null) {
	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream), '\t', CSVWriter.NO_QUOTE_CHARACTER); // tab delimited
		    	
	        	// images
	        	Map<String, Integer> images = new TreeMap<String, Integer>();
	        	for (int i=1; i<=gPRODUCT_IMAGES; i++) {
	        		images.put("image" + i, null);
	        	}
	        	
	        	// categories
	        	Map<String, Integer> categories = new HashMap<String, Integer>();
	        	for (int i=1; i<=10; i++) {
	        		categories.put("category" + i, null);
	        	}
	        	
	        	String[] headers = reader.readNext();
	        	for (int i=0; i<headers.length; i++) {
					if (csvFeedMap.containsKey(headers[i].toLowerCase())) {
						CsvFeed csvFeed = csvFeedMap.get(headers[i].toLowerCase());
						csvFeed.setColumn(i);
						csvFeedList.add(csvFeed);
					} else if (images.containsKey(headers[i].toLowerCase())) {
						images.put(headers[i].toLowerCase(), i);
					} else if (categories.containsKey(headers[i].toLowerCase())) {
						categories.put(headers[i].toLowerCase(), i);
					} else if (headers[i].equalsIgnoreCase("PROMOPRICE")) {
						promoPrice = i;
					}
				}
	        	
	        	// remove images that are not present
	        	for (int i=1; i<=gPRODUCT_IMAGES; i++) {
	        		if (images.get("image" + i) == null) {
	        			images.remove("image" + i);
	        		}
	        	}
	        	
	        	// remove categories that are not present
	        	for (int i=1; i<=10; i++) {
	        		if (categories.get("category" + i) == null) {
	        			categories.remove("category" + i);
	        		}
	        	}
	        	
				String[] nextLine;
				while ((nextLine = reader.readNext()) != null) {
					HashMap<String, Object> map = new HashMap<String, Object>();
					
					for (CsvFeed csvFeed: csvFeedList) {
						if (csvFeed.getColumnName().equals("protected_level")) {
							// protected level
							if (gPROTECTED > 0) {
								try {
									int protectedlevel = Integer.parseInt(nextLine[csvFeed.getColumn()]);
									if (protectedlevel > gPROTECTED || protectedlevel == 0) {
										map.put("protected_level", "0");
									} else {
										StringBuffer protectedLevel = new StringBuffer("1");
							           	for (int i=1; i<protectedlevel; i++) {
							           		protectedLevel.append( "0" );
							           	}
							           	map.put("protected_level", protectedLevel.toString());
									}
								} catch (Exception e) {
									map.put("protected_level", "0");
								} 
							}  else {
								map.put("protected_level", "0");
							}
						} else if (csvFeed.getColumnName().equals("sales_tag_id")) {
							map.put("sales_tag_id", salesTagCodeMap.get(nextLine[csvFeed.getColumn()].toLowerCase()));
						} else {
							if (csvFeed.isNumeric()) {
								try {
									if (csvFeed.isWholeNum()) {
										map.put(csvFeed.getColumnName(), Integer.parseInt(nextLine[csvFeed.getColumn()]));
									} else {
										map.put(csvFeed.getColumnName(), Double.parseDouble(nextLine[csvFeed.getColumn()]));
									}										
								} catch (NumberFormatException e) {
									map.put(csvFeed.getColumnName(), null);									
								}
							} else {
								map.put(csvFeed.getColumnName(), nextLine[csvFeed.getColumn()]);					
							}
						}
					}
					// images
					if (images.size() > 0) {
						map.put("images", "");
						for (String image: images.keySet()) {
							map.put("images", map.get("images") + nextLine[images.get(image)] + ",");
						}						
					}
					// categories
					if (categories.size() > 0) {
						map.put("categories", "");
						for (String category: categories.keySet()) {
							map.put("categories", map.get("categories") + nextLine[categories.get(category)] + ",");
						}						
					}
					// promo price
		        	if (promoPrice > -1 && map.get("price_1") != null) {
		        		try {
			        		Double value = Double.parseDouble(nextLine[promoPrice]);	
			        		if (value != null) {
			        			map.put("msrp", map.get("price_1"));
			        			map.put("price_1", value);
			        		}
		        		} catch (NumberFormatException e) { 
		        			// do nothing 
		        		}
		        	}
					data.add(map);
				}
				reader.close();
				inStream.close();
				
				if (data.size() > 0 && data.get(0).containsKey("sku")) {
		        	if (promoPrice > -1) {
		        		csvFeedList.add(new CsvFeed("msrp"));
		        	}
					//this.webJaguar.updateEvergreenProduct(csvFeedList, data);
					// delete file
					//ftp.deleteFile(config[3]);
				}
	        }
			
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			notifyAdmin("Evergreen updateInventory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		sbuff.append("Read PRODUCT.csv file in EVE folder." + "\n\n");
		notifyAEM("updateInventory() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
	
	/*
	 * They send SalesRep info and we save them.
	 * Triggers the update everyday at 10:55 PM (server is at EST)
	 * 
	 * Log on to ftp using following credentials on configuration: 
	 * URL : 1st value
	 * Username : 2nd Value
	 * Password : 3rd Value
	 * 
	 * Read REPZIP.csv file in EVE folder. Location is specified as 6th value on Configuration
	 * CSV file is tab delimited.
	 * This csv contains Sales Rep and associated  Zip code
	 * 
	 * Read CUSTOMER.csv file in EVE folder. Location is specified as 7th value on Configuration
	 * CSV file is tab delimited.
	 * This csv contains CUSTOMER_NO and REP_ID. Rest of the columns are not required for this update.
	 * 
	 * Read REPINFO.csv file in EVE folder. Location is specified as 7th value on Configuration
	 * CSV file is tab delimited.
	 * This csv contains REP_ID, NAME, EMAIL, ADDRESS, CITY, STATE and ZIP
	 * 
	 * 
	 * If sales rep does not exist on database, it will be added.
	 * Old Sales reps which are not available on this csv will be deleted
	 * If exception is thrown during updating, an email will be sent to AEM.
	 * 
	 */
	public void updateSalesRep() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map<String, CsvFeed> csvFeedMap = this.webJaguar.getCsvFeedMapping("evergreen-salesrep");
		
		// EVERGREEN configuration
		String[] config = siteConfig.get("EVERGREEN").getValue().split(",");
		
		if (!(gSiteConfig.get("gACCOUNTING").equals("EVERGREEN") && config.length > 6 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if evergreen feature is disabled 
			return;
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();

		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();

			// REPZIP.csv (REP_ID  ZIPCODES)
	        InputStream inStream = ftp.retrieveFileStream(config[5]);
	        Map<String, Set<String>> salesRepMap = new TreeMap<String, Set<String>>();
	        if (inStream != null) {
	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream), '\t', CSVWriter.NO_QUOTE_CHARACTER); // tab delimited
	        	
	        	// headers
	        	Map<String, Integer> headerMap = new HashMap<String, Integer>();
	        	String[] headers = reader.readNext();
	        	for (int i=0; i<headers.length; i++) {
	        		headerMap.put(headers[i].toLowerCase(), i);
	        	} 
	        	
	        	if (headerMap.containsKey("rep_id") && headerMap.containsKey("zipcodes")) {
					String[] nextLine;
					while ((nextLine = reader.readNext()) != null) {
						String rep_id = nextLine[headerMap.get("rep_id")];
						if (!salesRepMap.containsKey(rep_id)) {
							salesRepMap.put(rep_id, new TreeSet<String>());
						}
						salesRepMap.get(rep_id).add(nextLine[headerMap.get("zipcodes")]);
					}
	        	}
				reader.close();
				inStream.close();
	        }	     
	        
	        ftp.completePendingCommand(); // very important for next retrieveFileStream to work
	        
	        // CUSTOMER.csv - only look at CUSTOMER_NO and REP_ID columns
	        Map<String, String> customerSalesRepMap = new HashMap<String, String>();
	        inStream = ftp.retrieveFileStream(config[6]);
	        if (inStream != null) {
	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream), '\t', CSVWriter.NO_QUOTE_CHARACTER); // tab delimited
	        	
	        	// headers
	        	Map<String, Integer> headerMap = new HashMap<String, Integer>();
	        	String[] headers = reader.readNext();
	        	for (int i=0; i<headers.length; i++) {
	        		headerMap.put(headers[i].toLowerCase(), i);
	        	} 
	        	
	        	if (headerMap.containsKey("rep_id") && headerMap.containsKey("customer_no")) {
					String[] nextLine;
					while ((nextLine = reader.readNext()) != null) {
						customerSalesRepMap.put(nextLine[headerMap.get("customer_no")], nextLine[headerMap.get("rep_id")]);
					}
	        	}
				reader.close();
				inStream.close();
	        }
	        
	        ftp.completePendingCommand(); // very important for next retrieveFileStream to work
	        
	        // REPINFO.csv (REP_ID  NAME    EMAIL   ADDRESS CITY    STATE   ZIP)  
	        inStream = ftp.retrieveFileStream(config[4]);
	        if (inStream != null) {
	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream), '\t', CSVWriter.NO_QUOTE_CHARACTER); // tab delimited
	        	
	        	// headers
	        	String[] headers = reader.readNext();
	        	for (int i=0; i<headers.length; i++) {
					if (csvFeedMap.containsKey(headers[i].toLowerCase())) {
						CsvFeed csvFeed = csvFeedMap.get(headers[i].toLowerCase());
						csvFeed.setColumn(i);
						csvFeedList.add(csvFeed);
					}
				}
	        	
				String[] nextLine;
				while ((nextLine = reader.readNext()) != null) {
					HashMap<String, Object> map = new HashMap<String, Object>();
					for (CsvFeed csvFeed: csvFeedList) {
						map.put(csvFeed.getColumnName(), nextLine[csvFeed.getColumn()]);
					}
					
					if (!csvFeedMap.containsKey("country")) {
						map.put("country", "US");
					}
					
					if (map.containsKey("sales_rep_account_num") && salesRepMap.containsKey(map.get("sales_rep_account_num"))) {
						StringBuffer zipcodes = new StringBuffer(",");
						for (String zip: salesRepMap.get(map.get("sales_rep_account_num"))) {
							zipcodes.append(zip + ",");
						}
						map.put("territory_zip", zipcodes);
					}
					
					map.put("feed_new", true);
					
					data.add(map);
				}
				reader.close();
				inStream.close();
				
				if (data.size() > 0 && data.get(0).containsKey("sales_rep_account_num")) {
					csvFeedList.add(new CsvFeed("feed_new"));
					if (!csvFeedMap.containsKey("country")) {
						csvFeedList.add(new CsvFeed("country"));
					}
					if (salesRepMap.size() > 0) {
						csvFeedList.add(new CsvFeed("territory_zip"));
					}
					this.webJaguar.updateEvergreenSalesRep(csvFeedList, data, customerSalesRepMap);
				}
	        }
			
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			notifyAdmin("Evergreen updateSalesRep() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		sbuff.append("Read REPZIP.csv, CUSTOMER.csv, REPINFO.csv files in EVE folder." + "\n\n");
		notifyAEM("updateSalesRep() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
	
	/*
	 * Some customers are belong to a group that they need to pay using a custom payment.
	 * Triggers the update everyday at 10:30 PM (server is at EST)
	 * 
	 * Log on to ftp using following credentials on configuration: 
	 * URL : 1st value
	 * Username : 2nd Value
	 * Password : 3rd Value
	 * 
	 * Read CUSTOMER.csv file in EVE folder. Location is specified as 7th value on Configuration
	 * CSV file is tab delimited.
	 * This csv contains CUSTOMER_NO and PO_GROUP. Rest of the columns are not required for this update.
	 * 
	 * If po_group has value 1, customer will be added to Group having id 5.
	 * If po_group has value other than 1, customer will be deleted from Group having id 5.
	 * If exception is thrown during updating products, an email will be sent to Shahin.
	 */
	public void updateCustomerGroup() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// EVERGREEN configuration
		String[] config = siteConfig.get("EVERGREEN").getValue().split(",");
		
		if (!(gSiteConfig.get("gACCOUNTING").equals("EVERGREEN") && config.length > 6 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if evergreen feature is disabled 
			return;
		}
		
		Date start = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();

	        
	        // CUSTOMER.csv - only look at CUSTOMER_NO and PO_GROUP columns
	        InputStream inStream = ftp.retrieveFileStream(config[6]);
	        if (inStream != null) {
	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream), '\t', CSVWriter.NO_QUOTE_CHARACTER); // tab delimited
	        	
	        	// headers
	        	Map<String, Integer> headerMap = new HashMap<String, Integer>();
	        	String[] headers = reader.readNext();
	        	for (int i=0; i<headers.length; i++) {
	        		headerMap.put(headers[i].toLowerCase(), i);
	        	} 
	        	
	        	// po group's id on myevergeen is 5
	        	if (headerMap.containsKey("rep_id") && headerMap.containsKey("po_group")) {
					String[] nextLine;
					StringBuffer sbuff = new StringBuffer();
					Set<String> insertAccountNumbers = new HashSet<String>();
					Set<String> deleteAccountNumbers = new HashSet<String>();
					while ((nextLine = reader.readNext()) != null) {
						if (nextLine[headerMap.get("po_group")].trim().equals("1")) {
							insertAccountNumbers.add(nextLine[headerMap.get("customer_no")].trim());
						} else {
							deleteAccountNumbers.add(nextLine[headerMap.get("customer_no")].trim());
						}
					}
					try {
						this.webJaguar.nonTransactionSafeInsertCustomerGroup(5, insertAccountNumbers);								
					} catch (Exception e) {
						// ignore duplicate keys
					}
					
					try {
						this.webJaguar.nonTransactionSafeDeleteCustomerGroup(5, deleteAccountNumbers);							
					} catch (Exception e) {
						if (sbuff.length() == 0) {
							sbuff.append(e.toString());
						}
						sbuff.append("\n'" + deleteAccountNumbers.toString() + "'");
					}
					
					if (sbuff.length() > 0) {
						notifyAdmin("updateCustomerGroup() - nonTransactionSafeDeleteCustomerGroup on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
					} else {
						Date end = new Date();
						sbuff.append("Started : " + dateFormatter.format(start) + "\n");
						sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");	
						sbuff.append(insertAccountNumbers.size() + " Inserted to Group Id = 5\n" + insertAccountNumbers.toString() + "\n\n" );
						sbuff.append(deleteAccountNumbers.size() + " Deleted from Group Id = 5\n\n" + deleteAccountNumbers.toString() + "\n\n");
						notifyAdmin("updateCustomerGroup() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
					}
	        	}
				reader.close();
				inStream.close();
	        }
			
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			notifyAdmin("Evergreen updateCustomerGroup() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}	
	
	/*
	 * export email and lastLogin for now
	 * Triggers the update everyday at 00:20 AM (server is at EST)
	 * 
	 * Log on to ftp using following credentials on configuration: 
	 * URL : 1st value
	 * Username : 2nd Value
	 * Password : 3rd Value
	 * 
	 * Create CUSTOMER_EXPORT.csv file in EVE folder. Location and name is specified as 8th value on Configuration
	 * CSV file is tab delimited.
	 * This csv contains Email, createDate and LastLogin.
	 * 
	 * If exception is thrown during updating products, an email will be sent to Shahin.
	 */
	public void exportCustomerInfo() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// EVERGREEN configuration
		String[] config = siteConfig.get("EVERGREEN").getValue().split(",");
		
		if (!(gSiteConfig.get("gACCOUNTING").equals("EVERGREEN") && config.length > 7 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if evergreen feature is disabled 
			return;
		}
		
		File baseFile = new File(getServletContext().getRealPath("/"));
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				baseFile = new File( (String) prop.get( "site.root" ) + "/" );
			}
		} catch ( Exception e ) {}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		Format formatter = new SimpleDateFormat("MM/dd/yyyy");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		try {
			// create CSV file
			// EVE/CUSTOMER_EXPORT.csv
			File exportFile = new File(baseFile + "/tempCUSTOMER_EXPORT.csv");
	    	CSVWriter writer = new CSVWriter(new FileWriter(exportFile));
	    	
	    	List<String> line = new ArrayList<String>();
	    	line.add("Email");
	    	line.add("createDate");
	    	line.add("LastLogin");
	    	
	    	String[] lines = new String[line.size()];
	    	writer.writeNext(line.toArray(lines));
	   
	    	
	    	CustomerSearch customerSearch = new CustomerSearch();
	    	customerSearch.setLimit(null);
	    	
	    	for (Customer customer: this.webJaguar.getCustomerListWithDate(customerSearch)) {
	    		line = new ArrayList<String>();
	    		
	        	line.add(customer.getUsername());
	        	line.add(customer.getCreated() != null ? formatter.format(customer.getCreated()) : "");
	        	line.add(customer.getLastLogin() != null ? formatter.format(customer.getLastLogin()) : "");
	        	
	        	lines = new String[line.size()];
	        	writer.writeNext(line.toArray(lines));
	    	}
	    	
	    	writer.close();
			
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	
	 
	        if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
	            // upload file
	            FileInputStream fis = new FileInputStream(exportFile);
	            ftp.storeFile("/"+config[7], fis );
	            fis.close();        	
	        }
	        
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
        
		} catch (Exception e) {
			e.printStackTrace();
			notifyAdmin("Evergreen exportCustomerInfo() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		sbuff.append("Create CUSTOMER_EXPORT.csv file in EVE folder. This csv contains Email, createDate and LastLogin." + "\n\n");
		notifyAEM("exportCustomerInfo() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}

	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		//msg.setTo("gerald.evergreen@gmail.com");
		msg.setCc(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			//mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
	
	private void notifyAEM(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
}