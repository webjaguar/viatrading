/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.20.2008
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;

public class Mas90 extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	/*
	 * To update inventory: 
	 * Create a csv file with columns: SKU, Inventory (Eg: ABCD,10) and upload it to the ftp. 
	 * Update Mas90 field in siteConfig with this format: ftp_domainName,ftp_username,ftp_password,invetory.csv,orders.csv,(if site is on wjserver500 use port number too)port number 
	 * Once data is imported file(inventory.csv) is deleted from the server. 
	 */
	public void updateInventory() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// Mas90 configuration
		String[] config = siteConfig.get("MAS90").getValue().split(",");
		
		if (!((Boolean) gSiteConfig.get("gINVENTORY") && (Boolean) gSiteConfig.get("gSITE_ACTIVE")
				&& gSiteConfig.get("gACCOUNTING").equals("MAS90") && config.length > 3)) {
			// return if inventory, mas90 feature is disabled 
			return;
		}

		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        
	        // ftp on wjserver500 uses port 2121. 
	        try {
	        	ftp.connect(config[0], Integer.parseInt(config[5]));
	        } catch (ArrayIndexOutOfBoundsException e) {
	        	ftp.connect(config[0]);	        	
	        } catch (Exception e) {
	        	ftp.connect(config[0]);
	        }
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	        
	        InputStream inStream = ftp.retrieveFileStream(config[3]);
	        if (inStream != null) {
	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream));
				String [] nextLine;
				while ((nextLine = reader.readNext()) != null) {
					HashMap<String, Object> map = new HashMap<String, Object>();
					String sku = nextLine[0].trim();
					String inventoryString = nextLine[1];
					if (inventoryString.endsWith("-")) {
						// move - at end to beginning of string
						inventoryString = "-" + inventoryString.substring(0, inventoryString.length()-1);
					}
					if (!sku.equals("")) {
						map.put("sku", sku);
						try {
							Integer inventory = Integer.parseInt(inventoryString);
							Inventory inventoryObject = new Inventory();
							inventoryObject.setSku(sku);
							inventoryObject = this.webJaguar.getInventory(inventoryObject);
							if(inventoryObject != null) {
								inventory = inventory - inventoryObject.getInventoryAFS();
								map.put("inventory_afs", inventoryObject.getInventoryAFS() + inventory);
								map.put("inventory", inventoryObject.getInventory() + inventory);
								
//								Please Add these for Inventory history
//								InventoryActivity inventoryActivity = new InventoryActivity();	
//								inventoryActivity.setSku(sku);
//								inventoryActivity.setInventory(inventoryObject.getInventory());
//								inventoryActivity.setinventoryAFS(inventoryObject.getinventoryAFS());
//								inventoryActivity.setAccessUserId(0);
//								inventoryActivity.setQuantity(inventory);
//								inventoryActivity.setInventory(inventoryObject.getinventoryAFS() + inventory);
//								inventoryActivity.setType("mass90 import");
//								this.webJaguar.productIventoryHistory(inventoryActivity);
								
							} else {
								map.put("inventory", inventory);
								map.put("inventory_afs", inventory);
							}
						} catch (NumberFormatException e) {
							continue;
						}
						data.add(map);
					}
				}
				reader.close();
				inStream.close();
				
				// delete file
				ftp.deleteFile(config[3]);
	        }
			
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			// do nothing
			notifyAdmin("1- Mas90 updateInventory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
		}
		
		// update inventory
		if (!data.isEmpty()) {
			this.webJaguar.updateInventoryBySkus(null, data);
			
			Date end = new Date();
			sbuff.append("Started : " + dateFormatter.format(start) + "\n");
			sbuff.append("Ended : " + dateFormatter.format(end) + "\n");
			sbuff.append("Products updated : " + data.size() + "\n\n");
			notifyAdmin("Success: Mas90 updateInventory() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
		} else {
			notifyAdmin("2- Mas90 updateInventory() on " + siteConfig.get("SITE_URL").getValue(), ""+data.size());
		}
	}
	
	/*
	 * Sample orders.csv Format
	 * 1000002,06/08/12,Summary,W000002,,,,,2815.5,0.0,2892.0,Credit Card,pending,,Shahin,Naji,,US,6B,,Aliso Viejo,CA,92656,9496008868,,,Shahin,Naji,,US,6B,,Aliso Viejo,CA,92656,9496008868,,,Shahin Naji,snaji@advancedemedia.com,Credit Card,,UPS 3 Day Select
	 * 1000002,06/08/12,Item,10-310-1002,Coupling 1/4" HoseBarb,2,,9.25
	 * 1000002,06/08/12,Item,ISCT0500,500VA Industrial Control Transformer,3,,99.0
	 * 1000002,06/08/12,Item,4-54-2000-2,Control Transformers,5,,500.0 
	 */
	public void exportInvoices() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// Mas90 configuration
		String[] config = siteConfig.get("MAS90").getValue().split(",");
		
		if (!(gSiteConfig.get("gACCOUNTING").equals("MAS90") && config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if mas90 feature is disabled 
			return;
		}

		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		File tempFolder = new File(getServletContext().getRealPath("temp"));	
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		
		OrderSearch search = new OrderSearch();
		search.setMas90(false);
		Date cutoff = new Date();
		search.setEndDate(cutoff);
		
		SimpleDateFormat dateFormatter2 = new SimpleDateFormat("MM/dd/yy");
		DecimalFormat df = new DecimalFormat("#0");
		df.setMinimumIntegerDigits(6);
		
		List<Order> orderList = this.webJaguar.getInvoiceExportList(search, siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());

		try {
			File invoiceFile = new File(tempFolder, "mas90invoice.csv");
			PrintWriter pw = new PrintWriter(new FileWriter(invoiceFile));
			for (Order order: orderList) {
				pw.print(order.getOrderId());
				pw.print(",");
				pw.print(dateFormatter2.format(order.getDateOrdered()));
				pw.print(",");
				pw.print("Summary");
				pw.print(",");
				if (order.getUserId() != null) {
					pw.print("W" + df.format(order.getUserId()));
				}
				pw.print(",");
				pw.print(",");
				pw.print(",");
				pw.print(",");
				pw.print(",");
				pw.print(order.getSubTotal());
				pw.print(",");
				if (order.getTax() != null) {
					pw.print(order.getTax());
				}
				pw.print(",");
				pw.print(order.getGrandTotal());
				pw.print(",");
				pw.print(order.getPaymentMethod());
				pw.print(",");
				pw.print(getMessageSourceAccessor().getMessage("orderStatus_" + order.getStatus()));
				pw.print(",");
				if (order.getTrackcode() != null) {
					pw.print(order.getTrackcode());
				}
				pw.print(",");
				addressInfo(order.getBilling(), pw);
				pw.print(",");
				addressInfo(order.getShipping(), pw);
				pw.print(",");
				pw.print(order.getBilling().getFirstName() + " " + order.getBilling().getLastName());
				pw.print(",");
				pw.print(order.getUserEmail());
				pw.print(",");
				pw.print(order.getPaymentMethod()); // Terms
				pw.print(",");
				pw.print(order.getPurchaseOrder());	// PO
				pw.print(",");
				pw.print(order.getShippingMethod()); // Ship Via
				pw.print(",");
				pw.print(order.getShippingCost()); // Ship Cost
				pw.println();
				// line items
	    		for (LineItem lineItem : order.getLineItems()) {
	    			pw.print(order.getOrderId());
					pw.print(",");
					pw.print(dateFormatter2.format(order.getDateOrdered()));
					pw.print(",");
					pw.print("Item");
					pw.print(",");
					if (lineItem.getProduct().getSku() != null) {
						pw.print(lineItem.getProduct().getSku().replace(",", " "));
					}
					pw.print(",");
					if (lineItem.getProduct().getName() != null) {
						pw.print(lineItem.getProduct().getName().replace(",", " "));						
					}
					pw.print(",");
					pw.print(lineItem.getTotalQty());
					pw.print(",");
	            	if (lineItem.getProduct().getCaseContent() != null) {
	            		pw.print(lineItem.getProduct().getCaseContent());
	        		}
					pw.print(",");
	            	if (lineItem.getUnitPrice() != null) {
	            		pw.print(lineItem.getUnitPrice());
	            	}
	    			pw.println();
	    		}
			}
			pw.close();
			
			// Connect and logon to FTP Server
    	    FTPClient ftp = new FTPClient();
    	    
    	    // ftp on wjserver500 uses port 2121.
    	    try {
	        	ftp.connect(config[0], Integer.parseInt(config[5]));
	        } catch (ArrayIndexOutOfBoundsException e) {
	        	ftp.connect(config[0]);	        	
	        }
    	    ftp.enterLocalPassiveMode();
    	    
    	    // login
    	    ftp.login( config[1].trim(), config[2].trim() );
    	   
    	    if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
	            // upload file
    	    	FileInputStream fis = new FileInputStream(invoiceFile);
    	        if (ftp.storeFile(config[4], fis)) {
    	        	this.webJaguar.updateOrderExported("mas90", cutoff, null);
    	        } else {
    	        	notifyAdmin("1- Mas90 exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
    	        }       	
	        } else {
	        	notifyAdmin("2- Mas90 exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
	        } 

	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
			
		} catch (Exception e) {
			e.printStackTrace();
			notifyAdmin("3- Mas90 exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n");
		sbuff.append("Invoices Exported : " + orderList.size() + "\n\n");
		notifyAdmin("Success: Mas90 exportInvoices() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
	
    private void addressInfo(Address address, PrintWriter pw) {   
    	address.replace(",", " ");
		pw.print(address.getFirstName());
		pw.print(",");
		pw.print(address.getLastName());
		pw.print(",");
		pw.print(address.getCompany());
		pw.print(",");
		pw.print(address.getCountry());
		pw.print(",");
		pw.print(address.getAddr1());
		pw.print(",");
		pw.print(address.getAddr2());
		pw.print(",");
		pw.print(address.getCity());
		pw.print(",");
		pw.print(address.getStateProvince());
		pw.print(",");
		pw.print(address.getZip());
		pw.print(",");
		pw.print(address.getPhone());
		pw.print(",");
		pw.print(address.getCellPhone());
		pw.print(",");
		pw.print(address.getFax());
    }
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}
