/* Copyright 2012 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 03.22.2012
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;

public class FrameStoreDirectJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void exportInvoices() {
		System.out.println("expost invoice start");
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// Mas90 configuration
		String[] config = siteConfig.get("MAS90").getValue().split(",");
		
		if (!(gSiteConfig.get("gACCOUNTING").equals("MAS90") && config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if mas90 feature is disabled 
			System.out.println("returned back");
			return;
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		File tempFolder = new File(getServletContext().getRealPath("temp"));	
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		
		OrderSearch search = new OrderSearch();
		search.setMas90(false);
		Date cutoff = new Date();
		search.setEndDate(cutoff);
		System.out.println("orders list start");
		List<Order> orderList = this.webJaguar.getInvoiceExportList(search, siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());
		System.out.println("orders list end "+orderList.size());
		
		try {
			File invoiceFile = new File(tempFolder, "invoiceM90.csv");
			File invoiceProductionFile = new File(tempFolder, "invoiceM90Production.csv");
			
			createOrderExport(invoiceFile, orderList);
			createOrderExportProduction(invoiceProductionFile, orderList);
			
			
			// Connect and logon to FTP Server
    	    FTPClient ftp = new FTPClient();
    	    ftp.connect(config[0].trim());    
    	    ftp.enterLocalPassiveMode();
    	    
    	    // login
    	    ftp.login( config[1].trim(), config[2].trim() );
    	    System.out.println("connecting to ftp");
			
    	    if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
	            // upload file
    	    	System.out.println("connected to ftp");
    			FileInputStream fis = new FileInputStream(invoiceFile);
    	    	FileInputStream fis2 = new FileInputStream(invoiceProductionFile);
    	        if (ftp.storeFile(config[3], fis) && ftp.storeFile(config[3].replace(".csv", "Production.csv"), fis2)) {
    	        	System.out.println("mark as success");
        			this.webJaguar.updateOrderExported("mas90", cutoff, null);
    	        } else {
    	        	System.out.println("does not mark ");
        			notifyAdmin("Mas90 exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
    	        }       	
	        }

	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
			
		} catch (Exception e) {
			e.printStackTrace();
			notifyAdmin("Mas90 exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		notifyAdmin("Success: Mas90 exportInvoices() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
	
	private void createOrderExport(File invoiceFile, List<Order> orderList) throws Exception  {
		SimpleDateFormat dateFormatter2 = new SimpleDateFormat("MM/dd/yy");
		DecimalFormat df = new DecimalFormat("#0");
		df.setMinimumIntegerDigits(6);
		
		PrintWriter pw = new PrintWriter(new FileWriter(invoiceFile));
		for (Order order: orderList) {
			pw.print(order.getOrderId());
			pw.print(",");
			pw.print(dateFormatter2.format(order.getDateOrdered()));
			pw.print(",");
			pw.print("Summary");
			pw.print(",");
			if (order.getUserId() != null) {
				pw.print("W" + df.format(order.getUserId()));
			}
			pw.print(",");
			pw.print(",");
			pw.print(",");
			pw.print(",");
			pw.print(order.getShippingCost());
			pw.print(",");
			pw.print(order.getSubTotal());
			pw.print(",");
			if (order.getTax() != null) {
				pw.print(order.getTax());
			}
			pw.print(",");
			pw.print(order.getGrandTotal());
			pw.print(",");
			pw.print(order.getPaymentMethod());
			pw.print(",");
			pw.print(getMessageSourceAccessor().getMessage("orderStatus_" + order.getStatus()));
			pw.print(",");
			if (order.getTrackcode() != null) {
				pw.print(order.getTrackcode());
			}
			pw.print(",");
			addressInfo(order.getBilling(), pw);
			pw.print(",");
			addressInfo(order.getShipping(), pw);
			pw.print(",");
			pw.print(order.getBilling().getFirstName() + " " + order.getBilling().getLastName());
			pw.print(",");
			pw.print(order.getUserEmail());
			pw.print(",");
			pw.print(order.getPaymentMethod()); // Terms
			pw.print(",");
			pw.print(order.getPurchaseOrder());	// PO
			pw.print(",");
			pw.print(order.getShippingMethod()); // Ship Via
			pw.println();
			// line items
    		for (LineItem lineItem : order.getLineItems()) {
    			pw.print(order.getOrderId());
				pw.print(",");
				pw.print(dateFormatter2.format(order.getDateOrdered()));
				pw.print(",");
				pw.print("Item");
				pw.print(",");
				if (lineItem.getProduct().getSku() != null) {
					pw.print(lineItem.getProduct().getSku().replace(",", " "));
				}
				pw.print(",");
				if (lineItem.getProduct().getName() != null) {
					pw.print(lineItem.getProduct().getName().replace(",", " "));						
				}
				pw.print(",");
				pw.print(lineItem.getQuantity());
				pw.print(",");
            	if (lineItem.getProduct().getCaseContent() != null) {
            		pw.print(lineItem.getProduct().getCaseContent());
        		}
				pw.print(",");
            	if (lineItem.getUnitPrice() != null) {
            		pw.print(lineItem.getUnitPrice());
            	}
    			pw.println();
    		}
		}
		pw.close();
	}
	
	private void createOrderExportProduction(File invoiceProductionFile, List<Order> orderList) throws Exception  {
		
		SimpleDateFormat dateFormatter2 = new SimpleDateFormat("MM/dd/yy");
		DecimalFormat df = new DecimalFormat("#0");
		df.setMinimumIntegerDigits(6);
		
		PrintWriter pw = new PrintWriter(new FileWriter(invoiceProductionFile));
		for (Order order: orderList) {
			// line items
    		for (LineItem lineItem : order.getLineItems()) {
    			Product product = this.webJaguar.getProductById(lineItem.getProductId(), 0, false, null);
    			pw.print(order.getOrderId());
    			pw.print(",");
    			pw.print(dateFormatter2.format(order.getDateOrdered()));
    			pw.print(",");
    			if (lineItem.getProduct().getSku() != null) {
					pw.print(lineItem.getProduct().getSku().replace(",", " "));
				}
    			pw.print(",");
				pw.print(lineItem.getQuantity());
				pw.print(",");
				if (lineItem.getUnitPrice() != null) {
            		pw.print(lineItem.getUnitPrice());
            	}
				pw.print(",");
				if (lineItem.getProduct().getName() != null) {
					pw.print(lineItem.getProduct().getName().replace(",", " "));
				}
				pw.print(",");
//				if (lineItem.getProductAttributes() != null && !lineItem.getProductAttributes().isEmpty() ) {
//					for (ProductAttribute pa : lineItem.getProductAttributes()) {
//						pw.print("|"+pa.getOptionName().replace(',', ' ') + " : " + pa.getValueString().replace(',', ' ')+"|");
//						pw.print(",");
//					}
//				}
				if (product != null) {
					pw.print(product.getWeight());
					pw.print(",");
					pw.print(product.getPriceTable1());
					pw.print(",");
					pw.print(product.getField1());
					pw.print(",");
					pw.print(product.getField2());
					pw.print(",");
					pw.print(product.getField3());
					pw.print(",");
					pw.print(product.getField4());
					pw.print(",");
					pw.print(product.getField5());
					pw.print(",");
					pw.print(product.getField6());
					pw.print(",");
					pw.print(product.getField7());
					pw.print(",");
					pw.print(product.getField8());
					pw.print(",");
					pw.print(product.getField9());
					pw.print(",");
					pw.print(product.getField10());
					pw.print(",");
					pw.print(product.getField11());
					pw.print(",");
					pw.print(product.getField12());
					pw.print(",");
					pw.print(product.getField13());
					pw.print(",");
					pw.print(product.getField14());
					pw.print(",");
					pw.print(product.getField15());
					pw.print(",");
				}
				if (lineItem.getProductAttributes() != null && !lineItem.getProductAttributes().isEmpty() ) {
					for (ProductAttribute pa : lineItem.getProductAttributes()) {
						pw.print("|"+pa.getOptionName().replace(',', ' ') + " : " + pa.getValueString().replace(',', ' ')+"|");
						pw.print(",");
					}
				}
    			pw.println();
    		}
		}
		pw.close();
	}
	
	private void addressInfo(Address address, PrintWriter pw) {   
    	address.replace(",", " ");
		pw.print(address.getFirstName());
		pw.print(",");
		pw.print(address.getLastName());
		pw.print(",");
		pw.print(address.getCompany());
		pw.print(",");
		pw.print(address.getCountry());
		pw.print(",");
		pw.print(address.getAddr1());
		pw.print(",");
		pw.print(address.getAddr2());
		pw.print(",");
		pw.print(address.getCity());
		pw.print(",");
		pw.print(address.getStateProvince());
		pw.print(",");
		pw.print(address.getZip());
		pw.print(",");
		pw.print(address.getPhone());
		pw.print(",");
		pw.print(address.getCellPhone());
		pw.print(",");
		pw.print(address.getFax());
    }
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
	
	// Copies src file to dst file.
	// If the dst file does not exist, it is created
	private void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}
}
