/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.09.2009
 */

package com.webjaguar.web.quartz;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;

public class Mas200 extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	private JdbcTemplate jdbcTemplate;
	public void setDataSource(DataSource dataSource) { 
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public void updateInventory() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map<String, CsvFeed> csvFeedMap = this.webJaguar.getCsvFeedMapping("mas200-inventory");
		
		// Mas200 configuration
		String[] config = siteConfig.get("MAS200").getValue().split(",");
		
		if (!((Boolean) gSiteConfig.get("gINVENTORY") && (Boolean) gSiteConfig.get("gSITE_ACTIVE")
				&& config.length > 3)) {
			// return if inventory, mas200 feature is disabled 
			return;
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	        
	        InputStream inStream = ftp.retrieveFileStream(config[3]);

	        if (inStream != null) {
	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream));
	        	
	        	List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		        
	        	boolean deleteFile = false;
	        	
	        	String[] headers = reader.readNext();
	        	for (int i=0; i<headers.length; i++) {
					if (csvFeedMap.containsKey(headers[i].toLowerCase())) {
						CsvFeed csvFeed = csvFeedMap.get(headers[i].toLowerCase());
						csvFeed.setColumn(i);
						csvFeedList.add(csvFeed);
					}
				}
	        	
				String [] nextLine;
				while ((nextLine = reader.readNext()) != null) {
					HashMap<String, Object> map = new HashMap<String, Object>();
					
					for (CsvFeed csvFeed: csvFeedList) {
						if (csvFeed.getColumn() != null) {
							String sValue = nextLine[csvFeed.getColumn()].trim();
							if (csvFeed.isZeroAsNull()) {
								try {
									Double value = Double.parseDouble(sValue);
									if (value == 0) {
										value = null;
									}
									map.put(csvFeed.getColumnName(), value);
								} catch (NumberFormatException e) {
									map.put(csvFeed.getColumnName(), null);									
								}
							} else {
								map.put(csvFeed.getColumnName(), sValue);								
							}
							
						}
					}
					
					data.add(map);
					
					if (data.size() == 5000) {
						// products
						this.webJaguar.updateProduct(csvFeedList, data, null);
						data.clear();
						deleteFile = true;
					}
				}
				
				if (data.size() > 0) {
					// products
					this.webJaguar.updateProduct(csvFeedList, data, null);
					data.clear();
					deleteFile = true;
				}
				
				reader.close();
				inStream.close();
				
				if (deleteFile) {
					// delete file
					ftp.rename(config[3], config[3] + "OLD");
					//ftp.deleteFile(config[3]);	
					
				}
	        } else {
	        	notifyAdmin("MAS200 updateInvoices() on" + siteConfig.get("SITE_URL").getValue(), config[3] +" not found");
	        }
			
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			notifyAdmin("MAS200 updateInventory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		sbuff.append("Read inventory.csv file." + "\n\n");
		notifyAdmin("MAS200 updateInvoices() on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
	
	public void exportInvoices() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		if (!(siteConfig.get("MAS200").getValue().trim().length() > 0 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if Mas200 feature is disabled 
			return;
		}
		
		OrderSearch search = new OrderSearch();
		search.setMas200(false);
		Date cutoff = new Date();
		search.setEndDate(cutoff);
		
		List<Order> orderList = this.webJaguar.getInvoiceExportList(search, null);
		if (orderList.isEmpty()) {
			return;
		}
		Connection connection = null; 
		
		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/jdbc.properties")));
			
			Class.forName((String) prop.get("jbd.driverClassName")).newInstance(); 
			connection = DriverManager.getConnection((String) prop.get("jbd.url"), (String) prop.get("jbd.username"), (String) prop.get("jbd.password"));
			connection.setAutoCommit(false);  // transaction
			
			this.webJaguar.insertJbdInvoices(connection, orderList, cutoff);
		} catch (Exception e) {
			//An error occurred so we rollback the changes.
			if (connection != null) {
				try {
					connection.rollback();
				} catch (SQLException ex1) {
					ex1.printStackTrace();
				}				
			}
			notifyAdmin("MAS200 exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}	

	public void updateInvoices() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		if (!(siteConfig.get("MAS200").getValue().trim().length() > 0 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if Mas200 feature is disabled 
			return;
		}

		try {
			List orders = jdbcTemplate.queryForList(
	    			"SELECT " +
	    			"    SalesOrderNumber " +
	    			"  , WebOrderNo " +
	    			"  , OrderStatus " +
	    			"FROM " +
	    			"  JBD_MASORDERHIST " +
	    			"WHERE " +
	    			"  WebOrderNo > 0 " +
	    			"GROUP BY " +
	    			"  WebOrderNo "
					);
			for (Object JBD_MASORDERHIST: orders) {
				Order order = new Order();
				order.setMas200orderNo(((Map<String, String>) JBD_MASORDERHIST).get("SalesOrderNumber"));
				order.setMas200status(((Map<String, String>) JBD_MASORDERHIST).get("OrderStatus"));			
				// WebOrderNo used to be a String column. 
				// order.setOrderId(Integer.parseInt(((Map<String, String>) JBD_MASORDERHIST).get("WebOrderNo")));
				order.setOrderId(((Map<String, BigDecimal>) JBD_MASORDERHIST).get("WebOrderNo").intValue());
				this.webJaguar.nonTransactionSafeUpdateMas200order(order);
			}			
		} catch (Exception e) {
			notifyAdmin("MAS200 updateInvoices() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}		
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}
