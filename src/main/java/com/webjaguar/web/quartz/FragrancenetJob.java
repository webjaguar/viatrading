/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 02.11.2009
 */

package com.webjaguar.web.quartz;

import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;

public class FragrancenetJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void updateInventory() {

		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map<String, CsvFeed> csvFeedMap = this.webJaguar.getCsvFeedMapping("fragrancenet-inventory");
		Map <String, Long> categoryMap = this.webJaguar.getCategoryExternalIdMap();
		
		if (!(siteConfig.get("FRAGRANCENET").getValue().length() > 0 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if fragrancenet feature is disabled
			return;
		}
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		int imageCol = -1;
		int productCategory = -1;
		int itemType = -1;
		int gender = -1;
		int wholesalePrice = -1;
		
		NumberFormat nf = new DecimalFormat("#0.00");

		Double wholesalePriceChange = null;
		try {
			wholesalePriceChange = Double.parseDouble(siteConfig.get("FRAGRANCENET").getValue());			
		} catch (NumberFormatException e) {
			// return if fragrancenet feature is disabled 
			return;
		}		
		Double priceChange = null;
		try {
			priceChange = Double.parseDouble(siteConfig.get("FRAGRANCENET_PRICECHANGE").getValue());			
		} catch (NumberFormatException e) {
			// do nothing
		}
		Double priceTableChange[] = { null, null, null, null };
		for (int i=1; i<priceTableChange.length; i++) {
			try {
				priceTableChange[i] = Double.parseDouble(siteConfig.get("FRAGRANCENET_PRICETABLE" + i + "_CHANGE").getValue());			
			} catch (NumberFormatException e) {
				// do nothing
			}		
		}
		
		try {
	    	Authenticator.setDefault(new MyAuthenticator());
	    	
	    	//URL url = new URL("http://www.fragrancenet-wholesale.com/wsdata/datafeed2.txt");
	    	URL url = new URL("http://www.fragrancenet.com/wsdata/datafeed2.txt");
	    	// This username and password remains same for all customers
	    	String usernameAndPassword = "fnetdatafeed:datafeed";
			String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(usernameAndPassword.getBytes());
			
			URLConnection fragrancetConnection = url.openConnection();
			fragrancetConnection.setRequestProperty("Authorization", basicAuth);
			
	    	CSVReader reader = new CSVReader(new InputStreamReader(fragrancetConnection.getInputStream()), '\t', CSVWriter.NO_QUOTE_CHARACTER); 
	      	
	    	String[] headers = reader.readNext();
        	for (int i=0; i<headers.length; i++) {
				if (csvFeedMap.containsKey(headers[i].toLowerCase())) {
					CsvFeed csvFeed = csvFeedMap.get(headers[i].toLowerCase());
					csvFeed.setColumn(i);
					csvFeedList.add(csvFeed);
				}
			}
        	for (int i=0; i<headers.length; i++) {
				if (headers[i].equalsIgnoreCase("Product Category")) {
					productCategory = i;
				} else if (headers[i].equalsIgnoreCase("Item Type")) {
					itemType = i;
				} else if (headers[i].equalsIgnoreCase("Gender")) {
					gender = i;
				} else if (headers[i].equalsIgnoreCase("Image Large")) {
					imageCol = i;
				} else if (headers[i].equalsIgnoreCase("FNET Wholesale Price")) {
					wholesalePrice = i;
				}
			}
	    	
	    	String[] nextLine;
	    	while ((nextLine = reader.readNext()) != null) {
	    		HashMap<String, Object> map = new HashMap<String, Object>();
	    		
	    		for (CsvFeed csvFeed: csvFeedList) {
	    			String value = nextLine[csvFeed.getColumn()];
	    			if (csvFeed.getColumn() == wholesalePrice) {
		    			try {
							Double price = Double.parseDouble(value);
							price = price + price*wholesalePriceChange/100;					
							value = nf.format(price);
			    			if (priceChange != null) {
			    				map.put("price_1", price + price*priceChange/100);
			    			}
		    				for (int i=1; i<priceTableChange.length; i++) {
		    					if (priceTableChange[i] != null) {
				    				map.put("price_table_" + i, price + price*priceTableChange[i]/100);
		    					}
		    				}
						} catch (NumberFormatException e) {
							value = null;
						}
	    			}
	    			map.put(csvFeed.getColumnName(), value);
	    		}
	    		
	    		// image
				if (imageCol > -1) {
					map.put("image", nextLine[imageCol]);
				}
	    		
				// categories
				map.put("categories", "");
				if (productCategory > -1 && categoryMap.containsKey(nextLine[productCategory].toLowerCase())) {
					map.put("categories", map.get("categories") + categoryMap.get(nextLine[productCategory].toLowerCase()).toString() + ",");			
				}
				if (productCategory > -1 && itemType > -1 && categoryMap.containsKey(nextLine[productCategory].toLowerCase() + "--" + nextLine[itemType].toLowerCase())) {
					map.put("categories", map.get("categories") + categoryMap.get(nextLine[productCategory].toLowerCase() + "--" + nextLine[itemType].toLowerCase()).toString() + ",");								
				}
				if (gender > -1 && categoryMap.containsKey(nextLine[gender].toLowerCase())) {
					map.put("categories", map.get("categories") + categoryMap.get(nextLine[gender].toLowerCase()).toString() + ",");						
				}				
				
				// NewEggMall 
				if (((String) gSiteConfig.get("gDATA_FEED")).contains("newEggMall,") && productCategory > -1 
						&& csvFeedMap.containsKey("neweggmall industry") && csvFeedMap.containsKey("neweggmall category")) {
					if (nextLine[productCategory].equalsIgnoreCase("Candle")) {
						map.put(csvFeedMap.get("neweggmall industry").getColumnName(), "Home Living");
						map.put(csvFeedMap.get("neweggmall category").getColumnName(), "Decorative Accents");
					} else if (nextLine[productCategory].equalsIgnoreCase("Aromatherapy")) {
						map.put(csvFeedMap.get("neweggmall industry").getColumnName(), "Home Living");
						map.put(csvFeedMap.get("neweggmall category").getColumnName(), "Aromatherapy");
					} else if (nextLine[productCategory].equalsIgnoreCase("Fragrance")) {
						map.put(csvFeedMap.get("neweggmall industry").getColumnName(), "Beauty");
						map.put(csvFeedMap.get("neweggmall category").getColumnName(), "Fragrances");
					} else if (nextLine[productCategory].equalsIgnoreCase("Haircare")) {
						map.put(csvFeedMap.get("neweggmall industry").getColumnName(), "Beauty");
						map.put(csvFeedMap.get("neweggmall category").getColumnName(), "Hair Care Products");
					} else if (nextLine[productCategory].equalsIgnoreCase("Skincare")) {
						map.put(csvFeedMap.get("neweggmall industry").getColumnName(), "Beauty");
						map.put(csvFeedMap.get("neweggmall category").getColumnName(), "Cleansers");
					}		
				}
				
	    		map.put("feed", "FRAGRANCENET");				
				map.put("feed_new", true);
				
	    		// make item active
	    		map.put("active", true);
				
	    		data.add(map);
	    	}
	    	
	    	reader.close();
			
			// update
			if (!data.isEmpty() && data.get(0).containsKey("sku")) {
				csvFeedList.add(new CsvFeed("feed"));
				csvFeedList.add(new CsvFeed("feed_new"));
				csvFeedList.add(new CsvFeed("active"));
				csvFeedList.add(new CsvFeed("price_1"));
				for (int i=1; i<priceTableChange.length; i++) {
					csvFeedList.add(new CsvFeed("price_table_" + i));
				}				
				// NewEggMall 
				if (((String) gSiteConfig.get( "gDATA_FEED" )).contains("newEggMall,") && productCategory > -1 
						&& csvFeedMap.containsKey("neweggmall industry") && csvFeedMap.containsKey("neweggmall category")) {
					csvFeedList.add(csvFeedMap.get("neweggmall industry"));
					csvFeedList.add(csvFeedMap.get("neweggmall category"));
				}
				this.webJaguar.updateFragrancenetProduct(csvFeedList, data);
			}
			
		} catch (Exception e) {
			notifyAdmin("Fragrancenet updateInventory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		sbuff.append("Read datafeed2.txt file." + "\n\n");
		notifyAdmin("updateInventory() - Fragrancenet on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
	
	private class MyAuthenticator extends Authenticator {
		// This method is called when a password-protected URL is accessed
		protected PasswordAuthentication getPasswordAuthentication() {
			String username = "fnetdatafeed";
			String password = "datafeed";

			// Return the information
			return new PasswordAuthentication(username, password.toCharArray());
		}
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
}