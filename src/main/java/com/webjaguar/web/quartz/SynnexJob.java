/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.28.2009
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Product;
import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.synnex.SynnexCategory;

public class SynnexJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
    
	public void updateCatalog() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		List<CsvFeed> csvFeedList = this.webJaguar.getCsvFeed("synnex-catalog");
		
		// Synnex configuration
		String[] config = siteConfig.get("SYNNEX").getValue().split(",");
		
		if (!(config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE")) || csvFeedList.isEmpty()) {
			// return if synnex feature is disabled 
			return;
		}
		
		String[] extra = {};
		if (siteConfig.get("SYNNEX_EXTRA").getValue().trim().length() > 0) {
			extra = siteConfig.get("SYNNEX_EXTRA").getValue().split(",");
		}
		
		// supplier id
		Integer supplierId = Integer.parseInt(config[4]);
		if (!this.webJaguar.isValidSupplierId(supplierId)) {
			// not a valid supplier
			return;
		}
		
		File tempFolder = new File(getServletContext().getRealPath("temp"));	
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File synnexDir = new File(tempFolder, "synnex");
		if (!synnexDir.exists()) {
			synnexDir.mkdir();
		}
		File priceZip = new File(synnexDir, config[3]);
		File categoryCsv = new File(synnexDir, "category_list.txt");
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        
	        // categories
	        boolean categoryDownloaded = false;
	        ftp.connect(config[0]);
	        ftp.login("stock", "stockedi");
	        ftp.enterLocalPassiveMode();
	        if (ftp.retrieveFile("category_list.txt", new FileOutputStream(categoryCsv))) {
	        	categoryDownloaded = true;
	        }
	        ftp.logout();
	        
	        // login
	        ftp.connect(config[0]);
	        ftp.login(config[1], config[2]);
				        
	        if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
		        ftp.enterLocalPassiveMode();
		        ftp.setFileType(FTP.BINARY_FILE_TYPE);	// for zip files	        
		        
		        boolean hasCatId = false;
	        	Map<String, SynnexCategory> categoryMap = this.webJaguar.getSynnexCategoryMap();
	        	for (SynnexCategory category: categoryMap.values()) {
	        		if (category.getCatId() != null) {
	        			hasCatId = true;
	        			break;
	        		}
	        	}
	        	
		        // Price & Availability file
		        boolean priceDownloaded = false;
	        	if (hasCatId && ftp.retrieveFile(config[3], new FileOutputStream(priceZip))) {
	        		priceDownloaded = true;
	        	}	  	        	
	        	
    	        // Logout from the FTP Server and disconnect
    	        ftp.logout();
    	        ftp.disconnect();	        			        	
    	        
    	        if (categoryDownloaded) parseCategoryFile(categoryCsv);
    	        
    	        if (priceDownloaded) parsePriceFile(priceZip, categoryMap, csvFeedList, supplierId, extra);
 
	        } else { // login failed
	        	notifyAdmin("SYNNEX updateCatalog() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
	        }
		} catch (Exception e) {
			notifyAdmin("SYNNEX updateCatalog() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}
	
	private int parseCategoryFile(File categoryCsv) throws Exception {
		
		int count = 0;
		
		List<SynnexCategory> categories = new ArrayList<SynnexCategory>();
		Set<String> categorySet = new HashSet<String>();
		CSVReader reader = new CSVReader(new FileReader(categoryCsv));
		
		// get headers
    	Map<String, Integer> headerMap = new HashMap<String, Integer>();			        	
		String[] headers = reader.readNext();
		if (headers != null) {
	    	for (int i=0; i<headers.length; i++) {
	    		headerMap.put(headers[i], i);
			}			
		}

		String[] nextLine;

		while ((nextLine = reader.readNext()) != null) {
			String catCode = nextLine[headerMap.get("CatCode")];
			if (catCode.length() < 9) {
				continue;
			}
			SynnexCategory category = new SynnexCategory();
			category.setCat(catCode.substring(0, 3));
			category.setSub(catCode.substring(3, 6));
			if (nextLine[headerMap.get("Level3Description")].length() > 0) {
				// level 3
				category.setSubSub(catCode.substring(6, 9));
				category.setDescr(nextLine[headerMap.get("Level3Description")]);
			} else {
				// level 2
				category.setDescr(nextLine[headerMap.get("Level2Description")]);				
				category.setSubSub("*");
				categorySet.add(category.getCat() + category.getSub() + "*");
			}
			categories.add(category);
			// level 1
			if (categorySet.add(category.getCat() + "*")) {
				SynnexCategory level1 = new SynnexCategory();
				level1.setCat(catCode.substring(0, 3));
				level1.setSub("*");
				level1.setSubSub("*");
				level1.setDescr(nextLine[headerMap.get("Level1Description")]);
				categories.add(level1);
			}
			// level 2
			if (categorySet.add(category.getCat() + category.getSub() + "*")) {
				SynnexCategory level2 = new SynnexCategory();
				level2.setCat(catCode.substring(0, 3));
				level2.setSub(catCode.substring(3, 6));
				level2.setSubSub("*");
				level2.setDescr(nextLine[headerMap.get("Level2Description")]);
				categories.add(level2);
			}
			
			count++;
		}
		
		if (categories.size() > 0) {
			this.webJaguar.nonTransactionSafeInsertSynnexCategories(categories);
		}
		
		reader.close();
		
		return count;
	}	
	
	private int parsePriceFile(File priceZip, Map<String, SynnexCategory> categoryMap, List<CsvFeed> csvFeedList, int supplierId, String[] extra) throws Exception {
		
		csvFeedList.add(new CsvFeed("feed"));
		csvFeedList.add(new CsvFeed("feed_new"));
		csvFeedList.add(new CsvFeed("price_1"));

		// extra configuration
		for (int i=0; i<extra.length; i++) {
			csvFeedList.add(new CsvFeed(extra[i].substring(0, extra[i].indexOf("="))));
		}
		
		// according to Synnex Price & Availability (P&A)
		int cost = 20;
		int cat = 24;
		
		int total = 0;
		
		ZipFile zipFile = new ZipFile(priceZip);
		ZipEntry entry = zipFile.entries().nextElement(); // first entry
		if (entry != null) {
			InputStream inStream = zipFile.getInputStream(entry);
			CSVReader reader = new CSVReader(new InputStreamReader(inStream), '~', CSVWriter.NO_QUOTE_CHARACTER);

			List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
			List<Map <String, Object>> categoryData = new ArrayList<Map <String, Object>>();
			
			String[] nextLine = reader.readNext(); // 1st line

			while ((nextLine = reader.readNext()) != null) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				List<Integer> catIds = new ArrayList<Integer>();
				
				Double priceChange = null;
				
				// categories
				String code = nextLine[cat].trim();
				if (code.length() >= 3 && categoryMap.containsKey(code.substring(0, 3) + "**")) {
					// level 1
					SynnexCategory category = categoryMap.get(code.substring(0, 3) + "**");
					if (category.getCatId() != null) catIds.add(category.getCatId());
					if (category.getPriceChange() != null) priceChange = category.getPriceChange();
				}
				if (code.length() >= 6 && categoryMap.containsKey(code.substring(0, 6) + "*")) {
					// level 2
					SynnexCategory category = categoryMap.get(code.substring(0, 6) + "*");
					if (category.getCatId() != null) catIds.add(category.getCatId());
					if (category.getPriceChange() != null) priceChange = category.getPriceChange();
				}
				if (code.length() >= 9 && categoryMap.containsKey(code.substring(0, 9))) {
					// level 3
					SynnexCategory category = categoryMap.get(code.substring(0, 9));
					if (category.getCatId() != null) catIds.add(category.getCatId());
					if (category.getPriceChange() != null) priceChange = category.getPriceChange();
				}
				if (catIds.isEmpty()) {
					// skip this item
					continue;
				}
				
				for (CsvFeed csvFeed: csvFeedList) {
					if (csvFeed.getColumn() != null) {
						String sValue = nextLine[csvFeed.getColumn()];
						if (csvFeed.getColumnName().equals("sku")) {
							map.put("sku", "SYN-" + sValue);
						} else if (csvFeed.isZeroAsNull()) {
    						try {
    							Double value = Double.parseDouble(sValue);
    							if (value == 0) {
    								value = null;
    							}
								map.put(csvFeed.getColumnName(), value);
    						} catch (NumberFormatException e) {
    							map.put(csvFeed.getColumnName(), null);									
    						}
    					} else {
    						map.put(csvFeed.getColumnName(), sValue);							
    					}	        						
					}
				}
				
				map.put("feed", "SYNNEX");
				map.put("feed_new", true);
				map.put("cost", Double.parseDouble(nextLine[cost]));
				
				// price change
				if (priceChange != null) {
					map.put("price_1", (Double) map.get("cost") + (Double) map.get("cost")*priceChange/100);
				}

				// extra configuration
				for (int i=0; i<extra.length; i++) {
					map.put(extra[i].substring(0, extra[i].indexOf("=")), extra[i].substring(extra[i].indexOf("=")+1));
				}
				
				map.put("productId", this.webJaguar.getProductIdBySku((String) map.get("sku")));
				if (map.get("productId") == null) {
					// new item
					Product newProduct = new Product((String) map.get("sku"), "Synnex Feed");
					this.webJaguar.insertProduct(newProduct, null);
					map.put("productId", newProduct.getId());
					
					// add supplier
					Supplier supplier = new Supplier();
					supplier.setId(supplierId);
					supplier.setSku((String) map.get("sku"));
					supplier.setSupplierSku(((String) map.get("sku")).substring(4)); // remove leading SYN-
					this.webJaguar.insertProductSupplier(supplier);
				}
				for (Integer catId: catIds) {
					HashMap<String, Object> mapCategory = new HashMap<String, Object>();
					mapCategory.put("productId", map.get("productId"));
					mapCategory.put("categoryId", catId);
					categoryData.add(mapCategory);
				}
				
				total++;
				
				data.add(map);
				
				if (data.size() == 5000) {
					// products
					this.webJaguar.updateProduct(csvFeedList, data, null);
					
					// associate to supplier
					this.webJaguar.nonTransactionSafeUpdateProductSupplier(supplierId, data);
					data.clear();
					
					// associate to categories				
					this.webJaguar.nonTransactionSafeInsertCategories(categoryData);
					categoryData.clear();
				}
			}
			
			if (data.size() > 0) {
				// products
				this.webJaguar.updateProduct(csvFeedList, data, null);	
				
				// associate to supplier
				this.webJaguar.nonTransactionSafeUpdateProductSupplier(supplierId, data);
				data.clear();	
				
				// associate to categories
				this.webJaguar.nonTransactionSafeInsertCategories(categoryData);
				categoryData.clear();
			}
			
			reader.close();
			inStream.close();
			
			// make old synnex items inactive
			this.webJaguar.deleteOldProducts("SYNNEX", true);				
		}
		zipFile.close();	
		
		return total;
	}	

	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}
