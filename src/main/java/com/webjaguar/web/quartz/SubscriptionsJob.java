/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.21.2008
 */

package com.webjaguar.web.quartz;

import java.util.Date;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CreditCard;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Subscription;

public class SubscriptionsJob extends QuartzJobBean {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
		// do the actual work
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		if (!((Boolean) gSiteConfig.get("gSUBSCRIPTION") && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if subscription feature is disabled
			return;
		}
		
		
		OrderStatus orderStatus = new OrderStatus();
		orderStatus.setStatus( "p" ); // pending

		for (Subscription subscription: this.webJaguar.getDueSubscriptions()) {
			// get subscription
			subscription = this.webJaguar.getSubscription(subscription.getCode());
			
			Order order = initOrder(gSiteConfig, siteConfig, subscription, this.webJaguar.getCustomerById(subscription.getUserId()));
			
			// inventory
			boolean inventoryTypeOnFly = false; 
			if ((Boolean) gSiteConfig.get("gINVENTORY") && siteConfig.get( "INVENTORY_DEDUCT_TYPE" ).getValue().equals( "fly" )) {
				inventoryTypeOnFly = true; 
			}
			// affiliate 
			int gAFFILIATE = (Integer) gSiteConfig.get( "gAFFILIATE" );
			// insert order
			this.webJaguar.insertOrder(order, orderStatus, false, true, gAFFILIATE, siteConfig, gSiteConfig);
		}
	}
	
	private Order initOrder(Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig, Subscription subscription, Customer customer) {
		Order order = new Order();
		order.setSubscriptionCode(subscription.getCode());
		order.setUserId(subscription.getUserId());
		
		// get old order
		Order oldOrder = null;
		if (subscription.getOrderIdLast() != null) {
			oldOrder = this.webJaguar.getOrder(subscription.getOrderIdLast(), null);
			order.setShippingMethod(oldOrder.getShippingMethod());
			order.setShippingCost(oldOrder.getShippingCost());	
		} else {
			// first recurring order
			oldOrder = this.webJaguar.getOrder(subscription.getOrderIdOrig(), null);
			order.setShippingMethod("To Be Determined");
		}
		order.setBilling(oldOrder.getBilling());
		order.setShipping(oldOrder.getShipping());
		
		// for now credit card payments only
		order.setPaymentMethod("Credit Card");
		order.setCreditCard(new CreditCard());
		if (oldOrder.getCreditCard() != null) {
		    order.getCreditCard().setType(oldOrder.getCreditCard().getType());
		    order.getCreditCard().setNumber(oldOrder.getCreditCard().getNumber());
		    order.getCreditCard().setExpireMonth(oldOrder.getCreditCard().getExpireMonth());
		    order.getCreditCard().setExpireYear(oldOrder.getCreditCard().getExpireYear());
		    order.getCreditCard().setCardCode(oldOrder.getCreditCard().getCardCode());
		} else {
		    order.getCreditCard().setType("");
		    order.getCreditCard().setNumber("");
		    order.getCreditCard().setExpireMonth("");
		    order.getCreditCard().setExpireYear("");
		    order.getCreditCard().setCardCode("");			
		}
		
		// set line item
		LineItem lineItem = new LineItem();
		lineItem.setLineNumber(1);
		lineItem.setProduct(subscription.getProduct());
		lineItem.setQuantity(subscription.getQuantity());
		lineItem.setUnitPrice(subscription.getUnitPrice());
		lineItem.setOriginalPrice(subscription.getUnitPrice());
		lineItem.setProductAttributes(subscription.getProductAttributes());
		order.addLineItem(lineItem);
		
		if (lineItem.getTotalPrice() != null) {
			order.setSubTotal(lineItem.getTotalPrice());
		} else {
			order.setSubTotal(0.0);
		}
		
		order.setTaxExemption((customer.getTaxId() == null || customer.getTaxId().trim().equals( "" )) ? false : true);
		this.webJaguar.updateTaxAndTotal(gSiteConfig, globalDao, order);
		
		// set language
		if(!((String)gSiteConfig.get("gI18N")).isEmpty()){
			order.setLanguageCode(customer.getLanguageCode());
		}
		
		return order;
	}
	
}
