/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 12.16.2008
 */

package com.webjaguar.web.quartz;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.EmailCampaign;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Product;
import com.webjaguar.model.Promo;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.SiteMessageSearch;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.web.admin.massEmail.ContentBuilderController;
import com.webjaguar.web.domain.Utilities;
import com.webjaguar.web.form.MassEmailForm;

import org.apache.commons.codec.binary.Base64;

public class MassEmailJob implements Job{

	private WebJaguarFacade webJaguar;
	private JavaMailSenderImpl mailSender2;
	private ContentBuilderController contentBuilder;
	private GlobalDao globalDao;
	private MessageSourceAccessor  messageSourceAccessor;
	
	private final static String KEY = "AECCF9008EDBF4535CCE2345435364FA";
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	MassEmailForm massEmailForm = new MassEmailForm();
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		massEmailForm = (MassEmailForm)context.getJobDetail().getJobDataMap().get("massEmailForm");
		int emailSent = 0;
		Calendar emailCampaignStart = Calendar.getInstance();
		
		Map<Integer, Integer> messageMap = null;
		
		try {
			webJaguar = (WebJaguarFacade)context.getScheduler().getContext().get("webJaguar");
			mailSender2 = (JavaMailSenderImpl)context.getScheduler().getContext().get("mailSender2");
			globalDao = (GlobalDao)context.getScheduler().getContext().get("globalDao");
			messageSourceAccessor = (MessageSourceAccessor)context.getJobDetail().getJobDataMap().get("messageContect");
			contentBuilder = (ContentBuilderController)context.getScheduler().getContext().get("contentBuilder");	
			siteConfig = this.webJaguar.getSiteConfig();
			gSiteConfig = this.globalDao.getGlobalSiteConfig();
			messageMap = this.webJaguar.getSiteMessageListByGroupId(6);
			
			Boolean find = false;
			Iterator iterator = messageMap.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry mapEntry = (Map.Entry) iterator.next();
				if(massEmailForm!=null && massEmailForm.getMessageId()!=null && mapEntry!=null && mapEntry.getKey()!=null)
				if(mapEntry.getKey().toString().equals(massEmailForm.getMessageId().toString())){
					find = true;
				}
			}
			
			if (massEmailForm.getType().equalsIgnoreCase("order")) {
				
				if (massEmailForm.isUniquePerOrder()) {
					Order order;
					OrderStatus orderStatus;
					for ( Integer orderId : massEmailForm.getOrderIds()) {
						if ( (emailSent % 40 ) == 0 ) {
							// delay 1 second
							Thread.sleep(1000);
						}
						
						MimeMessage mms = mailSender2.createMimeMessage();
						MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
						order = webJaguar.getOrder(orderId, "");
						orderStatus = new OrderStatus();
						orderStatus = (OrderStatus) order.getStatusHistory().get(order.getStatusHistory().size()-1);
						Customer customer = webJaguar.getCustomerById(order.getUserId());
						massEmailForm.setTo(customer.getUsername());
						massEmailForm.setCc(( customer.getExtraEmail() == null ) ? "" : customer.getExtraEmail());
						try {
							constructMessage(helper, massEmailForm, customer, order, orderStatus, null, siteConfig, gSiteConfig);
							mailSender2.send(mms);
							emailSent++;
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				} else {
					for ( Integer userId : massEmailForm.getUserIds()) {
						if ( (emailSent % 40 ) == 0 ) {
							// delay 1 second
							Thread.sleep(1000);
						}
						
						MimeMessage mms = mailSender2.createMimeMessage();
						MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
						Customer customer = webJaguar.getCustomerById( userId );
						massEmailForm.setTo(customer.getUsername());
						massEmailForm.setCc(( customer.getExtraEmail() == null ) ? "" : customer.getExtraEmail());
						try {
							constructMessage(helper, massEmailForm, customer, null, null, null, siteConfig, gSiteConfig);
							mailSender2.send(mms);
							emailSent++;
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				}
			} else if (massEmailForm.getType().equalsIgnoreCase("customer")){
				for ( Integer userId : massEmailForm.getUserIds()) {
					ArrayList<Product> products = null;
					if ( (emailSent % 40 ) == 0 ) {
						// delay 1 second
						Thread.sleep(1000);
					}
					
					MimeMessage mms = mailSender2.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
					Customer customer = webJaguar.getCustomerById( userId );
					if (massEmailForm.getCartNotEmpty() != null && massEmailForm.getCartNotEmpty()) {
						products = (ArrayList<Product>) webJaguar.getCustomerShoppingCartProductInfo(userId);
					}
					massEmailForm.setTo(customer.getUsername());
					massEmailForm.setCc(( customer.getExtraEmail() == null ) ? "" : customer.getExtraEmail());
					try {
						constructMessage(helper, massEmailForm, customer, null, null, products, siteConfig, gSiteConfig);
						if(find){
							this.webJaguar.insertEmailHistory(userId,null,massEmailForm.getMessageId(),massEmailForm.getScheduleTime());
						}
						mailSender2.send(mms);
						emailSent++;
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}else if (massEmailForm.getType().equalsIgnoreCase("customerTextMessage")){
				for ( Integer userId : massEmailForm.getUserIds()) {
					ArrayList<Product> products = null;
					if ( (emailSent % 40 ) == 0 ) {
						// delay 1 second
						Thread.sleep(1000);
					}
					
					MimeMessage mms = mailSender2.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
					Customer customer = webJaguar.getCustomerById( userId );
					if (massEmailForm.getCartNotEmpty() != null && massEmailForm.getCartNotEmpty()) {
						products = (ArrayList<Product>) webJaguar.getCustomerShoppingCartProductInfo(userId);
					}
					
					String cellPhone = this.webJaguar.getNumber(customer.getAddress().getCellPhone());
					Integer textMessageServerId = customer.getAddress().getMobileCarrierId();
					String textMessageServer = null;
					if( textMessageServerId != null ) textMessageServer = this.webJaguar.getTextMessageServerById(textMessageServerId);
					if(cellPhone != null && !cellPhone.equals("") && textMessageServer != null && !textMessageServer.equals("")){
						massEmailForm.setTo(cellPhone+textMessageServer);
						massEmailForm.setCc(( customer.getExtraEmail() == null ) ? "" : customer.getExtraEmail());
						try {
							constructMessage(helper, massEmailForm, customer, null, null, products, siteConfig, gSiteConfig);
							mailSender2.send(mms);
							emailSent++;
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
					
				}
			}
			
			if ((Boolean) gSiteConfig.get("gCRM")) {
				for ( Integer crmContactId : massEmailForm.getCrmContactIds()) {
					if ( (emailSent % 40 ) == 0 ) {
						// delay 1 second
						Thread.sleep(1000);
					}
					
					CrmContact crmContact = webJaguar.getCrmContactById(crmContactId);
					MimeMessage mms = mailSender2.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
					massEmailForm.setTo(crmContact.getEmail1().trim());
					if (crmContact.getEmail2() != null) {
						massEmailForm.setCc(crmContact.getEmail2());
					}
					try {
						
						constructMessage(helper, massEmailForm, crmContact, siteConfig, gSiteConfig);
						
						Properties props = System.getProperties();
						//props.put("mail.smtp.auth", "true");
						//props.put("mail.smtp.from", webJaguar.getSiteId() + "_" + massEmailForm.getCampaignId() + "_" + crmContact.getId() + "_con_@webjaguarmail.com");
						//mailSender2.setJavaMailProperties(props);
						if(find){
							this.webJaguar.insertEmailHistory(null,crmContact.getId(),massEmailForm.getMessageId(),massEmailForm.getScheduleTime());
						}
						mailSender2.send(mms);
						emailSent++;
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (SchedulerException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Calendar emailCampaignEnd = Calendar.getInstance();
		long campaignElapse = (emailCampaignEnd.getTimeInMillis() - emailCampaignStart.getTimeInMillis()) / 1000;
		// save this campaign in database.
		EmailCampaign campaign = new EmailCampaign(massEmailForm.getCampaignId(), "com", emailSent);
		webJaguar.finalizeEmailCampaign( campaign );
		webJaguar.deductEmailCampaignPoint( emailSent );
		
		// notify admin
		notifyAdmin(massEmailForm, emailSent, campaignElapse);
	}
		
	// customer
	private void constructMessage(MimeMessageHelper helper, MassEmailForm form, Customer customer, Order order, OrderStatus orderStatus, ArrayList<Product> products,  Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) throws Exception {
		String secureUrl = siteConfig.get("SECURE_URL").getValue();
		
		String host = null;
		if(customer != null && customer.getHost() != null) {
			host = customer.getHost();
		} else if(order != null && order.getHost() != null) {
			host = order.getHost();
		}
		// multi store
		if(host != null) {
			MultiStore multiStore = null;
			List<MultiStore> multiStores = this.webJaguar.getMultiStore(host);
			if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
				multiStore = multiStores.get(0);
			}
			secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
		}
		
		String subject = form.getSubject();
		String message ;
		if(form.getType().equalsIgnoreCase("customerTextMessage")){
			message = form.getMessage();
		}else{
			message = contentBuilder.replaceClick(form.getMessage(), form.getCampaignId(), customer, null);
		}
			
		
		//String message = form.getMessage();
		SalesRep salesRep = null;
		// use custome sales rep id
		if ( (Boolean) gSiteConfig.get( "gSALES_REP" ) && customer.getSalesRepId() != null ) {
			salesRep = webJaguar.getSalesRepById( customer.getSalesRepId() );
		}
		
		// to emails
		String[] emails = form.getTo().split( "[,]" ); // split by commas
		for ( int x = 0; x < emails.length; x++ ) {
			if (emails[x].trim().length() > 0) {
				helper.addTo(emails[x].trim());
			}
		}
		// cc emails
		emails = form.getCc().split( "[,]" ); // split by commas
		for ( int x = 0; x < emails.length; x++ ) {
			if (emails[x].trim().length() > 0) {
				helper.addCc(emails[x].trim());
			}
		}
		String from = form.getFrom().replaceAll( "(?i)#salesrepemail#", ( salesRep == null||salesRep.getEmail()==null ) ? siteConfig.get( "CONTACT_EMAIL" ).getValue() : salesRep.getEmail() );
		helper.setFrom(from);	

    	SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

    	StringBuffer messageBuffer = new StringBuffer();

       	// subject
    	subject = subject.replaceAll( "(?i)#firstname#", ( customer.getAddress().getFirstName() == null ) ? "" : customer.getAddress().getFirstName() );
    	subject = subject.replaceAll( "(?i)#lastname#", ( customer.getAddress().getLastName() == null ) ? "" : customer.getAddress().getLastName() );
    	subject = subject.replaceAll( "(?i)#email#", ( customer.getUsername() == null ) ? "" : customer.getUsername() );
    	if (order != null) {
    		subject = subject.replaceAll( "(?i)#order#", order.getOrderId().toString());
    		subject = subject.replaceAll( "(?i)#status#", messageSourceAccessor.getMessage("orderStatus_" + orderStatus.getStatus()));
    		subject = subject.replaceAll( "(?i)#tracknum#", (orderStatus.getTrackNum() == null) ? "" : orderStatus.getTrackNum());
    		// XXX would be replaced by OrderID-UserID Base64-encoded.
    		String orig = order.getOrderId() + "-" + order.getUserId();
    		byte[] encode_orig = Base64.encodeBase64(orig.getBytes());         
    		orig = new String(encode_orig);
    		if(order.getShipping().getStateProvince().equals("CA")) {
    			//If user is in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&walkin=true
    			//If user is not in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&other=true
    			//subject = subject.replaceAll( "(?i)#orderlink#", secureUrl + "dv/inv/invoice.php?order_id="+orig+"=&walkin=true");
        		String viewInvoiceHrefInSubject = secureUrl + "dv/inv/invoice.php?order_id="+orig+"=&walkin=true";
        		StringBuilder sb_aTagInSubject = new StringBuilder();
        		sb_aTagInSubject.append("<a href=").append("\"").append(viewInvoiceHrefInSubject).append("\"").append(">").append("View Invoice").append("</a>");
        		subject = subject.replaceAll("(?i)#orderlink#", sb_aTagInSubject.toString());
        		
        		StringBuilder sb_aTagInSubject_orderlinkes = new StringBuilder();
        		sb_aTagInSubject_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHrefInSubject).append("\"").append(">").append("Ver Factura").append("</a>");
        		subject = subject.replace("#orderlinkes#", sb_aTagInSubject_orderlinkes.toString());
    		} else {
    			//subject = subject.replaceAll( "(?i)#orderlink#", secureUrl + "dv/inv/invoice.php?order_id="+orig+"=&other=true");
        		String viewInvoiceHrefInSubject = secureUrl + "dv/inv/invoice.php?order_id="+orig+"=&other=true";
        		StringBuilder sb_aTagInSubject = new StringBuilder();
        		sb_aTagInSubject.append("<a href=").append("\"").append(viewInvoiceHrefInSubject).append("\"").append(">").append("View Invoice").append("</a>");
        		subject = subject.replaceAll("(?i)#orderlink#", sb_aTagInSubject.toString());
        		
        		StringBuilder sb_aTagInSubject_orderlinkes = new StringBuilder();
        		sb_aTagInSubject_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHrefInSubject).append("\"").append(">").append("Ver Factura").append("</a>");
        		subject = subject.replace("#orderlinkes#", sb_aTagInSubject_orderlinkes.toString());
    		}
    	}

    	if ( salesRep != null ) {
    		subject = subject.replaceAll( "(?i)#salesRepName#", salesRep.getName() );
    		subject = subject.replaceAll( "(?i)#salesRepEmail#", salesRep.getEmail() );
    		subject = subject.replaceAll( "(?i)#salesRepPhone#", ( salesRep.getPhone() == null ) ? "" : salesRep.getPhone() );
    		subject = subject.replaceAll( "(?i)#salesRepCellPhone#", ( salesRep.getCellPhone() == null ) ? "" : salesRep.getCellPhone() );
    	}
    	
    	helper.setSubject(subject);
    	
    	Random rand = new Random();
    	int  n1 = rand.nextInt(500) + 1;
    	int  n2 = rand.nextInt(500) + 1;
    	
        String originalString = customer.getId() + "-" + n1 + "-" + n2;   
	    byte[] encoded = Base64.encodeBase64(originalString.getBytes());         
	    String encodedString = new String(encoded);
       
    	message = message.replaceAll("#DOC_BSUSRC#", "https://www.viatrading.com/dv/sign/form1.php?id=" + encodedString);
    	message = message.replaceAll("#DOC_BSUSRC_SPA#", "https://www.viatrading.com/dv/sign/form1es.php?id=" + encodedString);
    	message = message.replaceAll("#DOC_CRC#", "https://www.viatrading.com/dv/sign/form2.php?id=" + encodedString);
    	message = message.replaceAll("#DOC_CRC_SPA#", "https://www.viatrading.com/dv/sign/form2es.php?id=" + encodedString);
    	message = message.replaceAll("#DOC_MPA#", "https://www.viatrading.com/dv/sign/form3.php?id=" + encodedString);
    	message = message.replaceAll("#DOC_MAP_SPA#", "https://www.viatrading.com/dv/sign/form3es.php?id=" + encodedString);    	
    	
    	message = message.replaceAll( "(?i)#date#", dateFormatter.format(new Date()));
    	message = message.replaceAll( "(?i)#firstname#", ( customer.getAddress().getFirstName() == null ) ? "" : customer.getAddress().getFirstName() );
    	message = message.replaceAll( "(?i)#lastname#", ( customer.getAddress().getLastName() == null ) ? "" : customer.getAddress().getLastName() );
    	message = message.replaceAll( "(?i)#email#", ( customer.getUsername() == null ) ? "" : customer.getUsername() );
    	message = message.replaceAll( "(?i)#address1#", ( customer.getAddress().getAddr1() == null ) ? "" :  customer.getAddress().getAddr1());
    	message = message.replaceAll( "(?i)#address2#", ( customer.getAddress().getAddr2() == null ) ? "" :  customer.getAddress().getAddr2());
    	message = message.replaceAll( "(?i)#country#", ( customer.getAddress().getCountry() == null ) ? "" :  customer.getAddress().getCountry());
		message = message.replaceAll( "(?i)#city#", ( customer.getAddress().getCity() == null ) ? "" :  customer.getAddress().getCity());
		message = message.replaceAll( "(?i)#state#", ( customer.getAddress().getStateProvince() == null ) ? "" :  customer.getAddress().getStateProvince());
		message = message.replaceAll( "(?i)#phone#", ( customer.getAddress().getPhone() == null ) ? "" :  customer.getAddress().getPhone());
		message = message.replaceAll( "(?i)#cellphone#", ( customer.getAddress().getCellPhone() == null ) ? "" :  customer.getAddress().getCellPhone());
		message = message.replaceAll( "(?i)#company#", ( customer.getAddress().getCompany() == null ) ? "" :  customer.getAddress().getCompany());
		message = message.replaceAll( "(?i)#deliverytype#", ( customer.getAddress().isResidential() == true ) ? "Yes" :  "No");
		message = message.replaceAll( "(?i)#liftgate#", ( customer.getAddress().isLiftGate() == true ) ? "Yes" :  "No");
		if (order != null) {
			message = message.replaceAll( "(?i)#order#", order.getOrderId().toString());
			message = message.replaceAll( "(?i)#status#", messageSourceAccessor.getMessage("orderStatus_" + orderStatus.getStatus()));
			message = message.replaceAll( "(?i)#tracknum#", (orderStatus.getTrackNum() == null) ? "" : orderStatus.getTrackNum());
			// XXX would be replaced by OrderID-UserID Base64-encoded.
			String orig = order.getOrderId() + "-" + order.getUserId();
			byte[] encode_orig = Base64.encodeBase64(orig.getBytes());         
			orig = new String(encode_orig);
			if(order.getShipping().getStateProvince().equals("CA")) {
				//If user is in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&walkin=true
				//If user is not in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&other=true
				//message = message.replaceAll( "(?i)#orderlink#", secureUrl + "dv/inv/invoice.php?order_id="+orig+"=&walkin=true");
				String viewInvoiceHrefInMessage = secureUrl + "dv/inv/invoice.php?order_id="+orig+"=&walkin=true";
				StringBuilder sb_aTagInMessage = new StringBuilder();
				sb_aTagInMessage.append("<a href=").append("\"").append(viewInvoiceHrefInMessage).append("\"").append(">").append("View Invoice").append("</a>");
				message = message.replaceAll("(?i)#orderlink#", sb_aTagInMessage.toString());
				
				StringBuilder sb_aTagInMessage_orderlinkes = new StringBuilder();
				sb_aTagInMessage_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHrefInMessage).append("\"").append(">").append("Ver Factura").append("</a>");
				message = message.replace("#orderlinkes#", sb_aTagInMessage_orderlinkes.toString());				
			} else {
				//message = message.replaceAll( "(?i)#orderlink#", secureUrl + "dv/inv/invoice.php?order_id="+orig+"=&other=true");
				String viewInvoiceHrefInMessage = secureUrl + "dv/inv/invoice.php?order_id="+orig+"=&other=true";
				StringBuilder sb_aTagInMessage = new StringBuilder();
				sb_aTagInMessage.append("<a href=").append("\"").append(viewInvoiceHrefInMessage).append("\"").append(">").append("View Invoice").append("</a>");
				message = message.replaceAll("(?i)#orderlink#", sb_aTagInMessage.toString());
				
				StringBuilder sb_aTagInMessage_orderlinkes = new StringBuilder();
				sb_aTagInMessage_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHrefInMessage).append("\"").append(">").append("Ver Factura").append("</a>");
				message = message.replace("#orderlinkes#", sb_aTagInMessage_orderlinkes.toString());
			}
			message = message.replaceAll( "(?i)#orderduedate#", ( order.getDueDate() == null ) ? "" :  dateFormatter.format(order.getDueDate()).toString());
			message = message.replaceAll( "(?i)#orderuserduedate#", ( order.getUserDueDate() == null ) ? "" : dateFormatter.format(order.getUserDueDate()).toString());
			message = message.replaceAll( "(?i)#ordercontent#", getOrderContect(order));
		}
		if (products != null) {
			message = message.replaceAll( "(?i)#cartcontent#", getShoppingCartContect(products, form.isHtml()));
			if (message.contains("#wjstart#") && message.contains("#wjend#")) {
				StringBuffer sb = new StringBuffer();
				sb.append(message);
				sb.insert(message.indexOf("#wjstart#"), getContent(message, "#wjstart#" , "#wjend#", products));
				if (message.contains("#wjend#")) {
					sb.delete(sb.indexOf("#wjstart#"), sb.indexOf("#wjend#")+"#wjend#".length());
				}
				message = sb.toString();
			}
		}

    	if ( salesRep != null ) {
    		message = message.replaceAll( "(?i)#salesRepName#", salesRep.getName() );
    		message = message.replaceAll( "(?i)#salesRepEmail#", salesRep.getEmail() );
    		message = message.replaceAll( "(?i)#salesRepPhone#", ( salesRep.getPhone() == null ) ? "" : salesRep.getPhone() );
    		message = message.replaceAll( "(?i)#salesRepCellPhone#", ( salesRep.getCellPhone() == null ) ? "" : salesRep.getCellPhone() );
    	}
		

		if(siteConfig.get("DYNAMIC_PROMO").getValue().equals("true") && (message!="") && (message!=null)){
		// REPLACING DYNAMIC ELEMENTS FOR PROMOS
				Promo promo = new Promo();
				promo.setPercent(false);
				String promoCode = null;
				
		    	//random promocode generator 
		    	char[] chars = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();
		    	StringBuilder sb3= new StringBuilder();
		    	Random random = new Random();
		    	for (int i = 0; i < 10; i++) {
		    	    char c = chars[random.nextInt(chars.length)];
		    	    sb3.append(c);
		    	}
		    	String output = sb3.toString();
		    	promoCode = output;
		    	message = message.replaceAll("#dynamicPromo#",promoCode);
		    	//percentage dynamic tag
		    	StringBuffer sb2 = new StringBuffer(message);
		    	Pattern pattern = Pattern.compile("#percentage.*");
			    Matcher matcher = pattern.matcher(sb2);
			    String tempPercentageNum = null;
			    int percentageNum=0;				
			    
			    while (matcher.find())
			    {					
			    	
			    	promo.setPercent(true);
			        String temp = matcher.group().toString();
			        tempPercentageNum = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
			         percentageNum = Integer.parseInt(tempPercentageNum);
			        message=message.replaceAll("#percentage,"+percentageNum+"#","");
			    }    
			    //fixed dynamic tag
			    Pattern pattern2 = Pattern.compile("#fixed.*");
			    Matcher matcher2 = pattern2.matcher(sb2);
			    int fixedNum=0;
			    while(matcher2.find()){
					 
			    	 String temp = matcher2.group().toString();
			    	 String fixedAmount = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
			    	 fixedNum = Integer.parseInt(fixedAmount);
			    	 message=message.replaceAll("#fixed,"+fixedNum+"#","");
			    }
			    // Replacing amount with percentage or fixed
			    Pattern pattern3 = Pattern.compile("#amount.*");
			    Matcher matcher3 = pattern3.matcher(sb2);
			    String helperText = "";
			    if(promo.isPercent()){
			    	helperText="%";
			    }
			    else{
			    	helperText="\\$";
			    }
			    int amountNum=0;
			    while(matcher3.find()){
					 
			    	 String temp = matcher3.group().toString();
			    	 String amount = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
			    	 amountNum = Integer.parseInt(amount);
			    	message=message.replaceAll("#amount,"+amountNum+"#",""+amountNum+""+helperText+""); 
			    }
			    // replacing delta tag
			    Pattern pattern4 = Pattern.compile("#delta.*");
			    Matcher matcher4 = pattern4.matcher(sb2);
			    int daysNum=0;
			    Date date = null;
			    Date endDate = null;
			    while(matcher4.find()){
					 
			    	 String temp = matcher4.group().toString();
			    	 //System.out.println("temp is---" +temp);
			    	 String days = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
			    	 daysNum = Integer.parseInt(days);
			    	// System.out.println(daysNum+"---number of dats"); 
			    // setting start and end dates to the promo
			    	 Calendar cal = Calendar.getInstance();
			    	  date = new Date();
			    	 cal.setTime(date);
			    	 cal.add(Calendar.DATE,daysNum);
			    	  endDate = cal.getTime();
			    	message=message.replaceAll("#delta,"+daysNum+"#",""+endDate+"");
			    }
			  //replacing hours tag
				
			    Pattern pattern5 = Pattern.compile("hours.*");
			    Matcher matcher5 = pattern5.matcher(sb2);
			    while(matcher5.find()){
					
			    	String temp = matcher5.group().toString();
			    	String hours = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
			    	Calendar cal = Calendar.getInstance();
			    	 date = new Date();
			    	 cal.setTime(date);
			    	 cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(hours));
			    	 endDate = cal.getTime();
			    	 message=message.replaceAll("#hours,"+hours+"#",""+endDate+""); 
			    }
			 // Writing into promo table in the database
			    if(amountNum != 0) {
						promo.setStartDate(date);
					    	promo.setEndDate(endDate);
					    	promo.setTitle(promoCode);
					    	promo.setDiscount((double)amountNum);
					    	promo.setOnetimeUseOnly(false);
					    	promo.setOnetimeUsePerCustomer(false);
						promo.setMinOrder(0.00);
					    	this.webJaguar.insertPromo(promo);
			    }

		}
		
		// customer fields
		Class<Customer> c1 = Customer.class;
		Method m1 = null;
		Object arglist1[] = null;
		for ( CustomerField customerField : webJaguar.getCustomerFields() ) {
			if ( customerField.isEnabled() ) {
				m1 = c1.getMethod( "getField" + customerField.getId() );
				customerField.setValue( (String) m1.invoke( customer, arglist1 ) );
				message = message.replaceAll( "(?i)#customerfield"+ customerField.getId() +"#", ( customerField.getValue() == null ) ? "" :  customerField.getValue());
			}
		}

    	messageBuffer.append( message );
    	if(!form.getType().equalsIgnoreCase("customerTextMessage")){
    		String finalValue = null;
        	try {
        		String input = customer.getId()+",-|"+form.getCampaignId();
        		finalValue = Utilities.AESencrypt(KEY, input);
    		} catch (Exception e) {
    			//System.out.println("Exception: " +e);
    		}
        	if(finalValue != null) {
        		messageBuffer.append("<span style=\"position:absolute;bottom:0;\"><img src=\""+siteConfig.get( "SITE_URL" ).getValue()+"image.jhtm?value="+finalValue+"\" /></span>");
        	} 
    	
    	}
    	
    	if ( form.isHtml() ) { 
    		String completeMessage = messageBuffer.toString().concat( setEmailClosing(siteConfig, gSiteConfig, customer, null) );
    		helper.setText( completeMessage, true );
    	} else {
    		helper.setText(messageBuffer.toString());
    	}
    	
    	
	}
	
	private void constructMessage(MimeMessageHelper helper, MassEmailForm form, CrmContact crmContact, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) throws Exception {
		try {
			String subject = form.getSubject();
			
			String message = contentBuilder.replaceClick(form.getMessage(), form.getCampaignId(), null, crmContact);
			

			//String message = form.getMessage();
			SalesRep salesRep = null;
			
			// to email1
			helper.addTo(crmContact.getEmail1());
			if (crmContact.getEmail2() != null && !crmContact.getEmail2().isEmpty()) {
				helper.addCc(crmContact.getEmail2());
			}
			
			Customer customer = null;
			// use custome sales rep id
			if ( (Boolean) gSiteConfig.get( "gSALES_REP" ) && crmContact.getId() != null) {
				customer = this.webJaguar.getCustomerById(this.webJaguar.getUserIdByCrmId(crmContact.getId()));
				if(customer!=null && customer.getSalesRepId()!=null){
					salesRep = webJaguar.getSalesRepById( customer.getSalesRepId());
				}
			}

			if(salesRep!=null && salesRep.getEmail()!=null && salesRep.getEmail()!=""){
				helper.setFrom(form.getFrom().replace( "#salesRepEmail#", salesRep.getEmail()));	
			}else{
				helper.setFrom(form.getFrom());	
			}
					
	    	SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");

	    	StringBuffer messageBuffer = new StringBuffer();
	       	// subject
	    	subject = subject.replace( "#firstname#", ( crmContact.getFirstName() == null ) ? "" : crmContact.getFirstName() );
	    	subject = subject.replace( "#lastname#", ( crmContact.getLastName() == null ) ? "" : crmContact.getLastName() );
	    	subject = subject.replace( "#email#", ( crmContact.getEmail1() == null ) ? "" : crmContact.getEmail1() );

	    	helper.setSubject(subject);
	    	

	    	if(customer!=null){
				
	    	}else{
				
	    	}
	    	
	    	if(customer!=null && customer.getId()!=null){
				
	    	}else{
				
	    	}
	    	
			
	    	if(customer!=null && customer.getId()!=null){
	    		
	        	Random rand = new Random();
	        	int  n1 = rand.nextInt(500) + 1;
	        	int  n2 = rand.nextInt(500) + 1;
	        	
	            String originalString = customer.getId() + "-" + n1 + "-" + n2;   
	    	    byte[] encoded = Base64.encodeBase64(originalString.getBytes());         
	    	    String encodedString = new String(encoded);  
	    		
				
	    		message = message.replaceAll("#DOC_BSUSRC#", "https://www.viatrading.com/dv/sign/form1.php?id=" + encodedString);
	    		message = message.replaceAll("#DOC_BSUSRC_SPA#", "https://www.viatrading.com/dv/sign/form1es.php?id=" + encodedString);
	    		message = message.replaceAll("#DOC_CRC#", "https://www.viatrading.com/dv/sign/form2.php?id=" + encodedString);
	    		message = message.replaceAll("#DOC_CRC_SPA#", "https://www.viatrading.com/dv/sign/form2es.php?id=" + encodedString);
	    		message = message.replaceAll("#DOC_MPA#", "https://www.viatrading.com/dv/sign/form3.php?id=" + encodedString);
	    		message = message.replaceAll("#DOC_MAP_SPA#", "https://www.viatrading.com/dv/sign/form3es.php?id=" + encodedString);
				
	    	}    	
	    	
	    	
	    	if(crmContact!=null && crmContact.getId()!=null){
	    		try{
			    	
			    	message = message.replaceAll("#crmcontactid#", crmContact.getId().toString());
	    		}catch(Exception ex){}
	    	}
	    	
	    	message = message.replace( "#date#", dateFormatter.format(new Date()));
	    	message = message.replace( "#firstname#", ( crmContact.getFirstName() == null ) ? "" : crmContact.getFirstName() );
	    	message = message.replace( "#lastname#", ( crmContact.getLastName() == null ) ? "" : crmContact.getLastName() );
	    	message = message.replace( "#email#", ( crmContact.getEmail1() == null ) ? "" : crmContact.getEmail1() );
	    	if ( salesRep != null ) {
	    		message = message.replace( "#salesRepName#", salesRep.getName() );
	    		message = message.replace( "#salesRepEmail#", salesRep.getEmail() );
	    		message = message.replace( "#salesRepPhone#", ( salesRep.getPhone() == null ) ? "" : salesRep.getPhone() );
	    		message = message.replace( "#salesRepCellPhone#", ( salesRep.getCellPhone() == null ) ? "" : salesRep.getCellPhone());
	    		
	    	}
	    	
			if(siteConfig.get("DYNAMIC_PROMO").getValue().equals("true") && (message!="") && (message!=null)){
				// REPLACING DYNAMIC ELEMENTS FOR PROMOS
					Promo promo = new Promo();
					promo.setPercent(false);
					String promoCode = null;
			 
					
			    	//random promocode generator 
			    	char[] chars = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();
			    	StringBuilder sb3= new StringBuilder();
			    	Random random = new Random();
			    	for (int i = 0; i < 10; i++) {
			    	    char c = chars[random.nextInt(chars.length)];
			    	    sb3.append(c);
			    	}
			    	String output = sb3.toString();
			    	promoCode = output;
			    	message = message.replaceAll("#dynamicPromo#",promoCode);
			    	//percentage dynamic tag
			    	StringBuffer sb2 = new StringBuffer(message);
			    	Pattern pattern = Pattern.compile("#percentage.*");
				    Matcher matcher = pattern.matcher(sb2);
				    String tempPercentageNum = null;
				    int percentageNum=0;
					
				    while (matcher.find())
				    {	
						
				    	promo.setPercent(true);
				        String temp = matcher.group().toString();
				        tempPercentageNum = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
				         percentageNum = Integer.parseInt(tempPercentageNum);
				        message=message.replaceAll("#percentage,"+percentageNum+"#","");
				    }    
				    //fixed dynamic tag
				    Pattern pattern2 = Pattern.compile("#fixed.*");
				    Matcher matcher2 = pattern2.matcher(sb2);
				    int fixedNum=0;
				    while(matcher2.find()){
						 
				    	 String temp = matcher2.group().toString();
				    	 String fixedAmount = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
				    	 fixedNum = Integer.parseInt(fixedAmount);
				    	 message=message.replaceAll("#fixed,"+fixedNum+"#","");
				    }
				    // Replacing amount with percentage or fixed
				    Pattern pattern3 = Pattern.compile("#amount.*");
				    Matcher matcher3 = pattern3.matcher(sb2);
				    String helperText = "";
				    if(promo.isPercent()){
				    	helperText="%";
				    }
				    else{
				    	helperText="\\$";
				    }
				    int amountNum=0;
				    while(matcher3.find()){
				    	 String temp = matcher3.group().toString();
				    	 String amount = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
				    	 amountNum = Integer.parseInt(amount);
				    	message=message.replaceAll("#amount,"+amountNum+"#",""+amountNum+""+helperText+""); 
				    }
					
				    // replacing delta tag
				    Pattern pattern4 = Pattern.compile("#delta.*");
				    Matcher matcher4 = pattern4.matcher(sb2);
				    int daysNum=0;
				    Date date = null;
				    Date endDate = null;
				    while(matcher4.find()){
						 
				    	 String temp = matcher4.group().toString();
				    	 
				    	 String days = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
				    	 daysNum = Integer.parseInt(days);
				    	 
				    // setting start and end dates to the promo
				    	 Calendar cal = Calendar.getInstance();
				    	  date = new Date();
				    	 cal.setTime(date);
				    	 cal.add(Calendar.DATE,daysNum);
				    	  endDate = cal.getTime();
				    	message=message.replaceAll("#delta,"+daysNum+"#",""+endDate+"");
				    }
				  //replacing hours tag
					
				    Pattern pattern5 = Pattern.compile("hours.*");
				    Matcher matcher5 = pattern5.matcher(sb2);
				    while(matcher5.find()){
						
				    	String temp = matcher5.group().toString();
				    	String hours = temp.substring(temp.indexOf(",")+1,temp.indexOf("#", temp.indexOf(",")));
				    	Calendar cal = Calendar.getInstance();
				    	 date = new Date();
				    	 cal.setTime(date);
				    	 cal.add(Calendar.HOUR_OF_DAY, Integer.parseInt(hours));
				    	 endDate = cal.getTime();
				    	 message=message.replaceAll("#hours,"+hours+"#",""+endDate+""); 
				    }
				 // Writing into promo table in the database
				    if(amountNum != 0) {
				    		promo.setStartDate(date);
					    	promo.setEndDate(endDate);
					    	promo.setTitle(promoCode);
					    	promo.setDiscount((double)amountNum);
					    	promo.setOnetimeUseOnly(false);
					    	promo.setOnetimeUsePerCustomer(false);
						promo.setMinOrder(0.00);
					    	this.webJaguar.insertPromo(promo);	
				    }
	
			}

			messageBuffer.append(message);
			

	    	String finalValue = null;
	    	try {
	    		String input="-,"+crmContact.getId()+"|"+form.getCampaignId();		
	    		finalValue = Utilities.AESencrypt(KEY, input);
			} catch (Exception e) {
				
			}
	    	if(finalValue != null) {
	    		messageBuffer.append("<img src=\""+siteConfig.get( "SITE_URL" ).getValue()+"image.jhtm?value="+finalValue+"\" />");
	    	}
	    	
	    	if ( form.isHtml() ) { 
	    		String completeMessage = messageBuffer.toString().concat(setEmailClosing(siteConfig, gSiteConfig, null, crmContact));
	    		helper.setText( completeMessage, true );
	    	} else {
	    		helper.setText(messageBuffer.toString());
	    	}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	// add email ending
	public String setEmailClosing(Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig, Customer customer, CrmContact crmContact){
		StringBuffer closing = new StringBuffer();
		closing.append("<p></p><HR color=#e0e0e0>" +
		                 "<div style=\"font: 10px Arial, Helvetica, sans-serif; color: #666\">" +
		                 "This message was sent from " + siteConfig.get("COMPANY_NAME").getValue() + ", " + siteConfig.get("COMPANY_ADDRESS1").getValue() + ", " + siteConfig.get("COMPANY_CITY").getValue() + ", " + siteConfig.get("COMPANY_STATE_PROVINCE").getValue() + " " + siteConfig.get("COMPANY_ZIPCODE").getValue() + ", " + siteConfig.get("COMPANY_COUNTRY").getValue() + ". To unsubscribe from our list, ");
		                 if (customer!=null) {
		                	 closing.append("<a href=\"" + siteConfig.get("SITE_URL").getValue() + "unsubscribe.jhtm?email="+customer.getUsername()+ "&campaignId=" + massEmailForm.getCampaignId()  +"&token="+customer.getToken()+"\" style=\"text-decoration: none\"><font color=\"#FF0000\">click here</font></a>.");
		                 } else {
		                	 closing.append("<a href=\"" + siteConfig.get("SITE_URL").getValue() + "unsubscribe.jhtm?email="+crmContact.getEmail1()+ "&campaignId=" + massEmailForm.getCampaignId()  +"&crmToken="+crmContact.getToken()+"\" style=\"text-decoration: none\"><font color=\"#FF0000\">click here</font></a>.");
		                 }
		                 closing.append("</div>" );
//		                 "<hr color=#e0e0e0>" +
//		                 "<div align=\"center\" style=\"font: 10px Arial, Helvetica, sans-serif; color: #666\">" +
//		                 siteConfig.get("SITE_NAME").getValue() +
//		                 "<br>Powered by <a style=\"text-decoration: none\" href=\"http://www.advancedemedia.com\"><font color=\"#0000FF\">Advanced E-Media, Inc</font></a>. " + Calendar.getInstance().get( Calendar.YEAR ) + "</div>");
		                 return closing.toString();
	}
	
	private void notifyAdmin(MassEmailForm form, int numEmailSent, long seconds) {
		// send email notification
    	String adminEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
    	String siteDomain = gSiteConfig.get("gSITE_DOMAIN").toString();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	try {
			MimeMessage mms = mailSender2.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			
			helper.setTo(adminEmail);
			helper.setFrom(adminEmail);
			helper.setBcc("developers@advancedemedia.com");
			helper.setSubject("Mass Email Report on " + siteDomain);
			
			StringBuffer message = new StringBuffer();
	    	
			if ( form.isHtml() ) { 
				message.append( "<b>Date: " + dateFormatter.format(new Date()) + "</b><br />" );
		    	message.append( "<br /><b>Number Email Sent: " + numEmailSent + "</b><br />");
		    	message.append( "<br /><b>" + String.format("%1$02d:%2$02d:%3$02d", seconds / (60*60), (seconds / 60) % 60, seconds % 60) + "</b><br />");
		    	message.append( "<b>--------------------------------------" + "</b><br />");
		    	message.append( "<b>Subject: " + form.getSubject() + "</b><br />");
		    	message.append( "<b>Message: </b><br />" + form.getMessage());
				helper.setText( message.toString(), true );
	    	} else {
	    		message.append( "Date:\t" + dateFormatter.format(new Date()) + "\n" );
		    	message.append( "Number Email Sent:\t " + numEmailSent + "\n" );
		    	message.append( "" + String.format("%1$02d:%2$02d:%3$02d", seconds / (60*60), (seconds / 60) % 60, seconds % 60) );
		    	message.append( "\n--------------------------------------\n" );
		    	message.append( "\nSubject:\t" + form.getSubject() );
		    	message.append( "\nMessage:\t" + form.getMessage() );
	    		helper.setText(message.toString());
	    	}
			
			mailSender2.send(mms);
		}  catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getOrderContect(Order order) {
		StringBuffer sb = new StringBuffer();
		for (LineItem lineItem:order.getLineItems()) {
			sb.append(lineItem.getProduct().getSku() + " ");
		}
		return sb.toString();
	}
	
	private String getShoppingCartContect(ArrayList<Product> products, boolean html) {
		StringBuffer sb = new StringBuffer();
		if (html) {
			sb.append("<div class=\"cartContentBox\"><ul>");
			for (Product product:products) {
				sb.append("<li>" + product.getName() + " ("+product.getSku()+")" + "</li>");
			}
			sb.append("</ul></div>");
		} else {
			int index = 1;
			for (Product product:products) {
				sb.append(index+".\t" + product.getName() + " ("+product.getSku()+")" + "\n");
			}
		}
		return sb.toString();
	}
	
	private String getContent(String originalString, String startPoint, String endPoint, ArrayList<Product> products) {
		StringBuffer sb = new StringBuffer();
		int beginIndex = originalString.indexOf(startPoint) + startPoint.length();
		int endIndex = originalString.indexOf(endPoint, beginIndex);
		String tempString = null;
		for(Product product : products) {
			tempString = originalString.substring(beginIndex, endIndex);;
			tempString = tempString.replace("#productName#", product.getName());
			tempString = tempString.replace("#shortDesc#", product.getShortDesc());
			tempString = tempString.replace("#sku#", product.getSku());
			if (product.getThumbnail() != null && product.getThumbnail().getImageUrl() != null && product.getThumbnail().getImageUrl() != "") {
				if (product.getThumbnail().isAbsolute()) {
					tempString = tempString.replace("#image#", "<img src=\"" + product.getThumbnail().getImageUrl() + "\" border=\"0\"");
				} else {
					tempString = tempString.replace("#image#", "<img src=\"" + siteConfig.get("SITE_URL").getValue() + "assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\" border=\"0\"");
				}
			} else {
				tempString = tempString.replace("#image#", "");
			}
			
			sb.append(tempString);
		}
		
		return sb.toString();
	}
}