/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 06.15.2009
 */

package com.webjaguar.web.quartz;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.linuxense.javadbf.DBFReader;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Product;
import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.techdata.TechdataCategory;
import com.webjaguar.thirdparty.techdata.TechdataManufacturer;

public class TechdataJob extends WebApplicationObjectSupport {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void updateCatalog() {

		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();	
		List<CsvFeed> csvFeedList = this.webJaguar.getCsvFeed("techdata-catalog");
		
		// Ingram configuration
		String[] config = siteConfig.get("TECHDATA").getValue().split(",");
		
		if (!(config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE")) || csvFeedList.isEmpty()) {
			// return if techdata feature is disabled 
			return;
		}
		
		String[] extra = {};
		if (siteConfig.get("TECHDATA_EXTRA").getValue().trim().length() > 0) {
			extra = siteConfig.get("TECHDATA_EXTRA").getValue().split(",");
		}
		
		// supplier id
		Integer supplierId = Integer.parseInt(config[4]);
		if (!this.webJaguar.isValidSupplierId(supplierId)) {
			// not a valid supplier
			return;
		}
		
		File tempFolder = new File(getServletContext().getRealPath("temp"));	
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File techdataDir = new File(tempFolder, "techdata");
		if (!techdataDir.exists()) {
			techdataDir.mkdir();
		}
		File zip_file = new File(techdataDir, "ProdCOD.ZIP");
				
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        
	        if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
	        	ftp.enterLocalPassiveMode();
		        ftp.setFileType(FTP.BINARY_FILE_TYPE);	// for zip files
		        
	        	if (ftp.retrieveFile(config[3], new FileOutputStream(zip_file))) {
	        		
	        		final int BUFFER = 2048;
	        		BufferedOutputStream dest = null;	
	        		
	        		FileInputStream fis = new FileInputStream(zip_file);
	        		ZipInputStream zin = new ZipInputStream(new BufferedInputStream(fis));
	        		
	        		ZipEntry entry;
	        		while((entry = zin.getNextEntry()) != null) {
	        			if (entry.getName().equals("manuf.dbf") || 
	        				entry.getName().equals("comgrp.dbf") ||
	        				entry.getName().equals("comcat.dbf") ||
	        				entry.getName().equals("comsub.dbf") ||
	        				entry.getName().equals("products.dbf")) {
	        				
	        				int count;
	        				byte data[] = new byte[BUFFER];

	        				// write the files to the disk
	        				File entryFile = new File(techdataDir, entry.getName());
	        				FileOutputStream fos = new FileOutputStream(entryFile);
	        				dest = new BufferedOutputStream(fos, BUFFER);
	        				while ((count = zin.read(data, 0, BUFFER)) != -1) {
	        					dest.write(data, 0, count);
	        				}
	        				dest.flush();
	        				dest.close();
	        			}
	        		}
	        		
	        	    fis.close();
	        	    zin.close();
	        	    
	    	        // Logout from the FTP Server and disconnect
	    	        ftp.logout();
	    	        ftp.disconnect();
	    	        
	    	        // manufacturers
	    	        parseManufFile(new File(techdataDir, "manuf.dbf"));
	    	        
	    	        // categories
	    	        parseCategoryFiles(techdataDir);
	    	        
	    	        boolean hasCatId = false;
	    			Map<String, TechdataCategory> categoryMap = this.webJaguar.getTechdataCategoryMap();
		        	for (TechdataCategory category: categoryMap.values()) {
		        		if (category.getCatId() != null) {
		        			hasCatId = true;
		        			break;
		        		}
		        	}
		        	
		        	// products
	    			if (hasCatId) {
	    				parseProductFile(new File(techdataDir, "products.dbf"), this.webJaguar.getTechdataRestrictedProductLines(), categoryMap, csvFeedList, supplierId, extra);
	    			}
	        	} else { // file not on ftp
	    	        // Logout from the FTP Server and disconnect
	    	        ftp.logout();
	    	        ftp.disconnect();	        		
	        	}
	        } else { // login failed
	        	notifyAdmin("TECHDATA updateCatalog() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
	        }
		} catch (Exception e) {
			notifyAdmin("TECHDATA updateCatalog() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}
	
	private int parseManufFile(File file) throws Exception {
		// vend_code,vend_name
		
		InputStream inputStream = new FileInputStream(file);
	    
		List<TechdataManufacturer> manufacturers = new ArrayList<TechdataManufacturer>();
	    
		DBFReader reader = new DBFReader(inputStream);

	    int count = 0;
		Object[] rowObjects;
	    
		while((rowObjects = reader.nextRecord()) != null) {
			TechdataManufacturer manufacturer = new TechdataManufacturer();
			Object vendCode = rowObjects[0];
			if (vendCode.getClass() == String.class) {
				manufacturer.setVendCode(((String) vendCode).trim());				
			}
			Object vendName = rowObjects[1];
			if (vendName.getClass() == String.class) {
				manufacturer.setVendName(((String) vendName).trim());				
			}	
			manufacturers.add(manufacturer);
			
			count++;
	    }
		
	    inputStream.close();
		
		if (manufacturers.size() > 0) {
			this.webJaguar.nonTransactionSafeInsertTechdataManufacturers(manufacturers);
		}
		
		file.delete();
	    
	    return count;
	}	

	private int parseCategoryFiles(File techdataDir) throws Exception {
	    int count = 0;
	   
	    List<TechdataCategory> categories = new ArrayList<TechdataCategory>();
	    
	    // comgrp
		// grp,descr
	    File comgrpFile = new File(techdataDir, "comgrp.dbf");
	    InputStream inputStream = new FileInputStream(comgrpFile);
		DBFReader reader = new DBFReader(inputStream);
		Object[] rowObjects;
	    
		while((rowObjects = reader.nextRecord()) != null) {
			TechdataCategory category = new TechdataCategory();
			category.setGrp(((String) rowObjects[0]).trim());
			category.setCat("*");
			category.setSub("*");
			category.setDescr(((String) rowObjects[1]).trim());				
			categories.add(category);
			
			count++;
	    }
		
	    // comcat
		// grp,cat,descr
		File comcatFile = new File(techdataDir, "comcat.dbf");
	    inputStream = new FileInputStream(comcatFile);
		reader = new DBFReader(inputStream);
	    
		while((rowObjects = reader.nextRecord()) != null) {
			TechdataCategory category = new TechdataCategory();
			category.setGrp(((String) rowObjects[0]).trim());
			category.setCat(((String) rowObjects[1]).trim());
			category.setSub("*");
			category.setDescr(((String) rowObjects[2]).trim());				
			categories.add(category);
			
			count++;
	    }
		
	    // comsub
		// grp,cat,sub,descr
		File comsubFile = new File(techdataDir, "comsub.dbf");
	    inputStream = new FileInputStream(comsubFile);
		reader = new DBFReader(inputStream);
	    
		while((rowObjects = reader.nextRecord()) != null) {
			TechdataCategory category = new TechdataCategory();
			category.setGrp(((String) rowObjects[0]).trim());
			category.setCat(((String) rowObjects[1]).trim());
			category.setSub(((String) rowObjects[2]).trim());
			category.setDescr(((String) rowObjects[3]).trim());
			categories.add(category);
			
			count++;
	    }
		
	    inputStream.close();
		
		if (categories.size() > 0) {
			this.webJaguar.nonTransactionSafeInsertTechdataCategories(categories);
		}
	    
		comgrpFile.delete();
		comcatFile.delete();
		comsubFile.delete();
		
	    return count;
	}	
	
	private int parseProductFile(File file, List<String> restrictedProductLines, Map<String, TechdataCategory> categoryMap, List<CsvFeed> csvFeedList, int supplierId, String[] extra) throws Exception {
		// grp,cat,sub,vend_code,manu_part,part_num,descr,cost,retail,qty,list_price,eff_date,tech_fax,status,upc
		int grp = 0, cat = 1, sub = 2, vend_code = 3, cost = 7;
		
		Map<String, String> manufacturers = this.webJaguar.getTechdataManufacturerMap();
		
		InputStream inputStream = new FileInputStream(file);
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		List<Map <String, Object>> categoryData = new ArrayList<Map <String, Object>>();
	    
		DBFReader reader = new DBFReader(inputStream);
				
	    int total = 0;
		Object[] rowObjects;
		
		csvFeedList.add(new CsvFeed("feed"));
		csvFeedList.add(new CsvFeed("feed_new"));
		csvFeedList.add(new CsvFeed("price_1"));
		
		// extra configuration
		for (int i=0; i<extra.length; i++) {
			csvFeedList.add(new CsvFeed(extra[i].substring(0, extra[i].indexOf("="))));
		}
		
		while((rowObjects = reader.nextRecord()) != null) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			List<Integer> catIds = new ArrayList<Integer>();
			
			Double priceChange = null;
			
			if (restrictedProductLines.contains(rowObjects[vend_code])) {
				// skip this item
				continue;
			}
			// categories
			if (categoryMap.containsKey(rowObjects[grp] + "**")) {
				// grp
				TechdataCategory category = categoryMap.get(rowObjects[grp] + "**");
				if (category.getCatId() != null) catIds.add(category.getCatId());
				if (category.getPriceChange() != null) priceChange = category.getPriceChange();
			}
			if (categoryMap.containsKey(rowObjects[grp] + "" + rowObjects[cat] + "*")) {
				// cat
				TechdataCategory category = categoryMap.get(rowObjects[grp] + "" + rowObjects[cat] + "*");
				if (category.getCatId() != null) catIds.add(category.getCatId());
				if (category.getPriceChange() != null) priceChange = category.getPriceChange();
			}
			if (categoryMap.containsKey(rowObjects[grp] + "" + rowObjects[cat] + rowObjects[sub])) {
				// sub
				TechdataCategory category = categoryMap.get(rowObjects[grp] + "" + rowObjects[cat] + rowObjects[sub]);
				if (category.getCatId() != null) catIds.add(category.getCatId());
				if (category.getPriceChange() != null) priceChange = category.getPriceChange();
			}
			if (catIds.isEmpty()) {
				// skip this item
				continue;
			}
			
			for (CsvFeed csvFeed: csvFeedList) {
				if (csvFeed.getColumn() != null) {
					Object value = rowObjects[csvFeed.getColumn()];
					if (value.getClass() == String.class) {
						value = ((String) value).trim();
					}
					if (csvFeed.getColumnName().equals("sku")) {
						map.put("sku", "TD-" + value);
					} else if (csvFeed.getColumn() == vend_code) {
						map.put(csvFeed.getColumnName(), manufacturers.get(value));
					} else if (csvFeed.isZeroAsNull()) {
						try {
							Double dValue = Double.parseDouble(value.toString());
							if (dValue == 0) {
								dValue = null;
							}
							map.put(csvFeed.getColumnName(), dValue);
						} catch (NumberFormatException e) {
							map.put(csvFeed.getColumnName(), null);									
						}
					} else {
						map.put(csvFeed.getColumnName(), value);
					}					
				}
			}
			map.put("feed", "TECHDATA");
			map.put("feed_new", true);
			map.put("cost", rowObjects[cost]);
			
			// price change
			if (map.get("cost") != null && priceChange != null) {
				map.put("price_1", (Double) map.get("cost") + (Double) map.get("cost")*priceChange/100);
			}

			// extra configuration
			for (int i=0; i<extra.length; i++) {
				map.put(extra[i].substring(0, extra[i].indexOf("=")), extra[i].substring(extra[i].indexOf("=")+1));
			}
			
			map.put("productId", this.webJaguar.getProductIdBySku((String) map.get("sku")));
			if (map.get("productId") == null) {
				// new item
				Product newProduct = new Product((String) map.get("sku"), "Techdata Feed");
				this.webJaguar.insertProduct(newProduct, null);
				map.put("productId", newProduct.getId());
				
				// add supplier
				Supplier supplier = new Supplier();
				supplier.setId(supplierId);
				supplier.setSku((String) map.get("sku"));
				supplier.setSupplierSku(((String) map.get("sku")).substring(3)); // remove leading TD-
				this.webJaguar.insertProductSupplier(supplier);
			}
			for (Integer catId: catIds) {
				HashMap<String, Object> mapCategory = new HashMap<String, Object>();
				mapCategory.put("productId", map.get("productId"));
				mapCategory.put("categoryId", catId);
				categoryData.add(mapCategory);
			}
			
			total++;
			
			data.add(map);
			
			if (data.size() == 5000) {
				// products
				this.webJaguar.updateProduct(csvFeedList, data, null);
				
				// associate to supplier
				this.webJaguar.nonTransactionSafeUpdateProductSupplier(supplierId, data);
				data.clear();

				// associate to categories				
				this.webJaguar.nonTransactionSafeInsertCategories(categoryData);
				categoryData.clear();
			}
	    }
		
		if (data.size() > 0) {
			// products
			this.webJaguar.updateProduct(csvFeedList, data, null);	
			
			// associate to supplier
			this.webJaguar.nonTransactionSafeUpdateProductSupplier(supplierId, data);
			data.clear();
			
			// associate to categories
			this.webJaguar.nonTransactionSafeInsertCategories(categoryData);
			categoryData.clear();	
		}
		
	    inputStream.close();
	    
	    // make old techdata items inactive
		this.webJaguar.deleteOldProducts("TECHDATA", true);
		
		file.delete();
	    
	    return total;
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}
