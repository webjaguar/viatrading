package com.webjaguar.web.quartz;

import java.util.Date;
import java.util.Properties;
import org.springframework.util.StringUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.crm.CrmContact;


public class BounceMailReaderJob extends WebApplicationObjectSupport{
		
		private WebJaguarFacade webJaguar;
		public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
		
	// set up pop server attribute, invoke methods to receive email messages
	public void executeBounceMailReader() {
		System.out.println("executeInternal:");
		// set pop server attributes
		String popServer = "mail.webjaguarmail.com";
        String popUser = "bounce@webjaguarmail.com";
        String popPassword = "12jjh789";
        
        // get webjaguarFacade instance
        try {
            // receive email message
			receive(popServer, popUser, popPassword);
		} catch (Exception e) {
			System.out.println("2 Exception:");
		}
    }
	
	// receive email messages
	private void receive (String popServer, String popUser, String popPassword)
	{
        Store store=null;
	    Folder folder=null;
	    
        try
	    {
	        // -- Get hold of the default session --
	        Properties props = System.getProperties();
	        Session session = Session.getDefaultInstance(props, null);

	        // -- Get hold of a POP3 message store, and connect to it --
	        store = session.getStore("pop3");
	        store.connect(popServer, popUser, popPassword);
	      
	        // -- Try to get hold of the default folder --
	        folder = store.getDefaultFolder();
	        if (folder == null) throw new Exception("No default folder");

	        // -- ...and its INBOX --
	        folder = folder.getFolder("INBOX");
	        if (folder == null) throw new Exception("No POP3 INBOX");

	        // -- Open the folder for read only --
	        folder.open(Folder.READ_WRITE);

	        // -- Get the message wrappers and process them --
	        Message[] msgs = folder.getMessages();
	        
	        for (int msgNum = 0; msgNum < msgs.length; msgNum++)
	        {
	            setBouncedAttribute(msgs[msgNum]);
	        }
        }
	    catch (Exception ex)
	    {
	      ex.printStackTrace();
	    }
	    finally
	    {
	        // -- Close down nicely --
	        try
	        {
	            if (folder!=null) folder.close(true);
	            if (store!=null) store.close();
	        }
	        catch (Exception ex2) {ex2.printStackTrace();}
	    }
	}
	
	// read email message
	private void setBouncedAttribute(Message message){
		try
	    {
	    	
            
	        // -- Get the header information --
	        String returnPathEmail = message.getAllRecipients()[0].toString();
	        
	        // tokenize the email header as userid,emailid,campaignid
	        // siteId_campaignName_crmContactId_con_@webjaguarmail.com
	        String[] idList = StringUtils.tokenizeToStringArray(returnPathEmail,"_");
	        
	        String siteId = idList[0];
	        Integer campaignId = new Integer(idList[1]);
	        Integer crmContactId = new Integer(idList[2]);
	        String contact = idList[3];
	        
	        if (webJaguar.getSiteId().equalsIgnoreCase(siteId)) {
	        	// flag message as deleted
		    	message.setFlag(Flags.Flag.DELETED, true);
		    	
		    	if ( contact.equalsIgnoreCase("con")) {
		        	CrmContact crmContact = webJaguar.getCrmContactById(new Integer(crmContactId));
		        	crmContact.setBounced(true);
		        	webJaguar.updateCrmContact(crmContact);
		        } else {
		        	
		        }
		        
		        //EmailCampaign emailCampaign = webJaguar.getEmailCampaignById(campaignId);
		        webJaguar.addEmailCampaignBounce(campaignId);
	        }
	    }
	    catch (Exception ex)
	    {
	        ex.printStackTrace();
	    }
    }
}