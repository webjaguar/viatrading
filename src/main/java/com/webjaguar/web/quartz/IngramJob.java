/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.17.2008
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;

import com.webjaguar.web.domain.Utilities;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Product;
import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.ingrammicro.IngramMicroCategory;

public class IngramJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
    
	public void updateCatalog() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		List<CsvFeed> csvFeedList = this.webJaguar.getCsvFeed("ingram-catalog");
		
		// Ingram configuration
		String[] config = siteConfig.get("INGRAM").getValue().split(",");
		
		if (!(config.length > 6 && (Boolean) gSiteConfig.get("gSITE_ACTIVE")) || csvFeedList.isEmpty()) {
			// return if ingram feature is disabled 
			return;
		}
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		// for example put this on database
		// neg_inventory=1,show_neg_inventory=1
		String[] extra = {};
		if (siteConfig.get("INGRAM_EXTRA").getValue().trim().length() > 0) {
			extra = siteConfig.get("INGRAM_EXTRA").getValue().split(",");
		}
		
		// supplier id
		Integer supplierId = Integer.parseInt(config[4]);
		if (!this.webJaguar.isValidSupplierId(supplierId)) {
			// not a valid supplier
			return;
		}

		File tempFolder = new File(getServletContext().getRealPath("temp"));	
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File ingramMicroDir = new File(tempFolder, "ingram");
		if (!ingramMicroDir.exists()) {
			ingramMicroDir.mkdir();
		}
		File priceZip = new File(ingramMicroDir, "PRICE.ZIP");
		File categoryCsv = new File(ingramMicroDir, "NEWCATS.TXT");
		File inventoryZip = new File(ingramMicroDir, "TOTAL.ZIP");
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
				        
	        if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
		        ftp.enterLocalPassiveMode();
		        ftp.setFileType(FTP.BINARY_FILE_TYPE);	// for zip files
		        
		        // NEWCATS.TXT
		        boolean categoryDownloaded = false;
	        	if (ftp.retrieveFile("FUSION/US/NEWCATS/NEWCATS.TXT", new FileOutputStream(categoryCsv))) {
	        		categoryDownloaded = true;
	        	}		        
		        
	        	boolean hasCatId = false;
	        	Map<String, IngramMicroCategory> categoryMap = this.webJaguar.getIngramMicroCategoryMap();
	        	for (IngramMicroCategory category: categoryMap.values()) {
	        		if (category.getCatId() != null) {
	        			hasCatId = true;
	        			break;
	        		}
	        	}
	        	
		        // PRICE.TXT
		        boolean priceDownloaded = false;
	        	if (hasCatId && ftp.retrieveFile(config[3], new FileOutputStream(priceZip))) {
	        		priceDownloaded = true;
	        	}	  
	        	
	        	// AVAIL (INVENTORY) -- TOTAL.TXT
		        boolean inventoryDownloaded = false;
	        	if (priceDownloaded && ftp.retrieveFile("FUSION/US/AVAIL/TOTAL.ZIP", new FileOutputStream(inventoryZip))) {
	        		inventoryDownloaded = true;
	        	}	  	        	
	        	
    	        // Logout from the FTP Server and disconnect
    	        ftp.logout();
    	        ftp.disconnect();	        			        	
    	        
    	        if (categoryDownloaded) parseCategoryFile(categoryCsv);
    	        
    	        if (priceDownloaded) parsePriceFile(priceZip, categoryMap, csvFeedList, supplierId, extra, siteConfig);
    	        
    	        if (inventoryDownloaded && (Boolean) gSiteConfig.get("gINVENTORY")) parseInventoryFile(inventoryZip);
 
    	        if (priceDownloaded) {
        			// make ingram micro zero inventory inactive
    	        	// SOMA2U WANTS TO HAVE ALL PRODUCTS TO BE ACTIVE REGARDLESS OF INVENTORY.
    				// CHANNGED THEIR MIND 
        			this.webJaguar.inventoryZeroInactive("INGRAM");	    	        	
    	        }
    	        
	        } else { // login failed
	        	notifyAdmin("INGRAM updateCatalog() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
	        }
		} catch (Exception e) {
			notifyAdmin("INGRAM updateCatalog() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		sbuff.append("Read PRICE.TXT file." + "\n\n");
		notifyAdmin("updateCatalog() - IngramJob on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
	
	private int parseCategoryFile(File categoryCsv) throws Exception {
		
		int count = 0;
		
		List<IngramMicroCategory> categories = new ArrayList<IngramMicroCategory>();
		CSVReader reader = new CSVReader(new FileReader(categoryCsv));
		
		String[] nextLine;
		
		while ((nextLine = reader.readNext()) != null) {
			IngramMicroCategory category = new IngramMicroCategory();
			category.setDescr(nextLine[0].trim());
			category.setCat(nextLine[1].trim());
			category.setSub(nextLine[2].trim());
			categories.add(category);
			
			count++;
		}
		
		if (categories.size() > 0) {
			this.webJaguar.nonTransactionSafeInsertIngramMicroCategories(categories);
		}
		
		reader.close();
		
		return count;
	}
	
	private int parsePriceFile(File priceZip, Map<String, IngramMicroCategory> categoryMap, List<CsvFeed> csvFeedList, int supplierId, String[] extra, Map<String, Configuration> siteConfig) throws Exception {
		
		String[] config = siteConfig.get("INGRAM").getValue().split(",");
		int cost = Integer.parseInt(config[5]);
		int cat = Integer.parseInt(config[6]);
		String fieldForCost = config[7];
		
		Double priceTableChange[] = { null, null, null, null, null };
		for (int i=1; i<priceTableChange.length; i++) {
			try {
				priceTableChange[i] = Double.parseDouble(siteConfig.get("INGRAM_PRICETABLE" + i + "_CHANGE").getValue());	
				csvFeedList.add(new CsvFeed("price_table_" + i));
			} catch (NumberFormatException e) {
				// do nothing
			}		
		}

		Date today = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("EEE"); // day of week
		
		csvFeedList.add(new CsvFeed("active"));
		csvFeedList.add(new CsvFeed("feed"));
		if (dateFormatter.format(today).equals("Mon")) {
			// full on Monday
			// incremental on Tues-Fri
			csvFeedList.add(new CsvFeed("feed_new"));
		}
		// cost
		csvFeedList.add(new CsvFeed("field_"+fieldForCost));
		
		// price 1
		List<CsvFeed> csvFeedPrice = new ArrayList<CsvFeed>();
		List<Map <String, Object>> priceData = new ArrayList<Map <String, Object>>();
		csvFeedPrice.add(new CsvFeed("sku")); 		
		csvFeedPrice.add(new CsvFeed("price_1")); // price 1
		
		csvFeedPrice.add(new CsvFeed("hide_price")); // hide price
		
		// extra configuration
		for (int i=0; i<extra.length; i++) {
			csvFeedList.add(new CsvFeed(extra[i].substring(0, extra[i].indexOf("="))));
		}
		
		int total = 0;
		
		ZipFile zipFile = new ZipFile(priceZip);
		ZipEntry entry = zipFile.getEntry("PRICE.TXT");
		if (entry != null) {
			InputStream inStream = zipFile.getInputStream(entry);
			CSVReader reader = new CSVReader(new InputStreamReader(inStream));

			List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
			List<Map <String, Object>> categoryData = new ArrayList<Map <String, Object>>();
			
			String[] nextLine;

			while ((nextLine = reader.readNext()) != null) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				List<Integer> catIds = new ArrayList<Integer>();
				
				Double priceChange = null;
				String priceChangeBase = "";
				
				// categories
				String code = nextLine[cat].trim();
				//System.out.print(" " + code);
				if (code.length() >= 2 && categoryMap.containsKey(code.substring(0, 2) + "*")) {
					// level 1
					IngramMicroCategory category = categoryMap.get(code.substring(0, 2) + "*");
					if (category.getCatId() != null) catIds.add(category.getCatId());
					if (category.getPriceChange() != null) priceChange = category.getPriceChange();
					if (category.getPriceChangeBase() != null) priceChangeBase = category.getPriceChangeBase();
				}
				if (code.length() >= 4 && categoryMap.containsKey(code.substring(0,4))) {
					// level 2
					IngramMicroCategory category = categoryMap.get(code.substring(0,4));
					if (category.getCatId() != null) catIds.add(category.getCatId());
					if (category.getPriceChange() != null) priceChange = category.getPriceChange();
					if (category.getPriceChangeBase() != null) priceChangeBase = category.getPriceChangeBase();
				}
				if (catIds.isEmpty()) {
					// skip this item
					System.out.println(" skip this item " + code);
					continue;
				}
				
				for (CsvFeed csvFeed: csvFeedList) {
					if (csvFeed.getColumn() != null) {
						String sValue = nextLine[csvFeed.getColumn()].trim();
						if (csvFeed.getColumnName().equals("sku")) {
							map.put("sku", "IM-" + sValue);
						} else if (csvFeed.isZeroAsNull()) {
    						try {
    							Double value = Double.parseDouble(sValue);
    							if (value == 0) {
    								value = null;
    							}
								map.put(csvFeed.getColumnName(), value);
    						} catch (NumberFormatException e) {
    							map.put(csvFeed.getColumnName(), null);									
    						}
    					} else {
    						map.put(csvFeed.getColumnName(), sValue);							
    					}	        						
					}
				}
				
				// active
				if (nextLine[0].equals("D")) {
					map.put("active", false);
				} else {
					map.put("active", true);
				}
				
				// cost
				map.put("field_"+fieldForCost, Double.parseDouble(nextLine[cost]));
				
				map.put("feed", "INGRAM");
				map.put("cost", Double.parseDouble(nextLine[cost]));
				if (dateFormatter.format(today).equals("Mon")) {
					map.put("feed_new", true);
				}
				
				// hide price
				map.put("hide_price", false);

				// price 1
				boolean manual = false;
				Double basePrice = null;
				boolean custom = false;
				for (String base: priceChangeBase.split(",")) {
					try {
						if (base.equals("COST") || base.equals("")) {
							basePrice = (Double) map.get("cost");
						} else if (base.equals("MSRP")) {
							basePrice = (Double) map.get("msrp");		
						} else if (base.equals("MANUAL")) {
							manual = true;
						} else if (base.equals("CUSTOM")) {
							custom = true;
						}
					} catch (Exception e) {}
					if (basePrice != null || manual || custom) break;
				}
				if (basePrice != null && basePrice >= 0 && priceChange != null) {
					map.put("price_1", Utilities.roundFactory((basePrice + basePrice*priceChange/100),2,BigDecimal.ROUND_DOWN));
				} else if (basePrice == null && custom) {
					float RegMg = (float) 0.1765;   /* Regular Margin equivalent to 17.65% over cost, 15% of selling price or 1.1765 times the cost. */
					float MinMg = (float) 0.0753;	/* Minimum Margin equivalent to 7.55% over cost, 7% of selling price or 1.0753 times the cost. */
					
					if (siteConfig.get("INGRAM_CUSTOM_FORMULA_MARGIN").getValue().trim().length() > 0) {
						String[] marginString = siteConfig.get("INGRAM_CUSTOM_FORMULA_MARGIN").getValue().split(",");
						RegMg = (float) Double.parseDouble(marginString[0]);
						MinMg = (float) Double.parseDouble(marginString[1]);
					}
					
					Double productCost = null;
					Double productMsrp = null;
					try { productCost = (Double) map.get("cost");} catch (Exception e) {}
					try { productMsrp = (Double) map.get("msrp");} catch (Exception e) {}
					
					if(productMsrp != null && productCost != null) {
					      if(productCost == 0.01) { 
					    	  map.put("price_1", Utilities.roundFactory(0.00,2,BigDecimal.ROUND_DOWN));
							  map.put("hide_price", true);
					      } else if(productMsrp-productCost > productCost*(RegMg)) { 
							  map.put("price_1", Utilities.roundFactory(productCost*(1+RegMg), 2, BigDecimal.ROUND_DOWN).intValue() + 0.95);
						  } else if(productCost>productMsrp) { 
							  map.put("price_1", Utilities.roundFactory(productCost*(1+RegMg), 2, BigDecimal.ROUND_DOWN).intValue() + 0.95);
						  } else { 
							  map.put("price_1", Utilities.roundFactory(productCost*(1+MinMg), 2, BigDecimal.ROUND_DOWN).intValue() + 0.95);
						  }
				     } else if(productMsrp == null && productCost != null) { 
				    	 map.put("price_1", Utilities.roundFactory(productCost*(1+RegMg), 2, BigDecimal.ROUND_DOWN).intValue() + 0.95);
				     } else if(productMsrp != null && productCost == null) { 
				    	 map.put("price_1", Utilities.roundFactory(0.00,2,BigDecimal.ROUND_DOWN));
				    	 map.put("hide_price", true);
				     } else  {
				    	 map.put("price_1", Utilities.roundFactory(0.00,2,BigDecimal.ROUND_DOWN));
				    	 map.put("hide_price", true);
				     }
				}
				if (!manual) {
					double price1 = (Double) map.get("price_1");
					for (int i=1; i<priceTableChange.length; i++) {
    					if (priceTableChange[i] != null) {
		    				map.put("price_table_" + i, price1 * (1 + priceTableChange[i]/100));
    					}
    				}
					
					// change the price only if not MANUAL
					priceData.add(map);							
				}
				
				// extra configuration
				for (int i=0; i<extra.length; i++) {
					map.put(extra[i].substring(0, extra[i].indexOf("=")), extra[i].substring(extra[i].indexOf("=")+1));
				}
				
				map.put("productId", this.webJaguar.getProductIdBySku((String) map.get("sku")));
				if (map.get("productId") == null) {
					// new item
					Product newProduct = new Product((String) map.get("sku"), "Ingram Feed");
					this.webJaguar.insertProduct(newProduct, null);
					map.put("productId", newProduct.getId());
					
					// add supplier
					Supplier supplier = new Supplier();
					supplier.setId(supplierId);
					supplier.setSku((String) map.get("sku"));
					supplier.setSupplierSku(((String) map.get("sku")).substring(3)); // remove leading IM-
					this.webJaguar.insertProductSupplier(supplier);
				}
				for (Integer catId: catIds) {
					HashMap<String, Object> mapCategory = new HashMap<String, Object>();
					mapCategory.put("productId", map.get("productId"));
					mapCategory.put("categoryId", catId);
					categoryData.add(mapCategory);
				}
				
				total++;
				
				data.add(map);
				// for test
				try {
					System.out.println("sku: " + (String) map.get("sku") + "msrp " +   (Double) map.get("msrp"));
				}
				catch (Exception e) {}
				
				if (data.size() == 5000) {
					// products
					this.webJaguar.updateProduct(csvFeedList, data, null);
					
					// associate to supplier
					this.webJaguar.nonTransactionSafeUpdateProductSupplier(supplierId, data);
					data.clear();
					
					// associate to categories				
					this.webJaguar.nonTransactionSafeInsertCategories(categoryData);
					categoryData.clear();
				}
				
				// price 1
				if (priceData.size() == 5000) {
					this.webJaguar.updateProduct(csvFeedPrice, priceData, null);
					priceData.clear();
				}
			}
			
			if (data.size() > 0) {
				// products
				this.webJaguar.updateProduct(csvFeedList, data, null);	
				
				// associate to supplier
				this.webJaguar.nonTransactionSafeUpdateProductSupplier(supplierId, data);
				data.clear();	
				
				// associate to categories
				this.webJaguar.nonTransactionSafeInsertCategories(categoryData);
				categoryData.clear();
			}
			
			// price 1
			if (priceData.size() > 0) {
				this.webJaguar.updateProduct(csvFeedPrice, priceData, null);
				priceData.clear();
			}
			
			reader.close();
			inStream.close();
			
			if (dateFormatter.format(today).equals("Mon")) {
				// make old ingram micro items inactive
				// SOMA2U WANTS TO HAVE ALL PRODUCTS TO BE ACTIVE REGARDLESS OF INVENTORY.
				// CHANNGED THEIR MIND 
				this.webJaguar.deleteOldProducts("INGRAM", true);				
			}
			
			// make items free shipping
			if (data.size() > 0 && data.get(0).containsKey("weight") && siteConfig.get("FREESHIPPING_OPTIONCODE").getValue().length() > 0) {
				this.webJaguar.nonTransactionSafeUpdateFreeShipping(siteConfig.get("FREESHIPPING_OPTIONCODE").getValue(), siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue());
			}    
		}
		zipFile.close();	
		
		return total;
	}	

	private void parseInventoryFile(File inventoryZip) throws Exception {
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		
		ZipFile zipFile = new ZipFile(inventoryZip);
		ZipEntry entry = zipFile.getEntry("TOTAL.TXT");
		InputStream inStream = zipFile.getInputStream(entry);
		CSVReader reader = new CSVReader(new InputStreamReader(inStream));
		
		String[] nextLine;
		
		while ((nextLine = reader.readNext()) != null) {			
			HashMap<String, Object> map = new HashMap<String, Object>();
			map.put("sku", "IM-" + nextLine[0].trim());
			if (nextLine[0].trim().length() > 0) {
				try {
					map.put("inventory", Integer.parseInt(nextLine[1]));
				} catch (NumberFormatException e) {
					continue;
				}
				data.add(map);				
			}
			
			if (data.size() == 10000) {
				this.webJaguar.updateIngramMicroInventoryBySkus(data);
				data.clear();
			}
		}
		
		if (data.size() > 0) {
			this.webJaguar.updateIngramMicroInventoryBySkus(data);
			data.clear();
		}
		
		reader.close();
		inStream.close();
		zipFile.close();
	}	
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}