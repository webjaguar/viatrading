/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 01.21.2010
 */

package com.webjaguar.web.quartz;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.thirdparty.payment.ebillme.EBillme;

public class EBillmeJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void getOrderStatus() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		if (!((Boolean) gSiteConfig.get("gSITE_ACTIVE")
				&& siteConfig.get("EBILLME_URL").getValue().trim().length() > 0
				&& siteConfig.get("EBILLME_PAYEETOKEN").getValue().trim().length() > 0
				&& siteConfig.get("EBILLME_UID").getValue().trim().length() > 0
				&& siteConfig.get("EBILLME_PWD").getValue().trim().length() > 0
				)) {
			// return if eBillme is disabled 
			return;
		}
		
		SimpleDateFormat dFormatter = new SimpleDateFormat("yyyyMMddhhmm");
		
		File eBillmeDir = new File(getServletContext().getRealPath("/eBillme"));	
		if (!eBillmeDir.exists()) {
			eBillmeDir.mkdir();
		}
		File eBillmeProcessed = new File(eBillmeDir, "processed");
		if (!eBillmeProcessed.exists()) {
			eBillmeProcessed.mkdir();
		}
		File getOrderStatus = new File(eBillmeProcessed, "getOrderStatus-" + dFormatter.format(new Date()) + ".xml");
		
		try {
			StringBuffer xml = new StringBuffer();
			xml.append("<getorderstatus_request xmlns=\"http://ebillme.com/ws/v3\">");
			xml.append("  <apiversion>3.0</apiversion>");
			if (siteConfig.get("EBILLME_URL").getValue().equals("https://my.eBillme.com/Auth/")) {
				// production
				xml.append("  <transtype>P</transtype>");
			} else {
				// test
				xml.append("  <transtype>T</transtype>");				
			}
			xml.append("  <authorization>");
			xml.append("  <payeetoken>" + siteConfig.get("EBILLME_PAYEETOKEN").getValue() + "</payeetoken>");
			xml.append("    <username>" + siteConfig.get("EBILLME_UID").getValue() + "</username>");
			xml.append("    <password>" + siteConfig.get("EBILLME_PWD").getValue() + "</password>");
			xml.append("  </authorization>");
			xml.append("</getorderstatus_request>");
			String responseXml = null;
			
			if (siteConfig.get("EBILLME_URL").getValue().equals("https://my.eBillme.com/Auth/")) {
				// production
				com.ebillme.production.v3.EBillmeServiceV3 ebillmeServiceV3 = new com.ebillme.production.v3.EBillmeServiceV3ServiceLocator().geteBillmeServiceV3();
				responseXml = ebillmeServiceV3.getOrderStatus(xml.toString());								
			} else {
				// test
				com.ebillme.v3.uat.EBillmeServiceV3 ebillmeServiceV3 = new com.ebillme.v3.uat.EBillmeServiceV3ServiceLocator().geteBillmeServiceV3();
				responseXml = ebillmeServiceV3.getOrderStatus(xml.toString());				
			}

			// write response to file 
			FileWriter fstream = new FileWriter(getOrderStatus);
	        BufferedWriter out = new BufferedWriter(fstream);
	        out.write(responseXml);
	        out.close();
	        fstream.close();
			
	        SAXBuilder builder = new SAXBuilder(false);
			Document doc = builder.build(getOrderStatus);
			Element paymentchanges = doc.getRootElement().getChild("paymentchanges", Namespace.getNamespace("http://ebillme.com/ws/v3"));
			
			if (paymentchanges == null || Integer.parseInt(paymentchanges.getAttributeValue("recordsize", Namespace.getNamespace("base", "http://ebillme.com/ws/base"))) == 0) {
				// delete file
				getOrderStatus.delete();
			} else {
				Iterator orders = paymentchanges.getChildren().iterator();
				while (orders.hasNext()) {
					Element order = (Element) orders.next();
					EBillme eBillme = new EBillme();
					
					eBillme.setOrderrefid(order.getChildText("orderrefid", Namespace.getNamespace("http://ebillme.com/ws/v3")));
					eBillme.setXmlFile(getOrderStatus.getName());
					
					this.webJaguar.updateEbillmePayment(eBillme);
				}
				
				// move file to processed folder
				getOrderStatus.renameTo(new File(eBillmeProcessed, getOrderStatus.getName()));
			}
		} catch (Exception e) {
			notifyAdmin("EBillme getOrderStatus() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}	
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
	
}


/*
			// parse with SAX first
			XMLParse handler = new XMLParse();
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser saxParser = spf.newSAXParser();
			saxParser.parse(getOrderStatus, handler);

package com.webjaguar.thirdparty.payment.ebillme;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLParse extends DefaultHandler {
	String qualifiedName = "";
	int recordSize = 0;
	
	public void startElement(String uri, String localName, 
			String qualifiedName, Attributes attributes) throws SAXException {

		this.qualifiedName = qualifiedName;
		if (qualifiedName.equals("paymentchanges")) {
			for (int i = 0; i< attributes.getLength(); i++) {
				if (attributes.getQName(i).equals("base:recordsize")) {
					recordSize = Integer.parseInt(attributes.getValue(i));					
				}
			}
		}
	}
	
	public int getRecordSize() {
		return recordSize;	
	}
}
			
*/