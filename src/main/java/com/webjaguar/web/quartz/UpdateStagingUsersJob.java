/*
 * Copyright 2014 Advanced E-Media Solutions
 * @author Soujanya Chichula
 * @since 04.23.2014
 * UPDATE Users Rating I & II & Dialing Notes From PHP application
 */
package com.webjaguar.web.quartz;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.dao.StagingDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.DialingNote;
import com.webjaguar.model.StagingUser;

public class UpdateStagingUsersJob {
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private StagingDao stagingDao;
	public void setStagingDao(StagingDao stagingDao) { this.stagingDao = stagingDao; }
	
	private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
    private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	Map<String, Object> gSiteConfig;
	Map<String, Configuration> siteConfig;
	
	public void updateUsers() {
		gSiteConfig = this.globalDao.getGlobalSiteConfig();
		siteConfig = this.webJaguar.getSiteConfig();
		
		List<StagingUser> usersList = this.stagingDao.getStagingUsersData();
		if(usersList != null && !usersList.isEmpty()) {
			for(StagingUser sUser: usersList) {
				if((sUser.getUsername() != null && !sUser.getUsername().isEmpty()) || (sUser.getCrmContactId() != null && sUser.getCrmContactId() > 0)) {
					if((sUser.getRating1() != null && !sUser.getRating1().isEmpty()) || sUser.getRating2() != null && !sUser.getRating2().isEmpty()) {
						try {
							if(this.webJaguar.updateUserRatings(sUser) > 0) {
								this.stagingDao.deleteStagingUsers(sUser.getId());
							}
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
					// Insert dialing note history
					if(sUser.getNewDialingNote() != null && sUser.getNewDialingNote().getNote() != null && !sUser.getNewDialingNote().getNote().isEmpty()) {
						DialingNote note = new DialingNote();
						if(sUser.getCrmContactId() != null && sUser.getCrmContactId() > 0) {
							note.setCrmId(sUser.getCrmContactId());
						}else {
							if(sUser.getUsername()!=null && sUser.getUsername().length()>0){
								note.setCrmId(this.webJaguar.getCrmIdByUsername(sUser.getUsername()));
							}
						}
						
						if(sUser.getUsername()!=null && sUser.getUsername().length()>0){
							note.setCustomerId(this.webJaguar.getCustomerIdByUsername(sUser.getUsername()));
						}else {
							if(sUser.getCrmContactId() != null && sUser.getCrmContactId() > 0) {
								note.setCustomerId(this.webJaguar.getUserIdByCrmId(sUser.getCrmContactId()));
							}
						}
						note.setNote(sUser.getNewDialingNote().getNote());
						note.setCreated(sUser.getNewDialingNote().getCreated());
						if((note.getCustomerId() != null && note.getCustomerId() > 0) || (note.getCrmId() != null && note.getCrmId() > 0)) {
							if(this.webJaguar.insertDialingNoteHistory(note) > 0) {
								this.stagingDao.deleteStagingUsers(sUser.getId());
							}
						}
					}
				}
			}
		}
	}
}
