/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 02.11.2009
 */

package com.webjaguar.web.quartz;

import java.util.Map;
import org.springframework.mail.MailSender;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.thirdparty.jgoodin.Jgoodin;

public class JgoodinJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void updateProducts() {

		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		Jgoodin jgoodin = new Jgoodin(siteConfig, gSiteConfig, mailSender, webJaguar);
		jgoodin.getJgoodinProducts();
	}
}