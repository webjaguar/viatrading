/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.19.2008
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Product;
import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.dsi.DsiCategory;

public class DsiJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void updateInventory() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map<String, CsvFeed> csvFeedMap = this.webJaguar.getCsvFeedMapping("dsi-inventory");
		Map <String, DsiCategory> categoryMap = this.webJaguar.getDsiCategoryMap();
		
		int discoDate = -1;
		int available = -1;
		int contractPrice = -1;
		int MAP = -1;
		int MSRP = -1;
		int category = -1;
		int subCategory = -1;
		int manuf = -1;
		
		// DSI configuration
		String[] config = siteConfig.get("DSI").getValue().split(",");
		
		if (!(config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if dsi feature is disabled 
			return;
		}
		
		// supplier id
		Integer supplierId = Integer.parseInt(config[4]);
		if (!this.webJaguar.isValidSupplierId(supplierId)) {
			// not a valid supplier
			return;
		}
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		
		// price 1
		List<CsvFeed> csvFeedPrice = new ArrayList<CsvFeed>();
		List<Map <String, Object>> priceData = new ArrayList<Map <String, Object>>();
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	        	        
	        InputStream inStream = ftp.retrieveFileStream("Inventory/" + config[3] + "_InventoryFeed.csv");

	        if (inStream != null) {
	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream));
		    	
	        	String[] headers = reader.readNext();
	        	for (int i=0; i<headers.length; i++) {
					if (csvFeedMap.containsKey(headers[i].toLowerCase())) {
						CsvFeed csvFeed = csvFeedMap.get(headers[i].toLowerCase());
						csvFeed.setColumn(i);
						csvFeedList.add(csvFeed);
					}
					if (headers[i].equalsIgnoreCase("DiscoDate")) {
						discoDate = i;
					} else if (headers[i].equalsIgnoreCase("Available")) {
						available = i;
					} else if (headers[i].equalsIgnoreCase("ContractPrice")) {
						contractPrice = i;
					} else if (headers[i].equalsIgnoreCase("MAP")) {
						MAP = i;
					} else if (headers[i].equalsIgnoreCase("MSRP")) {
						MSRP = i;
					} else if (headers[i].equalsIgnoreCase("Category")) {
						category = i;
					} else if (headers[i].equalsIgnoreCase("SubCategory")) {
						subCategory = i;
					} else if (headers[i].equalsIgnoreCase("Vendor")) {
						manuf = i;
					}
				}
	        	if (discoDate > -1 && available > -1) {
	        		csvFeedList.add(new CsvFeed("active"));
	        	}
	        	
				if (contractPrice > -1 && siteConfig.get("DSI_PRICETABLE_CHANGE").getValue().length() > 0) {
		    		csvFeedList.add(new CsvFeed("price_table_1"));				
		    		csvFeedList.add(new CsvFeed("price_table_2"));				
		    		csvFeedList.add(new CsvFeed("price_table_3"));				
		    		csvFeedList.add(new CsvFeed("price_table_4"));				
		    		csvFeedList.add(new CsvFeed("price_table_5"));				
		    		csvFeedList.add(new CsvFeed("price_table_6"));				
		    		csvFeedList.add(new CsvFeed("price_table_7"));				
		    		csvFeedList.add(new CsvFeed("price_table_8"));				
		    		csvFeedList.add(new CsvFeed("price_table_9"));				
		    		csvFeedList.add(new CsvFeed("price_table_10"));				
				}
				boolean computePrice = false;
				if (contractPrice > -1 && category > -1 && subCategory > -1 && manuf > -1 && MAP > -1 && MSRP > -1) {
					csvFeedPrice.add(new CsvFeed("sku")); 		
					csvFeedPrice.add(new CsvFeed("price_1")); // price 1
					computePrice = true;
				}

				csvFeedList.add(new CsvFeed("feed"));
				csvFeedList.add(new CsvFeed("feed_new"));
	        	
				String[] nextLine;
				while ((nextLine = reader.readNext()) != null) {
					HashMap<String, Object> map = new HashMap<String, Object>();
					
					for (CsvFeed csvFeed: csvFeedList) {
						if (csvFeed.getColumn() != null) {
							String sValue = nextLine[csvFeed.getColumn()].trim();
							if (csvFeed.getColumnName().equals("sku")) {
								map.put("sku", "DSI-" + sValue);
							} else if (csvFeed.isZeroAsNull()) {
								try {
									Double value = Double.parseDouble(sValue);
									if (value == 0) {
										value = null;
									}
									map.put(csvFeed.getColumnName(), value);
								} catch (NumberFormatException e) {
									map.put(csvFeed.getColumnName(), null);									
								}
							} else {
								map.put(csvFeed.getColumnName(), sValue);					
							}								
						}
					}
					
					// active
					if (discoDate > -1 && available > -1) {
						if (nextLine[discoDate].length() > 0 && nextLine[available].equals("0")) {
							map.put("active", false);
						} else {
							map.put("active", true);
						}
					}
					
		    		map.put("feed", "DSI");				
					map.put("feed_new", true);
					map.put("cost", Double.parseDouble(nextLine[contractPrice]));
					
					map.put("productId", this.webJaguar.getProductIdBySku((String) map.get("sku")));
					if (map.get("productId") == null) {
						// new item
						Product newProduct = new Product((String) map.get("sku"), "DSI Feed");
						if (config[3].equals("2002045")) {
							// perkslive
							newProduct.setProtectedLevel("1"); // protected level 1
						}
						newProduct.setAddToList(true);
						this.webJaguar.insertProduct(newProduct, null);
						map.put("productId", newProduct.getId());
						
						// add supplier
						Supplier supplier = new Supplier();
						supplier.setId(supplierId);
						supplier.setSku((String) map.get("sku"));
						supplier.setSupplierSku(((String) map.get("sku")).substring(4)); // remove leading DSI-
						this.webJaguar.insertProductSupplier(supplier);
					}
					
					// price tables
		        	if (contractPrice > -1 && !nextLine[contractPrice].equals("0") && siteConfig.get("DSI_PRICETABLE_CHANGE").getValue().length() > 0) {
						String[] priceTableChange = siteConfig.get("DSI_PRICETABLE_CHANGE").getValue().split(",");
						try {
							Double price = Double.parseDouble(nextLine[contractPrice]);
							for (int i=0; i<priceTableChange.length; i++) {
								map.put("price_table_" + (i+1), price + price*Double.parseDouble(priceTableChange[i])/100);	
							}
						} catch (NumberFormatException e) {
							for (int i=0; i<priceTableChange.length; i++) {
								map.put("price_table_" + (i+1), null);								
							}
						}
		        	}
		        	
					// price 1
					if (computePrice) {
						Double priceChange = null;
						String priceChangeBase = "";
						
						if (categoryMap.containsKey(nextLine[manuf].toLowerCase().trim() + "--*--*")) {
							// level 1 
							DsiCategory dsiCategory = categoryMap.get(nextLine[manuf].toLowerCase().trim() + "--*--*");
							if (dsiCategory.getPriceChange() != null) priceChange = dsiCategory.getPriceChange();
							if (dsiCategory.getPriceChangeBase() != null) priceChangeBase = dsiCategory.getPriceChangeBase();
						}
						if (categoryMap.containsKey(nextLine[manuf].toLowerCase().trim() + "--" + nextLine[category].toLowerCase().trim() + "--*")) {
							// level 2
							DsiCategory dsiCategory = categoryMap.get(nextLine[manuf].toLowerCase().trim() + "--" + nextLine[category].toLowerCase().trim() + "--*");
							if (dsiCategory.getPriceChange() != null) priceChange = dsiCategory.getPriceChange();
							if (dsiCategory.getPriceChangeBase() != null) priceChangeBase = dsiCategory.getPriceChangeBase();
						}
						if (categoryMap.containsKey(nextLine[manuf].toLowerCase().trim() + "--" + nextLine[category].toLowerCase().trim() + "--" + nextLine[subCategory].toLowerCase().trim())) {
							// level 3
							DsiCategory dsiCategory = categoryMap.get(nextLine[manuf].toLowerCase().trim() + "--" + nextLine[category].toLowerCase().trim() + "--" + nextLine[subCategory].toLowerCase().trim());
							if (dsiCategory.getPriceChange() != null) priceChange = dsiCategory.getPriceChange();
							if (dsiCategory.getPriceChangeBase() != null) priceChangeBase = dsiCategory.getPriceChangeBase();
						}						
						
						boolean manual = false;
						Double basePrice = null;								
						for (String base: priceChangeBase.split(",")) {
							try {
								if (base.equals("COST") || base.equals("")) {
									basePrice = (Double) map.get("cost");
								} else if (base.equals("MSRP") && Double.parseDouble(nextLine[MSRP]) > 0) {
									basePrice = Double.parseDouble(nextLine[MSRP]);		
								} else if (base.equals("MAP") && Double.parseDouble(nextLine[MAP]) > 0) {
									basePrice = Double.parseDouble(nextLine[MAP]);
								} else if (base.equals("MANUAL")) {
									manual = true;
								}
							} catch (Exception e) {}
							if (basePrice != null || manual) break;
						}
						if (basePrice != null && basePrice >= 0 && priceChange != null) {
							map.put("price_1", basePrice + basePrice*priceChange/100);									
						}
						if (!manual) {
							// change the price only if not MANUAL
							priceData.add(map);							
						}
					}
					
					data.add(map);
					
					if (data.size() == 5000) {
						// products
						this.webJaguar.updateProduct(csvFeedList, data, null);
						
						// associate to supplier
						this.webJaguar.nonTransactionSafeUpdateProductSupplier(supplierId, data);
						data.clear();
					}
					
					// price 1
					if (priceData.size() == 5000) {
						this.webJaguar.updateProduct(csvFeedPrice, priceData, null);
						priceData.clear();
					}
				}
				reader.close();
				inStream.close();
				
				// update and add only if sku column is present
				if (data.size() > 0 && data.get(0).containsKey("sku")) {
					// products
					this.webJaguar.updateProduct(csvFeedList, data, null);
					
					// associate to supplier
					this.webJaguar.nonTransactionSafeUpdateProductSupplier(supplierId, data);
					data.clear();
				}
				
				// price 1
				if (priceData.size() > 0) {
					this.webJaguar.updateProduct(csvFeedPrice, priceData, null);
					priceData.clear();
				}
				
				// delete file
				ftp.deleteFile("Inventory/" + config[3] + "_InventoryFeed.csv");
				
				// make old dsi items inactive
				this.webJaguar.deleteOldProducts("DSI", true);	
				
				// make items free shipping
				if (data.size() > 0 && data.get(0).containsKey("weight") && siteConfig.get("FREESHIPPING_OPTIONCODE").getValue().length() > 0) {
					this.webJaguar.nonTransactionSafeUpdateFreeShipping(siteConfig.get("FREESHIPPING_OPTIONCODE").getValue(), siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue());
				}
				
	        }			
			
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			notifyAdmin("DSI updateInventory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}	
	}
	
	public void updateCatalog() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map<String, CsvFeed> csvFeedMap = this.webJaguar.getCsvFeedMapping("dsi-catalog");	
		Map <String, DsiCategory> categoryMap = this.webJaguar.getDsiCategoryMap();
		
		// DSI configuration
		String[] config = siteConfig.get("DSI").getValue().split(",");
		int category = -1;
		int subCategory = -1;
		int manuf = -1;
		
		if (!(config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if dsi feature is disabled 
			return;
		}
		
		// supplier id
		Integer supplierId = Integer.parseInt(config[4]);
		if (!this.webJaguar.isValidSupplierId(supplierId)) {
			// not a valid supplier
			return;
		}
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		List<Map <String, Object>> categoryData = new ArrayList<Map <String, Object>>();
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		
		// categories
		List<DsiCategory> categories = new ArrayList<DsiCategory>();
		Set<String> categorySet = new HashSet<String>();
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login("images", "1m@93S");
	        ftp.enterLocalPassiveMode();
	        	        
	        InputStream inStream = ftp.retrieveFileStream("CatalogFeed.csv");

	        if (inStream != null) {
	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream));
		        
	        	String[] headers = reader.readNext();
	        	for (int i=0; i<headers.length; i++) {
					if (csvFeedMap.containsKey(headers[i].toLowerCase())) {
						CsvFeed csvFeed = csvFeedMap.get(headers[i].toLowerCase());
						csvFeed.setColumn(i);
						csvFeedList.add(csvFeed);
						if (headers[i].equalsIgnoreCase("Category")) {
							category = i;
						} else if (headers[i].equalsIgnoreCase("SubCategory")) {
							subCategory = i;
						} else if (headers[i].equalsIgnoreCase("Vendor_Manuf")) {
							manuf = i;
						}
					}
				}
	        	
				String[] nextLine;
				while ((nextLine = reader.readNext()) != null) {
					HashMap<String, Object> map = new HashMap<String, Object>();
					
					for (CsvFeed csvFeed: csvFeedList) {
						if (csvFeed.getColumnName().equals("sku")) {
							map.put("sku", "DSI-" + nextLine[csvFeed.getColumn()].trim());
						} else {
							map.put(csvFeed.getColumnName(), nextLine[csvFeed.getColumn()]);
						}
					}
					
					if (map.containsKey("sku")) {
						map.put("productId", this.webJaguar.getProductIdBySku((String) map.get("sku")));
					}
					if (map.get("productId") == null) {
						// not in database
						continue;
					}
					
		        	if (manuf > -1 && category > -1 && subCategory > -1) {
		        		String manufName = nextLine[manuf].toLowerCase().trim();
		        		if (manufName.length() > 0) {
		        			String cat = nextLine[category].toLowerCase().trim();
			        		String sub = nextLine[subCategory].toLowerCase().trim();
			        		if (cat.length() == 0) cat = "*";
			        		if (sub.length() == 0) sub = "*";
			        		if (categorySet.add(manufName + "--" + "*" + "--" + "*")) {
			        			DsiCategory level1 = new DsiCategory();
			        			level1.setManuf(nextLine[manuf].trim());
			        			level1.setCat("*");
			        			level1.setSub("*");
			    				categories.add(level1);
			        		}		  
			        		if (categorySet.add(manufName + "--" + cat + "--" + "*")) {
			        			DsiCategory level2 = new DsiCategory();
			        			level2.setManuf(nextLine[manuf].trim());
			        			level2.setCat(nextLine[category].trim());
			        			level2.setSub("*");
			    				categories.add(level2);
			        		}
			        		if (categorySet.add(manufName + "--" + cat + "--" + sub)) {
				        		DsiCategory level3 = new DsiCategory();
				        		level3.setManuf(nextLine[manuf].trim());
				        		level3.setCat(nextLine[category].trim());
				        		level3.setSub(nextLine[subCategory].trim());
				    			categories.add(level3);			        			
			        		}
		        		}
		        	}
					
					// overview
					map.put("tab_1", "Overview");
					if (map.containsKey("tab_1_content")) {
						map.put("tab_1_content", formatTabContent((String) map.get("tab_1_content")));
					}
					// technical specifications
					map.put("tab_2", "Technical Specifications");
					if (map.containsKey("tab_2_content")) {
						map.put("tab_2_content", formatTabContent((String) map.get("tab_2_content")));
					}
					
					// keywords
					StringBuffer keywords = new StringBuffer(" ");
					if (map.containsKey("sku")) keywords.append(((String) map.get("sku")).substring(4) + " "); // remove leading DSI-
					if (map.containsKey("name")) keywords.append(map.get("name") + " ");
					map.put("keywords", keywords.toString().replace(" ", ","));
					
					// categories
					List<Integer> catIds = new ArrayList<Integer>();
					if (manuf > -1 && categoryMap.containsKey(nextLine[manuf].toLowerCase().trim() + "--*--*")) {
						// level 1 
						DsiCategory dsiCategory = categoryMap.get(nextLine[manuf].toLowerCase().trim() + "--*--*");
						if (dsiCategory.getCatId() != null) catIds.add(dsiCategory.getCatId());
					}
					if (manuf > -1 && category > -1 && 
							categoryMap.containsKey(nextLine[manuf].toLowerCase().trim() + "--" + nextLine[category].toLowerCase().trim() + "--*")) {
						// level 2
						DsiCategory dsiCategory = categoryMap.get(nextLine[manuf].toLowerCase().trim() + "--" + nextLine[category].toLowerCase().trim() + "--*");
						if (dsiCategory.getCatId() != null) catIds.add(dsiCategory.getCatId());
					}
					if (manuf > -1 && category > -1 && subCategory > -1 && 
							categoryMap.containsKey(nextLine[manuf].toLowerCase().trim() + "--" + nextLine[category].toLowerCase().trim() + "--" + nextLine[subCategory].toLowerCase().trim())) {
						// level 3
						DsiCategory dsiCategory = categoryMap.get(nextLine[manuf].toLowerCase().trim() + "--" + nextLine[category].toLowerCase().trim() + "--" + nextLine[subCategory].toLowerCase().trim());
						if (dsiCategory.getCatId() != null) catIds.add(dsiCategory.getCatId());
					}
					for (Integer catId: catIds) {
						HashMap<String, Object> mapCategory = new HashMap<String, Object>();
						mapCategory.put("productId", map.get("productId"));
						mapCategory.put("categoryId", catId);
						categoryData.add(mapCategory);
					}
					
					data.add(map);
				}
				reader.close();
				inStream.close();
				
				// update only if sku column is present
				if (!data.isEmpty() && data.get(0).containsKey("sku")) {
					csvFeedList.add(new CsvFeed("tab_1"));	// overview
					csvFeedList.add(new CsvFeed("tab_2"));	// technical specifications
					csvFeedList.add(new CsvFeed("keywords")); // keywords
					this.webJaguar.updateProduct(csvFeedList, data, null);
					
					// associate to categories		
					if (categoryData.size() > 0) {
						this.webJaguar.nonTransactionSafeInsertCategories(categoryData);						
					}
				}

				if (categories.size() > 0) {
					this.webJaguar.nonTransactionSafeInsertDsiCategories(categories);
				}
				
				// make items free shipping
				if (data.size() > 0 && data.get(0).containsKey("weight") && siteConfig.get("FREESHIPPING_OPTIONCODE").getValue().length() > 0) {
					this.webJaguar.nonTransactionSafeUpdateFreeShipping(siteConfig.get("FREESHIPPING_OPTIONCODE").getValue(), siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue());
				}
	        }
			
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			notifyAdmin("DSI updateCatalog() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}	

	private String formatTabContent(String content) {
		boolean withHeaders = false;
		if (content.contains("::")) {
			StringBuffer sbuff = new StringBuffer("");
			String currentAttr = "";
			String attr[] = content.split("::");
			sbuff.append("<table class='tabAttr'>");
			for (int i=0; i<attr.length; i++) {
				String inner[] = attr[i].split("--");
				if (inner.length > 2 && !withHeaders) withHeaders = true;
				if (inner.length > 0 && !currentAttr.equals(inner[0]) && withHeaders) {
					currentAttr = inner[0];
					sbuff.append("<tr>");
					sbuff.append("<td colspan='2' class='tabAttr0'>");
					sbuff.append(currentAttr);
					sbuff.append("</td>");		
					sbuff.append("</tr>");
				}
				sbuff.append("<tr>");
				int start = 0, style = 1;
				if (withHeaders) start = 1;
				for (int y=start; y<inner.length; y++) {
					sbuff.append("<td class='tabAttr" + style++ + "'>");
					if (y == start) {
						sbuff.append(inner[y] + ":");											
					} else {
						sbuff.append(inner[y].replaceAll("[\n]", "<br>")); // by line breaks				
						// sbuff.append(inner[y].replaceAll("[,\n]", "<br>")); // by commas and line breaks				
					}
					sbuff.append("</td>");	
				}
				sbuff.append("</tr>");
			}
			sbuff.append("</table>");
			return sbuff.toString();
		} else {
			return content.replace("\n", "<br>");			
		}
	}
	
	public void updateImage() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		int skuCol = -1;
		int catCol = -1;
		int fullPathCol = -1;
		int fileNameCol = -1;
		
		// DSI configuration
		String[] config = siteConfig.get("DSI").getValue().split(",");
		
		if (!(config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if dsi feature is disabled 
			return;
		}
		
		Map <Long, List<String>> data = new HashMap<Long, List<String>>();
		Map<String, Integer> skuMap = new HashMap<String, Integer>();
		List<CsvFeed> csvFeedList = this.webJaguar.getCsvFeed("dsi-manuals");
		
		Map<Long, Map <String, Object>> manualsDataMap = new HashMap<Long, Map <String, Object>>();
		
    	String allowedImages = "Jack-Pack,Large,Left,Life-Style,Rear,Right,Top,Tour,"; 
    	Map<String, String> allowedManuals = new HashMap<String, String>();
    	for (CsvFeed csvFeed: csvFeedList) {
    		allowedManuals.put(csvFeed.getHeaderName(), csvFeed.getColumnName());
    	}
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login("images", "1m@93S");
	        ftp.enterLocalPassiveMode();
	        	        
	        InputStream inStream = ftp.retrieveFileStream("Image_Data.csv");

	        if (inStream != null) {
	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream));
		        
	        	String[] headers = reader.readNext();
	        	for (int i=0; i<headers.length; i++) {
	        		if (headers[i].equalsIgnoreCase("VendorSKU")) {
	        			skuCol = i;
	        		} else if (headers[i].equalsIgnoreCase("Category")) {
	        			catCol = i;
	        		} else if (headers[i].equalsIgnoreCase("FullPath")) {
	        			fullPathCol = i;
	        		} else if (headers[i].equalsIgnoreCase("FileName")) {
	        			fileNameCol = i;
	        		}
				}
	        	
	        	if (skuCol > -1 && catCol > -1 && fullPathCol > -1 && fileNameCol > 1) {
					String[] nextLine;
					while ((nextLine = reader.readNext()) != null) {	
						String sku = "DSI-" + nextLine[skuCol].trim();
						if (!skuMap.containsKey(sku.toLowerCase())) {
							skuMap.put(sku.toLowerCase(), this.webJaguar.getProductIdBySku(sku));
						}
						if (skuMap.get(sku.toLowerCase()) != null) {
							// valid item
							Long productId = skuMap.get(sku.toLowerCase()).longValue();
							
							if (nextLine[fileNameCol].toLowerCase().endsWith(".jpg") && allowedImages.contains(nextLine[catCol])) {
								if (data.containsKey(productId)) {
									if (nextLine[catCol].equalsIgnoreCase("Large")) {
										data.get(productId).add(0, nextLine[fullPathCol]);
									} else {
										data.get(productId).add(nextLine[fullPathCol]);								
									}							
								} else {
									List<String> images = new ArrayList<String>();
									images.add(nextLine[fullPathCol]);
									data.put(productId, images);
								}							
							}
							
							if (nextLine[fileNameCol].toLowerCase().endsWith(".pdf") && allowedManuals.containsKey(nextLine[catCol].toLowerCase())) {
								if (manualsDataMap.containsKey(productId)) {
									manualsDataMap.get(productId).put(allowedManuals.get(nextLine[catCol].toLowerCase()), 
											"<a href=\"" + nextLine[fullPathCol] + "\">" + nextLine[fileNameCol] + "</a>");					
								} else {
									HashMap<String, Object> map = new HashMap<String, Object>();
									map.put("sku", sku);
									map.put(allowedManuals.get(nextLine[catCol].toLowerCase()), 
											"<a href=\"" + nextLine[fullPathCol] + "\">" + nextLine[fileNameCol] + "</a>");	
									manualsDataMap.put(productId, map);
								}							
							}						
						}
					}	        		
	        	}
				reader.close();
				inStream.close();
				
				// update only if data is not empty
				if (!data.isEmpty()) {
					this.webJaguar.updateProductImage(data);
				}
				
				if (!manualsDataMap.isEmpty()) {
					List<Map <String, Object>> manualsData = new ArrayList<Map <String, Object>>();
					for (Map<String, Object> map: manualsDataMap.values()) {
						manualsData.add(map);
					}
					this.webJaguar.updateProduct(csvFeedList, manualsData, null);
				}
	        }
			
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			notifyAdmin("DSI updateImage() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}	
	}
	
	public void exportInvoices() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// DSI configuration
		String[] config = siteConfig.get("DSI").getValue().split(",");
		
		if (!(config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if dsi feature is disabled 
			return;
		}
		
		File dsiFolder = new File(getServletContext().getRealPath("/dsi"));
		if (!dsiFolder.exists()) {
			dsiFolder.mkdir();
		}
		File dsiSentFolder = new File(dsiFolder, "sent");
		if (!dsiSentFolder.exists()) {
			dsiSentFolder.mkdir();
		}
		
		OrderSearch search = new OrderSearch();
		search.setDsi(false);
		Date cutoff = new Date();
		search.setEndDate(cutoff);
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat prefixFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
		DecimalFormat df = new DecimalFormat("#0");
		df.setMinimumIntegerDigits(6);
		
		List<Order> orderList = this.webJaguar.getInvoiceExportList(search, null);
		if (orderList.isEmpty()) {
			return;
		}
		
		Map<String, String> dsiShipping = new HashMap<String, String>();
		dsiShipping.put("UPS Ground", "UPS*");
		dsiShipping.put("UPS 3 Day Select", "UPS*3DAY");
		dsiShipping.put("UPS 2nd Day Air", "UPS*BLUE");
		dsiShipping.put("UPS Next Day Air", "UPS*RED");
		dsiShipping.put("UPS Next Day Air Saver", "UPS*REDSVR");
		// dsiShipping.put("", "UPS*SAT");
		dsiShipping.put("Curbside", "AIT*CURB");
		//dsiShipping.put("", "AIT*CURB-LATE");
		//dsiShipping.put("", "AIT*CURB-SAT");
		//dsiShipping.put("", "AIT*INSIDE");
		//dsiShipping.put("", "AIT*INSIDE-LATE");
		//dsiShipping.put("", "AIT*INSIDE-SAT");
		dsiShipping.put("White Glove", "AIT*WHTEGLOV");
		//dsiShipping.put("", "AIT*WHTEGLOV-LATE");
		//dsiShipping.put("", "AIT*WHTEGLOV-SAT");
		
		try {
			File invoiceFile = new File(dsiFolder, "dsiInvoice.csv");
			CSVWriter writer = new CSVWriter(new FileWriter(invoiceFile), ',', CSVWriter.NO_QUOTE_CHARACTER);

			for (Order order: orderList) {
				List<String> line = new ArrayList<String>();
				line.add(order.getOrderId().toString());
				line.add("");
				
				// shipping method
				String shippingMethod = order.getShippingMethod();
				if (order.getCustomShippingTitle() != null && order.getCustomShippingTitle().length() > 0) {
					shippingMethod = order.getCustomShippingTitle();
				}
				if (dsiShipping.containsKey(shippingMethod)) {
					shippingMethod = dsiShipping.get(shippingMethod);
				}
				line.add(shippingMethod); 
				
				order.getShipping().replace(",", " ");
				
		    	line.add(order.getShipping().getFirstName());
				line.add(order.getShipping().getLastName());
				line.add("");
				line.add(order.getShipping().getAddr1());
				line.add(order.getShipping().getAddr2());
				line.add(order.getShipping().getCity());
				line.add(order.getShipping().getStateProvince());
				line.add(order.getShipping().getZip());
				line.add(order.getShipping().getAddr1());
				line.add(order.getShipping().getAddr2());
				line.add(order.getShipping().getCity());
				line.add(order.getShipping().getStateProvince());
				line.add(order.getShipping().getZip());
				line.add(numericOnly(order.getShipping().getPhone())); // phone number 1
				line.add(""); // phone number 2
				line.add(""); // phone number 3
				line.add(order.getUserEmail());
				line.add(""); // pending account number
				line.add(dateFormatter.format(order.getDateOrdered()));
				line.add(""); // memo 1
				line.add(""); // memo 2
				line.add(""); // memo 3
				// line items
	    		for (LineItem lineItem : order.getLineItems()) {
	    			String sku = lineItem.getProduct().getSku();
	    			if (lineItem.getProduct().getFeed() == null || !lineItem.getProduct().getFeed().equalsIgnoreCase("DSI")) {
	    				// send only DSI items
	    				continue;
	    			}
	    			
	    			if (sku.toLowerCase().startsWith("dsi-")) {
	    				sku = sku.substring(4); // remove leading DSI-
	    			}
	    			line.add(sku.replace(",", " "));
	    			
					// quantity
					int totalQty = lineItem.getQuantity();
					if (lineItem.getProduct().getCaseContent() != null) {
						totalQty = totalQty * lineItem.getProduct().getCaseContent();
					}	    			
	    			line.add(String.valueOf(totalQty));
	    		}
				String[] lines = new String[line.size()];
				writer.writeNext(line.toArray(lines));
			}
			writer.close();
			
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login("orders", "0Rd3r$");
	        ftp.enterLocalPassiveMode();
	        
	        FileInputStream fis = new FileInputStream(invoiceFile);
	        if (ftp.storeFile("PROD/Order" + config[3] + ".csv2", fis)) {
	        	this.webJaguar.updateOrderExported("dsi", cutoff, null);
	        	copy(invoiceFile, new File(dsiSentFolder, prefixFormatter.format(new Date()) + "dsiInvoice.csv"));
	        	invoiceFile.delete();
	        } else {
	        	notifyAdmin("DSI exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
	        }
	        fis.close();

	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
			
		} catch (Exception e) {
			notifyAdmin("DSI exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}	

	public void updateInvoices() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// DSI configuration
		String[] config = siteConfig.get("DSI").getValue().split(",");
		
		if (!(config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if dsi feature is disabled 
			return;
		}
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		File dsiFolder = new File(getServletContext().getRealPath("/dsi"));
		if (!dsiFolder.exists()) {
			dsiFolder.mkdir();
		}
		File dsiProcessedFolder = new File(dsiFolder, "processed");
		if (!dsiProcessedFolder.exists()) {
			dsiProcessedFolder.mkdir();
		}
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	        	        
	        if (ftp.changeWorkingDirectory("EDR")) {
		        String[] fileNames = ftp.listNames();
		        for (String fileName: fileNames) {
		        	if (fileName.startsWith("EDR") && fileName.endsWith(".csv")) {
			        	File file = new File(dsiProcessedFolder, fileName);
			        	
				        if (ftp.retrieveFile(fileName, new FileOutputStream(file))) {
				        	CSVReader reader = new CSVReader(new FileReader(file));

				        	// get headers
				        	Map<String, Integer> headerMap = new HashMap<String, Integer>();			        	
			        		String[] headers = reader.readNext();
				        	for (int i=0; i<headers.length; i++) {
				        		headerMap.put(headers[i], i);
							}

				        	// get orders
				        	List<Order> orders = new ArrayList<Order>();
				        	String[] nextLine;
							while ((nextLine = reader.readNext()) != null) {
					        	Order order = new Order();
					        	order.setOrderId(Integer.parseInt(nextLine[headerMap.get("DealerOrderNumber")]));
					        	order.setShipped(df.parse(nextLine[headerMap.get("ShippedDate")]));
					        	order.setShippingMethod(nextLine[headerMap.get("ShippedBy")]);
								// get line items
					        	String sku;
					        	for (int lineNum=1; lineNum<=12; lineNum++) {
					        		if ((sku = nextLine[headerMap.get("ItemSKU" + lineNum)]).length() > 0) {
						        		LineItem lineItem = new LineItem();
					        			lineItem.setLineNumber(lineNum);
					        			lineItem.setOrderId(order.getOrderId());
					        			lineItem.setProduct(new Product(sku, null));
					        			lineItem.setTrackNum(nextLine[headerMap.get("ItemTracking" + lineNum)]);
					        			lineItem.setDateShipped(order.getShipped());
					        			lineItem.setShippingMethod(order.getShippingMethod());
					        			lineItem.getProduct().setFeed("DSI");
					        			order.addLineItem(lineItem);
					        		} else {
					        			break;
					        		}
					        	}
					        	orders.add(order);
							}
							
							reader.close();
							
							// update status and line item tracking numbers
							for (Order order: orders) {
								Order existingOrder = this.webJaguar.getOrder(order.getOrderId(), null);
								
								if (existingOrder != null) {
									// valid order
									
									OrderStatus orderStatus = new OrderStatus();
									orderStatus.setOrderId(existingOrder.getOrderId());
									orderStatus.setStatus("pr"); /// processing
									this.webJaguar.updateDsiOrder(order, orderStatus);
								}
							}
							
							// delete file
							ftp.deleteFile(fileName);
				        }		        		
		        	}
		        }
	        }
	        
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			notifyAdmin("DSI updateInvoices() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}	
	
    private String numericOnly(String string) {
    	StringBuffer sbuff = new StringBuffer();
    	
    	for (int i=0; i<string.length(); i++) {
    		try {
    			Integer.parseInt(string.charAt(i) + "");
    			sbuff.append(string.charAt(i));
    		} catch (Exception e) {
    			// do nothing
    		}
    	}
    	
    	return sbuff.toString();
    }
    	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
	
	private boolean notifyCustomer(OrderStatus orderStatus, Map<String, Configuration> siteConfig, Order order, MultiStore multiStore) {

		Customer customer = this.webJaguar.getCustomerById(order.getUserId());
		
		if (!customer.isEmailNotify()) {
			// do not send email
			return false;
		}
		
		// send email
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

    	StringBuffer messageBuffer = new StringBuffer();
    	messageBuffer.append( dateFormatter.format(new Date()) + "\n\n" );
    	
    	String firstName = order.getBilling().getFirstName();  
    	if (firstName == null) {
    		firstName = "";
    	}
    	String lastname = order.getBilling().getLastName();
    	if (lastname == null) {
    		lastname = "";
    	}
    	String tracknum = orderStatus.getTrackNum();
    	if (tracknum == null) {
    		tracknum = "";
    	}
    	String email = customer.getUsername();
    	if (email == null) {
    		email = "";
    	}
    
    	// subject
    	orderStatus.setSubject( orderStatus.getSubject().replace( "#firstname#", firstName ) );
    	orderStatus.setSubject( orderStatus.getSubject().replace( "#lastname#", lastname ) );
    	orderStatus.setSubject( orderStatus.getSubject().replace( "#email#", email ) );
    	orderStatus.setSubject( orderStatus.getSubject().replace( "#order#", order.getOrderId().toString() ) );
    	orderStatus.setSubject( orderStatus.getSubject().replace( "#status#", getMessageSourceAccessor().getMessage( "orderStatus_" + orderStatus.getStatus() ) ) );    	    	
    	orderStatus.setSubject( orderStatus.getSubject().replace( "#tracknum#", tracknum ) );
    	// XXX would be replaced by OrderID-UserID Base64-encoded.
    	String orig = order.getOrderId() + "-" + order.getUserId();
    	byte[] encode_orig = Base64.encodeBase64(orig.getBytes());         
    	orig = new String(encode_orig);
    	if(order.getShipping().getStateProvince().equals("CA")) {
    		//If user is in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&walkin=true
    		//If user is not in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&other=true
    		orderStatus.setSubject( orderStatus.getSubject().replace( "#orderlink#", secureUrl + "dv/inv/invoice.php?order_id="+orig+"=&walkin=true" ) );    	
    	} else {
    		orderStatus.setSubject( orderStatus.getSubject().replace( "#orderlink#", secureUrl + "dv/inv/invoice.php?order_id="+orig+"=&other=true" ) );    	
    	}
    	
    	// message
    	orderStatus.setMessage( orderStatus.getMessage().replace( "#firstname#", firstName ) );
    	orderStatus.setMessage( orderStatus.getMessage().replace( "#lastname#", lastname ) );
    	orderStatus.setMessage( orderStatus.getMessage().replace( "#email#", email ) );
    	orderStatus.setMessage( orderStatus.getMessage().replace( "#order#", order.getOrderId().toString() ) );
    	orderStatus.setMessage( orderStatus.getMessage().replace( "#status#", getMessageSourceAccessor().getMessage( "orderStatus_" + orderStatus.getStatus() ) ) );    	    	
    	orderStatus.setMessage( orderStatus.getMessage().replace( "#tracknum#", tracknum ) );
    	if(order.getShipping().getStateProvince().equals("CA")) {
    		//If user is in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&walkin=true
    		//If user is not in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&other=true
    		orderStatus.setMessage( orderStatus.getMessage().replace( "#orderlink#", secureUrl + "dv/inv/invoice.php?order_id="+orig+"=&walkin=true") );   	
    	} else {
    		orderStatus.setMessage( orderStatus.getMessage().replace( "#orderlink#", secureUrl + "dv/inv/invoice.php?order_id="+orig+"=&other=true") );    	
    	}
    	messageBuffer.append( orderStatus.getMessage() );
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(customer.getUsername());
		msg.setFrom( contactEmail );
		msg.setBcc( contactEmail );
		msg.setSubject( orderStatus.getSubject() );
		msg.setText( messageBuffer.toString() );
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
		return true;
	}	
	
	// Copies src file to dst file.
	// If the dst file does not exist, it is created
	private void copy(File src, File dst) throws IOException
	{
		InputStream in = new FileInputStream( src );
		OutputStream out = new FileOutputStream( dst );

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ( (len = in.read( buf )) > 0 )
		{
			out.write( buf, 0, len );
		}
		in.close();
		out.close();
	}
}
