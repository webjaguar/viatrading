/* Copyright 2011 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 07.15.2011
 */

package com.webjaguar.web.quartz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.SalesRep;


public class CdsAndDvdsJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	private GlobalDao globalDao;	
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }

	private String feed = "CdsAndDvds";
	private static File productFile;
	private static Map<String, Configuration> siteConfig;
	List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
	
	public void downloadProducts() throws Exception {
		
		//As of now it is manual update. In future we can enable this code to update automatic
		/*siteConfig = webJaguar.getSiteConfig();
		// CdsAndDvds configuration
		String[] config = siteConfig.get("CDS_AND_DVDS").getValue().split(",");
		
		System.out.println("Downloading Started");
    	// get generated data feed file
        File baseFile = new File(getServletContext().getRealPath("/admin/feed/"));
        if (!baseFile.exists()) {
        	baseFile.mkdir();
        }
        System.out.println("baseFile exist "+baseFile);
    	
    	try {
    		String fileName = config[3].trim();
    	    productFile = new File(baseFile, fileName);
    	   // productFile = new File("/Users/shahin/Desktop/ProductFeed.txt");
    		System.out.println("productFile exist "+productFile);
        	StringBuffer sbuff = new StringBuffer();
    	   	if (config[0].trim().equals( "" )) {
    	   		throw new UnknownHostException();
    	   	}
    	   	if (config[3].trim().equals( "" )) {
    	   		throw new Exception();
    	   	}
    	   	
    	   	// Connect and logon to FTP Server
    	    FTPClient ftp = new FTPClient();
    	    ftp.connect( config[0].trim() );
    	        
    	    // login
    	    ftp.login( config[1].trim(), config[2].trim() );
    	   
    	    if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
    	    	 System.out.println("Downloading File ");
    	        	// download file
    	        FileOutputStream fos = new FileOutputStream(productFile);
    	      
    	        ftp.retrieveFile(config[3].trim(), fos);
    	        sbuff.append("Downloading file: " + config[3].trim() + "Compeleted.");
    	        fos.close();        	
    	    }
    	        
    	    // Logout from the FTP Server and disconnect
    	    ftp.logout();
    	    ftp.disconnect(); 
    	    Long startTime = new Date().getTime();
    	    parseProductTextFile();
    	    System.out.println("Time to complete all products "+(new Date().getTime() - startTime));
    	} catch (Exception e) {
    		notifyAdmin("CdsBooksDvds Datafeed downloadProducts() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
    		e.printStackTrace();
    	}*/
		
    }
	
	public void updateInventory() {
		siteConfig = webJaguar.getSiteConfig();
		// CdsAndDvds configuration
		String[] config = siteConfig.get("CDS_AND_DVDS").getValue().split(",");
		
		System.out.println("Downloading Started");
    	// get generated data feed file
        File baseFile = new File(getServletContext().getRealPath("/admin/feed/"));
        if (!baseFile.exists()) {
        	baseFile.mkdir();
        }
        System.out.println("baseFile exist "+baseFile);
    	
    	try {
    		if (config[0].trim().equals( "" )) {
    	   		throw new UnknownHostException();
    	   	}
    	   	if (config[4].trim().equals( "" )) {
    	   		throw new Exception();
    	   	}
    	   	String fileName = config[4].trim();
    	    productFile = new File(baseFile, fileName);
    	   // productFile = new File("/Users/shahin/Download/Inventory.txt");
    		System.out.println("productFile exist "+productFile);
        	StringBuffer sbuff = new StringBuffer();
    	   	
    	   	
    	   	// Connect and logon to FTP Server
    	   FTPClient ftp = new FTPClient();
    	    ftp.connect( config[0].trim() );
    	        
    	    // login
    	    ftp.login( config[1].trim(), config[2].trim() );
    	   
    	    if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
    	    	 System.out.println("Downloading File ");
    	        	// download file
    	        FileOutputStream fos = new FileOutputStream(productFile);
    	        ftp.retrieveFile(config[4].trim(), fos);
    	        sbuff.append("Downloading file: " + config[4].trim() + "Compeleted.");
    	        fos.close();        	
    	    }
    	        
    	    // Logout from the FTP Server and disconnect
    	    ftp.logout();
    	    ftp.disconnect();
    	    Long startTime = new Date().getTime();
    	    parseInventoryTextFile();
    	    System.out.println("Time to complete all products "+(new Date().getTime() - startTime));
    	} catch (Exception e) {
    		notifyAdmin("CdsBooksDvds Datafeed downloadProducts() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
    		e.printStackTrace();
    	}
	}
	
	public void parseProductTextFile() {
		try {
			
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
			StringBuffer sbuff = new StringBuffer();
			Date start = new Date();
			
			HashMap<String, Object> map = null;
 			BufferedReader bReader = new BufferedReader( new FileReader(productFile));
 			String line;

 		    while ((line = bReader.readLine()) != null) {
 	    		/**
 			    * Splitting the content of tabbed separated line
 			    */
 			    String datavalue[] = line.split("\t");
 			   
 			    if(datavalue[0] != null && !datavalue[0].isEmpty()) {
 			    	map = new HashMap<String, Object>();
 				
 					try {
 						map.put("sku", datavalue[0]);
 					} catch (Exception e) { map.put("sku", null); }
 					try {
 						map.put("name", datavalue[1] );
 					} catch (Exception e) { map.put("name", null); }
 					try {
 						map.put("active", Integer.parseInt( datavalue[2] ));
 					} catch (Exception e) { map.put("active", 1); }
 					try {
 						map.put("long_desc", datavalue[3] );
 					} catch (Exception e) { map.put("long_desc", null); }
 					try {
 						map.put("image1", datavalue[4] );
 					} catch (Exception e) { map.put("image1", null); }
 					try {
 						map.put("weight", Double.parseDouble( datavalue[5] ));
 					} catch (Exception e) { map.put("weight", 0.0); }
 					try {
 						map.put("msrp", Double.parseDouble( datavalue[6] ));
 					} catch (Exception e) { map.put("msrp", null); }
 					try {
 						map.put("price_1", Double.parseDouble( datavalue[7] ));
 					} catch (Exception e) { map.put("price_1", null); }
 					try {
 						map.put("taxable", Integer.parseInt( datavalue[8] ));
 					} catch (Exception e) { map.put("taxable", 1); }
 					try {
 						map.put("searchable", Integer.parseInt( datavalue[9] ));
 					} catch (Exception e) { map.put("searchable", 1); }
 					try {
 						map.put("inventory", Integer.parseInt(datavalue[10]));						
 					} catch (Exception e) {
 						map.put("inventory", null);
 					}
 					try {
 						map.put("inventory_afs", Integer.parseInt(datavalue[11]));						
 					} catch (Exception e) {
 						map.put("inventory_afs", null);
 					}
 					try {
 						map.put("neg_inventory", Integer.parseInt(datavalue[12]));						
 					} catch (Exception e) {
 						map.put("neg_inventory", 0);
 					}
 					try {
 						map.put("show_neg_inventory", Integer.parseInt(datavalue[13]));						
 					} catch (Exception e) {
 						map.put("show_neg_inventory", 0);
 					}
 					try {
 						map.put("categoryId", Integer.parseInt(datavalue[14]));						
 					} catch (Exception e) {
 						map.put("categoryId", null);
 					}
 					try {
 						map.put("field_1", Double.parseDouble(datavalue[15]));						
 						map.put("field_2", 0);						
 	 				} catch (Exception e) {
 	 					map.put("field_1", null);						
 						map.put("field_2", null);						
 	 				}
 					map.put("feed", feed);				
					map.put("feed_new", true);
					
 					data.add(map);
 					if (data.size() == 3000) {
 						// products
 						Long startTime = new Date().getTime();
 	 					updateProduct(data, true, true);
 						data.clear();
 						map.clear();
 						System.out.println("Time to update 3000 "+(new Date().getTime() - startTime));
 		 			}
 				 }
 			  }
 			
 			// update
 	    	if (!data.isEmpty()) {
	 	    	updateProduct(data, true, true);
				data.clear();
				map.clear();
			}
 			
 			Date end = new Date();
 			sbuff.append("Started : " + dateFormatter.format(start) + "\n");
 			sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
 			sbuff.append("Downloaded Products." + "\n\n");
 			notifyAdmin("Successful downloadProducts() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());


 		} catch (Exception e) {System.out.println("error 1 ");
			notifyAdmin("CdsAndDvds Datafeed downloadProducts() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
	    }
	}
	
	public void parseInventoryTextFile() {
		try {
			
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
			StringBuffer sbuff = new StringBuffer();
			Date start = new Date();
			
			HashMap<String, Object> map = null;
 			BufferedReader bReader = new BufferedReader( new FileReader(productFile));
 			String line;

 		    while ((line = bReader.readLine()) != null) {
 	    		/**
 			    * Splitting the content of tabbed separated line
 			    */
 			    String datavalue[] = line.split("\t");
 			   
 			    if(datavalue[0] != null && !datavalue[0].isEmpty()) {
 			    	map = new HashMap<String, Object>();
 				
 					try {
 						map.put("sku", datavalue[0]);
 					} catch (Exception e) { map.put("sku", null); }
 					try {
 						map.put("inventory", Integer.parseInt(datavalue[1]));						
 					} catch (Exception e) {
 						map.put("inventory", null);
 					}
 					try {
 						map.put("inventory_afs", Integer.parseInt(datavalue[1]));						
 					} catch (Exception e) {
 						map.put("inventory_afs", null);
 					}
 					try {
 						map.put("price_1", Double.parseDouble( datavalue[2] ));
 					} catch (Exception e) { map.put("price_1", null); }
 					try {
 						map.put("field_1", Double.parseDouble(datavalue[3]));						
 						map.put("field_2", 0);						
 	 				} catch (Exception e) {
 	 					map.put("field_1", null);						
 						map.put("field_2", null);						
 	 				}
 					map.put("feed", feed);				
					map.put("feed_new", true);
					
 					data.add(map);
 					if (data.size() == 3000) {
 						// products
 						Long startTime = new Date().getTime();
 	 					updateProduct(data, false, false);
 						data.clear();
 						map.clear();
 						System.out.println("Time to update 3000 "+(new Date().getTime() - startTime));
 		 			}
 				 }
 			  }
 			
 			// update
 	    	if (!data.isEmpty()) {
	 	    	updateProduct(data, false, false);
				data.clear();
				map.clear();
			}
 	    	
 			Date end = new Date();
 			sbuff.append("Started : " + dateFormatter.format(start) + "\n");
 			sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
 			sbuff.append("Imported Inventory." + "\n\n");
 			notifyAdmin("parseInventoryTextFile() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());

 			
 		} catch (Exception e) {System.out.println("error 1 ");
			notifyAdmin("CdsAndDvds Datafeed downloadProducts() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
	    }
	}
	
	private void updateProduct(List<Map <String, Object>> data, boolean productFeed, boolean insertNewProduct) {
    	// update Products
		webJaguar.nonTransactionSafeUpdateCdsDvdsProduct(getCsvFeedList(productFeed), data, insertNewProduct);
	}
    
    private List<CsvFeed> getCsvFeedList(boolean productFeed) {
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		
		csvFeedList.add(new CsvFeed("sku"));
		csvFeedList.add(new CsvFeed("price_1"));
		csvFeedList.add(new CsvFeed("inventory"));
		csvFeedList.add(new CsvFeed("inventory_afs"));
		csvFeedList.add(new CsvFeed("field_1"));
		csvFeedList.add(new CsvFeed("field_2"));
		csvFeedList.add(new CsvFeed("feed"));
		csvFeedList.add(new CsvFeed("feed_new"));
		
		if(productFeed){
			csvFeedList.add(new CsvFeed("name"));
			csvFeedList.add(new CsvFeed("active"));
			csvFeedList.add(new CsvFeed("long_desc"));
			csvFeedList.add(new CsvFeed("weight"));
			csvFeedList.add(new CsvFeed("msrp"));
			csvFeedList.add(new CsvFeed("neg_inventory"));
			csvFeedList.add(new CsvFeed("show_neg_inventory"));
			csvFeedList.add(new CsvFeed("taxable"));
			csvFeedList.add(new CsvFeed("searchable"));
		}	
		return csvFeedList;
	}

    public void uploadInvoices() {
    	
    	Date start = new Date();
			
    	siteConfig = webJaguar.getSiteConfig();
		Map<String, Object> gSiteConfig = globalDao.getGlobalSiteConfig();
		// CdsAndDvds configuration
		String[] config = siteConfig.get("CDS_AND_DVDS").getValue().split(",");
		File invoiceFile = null;
		try {
			if (config[5].trim().equals( "" )) {
    	   		throw new UnknownHostException();
    	   	}
    		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    		String fileName = config[8].trim()+ dateFormatter.format(new Date()) + ".xml";
    	    
    		invoiceFile = new File(getServletContext().getRealPath("/"), fileName);
    	    StringBuffer sbuff = new StringBuffer();
    	    sbuff.append("Started : " + dateFormatter.format(start) + "\n");
 			PrintWriter pw = new PrintWriter(new FileWriter(invoiceFile));
			
    	    OrderSearch search = new OrderSearch();
    	    //  old behavior to export order only once
    	    //search.setExportSuccess(false);
    	    
    	    // new behavior : export all orders that are in pending or processing status
    	    search.setStatus("ppr");
    	    
    	    Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
    		if ((Boolean) gSiteConfig.get("gSALES_REP")) {
    			// get sales rep
    			Iterator<SalesRep> iter = webJaguar.getSalesRepList().iterator();
    			while (iter.hasNext()) {
    				SalesRep salesRep = iter.next();
    				salesRepMap.put(salesRep.getId(), salesRep);
    			}
    		}
    		
    	    List<Order> ordersList = webJaguar.getInvoiceExportList(search,siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());
    	    System.out.println("Size "+ordersList.size());
    	    if(ordersList.size() > 0) {
    	    	Set<Integer> orderIds = new HashSet<Integer>();
    	    	// product fields
    			List<ProductField> productFields = null;
    			if( siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue() != null && siteConfig.get("PRODUCT_FIELDS_ON_INVOICE_EXPORT").getValue().equals("true") ) {
    				productFields = this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
    				Iterator pfIter = productFields.iterator();
    				while ( pfIter.hasNext() )
    				{
    					ProductField productField = (ProductField) pfIter.next();
    					if ( !productField.isEnabled() || !productField.isShowOnInvoiceExport())
    					{
    						pfIter.remove();
    					}
    				}
    			}
    			//create xml file
        	    webJaguar.exportXMLInvoices(ordersList, salesRepMap, productFields, pw, orderIds, true);
        	    
        	    JSch jsch = new JSch();
        	    java.util.Properties prop = new java.util.Properties(); 
        	    prop.put("StrictHostKeyChecking", "no");
        	    
        	    Session session = jsch.getSession(config[6].trim(), config[5].trim());
	        	session.setConfig(prop);
	        	session.setPassword(config[7].trim());
	        	session.connect();
	        	
	        	Channel channel = session.openChannel("sftp");
	        	channel.connect();
	        	ChannelSftp sftpChannel = (ChannelSftp) channel;
	        	FileInputStream fis = new FileInputStream(invoiceFile);
	        	try {
	        		sftpChannel.cd("Invoice");
		        }catch(Exception e){
		        	sftpChannel.mkdir("Invoice");
	        		sftpChannel.cd("Invoice");
	        	}
	        	sftpChannel.put(fis, invoiceFile.getName());
			    
			    fis.close();
			    sftpChannel.exit();
			    sftpChannel.disconnect();
			    session.disconnect();
			    // no need to update order as exported in new behavior
			    //  webJaguar.updateInvoiceExported(orderIds);
			    sbuff.append("No of orders : "+ordersList.size()+"\n");
			    sbuff.append("Successfully Uploaded Invoices." + "\n\n");
		 	} else {
        		sbuff.append("No of orders : 0 \n");
       	    }
    	    Date end = new Date();
 			sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
 			notifyAdmin(" uploadInvoices() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
		} catch(Exception e) {
	   		notifyAdmin("CdsBooksDvds Invoice Uploading on " + siteConfig.get("SITE_URL").getValue(), e.toString());
    		e.printStackTrace();
    	}
		
		if(invoiceFile.exists()){
			invoiceFile.delete();
		}
 	}
    
    public void downloadTrackingNumber() {
    	
    	Date start = new Date();
		siteConfig = webJaguar.getSiteConfig();
		
		// CdsAndDvds configuration
		String[] config = siteConfig.get("CDS_AND_DVDS").getValue().split(",");
		try {
			if (config[5].trim().equals( "" )) {
    	   		throw new UnknownHostException();
    	   	}
    		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    		
    		StringBuffer sbuff = new StringBuffer();
    	    sbuff.append("Started : " + dateFormatter.format(start) + "\n");
 			
    	    
    	    JSch jsch = new JSch();
        	java.util.Properties prop = new java.util.Properties(); 
        	prop.put("StrictHostKeyChecking", "no");
        	
        	Session session = jsch.getSession(config[6].trim(), config[5].trim());
	        session.setConfig(prop);
	        session.setPassword(config[7].trim());
	        session.connect();
	        
	        Channel channel = session.openChannel("sftp");
	        channel.connect();
	        ChannelSftp sftpChannel = (ChannelSftp) channel;
	        
	        Vector<String> files = sftpChannel.ls("Tracking");
	        System.out.println("Downloading Tracking file Started");
	    	// get generated data feed file
	        File baseFile = new File(getServletContext().getRealPath("/admin/feed/"));
	        if (!baseFile.exists()) {
	        	baseFile.mkdir();
	        }
	        System.out.println("baseFile exist for tracking numbers "+baseFile);
	    	File tempFile; 
	        for(int i=0; i< files.size(); i++) {
	    		String[] fileDetails = files.get(i).split(" ");
	    		String fileName = fileDetails[fileDetails.length - 1] ;
	    		if(fileName.startsWith("Tracking") && fileName.endsWith("txt")){
	        		tempFile = new File(baseFile, fileName);
	            	sftpChannel.get("/Tracking/"+fileName, new FileOutputStream(tempFile));
	        		this.parseTrackingCodeFile(tempFile, sftpChannel, sbuff);
	        	}
	        }
	    	
	        sftpChannel.exit();
			sftpChannel.disconnect();
			session.disconnect();
			    
    	    Date end = new Date();
 			sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
 			notifyAdmin("uploadInvoices() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
		} catch(Exception e) {
			notifyAdmin("Exception in Invoice Uploading on " + siteConfig.get("SITE_URL").getValue(), e.toString());
    		e.printStackTrace();
 		}
	}
    
    public void parseTrackingCodeFile(File trackCodeFile, ChannelSftp sftpChannel, StringBuffer sbuff) {
		try {
			
			BufferedReader bReader = new BufferedReader( new FileReader(trackCodeFile));
 			String line;
 			OrderStatus orderStatus = new OrderStatus();
 			Map<String, Object> gSiteConfig = globalDao.getGlobalSiteConfig();
 			
 		    while ((line = bReader.readLine()) != null) {
 	    		/**
 			    * Splitting the content of tabbed separated line
 			    */
 			    String datavalue[] = line.split("\t");
 			    orderStatus = new OrderStatus();
 			    orderStatus.setStatus( "s" );
				
 			    try {
 			    	orderStatus.setOrderId( Integer.parseInt( datavalue[0] ) );
 	 			} catch (Exception e) { continue; }
 	 			try {
 			    	orderStatus.setTrackNum( datavalue[1] );
 	 			} catch (Exception e) { continue; }
 	 			
 	 			try {
 	 				orderStatus.setComments( "Shipped by -- "+datavalue[2] );
 	 			} catch (Exception e) { orderStatus.setComments( "" ); }
				
 			    orderStatus.setUsername("Tracking Update");
 			    this.webJaguar.insertOrderStatus( orderStatus );	
				
				if((Boolean) gSiteConfig.get("gINVENTORY")) {
					Order order = this.webJaguar.getOrder( orderStatus.getOrderId() , null);
					// Generates  packingList 
		    		if(order != null) {
						if ( !order.isProcessed() ) {  			
							this.webJaguar.generatePackingList(order, false, new Date(), siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"));
			    		}
			    		// adds ship date to packinglist without shipdate and adjusts on hand
			    		this.webJaguar.ensurePackingListShipDate(order, siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"), 0);
		    		}
				}
			    
 		    }
 		    try{
 		    	sftpChannel.cd("UpdatedTracking");
 		    	sftpChannel.cd("/");
 		    }catch(Exception e){
 		    	sftpChannel.mkdir("UpdatedTracking");
 		    	sbuff.append("Created new directory : UpdatedTracking \n\n");
 	 		}
 	 		sftpChannel.rename("/Tracking/"+trackCodeFile.getName(), "/UpdatedTracking/Updated_"+trackCodeFile.getName());
 		}catch(Exception e){
 			sbuff.append("Exception while parsing file : "+trackCodeFile.getName()+" \n\n");
 	 		e.printStackTrace();
		}   
 	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
		String contactEmail = "jwalant@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		//msg.setCc("developers@advancedemedia.com");
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
}