/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 04.27.2010
 */

package com.webjaguar.web.quartz;

import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.thirdparty.echosign.EchoSign;
import com.webjaguar.thirdparty.echosign.EchoSignApi;
import com.webjaguar.thirdparty.echosign.EchoSignSearch;
import com.webjaguar.thirdparty.echosign.EchoSignWidgetSearch;
import com.webjaguar.thirdparty.echosign.Widget;

import echosign.api.dto.DocumentHistoryEvent;
import echosign.api.dto.DocumentInfo;
import echosign.api.dto8.GetFormDataResult;

public class EchoSignJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void getDocumentInfo() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		System.out.println("echo start " + new Date());
		
		if (!((Boolean) gSiteConfig.get("gSITE_ACTIVE")
				&& siteConfig.get("ECHOSIGN_API_KEY").getValue().trim().length() > 0
				)) {
			// return if EchoSign is disabled 
			return;
		}
		
		// get widget documents
		try {
			for (Widget widget : this.webJaguar.getEchoSignWidgetBySearch(new EchoSignWidgetSearch())) {
				EchoSignApi api = new EchoSignApi();
				GetFormDataResult result = api.getFormData(widget.getApiKey(), widget.getDocumentKey());
				if (result.isSuccess()) {
					CSVReader reader = new CSVReader(new StringReader(result.getFormDataCsv()));
					String[] headers = reader.readNext();
					Map<String, Integer> headerMap = new HashMap<String, Integer>();
					for (int i=0; i<headers.length; i++) {
						headerMap.put(headers[i], i);
					}
					EchoSign echoSign = new EchoSign();
					echoSign.setApiKey(widget.getApiKey());
					System.out.println("echo apiKey " + widget.getApiKey());
					echoSign.setUserKey(widget.getUserKey());
					echoSign.setWidgetDocumentKey(widget.getDocumentKey());
					echoSign.setDocumentName(widget.getDocumentName());
					echoSign.setFileName(widget.getFileName());
					echoSign.setDateSent(widget.getCreated());
					String[] nextLine;
					while ((nextLine = reader.readNext()) != null) {
						echoSign.setDocumentKey(nextLine[headerMap.get("Document Key")]);		
						echoSign.setRecipient(nextLine[headerMap.get("email")]);
						this.webJaguar.nonTransactionSafeInsertEchoSign(echoSign, true);
					}
				}
			}
		} catch (Exception e) {
			notifyAdmin("EchoSign getDocumentInfo() -- widget on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
		
		EchoSignSearch search = new EchoSignSearch();
		search.setAgreementStatus("notSIGNEDorABORTED");
		search.setSort("date_sent");
		
		GregorianCalendar now = new GregorianCalendar();
		if (now.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY && now.get(Calendar.HOUR_OF_DAY) == 24) {
			// only check documents sent a month ago
			search.setExtraSQL("(date_sent > DATE_SUB(now(), INTERVAL 1 MONTH) or widget_document_key is not null)");
		} else {
			// only check documents sent a week ago
			search.setExtraSQL("(date_sent > DATE_SUB(now(), INTERVAL 1 WEEK) or widget_document_key is not null)");
		}

		List<EchoSign> list = this.webJaguar.getEchoSignBySearch(search);
		if (list.isEmpty()) {
			return;
		}
		
		try {
			System.out.println("echo list.size() " + list.size());
			for (EchoSign echoSign: list) {
		    	EchoSignApi echoSignApi = new EchoSignApi();
		    	DocumentInfo documentInfo = echoSignApi.getDocumentInfo(echoSign.getApiKey(), echoSign.getDocumentKey());
		    	echoSign.setAgreementStatus(documentInfo.getStatus().toString());
		    	String versionKey = null;
		    	StringBuffer history = new StringBuffer();
		    	for (DocumentHistoryEvent event: documentInfo.getEvents()) {
		    	  versionKey = event.getDocumentVersionKey();
		    	  history.append(event.getDescription() + " on " + event.getDate() +
		    	    (versionKey == null ? "" : " (versionKey: " + versionKey + ")") + "\n");
		    	}
		    	echoSign.setHistory(history.toString());
		    	echoSign.setDocumentUrl(echoSignApi.getLatestDocumentUrl(echoSign.getApiKey(), echoSign.getDocumentKey()));
		    	this.webJaguar.updateEchoSign(echoSign);
			}

		} catch (Exception e) {
			notifyAdmin("EchoSign getDocumentInfo() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
		
		System.out.println("echo end " + new Date());
	}	
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
}
