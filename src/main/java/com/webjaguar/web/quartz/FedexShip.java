package com.webjaguar.web.quartz;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.dao.StagingDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.WorldShipTrackcode;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;

public class FedexShip extends WebApplicationObjectSupport {
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
    private StagingDao stagingDao;
	public void setStagingDao(StagingDao stagingDao) { this.stagingDao = stagingDao; }
	private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	Map<String, Object> gSiteConfig;
	Map<String, Configuration> siteConfig;
	
	public void importTrackingNumber() {
		gSiteConfig = this.globalDao.getGlobalSiteConfig();
		siteConfig = this.webJaguar.getSiteConfig();
		
		System.out.println("Fedex Ship on " + gSiteConfig.get("gSITE_DOMAIN") + " at " + new Date());
		try {
			List<WorldShipTrackcode> orderWithTrackcode = this.stagingDao.getFedexShipTrackcode();
			for (WorldShipTrackcode fedexShipTrackcode : orderWithTrackcode) {
				if ( this.webJaguar.isValidOrderId( fedexShipTrackcode.getOrderId() )) {
					System.out.println("orderId  " + fedexShipTrackcode.getOrderId());
					OrderStatus orderStatus = new OrderStatus();
					orderStatus.setOrderId( Integer.parseInt( fedexShipTrackcode.getOrderId() ) );
					orderStatus.setComments( "FedexShip -- " + new Date() );
					orderStatus.setStatus( "s" );
					orderStatus.setTrackNum( fedexShipTrackcode.getTrackNum() );
					orderStatus.setUsername("FedexShip");
					Order order = this.webJaguar.getOrder(Integer.parseInt( fedexShipTrackcode.getOrderId() ) , null);
					
					// notify user
					if (siteConfig.get( "FEDEX_SHIPPING_LABEL_EMAIL_NOTIFICATION" ).getValue().equals("true")) {
						SiteMessage siteMessage = null;
						try {
							Integer messageId = null;
							messageId = Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_FEDEX_SHIPPING_LABEL" ).getValue() );								
							siteMessage = this.webJaguar.getSiteMessageById( messageId );
						} catch (NumberFormatException e) {
							// do nothing
						}
						if (siteMessage != null) {
							orderStatus.setSubject( siteMessage.getSubject() );
							orderStatus.setMessage( siteMessage.getMessage() );
							orderStatus.setHtmlMessage( siteMessage.isHtml() );
							// multi store
							List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
							MultiStore multiStore = null;
							if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
								multiStore = multiStores.get(0);
							}
							
							if (notifyCustomer( orderStatus, order, multiStore )){
								orderStatus.setUserNotified(true);
							}
						}
					}
					
					this.webJaguar.insertOrderStatus( orderStatus );
					if((Boolean) gSiteConfig.get("gINVENTORY")) {
						// Generates  packingList
			    		if ( !order.isProcessed() ) {  			
							this.webJaguar.generatePackingList(order, false, new Date(), siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"));
			    		}
			    		// adds ship date to packinglist without shipdate and adjusts on hand
			    		this.webJaguar.ensurePackingListShipDate(order, siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"), 0);
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
			notifyAdmin(gSiteConfig, e.toString() );
		}
		this.stagingDao.updateFedexShipProcessed();
	}
	
	private boolean notifyCustomer(OrderStatus orderStatus, Order order, MultiStore multiStore)
	{
		Customer customer = this.webJaguar.getCustomerById(order.getUserId());
		
		if (!customer.isEmailNotify()) {
			// do not send email
			return false;
		}
		
		SalesRep salesRep = null;
		if ( (Boolean) gSiteConfig.get( "gSALES_REP" ) && order.getSalesRepId() != null ) {
			salesRep = this.webJaguar.getSalesRepById( order.getSalesRepId() );
		}
		// send email
		String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
		String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append( dateFormatter.format( new Date() ) + "\n\n" );

		// replace dyamic elements
		try {
			this.webJaguar.replaceDynamicElement( orderStatus, customer, salesRep, order, secureUrl, null);
		} catch (Exception e) {e.printStackTrace();}
		messageBuffer.append( orderStatus.getMessage() );

		try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.setBcc(contactEmail);
			if ( salesRep != null )
				helper.setCc( salesRep.getEmail() );
			helper.setSubject( orderStatus.getSubject() );
			helper.setText( messageBuffer.toString(), orderStatus.isHtmlMessage() );
			mailSender.send(mms);
		} catch (Exception ex) {
			notifyAdmin(gSiteConfig, ex.toString() );
			return false;
		}
		return true;
	}
	
	private void notifyAdmin(Map<String, Object> gSiteConfig, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	String siteDomain = gSiteConfig.get("gSITE_DOMAIN").toString();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("FedexShipManager on " + siteDomain);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
}