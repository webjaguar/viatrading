/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.12.2008
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;

public class TriplefinJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void exportInvoices() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// TRIPLEFIN configuration
		String[] config = siteConfig.get("TRIPLEFIN").getValue().split(",");
		
		if (!(gSiteConfig.get("gACCOUNTING").equals("TRIPLEFIN") && config.length > 2 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if triplefin feature is disabled 
			return;
		}
		
		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}		
		File triplefinFolder = new File(tempFolder, "triplefin");
		if (!triplefinFolder.exists()) {
			triplefinFolder.mkdir();
		} else {
			// clear old files
			for (File file: triplefinFolder.listFiles()) {
				file.delete();
			}
		}
		File triplefinSentFolder = new File(tempFolder, "triplefinSent");
		if (!triplefinSentFolder.exists()) {
			triplefinSentFolder.mkdir();
		}
		Set<Integer> orderIds = new HashSet<Integer>();
		
		OrderSearch search = new OrderSearch();
		search.setTriplefin(false);
		search.setEndDate(new Date());
		// send orders that are credit card captured, paypal completed or status=processing 
		search.setCustomSearch("cc_payment_status = 'CAPTURED' or pp_payment_status = 'Completed' or status = 'pr'");
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat prefixFormatter = new SimpleDateFormat("yyyyMMddkkmmssSSSSSS");
		
		NumberFormat nf = new DecimalFormat("#0.00");
		
		List<Order> orderList = this.webJaguar.getInvoiceExportList(search, null);
		if (orderList.isEmpty()) {
			return;
		}
		
		Map<String, String> triplefinShipping = new HashMap<String, String>();
		// triplefinShipping.put("Will Call", "001");
		triplefinShipping.put("FedEx Ground<sup>¨</sup>", "700");
		// triplefinShipping.put("ABF", "810");	
		// triplefinShipping.put("USPS First-Class", "600");
		// triplefinShipping.put("USPS Priority", "601");	
		// triplefinShipping.put("USPS Parcel Post", "602");	
		// triplefinShipping.put("USPS Media Mail Single-Piece", "604");	
		// triplefinShipping.put("USPS Library Mail Single-Piece", "605");	
		// triplefinShipping.put("USPS Expr Mail-Custom", "606");	
		// triplefinShipping.put("USPS Expr Mail-Post Office", "607");	
		// triplefinShipping.put("USPS Expr Mail-Post Addressee", "608");	
		// triplefinShipping.put("FedEx Ground U.S. to Canada", "701");	
		triplefinShipping.put("FedEx Standard Overnight<sup>¨</sup>", "704");	
		// triplefinShipping.put("FedExPS 1Day Freight", "707");	
		triplefinShipping.put("FedEx International Priority<sup>¨</sup>", "711");	
		triplefinShipping.put("FedEx International Economy<sup>¨</sup>", "712");	
		// triplefinShipping.put("Fed Ex Freight", "804");	
		// triplefinShipping.put("Roadway Express", "805");					 
		// triplefinShipping.put("SEKO", "811");	
		// triplefinShipping.put("Brennan - Freight Forwarder", "812");	
		// triplefinShipping.put("MegaSys", "813");			
		// triplefinShipping.put("Tripar Transportation", "814");	
		// triplefinShipping.put("Con Way", "815");	
		// triplefinShipping.put("RAM Trucking", "820");			 
		// triplefinShipping.put("Werner Enterprises", "821");	
		// triplefinShipping.put("Watkins", "822");	
		// triplefinShipping.put("USF Distribution", "823");			
		triplefinShipping.put("UPS Ground", "900");	
		triplefinShipping.put("UPS Next Day Air", "902");				 
		triplefinShipping.put("UPS 2nd Day Air", "905");	
		// triplefinShipping.put("Best Way Possible", "999");			
		// triplefinShipping.put("Use Customer Routing Guide", "998");	
		triplefinShipping.put("UPS Next Day Air Early A.M.", "901");	
		triplefinShipping.put("UPS Next Day Air Saver", "903");	
		triplefinShipping.put("UPS 2nd Day Air A.M.", "904");			
		triplefinShipping.put("UPS 3 Day Select", "906");	
		triplefinShipping.put("FedEx First Overnight<sup>¨</sup>", "702");	
		triplefinShipping.put("FedEx Priority Overnight<sup>¨</sup>", "703");	
		triplefinShipping.put("FedEx 2Day<sup>¨</sup>", "705");
		triplefinShipping.put("FedEx Express Saver<sup>¨</sup>", "706");	
		// triplefinShipping.put("FedExPS 2Day Freight", "708");			 
		// triplefinShipping.put("FedExPS 3Day Freight", "709");	
		// triplefinShipping.put("USPS Standard Canadian", "609");			ΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚ 
		// triplefinShipping.put("USPS Certified Canadian", "610");	
		// triplefinShipping.put("Relay Express", "824");			 
		// triplefinShipping.put("USPS Bulk/Flat Rate", "611");	
		// triplefinShipping.put("NO ACTION REQUIERED", "N/A");			 
		triplefinShipping.put("UPS Standard", "907");	
		// triplefinShipping.put("FedEx Saturday Delivery", "713");			ΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚ 
		// triplefinShipping.put("Fed Ex Ground, Signature Required", "714");	
		// triplefinShipping.put("Fed Ex Ground Residential", "715");			ΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚ 
		// triplefinShipping.put("Fed Ex Ground Res,Κ Signature Required", "716");	
		// triplefinShipping.put("Fed Ex Standard OVNT, Signature Required", "717");			 
		// triplefinShipping.put("Fed Ex INTL Economy, Deliver to Nearest", "720");	
		// triplefinShipping.put("Priority", "997");			ΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚΚ 
		// triplefinShipping.put("Fed Ex 2 Day Signature Required", "721");	
		// triplefinShipping.put("Fed Ex 2 Day Residential", "722");			 
		// triplefinShipping.put("Fed Ex 2 Day Residential Sig Required", "723");	
		// triplefinShipping.put("YELLOW FREIGHT LINES", "806");			 
		// triplefinShipping.put("OVERNITE TRANSPORTATION", "807");	
		// triplefinShipping.put("Misc TMS Carrier", "MISC");			 
		// triplefinShipping.put("See Service Name", "799");	
		// triplefinShipping.put("Fed Ex Freight", "ARFW");			 
		// triplefinShipping.put("Overnight Transportation", "OVNT");	
		// triplefinShipping.put("Roadway", "RDWY");			 
		// triplefinShipping.put("Yellow Freight Systems", "YFSY");	
		// triplefinShipping.put("ABF", "ABF");			 
		
		try {
			// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
			
			for (Order order: orderList) {
				orderIds.clear();
				orderIds.add(order.getOrderId());
				String prefix = prefixFormatter.format(new Date());
				File worder = new File(triplefinFolder, prefix + "worder.txt");

				PrintWriter pw = new PrintWriter(new FileWriter(worder));
				
				pw.print("\"SOURCEDOC|CHARGCUST|CUSTID|SHIPTONAME|ADDRESS1|ADDRESS2|CITY|STATE|ZIPCODE|COUNTRY|SHIPTOPHONE|ORDERDATE|CARRIERID|SHIPTOEMAIL|REMARKS|SRCPORELEASE|;");
				
				// shipping method
				String shippingMethod = order.getShippingMethod();
				if (triplefinShipping.containsKey(shippingMethod)) {
					shippingMethod = triplefinShipping.get(shippingMethod);
				} else {
					shippingMethod = "";
				}

				order.getShipping().replace("\"", " ");
				order.getShipping().replace("|", " ");
				order.getShipping().replace(";", " ");
				
				pw.print(order.getOrderId());
				pw.print("|");
				pw.print("");		// CHARGCUST	
				pw.print("|");
				pw.print("");		// CUSTID	
				pw.print("|");
				pw.print(order.getShipping().getFirstName() + " " + order.getShipping().getLastName());
				pw.print("|");
				pw.print(order.getShipping().getAddr1());
				pw.print("|");
				pw.print(order.getShipping().getAddr2());
				pw.print("|");
				pw.print(order.getShipping().getCity());
				pw.print("|");
				pw.print(order.getShipping().getStateProvince());
				pw.print("|");
				pw.print(order.getShipping().getZip());				
				pw.print("|");
				pw.print(order.getShipping().getCountry());	
				pw.print("|");
				pw.print(order.getShipping().getPhone());				
				pw.print("|");
				pw.print(dateFormatter.format(order.getDateOrdered()));	
				pw.print("|");
				pw.print(shippingMethod);	
				pw.print("|");
				pw.print(order.getUserEmail());	
				pw.print("|");
				pw.print("");		// REMARKS	
				pw.print("|");
				pw.print("");		// SRCPORELEASE
				pw.print("|");
				pw.println("\"");	

				pw.close();
				
				// ftp file
		        FileInputStream fis = new FileInputStream(worder);
		        boolean worderSuccess = ftp.storeFile(worder.getName(), fis);
		        fis.close();
				
				File worderline = new File(triplefinFolder, prefix + "worderline.txt");
				pw = new PrintWriter(new FileWriter(worderline));
				
				// line items
	    		for (LineItem lineItem : order.getLineItems()) {
					pw.print("\"LINENO|SOURCEDOC|UOMCODE|ITEMID|QTYORDER|LINETOTAL|UNITPRICE|;");
					
					pw.print(lineItem.getLineNumber());
					pw.print("|");
					pw.print(order.getOrderId());
					pw.print("|");
					pw.print("EA");		// UOMCODE	
					pw.print("|");
					pw.print(lineItem.getProduct().getSku());
					pw.print("|");
					
					// quantity
					int totalQty = lineItem.getQuantity();
					if (lineItem.getProduct().getCaseContent() != null) {
						totalQty = totalQty * lineItem.getProduct().getCaseContent();
					}
					pw.print(totalQty);

					pw.print("|");
					if (lineItem.getTotalPrice() != null) {
						pw.print(nf.format(lineItem.getTotalPrice()));						
					} else {
						pw.print("");
					}
					pw.print("|");
					if (lineItem.getUnitPrice() != null) {
						pw.print(nf.format(lineItem.getUnitPrice()));						
					} else {
						pw.print("");
					} 
					pw.print("|");
					pw.println("\"");
	    		}
				pw.close();

				// ftp file
		        fis = new FileInputStream(worderline);
		        boolean worderlineSuccess = ftp.storeFile(worderline.getName(), fis);
		        fis.close();
		        
		        if (worderlineSuccess & worderSuccess) {
		        	copy(worder, new File(triplefinSentFolder, worder.getName()));
		        	worder.delete();		        	
		        	copy(worderline, new File(triplefinSentFolder, worderline.getName()));
		        	worderline.delete();
		        	
		        	this.webJaguar.updateOrderExported("triplefin", null, orderIds);
		        }
			}			

	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
			
		} catch (Exception e) {
			notifyAdmin("Triplefin exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}	
    	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
	
	// Copies src file to dst file.
	// If the dst file does not exist, it is created
	private void copy(File src, File dst) throws IOException
	{
		InputStream in = new FileInputStream( src );
		OutputStream out = new FileOutputStream( dst );

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ( (len = in.read( buf )) > 0 )
		{
			out.write( buf, 0, len );
		}
		in.close();
		out.close();
	}
}
