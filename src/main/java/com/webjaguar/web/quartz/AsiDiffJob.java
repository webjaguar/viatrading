/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 10.13.2010
 */

package com.webjaguar.web.quartz;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;


import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;


import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.thirdparty.asi.AsiDiff;

public class AsiDiffJob extends WebApplicationObjectSupport {
	
	private static WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }

	public void getDiffProduct() {	
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();	
		
		//String diffDate = ServletRequestUtils.getStringParameter(request, "diffDate", null);
		AsiDiff asiUpdater = new AsiDiff(siteConfig, getServletContext().getRealPath("temp"), null);
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		

		if (asiUpdater.getAsiSupplierDiff()) {
			notifyAdmin("Exception : ASI parseSupplierDiff() on " + siteConfig.get("SITE_URL").getValue(), null);
		}
		
		if (asiUpdater.getAsiProductDiff()) {
			notifyAdmin("Exception : ASI parseProductDiff() on " + siteConfig.get("SITE_URL").getValue(), null);
		}

		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		notifyAdmin("Success: asiDiffController - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
		
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}