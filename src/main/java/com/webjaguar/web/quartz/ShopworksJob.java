/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 07.12.2010
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;

public class ShopworksJob extends WebApplicationObjectSupport {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }

	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }

	private void exportInvoices() throws IOException {
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		if (!(siteConfig.get("SHOPWORKS").getValue().equals("true") && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if shopworks feature is disabled
			return;
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
		DecimalFormat df = new DecimalFormat("#0");
		df.setMinimumIntegerDigits(6);

		// search orderds to export
		OrderSearch search = new OrderSearch();
		search.setShopworks(false);
		search.setStatus("pprs");
		Date cutoff = new Date();
		search.setEndDate(cutoff);

		List<Order> orderList = this.webJaguar.getInvoiceExportList(search, null);
		if (orderList.isEmpty()) {
			return;
		}
		
		File baseFile = new File(getServletContext().getRealPath("/admin/export/order/"));
		String fileName = "/invoiceExport_shopworks_" + dateFormatter.format(new Date()) + ".txt";
		File exportFile = new File(baseFile, fileName);
		PrintWriter pw = new PrintWriter(new FileWriter(exportFile));

		try {
			for (Order order : orderList) {
				Customer customer = this.webJaguar.getCustomerById(order.getUserId());
				pw.println("\"");
				pw.println();
				// Order Required
				pw.println("---- Start Order ----\n");
				pw.println("ExtOrderID: " + order.getOrderId());
				pw.println("ExtSource: WebSite");
				pw.println("date_External: " + dateFormatter.format(order.getDateOrdered()));
				pw.println("id_OrderType: " /* ???? */);

				pw.println();

				// pw.println("CustomerPurchaseOrder: ");
				// pw.println("TermsName: ");
				// pw.println("CustomerServiceRep: ");
				// pw.println("CustomerType: ");
				// pw.println("id_CompanyLocation: ");
				// pw.println("id_SalesStatus: ");
				// pw.println("sts_CommishAllow: ");
				// pw.println("HoldOrderText: ");

				pw.println();

				pw.println("date_OrderPlaced: " + dateFormatter.format(order.getDateOrdered()));
				if (order.getUserDueDate() != null) {
					pw.println("date_OrderRequestedToShip: " + dateFormatter.format(order.getUserDueDate()));
				}
				// pw.println("date_OrderDropDead: " +
				// dateFormatter2.format(order.getDateOrdered()));

				// pw.println("sts_Order_SalesTax_Override: ");
				// pw.println("sts_ApplySalesTax01: ");
				// pw.println("sts_ApplySalesTax02: ");
				// pw.println("sts_ApplySalesTax03: ");
				// pw.println("sts_ApplySalesTax04: ");
				// pw.println("sts_ShippingTaxable: ");
				pw.println();

				// pw.println("coa_AccountSalesTax01: ");
				// pw.println("coa_AccountSalesTax02: ");
				// pw.println("coa_AccountSalesTax03: ");
				// pw.println("coa_AccountSalesTax04: ");
				pw.println();

				// pw.println("AddressDescription: ");
				if (customer.getAddress() != null) {
					pw.println("AddressCompany: " + customer.getAddress().getCompany());
					pw.println("Address1: " + order.getShipping().getAddr1());
					pw.println("Address2: " + order.getShipping().getAddr2());
					pw.println("AddressCity: " + order.getShipping().getCity());
					pw.println("AddressState: " + order.getShipping().getStateProvince());
					pw.println("AddressZip: " + order.getShipping().getZip());
					pw.println("AddressCountry: " + order.getShipping().getCountry());
				}
				pw.println("ShipMethod: " + order.getShippingMethod());
				// pw.println("cur_Shipping: ");
				// pw.println("sts_Order_ShipAddress_Add: ");
				// pw.println("NotesToArt: ");
				// pw.println("NotesToProduction: ");
				// pw.println("NotesToReceiving: ");
				// pw.println("NotesToPurchasing: ");
				// pw.println("NotestoShipping: ");
				// pw.println("NotesToAccounting: ");
				// pw.println("NotesToPurchasingSub: ");

				pw.println("---- End Order ----\n\n");

				// Customer Required

				pw.println("---- Start Customer ----\n");
				if (customer.getOrderCount() < 1) {
					// Required:
					// Yes for new customers. Yes for existing customers IF id_Customer is not populated.
					pw.println("ExtCustomerID: " + customer.getId());
				} else {
					// Required:
					// No for new customers. Yes for existing customers IF ExtCustomerID is not populated.
					pw.println("id_Customer: " + customer.getId());
				}
				
				
				if (customer.getAddress() != null) {
					pw.println("Company: " + customer.getAddress().getCompany());
				}
				// pw.println("id_CompanyLocation: ");
				pw.println("Terms: " + customer.getPayment());
				// pw.println("WebsiteURL: ");
				pw.println("EmailMain: " + customer.getUsername());
				pw.println();

				// pw.println("AddressDescription: ");
				if (customer.getAddress() != null) {
					pw.println("AddressCompany: " + customer.getAddress().getCompany());
					pw.println("Address1: " + order.getShipping().getAddr1());
					pw.println("Address2: " + order.getShipping().getAddr2());
					pw.println("AddressCity: " + order.getShipping().getCity());
					pw.println("AddressState: " + order.getShipping().getStateProvince());
					pw.println("AddressZip: " + order.getShipping().getZip());
					pw.println("AddressCountry: " + order.getShipping().getCountry());
				}

				// pw.println("sts_ApplySalesTax01: ");
				// pw.println("sts_ApplySalesTax02: ");
				// pw.println("sts_ApplySalesTax03: ");
				// pw.println("sts_ApplySalesTax04: ");

				// pw.println("coa_AccountSalesTax01: ");
				// pw.println("coa_AccountSalesTax02: ");
				// pw.println("coa_AccountSalesTax03: ");
				// pw.println("coa_AccountSalesTax04: ");

				// pw.println("TaxExemptNumber: ");

				// pw.println("id_DiscountLevel: ");
				// pw.println("id_DefaultCalculator1: ");
				// pw.println("id_DefaultCalculator2: ");

				if (customer.getSalesRepId() != null) {
					// pw.println("CustomerServiceRep: James Brown
				}
				// pw.println("CustomerType: Technology
				// pw.println("CustomerSource: Yellow Pages
				pw.println("ReferenceFrom: " + customer.getTrackcode());
				// pw.println("SICCode: 8843
				// pw.println("SICDescription: Computer Companies, National
				// pw.println("n_EmployeeCount: 33

				pw.println("CustomField01: " + customer.getField1());
				pw.println("CustomField02: " + customer.getField2());
				pw.println("CustomField03: " + customer.getField3());
				pw.println("CustomField04: " + customer.getField4());
				pw.println("CustomField05: " + customer.getField5());
				pw.println("CustomField06: " + customer.getField6());
				pw.println("CustomField07: " + customer.getField7());
				pw.println("CustomField08: " + customer.getField8());
				pw.println("CustomField09: " + customer.getField9());
				pw.println("CustomField10: " + customer.getField10());
				pw.println();

				pw.println("---- End Customer ----\n\n");

				// Contact Optional
				pw.println("---- Start Contact ----\n");
				pw.println("NameFirst: " + customer.getAddress().getFirstName());
				pw.println("NameLast: " + customer.getAddress().getLastName());
				// pw.println("Department: ");
				// pw.println("Title: ");
				pw.println("Phone: " + customer.getAddress().getPhone());
				pw.println("Fax: " + customer.getAddress().getFax());
				pw.println("Email: " + customer.getUsername());
				// pw.println("sts_Contact_Add: ");
				// pw.println("sts_EnableBulkEmail: ");
				pw.println();

				pw.println("---- End Contact ----\n\n");

				// Design Optional
				pw.println("---- Start Design ----\n");

				pw.println("\t---- Start Location ----\n");
				pw.println("\t---- End Location ----\n");

				pw.println("---- End Design ----\n\n");

				// Design Product
				for (LineItem lineItem : order.getLineItems()) {
					pw.println("---- Start Product ----\n");
					pw.println("PartNumber: " + lineItem.getProduct().getSku());
					// pw.println("PartColorRange: Whites
					// pw.println("PartColor: White
					// pw.println("cur_UnitPriceUserEntered: 8.77
					// pw.println("OrderInstructions: Make sure this is printed properly.<cr>Last
					// time we had errors with this.

					// pw.println("Size01_Req:
					// pw.println("Size02_Req:
					// pw.println("Size03_Req: 22
					// pw.println("Size04_Req: 13
					// pw.println("Size05_Req:
					// pw.println("Size06_Req:

					// pw.println("sts_Prod_Product_Override: 0
					// pw.println("PartDescription:
					// pw.println("cur_UnitCost:
					// pw.println("sts_EnableCommission:
					// pw.println("id_ProductClass:

					// pw.println("sts_Prod_SalesTax_Override: 0
					// pw.println("sts_EnableTax01:
					// pw.println("sts_EnableTax02:
					// pw.println("sts_EnableTax03:
					// pw.println("sts_EnableTax04:

					// pw.println("sts_Prod_SecondaryUnits_Override: 1
					// pw.println("sts_UseSecondaryUnits: 1
					// pw.println("Units_Qty: 10
					// pw.println("Units_Type: Square Feet
					// pw.println("Units_Area1: 2
					// pw.println("Units_Area2: 5
					// pw.println("sts_UnitsPricing: 0
					// pw.println("sts_UnitsPurchasing: 1
					// pw.println("sts_UnitsPurchasingExtraPercent: .05
					// pw.println("sts_UnitsPurchasingExtraRound: 2

					// pw.println("sts_Prod_Behavior_Override: 0
					// pw.println("sts_ProductSource_Supplied:
					// pw.println("sts_ProductSource_Purchase:
					// pw.println("sts_ProductSource_Inventory:

					// pw.println("sts_Production_Designs:
					// pw.println("sts_Production_Subcontract:
					// pw.println("sts_Production_Components:

					// pw.println("sts_Storage_Ship:
					// pw.println("sts_Storage_Inventory:

					// pw.println("sts_Invoicing_Invoice:
					pw.println();

					pw.println("---- End Product ----\n\n");
				}

				// Design Payment
				pw.println("---- Start Payment ----\n");
				// pw.println("date_Payment: 9/22/08
				pw.println("cur_Payment: " + order.getPayment());
				pw.println("PaymentType: " + order.getPaymentMethod());
				// pw.println("PaymentNumber: xxxx-xxxx-xxxx-4435
				// pw.println("Card_Name_Last: McSwarthy
				// pw.println("Card_Name_First: Patrick
				// pw.println("Card_Exp_Date: 0310
				// pw.println("Notes: Card was processed successfully using Authorize.net
				// gateway<cr>
				// pw.println("Transaction number 22345678
				pw.println();

				pw.println("---- End Payment ----\n\n");
				pw.println();
				pw.println("\"");
			}
			pw.println();
			pw.close();

			this.webJaguar.updateOrderExported("shopworks", cutoff, null);
			

		} catch (Exception e) {
			notifyAdmin("DSI exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}

	private void notifyAdmin(String subject, String message) {
		// send email notification
		String contactEmail = "developers@advancedemedia.com";
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}