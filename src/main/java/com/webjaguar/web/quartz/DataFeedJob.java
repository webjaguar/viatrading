/* Copyright 2011 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 07.15.2011
 */

package com.webjaguar.web.quartz;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.DataFeedSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.ProductXML;
import com.webjaguar.model.WebjaguarDataFeed;
import com.webjaguar.web.admin.datafeed.WebjaguarCategory;

public class DataFeedJob extends WebApplicationObjectSupport {
	
	private static WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }

	
	private static JAXBContext context;
	private static File productFile;
	private static String content;
	private static int gPRODUCT_FIELDS;
	private static List<ProductField> productFields;
	private static Map <String, WebjaguarCategory> categoryMap;
	private static Map<String, Configuration> siteConfig;
	
	public void uploadProducts() throws Exception {
		Map<String, Object> gSiteConfig =  globalDao.getGlobalSiteConfig();
		siteConfig = webJaguar.getSiteConfig();
		gPRODUCT_FIELDS = (Integer) gSiteConfig.get("gPRODUCT_FIELDS");
		productFields = webJaguar.getProductFields(null, gPRODUCT_FIELDS);
		context  = JAXBContext.newInstance(ProductXML.class);
		context.createUnmarshaller();
		
		if(siteConfig.get("DATA_FEED_MEMBER_TYPE").getValue().isEmpty()) {
			return;
		}
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		DataFeedSearch search = new DataFeedSearch();
		search.setActive(true);
		List<WebjaguarDataFeed> dataFeedList = webJaguar.getWebjaguarDataFeedList(search);
		
		String fName = null;
		for(WebjaguarDataFeed dataFeed : dataFeedList) {
			search = new DataFeedSearch();
			if(dataFeed.getCategory() != null) {
				Object category = dataFeed.getCategory();
				search.setCategory(Integer.parseInt(category.toString()));
			}
			fName = dataFeed.getFilename();
			if(fName != null && !fName.isEmpty()) {
				dataFeed.setFilename(fName.split("\\.")[0]);
			}
			// get generated data feed file
	    	File baseFile = new File(getServletContext().getRealPath("/admin/feed/"));
	    	if (!baseFile.exists()) {
	    		baseFile.mkdir();
	    	}
			
			File file[] = baseFile.listFiles();
			for (int f=0; f<file.length; f++) {
				if (file[f].getName().startsWith(dataFeed.getFilename()) & ( file[f].getName().endsWith(".xml") || file[f].getName().endsWith(".txt") )) {
					file[f].delete();
				}
			}
	    	
			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
		    String fileName = dataFeed.getFilename() + "_" + dateFormatter.format(new Date()) +"."+dataFeed.getFiletype();
		    File dataFeedFile = new File(baseFile, fileName);
		    
		    ProductXML pxml = null;
	    	PrintWriter pw = null;
	    	if(dataFeed.getFiletype().equalsIgnoreCase("xml")) {
	    		pxml = new ProductXML();
	    		pxml.setToken(dataFeed.getToken());
	    	} else if(dataFeed.getFiletype().equalsIgnoreCase("txt")){
	    		try {
	    			pw = new PrintWriter(new FileWriter(dataFeedFile));
	    			pw.print(dataFeed.getToken()+"\n");
	    			
	    		} catch (IOException e1) { e1.printStackTrace();}
	    	}
	    	
	    	// set category
	   		search.setCategory( dataFeed.getCategory() );
	   		
	    	int productCount = webJaguar.webjaguarDataFeedProductListCount(search);
			System.out.println("Count "+productCount);
			int limit = 10;
			search.setLimit(limit);
			String imagePath = siteConfig.get("SITE_URL").getValue()+"assets/Image/Product/detailsbig/";
			
			for (int offset=0, excel = 1; offset<productCount; excel++) {
			   	List<Product> productList = webJaguar.getWebjaguarDataFeedProductList(search, 5, imagePath, true);
			   	
			   	if(dataFeed.getFiletype().equalsIgnoreCase("xml")) {
		    		pxml.addToList(productList);
			    } else if(dataFeed.getFiletype().equalsIgnoreCase("txt") && pw != null){
			    	this.writeTextFile(pw, productList);
			    }
			   	offset = offset + limit;
			    search.setOffset(offset);
			}
			if(pw != null){
				pw.close();
			}
			if(dataFeed.getFiletype().equalsIgnoreCase("xml")) {
	    		this.writeXMLFile(dataFeedFile, pxml, dataFeed);
		    } 
			if (dataFeedFile != null && dataFeedFile.exists()) {
				try {
					if (dataFeed.getServer().trim().equals( "" )) {
			    		throw new UnknownHostException();
			    	}
			    	if (dataFeed.getFilename().trim().equals( "" )) {
			    		throw new Exception();
			    	}
			    	
			    	// Connect and logon to FTP Server
			        FTPClient ftp = new FTPClient();
			        ftp.connect( dataFeed.getServer() );
			        
			        // login
			        ftp.login( dataFeed.getUsername(), dataFeed.getPassword() );
			        
			        if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
			            // upload file
			            FileInputStream fis = new FileInputStream(dataFeedFile);
			            ftp.storeFile(dataFeed.getFilename()+"."+dataFeed.getFiletype(), fis );
			            fis.close();        	
			        }
			        
			        // Logout from the FTP Server and disconnect
			        ftp.logout();
			        ftp.disconnect();
			        
			        Date end = new Date();
	    			sbuff.append("Started : " + dateFormatter.format(start) + "\n");
	    			sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
	    			notifyAdmin("Successful Datafeed uploadProducts() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	        	} catch (Exception e) {
					notifyAdmin("Exception in Datafeed uploadProducts() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
					e.printStackTrace();
				}
			} else {
				System.out.println("Missing Data Feed File");
			}
			
		}
		
	}
	
	private void writeXMLFile(File dataFeedFile, ProductXML pxml, WebjaguarDataFeed dataFeed) {
    	
    	for (Product product : pxml.getProduct()) {
	    	String breadCrumb = null;
	    	for(Object catId : product.getCatIds()) {
	    		breadCrumb = this.getBreadCrumb( Integer.parseInt(catId.toString()));
	    		
	    		if(breadCrumb != null) {
	    			product.getBreadCrumb().add(breadCrumb);
	    		}
	    	}
	    	
	    	if(dataFeed.getPrefix() != null && !dataFeed.getPrefix().isEmpty()) {
	    		product.setSku(dataFeed.getPrefix()+product.getSku());
	    	}
	    	
	    	// trim long desc as it creates problem
	    	if(product.getLongDesc() != null && product.getLongDesc().length() > 0){
	    		product.setLongDesc(product.getLongDesc().trim());
	    	}
		}

		try {
			context.createMarshaller().marshal(pxml, dataFeedFile);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
    }
   
	private void writeTextFile(PrintWriter pw, List<Product> productList) {
    	Method m = null;
		Object arglist[] = null;
		Class<Product> c = Product.class;
		
    	for(Product product : productList){
    		pw.print(product.getName()+"\t"+product.getSku()+"\t"+product.getShortDesc()+"\t"+product.getLongDesc()+"\t");
    		pw.print(product.getWeight()+"\t"+product.getMsrp()+"\t"+product.isHideMsrp()+"\t");
    		for(String breadCrumb : product.getBreadCrumb()){
    			pw.print(breadCrumb+"\t");
        	}
    		pw.print(product.getPrice1()+"\t");
    		for(ProductImage image : product.getImages()){
    			pw.print(image.getImageUrl() +"\t");
        	}
    		for(int i=1; i<=30; i++){
    			try {
    				m = c.getMethod("getField"+i);
        			pw.print((m.invoke(product, arglist) != null ? m.invoke(product, arglist) : "") +"\t");
                }catch(Exception e) {
                	pw.print("" +"\t");
                }
    		}
    		pw.print(product.getKeywords()+"\t"+product.getProtectedLevel()+"\t"+product.getPacking()+"\t"+product.isLoginRequire()+"\t");
    		pw.print(product.isHidePrice()+"\t"+product.getRecommendedList()+"\t"+product.getRecommendedListTitle()+"\t"+product.getRecommendedListDisplay()+"\t");
    		pw.print(product.getAlsoConsider()+"\t"+product.isTaxable()+"\t"+product.isSearchable()+"\t"+product.getOptionCode()+"\t");
    		pw.print(product.isNegInventory()+"\t"+product.isShowNegInventory()+"\t"+product.getInventoryAFS()+"\t"+product.isActive()+"\t");
    		pw.print(product.getTab1()+"\t"+product.getTab1Content()+"\t"+product.getTab2()+"\t"+product.getTab2Content()+"\t");
    		pw.print(product.getTab3()+"\t"+product.getTab3Content()+"\t"+product.getTab4()+"\t"+product.getTab4Content()+"\t");
    		pw.print(product.getManufactureName()+"\t"+product.isEnableRate()+"\t"+product.getSiteMapPriority()+"\t");
    		pw.print("\n");
    		pw.flush();
      }
    }
   
	public void downloadProducts() throws Exception {
		Map<String, Object> gSiteConfig =  globalDao.getGlobalSiteConfig();
		siteConfig = webJaguar.getSiteConfig();
		gPRODUCT_FIELDS = (Integer) gSiteConfig.get("gPRODUCT_FIELDS");
		productFields = webJaguar.getProductFields(null, gPRODUCT_FIELDS);
		context  = JAXBContext.newInstance(ProductXML.class);
		context.createUnmarshaller();
		
		if(siteConfig.get("DATA_FEED_MEMBER_TYPE").getValue().isEmpty()) {
			return;
		}
		Date start = new Date();
		DataFeedSearch search = new DataFeedSearch();
		search.setActive(true);
		search.setSubscriber(false);
		List<WebjaguarDataFeed> dataFeedList = webJaguar.getWebjaguarDataFeedList(search);
		String fName = null;
		for(WebjaguarDataFeed dataFeed : dataFeedList) {
        	
        	System.out.println("Downloading "+dataFeed.getName());
    		// get generated data feed file
        	fName = dataFeed.getFilename();
			if(fName != null && !fName.isEmpty()) {
				dataFeed.setFilename(fName.split("\\.")[0]);
			}
			File baseFile = new File(getServletContext().getRealPath("/admin/feed/"));
        	if (!baseFile.exists()) {
        		baseFile.mkdir();
        	}
    		
    		File file[] = baseFile.listFiles();
    		for (int f=0; f<file.length; f++) {
    			if (file[f].getName().startsWith(dataFeed.getFilename()) & file[f].getName().endsWith("."+dataFeed.getFiletype())) {
    				context  = JAXBContext.newInstance(Product.class);
    				productFile = file[f];
    			}
    		}
        	
    		try {
    			context  = JAXBContext.newInstance(Product.class);
    			SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    		    String fileName = dataFeed.getFilename() + "_" + dateFormatter.format(new Date()) + ".xml";
    		    productFile = new File(baseFile, fileName);
    		    StringBuffer sbuff = new StringBuffer();
    	    	if (dataFeed.getServer().trim().equals( "" )) {
    	    		throw new UnknownHostException();
    	    	}
    	    	if (dataFeed.getFilename().trim().equals( "" )) {
    	    		throw new Exception();
    	    	}
    	    	
    	    	// Connect and logon to FTP Server
    	        FTPClient ftp = new FTPClient();
    	        ftp.connect( dataFeed.getServer() );
    	        
    	        // login
    	        ftp.login( dataFeed.getUsername(), dataFeed.getPassword() );
    	        
    	        if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
    	            // download file
    	            FileOutputStream fos = new FileOutputStream(productFile);
    	            ftp.retrieveFile(dataFeed.getFilename()+".xml", fos);
    	            sbuff.append("Downloading file: " + dataFeed.getFilename() + "Compeleted.");
    	            fos.close();        	
    	        }
    	        
    	        // Logout from the FTP Server and disconnect
    	        ftp.logout();
    	        ftp.disconnect();
    	        
    	        parseProductXmlFile(productFile);
    	        
    	        Date end = new Date();
    			sbuff.append("Started : " + dateFormatter.format(start) + "\n");
    			sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
    			notifyAdmin("Successful Datafeed downloadProducts() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
    		} catch (Exception e) {
    			notifyAdmin("Exception in Datafeed downloadProducts() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
    			e.printStackTrace();
    		}
        	
        }
		
    }
	
	private String getBreadCrumb(Integer catId) {
    	List<Category> breadCrumb = webJaguar.getCategoryTree(catId, true, null);
    	String categoryTree = "";
    	for (Category category : breadCrumb) {
    		categoryTree += category.getName() + "-->" ;
    	}
    	if(categoryTree.isEmpty()) {
    		return null;
    	}
    	return categoryTree.substring(0, categoryTree.lastIndexOf('-')-1);

	}
    
    
 	public void parseProductXmlFile(File xml_file) {
 		categoryMap = webJaguar.getWebjaguarCategoryMap();
		try {
			SAXParserFactory f = SAXParserFactory.newInstance();
	        SAXParser parser = f.newSAXParser();
	        DefaultHandler handler = new MyContentHandler();
	        content = FileUtils.readFileToString(productFile, "UTF-8");
	        parser.parse(xml_file,handler);
	    } catch (ParserConfigurationException e) {System.out.println("error 1 ");
	    } catch (SAXException e) {System.out.println("error 2 ");
	    } catch (IOException e) {System.out.println("error 3 ");
			notifyAdmin("Webjaguar Datafeed uploadProducts() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
	    }
	}
	
	public static class MyContentHandler extends DefaultHandler {
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		List<String> dataDeleted = new ArrayList<String>();
		List<Map <String, Object>> categoryData = new ArrayList<Map <String, Object>>();
		HashMap<String, Object> map;
		Set<String> breadCrumbs = new TreeSet<String>();
		StringBuffer xml = new StringBuffer();
		boolean authorizedFeed = false;
		WebjaguarDataFeed dataFeed = null;
		
		boolean product;
		// for DIFF
		boolean modified, added, deleted;
		
		public void startDocument() throws SAXException {
			xml = new StringBuffer();
		}
	    
	    public void endDocument() throws SAXException {
	       // for files that has less than 500 Product
	       if (data.size() > 0) {
				// products
				updateProduct(data);
				data.clear();
				map.clear();
	       }
	       if (breadCrumbs.size() > 0) {
				// categories
	    	    updateCategories(breadCrumbs);
				breadCrumbs.clear();
		   }
	       if (dataDeleted.size() > 0) {
				// products
				deleteProduct(dataDeleted);
				dataDeleted.clear();
	       }
	       if (categoryData.size() > 0) {
	    	    System.out.println("Category Mapping Size "+categoryData.size());
				// product category
				updateProductCategory(categoryData);
				categoryData.clear();
		   }
	       if(dataFeed != null) {
	    	   webJaguar.markAndInactiveOldProducts("Webjaguar", dataFeed.getId(), true);
	    	   System.out.println("Downloading complete for"+dataFeed.getName());
	    		
	       }
		}
	    
	    public void startElement(String nsURI, String strippedName, String tagName, Attributes attributes) throws SAXException {
	    	if(tagName.equalsIgnoreCase("Token")){
	    		// read and save from file directly
				int start = content.indexOf("<token>");
	    		int end = content.indexOf("</token>", start);
	    		String token = content.substring(start+7, end);
	    		
	    		dataFeed = webJaguar.getWebjaguarDataFeed(null, token.trim(), "webjaguar");
	    		if(dataFeed != null) {
	    			authorizedFeed = true;
	    			webJaguar.markAndInactiveOldProducts("Webjaguar", dataFeed.getId(), false);
	    	    } 
	    	}
 	    	
	    	if(!tagName.equalsIgnoreCase("Products") && !authorizedFeed) {
    	    	throw new SAXException();
    	    }
	    	
	    	if (tagName.equalsIgnoreCase("Product") && authorizedFeed) { 
	    		map = new HashMap<String, Object>();
	    		
	    		// read and save from file directly
				int start = content.indexOf("<product id=\""+attributes.getValue("id")+"\"");
	    		int end = content.indexOf("</product>", start) + 10;
	    		xml.append(content.substring(start, end));
	    		
	    		// convert to Product object
	    		JAXBContext context;
	    		Unmarshaller unmarshaller = null;
	    		ByteArrayInputStream bis;
	    		Product prod = null;
				try {
					context = JAXBContext.newInstance(Product.class);
					unmarshaller = context.createUnmarshaller();
					StringBuffer sb = new StringBuffer(xml);
					bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
					prod = (Product) unmarshaller.unmarshal(bis);
				} catch (Exception e) { e.printStackTrace(); }
				
				if(prod != null) {
					Class<Product> c = Product.class;
					Method m = null;
					Object arglist[] = null;
					
					map.put("name", prod.getName());
					map.put("sku", prod.getSku());
					map.put("master_sku", prod.getMasterSku());
					map.put("feed_freeze", prod.isFeedFreeze());
					map.put("short_desc", prod.getShortDesc());
					map.put("long_desc", prod.getLongDesc());
					map.put("keywords", prod.getKeywords());
					map.put("weight", prod.getWeight());
					map.put("msrp", prod.getMsrp());
					map.put("hide_msrp", prod.isHideMsrp());
					map.put("price_by_customer", prod.isPriceByCustomer());
					map.put("price_1", prod.getPrice1());
					map.put("price_2", prod.getPrice2());
					map.put("price_3", prod.getPrice3());
					map.put("price_4", prod.getPrice4());
					map.put("price_5", prod.getPrice5());
					map.put("price_6", prod.getPrice6());
					map.put("price_7", prod.getPrice7());
					map.put("price_8", prod.getPrice8());
					map.put("price_9", prod.getPrice9());
					map.put("price_10", prod.getPrice10());
					map.put("qty_break_1", prod.getQtyBreak1());
					map.put("qty_break_2", prod.getQtyBreak2());
					map.put("qty_break_3", prod.getQtyBreak3());
					map.put("qty_break_4", prod.getQtyBreak4());
					map.put("qty_break_5", prod.getQtyBreak5());
					map.put("qty_break_6", prod.getQtyBreak6());
					map.put("qty_break_7", prod.getQtyBreak7());
					map.put("qty_break_8", prod.getQtyBreak8());
					map.put("qty_break_9", prod.getQtyBreak9());
					map.put("quote", prod.isQuote());
					for (ProductField productField: productFields) {
		    			if (productField.isEnabled() && productField.isProductExport()) {
		            		try {
								m = c.getMethod("getField" + productField.getId());
								map.put("field_"+productField.getId(), (String) m.invoke(prod, arglist));
							} catch (Exception e) { e.printStackTrace(); }
		            	}
		        	}
		        	map.put("minimum_qty", prod.getMinimumQty());
					map.put("protected_level", prod.getProtectedLevel());
					map.put("packing", prod.getPacking());
					map.put("login_require", prod.isLoginRequire());
					map.put("hide_price", prod.isHidePrice());
					map.put("hide_header", prod.isHideHeader());
					map.put("hide_topbar", prod.isHideTopBar());
					map.put("hide_leftbar", prod.isHideLeftBar());
					map.put("hide_rightbar", prod.isHideRightBar());
					map.put("hide_footer", prod.isHideFooter());
					map.put("hide_breadcrumbs", prod.isHideBreadCrumbs());
					map.put("add_to_list", prod.isAddToList());
					map.put("compare", prod.isCompare());
					map.put("viewed", prod.getViewed());
					map.put("taxable", prod.isTaxable());
					map.put("searchable", prod.isSearchable());
					map.put("inventory", prod.getInventory());
					map.put("inventory_afs", prod.getInventoryAFS());
					map.put("neg_inventory", prod.isNegInventory());
					map.put("show_neg_inventory", prod.isShowNegInventory());
					map.put("custom_shipping_enabled", prod.isCustomShippingEnabled());
					map.put("subscription", prod.isSubscription());
					map.put("active", prod.isActive());
					map.put("enable_rate", prod.isEnableRate());
					map.put("sitemap_priority", prod.getSiteMapPriority());
					map.put("recommended_list", prod.getRecommendedList());
					map.put("recommended_list_title", prod.getRecommendedListTitle());
					map.put("recommended_list_display", prod.getRecommendedListDisplay());
					map.put("option_code", prod.getOptionCode());
					map.put("cross_items_code", prod.getCrossItemsCode());
					map.put("feed", "Webjaguar");				
					map.put("feed_new", true);
					map.put("feed_id",dataFeed.getId());
					if (prod.getImages() != null && !prod.getImages().isEmpty()) {
			    		int imageIndex = 1;
			    		for (ProductImage image : prod.getImages()) {
			    			map.put("image"+imageIndex++, image.getImageUrl());
			    		}
			    	}
					
					map.put("productId", webJaguar.getProductIdBySku(prod.getSku()));
					breadCrumbs.addAll(prod.getBreadCrumb());
					
					// categories
					List<Integer> catIds = new ArrayList<Integer>();
					boolean productAdded = false;
					for(String breadCrumb : prod.getBreadCrumb()) {
						
						if(categoryMap.containsKey(breadCrumb)) {
							if(!productAdded) {
								data.add(map);
								if (map.get("productId") == null) {
									// new item
									Product newProduct = new Product(prod.getSku(), "Feed");
									webJaguar.insertProduct(newProduct, null);
									map.put("productId", newProduct.getId());
								}
								productAdded = true;
							}
							catIds.add(categoryMap.get(breadCrumb).getCatId());
						}	
					}
					// category mapping
					for (Integer catId: catIds) {
						HashMap<String, Object> mapCategory = new HashMap<String, Object>();
						mapCategory.put("productId", map.get("productId"));
						mapCategory.put("categoryId", catId);
						categoryData.add(mapCategory);
					}
				}
			}
	    }
	    
	    public void endElement(String nsURI, String strippedName, String tagName) throws SAXException {
    		
	    	if (tagName.equalsIgnoreCase("product")) {	
	    		product = false;
	    		xml = new StringBuffer();
	     		if (data.size() == 500) {
					// products
					updateProduct(data);
					data.clear();
					map.clear();
				}
	     		if (categoryData.size() == 500) {
					// product category
					updateProductCategory(categoryData);
					categoryData.clear();
				}
	    	}
	    }
	    
	    public void warning(SAXParseException e) throws SAXException {
	        System.out.println("Warning: " + e.getMessage());
	    }

	    private void updateProduct(List<Map <String, Object>> data) {
	    	// update Products
	    	webJaguar.nonTransactionSafeUpdateWebjaguarProduct(getCsvFeedList(), data);
	    }
	    
	    private void updateProductCategory(List<Map <String, Object>> categoryData) {
	    	// update Product Category
	    	webJaguar.nonTransactionSafeInsertCategories(categoryData);		
		}
	    
	    private void updateCategories(Set<String> breadCrumb) {
	    	//get all possible breadcrumb
	    	getAllBreadCrumbs(breadCrumb);
	    	
	    	// update Categories
	    	List<WebjaguarCategory> wjCategoryList = new ArrayList<WebjaguarCategory>();
			WebjaguarCategory wjCategory = null;
	    	for(String category : breadCrumb) {
	    		String[] categories = category.split("-->");
		    	if(categories.length > 0) {
					wjCategory = new WebjaguarCategory();
		    		wjCategory.setBreadCrumb(category.trim());
		    		wjCategory.setCat(categories[categories.length - 1].trim());
		    
		    		wjCategoryList.add(wjCategory);
				}
	    	}
	    	webJaguar.nonTransactionSafeInsertWebjaguarCategories(wjCategoryList);
	    }
	    
	    private void getAllBreadCrumbs(Set<String> breadCrumb) {
	    	Set<String> breadCrumbTemp = new TreeSet<String>();
	    	for(String category : breadCrumb) {
	    		String[] categories = category.split("-->");
		    	if(categories.length > 0) {
		    	  StringBuffer sb = new StringBuffer();
		    	  sb.append(categories[0].trim());
		    	  breadCrumbTemp.add(sb.toString());
				  for(int i=0; i< categories.length-1; i++) {
					  breadCrumbTemp.add(sb.append("-->"+categories[i+1].trim()).toString());
				  }
		    	}
	    	}
	    	
	    	breadCrumb.clear();
	    	breadCrumb.addAll(breadCrumbTemp);
	    }
	    
	    private void deleteProduct(List<String> data) {
	    	// inactive Products
	        // webJaguar.nonTransactionSafeInactiveASIFileFeedProduct(data, "asi_unique_id");
	    }
	    
	    private List<CsvFeed> getCsvFeedList() {
			List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
			
			csvFeedList.add(new CsvFeed("name"));
			csvFeedList.add(new CsvFeed("sku"));
			csvFeedList.add(new CsvFeed("master_sku"));
			csvFeedList.add(new CsvFeed("feed_freeze"));
			csvFeedList.add(new CsvFeed("short_desc"));
			csvFeedList.add(new CsvFeed("long_desc"));
			csvFeedList.add(new CsvFeed("keywords"));
			csvFeedList.add(new CsvFeed("weight"));
			csvFeedList.add(new CsvFeed("msrp"));
			csvFeedList.add(new CsvFeed("hide_msrp"));
			csvFeedList.add(new CsvFeed("price_by_customer"));
			csvFeedList.add(new CsvFeed("price_1"));
			csvFeedList.add(new CsvFeed("price_2"));
			csvFeedList.add(new CsvFeed("price_3"));
			csvFeedList.add(new CsvFeed("price_4"));
			csvFeedList.add(new CsvFeed("price_5"));
			csvFeedList.add(new CsvFeed("price_6"));
			csvFeedList.add(new CsvFeed("price_7"));
			csvFeedList.add(new CsvFeed("price_8"));
			csvFeedList.add(new CsvFeed("price_9"));
			csvFeedList.add(new CsvFeed("price_10"));
			csvFeedList.add(new CsvFeed("qty_break_1"));
			csvFeedList.add(new CsvFeed("qty_break_2"));
			csvFeedList.add(new CsvFeed("qty_break_3"));
			csvFeedList.add(new CsvFeed("qty_break_4"));
			csvFeedList.add(new CsvFeed("qty_break_5"));
			csvFeedList.add(new CsvFeed("qty_break_6"));
			csvFeedList.add(new CsvFeed("qty_break_7"));
			csvFeedList.add(new CsvFeed("qty_break_8"));
			csvFeedList.add(new CsvFeed("qty_break_9"));
			csvFeedList.add(new CsvFeed("quote"));
			for(ProductField field : productFields) {
				if(field.isEnabled()) {
					csvFeedList.add(new CsvFeed("field_"+field.getId()));
				}
			}
			csvFeedList.add(new CsvFeed("minimum_qty"));
			csvFeedList.add(new CsvFeed("protected_level"));
			csvFeedList.add(new CsvFeed("packing"));
			csvFeedList.add(new CsvFeed("login_require"));
			csvFeedList.add(new CsvFeed("hide_price"));
			csvFeedList.add(new CsvFeed("hide_header"));
			csvFeedList.add(new CsvFeed("hide_topbar"));
			csvFeedList.add(new CsvFeed("hide_leftbar"));
			csvFeedList.add(new CsvFeed("hide_rightbar"));
			csvFeedList.add(new CsvFeed("hide_footer"));
			csvFeedList.add(new CsvFeed("hide_breadcrumbs"));
			csvFeedList.add(new CsvFeed("add_to_list"));
			csvFeedList.add(new CsvFeed("compare"));
			csvFeedList.add(new CsvFeed("viewed"));
			csvFeedList.add(new CsvFeed("taxable"));
			csvFeedList.add(new CsvFeed("searchable"));
			csvFeedList.add(new CsvFeed("inventory"));
			csvFeedList.add(new CsvFeed("inventory_afs"));
			csvFeedList.add(new CsvFeed("neg_inventory"));
			csvFeedList.add(new CsvFeed("show_neg_inventory"));
			csvFeedList.add(new CsvFeed("custom_shipping_enabled"));
			csvFeedList.add(new CsvFeed("subscription"));
			csvFeedList.add(new CsvFeed("active"));
			csvFeedList.add(new CsvFeed("enable_rate"));
			csvFeedList.add(new CsvFeed("sitemap_priority"));
			csvFeedList.add(new CsvFeed("recommended_list"));
			csvFeedList.add(new CsvFeed("recommended_list_title"));
			csvFeedList.add(new CsvFeed("recommended_list_display"));
			csvFeedList.add(new CsvFeed("option_code"));
			csvFeedList.add(new CsvFeed("cross_items_code"));
			csvFeedList.add(new CsvFeed("feed"));
			csvFeedList.add(new CsvFeed("feed_new"));
			csvFeedList.add(new CsvFeed("feed_id"));
			
			return csvFeedList;
		}
	 }
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "jwalant@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 

}