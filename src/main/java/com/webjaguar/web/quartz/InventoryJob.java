/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.20.2008
 */

package com.webjaguar.web.quartz;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.web.admin.inventory.utility.InventoryUtility;
import com.webjaguar.web.domain.KeyBean;

public class InventoryJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	/*
	 * To send Low Inventory Products: 
	 *  
	 */
	
	public void lowInventory() {
		
		
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		if ((Boolean) gSiteConfig.get("gINVENTORY")) {
			InventoryUtility inventory = new InventoryUtility(siteConfig);
			inventory.getLowInventory();
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n");
		notifyAdmin("Success: Inventory getLowInventory() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
	
	/*
	 * To update inventory: 
	 * Create a csv file with columns: SKU, Inventory (Eg: ABCD,10) and upload it to the ftp. 
	 * Update Mas90 field in siteConfig with this format: ftp_domainName,ftp_username,ftp_password,(if site is on wjserver500 use port number too)port number 
	 * Once data is imported file(inventory.csv) is deleted from the server. 
	 */
	public void updateInventory() {
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		InventoryUpdate invUpdate = new InventoryUpdate(siteConfig, webJaguar, mailSender);
		invUpdate.updateInventory();
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}
