/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 03.03.2010
 */

package com.webjaguar.web.quartz;

import java.text.SimpleDateFormat;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.web.form.crm.CrmTaskForm;

public class CrmTaskReminderJob extends QuartzJobBean{
	
	private WebJaguarFacade webJaguar;
	private JavaMailSenderImpl mailSender2;
	private GlobalDao globalDao;	
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	private String adminEmail;
	private String userEmail;
	
	protected void executeInternal(JobExecutionContext context) {
		CrmTaskForm crmTaskForm = (CrmTaskForm)context.getJobDetail().getJobDataMap().get("crmTaskForm");

		try {
			webJaguar = (WebJaguarFacade)context.getScheduler().getContext().get("webJaguar");
			mailSender2 = (JavaMailSenderImpl)context.getScheduler().getContext().get("mailSender2");
			globalDao = (GlobalDao)context.getScheduler().getContext().get("globalDao");
			
			siteConfig = this.webJaguar.getSiteConfig();
			gSiteConfig = this.globalDao.getGlobalSiteConfig();

		} catch (SchedulerException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		adminEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();;
		
		AccessUser user = this.webJaguar.getAccessUserById(crmTaskForm.getCrmTask().getAssignedToId());
		if (user != null && user.getEmail() != null){
			userEmail = user.getEmail();
		} else {
			userEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
		}
		// reminder
		sendReminder(crmTaskForm);
	}

	private void sendReminder(CrmTaskForm form) {
		// send email reminder
    	String siteDomain = gSiteConfig.get("gSITE_DOMAIN").toString();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy");
    	try {
			MimeMessage mms = mailSender2.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");

			helper.setTo(userEmail);
			helper.setFrom(adminEmail);
			//removed BCC to admin for now
			//helper.setBcc(adminEmail);
			helper.setSubject("Reminder " + form.getCrmTask().getTitle());
			
			StringBuffer message = new StringBuffer();
			message.append( "<center>" );
			message.append( "<div style=\"background-color:#7F7F5F;margin-bottom:16px;padding:10px 0 10px;width:500px;height:20px;\">" );
			
			message.append( "<div id=\"main_box\" style=\"background-color:#7F7F5F;margin:0;padding:0 0 1px;width:500px;position:relative\">" );
			message.append( "<div id=\"main_header\" style=\"height:37px;width:250px; float:left\" >" );
			message.append( "<div id=\"header_left\" style=\"text-align:left;padding-left:10px;color:#ffffff;font-weight:700;font-size:19px\">Reminder</div>" );
			message.append( "<div id=\"header_right\" style=\"width:250px; float:right\" />" );
			message.append( "</div>" );
			
			message.append( "<div style=\"text-align:left;background-color:#FFFFFF;clear:both;padding-left:15px;margin:10px;\">" );
			message.append( "<table style=\"padding:10px 0;font-family:times new roman,new york,times,serif;\" cellpadding=\"0\" cellspacing=\"0\">" );
			
			message.append( "<tr>" );
			message.append( "<td style=\"color:#686F6E;font-size:13px;text-align:right;vertical-align:top;white-space:nowrap;\">Reminder from:" );
			message.append( "</td>" );
			message.append( "<td style=\"width:10px;\">&nbsp;" );
			message.append( "</td>" );
			message.append( "<td style=\"font-size:13px;text-align:left;vertical-align:top;\">" + siteDomain );
			message.append( "</td>" );
			message.append( "</tr>" );
			
			message.append( "<tr>" );
			message.append( "<td colspan=\"3\">&nbsp;</td>" );
			message.append( "</tr>" );
			
			message.append( "<tr>" );
			message.append( "<td style=\"color:#686F6E;font-size:13px;text-align:right;vertical-align:top;white-space:nowrap;\">Title:" );
			message.append( "</td>" );
			message.append( "<td style=\"width:10px;\">&nbsp;" );
			message.append( "</td>" );
			message.append( "<td style=\"font-size:13px;text-align:left;vertical-align:top;\"><a href=\""+siteConfig.get("SITE_URL").getValue()+"admin/crm/crmTaskForm.jhtm?taskId="+form.getCrmTask().getId()+"\">" + form.getCrmTask().getTitle()+"</a>" );
			message.append( "</td>" );
			message.append( "</tr>" );
			
			message.append( "<tr>" );
			message.append( "<td colspan=\"3\">&nbsp;</td>" );
			message.append( "</tr>" );
			
			message.append( "<tr>" );
			message.append( "<td style=\"color:#686F6E;font-size:13px;text-align:right;vertical-align:top;white-space:nowrap;\">Account :" );
			message.append( "</td>" );
			message.append( "<td style=\"width:10px;\">&nbsp;" );
			message.append( "</td>" );
			message.append( "<td style=\"font-size:13px;text-align:left;vertical-align:top;\">" + (( form.getCrmTask().getAccountName() == null ) ? "" : form.getCrmTask().getAccountName()) );
			message.append( "</td>" );
			message.append( "</tr>" );
			
			message.append( "<tr>" );
			message.append( "<td style=\"color:#686F6E;font-size:13px;text-align:right;vertical-align:top;white-space:nowrap;\">Contact :" );
			message.append( "</td>" );
			message.append( "<td style=\"width:10px;\">&nbsp;" );
			message.append( "</td>" );
			message.append( "<td style=\"font-size:13px;text-align:left;vertical-align:top;\">" + (( form.getCrmTask().getContactName() == null ) ? "" : form.getCrmTask().getContactName()) );
			message.append( "</td>" );
			message.append( "</tr>" );
			
			message.append( "<tr>" );
			message.append( "<td style=\"color:#686F6E;font-size:13px;text-align:right;vertical-align:top;white-space:nowrap;\">Due Date:" );
			message.append( "</td>" );
			message.append( "<td style=\"width:10px;\">&nbsp;" );
			message.append( "</td>" );
			message.append( "<td style=\"font-size:13px;text-align:left;vertical-align:top;\">" + (( form.getCrmTask().getDueDate() == null ) ? "" : dateFormatter.format(form.getCrmTask().getDueDate())) );
			message.append( "</td>" );
			message.append( "</tr>" );
			
			message.append( "<tr>" );
			message.append( "<td style=\"color:#686F6E;font-size:13px;text-align:right;vertical-align:top;white-space:nowrap;\">Assigned To:" );
			message.append( "</td>" );
			message.append( "<td style=\"width:10px;\">&nbsp;" );
			message.append( "</td>" );
			message.append( "<td style=\"font-size:13px;text-align:left;vertical-align:top;\">" + form.getCrmTask().getAssignedTo() );
			message.append( "</td>" );
			message.append( "</tr>" );
			
			message.append( "<tr>" );
			message.append( "<td style=\"color:#686F6E;font-size:13px;text-align:right;vertical-align:top;white-space:nowrap;\">Email:" );
			message.append( "</td>" );
			message.append( "<td style=\"width:10px;\">&nbsp;" );
			message.append( "</td>" );
			message.append( "<td style=\"font-size:13px;text-align:left;vertical-align:top;\">" + (( form.getCrmTask().getContactEmail() == null ) ? "" : form.getCrmTask().getContactEmail())  );
			message.append( "</td>" );
			message.append( "</tr>" );
			
			message.append( "<tr>" );
			message.append( "<td style=\"color:#686F6E;font-size:13px;text-align:right;vertical-align:top;white-space:nowrap;\">Phone:" );
			message.append( "</td>" );
			message.append( "<td style=\"width:10px;\">&nbsp;" );
			message.append( "</td>" );
			message.append( "<td style=\"font-size:13px;text-align:left;vertical-align:top;\">" + ((form.getCrmTask().getContactPhone() == null ) ? "" : form.getCrmTask().getContactPhone()) );
			message.append( "</td>" );
			message.append( "</tr>" );
			
			message.append( "<tr>" );
			message.append( "<td colspan=\"3\">&nbsp;</td>" );
			message.append( "</tr>" );
			
			message.append( "<tr>" );
			message.append( "<td style=\"color:#686F6E;font-size:13px;text-align:right;vertical-align:top;white-space:nowrap;\">Note:" );
			message.append( "</td>" );
			message.append( "<td style=\"width:10px;\">&nbsp;" );
			message.append( "</td>" );
			message.append( "<td style=\"font-size:13px;text-align:left;vertical-align:top;\">" + form.getCrmTask().getDescription());
			message.append( "</td>" );
			message.append( "</tr>" );
			
			message.append( "</table>" );
			message.append( "</div>" );
			
			message.append( "</div>" );
			message.append( "</center>" );
			helper.setText( message.toString(), true );
			
			mailSender2.send(mms);
		}  catch (Exception e) {
			e.printStackTrace();
		}
	}
}