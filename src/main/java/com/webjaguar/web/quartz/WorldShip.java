/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 09.24.2008
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.dao.StagingDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UpsOrder;
import com.webjaguar.model.WorldShipTrackcode;


public class WorldShip extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
    private StagingDao stagingDao;
	public void setStagingDao(StagingDao stagingDao) { this.stagingDao = stagingDao; }
	private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	Map<String, Object> gSiteConfig;
	Map<String, Configuration> siteConfig;
	
	public void importTrackingNumber() {
		gSiteConfig = this.globalDao.getGlobalSiteConfig();
		siteConfig = this.webJaguar.getSiteConfig();
		
		
		List<Integer> validOrderIds = new ArrayList<Integer>();
		try {
			List<WorldShipTrackcode> orderWithTrackcode = this.stagingDao.getWorldShipTrackcode();
			for (WorldShipTrackcode worldShipTrackcode : orderWithTrackcode) {
				boolean isValidOrderId = this.webJaguar.isValidOrderId( worldShipTrackcode.getOrderId() );
				if ( isValidOrderId ) {
					try {
						validOrderIds.add( Integer.parseInt(worldShipTrackcode.getOrderId()) );  
					}catch (Exception e){ e.printStackTrace();}
					OrderStatus orderStatus = new OrderStatus();
					orderStatus.setOrderId( Integer.parseInt( worldShipTrackcode.getOrderId() ) );
					orderStatus.setComments( "WorldShip -- " + new Date() + (worldShipTrackcode.getTrackNumVoid().equals( "Y" ) ? " -- Void -- " : "") );
					orderStatus.setStatus( "s" );
					orderStatus.setTrackNum( worldShipTrackcode.getTrackNum() );
					orderStatus.setUsername("WorldShip");
					Order order = this.webJaguar.getOrder(Integer.parseInt( worldShipTrackcode.getOrderId() ) , null);
					
					// notify user
					if (siteConfig.get( "WORLD_SHIP_EMAIL_NOTIFICATION" ).getValue().equals("true")) {
						SiteMessage siteMessage = null;
						try {
							Integer messageId = null;
							messageId = Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_WORLD_SHIP" ).getValue() );								
							siteMessage = this.webJaguar.getSiteMessageById( messageId );
						} catch (NumberFormatException e) {
							// do nothing
						}
						if (siteMessage != null) {
							orderStatus.setSubject( siteMessage.getSubject() );
							orderStatus.setMessage( siteMessage.getMessage() );
							orderStatus.setHtmlMessage( siteMessage.isHtml() );
							// multi store
							List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
							MultiStore multiStore = null;
							if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
								multiStore = multiStores.get(0);
							}
							if (notifyCustomer( orderStatus, order, multiStore )){
								orderStatus.setUserNotified(true);
							}
						}
					}					
					this.webJaguar.insertOrderStatus( orderStatus );	
					
					if(siteConfig.get("WORLD_SHIP_IMPORT_ORDERS").getValue().equals("true")) {
						UpsOrder upsWorldShip = new UpsOrder();
						upsWorldShip.setTrackingNumber(worldShipTrackcode.getTrackNum());
						upsWorldShip.setOrderId(new Integer (worldShipTrackcode.getOrderId()));
						this.webJaguar.updateUpsOrdersTrackAndShipDate(upsWorldShip);	    				
	    			}
					
					if((Boolean) gSiteConfig.get("gINVENTORY")) {
						// Generates  packingList 
			    		if ( !order.isProcessed() ) {  			
							this.webJaguar.generatePackingList(order, false, new Date(), siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"));
			    		}
			    		// adds ship date to packinglist without shipdate and adjusts on hand
			    		this.webJaguar.ensurePackingListShipDate(order, siteConfig.get("INVENTORY_HISTORY").getValue().equals("true"), 0);
					}
				}

			}
		} catch (Exception e) {
			notifyAEM(gSiteConfig, e.toString() );
		}
		if(!validOrderIds.isEmpty()) {
			this.stagingDao.updateWorldShipProcessed(validOrderIds);		
		}
	}
	
	private boolean notifyCustomer(OrderStatus orderStatus, Order order, MultiStore multiStore)
	{
		Customer customer = this.webJaguar.getCustomerById(order.getUserId());
		
		if (!customer.isEmailNotify()) {
			// do not send email
			return false;
		}
		
		SalesRep salesRep = null;
		if ( (Boolean) gSiteConfig.get( "gSALES_REP" ) && order.getSalesRepId() != null ) {
			salesRep = this.webJaguar.getSalesRepById( order.getSalesRepId() );
		}
		// send email
		String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
		String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append( dateFormatter.format( new Date() ) + "\n\n" );

		// replace dyamic elements
		try {
			this.webJaguar.replaceDynamicElement( orderStatus, customer, salesRep, order, secureUrl, null);
		} catch (Exception e) {e.printStackTrace();}
		messageBuffer.append( orderStatus.getMessage() );

		try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.setBcc(contactEmail);
			if ( salesRep != null )
				helper.setCc( salesRep.getEmail() );
			helper.setSubject( orderStatus.getSubject() );
			helper.setText( messageBuffer.toString(), orderStatus.isHtmlMessage() );
			mailSender.send(mms);
		} catch (Exception ex) {
			notifyAEM(gSiteConfig, ex.toString() );
			return false;
		}
		return true;
	}
	
	// SuperiorWasher
	public void importedUpsOredrs() throws NumberFormatException, IOException, ParseException {
		gSiteConfig = this.globalDao.getGlobalSiteConfig();
		siteConfig = this.webJaguar.getSiteConfig();
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		File file = null;			
		File wsOrders = new File(getServletContext().getRealPath("/wsOrders/"));
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				wsOrders = new File((String) prop.get("site.root"), "wsOrders");
			}
		} catch (Exception e) {}  
		
		String[] files = new String[] {"EXPORT.CSV","exportsc.csv"};
		int i = 0;
		while (i < 2) {
			try {
				file = new File(wsOrders, files[i++]);
				CSVReader reader = new CSVReader(new FileReader(file));
				UpsOrder upsWorldShip = new UpsOrder();
				String [] nextLine;
				while ((nextLine = reader.readNext()) != null) {
					upsWorldShip.setAccountNumber(nextLine[0]);
					upsWorldShip.setAddedBy(file.getName());
					upsWorldShip.setCompanyName(nextLine[1]);
					String dateStr =nextLine[2];
					SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddhhmmss"); 
					Date date = formatter.parse(dateStr);
					upsWorldShip.setOrderShipDate(date);
					upsWorldShip.setCustomerPO(nextLine[3]);
					upsWorldShip.setTrackingNumber(nextLine[4]);
					upsWorldShip.setAddr1(nextLine[5]);
					upsWorldShip.setCity(nextLine[6]);
					upsWorldShip.setStateProvince(nextLine[7]);
					upsWorldShip.setWeight(new Double (nextLine[8]));
					upsWorldShip.setNumberPackages(new Integer (nextLine[9]));
					upsWorldShip.setShippingCompany(nextLine[10]);
					upsWorldShip.setLineItems(nextLine[11]);
					this.webJaguar.importUpsOrders(upsWorldShip);
				}
			} catch (FileNotFoundException e) {
				notifyAEM(gSiteConfig, e.toString() );
			} catch (Exception ex) {
				notifyAEM(gSiteConfig, ex.toString() );
			} finally {
				SimpleDateFormat dateFormatter2 = new SimpleDateFormat("yyyy-MMM-d-HH");
				File file2 = new File(wsOrders, file.getName()+dateFormatter2.format(new Date()));
				Boolean success = file.renameTo(file2);
				if (!success) {
					notifyAEM(gSiteConfig, "File cannot be renamed" );
				}
			}
			
			Date end = new Date();
			sbuff.append("Started : " + dateFormatter.format(start) + "\n");
			sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
			notifyAEM(gSiteConfig, "read UPS files - WorldShipOrder on" + siteConfig.get("SITE_URL").getValue());
		}
	}
	
	private void notifyAEM(Map<String, Object> gSiteConfig, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	String siteDomain = gSiteConfig.get("gSITE_DOMAIN").toString();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("WorldShip on " + siteDomain);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
}
