/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.03.2008
 */

package com.webjaguar.web.quartz;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;

public class ConcordJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void updateInventory() {

		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// CONCORD configuration
		String[] config = siteConfig.get("CONCORD").getValue().split(",");
		
		if (!(gSiteConfig.get("gACCOUNTING").equals("CONCORD") && config[0].length() > 0 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if concord feature is disabled 
			return;
		}				
		
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		csvFeedList.add(new CsvFeed("sku"));
		csvFeedList.add(new CsvFeed("name"));
		csvFeedList.add(new CsvFeed("case_content"));
		csvFeedList.add(new CsvFeed("packing"));
		csvFeedList.add(new CsvFeed("weight"));
		csvFeedList.add(new CsvFeed("keywords"));
		csvFeedList.add(new CsvFeed("feed"));
		csvFeedList.add(new CsvFeed("feed_new"));
		csvFeedList.add(new CsvFeed("price_1"));
		csvFeedList.add(new CsvFeed("inventory"));
		csvFeedList.add(new CsvFeed("field_1"));
		csvFeedList.add(new CsvFeed("field_2"));
		csvFeedList.add(new CsvFeed("field_3"));
		csvFeedList.add(new CsvFeed("field_4"));
		csvFeedList.add(new CsvFeed("field_5"));
		csvFeedList.add(new CsvFeed("field_6"));
		csvFeedList.add(new CsvFeed("field_7"));
		csvFeedList.add(new CsvFeed("field_8"));
		csvFeedList.add(new CsvFeed("field_9"));
		csvFeedList.add(new CsvFeed("field_10"));
		csvFeedList.add(new CsvFeed("field_11"));
		csvFeedList.add(new CsvFeed("field_12"));
		// for master site
		if (config[0].equals("http://www.dollaritem.com/xml/default.asp")) {
			csvFeedList.add(new CsvFeed("field_13"));			
			csvFeedList.add(new CsvFeed("field_14"));
			csvFeedList.add(new CsvFeed("field_15"));
			csvFeedList.add(new CsvFeed("field_16"));
		}
		if (config[0].equals("http://www.dollaritem.com/xml/default.asp") || ((String) gSiteConfig.get( "gDATA_FEED" )).contains("newEggMall,")) {
			// NewEggMall 
			csvFeedList.add(new CsvFeed("field_17"));
			csvFeedList.add(new CsvFeed("field_18"));
		}
		
		// extra concord configuration
		for (int i=1; i<config.length; i++) {
			csvFeedList.add(new CsvFeed(config[i].substring(0, config[i].indexOf("="))));		
		}
		
		Double priceChange = null;
		try {
			priceChange = Double.parseDouble(siteConfig.get("CONCORD_PRICECHANGE").getValue());			
		} catch (NumberFormatException e) {
			// do nothing
		}
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		// NewEggMall 
		Map <String, Map<String, String>> newEggMallMap = this.webJaguar.getCategoryExternalIdNewEggMap();
		
		try {
			URL url = new URL(config[0]);

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(url);
	
			Element root = doc.getRootElement();
			Iterator<Element> results = root.getChildren("Catalog_Item").iterator();
			while (results.hasNext()) {
				HashMap<String, Object> map = new HashMap<String, Object>();

				Element item = results.next();
				
				// sku
				String sku = item.getAttributeValue("value");
				map.put("sku", sku);

				map.put("name", item.getChild("char_Description_English").getText());	
				map.put("field_1", item.getChild("drop_Category").getText());
				map.put("field_2", item.getChild("drop_Subcategory").getText());
				map.put("field_3", item.getChild("int_Pieces_Per_Case").getText());
				map.put("case_content", item.getChild("int_Pieces_Per_Case").getText());
				map.put("packing", "Cs");
				map.put("field_4", item.getChild("int_Pieces_Per_Inner_Box").getText());
				map.put("field_5", item.getChild("weight").getText());	
				map.put("weight", item.getChild("weight").getText());	
				map.put("field_6", "");	
				map.put("field_7", item.getChild("width").getText() + " x " +
								   item.getChild("length").getText() + " x " +
								   item.getChild("height").getText());	
				map.put("field_8", item.getChild("cube").getText());	
				map.put("field_9", item.getChild("char_Country_Origin").getText());	
				map.put("field_10", item.getChild("char_UPC_Number").getText());
				map.put("field_11", item.getChild("sales_price_36").getText());
				map.put("field_12", item.getChild("drop_Keyword").getText());
				// keywords
				map.put("keywords", (" " 
						+ sku + " "
						+ item.getChild("drop_Keyword").getText() + " "
						+ item.getChild("drop_Category").getText() + " " 
						+ item.getChild("drop_Subcategory").getText() + " "
						+ item.getChild("char_Description_English").getText() + " ").replace(" ", ","));
				map.put("feed", "CONCORD");				
				map.put("feed_new", true);
				
				// image
				if (item.getChild("bool_Has_Picture").getText().equalsIgnoreCase("True")) {
					map.put("image", "http://www.dollaritem.com/wms/images/catalog/" + sku + ".jpg");
				}
				
				if (priceChange != null) {
					try {
						Double price = Double.parseDouble(item.getChild("sales_price_36").getText());
						price = price + price*priceChange/100;
						map.put("price_1", price);						
					} catch (NumberFormatException e) {
						map.put("price_1", null);
					}
				} else {
					map.put("price_1", null);					
				}
				
				// inventory
				try {
					map.put("inventory", Integer.parseInt(item.getChild("qtyoh").getText()));						
				} catch (NumberFormatException e) {
					map.put("inventory", null);
				}
				
				// for master site
				if (config[0].equals("http://www.dollaritem.com/xml/default.asp")) {
					map.put("field_13", item.getChild("bool_Has_Picture").getText());
					map.put("field_14", item.getChild("width").getText());
					map.put("field_15", item.getChild("length").getText());
					map.put("field_16", item.getChild("height").getText());
					// NewEggMall 
					if (newEggMallMap.containsKey(item.getChild("char_Subcategory_Abbrev").getText().toLowerCase())) {
						map.put("field_17", newEggMallMap.get(item.getChild("char_Subcategory_Abbrev").getText().toLowerCase()).get("nem_industry"));
						map.put("field_18", newEggMallMap.get(item.getChild("char_Subcategory_Abbrev").getText().toLowerCase()).get("nem_category"));						
					}
				}
				
				// extra concord configuration
				for (int i=1; i<config.length; i++) {
					map.put(config[i].substring(0, config[i].indexOf("=")), config[i].substring(config[i].indexOf("=")+1));
				}
				
				if (config[0].equals("http://concord.wjserver10.com/product-xml.jsp") 
							&& ((String) gSiteConfig.get("gDATA_FEED")).contains("newEggMall,")) {
					// NewEggMall 
					map.put("field_17", item.getChild("NewEggMallIndustry").getText());
					map.put("field_18", item.getChild("NewEggMallCategory").getText());						
				}
				
				data.add(map);
			}
			
			// update
			if (!data.isEmpty()) {
				this.webJaguar.updateConcordProduct(csvFeedList, data);
			}
			
		} catch (Exception e) {
			notifyAdmin("Concord updateInventory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}

	}
	
	public void updateCategory() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		if (!(gSiteConfig.get("gACCOUNTING").equals("CONCORD") && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if concord feature is disabled 
			return;
		}
		
		Map <Long, List<Long>> data = new HashMap<Long, List<Long>>();
		Map <String, Long> productMap = this.webJaguar.getProductSkuMap();
		Map <String, Long> categoryMap = this.webJaguar.getCategoryExternalIdMap();
		
		try {
			URL url = new URL("http://www.dollaritem.com/xml/default.asp");

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(url);
	
			Element root = doc.getRootElement();
			Iterator<Element> results = root.getChildren("Catalog_Item").iterator();
			while (results.hasNext()) {
				Element item = results.next();

				String sku = item.getAttributeValue("value");
				if (productMap.containsKey(sku.toLowerCase())) {
					// valid item
					Long productId = productMap.get(sku.toLowerCase());
					List<Long> category = new ArrayList<Long>();

					if (categoryMap.containsKey(item.getChild("char_Category_Abbrev").getText().toLowerCase())) {
						category.add(categoryMap.get(item.getChild("char_Category_Abbrev").getText().toLowerCase()));						
					}
					if (categoryMap.containsKey(item.getChild("char_Subcategory_Abbrev").getText().toLowerCase())) {
						category.add(categoryMap.get(item.getChild("char_Subcategory_Abbrev").getText().toLowerCase()));
					}
					data.put(productId, category);
				}
			}	
		} catch (Exception e) {
			notifyAdmin("Concord updateCategory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
		}
			
		// update only if data is not empty
		if (!data.isEmpty()) {
			this.webJaguar.updateProductCategory(data);
		}
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}  
	
}
