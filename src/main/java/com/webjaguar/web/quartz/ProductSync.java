/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 06.05.2009
 */

package com.webjaguar.web.quartz;

import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;


public class ProductSync extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void syncProduct() {
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		
		getSalesTag();
		getProduct();
	}
	
	private int getSalesTag() {
		int quantity = 0;
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		String[] config = siteConfig.get("PRODUCT_SYNC").getValue().split(",");
		
		if (config.length < 1 || config[3].equals("")) {
			// return if product sync feature is disabled 
			return 0;
		}				
		
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		csvFeedList.add(new CsvFeed("tag_id"));
		csvFeedList.add(new CsvFeed("title"));
		csvFeedList.add(new CsvFeed("discount"));
		csvFeedList.add(new CsvFeed("start_date"));
		csvFeedList.add(new CsvFeed("end_date"));
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		
		try {
			URL url = new URL(config[0]+config[3]);

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(url);
	
			Element root = doc.getRootElement();
			Iterator<Element> results = root.getChildren("SalesTag").iterator();
			
			while (results.hasNext()) {
				quantity++;
				HashMap<String, Object> map = new HashMap<String, Object>();

				Element item = results.next();
				map.put("tag_id", item.getChild("TagId").getText() );
				map.put("title", item.getChild("Title").getText());
				map.put("discount", item.getChild("Discount").getText());	
				map.put("start_date", item.getChild("StartDate").getText());
				map.put("end_date", item.getChild("EndDate").getText());
				data.add(map);
			}
			
			// update
			if (!data.isEmpty()) {
				this.webJaguar.updateSyncSalesTag(csvFeedList, data);
			}
			
		} catch (Exception e) {
			notifyAdmin("Sync getSalesTag() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
		}
		
		return quantity;
	}
	public int getProduct() {
		int quantity = 0;
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		String[] config = siteConfig.get("PRODUCT_SYNC").getValue().split(",");
		
		if (config.length < 1 || config[0].equals("")) {
			// return if product sync feature is disabled 
			return 0;
		}				
		
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		try {
			if (config[4].equalsIgnoreCase("exactive")) {
				// don't update 'active'
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			csvFeedList.add(new CsvFeed("active"));
		}
		
		csvFeedList.add(new CsvFeed("sku"));
		csvFeedList.add(new CsvFeed("name"));
		csvFeedList.add(new CsvFeed("also_consider"));
		csvFeedList.add(new CsvFeed("case_content"));
		csvFeedList.add(new CsvFeed("short_desc"));
		csvFeedList.add(new CsvFeed("long_desc"));
		csvFeedList.add(new CsvFeed("packing"));
		csvFeedList.add(new CsvFeed("weight"));
		csvFeedList.add(new CsvFeed("keywords"));
		csvFeedList.add(new CsvFeed("protected_level"));
		csvFeedList.add(new CsvFeed("feed"));
		csvFeedList.add(new CsvFeed("feed_new"));
		csvFeedList.add(new CsvFeed("msrp"));
		csvFeedList.add(new CsvFeed("price_1"));
		csvFeedList.add(new CsvFeed("sales_tag_id"));
		csvFeedList.add(new CsvFeed("option_code"));
		csvFeedList.add(new CsvFeed("inventory"));
		csvFeedList.add(new CsvFeed("field_1"));
		csvFeedList.add(new CsvFeed("field_2"));
		csvFeedList.add(new CsvFeed("field_3"));
		csvFeedList.add(new CsvFeed("field_4"));
		csvFeedList.add(new CsvFeed("field_5"));
		csvFeedList.add(new CsvFeed("field_6"));
		csvFeedList.add(new CsvFeed("field_7"));
		csvFeedList.add(new CsvFeed("field_8"));
		csvFeedList.add(new CsvFeed("field_9"));
		csvFeedList.add(new CsvFeed("field_10"));
		csvFeedList.add(new CsvFeed("field_11"));
		csvFeedList.add(new CsvFeed("field_12"));
		csvFeedList.add(new CsvFeed("field_13"));			
		csvFeedList.add(new CsvFeed("field_14"));
		csvFeedList.add(new CsvFeed("field_15"));
		csvFeedList.add(new CsvFeed("field_16"));
		csvFeedList.add(new CsvFeed("field_17"));
		csvFeedList.add(new CsvFeed("field_18"));
		csvFeedList.add(new CsvFeed("field_19"));
		csvFeedList.add(new CsvFeed("field_20"));
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		String sku = "";
		try {
			URL url = new URL(config[0]+config[1]);

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(url);
	
			Element root = doc.getRootElement();
			Iterator<Element> results = root.getChildren("Item").iterator();
			
			while (results.hasNext()) {
				quantity++;
				HashMap<String, Object> map = new HashMap<String, Object>();
				Element item = results.next();
				map.put("active", Boolean.parseBoolean( item.getChild("Active").getText() ));
				map.put("sku", item.getChild("Sku").getText());
				sku = item.getChild("Sku").getText();
				map.put("name", URLDecoder.decode(item.getChild("Name").getText(), "UTF-8"));
				map.put("also_consider", item.getChild("AlsoConsider").getText());
				map.put("short_desc", URLDecoder.decode( item.getChild("ShortDescription").getText(), "UTF-8"));
				map.put("long_desc", URLDecoder.decode( item.getChild("LongDescription").getText(), "UTF-8"));
				map.put("packing", item.getChild("Packing").getText());
				map.put("caseContent", item.getChild("CaseContent").getText());
				map.put("weight", item.getChild("Weight").getText());
				map.put("keywords", URLDecoder.decode( item.getChild("Keywords").getText(), "UTF-8"));
				map.put("protected_level", item.getChild("ProtectedLevel").getText());
				map.put("feed", item.getChild("Site").getText());
				map.put("feed_new", true);
				try {
					map.put("msrp", Double.parseDouble( item.getChild("Msrp").getText()));
				} catch (NumberFormatException e) { map.put("msrp", null); }
				try {
					map.put("price_1", Double.parseDouble( item.getChild("Price1").getText()));
				} catch (NumberFormatException e) { map.put("price_1", null); }
				if (item.getChild("SalesTagId").getText().length() > 0) {
					map.put("sales_tag_id", item.getChild("SalesTagId").getText());					
				}
				map.put("option_code", item.getChild("OptionCode").getText());
				// inventory
				try {
					map.put("inventory", Integer.parseInt(item.getChild("Inventory").getText()));						
				} catch (NumberFormatException e) {
					map.put("inventory", null);
				}
				
				Iterator<Element> imageResults = item.getChildren("ImageUrl").iterator();
				int index = 1;
				while (imageResults.hasNext()) {

					Element imageItem = imageResults.next();
					for (index=1; index <=10; index++) {
						try {
							map.put("image"+index, config[0] + "assets/Image/Product/detailsbig/" + imageItem.getChild("Image"+index).getText());
						} catch (Exception e) {
							break;
						}
					}
				}
				
				map.put("field_1", item.getChild("Field1").getText());
				map.put("field_2", item.getChild("Field2").getText());
				map.put("field_3", item.getChild("Field3").getText());
				map.put("field_4", item.getChild("Field4").getText());
				map.put("field_5", item.getChild("Field5").getText());
				map.put("field_6", item.getChild("Field6").getText());
				map.put("field_7", item.getChild("Field7").getText());
				map.put("field_8", item.getChild("Field8").getText());
				map.put("field_9", item.getChild("Field9").getText());
				map.put("field_10", item.getChild("Field10").getText());
				map.put("field_11", item.getChild("Field11").getText());
				map.put("field_12", item.getChild("Field12").getText());
				map.put("field_13", item.getChild("Field13").getText());
				map.put("field_14", item.getChild("Field14").getText());
				map.put("field_15", item.getChild("Field15").getText());
				map.put("field_16", item.getChild("Field16").getText());
				map.put("field_17", item.getChild("Field17").getText());
				map.put("field_18", item.getChild("Field18").getText());
				map.put("field_19", item.getChild("Field19").getText());
				map.put("field_20", item.getChild("Field20").getText());
				
				data.add(map);
			}
			
			// update
			if (!data.isEmpty()) {
				// removed in FAVOR of webjagaur Feed
				//this.webJaguar.updateSyncProduct(csvFeedList, data, config[2]);
			}
			
		} catch (Exception e) {
			notifyAdmin("Sync getProduct() on sku " + sku + siteConfig.get("SITE_URL").getValue(), e.toString() + "\n sku = " + sku);
			quantity = 0;
		}
		return quantity;
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}  
}
