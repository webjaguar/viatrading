/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.20.2008
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.Payment;
import com.webjaguar.model.PaymentSearch;
import com.webjaguar.thirdparty.tcintlinc.Tcintl;
import com.webjaguar.web.domain.Utilities;

public class TcintlincJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	/*
	 * To update inventory: 
	 * Create a csv file with columns: SKU, Inventory (Eg: ABCD,10) and upload it to the ftp. 
	 * Update Mas90 field in siteConfig with this format: ftp_domainName,ftp_username,ftp_password,invetory.csv,orders.csv,(if site is on wjserver500 use port number too)port number 
	 * Once data is imported file(inventory.csv) is deleted from the server. 
	 */
	
	public void updateInventory() {
		
		
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		
		Tcintl tci = new Tcintl(siteConfig, gSiteConfig);
		tci.updateInventory();
		
	}
	
	/*
	 * Sample orders.csv Format
	 * 1000002,06/08/12,Summary,W000002,,,,,,2815.5,0.0,2892.0,Credit Card,pending,,Shahin,Naji,,US,6B,,Aliso Viejo,CA,92656,9496008868,,,Shahin,Naji,,US,6B,,Aliso Viejo,CA,92656,9496008868,,,Shahin Naji,snaji@advancedemedia.com,Credit Card,,UPS 3 Day Select
	 * 1000002,06/08/12,Item,10-310-1002,Coupling 1/4" HoseBarb,2,,9.25
	 * 1000002,06/08/12,Item,ISCT0500,500VA Industrial Control Transformer,3,,99.0
	 * 1000002,06/08/12,Item,4-54-2000-2,Control Transformers,5,,500.0 
	 */
	public void exportInvoices() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// Mas90 configuration
		String[] config = siteConfig.get("MAS90").getValue().split(",");
		
		if (!(gSiteConfig.get("gACCOUNTING").equals("MAS90") && config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if mas90 feature is disabled 
			return;
		}
		
		Set<Integer> orderIds = new HashSet<Integer>();

		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		File tempFolder = new File(getServletContext().getRealPath("temp"));	
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		
		// get all Shipped Order from day one which the MAS90 flag is false
		OrderSearch search = new OrderSearch();
		search.setMas90(false);
		Date cutoff = new Date();
		search.setEndDate(cutoff);
		search.setStatus("s");
		
		SimpleDateFormat dateFormatter2 = new SimpleDateFormat("MM/dd/yy");
		SimpleDateFormat dateFormatter3 = new SimpleDateFormat("MMddyyyyHHmm");
		DecimalFormat df = new DecimalFormat("#0");
		df.setMinimumIntegerDigits(6);
		DecimalFormat df2 = new DecimalFormat("#0.000");
		
		List<Order> orderList = this.webJaguar.getInvoiceExportList(search, siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());

		try {
			File invoiceFile = new File(tempFolder, "mas90invoice.csv");
			PrintWriter pw = new PrintWriter(new FileWriter(invoiceFile));
			for (Order order: orderList) {
				orderIds.add(order.getOrderId());
				pw.print(order.getOrderId());
				pw.print(",");
				pw.print(dateFormatter2.format(order.getDateOrdered()));
				pw.print(",");
				pw.print("Summary");
				pw.print(",");
				if (order.getUserId() != null) {
					pw.print("W" + df.format(order.getUserId()));
				}
				pw.print(",");
				pw.print(this.webJaguar.getCustomerById(order.getUserId()).getAccountNumber());
				pw.print(",");
				pw.print(",");
				pw.print(",");
				pw.print(",");
				pw.print(",");
				pw.print(",");
				pw.print(order.getSubTotal());
				pw.print(",");
				if (order.getTax() != null) {
					pw.print(order.getTax());
				}
				pw.print(",");
				pw.print(order.getGrandTotal());
				pw.print(",");
				pw.print(order.getPaymentMethod());
				pw.print(",");
				pw.print(getMessageSourceAccessor().getMessage("orderStatus_" + order.getStatus()));
				pw.print(",");
				if (order.getTrackcode() != null) {
					pw.print(order.getTrackcode());
				}
				pw.print(",");
				addressInfo(order.getBilling(), pw);
				// Ship to code
				pw.print(",");
				pw.print(order.getShipping().getCode());
				pw.print(",");
				addressInfo(order.getShipping(), pw);
				pw.print(",");
				pw.print(order.getBilling().getFirstName() + " " + order.getBilling().getLastName());
				pw.print(",");
				pw.print(order.getUserEmail());
				pw.print(",");
				pw.print(order.getPaymentMethod()); // Terms
				pw.print(",");
				if (order.getPurchaseOrder() != null) {
					pw.print(order.getPurchaseOrder());	// PO
				}
				pw.print(",");
				if (order.getShippingMethod() != null) {
					pw.print(order.getShippingMethod()); // Ship Via
				}
				pw.print(",");
				if (order.getShippingCost() != null) {
					pw.print(order.getShippingCost()); // Ship cost
				}
				pw.print(",");
				if (order.getCustomField1() != null) {
					pw.print(order.getCustomField1()); // used for Handling
				}
				pw.println();
				// line items
	    		for (LineItem lineItem : order.getLineItems()) {
	    			pw.print(order.getOrderId());
					pw.print(",");
					pw.print(dateFormatter2.format(order.getDateOrdered()));
					pw.print(",");
					pw.print("Item");
					pw.print(",");
					if (lineItem.getProduct().getSku() != null) {
						pw.print(lineItem.getProduct().getSku().replace(",", " "));
					}
					pw.print(",");
					if (lineItem.getProduct().getName() != null) {
						pw.print(lineItem.getProduct().getName().replace(",", " "));						
					}
					pw.print(",");
					//Original Quantity
					pw.print(lineItem.getOriginalQuantity());
					pw.print(",");
					//Shipped Quantity
					pw.print(lineItem.getQuantity());
					pw.print(",");
					//Case Content
					pw.print(lineItem.getProduct().getCaseContent() !=  null ? lineItem.getProduct().getCaseContent() : 1);
	            	pw.print(",");
		            // Case Content * Shipped Quantity
	            	pw.print((lineItem.getProduct().getCaseContent() !=  null ? lineItem.getProduct().getCaseContent() : 1) * lineItem.getQuantity());
	            	pw.print(",");
		            if (lineItem.getUnitPrice() != null) {
		            	pw.print(df2.format(lineItem.getUnitPrice()));
	            	}
	    			pw.println();
	    		}
			}
			pw.close();
			
			// Connect and logon to FTP Server
    	    FTPClient ftp = new FTPClient();
    	    
    	    // ftp on wjserver500 uses port 2121.
    	    try {
	        	ftp.connect(config[0], Integer.parseInt(config[5]));
	        } catch (ArrayIndexOutOfBoundsException e) {
	        	ftp.connect(config[0]);	        	
	        }
    	    ftp.enterLocalPassiveMode();
    	    
    	    // login
    	    ftp.login( config[1].trim(), config[2].trim() );
    	   
    	    if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
	            // upload file
    	    	FileInputStream fis = new FileInputStream(invoiceFile);
    	    	// config[4] if file name on DB contains XXXXXXXX then add time to the filename. order.csv OR orderXXXXXXXX.csv
    	    	String orderFileName = config[4];
    	    	if (orderFileName.contains("XXXXXXXX")) {
    	    		orderFileName = orderFileName.replace("XXXXXXXX", dateFormatter3.format(new Date()));
    	    	}
       	        if (ftp.storeFile(orderFileName, fis)) {
    	        	this.webJaguar.updateOrderExported("mas90", null, orderIds);
    	        } else {
    	        	notifyAdmin("1- Mas90 exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
    	        }       	
	        } else {
	        	notifyAdmin("2- Mas90 exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
	        } 

	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
			
		} catch (Exception e) {
			e.printStackTrace();
			notifyAdmin("3- Mas90 exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n");
		sbuff.append("Invoices Exported : " + orderList.size() + "\n\n");
		notifyAdmin("Success: Mas90 exportInvoices() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
	
	/*
	 * Sample payments.csv Format
	 * Summary,AccountNumber,10/24/12,Bill Me Later me,Memo,125.60
	 * Items,1000140,25.60
	 * Items,1000170,0.00
	 */
	public void exportPayments() {
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// Mas90 configuration
		String[] config = siteConfig.get("MAS90").getValue().split(",");
		
		if (!(gSiteConfig.get("gACCOUNTING").equals("MAS90") && config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if mas90 feature is disabled 
			return;
		}

		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		File tempFolder = new File(getServletContext().getRealPath("temp"));	
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		
		PaymentSearch search = new PaymentSearch();
		search.setExportSuccess("0");
		
		SimpleDateFormat dateFormatter2 = new SimpleDateFormat("MM/dd/yy");
		SimpleDateFormat dateFormatter3 = new SimpleDateFormat("MMddyyyyHHmm");
		DecimalFormat df = new DecimalFormat("0.00");
		
		List<Payment> paymentList = this.webJaguar.getPaymentExportList(search);
		Set<Integer> paymentIds = new HashSet<Integer>();
		try {
			File invoiceFile = new File(tempFolder, "mas90invoice.csv");
			PrintWriter pw = new PrintWriter(new FileWriter(invoiceFile));
			for (Payment payment: paymentList) {
				paymentIds.add(payment.getId());
				System.out.println("payment.getId()  " + payment.getId());
				pw.print("Summary");
				pw.print(",");
				pw.print(payment.getCustomer().getAccountNumber());
				pw.print(",");
				pw.print(dateFormatter2.format(payment.getDate()));
				pw.print(",");
				pw.print(payment.getPaymentMethod());
				pw.print(",");
				pw.print(payment.getMemo());
				pw.print(",");
				pw.print(df.format(payment.getAmount()));
				pw.println();
				// line items
	    		for (Payment lineItem : payment.getLineItems()) {
					pw.print("Items");
					pw.print(",");
					//OrderId
					pw.print(lineItem.getOrderId());
					pw.print(",");
					//Payment Amount
					pw.print(df.format(lineItem.getAmount()));
	    			pw.println();
	    		}
			}
			pw.close();
			
			
			// Connect and logon to FTP Server
    	    FTPClient ftp = new FTPClient();
    	    
    	    // ftp on wjserver500 uses port 2121.
    	    try {
	        	ftp.connect(config[0], Integer.parseInt(config[5]));
	        } catch (ArrayIndexOutOfBoundsException e) {
	        	ftp.connect(config[0]);	        	
	        }
    	    ftp.enterLocalPassiveMode();
    	    
    	    // login
    	    ftp.login( config[1].trim(), config[2].trim() );
    	   
    	    if (FTPReply.isPositiveCompletion(ftp.getReplyCode())) {
	            // upload file
    	    	FileInputStream fis = new FileInputStream(invoiceFile);
    	    	// config[4] if file name on DB contains XXXXXXXX then add time to the filename. order.csv OR orderXXXXXXXX.csv
    	    	String paymentFileName = "MASPAYMENTS/paymentXXXXXXXX.csv";
    	    	if (paymentFileName.contains("XXXXXXXX")) {
    	    		paymentFileName = paymentFileName.replace("XXXXXXXX", dateFormatter3.format(new Date()));
    	    	}
       	        if (ftp.storeFile(paymentFileName, fis)) {
       	        	if (!paymentIds.isEmpty()) {
       	 			this.webJaguar.updatePaymentExported(paymentIds);
       	 		}
    	        } else {
    	        	notifyAdmin("1- Mas90 exportPayments() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
    	        }       	
	        } else {
	        	notifyAdmin("2- Mas90 exportPayments() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
	        } 

	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
			
		} catch (Exception e) {
			e.printStackTrace();
			notifyAdmin("3- Mas90 exportPayments() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
		}
		
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n");
		sbuff.append("Payments Exported : " + paymentList.size() + "\n\n");
		notifyAdmin("Success: Mas90 exportPayments() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
	
	public void importPayments() {
		
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		Tcintl tci = new Tcintl(siteConfig);
		String realPath = getServletContext().getRealPath("/tci");
		tci.importPayments2(realPath);

	}
	
    private void addressInfo(Address address, PrintWriter pw) {   
    	address.replace(",", " ");
		pw.print(address.getFirstName());
		pw.print(",");
		pw.print(address.getLastName());
		pw.print(",");
		pw.print(address.getCompany());
		pw.print(",");
		pw.print(address.getCountry());
		pw.print(",");
		pw.print(Utilities.limitString(address.getAddr1(), 0, 30, null));
		pw.print(",");
		pw.print(Utilities.limitString(address.getAddr2(), 0, 30, null));
		pw.print(",");
		pw.print(address.getCity());
		pw.print(",");
		pw.print(address.getStateProvince());
		pw.print(",");
		pw.print(address.getZip());
		pw.print(",");
		pw.print(address.getPhone());
		pw.print(",");
		pw.print(address.getCellPhone());
		pw.print(",");
		pw.print(address.getFax());
    }
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}
