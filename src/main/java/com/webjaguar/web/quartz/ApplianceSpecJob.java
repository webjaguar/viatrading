/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 10.21.2009
 */

package com.webjaguar.web.quartz;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;

public class ApplianceSpecJob extends WebApplicationObjectSupport {

	private static WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	int size=2048;
	
	public void downloadApplianceSpecZipFile() {
		System.out.println("downloadApplianceSpecZipFile:" );
		Date start = new Date();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();	
		
		// Appliance Spec configuration
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		String[] config = siteConfig.get("APPLIANCE_SPEC_URL").getValue().split(",");
		
		if ( config.length < 2 || !(Boolean) gSiteConfig.get("gSITE_ACTIVE")) {
			// return if Appliance Spec feature is disabled 
			return;
		}
		
		File tempFolder = new File(getServletContext().getRealPath("temp"));	
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File applianceSpecDir = new File(tempFolder, "appliancespec");
		if (!applianceSpecDir.exists()) {
			applianceSpecDir.mkdir();
		}
		File zip_file = new File(applianceSpecDir, "XML2.ZIP");

		OutputStream outStream = null;
		URLConnection  urlConnection = null;
		InputStream inputStream = null;
		
		try {
			URL Url = new URL(config[0] + "?USER=" + config[1] + "&PASS=" + config[2]);
			byte[] buffer;
			int ByteRead=0;
			outStream = new BufferedOutputStream(new FileOutputStream(zip_file));

			urlConnection = Url.openConnection();
			inputStream = urlConnection.getInputStream();
			buffer = new byte[size];
			while ((ByteRead = inputStream.read(buffer)) != -1) {
				outStream.write(buffer, 0, ByteRead);
			}
			outStream.close();
		} catch (Exception e) {
			e.printStackTrace();
			notifyAdmin("Number1Appliance downloadZipFile() on " + siteConfig.get("SITE_URL").getValue(), e.getMessage());
		}
		 System.out.println("download start:" + start + " end: " + new Date() );
	}
	
	public void processApplianceSpec() {
		System.out.println("processApplianceSpec:" );
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();	
		// Brands need to be imported
		//String brandFileNameName = "AMRG,AIRK,AMN,BER,BLU,BOSCH,BRN,CAPITAL,DAC,DAN,DCS,ELECTROLUX,ICON,FPK,FIV,FRIG,GAG,GE,HLAND,HOT,JAD,JEN,KTA,LG,LBR,MAR,MAY,MIE,NLAND,SAMSUNG,SCO,SHP,SNY,SPQ,THE,ULN,VIK,VHOOD,WHIRL,ZEP";
		String brandFileNameName = siteConfig.get("APPLIANCE_SPEC_BRANDNAME").getValue();

		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File applianceSpecDir = new File(tempFolder, "appliancespec");
		if (!applianceSpecDir.exists()) {
			applianceSpecDir.mkdir();
		}
		
		// unzipfile
		File zip_file = new File(applianceSpecDir, "XML2.ZIP");
		File BY_BRAND_FOLDER = new File(applianceSpecDir,"BY_BRAND"); 
		BY_BRAND_FOLDER.mkdir();
		try {

			ZipFile zf = new ZipFile(zip_file);
			
			// Enumerate each entry
			for (Enumeration entries = zf.entries(); entries.hasMoreElements();) {
				// Get the entry and its name
				ZipEntry zipEntry = (ZipEntry)entries.nextElement();
				if (zipEntry.isDirectory()) {
					// this never detect folder. I have to create BY_BRAND folder manually before for loop.
					boolean success = (new File(zipEntry.getName())).mkdir();
				}
				else {
					String zipEntryName = zipEntry.getName();
					OutputStream out = new FileOutputStream(new File(applianceSpecDir,zipEntryName));
					InputStream in = zf.getInputStream(zipEntry);
					byte[] buf = new byte[size];
					int len;
					while((len = in.read(buf)) > 0) {
						out.write(buf, 0, len);
					}
					// Close streams
					out.close();
					in.close();
				}
			}

		} catch (IOException e) {
			notifyAdmin("Number1Appliance unzipfile on " + siteConfig.get("SITE_URL").getValue(), e.getMessage());
		}
		
		// read files
		File[] listOfFiles = BY_BRAND_FOLDER.listFiles();
		Date start =  new Date();
	    for (int i = 0; i < listOfFiles.length; i++) {
	      if (listOfFiles[i].isFile()) {
	    	  try {
	    		  if (brandFileNameName.contains(listOfFiles[i].getName().toUpperCase().substring(0, listOfFiles[i].getName().toUpperCase().lastIndexOf("XML")-1))) {
	    			  System.out.println("file name " + listOfFiles[i].getName());
	    			  parseXmlFile(listOfFiles[i], siteConfig);
	              }
	    	  } catch (StringIndexOutOfBoundsException e) {
	    		  // take care of .DS_Store
	    	  }
	    	  
	      } else if (listOfFiles[i].isDirectory()) {
	        System.out.println("Directory " + listOfFiles[i].getName());
	      }
	    }
	    System.out.println("process start:" + start + " end: " + new Date() );
	}
	
	public void parseXmlFile(File xml_file, Map<String, Configuration> siteConfig) {
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		
		csvFeedList.add(new CsvFeed("sku"));
		csvFeedList.add(new CsvFeed("name"));
		
		try {
			SAXParserFactory f = SAXParserFactory.newInstance();
	        SAXParser parser = f.newSAXParser();
	        DefaultHandler handler = new MyContentHandler();
	        parser.parse(xml_file,handler);
	    } catch (ParserConfigurationException e) {
	        notifyAdmin("parseXmlFile ApplianceSpec on " + siteConfig.get("SITE_URL").getValue(), e.toString());
	    } catch (SAXException e) {
	    	notifyAdmin("parseXmlFile ApplianceSpec on " + siteConfig.get("SITE_URL").getValue(), e.toString());
	    } catch (IOException e) {
	    	notifyAdmin("parseXmlFile ApplianceSpec on " + siteConfig.get("SITE_URL").getValue(), e.toString());
	    }
	}

	public static class MyContentHandler extends DefaultHandler {
		
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		HashMap<String, Object> map;
		StringBuffer keywords = new StringBuffer();
		StringBuffer featureValue = new StringBuffer();
		StringBuffer specTableHtmlValue = new StringBuffer();
		StringBuffer manuallyAddToSpecHtml = new StringBuffer();
		String pdfDescriptionName = "";
		String pdfValue = "";
		String recommendedList = "";
		int imageIndex = 1;
		int tabIndex = 1;

		boolean product_specs,classification,marketing_copy,features, pdfs, images;
		boolean key, pn, brand_name, short_description, paragraph_description, color_code_description, spec_table_html, 
				feature, width_string, height_string, depth_string, pdfDescription, pdfUrl, file_name, full_size_url, thumbnail_url,
				energy_star_compliant, capacity_in_cu_ft;
	    boolean this_item_in_other_colors;
	    
		public void startDocument() throws SAXException {
	    	// do nothing
	    }
	    
	    public void endDocument() throws SAXException {
	       // for files that has less than 500 product_specs
	       if (data.size() > 0) {
				// products
				updateProduct(data);
				data.clear();
				map.clear();
			}
	    }
	    
	    public void startElement(String ns, String sName, String qName, Attributes attrs) throws SAXException {
	    	
	    	if (qName.equalsIgnoreCase("product_specs")) {
	    		map = new HashMap<String, Object>();
	    		map.put("feed", "APPLIANCE");				
				map.put("feed_new", true);
	    	}
	    	
	    	if (qName.equalsIgnoreCase("classification")) { 
	    		classification = true; 
	    		manuallyAddToSpecHtml.append("<div><ul>");
	    	}
	    	if (qName.equalsIgnoreCase("marketing_copy")) { marketing_copy = true; }
	    	if (qName.equalsIgnoreCase("this_item_in_other_colors")) { this_item_in_other_colors = true; }
	    	
	    	
	    	if (qName.equalsIgnoreCase("features")) { 
	    		features = true; 
	    		featureValue.append("<div><ul>");
	    		map.put("tab_"+tabIndex, "Features");
	    	}
	    	
	    	if (qName.equalsIgnoreCase("spec_table_html")) { 
	    		spec_table_html = true; 
	    		map.put("tab_"+tabIndex, "Specification");
	    	}
	    	
	    	if (qName.equalsIgnoreCase("pdfs")) { 
	    		pdfs = true; 
	    		pdfValue += "<div><ul>";
	    		map.put("tab_"+tabIndex, "Documentation");
	    	}
	    	
	    	if (qName.equalsIgnoreCase("images")) { images = true; }
	    	if (qName.equalsIgnoreCase("key")) { key = true; }
	    	if (qName.equalsIgnoreCase("pn")) { pn = true; }
	    	if (qName.equalsIgnoreCase("brand_name")) { brand_name = true; }
	    	if (qName.equalsIgnoreCase("energy_star_compliant")) { energy_star_compliant = true; }
	    	if (qName.equalsIgnoreCase("capacity_in_cu_ft")) { capacity_in_cu_ft = true; }
	    	if (qName.equalsIgnoreCase("short_description")) { short_description = true; }
	    	if (qName.equalsIgnoreCase("paragraph_description")) { paragraph_description = true; }
	    	if (qName.equalsIgnoreCase("color_code_description")) { color_code_description = true; }
	    	if (qName.equalsIgnoreCase("feature")) { feature = true; }
	    	if (qName.equalsIgnoreCase("width_string")) { width_string = true; }
	    	if (qName.equalsIgnoreCase("height_string")) { height_string = true; }
	    	if (qName.equalsIgnoreCase("depth_string")) { depth_string = true; }
	    	if (qName.equalsIgnoreCase("url")) { pdfUrl = true; }
	    	if (qName.equalsIgnoreCase("description")) { pdfDescription = true; }
	    	if (qName.equalsIgnoreCase("full_size_url")) { full_size_url = true; }
	    	if (qName.equalsIgnoreCase("file_name")) { file_name = true; }
	    }
	    
	    public void endElement(String ns, String sName, String qName) throws SAXException {
	    	
	    	if (qName.equalsIgnoreCase("classification")) { 
	    		classification = false; 
	    		map.put("keywords", keywords.toString());
	    	}
	    	if (qName.equalsIgnoreCase("key")) { key = false; }
	    	if (qName.equalsIgnoreCase("brand_name")) { brand_name = false; }
	    	if (qName.equalsIgnoreCase("color_code_description")) { color_code_description = false; }
	    	if (qName.equalsIgnoreCase("this_item_in_other_colors")) { 
	    		this_item_in_other_colors = false; 
	    		recommendedList = "";
	    	}
	    	if (qName.equalsIgnoreCase("marketing_copy")) { marketing_copy = false; }
	    	
	    	if (qName.equalsIgnoreCase("features")) { 
	    		features = false;
	    		if (featureValue.toString().equalsIgnoreCase("<div><ul>")){
	    			map.remove("tab_"+tabIndex);
	    			map.remove("tab_"+tabIndex+"_content");
	    		} else {
	    			map.put("tab_"+tabIndex++ +"_content", featureValue.append( "</ul></div>" ).toString() );
	    			featureValue.setLength(0);
	    		}
	    	}
	    	if (qName.equalsIgnoreCase("spec_table_html")) {  
	    		spec_table_html = false;
	    		map.put("tab_"+tabIndex++ +"_content", specTableHtmlValue.append(manuallyAddToSpecHtml).toString());	    		
	    	}
	    	if (qName.equalsIgnoreCase("pdfs")) { 
	    		pdfs = false; 
	    		if (pdfValue.equalsIgnoreCase("<div><ul>")){
	    			pdfValue = "";
	    			map.remove("tab_"+tabIndex);
	    			map.remove("tab_"+tabIndex+"_content");
	    		} else {
	    			pdfValue += "</ul></div>";
	    			map.put("tab_"+tabIndex++ +"_content", pdfValue );
	    			pdfValue = ""; 
	    		}
	    	}
	    	if (qName.equalsIgnoreCase("images")) { 
	    		images = false; 
	    		imageIndex = 1;
	    	}
	    	
	    	if (qName.equalsIgnoreCase("product_specs")) {
	    		keywords.setLength(0);
	    		specTableHtmlValue.setLength(0); 
	    		manuallyAddToSpecHtml.setLength(0);
	    		
	    		tabIndex = 1;
	    		data.add(map);
	    	   
	    		if (data.size() == 500) {
					// products
					updateProduct(data);
					data.clear();
					map.clear();
				}
	    	}
	    }
	    
	    public void characters(char buf[], int offset, int len) throws SAXException {
	       String value = new String(buf, offset, len);
	       if (value.trim().length() > 0) {
	    	   if (classification && key && !this_item_in_other_colors) {
		    	   map.put("sku", value );
		    	   keywords.append(","+value+",");
		           key = false;
		       }
		 
	    	   else if (classification && pn && !this_item_in_other_colors) {
		           map.put("name", value );
		           keywords.append(value+",");
		           pn = false;
		       }
		       
		       else if (classification && this_item_in_other_colors && key) {
		           recommendedList += value + ",";
		           map.put("recommended_list", recommendedList);
		       }
		       
		       else if (classification && capacity_in_cu_ft) {
		           manuallyAddToSpecHtml.append("<li><span>Capaciaty (cu. ft.)</span>: " + value + "</li>");
		           capacity_in_cu_ft = false;
		       }
		       
		       else if (classification && brand_name && !this_item_in_other_colors) {
		           map.put("field_6", value );
		           keywords.append(value+",");
		           manuallyAddToSpecHtml.append("<li><span>Brand</span>: " + value + "</li>");
		           brand_name = false;
		       }
	    	   
		       else if (classification && energy_star_compliant && !this_item_in_other_colors) {
		           map.put("field_27", value );
		           manuallyAddToSpecHtml.append("<li><span>Energy Star</span>: " + value + "</li>");
		           energy_star_compliant = false;
		       }
		       
		       else if (marketing_copy && short_description) {
		           map.put("short_desc", value );
		           short_description = false;
		       }
		       
		       else if (marketing_copy && paragraph_description) {
		           map.put("long_desc", value );
		           paragraph_description = false;
		       }
		       
		       else if (features && feature) {
		           map.put("tab_"+tabIndex+"_content", featureValue.append( "<li>"+value+"</li>" ).toString() );
		           feature = false;
		       }
		       
		       else if (classification && color_code_description && !this_item_in_other_colors) {
		           map.put("field_26", value );
		           manuallyAddToSpecHtml.append("<li><span>Color</span>: " + value + "</li>");
		           color_code_description = false;
		       }
		       
		       else if (width_string) {
		    	   map.put("field_19", value );
		    	   manuallyAddToSpecHtml.append("<li><span><b>Width</b></span>: " + value + "</li>");
		           width_string = false;
		       }
		       else if (height_string) {
		    	   map.put("field_20", value );
		    	   manuallyAddToSpecHtml.append("<li><span><b>Height</b></span>: " + value + "</li>");
		           height_string = false;
		       }
		       else if (depth_string) {
		    	   map.put("field_21", value );
		    	   manuallyAddToSpecHtml.append("<li><span><b>Depth</b></span>: " + value + "</li>");
		           depth_string = false;
		       }
		       
		       else if (marketing_copy && spec_table_html) {
		           map.put("tab_"+tabIndex+"_content", specTableHtmlValue.append( value ).toString() );
		       }
		       
		       // Pdfs
		       else if (pdfs && pdfDescription) {
		    	   pdfDescriptionName = value;
		           pdfDescription = false;
		       }
		       else if (pdfs && pdfUrl) {
		    	   pdfValue += "<li><a href=\"" + value +"\">"+ pdfDescriptionName +"</a></li>";
		    	   map.put("tab_"+tabIndex+"_content", pdfValue);
		           pdfUrl = false;
		       }
		       
		       /* Images (some image urls dropped like VCQS001SS)
		       else if (images && full_size_url) {
		    	   map.put("image"+imageIndex++, value);
		           full_size_url = false;
		       }*/
		       
		    // Images
		       else if (images && file_name) {
		    	   map.put("image"+imageIndex++, "http://www.specsserver.com/CACHE/" + value);
		    	   file_name = false;
		       }
	       }  
	    }
	    
	    public void ignorableWhitespace(char buf[], int offset, int len) throws SAXException {
	    	// do nothing
	    }
	    
	    private void updateProduct(List<Map <String, Object>> data) {
	    	// update Products
			webJaguar.updateNum1ApplainceProduct(getCsvFeedList(), data);
	    }
	    
	    private List<CsvFeed> getCsvFeedList() {
			List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
			csvFeedList.add(new CsvFeed("sku"));
			csvFeedList.add(new CsvFeed("name"));
			csvFeedList.add(new CsvFeed("short_desc"));
			csvFeedList.add(new CsvFeed("long_desc"));
			csvFeedList.add(new CsvFeed("weight"));
			csvFeedList.add(new CsvFeed("keywords"));
			csvFeedList.add(new CsvFeed("feed"));
			csvFeedList.add(new CsvFeed("feed_new"));
			csvFeedList.add(new CsvFeed("recommended_list"));
			csvFeedList.add(new CsvFeed("field_6"));
			csvFeedList.add(new CsvFeed("field_26"));
			csvFeedList.add(new CsvFeed("field_19"));
			csvFeedList.add(new CsvFeed("field_20"));
			csvFeedList.add(new CsvFeed("field_21"));
			csvFeedList.add(new CsvFeed("field_27"));
			csvFeedList.add(new CsvFeed("field_28"));
			csvFeedList.add(new CsvFeed("tab_1"));
			csvFeedList.add(new CsvFeed("tab_1_content"));
			csvFeedList.add(new CsvFeed("tab_2"));
			csvFeedList.add(new CsvFeed("tab_2_content"));
			csvFeedList.add(new CsvFeed("tab_3"));
			csvFeedList.add(new CsvFeed("tab_3_content"));
			
			return csvFeedList;
		}
	 }

	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}
