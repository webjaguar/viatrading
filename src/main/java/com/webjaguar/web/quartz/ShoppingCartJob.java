/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.29.2010
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;

public class ShoppingCartJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	/*
	 * export shopping cart (abandon cart)
	 * Triggers everyday at XX:XX AM (server is at EST)
	 * 
	 * 
	 * Create shoppingCartExport.csv file in root folder.
	 * CSV file is " delimited.
	 * This csv contains CustomerID, FirstName, ...
	 * 
	 * If exception is thrown during updating products, an email will be sent to Shahin.
	 */
	public void export() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// configuration
		String[] config = siteConfig.get("AUTO_SHOPPINGCART_EXPORT").getValue().split(",");
		
		if (!(config.length > 3 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if disabled 
			return;
		}
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		File baseFolder = new File(getServletContext().getRealPath("/temp"));
		if (!baseFolder.exists()) {
			baseFolder.mkdir();
		}
				
		CustomerSearch customerSearch = new CustomerSearch();
		customerSearch.setCartNotEmpty(true);
		int customerCount = this.webJaguar.customerCount(customerSearch);
		
		String fileSize = "";
		int limit = 100;
		customerSearch.setLimit(limit);
			
		try {
			
			File exportFile = new File(baseFolder, "shoppingCartExport.csv"); 
			CSVWriter writer = new CSVWriter(new FileWriter(exportFile));
			
			// headers
			List<String> line = new ArrayList<String>();
			line.add("CustomerID");
			line.add("FirstName");
			line.add("LastName");
			line.add("EmailAddress");
			line.add("AccountNumber");
			line.add("Sku");
			line.add("Qty");
			line.add("DateAdded");
			line.add("UnitPrice");
	        
			String[] lines = new String[line.size()];
			writer.writeNext(line.toArray(lines));
			
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			
			for (int offset=0; offset<customerCount; ) {
		    	List<Customer> customerList = this.webJaguar.getCustomerExportList(customerSearch);

		    	for (Customer customer: customerList) {    		
		        	Cart cart = this.webJaguar.getUserCart(customer.getId(), null);
		        	cart.setUserId(customer.getId());
			    	this.webJaguar.updateCart(cart, gSiteConfig, siteConfig, null, null, null);
			    	
		        	for (CartItem cartItem: cart.getCartItems()) {
		        		line = new ArrayList<String>();
						line.add("" + customer.getId());
						line.add(customer.getAddress().getFirstName());
						line.add(customer.getAddress().getLastName());
						line.add(customer.getUsername());
						line.add(customer.getAccountNumber());
						line.add(cartItem.getProduct().getSku());
						line.add("" + cartItem.getQuantity());
						line.add((cartItem.getDateCreated() != null) ? df.format(cartItem.getDateCreated()) : "");   
						line.add("" + cartItem.getUnitPrice());   
		            	
						lines = new String[line.size()];
						writer.writeNext(line.toArray(lines));
		        	}
		    	} // for	 
		    	
		        offset = offset + limit;
		        customerSearch.setOffset(offset);
	    	}		
			writer.close();
			
			// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	        
			// ftp file
	        FileInputStream fis = new FileInputStream(exportFile);
	        if (!ftp.storeFile(config[3], fis)) {
	        	notifyAdmin("ShoppingCartJob export() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
	        }
	        fileSize = "" + exportFile.length();
	        fis.close();
			
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			notifyAdmin("ShoppingCartJob export() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
		
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		sbuff.append("customerCount: " + customerCount);
		sbuff.append("file size: " + config[3] + "" + fileSize);
		notifyAdmin("export() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}	

	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}
