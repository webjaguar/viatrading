/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.27.2010
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.thirdparty.plowAndHearth.CCEncryptDecrypt;

public class PlowAndHearthJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void exportInvoices() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// Plow & Hearth configuration
		String[] config = siteConfig.get("PLOW_AND_HEARTH").getValue().split(",");
		
		if (!(config.length > 2 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if Plow & Hearth feature is disabled 
			return;
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyMMddkkmmss");
		SimpleDateFormat dateFormatter2 = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		File baseFolder = new File(getServletContext().getRealPath("/plowAndHearth"));
		if (!baseFolder.exists()) {
			baseFolder.mkdir();
		}
		File sentFolder = new File(baseFolder, "sent");
		if (!sentFolder.exists()) {
			sentFolder.mkdir();
		}
		
		OrderSearch search = new OrderSearch();
		search.setPnh(false);
		Date cutoff = new Date();
		search.setEndDate(cutoff);
		
		
		List<Order> orderList = this.webJaguar.getInvoiceExportList(search, null);
		if (orderList.isEmpty()) {
			return;
		}
		
		
		Map<String, String> cardMap = new HashMap<String, String>();
		cardMap.put("VISA", "VISA");
		cardMap.put("AMEX", "AMERICAN EXPRESS");
		cardMap.put("MC", "MASTERCARD");
		cardMap.put("DISC", "DISCOVER");
		cardMap.put("JCB", "JCB");

		try {
			
			String fileName = "order_" + dateFormatter.format(new Date()) + ".txt";
			File invoiceFile = new File(baseFolder, fileName); 
			CSVWriter writer = new CSVWriter(new FileWriter(invoiceFile), '~', CSVWriter.NO_QUOTE_CHARACTER);
			
			NumberFormat nf = new DecimalFormat("#0");
			
			// LinkShare
			//SimpleDateFormat linkShareFormatter = new SimpleDateFormat("yyyy-MM-dd/kk:mm:ss");
			//linkShareFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));	
			
			for (Order order: orderList) {
				
				// promo
				String promoTitle = "";
				String promoDiscount = "";
				double shippingDiscount = 0.0;
	    		if (order.getPromo() != null && order.getPromo().getTitle() != null) {
	    			promoTitle =  order.getPromo().getTitle();
					if (order.getPromo().getDiscountType().equals("order")) {
						if (order.getPromo().isPercent()) {
							promoDiscount = nf.format(order.getSubTotal()*order.getPromo().getDiscount());								
						} else {
							promoDiscount = nf.format(order.getPromo().getDiscount()*100);
						}	
					}
					if (order.getPromo().getDiscountType().equals("shipping") && order.getShippingCost() != null) {
						if (order.getPromo().isPercent()) {
							shippingDiscount = order.getShippingCost()*order.getPromo().getDiscount()/100.00;								
						} else if (order.getPromo().getDiscount() > order.getShippingCost()) {
							shippingDiscount = order.getShippingCost();
						} else {
							shippingDiscount = order.getPromo().getDiscount();
						}
					}
	    		}		
				
				// headers
				List<String> line = new ArrayList<String>();
				line.add("H1:01=" + order.getOrderId() + "00");						// Flowers Order Number
				line.add("02=" + dateFormatter.format(order.getDateOrdered()));		// Order Date/Time Stamp
				line.add("03=" + order.getUserEmail());								// Sold To Email Stamp
				line.add("03B=");													// Sold to Opt-in
				line.add("04=" + order.getBilling().getFirstName());				// Sold To First Name
				line.add("05=" + order.getBilling().getLastName());					// Sold To Last Name
				line.add("06=" + order.getBilling().getAddr1());					// Sold To Address 1 (Street)
				line.add("07=" + order.getBilling().getAddr2());					// Sold To Address 2
				line.add("07B=");													// Sold To Business Address
				line.add("08=" + order.getBilling().getCity());						// Sold To City
				line.add("09=" + order.getBilling().getStateProvince());			// Sold To State
				line.add("10=" + order.getBilling().getZip());						// Sold To Zip
				line.add("11=" + order.getBilling().getCountry());					// Sold To Country
				line.add("12=" + digitsOnly(order.getBilling().getPhone()));		// Sold To Phone
				line.add("12B=");													// Sold To Business Phone
				line.add("13=" + ((order.getShippingCost() != null) ? 				// Shipping Charges
								nf.format((order.getShippingCost()-shippingDiscount)*100) : "0"));		
				if (order.getCreditCard() != null) {
					line.add("14=" + cardMap.get(order.getCreditCard().getType()));	// Type of Credit Card
					line.add("15=" + order.getBilling().getFirstName()
									+ order.getBilling().getLastName());			// Credit Card Name (person)
					line.add("16=" + 												// Credit Card Number
						CCEncryptDecrypt.fCreditCardRoutine(
							  order.getCreditCard().getNumber()						// ccNumber
							, order.getCreditCard().getExpireMonth() + "/" + order.getCreditCard().getExpireYear()	// ccExpDate
							, order.getOrderId() + ""								// orderNumber
							, order.getBilling().getFirstName()						// custFname
							, order.getBilling().getLastName()						// custLname
							, order.getShipping().getZip()							// recipientZip
							, order.getOrderId()  + ""								// originalOrder
							, dateFormatter.format(order.getDateOrdered()) 			// dateTimeOrder
							, order.getUserEmail()									// emailAddress
							, "ECR"													// ccEncryptDecrypt
							));
					line.add("17=" + order.getCreditCard().getExpireMonth() + "/"
									+ order.getCreditCard().getExpireYear());		// Credit Card Expiration Date
				} else {
					line.add("14=");
					line.add("15=");
					line.add("16=");
					line.add("17=");
				}
				line.add("18=");													// Corporate ID (not used)
				line.add("19=");													// Corporate Cost Center (not used)	
				line.add("20=" + order.getShipping().getFirstName());				// Ship To First Name
				line.add("21=" + order.getShipping().getLastName());				// Ship To Last Name
				line.add("22=" + order.getShipping().getAddr1());					// Ship To Street Address
				line.add("23=" + order.getShipping().getAddr2());					// Ship To Street Address 2
				line.add("23B=");													// Ship To Business Address
				line.add("24=" + order.getShipping().getCity());					// Ship To City
				line.add("25=" + order.getShipping().getStateProvince());			// Ship To State
				line.add("26=" + order.getShipping().getZip());						// Ship To Zip
				line.add("27=" + order.getShipping().getCountry());					// Ship To Country
				line.add("28=" + digitsOnly(order.getShipping().getPhone()));		// Ship To Phone Number
				line.add("28B=");													// Ship To Business Number
				line.add("29=");													// Ship To Email Address
				line.add("30=");													// Ship To Gender
	    		line.add("31=" + promoTitle);										// Referral Code
				line.add("31B=" + promoDiscount);									// Referral Amount
				line.add("32=");													// Occasion
				line.add("33=");													// Company Number
				line.add("34=");													// Reference Code
	
				// Credit Card Approval Code
				String paymentStatus = (order.getCreditCard() != null) ? order.getCreditCard().getPaymentStatus() : null;
				if (order.getCreditCard() != null && order.getCreditCard().getAuthCode() != null) {
					line.add("35=" + order.getCreditCard().getAuthCode());
				} else if (paymentStatus != null && (paymentStatus.equals("FAITH") || paymentStatus.equals("FAIL"))) {
					line.add("35=" + paymentStatus);
				} else if (order.getCreditCard() != null) {
					line.add("35=FAITH");
				} else {
					line.add("35=");
				}
				
				line.add("36=");													// Credit Card Return Value
				line.add("37=" + nf.format(order.getTax()*100));					// Tax
				line.add("38=");													// Sold To Title - (Mr., Mrs.) 
				line.add("39=");													// Ship To Title
				line.add("30=");													// Reserved
				line.add("41=");													// External Order Number (to be implemented)
				line.add("42=");													// Customer Reorder
				line.add("43=" + nf.format(order.getGrandTotal()*100));				// Order Total Amount
				line.add("44=");													// NOT USED
				line.add("45=");													// NOT USED
				line.add("46=");													// Buyer Club Amount of Discount
				line.add("47=");													// Agent ID (Web OE)
				line.add("48=");													// Source Code (Web OE)
				if (order.getLinkShare() != null) {
					line.add("49=" + order.getLinkShare().getSiteID());				// Site ID (for Affiliates. If first character is �&�, then a non-linkshare affiliate, all others Linkshare)
					if (order.getLinkShare().getDateEntered() != null) {
						line.add("50=" + 											// Time Entered (Linkshare)
								dateFormatter.format(order.getLinkShare().getDateEntered()));
					} else {
						line.add("50=");																	
					}
					line.add("51=" + 												// Time Completed (Linkshare)
								dateFormatter.format(order.getDateOrdered()));																	
				} else {
					line.add("49=" + ((order.getTrackcode() == null || order.getTrackcode().trim().length() == 0) 
								? "" : "&" + order.getTrackcode()));
					line.add("50=");
					line.add("51=");
				}
				line.add("52=");													// Gift Card Number
				line.add("53=");													// Gift Card Amt redeemed
				line.add("54=");													// Gift Card Authorization number from Paymentech
				line.add("");
				line.add("");
				
				String[] lines = new String[line.size()];
				writer.writeNext(line.toArray(lines));
				
				// line items
				int index = 1;
	    		for (LineItem lineItem : order.getLineItems()) {
					line = new ArrayList<String>();
					line.add("D" + index++ + ":01=" + order.getOrderId() + "00");	// External Order Number (Matches Header)
					// quantity
					int totalQty = lineItem.getQuantity();
					if (lineItem.getProduct().getCaseContent() != null) {
						totalQty = totalQty * lineItem.getProduct().getCaseContent();
					}
					line.add("02=" + totalQty);										// Quantity Order
					line.add("03=" + ((lineItem.getUnitPrice() != null) ? 
							nf.format(lineItem.getUnitPrice()*100) : "0"));			// Unit Price
					StringBuffer sku = new StringBuffer("Sku:" + lineItem.getProduct().getSku());
					for (ProductAttribute productAttribute: lineItem.getProductAttributes()) {
						sku.append("|" + productAttribute.getOptionName().trim() + ":" + productAttribute.getValueString().trim());
					}
					line.add("04=" + sku);											// Item Number w/ variants
					line.add("05=" + lineItem.getProduct().getName());				// Item Description
					line.add("06=");												// not used
					line.add("07=");												// not used
					line.add("08=");												// not used
					line.add("09=");												// not used
					String freightType = "A4";
					if (order.getShippingMethod() != null && order.getShippingMethod().toLowerCase().contains("express")) {
						freightType = "A3";
					}
					line.add("10=" + freightType);									// Freight Type
					line.add("11=");												// Monogrammed/Personalization Indicator
					line.add("12=");												// Style of Monogramming
					line.add("13=");												// Monogrammed/Personalization Text
					line.add("14=");												// not used
					line.add("15=");												// Gift Wrap Amount
					line.add("OPTIONS:01=Product Size:");							// Product Size, Greeting Message
					line.add("01=Greeting Card Message:");
					line.add("02=Delivery Date:");									// Delivery Date	
					line.add("03=Special Instructions:");							// Specialized Instruction
					line.add("04=American Airlines:");								// American Airlines ID
					line.add("05=GiftCirt:");										// Gift Certificate Number
					line.add("");
					line.add("");
					
					lines = new String[line.size()];
					writer.writeNext(line.toArray(lines));
	    		}
			}
			writer.close();
			
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	        
	        FileInputStream fis = new FileInputStream(invoiceFile);
	        if (ftp.storeFile(fileName, fis)) {
	        	this.webJaguar.updateOrderExported("pnh", cutoff, null);
	        	copy(invoiceFile, new File(sentFolder, fileName));
	        	invoiceFile.delete();
	        } else {
	        	notifyAdmin("Plow & Hearth exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
	        }
	        fis.close();

	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();	
	        
	        Date end = new Date();
			sbuff.append("Started : " + dateFormatter2.format(start) + "\n");
			sbuff.append("Ended : " + dateFormatter2.format(end) + "\n\n");
			notifyAdmin("Success: Plow & Hearth exportInvoices() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
			
		} catch (Exception e) {
			notifyAdmin("Plow & Hearth exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}	
	
	public void updateInventory() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		List<CsvFeed> csvFeedList = this.webJaguar.getCsvFeed("pnh-inventory");

		// Plow & Hearth configuration
		String[] config = siteConfig.get("PLOW_AND_HEARTH").getValue().split(",");
		
		if (!(config.length > 3 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if Plow & Hearth feature is disabled 
			return;
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	        
	        InputStream inStream = ftp.retrieveFileStream(config[3]);

	        if (inStream != null) {
	        	
	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream));
	        	
	        	List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		        
	        	boolean deleteFile = false;
	        	
				String [] nextLine;
				while ((nextLine = reader.readNext()) != null) {
					HashMap<String, Object> map = new HashMap<String, Object>();
					
					for (CsvFeed csvFeed: csvFeedList) {
						if (csvFeed.getColumn() != null) {
							String sValue = nextLine[csvFeed.getColumn()].trim();
							if (csvFeed.isNumeric()) {
								try {
									map.put(csvFeed.getColumnName(), Double.parseDouble(sValue));
								} catch (NumberFormatException e) {
									map.put(csvFeed.getColumnName(), null);									
								}
							} else {
								map.put(csvFeed.getColumnName(), sValue);					
							}       						
						}
					}

					data.add(map);
					if (data.size() == 5000) {
						// products
						this.webJaguar.updateProduct(csvFeedList, data, null);
						data.clear();
						deleteFile = true;
					}
				}
				
				if (data.size() > 0) {
					// products
					this.webJaguar.updateProduct(csvFeedList, data, null);
					data.clear();
					deleteFile = true;
				}
				
				reader.close();
				inStream.close();
				
				if (deleteFile) {
					// delete file
					ftp.deleteFile(config[3]);					
				}

	        }			
			
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
	        
	        Date end = new Date();
			sbuff.append("Started : " + dateFormatter.format(start) + "\n");
			sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
			notifyAdmin("Success: Plow & Hearth updateInventory() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
			
		} catch (Exception e) {
			notifyAdmin("Plow & Hearth updateInventory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}
	
	public void updateInvoices() {
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();

		// Plow & Hearth configuration
		String[] config = siteConfig.get("PLOW_AND_HEARTH").getValue().split(",");
		
		if (!(config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if Plow & Hearth feature is disabled 
			return;
		}
		
		SimpleDateFormat dateFormatter2 = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		File baseFolder = new File(getServletContext().getRealPath("/plowAndHearth"));
		if (!baseFolder.exists()) {
			baseFolder.mkdir();
		}
		File processedFolder = new File(baseFolder, "processed");
		if (!processedFolder.exists()) {
			processedFolder.mkdir();
		}
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	        	        
	        String[] fileNames = ftp.listNames();
	        
	        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
	        SimpleDateFormat dateShippedFormatter = new SimpleDateFormat("dd-MMM-yy");
	        
	        Map<Date, String> fileMap = new TreeMap<Date, String>();
	        for (String fileName: fileNames) {	        	
	        	if (fileName.startsWith(config[4])) {
	        		fileMap.put(dateFormatter.parse(fileName.substring(config[4].length())), fileName);
	        	}
	        }
	        
	        // sorted by timestamp
	        for (String fileName: fileMap.values()) {

	        	File file = new File(processedFolder, fileName);
	        	
		        if (ftp.retrieveFile(fileName, new FileOutputStream(file))) {
		        	CSVReader reader = new CSVReader(new FileReader(file), '^', CSVWriter.NO_QUOTE_CHARACTER);

		        	// get line items
		        	List<LineItem> lineItems = new ArrayList<LineItem>();
		        	
					String[] nextLine;
					while ((nextLine = reader.readNext()) != null) {
						LineItem lineItem = new LineItem();
						try {
							lineItem.setOrderId(Integer.parseInt(nextLine[1].trim()));
						} catch (NumberFormatException e) {}
						if (lineItem.getOrderId() != null) {
							lineItem.setExternalOrderId(nextLine[0].trim());
							lineItem.setProduct(new Product(nextLine[2].trim(), null));
							if (nextLine[8].trim().length() > 0) {
								lineItem.setTrackNum(nextLine[8].trim());								
							}
							try {
								lineItem.setDateShipped(dateShippedFormatter.parse(nextLine[9].trim()));								
							} catch (ParseException e) {}
							lineItem.setStatus(nextLine[12].trim());								
							lineItems.add(lineItem);
						}
					}
					
					reader.close();
					
					if (!lineItems.isEmpty()) {
						this.webJaguar.updatePlowAndHearthLineItems(lineItems);
					}
					
					// delete file
					ftp.deleteFile(fileName);
		        }
	        }
	        	        
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
	        
	        Date end = new Date();
			sbuff.append("Started : " + dateFormatter2.format(start) + "\n");
			sbuff.append("Ended : " + dateFormatter2.format(end) + "\n\n");
			notifyAdmin("Success: Plow & Hearth updateInventory() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
			
		} catch (Exception e) {
			notifyAdmin("Plow & Hearth updateInvoices() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}
	
	private String digitsOnly(String in) {
		if (in == null) {
			return "";
		}
		
		String digits = "0123456789";

		StringBuffer sbuff = new StringBuffer(in);
		for (int i=sbuff.length()-1; i>=0; i--) {
			if (digits.indexOf(sbuff.charAt(i)) < 0) {
				sbuff.deleteCharAt(i);
			}
		}
		
		return sbuff.toString();
	}

	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
	
	// Copies src file to dst file.
	// If the dst file does not exist, it is created
	private void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}
}
