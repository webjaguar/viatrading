/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 08.19.2010
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.SalesRep;

public class BusinessVisionJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void updateInventory() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		List<CsvFeed> csvFeedList = this.webJaguar.getCsvFeed("bv-inventory");
		
		// Business Vision configuration
		String[] config = siteConfig.get("BUSINESS_VISION").getValue().split(",");
		
		if (!((Boolean) gSiteConfig.get("gINVENTORY") && (Boolean) gSiteConfig.get("gSITE_ACTIVE")
				&& config.length > 3)) {
			// return if inventory, business vision feature is disabled 
			return;
		}
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	        
	        InputStream inStream = ftp.retrieveFileStream(config[3]);

	        if (inStream != null) {
	        	
	        	CSVReader reader = new CSVReader(new InputStreamReader(inStream), '|', CSVWriter.NO_QUOTE_CHARACTER);
	        	
	        	List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		        
	        	boolean deleteFile = false;
	        	
				String [] nextLine;
				while ((nextLine = reader.readNext()) != null) {
					HashMap<String, Object> map = new HashMap<String, Object>();
					
					for (CsvFeed csvFeed: csvFeedList) {
						if (csvFeed.getColumn() != null) {
							String sValue = nextLine[csvFeed.getColumn()].trim();
							if (csvFeed.isWholeNum()) {
	    						try {
	    							Double value = Double.parseDouble(sValue);
									map.put(csvFeed.getColumnName(), value.intValue());
	    						} catch (NumberFormatException e) {
	    							map.put(csvFeed.getColumnName(), null);									
	    						}
	    					} else {
	    						map.put(csvFeed.getColumnName(), sValue);							
	    					}	        						
						}
					}
					data.add(map);
					if (data.size() == 5000) {
						// products
						
						this.webJaguar.updateProduct(csvFeedList, data, null);
						data.clear();
						deleteFile = true;
					}
				}
				
				if (data.size() > 0) {
					// products
					this.webJaguar.updateProduct(csvFeedList, data, null);
					data.clear();
					deleteFile = true;
				}
				
				reader.close();
				inStream.close();
				
				if (deleteFile) {
					// delete file
					ftp.deleteFile(config[3]);					
				}

	        }			
			
	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();
		} catch (Exception e) {
			notifyAdmin("Business Vision updateInventory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}
	
	public void exportInvoices() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
		// Mas200 configuration
		String[] config = siteConfig.get("BUSINESS_VISION").getValue().split(",");
		
		if (!(config.length > 4 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if Business Vision feature is disabled 
			return;
		}
		
		File bvFolder = new File(getServletContext().getRealPath("/bv"));
		if (!bvFolder.exists()) {
			bvFolder.mkdir();
		}
		File bvSentFolder = new File(bvFolder, "sent");
		if (!bvSentFolder.exists()) {
			bvSentFolder.mkdir();
		}
		
		OrderSearch search = new OrderSearch();
		search.setBv(false);
		Date cutoff = new Date();
		search.setEndDate(cutoff);
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yy");
		SimpleDateFormat prefixFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
		
		List<Order> orderList = this.webJaguar.getInvoiceExportList(search, null);
		if (orderList.isEmpty()) {
			return;
		}
		
		try {
			
			String fileName = config[4];
			if (fileName.contains("/")) {
				// remove directory
				fileName = fileName.substring(config[4].lastIndexOf("/")+1);
			}
			
			File invoiceFile = new File(bvFolder, fileName); 
			CSVWriter writer = new CSVWriter(new FileWriter(invoiceFile));

			// headers
			List<String> line = new ArrayList<String>();
			line.add("OrderNumber");
			line.add("Customer #");
			line.add("Customer Name");
			line.add("Ship To ID");
			line.add("Customer PO #");
			line.add("Order Date");
			line.add("Order Status");
			line.add("Invoice Date");
			line.add("Reference #");
			line.add("Territory Code");
			line.add("Salesperson Code");
			line.add("Discount %");
			line.add("Terms Code");
			line.add("FOB");
			line.add("Freight");
			line.add("Ship Via");
			line.add("Order Comment");
			line.add("Warehouse");
			line.add("Part #");
			line.add("Order Qty");
			line.add("Line Discount %");
			
			String[] lines = new String[line.size()];
			writer.writeNext(line.toArray(lines));
			
			Map<Integer, String> customers = new HashMap<Integer, String>();
			
	    	Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			for (SalesRep salesRep: this.webJaguar.getSalesRepList()) {
				salesRepMap.put(salesRep.getId(), salesRep);
			}
			
			for (Order order: orderList) {
				if (!customers.containsKey(order.getUserId())) {
					Customer customer = this.webJaguar.getCustomerById(order.getUserId());
					if (customer != null) {
						customers.put(order.getUserId(), customer.getAccountNumber());
					} else {
						customers.put(order.getUserId(), "");						
					}
				}
				
				// line items
	    		for (LineItem lineItem : order.getLineItems()) {
					line = new ArrayList<String>();
					line.add("" + order.getOrderId());
					line.add(customers.get(order.getUserId()));
					line.add(order.getBilling().getFirstName() + " " + order.getBilling().getLastName());
					line.add((order.getShipping().getCode() != null) ? order.getShipping().getCode() : "");
					line.add(order.getPurchaseOrder());
					line.add(dateFormatter.format(order.getDateOrdered()));
					line.add(getMessageSourceAccessor().getMessage("orderStatus_" + order.getStatus()));
					line.add("");	// Invoice Date
					line.add("");	// Reference #
					line.add("");	// Territory Code
					if (salesRepMap.containsKey(order.getSalesRepId())) {
						line.add(salesRepMap.get(order.getSalesRepId()).getAccountNumber());
					} else {
						line.add("");
					}
					line.add("");	// Discount %
					line.add("");	// Terms Code
					line.add("");	// FOB
					line.add("");	// Freight
					line.add(order.getShippingMethod());
					line.add(order.getInvoiceNote().replace("\n", " ").replace("\r", " "));
					line.add(""); 	// Warehouse
					line.add(lineItem.getProduct().getSku());
					line.add("" + (lineItem.getQuantity() * ((lineItem.getProduct().getCaseContent() == null) ? 1 :lineItem.getProduct().getCaseContent())));
					line.add("");	// Line Discount %		
					
					lines = new String[line.size()];
					writer.writeNext(line.toArray(lines));
	    		}
			}
			writer.close();
			
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        ftp.connect(config[0]);
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode();
	        
	        FileInputStream fis = new FileInputStream(invoiceFile);
	        if (ftp.storeFile(config[4], fis)) {
	        	this.webJaguar.updateOrderExported("bv", cutoff, null);
	        	copy(invoiceFile, new File(bvSentFolder, prefixFormatter.format(new Date()) + "-" + fileName));
	        	invoiceFile.delete();
	        } else {
	        	notifyAdmin("Business Vision exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), ftp.getReplyString());
	        }
	        fis.close();

	        // Logout from the FTP Server and disconnect
	        ftp.logout();
	        ftp.disconnect();	        
			
		} catch (Exception e) {
			notifyAdmin("Business Vision exportInvoices() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}
	}	
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
	
	// Copies src file to dst file.
	// If the dst file does not exist, it is created
	private void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read(buf)) > 0) {
			out.write(buf, 0, len);
		}
		in.close();
		out.close();
	}
}
