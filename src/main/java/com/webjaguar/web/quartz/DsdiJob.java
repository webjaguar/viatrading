/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.29.2010
 */

package com.webjaguar.web.quartz;


import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import au.com.bytecode.opencsv.CSVReader;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Product;
import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.dsdi.DsdiCategory;;

public class DsdiJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void updateInventory() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		List<CsvFeed> csvFeedList = this.webJaguar.getCsvFeed("DSDI-imports");

		// DSDI configuration
		String[] config = siteConfig.get("DSDI_FILE_LINK").getValue().split(","); 
		
		if (!(config.length > 0 && (Boolean) gSiteConfig.get("gSITE_ACTIVE")) || csvFeedList.isEmpty()) {
			// return if dsdi imports feature is disabled 	
			return;
		}
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
		
		csvFeedList.add(new CsvFeed("price_1"));
		csvFeedList.add(new CsvFeed("feed"));
		csvFeedList.add(new CsvFeed("feed_new"));
		
	    //supplier id
		Integer supplierId = Integer.parseInt(config[1]);
	 	if (!this.webJaguar.isValidSupplierId(supplierId)) {
			// not a valid supplier
			return;
		}

		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		Map <Long, List<String>> imageData = new HashMap<Long, List<String>>();
		List<Map <String, Object>> categoryData = new ArrayList<Map <String, Object>>();
		
		// categories
		Map <String, DsdiCategory> categoryMap = this.webJaguar.getDsdiCategoryMap();
		List<DsdiCategory> categories = new ArrayList<DsdiCategory>();
		Set<String> categorySet = new HashSet<String>();
		
		try {
			
			URL url = new URL(config[0]);				
			CSVReader reader = new CSVReader(new InputStreamReader(url.openStream())); 
			
			// get headers
	    	Map<String, Integer> headerMap = new HashMap<String, Integer>();			        	
			String[] headers = reader.readNext();
	    	for (int i=0; i<headers.length; i++) {
	    		headerMap.put(headers[i], i);
			}
			
	    	String [] nextLine;
			while ((nextLine = reader.readNext()) != null) { 
				
				String cat = nextLine[headerMap.get("MASTER_CATEGORY")].trim();
				String sub = nextLine[headerMap.get("CATEGORY")].trim();		
				HashMap<String, Object> map = new HashMap<String, Object>();
			
        		if (categorySet.add(cat.toLowerCase() + "--" + "*")) {
        			DsdiCategory level1 = new DsdiCategory();
        			level1.setCat(cat);
        			level1.setSub("*");
    				categories.add(level1);
        		}
        		if (categorySet.add(cat.toLowerCase() + "--" + sub.toLowerCase())) {
        			DsdiCategory level2 = new DsdiCategory();
        			level2.setCat(cat);
        			level2.setSub(sub);
	    			categories.add(level2);			        			
        		}
				
        		// categories
				List<Integer> catIds = new ArrayList<Integer>();
				Double priceChange = null;
				if (categoryMap.containsKey(cat.toLowerCase() + "--*")) {
					// level 1 
					DsdiCategory category = categoryMap.get(cat.toLowerCase() + "--*");
					if (category.getCatId() != null) catIds.add(category.getCatId());
					if (category.getPriceChange() != null) priceChange = category.getPriceChange();
				}
				if (categoryMap.containsKey(cat.toLowerCase() + "--" + sub.toLowerCase())) {
					// level 2
					DsdiCategory category = categoryMap.get(cat.toLowerCase() + "--" + sub.toLowerCase());
					if (category.getCatId() != null) catIds.add(category.getCatId());
					if (category.getPriceChange() != null) priceChange = category.getPriceChange();
				}

				if (catIds.isEmpty()) {					
					// do not download this item
										
				} else {				
					for (CsvFeed csvFeed: csvFeedList) {
						if (csvFeed.getHeaderName() != null) {
							String sValue;
							if(csvFeed.getOverrideValue() == null) {
								sValue = nextLine[headerMap.get(csvFeed.getHeaderName().toUpperCase())].trim();
							} else { // override value with the header name.
								sValue = csvFeed.getOverrideValue();
							}
							if (csvFeed.getColumnName().equals("sku")) {
								map.put("sku", sValue);
	    					} else if (csvFeed.isNumeric()) {
								try {
									map.put(csvFeed.getColumnName(), Double.parseDouble(sValue));
								} catch (Exception e) {
									map.put(csvFeed.getColumnName(), null);									
								}
							}  else {
								map.put(csvFeed.getColumnName(), sValue);	
							}							
						}
					}
										
					// pricing
					Double price =Double.parseDouble( nextLine[headerMap.get("RESELLER_PRICE")].trim());
					String mapValue =  nextLine[headerMap.get("MAP")].trim();
					Double percentageCost =0.00;
					if(priceChange != null) {
						percentageCost=  price * (100+priceChange)/100;
					} else {
						percentageCost = price;
					}
					String mapP = nextLine[headerMap.get("MAP_PRICE")].trim();
					Double mapPrice = 0.00;
					if(! mapP.equalsIgnoreCase("NA")) {
						mapPrice = Double.parseDouble(mapP);
					}
					// If MAP is YES get MAP_PRICE and calculate the 'price_1' value else get the RESELLER_PRICE in 'price_1'
					if(mapValue.equalsIgnoreCase("YES") && percentageCost < mapPrice) {
						map.put("price_1",mapPrice);
					} else {
						map.put("price_1",percentageCost);
					}
					
					map.put("feed", "DSDI");
					map.put("feed_new", true);
					
					map.put("productId", this.webJaguar.getProductIdBySku((String) map.get("sku")));
					if (map.get("productId") == null) {
						// new item
						Product newProduct = new Product((String) map.get("sku"), "Dsdi Feed");
						this.webJaguar.insertProduct(newProduct, null);
						map.put("productId", newProduct.getId());
						
						// add supplier
						Supplier supplier = new Supplier();
						supplier.setId(supplierId);
						supplier.setSku((String) map.get("sku"));
						supplier.setSupplierSku(((String) map.get("sku")).substring(3)); // remove leading DSD-
						this.webJaguar.insertProductSupplier(supplier); 
					}
					
					// images		
					List<String> images = new ArrayList<String>();
					String imagePath = nextLine[headerMap.get("IMAGES_PATH")].trim();
					String imageName = nextLine[headerMap.get("PRODUCT_IMAGE")].trim();
					String absoluteImagePath = "http://dropshipdirect.com/warehouse/product_thumb.php?img=images/images_products/"+imagePath+imageName +"&w=200&h=200" ;
					
					images.add(absoluteImagePath);				
					
					data.add(map);
					imageData.put(((Integer) map.get("productId")).longValue(), images); 
					
					
					// category mapping
					for (Integer catId: catIds) {
						HashMap<String, Object> mapCategory = new HashMap<String, Object>();
						mapCategory.put("productId", map.get("productId"));
						mapCategory.put("categoryId", catId);
						categoryData.add(mapCategory);
					}
					
					if (data.size() == 5000) {						
						// products
						this.webJaguar.updateProduct(csvFeedList, data, null);
						
						// associate to supplier
						this.webJaguar.nonTransactionSafeUpdateProductSupplier(supplierId, data);
						data.clear();
						
						// images
						this.webJaguar.updateProductImage(imageData);
						imageData.clear();
						
						// associate to categories		
						this.webJaguar.nonTransactionSafeInsertCategories(categoryData);		
						categoryData.clear();
					}						
				}
				
				if (categories.size() == 5000) {
					this.webJaguar.nonTransactionSafeInsertDsdiCategories(categories);
					categories.clear();
				}
			}
					
			if (data.size() > 0) {
				// products
				this.webJaguar.updateProduct(csvFeedList, data, null);
				
				// associate to supplier
				this.webJaguar.nonTransactionSafeUpdateProductSupplier(supplierId, data);
				data.clear();
				
				// images
				this.webJaguar.updateProductImage(imageData);
				imageData.clear();
				
				// associate to categories		
				this.webJaguar.nonTransactionSafeInsertCategories(categoryData);
			}
			
			if (categories.size() > 0) {
				this.webJaguar.nonTransactionSafeInsertDsdiCategories(categories);
				categories.clear();
			}
						
			// make old dsdi imports items inactive
			this.webJaguar.deleteOldProducts("DSDI", true);	

		} catch (Exception e) {
			notifyAdmin("Dsdi Imports updateInventory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}	
		Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		notifyAdmin("updateInventory() - Dsdi on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
	}
    	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}