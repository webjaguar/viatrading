/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.09.2009
 */

package com.webjaguar.web.quartz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.namespace.QName;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import spexlive.etilize.com.Catalog;
import spexlive.etilize.com.CatalogServiceIntf;
import spexlive.etilize.com.ManageProductLists;
import spexlive.etilize.com.ManageProductListsResponse;
import spexlive.etilize.com.TaskResult;
import spexlive.etilize.com.UploadProductListFile;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;

public class EtilizeJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void uploadProductList() {    	

		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		
    	String appId = siteConfig.get("ETILIZE").getValue();

		if (!(appId.trim().length() > 0 && (Boolean) gSiteConfig.get("gSITE_ACTIVE"))) {
			// return if etilize is disabled 
			return;
		} 
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		Date start = new Date();
    	
    	try {
    		String wsdlLocation = "http://ws.spexlive.net/service/soap/catalog?appId=" + siteConfig.get("ETILIZE").getValue() + "&wsdl";
    		Catalog service = new Catalog(new URL(wsdlLocation), new QName("http://com.etilize.spexlive", "catalog"));
    		CatalogServiceIntf port = service.getCatalogHttpPort();
    		
    		ManageProductLists manageProductLists = new ManageProductLists();
    		manageProductLists.setCatalog("na");
    		manageProductLists.setSiteId(0);
    		
    		// attachment
    		File zipFile = createZipFile();
    		InputStream is = new FileInputStream(zipFile);
    		long length = zipFile.length();
    		if (length > Integer.MAX_VALUE) {
                // File is too large
    			notifyAdmin("ETILIZE uploadProductList() on " + siteConfig.get("SITE_URL").getValue(), "Zip file too large.");
    			return;
            }
    		// Create the byte array to hold the data
            byte[] bytes = new byte[(int)length];
            // Read in the bytes
            int offset = 0;
            int numRead = 0;
            while (offset < bytes.length
                   && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
                offset += numRead;
            }
            // Ensure all the bytes have been read in
            if (offset < bytes.length) {
                // not all read
    			notifyAdmin("ETILIZE uploadProductList() on " + siteConfig.get("SITE_URL").getValue(), "Zip not read completely.");
    			return;
            }
            is.close();
    		UploadProductListFile uploadProductListFile = new UploadProductListFile();
    		uploadProductListFile.setFile(bytes);
    		uploadProductListFile.setListName("allitems");
    		manageProductLists.setUploadProductListFile(uploadProductListFile);
    		
    		ManageProductListsResponse manageProductListsResponse = port.manageProductLists(manageProductLists);
    		for (TaskResult taskResult: manageProductListsResponse.getFileUploadResult().getProduct()) {
    			if (!taskResult.isSuccess()) {
    				notifyAdmin("ETILIZE uploadProductList() on " + siteConfig.get("SITE_URL").getValue(), taskResult.getFailureCause().get(0).getCode());    				
    			}
    		}

    	} catch (Exception e) {
			notifyAdmin("ETILIZE uploadProductList() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
    	} 
    	
    	Date end = new Date();
		sbuff.append("Started : " + dateFormatter.format(start) + "\n");
		sbuff.append("Ended : " + dateFormatter.format(end) + "\n\n");
		sbuff.append("Read http://ws.spexlive.net/service/soap/catalog?appId=" + siteConfig.get("ETILIZE").getValue() + "\n\n");
		notifyAdmin("uploadProductList() - Etilize on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
    }
    
    private File createZipFile() throws Exception {
    	
    	File tempFolder = new File(getServletContext().getRealPath("temp"));	
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File etilizeFolder = new File(tempFolder, "etilize");
		if (!etilizeFolder.exists()) {
			etilizeFolder.mkdir();
		}
		
		// create csv file
		File csvFile = new File(etilizeFolder, "ProductList.CSV");
		PrintWriter pw = new PrintWriter(new FileWriter(csvFile));
		pw.print("productId,skuType,sku\n");
		
		// Etilize ID has to be unique
		Set<Integer> etilizeIds = new HashSet<Integer>();
		
		// SKU and UPC has to be unique
		Set<String> skus = new HashSet<String>();
		
		int productCount = this.webJaguar.getEtilizeCount();
		int limit = 3000;
		for (int offset=0; offset<productCount; ) {
			for (Product product: this.webJaguar.getEtilizeList(limit, offset)) {
				int start = 0;
				if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("TECHDATA")) {
					pw.print(",\"Tech Data\",");
					if (product.getSku().startsWith("TD-")) {  // remove leading TD-
						start = 3;
					}
					if (skus.add("Tech Data" + product.getSku().substring(start).toLowerCase())) {
						pw.println(product.getSku().substring(start));
					}
				} else if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("INGRAM")) {
					pw.print(",\"Ingram Micro USA\",");
					if (product.getSku().startsWith("IM-")) {  // remove leading IM-
						start = 3;
					}
					if (skus.add("Ingram Micro USA" + product.getSku().substring(start).toLowerCase())) {
						pw.println(product.getSku().substring(start));
					}
				} else if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("SYNNEX")) {
					pw.print(",\"Synnex\",");
					if (product.getSku().startsWith("SYN-")) {  // remove leading SYN-
						start = 4;
					}
					if (skus.add("Synnex" + product.getSku().substring(start).toLowerCase())) {
						pw.println(product.getSku().substring(start));
					}
				} else if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("DSI")) {
					pw.print(",\"DSI\",");
					if (product.getSku().startsWith("DSI-")) {  // remove leading DSI-
						start = 4;
					}
					if (skus.add("DSI" + product.getSku().substring(start).toLowerCase())) {
						pw.println(product.getSku().substring(start));
					}
				} else if (product.getEtilizeId() != null) {
					if (etilizeIds.add(product.getEtilizeId())) {
						pw.println(product.getEtilizeId() + ",,");						
					}
				} else if (product.getUpc() != null) {
					if (skus.add("UPC" + product.getUpc().toLowerCase())) {
						pw.println(",\"UPC\"," + product.getUpc());						
					}
				}
			}
			offset = offset + limit;
		}
		pw.close();

        // Create the ZIP file
        File zipFile = new File(etilizeFolder, "ProductList.ZIP");
        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile));
        
        // Create a buffer for reading the files
	    byte[] buf = new byte[1024];
        
        FileInputStream in = new FileInputStream(csvFile);
	    
        // Add ZIP entry to output stream.
        out.putNextEntry(new ZipEntry(csvFile.getName()));

        // Transfer bytes from the file to the ZIP file
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }

        // Complete the entry
        out.closeEntry();
        in.close();
        
        // Complete the ZIP file
        out.close();
		
		csvFile.delete();
		
    	return zipFile;
    }

	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
}
