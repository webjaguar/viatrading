package com.webjaguar.web.quartz;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import au.com.bytecode.opencsv.CSVReader;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.InventoryActivity;
import com.webjaguar.web.domain.KeyBean;

public class InventoryUpdate {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private static MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	static Map<String, Configuration> siteConfig;
	
	
	public InventoryUpdate(Map<String, Configuration> siteConfig, WebJaguarFacade webJaguar, MailSender mailSender) {
		this.siteConfig = siteConfig;
		this.webJaguar = webJaguar;
		this.mailSender = mailSender;
	}
	
	public KeyBean updateInventory() {
		// Mas90 configuration
		String[] config = siteConfig.get("MAS90").getValue().split(",");
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
		StringBuffer sbuff = new StringBuffer();
		StringBuffer fileNamesList = new StringBuffer();
		Date start = new Date();
		
		KeyBean result = new KeyBean("false", null);
		List<CsvFeed> csvFeedList = new ArrayList<CsvFeed>();
		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		
		try {
	    	// Connect and logon to FTP Server
	        FTPClient ftp = new FTPClient();
	        
	        // ftp on wjserver500 uses port 2121. 
	        try {
	        	ftp.connect(config[0], Integer.parseInt(config[5]));
	        } catch (ArrayIndexOutOfBoundsException e) {
	        	ftp.connect(config[0]);	        	
	        } catch (Exception e) {
	        	ftp.connect(config[0]);
	        }
	        
	        // update filed with Inventory
	        String fieldName = null;
	        try {
	        	fieldName = config[6];
	        	csvFeedList.add(new CsvFeed(fieldName));
	        } catch (ArrayIndexOutOfBoundsException e) {
	        	fieldName = null;
	        }
	        
	        // login
	        ftp.login(config[1], config[2]);
	        ftp.enterLocalPassiveMode(); 
	        
	        if (ftp.changeWorkingDirectory("INVENTORY")) {
	        	
	        	FTPFile[] listOfFiles = ftp.listFiles();

	        	for (FTPFile file : listOfFiles) {
	        		
	        	    if (file.isFile()) {
	        	        
			        	InputStream inStream = ftp.retrieveFileStream(file.getName());

						fileNamesList.append(file.getName() + "\n");
				        // FOR TESTING instead of FTP file, read local file
						// final String FILE_NAME = "/Users/shahinnaji/Desktop/INVENTORY.CSV";
						// InputStream inStream = new FileInputStream(FILE_NAME);
				        
						boolean inventoryHistory = false;
						if (siteConfig.get("INVENTORY_HISTORY").getValue().equals("true")) {
							inventoryHistory = true;
						}
				        if (inStream != null) {
				        	CSVReader reader = new CSVReader(new InputStreamReader(inStream));
							String [] nextLine;
							// ignore first line.
							reader.readNext();
							while ((nextLine = reader.readNext()) != null) {
								
								HashMap<String, Object> map = new HashMap<String, Object>();
								String sku = nextLine[0].trim();
								String inventoryString = nextLine[1];
								if (!sku.equals("")) {
									map.put("sku", sku);
									try {
										Integer inventory = Integer.parseInt(inventoryString.trim());
										Inventory inventoryObject = new Inventory();
										
										inventoryObject.setSku(sku);
										inventoryObject = webJaguar.getInventory(inventoryObject);
										if(inventoryObject != null) {
											map.put("inventory_afs",  inventory);
											map.put("inventory", inventory);
											if (fieldName != null) {
												map.put(fieldName, inventory);
											}
											
											// Add to Inventory history
											if (inventoryHistory) {
												InventoryActivity inventoryActivity = new InventoryActivity();	
												inventoryActivity.setSku(sku);
												inventoryActivity.setInventory(inventory);
												inventoryActivity.setinventoryAFS(inventory);
												inventoryActivity.setAccessUserId(0);
												inventoryActivity.setQuantity(inventory);
												inventoryActivity.setType("inventory import");
												webJaguar.productIventoryHistory(inventoryActivity);
											}
											
										} else {
											map.put("inventory", inventory);
											map.put("inventory_afs", inventory);
											if (fieldName != null) {
												map.put(fieldName, inventory);
											}
										}
									} catch (NumberFormatException e) {
										e.printStackTrace();
										continue;
									}
									data.add(map);
								}
							}
							reader.close();
							inStream.close();
							
							// delete file
							ftp.deleteFile(file.getName());
							ftp.completePendingCommand(); // very important for next retrieveFileStream to work
				        }
	        	    }
	        	}
				
		        // Logout from the FTP Server and disconnect
		        ftp.logout();
		        ftp.disconnect();
	        } else {
	        	notifyAdmin("Folder INVENTORY not found" + siteConfig.get("SITE_URL").getValue(), null);
		        result.setValue("Folder INVENTORY not found on " + siteConfig.get("SITE_URL").getValue() + "\n\n" + sbuff.toString());
	        	return result;
	        }
	        
		} catch (Exception e) {
			// do nothing
			notifyAdmin("1-Exception Mas90 updateInventory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
			result.setValue("1-Exception updateInventory() on " + "\n\n" + e.toString());
			return result;
		}
		
		// update inventory
		if (!data.isEmpty()) {
			webJaguar.updateInventoryBySkus(csvFeedList, data);

			
			Date end = new Date();
			sbuff.append("Started : " + dateFormatter.format(start) + "\n");
			sbuff.append("Ended : " + dateFormatter.format(end) + "\n");
			sbuff.append("Products updated : " + data.size() + "\n\n");
			sbuff.append("FileNames read : " + fileNamesList.toString() + "\n\n");
			notifyAdmin("Success:  updateInventory() - on" + siteConfig.get("SITE_URL").getValue(), sbuff.toString());
			result.setName("true");
			result.setValue("Success:  updateInventory() - on " + "\n\n" + sbuff.toString());
			return result;

		} else {
			notifyAdmin("2-Exception updateInventory() on " + siteConfig.get("SITE_URL").getValue(), ""+data.size());
			result.setValue("2-Exception updateInventory() on " + "\n\n" + sbuff.toString());
			return result;
		}
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
}