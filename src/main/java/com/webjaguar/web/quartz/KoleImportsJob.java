/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.29.2010
 */

package com.webjaguar.web.quartz;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CsvFeed;
import com.webjaguar.model.Product;
import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.koleImports.KoleImportsCategory;

public class KoleImportsJob extends WebApplicationObjectSupport {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
    
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public void updateInventory() {
		
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		List<CsvFeed> csvFeedList = this.webJaguar.getCsvFeed("kole-inventory");

		// DSI configuration
		String[] config = siteConfig.get("KOLE_IMPORTS").getValue().split(",");
		
		if (!(config.length > 1 && (Boolean) gSiteConfig.get("gSITE_ACTIVE")) || csvFeedList.isEmpty()) {
			// return if kole imports feature is disabled 
			return;
		}
		
		csvFeedList.add(new CsvFeed("minimum_qty"));
		csvFeedList.add(new CsvFeed("incremental_qty"));		
		csvFeedList.add(new CsvFeed("price_1"));
		csvFeedList.add(new CsvFeed("price_2"));
		csvFeedList.add(new CsvFeed("price_3"));
		csvFeedList.add(new CsvFeed("price_4"));
		csvFeedList.add(new CsvFeed("price_5"));
		csvFeedList.add(new CsvFeed("qty_break_1"));
		csvFeedList.add(new CsvFeed("qty_break_2"));
		csvFeedList.add(new CsvFeed("qty_break_3"));
		csvFeedList.add(new CsvFeed("qty_break_4"));
		csvFeedList.add(new CsvFeed("feed"));
		csvFeedList.add(new CsvFeed("feed_new"));
		
		// supplier id
		Integer supplierId = Integer.parseInt(config[1]);
		if (!this.webJaguar.isValidSupplierId(supplierId)) {
			// not a valid supplier
			return;
		}

		List<Map <String, Object>> data = new ArrayList<Map <String, Object>>();
		Map <Long, List<String>> imageData = new HashMap<Long, List<String>>();
		List<Map <String, Object>> categoryData = new ArrayList<Map <String, Object>>();
		
		// categories
		Map <String, KoleImportsCategory> categoryMap = this.webJaguar.getKoleImportsCategoryMap();
		List<KoleImportsCategory> categories = new ArrayList<KoleImportsCategory>();
		Set<String> categorySet = new HashSet<String>();
		
		try {
			
			URL url = new URL(config[0]);
			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(url);
			Element root = doc.getRootElement();
			
			Iterator<Element> results = root.getChild("products").getChildren("item").iterator();
			while (results.hasNext()) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				
				Element item = results.next();
				
				// categories
				String cat = item.getChildText("category").trim();
        		String sub = item.getChildText("subcategory").trim();
        		if (categorySet.add(cat.toLowerCase() + "--" + "*")) {
        			KoleImportsCategory level1 = new KoleImportsCategory();
        			level1.setCat(cat);
        			level1.setSub("*");
    				categories.add(level1);
        		}
        		if (categorySet.add(cat.toLowerCase() + "--" + sub.toLowerCase())) {
        			KoleImportsCategory level2 = new KoleImportsCategory();
        			level2.setCat(cat);
        			level2.setSub(sub);
	    			categories.add(level2);			        			
        		}
				
        		// categories
				List<Integer> catIds = new ArrayList<Integer>();
				Double priceChange = null;
				if (categoryMap.containsKey(cat + "--*")) {
					// level 1 
					KoleImportsCategory category = categoryMap.get(cat + "--*");
					if (category.getCatId() != null) catIds.add(category.getCatId());
					if (category.getPriceChange() != null) priceChange = category.getPriceChange();
				}
				if (categoryMap.containsKey(cat + "--" + sub)) {
					// level 2
					KoleImportsCategory category = categoryMap.get(cat + "--" + sub);
					if (category.getCatId() != null) catIds.add(category.getCatId());
					if (category.getPriceChange() != null) priceChange = category.getPriceChange();
				}

				if (catIds.isEmpty()) {
					// do not download this item
										
				} else {
					
					for (CsvFeed csvFeed: csvFeedList) {
						String sValue = item.getChildText(csvFeed.getHeaderName()).trim();
						if (csvFeed.getColumnName().equals("sku")) {
							map.put("sku", "KI-" + sValue);
    					} else if (csvFeed.isNumeric()) {
							try {
								map.put(csvFeed.getColumnName(), Double.parseDouble(sValue));
							} catch (NumberFormatException e) {
								map.put(csvFeed.getColumnName(), null);									
							}
						} else {
							map.put(csvFeed.getColumnName(), sValue);					
						}	        						
					}
										
					// pricing
					Element pricing = item.getChild("pricing");
					int index = 1;
					Double cost = null;
					boolean hasSmallPackage = false;
					for (Object tier: pricing.getChildren("tier")) {
						Double price = Double.parseDouble(((Element) tier).getChildText("price"));
						Double pack = Double.parseDouble(((Element) tier).getChildText("pack"));
						if (priceChange != null) {							
							map.put("price_" + index, price/pack * (100+priceChange)/100);
						}

						if (index == 1) {
							map.put("minimum_qty", pack);
							map.put("incremental_qty", pack);		
							cost = price/pack;
						} else {
							map.put("qty_break_" + (index - 1), pack);						
						}
						
						hasSmallPackage = true;
						
						index++;
					}
					Element caseE = pricing.getChild("case");
					if (caseE != null) {
						Double price = Double.parseDouble(caseE.getChildText("price"));
						Double pack = Double.parseDouble(caseE.getChildText("pack"));
						if (hasSmallPackage) {
							if (priceChange != null) {
								map.put("price_" + index, price/pack * (100+priceChange)/100);
							}
							map.put("qty_break_" + (index - 1), pack);
						} else {
							map.put("minimum_qty", pack);
							map.put("incremental_qty", pack);
							if (priceChange != null) {
								map.put("price_1", price/pack * (100+priceChange)/100);								
							}
							cost = price/pack;
						}
					}
					
					map.put("cost", cost);
					map.put("feed", "KOLE");
					map.put("feed_new", true);
					
					map.put("productId", this.webJaguar.getProductIdBySku((String) map.get("sku")));
					if (map.get("productId") == null) {
						// new item
						Product newProduct = new Product((String) map.get("sku"), "Kole Feed");
						this.webJaguar.insertProduct(newProduct, null);
						map.put("productId", newProduct.getId());
						
						// add supplier
						Supplier supplier = new Supplier();
						supplier.setId(supplierId);
						supplier.setSku((String) map.get("sku"));
						supplier.setSupplierSku(((String) map.get("sku")).substring(3)); // remove leading KI-
						this.webJaguar.insertProductSupplier(supplier);
					}
					
					// images
					ListIterator<Element> imagesIter = item.getChild("images").getChildren("image").listIterator();
					List<String> images = new ArrayList<String>();
					while(imagesIter.hasNext()) { Object element = imagesIter.next(); }
					// get the large image
					while (imagesIter.hasPrevious()) {
						Element image = (Element) imagesIter.previous();
						images.add(image.getText());
						break; // get last image only
					}
					
					data.add(map);
					imageData.put(((Integer) map.get("productId")).longValue(), images);
					
					// category mapping
					for (Integer catId: catIds) {
						HashMap<String, Object> mapCategory = new HashMap<String, Object>();
						mapCategory.put("productId", map.get("productId"));
						mapCategory.put("categoryId", catId);
						categoryData.add(mapCategory);
					}
					
					if (data.size() == 5000) {
						// products
						this.webJaguar.updateProduct(csvFeedList, data, null);
						
						// associate to supplier
						this.webJaguar.nonTransactionSafeUpdateProductSupplier(supplierId, data);
						data.clear();
						
						// images
						this.webJaguar.updateProductImage(imageData);
						imageData.clear();
						
						// associate to categories		
						this.webJaguar.nonTransactionSafeInsertCategories(categoryData);		
						categoryData.clear();
					}						
				}
				
				if (categories.size() == 5000) {
					this.webJaguar.nonTransactionSafeInsertKoleImportsCategories(categories);
					categories.clear();
				}
			}
					
			if (data.size() > 0) {
				// products
				this.webJaguar.updateProduct(csvFeedList, data, null);
				
				// associate to supplier
				this.webJaguar.nonTransactionSafeUpdateProductSupplier(supplierId, data);
				data.clear();
				
				// images
				this.webJaguar.updateProductImage(imageData);
				imageData.clear();
				
				// associate to categories		
				this.webJaguar.nonTransactionSafeInsertCategories(categoryData);
			}
			
			if (categories.size() > 0) {
				this.webJaguar.nonTransactionSafeInsertKoleImportsCategories(categories);
				categories.clear();
			}
						
			// make old kole imports items inactive
			this.webJaguar.deleteOldProducts("KOLE", true);	

		} catch (Exception e) {
			notifyAdmin("Kole Imports updateInventory() on " + siteConfig.get("SITE_URL").getValue(), e.toString());
			e.printStackTrace();
		}	
	}
    	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
}