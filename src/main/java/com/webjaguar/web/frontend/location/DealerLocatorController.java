/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.22.2009
 */

package com.webjaguar.web.frontend.location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Location;
import com.webjaguar.model.LocationSearch;
import com.webjaguar.model.ZipCode;

public class DealerLocatorController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {	
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
		LocationSearch locationSearch = this.webJaguar.getLocationSearch(request);
		
		Map<String, Object> model = new HashMap<String, Object>();
		locationSearch.setSort(siteConfig.get("LOCATION_SORTING").getValue());
		List<Location> Locations = this.webJaguar.searchLocationList(locationSearch);
		locationSearch.setKeywordsMinChars(Integer.parseInt(siteConfig.get("SEARCH_KEYWORDS_MINIMUM_CHARACTERS").getValue()));
		
		if ( locationSearch.getZipcode() != null && locationSearch.getZipcode().length() > 0 && locationSearch.getRadius() != null ) {
			ZipCode zipObject = this.globalDao.getZipCode( locationSearch.getZipcode() );
			List<Location> cloneLocations = new ArrayList();
			if ( zipObject != null ) {
				locationSearch.setZipcodeObject( zipObject );
				List<ZipCode> zipcodeList = this.globalDao.getZipCodeList( locationSearch );
				for (Location location : Locations) {
					for (ZipCode zipcode : zipcodeList) {
						if ( location.getZip().equals( zipcode.getZipCode() ) ) {
							cloneLocations.add( location );
							break;
						}
					}
				}
			}
			Locations = cloneLocations;
		}
		
		PagedListHolder locationList =  new PagedListHolder(Locations);
		locationList.setPageSize( locationSearch.getPageSize() );
		locationList.setPage( locationSearch.getPage() - 1 );
		model.put("locations", locationList);
		model.put("locationSearch", locationSearch);
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		setLayout(request, model); 
		model.put("locationLayout", this.webJaguar.getSystemLayout("location", request.getHeader("host"), request));
		if (request.getServletPath().endsWith( "company.jhtm" )) {
			return new ModelAndView("frontend/location/list2", "model", model);   
		} else {
			return new ModelAndView("frontend/location/list", "model", model);   
		}
    }

	private void setLayout(HttpServletRequest request, Map<String, Object> model) {
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true" )) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} else
		{
			model.put( "mainCategories", this.webJaguar.getMainCategoryLinks( request, null) );
		}	
	}
}