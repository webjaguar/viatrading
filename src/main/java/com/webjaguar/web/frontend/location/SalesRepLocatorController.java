/* Copyright 2009 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 01.13.2010
 */

package com.webjaguar.web.frontend.location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LocationSearch;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.ZipCode;

public class SalesRepLocatorController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {	
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
		LocationSearch srLocationSearch = this.webJaguar.getLocationSearch(request);
		
		Map<String, Object> model = new HashMap<String, Object>();
		List<SalesRep> salesRepList = this.webJaguar.getSalesRepList();
		if ( srLocationSearch.getZipcode() != null && srLocationSearch.getZipcode().length() > 0 && srLocationSearch.getRadius() != null ) {
			ZipCode zipObject = this.globalDao.getZipCode( srLocationSearch.getZipcode() );
			if ( zipObject != null ) {
				srLocationSearch.setZipcodeObject( zipObject );
				List<ZipCode> zipcodes = this.globalDao.getZipCodeList( srLocationSearch );
				ArrayList<String> zipcodeList = new ArrayList<String>();
				for(ZipCode zipCode: zipcodes){
					zipcodeList.add(zipCode.getZipCode());
				}
				srLocationSearch.setZipcodeList(zipcodeList);
				salesRepList = this.webJaguar.getSalesRepListByTerritoryZipcode(srLocationSearch);
			}
		}
		PagedListHolder srLocationList =  new PagedListHolder(salesRepList);
		srLocationList.setPageSize( srLocationSearch.getPageSize() );
		srLocationList.setPage( srLocationSearch.getPage() - 1 );
		model.put("srLocations", srLocationList);
		model.put("srLocationSearch", srLocationSearch);
		
		setLayout(request, model); 
		model.put("locationLayout", this.webJaguar.getSystemLayout("location", request.getHeader("host"), request));
		
		return new ModelAndView("frontend/location/srLocations", "model", model);   
		
    }

	private void setLayout(HttpServletRequest request, Map<String, Object> model) {
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true" )) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} else
		{
			model.put( "mainCategories", this.webJaguar.getMainCategoryLinks( request, null) );
		}	
	}
}