/* Copyright 2005, 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 04.14.2009
 */

package com.webjaguar.web.frontend.location;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Location;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.form.LocationForm;

public class LocationFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private Map<String, Configuration> siteConfig;
	
	public LocationFormController() {
		setSessionForm(true);
		setCommandName("locationForm");
		setCommandClass(LocationForm.class);
		setFormView("frontend/account/location/locationForm");
		setSuccessView("account.jhtm");
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
    	LocationForm locationForm = (LocationForm) command;
    	UserSession userSession = this.webJaguar.getUserSession(request);	     	
		

    	if (locationForm.isNewLocation()) {
			//
		} else {
			this.webJaguar.updateLocation( locationForm.getLocation() );
		}

    	return new ModelAndView(new RedirectView(getSuccessView()));  
	}
    
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}    
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) {	
		LocationForm locationForm = new LocationForm();
	}
			
    protected Map referenceData(HttpServletRequest request) throws Exception {
	
       	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();
	    map.put("model", myModel);  
	    
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} else{
			myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		}
		
	    return map;
    }  

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
    	UserSession userSession = this.webJaguar.getUserSession(request);	
		LocationForm locationForm = new LocationForm();
		// for now only allow the first location
		List<Location> defaultLocation = this.webJaguar.getLocationListByUserId( userSession.getUserid(), 1 );
		if (defaultLocation == null || defaultLocation.size() < 1) {
			Map<String, Object> myModel = new HashMap<String, Object>();
			myModel.put("message", "Location does not exist.");
			ModelAndView modelAndView = new ModelAndView("frontend/error");
			modelAndView.addObject("model", myModel);
			throw new ModelAndViewDefiningException(modelAndView);				
		}
		locationForm = new LocationForm(defaultLocation.get( 0 ), false);
		
		return locationForm;
	}
}
