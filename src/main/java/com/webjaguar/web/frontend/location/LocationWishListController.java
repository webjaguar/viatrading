/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 04.01.2009
 */

package com.webjaguar.web.frontend.location;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LocationSearch;
import com.webjaguar.model.UserSession;

public class LocationWishListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {	
		
    	UserSession userSession = this.webJaguar.getUserSession(request);
    	
    	// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				Set<Integer> locationIds = new HashSet<Integer>();
				for ( String id : ids ) {
					locationIds.add( Integer.parseInt( id ) );
				}
				this.webJaguar.removeFromLocationWishList( userSession.getUserid(), locationIds );
			}
		}
		
    	LocationSearch search = getLocationSearch( request );    	
    	search.setUserId( userSession.getUserid() );
		Map<String, Object> model = new HashMap<String, Object>();
				
		PagedListHolder locationWishList = 
			new PagedListHolder(this.webJaguar.getLocationWishListByUserid(search));
		locationWishList.setPageSize( 20 );
		locationWishList.setPage( ServletRequestUtils.getIntParameter( request, "page", 1) - 1 );
		model.put("locationWishList", locationWishList);
		
		setLayout(request, model); 
		
		return new ModelAndView("frontend/account/location/locationWishList", "model", model);    	
    }

	private void setLayout(HttpServletRequest request, Map<String, Object> model) {
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} else
		{
			model.put( "mainCategories", this.webJaguar.getMainCategoryLinks( request, null) );
		}	
	}
	
	private LocationSearch getLocationSearch(HttpServletRequest request) {
		LocationSearch LocationSearch = (LocationSearch) request.getSession().getAttribute( "LocationSearch" );
		if (LocationSearch == null) {
			LocationSearch = new LocationSearch();
			request.getSession().setAttribute( "LocationSearch", LocationSearch );
		}
		
		// sorting
		String sortBy = request.getParameter("sortBy");
		if (sortBy != null) {
			switch (ServletRequestUtils.getIntParameter( request, "sortBy", 10 )) {
				case 1:
					LocationSearch.setSort("added");
					break;
				default:
					LocationSearch.setSort("added DESC");
					break;
			}
		}	
		
		// name
		if (request.getParameter("name") != null) {
			LocationSearch.setName( ServletRequestUtils.getStringParameter( request, "name", "" ));
		}	

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				LocationSearch.setPage( 1 );
			} else {
				LocationSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 20 );
			if (size < 10) {
				LocationSearch.setPageSize( 20 );
			} else {
				LocationSearch.setPageSize( size );				
			}
		}	
		return LocationSearch;
	}
}