/* Copyright 2009 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 01.13.2010
 */

package com.webjaguar.web.frontend.location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LocationSearch;
import com.webjaguar.model.ZipCode;

public class CustomerLocatorController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {	
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
		LocationSearch locationSearch = this.webJaguar.getLocationSearch(request);
		Map<String, Object> model = new HashMap<String, Object>();
		if ( locationSearch.getZipcode() != null && locationSearch.getZipcode().length() > 0 && locationSearch.getRadius() != null ) {
			ZipCode zipObject = this.globalDao.getZipCode( locationSearch.getZipcode() );
			if ( zipObject != null ) {
				locationSearch.setZipcodeObject( zipObject );
				List<ZipCode> zipcodes = this.globalDao.getZipCodeList( locationSearch );
				ArrayList<String> zipcodeList = new ArrayList<String>();
				
				for(ZipCode zipCode: zipcodes){
					zipcodeList.add(zipCode.getZipCode());
				}
				locationSearch.setZipcodeList(zipcodeList);
			}
		}
		
		if(siteConfig.get("CUSTOMER_LOCATOR_FIELD").getValue().length() > 0) {
			try {
				locationSearch.setCustomerLocatorField( Integer.parseInt(siteConfig.get("CUSTOMER_LOCATOR_FIELD").getValue()));
			} catch(Exception e) {}
			
		}
		int count = this.webJaguar.getCustomerListByZipCodeCount(locationSearch);
		if (count < locationSearch.getOffset()-1) {
			locationSearch.setPage(1);
		}

		locationSearch.setLimit(locationSearch.getPageSize());
		locationSearch.setOffset((locationSearch.getPage()-1)*locationSearch.getPageSize());

		List<Customer> customerList = this.webJaguar.getCustomerListByZipCode(locationSearch);

		int pageCount = count/locationSearch.getPageSize();
		if (count%locationSearch.getPageSize() > 0) {
			pageCount++;
		}
		model.put("pageCount", pageCount);
		model.put("pageEnd", locationSearch.getOffset()+customerList.size());
		model.put("search", locationSearch);
		model.put("count", count);
		model.put("customerList", customerList);
		
		setLayout(request, model); 
		model.put("locationLayout", this.webJaguar.getSystemLayout("location", request.getHeader("host"), request));
		
		return new ModelAndView("frontend/location/customerLocations", "model", model);   
		
    }

	private void setLayout(HttpServletRequest request, Map<String, Object> model) {
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true" )) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} else
		{
			model.put( "mainCategories", this.webJaguar.getMainCategoryLinks( request, null) );
		}
	}
}