/* Copyright 2005, 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 04.01.2009
 */

package com.webjaguar.web.frontend.location;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.UserSession;

public class AddToWishListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {

    	UserSession userSession = this.webJaguar.getUserSession(request);
    	Integer id = ServletRequestUtils.getIntParameter(request, "l_id", 0);
    	if (id != 0) {
    		this.webJaguar.addToLocationWishList( userSession.getUserid(), id );
    	}

		return new ModelAndView(new RedirectView("locationWishList.jhtm"));
	}
}