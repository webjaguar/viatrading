/* Copyright 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 07.26.2007
 */

package com.webjaguar.web.frontend;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.UserSession;

public class WebFormController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	private JavaMailSenderImpl mailSender;
	public void setWebJaguar(WebJaguarFacade webJaguar)	{  this.webJaguar = webJaguar;	}
	public void setMailSender(JavaMailSenderImpl mailSender) {  this.mailSender = mailSender;  }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    	throws Exception {	

		Map<String, Object> myModel = new HashMap<String, Object>();    	
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		
		if (siteConfig.get( "RECAPTCHA_PRIVATE_KEY" ).getValue().toString().length() > 0) {
			String responseField = ServletRequestUtils.getStringParameter( request, "recaptcha_response_field", null ); 
			String challengeField = ServletRequestUtils.getStringParameter( request, "recaptcha_challenge_field", null ); 
			if ( challengeField != null && responseField != null) {
				// Validate the reCAPTCHA
			    String remoteAddr = request.getRemoteAddr();
			    ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
			    
			    reCaptcha.setPrivateKey(siteConfig.get( "RECAPTCHA_PRIVATE_KEY" ).getValue().toString());
		    
		    	ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(remoteAddr, challengeField, responseField);
		    	if (!reCaptchaResponse.isValid()) {
			    	myModel.put("message", "form.captcha");
			    }
		    } else {
		    	myModel.put("message", "form.captcha");
		    }
		}
	    
		// check if form is submitted
		if (request.getMethod().equalsIgnoreCase( "POST" )) {
			if (myModel.get( "message" ) == null && sentEmail(siteConfig, gSiteConfig, request, myModel)) {
				return new ModelAndView(new RedirectView(myModel.get( "returnURL" ).toString()));
			}
		}

		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
    	return new ModelAndView("frontend/webForm", "model", myModel);    	
    }
    
	private boolean sentEmail(Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig , HttpServletRequest request, Map<String, Object> myModel) throws MessagingException
	{
		UserSession userSession = this.webJaguar.getUserSession(request);	
		SalesRep salesRep = null;
		Customer customer = null;
		if ( (Boolean) gSiteConfig.get( "gSALES_REP" ) && userSession != null ) {
			salesRep = this.webJaguar.getSalesRepByUserId( userSession.getUserid() );
			customer = this.webJaguar.getCustomerById(userSession.getUserid());
		}
		org.springframework.web.multipart.MultipartFile uploadFile = null;

		// send email
		String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat( "MMM-d-yyyy HH:mm:ss z", RequestContextUtils.getLocale( request ) );
		
		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append( dateFormatter.format( new Date() ) + "\n\n" );
		
		// referer
		messageBuffer.append("Referer: " + request.getHeader("referer") + "\n\n" );		
		
		String subject = "NO SUBJECT";
		String returnURL = "";
		
		Enumeration paramNames = request.getParameterNames();
		Map<String, String> paramMap = new TreeMap<String, String>();
		// for actual data to be emailed, form variables must start with two underscores
		// for example: <input type="text" name="__Name1">
		while ( paramNames.hasMoreElements() )
		{
			String paramName = paramNames.nextElement().toString();
			if ( paramName.startsWith( "__" ) &&  paramName.length() > 2)
			{
				paramMap.put(paramName.substring( 2 ), request.getParameter(paramName));
			}
			// get subject
			if (paramName.equalsIgnoreCase( "subject" )) {
				subject = request.getParameter(paramName);
			}
			// get return URL
			if (paramName.equalsIgnoreCase( "returnURL" )) {
				returnURL = request.getParameter(paramName);
			}
		}
    	for (String param: paramMap.keySet()) {
			messageBuffer.append(param + ": " + paramMap.get(param) + "\n\n");
    	}
    	
		// check return URL
		// note: value must not contain "http" meaning path must be relative
		if (returnURL.trim().equals( "" )) {
			myModel.put("message", "webform.returnUrlMissing");
			return false;
		} else if (returnURL.toLowerCase().startsWith( "http://" ) || returnURL.toLowerCase().startsWith( "https://" )) {
			myModel.put("message", "webform.returnUrlNotRelative");
			return false;
		} else {
			myModel.put("returnURL", returnURL);
		}
		
		// construct email messages 
       	MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms,true, "UTF-8");
		try {
			helper.setTo(contactEmail);
			helper.setFrom(contactEmail);
			if (salesRep != null) {
				helper.addCc( salesRep.getEmail() );
			}
		} catch (AddressException e) {
			myModel.put("message", "webform.contactEmailInvalid");
			return false;			
		}
		// get subject
		helper.setSubject(subject);	
		
        boolean hasAttachment = false;
		File tempFolder = new File(getServletContext().getRealPath(""), "temp");
        File tempFile = new File(tempFolder, "temp");
		try {
			// get file if form is submitted with enctype="multipart/form-data"
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	        uploadFile = multipartRequest.getFile( "file" );
	        
	        if ( uploadFile != null && !uploadFile.isEmpty() ) {
	        	if (uploadFile.getContentType() != null && (uploadFile.getContentType().equalsIgnoreCase( "application/pdf" )
	            		|| uploadFile.getContentType().equalsIgnoreCase( "application/msword" ))) {
	        		tempFile = new File(tempFolder, uploadFile.getOriginalFilename());
	    			try	{
	    	       		if (!tempFolder.exists()) {
	    	       			tempFolder.mkdir();
	    	       		}
	    	       		uploadFile.transferTo( tempFile );
	    				helper.addAttachment( uploadFile.getOriginalFilename(), tempFile);
	    				hasAttachment = true;
	    			} 
	    			catch ( IOException e ) {
	    				messageBuffer.append( "\nError uploading file: " );  		
	    				messageBuffer.append( uploadFile.getOriginalFilename() );     
	    				messageBuffer.append( " (temp folder missing)" );
	    			}        		
	        	} else {
					messageBuffer.append( "\nError uploading file: " );  		
					messageBuffer.append( uploadFile.getOriginalFilename() );   
					messageBuffer.append( " (invalid file type)" );
	        	}
			}			
		} catch (ClassCastException e) {
			// do nothing
		}
	
		if(customer!=null){
			messageBuffer.append("User ID: "+ customer.getUsername()+ "\n\n");
			messageBuffer.append("Name: "+ customer.getAddress().getFirstName()+" "+customer.getAddress().getLastName()+ "\n\n");
			messageBuffer.append("Address: "+ customer.getAddress().getAddr1()+" "+customer.getAddress().getAddr2()+ "\n\n");
			messageBuffer.append("City: "+ customer.getAddress().getCity()+ "\n\n");
			messageBuffer.append("State: "+ customer.getAddress().getStateProvince()+ "\n\n");
			messageBuffer.append("Zip: "+ customer.getAddress().getZip()+ "\n\n");
			messageBuffer.append("Country: "+ customer.getAddress().getCountry()+ "\n\n");
			messageBuffer.append("Phone: "+ customer.getAddress().getPhone()+ "\n\n");
		}
		helper.setText(messageBuffer.toString());
	
		// now send it
        try {
            mailSender.send(mms);
        }
        catch(MailException ex) {
			myModel.put("message", "webform.notSuccessful");
			if (hasAttachment) {
				tempFile.delete();
			}
			return false;
        }
		if (hasAttachment) {
			tempFile.delete();
		}
        
        return true;
	}
}
