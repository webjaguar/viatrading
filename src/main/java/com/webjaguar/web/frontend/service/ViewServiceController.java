/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.27.2007
 */

package com.webjaguar.web.frontend.service;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ServiceWork;
import com.webjaguar.model.UserSession;

public class ViewServiceController implements Controller {

	private WebJaguarFacade webJaguar;

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
    	
    	UserSession userSession = this.webJaguar.getUserSession(request);
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		ServiceWork service = this.webJaguar.getService(ServletRequestUtils.getIntParameter(request, "num", -1));
		boolean valid = false;
		if (service != null) {
			if (service.getItem().getCustomer() != null) {
				for (Customer customer:this.webJaguar.getFamilyTree( service.getItem().getCustomer().getId(), "up" )) {
					if (customer.getId().compareTo( userSession.getUserid() ) == 0) {
						valid = true;
						break;
					}
				}
			}
		}
		
		if (service != null && valid) {
			model.put("service", service);
			
			// get product
			Product product = this.webJaguar.getProductById( this.webJaguar.getProductIdBySku( service.getItem().getSku() ), 0, false, null );
			if (product == null) {
				product = new Product();
			}
			Class c = Product.class;
			Method m = null;
			Object arglist[] = null;
			for ( ProductField productField : this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")) ) {
				if ( productField.isEnabled() && productField.isServiceField() ) {
					try {
						m = c.getMethod( "getField" + productField.getId() );
						productField.setValue( (String) m.invoke( product, arglist ) );						
					} catch (Exception e) {
						// do nothing
					}
					product.addProductField( productField );
				}
			}			
			model.put( "product", product );
			model.put( "countries", this.webJaguar.getCountryMap() );
		} else {
      	  	model.put("message", "service.exception.notfound");			
		}
		
		setLayout(request); 
    	
		return new ModelAndView("frontend/account/service", "model", model);
	}
    
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private void setLayout(HttpServletRequest request) {
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		}		
	}
}
