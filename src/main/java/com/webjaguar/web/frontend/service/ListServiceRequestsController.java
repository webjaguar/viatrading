/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.26.2007
 */

package com.webjaguar.web.frontend.service;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.ServiceWork;
import com.webjaguar.model.ServiceSearch;
import com.webjaguar.model.ServiceableItem;
import com.webjaguar.model.ServiceableItemSearch;
import com.webjaguar.model.UserSession;

public class ListServiceRequestsController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {	

		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("newService.jhtm"));				
		}
		
    	UserSession userSession = this.webJaguar.getUserSession(request);
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
				
		// get service requests
		ServiceSearch search = getSearch(request);
		ServiceableItemSearch itemSearch = new ServiceableItemSearch();
		for (Customer customer:this.webJaguar.getFamilyTree( userSession.getUserid(), "down" )) {
			search.addUserId(customer.getId());
			itemSearch.addUserId(customer.getId());
		}
		List<ServiceableItem> items = this.webJaguar.getServiceableItemList(itemSearch);
		if (items.size() > 0) {
			model.put("serviceItems", items);
		}
		model.put("servicesSearch", search);
		
		List<ServiceWork> services = this.webJaguar.getServiceList(search);
		model.put("hasTrackNumIn", this.webJaguar.getServiceCount(search, "tracknum_in") > 0);
		model.put("hasTrackNumOut", this.webJaguar.getServiceCount(search, "tracknum_out") > 0);
		model.put("hasItemFrom", this.webJaguar.getServiceCount(search, "item_from") > 0);
		model.put("hasItemTo", this.webJaguar.getServiceCount(search, "item_to") > 0);
		boolean hasShipDate = false;
		PagedListHolder servicesList = new PagedListHolder(services);
		servicesList.setPageSize(search.getPageSize());
		servicesList.setPage(search.getPage()-1);
		Iterator<ServiceWork> iter = servicesList.getPageList().iterator();
		while (iter.hasNext()) {
			if (iter.next().getShipDate() != null) {
				hasShipDate = true;
				break;
			}
		}		
		model.put("hasShipDate", hasShipDate);
		model.put("services", servicesList);
		model.put("statuses", this.getStatuses());
		
		setLayout(request); 
		
		return new ModelAndView("frontend/account/services", "model", model);    	
    }

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private void setLayout(HttpServletRequest request) {
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		}		
	}
	

	private Set<String> getStatuses() {
		Set<String> statuses = new TreeSet<String>();
			statuses.add("Completed");
			statuses.add("Pending");
			statuses.add("Customer Contacted");
			statuses.add("Customer Unavailable");
			statuses.add("In-Process");
			statuses.add("Dispatched");
			statuses.add("Parts Ordered");
			statuses.add("Awaiting Pickup");
			statuses.add("Awaiting Approval");
			statuses.add("Estimate Requested");
			statuses.add("Out for Delivery");
			statuses.add("Parts on Backorder");
			statuses.add("Received");
			statuses.add("Pre Testing");
			statuses.add("Post Testing");
			statuses.add("Awaiting Support");
			statuses.add("In Diagnostics");
			statuses.add("Canceled");
			statuses.add("Delivered");
			statuses.add("To be Reuse");
			statuses.add("Completed - Pending Invoicing");
			statuses.add("Declined Repair");	
		return statuses;
	}
	
	private ServiceSearch getSearch(HttpServletRequest request) {
		ServiceSearch search = new ServiceSearch();
		
		// status
		if (request.getParameter("status") != null) {
			search.setStatus(ServletRequestUtils.getStringParameter( request, "status", "" ));
		}
		
		// service number
		if (request.getParameter("serviceNum") != null) {
			search.setServiceNum(ServletRequestUtils.getStringParameter( request, "serviceNum", "" ));
		}

		// item id
		if (request.getParameter("itemId") != null) {
			search.setItemId(ServletRequestUtils.getStringParameter( request, "itemId", "" ));
		}
		
		// purchase order
		if (request.getParameter("purchaseOrder") != null) {
			search.setPurchaseOrder(ServletRequestUtils.getStringParameter(request, "purchaseOrder", ""));
		}
		
		if (request.getParameter("trackNumIn") != null) {
			search.setTrackNumIn(ServletRequestUtils.getStringParameter(request, "trackNumIn", ""));
		}

		if (request.getParameter("trackNumOut") != null) {
			search.setTrackNumOut(ServletRequestUtils.getStringParameter(request, "trackNumOut", ""));
		}

		if (request.getParameter("itemFrom") != null) {
			search.setItemFrom(ServletRequestUtils.getStringParameter(request, "itemFrom", ""));
		}

		if (request.getParameter("itemTo") != null) {
			search.setItemTo(ServletRequestUtils.getStringParameter(request, "itemTo", ""));
		}

		// 3rd party work order
		if (request.getParameter("wo3rd") != null) {
			search.setWo3rd(ServletRequestUtils.getStringParameter(request, "wo3rd", ""));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}	
		return search;
	}
	
}
