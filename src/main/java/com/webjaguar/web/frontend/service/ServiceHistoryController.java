/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 04.25.2008
 */

package com.webjaguar.web.frontend.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.ServiceableItem;
import com.webjaguar.model.UserSession;

public class ServiceHistoryController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {
		
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
    	UserSession userSession = this.webJaguar.getUserSession(request);
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
				
		// get serviceable item
		ServiceableItem item = this.webJaguar.getServiceableItemById(ServletRequestUtils.getIntParameter(request, "id", -1));
		boolean valid = false;
		if (item != null) {
			if (item.getCustomer() != null) {
				for (Customer customer:this.webJaguar.getFamilyTree(item.getCustomer().getId(), "up")) {
					if (customer.getId().compareTo(userSession.getUserid()) == 0) {
						valid = true;
						break;
					}
				}
			}
		}
		
		if (item != null && valid) {
			this.webJaguar.getServiceHistory(item, model, webJaguar, gSiteConfig);
		} else {
			return new ModelAndView(new RedirectView("account_serviceItems.jhtm"));
		}

		setLayout(request); 
		
		return new ModelAndView("frontend/account/serviceHistory", "model", model);    	
    }

	private void setLayout(HttpServletRequest request) {
		Layout layout = (Layout) request.getAttribute("layout");
		Map siteConfig = (Map) request.getAttribute("siteConfig");
		if (((Configuration) siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES")).getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		}		
	}
	

	
}
