/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.01.2007
 */

package com.webjaguar.web.frontend.service;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ServiceWork;
import com.webjaguar.model.ServiceableItem;
import com.webjaguar.model.ServiceableItemSearch;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.ServiceForm;

public class ServiceWizardController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }

	private Map<String, Configuration> siteConfig;
	
	public ServiceWizardController() {
		setCommandName("serviceForm");
		setCommandClass(ServiceForm.class);
		setPages(new String[] {"frontend/service/step1", "frontend/service/step2"});
	}
	
    public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {	
    	ServiceForm serviceForm = (ServiceForm) command;
    	
    	// check serviceable item
    	boolean valid = false;
    	Integer itemId = serviceForm.getService().getItem().getId();
    	ServiceableItem serviceItem = null;
    	if (itemId == null) {
    		itemId = this.webJaguar.getServiceableItemId(null, serviceForm.getService().getItem().getSerialNum());
    	}
    	if (itemId == null) {
    		valid = true;
    	} else {
    		serviceItem = this.webJaguar.getServiceableItemById(itemId);
        	UserSession userSession = this.webJaguar.getUserSession(request);
			if (serviceItem.getCustomer() != null) {
				for (Customer customer:this.webJaguar.getFamilyTree( serviceItem.getCustomer().getId(), "up" )) {
					if (customer.getId().compareTo( userSession.getUserid() ) == 0) {
						valid = true;
						break;
					}
				}						
			}    		
    	}
    	
    	if (valid) {

    		// check files first
    		Boolean poPDF = null, 
    				wo3rdPDF = null; 
    		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
    		MultipartFile file_purchaseOrder = multipartRequest.getFile("file_purchaseOrder");
    		if (file_purchaseOrder != null && !file_purchaseOrder.isEmpty()) {
    			if (file_purchaseOrder.getContentType().equalsIgnoreCase("application/pdf")) {
    				poPDF = true;
    			} else {
    				errors.rejectValue("service.purchaseOrder", "pdffile.invalid");
    				request.setAttribute("file_purchaseOrder_error", true);
    				poPDF = false;
    			}
    		}
    		MultipartFile file_wo3rd = multipartRequest.getFile("file_wo3rd");
    		if (file_wo3rd != null && !file_wo3rd.isEmpty()) {
    			if (file_wo3rd.getContentType().equalsIgnoreCase("application/pdf")) {
    				wo3rdPDF = true;
    			} else {
    				errors.rejectValue("service.wo3rd", "pdffile.invalid");
    				request.setAttribute("file_wo3rd_error", true);
    				wo3rdPDF = false;
    			}
    		}
    		if ((poPDF != null && !poPDF) || (wo3rdPDF != null && !wo3rdPDF)) {
				return showPage(request, errors, 1);    			
    		}
    		
    		ServiceWork service = serviceForm.getService();
    		
        	this.webJaguar.insertService(serviceForm.getService());   
        	
        	if ((poPDF != null && poPDF) || (wo3rdPDF != null && wo3rdPDF)) {
            	File baseFile = new File(getServletContext().getRealPath("/assets/pdf/"));
        		Properties prop = new Properties();
        		try {
        			prop.load( new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
        			if (prop.get("site.root") != null) {
        				baseFile = new File((String) prop.get("site.root") + "/assets/pdf/");
        			}
        		} catch (Exception e) {}
            	    	
        		if (!baseFile.exists()) {
        			baseFile.mkdir();
        		}
            	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
            	if (poPDF) {
                	String fileName = "s" + service.getServiceNum() + "-" + dateFormatter.format(new Date()) + ".pdf";
        			File tempFile = new File(baseFile, fileName);
        			file_purchaseOrder.transferTo(tempFile);
            		service.setPurchaseOrderPdfUrl(fileName);
            		this.webJaguar.updatePdfService(service, "po");
            	}
            	if (wo3rdPDF) {
            		// make sure to add a second
                	String fileName = "s" + service.getServiceNum() + "-" + dateFormatter.format(new Date(System.currentTimeMillis() + 1000)) + ".pdf";
        			File tempFile = new File(baseFile, fileName);
        			file_wo3rd.transferTo(tempFile);
                	service.setWo3rdPdfUrl(fileName);
            		this.webJaguar.updatePdfService(service, "wo3rd");
            	}
        	}
        	
        	notifyAdmin(service, request);
    		if (serviceItem != null && serviceItem.getAddress() != null) {
            	notify(service, serviceItem, request);    			
    		}   		
    	} else {
    		errors.rejectValue("service.item.serialNum", "INVALID_SERIAL_NUM");
        	return showForm(request, response, errors);
    	}
		
    	return new ModelAndView(new RedirectView("account_services.jhtm"));
    }
    
    private void uploadPdf(HttpServletRequest request) throws Exception {
    	File baseFile = new File(getServletContext().getRealPath("/assets/pdf/"));
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				baseFile = new File((String) prop.get("site.root") + "/assets/pdf/");
			}
		} catch (Exception e) {}
    	    	
		if (!baseFile.exists()) {
			baseFile.mkdir();
		}
		
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile file = multipartRequest.getFile("file_purchaseOrder");
		if (file != null && !file.isEmpty()) {
			File tempFile = new File(baseFile, file.getOriginalFilename());
			file.transferTo(tempFile);
		}
    }
 
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
    	throws Exception {    	
    	UserSession userSession = this.webJaguar.getUserSession(request);
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
		ServiceForm serviceForm = (ServiceForm) command;
		
       	Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		map.put("model", model); 
		map.put( "countries", this.webJaguar.getCountryMap() );
		
		switch ( page ) {
			case 0:			
				// get serviceable items
				ServiceableItemSearch search = new ServiceableItemSearch();
				for (Customer customer:this.webJaguar.getFamilyTree( userSession.getUserid(), "down" )) {
					search.addUserId(customer.getId());
				}
				List<ServiceableItem> items = this.webJaguar.getServiceableItemList(search);
				if (items.size() > 0) {
					map.put("serviceItems", items);
				}
				break;
			case 1:
				if (serviceForm.getService().getItem().getId() != null) {
					ServiceableItem serviceItem = this.webJaguar.getServiceableItemById(serviceForm.getService().getItem().getId());
					if (serviceItem != null) {
						map.put("serviceItem", serviceItem);
						// get product
						Product product = this.webJaguar.getProductById( this.webJaguar.getProductIdBySku( serviceItem.getSku() ), 0, false, null );
						if (product == null) {
							product = new Product();
						}
						Class<Product> c = Product.class;
						Method m = null;
						Object arglist[] = null;
						for ( ProductField productField : this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS")) ) {
							if ( productField.isEnabled() && productField.isServiceField() ) {
								try {
									m = c.getMethod( "getField" + productField.getId() );
									productField.setValue( (String) m.invoke( product, arglist ) );						
								} catch (Exception e) {
									// do nothing
								}
								product.addProductField( productField );
							}
						}			
						model.put( "product", product );
						
						if (serviceItem.getAlternateContacts() != null && serviceItem.getAlternateContacts().trim().length() > 0) {
							String[] contacts = serviceItem.getAlternateContacts().split("[\n]"); // split by commas
							List<String> alternateContacts = new ArrayList<String>();
							for (int i=0; i<contacts.length; i++) {
								if (contacts[i].trim().length() > 0) {
									alternateContacts.add(contacts[i].trim());
								}
							}
							if (alternateContacts.size() > 0) {
								map.put("alternateContacts", alternateContacts);
							}
						}
						
					}
				}
				break;
		}
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		return map;
    }
    
    protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {

    	UserSession userSession = this.webJaguar.getUserSession(request);
    	ServiceForm serviceForm = (ServiceForm) command;
    	
		switch ( page ) {
			case 0:			
				// get serviceable item
				if (serviceForm.getService().getItem().getId() == null && 
						(serviceForm.getService().getItem().getSerialNum() != null && !serviceForm.getService().getItem().getSerialNum().trim().equals( "" ))) {
					Integer itemId = this.webJaguar.getServiceableItemId(null, serviceForm.getService().getItem().getSerialNum());
					boolean valid = false;
			    	if (itemId == null) {
			    		// new item
			    		valid = true;
			    	} else {
						ServiceableItem serviceItem = this.webJaguar.getServiceableItemById(itemId);
						if (serviceItem.getCustomer() != null) {
							for (Customer customer:this.webJaguar.getFamilyTree( serviceItem.getCustomer().getId(), "up" )) {
								if (customer.getId().compareTo( userSession.getUserid() ) == 0) {
									valid = true;
									break;
								}
							}						
						}
					}
					if (valid) {
						serviceForm.getService().getItem().setId(itemId);

					} else {
						errors.rejectValue("service.item.serialNum", "INVALID_SERIAL_NUM");
					}
				}
				// check if serviceable item is still in service
				ServiceWork lastService = this.webJaguar.getLastService(serviceForm.getService().getItem());
				if (lastService != null && !lastService.getStatus().equalsIgnoreCase("completed")) {
					Object[] errorArgs = {siteConfig.get("SERVICEABLE_ITEM_TITLE").getValue()};
					errors.rejectValue("service.item.id", "EXISTING_SERVICE_REQUEST", errorArgs, 
							"There is an existing request for this.");
					request.setAttribute("lastService", lastService);
				}
				break;		
		}
    }
    
    protected void validatePage(Object command, Errors errors, int page) {
		ServiceForm serviceForm = (ServiceForm) command;
		
		switch (page) {
			case 0:
				if (serviceForm.getService().getItem().getId() == null) {
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "service.item.serialNum", "form.required");
				}
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "service.problem", "form.required");
				Object[] errorArgs = {""};
				if (serviceForm.getService().getTrackNumInCarrier().length() > 0) {
					errorArgs[0] = getApplicationContext().getMessage("trackNum", null, null);
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "service.trackNumIn", "form.isRequired", errorArgs);
				}
				if (serviceForm.getService().getTrackNumIn().length() > 0) {
					errorArgs[0] = getApplicationContext().getMessage("shippingCarrier", null, null);
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "service.trackNumInCarrier", "form.isRequired", errorArgs);
				}
				break;
		}
    }
    
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		UserSession userSession = this.webJaguar.getUserSession(request);
		Customer customer = new Customer();
		customer.setId( userSession.getUserid() );
		
		ServiceForm form = new ServiceForm();
		form.setService( new ServiceWork() );
		form.getService().setItem( new ServiceableItem() );
		form.getService().getItem().setCustomer( customer );
		
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
		return form;
	}
	
	private void notifyAdmin(ServiceWork service, HttpServletRequest request) {
		// send email notification
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("New Service Request on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" +
					"You have a new Service Request on your site.\n\n" +
					siteURL + "admin/service/service.jhtm?num=" + service.getServiceNum());
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
	
	private void notify(ServiceWork service, ServiceableItem serviceItem, HttpServletRequest request) {
		Product product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(serviceItem.getSku()), 0, false, null);
		
		if (serviceItem.getAddress().getEmail() == null || !validateEmails(serviceItem.getAddress().getEmail())) {
			// do not send if emails are invalid
			return;
		}

		// send email notification
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));
    	
    	MimeMessage msg = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(msg);

		try {
			// to emails
			String[] emails = serviceItem.getAddress().getEmail().split( "[,]" ); // split by commas
			for ( int x = 0; x < emails.length; x++ ) {
				if (emails[x].trim().length() > 0) {
					helper.addTo(emails[x].trim());
				}
			}
			
			helper.setFrom(contactEmail);
			helper.setBcc(contactEmail);
			helper.setSubject("Service (" + service.getServiceNum() + ") requested at " + siteConfig.get("SITE_NAME").getValue());

			// message
			StringBuffer messageBuffer = new StringBuffer();
	    	messageBuffer.append(
	    				dateFormatter.format(new Date()) + "\n\n" +
						siteURL + "service.jhtm?num=" + service.getServiceNum() + "\n" +
						"\n-----------PRINTER LOCATION---------------------------------------------\n\n" +
						serviceItem.getAddress().getCompany() + "\n" +
						serviceItem.getAddress().getFirstName() + " " + serviceItem.getAddress().getLastName() + "\n" +
						serviceItem.getAddress().getAddr1() + "\n");
			if (!serviceItem.getAddress().getAddr2().trim().equals("")) {
				messageBuffer.append(
						serviceItem.getAddress().getAddr2() + "\n");
			}
			messageBuffer.append(				
						serviceItem.getAddress().getCity() + ", " + serviceItem.getAddress().getStateProvince() + " " + serviceItem.getAddress().getZip() + "\n" +
						serviceItem.getAddress().getCountry() + "\n" +
						"Tel: " + serviceItem.getAddress().getPhone() + "\n" +
						"Fax: " + serviceItem.getAddress().getFax() + "\n" +
						"Cell: " + serviceItem.getAddress().getCellPhone() + "\n" +
						"\n-----------SERVICE REQUESTED----------------------------------------------\n\n" +
						"Service Request #:\n" +
					    "    " + service.getServiceNum() + "\n\n" +						
					    "Printer ID:\n" +
					    "    " + serviceItem.getItemId() + "\n\n" +	
					    "Printer S/N:\n" +
					    "    " + serviceItem.getSerialNum() + "\n\n" +	
					    "Sku:\n" +
					    "    " + serviceItem.getSku() + "\n\n");
			if (product != null) {
				messageBuffer.append(
					    "Description:\n" +
					    "    " + product.getName() + "\n\n");
			}
			messageBuffer.append(
						"Problem/Error:\n" +
					    "    " + service.getProblem());
						
			helper.setText(messageBuffer.toString());
			
			mailSender.send(msg);
		} catch (Exception ex) {
			ex.printStackTrace();
		} 
		
	}
	
    private boolean validateEmails(String field) {
		boolean valid = true;
		String[] emails = field.split( "[,]" ); // split by commas
		int counter = 0;
		for ( int x = 0; x < emails.length; x++ ) {
			if (emails[x].trim().length() > 0) {
				if (Constants.validateEmail(emails[x].trim(), null)) {
					counter++;
				} else {
					return false;
				}
			}
		}
		if (counter < 1) {
			return false;
		}
		
		return valid;
    }
}
