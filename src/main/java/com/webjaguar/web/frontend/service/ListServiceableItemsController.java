/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 04.20.2008
 */

package com.webjaguar.web.frontend.service;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.ServiceableItemSearch;
import com.webjaguar.model.UserSession;

public class ListServiceableItemsController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {
		
    	UserSession userSession = this.webJaguar.getUserSession(request);
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
				
		// get serviceable items
		ServiceableItemSearch search = getSearch(request);
		for (Customer customer:this.webJaguar.getFamilyTree( userSession.getUserid(), "down" )) {
			search.addUserId(customer.getId());
		}
		search.setWithServiceTotal(true);
		model.put("search", search);
		
		PagedListHolder items = 
			new PagedListHolder(this.webJaguar.getServiceableItemList(search));
		items.setPageSize(search.getPageSize());
		items.setPage(search.getPage()-1);
		model.put("items", items);
		
		setLayout(request); 
		
		return new ModelAndView("frontend/account/serviceItems", "model", model);    	
    }

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private void setLayout(HttpServletRequest request) {
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		}		
	}
	
	private ServiceableItemSearch getSearch(HttpServletRequest request) {
		ServiceableItemSearch search = new ServiceableItemSearch();
		
		// item id
		if (request.getParameter("itemId") != null) {
			search.setItemId(ServletRequestUtils.getStringParameter( request, "itemId", "" ));
		}
		
		// serial number
		if (request.getParameter("serialNum") != null) {
			search.setSerialNum(ServletRequestUtils.getStringParameter( request, "serialNum", "" ));
		}

		// sku
		if (request.getParameter("sku") != null) {
			search.setSku( ServletRequestUtils.getStringParameter( request, "sku", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}	
		return search;
	}
	
}
