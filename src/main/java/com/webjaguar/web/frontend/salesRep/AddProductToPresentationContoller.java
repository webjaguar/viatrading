/* Copyright 2012 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.salesRep;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Presentation;
import com.webjaguar.model.Product;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.form.ProductPresentationForm;

public class AddProductToPresentationContoller implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		ProductPresentationForm productPresentationForm = new ProductPresentationForm();
		SalesRep salesRep = this.webJaguar.getSalesRep(request);
		
		Integer productId = ServletRequestUtils.getIntParameter(request, "id", -1);
		String productIds[] = request.getParameterValues("product.id");
		List<Product> products = new ArrayList<Product>();
		if(productId != null) {
			products.add(this.webJaguar.getProductById(productId, request));
		}
		if(productIds != null) {
			for ( int i = 0; i < productIds.length; i++ ) {
				if(ServletRequestUtils.getBooleanParameter(request, "__selected_"+productIds[i],false)){
					products.add(this.webJaguar.getProductById(Integer.parseInt( productIds[i] ), request));
				}
			}
		}
		productPresentationForm.setProduct(products);
		
		Presentation presentationSearch = new Presentation();
		presentationSearch.setSaleRepId(salesRep.getId());
		List<Presentation> presentations = new ArrayList<Presentation>();
		List<Presentation> presentationList = webJaguar.getPresentationList(presentationSearch);
		for(Presentation presentationExisting : presentationList) {
			for(Product product: products){
				boolean existsInPresentation = false;
				if (presentationExisting.getProduct() != null) {
					for(Product productInPresentation : presentationExisting.getProduct()) {
						if(productInPresentation.getId() == product.getId()) {
							existsInPresentation = true;
						}
					}
				}
				if(! existsInPresentation && !presentations.contains(presentationExisting)) {
					presentations.add(presentationExisting);
				}
				
			}
		}
		productPresentationForm.setPresentationList(presentations);
		String presentationIds[] = request.getParameterValues("presentationId");
		if(presentationIds == null) {
			productPresentationForm.setAdded(false);
		}
		String sku[] = request.getParameterValues("__selected_sku");
		if(sku != null ) {
			this.webJaguar.addProductToPresentation(sku , null, salesRep.getId() );
			List<Product> insertedProductsList = new ArrayList<Product>();
			for(int i=0;i<sku.length;i++) {
				Product p = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(sku[i]), request);
				if(p!=null){
					insertedProductsList.add(p);
				}
			}
			productPresentationForm.setProduct(insertedProductsList);
		}
		if(presentationIds != null){
			for(int i = 0;i< presentationIds.length; i++) {
				Presentation presentationExisting = this.webJaguar.getPresentation(Integer.parseInt( presentationIds[i]));
				this.webJaguar.addProductToPresentation(sku,presentationExisting.getId(),salesRep.getId());
				productPresentationForm.setAdded(true);
			}
		}
		Layout layout = (Layout) request.getAttribute("layout");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		model.put("salesrepLayout", this.webJaguar.getSystemLayout("salesRep", request.getHeader("host"), request));
    	
		model.put("productPresentationForm", productPresentationForm);
		return new ModelAndView("frontend/salesRep/presentation/ajaxAddProductToPresentation", "model", model); 
	}
}
