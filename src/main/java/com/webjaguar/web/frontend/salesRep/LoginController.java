/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.02.2009
 */

package com.webjaguar.web.frontend.salesRep;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.SalesRep;

public class LoginController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		SalesRep salesRep = this.webJaguar.getSalesRep(request);
		if (salesRep != null) {
			// check if sales rep is logged in
			return new ModelAndView(new RedirectView("salesRepAccount.jhtm"));
		}
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		Map<String, Object> model = new HashMap<String, Object>();
		
		String accountNumber = ServletRequestUtils.getStringParameter(request, "accountNumber", "");
		String password = ServletRequestUtils.getStringParameter(request, "password", "");
		String forwardAction = request.getParameter("forwardAction");
		
		if (accountNumber.trim().length() > 0) {
			SalesRep search = new SalesRep();
			search.setAccountNumber(accountNumber);
			search.setPassword(password);
			try {
				salesRep = this.webJaguar.getSalesRep(search);				
			} catch (UncategorizedSQLException e) {
				model.put("message", "SALESREP_LOGIN_EXCEPTION");
			}
		}
		
		if (salesRep == null) {
			model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
			if ((accountNumber.length() > 0 || password.length() > 0) && !model.containsKey("message")) {
				model.put("message", "SALESREP_LOGIN_INVALID");				
			}
			
			ModelAndView modelAndView = new ModelAndView("frontend/salesRep/login", "model", model);
			if (siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
				modelAndView = new ModelAndView("frontend/layout/template6/salesRep/login", "model", model);
			}

			if (forwardAction != null) modelAndView.addObject("signonForwardAction", forwardAction);
			
			Layout layout = (Layout) request.getAttribute("layout");
			if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
				layout.setLeftBarTopHtml("");
				layout.setLeftBarBottomHtml("");
				layout.setHideLeftBar(true);
			} 
			
			model.put("loginLayout", this.webJaguar.getSystemLayout("srLogin", request.getHeader("host"), request));
			
			return modelAndView;			
		} else {
			// update login statistics
			this.webJaguar.nonTransactionSafeLoginStats(salesRep);
			request.setAttribute( "sessionSalesRep", salesRep );
			Cookie salesRepCookie = new Cookie("salesrep", salesRep.getToken());
			response.addCookie(salesRepCookie);
			
			if (forwardAction != null) {
				response.sendRedirect(forwardAction);
				return null;
			} else {
				return new ModelAndView(new RedirectView("salesRepAccount.jhtm"));
			}
		}
	}
}
