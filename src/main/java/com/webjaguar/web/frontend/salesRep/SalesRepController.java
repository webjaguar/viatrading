/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.02.2009
 */

package com.webjaguar.web.frontend.salesRep;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.SalesRep;

public class SalesRepController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	// joanbaker 
	private JdbcTemplate jdbcTemplate;
	public void setDataSource(DataSource dataSource) { 
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {	
    	SalesRep salesRep = this.webJaguar.getSalesRep(request);
		if(request.getParameter("__sendEmail") != null){
    		HttpSession session = request.getSession();
			String ids[] = request.getParameterValues("__selected_id");
			if(ids != null && ids.length != 0){
				Set<Integer> customerIds = new HashSet<Integer>();
				for (int i=0; i<ids.length; i++) {
					customerIds.add( Integer.parseInt(ids[i]) );
				}
			Map<String, Object> salesRepParams = new HashMap<String, Object>();
			salesRepParams.put("salesRep", salesRep);
			salesRepParams.put("customerIds", customerIds);
			session.setAttribute("salesRepParams", salesRepParams);
			
			return new ModelAndView( new RedirectView("srMassEmail.jhtm"));
			}
		}
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		model.put("salesRep", salesRep);
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");		
		
		int salesrepId = salesRep.getId();
		String salesrepAccountNumber = salesRep.getAccountNumber();
		
		// sub accounts
		if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = this.webJaguar.getSalesRepList(salesRep.getId());
			for (SalesRep thisSalesRep: salesReps) {
				salesRepMap.put(thisSalesRep.getId(), thisSalesRep);				
			}
			// associate reps to parents
			for (SalesRep thisSalesRep: salesReps) {
				if (salesRepMap.containsKey(thisSalesRep.getParent())) {
					salesRepMap.get(thisSalesRep.getParent()).getSubAccounts().add(thisSalesRep);					
				}
			}
			salesRep.setSubAccounts(salesRepMap.get(salesRep.getId()).getSubAccounts());
			
			salesrepId = ServletRequestUtils.getIntParameter(request, "id", salesRep.getId());
			SalesRep account = new SalesRep();
			if (!salesRepMap.containsKey(salesrepId)) {
				salesrepId = salesRep.getId();
				account = salesRep;
			} else if (salesRep.getId().compareTo(salesrepId) != 0) {
				// valid 
				account = this.webJaguar.getSalesRepById(salesrepId);
				salesrepAccountNumber = account.getAccountNumber();
			}
			model.put("account", account);
		}
		
		String sort = ServletRequestUtils.getStringParameter(request, "sort", "");
		if (sort.equals("")) sort = "company";
		model.put("sort", sort);
		List<Customer> customersList = this.webJaguar.getCustomerListBySalesRep(salesrepId, sort);
		PagedListHolder customers = new PagedListHolder(customersList);
		customers.setPageSize(ServletRequestUtils.getIntParameter(request, "size", 10));
		customers.setPage(ServletRequestUtils.getIntParameter(request, "page", 1) - 1);
		model.put("customers", customers);
		int count = customersList.size();
		model.put("count", count);
		
		if (siteConfig.get("MAS200").getValue().trim().length() > 0) {
			model.put("jbdRepSales", getJbdRepSales(salesrepAccountNumber));
		}
		
		Layout layout = (Layout) request.getAttribute("layout");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		
		model.put("currentPage", "customer");
		model.put("salesrepLayout", this.webJaguar.getSystemLayout("salesRep", request.getHeader("host"), request));
		
		return new ModelAndView("frontend/salesRep/customers", "model", model);    	
    }
    
    private Map getJbdRepSales(String SalesPersonNumber) {
    	Object[] args = { SalesPersonNumber };
    	try {
        	return jdbcTemplate.queryForMap(
        			"SELECT " +
        			"    * " +
        			"FROM " +
        			"  MASREPSALES " +
        			"WHERE " +
        			"  SalesPersonNumber = ? "
        		, args);    		
    	} catch (Exception e) {
    		return null;
    	}
    }
}
