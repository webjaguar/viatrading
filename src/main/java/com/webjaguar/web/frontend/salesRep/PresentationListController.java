package com.webjaguar.web.frontend.salesRep;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Presentation;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.domain.Encoder;

public class PresentationListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		Map<String, Object> model = new HashMap<String, Object>();
		SalesRep salesRep = this.webJaguar.getSalesRep(request);
		String presentationName = request.getParameter("presentationName");
		Presentation presentationSearch = new Presentation();
		presentationSearch.setName(presentationName);
		
		
		int salesrepId = salesRep.getId();
		String salesrepAccountNumber = salesRep.getAccountNumber();
		
		// sub accounts
		if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
			Map <Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
			List<SalesRep> salesReps = this.webJaguar.getSalesRepList(salesRep.getId());
			for (SalesRep thisSalesRep: salesReps) {
				salesRepMap.put(thisSalesRep.getId(), thisSalesRep);				
			}
			// associate reps to parents
			for (SalesRep thisSalesRep: salesReps) {
				if (salesRepMap.containsKey(thisSalesRep.getParent())) {
					salesRepMap.get(thisSalesRep.getParent()).getSubAccounts().add(thisSalesRep);					
				}
			}
			salesRep.setSubAccounts(salesRepMap.get(salesRep.getId()).getSubAccounts());
			
			salesrepId = ServletRequestUtils.getIntParameter(request, "id", salesRep.getId());
			SalesRep account = new SalesRep();
			if (!salesRepMap.containsKey(salesrepId)) {
				salesrepId = salesRep.getId();
				account = salesRep;
			} else if (salesRep.getId().compareTo(salesrepId) != 0) {
				// valid 
				account = this.webJaguar.getSalesRepById(salesrepId);
				salesrepAccountNumber = account.getAccountNumber();
			}
			model.put("account", account);
		}
		
		
		List<Presentation> presentations = new ArrayList<Presentation>();
		presentationSearch.setSaleRepId(salesrepId);
		presentations.addAll(this.webJaguar.getPresentationList(presentationSearch));
		for(Presentation presentation : presentations) {
			presentation.setUrl(siteConfig.get("SITE_URL").getValue()+"presentation.jhtm?id="+Encoder.encode(presentation.getId().toString())+"&salesRep="+Encoder.encode(salesRep.getEmail())+"&editParam="+Encoder.encode("0"));
			List<SalesRep> salesReps = this.webJaguar.getSalesRepList(salesRep.getId());
			SalesRep lastUpdateBySalesRep = this.webJaguar.getSalesRepById(presentation.getLastUpdatedBy());
			if(lastUpdateBySalesRep != null) {
				presentation.setLastUpdatedByName(lastUpdateBySalesRep.getName());
			}
			boolean managerEdited = true;
			for(SalesRep subSalesReps : salesReps) {
				if(managerEdited != false && lastUpdateBySalesRep!= null && (subSalesReps.getId().equals(lastUpdateBySalesRep.getId()))) {
					managerEdited = false;
				}
			}
			if(salesRep.isEditPrice() && ! managerEdited) {
				presentation.setEditUrl(siteConfig.get("SITE_URL").getValue()+"presentation.jhtm?id="+Encoder.encode(presentation.getId().toString())+"&salesRep="+Encoder.encode(salesRep.getEmail())+"&editParam="+Encoder.encode("1"));
			}
		}
		PagedListHolder presentationList = new PagedListHolder(presentations);

		presentationList.setPageSize(20);
		String page = request.getParameter("page");
		if (page != null) {
			presentationList.setPage(Integer.parseInt(page)-1);
		}
		model.put("currentPage", "presentation");
		model.put("presentationName", presentationName);
		model.put("salesRep", salesRep);
		model.put("presentationList", presentationList);
		
		Layout layout = (Layout) request.getAttribute("layout");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		model.put("salesrepLayout", this.webJaguar.getSystemLayout("salesRep", request.getHeader("host"), request));
		
		return new ModelAndView("frontend/salesRep/presentation/presentation", "model", model);  
	}
}