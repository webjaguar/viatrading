package com.webjaguar.web.frontend.salesRep;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Presentation;
import com.webjaguar.model.PresentationProduct;
import com.webjaguar.model.PresentationTemplateSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.domain.Encoder;

public class PresentationLinkController implements Controller {

	private WebJaguarFacade webJaguar;
	private Map<String, Configuration> siteConfig;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		Presentation presentation = new Presentation();
		Integer idParam = Integer.parseInt(Encoder.decode(ServletRequestUtils.getStringParameter(request, "id")));

		if (idParam != null && this.webJaguar.getPresentation(idParam) != null) {
			presentation.setId(idParam);
			presentation = this.webJaguar.getPresentation(presentation.getId());
			SalesRep salerep = this.webJaguar.getSalesRep(request);
			if (salerep != null) {

				List<SalesRep> salesReps = this.webJaguar.getSalesRepList(salerep.getId());
				SalesRep lastUpdateBySalesRep = this.webJaguar.getSalesRepById(presentation.getLastUpdatedBy());
				presentation.setManagerUpdated(true);
				for (SalesRep subSalesReps : salesReps) {
					if (presentation.isManagerUpdated() && lastUpdateBySalesRep != null && (subSalesReps.getId().equals(lastUpdateBySalesRep.getId()))) {
						presentation.setManagerUpdated(false);
					}
				}

				// sub accounts
				if (siteConfig.get("SALESREP_PARENT").getValue().equals("true")) {
					Map<Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
					List<SalesRep> salesRepsList = this.webJaguar.getSalesRepList(salerep.getId());
					for (SalesRep thisSalesRep : salesRepsList) {
						salesRepMap.put(thisSalesRep.getId(), thisSalesRep);
					}
					// associate reps to parents
					for (SalesRep thisSalesRep : salesRepsList) {
						if (salesRepMap.containsKey(thisSalesRep.getParent())) {
							salesRepMap.get(thisSalesRep.getParent()).getSubAccounts().add(thisSalesRep);
						}
					}
					salerep.setSubAccounts(salesRepMap.get(salerep.getId()).getSubAccounts());
				}
				if (salerep.getParent() == null) {
					presentation.setManager(true);
				} else {
					for (SalesRep sa : salerep.getSubAccounts()) {
						if ((lastUpdateBySalesRep != null && (sa.getId().equals(lastUpdateBySalesRep.getId()) || lastUpdateBySalesRep.getId().equals(salerep.getId())) && !presentation.getSaleRepId()
								.equals(salerep.getId()))) {
							presentation.setManager(true);
						}
					}
				}

				Integer editParam = Integer.parseInt(Encoder.decode(ServletRequestUtils.getStringParameter(request, "editParam")));
				if (editParam != null & editParam == 1 & salerep.isEditPrice()) {
					presentation.setEditPrice(true);
					presentation.setEditUrl(siteConfig.get("SITE_URL").getValue() + "presentation.jhtm?id=" + Encoder.encode(presentation.getId().toString()) + "&salesRep="
							+ Encoder.encode(salerep.getEmail()) + "&editParam=" + Encoder.encode("1"));
				}
			}
			presentation.setUrl(siteConfig.get("SITE_URL").getValue() + "presentation.jhtm?id=" + Encoder.encode(presentation.getId().toString()) + "&salesRep=" + Encoder.encode(salerep.getEmail())
					+ "&editParam=" + Encoder.encode("0"));
			Integer page = null;
			Integer size = null;
			if (ServletRequestUtils.getStringParameter(request, "view", null) != null) {
				List<Integer> productIds = new ArrayList<Integer>();
				Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
				Integer noOfProduct = presentation.getMaxNoOfProduct();
				Map<Integer, Double> presentationProductPrice = new HashMap<Integer, Double>();
				boolean admin = false;
				for (PresentationProduct presentationProduct : this.webJaguar.getPresentationProductList(presentation)) {
					if (presentationProduct.getSku() != null) {
						productIds.add(this.webJaguar.getProductIdBySku(presentationProduct.getSku()));
						presentationProductPrice.put(this.webJaguar.getProductIdBySku(presentationProduct.getSku()), presentationProduct.getPrice());
					}
				}
				if (noOfProduct > 6) {
					noOfProduct = 6;
				}
				if (productIds.size() > 0) {
					if (this.webJaguar.createPdfPresentation(request, response, productIds, noOfProduct, presentation, salerep, gSiteConfig, presentationProductPrice, admin)) {
						// pdf generated
						return null;
					}
				}
			}
			try {
				page = Integer.parseInt(ServletRequestUtils.getStringParameter(request, "page"));
				size = Integer.parseInt(ServletRequestUtils.getStringParameter(request, "size"));
			} catch (Exception e) {
			}
			if (page == null) {
				presentation.setPage(1);
			} else {
				presentation.setPage(page);
			}
			if (size == null) {
				presentation.setPageSize(presentation.getMaxNoOfProduct());
			} else {
				presentation.setPageSize(size);
			}

			List<Product> presentationProductList = new ArrayList<Product>();
			presentation.setOffSet((presentation.getPage() - 1) * presentation.getPageSize());
			for (PresentationProduct presentationProduct : this.webJaguar.getPresentationProductList(presentation)) {
				if (!presentationProduct.getSku().isEmpty()) {
					int id = this.webJaguar.getProductIdBySku(presentationProduct.getSku());
					Product p = this.webJaguar.getProductById(id, request);
					p.setMsrp(presentationProduct.getPrice());
					presentationProductList.add(p);
				}
			}
			Integer count = (presentation.getNoOfProduct() / presentation.getPageSize());
			count = (presentation.getNoOfProduct() % presentation.getPageSize()) == 0 ? count : ++count;
			presentation.setPageCount(count);
			if (presentation.getPageCount() == presentation.getPage()) {
				if (presentationProductList.size() < presentation.getPageSize()) {
					PresentationTemplateSearch templateSearch = new PresentationTemplateSearch();
					templateSearch.setNoOfProduct(presentationProductList.size());
					presentation.setDesign(this.webJaguar.getPresentationTemplateList(templateSearch).get(0).getDesign());
				}
			}
			int produceCount = 0;
			String design = presentation.getDesign();
			String footerHtmlCode = presentation.getFooterHtmlCode();
			String presentationTitle = "#presentationTitle#";
			design = design.replaceAll(presentationTitle, presentation.getTitle());
			footerHtmlCode = footerHtmlCode.replaceAll(presentationTitle, presentation.getTitle());
			String created = "#created#";
			footerHtmlCode = footerHtmlCode.replaceAll(created, presentation.getCreated() + "");
			String salesRepName = "#salesRepName#";
			SalesRep salesRep = this.webJaguar.getSalesRepById(presentation.getSaleRepId());
			if (salesRep != null) {
				footerHtmlCode = footerHtmlCode.replaceAll(salesRepName, salesRep.getName());
			}
			String note = "#note#";
			footerHtmlCode = footerHtmlCode.replaceAll(note, presentation.getNote());
			boolean setProductQuoteColumn = false;
			for (Product p : presentationProductList) {
				// ProductQuoteColumn
				if (siteConfig.get("PRODUCT_QUOTE").getValue().equals("true")) {
					if (p.isQuote()) {
						setProductQuoteColumn = true;
					}
				}
				if (presentation.isEditPrice()) {
					String sku1 = ServletRequestUtils.getStringParameter(request, "sku" + produceCount, "");
					boolean remove = ServletRequestUtils.getBooleanParameter(request, "remove" + produceCount, false);
					double price = 0;
					try {
						price = Double.parseDouble(ServletRequestUtils.getStringParameter(request, "price" + produceCount, ""));
					} catch (Exception e) {
						e.printStackTrace();
					}
					if (price != 0 || remove) {
						this.webJaguar.updatePresentationPrice(sku1, price, idParam, salerep.getId(), remove);
					}
				}
				++produceCount;
				for (ProductImage image : p.getImages()) {
					String imageurl = "#image" + produceCount + "#";
					design = design.replaceAll(imageurl, siteConfig.get("SITE_URL").getValue() + "assets/Image/Product/detailsbig/" + image.getImageUrl());
					footerHtmlCode = footerHtmlCode.replaceAll(imageurl, siteConfig.get("SITE_URL").getValue() + "assets/Image/Product/detailsbig/" + image.getImageUrl());
					break;
				}
				String sku = "#sku" + produceCount + "#";
				design = design.replaceAll(sku, p.getSku());
				footerHtmlCode = footerHtmlCode.replaceAll(sku, p.getSku());
				String name = "#productName" + produceCount + "#";
				design = design.replaceAll(name, p.getName());
				footerHtmlCode = footerHtmlCode.replaceAll(name, p.getName());
				String shortDesc = "#shortDesc" + produceCount + "#";
				design = design.replaceAll(shortDesc, p.getShortDesc());
				footerHtmlCode = footerHtmlCode.replaceAll(shortDesc, p.getShortDesc());
				presentation.setDesign(design);
				presentation.setFooterHtmlCode(footerHtmlCode);
			}

			List<ProductField> productField = null;
			try {
				productField = this.webJaguar.getProductFields(request, presentationProductList, true);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// QUOTE
			if (siteConfig.get("PRODUCT_QUOTE").getValue().equals("true")) {
				model.put("showProductQuoteColumn", setProductQuoteColumn);
			}
			model.put("productFields", productField);
			presentation.setProduct(presentationProductList);
			model.put("presentation", presentation);
			if (ServletRequestUtils.getStringParameter(request, "__update", null) != null) {
				return new ModelAndView(new RedirectView("presentation.jhtm" + presentation.getUrl()));
			} else {
				this.webJaguar.updateNoOfViewToPresentation(presentation.getId());
				return new ModelAndView("frontend/salesRep/presentation/template1", "model", model);
			}
		} else {
			return new ModelAndView(new RedirectView(siteConfig.get("PRESENTATION_NOT_FOUND_LINK").getValue()));
		}
	}
}