package com.webjaguar.web.frontend.salesRep;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Presentation;
import com.webjaguar.model.PresentationTemplateSearch;
import com.webjaguar.model.SalesRep;
import com.webjaguar.web.form.PresentationForm;

public class AddPresentationController extends SimpleFormController {
	private WebJaguarFacade webJaguar; 
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	public AddPresentationController() {
		setSessionForm(true);
		setCommandName("presentationForm");
		setCommandClass(PresentationForm.class);
		setFormView("frontend/salesRep/presentation/addPresentation");
		setSuccessView("presentationList.jhtm");		
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		PresentationForm presentationForm = (PresentationForm) command;
		Map<String, Object> model1 = new HashMap<String, Object>();
		String message = "";
		SalesRep salesRep = this.webJaguar.getSalesRep(request);
		if(request.getParameter("add")!=null) {
			presentationForm.getPresentation().setSaleRepId(salesRep.getId());
			this.webJaguar.insertPresentation(presentationForm.getPresentation());
			message = "Presentation "+presentationForm.getPresentation().getName() +" was added.";
			model1.put("message", message);
		}
		
		if(request.getParameter("update")!=null) {
			this.webJaguar.updatePresentation(presentationForm.getPresentation());
		}
		if(request.getParameter("_delete")!=null) {
			this.webJaguar.deletePresentation(presentationForm.getPresentation());
			message = "Presentation "+presentationForm.getPresentation().getName() +" was deleted.";
			model1.put("message", message);
		}
		if(request.getParameter("_cancel")!=null) {
			return new ModelAndView(new RedirectView("presentationList.jhtm"));
		} else {
			return new ModelAndView(new RedirectView(getSuccessView()),"model1",model1);
		}
	}
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_delete") != null || request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	} 
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception
	{
		PresentationForm presentationForm = (PresentationForm) command;
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		layout.setHideLeftBar( true );
		// hide right bar
		layout.setRightBarTopHtml( "" );
		layout.setRightBarBottomHtml( "" );

		
		myModel.put("salesrepLayout", this.webJaguar.getSystemLayout("salesRep", request.getHeader("host"), request));
		map.put( "model", myModel );
		return map;
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		PresentationForm presentationForm = (PresentationForm) command;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "presentation.name", "form.required", "required");
		if (presentationForm.getPresentation().getTemplateId() == 0) {
			errors.rejectValue("presentation.templateId", "form.required", "required");
		}
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		PresentationForm presentationForm = new PresentationForm();
		Presentation presentation = new Presentation();
		if (request.getParameter("id") != null) {
			Integer presentationId = new Integer(request.getParameter("id"));
			presentation = this.webJaguar.getPresentation(presentationId);
			presentationForm.setNewPresentation(false);
		} else {
			presentationForm.setNewPresentation(true);
			presentation.setCreated(new Date());
		}
		
		PresentationTemplateSearch templateSearch = new PresentationTemplateSearch();
		presentationForm.setTemplateList(this.webJaguar.getPresentationTemplateList(templateSearch));
		presentationForm.setPresentation(presentation);
		return presentationForm;
	}
}
