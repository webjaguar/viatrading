/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.08.2009
 */

package com.webjaguar.web.frontend.salesRep;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.UserSession;

public class LoginAsCustomerController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws Exception {
    	
    	SalesRep salesRep = this.webJaguar.getSalesRep(request);

    	String token = ServletRequestUtils.getStringParameter(request, "cid", null);
    	Customer customer = this.webJaguar.getCustomerByToken(token);
    	if (customer != null && customer.getSalesRepId().compareTo(salesRep.getId()) == 0) {
    		//  set user session
			UserSession userSession = new UserSession();
			userSession.setUsername(customer.getUsername());
			userSession.setFirstName(customer.getAddress().getFirstName());
			userSession.setLastName(customer.getAddress().getLastName());
			userSession.setUserid(customer.getId());
			this.webJaguar.setUserSession(request, userSession);
    	}
        return new ModelAndView(new RedirectView(request.getContextPath() + "/account.jhtm"));    	
    }
}
