/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.07.2009
 */

package com.webjaguar.web.frontend.salesRep;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

public class LogoutController implements Controller {

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		// clear cookie
		Cookie salesRepCookie = new Cookie("salesrep", "");
		salesRepCookie.setMaxAge(0);	
		response.addCookie(salesRepCookie);
		
		// invalidate session
		request.getSession().invalidate();
		
		return new ModelAndView(new RedirectView("salesRepAccount.jhtm"));
	}

}
