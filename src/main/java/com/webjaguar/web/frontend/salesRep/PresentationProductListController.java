/* Copyright 2012 Advanced E-Media Solutions
 *
 */
package com.webjaguar.web.frontend.salesRep;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Presentation;
import com.webjaguar.model.PresentationProduct;
import com.webjaguar.model.SalesRep;

public class PresentationProductListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	Map<String, Object> model = new HashMap<String, Object>();
    	String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();
		model.put( "url", url + "?" + query );
        // continue shopping
		request.getSession().setAttribute( "continueShoppingUrl", model.get( "url" ) );
    	
    	SalesRep salesRep = this.webJaguar.getSalesRep(request);
    	
		// get the list of product in presentation list
    	Presentation presentation = new Presentation();
    	presentation.setSaleRepId(salesRep.getId());
		List<String> addPresentationError = new ArrayList<String>();
    	if(request.getParameter("sort") != null && ! request.getParameter("sort").isEmpty()) {
        	presentation.setSort(request.getParameter("sort"));
        	model.put("sort", request.getParameter("sort"));
    	}
		if (request.getParameter("_removeFromList") != null) {
			String sku[] = request.getParameterValues("__selected_sku");
			if(sku != null && sku.length > 0) {
				this.webJaguar.removeProductFromPresentationList(sku, salesRep.getId());
				addPresentationError.add("Products removed from List");
			} else {
				addPresentationError.add("Please Select The Product To Be Removed");
			}
			
		}
    	presentation.setSaleRepId(salesRep.getId());
    	presentation.setId(null);
    	java.util.List<PresentationProduct> presentationProductList = this.webJaguar.getPresentationProductList(presentation);
    	for(PresentationProduct presentationProduct : presentationProductList) {
    		int id = this.webJaguar.getProductIdBySku(presentationProduct.getSku());
    		presentationProduct.setProduct(this.webJaguar.getProductById(id, request));
    	}
    	PagedListHolder productList = new PagedListHolder(presentationProductList);
    	productList.setPageSize(10);
		String page = request.getParameter("page");
		if (page != null) {
			productList.setPage(Integer.parseInt(page)-1);
		}
		model.put("products", productList);
		
		//get the list of presentation
		Presentation presentationSearch = new Presentation();
		presentationSearch.setSaleRepId(salesRep.getId());
		List<Presentation> presentationList = this.webJaguar.getPresentationList(presentationSearch);
		model.put("presentationList", presentationList);
		// check if add to presentation was selected
		if (request.getParameter("_addToPresentation") != null) {
			Integer presentationId = Integer.parseInt( request.getParameter("_presentationId") );
			String sku[] = request.getParameterValues("__selected_sku");
			Presentation presentationExisting = this.webJaguar.getPresentation(presentationId);
			if(presentationId != 0) {
				if ( sku == null || sku.length == 0) {
					addPresentationError.add("No product selected");
				}
				else {
					if (presentationExisting != null && (presentationExisting.getMaxNoOfProduct() - presentationExisting.getNoOfProduct() >= sku.length)) {
						this.webJaguar.addProductToPresentation(sku,presentationId,salesRep.getId());
						addPresentationError.add("Products added to presentation ");
					}
					else {
						addPresentationError.add("No. of product selected exceeds the max allowed on presentation " + presentationExisting.getName());
					}
				}
			} else {
				addPresentationError.add("No presentation selected");
			}
		}
		model.put("addPresentationError", addPresentationError);
		Layout layout = (Layout) request.getAttribute("layout");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		model.put("salesrepLayout", this.webJaguar.getSystemLayout("salesRep", request.getHeader("host"), request));
		
		return new ModelAndView("frontend/salesRep/presentation/presentationProductList", "model", model);    	
    }
}