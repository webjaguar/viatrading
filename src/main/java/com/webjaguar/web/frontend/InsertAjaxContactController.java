/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.web.frontend;


import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.crm.CrmContact;

public class InsertAjaxContactController implements Controller
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		StringBuffer message = new StringBuffer();
		if(ServletRequestUtils.getStringParameter(request, "sku") != null) {
			message.append("Sku : "+ServletRequestUtils.getStringParameter(request, "sku")+" \n");
		}
		if(ServletRequestUtils.getStringParameter(request, "name") != null) {
			message.append("Product Name : "+ServletRequestUtils.getStringParameter(request, "name")+" \n");
		}
		if(ServletRequestUtils.getStringParameter(request, "priceGridId") != null) {
			message.append("PriceGrid Id : "+ServletRequestUtils.getStringParameter(request, "priceGridId")+" \n");
		}
		if(ServletRequestUtils.getStringParameter(request, "priceGridDependecyValue") != null) {
			message.append("PriceGrid Value : "+ServletRequestUtils.getStringParameter(request, "priceGridDependecyValue")+" \n");
		}
		if(ServletRequestUtils.getStringParameter(request, "company") != null) {
			message.append("Company : "+ServletRequestUtils.getStringParameter(request, "company")+" \n");
		}
		if(ServletRequestUtils.getStringParameter(request, "note") != null) {
			message.append("Note from Customer : "+ServletRequestUtils.getStringParameter(request, "note")+" \n");
		}
		
		CrmContact crmContact = new CrmContact();
		crmContact.setAccountId(1);
		crmContact.setAccountName("Leads");
		if(ServletRequestUtils.getStringParameter(request, "leadSource") != null) {
			crmContact.setLeadSource(ServletRequestUtils.getStringParameter(request, "leadSource"));
		}
		crmContact.setFirstName(ServletRequestUtils.getStringParameter(request, "firstName"));
		crmContact.setLastName(ServletRequestUtils.getStringParameter(request, "lastName"));
		crmContact.setStreet(ServletRequestUtils.getStringParameter(request, "address"));
		crmContact.setCity(ServletRequestUtils.getStringParameter(request, "city"));
		crmContact.setStateProvince(ServletRequestUtils.getStringParameter(request, "state"));
		crmContact.setZip(ServletRequestUtils.getStringParameter(request, "zipCode"));
		crmContact.setCountry(ServletRequestUtils.getStringParameter(request, "country"));
		crmContact.setEmail1(ServletRequestUtils.getStringParameter(request, "email"));
		crmContact.setPhone1(ServletRequestUtils.getStringParameter(request, "phone"));
		crmContact.setDescription(message.toString());
		
		if(crmContact.getFirstName() == null || crmContact.getFirstName().isEmpty() || crmContact.getLastName() == null || crmContact.getLastName().isEmpty() || ((crmContact.getEmail1() == null || crmContact.getEmail1().isEmpty()) && (crmContact.getPhone1() == null || crmContact.getPhone1().isEmpty()))) {
			myModel.put("success", false);
		} else {
			this.webJaguar.insertCrmContact(crmContact);
			myModel.put("success", true);
		}
		if(siteConfig.get("RFQ_EMAIL") != null && !siteConfig.get("RFQ_EMAIL").getValue().equals("")) {
			this.webJaguar.newLeadEmail(crmContact, siteConfig.get("RFQ_EMAIL").getValue(), null, null, request, null, null, mailSender);
		} else {
			this.webJaguar.newLeadEmail(crmContact, ((Configuration) siteConfig.get("CONTACT_EMAIL")).getValue(), null, null, request, null, null, mailSender);
	    }
		
		return new ModelAndView("frontend/ajax/insertContact", "model", myModel);
	}
}
