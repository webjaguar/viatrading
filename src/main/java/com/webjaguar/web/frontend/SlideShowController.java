/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.23.2009
 */

package com.webjaguar.web.frontend;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.web.domain.Constants;

public class SlideShowController extends MultiActionController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	/*
	 * TYPE: Image SlideShow
	 * returns list of Images of a product
	 * INPUT: id (product id)
	 * OUTPUT: list of images for a product
	 */
    public ModelAndView slideShow1(HttpServletRequest request, HttpServletResponse response)
    			throws Exception {				    	
    	
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		Map<String, Object> model = new HashMap<String, Object>();
			
		Product product = this.webJaguar.getProductById( ServletRequestUtils.getIntParameter( request, "id", 0 ), request );
		if (product != null && product.getImageLayout() != null && siteConfig.get( "PRODUCT_IMAGE_LAYOUTS" ).getValue().contains( "s2" )) {
			model.put("product", product);			
		}
		setModel(model, request);
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/slideShow/slideShow3", model);
		}
		return new ModelAndView("frontend/slideShow/slideShow1", model);    	
    }
    
    /*
     * TYPE: Image SlideShow
	 * returns list of Category Image of subs of a category id
	 * INPUT: cid (category id)
	 * OUTPUT: list of images for subs of a category
	 */
    public ModelAndView slideShow2(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	Map<String, Object> model = new HashMap<String, Object>();
		
		// return list of sub-categories
		List<Category> subCategories = this.webJaguar.getCategoryLinks( ServletRequestUtils.getIntParameter( request, "cid", -1 ), request );
		// base image file
		File baseImageFile = new File(getServletContext().getRealPath("/assets/Image"));
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				baseImageFile = new File( (String) prop.get( "site.root" ), "/assets/Image" );
			}
		}
		catch ( Exception e ) {}
		File baseCategoryFile = new File(baseImageFile, "Category");
		// check if image exists
		for ( Iterator<Category> i = subCategories.iterator(); i.hasNext(); ) {
			Category cat = i.next();
			File image = null;
			if(cat.getImageUrl() != null) {
				if(cat.isAbsolute()) {
					//if image url is aboslute, do not remove it from the list
					continue;
				} else {
					image = new File(baseCategoryFile,  cat.getImageUrl());
				}
			} else {
				image = new File(baseCategoryFile, "cat_" + cat.getId() + ".gif");
			}
			if ( !image.exists() ) {
				i.remove();
			}
		}
		if (siteConfig.get( "PRODUCT_IMAGE_LAYOUTS" ).getValue().contains( "s2" )) {
			model.put("subCategories", subCategories);	
		}
		setModel(model, request);
		
		return new ModelAndView("frontend/slideShow/slideShow2", model);        	
    }
 
    /*
     * TYPE: Image SlideShow
	 * returns list of Product Image belong to a category
	 * INPUT: cid (category id)
	 * OUTPUT: list of Product image belong to a category
	 * MAX: 10 image
	 * 
	 * OR
	 * 
	 * returns list of Product Image belong to a category or products having keywords on the specified field
	 * INPUT: keywords and field
	 * OUTPUT: list of Product image of the products having same keywords on the spefied field
	 * 
	 */
    public ModelAndView slideShow3(HttpServletRequest request, HttpServletResponse response) throws ServletException{
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		Map<String, Object> model = new HashMap<String, Object>();
		
		// return list of products
    	List<Product> featuredProducts = new ArrayList<Product>();
    	List<Product> products = new ArrayList<Product>();
		
    	if (ServletRequestUtils.getIntParameter( request, "cid", -1 ) != -1) {
    		Category thisCategory = this.webJaguar.getCategoryById( ServletRequestUtils.getIntParameter( request, "cid", -1 ), Constants.FULL_PROTECTED_ACCESS );
    		if (thisCategory != null) {
    			products = this.webJaguar.getProductsByCategoryId( thisCategory, null, request, new ProductSearch(), false );
    		}
    	} else if (ServletRequestUtils.getIntParameter( request, "field", -1 ) != -1 && ServletRequestUtils.getStringParameter(request, "keywords") != null) {
			// return list of products based on keywrod and field search and display images of the products
			ProductSearch search = new ProductSearch();
			search.setSort("");
			// search keywords
			search.setKeywords(ServletRequestUtils.getStringParameter(request, "keywords"));
			// search field
			search.setLimit(33);
			search.setSearchFields(new ArrayList<String>());
			search.getSearchFields().add("field_"+ServletRequestUtils.getIntParameter( request, "field", -1 ));
			search.setDelimiter(siteConfig.get("SEARCH_KEYWORDS_DELIMITER").getValue());
			search.setConjunction(siteConfig.get("SEARCH_DEFAULT_KEYWORDS_CONJUNCTION").getValue());
	    	search.setgPRODUCT_FIELDS((Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
			products = this.webJaguar.searchProducts(search, request, null);
		}
    	
    	for (Product thisProduct:products) {
			if (thisProduct.getThumbnail() != null) {
				featuredProducts.add( thisProduct );
			}
			if (featuredProducts.size() == 10) {
				break;
			}
		}
    	
    	if (siteConfig.get( "PRODUCT_IMAGE_LAYOUTS" ).getValue().contains( "s2" )) {
			model.put( "featuredProducts", featuredProducts );
		}		
		setModel(model, request);
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			model.put( "featuredProducts", featuredProducts );
			return new ModelAndView("frontend/layout/template6/slideShow/slideShow3", model);
		}
		return new ModelAndView("frontend/slideShow/slideShow3", model);        	
    }
    
    public ModelAndView slideShow4(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	Map<String, Object> model = new HashMap<String, Object>();
		
		// return list of sub-categories
		List<Category> subCategories = this.webJaguar.getCategoryLinks( ServletRequestUtils.getIntParameter( request, "cid", -1 ), request );
		// base image file
		File baseImageFile = new File(getServletContext().getRealPath("/assets/Image"));
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				baseImageFile = new File( (String) prop.get( "site.root" ), "/assets/Image" );
			}
		}
		catch ( Exception e ) {}
		File baseCategoryFile = new File(baseImageFile, "Category");
		// check if image exists
		for ( Iterator<Category> i = subCategories.iterator(); i.hasNext(); ) {
			Category cat = i.next();
			File image = null;
			if(cat.getImageUrl() != null) {
				if(cat.isAbsolute()) {
					//if image url is aboslute, do not remove it from the list
					continue;
				} else {
					image = new File(baseCategoryFile, "/assets/Image/" + cat.getImageUrl());
				}
			} else {
				image = new File(baseCategoryFile, "cat_" + cat.getId() + ".gif");
			}
			if ( !image.exists() ) {
				i.remove();
			}
		}
		if (siteConfig.get( "PRODUCT_IMAGE_LAYOUTS" ).getValue().contains( "s2" )) {
			model.put("subCategories", subCategories);	
		}
		setModel(model, request);
		
		return new ModelAndView("frontend/slideShow/slideShow4", model);        	
    }
    
    /*
     * TYPE: HTML SlideShow
	 * returns list of Product Image
	 * INPUT: id (product id)
	 * OUTPUT: list of Product image
	 * MAX:
	 */
    public ModelAndView slideShow11(HttpServletRequest request, HttpServletResponse response) throws Exception {				    	

		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		Map<String, Object> model = new HashMap<String, Object>();
		
		Product product = this.webJaguar.getProductById( ServletRequestUtils.getIntParameter( request, "id", 0 ), request );
		if (product != null && siteConfig.get( "HTML_SCROLLER" ).getValue().equals( "true" )) {
		    model.put("product", product);			
		}
		setModel(model, request);
		
		return new ModelAndView("frontend/slideShow/slideShow11", model);    	
	}
    
    public ModelAndView slideShow12(HttpServletRequest request, HttpServletResponse response) {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	Map<String, Object> model = new HashMap<String, Object>();
		
		// return list of sub-categories
		List<Category> subCategories = this.webJaguar.getCategoryLinks( ServletRequestUtils.getIntParameter( request, "cid", -1 ), request );
		// base image file
		File baseImageFile = new File(getServletContext().getRealPath("/assets/Image"));
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				baseImageFile = new File( (String) prop.get( "site.root" ), "/assets/Image" );
			}
		}
		catch ( Exception e ) {}
		File baseCategoryFile = new File(baseImageFile, "Category");
		// check if image exists
		for ( Iterator<Category> i = subCategories.iterator(); i.hasNext(); ) {
			Category cat = i.next();
			File image = null;
			if(cat.getImageUrl() != null) {
				if(cat.isAbsolute()) {
					//if image url is aboslute, do not remove it from the list
					continue;
				} else {
					image = new File(baseCategoryFile, "/assets/Image/" + cat.getImageUrl());
				}
			} else {
				image = new File(baseCategoryFile, "cat_" + cat.getId() + ".gif");
			}
			if ( !image.exists() ) {
				i.remove();
			}
		}
		if (siteConfig.get( "HTML_SCROLLER" ).getValue().equals( "true" )) {
			model.put("subCategories", subCategories);	
		}
		setModel(model, request);
		
		return new ModelAndView("frontend/slideShow/slideShow12", model);        	
    }
    
    public ModelAndView slideShow13(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		Map<String, Object> model = new HashMap<String, Object>();
		
		List<Product> featuredProducts = new ArrayList<Product>();
		List<Product> products = new ArrayList<Product>();
		/* return list of products belongs to a category and display image of the products
		 * OR
		 * 
		 * returns list of Product Image belong to a category or products having keywords on the specified field
		 * INPUT: keywords and field
		 * OUTPUT: list of Product image of the products having same keywords on the spefied field
		 */
		if (ServletRequestUtils.getIntParameter( request, "cid", -1 ) != -1) {
			Category thisCategory = this.webJaguar.getCategoryById( ServletRequestUtils.getIntParameter( request, "cid", -1 ), Constants.FULL_PROTECTED_ACCESS );
			if (thisCategory != null) {
				products = this.webJaguar.getProductsByCategoryId( thisCategory, null, request, new ProductSearch(), false );
			}
		} else if (ServletRequestUtils.getIntParameter( request, "field", -1 ) != -1 && ServletRequestUtils.getStringParameter(request, "keywords") != null) {
			// return list of products based on keywrod and field search and display images of the products
			ProductSearch search = new ProductSearch();
			search.setSort("");
			// search keywords
			search.setKeywords(ServletRequestUtils.getStringParameter(request, "keywords"));
			// search field
			search.setLimit(33);
			search.setSearchFields(new ArrayList<String>());
			search.getSearchFields().add("field_"+ServletRequestUtils.getIntParameter( request, "field", -1 ));
			search.setDelimiter(siteConfig.get("SEARCH_KEYWORDS_DELIMITER").getValue());
			search.setConjunction(siteConfig.get("SEARCH_DEFAULT_KEYWORDS_CONJUNCTION").getValue());
	    	search.setgPRODUCT_FIELDS((Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
			products = this.webJaguar.searchProducts(search, request, null);
		} 
		
		for (Product thisProduct:products) {
			if (thisProduct.getThumbnail() != null) {
				featuredProducts.add( thisProduct );
			}
			if (featuredProducts.size() == 33) {
				break;
			}
		}
		if (siteConfig.get( "HTML_SCROLLER" ).getValue().equals( "true" )) {
			model.put( "numItem", 6 );
			//model.put( "maxItemNumber", (featuredProducts.size() + (3 - (featuredProducts.size() % 3)) ));
			//model.put( "page",(((featuredProducts.size() % 3) == 0) ? ((int) Math.ceil(featuredProducts.size() /3)) : ((int) Math.ceil(featuredProducts.size() /3)) + 1));
			model.put( "page", 2);
			model.put( "featuredProducts", featuredProducts );
		}		
		setModel(model, request);
		
		return new ModelAndView("frontend/slideShow/slideShow13", "model", model);        	
    }
    
    public void setModel(Map<String, Object> model, HttpServletRequest request) {
    	model.put("stylesheet", ServletRequestUtils.getStringParameter( request, "css", "slideshow" ));
		model.put("height", ServletRequestUtils.getStringParameter( request, "h", "300" ));
		model.put("width", ServletRequestUtils.getStringParameter( request, "w", "400" ));
		model.put("delay", ServletRequestUtils.getStringParameter( request, "d", "2000" ));
		model.put("duration", ServletRequestUtils.getStringParameter( request, "du", "750" ));
		model.put("transition", getTransition(ServletRequestUtils.getIntParameter( request, "t", -1 )));
		model.put("overlap", ServletRequestUtils.getBooleanParameter( request, "o", true ));
		model.put("resize", ServletRequestUtils.getStringParameter( request, "r", "400" ));
		model.put("captions", ServletRequestUtils.getBooleanParameter( request, "c", false ));
		model.put("thumb", ServletRequestUtils.getBooleanParameter( request, "thumb", false ));
		model.put("random", ServletRequestUtils.getBooleanParameter( request, "random", false ));
		model.put("controller", ServletRequestUtils.getBooleanParameter( request, "controller", false ));
    }
    
    public String getTransition(int t) {
    	String transition = null;
    	switch (t) {
    	case 1:
    		transition = "back:in:out";
    		break;
    	case 2:
    		transition = "bounce:out";
    		break;
    	case 3:
    		transition = "elastic:out";
    		break;
    	default: 
    		transition = null;
    		break;
    	}
    	return transition;
    }
}