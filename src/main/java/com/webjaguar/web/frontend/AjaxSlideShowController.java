/* Copyright 2005, 2011 Advanced E-Media Solutions
 * author: Jwalant Patel
 */

package com.webjaguar.web.frontend;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Brand;
import com.webjaguar.model.BudgetProduct;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.domain.Utilities;

public class AjaxSlideShowController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {	
		
		Map<String, Object> model = new HashMap<String, Object>();
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );

		String skuList = ServletRequestUtils.getStringParameter(request, "skuList");
		//System.out.println("-------" + skuList);
		
		String slideShow = getSlidShowCode(request, skuList);
		//System.out.println("#####################slideShow" + slideShow);

		model.put("slideShow", slideShow);
		//System.out.println("#####################");
		return new ModelAndView("frontend/common/sliderSkuView", "model", model);
	}
	
	private String getSlidShowCode(HttpServletRequest  request,  String skuList) throws UnsupportedEncodingException {
		StringBuffer sb = new StringBuffer();

		if(skuList==null || skuList.isEmpty())
			return null;
		
		String[] skuArray = skuList.split(",");
		List<Product> products = new LinkedList<Product>();
		
		for(String sku: skuArray){
			Product pro = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(sku), request);
			if(pro!=null){
				products.add(pro);
			}
		}
	
		if(products.size()<=0) {
			return sb.toString();
		}
		
		Iterator<Product> itr = products.iterator();
		while(itr.hasNext()) {
			Product product = itr.next();
			if (product!=null && product.getThumbnail() == null) {
				itr.remove();
			}
		}
		if(products.isEmpty()) {
			return sb.toString();
		}
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		ApplicationContext context = getApplicationContext();
		String currencySymbol = "$";
		
			
		sb.append("<div class=\"products_carousel_wrapper\">");
		sb.append("<div class=\"products_carousel owl-carousel carousel-theme owl-responsive-1200 owl-loaded\">");
		
		for(Product product : products) {
			//System.out.println(product.getId());
			if(product.getProductLayout().equalsIgnoreCase("047")){
				String productLink = null;		

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");		
					

				String saleTag = "";
				if(product.getSalesTag() != null){
					saleTag = product.getSalesTag().getTitle();
				}
				
				if(product.getField32()!=null && !product.getField32().equalsIgnoreCase("")){
					sb.append("<div class=\"product_bordered_box is_ribboned\">");
					sb.append("<div class=\"ribbon_wrapper\">");
				if(product.getField32().equalsIgnoreCase("New_Item")){
					sb.append("<div class=\"ribbon ribbon_6\">NEW ARRIVAL</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Out_of_Stock")){
					sb.append("<div class=\"ribbon ribbon_4\">OUT OF STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Back_in_Stock")){
					sb.append("<div class=\"ribbon ribbon_3\">BACK IN STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Load_Added")){
					sb.append("<div class=\"ribbon ribbon_5\">NEW LOAD ADDED</div>");
				}
				else{
					sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				}
				sb.append("</div>");
				}else{
					sb.append("<div class=\"product_bordered_box\">");
				}

				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}

				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
		
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
				
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price\">");
				sb.append(product.getField8());
				sb.append("</div>");
				sb.append("</div>");
				//System.out.println("1102------------------------------------------------------------------------------>>>>>>");
				sb.append("</div>");
				
				sb.append("<div class=\"total_price\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(currencySymbol);
				sb.append(valueFormat(product.getPrice1()));
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}
			
			if(product.getProductLayout().equalsIgnoreCase("048")){
				String productLink = null;
				
				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");
				
				if(product.getField32()!=null && !product.getField32().equalsIgnoreCase("")){
					sb.append("<div class=\"product_bordered_box is_ribboned\">");
					sb.append("<div class=\"ribbon_wrapper\">");
				if(product.getField32().equalsIgnoreCase("New_Item")){
					sb.append("<div class=\"ribbon ribbon_6\">NEW ARRIVAL</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Out_of_Stock")){
					sb.append("<div class=\"ribbon ribbon_4\">OUT OF STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Back_in_Stock")){
					sb.append("<div class=\"ribbon ribbon_3\">BACK IN STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Load_Added")){
					sb.append("<div class=\"ribbon ribbon_5\">NEW LOAD ADDED</div>");
				}
				else{
					sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				}
				sb.append("</div>");
				}else{
					sb.append("<div class=\"product_bordered_box\">");
				}
			
				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
				
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price\">");
				sb.append(product.getField8());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"total_price\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(currencySymbol);
				sb.append((product.getPriceRangeMinimum()!=null?valueFormat(product.getPriceRangeMinimum()):0.0));
				sb.append("-");	
				sb.append(currencySymbol);
				sb.append((product.getPriceRangeMaximum()!=null?valueFormat(product.getPriceRangeMaximum()):0.0));
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}

			if(product.getProductLayout().equalsIgnoreCase("049")){
				String productLink = null;
				//System.out.println("1184------------------------------------------------------------------------------>>>>>>");

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");
				
				if(product.getField32()!=null && !product.getField32().equalsIgnoreCase("")){
					sb.append("<div class=\"product_bordered_box is_ribboned\">");
					sb.append("<div class=\"ribbon_wrapper\">");
				if(product.getField32().equalsIgnoreCase("New_Item")){
					sb.append("<div class=\"ribbon ribbon_6\">NEW ARRIVAL</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Out_of_Stock")){
					sb.append("<div class=\"ribbon ribbon_4\">OUT OF STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Back_in_Stock")){
					sb.append("<div class=\"ribbon ribbon_3\">BACK IN STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Load_Added")){
					sb.append("<div class=\"ribbon ribbon_5\">NEW LOAD ADDED</div>");
				}
				else{
					sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				}
				sb.append("</div>");
				}else{
					sb.append("<div class=\"product_bordered_box\">");
				}
				
				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");			
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");	
				
				sb.append("<div class=\"product_manifest_status\">");
				sb.append(product.getField14()!=null?product.getField14().replace(",", ""):"");
				sb.append("</div>");
				sb.append("<div class=\"price_percentage\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getField11());
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}

			if(product.getProductLayout().equalsIgnoreCase("050")){
				String productLink = null;
				//System.out.println("1239------------------------------------------------------------------------------>>>>>>");

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");		
				
				String saleTag = "";
				if(product.getSalesTag() != null){
					saleTag = product.getSalesTag().getTitle();
				}
				
				sb.append("<div class=\"product_bordered_box is_ribboned\">");
				sb.append("<div class=\"ribbon_wrapper\">");
				sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				sb.append("</div>");

				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}

				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
								
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
		
				Double discount = 0.0;
				if(product.getSalesTag() != null && product.getSalesTag().getDiscount() != 0.0){
					if(product.getSalesTag().isPercent()) {
						discount = product.getPrice1() * (100 - product.getSalesTag().getDiscount()) / 100;
					} else {
						discount = product.getPrice1() - product.getSalesTag().getDiscount();
					}
				}else{
					discount = product.getPrice1();
				}
				
				discount = Utilities.roundFactory(discount,2,BigDecimal.ROUND_HALF_UP);
	
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price discount\">");
				sb.append("<span class=\"new_price\">");sb.append(product.getField8());sb.append("</span>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"total_price discount\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append("<span class=\"strikethrough\">");
				sb.append("<span class=\"old_price\">");				
				sb.append(currencySymbol);
				sb.append(valueFormat(product.getPrice1()));
				sb.append("</span>");
				sb.append("</span>");
				sb.append("<span class=\"new_price\">");
				sb.append(currencySymbol);
				sb.append(valueFormat(discount));
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</span>");
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}

			if(product.getProductLayout().equalsIgnoreCase("051")){
				String productLink = null;
				//System.out.println("1336------------------------------------------------------------------------------>>>>>>");

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");				

				sb.append("<div class=\"product_bordered_box is_ribboned\">");
				sb.append("<div class=\"ribbon_wrapper\">");
				sb.append("<div class=\"ribbon ribbon_2\">ONE TIME DEAL</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}

				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");	
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
			
				sb.append("<div class=\"clearfix\">");
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("<div class=\"pull-right\">");
				sb.append("	<div class=\"product_manifest_status\">");
				sb.append(product.getField14()!=null?product.getField14().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price\">");
				sb.append(product.getField8());
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("<div class=\"total_price\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(currencySymbol);
				sb.append(valueFormat(product.getPrice1()));
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}		
			
			
			if(!product.getProductLayout().equalsIgnoreCase("047") && !product.getProductLayout().equalsIgnoreCase("048") && !product.getProductLayout().equalsIgnoreCase("049") && !product.getProductLayout().equalsIgnoreCase("050") && !product.getProductLayout().equalsIgnoreCase("051")){
				String productLink = null;
				//System.out.println("1417------------------------------------------------------------------------------>>>>>>");

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}
				//System.out.println("---------------------------------product------------------------------------->" + product.getId());

				sb.append("<div class=\"product_wrapper\">");
				
				if(product.getField32()!=null && !product.getField32().equalsIgnoreCase("")){
					sb.append("<div class=\"product_bordered_box is_ribboned\">");
					sb.append("<div class=\"ribbon_wrapper\">");
				if(product.getField32().equalsIgnoreCase("New_Item")){
					sb.append("<div class=\"ribbon ribbon_6\">NEW ARRIVAL</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Out_of_Stock")){
					sb.append("<div class=\"ribbon ribbon_4\">OUT OF STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Back_in_Stock")){
					sb.append("<div class=\"ribbon ribbon_3\">BACK IN STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Load_Added")){
					sb.append("<div class=\"ribbon ribbon_5\">NEW LOAD ADDED</div>");
				}
				else{
					sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				}
				sb.append("</div>");
				}else{
					sb.append("<div class=\"product_bordered_box\">");
				}
				
				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}
				
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
				
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price\">");
				sb.append(product.getField8());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"total_price\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(currencySymbol);
				sb.append(valueFormat(product.getPrice1()));
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");		
			}
			
		}
		sb.append("</div>");
		sb.append("</div>");
		
		sb.append("</div>");
		return sb.toString();
	}
	
	private String valueFormat(Double val){
		DecimalFormat numFormat;
		String number;
		//System.out.println("-------------val----------" + val);
		if(val!=null){
			numFormat = new DecimalFormat("#,###.00");
			number = numFormat.format(val);
			//System.out.println("3. DecimalFormat with ,: " + number);
			return number;
		}else{
			return "";
		}
	}
	
}