/*
 * Copyright 2005, 2011 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.web.frontend;


import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;

public class AjaxAddToCompareController implements Controller
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		siteConfig = (Map) request.getAttribute( "siteConfig" );
		int maxComparisons = new Integer( ((Configuration) siteConfig.get( "COMPARISON_MAX" )).getValue() );
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		if(ServletRequestUtils.getStringParameter(request, "option") != null) {
			
			Map<String, Map<Integer, Integer>> valueMap = new HashMap<String, Map<Integer,Integer>>();
			String cusValue = null;
			for(String option : ServletRequestUtils.getStringParameters(request, "option")) {
				
				String[] data = option.split("-");
				
				String optionCode = null;
				Integer optionIndex = null;
				Integer valueIndex = null;
				
				try {
					optionCode = data[0];
					if(!data[1].contains("cus_with_index_")) {
						optionIndex = Integer.parseInt( data[1].split("_")[3] );
						valueIndex = Integer.parseInt( data[2] );
					} else {
						optionIndex = Integer.parseInt( data[1].split("_")[6] );
						cusValue = data[2].split("_")[1];
					}
					if(valueMap.get(optionCode) != null) {
						valueMap.get(optionCode).put(optionIndex, valueIndex);
					} else {
						Map<Integer, Integer> tempMap = new HashMap<Integer, Integer>();
						tempMap.put(optionIndex, valueIndex);
						valueMap.put(optionCode, tempMap);
					}
				} catch(Exception e) { e.printStackTrace(); }
			}
			
			if(ServletRequestUtils.getIntParameter(request, "productId") != null) {
				Product assignedProduct = this.webJaguar.getProductById(ServletRequestUtils.getIntParameter(request, "productId"), request);
				myModel.put("assignedProduct", assignedProduct);
	
				siteConfig = (Map) request.getAttribute( "siteConfig" );
				List<CartItem> productComparisonList = (List) request.getSession().getAttribute( "cartItemsComparisonList" );
				if (productComparisonList == null) {
					productComparisonList = new ArrayList<CartItem>();
					request.getSession().setAttribute( "cartItemsComparisonList", productComparisonList );
				}
				
				if(maxComparisons > productComparisonList.size()) {
					CartItem item = new CartItem();
					item.setProduct(assignedProduct);
					List<ProductAttribute> attributeList = new ArrayList<ProductAttribute>();
					ProductAttribute attribute;
					for(String key : valueMap.keySet()) {
						Integer optionIndex = -1;
						for(Integer index : valueMap.get(key).keySet()) {
							optionIndex = index;
						}
						
						if(valueMap.get(key).get(optionIndex) != null) {
							attribute = new ProductAttribute(key, optionIndex, valueMap.get(key).get(optionIndex), null);
						} else {
							attribute = new ProductAttribute(key, optionIndex, cusValue, -1);
						}
						attributeList.add(attribute);
					}
					if ( ServletRequestUtils.getStringParameter(request, "optionNVPair") != null ) {
						String optionNVPairs[] = ServletRequestUtils.getStringParameters(request, "optionNVPair");
						for ( String optionNV : optionNVPairs)
						{
							try {
								attribute = new ProductAttribute( optionNV.split("_value_")[0], optionNV.split("_value_")[1], true );
								attributeList.add(attribute);
							}catch(Exception e){ e.printStackTrace(); }
						}
						
					}
					
					item.setProductAttributes(attributeList);
					if(!productComparisonList.contains(item.toString())) {
						productComparisonList.add(item);
					}
				}
			}  
		} else {
			List<Integer> comparisonList = (List) request.getSession().getAttribute( "comparisonList" );
			if (comparisonList == null) {
				comparisonList = new ArrayList<Integer>();
				request.getSession().setAttribute( "comparisonList", comparisonList );
			}
			if(maxComparisons > comparisonList.size()) {
				if (ServletRequestUtils.getIntParameter(request, "productId") != null && !comparisonList.contains(ServletRequestUtils.getIntParameter(request, "productId"))) {
					comparisonList.add( ServletRequestUtils.getIntParameter(request, "productId") );	
				} else {
					myModel.put("message", "comparison.max");
				}
			}
		}
		return null;
	}
}