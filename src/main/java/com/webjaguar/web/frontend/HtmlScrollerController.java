/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 09.25.2009
 */

package com.webjaguar.web.frontend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.domain.Constants;

public class HtmlScrollerController implements Controller {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException {

		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		// return list of products
		List<Product> featuredProducts = new ArrayList<Product>();
		List<Product> products = new ArrayList<Product>();
		boolean detailsBig = ServletRequestUtils.getBooleanParameter(request, "detailsbig", false);
		if (ServletRequestUtils.getIntParameter(request, "cid", -1) != -1) {
			Category thisCategory = this.webJaguar.getCategoryById(ServletRequestUtils.getIntParameter(request, "cid", -1), Constants.FULL_PROTECTED_ACCESS);
			if (thisCategory != null) {
				if (thisCategory.getUrl()!= null && !thisCategory.getUrl().isEmpty()) {
					try {
						Map<String, ProductField> productFieldMap = new HashMap<String, ProductField>();
						
						Pattern pattern = Pattern.compile("(field_)([0-9]+)(=)([^&]*)");
						Matcher matcher = pattern.matcher(thisCategory.getUrl());
						try {
							while (matcher.find()) {
								ProductField pField = new ProductField(Integer.parseInt(matcher.group(2)), matcher.group(4).replace("+", " "));
							//	pField.setOperator("=");
								productFieldMap.put("field_" + Integer.parseInt(matcher.group(2)), pField);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}

						ProductSearch search = new ProductSearch();
						search.setgPRODUCT_FIELDS((Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
						search.setLimit(33);
						search.setProductFieldMap(productFieldMap);
						
						if (search.getProductFieldMap() != null) {
							search.setProductField(new ArrayList<ProductField>());
							search.getProductField().addAll(search.getProductFieldMap().values());
						}
						products = this.webJaguar.searchProducts(search, request, null);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					ProductSearch search = new ProductSearch();
					search.setSort("");
					if (ServletRequestUtils.getStringParameter(request, "language") != null) {
						search.setLang(ServletRequestUtils.getStringParameter(request, "language"));
					}
					products = this.webJaguar.getProductsByCategoryId(thisCategory, null, request, search, false);
				}

			}
		} else if (ServletRequestUtils.getIntParameter(request, "wishlist", -1) != -1) {
			UserSession userSession = this.webJaguar.getUserSession(request);
			if (userSession != null) {
				products = this.webJaguar.getShoppingListByUserid(userSession.getUserid(), 0, request);
			}
		} else if (ServletRequestUtils.getIntParameter(request, "field", -1) != -1 && ServletRequestUtils.getStringParameter(request, "keywords") != null) {
			/*
			 * returns list of Product Image belong to a category or products
			 * having keywords on the specified field INPUT: keywords and field
			 * OUTPUT: list of Product image of the products having same
			 * keywords on the spefied field
			 */
			ProductSearch search = new ProductSearch();
			search.setSort("");
			// search keywords
			search.setKeywords(ServletRequestUtils.getStringParameter(request, "keywords"));
			// search field
			search.setLimit(33);
			search.setSearchFields(new ArrayList<String>());
			search.getSearchFields().add("field_" + ServletRequestUtils.getIntParameter(request, "field", -1));
			search.setDelimiter(siteConfig.get("SEARCH_KEYWORDS_DELIMITER").getValue());
			search.setConjunction(siteConfig.get("SEARCH_DEFAULT_KEYWORDS_CONJUNCTION").getValue());
			search.setgPRODUCT_FIELDS((Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
			products = this.webJaguar.searchProducts(search, request, null);
		} else if (ServletRequestUtils.getIntParameter(request, "orderlist", -1) != -1) {
			UserSession userSession = this.webJaguar.getUserSession(request);
			if (userSession != null && (Boolean) gSiteConfig.get("gMYORDER_LIST")) {
				for (String sku : this.webJaguar.getSKUsByUserId(userSession.getUserid())) {
					if (!("".equals(sku))) {
						Product product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(sku), request);
						if (product != null) {
							products.add(product);
						}
					}
				}
			}
		}

		for (Product thisProduct : products) {
			if (featuredProducts.size() == 33) {
				break;
			}
			if (detailsBig) {
				thisProduct.setImages(this.webJaguar.getImages(thisProduct.getId(), 1));
				featuredProducts.add(thisProduct);
			} else if (thisProduct.getThumbnail() != null) {
				featuredProducts.add(thisProduct);
			}

		}

		if (siteConfig.get("HTML_SCROLLER").getValue().equals("true") && !featuredProducts.isEmpty()) {
			int numBlock = ServletRequestUtils.getIntParameter(request, "numBlock", 3);
			model.put("numItem", featuredProducts.size());
			model.put("page", (((featuredProducts.size() % numBlock) == 0) ? ((int) Math.ceil(featuredProducts.size() / numBlock)) : ((int) Math.ceil(featuredProducts.size() / numBlock)) + 1));
			model.put("featuredProducts", featuredProducts);
			model.put("stylesheet", ServletRequestUtils.getStringParameter(request, "css", "mycss"));
			model.put("increment", ServletRequestUtils.getStringParameter(request, "increment", "636"));
			model.put("detailsbig", ServletRequestUtils.getBooleanParameter(request, "detailsbig", false));
			model.put("numBlock", "" + numBlock);
		}
		if (gSiteConfig.get("gSITE_DOMAIN").equals("viatrading.com") || gSiteConfig.get("gSITE_DOMAIN").equals("test.com")) {
			return new ModelAndView("frontend/viatrading/htmlSlideShow1", "model", model);
		} else {
			return new ModelAndView("frontend/slideShow/htmlSlideShow", "model", model);
		}
	}
}