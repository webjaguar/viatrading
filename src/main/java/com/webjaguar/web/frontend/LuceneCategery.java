package com.webjaguar.web.frontend;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.facet.params.FacetSearchParams;
import org.apache.lucene.facet.search.CountFacetRequest;
import org.apache.lucene.facet.search.FacetRequest;
import org.apache.lucene.facet.search.FacetResult;
import org.apache.lucene.facet.search.FacetResultNode;
import org.apache.lucene.facet.search.FacetsCollector;
import org.apache.lucene.facet.taxonomy.CategoryPath;
import org.apache.lucene.facet.taxonomy.TaxonomyReader;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyReader;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queries.BooleanFilter;
import org.apache.lucene.queries.ChainedFilter;
import org.apache.lucene.queries.FilterClause;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.MultiCollector;
import org.apache.lucene.search.NumericRangeFilter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopFieldDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.BooleanQuery.TooManyClauses;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.logic.WebJaguarImpl;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.FilterAttribute;
import com.webjaguar.model.LuceneProductSearch;
import com.webjaguar.model.Price;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.RangeValue;
import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.lucene.ViaSpecialQuery;
import com.webjaguar.web.domain.Constants;


public class LuceneCategery {
	
	public Map<String, Object> getLuceneProduct(HttpServletRequest request, Map<String, Object> gSiteConfig,Map<String, Configuration> siteConfig, WebJaguarFacade webJaguar, Customer customer, File indexFile, Map<String, Object> model, File productsTaxoDir, int categoryId, String categoryName, boolean includeSubCategoriesProducts) throws ServletRequestBindingException, IOException {
		
		QueryParser qp;
		
		LuceneProductSearch search = getProductSearch(request, categoryId, categoryName, customer, gSiteConfig, webJaguar);
		
		// protected access
		if (customer != null) {
			search.setProtectedAccess(customer.getProtectedAccess());
		} else {
			search.setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue());
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		}
		if ((Boolean) gSiteConfig.get("gMASTER_SKU") && Integer.parseInt(siteConfig.get("PARENT_CHILDERN_DISPLAY").getValue()) == 0) {
			search.setMasterSku(true);
		}
		
		List<Product> results = new ArrayList<Product>();

		boolean showPriceColumn = false;
		boolean showQtyColumn = false;
		boolean minCharsMet = false;
		
		// default proximity set to 50. If it is greater than 100, set it back to 50
		Integer proximity = Integer.parseInt(siteConfig.get("LUCENE_PROXIMITY_VALUE").getValue());
		if (ServletRequestUtils.getIntParameter(request, "pr") != null) {
			proximity = ServletRequestUtils.getIntParameter(request, "pr", proximity) <= 100 ? ServletRequestUtils.getIntParameter(request, "pr", proximity) : proximity;
		} else if (request.getSession().getAttribute("proximity") != null) {
			proximity = (Integer) request.getSession().getAttribute("proximity");
		}
		request.getSession().setAttribute("proximity", proximity);
		// required to set to request directly as frontend jsps are disabled to access session
		// sometimes jsp creates multiple jsessionid
		request.setAttribute("proximity", proximity);

		// Either keywords are required or must have noKeywords=true
		if ((search.getKeywords() != null && search.getKeywords().trim().length() > 0 && minCharsMet) || (ServletRequestUtils.getBooleanParameter(request, "noKeywords", false) || search.getNoKeywords())) {
			DirectoryReader reader = DirectoryReader.open(FSDirectory.open(indexFile));
			IndexSearcher searcher = new IndexSearcher(reader);
			    

			SortField sfName = new SortField("nameSortField", SortField.Type.STRING, false);;
			SortField sfSearchRank = new SortField("search_rank", SortField.Type.INT, false);
			SortField sfPriceRank = new SortField("min_price", SortField.Type.DOUBLE, false);
			
			// default sort by rank, followed by name, followed by price
			Sort sort = new Sort(sfSearchRank, sfName, sfPriceRank);
			
			if(search.getSort().equalsIgnoreCase("lth")) {
				sort = new Sort(sfPriceRank, sfSearchRank, sfName);
			} else if(search.getSort().equalsIgnoreCase("htl")) {
				sfPriceRank = new SortField("min_price", SortField.Type.DOUBLE, true);
				sort = new Sort(sfPriceRank, sfSearchRank, sfName);
			} else if(search.getSort().equalsIgnoreCase("name")) {
				sort = new Sort(sfName, sfSearchRank, sfPriceRank);
			}  
			
			StringBuffer keywords = new StringBuffer();

			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
			qp = new QueryParser(Version.LUCENE_CURRENT, "keywords", analyzer);

			// Use || to search multiple search phrases
			// For example, /lsearch.jhtm?keywords=Red+Pen||Red+Pencil will search for "Red Pen" OR "Red Pencil"
			String[] searchPhrases = search.getKeywords().split("\\|\\|");

			// search fields
			if (siteConfig.get("LUCENE_GROUP_FIELDS_INTO_ONE").getValue().equals("true")) {
				if(search.getKeywords().trim().split(" ").length == 1) {
					// Like Search *
					keywords.append("(all_fields_data : " + search.getKeywords().trim().split(" ")[0] +"*");
				} else {
					keywords.append("( \"" + searchPhrases[0] + "\"~" + proximity);
					for (String searchPhrase : searchPhrases) {
						keywords.append(" OR all_fields_data :\"" + searchPhrase + "\"~" + proximity);
					}
				}
				
			} else {
				keywords.append("( \"" + searchPhrases[0] + "\"~" + proximity);
				for (String searchPhrase : searchPhrases) {
					keywords.append(" OR name :\"" + searchPhrase + "\"~" + proximity);
				}

				String[] searchFieldsList = siteConfig.get("SEARCH_FIELDS").getValue() != null ? siteConfig.get("SEARCH_FIELDS").getValue().split(",") : null;
				if (searchFieldsList != null) {
					for (String field : searchFieldsList) {
						if (!field.isEmpty() && !(field.equalsIgnoreCase("id") || field.equalsIgnoreCase("name") || field.equalsIgnoreCase("search_rank"))) {
							for (String searchPhrase : searchPhrases) {
								keywords.append(" OR " + field + " :\"" + searchPhrase + "\"~" + proximity);
							}
						}
					}
				}
			}
			//faceted search filters
			/*
			List<String> selectedFilters = null;
			for(FilterAttribute filterAttribute : search.getFilters()) {
				if(selectedFilters == null) {
					selectedFilters = new ArrayList<String>();
				} 
				selectedFilters.add(filterAttribute.getParent());
				
				String fieldName = filterAttribute.getParent();
				fieldName = fieldName.replace(" ", "\\ ");
				fieldName = fieldName.replace("(", "\\(");
				fieldName = fieldName.replace(")", "\\)");
			//	keywords.append(" AND ("+fieldName+" :\"" + StringEscapeUtils.escapeJava(filterAttribute.getName().trim()) +"\")");
			}*/
			
			if(ServletRequestUtils.getStringParameter(request, "madeIn") != null) {
				keywords.append(" AND (all_fields_data : \"U.S.A.\" OR all_fields_data : \"USA\")" );
			}
			
			if(ServletRequestUtils.getStringParameter(request, "eco") != null) {
				keywords.append(" AND (all_fields_data : \"eco\")" );
			}
				
			// complete query bracket
			keywords.append(")");

			BooleanFilter multipleFilterWrapper = null;
			BooleanFilter taxonomyFilterWrapper = null;
				
			
			// min & max price
			Double minPrice = ServletRequestUtils.getDoubleParameter(request, "minPrice", -1.0);
			Double maxPrice = ServletRequestUtils.getDoubleParameter(request, "maxPrice", -1.0);

			List<Filter> filtersList = null;
			Filter minPriceFilter = null;
			Filter maxPriceFilter = null;

			if (minPrice != -1.0 && maxPrice != -1.0) {
				minPriceFilter = NumericRangeFilter.newDoubleRange("min_price", minPrice, maxPrice, true, true);
				maxPriceFilter = NumericRangeFilter.newDoubleRange("max_price", minPrice, maxPrice, true, true);

				Filter minLimitPriceFilter = NumericRangeFilter.newDoubleRange("min_price", Double.MIN_VALUE, maxPrice, true, true);
				Filter maxLimitPriceFilter = NumericRangeFilter.newDoubleRange("max_price", minPrice, Double.MAX_VALUE, true, true);

				// convert to array of filters and 
				Filter[] filterArray = new Filter[4];
				filterArray[0] = minPriceFilter;
				filterArray[1] = maxPriceFilter;
				filterArray[2] = minLimitPriceFilter;
				filterArray[3] = maxLimitPriceFilter;
				// 0 for OR operator
				// 1 for AND operator
				int[] logicArray = new int[4];
				logicArray[0] = 1;
				logicArray[1] = 0;
				logicArray[2] = 0;
				logicArray[3] = 1;
					
				org.apache.lucene.queries.ChainedFilter chainFilter = new ChainedFilter(filterArray, logicArray);
				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(chainFilter, Occur.MUST);
				taxonomyFilterWrapper.add(chainFilter, Occur.MUST);
			}
			if (minPrice != -1.0 && maxPrice == -1.0) {
				maxPriceFilter = NumericRangeFilter.newDoubleRange("max_price", minPrice, Double.MAX_VALUE, true, true);
				
				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(maxPriceFilter, Occur.MUST);
				taxonomyFilterWrapper.add(maxPriceFilter, Occur.MUST);
			}
			if (minPrice == -1.0 && maxPrice != -1.0) {
				minPriceFilter = NumericRangeFilter.newDoubleRange("min_price", Double.MIN_VALUE, maxPrice, true, true);
				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(minPriceFilter, Occur.MUST);
				taxonomyFilterWrapper.add(minPriceFilter, Occur.MUST);
			}
				
			// Minimum Quantity Filter
			Filter minQtyFilter = null;
			int minQty = 0;
			int maxQty = 0;
			if(siteConfig.get("LUCENE_SEARCH_EXACT_MIN_QTY").getValue() != null && siteConfig.get("LUCENE_SEARCH_EXACT_MIN_QTY").getValue().equals("true")) {
				maxQty = ServletRequestUtils.getIntParameter(request, "minQty", 0);
			} else {
				minQty = ServletRequestUtils.getIntParameter(request, "minQty", 0);
				maxQty = ServletRequestUtils.getIntParameter(request, "maxQty", 0);
				
			}
			if(minQty > 0 || maxQty > 0){
					
				minQtyFilter = NumericRangeFilter.newIntRange("min_quantity", ((minQty <= 0) ? 1: minQty), ((maxQty <= 0) ? Integer.MAX_VALUE: maxQty), true, true);
				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(minQtyFilter, Occur.MUST);
				taxonomyFilterWrapper.add(minQtyFilter, Occur.MUST);
			}
				
			// search rank Filter
			Filter searchRankFilter = null;
			int searchRank = ServletRequestUtils.getIntParameter(request, "searchRank", -1);
			if(searchRank >= 0){
						
				searchRankFilter = NumericRangeFilter.newIntRange("search_rank", 0, searchRank, true, true);
				searchRankFilter = NumericRangeFilter.newIntRange("search_rank", 0, searchRank, true, true);
				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(searchRankFilter, Occur.MUST);
				taxonomyFilterWrapper.add(searchRankFilter, Occur.MUST);
			}
			
			// range filters on product fields
				
				List<ProductField> productFieldsForSearch = new ArrayList<ProductField>(); 
				for(ProductField productField : webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"))) {
					if(productField.isIndexForSearch() || productField.isIndexForFilter() || productField.isRangeFilter()) {
						productFieldsForSearch.add(productField);
					}
				}
				for(ProductField productField : productFieldsForSearch) {
					if(productField.isRangeFilter()) {
						
						Filter fieldRangeFilter = null;
						Double minRange = ServletRequestUtils.getDoubleParameter(request, "field_"+productField.getId()+"_min", -1.0);
						Double maxRange = ServletRequestUtils.getDoubleParameter(request, "field_"+productField.getId()+"_max", -1.0);
						if (minRange == -1.0 && maxRange == -1.0) {
							continue;
						} else if (minRange != -1.0 && maxRange != -1.0) {
							fieldRangeFilter = NumericRangeFilter.newDoubleRange(productField.getName(), minRange, maxRange, true, true);
						} else if (minRange != -1.0 && maxRange == -1.0) {
							fieldRangeFilter = NumericRangeFilter.newDoubleRange(productField.getName(), minRange, Double.MAX_VALUE, true, true);
						} else if (minRange == -1.0 && maxRange != -1.0) {
							fieldRangeFilter = NumericRangeFilter.newDoubleRange(productField.getName(), Double.MIN_VALUE, maxRange, true, true);
						}
						
						if(multipleFilterWrapper == null) {
							multipleFilterWrapper = new BooleanFilter();
							taxonomyFilterWrapper = new BooleanFilter();
						}
						multipleFilterWrapper.add(fieldRangeFilter, Occur.MUST);
						taxonomyFilterWrapper.add(fieldRangeFilter, Occur.MUST);
					}
				}
			
			// markup pricing
			if(search.getSpecialPricingAvailable() != null){
				TermQuery markUpQuery= new TermQuery(new Term("mark_up_available", ""+search.getSpecialPricingAvailable())); 
				Filter markUpFilter = new QueryWrapperFilter(markUpQuery); 

				TermQuery salesTagQuery= new TermQuery(new Term("active_sales_tag", ""+search.getSpecialPricingAvailable())); 
				Filter salesTagFilter = new QueryWrapperFilter(salesTagQuery); 

				// convert to array of filters and 
				Filter[] filterArray = new Filter[2];
				filterArray[0] = markUpFilter;
				filterArray[1] = salesTagFilter;
				
				// 0 for OR operator
				// 1 for AND operator
				int[] logicArray = new int[2];
				logicArray[0] = 1;
				logicArray[1] = 0;
					
				org.apache.lucene.queries.ChainedFilter chainFilter = new ChainedFilter(filterArray, logicArray);
				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(chainFilter, Occur.MUST);
				taxonomyFilterWrapper.add(chainFilter, Occur.MUST);
			}

			// sales tag
			if(search.getSalesTagActive() != null){
				TermQuery salesTagQuery= new TermQuery(new Term("active_sales_tag", ""+search.getSalesTagActive())); 
				Filter salesTagFilter = new QueryWrapperFilter(salesTagQuery); 

				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(salesTagFilter, Occur.MUST);
				taxonomyFilterWrapper.add(salesTagFilter, Occur.MUST);
			}
			
			// supplier Account number 
			/*
			 * This block sees that when a supplier is logged in and search for a product, they can search only products belonging to them.
			 */
			if(search.getSupplierAccountNumber() != null){
				/*
				 * Lucene filters doesn't work if term from request contains CAPS 
				 */
				TermQuery suppAccountNumberQuery= new TermQuery(new Term("account_number", search.getSupplierAccountNumber().toLowerCase())); 
				Filter suppAccountNumberFilter = new QueryWrapperFilter(suppAccountNumberQuery); 

				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
					taxonomyFilterWrapper = new BooleanFilter();
				}
				multipleFilterWrapper.add(suppAccountNumberFilter, Occur.MUST);
				taxonomyFilterWrapper.add(suppAccountNumberFilter, Occur.MUST);
			}
			
			// catId filter
			if(categoryId > 0) {
				// include all parents catIds and direct catId filter
				if(includeSubCategoriesProducts ) {
					TermQuery parentCatgoriesQuery= new TermQuery(new Term("category_up_tree", ""+String.valueOf(categoryId))); 
					Filter parentCatgoriesQueryFilter = new QueryWrapperFilter(parentCatgoriesQuery); 
					
					if(multipleFilterWrapper == null) {
						multipleFilterWrapper = new BooleanFilter();
						taxonomyFilterWrapper = new BooleanFilter();
					}
					multipleFilterWrapper.add(parentCatgoriesQueryFilter, Occur.MUST);
					taxonomyFilterWrapper.add(parentCatgoriesQueryFilter, Occur.MUST);
				} else {
					TermQuery catIdQuery = new TermQuery(new Term("catIds", String.valueOf(categoryId))); 
					Filter catIdFilter = new QueryWrapperFilter(catIdQuery); 
					
					if(multipleFilterWrapper == null) {
						multipleFilterWrapper = new BooleanFilter();
						taxonomyFilterWrapper = new BooleanFilter();
					}
					multipleFilterWrapper.add(catIdFilter, Occur.MUST);
					taxonomyFilterWrapper.add(catIdFilter, Occur.MUST);
				}
			}
			
			
			
			//faceted search filters
			// Filter does not work with String so it needs to be converted to eqvivalent integer value.
			List<String> selectedFilters = null;
			Map<String, List<FilterAttribute>> nameFilterMap = new HashMap<String, List<FilterAttribute>>();
 			for(FilterAttribute filterAttribute : search.getFilters()) {
 				List<FilterAttribute> filterAttrList = null;
 				if(nameFilterMap.containsKey(filterAttribute.getParent())) {
 					filterAttrList = nameFilterMap.get(filterAttribute.getParent());
 				} else {
 					filterAttrList = new ArrayList<FilterAttribute>();
 	 			}
 				filterAttrList.add(filterAttribute);
 				
 				nameFilterMap.put(filterAttribute.getParent(), filterAttrList);
 			}
 			
 			for(String filterName : nameFilterMap.keySet()) {
 				if(selectedFilters == null) {
					selectedFilters = new ArrayList<String>();
				}	
 				String[] valuesArray = new String[nameFilterMap.get(filterName).size()];
 				boolean isCheckboxFilter = false;
 				BooleanFilter tempFiler = new BooleanFilter();
				for(int i=0; i<nameFilterMap.get(filterName).size(); i++) {
 					FilterAttribute filterAttribute = nameFilterMap.get(filterName).get(i);
 	 				valuesArray[i] = this.getEquivalentIntNumber(filterAttribute.getName().trim()).toString();
 					isCheckboxFilter = filterAttribute.isCheckboxFilter();
 				
 					TermQuery filterTermQuery = new TermQuery(new Term(filterName, this.getEquivalentIntNumber(filterAttribute.getName().trim()).toString())); 
 					Filter filterTermFilter = new QueryWrapperFilter(filterTermQuery); 
 					tempFiler.add(filterTermFilter, Occur.SHOULD);
 				}
 				
				if(multipleFilterWrapper == null) {
					multipleFilterWrapper = new BooleanFilter();
				}
 				multipleFilterWrapper.add(tempFiler, Occur.MUST);
 				if(!isCheckboxFilter) {
	 				if(taxonomyFilterWrapper == null) {
	 					taxonomyFilterWrapper = new BooleanFilter();
	 				}
 					taxonomyFilterWrapper.add(tempFiler, Occur.MUST);
 	 	 		}
 				
			}
			
					
			
			Query query = null;
			TopFieldDocs docs = null;
			try {
				if(search.getKeywords() != null && !search.getKeywords().toString().trim().isEmpty()) {
					query = qp.parse(keywords.toString());
				} else {
					query = new MatchAllDocsQuery();
				}
				if(search.getSort().equalsIgnoreCase("rlv")) {
					Query adiSpecialQuery = new ViaSpecialQuery(query, search.getKeywords().toString());
					docs = searcher.search(adiSpecialQuery, multipleFilterWrapper, 10000, sort);
				} else {
					docs = searcher.search(query, multipleFilterWrapper, 10000, sort);
				}
			} catch (ParseException e) {
				e.printStackTrace();
				//model.put("tooManyClauses", true);
			} catch (TooManyClauses e) {
				e.printStackTrace();
				//model.put("tooManyClauses", true);
			}
			
				
				
			if (docs != null) {
				ScoreDoc[] hits = docs.scoreDocs;
				int count = hits.length;
				int start = (search.getPage() - 1) * search.getPageSize();
				if (start > count) {
					search.setPage(1);
					start = 0;
				}
				int end = start + search.getPageSize();
				if (end > count) {
					end = count;
				}

				int pageCount = count / search.getPageSize();
				if (count % search.getPageSize() > 0) {
					pageCount++;
				}

				model.put("start", start);
				model.put("pageCount", pageCount);
				model.put("count", count);
				
				
				//faceted search filters
				Map<String, List<String>> selectedFilterNameValuesMap = null;
				for(FilterAttribute filterAttribute : search.getFilters()) {
					if(selectedFilterNameValuesMap == null) {
						selectedFilterNameValuesMap = new HashMap<String, List<String>>();
					} 
					
					String fieldName = filterAttribute.getParent();
					List<String> values = selectedFilterNameValuesMap.get(fieldName);
					if(values == null) {
						values = new ArrayList<String>();
					}
					values.add(filterAttribute.getName().trim());
				
					selectedFilterNameValuesMap.put(fieldName, values);
				}
				
				// Multifaceted Search on Product Fields
				try {
					this.getMultiFacetedSearchOnRegularFields(searcher, count, reader, query, multipleFilterWrapper, taxonomyFilterWrapper, selectedFilterNameValuesMap, nameFilterMap, model, gSiteConfig, request, siteConfig, productsTaxoDir, webJaguar);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				
				for (int i = start; i < end; i++) {
					Document doc = searcher.doc(hits[i].doc);
					Product product = webJaguar.getProductById(new Integer(doc.get("id")), request);
					if (product != null) {

						if (search.isMasterSku() && product.getMasterSku() != null && !product.getMasterSku().isEmpty()) {
							// Don't add Child product to result
						} else {
							
							if(siteConfig.get("LUCENE_SEARCH_SHOW_INDEXED_PRICE").getValue().equals("true")) {
								
								List<Price> priceList = new ArrayList<Price>();
								Price price1 = new Price();
								try {
									if((Boolean) gSiteConfig.get("gWILDMAN_GROUP")){
										price1.setAmt(Double.parseDouble(doc.get("min_price")));
									} else {
										price1.setAmt(Double.parseDouble(doc.get("max_price")));
									}
									
									priceList.add(price1);
								} catch(Exception e) {
									e.printStackTrace();
								}
								
								if(!(Boolean) gSiteConfig.get("gWILDMAN_GROUP")){
									try {
										if(Double.parseDouble(doc.get("min_price")) != Double.parseDouble(doc.get("max_price"))) {
											Price price2 = new Price();
											price2.setAmt(Double.parseDouble(doc.get("min_price")));
											priceList.add(price2);
										}
									} catch(Exception e) {
										e.printStackTrace();
									}
								}
								product.setPrice(priceList);
								
							} else {
									// for ASI Products check if markup is available
								if(product.getAsiId() != null) {
									/*
									 * For hbgmarketplace.com markup rule:
									 * First apply supplier, if supplier is empty look for customer, if customer is empty apply global markups.
									 * For bnoticed, ribbon should appear only based on suplier or customer markups.
									 */
									if(product.getDefaultSupplierId() > 0) {
										Supplier supplier = webJaguar.getSupplierById(product.getDefaultSupplierId());
										Class<Supplier> c1 = Supplier.class;
										for (int j = 1; j < 10; j++) {
											Double value = null;
											try {
												Method m2 = c1.getMethod("getMarkup" + j);
												value = (Double) m2.invoke(supplier, null);
											} catch (Exception e) {
											}
											if (value != null) {
												product.setMarkUpApplied(true);
											}
											if(product.isMarkUpApplied()) {
												break;
											}
										}
									}
									if(!product.isMarkUpApplied() && customer!= null) {
										Class<Customer> c1 = Customer.class;
										for (int j = 1; j < 10; j++) {
											Double value = null;
											try {
												Method m2 = c1.getMethod("getMarkup" + j);
												value = (Double) m2.invoke(customer, null);
											} catch (Exception e) {
											}
											if (value != null) {
												product.setMarkUpApplied(true);
											}
											if(product.isMarkUpApplied()) {
												break;
											}
										}
									}
								}
							}
							results.add(product);
						}

						if (!showPriceColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) || product.getMsrp() != null)) {
							showPriceColumn = true;
						}
						if (!showQtyColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) && (!product.isLoginRequire() || (product.isLoginRequire() && customer != null)))) {
							showQtyColumn = true;
						}

						// master sku
						if (gSiteConfig.get("gSITE_DOMAIN").equals("gajoodle.com")) {
								
							if((Boolean) gSiteConfig.get("gMASTER_SKU")) {

								//Add Child Products
								String childProductIds = doc.get("child_product_ids");
								if(childProductIds != null) {
									String[] childProductId = childProductIds.split(",");
									List<Product> childProductsList = new ArrayList<Product>();
									for(int j=0; j <childProductId.length; j++ ) {
										if(childProductId[j] != null && !childProductId[j].isEmpty()) {
											Product childProduct = webJaguar.getProductById(new Integer(childProductId[j]), request);
											if(childProduct != null) {
												childProductsList.add(childProduct);
											}
										}
									}
									if(!childProductsList.isEmpty()) {
										product.setVariants(childProductsList);
									}
								}
							}
						}
					}
				}
				model.put("lProductSearch", search);
				model.put("results", results);
				model.put("pageEnd", start + results.size());
			}
		}
		return model;
	}
	
	public Integer getEquivalentIntNumber(String fieldValue) {
		final int prime = 31;
		Integer result = 1;
		for(char c : fieldValue.toCharArray()) {
			result = prime * result + Character.getNumericValue(c);
		}
		return  Math.abs(result);
	}

	
	private LuceneProductSearch getProductSearch(HttpServletRequest request, int categoryId, String categoryName, Customer customer, Map<String, Object> gSiteConfig, WebJaguarFacade webJaguar) {

		
		LuceneProductSearch search = (LuceneProductSearch) WebUtils.getSessionAttribute(request, "catLuceneSearch" + categoryId);

		if (search == null) {
			Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
			search = new LuceneProductSearch();
			search.setSort(siteConfig.get("SEARCH_SORT").getValue());
			search.setPageSize(new Integer(siteConfig.get("NUMBER_PRODUCT_PER_PAGE_SEARCH").getValue()));
			if(request.getSession().getAttribute("catPageSize") != null) {
				search.setPageSize(new Integer(request.getSession().getAttribute("catPageSize").toString()));
			}
			request.getSession().setAttribute("catLuceneSearch" + categoryId, search);
		}
		// required to set to request directly as frontend jsps are disabled to access session
		// sometimes jsp creates multiple jsessionid
		request.getSession().setAttribute("catLuceneSearch"+categoryId, search);
		
		
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		
		search.setPageSize(new Integer(siteConfig.get("NUMBER_PRODUCT_PER_PAGE_SEARCH").getValue()));
		if(request.getSession().getAttribute("catPageSize") != null) {
			search.setPageSize(new Integer(request.getSession().getAttribute("catPageSize").toString()));
		}
		search.setNoKeywords(true);
		
		// sort
		if (request.getParameter("sort") != null) {
			String sortBy = ServletRequestUtils.getStringParameter(request, "sort", siteConfig.get("SEARCH_SORT").getValue());
			search.setSort(sortBy);
		}
		request.setAttribute("catLuceneSearch", search);
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);
			}
		}

		// size (For new responsive design, page sizes are kept at 24,49,100)
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 24);
			if (size < 24) {
				search.setPageSize(24);
			} else {
				search.setPageSize(size);
			}
			request.getSession().setAttribute("catPageSize", request.getParameter("size"));
		}

		// faceted search
		search.getFilters().clear();
		//for ADI, top level category needs to show 2nd level category filters. To make it work, add 2nd Level Filters on top level category.
		if (ServletRequestUtils.getStringParameters(request, "facetNameValue") != null && ServletRequestUtils.getStringParameters(request, "facetNameValue").length > 0) {
			for(String facetNameValue : ServletRequestUtils.getStringParameters(request, "facetNameValue")) {
				try {
					FilterAttribute filterAttribute = new FilterAttribute();
				    filterAttribute.setParent(facetNameValue.split("_value_")[0]);
				    filterAttribute.setParentDisplayName(facetNameValue.split("_value_")[0]);
				    filterAttribute.setName(facetNameValue.split("_value_")[1]);
				    
					filterAttribute.setDisplayName(facetNameValue.split("_value_")[1]);
					search.getFilters().add(filterAttribute);
					
				} catch(Exception e) {
					
				}
			}
		}
		
		return search;
	}
	
	private void getMultiFacetedSearchOnRegularFields(IndexSearcher searcher, int searchResultCount, DirectoryReader reader, Query query, Filter multipleFilterWrapper, Filter taxonomyFilterWrapper, Map<String, List<String>> selectedFilterNameValuesMap, Map<String, List<FilterAttribute>> nameFilterMap, Map<String, Object> model, Map<String, Object> gSiteConfig, HttpServletRequest request, Map<String, Configuration> siteConfig, File productTaxoDir, WebJaguarFacade webJaguar) throws IOException{

		TopScoreDocCollector tdc = TopScoreDocCollector.create(1, true);
		TaxonomyReader taxo = new DirectoryTaxonomyReader(FSDirectory.open(productTaxoDir));
		
		List<ProductField> productFieldFiltersList = new ArrayList<ProductField>(); 
		Map<String, ProductField> fieldIdMap = new HashMap<String, ProductField>();
		for(ProductField productField : webJaguar.getProductFieldsRanked(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"), null)) {
			if(productField.isIndexForSearch() || productField.isIndexForFilter() || productField.isRangeFilter()) {
				productFieldFiltersList.add(productField);
				fieldIdMap.put(productField.getName(), productField);
			}
		}
		
		//Format must be #name#_rank_#value# (Category_rank_5, Price Range_rank_3);
		if(!siteConfig.get("LUCENE_FILTERS_RANK").getValue().trim().isEmpty()) {
			String[] filterRankValueArray = siteConfig.get("LUCENE_FILTERS_RANK").getValue().split(",");
			int id = -1;
			for(String filterRankValue : filterRankValueArray) {
				String[] frv = filterRankValue.split("_rank_");
				
				ProductField pf = new ProductField();
				pf.setId(id);
				id = id - 1;
				
				pf.setName(frv[0].trim());
				pf.setRank(Integer.parseInt(frv[1].trim()));
				pf.setIndexForFilter(true);
				pf.setIndexForSearch(true);
				productFieldFiltersList.add(pf);
				fieldIdMap.put(pf.getName(), pf);
			}
		}
				
		List<FacetRequest> facetRequests = new ArrayList<FacetRequest>();
		FacetRequest facetRequest = null;
		
		for(ProductField productField : productFieldFiltersList) {
			if(productField.isIndexForFilter()) {
				facetRequest = new CountFacetRequest(new CategoryPath(productField.getName()),100);
				// avoid duplicate fields/faceted request like multiple categories from appearing on the filters
				if(!facetRequests.contains(facetRequest)) {
					facetRequests.add(facetRequest);
				}
			}
		}
		if(!facetRequests.isEmpty()) {
			
			
			FacetSearchParams facetSearchParams = new FacetSearchParams(facetRequests);
			FacetsCollector facetsCollector = FacetsCollector.create(facetSearchParams, reader, taxo);
			try {
				searcher.search(query, multipleFilterWrapper, MultiCollector.wrap(tdc, facetsCollector));
			} catch(Exception e) {
				
			}
			List<FacetResult> res = facetsCollector.getFacetResults();
			
			Map<String, List<FilterAttribute>> filterMap = new HashMap<String, List<FilterAttribute>>();
			List<FilterAttribute> filterAttributes = new ArrayList<FilterAttribute>();
			FilterAttribute filterAttribute = null;
			for (FacetResult fr:res) {
				
				if(fr.getFacetResultNode().subResults.isEmpty()) {
					continue;
				}
				ProductField  pf = fieldIdMap.get(fr.getFacetResultNode().label.toString());
				//continue for checkbox filter as there is a separate function call to add checkbox filter at the end of this function.
				if(pf.isCheckboxFilter()) {
					continue;
				}
				
				if(!pf.isCheckboxFilter() && selectedFilterNameValuesMap != null && selectedFilterNameValuesMap.containsKey(fr.getFacetResultNode().label.toString())) {
					continue;
				}
				
				filterAttribute = new FilterAttribute();
			    filterAttribute.setName(fr.getFacetResultNode().label.toString());
			    filterAttribute.setCheckboxFilter(pf.isCheckboxFilter());
				
			    FilterAttribute subFilterAttribute = null;
			    List<FilterAttribute> subFilterAttributes = new ArrayList<FilterAttribute>();
			    
			    for ( FacetResultNode sr : fr.getFacetResultNode().subResults) {
			    	// do not show filter with no name 
			    	if(sr.label.components[1].trim().isEmpty()) {
			    		continue;
			    	}
			    	
			    	// do not show filter if count is same as search result count 
			    	// ADI Requirement: If clicking on a refiner would not change the number of products displaying, the refiner should not be displayed.
			    	if(!filterAttribute.isCheckboxFilter() && (new Double(sr.value).intValue() >= searchResultCount)) {
			    		continue;
			    	}
			    	
			    	//for checkbox filter, do not show selected value 
			    	if(filterAttribute.isCheckboxFilter() && selectedFilterNameValuesMap != null 
							&& selectedFilterNameValuesMap.containsKey(fr.getFacetResultNode().label.toString())) {
						List<String> values = selectedFilterNameValuesMap.get(fr.getFacetResultNode().label.toString());
						if(values.contains(sr.label.components[1].trim())) {
							continue;
						}
			    	}
			    	
			    	subFilterAttribute = new FilterAttribute();
				    subFilterAttribute.setName(sr.label.components[1]);
				    subFilterAttribute.setDisplayName(sr.label.components[1]);
				    subFilterAttribute.setResultCount(new Double(sr.value).intValue());
					
				    subFilterAttributes.add(subFilterAttribute);
			    }
			    // Do not display filter, if sub filters are empty
			    if(subFilterAttributes.isEmpty()) {
			    	// do not show filter with no name 
			    	continue;
			    }
			    filterAttribute.setSubFilters(subFilterAttributes);
				filterAttribute.setRangeFilter(false);
				filterAttribute.setFilterId(pf.getId());
				filterAttribute.setRank(pf.getRank());
					
							
				if(filterMap.get(pf.getGroupName()) != null) {
					filterMap.get(pf.getGroupName()).add(filterAttribute);
			    } else {
			    	filterAttributes = new ArrayList<FilterAttribute>();
			    	filterAttributes.add(filterAttribute);
					filterMap.put(pf.getGroupName(), filterAttributes);
				}
			}
			filterMap = this.updateFiltersOrderByRank(filterMap, siteConfig, webJaguar);
			
			model.put("filterMap", filterMap);
			model.put("filterAttributes", filterAttributes);
			
			this.getMultiFacetedSearchOnRegularFieldsWithMultipleSelection(filterAttributes, filterMap, fieldIdMap, productFieldFiltersList, taxo, searcher, reader, query, taxonomyFilterWrapper, selectedFilterNameValuesMap, nameFilterMap, model, siteConfig, webJaguar);
		//	this.getMultiFacetedSearchOnRegularFieldsWithMultipleSelection(filterAttributes, filterMap, fieldIdMap, productFieldFiltersList, taxo, searcher, reader, query, taxonomyFilterWrapper, selectedFilterNameValuesMap, model, siteConfig);
		}
	}

	private void getMultiFacetedSearchOnRegularFieldsWithMultipleSelection(List<FilterAttribute> filterAttributes, Map<String, List<FilterAttribute>> filterMap, Map<String, ProductField> fieldIdMap, List<ProductField> productFieldFiltersList, TaxonomyReader taxo, IndexSearcher searcher, DirectoryReader reader, Query query, Filter taxonomyFilterWrapper, Map<String, List<String>> selectedFilterNameValuesMap, Map<String, List<FilterAttribute>> nameFilterMap2, Map<String, Object> model, Map<String, Configuration> siteConfig, WebJaguarFacade webJaguar) throws IOException{

		TopScoreDocCollector tdc = TopScoreDocCollector.create(1, true);
		//Format must be #name#_rank_#value# (Category_rank_5, Price Range_rank_3);
		if(!siteConfig.get("LUCENE_FILTERS_RANK").getValue().trim().isEmpty()) {
			String[] filterRankValueArray = siteConfig.get("LUCENE_FILTERS_RANK").getValue().split(",");
			int id = -1;
			for(String filterRankValue : filterRankValueArray) {
				String[] frv = filterRankValue.split("_rank_");
				
				ProductField pf = new ProductField();
				pf.setId(id);
				id = id - 1;
				
				pf.setName(frv[0].trim());
				pf.setRank(Integer.parseInt(frv[1].trim()));
				pf.setIndexForFilter(true);
				pf.setIndexForSearch(true);
				productFieldFiltersList.add(pf);
				fieldIdMap.put(pf.getName(), pf);
			}
		}
		
		Set<String> checkBoxFiltersList = new HashSet<String>();
		for(ProductField productField : productFieldFiltersList) {
			if(productField.isIndexForFilter() && productField.isCheckboxFilter()) {
				checkBoxFiltersList.add(productField.getName());
			}
		}
		for(String checkBoxFilter : checkBoxFiltersList) {
			FacetRequest facetRequest = new CountFacetRequest(new CategoryPath(checkBoxFilter),100);
			List<FacetRequest> facetRequests = new ArrayList<FacetRequest>();
			facetRequests.add(facetRequest);
			BooleanFilter tempTaxoFilter = new BooleanFilter();
			
			if(taxonomyFilterWrapper != null) {
				for(FilterClause fc : ((BooleanFilter) taxonomyFilterWrapper).clauses()) {
					if(!fc.toString().contains(checkBoxFilter)) {
						tempTaxoFilter.add(fc);
					}
				}
			}
			for(String checkBoxFilter2 : checkBoxFiltersList) {
				if(checkBoxFilter2.equalsIgnoreCase(checkBoxFilter)) {
 					continue;
 				}	
				BooleanFilter tempFiler = new BooleanFilter();
 				boolean addedNewFilter = false;
 				if(selectedFilterNameValuesMap != null) {
 					for(String key : selectedFilterNameValuesMap.keySet()) {
 	 					if(key.equalsIgnoreCase(checkBoxFilter)) {
 	 						continue;
 	 					}
 	 					List<String> selectedValues = selectedFilterNameValuesMap.get(key);
 	 					for(String value : selectedValues) {
 	 						TermQuery filterTermQuery = new TermQuery(new Term(key, this.getEquivalentIntNumber(value).toString())); 
 	 	 					Filter filterTermFilter = new QueryWrapperFilter(filterTermQuery); 
 	 	 					tempFiler.add(filterTermFilter, Occur.SHOULD);
 	 	 					addedNewFilter = true;
 	 	 				}
 	 				} 
 	 			}
 				if(addedNewFilter) {
 					if(tempTaxoFilter == null) {
 						tempTaxoFilter = new BooleanFilter();
 					}
 					tempTaxoFilter.add(tempFiler, Occur.MUST);
 		 	 	}
 			}
			
			FacetSearchParams facetSearchParams = new FacetSearchParams(facetRequests);
			FacetsCollector facetsCollector = FacetsCollector.create(facetSearchParams, reader, taxo);
			try {
				searcher.search(query, tempTaxoFilter, MultiCollector.wrap(tdc, facetsCollector));
			} catch(Exception e) {
				
			}
			List<FacetResult> res = facetsCollector.getFacetResults();
			
			for (FacetResult fr:res) {
				
				if(fr.getFacetResultNode().subResults.isEmpty()) {
					continue;
				}
				ProductField  pf = fieldIdMap.get(fr.getFacetResultNode().label.toString());
				
				if(!pf.isCheckboxFilter()) {
					continue;
				}
				FilterAttribute filterAttribute = new FilterAttribute();
			    filterAttribute.setName(fr.getFacetResultNode().label.toString());
			    filterAttribute.setCheckboxFilter(pf.isCheckboxFilter());
				
			    FilterAttribute subFilterAttribute = null;
			    List<FilterAttribute> subFilterAttributes = new ArrayList<FilterAttribute>();
			    
				
	    		filterAttribute.setCheckboxFilter(pf.isCheckboxFilter());
				
			    
			    for ( FacetResultNode sr : fr.getFacetResultNode().subResults) {
			    	
			    	// do not show filter with no name 
			    	if(sr.label.components[1].trim().isEmpty()) {
			    		continue;
			    	}
			    	
			    	subFilterAttribute = new FilterAttribute();
				    if(filterAttribute.isCheckboxFilter() && selectedFilterNameValuesMap != null 
							&& selectedFilterNameValuesMap.containsKey(fr.getFacetResultNode().label.toString())) {
						List<String> values = selectedFilterNameValuesMap.get(fr.getFacetResultNode().label.toString());
						if(values.contains(sr.label.components[1].trim())) {
							subFilterAttribute.setSelected(true);
						}
			    	}
			    	
			    	subFilterAttribute.setName(sr.label.components[1]);
				    subFilterAttribute.setDisplayName(sr.label.components[1]);
				    subFilterAttribute.setResultCount(new Double(sr.value).intValue());
					
				    subFilterAttributes.add(subFilterAttribute);
			    }
				
			    if(!subFilterAttributes.isEmpty()) {
			    	filterAttribute.setSubFilters(subFilterAttributes);
			    	filterAttributes.add(filterAttribute);
			    	filterMap.put(null, filterAttributes);
				}
			}
				
		}
		filterMap = this.updateFiltersOrderByRank(filterMap, siteConfig, webJaguar);
		model.put("filterMap", filterMap);
		model.put("filterAttributes", filterAttributes);
		}

	
	public Map<String, List<FilterAttribute>> updateFiltersOrderByRank(Map<String, List<FilterAttribute>> filterMap, WebJaguarFacade webJaguar) {
		
		
		List<RangeValue> ranges = webJaguar.getRangeValues(null);
		Map<String, Integer> rvMap = null;
		if(ranges != null && !ranges.isEmpty()) {
			rvMap = new HashMap<String, Integer>();
			for(RangeValue rv : ranges) {
				rvMap.put(rv.getRangeDisplayValue(), rv.getId());
			}
		}
		
		List<FilterAttribute> tempList = new ArrayList<FilterAttribute>();
		Map<String, String> fieldGroupMap = new HashMap<String, String>();
		for(String key : filterMap.keySet()) {
			
			List<FilterAttribute> faList = filterMap.get(key);
			Collections.sort(faList);
			filterMap.put(key, faList);
			
			for(FilterAttribute faAttr : faList) {
				if(rvMap != null && faAttr.getName().equalsIgnoreCase("Price Range")) {
					if(faAttr.getSubFilters() != null) {
						for(FilterAttribute tempAttr : faAttr.getSubFilters()) {
							tempAttr.setRank(rvMap.get(tempAttr.getDisplayName()));
						}
					}
				}
				
				if(rvMap != null && faAttr.getName().equalsIgnoreCase("Reward Points Range")) {
					if(faAttr.getSubFilters() != null) {
						for(FilterAttribute tempAttr : faAttr.getSubFilters()) {
							tempAttr.setRank(rvMap.get(tempAttr.getDisplayName()));
						}
					}
				}
				if(faAttr.getSubFilters() != null) {
					Collections.sort(faAttr.getSubFilters());
				}
			}
			
			tempList.addAll(filterMap.get(key));
			
			for(FilterAttribute attr : filterMap.get(key)) {
				fieldGroupMap.put(attr.getName(), key);
			}
		}
		
		Collections.sort(tempList);
		
		Map<String, List<FilterAttribute>> tempFilterMap = new LinkedHashMap<String, List<FilterAttribute>>();
		for(FilterAttribute attr : tempList) {
			if(!tempFilterMap.containsKey(fieldGroupMap.get(attr.getName()))) {
				tempFilterMap.put(fieldGroupMap.get(attr.getName()), filterMap.get(fieldGroupMap.get(attr.getName())));
			}
		}
		
		return tempFilterMap;
	}
	
	
	public Map<String, List<FilterAttribute>> updateFiltersOrderByRank(Map<String, List<FilterAttribute>> filterMap, Map<String, Configuration> siteConfig, WebJaguarFacade webJaguar) {
		
		List<RangeValue> ranges = webJaguar.getRangeValues(null);
		//List<RangeValue> ranges = null;
		Map<String, Integer> rvMap = null;
		if(ranges != null && !ranges.isEmpty()) {
			rvMap = new HashMap<String, Integer>();
			for(RangeValue rv : ranges) {
				rvMap.put(rv.getRangeDisplayValue(), rv.getId());
			}
		}
		
		List<FilterAttribute> tempList = new ArrayList<FilterAttribute>();
		Map<String, String> fieldGroupMap = new HashMap<String, String>();
		for(String key : filterMap.keySet()) {
			
			List<FilterAttribute> faList = filterMap.get(key);
			Collections.sort(faList);
			filterMap.put(key, faList);
			
			for(FilterAttribute faAttr : faList) {
				if(rvMap != null && faAttr.getName().equalsIgnoreCase("Price Range")) {
					if(faAttr.getSubFilters() != null) {
						for(FilterAttribute tempAttr : faAttr.getSubFilters()) {
							tempAttr.setRank(rvMap.get(tempAttr.getDisplayName()));
						}
					}
				}
				
				if(rvMap != null && faAttr.getName().equalsIgnoreCase("Reward Points Range")) {
					if(faAttr.getSubFilters() != null) {
						for(FilterAttribute tempAttr : faAttr.getSubFilters()) {
							tempAttr.setRank(rvMap.get(tempAttr.getDisplayName()));
						}
					}
				}
				
				if(faAttr.getSubFilters() != null) {
					// for domains other than bnoticed, show top 20 filters by result count and sort it alpabatically
					// if bntoiced agree on this behavior, remove this if condition.
					if(!(siteConfig.get("SITE_URL").getValue().contains("bnoticed") || siteConfig.get("SITE_URL").getValue().contains("test300"))) {
						if(faAttr.getSubFilters().size() > 20) {
							for(int i=20; i<faAttr.getSubFilters().size(); i++) {
								faAttr.getSubFilters().remove(i);
							}
						}
						Collections.sort(faAttr.getSubFilters());
					}
				}
			}
			
			tempList.addAll(filterMap.get(key));
			
			for(FilterAttribute attr : filterMap.get(key)) {
				fieldGroupMap.put(attr.getName(), key);
			}
		}
		
		Collections.sort(tempList);
		
		Map<String, List<FilterAttribute>> tempFilterMap = new LinkedHashMap<String, List<FilterAttribute>>();
		for(FilterAttribute attr : tempList) {
			if(!tempFilterMap.containsKey(fieldGroupMap.get(attr.getName()))) {
				tempFilterMap.put(fieldGroupMap.get(attr.getName()), filterMap.get(fieldGroupMap.get(attr.getName())));
			}
		}
		
		return tempFilterMap;
	}
}
