/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ConsignmentReport;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerCreditHistory;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;
import java.util.Collections;

public class ViewMyCreditHistory implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	

    	UserSession userSession = this.webJaguar.getUserSession(request);	
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		int custId = userSession.getUserid();
		model.put("userId", custId);	
		Customer customer = this.webJaguar.getCustomerById( custId );
		ConsignmentReport search = new ConsignmentReport();
		
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		List<CustomerCreditHistory> creditHistory = this.webJaguar.getCustomerCreditHistory(customer.getSupplierId(), customer.getId());
		
		DecimalFormat twoDForm = new DecimalFormat("#.##");	     
		double credit= 0.00;
		for(CustomerCreditHistory customerCredit: creditHistory ) {
			credit = customerCredit.getCredit() + credit;
			customerCredit.setTotal(Double.valueOf(twoDForm.format(credit)));
		}
		Collections.reverse(creditHistory); // To display the newest date first. 
		
		PagedListHolder creditHistoryList = new PagedListHolder(creditHistory);
		creditHistoryList.setPageSize(search.getPageSize());
		creditHistoryList.setPage(search.getPage()-1);
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "My Balance"));
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		model.put("creditHistory", creditHistoryList);	
		model.put("viewMyBalanceLayout", this.webJaguar.getSystemLayout("viewMyBalanceLayout", request.getHeader("host"), request));
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/account/myCreditHistory", "model", model);    	
		}
		return new ModelAndView("frontend/account/myCreditHistory", "model", model);    	
    }
}
