/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.12.2009
 */

package com.webjaguar.web.frontend.account;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.CustomerForm;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class UserCategoryFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	public UserCategoryFormController() {
		setCommandName("customerForm");
		setCommandClass(CustomerForm.class);
		setFormView("frontend/account/category");
		setSuccessView("account.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {

		CustomerForm customerForm = (CustomerForm) command;

		// check if cancel button was pressed
		if (request.getParameter("_cancel") == null) {
	    	// category ids
	        Set<Object> categoryIds = new HashSet<Object>();
			for (int i=0; i<customerForm.getParentIds().length; i++) {
				String id = ServletRequestUtils.getStringParameter(request, "cat" + i + "_categoryIds", "");
	            if (!"".equals(id)) {
	            	categoryIds.add(new Integer(id));                                        
	            }                                        		
			}	
			this.webJaguar.updateCategoryIdsByUser(customerForm.getCustomer().getId(), categoryIds);
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
		UserSession userSession = this.webJaguar.getUserSession(request);
		
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );	
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		CustomerForm customerForm = new CustomerForm();	
		customerForm.getCustomer().setId(userSession.getUserid());
		
		if (gSiteConfig.get("gCUSTOMER_CATEGORY") != null) {
			// category associations
			List<Object> catIds = new ArrayList<Object>();
			catIds.addAll(this.webJaguar.getCategoryIdsByUser(userSession.getUserid()));	
			for (int i=catIds.size(); i<5; i++) {
				catIds.add("");				
			}
			customerForm.setCategoryIds(catIds.toArray());
			List<Object> parentIds = new ArrayList<Object>();
			for (int i=0; i<catIds.size(); i++) {
				Integer catId = null;
				try {					
					catId = new Integer(catIds.get(i).toString());
				} catch (NumberFormatException e) {}
				Category category = this.webJaguar.getCategoryById(catId, "");
				if (category == null || category.getParent() == null) {
					parentIds.add(gSiteConfig.get("gCUSTOMER_CATEGORY"));					
				} else {
					parentIds.add(category.getParent());					
				}
			}
			customerForm.setParentIds(parentIds.toArray());			
		}
		
		return customerForm;
		
	}
	
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {

    	CustomerForm customerForm = (CustomerForm) command;
    	
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> myModel = new HashMap<String, Object>();
 	
		if (customerForm.getParentIds() != null) {
			Map <Integer, Object> categories = new HashMap<Integer, Object>();
			Map <Integer, Object> tree = new HashMap<Integer, Object>();
			for (int i=0; i<customerForm.getParentIds().length; i++) {
				categories.put(i, this.webJaguar.getCategoryLinks(new Integer(customerForm.getParentIds()[i].toString()), request));						
				myModel.put("categories", categories);
				tree.put(i, this.webJaguar.getCategoryTree(new Integer(customerForm.getParentIds()[i].toString()), true, null));						
				myModel.put("tree", tree);
			}					
		}
		
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		map.put("model", myModel);
		
		Layout layout = (Layout) request.getAttribute("layout");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		
		return map;
    }
}
