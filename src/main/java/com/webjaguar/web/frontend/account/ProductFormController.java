/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.text.NumberFormat;
import java.util.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.ProductForm;
import com.webjaguar.web.validator.ProductFormValidator;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.UserSession;

// If we enable 'Add Product' from Front end, we have to add more code. Should insert entry into 'Product_Supplier database' and assign Cost to '0'.

public class ProductFormController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ProductFormController () {
		setCommandName("productForm");
		setCommandClass(ProductForm.class);		
		setPages(new String[] {"frontend/account/productForm", "frontend/account/gallery"});
	}	
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder( request, binder );
		NumberFormat nf = NumberFormat.getInstance( RequestContextUtils.getLocale( request ) );
		nf.setMaximumFractionDigits( 2 );
		nf.setMinimumFractionDigits( 2 );
		binder.registerCustomEditor( Double.class, new CustomNumberEditor( Double.class, nf, true ) );
		// for qty breaks
		binder.registerCustomEditor( Integer.class, new CustomNumberEditor( Integer.class, true ) );
	}
	
	protected boolean suppressValidation(HttpServletRequest request, Object command) {
		if (ServletRequestUtils.getStringParameter(request, "_delete", "").equals("true")) {
			return true;
		}
		return false;
	}
	
	public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		ProductForm productForm = (ProductForm) command;

		// check if delete button was pressed
		if (ServletRequestUtils.getStringParameter(request, "_delete", "").equals("true")) {
			this.webJaguar.deleteProduct(productForm.getProduct());
			return new ModelAndView(new RedirectView("account_products.jhtm"));
		}
                
        Set<Object> categories = new HashSet<Object>();
		for (int i=0; i<productForm.getParentIds().length; i++) {
			String id = ServletRequestUtils.getStringParameter(request, "cat" + i + "_categoryIds", "");
            if (!"".equals(id)) {
            	categories.add(new Integer(id));                                        
            }                                        		
		}
        productForm.getProduct().setCatIds(categories);
        
        // images
        productForm.getProduct().setImages(new ArrayList<ProductImage>());
        for (ProductImage image : productForm.getProductImages()) {
        	if (image.getImageUrl() != null) {
        		productForm.getProduct().addImage(image);
        	}
        }
        /*
        // product fields
        Class paramTypeString[] = {String.class};
        Class<Product> c = Product.class;
        for (ProductField productField : productForm.getProductFields()) {
        	boolean clearData = true;
        	for (Object catId: productForm.getProduct().getCatIds()) {
        		if (productField.getCategoryIds().contains("," + catId + ",")) {
        			clearData = false;
                	break;
        		}
        	}
        	if (clearData) {
				Method m = c.getMethod("setField" + productField.getId(), paramTypeString);
				Object arglist[] = { null };
				m.invoke(productForm.getProduct(), arglist);
        	}
        }*/
       
		if (productForm.isNewProduct()) {
			List<ProductField> fileds = productForm.getProductFields();
			//TODO Product Created by 
			this.webJaguar.insertProduct(productForm.getProduct(), null);
		} else {
			//TODO Product Modified by 
			this.webJaguar.updateProduct(productForm.getProduct(), null);			
		}

		return new ModelAndView(new RedirectView("account_products.jhtm"));
	}
	
	protected ModelAndView processCancel(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
     throws Exception {
		return new ModelAndView(new RedirectView("account_products.jhtm"));
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		
		ProductForm productForm = (ProductForm) command;
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	
       	Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		map.put("model", myModel);    	
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		switch (page) {
			case 0:
				Map <Integer, Object> categories = new HashMap<Integer, Object>();
				Map <Integer, Object> tree = new HashMap<Integer, Object>();
				for (int i=0; i<productForm.getParentIds().length; i++) {
					categories.put(i, this.webJaguar.getCategoryLinks(new Integer(productForm.getParentIds()[i].toString()), request));						
					tree.put(i, this.webJaguar.getCategoryTree(new Integer(productForm.getParentIds()[i].toString()), true, null));						
				}
				myModel.put("categories", categories);
				myModel.put("tree", tree);
				myModel.put("supplierDirectory", productForm.getProduct().getDefaultSupplierId() + "/");
				myModel.put("numOfCategories", productForm.getParentIds().length);
				break;
			case 1:
				if (productForm.getSupplierFolder()[0].exists()) {
					File file[] = productForm.getSupplierFolder()[0].listFiles();
					List<Map<String, Object>> galleryFiles = new ArrayList<Map<String, Object>>();
					for (int f=0; f<file.length; f++) {
						HashMap<String, Object> fileMap = new HashMap<String, Object>();
						fileMap.put("file", file[f]);
						fileMap.put("lastModified", new Date(file[f].lastModified()));
						long size = file[f].length();
						fileMap.put("size", size);
						if (productForm.getSelectedImage().getImageUrl() != null &&
								productForm.getSelectedImage().getImageUrl().equals(productForm.getProduct().getDefaultSupplierId()
								+ "/" + file[f].getName())) {
							fileMap.put("selected", true);
						} else {
							fileMap.put("selected", false);							
						}
						galleryFiles.add(fileMap);				
					}
					if (!galleryFiles.isEmpty()) {
						map.put("galleryFiles", galleryFiles);	
					}
					if (productForm.getSupplierFolder()[0].canWrite()) {
						map.put("canUpload", true);						
					}
				}
				break;
		}
		
		return map;
    } 
	
	protected Object formBackingObject(HttpServletRequest request) 
		throws Exception {
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
    	UserSession userSession = this.webJaguar.getUserSession(request);	

    	Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());
    	
    	Integer supplierId = customer.getSupplierId();
		if (supplierId == null) {
			Map<String, Object> myModel = new HashMap<String, Object>();
			myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
			myModel.put("message", "Invalid account.");
			ModelAndView modelAndView = new ModelAndView("frontend/error");
			modelAndView.addObject("model", myModel);
			throw new ModelAndViewDefiningException(modelAndView);
		}
		
		ProductForm productForm = new ProductForm();
		
		List<Object> catIds = new ArrayList<Object>();
		if ( request.getParameter("id") != null ) {
			Integer productId = new Integer(request.getParameter("id"));
			Product product = this.webJaguar.getProductById(productId, (Integer) gSiteConfig.get("gPRODUCT_IMAGES"), false, null);
			
			/*
			 * For when product can be added form front-end
			if (product == null || (supplierId != null && product.getDefaultSupplierId() != supplierId)) {
				Map<String, Object> myModel = new HashMap<String, Object>();
				myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
				myModel.put("message", "Product does not exist.");
				ModelAndView modelAndView = new ModelAndView("frontend/error");
				modelAndView.addObject("model", myModel);
				throw new ModelAndViewDefiningException(modelAndView);
			}
			*/
			productForm = new ProductForm(product);
			catIds.addAll(product.getCatIds());	
		}
		for (int i=catIds.size(); i<5; i++) {
			catIds.add("");				
		}
		productForm.setCategoryIds(catIds.toArray());
		List<Object> parentIds = new ArrayList<Object>();
		for (int i=0; i<catIds.size(); i++) {
			Integer catId = null;
			try {					
				catId = new Integer(catIds.get(i).toString());
			} catch (NumberFormatException e) {}
			Category category = this.webJaguar.getCategoryById(catId, "");
			if (category == null || category.getParent() == null) {
				parentIds.add(siteConfig.get("SUPPLIER_PRODUCT_CATEGORY").getValue());					
			} else {
				parentIds.add(category.getParent());					
			}
		}
		productForm.setParentIds(parentIds.toArray());
		productForm.getProduct().setDefaultSupplierId(supplierId);
        
		// price tiers
		productForm.setPriceTiers((Integer) gSiteConfig.get("gPRICE_TIERS"));
		
		productForm.setSkuPrefix(customer.getSupplierPrefix() + "-");
		if (productForm.isNewProduct()) {
			// check if supplier can still upload
			if (customer.getSupplierProdLimit() != null && customer.getSupplierProdLimit() <= this.webJaguar.productCountByCustomerSupplierId(supplierId)) {
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("account_products.jhtm")));
			}
			// supplier prefix
			productForm.getProduct().setSku(productForm.getSkuPrefix());			
		}
		
		// product fields
		List<ProductField> productFields = this.webJaguar.getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
		Iterator<ProductField> pfIter = productFields.iterator();
		while (pfIter.hasNext()) {
			ProductField productField = pfIter.next();
			if (!productField.isEnabled()) {
				pfIter.remove();
			}
		}
		productForm.setProductFields(productFields);
		
		// product images
		List<ProductImage> productImages = new ArrayList<ProductImage>();
		byte index = 1;
		if (productForm.getProduct().getImages() != null) {
			for (ProductImage image : productForm.getProduct().getImages()) {
				image.setIndex(index++);
				productImages.add(image);
			}
		}
		int numImages = 1;
		for (; index <= numImages; index++) {
			ProductImage image = new ProductImage();
			image.setIndex(index);
			productImages.add(image);
		}
		productForm.setProductImages(productImages);
		
		// supplier's image folder
		File baseFile = new File(getServletContext().getRealPath("/assets/Image/Product/"));
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if ( prop.get( "site.root" ) != null ) {
				baseFile = new File((String) prop.get("site.root"), "/assets/Image/Product/");
			}
		} catch (Exception e) {}
		File detailsbigDir = new File(baseFile, "detailsbig");
		File thumbDir = new File(baseFile, "thumb");
		File tempDir = new File(baseFile, "temp"); // temporary folder		

		// create supplier's folder
		detailsbigDir = new File(detailsbigDir, productForm.getProduct().getDefaultSupplierId() + "");
		if (!detailsbigDir.exists()) {
			detailsbigDir.mkdir();
		}
		thumbDir = new File(thumbDir, productForm.getProduct().getDefaultSupplierId() + "");
		if (!thumbDir.exists()) {
			thumbDir.mkdir();
		}		
		tempDir = new File(tempDir, productForm.getProduct().getDefaultSupplierId() + "");
		if (!tempDir.exists()) {
			tempDir.mkdir();
		}
		File folders[] = {detailsbigDir, thumbDir, tempDir};
		productForm.setSupplierFolder(folders);
		
		return productForm;
	}

	
	protected void validatePage(Object command, Errors errors, int page) {
		ProductFormValidator validator = (ProductFormValidator) getValidator();
		ProductForm productForm = (ProductForm) command;
		
		switch (page) {
			case 0:
				// validate 
				validator.validate(command, errors);
				
				// check sku
				productForm.getProduct().setSku(productForm.getProduct().getSku().trim());
				
				Integer id = this.webJaguar.getProductIdBySku(productForm.getProduct().getSku());
				if (id != null) {
					if ( productForm.isNewProduct() || id.compareTo(productForm.getProduct().getId()) != 0) {
						// duplicate
						errors.rejectValue( "product.sku", "SKU_ALREADY_EXISTS", "An item using this SKU already exists." );				
					}
				}
				break;
		}
	}
	
	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		ProductForm productForm = (ProductForm) command;

		switch (page) {
			case 0:
				int selected_image = ServletRequestUtils.getIntParameter(request, "selected_image", -1);
				if (selected_image > -1) {
					productForm.setSelectedImage(productForm.getProductImages().get(selected_image));
				}
				if (request.getParameter("_remove") != null) {
					productForm.getSelectedImage().setImageUrl(null);
				}
				for (int i=0; i<productForm.getParentIds().length; i++) {
					productForm.getCategoryIds()[i] = ServletRequestUtils.getStringParameter(request, "cat" + i + "_categoryIds", "");
				}
				break;
			case 1:
				File files[] = productForm.getSupplierFolder()[0].listFiles();
				
				String selectedImages[] = request.getParameterValues("__selected_index");
				
				if(selectedImages != null && selectedImages.length > 0 && request.getParameter("_target0") != null){
					List<ProductImage> imageList = new ArrayList<ProductImage>();
					for(int i=0; i <selectedImages.length; i++ ) {
						ProductImage image = new ProductImage();
						image.setImageUrl(productForm.getProduct().getDefaultSupplierId() + "/" + files[Integer.parseInt(selectedImages[i])].getName());
						imageList.add(image);
					}
					productForm.setProductImages(imageList);
				}
				if (productForm.getSupplierFolder()[0].canWrite() && productForm.getSupplierFolder()[1].canWrite() && productForm.getSupplierFolder()[2].canWrite()) {
					
					// delete
					if (request.getParameter("_delete") != null) {
						for(int i=0; i <selectedImages.length; i++ ) {
							files[Integer.parseInt(selectedImages[i])].delete();
							new File(productForm.getSupplierFolder()[1], files[Integer.parseInt(selectedImages[i])].getName()).delete();
							for (ProductImage image : productForm.getProductImages()) {
								if (image.getImageUrl() != null && image.getImageUrl().equals(productForm.getProduct().getDefaultSupplierId()
										+ "/" + files[Integer.parseInt(selectedImages[i])].getName())) {
									image.setImageUrl(null);
					        	}
					        }
						}
					}
					
					if (request.getParameter("_upload") != null) {
						// upload image
						MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
						MultipartFile image_file = multipartRequest.getFile("image_file");
						
						Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
						int thumbnail = Integer.parseInt(siteConfig.get("THUMBNAIL_RESIZE").getValue());
						
						if (image_file != null && !image_file.isEmpty() &&
								(image_file.getContentType().equalsIgnoreCase("image/gif") || image_file.getContentType().equalsIgnoreCase("image/pjpeg")
										|| image_file.getContentType().equalsIgnoreCase("image/jpeg"))) {
							String fileName = image_file.getOriginalFilename();
							File detailsbig = new File(productForm.getSupplierFolder()[0], fileName);
							File thumb = new File(productForm.getSupplierFolder()[1], fileName);
							File temp = new File(productForm.getSupplierFolder()[2], fileName);
							try {
								// resize image in temporary folder
								image_file.transferTo(temp);
								boolean success = this.webJaguar.resizeImage(temp.getAbsolutePath(), temp.getAbsolutePath(), 800, 600, image_file.getContentType());
								// if success, copy resized image to detailsbig folder, and then create thumbnail
								if (success) {
									BufferedImage img = ImageIO.read( temp );
									if (img.getHeight() <= thumbnail && img.getWidth() <= thumbnail) {
										copy(temp, thumb);
									}
									else {
										this.webJaguar.resizeImage( temp.getAbsolutePath(), thumb.getAbsolutePath(), thumbnail, thumbnail, image_file.getContentType() );
									}
									copy(temp, detailsbig);
								} else {
									request.setAttribute("message", "imagefile.imagej.exception");
								}
								// delete the image in temporary folder
								temp.delete();
							} catch (IOException e) {
								// do nothing
							}						
						} else {
							request.setAttribute("message", "imagefile.invalid");
						}
					}
				}
				break;
		}		
	}
	
	// Copies src file to dst file.
	private void copy(File src, File dst) throws IOException {
		InputStream in = new FileInputStream(src);
		OutputStream out = new FileOutputStream(dst);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len;
		while ((len = in.read( buf )) > 0) {
			out.write( buf, 0, len );
		}
		in.close();
		out.close();
	}
}
