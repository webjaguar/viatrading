/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class ListAddressesController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("account_address.jhtm"));				
		}		

		if (request.getParameter("deletedId") != null) {
			Address address = this.webJaguar.getAddressById(Integer.parseInt(request.getParameter("deletedId")));
			this.webJaguar.deleteAddress(address);
		}
    	UserSession userSession = this.webJaguar.getUserSession(request);	
		    	
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		// default address
		myModel.put("defaultAddress", this.webJaguar.getDefaultAddressByUserid(userSession.getUserid()));
		
		// customer information
		if (userSession != null) {
			myModel.put("customer", (Customer) request.getAttribute("sessionCustomer"));
		}
			
		// addresses
		PagedListHolder addressList = 
			new PagedListHolder(this.webJaguar.getAddressListByUserid( userSession.getUserid() ));
		addressList.setPageSize(10);
		String page = request.getParameter("page");
		if (page != null) {
			addressList.setPage(Integer.parseInt(page)-1);
		}
		myModel.put("addresses", addressList);		
		
		Layout layout = null;
		layout = this.webJaguar.getSystemLayout("manageStoredAddress", request.getHeader("host"), request);
		if(layout != null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#pageName#", "My Address"));
		}
		if(layout == null){
			layout = (Layout) request.getAttribute("layout");
		}
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		request.setAttribute("layout", layout);
		//myModel.put("myAddressLayout", this.webJaguar.getSystemLayout("myaddress", request.getHeader("host"), request));
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/account/addresses", "model", myModel);
		}
		return new ModelAndView("frontend/account/addresses", "model", myModel);    	
    }

}
