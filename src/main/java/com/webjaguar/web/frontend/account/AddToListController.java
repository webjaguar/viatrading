/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class AddToListController implements Controller {

	private WebJaguarFacade webJaguar;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {

    	UserSession userSession = this.webJaguar.getUserSession(request);
    	String ids[] = request.getParameterValues("product.id");
    	if (ids != null) {
    		this.webJaguar.addToList(ids, userSession.getUserid());
    	}
    	
		Layout layout = (Layout) request.getAttribute("layout");
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Edit Information"));

		return new ModelAndView(new RedirectView("viewList.jhtm"));
	}
    
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

}
