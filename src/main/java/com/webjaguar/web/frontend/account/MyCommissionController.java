/*
 * Copyright 2005, 2007 Advanced E-Media Solutions 
 * @author Shahin Naji
 * @since 07.05.2007
 */

package com.webjaguar.web.frontend.account;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CommissionReport;
import com.webjaguar.model.CommissionSearch;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class MyCommissionController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private String successView;
	public void setSuccessView(String successView) { this.successView = successView; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {
    	UserSession userSession = this.webJaguar.getUserSession(request);	
    	Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
    	Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
    	
    	int level = org.springframework.web.bind.ServletRequestUtils.getIntParameter( request, "l", 1 );
    	Integer affiliateLevel = (Integer) gSiteConfig.get( "gAFFILIATE" );
    	
    	// sort by last name
    	CommissionSearch search = getCommissionSearch( request, userSession.getUserid(), level );

    	Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		if ( userSession != null && affiliateLevel > 0 )
		{
			List<CommissionReport> commissionList = new Vector();
			if ( level == 1 ) {
				commissionList = this.webJaguar.getCommissionsByUserId( userSession.getUserid(), level, search );
			}
			else if ( level == 2 && affiliateLevel == 2 ) {
				commissionList = this.webJaguar.getCommissionsByUserId( userSession.getUserid(), level, search );
			}
			PagedListHolder myCommissionList = new PagedListHolder(commissionList);
			myCommissionList.setPageSize(20);
			myCommissionList.setPage( search.getPage() - 1 );
			
			myModel.put("level", level);		
			myModel.put("orders", myCommissionList);
			
			double sum=0;
			int month=0;
			int newMonth=0;
		
			HashMap<String,Double> map= new HashMap<String,Double>();
			SimpleDateFormat sdf= new SimpleDateFormat("yyyyMM");
			
			for(int i=0;i<commissionList.size();i++){
				Date date = commissionList.get(i).getOrderDate();
				newMonth=Integer.parseInt(sdf.format(date));
				if((newMonth==month)&&(i<commissionList.size())){
					sum=sum+commissionList.get(i).getMyCommission();   
					if(i==commissionList.size()-1){
						map.put(sdf.format(date),sum);
					}
				}
				if((newMonth!=month)&&(i==0)){
					sum=sum+commissionList.get(i).getMyCommission();
					month=newMonth;
					if(commissionList.size()==1){
						map.put(sdf.format(date),sum);
					}
				}
				if((newMonth!=month)&&(i!=0)&&(i<commissionList.size())){
					map.put(sdf.format(commissionList.get(i-1).getOrderDate()),sum);
					sum=commissionList.get(i).getMyCommission();
					month=newMonth;
					if(i==commissionList.size()-1){
						map.put(sdf.format(date),sum);
					}
				}
			 }
			myModel.put("map", map);
		}
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/account/commissions", "model", myModel);    	
		}
		return new ModelAndView(this.successView, "model", myModel);    	
    }
    
    private CommissionSearch getCommissionSearch(HttpServletRequest request, Integer userId, Integer level) {
		CommissionSearch commissionSearch = new  CommissionSearch();
		commissionSearch.setSort( "date_ordered DESC" );   			

		switch ( level ) {
			case 0:
				commissionSearch.setLevel( "(orders.affiliate_id_level1 = " + userId + " or orders.affiliate_id_level2 = " + userId + ")" );
				break;
			case 2:
				commissionSearch.setLevel( "orders.affiliate_id_level2 = " + userId );
				break;
			default:
				commissionSearch.setLevel( "orders.affiliate_id_level1 = " + userId );
			    break;
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				commissionSearch.setPage( 1 );
			} else {
				commissionSearch.setPage( page );				
			}
		}

		return commissionSearch;
    }
}