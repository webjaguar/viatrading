/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.UserSession;

public class LoginAsCustomerController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	if (!siteConfig.get( "SUBACCOUNT_LOGINAS_FRONTEND" ).getValue().equals( "true" ) ) {
			return new ModelAndView(new RedirectView("account.jhtm"));
		}
    	String token = org.springframework.web.bind.ServletRequestUtils.getStringParameter( request, "cid", null );
    	Customer customer = this.webJaguar.getCustomerByToken( token );
    	if ( customer != null )
    	{
    		//  set user session
			UserSession userSession = new UserSession();
			userSession.setUsername(customer.getUsername());
			userSession.setFirstName( customer.getAddress().getFirstName() );
			userSession.setLastName( customer.getAddress().getLastName() );
			userSession.setUserid(customer.getId());
			this.webJaguar.setUserSession(request, userSession);
    	}
        return new ModelAndView(new RedirectView(request.getContextPath() + "/account.jhtm"));    	
    }
}
