package com.webjaguar.web.frontend.account;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CustomerBudgetPartner;
import com.webjaguar.model.UserSession;

public class PartnersReportController implements Controller{
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	 	
		UserSession userSession = this.webJaguar.getUserSession(request);	
    	Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		Map<String, Object> model = new HashMap<String, Object>();

    	CustomerBudgetPartner partner = new CustomerBudgetPartner();
    	partner.setUserId(userSession.getUserid());
		
		List<CustomerBudgetPartner> partnersList = this.webJaguar.getCustomerPartnerHistory(partner);
		
		if(partnersList != null) {
			PagedListHolder partnersListHolder = new PagedListHolder(partnersList);
			partnersListHolder.setPageSize(20);
			String page = request.getParameter("page");
			if (page != null) {
				partnersListHolder.setPage(Integer.parseInt(page)-1);
			}
			
			model.put("partnersList", partnersListHolder);
		}
		
		return new ModelAndView("frontend/account/partnerReport", "model", model);    	
	}


}
