/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.21.2009
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Brand;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class ReportByBrandController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {	

    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
    	ModelAndView modelAndView = new ModelAndView("frontend/account/reportByBrand", "model", model);

    	UserSession userSession = this.webJaguar.getUserSession(request);
    	Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());
    	
    	String skuPrefix = ServletRequestUtils.getStringParameter(request, "brand", "");
    	String sku = ServletRequestUtils.getStringParameter(request, "sku", "");
    	
    	Map<String, Brand> brandMap = this.webJaguar.getBrandMap();
		if ((Boolean) gSiteConfig.get("gBUDGET_BRAND") 
				&& brandMap.containsKey(skuPrefix)
				&& customer.getBrandReport() != null && customer.getBrandReport().contains("," + skuPrefix + ",")) {
			model.put("brand", brandMap.get(skuPrefix));	
			if (sku.trim().length() == 0) {
				model.put("products", this.webJaguar.getProductListByBrand(brandMap.get(skuPrefix)));
				model.put("orderReportMap", this.webJaguar.getOrderReportByBrand(brandMap.get(skuPrefix)));					
			} else {
				model.put("lineItems", this.webJaguar.getOrderReportByBrand(brandMap.get(skuPrefix), sku));
				modelAndView.setViewName("frontend/account/reportByBrandSku");
			}
		}
		
		// layout
		Layout layout = (Layout) request.getAttribute("layout");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		}
		
		return modelAndView;   	
    }

}
