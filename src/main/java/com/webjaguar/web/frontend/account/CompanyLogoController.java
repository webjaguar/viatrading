/* Copyright 2005, 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 04.15.2009
 */

package com.webjaguar.web.frontend.account;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.UserSession;

public class CompanyLogoController extends SimpleFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private File baseFile;
	
	public CompanyLogoController()
	{
		setSessionForm( true );
		setCommandName( "companyLogoForm" );
		setCommandClass( CompanyLogoForm.class );
		setFormView( "frontend/account/companyLogo" );
		setSuccessView( "account.jhtm" );
		this.setValidateOnBinding( true );
	}

	protected java.util.Map referenceData(HttpServletRequest request, Object command, Errors errors)
	{
		CompanyLogoForm companyLogoForm = (CompanyLogoForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
	
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		baseFile = new File( getServletContext().getRealPath( "/assets/Image/" ) );
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				baseFile = new File( (String) prop.get( "site.root" ) + "/assets/Image/" );
			}
		}
		catch ( Exception e )
		{}
		
		File companyLogoDir = new File(baseFile, "CompanyLogo");
		if (!companyLogoDir.exists()) {
			companyLogoDir.mkdir();
		}

		File companyLogoImage = new File(companyLogoDir, "logo_" + companyLogoForm.getUserId().toString() + ".gif");
		if (companyLogoImage.exists()) {
			model.put( "companyLogo", "logo_" + companyLogoForm.getUserId() + ".gif");
		}
		model.put( "randomNum", new Random().nextInt( 1000 ) );
		return model;
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws ServletException
	{
		CompanyLogoForm companyLogoForm = (CompanyLogoForm) command;
		
		// check if cancel button was pressed
		if ( request.getParameter( "_cancel" ) != null ) {
			return new ModelAndView( new RedirectView( getSuccessView() ) );
		}
		
		MultipartFile companyLogo = null;
		
		//save logo
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		companyLogo = multipartRequest.getFile( "company_logo" );
		File newFile = new File( baseFile, "CompanyLogo/logo_" + companyLogoForm.getUserId() + ".gif" );
		newFile.delete();
		if ( request.getParameter( "_delete" ) == null) {
			try {
				companyLogo.transferTo( newFile );
			} catch ( IOException e ) {
				//
			}
		}

		return new ModelAndView( new RedirectView( getSuccessView() ) );
	}
	
	protected boolean suppressBinding(HttpServletRequest request)
	{
		if ( request.getParameter( "_delete" ) != null || request.getParameter( "_cancel" ) != null )
		{
			return true;
		}
		return false;
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		CompanyLogoForm companyLogoForm = (CompanyLogoForm) command;

		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		MultipartFile newCompanyLogo = multipartRequest.getFile( "company_logo" );
		
		if ( newCompanyLogo != null && !newCompanyLogo.isEmpty() ) {
			if ( newCompanyLogo.getContentType().equalsIgnoreCase( "image/gif" ) || newCompanyLogo.getContentType().equalsIgnoreCase( "image/jpeg" ) ) {
				File tempFile = new File( baseFile, "CompanyLogo/temp" + companyLogoForm.getUserId() + ".gif" );
				try {
					newCompanyLogo.transferTo( tempFile );
					BufferedImage img = ImageIO.read( tempFile );
					if ( img.getHeight() != 200 && img.getWidth() != 200 ) {
						errors.rejectValue( "logoName", "Size should be 200px x 200px", "size should be 200px x 200px" );
					}
					tempFile.delete();
				} catch ( IOException e ) {
					//
				}
			} else {
				errors.rejectValue( "logoName", "Only use gif or jpg image", "Only use gif or jpg image" );
			}
		} else {
			errors.rejectValue( "logoName", "First Browse an image.", "First Browse an image." );
		}
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception
	{
		CompanyLogoForm companyLogoForm = new CompanyLogoForm();

		UserSession userSession = this.webJaguar.getUserSession(request);
		Customer user = this.webJaguar.getCustomerById( userSession.getUserid() );
		if (user != null && user.getSupplierId() != null) {
			companyLogoForm.setUserId( user.getId() );
		} else {
			throw new ModelAndViewDefiningException( new ModelAndView( new RedirectView("account.jhtm")));
		}
		return companyLogoForm;

	}
	
	public class CompanyLogoForm {

	    private String logoName;
	    private Integer userId;
		public String getLogoName()
		{
			return logoName;
		}
		public void setLogoName(String logoName)
		{
			this.logoName = logoName;
		}
		public Integer getUserId()
		{
			return userId;
		}
		public void setUserId(Integer userId)
		{
			this.userId = userId;
		}
	}
}