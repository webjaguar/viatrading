/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.03.2008
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Brand;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.UserSession;

public class ReportByBrandsController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {	

    	UserSession userSession = this.webJaguar.getUserSession(request);	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		int custId = userSession.getUserid();
		if ((Boolean) gSiteConfig.get( "gSUB_ACCOUNTS" ) && request.getParameter( "id" ) != null
				&& !this.webJaguar.getCustomerById(userSession.getUserid()).isHideSubAccts()) {
			custId = ServletRequestUtils.getIntParameter(request, "id", userSession.getUserid());
			boolean valid = false;
			for (Customer node: this.webJaguar.getFamilyTree( custId, "up" )) {
				// check if a valid sub-account
				if (node.getId().compareTo(userSession.getUserid()) == 0) {
					valid = true;
					break;
				}
			}
			if (valid) {
				model.put("subAccount", this.webJaguar.getCustomerById(custId));
			} else {
				custId = userSession.getUserid();
			}
		}		
		
		// current year
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		model.put("currentYear", currentYear);
		
		OrderSearch search = getSearch(request, currentYear);
		search.setUserId(custId);	
		model.put("orderSearch", search);
		
		// orders
		List<Order> orders = this.webJaguar.getInvoiceExportList(search, null);
		model.put("orders", orders);	
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		List<Brand> brands = this.webJaguar.getBrands(custId);
		Iterator<Brand> iter = brands.iterator();
		while (iter.hasNext()) {
			Brand brand = iter.next();
			brand.setTotal(this.webJaguar.getOrderTotalByBrand(brand, custId, search.getYear()));
			if (brand.getTotal() == null) {
				iter.remove();
			}
		}
		model.put("brands", brands);
		model.put("brandsMap", getSubTotalPerBrand(orders, brands));
		setColspans(brands);
		
		return new ModelAndView("frontend/account/reportByBrands", "model", model);    	
    }
	
	private OrderSearch getSearch(HttpServletRequest request, int currentYear) {
		OrderSearch search = new OrderSearch();
			
		// year
		search.setYear(ServletRequestUtils.getIntParameter(request, "year", currentYear));
		
		// status (all orders not cancelled)
		search.setStatusTemp("status not like '%x%'");
			
		return search;
	}
	
	private Map<Integer, Map<Brand, Double>> getSubTotalPerBrand(List<Order> orders, List<Brand> brands) {
		Map<Integer, Map<Brand, Double>> results = new HashMap<Integer, Map<Brand, Double>>();
		
		Iterator<Order> iter = orders.iterator();
		while (iter.hasNext()) {
			Order order = iter.next();
			Map<Brand, Double> map = new HashMap<Brand, Double>();
			nextItem: for (LineItem lineItem: order.getLineItems()) {
				String sku = lineItem.getProduct().getSku();
				for (Brand brand: brands) {
					if (sku != null && sku.toLowerCase().startsWith(brand.getSkuPrefix().toLowerCase()) && lineItem.getTotalPrice() != null) {
						if (brand.getOptionName() == null) {
							if (map.get(brand) == null) {
								map.put(brand, 0.0);
							}
							map.put(brand, map.get(brand) + lineItem.getTotalPrice());	
							continue nextItem;
						} else if (lineItem.getProductAttributes() != null) {
							for (ProductAttribute pa: lineItem.getProductAttributes()) {
								if (pa.getOptionName().equalsIgnoreCase(brand.getOptionName()) && pa.getValueName().equalsIgnoreCase(brand.getValueName())) {
									if (map.get(brand) == null) {
										map.put(brand, 0.0);
									}
									map.put(brand, map.get(brand) + lineItem.getTotalPrice());		
									continue nextItem;
								}
							}
						}						
					}
				}
			}
			if (map.isEmpty()) {
				iter.remove();
			} else {
				results.put(order.getOrderId(), map);				
			}
		}
		
		return results;
	}
	
	private void setColspans(List<Brand> brands) {
		Brand currentBrand = null;
		Brand currentOption = null;
		for (Brand brand: brands) {
			if (brand.getOptionName() != null) {
				if (currentBrand != null && currentBrand.getName().equals(brand.getName())) {
					currentBrand.setColspan1(currentBrand.getColspan1()+1);
					if (currentOption.getOptionName().equals(brand.getOptionName())) {
						currentOption.setColspan2(currentOption.getColspan2()+1);
					} else { 
						currentOption = brand;
						currentOption.setColspan2(1);
					}
				} else { 
					currentBrand = brand;
					currentOption = brand;
					currentBrand.setColspan1(1);
					currentOption.setColspan2(1);
				}
			}
		}
	}

}
