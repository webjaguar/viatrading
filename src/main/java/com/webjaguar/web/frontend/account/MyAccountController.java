/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.EventSearch;
import com.webjaguar.model.Layout;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SubscriptionSearch;
import com.webjaguar.model.Ticket;
import com.webjaguar.model.TicketSearch;
import com.webjaguar.model.UserSession;
import com.webjaguar.thirdparty.evergreen.EvergreenApi;

public class MyAccountController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {

    	UserSession userSession = this.webJaguar.getUserSession(request);	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
    	Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		EventSearch eventSearch = new EventSearch();
		
		if ( userSession != null )
		{
			Customer customer = (Customer) request.getAttribute("sessionCustomer");
			if(customer.isUpdateNewInformation() && request.getParameter("updateInformation") == null){
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("account_information_edit.jhtm?forwardAction=account.jhtm")));
			}
		    model.put("customer", customer);
		    if(customer!=null){
		    	model.put("id",customer.getId());
		    }
		    eventSearch.setEmail(customer.getUsername());
		    model.put("protectedAccessLinkStatus", protectedAccessLink(customer.getProtectedAccess()));
		    
			if ( (Integer) gSiteConfig.get( "gAFFILIATE" ) > 0 ) {
				model.put( "affiliateCustomer", customer.getIsAffiliate() );
				model.put( "level1", this.webJaguar.getCommissionTotalByUserId( customer.getId(), 1 ) );
			    if ( (Integer) gSiteConfig.get( "gAFFILIATE" ) == 2 ) { 
			    	model.put( "level2", this.webJaguar.getCommissionTotalByUserId( customer.getId(), 2 ) );
			    }
			}
			if (this.webJaguar.getFamilyTree( customer.getId(), "down" ).size() > 1 && !customer.isHideSubAccts()) {
				model.put( "hasSubAccounts", true );
			}
			if (!siteConfig.get( "CUSTOMER_CUSTOM_NOTE_1" ).getValue().equals( "" ) &&
					customer.getCustomNote1() != null && !customer.getCustomNote1().trim().equals( "" )) {
				model.put( "hasCustomNote1", true );
			}
			if (!siteConfig.get( "CUSTOMER_CUSTOM_NOTE_2" ).getValue().equals( "" ) &&
					customer.getCustomNote2() != null && !customer.getCustomNote2().trim().equals( "" )) {
				model.put( "hasCustomNote2", true );
			}
			if (!siteConfig.get( "CUSTOMER_CUSTOM_NOTE_3" ).getValue().equals( "" ) &&
					customer.getCustomNote3() != null && !customer.getCustomNote3().trim().equals( "" )) {
				model.put( "hasCustomNote3", true );
			}
			
			// subscriptions
			SubscriptionSearch search = new SubscriptionSearch();
			search.setUserId(userSession.getUserid());
			if (!this.webJaguar.getSubscriptions(search).isEmpty()) {
				model.put( "hasSubscriptions", true );				
			}
			
			if ( (Boolean) gSiteConfig.get( "gSALES_REP" ) ) {
				SalesRep salesRep = this.webJaguar.getSalesRepById( customer.getSalesRepId() );
				model.put( "salesRep", salesRep );
			}
			
			if ((Boolean) gSiteConfig.get("gBUDGET_BRAND")) {
				model.put("brands", this.webJaguar.getBrandMap().values());
			}
			
			if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN") && customer.getAccountNumber() != null && customer.getAccountNumber().trim().length() > 0) {
				EvergreenApi evergreen = new EvergreenApi();
				model.put("evergreenClientCredit", evergreen.GetClientCredit(customer.getAccountNumber()));
			}
		}
		
		Layout layout = null;
		layout = this.webJaguar.getSystemLayout("myaccount", request.getHeader("host"), request);
		if(layout != null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#pageName#", "My Account"));
		}
		if(layout == null){
			layout = (Layout) request.getAttribute("layout");
		}
		
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		request.setAttribute("layout", layout);
		model.put("pageName", "accountPage"); 
		
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		
		TicketSearch ticketSearch = new TicketSearch();   
		List<Ticket> tickets = this.webJaguar.getTicketsListByUserid(userSession.getUserid(), ticketSearch);
		model.put("ticketCount", tickets.size());
		
		int count = this.webJaguar.getEventMemberListCount(eventSearch);
		model.put("eventCount", count);

		switch (ServletRequestUtils.getIntParameter( request, "note", -1)) {
			case 1:
				if (model.containsKey("hasCustomNote1")) {
					return new ModelAndView("frontend/account/note", "model", model);					
				}
				break;
			case 2:
				if (model.containsKey("hasCustomNote2")) {
					return new ModelAndView("frontend/account/note", "model", model);					
				}
				break;
			case 3:
				if (model.containsKey("hasCustomNote3")) {
					return new ModelAndView("frontend/account/note", "model", model);					
				}
				break;
		}
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/account/account", "model", model);
		}
		
		return new ModelAndView("frontend/account", "model", model);    	
    }

    public boolean protectedAccessLink(String protectedAccess){
    	int val = 1;
    	int sum = 0;
    	for (int i=0; i< protectedAccess.length(); i++) {
    		val = val * 2;
    		if(protectedAccess.charAt(protectedAccess.length()-i-1)=='1'){
    			sum = sum + val;
    		}
    	}
    	return ((sum & 512) == 512);
    }
	
}
