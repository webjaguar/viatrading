/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 02.05.2009
 */

package com.webjaguar.web.frontend.account;

import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Brand;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.domain.Constants;

public class ExportByBrandsController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws Exception {	
		
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
    	Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
    	
    	UserSession userSession = this.webJaguar.getUserSession(request);
    	Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());
    	
    	if ((Boolean) gSiteConfig.get("gBUDGET_BRAND") && this.webJaguar.getFamilyTree(customer.getId(), "down").size() > 1 && !customer.isHideSubAccts()) {
			// is a manager, only managers can export products

    		List<Brand> brands = this.webJaguar.getBrands(null);
    		
           	String protectedAccess = customer.getProtectedAccess();

    		// check if protected feature is enabled
    		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
    		if (protectedLevels == 0) {
    			protectedAccess = Constants.FULL_PROTECTED_ACCESS;
    		}
        	
    		File tempFolder = new File(getServletContext().getRealPath("temp"));
    		if (!tempFolder.exists()) {
    			tempFolder.mkdir();
    		}
    		File exportFolder = new File(tempFolder, "exportFiles");
    		if (!exportFolder.exists()) {
    			exportFolder.mkdir();
    		} else {
    			// clear old files
    			for (File file: exportFolder.listFiles()) {
    				if (file.getName().startsWith("brandExport" + userSession.getUserid()) 
    						& file.getName().endsWith(".csv")) {
    					file.delete();
    				}
    			}
    		}
        	
    		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
    		
        	File exportFile = new File(exportFolder, "brandExport" + userSession.getUserid() + 
        			dateFormatter.format(new Date()) + ".csv");
        	CSVWriter writer = new CSVWriter(new FileWriter(exportFile));
        	List<String> line = new ArrayList<String>();
        	
        	line.add("SKU");
        	line.add("Brand");
        	line.add("Description");
        	line.add("Inventory");
        	line.add("Price");
        	
        	String[] lines = new String[line.size()];
        	writer.writeNext(line.toArray(lines));
        	
        	for (Product product: this.webJaguar.getProductExportByBrands(protectedAccess)) {
        		line = new ArrayList<String>();
        		for (Brand brand: brands) {
    				if (product.getSku().toLowerCase().startsWith(brand.getSkuPrefix().toLowerCase())) {
    	            	line.add(product.getSku());
    	            	line.add(brand.getName());
    	            	line.add(product.getName());
    	            	if (product.getInventory() != null) {
    	                	line.add(product.getInventory().toString());        		
    	            	} else {
    	                	line.add("");        		
    	            	}
    	            	if (product.getPrice1() != null) {
    	                	line.add(product.getPrice1().toString());        		
    	            	} else {
    	                	line.add("");        		
    	            	}
    	            	lines = new String[line.size()];
    	            	writer.writeNext(line.toArray(lines));
    					break;
    				}
        		}
        	}
        	
        	writer.close();
        	
        	model.put("exportedFile", exportFile.getName());    		
		}
    	
    	// layout
		Layout layout = (Layout) request.getAttribute("layout");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		
		return new ModelAndView("frontend/account/exportByBrands", "model", model);
    } 
   
}
