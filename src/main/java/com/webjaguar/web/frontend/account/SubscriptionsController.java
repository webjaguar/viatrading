/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.31.2008
 */

package com.webjaguar.web.frontend.account;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.SubscriptionSearch;
import com.webjaguar.model.UserSession;

public class SubscriptionsController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {	

    	UserSession userSession = this.webJaguar.getUserSession(request);	
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		SubscriptionSearch search = getSearch(request);
		search.setUserId(userSession.getUserid());
		model.put("subscriptionSearch", search);
		
		// orders
		PagedListHolder list = 
			new PagedListHolder(this.webJaguar.getSubscriptions(search));
		list.setPageSize(search.getPageSize());
		list.setPage(search.getPage()-1);
		model.put("list", list);
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		// multi store
		model.put("subscriptionLayout", this.webJaguar.getSystemLayout("subscription", request.getHeader("host"), request));
		
		return new ModelAndView("frontend/account/subscriptions", "model", model);    	
    }

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}
	
	private SubscriptionSearch getSearch(HttpServletRequest request) {
		SubscriptionSearch search = new SubscriptionSearch();
		
		// enabled
		if (request.getParameter("enabled") != null) {
			search.setEnabled(ServletRequestUtils.getStringParameter( request, "enabled", "" ));
		}
		
		// code
		if (request.getParameter("code") != null) {
			search.setCode(ServletRequestUtils.getStringParameter( request, "code", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}	
		return search;
	}

}
