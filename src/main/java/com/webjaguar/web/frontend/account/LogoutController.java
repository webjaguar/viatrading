/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.dao.CustomerDao;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.domain.Utilities;

public class LogoutController implements Controller {

	private CustomerDao customerDao;
	public void setCustomerDao(CustomerDao customerDao) { this.customerDao = customerDao; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
		if (Integer.parseInt(siteConfig.get("COOKIE_AGE_KEEP_ME_LOGIN").getValue()) != -1) {
			// remove KeepMeLogin Cookie and change the Token
			Utilities.removeCookie(response, "UKWJ", null);
			UserSession userSession = (UserSession) request.getAttribute( "userSession" );
			if (userSession != null) {
				this.customerDao.changeCustomerToken(userSession.getUserid());
			}
		}
		
		//Remove customer ID from cookies	
		Utilities.removeCookie(response, "CI", null);
		request.getSession().invalidate();
		String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss",Locale.US).format(new Date());
		//Viatrading wants to add a parameter to logout page as they dont want cached home page to appear every time they logout
		String logoutUrl =  siteConfig.get("SITE_URL").getValue();
		return new ModelAndView(new RedirectView(logoutUrl+"?loggedOut"+timeStamp));
	}
}