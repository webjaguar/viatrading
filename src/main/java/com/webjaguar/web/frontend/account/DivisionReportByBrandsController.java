/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 02.04.2009
 */

package com.webjaguar.web.frontend.account;

import java.io.File;
import java.io.FileWriter;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import au.com.bytecode.opencsv.CSVWriter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Brand;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class DivisionReportByBrandsController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws Exception {	

    	UserSession userSession = this.webJaguar.getUserSession(request);	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		int custId = userSession.getUserid();
		if ((Boolean) gSiteConfig.get( "gSUB_ACCOUNTS" ) && request.getParameter( "id" ) != null
				&& !this.webJaguar.getCustomerById(userSession.getUserid()).isHideSubAccts()) {
			custId = ServletRequestUtils.getIntParameter(request, "id", userSession.getUserid());
			boolean valid = false;
			for (Customer node: this.webJaguar.getFamilyTree( custId, "up" )) {
				// check if a valid sub-account
				if (node.getId().compareTo(userSession.getUserid()) == 0) {
					valid = true;
					break;
				}
			}
			if (valid) {
				model.put("subAccount", this.webJaguar.getCustomerById(custId));
			} else {
				custId = userSession.getUserid();
			}
		}		

		// current year
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		model.put("currentYear", currentYear);
		
		int year = ServletRequestUtils.getIntParameter(request, "year", currentYear);
		model.put("year", year);
		
		// sub accounts
		CustomerSearch search = new CustomerSearch();
		search.setParent(custId);
		List<Customer> subAccounts = this.webJaguar.getCustomerListWithDate(search);
		model.put("subAccounts", subAccounts);
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		// get brand per sub account
		List<Brand> brands = this.webJaguar.getBrands(null);
		Map<Customer, List<Brand>> subAccountMap = new HashMap<Customer, List<Brand>>();
		for (Customer subAcct: subAccounts) {
			List<Brand> subAcctBrands = this.webJaguar.getBrands(subAcct.getId());
			int index = 0;
			for (Brand  brand: subAcctBrands) {
				brand.setTotal(this.webJaguar.getOrderTotalByBrand(brand, subAcct.getId(), year));
				if (brand.getTotal() != null) {
					if (brands.get(index).getTotal() == null) {
						brands.get(index).setTotal(brand.getTotal());
					} else {
						brands.get(index).setTotal(brands.get(index).getTotal() + brand.getTotal());						
					}
				}
				index++;
			}
			subAccountMap.put(subAcct, subAcctBrands);
		}
		// clean up list, remove brands not ordered
		if (brands.size() > 0) {
			for (Customer subAcct: subAccounts) {
				for (int i=brands.size()-1; i>=0; i--) {
					if (brands.get(i).getTotal() == null) {
						subAccountMap.get(subAcct).remove(i);		
					}
				}
			}	
			for (int i=brands.size()-1; i>=0; i--) {
				if (brands.get(i).getTotal() == null) {
					brands.remove(i);		
				}
			}
		}

		model.put("subAccountMap", subAccountMap);
		model.put("brands", brands);
		setColspans(brands);
		model.put("exportedFile", saveToFile(brands, subAccounts, subAccountMap, userSession.getUserid(), year, currentYear)); 
		
		return new ModelAndView("frontend/account/dReportByBrands", "model", model);    	
    }
	
	private void setColspans(List<Brand> brands) {
		Brand currentBrand = null;
		Brand currentOption = null;
		for (Brand brand: brands) {
			if (brand.getOptionName() != null) {
				if (currentBrand != null && currentBrand.getName().equals(brand.getName())) {
					currentBrand.setColspan1(currentBrand.getColspan1()+1);
					if (currentOption.getOptionName().equals(brand.getOptionName())) {
						currentOption.setColspan2(currentOption.getColspan2()+1);
					} else { 
						currentOption = brand;
						currentOption.setColspan2(1);
					}
				} else { 
					currentBrand = brand;
					currentOption = brand;
					currentBrand.setColspan1(1);
					currentOption.setColspan2(1);
				}
			}
		}
	}

	private String saveToFile(List<Brand> brands, List<Customer> subAccounts, Map<Customer, 
			List<Brand>> subAccountMap, int userId, int year, int currentYear) throws Exception {
		
		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File exportFolder = new File(tempFolder, "exportFiles");
		if (!exportFolder.exists()) {
			exportFolder.mkdir();
		} else {
			// clear old files
			for (File file: exportFolder.listFiles()) {
				if (file.getName().startsWith("dReport" + userId) 
						& file.getName().endsWith(".csv")) {
					file.delete();
				}
			}
		}
    	
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMddkkmmss");
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		
    	File exportFile = new File(exportFolder, "dReport" + userId + 
    			dateFormatter.format(new Date()) + ".csv");
    	CSVWriter writer = new CSVWriter(new FileWriter(exportFile));

    	boolean hasOption = false;
    	
    	List<String> line = new ArrayList<String>();    	
    	line.add("Sales Rep");
    	for (Brand brand: brands) {
    		line.add(brand.getName());
    		if (brand.getOptionName() != null) {
    			hasOption = true;
    		}
    	}
    	writer.writeNext(line.toArray(new String[line.size()]));
    	
    	if (hasOption) {
        	line = new ArrayList<String>();    	
        	line.add("");
        	for (Brand brand: brands) {
        		if (brand.getOptionName() != null) {
        			line.add(brand.getOptionName());
        		} else {
                	line.add("");        			
        		}
        	}
        	writer.writeNext(line.toArray(new String[line.size()]));    
        	line = new ArrayList<String>();    	
        	line.add("");
        	for (Brand brand: brands) {
        		if (brand.getOptionName() != null) {
        			line.add(brand.getValueTitle());
        		} else {
                	line.add("");        			
        		}
        	}
        	writer.writeNext(line.toArray(new String[line.size()]));    
    	}
    	
    	for (Customer subAccount: subAccounts) {
    		
    		// add space
    		line = new ArrayList<String>();
    		line.add("");
        	writer.writeNext(line.toArray(new String[line.size()]));
    		
    		line = new ArrayList<String>();
    		line.add(subAccount.getAddress().getFirstName() + " " + subAccount.getAddress().getLastName()
    					+ ((subAccount.getSubCount() > 0) ? " (manager)" : ""));
    		
        	for (Brand brand: subAccountMap.get(subAccount)) {
        		if (brand.getTotal() != null) {
        			line.add(nf.format(brand.getTotal()));        			
        		} else {
        			line.add("");
        		}
        	}
        	writer.writeNext(line.toArray(new String[line.size()]));
        	
        	if (year == currentYear) {
        		line = new ArrayList<String>();
        		line.add("Yearly Budget");
            	for (Brand brand: subAccountMap.get(subAccount)) {
            		line.add(nf.format(brand.getBudget()));        			
            	}        		
            	writer.writeNext(line.toArray(new String[line.size()]));
            	
        		line = new ArrayList<String>();
        		line.add("Balance");
            	for (Brand brand: subAccountMap.get(subAccount)) {
            		line.add(nf.format(brand.getBalance()));        			
            	}        		
            	writer.writeNext(line.toArray(new String[line.size()]));            	
        	}
    	}    	
    	
    	// add space
		line = new ArrayList<String>();
		line.add("");
    	writer.writeNext(line.toArray(new String[line.size()]));
    	
    	// total
    	line = new ArrayList<String>();
    	line.add(year + " Total");
    	for (Brand brand: brands) {
    		line.add(nf.format(brand.getTotal()));
    	}    	
    	writer.writeNext(line.toArray(new String[line.size()]));
    	
    	writer.close();
    	
		
		return exportFile.getName();
	}	

}
