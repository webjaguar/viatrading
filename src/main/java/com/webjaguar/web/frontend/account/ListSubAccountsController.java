/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.05.2007
 */

package com.webjaguar.web.frontend.account;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class ListSubAccountsController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {	
    	CustomerSearch customerSearch = getCustomerSearch( request );
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	UserSession userSession = this.webJaguar.getUserSession(request);
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());
		
		// see if this customer is not allowed to see sub accounts
		if (customer != null && customer.isHideSubAccts()) {
			return new ModelAndView(new RedirectView("account.jhtm"));
		}
		
		// get sub-accounts
		customerSearch.setParent( ServletRequestUtils.getIntParameter( request, "parent", userSession.getUserid()) );
		List<Customer> familyTree = this.webJaguar.getFamilyTree( customerSearch.getParent(), "up" );
		Iterator<Customer> iter = familyTree.iterator();
		while (iter.hasNext()) {
			Customer node = iter.next();
			if (node.getId().compareTo(userSession.getUserid()) == 0) {
				iter.remove();
				break;
			} else {
				iter.remove();
			}
		}
		if (familyTree.size() == 0) {
			customerSearch.setParent(userSession.getUserid());
		}

		PagedListHolder subAccounts = new PagedListHolder( this.webJaguar.getCustomerListWithDate(customerSearch) );
		subAccounts.setPageSize(customerSearch.getPageSize() );
		subAccounts.setPage( customerSearch.getPage() - 1 );
		model.put( "accounts", subAccounts );
		model.put( "familyTree", familyTree );
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		}
		
		model.put("customerCredit", customer.getCredit());
		model.put("subAccountLayout", this.webJaguar.getSystemLayout("subAccount", request.getHeader("host"), request));
		
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/account/subAccounts", "model", model);    	
		}
		return new ModelAndView("frontend/account/subAccounts", "model", model);    	
    }
    
	private CustomerSearch getCustomerSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
		CustomerSearch customerSearch = (CustomerSearch) request.getSession().getAttribute( "subAccountCustomerSearch" );
		if (customerSearch == null) {
			customerSearch = new CustomerSearch();
			customerSearch.setPageSize( 100 );
			request.getSession().setAttribute( "subAccountCustomerSearch", customerSearch );
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			customerSearch.setSort(ServletRequestUtils.getStringParameter( request, "sort", "" ));
		}
				
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				customerSearch.setPage( 1 );
			} else {
				customerSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				customerSearch.setPageSize( 10 );
			} else {
				customerSearch.setPageSize( size );				
			}
		}
		
		return customerSearch;
	}
}
