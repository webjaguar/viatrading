/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.EventSearch;
import com.webjaguar.model.Layout;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SubscriptionSearch;
import com.webjaguar.model.Ticket;
import com.webjaguar.model.TicketSearch;
import com.webjaguar.model.UserSession;
import com.webjaguar.thirdparty.evergreen.EvergreenApi;

public class ListDashboard implements Controller {
	
	private WebJaguarFacade webJaguar;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	
    	UserSession userSession = this.webJaguar.getUserSession(request);	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
    	Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		EventSearch eventSearch = new EventSearch();
		if ( userSession != null )
		{
			Customer customer = (Customer) request.getAttribute("sessionCustomer");
			if(customer.isUpdateNewInformation() && request.getParameter("updateInformation") == null){
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("account_information_edit.jhtm?forwardAction=account.jhtm")));
			}
		    model.put("customer", customer);  
		    if(customer!=null){
		    	model.put("id",customer.getId());
		   }
		}
		
		Layout layout = null;
		layout = this.webJaguar.getSystemLayout("myaccount", request.getHeader("host"), request);
		if(layout != null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#pageName#", "MY DASHBOARD"));
		}
		if(layout == null){
			layout = (Layout) request.getAttribute("layout");
		}
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		request.setAttribute("layout", layout);

		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		//model.put("myaccountLayout", this.webJaguar.getSystemLayout("myaccount", request.getHeader("host"), request));
		
		TicketSearch ticketSearch = new TicketSearch();   
		List<Ticket> tickets = this.webJaguar.getTicketsListByUserid(userSession.getUserid(), ticketSearch);
		model.put("ticketCount", tickets.size());
		
		int count = this.webJaguar.getEventMemberListCount(eventSearch);
		model.put("eventCount", count);
		
		model.put("viewMyProductsLayout", this.webJaguar.getSystemLayout("viewMyProductsLayout", request.getHeader("host"), request));
		return new ModelAndView("frontend/account/dashboard", "model", model);    	
    }

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

}
