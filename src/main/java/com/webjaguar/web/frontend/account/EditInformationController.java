package com.webjaguar.web.frontend.account;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.Layout;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.CustomerForm;
import com.webjaguar.web.validator.AddressFormValidator;

public class EditInformationController extends SimpleFormController {
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private Map<String, Object> gSiteConfig;
	Map<String, Configuration> siteConfig;

	public EditInformationController() {
		setSessionForm(true);
		setCommandName("customerForm");
		setCommandClass(CustomerForm.class);
		setFormView("frontend/account/editInformation");
		setSuccessView("account.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {
		CustomerForm customerForm = (CustomerForm) command;
		
		// check if cancel button was pressed
		if (request.getParameter("_cancel") == null) {
			customerForm.getCustomer().setUpdateNewInformation(false);
			this.webJaguar.updateCustomer(customerForm.getCustomer(), false, false);
			String forwardAction = request.getParameter("forwardAction");
			if(forwardAction == null){
				return new ModelAndView(new RedirectView(getSuccessView()));    
			}else{
				return new ModelAndView(new RedirectView(forwardAction));
			}
		}else{
			String forwardAction = request.getParameter("forwardAction");
			if(forwardAction == null){
				return new ModelAndView(new RedirectView(getSuccessView()+"?updateInformation=remindMeLater"));    
			}else{
				return new ModelAndView(new RedirectView(forwardAction+"?updateInformation=remindMeLater"));
			}
		}
	
	}

	
	protected Map referenceData(HttpServletRequest request) 
			throws Exception {
	
       	Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		model.put("countrylist", this.webJaguar.getEnabledCountryList());
		model.put("statelist", this.webJaguar.getStateList("US"));
		model.put("caProvinceList", this.webJaguar.getStateList("CA"));
		model.put("languageCodes", this.webJaguar.getLanguageSettings());
		model.put("mobileCarrierList",this.webJaguar.getMobileCarrierList());
		model.put("forwardAction", (String)request.getParameter("forwardAction"));
	    map.put("model", model);
	    
		Layout layout = null;
		layout = this.webJaguar.getSystemLayout("viewEditMyAccountPage", request.getHeader("host"), request);
		if(layout != null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#pageName#", "Edit Information"));
		}
		if(layout == null){
			layout = (Layout) request.getAttribute("layout");
		}
		this.webJaguar.updateCartQuantity(null, request, layout);
		request.setAttribute("layout", layout);
		
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
	    
	    return map;
    }
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		
		CustomerForm customerForm = new CustomerForm((Customer) request.getAttribute("sessionCustomer"));
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		// Customer Fields
		List<CustomerField> customerFields = this.webJaguar.getCustomerFields();
		Iterator<CustomerField> iter = customerFields.iterator();
		while (iter.hasNext()) {
			CustomerField customerField = iter.next();
			if (!customerField.isEnabled() || !customerField.isPublicField()) {
				iter.remove();
			}
		}
		if (!customerFields.isEmpty()) {
			customerForm.setCustomerFields(customerFields);
		}		
		return customerForm;
	}
	
	protected void onBindAndValidate(HttpServletRequest request,
            Object command, BindException errors) throws Exception {
		if(request.getParameter("_cancel") == null){
			CustomerForm customerForm = (CustomerForm) command;
			
			validateAddress(customerForm.getCustomer().getAddress(),errors);
			if (!customerForm.getCustomer().getUsername().equalsIgnoreCase(customerForm.getExistingUsername()) &&
					this.webJaguar.getCustomerByUsername(customerForm.getCustomer().getUsername()) != null) {
				errors.rejectValue("customer.username", "USER_ID_ALREADY_EXISTS", "An account using this email address already exists.");    					
			}
			if ( customerForm.getCustomer().getTaxId() != null && customerForm.getCustomer().getTaxId().trim().equals( "" ) )
			{
				customerForm.getCustomer().setTaxId( null );
			}
			if (customerForm.getCustomerFields() != null) {
				for (CustomerField customerField : customerForm.getCustomerFields()) {
					if (customerField.isRequired())
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.field" + customerField.getId(), "form.required", "required");
				}
			}	
		}
	}
	public void validateAddress(Address address, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.firstName", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.lastName", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.addr1", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.city", "form.required", "required");
		if (address.isStateProvinceNA()) {
			address.setStateProvince("");
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.stateProvince", "form.required", "required");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.country", "form.required", "required");

		if (address.getCountry().equalsIgnoreCase("us")) {
			// (123)456-7890, 123-456-7890, 1234567890, (123)-456-7890
			// final String US_PHONE_REGEXP = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
			// with ext (xxx) xxx-xxxx Ext-xxxxx
			final String US_PHONE_REGEXP = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})(\\sExt-\\d{1,5})?$";
			final String New_format = "^\\((\\d{3})\\)(\\d{3})[- ](\\d{4})(\\s Ext-\\d{1,5})?$";
			final String US_ZIP_REGEXP = "^\\d{5}(-\\d{4})?$";
			// if added as 10 digit number we add (xxx) xxx-xxxx format to it.
			if (Constants.isDigit(address.getPhone().trim()) && address.getPhone().trim().length() == 10) {
				address.setPhone("(" + address.getPhone().trim().substring(0, 3) + ") " + address.getPhone().trim().substring(3, 6) + "-" + address.getPhone().trim().substring(6));
			}
			if (!(Pattern.compile(New_format).matcher(address.getPhone().trim()).matches())) {
				errors.rejectValue("customer.address.phone", "form.usPhone", "invalid USA phone");
			}
			if (address.getZip() == null || address.getZip().isEmpty()) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.zip", "form.required", "required");
			} else {
				if (!(Pattern.compile(US_ZIP_REGEXP).matcher(address.getZip().trim()).matches())) {
					errors.rejectValue("customer.address.zip", "form.usZipcode", "invalid USA zip code");
				}
			}
			address.setPhone(address.getPhone().trim());
			address.setZip(address.getZip().trim());
		} else if (address.getCountry().equalsIgnoreCase("ca")) {
			final String CA_ZIP_REGEXP_SPACE = "[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]\\d[a-zA-Z] \\d[a-zA-Z]\\d";
			final String CA_ZIP_REGEXP = "[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]\\d[a-zA-Z]\\d[a-zA-Z]\\d";
			if (address.getZip() == null || address.getZip().isEmpty()) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.zip", "form.required", "required");
			} else {
				if (!Pattern.compile(CA_ZIP_REGEXP_SPACE).matcher(address.getZip()).matches() && Pattern.compile(CA_ZIP_REGEXP).matcher(address.getZip()).matches()) {
					address.setZip(address.getZip().substring(0, 3) + " " + address.getZip().substring(3, 6));
				}
				if (!(Pattern.compile(CA_ZIP_REGEXP).matcher(address.getZip()).matches() || Pattern.compile(CA_ZIP_REGEXP_SPACE).matcher(address.getZip()).matches())) {
					errors.rejectValue("customer.address.zip", "form.canadaZipcode", "invalid Canada zipcode");
				}
			}
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.zip", "form.required", "required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.phone", "form.required", "required");
		}
	}
}