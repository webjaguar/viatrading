package com.webjaguar.web.frontend.account;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.UserSession;

public class ListQuotesController implements Controller{
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		UserSession userSession = this.webJaguar.getUserSession(request);	
    	Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		OrderSearch search = getSearch(request);
		search.setStatus("xq");
		search.setUserId(userSession.getUserid());
		
		model.put("quoteSearch", search);
		
		// quotes
		PagedListHolder quotesList = new PagedListHolder(this.webJaguar.getQuotesListByUser(search));
		quotesList.setPageSize(search.getPageSize());
		quotesList.setPage(search.getPage()-1);	
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		model.put("quotes", quotesList);		
		model.put("myQuoteHistoryLayout", this.webJaguar.getSystemLayout("myquotehistory", request.getHeader("host"), request));
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/account/quotes", "model", model);
		}
		return new ModelAndView("frontend/account/quotes", "model", model);  
	}
	
	
	private OrderSearch getSearch(HttpServletRequest request) {
		OrderSearch search = new OrderSearch();
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		// sorting
		if (request.getParameter("sort") != null) {
			int sortValue = ServletRequestUtils.getIntParameter( request, "sort", 0 );
			if (sortValue == 0) {
				search.setSort("date_ordered");
			} else if(sortValue == 1) {
				search.setSort("date_ordered desc");
			}
		}
		return search;
	}

}
