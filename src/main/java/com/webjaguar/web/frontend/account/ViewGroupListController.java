/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class ViewGroupListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {		
		
    	UserSession userSession = this.webJaguar.getUserSession(request);			    	
    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();
		model.put( "url", url + "?" + query );
        // continue shopping
		request.getSession().setAttribute( "continueShoppingUrl", model.get( "url" ) );

		// check if delete button was clicked
		if (request.getParameter("__delete_group") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				this.webJaguar.removeMyListGroup(ids, userSession.getUserid());
			}
		}
		
		// check if delete button was clicked
		if (request.getParameter("_addGroup.x") != null) {
			String groupName = request.getParameter("_groupname");
			if (groupName != null && !groupName.isEmpty() && this.webJaguar.getShoppingListGroupByName( groupName ) == null) {
				this.webJaguar.insertShoppingGroupList(groupName, userSession.getUserid());
			}
		}
		
		// products
		PagedListHolder groupList = 
			new PagedListHolder(this.webJaguar.getShoppingListGroupByUserid(userSession.getUserid()));
		groupList.setPageSize(10);
		String page = request.getParameter("page");
		if (page != null) {
			groupList.setPage(Integer.parseInt(page)-1);
		}
		model.put("groupList", groupList);
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		//model.put("myGrouplistLayout", this.webJaguar.getSystemLayout("myGrouplist", request.getHeader("host"), request));
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/account/groupList", "model", model); 
		} else {
			return new ModelAndView("frontend/account/groupList", "model", model); 
		}
    }
}