/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.domain.Constants;

public class ViewListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {		
		
    	UserSession userSession = this.webJaguar.getUserSession(request);			    	

		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();
		model.put( "url", url + "?" + query );
        // continue shopping
		request.getSession().setAttribute( "continueShoppingUrl", model.get( "url" ) );
		
		int viewGroupId = 0;
		if (request.getParameter("gid") != null) {
			viewGroupId = ServletRequestUtils.getIntParameter( request, "gid", 0 );
		}
		
		String protectedAccess = "0";
		// check if protected feature is enabled
		Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());
		int protectedLevels = (Integer) gSiteConfig.get( "gPROTECTED" );
		if ( protectedLevels == 0 ) {
			protectedAccess = Constants.FULL_PROTECTED_ACCESS;
		} else {
			if ( customer != null ) {
				protectedAccess = customer.getProtectedAccess();
			} else {
				protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
			}
		}
		
		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				this.webJaguar.removeFromList(ids, userSession.getUserid());
			}
		}
		
		// check if add group was selected
		if (request.getParameter("_addToGroup.x") != null) {
			Integer groupId = Integer.parseInt( request.getParameter("_groupId") );
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				this.webJaguar.addToShoppingGroupList(ids, userSession.getUserid(), groupId);
			}
		}
		
		List<Product> productListTemp = this.webJaguar.getShoppingListByUserid(userSession.getUserid(), viewGroupId,  request);
		
		model.put("viewGroupId", viewGroupId);
		model.put("groupList", this.webJaguar.getShoppingListGroupByUserid( userSession.getUserid() ));
		boolean showPriceColumn = false;
		boolean showQtyColumn = false;
		List <Product> productsList = new ArrayList<Product>();
		for(Product product: productListTemp){
			if (!showPriceColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) || product.getMsrp() != null)) {
				showPriceColumn = true;					
			}
			if (!showQtyColumn && (product.getPrice() != null && !product.getPrice().isEmpty())) {
				showQtyColumn = true;					
			}
			if ((Boolean) gSiteConfig.get("gMASTER_SKU")) {
				this.webJaguar.setMasterSkuDetails(product, protectedAccess, (Boolean) gSiteConfig.get("gINVENTORY"));				
			}
			productsList.add(product);
		}
		List<ProductField> productFields = new ArrayList<ProductField>();

		//Showing product fields on my list
		try {
			productFields = (List<ProductField>) this.webJaguar.getProductFields(request, productsList, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		PagedListHolder productList = new PagedListHolder(productsList);
		productList.setPageSize(20);
		String page = request.getParameter("page");
		if (page != null) {
			productList.setPage(Integer.parseInt(page)-1);
		}
		
		// product fields
		model.put( "productFields", productFields );
		model.put("products", productList);

		model.put("showPriceColumn", showPriceColumn);
		model.put("showQtyColumn", showQtyColumn);
		
		Layout layout = null;
		layout = this.webJaguar.getSystemLayout("mylist", request.getHeader("host"), request);
		if(layout != null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#pageName#", "My List"));
		}
		if(layout == null){
			layout = (Layout) request.getAttribute("layout");
		}
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		request.setAttribute("layout", layout);
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/account/myList", "model", model);    	
		}
		return new ModelAndView("frontend/account/myList", "model", model);    	
    }
}