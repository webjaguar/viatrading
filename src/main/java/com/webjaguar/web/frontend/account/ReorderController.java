package com.webjaguar.web.frontend.account;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Price;
import com.webjaguar.model.Product;
import com.webjaguar.model.UserSession;
import com.webjaguar.model.LineItem;
import com.webjaguar.thirdparty.evergreen.EvergreenApi;

public class ReorderController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
    	
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		if ((Boolean) gSiteConfig.get( "gREORDER" )) {
			
			if(request.getParameter("order")!= null || request.getParameter("order") !="") {					
				UserSession userSession = this.webJaguar.getUserSession( request );

				Customer customer = null;
				if ( userSession != null ) {
					customer = this.webJaguar.getCustomerById(userSession.getUserid());
				}
				
				List<Product> myOrderList = new ArrayList<Product>();
				Set<String> skues = new TreeSet();
				
				EvergreenApi evergreen = null;	
				// evergreen
				if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
					evergreen = new EvergreenApi();
					myModel.put( "evergreen", true );	
				}
				
				for (LineItem lineItem : this.webJaguar.getLineItemByOrderId(new Integer(request.getParameter("order")))) {
					String sku = lineItem.getProduct().getSku();
					// remove duplicate sku
					if (skues.contains(sku)) {
						continue;
					} else {
						skues.add(sku);
					}
					if ( !("".equals(sku)) ) {
						Product product = this.webJaguar.getProductById( this.webJaguar.getProductIdBySku(sku), request );
						if(product != null) {
							// get evergreen inventory status								
							if (evergreen!=null) {
								String evergreenStatus = evergreen.GetInvStatus(sku);
								if (evergreenStatus != null) {
									product.setField6(evergreenStatus);	
								}
								//change the price to price table 
								if ((customer != null && customer.getPriceTable() == 6) && product.getPriceTable6() == null) {
									product.setPrice( null );
									product.setMsrp( null );
									product.setLoginRequire(true);
									product.setAltLoginRequire(true);
								}
							}
							
							myOrderList.add(product);
						}							
						if(product == null) {
							Product newProduct = new Product();
							Price productPrice = new Price() ;
							productPrice.setAmt(lineItem.getUnitPrice());
							newProduct.setName(lineItem.getProduct().getName());
							List<Price> price = new ArrayList<Price>();
							price.add(productPrice);
							newProduct.setPrice(price);
							newProduct.setSku(sku);
							newProduct.setActive(false);
							
							myOrderList.add(newProduct);
						}
					}
				}
					
				if (!myOrderList.isEmpty()) {	// not empty
					PagedListHolder productList = new PagedListHolder(myOrderList);
					productList.setPageSize(70);
					String page = request.getParameter("page");
					if (page != null) {
						productList.setPage(Integer.parseInt(page)-1);
					}
					myModel.put( "myOrderLineItems", productList );							
				}
			}
		}
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		myModel.put("myOrderListLayout", this.webJaguar.getSystemLayout("myorderlist", request.getHeader("host"), request));
		if(((Configuration)siteConfig.get("TEMPLATE")).getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/account/reorder", "model", myModel);
		}
		return new ModelAndView("frontend/account/reorder", "model", myModel);  
	}
}