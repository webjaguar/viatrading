/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 04.24.2009
 */

package com.webjaguar.web.frontend.account;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;

public class UserDescriptionFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public UserDescriptionFormController() {
		setCommandName("customer");
		setCommandClass(Customer.class);
		setFormView("frontend/account/descriptionForm");
		setSuccessView("account.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {

		Customer customer = (Customer) command;

		// check if cancel button was pressed
		if (request.getParameter("_cancel") == null) {
			this.webJaguar.updateUserDescription(customer);
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		return (Customer) request.getAttribute("sessionCustomer");
	}
	
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {

    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");	
    	
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		map.put("model", model);
		
		Layout layout = (Layout) request.getAttribute("layout");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		
		return map;
    }
}
