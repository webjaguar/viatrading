/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.BitSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.listener.SessionListener;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;
import com.webjaguar.thirdparty.evergreen.EvergreenApi;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.Utilities;

public class LoginController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	private Map<String, Configuration> siteConfig;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
		Customer customer = null;
		String forwardAction = request.getParameter("forwardAction");
		String redirectSku = ServletRequestUtils.getStringParameter( request, "redirectSku", "" );
		
		if (this.webJaguar.getUserSession(request) != null) {
			if (forwardAction != null) {
				response.sendRedirect(forwardAction);
			} else {
				return new ModelAndView(new RedirectView("account.jhtm"));
			}
		}
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Object> model = new HashMap<String, Object>();

		String username = ServletRequestUtils.getStringParameter( request, "username", "" );
//		String username2 = ServletRequestUtils.getStringParameter( request, "username", "" );
		String password = ServletRequestUtils.getStringParameter( request, "password", "" );
		String accountNum = request.getParameter("accountNum");
		String cardId = ServletRequestUtils.getStringParameter( request, "cardid", "" );
		boolean keepMeLogin = ServletRequestUtils.getBooleanParameter(request, "keepmelogin", false);
		
		System.out.println("Logincontroller line 74: " + username);
		
		if (username.length() > 0 && password.length() > 0) {
			customer = this.webJaguar.getCustomerByUsernameAndPassword(username, password);	
			if(customer == null) {
				if (username.matches(".*[0-9]+.*") && !username.matches(".*[a-zA-Z]+.*") && !username.matches(".*@.*")) {
					System.out.println("inside getCellPhoneByUsernameAndPassword line 79: " +username);
					username = username.replaceAll("[^0-9]", "");
					// only take substring if country code is (+1)
					username = (username.length() == 11 && username.charAt(0) == '1') ? username.substring(username.length()-10) : username;
					System.out.println("loginController line 89 extract numbers: "+username);
					customer = this.webJaguar.getCellPhoneByUsernameAndPassword(username, password);
				}
				
			}
		}else if ((Boolean) gSiteConfig.get( "gREGISTRATION_TOUCH_SCREEN" ) && cardId.length() > 0) {
			customer = this.webJaguar.getCustomerByCardIdAndPassword( cardId, password );
		}

		boolean validIpAddress = true;
		if ( siteConfig.get( "CUSTOMER_IP_ADDRESS" ).getValue().equals( "true" ) && customer != null && customer.getIpAddress() != null ) {
			String[] ips = customer.getIpAddress().split( "[,\n]" ); // split by line breaks or commas 
			for ( int x = 0; x < ips.length; x++ )
			{
				String ip = ips[x].trim();
				if ( !("".equals(ip)) ) {
					if ( x == 0  ) { validIpAddress = false; }
					if (request.getRemoteAddr().equals( ip )) {
						validIpAddress = true;
						break;
					}
				}
			}
		}
		boolean hasProtectedAccess = true;
		if ((Integer) gSiteConfig.get("gPROTECTED") > 0 && siteConfig.get("PROTECTED_HOST").getValue().equals(request.getHeader("host"))) {
			if (customer != null) {
				hasProtectedAccess = hasProtectedAccess(siteConfig.get("PROTECTED_HOST_PROTECTED_LEVEL").getValue(), customer.getProtectedAccess());				
			}
		}

		model.put("redirectSku", redirectSku);
		
		boolean hasExpiredPassword = customer != null ? customer.hasExpiredPassword() : false;
		if (customer == null || (customer != null && (customer.isSuspended() || hasExpiredPassword)) || (!validIpAddress) || !hasProtectedAccess) {
			model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
			if (customer != null && (customer.isSuspended() || hasExpiredPassword)) {
				if(customer.isSuspended()) {
					model.put("message", "LOGIN_SUSPENDED");				
				} else if(hasExpiredPassword) {
					model.put("message", "PASSWORD_EXPIRED");				
				}
			} else if ( !validIpAddress ) {
				model.put("message", "LOGIN_IPADDRESS_NOTVALID");	
				customer.setNote( customer.getNote().concat( "--START-- \nOn " + new Date() + " \n\"" + customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName() + "\" was trying to login with this IP ADDRESS: " + request.getRemoteAddr() + "\n--END--\n\n" ));
				this.webJaguar.updateCustomerNote( customer );
				notifyCustomer(request, customer);
			} else if (!hasProtectedAccess) {
				model.put("message", "LOGIN_INVALID_FOR_DOMAIN");	
			} else if (username.length() > 0 || password.length() > 0) {
				model.put("message", "LOGIN_INVALID");				
			}
			ModelAndView modelAndView = new ModelAndView("frontend/login", "model", model);
			if (siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
				if(siteConfig.get("FRONTEND_LOGIN_DIRECTORY").getValue()!=null && siteConfig.get("FRONTEND_LOGIN_DIRECTORY").getValue().length()>0){
					modelAndView = new ModelAndView(siteConfig.get("FRONTEND_LOGIN_DIRECTORY").getValue()+"/login", "model", model);
				}else{
					modelAndView = new ModelAndView("frontend/layout/template6/login", "model", model);
				}	
			}
			if (forwardAction != null) modelAndView.addObject("signonForwardAction", forwardAction);
			
			Layout layout = (Layout)request.getAttribute("layout");
			String viewManifest = (String)request.getParameter("viewManifest");
			
			if(viewManifest!=null && viewManifest.equalsIgnoreCase("true")){
				layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Login to View Manifests"));
			}else{
				layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Login"));	
			}

			this.webJaguar.updateCartQuantity(null, request, layout);
			if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) 
					|| siteConfig.get("PROTECTED_HOST").getValue().equals(request.getHeader("host")))
			{
				layout.setLeftBarTopHtml( "" );
				layout.setLeftBarBottomHtml( "" );
				layout.setHideLeftBar( true );
			} 
			if (siteConfig.get("PROTECTED_HOST").getValue().equals(request.getHeader("host"))) {
				layout.setRightBarTopHtml( "" );
				layout.setRightBarBottomHtml( "" );				
			}
			
			// multi store
			model.put("loginLayout", this.webJaguar.getSystemLayout("login", request.getHeader("host"), request));
			
			return modelAndView;			
		}
		else {
			// update login statistics
			this.webJaguar.nonTransactionSafeLoginStats(customer);
			
			// set user session
			UserSession userSession = new UserSession();
			userSession.setUsername(customer.getUsername());
			userSession.setFirstName( customer.getAddress().getFirstName() );
			userSession.setLastName( customer.getAddress().getLastName() );
			userSession.setUserid(customer.getId());
			this.webJaguar.setUserSession(request, userSession);
			
			SessionListener.getActiveSessions().put(request.getSession().getId(), customer.getId());
			
			Cart cart = (Cart) request.getSession().getAttribute("sessionCart");
			if (cart != null) {
				// clear saved cart to avoid confusion and surprises
				// this.webJaguar.deleteShoppingCart(userSession.getUserid(), null);
				this.webJaguar.mergeCart(cart, userSession.getUserid());
				request.getSession().setAttribute( "userCart", cart );
				request.getSession().removeAttribute("sessionCart");
			}
			String path = request.getContextPath().equals("") ? "/" : request.getContextPath();

			// KeepMeLogin Module
			if (Integer.parseInt(siteConfig.get("COOKIE_AGE_KEEP_ME_LOGIN").getValue()) > -1 && keepMeLogin) {
				String token = this.webJaguar.createPasswordToken(customer.getId());
				// without this mod rewrite pages will save multiple cookies with different paths
				//String path = request.getContextPath().equals("") ? "/" : request.getContextPath();
				int days = Integer.parseInt(siteConfig.get("COOKIE_AGE_KEEP_ME_LOGIN").getValue());
				String cookieValue = token;
				Utilities.addCookie(response,"UKWJ", cookieValue, Constants.SECONDS_IN_DAY * days, path);
			}
			
			// Encode customer ID in cookies
			if(customer!= null && customer.getId()!=null && !customer.getId().toString().isEmpty()){
				try{
				String orig = customer.getId().toString();
			    byte[] encoded = Base64.encodeBase64(orig.getBytes());         
			    String encode = new String(encoded);   
		
			    //System.out.println("orig"+ customer.getId().toString() +"\n encoded"+ encode + "\n decoded" + decode);
			    
				Cookie CustID = new Cookie("CI", encode);
				int days = Integer.parseInt(siteConfig.get("COOKIE_AGE").getValue());
				CustID.setMaxAge(-1);
				CustID.setPath(path);
				response.addCookie(CustID);
				} catch (Exception e){
					Cookie CustID = new Cookie("CI", "ERROR");
					CustID.setPath(path);
					response.addCookie(CustID);
				}
		   }
			
			
			
			if (customer!=null && redirectSku!=null && redirectSku.length()>0) {
				System.out.println("-----loadUrl1----" + redirectSku);
				String loadUrl1 = request.getScheme()+"://www.viatrading.com/wholesale/738/Load-Center.html?cid=738&sort=&sorttype=&state=asc&userId=1&field_39=" + redirectSku + "&reset=&fsort=&size=25&page=1&redirectSku=" + redirectSku;	
				return new ModelAndView(new RedirectView(loadUrl1.trim()));
			}
			
			if (forwardAction != null) {
				try{
				response.sendRedirect(forwardAction);
				} catch(Exception e){ 
					// do nothing		
				}
				return null;
			} else {
				String landingPage = siteConfig.get("LOGIN_SUCCESS_URL").getValue();
				if (customer.getLoginSuccessURL() != null && customer.getLoginSuccessURL().trim().length() > 0) {
					landingPage = customer.getLoginSuccessURL();
				}
				return new ModelAndView(new RedirectView(landingPage.trim()));
			}
		}
	}

	private void notifyCustomer(HttpServletRequest request, Customer customer) {
		
		// send email
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom( customer.getUsername() );
		msg.setSubject("Customer Access Denied!");
		msg.setText("" + customer.getAddress().getFirstName() + ", " + customer.getAddress().getLastName() + " Access denied.\n\n" +
					"On:\t" + dateFormatter.format(new Date()) + "\n" +
					"A note has been added to the customer note section.\n");
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}

	private BitSet getBitSet(String bitString) {
		BitSet bitSet = new BitSet();
		StringBuffer sbuff = new StringBuffer(bitString);
		sbuff.reverse();
		for (int i=0; i<sbuff.length(); i++) {
			if (sbuff.charAt(i) == '1') {
				bitSet.set(i);				
			}
		}
		return bitSet;
	}
	
	private boolean hasProtectedAccess(String protectedLevel, String protectedAccess) {
		BitSet oldPaBitSet = getBitSet(protectedAccess);
		BitSet newPaBitSet = getBitSet(protectedAccess);
		BitSet plBitSet = getBitSet(protectedLevel);
		newPaBitSet.or(plBitSet);
		
		return (oldPaBitSet.equals(newPaBitSet));
	}
	
	private void checkEvergreenAccount(String accountNum, Map<String, Object> model, 
			String forwardAction, HttpServletRequest request, HttpServletResponse response) throws Exception {
		accountNum = accountNum.trim();
		if (accountNum.length() == 0) {
			model.put("message", "ACCOUNT_NUMBER_INVALID");				
		} else if (this.webJaguar.isExistingCustomer(accountNum)) {
			model.put("message", "ACCOUNT_NUMBER_ALREADY_EXISTS");					
		} else {
			EvergreenApi evergreen = new EvergreenApi();
			Map<String, Object> client = evergreen.GetClient(accountNum);

			if (client == null) {
				// not found
				model.put("evergreenMessage", true);
			} else if (!client.get("B_ZIP").equals(request.getParameter("zipCode"))) {
				// check zip code
				model.put("evergreenMessage", true);
			} else {
				request.getSession().setAttribute("evergreenClient", client);
				if (forwardAction != null) {
					response.sendRedirect("register.jhtm?forwardAction=" + URLEncoder.encode(forwardAction, "UTF-8"));
				} else {
					response.sendRedirect("register.jhtm");					
				}
			}
		}
	}
}
