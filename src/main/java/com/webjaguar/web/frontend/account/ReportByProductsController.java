/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.16.2009
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.BudgetProduct;
import com.webjaguar.model.BudgetProductSearch;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.UserSession;

public class ReportByProductsController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {	

    	UserSession userSession = this.webJaguar.getUserSession(request);	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		int custId = userSession.getUserid();
		if ((Boolean) gSiteConfig.get("gSUB_ACCOUNTS") && request.getParameter("id") != null
				&& !this.webJaguar.getCustomerById(userSession.getUserid()).isHideSubAccts()) {
			custId = ServletRequestUtils.getIntParameter(request, "id", userSession.getUserid());
			boolean valid = false;
			for (Customer node: this.webJaguar.getFamilyTree(custId, "up")) {
				// check if a valid sub-account
				if (node.getId().compareTo(userSession.getUserid()) == 0) {
					valid = true;
					break;
				}
			}
			if (valid) {
				model.put("subAccount", this.webJaguar.getCustomerById(custId));
			} else {
				custId = userSession.getUserid();
			}
		}		
		
		// current year
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		model.put("currentYear", currentYear);
		
		OrderSearch search = getSearch(request, currentYear);
		search.setUserId(custId);	
		model.put("orderSearch", search);
		
		// orders
		List<Order> orders = this.webJaguar.getInvoiceExportList(search, null);
		model.put("orders", orders);	
		
		Layout layout = (Layout) request.getAttribute("layout");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		
		BudgetProductSearch bProdSearch = new BudgetProductSearch();
		bProdSearch.setUserId(custId);
		bProdSearch.setYear(search.getYear());
		List<BudgetProduct> budgetProducts = this.webJaguar.getBudgetProductList(bProdSearch);
		model.put("budgetProducts", budgetProducts);
		List<Integer> years = this.webJaguar.getBudgetProductYears(bProdSearch);
		if (years.isEmpty() || years.get(0) != currentYear) {
			years.add(0, currentYear);
		}
		model.put("years", years);
		model.put("budgetProductMap", getQtyBrand(orders, budgetProducts));
				
		return new ModelAndView("frontend/account/reportByProducts", "model", model);    	
    }
	
	private OrderSearch getSearch(HttpServletRequest request, int currentYear) {
		OrderSearch search = new OrderSearch();
			
		// year
		search.setYear(ServletRequestUtils.getIntParameter(request, "year", currentYear));
		
		// status (all orders not cancelled)
		search.setStatusTemp("status not like '%x%'");
			
		return search;
	}
	
	private Map<Integer, Map<BudgetProduct, Integer>> getQtyBrand(List<Order> orders, List<BudgetProduct> budgetProducts) {
		Map<Integer, Map<BudgetProduct, Integer>> results = new HashMap<Integer, Map<BudgetProduct, Integer>>();
		
		Map<String, BudgetProduct> budgetProductMap = new HashMap<String, BudgetProduct>();
		for (BudgetProduct budgetProduct: budgetProducts) {
			budgetProductMap.put(budgetProduct.getProduct().getSku(), budgetProduct);
		}
		
		Iterator<Order> iter = orders.iterator();
		while (iter.hasNext()) {
			Order order = iter.next();
			Map<BudgetProduct, Integer> map = new HashMap<BudgetProduct, Integer>();
			for (LineItem lineItem: order.getLineItems()) {
				String sku = lineItem.getProduct().getSku();
				if (budgetProductMap.containsKey(sku)) {
					if (map.get(budgetProductMap.get(sku)) == null) {
						map.put(budgetProductMap.get(sku), 0);
					}
					map.put(budgetProductMap.get(sku), map.get(budgetProductMap.get(sku)) + lineItem.getQuantity());	
				}
			}
			if (map.isEmpty()) {
				iter.remove();
			} else {
				results.put(order.getOrderId(), map);				
			}
		}
		
		return results;
	}

}
