package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Stack;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ezic.net.URL;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.thirdparty.sugarSync.SugarSync;
import com.webjaguar.thirdparty.sugarSync.SugarSyncAPI;
import com.webjaguar.thirdparty.sugarSync.SugarSyncSearch;
import com.webjaguar.thirdparty.sugarSync.util.HttpResponse;

public class SugarSyncSearchController extends SimpleFormController {

	public String subFolderXml = null;
	public static String subFolderXml2 = null;
	public String accessToken = null;
	public Stack<String> st = new Stack<String>();

	Map<String, Configuration> siteConfig;
	Map<String, Object> gSiteConfig;

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private Properties site;

	public void setSite(Properties site) {
		this.site = site;
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Object> myModel = new HashMap<String, Object>();
		SugarSyncAPI sugarSyncApi = new SugarSyncAPI(siteConfig);
		if (request.getParameter("keyword") != null) {
			String keyWordSearch = ServletRequestUtils.getStringParameter(request, "keyword", null);
			SugarSyncSearch search = new SugarSyncSearch();
			SugarSync sugarSync = new SugarSync();
			sugarSyncApi.method("list", sugarSync);
			subFolderXml = sugarSync.getResponseBody();
			accessToken = sugarSync.getAccessToken();
			HttpResponse searchFolderRepresentation = sugarSyncApi.getSearchFolderRepresentation(accessToken, keyWordSearch);
			if (searchFolderRepresentation == null) {
				myModel.put("subFolders", null);
				myModel.put("searchKeyWord", null);
				myModel.put("back", true);
			} else {
				sugarSync.setResponseBody(searchFolderRepresentation.getResponseBody());
				if (searchFolderRepresentation.getResponseBody() == null) {
					System.out.println("!-- searchFolderRepresentation.getResponseBody() is null");
				}
				changeNodeValues(sugarSync.getResponseBody(), null, null, sugarSync, myModel);
				if (!st.isEmpty() && !(st.peek().equals(request.getParameter("refText")))) {
					st.push(request.getParameter("keyword"));
				}
				myModel.put("subFolders", sugarSync.getFolderNames());
				myModel.put("searchKeyWord", keyWordSearch);
				myModel.put("back", true);
			}

		}
		myModel.put("sugarSyncLayout", this.webJaguar.getSystemLayout("sugarSync", request.getHeader("host"), request));
		return new ModelAndView("frontend/account/sugarSync", "model", myModel);
	}

	private SugarSyncSearch getSugarSyncSearch(HttpServletRequest request, Map<String, Object> model) {
		return null;
	}

	public void changeNodeValues(String xmlString, String refNew, String fileDataNew, SugarSync sugarSync, Map<String, Object> myModel) throws XPathExpressionException {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(xmlString));
			Document doc = db.parse(is);
			Element root = doc.getDocumentElement();
			NodeList rootlist = root.getChildNodes();
			List<String> names = new ArrayList<String>();
			myModel.put("length", rootlist.getLength());
			for (int i = 0; i < rootlist.getLength(); i++) {
				Element collection = (Element) rootlist.item(i);
				if (collection.getNodeName().equalsIgnoreCase("collection")) {
					NodeList collectionList = collection.getChildNodes();
					// name
					Element name = (Element) collectionList.item(0);
					NodeList nameList = name.getChildNodes();
					Text nameText = (Text) nameList.item(0);
					names.add(nameText.getData());

					// ref tag
					Element ref = (Element) collectionList.item(1);
					NodeList refList = ref.getChildNodes();
					Text refText = (Text) refList.item(0);
					// unique name on jsp
					URL refUrl = new URL(refText.getData());
					if (refUrl.getPathFile().split("_").length > 1) {
						myModel.put("name_" + i, refUrl.getPathFile().split("_")[1]);
					} else {
						myModel.put("name_" + i, i);
					}
					myModel.put("refText_" + i, refText.getData());
					if (refNew != null) {
						refText.setData(refNew);
					}
				}
				if (collection.getNodeName().equalsIgnoreCase("file")) {
					NodeList collectionList = collection.getChildNodes();
					// name
					Element name = (Element) collectionList.item(0);
					NodeList nameList = name.getChildNodes();
					Text nameText = (Text) nameList.item(0);
					names.add(nameText.getData() + "_fileName");

					// ref tag.
					Element ref = (Element) collectionList.item(5);
					NodeList refList = ref.getChildNodes();
					Text fileData = (Text) refList.item(0);
					// unique name on jsp
					URL fileUrl = new URL(fileData.getData());
					if (fileUrl.getPathFile().split("_").length > 1) {
						String[] split = fileUrl.getPathFile().split("_")[1].split("/");
						myModel.put("name_" + i, split[0]);
					} else {
						myModel.put("name_" + i, i);
					}

					myModel.put("fileData_" + i, fileData.getData());
					if (fileDataNew != null) {
						fileData.setData(fileDataNew);
					}
				}
			}
			sugarSync.setFolderNames(names);
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (SAXException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
