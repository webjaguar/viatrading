/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.UserSession;

public class ListOrdersController extends WebApplicationObjectSupport implements Controller  {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {	

    	UserSession userSession = this.webJaguar.getUserSession(request);	
    	Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		int custId = userSession.getUserid();
		model.put("userId", userSession.getUserid());
		if ((Boolean) gSiteConfig.get( "gSUB_ACCOUNTS" ) && request.getParameter( "id" ) != null
				&& !this.webJaguar.getCustomerById(userSession.getUserid()).isHideSubAccts()) {
			custId = ServletRequestUtils.getIntParameter(request, "id", userSession.getUserid());
			boolean valid = false;
			for (Customer node: this.webJaguar.getFamilyTree( custId, "up" )) {
				// check if a valid sub-account
				if (node.getId().compareTo(userSession.getUserid()) == 0) {
					valid = true;
					break;
				}
			}
			if (valid) {
				model.put("subAccount", this.webJaguar.getCustomerById(custId));
			} else {
				custId = userSession.getUserid();
			}
		}		
		
		OrderSearch search = getSearch(request);
		search.setUserId(custId);
		model.put("orderSearch", search);
		
		// orders
		PagedListHolder ordersList = new PagedListHolder();
		if(request.getParameter("getQuoteList") != null && request.getParameter("getQuoteList").equalsIgnoreCase("true")) {
			ordersList = new PagedListHolder(this.webJaguar.getOrdersListByUser(search, true));
			model.put("getQuoteList",true);
		} else {
			ordersList = new PagedListHolder(this.webJaguar.getOrdersListByUser(search, false));			
		}
		ordersList.setPageSize(search.getPageSize());
		ordersList.setPage(search.getPage()-1);
		model.put("orders", ordersList);
		
		// LastOrderbyCustomer
		Order order = this.webJaguar.getLastOrderByUserId(userSession.getUserid());
		model.put("lastOrderId", "");
		model.put("lastStaus", "");
		model.put("lasSubStaus", "");
		
		
		if(order != null) {
			ApplicationContext context = getApplicationContext();
			model.put("lastOrderId", order.getOrderId());
			model.put("lastStatus", context.getMessage( "orderStatus_"+order.getStatus(), new Object[0], RequestContextUtils.getLocale(request)));
			model.put("lastSubStatus", context.getMessage( "orderSubStatus_"+order.getSubStatus(), new Object[0], RequestContextUtils.getLocale(request)));
		}
		
		Layout layout = null;
		layout = this.webJaguar.getSystemLayout("myorderhistory", request.getHeader("host"), request);
		if(layout != null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#pageName#", "My Order History/Status"));
		}
		if(layout == null){
			layout = (Layout) request.getAttribute("layout");
		}
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		request.setAttribute("layout", layout);
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		//model.put("myOrderHistoryLayout", this.webJaguar.getSystemLayout("myorderhistory", request.getHeader("host"), request));
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/account/orders", "model", model);
		} 
		return new ModelAndView("frontend/account/orders", "model", model);    	
    }

	private OrderSearch getSearch(HttpServletRequest request) {
		OrderSearch search = new OrderSearch();
		
		// order number
		if (request.getParameter("orderNum") != null) {
			search.setOrderNum(ServletRequestUtils.getStringParameter( request, "orderNum", "" ));
		}
		
		// purchase order
		if (request.getParameter("purchaseOrder") != null) {
			search.setPurchaseOrder(ServletRequestUtils.getStringParameter(request, "purchaseOrder", ""));				
		}
	
		// status
		if (request.getParameter("status") != null) {
			search.setStatus(ServletRequestUtils.getStringParameter( request, "status", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}

		// sorting
		if (request.getParameter("sort") != null) {
			int sortValue = ServletRequestUtils.getIntParameter( request, "sort", 0 );
			if (sortValue == 0) {
				search.setSort("date_ordered");
			} else if(sortValue == 1) {
				search.setSort("date_ordered desc");
			}
		}
			
		return search;
	}
}