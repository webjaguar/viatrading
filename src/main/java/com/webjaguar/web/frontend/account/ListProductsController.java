/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class ListProductsController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	
		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("account_product.jhtm"));				
		}		
		
    	UserSession userSession = this.webJaguar.getUserSession(request);			    	
    	
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("userId", userSession.getUserid());
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());

		Integer supplierId = customer.getSupplierId();
		if (supplierId == null) {
			myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
			myModel.put("message", "Invalid account.");
			ModelAndView modelAndView = new ModelAndView("frontend/error");
			modelAndView.addObject("model", myModel);
			throw new ModelAndViewDefiningException(modelAndView);
		}
		myModel.put("supplierId", supplierId);
		myModel.put("supplierProdLimit", customer.getSupplierProdLimit());
		myModel.put("canAdd", customer.getSupplierProdLimit() == null ? true : 
			customer.getSupplierProdLimit() > this.webJaguar.productCountByCustomerSupplierId(supplierId));
		
		// products
		PagedListHolder productList = 
			new PagedListHolder(this.webJaguar.getProductsOnFrontendBySupplier(supplierId));
		productList.setPageSize(10);
		String page = request.getParameter("page");
		if (page != null) {
			productList.setPage(Integer.parseInt(page)-1);
		}
		myModel.put("products", productList);	
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "My Products"));
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		myModel.put("viewMyProductsLayout", this.webJaguar.getSystemLayout("viewMyProductsLayout", request.getHeader("host"), request));
		return new ModelAndView("frontend/account/products", "model", myModel);    	
    }

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

}
