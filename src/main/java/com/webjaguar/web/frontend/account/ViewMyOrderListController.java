/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.UserSession;

public class ViewMyOrderListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {		
		
    	UserSession userSession = this.webJaguar.getUserSession(request);			    	
    	Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
    	
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();
		myModel.put( "url", url + "?" + query );
        // continue shopping
		request.getSession().setAttribute( "continueShoppingUrl", myModel.get( "url" ) );
		
        //  view order list
		List<String> skus = null;
		if ((Boolean) gSiteConfig.get( "gMYORDER_LIST" )) {
			
				List myOrderList = new ArrayList<Product>();
				skus = this.webJaguar.getSKUsByUserId( userSession.getUserid() );
				for ( String sku : skus )
				{
					if ( !("".equals(sku)) ) {
						Product product = this.webJaguar.getProductById( this.webJaguar.getProductIdBySku( sku ), request );
						if (product != null) {
							myOrderList.add( product );
						}
					}
				}
				if (!myOrderList.isEmpty()) {	// not empty
					PagedListHolder productList = 
						new PagedListHolder(myOrderList);
					productList.setPageSize(10);
					String page = request.getParameter("page");
					if (page != null) {
						productList.setPage(Integer.parseInt(page)-1);
					}
					myModel.put( "myOrderProducts", productList );							
					myModel.put( "products", productList.getPageList() );
				}

		}
		
		Layout layout = null;
		layout = this.webJaguar.getSystemLayout("myorderlist", request.getHeader("host"), request);
		if(layout != null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#pageName#", "My Orders"));
		}
		if(layout == null){
			layout = (Layout) request.getAttribute("layout");
		}
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		request.setAttribute("layout", layout);
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		if(((Map<String, Configuration>)siteConfig).get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/account/myOrderList", "model", myModel);
		}
		return new ModelAndView("frontend/account/myOrderList", "model", myModel);    	
    }

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

}
