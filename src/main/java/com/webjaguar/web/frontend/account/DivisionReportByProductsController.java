/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.19.2009
 */

package com.webjaguar.web.frontend.account;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.BudgetProduct;
import com.webjaguar.model.BudgetProductSearch;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class DivisionReportByProductsController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws Exception {	

    	UserSession userSession = this.webJaguar.getUserSession(request);	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		int custId = userSession.getUserid();
		if ((Boolean) gSiteConfig.get("gSUB_ACCOUNTS") && request.getParameter("id") != null
				&& !this.webJaguar.getCustomerById(userSession.getUserid()).isHideSubAccts()) {
			custId = ServletRequestUtils.getIntParameter(request, "id", userSession.getUserid());
			boolean valid = false;
			for (Customer node: this.webJaguar.getFamilyTree(custId, "up")) {
				// check if a valid sub-account
				if (node.getId().compareTo(userSession.getUserid()) == 0) {
					valid = true;
					break;
				}
			}
			if (valid) {
				model.put("subAccount", this.webJaguar.getCustomerById(custId));
			} else {
				custId = userSession.getUserid();
			}
		}		

		// current year
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		model.put("currentYear", currentYear);
		
		int year = ServletRequestUtils.getIntParameter(request, "year", currentYear);
		model.put("year", year);
		
		// sub accounts
		CustomerSearch search = new CustomerSearch();
		search.setParent(custId);
		List<Customer> subAccounts = this.webJaguar.getCustomerListWithDate(search);
		model.put("subAccounts", subAccounts);
		
		Layout layout = (Layout) request.getAttribute("layout");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		
		// get budget product per sub account
		BudgetProductSearch bProdSearch = new BudgetProductSearch();
		bProdSearch.setYear(year);
		bProdSearch.setParent(custId);
		List<BudgetProduct> budgetProductList = this.webJaguar.getBudgetProductList(bProdSearch);
		Map<String, BudgetProduct> budgetProductMap = new TreeMap<String, BudgetProduct>();
		Map<Integer, Map<String, BudgetProduct>> subAccountMap = new HashMap<Integer, Map<String, BudgetProduct>>();		
		for (BudgetProduct budgetProduct: budgetProductList) {
			String sku = budgetProduct.getProduct().getSku();
			if (budgetProductMap.containsKey(sku)) {
				budgetProductMap.get(sku).addOrdered(budgetProduct.getOrdered());
			} else {
				budgetProductMap.put(sku, new BudgetProduct(budgetProduct.getProduct(), budgetProduct.getOrdered()));
			}
			if (!subAccountMap.containsKey(budgetProduct.getUserId())) {
				subAccountMap.put(budgetProduct.getUserId(), new HashMap<String, BudgetProduct>());
			}
			subAccountMap.get(budgetProduct.getUserId()).put(budgetProduct.getProduct().getSku(), budgetProduct);
		}
		model.put("budgetProducts", budgetProductMap.values());
		
		List<Integer> years = this.webJaguar.getBudgetProductYears(bProdSearch);
		if (years.isEmpty() || years.get(0) != currentYear) {
			years.add(0, currentYear);
		}
		model.put("years", years);
		
		model.put("subAccountMap", subAccountMap);
		
		return new ModelAndView("frontend/account/dReportByProducts", "model", model);    	
    }
    
}
