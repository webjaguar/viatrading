package com.webjaguar.web.frontend.account;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Stack;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.GetMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ezic.net.URL;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.thirdparty.sugarSync.SugarSync;
import com.webjaguar.thirdparty.sugarSync.SugarSyncAPI;

public class SugarSyncController extends SimpleFormController {

	public String subFolderXml = null;
	public static String subFolderXml2 = null;
	public String accessToken = null;
	public Stack<String> st = new Stack<String>();

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private Properties site;

	public void setSite(Properties site) {
		this.site = site;
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> myModel = new HashMap<String, Object>();

		SugarSyncAPI sugarSyncApi = new SugarSyncAPI(siteConfig);
		if (request.getParameter("back") != null) {
			SugarSync subSugarSync = new SugarSync();
			subSugarSync.setAccessToken(accessToken);
			st.pop();
			if (st.isEmpty()) {
				st.push("0");
			}
			String values = (String) st.pop();

			if (values != null && !values.isEmpty() && values.equalsIgnoreCase("0")) {
				return new ModelAndView(new RedirectView("account_sugarSync.jhtm"));
			} else {
				return new ModelAndView(new RedirectView("account_sugarSync.jhtm?sub=true&refText=" + values));
			}
		}
		if (request.getParameter("sub") == null && request.getParameter("fileDownload") == null) {
			SugarSync sugarSync = new SugarSync();
			sugarSyncApi.method("list", sugarSync);
			changeNodeValues(sugarSync.getResponseBody(), null, null, sugarSync, myModel);
			myModel.put("subFolders", sugarSync.getFolderNames());
			myModel.put("back", false);
			st.push("0");
			subFolderXml = sugarSync.getResponseBody();
			accessToken = sugarSync.getAccessToken();
		} else if (request.getParameter("sub") != null) {
			SugarSync subSugarSync = new SugarSync();
			subSugarSync.setResponseBody(subFolderXml);
			subSugarSync.setAccessToken(accessToken);
			if (!st.isEmpty() && !(st.peek().equals(request.getParameter("refText")))) {
				st.push(request.getParameter("refText"));
			}
			subSugarSync.setUrl(request.getParameter("refText"));
			sugarSyncApi.handleListCommand(subSugarSync, false);
			changeNodeValues(subSugarSync.getResponseBody(), request.getParameter("refText"), request.getParameter("fileData"), subSugarSync, myModel);
			myModel.put("subFolders", subSugarSync.getFolderNames());
		} else if (request.getParameter("fileDownload") != null && request.getParameter("fileDownload").equalsIgnoreCase("true")) {

			SugarSync fileDownload = new SugarSync();
			boolean success;

			fileDownload.setUrl(request.getParameter("fileData"));
			fileDownload.setAccessToken(accessToken);

			String siteRoot = site.getProperty("site.root");
			File baseFile = new File(siteRoot);

			File sugarSyncFolder = new File(baseFile, "sugarSync");
			if (!sugarSyncFolder.exists()) {
				sugarSyncFolder.mkdir();
			} else {
				// clear old files
				for (File file : sugarSyncFolder.listFiles()) {
					file.delete();
				}
			}

			try {

				HttpClient client = new HttpClient();
				GetMethod get = new GetMethod(fileDownload.getUrl());
				get.setRequestHeader("Authorization", accessToken);
				get.setRequestHeader("User-Agent", "Webjaguar");
				client.executeMethod(get);
				InputStream in = get.getResponseBodyAsStream();

				File newFile = new File(sugarSyncFolder.getAbsolutePath(), request.getParameter("fileName").split("_")[0]);
				FileOutputStream out = new FileOutputStream(newFile);
				byte[] b = new byte[(int) get.getResponseContentLength()];
				int len = 0;
				while ((len = in.read(b)) != -1) {
					out.write(b, 0, len);
					out.flush();
				}
				in.close();
				out.close();
				// redirect to the downloaded file.
				response.setStatus(301);
				response.setHeader("Location", request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/sugarSync/" + request.getParameter("fileName").split("_")[0]);
				response.setHeader("Connection", "close");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		myModel.put("sugarSyncLayout", this.webJaguar.getSystemLayout("sugarSync", request.getHeader("host"), request));
		return new ModelAndView("frontend/account/sugarSync", "model", myModel);
	}

	public void changeNodeValues(String xmlString, String refNew, String fileDataNew, SugarSync sugarSync, Map<String, Object> myModel) throws XPathExpressionException {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(xmlString));
			Document doc = db.parse(is);

			Element root = doc.getDocumentElement();
			NodeList rootlist = root.getChildNodes();
			List<String> names = new ArrayList<String>();
			myModel.put("length", rootlist.getLength());
			for (int i = 0; i < rootlist.getLength(); i++) {
				Element collection = (Element) rootlist.item(i);

				if (collection.getNodeName().equalsIgnoreCase("collection")) {
					NodeList collectionList = collection.getChildNodes();
					// name
					Element name = (Element) collectionList.item(0);
					NodeList nameList = name.getChildNodes();
					Text nameText = (Text) nameList.item(0);
					names.add(nameText.getData());

					// ref tag
					Element ref = (Element) collectionList.item(1);
					NodeList refList = ref.getChildNodes();
					Text refText = (Text) refList.item(0);
					// unique name on jsp
					URL refUrl = new URL(refText.getData());
					if (refUrl.getPathFile().split("_").length > 1) {
						myModel.put("name_" + i, refUrl.getPathFile().split("_")[1]);
					} else {
						myModel.put("name_" + i, i);
					}
					myModel.put("refText_" + i, refText.getData());
					if (refNew != null) {
						refText.setData(refNew);
					}
				}
				if (collection.getNodeName().equalsIgnoreCase("file")) {
					NodeList collectionList = collection.getChildNodes();
					// name
					Element name = (Element) collectionList.item(0);
					NodeList nameList = name.getChildNodes();
					Text nameText = (Text) nameList.item(0);
					names.add(nameText.getData() + "_fileName");

					// ref tag
					Element ref = (Element) collectionList.item(6);
					NodeList refList = ref.getChildNodes();
					Text fileData = (Text) refList.item(0);
					// unique name on jsp
					URL fileUrl = new URL(fileData.getData());
					if (fileUrl.getPathFile().split("_").length > 1) {
						String[] split = fileUrl.getPathFile().split("_")[1].split("/");
						myModel.put("name_" + i, split[0]);
					} else {
						myModel.put("name_" + i, i);
					}

					myModel.put("fileData_" + i, fileData.getData());
					if (fileDataNew != null) {
						fileData.setData(fileDataNew);
					}
				}
			}
			sugarSync.setFolderNames(names);
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (SAXException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}