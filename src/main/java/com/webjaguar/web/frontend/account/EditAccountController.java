/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.web.form.CustomerForm;

public class EditAccountController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private Map<String, Object> gSiteConfig;
	Map<String, Configuration> siteConfig;

	public EditAccountController() {
		setSessionForm(true);
		setCommandName("customerForm");
		setCommandClass(CustomerForm.class);
		setFormView("frontend/account/edit");
		setSuccessView("account.jhtm");
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException {

    	CustomerForm customerForm = (CustomerForm) command;
    	
		// check if cancel button was pressed
		if (request.getParameter("_cancel") == null) {
			this.webJaguar.updateCustomer(customerForm.getCustomer(), 
					!customerForm.getCustomer().getUsername().equalsIgnoreCase(customerForm.getExistingUsername()),
					customerForm.getCustomer().getPassword().length() > 0);
			
			if( (Boolean) gSiteConfig.get("gCRM") && customerForm.getCustomer().getCrmContactId() != null ) {
				CrmContact crmContact = this.webJaguar.getCrmContactById( customerForm.getCustomer().getCrmContactId() );
    			if(crmContact != null) {
    				try {
    					this.webJaguar.customerToCrmContactSynch(crmContact, customerForm.getCustomer());
    				} catch (Exception e) { e.printStackTrace();}
    				this.webJaguar.updateCrmContact(crmContact);
    			} 
    		}
		}
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			setFormView("frontend/layout/template6/account/edit");
		}

		return new ModelAndView(new RedirectView(getSuccessView()));    	
    }
    
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}

    protected Map referenceData(HttpServletRequest request) 
			throws Exception {
	
       	Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
	    
		Layout layout = null;
		layout = this.webJaguar.getSystemLayout("changeEmailPassword", request.getHeader("host"), request);
		if(layout != null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#pageName#", "Change Email or Password"));
			System.out.println(layout.getHeaderHtml());
		}
		if(layout == null){
			layout = (Layout) request.getAttribute("layout");
		}
		
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		this.webJaguar.updateCartQuantity(null, request, layout);
		request.setAttribute("layout", layout);
		map.put("model", model); 
	    return map;
    }  

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		CustomerForm customerForm = new CustomerForm((Customer) request.getAttribute("sessionCustomer"));
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		if(siteConfig!=null && siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			setFormView("frontend/layout/template6/account/edit");
		}
		return customerForm;
	}

	protected void onBindAndValidate(HttpServletRequest request,
            Object command, BindException errors) throws Exception {
		CustomerForm customerForm = (CustomerForm) command;

		if (siteConfig.get("CURRENT_PASSWORD_CHECK").getValue().equals("true") && this.webJaguar.getCustomerByUsernameAndPassword(customerForm.getExistingUsername(), customerForm.getCurrentPassword()) == null) {
			errors.rejectValue("currentPassword", "PASSWORD_NO_MATCH", "Any changes to your account requires that you enter your current password here.");   
		}		
		
		if (!customerForm.getCustomer().getUsername().equalsIgnoreCase(customerForm.getExistingUsername()) &&
				this.webJaguar.getCustomerByUsername(customerForm.getCustomer().getUsername()) != null) {
			errors.rejectValue("customer.username", "USER_ID_ALREADY_EXISTS", "An account using this email address already exists.");    					
		}		
	}
}
