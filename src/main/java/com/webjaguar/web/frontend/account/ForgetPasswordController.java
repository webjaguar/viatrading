/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.account;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.ForgetPasswordForm;

public class ForgetPasswordController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	private Map<String, Configuration> siteConfig;
    
	public ForgetPasswordController() {
		setSessionForm(false);
		setCommandName("forgetPasswordForm");
		setCommandClass(ForgetPasswordForm.class);
		setPages(new String[] {"frontend/password/step1", "frontend/password/step2", "frontend/password/step3"});
	}	
	
    public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {	
	    	ForgetPasswordForm fpForm = (ForgetPasswordForm) command;
	    	
	    	if (this.webJaguar.validToken(fpForm.getToken())) {
	    		this.webJaguar.updatePassword(fpForm.getNewPassword(), fpForm.getToken());
	        	return new ModelAndView(new RedirectView("account.jhtm"));
	    	} else {
			errors.rejectValue("message", "PLEASE_TRY_AGAIN", null, 
				"We're sorry. Please try again."); 	    		
	    		return showPage(request, errors, 0);
	    	}
		
    }
 
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
    	throws Exception { 	
    	
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );

    	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));	
		map.put("model", model);   
		
		Layout layout = (Layout) request.getAttribute("layout");
		
		
		//it will use default layout head tag if ForgetPassword page's layout is null/blank
		Layout layoutNew = this.webJaguar.getSystemLayout("forgetPassword", request.getHeader("host"), request);
		if(layoutNew != null)
			if(layoutNew.getHeadTag() != null || !layoutNew.getHeadTag().equals(""))
				layout.setHeadTag(layoutNew.getHeadTag());
		model.put("pageName", "forgetPasswordPage"); 
		
		if(layout.getTopBarHtml() != null) {
			layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Reset Password"));
			layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#PAGENAME#", "Reset Password"));
		}
			
		
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) 
				|| siteConfig.get("PROTECTED_HOST").getValue().equals(request.getHeader("host")))
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		if (siteConfig.get("PROTECTED_HOST").getValue().equals(request.getHeader("host"))) {
			layout.setRightBarTopHtml( "" );
			layout.setRightBarBottomHtml( "" );				
		}
		this.webJaguar.updateCartQuantity(null, request, layout);
		model.put("SITE_NAME", siteConfig.get("SITE_NAME").getValue());
		model.put("CONTACT_EMAIL", siteConfig.get("CONTACT_EMAIL").getValue());
		
		// multi store
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			model.put("SITE_NAME", multiStores.get(0).getHost());
			model.put("CONTACT_EMAIL", multiStores.get(0).getContactEmail());
		}
		
		return map;
    }
	
	protected int getInitialPage(HttpServletRequest request, Object command) {
    		ForgetPasswordForm fpForm = (ForgetPasswordForm) command;
    		
    		String token = request.getParameter("t");
		if (token != null && this.webJaguar.validToken(token)) {
			fpForm.setToken(token);
			return 2;
		}
		return 0;
	}
	
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );	
		
		ForgetPasswordForm forgetPasswordForm = new ForgetPasswordForm();
		
		// Mobile Layout
		String mobile = "";
		if ((request.getAttribute("_mobile").toString().equalsIgnoreCase("_mobile"))) {
			mobile = request.getAttribute("_mobile").toString();
		}
		forgetPasswordForm.setMobile(mobile);
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			setPages(new String[] {"frontend/layout/template6/password/step1"+mobile, "frontend/layout/template6/password/step2"+mobile, "frontend/layout/template6/password/step3"+mobile});
		}else {
			setPages(new String[] {"frontend/password/step1"+mobile, "frontend/password/step2"+mobile, "frontend/password/step3"+mobile});
		}

		return forgetPasswordForm;
	}
	
    protected void validatePage(Object command, Errors errors, int page, boolean finish) {
    	ForgetPasswordForm forgetPasswordForm = (ForgetPasswordForm) command;

    	if (page == 0 && !finish) {
    		// email
    		if (validateEmail("email", forgetPasswordForm.getEmail(), errors)) {
    	    	
    			// check email against database
    			Customer customer = this.webJaguar.getCustomerByUsername(forgetPasswordForm.getEmail());
    			if (customer == null) {
    				errors.rejectValue("email", "asterisk", "*"); 
    				Object [] args = { "" };
    				errors.rejectValue("message", "EMAIL_DOESNT_EXISTS", args, 
    						"We're sorry. There are no accounts associated with this email address."); 
    			}
    		}
    	}
    	if (page == 2) {
			// password
    		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword", "form.required", "required");
    		if (!forgetPasswordForm.getNewPassword().equals(forgetPasswordForm.getConfirmPassword())) {
    			errors.rejectValue("confirmPassword", "form.passwordMismatch", "matching passwords required");
    		}
    		if (forgetPasswordForm.getNewPassword().length() < 5) {
    			Object[] errorArgs = { new Integer(5) };
    			errors.rejectValue("newPassword", "form.charLessMin", errorArgs, "should be at least 5 chars");
    		}
    	}
    }
    
    private boolean validateEmail(String fieldName, String email, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, fieldName, "form.required", "required");    	
		
		if (!Constants.validateEmail(email, null)) {
			errors.rejectValue(fieldName, "form.invalidEmail", "invalid email");
			return false;
		}
		return true;
    }
    
	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		ForgetPasswordForm forgetPasswordForm = (ForgetPasswordForm) command;

		switch (page) {
			case 0:
				if (!errors.hasErrors()) {
					
	    	    	String siteName = siteConfig.get("SITE_NAME").getValue();
	    	    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
	    	    	String siteUrl = siteConfig.get("SITE_URL").getValue();
	    	    	
	    			// multi store
	    			Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
	    			List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
	    			if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
	    				siteName = multiStores.get(0).getHost();
	    				contactEmail = multiStores.get(0).getContactEmail();
	    				siteUrl = request.getScheme()+"://" + multiStores.get(0).getHost() + request.getContextPath() + "/";
	    			}
	    	    	
	    	    	Customer customer = this.webJaguar.getCustomerByUsername(forgetPasswordForm.getEmail());
	    	    	
    				// create password token
    				String token = this.webJaguar.createPasswordToken(customer.getId());
    				// send email
    				SimpleMailMessage msg = new SimpleMailMessage();
    				msg.setTo(forgetPasswordForm.getEmail());
    				msg.setFrom( contactEmail );
    				msg.setBcc( contactEmail );
    				msg.setSubject(siteName + " Password Assistance");
    				msg.setText("Greetings from " + siteName + ".\n\n" +
    							"Click the link below to go to Step 3 to reset your password:\n" +
    							siteUrl + "forgetPassword.jhtm?t="+ token + "\n\n" +
    							"Thank you for visiting " + siteName + "!");
    				try {
    					mailSender.send(msg);
    				} catch (MailException ex) {
    					System.err.println(ex.getMessage());
    				}
				}
				break;
		}
	}
	
}
