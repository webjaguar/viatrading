/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.20.2006
 */

package com.webjaguar.web.frontend.account;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerBudgetPartner;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Product;
import com.webjaguar.model.State;
import com.webjaguar.model.UserSession;

public class ViewInvoiceController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private MailSender mailSender;
    public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
    
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
    	
    	UserSession userSession = this.webJaguar.getUserSession(request);
		Map<String, Object> model = new HashMap<String, Object>();
		int orderId = 0;
		if (request.getParameter("order") != null) {
			orderId = ServletRequestUtils.getIntParameter(request, "order", 0);
		} else if (request.getParameter("quote") != null) {
			orderId = ServletRequestUtils.getIntParameter(request, "quote", 0);
			model.put("quote", true);
		}
		Order order = this.webJaguar.getOrder(orderId, siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());
		//set tax on shipping
		State state = this.webJaguar.getStateByCode(order.getShipping().getCountry(), order.getShipping().getStateProvince());
		if (state != null) {
			order.setTaxOnShipping(state.isApplyShippingTax());
		}
				
		boolean showChildInvoice = siteConfig.get( "SHOW_CHILD_INVOICE" ).getValue().equals( "true" );

		boolean showInvoice = false;
		if (order != null) {
			order.setupDueDate(order.getTurnOverday(), order.getShippingPeriod());
			
			if (order.getUserId().compareTo(userSession.getUserid()) == 0) {
				model.put( "showPaymentHistory", true );
				if (order.getStatusHistory() != null) {
					List trackNumList = new ArrayList();
					for (OrderStatus orderStatus:order.getStatusHistory()) {
						if (orderStatus.getTrackNum() != null) {
							trackNumList.add( orderStatus.getTrackNum() );
							model.put("hasTrackNum", true);
						}
						model.put("trackNumList", trackNumList);
					}
				}
				showInvoice = true;
			} 
			if (showChildInvoice && !showInvoice && order.getAffiliate() != null) {
				int level = (Integer) gSiteConfig.get( "gAFFILIATE" );
				if (level == 1 && order.getAffiliate().getAffiliateLevel1Id() != null &&
							order.getAffiliate().getAffiliateLevel1Id().compareTo(userSession.getUserid()) == 0) {
					showInvoice = true;					
				}
				if (showChildInvoice && level == 2 && ((order.getAffiliate().getAffiliateLevel1Id() != null && 
						order.getAffiliate().getAffiliateLevel1Id().compareTo(userSession.getUserid()) == 0)
						|| (order.getAffiliate().getAffiliateLevel2Id() != null && 
								order.getAffiliate().getAffiliateLevel2Id().compareTo(userSession.getUserid()) == 0))) {
					showInvoice = true;
				} 
			}

			List<Integer> parentsList = new ArrayList();
			if ((Boolean) gSiteConfig.get( "gSUB_ACCOUNTS" ) && !showInvoice 
					&& !this.webJaguar.getCustomerById(userSession.getUserid()).isHideSubAccts()) {
				for (Customer node: this.webJaguar.getFamilyTree( order.getUserId(), "up" )) {
						parentsList.add(node.getId());
				}
				
				for (Integer node: parentsList) {
					// check if a valid sub-account
					if (node.compareTo(userSession.getUserid()) == 0) {
						showInvoice = true;
						break;
					}
				}				
			}
			// Budget Earned Credits
			if( (Boolean) gSiteConfig.get("gBUDGET") && siteConfig.get("BUDGET_ADD_PARTNER").getValue().equalsIgnoreCase("true")) {
				CustomerBudgetPartner partner = new CustomerBudgetPartner();		
				Customer customer = this.webJaguar.getCustomerById(order.getUserId());
				partner.setOrderId(order.getOrderId());
				partner.setUserId(order.getUserId());
				List<CustomerBudgetPartner> partnerList = this.webJaguar.getCustomerPartnerHistory(partner);
				if(partnerList != null && !partnerList.isEmpty()) {
					model.put("partnerList", partnerList);
				}
				Double value = null;
				if(order.getBudgetEarnedCredits() != null && customer.getBudgetPlan() != null && customer.getBudgetPlan().equalsIgnoreCase("plan-b")){				
					value= order.getSubTotal() - order.getBudgetEarnedCredits();
				} else {
					value= order.getSubTotal();
				}
				model.put("subTotalByPlan", value);
			}
			// Approval
			if ( (Boolean) gSiteConfig.get( "gINVOICE_APPROVAL" ) && request.getParameter( "_submit") != null ) {
				int approvalStatus = ServletRequestUtils.getIntParameter(request, "_approvalStatus", 0);
				String approvaleNote = ServletRequestUtils.getStringParameter(request, "_actionNote", null);
				if ( approvalStatus == 1 ) {
					order.setApproval( "ap" );
				} else if(approvalStatus == 2){
					order.setApproval( "de" );						
				}
				notifyCustomer(siteConfig, order,  approvaleNote);
				this.webJaguar.updateOrderApproval( order );
			}
			if ( (Boolean) gSiteConfig.get( "gINVOICE_APPROVAL" ) && request.getParameter( "_submitQuote") != null ) {
				int approvalStatus = ServletRequestUtils.getIntParameter(request, "_approvalStatus", 0);
				String approvaleNote = ServletRequestUtils.getStringParameter(request, "_actionNote", null);
				if ( approvalStatus == 1  || approvalStatus == 2) {
					if((Boolean) gSiteConfig.get("gBUDGET")) {
						for (Integer node:parentsList) {
							//Check if user exists in the tree. 
							if(node.compareTo(userSession.getUserid()) == 0) {
								// Make entry into the orders approval table
								order.setActionBy(node);
								order.setActionNote(approvaleNote);
								if (approvalStatus == 1 ){
									order.setActionType("Approved");
								} else {
									order.setActionType("Denied");									
								}
								this.webJaguar.insertOrderAprrovalDenialHistory(order, parentsList, mailSender, siteConfig);
							}
						}
					} else if(approvalStatus == 1){
						order.setApproval( "ap" );
					} else if(approvalStatus == 2){
						order.setApproval( "de" );						
					}
				}
				if(!approvaleNote.isEmpty() || approvalStatus == 1) {
					notifyCustomer(siteConfig, order,  approvaleNote);
				}
				model.put("quote", true);
				this.webJaguar.updateOrderApproval( order );				
			}
		}
		
		// check user approval
		if((Boolean) gSiteConfig.get( "gBUDGET" )  && this.webJaguar.getApproval(order.getOrderId(), userSession.getUserid(), false)) {
			model.put("quoteApproval", "This order is successfully approved by you. ");
		} else if((Boolean) gSiteConfig.get( "gBUDGET" )  && this.webJaguar.getApproval(order.getOrderId(), userSession.getUserid(), true)){
			model.put("quoteApproval", "This order is denied by you. ");			
		} else {
			model.put("quoteApproval", null);
		}
		
		if (showInvoice) {
			model.put("order", order);
			model.put("productFieldsHeader", this.webJaguar.getProductFieldsHeader(order, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"))); 		
			Customer customer = this.webJaguar.getCustomerById(order.getUserId());
			if((Boolean) gSiteConfig.get( "gBUDGET" )) {
				// If creditAllowed is empty get value from global
				if(customer.getCreditAllowed() ==  null && siteConfig.get("BUDGET_PERCENTAGE").getValue() != null && !siteConfig.get("BUDGET_PERCENTAGE").getValue().isEmpty() ) {
					customer.setCreditAllowed(Double.parseDouble(siteConfig.get("BUDGET_PERCENTAGE").getValue()));
				}
			}
			model.put("customer", customer);
    		if (order.getWorkOrderNum() != null) {
        		model.put("workOrder", this.webJaguar.getWorkOrder(order.getWorkOrderNum()));    			
    		}
			model.put( "countries", this.webJaguar.getCountryMap() );			
			// multi store
			if (request.getParameter("order") != null  ) {
				model.put("invoiceLayout", this.webJaguar.getSystemLayout("invoice", order.getHost(), request));
			} else if (request.getParameter("quote") != null ) {
				model.put("quoteLayout", this.webJaguar.getSystemLayout("quotelayout", order.getHost(), request));
			}
			
			this.updateMasterSkus(order, model, customer);
		} else {
      	  	model.put("message", "order.exception.notfound");			
		}
    	
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/account/invoice", model);
		} 
		return new ModelAndView("frontend/account/invoice", model);
	}
    
	
	
	private boolean notifyCustomer(Map<String, Configuration> siteConfig, Order order, String approvaleNote) {

		Customer customer = this.webJaguar.getCustomerById(order.getUserId());
		
		if (!customer.isEmailNotify()) {
			// do not send email
			return false;
		}
		
		// send email
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	
    	//multiStore
    	List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
		MultiStore multiStore = null;
		if (multiStores.size() > 0) {
			multiStore = multiStores.get(0);
		}
    	String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

    	StringBuffer messageBuffer = new StringBuffer();
    	messageBuffer.append( dateFormatter.format(new Date()) + "\n\n" );

    	messageBuffer.append( "The Order has been updated.\n Please check your order at \n" + secureUrl + "/invoice.jhtm?order=" + order.getOrderId() + "\n\n ".concat( approvaleNote ) );
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(customer.getUsername());
		msg.setFrom( contactEmail );
		msg.setBcc( contactEmail );
		msg.setSubject( "order #" + order.getOrderId() + " " +  siteConfig.get("SITE_NAME").getValue());
		msg.setText( messageBuffer.toString() );
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
		return true;
	}

	private void updateMasterSkus(Order order, Map<String, Object> model, Customer customer){
		Map<String, CartItem> groupItems = null;
		
		for(LineItem lineItem : order.getLineItems()) {
			if(groupItems == null){
				groupItems = new HashMap<String, CartItem>();
			}
			if(lineItem.getItemGroup() != null){
				CartItem ci = null;
				if(groupItems.get(lineItem.getItemGroup()) != null) {
					ci = groupItems.get(lineItem.getItemGroup());
					ci.setTempItemTotal(ci.getTempItemTotal() + lineItem.getTotalPrice());
				} else {
					ci = new CartItem();
					ci.setTempItemTotal(lineItem.getTotalPrice());
				}
				if(ci.getProduct().getSku() == null) {
					Product product = this.webJaguar.getProductById(lineItem.getProduct().getId(), 0 ,false, customer);
					if(product.getMasterSku() != null) {
						Integer masterProductId = this.webJaguar.getProductIdBySku(product.getMasterSku());
						if(masterProductId != null) {
							Product parentProduct = this.webJaguar.getProductById(masterProductId, 0, false, null);
							ci.setProduct(parentProduct);
						}	
					}
				}
				
				// add quantity only if item is main item and not decorative (added) sku
				if(lineItem.isItemGroupMainItem()) {
					ci.setQuantity(ci.getQuantity() + lineItem.getQuantity());
				}
				groupItems.put(lineItem.getItemGroup(), ci);
			}
		}
		if(model != null) {
			model.put("groupItems", groupItems);
			model.put("viewInvoice", true);
		}
	}

}