/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.web.domain.Constants;

public class ProductSearchController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private String group;

	public void setGroup(String group) {
		this.group = group;
	}

	Map<String, Configuration> siteConfig;
	Map<String, Object> gSiteConfig;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Object> model = new HashMap<String, Object>();

		ProductSearch search = getProductSearch(request, model);
		if (search.getConjunction() == null) {
			search.setConjunction(siteConfig.get("SEARCH_DEFAULT_KEYWORDS_CONJUNCTION").getValue());
		}
		search.setDelimiter(siteConfig.get("SEARCH_KEYWORDS_DELIMITER").getValue());
		search.setKeywordsMinChars(Integer.parseInt(siteConfig.get("SEARCH_KEYWORDS_MINIMUM_CHARACTERS").getValue()));
		search.setgPRODUCT_FIELDS((Integer) gSiteConfig.get("gPRODUCT_FIELDS"));

		// search.setSort(siteConfig.get("SEARCH_SORT").getValue());

		if ((Boolean) gSiteConfig.get("gMASTER_SKU") && Integer.parseInt(siteConfig.get("PARENT_CHILDERN_DISPLAY").getValue()) == 0) {
			search.setMasterSku(true);
		}

		String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();

		if (siteConfig.get("LEFTBAR_DEFAULT_TYPE").getValue().equals("3")) {
			model.put("mainCategories", this.webJaguar.getMainsAndSubsCategoryLinks(request));
		} else {
			model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request, null));
		}
		model.put("url", url + "?" + query);
		// continue shopping
		request.getSession().setAttribute("continueShoppingUrl", model.get("url"));

		Customer customer = this.webJaguar.getCustomerByRequest(request);

		// Show add presentation only when salesrep cookie is there.
		if ((Boolean) gSiteConfig.get("gPRESENTATION")) {
			model.put("showAddPresentation", (request.getAttribute("cookieSalesRep") != null && customer == null) ? true : false);
		}

		// search fields
		if (siteConfig.get("SEARCH_FIELDS").getValue().trim().length() > 0 && !siteConfig.get("SEARCH_FIELDS").getValue().equalsIgnoreCase("keywords")) {
			search.setSearchFields(new ArrayList<String>());
			for (String field : siteConfig.get("SEARCH_FIELDS").getValue().split(",")) {
				search.getSearchFields().add(field);
			}
		} else {
			search.setSearchFields(null);
		}

		// check number of characters
		boolean minCharsMet = false;
		if (search.getKeywords().trim().length() > 0) {
			for (String token : search.getKeywords().split(" ")) {
				if (token.length() >= search.getKeywordsMinChars()) {
					minCharsMet = true;
				} else {
					model.put("hasKeywordsLessMinChars", true);
				}
			}
		} else if (search.getKeywordsMinChars() == 0) {
			minCharsMet = true;
		}
		model.put("minCharsMet", minCharsMet);

		// customer price table for searching purposes
		if (customer != null) {
			search.setPriceTable(customer.getPriceTable());
		}

		int count = 0;
		if (minCharsMet) {
			count = this.webJaguar.searchProductsCount(search, request, customer);
		}
		if (count < search.getOffset() - 1) {
			search.setPage(1);
		}

		if (minCharsMet && count < 1 && !((Configuration) siteConfig.get("PRODUCT_SEARCH_NOT_FOUND_URL")).getValue().isEmpty()) {
			return new ModelAndView(new RedirectView(siteConfig.get("PRODUCT_SEARCH_NOT_FOUND_URL").getValue()));
		}

		search.setLimit(search.getPageSize());
		search.setOffset((search.getPage() - 1) * search.getPageSize());

		List<Product> productList = new ArrayList<Product>();
		boolean basedOnHand = false;
		if (siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly")) {
			basedOnHand = true;
		}
		search.setBasedOnHand(basedOnHand);
		if (minCharsMet) {
			productList = this.webJaguar.searchProducts(search, request, customer);
		}

		int pageCount = count / search.getPageSize();
		if (count % search.getPageSize() > 0) {
			pageCount++;
		}

		model.put("pageCount", pageCount);
		model.put("pageEnd", search.getOffset() + productList.size());
		model.put("count", count);

		boolean showPriceColumn = false;
		boolean showQtyColumn = false;
		boolean setProductQuoteColumn = false;
		for (Product product : productList) {
			if (!showPriceColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) || product.getMsrp() != null)) {
				showPriceColumn = true;
			}
			if (!showQtyColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) && (!product.isLoginRequire() || (product.isLoginRequire() && customer != null)))) {
				showQtyColumn = true;
			}
			// ProductQuoteColumn
			if (siteConfig.get("PRODUCT_QUOTE").getValue().equals("true")) {
				if (product.isQuote()) {
					setProductQuoteColumn = true;
				}
			}
			// i18n on WebImpl (ProductSearch)
			// master sku
			if ((Boolean) gSiteConfig.get("gMASTER_SKU")) {
				this.webJaguar.setMasterSkuDetails(product, search.getProtectedAccess(), (Boolean) gSiteConfig.get("gINVENTORY"));
				if (product.getSlaveCount() != null && !showPriceColumn) {
					showPriceColumn = true;
				}

			}
		}
		model.put("showPriceColumn", showPriceColumn);
		model.put("showQtyColumn", showQtyColumn);

		// QUOTE
		if (siteConfig.get("PRODUCT_QUOTE").getValue().equals("true")) {
			model.put("showProductQuoteColumn", setProductQuoteColumn);
		}

		// Hide empty colums from list
		boolean hideEmptyColum = true;
		if (siteConfig.get("SEARCH_DISPLAY_MODE").getValue().equals("quick2")) {
			hideEmptyColum = false;
		}

		// superior washer
		if (siteConfig.get("GROUP_SEARCH").getValue().equals("true") && (this.group != null && group.endsWith("1"))) {
			Map<String, List<ProductField>> groupListMap = new HashMap<String, List<ProductField>>();
			Map<String, List<Product>> groupProductMap = new HashMap<String, List<Product>>();

			for (Product product : productList) {
				if (product.getField50() == null && groupProductMap.get("No Group") != null) {
					if (groupProductMap.get("No Group").size() < 10) {
						groupProductMap.get("No Group").add(product);
					}
				} else if (product.getField50() != null && groupProductMap.get(product.getField50()) != null) {
					if (groupProductMap.get(product.getField50()).size() < 10) {
						groupProductMap.get(product.getField50()).add(product);
					}
				} else {
					List<Product> tempList = new ArrayList<Product>();
					tempList.add(product);
					groupProductMap.put(product.getField50() != null ? product.getField50() : "No Group", tempList);
				}
			}

			for (String key : groupProductMap.keySet()) {
				groupListMap.put(key, this.webJaguar.getProductFields(request, groupProductMap.get(key), hideEmptyColum));
			}
			model.put("searchAction", request.getContextPath() + "/gsearch.jhtm");
			model.put("productFieldsMap", groupListMap);
			model.put("groupProductsMap", groupProductMap);
		} else {
			model.put("productFields", this.webJaguar.getProductFields(request, productList, hideEmptyColum));
			model.put("searchAction", request.getContextPath() + "/search.jhtm");
		}

		model.put("products", productList);

		// search tree
		List<Category> searchTree = new ArrayList<Category>();
		Integer categoryId = null;
		// multi store
		Integer hostCategoryId = this.webJaguar.getHomePageByHost(request.getHeader("host"));
		if (hostCategoryId != null) {
			if (this.webJaguar.getCategoryById(hostCategoryId, "").isShowSubcats()) {
				categoryId = hostCategoryId;
			}
		}
		for (Category category : this.webJaguar.getCategoryTree(categoryId, search.getProtectedAccess(), true, true, null, null)) {
			if (categoryId != null) {
				traverseSearchTree(searchTree, category, 0);
			} else {
				searchTree.add(category);
				traverseSearchTree(searchTree, category, 1);
			}
		}
		if (searchTree.size() > 0) {
			model.put("searchTree", searchTree);
		}
		// set layout
		setLayout(request, model);

		// comparison
		if ((Boolean) gSiteConfig.get("gCOMPARISON")) {
			List<Integer> comparisonList = (List) request.getSession().getAttribute("comparisonList");
			Map<Integer, Object> comparisonMap = new HashMap<Integer, Object>();
			if (comparisonList != null) {
				for (Integer id : comparisonList) {
					comparisonMap.put(id, true);
				}
			}

			model.put("comparisonMap", comparisonMap);
			model.put("maxComparisons", new Integer(((Configuration) siteConfig.get("COMPARISON_MAX")).getValue()));
		}
		
        return new ModelAndView(new RedirectView("https://www.viatrading.com/wholesale-products/"));

		//return new ModelAndView("frontend/search", "model", model);
	}

	private void traverseTree(List<Integer> categoryIds, Category category) {
		for (Category thisCategory : category.getSubCategories()) {
			categoryIds.add(thisCategory.getId());
			traverseTree(categoryIds, thisCategory);
		}
	}

	private void traverseSearchTree(List<Category> categories, Category category, int level) {
		for (Category thisCategory : category.getSubCategories()) {
			StringBuffer cName = new StringBuffer(thisCategory.getName());
			for (int i = 0; i < level; i++) {
				cName.insert(0, "&nbsp;&nbsp;");
			}
			thisCategory.setName(cName.toString());
			categories.add(thisCategory);
			traverseSearchTree(categories, thisCategory, level + 1);
		}
	}

	private void setLayout(HttpServletRequest request, Map<String, Object> model) {
		Layout layout = (Layout) request.getAttribute("layout");

		// bread crumbs
		List<Category> breadCrumb = this.webJaguar.getCategoryTree(ServletRequestUtils.getIntParameter(request, "cid", -1), true, request);
		model.put("breadCrumbs", breadCrumb);

		Layout productSearchLayout = this.webJaguar.getSystemLayout("productSearch", request.getHeader("host"), request);
		model.put("productSearchLayout", productSearchLayout);

		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} else {
			if (siteConfig.get("LEFTBAR_DEFAULT_TYPE").getValue().equals("3")) {
				model.put("mainCategories", this.webJaguar.getMainsAndSubsCategoryLinks(request));
			} else {
				model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request, null));
			}
			int cid = ServletRequestUtils.getIntParameter(request, "cid", -1);
			if (cid > -1) {
				Category category = this.webJaguar.getCategoryById(cid, "");
				if (category != null) {
					layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#parentId#", (category.getParent() != null) ? category.getParent().toString() : "-1"));
					layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#cid#", category.getId().toString()));

					// headTag
					if (productSearchLayout.getHeadTag() != null && !productSearchLayout.getHeadTag().equals("")) {
						productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#name#", category.getName()));
						productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#breadcrumb#", (breadCrumb == null) ? "" : Constants.showBreadCrumb(breadCrumb)));
						productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#field1#", (category.getField1() == null) ? "" : category.getField1()));
						productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#field2#", (category.getField2() == null) ? "" : category.getField2()));
						productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#field3#", (category.getField3() == null) ? "" : category.getField3()));
						productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#field4#", (category.getField4() == null) ? "" : category.getField4()));
						productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#field5#", (category.getField5() == null) ? "" : category.getField5()));
						layout.setHeadTag(productSearchLayout.getHeadTag());
					}
					if (productSearchLayout.getHeaderHtml() != null && !productSearchLayout.getHeaderHtml().equals("")) {
						productSearchLayout.setHeaderHtml(productSearchLayout.getHeaderHtml().replace("#name#", category.getName()));
					}
				} else {
					// it is used for LeftBar accordion effect javascript.
					layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#parentId#", "-1"));
					layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#cid#", "-1"));
					productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#name#", ""));
					productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#breadcrumb#", ""));
					productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#field1#", ""));
					productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#field2#", ""));
					productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#field3#", ""));
					productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#field4#", ""));
					productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#field5#", ""));
					layout.setHeadTag(productSearchLayout.getHeadTag());
				}
			} else {// if cid does not exist
				layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#parentId#", "-1"));
				layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#cid#", "-1"));
				if (productSearchLayout.getHeadTag() != null && !productSearchLayout.getHeadTag().equals("")) {
					productSearchLayout.setHeadTag(productSearchLayout.getHeadTag().replace("#name#", "Product Search"));
					layout.setHeadTag(productSearchLayout.getHeadTag());
				}

				if (productSearchLayout.getHeaderHtml() != null && !productSearchLayout.getHeaderHtml().equals("")) {
					productSearchLayout.setHeaderHtml(productSearchLayout.getHeaderHtml().replace("#name#", "Product Search"));
				}
			}
		}

		if (productSearchLayout.getHeaderHtml() != null && productSearchLayout.getHeaderHtml() != "") {
			productSearchLayout.setHeaderHtml(productSearchLayout.getHeaderHtml().replace("#resultNum#", model.get("count").toString()));
		}
		if (productSearchLayout.getHeaderHtmlLogin() != null && productSearchLayout.getHeaderHtmlLogin() != "") {
			productSearchLayout.setHeaderHtmlLogin(productSearchLayout.getHeaderHtmlLogin().replace("#resultNum#", model.get("count").toString()));
		}
	}

	private ProductSearch getProductSearch(HttpServletRequest request, Map<String, Object> model) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		ProductSearch search = (ProductSearch) request.getSession().getAttribute("frontProductSearch");
		if (search == null) {
			search = new ProductSearch();
			search.setSort(siteConfig.get("SEARCH_SORT").getValue());
			search.setPageSize(new Integer(siteConfig.get("NUMBER_PRODUCT_PER_PAGE_SEARCH").getValue()));
			// multi store. force to search only products under the MultiStore home category and subs.
			List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
			if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
				Integer hostCategoryId = this.webJaguar.getHomePageByHost(request.getHeader("host"));
				if (hostCategoryId != null) {
					search.setCategory(hostCategoryId);
					search.setIncludeSubs(true);
				}
			}
			request.getSession().setAttribute("frontProductSearch", search);
		}
		request.setAttribute("frontEndProductSearch", search);
		search.setEndDelimiter(Boolean.parseBoolean(siteConfig.get("END_DELIMITER_PRESENT").getValue()));

		if (request.getParameter("keywords") != null) {
			search.setKeywords(ServletRequestUtils.getStringParameter(request, "keywords", ""));
		}

		if (request.getParameter("category") != null) {
			int categoryId = ServletRequestUtils.getIntParameter(request, "category", -1);
			if (categoryId > -1) {
				search.setCategory(categoryId);
			} else {
				search.setCategory(null);
			}
			search.setCategoryIds(null);
			search.setIncludeSubs(ServletRequestUtils.getBooleanParameter(request, "includeSubs", false));
			if (search.isIncludeSubs()) {
				List<Category> categories = this.webJaguar.getCategoryTree(search.getCategory(), null, true, null, null, null); // include all categories
				if (!categories.isEmpty()) {
					List<Integer> categoryIds = new ArrayList<Integer>();
					categoryIds.add(categories.get(0).getId());
					// get subcategory id's
					traverseTree(categoryIds, categories.get(0));
					search.setCategoryIds(categoryIds);
				}
			}
		}

		if (request.getParameter("comp") != null) {
			search.setConjunction(ServletRequestUtils.getStringParameter(request, "comp", ""));
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);
			}
		}

		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				search.setPageSize(10);
			} else {
				search.setPageSize(size);
			}
		}

		// product fields
		int gPRODUCT_FIELDS = (Integer) ((Map<String, Object>) request.getAttribute("gSiteConfig")).get("gPRODUCT_FIELDS");
		Map<String, ProductField> productFieldMap = new HashMap<String, ProductField>();
		boolean setNewMapping = false;
		for (int i = 1; i <= gPRODUCT_FIELDS; i++) {
			Double minValue = null, maxValue = null;
			if (request.getParameter("field_" + i) != null) {
				setNewMapping = true;
				ProductField pField = new ProductField(i, request.getParameter("field_" + i));
				pField.setOperator(ServletRequestUtils.getStringParameter(request, "fieldOp_" + i, "="));
				productFieldMap.put("field_" + i, pField);
			}
			if ((request.getParameter("field_" + i + "_min") != null && !request.getParameter("field_" + i + "_min").isEmpty())
					|| (request.getParameter("field_" + i + "_max") != null && !request.getParameter("field_" + i + "_max").isEmpty())) {
				setNewMapping = true;
				if (request.getParameter("field_" + i + "_min") != null && !request.getParameter("field_" + i + "_min").isEmpty()) {
					minValue = Double.parseDouble(request.getParameter("field_" + i + "_min"));
					model.put("minValue_" + i, request.getParameter("field_" + i + "_min"));
				}
				if (request.getParameter("field_" + i + "_max") != null && !request.getParameter("field_" + i + "_max").isEmpty()) {
					maxValue = Double.parseDouble(request.getParameter("field_" + i + "_max"));
					model.put("maxValue_" + i, request.getParameter("field_" + i + "_max"));
				}
				productFieldMap.put("field_" + i, new ProductField(i, minValue, maxValue));
			}
		}
		if (setNewMapping) {
			search.setProductFieldMap(productFieldMap);
		}
		if (search.getProductFieldMap() != null) {
			search.setProductField(new ArrayList<ProductField>());
			search.getProductField().addAll(search.getProductFieldMap().values());
		}

		// min & max price
		if (request.getParameter("minPrice") != null) {
			double price = ServletRequestUtils.getDoubleParameter(request, "minPrice", -1.0);
			if (price >= 0) {
				search.setMinPrice(price);
			} else {
				search.setMinPrice(null);
			}
		}
		if (request.getParameter("maxPrice") != null) {
			double price = ServletRequestUtils.getDoubleParameter(request, "maxPrice", -1.0);
			if (price >= 0) {
				search.setMaxPrice(price);
			} else {
				search.setMaxPrice(null);
			}
		}

		// min quantity
		if (request.getParameter("minQuantity") != null) {
			int minQty = ServletRequestUtils.getIntParameter(request, "minQuantity", -1);
			if (minQty >= 0) {
				search.setMinQty(minQty);
				// System.out.println("minQty " + minQty);
			} else {
				search.setMinQty(null);
			}
		}

		// sorting
		if (request.getParameter("sort") != null) {
			int sortValue = ServletRequestUtils.getIntParameter(request, "sort", 0);
			switch (sortValue) {
			case 0:
				search.setSort("");
				break;
			case 10:
				search.setSort("name");
				break;
			case 20:
				search.setSort("price_1 ASC");
				break;
			case 30:
				search.setSort("price_1 DESC");
				break;
			case 40:
				search.setSort("rate DESC");
				break;
			}
		}
		return search;
	}
}