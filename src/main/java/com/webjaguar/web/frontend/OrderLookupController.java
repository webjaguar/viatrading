package com.webjaguar.web.frontend;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;


import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.UpsOrder;

public class OrderLookupController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, 	HttpServletResponse response) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		if(request.getParameter("_find") != null) {		
			String po = ServletRequestUtils.getStringParameter( request, "customerPO", null );
			
			if(po != null && !po.trim().isEmpty()) {	
				List<UpsOrder> searchList = this.webJaguar.getOrderLookupReport(po.trim());
				if(!searchList.isEmpty()) {
					map.put("orderLookup", searchList);
				} else {
					map.put("message", "noPO");
				}
			}  else {
				map.put("message", "PO field is empty");
			}
			map.put("customerPO", po);
		}
		return new ModelAndView("frontend/orderLookup", "model", map); 
	}
}
