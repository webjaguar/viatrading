/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Faq;
import com.webjaguar.model.FaqSearch;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.domain.Constants;

public class FaqController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	
    	Map<String, Object> model = new HashMap<String, Object>();
        
    	
    	// search criteria
    	FaqSearch search = getFaqSearch( request ); 
    	
    	UserSession userSession = this.webJaguar.getUserSession( request );
		Customer customer = null;
		if ( userSession != null )
			customer = this.webJaguar.getCustomerById(userSession.getUserid());
		
		String protectedAccess = "0";
		if ( customer != null ) {
			protectedAccess = customer.getProtectedAccess();
		} else {
			protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get( "gPROTECTED" );
		if ( protectedLevels == 0 ) {
			protectedAccess = Constants.FULL_PROTECTED_ACCESS;
		}
		search.setProtectedAccess( protectedAccess );
		
		model.put("groupId", search.getGroupId());
		model.put("keyword", search.getKeyword());
    	try {
    		List<Faq> faqList = new ArrayList<Faq>();
    		if (search.getQuestion() != null) {
    			Faq faq = this.webJaguar.getFaqByQuestion(search);
    			if (faq != null) { faqList.add(faq); }
    		} else {
    			faqList = this.webJaguar.getFaqList(search);
    		}
    		PagedListHolder faqListHolder = new PagedListHolder(faqList);
    		faqListHolder.setPageSize( search.getPageSize() );
    		faqListHolder.setPage( search.getPage() - 1 );
    		model.put("faqs", faqListHolder);    		
    	} catch(Exception e) {
    		// x' union select 'x on keywords causes
    		// SQL Injection Vulnerability
    		// hide the error message so that we don't disclose this
    	}

    	model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
    	model.put("groupList", this.webJaguar.getFaqGroupList(protectedAccess));	
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) ) {
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		}
		model.put("faqLayout", this.webJaguar.getSystemLayout("faq", request.getHeader("host"), request));
		
        return new ModelAndView("frontend/faq", "model", model);    	
    }
    
    private FaqSearch getFaqSearch(HttpServletRequest request) {
    	Map<String, Configuration> siteConfig = (Map) request.getAttribute( "siteConfig" );
    	FaqSearch search = (FaqSearch) request.getSession().getAttribute( "faqSearch" );
		if (search == null) {
			search = new FaqSearch();
			search.setPageSize( Integer.parseInt(siteConfig.get("DEFAULT_FAQ_LIST_SIZE").getValue()) );
			request.getSession().setAttribute( "faqSearch", search );
		}
		
		// check if drop down changed
		Integer groupId;
		try {
			groupId = ServletRequestUtils.getIntParameter( request, "__group_id" );
			search.setGroupId( groupId );
		}
		catch ( ServletRequestBindingException e ) {
			search.setGroupId( null );
		}
		String keyword = ServletRequestUtils.getStringParameter( request, "__keyword", null );
		search.setKeyword( keyword );
		
		if (request.getParameter("comp") != null) {
			search.setConjunction(ServletRequestUtils.getStringParameter(request, "comp", ""));
		}
		
		// don't save Question in session
		search.setQuestion(ServletRequestUtils.getStringParameter(request, "question", null));
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 20 );
			if (size < 20) {
				search.setPageSize( 20 );
			} else {
				search.setPageSize( size );				
			}
		}
		
		return search;
    }
}