/* Copyright 2010 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 05.03.2012
 */

package com.webjaguar.web.frontend.giftcard;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class AjaxShowPromoController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
    	Map<String, Object> model = new HashMap<String, Object>();
		model.put("promo", this.webJaguar.getPromoByName(ServletRequestUtils.getStringParameter(request, "promo")));
    	return new ModelAndView("frontend/checkout/promo/showPromo", "model", model);
	}
}