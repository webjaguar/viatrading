/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 */

package com.webjaguar.web.frontend.giftcard;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CreditCard;
import com.webjaguar.model.Customer;
import com.webjaguar.model.GiftCard;
import com.webjaguar.model.GiftCardStatus;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UserSession;
import com.webjaguar.thirdparty.payment.authorizenet.AuthorizeNetApi;
import com.webjaguar.thirdparty.payment.paypalpro.PayPalProApi;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.GiftCardForm;
import com.webjaguar.web.validator.GiftCardFormValidator;

public class GiftCardController extends AbstractWizardFormController
{
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
    
	private Map<String, Object> gSiteConfig;
	private Map<String, Configuration> siteConfig;
	
	public GiftCardController()
	{
		setCommandName( "giftCardForm" );
		setCommandClass( GiftCardForm.class );
		setPages( new String[] { "frontend/checkout/giftcard/step1", "frontend/checkout/giftcard/step2", "frontend/checkout/giftcard/step3" } );
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) throws Exception
	{
		GiftCardForm giftCardForm = (GiftCardForm) command;
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> myModel = new HashMap<String, Object>();
		UserSession userSession = (UserSession) request.getAttribute( "userSession" );
		Customer customer = this.webJaguar.getCustomerById( userSession.getUserid() );
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		layout.setHideLeftBar( true );
		// hide right bar
		layout.setRightBarTopHtml( "" );
		layout.setRightBarBottomHtml( "" );

		switch ( page )
		{
		case 0:
			if(!gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ebizcharge") && !gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("authorizenet") && ! gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("paypalpro") &&  !((Integer) gSiteConfig.get( "gPAYPAL" ) > 0)) {
				map.put("message", "GiftCardPaymentGateway");
				map.put("paymentGateway", false);
			}
			giftCardForm.getGiftCard().setSenderFirstName( customer.getAddress().getFirstName() );
			giftCardForm.getGiftCard().setSenderLastName( customer.getAddress().getLastName() );
			break;
		case 1:
			// expireYears
			myModel.put("expireYears", Constants.getYears(10));
			myModel.put("countrylist",this.webJaguar.getEnabledCountryList());
			myModel.put("statelist",this.webJaguar.getStateList("US"));
			myModel.put("caProvinceList", this.webJaguar.getStateList("CA"));
			giftCardForm.getGiftCardOrder().setSubTotal( giftCardForm.getGiftCard().getAmount() * giftCardForm.getGiftCardOrder().getQuantity() );
			giftCardForm.getGiftCardOrder().setBilling( this.webJaguar.getDefaultAddressByUserid( giftCardForm.getGiftCard().getUserId() ) );
			break;	
		}
		
		map.put("giftCardLayout", this.webJaguar.getSystemLayout("giftCard", request.getHeader("host"), request));
		map.put( "model", myModel );
		return map;
	}

	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception
	{
		GiftCardForm giftCardForm = (GiftCardForm) command;

		switch ( page )
		{
		case 0:
			if ( !errors.hasErrors() ) {
				if ( giftCardForm.getGiftCard().getAmount() < Integer.parseInt( siteConfig.get( "GIFTCARD_MIN_AMOUNT" ).getValue() ) ||
						giftCardForm.getGiftCard().getAmount() > Integer.parseInt( siteConfig.get( "GIFTCARD_MAX_AMOUNT" ).getValue() ) ) {
					errors.rejectValue("giftCard.amount", "form.amountRangeError", "Amount Range Error.");
				}
			}
			break;
		}
	}

	protected int getInitialPage(HttpServletRequest request, Object command)
	{
		return 0;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws ModelAndViewDefiningException
	{
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		siteConfig = this.webJaguar.getSiteConfig();
		if ( !(Boolean) gSiteConfig.get("gGIFTCARD") ) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("account.jhtm")));
		}
		
		GiftCardForm giftCardForm = new GiftCardForm();
		
		UserSession userSession = (UserSession) request.getAttribute( "userSession" );
		giftCardForm.getGiftCard().setUserId( userSession.getUserid() );
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			setPages( new String[] { "frontend/layout/template6/checkout1/giftcard/step1", "frontend/layout/template6/checkout1/giftcard/step2", "frontend/layout/template6/checkout1/giftcard/step3" } );
		}
		
		return giftCardForm;
	}

	protected ModelAndView processCancel(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors)
     throws Exception {
    	return new ModelAndView(new RedirectView(request.getContextPath() + "/account.jhtm"));
    }
	
	public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception
	{
		GiftCardForm giftCardForm = (GiftCardForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
		
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0) {
			multiStore = multiStores.get(0);
			//set giftCard host	
		}

		boolean isSuccess = false;
		
		// authorize and capture 
		Order creditCardOrder = new Order();
		init(creditCardOrder, giftCardForm);
		 GiftCardStatus giftCardStatus = new  GiftCardStatus();
		
		try {
			if(gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ebizcharge") || gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("authorizenet") || gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("paypalpro") || (Integer) gSiteConfig.get( "gPAYPAL" ) > 0)  {			
				//Basic Pay Pal
				if ((Integer) gSiteConfig.get( "gPAYPAL" ) > 0 && giftCardForm.getGiftCardOrder().getPaymentMethod() != null && giftCardForm.getGiftCardOrder().getPaymentMethod().equalsIgnoreCase("PayPal")) {					
					giftCardForm.getGiftCardOrder().setStatus("xp");				
					giftCardStatus.setStatus("xp");
					
					creditCardOrder.setOrderId(this.webJaguar.insertGiftCardOrder( giftCardForm.getGiftCardOrder(), giftCardStatus ));
					
					int i = giftCardForm.getGiftCardOrder().getQuantity();
					
					do {
						String claimCode = generateRandomCode();
						giftCardForm.getGiftCard().setCode( claimCode );	
						try {
							giftCardForm.getGiftCard().setGiftCardOrderId(creditCardOrder.getOrderId());
							giftCardForm.getGiftCard().setPaymentMethod("paypal");
							this.webJaguar.insertGiftCard(giftCardForm.getGiftCard());
							i=i-1;						
						} catch (Exception e){
							// duplicate code???
							e.printStackTrace();
							i=i+1;
						}
					} while ( i > 0 );
					
					model.put("order", creditCardOrder);
					return new ModelAndView( "frontend/checkout/giftcard/paypalPost", model);
				}
				//PayPalPro
				if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("paypalpro")) {				
					PayPalProApi payPalPro = new PayPalProApi();
					isSuccess = payPalPro.DoDirectPayment(creditCardOrder, siteConfig,"sale");			
				}
				//Authorize.net Or EbizCharge
				if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("authorizenet") || gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ebizcharge")) { 				
					AuthorizeNetApi authorizeNet = new AuthorizeNetApi();
					isSuccess = authorizeNet.authorize(creditCardOrder, false, false, siteConfig, gSiteConfig);
				}			
			}
		} catch (Exception e) {
			creditCardOrder.getCreditCard().setPaymentNote( "Error" );
			notifyAEM("Gift Card on " + siteConfig.get("SITE_URL").getValue(), e.toString());
		}
		if (isSuccess) { 
			giftCardForm.getGiftCardOrder().getCreditCard().setTransId(creditCardOrder.getCreditCard().getTransId());
			giftCardForm.getGiftCardOrder().getCreditCard().setPaymentStatus(creditCardOrder.getCreditCard().getPaymentStatus());							

			giftCardForm.getGiftCardOrder().getCreditCard().setPaymentDate( creditCardOrder.getCreditCard().getPaymentDate() );
			giftCardForm.getGiftCardOrder().getCreditCard().setPaymentNote( creditCardOrder.getCreditCard().getPaymentNote() );
			giftCardForm.getGiftCardOrder().setStatus("p"); //pending
			giftCardForm.getGiftCardOrder().setStatusChangeDate(creditCardOrder.getCreditCard().getPaymentDate());
			giftCardStatus.setStatus("p");
			giftCardForm.getGiftCard().setGiftCardOrderId( this.webJaguar.insertGiftCardOrder( giftCardForm.getGiftCardOrder(), giftCardStatus ) );
			this.webJaguar.updateGiftCardOrderCreditCardPayment(giftCardForm.getGiftCardOrder());

			
			int i = giftCardForm.getGiftCardOrder().getQuantity();
			do {
				String claimCode = generateRandomCode();
				giftCardForm.getGiftCard().setCode( claimCode );
				try {
					this.webJaguar.insertGiftCard(giftCardForm.getGiftCard());
					// send email
					MimeMessage mms = mailSender.createMimeMessage();
					MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
					SiteMessage siteMessage = null;
										
					try {
						Integer messageId = null;
						messageId = Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_GIFT_CARD" ).getValue() );							
						siteMessage = this.webJaguar.getSiteMessageById( messageId );
						
						if (siteMessage != null) {
							notifyCustomer(helper, giftCardForm.getGiftCard(), request, multiStore, getApplicationContext(), siteMessage);
							notifyAdmin(giftCardForm.getGiftCard(),siteConfig);
							mailSender.send(mms);
						}

					} catch (Exception e) {
						e.printStackTrace();
					}

					i=i-1;
				} catch (Exception e){
					// duplicate code???
					e.printStackTrace();
					i=i+1;
				}
				
			} while ( i > 0 );
		} else {
			if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("paypalpro")) {		
				
				return showPage(request, errors, 2);
			} else {
				
				errors.rejectValue( "giftCardOrder.creditCard.number", "TRANSACTION_DECLINED" );
				return showPage(request, errors, 2);
			}
		}
		if(siteConfig.get("GIFT_CARD_THANK_YOU_ID").getValue() != null && !siteConfig.get("GIFT_CARD_THANK_YOU_ID").getValue().equals("")){
			return new ModelAndView( new RedirectView("category.jhtm?cid="+siteConfig.get("GIFT_CARD_THANK_YOU_ID").getValue()) );
		}
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView( new RedirectView("account.jhtm") );
		}
		return new ModelAndView( "frontend/account", model );
	}

	protected void validatePage(Object command, Errors errors, int page)
	{
		GiftCardForm giftCardForm = (GiftCardForm) command;
		GiftCardFormValidator validator = (GiftCardFormValidator) getValidator();
		
		switch ( page )
		{
		case 0:
			validator.validate( command, errors );
			break;
		case 1:
			validator.validatePaymentMethod( giftCardForm.getGiftCardOrder(), errors );
			
			if (giftCardForm.getGiftCardOrder().getPaymentMethod() != null && !giftCardForm.getGiftCardOrder().getPaymentMethod().equalsIgnoreCase("PayPal")){
				validator.validateCC( giftCardForm, errors );
			} else {
				giftCardForm.getGiftCardOrder().setCreditCard(new CreditCard());
			}
			break;	
		}
	}
	
	private void notifyCustomer(MimeMessageHelper helper, GiftCard giftCard, HttpServletRequest request, MultiStore multiStore, ApplicationContext context, SiteMessage siteMessage ) throws Exception 
	{
		UserSession userSession = (UserSession) request.getAttribute( "userSession" );
		String senderMessage = giftCard.getMessage();
		
		// send email
		String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
		String siteUrl = siteConfig.get( "SITE_URL" ).getValue();
		if (multiStore != null) {
			contactEmail = multiStore.getContactEmail();
			siteUrl = request.getScheme()+"://" + multiStore.getHost() + request.getContextPath() + "/";
		}
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));

    	StringBuffer message = new StringBuffer();
    	
    	// message
    	siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardSender#", giftCard.getSenderFirstName()+ " " +giftCard.getSenderLastName() ) );
    	siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCard#", generateHtml(giftCard, siteUrl, context, request ) ) );
    	siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardRecipient#", giftCard.getRecipientFirstName()+ " " +giftCard.getRecipientLastName() ) );
    	siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardMessage#", giftCard.getMessage()) );
    	
    	message.append( siteMessage.getMessage() );
    	
    	helper.setTo( giftCard.getRecipientEmail() );
    	helper.setFrom(userSession.getUsername());	
    	helper.setSubject( giftCard.getSenderFirstName() + " " + giftCard.getSenderLastName() + " " + context.getMessage( "sentYouAGiftCardFrom", new Object[0], RequestContextUtils.getLocale(request)) + " " +  siteUrl );
    	helper.setText(message.toString(), true);
	}
	
	private String generateRandomCode() {
		
		String token = "";
		String temp =  UUID.randomUUID().toString().toUpperCase().replace( "-", "" ).substring( 0, 18 );
		return token.concat( temp.substring( 0, 5 ) + "-" + temp.substring( 6, 12 ) + "-" + temp.substring( 13, 18 ));
	}
	
	private void init(Order creditCardOrder, GiftCardForm giftCardForm) {
		creditCardOrder.setCreditCard( new CreditCard() );
		creditCardOrder.setBilling( new Address() );
		
		creditCardOrder.setGrandTotal( giftCardForm.getGiftCardOrder().getSubTotal() );
		creditCardOrder.setOrderId( giftCardForm.getGiftCardOrder().getGiftCardOrderId() );
		creditCardOrder.getCreditCard().setType(giftCardForm.getGiftCardOrder().getCreditCard().getType());
		creditCardOrder.getCreditCard().setNumber( giftCardForm.getGiftCardOrder().getCreditCard().getNumber() );
		creditCardOrder.getCreditCard().setCardCode( giftCardForm.getGiftCardOrder().getCreditCard().getCardCode() );
		creditCardOrder.getCreditCard().setExpireMonth(giftCardForm.getGiftCardOrder().getCreditCard().getExpireMonth() );
		creditCardOrder.getCreditCard().setExpireYear( giftCardForm.getGiftCardOrder().getCreditCard().getExpireYear() );
		creditCardOrder.getBilling().setFirstName( giftCardForm.getGiftCardOrder().getBilling().getFirstName() );
		creditCardOrder.getBilling().setLastName( giftCardForm.getGiftCardOrder().getBilling().getLastName() );
		creditCardOrder.getBilling().setCompany( giftCardForm.getGiftCardOrder().getBilling().getCompany() );
		creditCardOrder.getBilling().setAddr1( giftCardForm.getGiftCardOrder().getBilling().getAddr1() );
		creditCardOrder.getBilling().setCity( giftCardForm.getGiftCardOrder().getBilling().getCity() );
		creditCardOrder.getBilling().setStateProvince( giftCardForm.getGiftCardOrder().getBilling().getStateProvince() );
		creditCardOrder.getBilling().setZip( giftCardForm.getGiftCardOrder().getBilling().getZip() );
		creditCardOrder.getBilling().setCountry( giftCardForm.getGiftCardOrder().getBilling().getCountry() );
		creditCardOrder.getBilling().setPhone( giftCardForm.getGiftCardOrder().getBilling().getPhone() );
		creditCardOrder.getBilling().setFax( giftCardForm.getGiftCardOrder().getBilling().getFax() );
	}
	
	private String generateHtml(GiftCard giftCard, String siteUrl, ApplicationContext context, HttpServletRequest request) {
 
		StringBuffer html = new StringBuffer();
		//html.append( this.webJaguar.getSystemLayout("giftcard", request.getHeader("host")).getHeaderHtml() );
		html.append( "<img src=\"" + siteUrl + "assets/Image/Layout/companylogo.gif\"  border=\"0\" align=\"center\"/>" );
		html.append( "<table align=\"center\" width=\"430\" height=\"280\" border=\"0\" background=\"" + siteUrl + "assets/Image/Layout/gift_card_email.gif\" cellspacing=\"0\" cellpadding=\"0\">" );
		html.append( "<tr> <td> &nbsp; </td> </tr><tr> <td> &nbsp; </td> </tr><tr> <td> &nbsp; </td> </tr><tr> <td> &nbsp; </td> </tr>" );

		html.append( " <tr> " );
		html.append( "<td>" );
		html.append( "<p style=\"margin-left:210px; font-weight:700; font-height:15px; color:#333333; font-size:12px\">" + context.getMessage( "from", new Object[0], RequestContextUtils.getLocale(request))  + ": " + giftCard.getSenderFirstName() + " " + giftCard.getSenderLastName() + "</p>" );
		html.append( " <p style=\"margin-left:210px; font-weight:700; font-height:15px; color:#333333; font-size:12px\">" + context.getMessage( "to", new Object[0], RequestContextUtils.getLocale(request))  + ": " +  giftCard.getRecipientFirstName() + " " + giftCard.getRecipientLastName() + "</p>" );
		html.append( " </td>" );
		html.append( "</tr>" );
		html.append( "<tr>" );

		html.append( "<td>" );
		html.append( "<div style=\"padding:15px 40px 0px 165px; font-weight:700; line-height:25px; color:#333333; font-size:52px\">" + context.getMessage(siteConfig.get("CURRENCY").getValue(), new Object[0], RequestContextUtils.getLocale(request))  + new DecimalFormat("#,###.00").format( giftCard.getAmount().doubleValue() ) + "</div>" );
		html.append( "</td> " );
		html.append( "</tr>" );
		html.append( "<tr>" );
		html.append( " <td>" );
		html.append( "<div style=\"margin-left:100px; padding-top:10px; font-weight:700; font-height:18px; color:#983434; font-size:24px\">" + giftCard.getCode() + "</div>" );
		html.append( " </td>  " );
		html.append( "</tr>" );

		html.append( " <tr> <td> &nbsp; </td> </tr>" );
		html.append( "</table>" );
		
		html.append( "<table width=\"800\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" );
		html.append( "<tr> " );
		html.append( "<td>" );
		html.append( "<div align=\"center\" style=\"font-weight:700;color:#ff2332; font-size:11px \"><a href=\"" + siteUrl + "giftcardBalance.jhtm\">" + context.getMessage( "redeemYourGiftCard", new Object[0], RequestContextUtils.getLocale(request))  + " " + "</a></div>");
		html.append( "</td> " );
		html.append( "</tr>" );
		html.append( "</table>" );
		//html.append( this.webJaguar.getSystemLayout("giftcard", request.getHeader("host")).getFooterHtml() );
		
		return html.toString();
	}
	
	private void notifyAEM(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
	
    private void notifyAdmin(GiftCard giftCard, Map<String, Configuration> siteConfig) {
		// send email notification
    	String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Gift Card Order Number: "+giftCard.getGiftCardOrderId());
		msg.setText(dateFormatter.format(new Date()) + "\n\n Gift Card sent to "+giftCard.getRecipientEmail() );
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}