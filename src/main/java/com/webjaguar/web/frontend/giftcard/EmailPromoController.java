/* Copyright 2010 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 05.03.2012
 */

package com.webjaguar.web.frontend.giftcard;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.GiftCard;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Promo;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.form.GiftCardForm;

@SuppressWarnings("deprecation")
public class EmailPromoController extends SimpleFormController {

	private Map<String, Configuration> siteConfig;
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	public EmailPromoController() {
		setSessionForm(true);
		setCommandName("giftCardForm");
		setCommandClass(GiftCardForm.class);
		setFormView("frontend/checkout/promo/email");
		setSuccessView("home.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {

		GiftCardForm giftCardForm = (GiftCardForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
		
        // cancel
		if (request.getParameter("_cancel") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}
		
		Customer customer = this.webJaguar.getCustomerById(giftCardForm.getGiftCard().getUserId());
		
		// construct email message
		MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
		
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0) {
			multiStore = multiStores.get(0);
			//set giftCard host	
		}
		
		try {
			constructMessage(helper, giftCardForm.getGiftCard(), request, customer,multiStore, getApplicationContext());
			mailSender.send(mms);
			model.put("message", "email.sent" );
		} catch (Exception e) {
			e.printStackTrace();
			model.put("message", "email.exception" );
		}
		
		return showForm(request, response, errors, model);
	}	
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		GiftCardForm giftCardForm = new GiftCardForm();
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
		UserSession userSession = (UserSession) request.getAttribute( "userSession" );
		Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());
		
		if (customer != null) {
			giftCardForm.getGiftCard().setUserId(userSession.getUserid());
			giftCardForm.getGiftCard().setSenderFirstName( customer.getAddress().getFirstName() );
			giftCardForm.getGiftCard().setSenderLastName( customer.getAddress().getLastName() );
		} else {
			request.setAttribute("message", "customer.exception.notfound" );
		}
		
		return giftCardForm;
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		GiftCardForm giftCardForm = (GiftCardForm) command;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "giftCard.code", "form.required", "Code is required.");    	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "giftCard.recipientFirstName", "form.required", "First Name is required.");    	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "giftCard.recipientLastName", "form.required", "Last Name is required.");    	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "giftCard.senderFirstName", "form.required", "First Name is required.");    	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "giftCard.senderLastName", "form.required", "Last Name is required.");    	
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "giftCard.recipientEmail", "form.required", "Recepient Email is required.");    	
		Promo promo = this.webJaguar.getPromoByName(giftCardForm.getGiftCard().getCode());
		if ( promo == null) {
			errors.rejectValue("giftCard.code", "form.invalidPromo", "Invalid Promo Code");
		} else {
			//set the value as it is not coming from the form
			//later if required, get the amount if promo is having discount in percentage
			giftCardForm.getGiftCard().setAmount(promo.getDiscount());
		}
		if (!giftCardForm.getGiftCard().getRecipientEmail().equals(giftCardForm.getConfirmEmail())) {
			errors.rejectValue("confirmEmail", "giftCard.emailMismatch", "matching emails required");
		}
	}
	private void constructMessage(MimeMessageHelper helper, GiftCard giftCard, HttpServletRequest request, Customer customer, MultiStore multiStore, ApplicationContext context) throws Exception {
		
		helper.setTo(giftCard.getRecipientEmail());
		helper.setFrom(customer.getUsername());	
		helper.setCc(customer.getUsername());
		if(siteConfig.get( "CONTACT_EMAIL" ).getValue() != null && !siteConfig.get( "CONTACT_EMAIL" ).getValue().isEmpty()){
			helper.setBcc(siteConfig.get( "CONTACT_EMAIL" ).getValue());
		}
		
    	SiteMessage siteMessage = null;
    	String siteUrl = siteConfig.get( "SITE_URL" ).getValue();
		if (multiStore != null) {
			siteUrl = request.getScheme()+"://" + multiStore.getHost() + request.getContextPath() + "/";
		}
    	
		try {
			Integer messageId = Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_COUPON_CODE" ).getValue() );							
			siteMessage = this.webJaguar.getSiteMessageById( messageId );
			if (siteMessage != null) {
				siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardSender#", (giftCard.getSenderFirstName() != null ? giftCard.getSenderFirstName() : "")+ " " +(giftCard.getSenderLastName() != null ? giftCard.getSenderLastName() : "" )) );
				siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCard#", generateHtml(giftCard, siteUrl, context, request ) ) );
				siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardAmount#", context.getMessage(siteConfig.get("CURRENCY").getValue(), new Object[0], RequestContextUtils.getLocale(request))  + new DecimalFormat("#,###.00").format( giftCard.getAmount().doubleValue() )  ) );
				siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardRecipient#", (giftCard.getRecipientFirstName() != null ? giftCard.getRecipientFirstName()  : "")+ " " +(giftCard.getRecipientLastName() != null ? giftCard.getRecipientLastName()  : "")) );
				siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardMessage#", (giftCard.getMessage() != null ? giftCard.getMessage() : "")) );
				siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardCode#", giftCard.getCode()));
				
			 	helper.setSubject( giftCard.getSenderFirstName() + " " + giftCard.getSenderLastName() + " " + context.getMessage( "f_sentYouACouponCodeFrom", new Object[0], RequestContextUtils.getLocale(request)) + " " +  siteUrl );
		    	helper.setText(siteMessage.getMessage(), true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String generateHtml(GiftCard giftCard, String siteUrl, ApplicationContext context, HttpServletRequest request) {
		 
		StringBuffer html = new StringBuffer();
		//html.append( this.webJaguar.getSystemLayout("giftcard", request.getHeader("host")).getHeaderHtml() );
		html.append( "<img src=\"" + siteUrl + "assets/Image/Layout/companylogo.gif\"  border=\"0\" align=\"center\"/>" );
		html.append( "<table align=\"center\" width=\"430\" height=\"280\" border=\"0\" background=\"" + siteUrl + "assets/Image/Layout/coupon_code_email.gif\" cellspacing=\"0\" cellpadding=\"0\">" );
		html.append( "<tr> <td> &nbsp; </td> </tr><tr> <td> &nbsp; </td> </tr><tr> <td> &nbsp; </td> </tr><tr> <td> &nbsp; </td> </tr>" );

		html.append( " <tr> " );
		html.append( "<td>" );
		html.append( "<p style=\"margin-left:210px; font-weight:700; font-height:15px; color:#333333; font-size:12px\">" + context.getMessage( "from", new Object[0], RequestContextUtils.getLocale(request))  + ": " + giftCard.getSenderFirstName() + " " + giftCard.getSenderLastName() + "</p>" );
		html.append( " <p style=\"margin-left:210px; font-weight:700; font-height:15px; color:#333333; font-size:12px\">" + context.getMessage( "to", new Object[0], RequestContextUtils.getLocale(request))  + ": " +  giftCard.getRecipientFirstName() + " " + giftCard.getRecipientLastName() + "</p>" );
		html.append( " </td>" );
		html.append( "</tr>" );
		html.append( "<tr>" );

		html.append( "<td>" );
		html.append( "</td> " );
		html.append( "</tr>" );
		html.append( "<tr>" );
		html.append( " <td>" );
		html.append( "<div style=\"margin-left:100px; padding-top:10px; font-weight:700; font-height:18px; color:#983434; font-size:24px\">" + giftCard.getCode() + "</div>" );
		html.append( " </td>  " );
		html.append( "</tr>" );

		html.append( " <tr> <td> &nbsp; </td> </tr>" );
		html.append( "</table>" );
		
		html.append( "<table width=\"800\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" );
		html.append( "<tr> " );
		html.append( "<td>" );
		html.append( "</td> " );
		html.append( "</tr>" );
		html.append( "</table>" );
		return html.toString();
	}
}