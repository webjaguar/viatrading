/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.giftcard;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.GiftCard;
import com.webjaguar.model.GiftCardStatus;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class GiftCardBalanceController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	
    	Map<String, Object> myModel = new HashMap<String, Object>();
        if ( !(Boolean) gSiteConfig.get("gGIFTCARD") )
        	return new ModelAndView(new RedirectView("account.jhtm"));
    	
    	UserSession userSession = this.webJaguar.getUserSession( request );
		Customer customer = null;
		if ( userSession != null ) {
			customer = this.webJaguar.getCustomerById(userSession.getUserid());
			myModel.put("credit", customer.getCredit());
			
			String claimCode = ServletRequestUtils.getStringParameter( request, "__claimCode", null );
			
			if ( claimCode != null ) {
				GiftCard giftCard = this.webJaguar.getGiftCardByCode( claimCode, true );
				if (giftCard != null ) {
					//Change giftCardStatus to shipped if card is redeemed
					GiftCardStatus giftCardStatus = new GiftCardStatus();
					giftCardStatus.setStatus("s");
					giftCardStatus.setGiftCardOrderId(giftCard.getGiftCardOrderId());
					giftCardStatus.setComments("Gift Card redeemed by: "+ userSession.getUsername());
					
					this.webJaguar.redeemGiftCardByCode( giftCard, userSession, giftCardStatus );
					myModel.put("credit", customer.getCredit() + giftCard.getAmount());
					myModel.put("giftCardSuccessful", true);
				} else {
					myModel.put("message", "giftCard.giftCardNotFound");
				}
			}
		}
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		}    	
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			 return new ModelAndView("frontend/layout/template6/account/giftCardRedeem", "model", myModel);   
		}
        return new ModelAndView("frontend/account/giftCardRedeem", "model", myModel);    	
    }
}