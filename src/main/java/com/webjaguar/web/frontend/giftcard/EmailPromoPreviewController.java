/* Copyright 2010 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 05.03.2012
 */

package com.webjaguar.web.frontend.giftcard;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Promo;
import com.webjaguar.model.SiteMessage;

public class EmailPromoPreviewController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private Map<String, Configuration> siteConfig;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
    	
    	siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
    	Map<String, Object> model = new HashMap<String, Object>();
		Promo promo = this.webJaguar.getPromoByName(ServletRequestUtils.getStringParameter(request, "promo"));
		System.out.println("promo "+ServletRequestUtils.getStringParameter(request, "promo"));
	    
    	try {
			Integer messageId = Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_COUPON_CODE" ).getValue() );							
			SiteMessage siteMessage = this.webJaguar.getSiteMessageById( messageId );
			if (promo != null && siteMessage != null) {
				siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardSender#", ServletRequestUtils.getStringParameter(request, "sF", "")+ " " +ServletRequestUtils.getStringParameter(request, "sL", "")) );
				siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardAmount#", "$"+new DecimalFormat("#,###.00").format(promo.getDiscount().doubleValue()) ));
				siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardRecipient#", ServletRequestUtils.getStringParameter(request, "rF", "")+ " " +ServletRequestUtils.getStringParameter(request, "rL", "") ) );
				siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardMessage#", ServletRequestUtils.getStringParameter(request, "m", "")) );
				siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardCode#", ServletRequestUtils.getStringParameter(request, "promo")));
				model.put("message", siteMessage.getMessage());
				System.out.println("Message "+siteMessage);
		    } else {
		    	model.put("message", "Invalid Promo.");
		    }
		} catch (Exception e) {
			model.put("message", "No Site Message has been set.");
			e.printStackTrace();
		}
		
		System.out.println("Message ");
    	return new ModelAndView("frontend/checkout/promo/emailPreview", "model", model);
	}
}