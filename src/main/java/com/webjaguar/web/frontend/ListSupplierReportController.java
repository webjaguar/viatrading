package com.webjaguar.web.frontend;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;
import com.webjaguar.model.ConsignmentReport;

public class ListSupplierReportController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {		
		
		UserSession userSession = this.webJaguar.getUserSession(request);
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));		
		int custId = userSession.getUserid();
		model.put("userId", userSession.getUserid());
		Customer customer = this.webJaguar.getCustomerById( custId );
		ConsignmentReport search = new ConsignmentReport();
		
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}
		String sort = ServletRequestUtils.getStringParameter(request, "sort", "date_ordered desc");
		model.put("sort", sort);		
		PagedListHolder ordersList = new PagedListHolder(this.webJaguar.getConsignmentReport(customer.getSupplierId(), sort));
		ordersList.setPageSize(search.getPageSize());
		ordersList.setPage(search.getPage()-1);
		Layout layout = (Layout) request.getAttribute( "layout" );
		layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "My Reports"));
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		model.put("orders", ordersList);
		model.put("viewMyReportLayout", this.webJaguar.getSystemLayout("viewMyReportLayout", request.getHeader("host"), request));
		
		return  new ModelAndView("frontend/account/supplierReport", "model", model);
	}
}
