/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 04.16.2009
 */

package com.webjaguar.web.frontend;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.model.ReviewSearch;

public class MemberController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		
		if (gSiteConfig.get("gCUSTOMER_CATEGORY") == null) {
			return null;
		}

		Map<String, Object> model = new HashMap<String, Object>();
		
		ProductSearch search = new ProductSearch();
		request.setAttribute("frontProductSearchMember", search);
		
		// supplier/user
		Integer userId = null;
		String path = request.getServletPath();		// pattern /memberXXXX.jhtm
		try {
			userId = new Integer(path.substring(7, path.length()-5));			
		} catch (NumberFormatException e) {
			// do nothing
		}
		if (userId != null) {
			Customer account = this.webJaguar.getCustomerById(userId);
			if (account != null && !account.isSuspended()) {
				if (this.webJaguar.getCategoryIdsByUser(userId).size() > 0) {
					// display only if account is associated to any category
					account.setRate( this.webJaguar.getCompanyReviewAverage( account.getSupplierId() ) );
					model.put("account", account);
					search.setDefaultSupplierId(account.getSupplierId());
				}
				if (account.getLongDesc() != null) {
					account.setLongDesc(account.getLongDesc().replace("\n", "<br>"));
				}
			}							
		}
		if (search.getDefaultSupplierId() == null) {
			search.setDefaultSupplierId(-1);
		}
		
    	String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();

		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null)); 
		model.put( "url", url + "?" + query );
		// continue shopping
		request.getSession().setAttribute( "continueShoppingUrl", model.get( "url" ) );
		
		Customer customer = this.webJaguar.getCustomerByRequest(request);
		
		search.setLimit(30);
		
		for(int i=1; i<=3; i++){
			search.setListType(i);
			model.put("products"+i, this.webJaguar.searchProducts(search, request, customer));	
		}
		
		if ((Boolean) gSiteConfig.get( "gCOMPANY_REVIEW" )) {
			model.put("returnUrl", path.substring( 1 ));
			ReviewSearch companySearch = new ReviewSearch();
			// page
			int page = 1;
			if (request.getParameter("page") != null) {
				page = ServletRequestUtils.getIntParameter( request, "page", 1 );
				if (page < 1) {
					search.setPage( 1 );
				} else {
					search.setPage( page );				
				}
			}

			companySearch.setCompanyId( search.getDefaultSupplierId() + "" );
			PagedListHolder companyReviewList = new PagedListHolder( this.webJaguar.getCompanyReviewListByCompanyId( search.getDefaultSupplierId(), true ) );
			companyReviewList.setPageSize( 20 );
			companyReviewList.setPage( page - 1 );
			model.put("companyReviewList", companyReviewList);    		
			model.put("id", search.getDefaultSupplierId());
		}
		
		
		for(int i=1; i<=3; i++) {
			boolean showPriceColumn = false;
			boolean showQtyColumn = false;
			for (Product product: (List<Product>)model.get("products"+i)) {
				if (!showPriceColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) || product.getMsrp() != null)) {
					showPriceColumn = true;					
				}
				if (!showQtyColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) && (!product.isLoginRequire() || (product.isLoginRequire() && customer != null)))) {
					showQtyColumn = true;					
				}
			}
			model.put("showPriceColumn"+i, showPriceColumn);
			model.put("showQtyColumn"+i, showQtyColumn);
			model.put("productFields"+i, this.webJaguar.getProductFields(request, (List<Product>)model.get("products"+i), true));
		}
		Layout layout = (Layout) request.getAttribute("layout");
		layout.setLeftBarTopHtml("");
		layout.setLeftBarBottomHtml( "");
		layout.setHideLeftBar(true);
		layout.setRightBarTopHtml("");
		layout.setRightBarBottomHtml("");
    	
        return new ModelAndView("frontend/member", "model", model);
	}

	private ProductSearch getProductSearch(HttpServletRequest request) {
		ProductSearch search = new ProductSearch();
		request.setAttribute("frontProductSearchMember", search);
		
		return search;
	}
	
}
