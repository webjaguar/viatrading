package com.webjaguar.web.frontend;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.scheduling.quartz.CronTriggerBean;
import org.springframework.scheduling.quartz.JobDetailBean;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.web.form.MassEmailForm;
import com.webjaguar.web.quartz.MassEmailJob;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.EmailCampaign;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;

public class MassEmailController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
    private Scheduler scheduler;
	public void setScheduler(Scheduler scheduler) { this.scheduler = scheduler; }
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	public MassEmailController() {
		setSessionForm(true);
		setCommandName("form");
		setCommandClass(MassEmailForm.class);
		setFormView("frontend/massEmail");
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy[KK:mm a]", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, false, 20 ) );	
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
		
		if( request.getParameter("__back") != null ) {
			return new ModelAndView( new RedirectView("salesRep.jhtm") );
		}
		MassEmailForm form = (MassEmailForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
		boolean sendNow = false;
		if (request.getParameter("__sendnow") != null) {
			sendNow = true;
		}
		EmailCampaign campaign = new EmailCampaign(new Date());
		this.webJaguar.insertEmailCampaign( campaign );
		form.setCampaignId(campaign.getId());
		boolean scheduleResult = scheduleCampaign(form, sendNow, siteConfig, gSiteConfig);
		
		if ( !scheduleResult ) {
			model.put("message", "massEmail.error" );
		} else {
			model.put("scheduleDone", true );
			model.put("message", "massEmail.scheduledToSend" );
		}
		//campaign = new EmailCampaign(form.getCampaignName(), form.getScheduleTime(), form.getMessage(), ServletRequestUtils.getStringParameter( request, "_backend_user", "Admin" ), form.getSubject(), form.getNumEmails());
		campaign.setCampaignName(form.getCampaignName());
		campaign.setScheduled(form.getScheduleTime());
		campaign.setMessage(form.getMessage());
		campaign.setSender(ServletRequestUtils.getStringParameter( request, "_backend_user", "Admin" ));
		campaign.setSubject(form.getSubject());
		campaign.setEmailToSend(form.getNumEmails());
		this.webJaguar.updateEmailCampaign( campaign );
		
		return showForm(request, response, errors, model);
	}	
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Map<String, Object> salesRepParams = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		
		salesRepParams = (Map<String, Object>) session.getAttribute("salesRepParams");
		session.removeAttribute("salesRepParams");
		
		if(salesRepParams == null){
			throw new ModelAndViewDefiningException( new ModelAndView( new RedirectView("salesRep.jhtm") ) );
		}
		MassEmailForm form = new MassEmailForm();
		
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		
		Set<Integer> customerIds = (Set<Integer>) salesRepParams.get("customerIds");
		SalesRep salesRep = (SalesRep) salesRepParams.get("salesRep");
		
		form.setUserIds( customerIds );
		if(form.getUserIds() != null) {
			form.setNumEmails( form.getUserIds().size() );
		} else { 
			form.setNumEmails(0);
		}
		form.setFrom( salesRep.getEmail()  );	
    	GregorianCalendar now = new GregorianCalendar();
		now.add( Calendar.MINUTE, 1 );
		form.setScheduleTime( now.getTime() );

		return form;
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, org.springframework.validation.Errors errors) {
		MassEmailForm form = (MassEmailForm) command;
		Map<String, Object> map = new HashMap<String, Object>();
		int emailToSend = this.webJaguar.emailInProcessCount();
		List<Integer> Ids = new ArrayList<Integer>();
		if(!siteConfig.get( "SITE_MESSAGE_ID_FOR_SALES_REP" ).getValue().equals("")){
		String[] messageIds = siteConfig.get( "SITE_MESSAGE_ID_FOR_SALES_REP" ).getValue().split(",");
			if(messageIds != null && messageIds.length > 0){
				for(String id: messageIds){
					try{
						Ids.add(Integer.parseInt(id));
					} catch(Exception e){ }
				}
			}
		}
		List<SiteMessage> messageList =  new ArrayList<SiteMessage>();
		messageList = this.webJaguar.getSiteMessageList();
		Iterator<SiteMessage> itr = messageList.iterator();
		while(itr.hasNext()){
		    if(!Ids.contains(itr.next().getMessageId()))	{
		    	itr.remove();
		    }
		}
		map.put("messages", messageList);
		if ( Integer.parseInt( siteConfig.get( "MASS_EMAIL_POINT" ).getValue())  < form.getNumEmails() + emailToSend ) {
			map.put("massEmailPoint", Integer.parseInt( siteConfig.get( "MASS_EMAIL_POINT" ).getValue()));
			map.put("emailToSend", emailToSend);
			map.put("pointError", "massEmail.pointError");
			map.put("scheduleDone", true );
		}
		return map;
	}	
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		MassEmailForm form = (MassEmailForm) command;
		Calendar scheduledCalendar = Calendar.getInstance();
        scheduledCalendar.add(Calendar.MINUTE,1);
			
		boolean sendNow = false;
		if (request.getParameter("__sendnow") != null) {
			sendNow = true;
		}
	}
	
	private boolean scheduleCampaign(MassEmailForm massEmailForm, boolean sendNow, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) throws ParseException, SchedulerException{
		
		// get scheduled date,hour,minute
		Calendar scheduledCalendar = Calendar.getInstance();
		if ( sendNow ) {
			scheduledCalendar.add(Calendar.MINUTE,1);
		} else {
			scheduledCalendar.setTime( massEmailForm.getScheduleTime() );
		}
		
		int year = scheduledCalendar.get(Calendar.YEAR);
		int month = scheduledCalendar.get(Calendar.MONTH)+1;
		int day = scheduledCalendar.get(Calendar.DAY_OF_MONTH);
		int hour = scheduledCalendar.get(Calendar.HOUR_OF_DAY);
		int minute = scheduledCalendar.get(Calendar.MINUTE);
		
		// job detail quartz 2.x
//		JobDetail job = JobBuilder.newJob(MassEmailJob.class)
//	    .withIdentity(siteConfig.get( "MASS_EMAIL_JOB_NAME" ).getValue() + "_job_"+year+month+day+hour+minute, Scheduler.DEFAULT_GROUP)
//	    .storeDurably()
//	    .requestRecovery().build();
//		
//		job.getJobDataMap().put("massEmailForm",massEmailForm);
//		job.getJobDataMap().put("siteConfig",siteConfig);
//		job.getJobDataMap().put("gSiteConfig",gSiteConfig);

		// job detail 1.x
		JobDetailBean job = new JobDetailBean();
		//jobDetail.setJobClass(EmailCampaignJob.class);
		job.setJobClass(MassEmailJob.class);
		
		job.setName(siteConfig.get( "MASS_EMAIL_JOB_NAME" ).getValue() + "_job_"+year+month+day+hour+minute);
		job.setGroup(Scheduler.DEFAULT_GROUP);
		job.getJobDataMap().put("massEmailForm",massEmailForm);
		job.getJobDataMap().put("siteConfig",siteConfig);
		job.getJobDataMap().put("gSiteConfig",gSiteConfig);
		
		
		// trigger quartz 2.x
//		CronTrigger trigger = TriggerBuilder.newTrigger()
//	    .withIdentity(siteConfig.get( "MASS_EMAIL_JOB_NAME" ).getValue() + "_trigger_"+year+month+day+hour+minute, Scheduler.DEFAULT_GROUP)
//	    .withSchedule(CronScheduleBuilder.cronSchedule("0 "+ minute + " " + hour + " " + day + " " + month + " ? " + year))
//	    .build();
		
		
		// trigger quartz 1.x
		CronTriggerBean trigger = new CronTriggerBean();
		trigger.setStartTime(java.util.Calendar.getInstance().getTime());
		trigger.setCronExpression("0 "+ minute + " " + hour + " " + day + " " + month + " ? " + year);
		trigger.setName(siteConfig.get( "MASS_EMAIL_JOB_NAME" ).getValue() + "_trigger_"+year+month+day+hour+minute);
		trigger.setGroup(Scheduler.DEFAULT_GROUP);
		
		massEmailForm.setCampaignName(job.getDescription());
		try {
			// schedule a job
			scheduler.scheduleJob(job,trigger);
			return true;
		} catch (SchedulerException e) {
			return false;
		}
	}
}