/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.UserSession;

public class ListSupplierSalesController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {	

    	UserSession userSession = this.webJaguar.getUserSession(request);	
    	Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		int custId = userSession.getUserid();
		Customer customer = this.webJaguar.getCustomerById( custId );
		
		OrderSearch search = getSearch(request);
		search.setUserId(customer.getSupplierId());
		model.put("orderSearch", search);
		
		// orders
		PagedListHolder ordersList = 
			new PagedListHolder(this.webJaguar.getOrdersListBySupplier(search));
		ordersList.setPageSize(search.getPageSize());
		ordersList.setPage(search.getPage()-1);
		model.put("orders", ordersList);	
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		model.put("supplierSalesLayout", this.webJaguar.getSystemLayout("suppliersalelayout", request.getHeader("host"), request));
		return new ModelAndView("frontend/account/supplierSales", "model", model);    	
    }

	private OrderSearch getSearch(HttpServletRequest request) {
		OrderSearch search = new OrderSearch();
		
		// order number
		if (request.getParameter("orderNum") != null) {
			search.setOrderNum(ServletRequestUtils.getStringParameter( request, "orderNum", "" ));
		}
		
		// purchase order
		if (request.getParameter("purchaseOrder") != null) {
			search.setPurchaseOrder(ServletRequestUtils.getStringParameter(request, "purchaseOrder", ""));				
		}
	
		// status
		if (request.getParameter("status") != null) {
			search.setStatus(ServletRequestUtils.getStringParameter( request, "status", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}	
		return search;
	}

}
