/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.08.2010
 */

package com.webjaguar.web.frontend;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.Ordered;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.UserSession;

public class ApplicationErrorHandler implements HandlerExceptionResolver, Ordered
{
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	private int order;
	
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception e) {
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map<String, Object> myModel = new HashMap<String, Object>();
		String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();
		String ipAddress = request.getRemoteAddr();
		
		UserSession userSession = this.webJaguar.getUserSession( request );
		Customer customer = null;
		if ( userSession != null )
			customer = this.webJaguar.getCustomerById(userSession.getUserid());
		
		String message = "";
		String exception = "";
		
		if (customer != null) {
			exception += "Id:" + customer.getId() + "\n";
			exception += "first name:" + customer.getAddress().getFirstName() + "\n";
			exception += "last name:" + customer.getAddress().getLastName() + "\n";
		}
		
		exception = "root url\n";
		exception += url + "?" + query + "\n\n";
		exception += ipAddress;
		
		if (e instanceof NullPointerException) {
			message = "Error Code 0501";
		} else if (e instanceof Exception) {
			message = "Error Code 0500";
		} else {
			message = "Error Code 0000";
		}
		
		exception += "\nroot cause\n\n";
		for (StackTraceElement element : e.getStackTrace()) {
		   exception += element.toString() + "\n";
		}

		exception += e.toString();
		notifyAdmin(message + " on " + ((Configuration) siteConfig.get("SITE_URL")).getValue(), exception);
		
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		myModel.put("message", message);

		if (request.getServletPath().contains("admin")) {
			return new ModelAndView("admin/error", "model", myModel);  
		} else {
			request.setAttribute("_template", siteConfig.get("TEMPLATE").getValue());
			return new ModelAndView("frontend/error", "model", myModel);  
		}
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	String cc = "webjaguar@viatrading.com";

    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setCc(cc);
		msg.setBcc(cc);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 
	
	/**
	* Used by spring configuration to decide the order that the handler will be invokes
	* in relation to other handlers
	*/
	public int getOrder()
	{
		return order;
	}
	
	public void setOrder(int order)
	{
		this.order = order;
	}
}