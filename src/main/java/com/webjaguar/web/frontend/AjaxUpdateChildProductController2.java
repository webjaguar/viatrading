/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.web.frontend;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductOptionValue;
import com.webjaguar.model.ProductTab;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.domain.Constants;


public class AjaxUpdateChildProductController2 extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();

		UserSession userSession = this.webJaguar.getUserSession(request);
		Customer customer = null;
		if (userSession != null)
			customer = this.webJaguar.getCustomerById(userSession.getUserid());
		boolean customerSeeHiddendPrice = (customer == null || !customer.isSeeHiddenPrice()) ? false : true ;
		
		// protected access feature
		String protectedAccess = "0";
		if (customer != null) {
			protectedAccess = customer.getProtectedAccess();
		} else {
			protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			protectedAccess = Constants.FULL_PROTECTED_ACCESS;
		}
		
		// i18n
		String lang = null;
		if (request != null) {
			lang = RequestContextUtils.getLocale(request).getLanguage();
			if(lang.equalsIgnoreCase(LanguageCode.en.toString())) {
				lang = null;
			}
		}
		
		String skuParam = ServletRequestUtils.getStringParameter(request, "sku", "").trim();
		Integer idParam = ServletRequestUtils.getIntParameter(request, "id", 0);
		if (request.getParameter("id") != null || !skuParam.equals("")) {
			Product product = null;
			if (!skuParam.equals("")) {
				product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(skuParam), request);								
			} else {
				product = this.webJaguar.getProductById(idParam, request);				
			}
			if (product != null) {
				// update statistics
				this.webJaguar.nonTransactionSafeStats(product);
				
				// product fields
				Class<Product> c = Product.class;
				Method m = null;
				Object arglist[] = null;
				
				for (ProductField productField : this.webJaguar.getProductFieldsRanked(protectedAccess, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"), lang)) {
					if (productField.isDetailsField()) {
						m = c.getMethod("getField" + productField.getId());
						productField.setValue((String) m.invoke(product, arglist));
						if (productField.getValue() != null && productField.getValue().length() > 0) {
							product.addProductField(productField);
						}
					}
				}
				//	hide price
				if (product.isHidePrice() && !customerSeeHiddendPrice) {
					product.setPrice(null);
				}
				//	hide msrp
				if (product.isHideMsrp()) {
					product.setMsrp(null);
				}
				
				
				// initial ImageUrl
				Set<String> imageUrlSet = null;
				if (product.getProductOptions() != null) {
					imageUrlSet = new HashSet<String>();
					for (ProductOption op:product.getProductOptions()) {
						for(ProductOptionValue value : op.getValues()) {
							if(value.getIndex() == 0) {
								if(value.getImageUrl() != null && !value.getImageUrl().isEmpty()) {
									imageUrlSet.add(value.getImageUrl());
								}
							}
						}
					}
				}
				model.put("imageUrlSet", imageUrlSet);	
				
				// tab list
				List <ProductTab> tabList = null;
				Method m1 = null;
				Method m2 = null;
				int numTab = (Integer) gSiteConfig.get("gTAB");
				if (numTab > 0) {
					int i=0;
					tabList = new ArrayList<ProductTab>();
					while (i++ < numTab)
					{
						m1 = c.getMethod("getTab" + i);
						m2 = c.getMethod("getTab" + i + "Content");
						String tabName = ((String) m1.invoke(product, arglist));
						String tabContent = ((String) m2.invoke(product, arglist));
						if (tabName != null && tabName.length() > 0) {
							tabList.add(new ProductTab(tabName, tabContent, i));
						}
					}
					model.put("tabList", tabList);	
				}
				
				// long description
				if (siteConfig.get("DETAILS_LONG_DESC_HTML").getValue().equals("false") && product.getLongDesc() != null) {
					product.setLongDesc(product.getLongDesc().replace("\n", "<br>"));					
				}

				//inventory
				if ((Boolean) gSiteConfig.get("gINVENTORY")) {
					// check type on inventory for frontend
					if(siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly") ) {
						product.setInventory(product.getInventoryAFS());	
					}
					if(siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("ship") ) {
						product.setInventory(product.getInventory());	
					}
				}
			} else {
				// check redirect sku
				if (siteConfig.get("PRODUCT_INACTIVE_REDIRECT_FIELD").getValue().length() > 0) {
					String redirectURL = this.webJaguar.getProductRedirectUrl(skuParam, idParam, protectedAccess, "field_" + siteConfig.get("PRODUCT_INACTIVE_REDIRECT_FIELD").getValue());
					if (redirectURL != null && redirectURL.trim().length() > 0) {
						response.setStatus(301);
						response.setHeader("Location", redirectURL);
						response.setHeader("Connection", "close");
					}
				}
				
				Layout productLayout = this.webJaguar.getSystemLayout("productNotFound", request.getHeader("host"), request);
				model.put("productLayout", productLayout);
				model.put("message", "product.exception.notfound");
			}
			model.put("product", product);
		}
		return new ModelAndView("frontend/ajax/updateChildProduct2", "model", model);
	}
}