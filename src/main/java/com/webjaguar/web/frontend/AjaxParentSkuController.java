/* Copyright 2005, 2011 Advanced E-Media Solutions
 * author: Jwalant Patel
 */

package com.webjaguar.web.frontend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Brand;
import com.webjaguar.model.BudgetProduct;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.UserSession;

public class AjaxParentSkuController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {	
		
		Map<String, Object> model = new HashMap<String, Object>();
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	UserSession userSession = (UserSession) request.getAttribute( "userSession" );
    	Cart cart = new Cart();
		
		String sku = ServletRequestUtils.getStringParameter(request, "sku");
		System.out.print(sku+"" +"++++++");
		
		Product product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(sku), 0, false, null);
		System.out.print(product.getMasterSku() + "----" + product.getName());
		
		Product parentProduct = null;
		if(product.getMasterSku()!=null){
			parentProduct = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(product.getMasterSku()),  (Integer) gSiteConfig.get("gPRODUCT_IMAGES"), false, null);
		}
		
		// check if anything was removed
		String remove[] = request.getParameterValues("remove");

    	
		Integer userId = null;
		if (userSession != null) { // cart stored in database
			userId = userSession.getUserid();		
			// check if update button was clicked
			cart = this.webJaguar.getUserCart(userId, null);
			cart.setUserId( userId );
			model.put("email", this.webJaguar.getCustomerById(userId).getUsername());
		} else { // cart stored in session
			Cart sessionCart = (Cart) WebUtils.getSessionAttribute(request, "sessionCart");
			if (sessionCart != null) cart = sessionCart;
			// check if update button was clicked
			
			model.put("sessionId", request.getSession().getId());					
		}
		
		String message = this.webJaguar.updateCart(cart, (Map<String, Object>) request.getAttribute("gSiteConfig"), (Map<String, Configuration>) request.getAttribute("siteConfig"), RequestContextUtils.getLocale(request).getLanguage(), request.getHeader("host"), model);
		
		if (userSession != null) {
			request.getSession().setAttribute("userCart", cart);
			if ((Boolean) gSiteConfig.get("gBUDGET_BRAND")) {
				Map<Brand, Double> brandsMap = new HashMap<Brand, Double>();
				if (this.webJaguar.getSubTotalPerBrand(cart, brandsMap)) {
					model.put("isOverBudget", true);
				}
				if (brandsMap.size() > 0) {
					model.put("brandsMap", brandsMap);					
				}
			}
			if ((Boolean) gSiteConfig.get("gBUDGET_PRODUCT")) {
				Map<String, BudgetProduct> budgetProduct = new HashMap<String, BudgetProduct>();
				if (this.webJaguar.getBudgetByCart(cart, budgetProduct)) {
					model.put("isOverBudget", true);
				}
				model.put("budgetProduct", budgetProduct);
			}
			
			// save cart total per line item to filter customer on admin
			Double tempCartTotal = cart.getSubTotal();
			for(CartItem cartItem: cart.getCartItems()) {
				if(tempCartTotal != null && cartItem.getTempCartTotal() != null && !tempCartTotal.equals(cartItem.getTempCartTotal())) {
					cartItem.setTempCartTotal(tempCartTotal);
					this.webJaguar.updateCartItem(cartItem);
				}
			}
		}
			
		// Recommended List
		List <Product> recommendedList = new  ArrayList<Product>();
		List <String> masterSku = new ArrayList<String>();
		Set<String> recommendedSku = new TreeSet<String>();
		for(CartItem cartItem: cart.getCartItems()) {
			masterSku.add(cartItem.getProduct().getSku());
		}
		
		Iterator<String> iter = recommendedSku.iterator();
		while(iter.hasNext()){
			Product recommendedProduct = this.webJaguar.getProductById( this.webJaguar.getProductIdBySku( iter.next() ), request );
			if(recommendedProduct != null){
				recommendedList.add(recommendedProduct);	
			}	
		}
		
		if (!recommendedList.isEmpty()) {	
			model.put( "recommendedList", recommendedList );
			if(model.get("limit") == null) {
				model.put("limit", recommendedList.size());
			}
		}

		Map<String, Object> params = new HashMap<String, Object>();
		// check Buy safe
		
		// check if checkout button was pressed
		if (request.getParameter("_checkout.x") != null || request.getParameter("_checkout1.x") != null || request.getParameter("_checkout1.jhtm") != null) {

			String manufacturerName = ServletRequestUtils.getStringParameter(request, "manufacturerName", "");
			if (cart.isContinueCart() || manufacturerName.trim().length() > 0) {
				
				request.getSession().setAttribute("manufacturerName", manufacturerName);
				
				Random randomGenerator = new Random();
			    int randomInt = randomGenerator.nextInt(100);
			    if (randomInt < Integer.parseInt(siteConfig.get("CHECKOUT2_VISIBILITY").getValue())) {
			    	return new ModelAndView(new RedirectView("checkout2.jhtm"), params);
			    } else if(request.getParameter("_checkout1.x") != null) {
			    	return new ModelAndView(new RedirectView("checkout1.jhtm")); 
			    } else {
			    	return new ModelAndView(new RedirectView("checkout.jhtm"), params);
			    }
			}
		}	
		
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		if (message != null ) model.put("message", message); 
		if (request.getSession().getAttribute( "continueShoppingUrl" ) != null) model.put("continueShoppingUrl", request.getSession().getAttribute( "continueShoppingUrl" )); 

		model.put("cart", cart);
		model.put("product", parentProduct);
		Layout layout = (Layout) request.getAttribute( "layout" );
		layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Cart"));
		Layout cartLayout = this.webJaguar.getSystemLayout("cart", request.getHeader("host"), request);
		// rightBar
		if (cartLayout.getRightBarTopHtml() != null && cartLayout.getRightBarTopHtml().trim().length() > 0) {
			layout.setRightBarTopHtml(cartLayout.getRightBarTopHtml());				
		 }
		// leftBar
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} else {
			if (cartLayout.getLeftBarTopHtml() != null && cartLayout.getLeftBarTopHtml().trim().length() > 0) {
				layout.setLeftBarTopHtml(cartLayout.getLeftBarTopHtml());				
			 }
		}
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		model.put("cartLayout", cartLayout);

		model.put("cart", cart);
		//System.out.println("#####################");
		return new ModelAndView("frontend/common/parentSkuView", "model", model);
	}
	
}