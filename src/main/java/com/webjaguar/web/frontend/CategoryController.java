/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.soap.SOAPFaultException;


import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.facet.params.FacetSearchParams;
import org.apache.lucene.facet.search.CountFacetRequest;
import org.apache.lucene.facet.search.FacetRequest;
import org.apache.lucene.facet.search.FacetResult;
import org.apache.lucene.facet.search.FacetResultNode;
import org.apache.lucene.facet.search.FacetsCollector;
import org.apache.lucene.facet.taxonomy.CategoryPath;
import org.apache.lucene.facet.taxonomy.TaxonomyReader;
import org.apache.lucene.facet.taxonomy.directory.DirectoryTaxonomyReader;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queries.BooleanFilter;
import org.apache.lucene.queries.ChainedFilter;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.FieldCacheTermsFilter;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.MultiCollector;
import org.apache.lucene.search.NumericRangeFilter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.QueryWrapperFilter;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopFieldDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.BooleanClause.Occur;
import org.apache.lucene.search.BooleanQuery.TooManyClauses;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import spexlive.etilize.com.Catalog;
import spexlive.etilize.com.CatalogServiceIntf;
import spexlive.etilize.com.GetProduct;
import spexlive.etilize.com.ProductId;
import spexlive.etilize.com.SelectProductFields;
import spexlive.etilize.com.Sku;
import spexlive.etilize.com.SubcatalogFilter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.FilterAttribute;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LuceneProductSearch;
import com.webjaguar.model.NameValues;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.Price;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.model.RangeValue;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SpecialPricing;
import com.webjaguar.model.Supplier;
import com.webjaguar.model.UserSession;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.thirdparty.lucene.ViaSpecialQuery;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.Utilities;
import com.webjaguar.web.interceptor.SiteInterceptor;

public class CategoryController extends WebApplicationObjectSupport implements Controller {
	private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

	private WebJaguarFacade webJaguar;
	
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private String catName;

	public void setCatName(String catName) {
		this.catName = catName;
	}
	
	private String shipping;
	private Double totalCharges;
	private String message;
	private Double shippingCost;
	private DecimalFormat numFormat = new DecimalFormat("#,###.00");
	

	Map<String, Configuration> siteConfig;
	Category i18nCategory = null;
	String lang = null;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
		UserSession userSession = this.webJaguar.getUserSession(request);
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		List<String> skuList = new ArrayList<String>();
		String[] skuArray = null;
		
		// Mobile Layout
    	String mobile = "";
    	boolean homePage = true;
		if ((request.getAttribute("_mobile").toString().equalsIgnoreCase("_mobile"))) {
			mobile = request.getAttribute("_mobile").toString();
		}
		
		String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();

		Map<String, Object> myModel = new HashMap<String, Object>();
		
		Boolean adminLogin = (Boolean) request.getSession().getAttribute("adminLogin");
		if(adminLogin!=null && adminLogin){
			myModel.put("adminLogin", true);
		}

		
		if(request.getParameter("state")!=null && request.getParameter("state").equalsIgnoreCase("asc")){
			myModel.put("state", "desc");
		}else{
			myModel.put("state", "asc");
		}
		
		myModel.put("url", url + "?" + query);
		
		Cart cart = null;
		Integer userId = null;
		Double subTotal = 00.0;
		Double grandTotal = 00.0;
		Double discount = 00.0;
		Double tax = 00.0;
		Double ccFee = 00.0;

		Customer customer = null;
		if(userSession!=null){
	    	userSession = (UserSession) request.getAttribute( "userSession" );
		}
		if (userSession != null) { 
			userId = userSession.getUserid();
			customer = this.webJaguar.getCustomerById(userId);
			cart = this.webJaguar.getUserCart(userId, null);
		}else { 
			cart = (Cart) WebUtils.getSessionAttribute(request, "sessionCart");
		}
		
		if(cart!=null){
			for(CartItem cartItem : cart.getCartItems()) {
				cartItem.setProduct(this.webJaguar.getProductById(cartItem.getProduct().getId(), request));
			}
			String message = this.webJaguar.updateCart(cart, (Map<String, Object>) request.getAttribute("gSiteConfig"), (Map<String, Configuration>) request.getAttribute("siteConfig"), RequestContextUtils.getLocale(request).getLanguage(), request.getHeader("host"), null);
			for(CartItem cartItem: cart.getCartItems()) {
				
				if(cartItem.getUnitPrice()==null){
					cartItem.setUnitPrice(0.0);
				}
				
				subTotal += (cartItem.getQuantity()*cartItem.getUnitPrice());
				discount += (cartItem.getDiscount());
				
				URL url2 = new URL(request.getScheme()+"://www.viatrading.com/dv/liveapps/showrelatedskus.php?sku="+cartItem.getProduct().getSku());
				HttpURLConnection con = (HttpURLConnection) url2.openConnection();
			    con.setRequestMethod("GET");
			    con.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			    con.addRequestProperty("User-Agent", "Mozilla");
			    con.addRequestProperty("Referer", "google.com");
			    con.setRequestProperty("Accept", "text/html, text/*");
			    con.addRequestProperty("Content-Type","application/json; charset=UTF-8"); 
				
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				 
				while((inputLine = in.readLine()) != null)
				{
				    if(inputLine!=null && !inputLine.isEmpty()){
				    	skuArray = inputLine.split(",");
				    }
				}
				if(skuArray!=null){
					for(String tmp : skuArray){
						skuList.add(tmp);
					}
				}
				in.close();
			}
		}
		
		myModel.put("cart", cart);
		myModel.put("subTotal", subTotal);
		myModel.put("discount", discount);
		myModel.put("tax", tax);
	
		StringBuilder sb2 = new StringBuilder();
		if(skuList!=null){
			for(String sku : skuList){
				Integer productId = this.webJaguar.getProductIdBySku(sku);
				Product product = this.webJaguar.getProductById(productId, request);
				if(product!=null){
					sb2.append("<div class=\"sc_product\">");
					sb2.append("<div class=\"sc_product_image\">");
	
					String productLink = null;		
	
					if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
						productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
					} else {
						productLink = "product.jhtm?id=" + product.getId();
					}
					
					sb2.append("<a href=\""+productLink+"\">");
					if(product.getThumbnail()!=null){
						sb2.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
					}else{
						sb2.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
					}
					sb2.append("</a>");
					
					sb2.append("</div>");
					sb2.append("<div class=\"sc_product_name\">");
					sb2.append("<a href=\""+productLink+"\">");sb2.append(product.getName());sb2.append("</a>");
					sb2.append("</div>");
					sb2.append("<div class=\"sc_product_sku\">");
					sb2.append(product.getSku());
					sb2.append("</div>");
					sb2.append("<div class=\"sc_product_price\">");
					if(product.getProductLayout()!=null && product.getProductLayout().equalsIgnoreCase("049")){
						sb2.append((product.getField11()!=null)?product.getField11():"");
					}else{
						sb2.append((product.getPrice1()!=null)?product.getPrice1():"");
					}
					sb2.append("</div>");
					sb2.append("</div>");
				}
			}
		}
		
		myModel.put("sb", sb2); 
		
		String shippingType = "";
		String shippingMessage = "";
		
		shippingCost = 0.0;
		
		//shippingCalculate(myModel, request);
		
		myModel.put("shippingCost", shippingCost);
		myModel.put("shippingType", shippingType);
		myModel.put("shippingMessage", shippingMessage);
		
		if(shippingCost==null){
			shippingCost = 0.0;
		}
		
		
		//ccFee = (Math.floor((subTotal - discount + tax + shippingCost)*3))/100;
		ccFee = 0.0;
		
		grandTotal = subTotal - discount + ccFee + shippingCost + tax;
		myModel.put("ccFee", ccFee);
		myModel.put("grandTotal", grandTotal);

		if (request.getParameter("fsort") != null && request.getParameter("fsort") != "") {
			myModel.put("fsort", request.getParameter("fsort"));
		}
		
		if (request.getParameter("page") != null && request.getParameter("page") != "") {
			myModel.put("page", request.getParameter("page"));
		}
		
		// continue shopping
		request.getSession().setAttribute("continueShoppingUrl", myModel.get("url"));

		// Show add presentation only when salesrep cookie is there.
		if ((Boolean) gSiteConfig.get("gPRESENTATION")) {
			myModel.put("showAddPresentation", (request.getAttribute("cookieSalesRep") != null && customer == null) ? true : false);
		}
		myModel.put("userId", customer!=null?customer.getId():"");
		myModel.put("userSessionEnabled", userSession!=null?true:false);
		
		boolean customerSeeHiddendPrice = (customer == null || !customer.isSeeHiddenPrice()) ? false : true;
		// Show add presentation only when salesrep cookie is there.
		String protectedAccess = "0";
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			protectedAccess = Constants.FULL_PROTECTED_ACCESS;
		} else {
			if (customer != null) {
				protectedAccess = customer.getProtectedAccess();
			} else {
				protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
			}
		}
		Category thisCategory = null;
		Integer categoryId = null;
		if (request.getParameter("sid") == null || ((String) gSiteConfig.get("gSHARED_CATEGORIES")).equals("")) {
			if (request.getParameter("cid") != null) {
				categoryId = ServletRequestUtils.getIntParameter(request, "cid", -1);
				int homepageCategoryId = this.webJaguar.getHomePageByHost(null);
				homePage = false;
				if(categoryId == homepageCategoryId){
					homePage = true;
				}
				thisCategory = this.webJaguar.getCategoryById(categoryId, protectedAccess);
			} else {
				if (this.catName == null) {
					categoryId = this.webJaguar.getHomePageByHost(request.getHeader("host"));
					if (categoryId == null) {
						categoryId = this.webJaguar.getHomePageByHost(null);
						homePage = true;
					}
				} else {
					homePage = false;
					try {
						if (this.catName.equals("contactUs")) {// get contact us category id
							categoryId = Integer.parseInt(siteConfig.get("CONTACT_US_ID").getValue());
						} else if (this.catName.equals("aboutUs")) {// get about us category id
							categoryId = Integer.parseInt(siteConfig.get("ABOUT_US_ID").getValue());
						}
					} catch (Exception e) {
					}
				}
				thisCategory = this.webJaguar.getCategoryById(categoryId, protectedAccess);
			}
		}
		if (thisCategory != null) {
			ProductSearch productSearch = getProductSearch(request, thisCategory, gSiteConfig);
				
			if (request.getParameter("page") == null || request.getParameter("page").equalsIgnoreCase("") && (thisCategory.getLayoutId() == 7)) {
				myModel.put("page", 25);
			}
			if (request.getParameter("page") == null || request.getParameter("page").equalsIgnoreCase("") && (thisCategory.getLayoutId() != 7)) {
				myModel.put("page", 52);
			}
			
			if (request.getParameter("fsort") != null && request.getParameter("fsort") != "") {
				myModel.put("fsort", request.getParameter("fsort"));
			}
			
			if ((Boolean) gSiteConfig.get("gMASTER_SKU")) {
				if (thisCategory.getParentChildDisplay() == 0) {
					if (Integer.parseInt(siteConfig.get("PARENT_CHILDERN_DISPLAY").getValue()) == 0) {
						// Hide Children
						productSearch.setMasterSku(true);
					}
				} else if (thisCategory.getParentChildDisplay() == 2) {
					// Hide Children
					productSearch.setMasterSku(true);
				}
			}
			/*
			 * if ((Boolean) gSiteConfig.get("gBUDGET_PRODUCT")) { if (customer != null) { productSearch.setUserIdBudget(customer.getId()); } else { productSearch.setUserIdBudget(-1); } }
			 */

			// i18n
			lang = RequestContextUtils.getLocale(request).getLanguage();
			if (((String) gSiteConfig.get("gI18N")).length() > 0 && this.webJaguar.getLanguageSetting(lang) != null) {
				i18nCategory = this.webJaguar.getI18nCategory(thisCategory.getId(), lang).get(lang);
				if (i18nCategory != null) {
					if (i18nCategory.getI18nHtmlCode() != null && i18nCategory.getI18nHtmlCode().trim().length() > 0) {
						thisCategory.setHtmlCode(i18nCategory.getI18nHtmlCode());
					}
					if (i18nCategory.getI18nProtectedHtmlCode() != null && i18nCategory.getI18nProtectedHtmlCode().trim().length() > 0) {
						thisCategory.setProtectedHtmlCode(i18nCategory.getI18nProtectedHtmlCode());
					}
					if (i18nCategory.getI18nHeadTag() != null && i18nCategory.getI18nHeadTag().trim().length() > 0) {
						thisCategory.setHeadTag(i18nCategory.getI18nHeadTag());
					}
				}
			}
			myModel.put("lang", lang);

			// get protected html
			if (protectedLevels > 0) {
				if (!thisCategory.getProtectedLevel().equals("0") && thisCategory.isProtect()) {
					if (thisCategory.getProtectedHtmlCode() != null && thisCategory.getProtectedHtmlCode().trim().length() > 0) {
						thisCategory.setHtmlCode(thisCategory.getProtectedHtmlCode());
					}
				}
			}

			// get tracking code
			if (thisCategory.getHtmlCode() != null) {
				StringBuffer sb = new StringBuffer(thisCategory.getHtmlCode());
				sb = Utilities.replaceString(sb, "#trackcode#", (String) request.getAttribute("trackcode"), false);
				sb = Utilities.replaceString(sb, "#sessionid#", WebUtils.getSessionId(request), false);
				if (customer != null) {
					sb = Utilities.replaceString(sb, "#firstname#", customer.getAddress().getFirstName(), false);
					sb = Utilities.replaceString(sb, "#lastname#", customer.getAddress().getLastName(), false);
					sb = Utilities.replaceString(sb, "#email#", customer.getUsername(), false);
				}
				
				Pattern pattern = Pattern.compile("(#slideShow\\?)([0-9]+)(#)");
				Matcher matcher = pattern.matcher(sb);
				Integer cid = null;
				try {
					while (matcher.find()) {
						cid = Integer.parseInt(matcher.group(2));
						//test = test.replaceAll("#slideShow\\?"+cid+"#", "Hello");
						sb = Utilities.replaceString(sb, "#slideShow\\?"+cid+"#", this.getSlidShowCode(request, cid), false);

						String tempHtmlCode = sb.toString();
						tempHtmlCode = tempHtmlCode.replaceAll("#slideShow\\?"+cid+"#", Matcher.quoteReplacement(this.getSlidShowCode(request, cid)));
						sb = new StringBuffer(tempHtmlCode);	
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				SalesRep salesRep = null;
				if ((Boolean) gSiteConfig.get("gSALES_REP") && customer != null) {
					salesRep = this.webJaguar.getSalesRepById(customer.getSalesRepId());
				}
				if (salesRep != null) {
					sb = Utilities.replaceString(sb, "#salesRepName#", salesRep.getName(), false);
					sb = Utilities.replaceString(sb, "#salesRepEmail#", salesRep.getEmail(), false);
					sb = Utilities.replaceString(sb, "#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone(), false);
					sb = Utilities.replaceString(sb, "#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone(), false);
					if (salesRep.getAddress() != null) {
						sb = Utilities.replaceString(sb, "#salesRepAddress1#", (salesRep.getAddress().getAddr1() == null) ? "" : salesRep.getAddress().getAddr1(), false);
						sb = Utilities.replaceString(sb, "#salesRepAddress2#", (salesRep.getAddress().getAddr2() == null) ? "" : salesRep.getAddress().getAddr2(), false);
						sb = Utilities.replaceString(sb, "#salesRepCity#", (salesRep.getAddress().getCity() == null) ? "" : salesRep.getAddress().getCity(), false);
						sb = Utilities.replaceString(sb, "#salesRepCountry#", (salesRep.getAddress().getCountry() == null) ? "" : salesRep.getAddress().getCountry(), false);
						sb = Utilities.replaceString(sb, "#salesRepStateProvince#", (salesRep.getAddress().getStateProvince() == null) ? "" : salesRep.getAddress().getStateProvince(), false);
						sb = Utilities.replaceString(sb, "#salesRepZip#", (salesRep.getAddress().getZip() == null) ? "" : salesRep.getAddress().getZip(), false);
						sb = Utilities.replaceString(sb, "#salesRepFax#", (salesRep.getAddress().getFax() == null) ? "" : salesRep.getAddress().getFax(), false);
						sb = Utilities.replaceString(sb, "#salesRepCompany#", (salesRep.getAddress().getCompany() == null) ? "" : salesRep.getAddress().getCompany(), false);
					}
				} else {
					sb = Utilities.replaceString(sb, "#salesRepName#", "", false);
					sb = Utilities.replaceString(sb, "#salesRepEmail#", "", false);
					sb = Utilities.replaceString(sb, "#salesRepPhone#", "", false);
					sb = Utilities.replaceString(sb, "#salesRepCellPhone#", "", false);
					sb = Utilities.replaceString(sb, "#salesRepAddress1#", "", false);
					sb = Utilities.replaceString(sb, "#salesRepAddress2#", "", false);
					sb = Utilities.replaceString(sb, "#salesRepCity#", "", false);
					sb = Utilities.replaceString(sb, "#salesRepCountry#", "", false);
					sb = Utilities.replaceString(sb, "#salesRepStateProvince#", "", false);
					sb = Utilities.replaceString(sb, "#salesRepZip#", "", false);
					sb = Utilities.replaceString(sb, "#salesRepFax#", "", false);
					sb = Utilities.replaceString(sb, "#salesRepCompany#", "", false);
				}
				thisCategory.setHtmlCode(sb.toString());
			}
			// update statistics
			this.webJaguar.nonTransactionSafeStats(thisCategory);
			if ((lang.equalsIgnoreCase(LanguageCode.en.toString())) && (thisCategory.getUrl() != null && thisCategory.getUrl().length() > 0)) {
				if (thisCategory.isRedirect301()) {
					response.setStatus(301);
					response.setHeader("Location", thisCategory.getUrl());
					response.setHeader("Connection", "close");
				} else {
					return new ModelAndView(new RedirectView(thisCategory.getUrl()));
				}
			} else if ((lang.equalsIgnoreCase(LanguageCode.es.toString())) && (thisCategory.getUrlEs() != null && thisCategory.getUrlEs().length() > 0)){
				return new ModelAndView(new RedirectView(thisCategory.getUrlEs()));
			}
			myModel.put("thisCategory", thisCategory);
			myModel.put("cid", thisCategory.getId());
			// base image file
			File baseImageFile = new File(getServletContext().getRealPath("/assets/Image"));
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
				if (prop.get("site.root") != null) {
					baseImageFile = new File((String) prop.get("site.root"), "/assets/Image");
				}
			} catch (Exception e) {
			}
			myModel.put("baseImageFile", baseImageFile);
			File baseCategoryFile = new File(baseImageFile, "Category");
			File imageBig = new File(baseCategoryFile, "cat_" + thisCategory.getId() + "_big.gif");
			myModel.put("imageBigExist", (imageBig.exists() && !imageBig.getName().equals("cat_null_big.gif")) ? true : false);
			// sub categories
			Integer subcatLevels = new Integer(siteConfig.get("SUBCAT_LEVELS").getValue());
			if (thisCategory.getSubcatLevels() != null) {
				subcatLevels = thisCategory.getSubcatLevels();
			}
			if (subcatLevels > 0) {
				List<Category> subCategories = this.webJaguar.getCategoryLinks(thisCategory.getId(), request);
				// check if image exist. This is done just for Safari
				// other browsers do not show, no image.
				for (Iterator<Category> i = subCategories.iterator(); i.hasNext();) {
					Category cat = i.next();
					File image = new File(baseCategoryFile, "cat_" + cat.getId() + ".gif");
					cat.setHasImage(image.exists());
				}

				myModel.put("subCategories", subCategories);
				if (siteConfig.get("SUBCAT_DISPLAY").getValue().equals("") || siteConfig.get("SUBCAT_DISPLAY").getValue().equals("onePerRow")) {
					// subs by column
					int subcatCols = thisCategory.getSubcatCols();
					if (subcatCols < 1) {
						subcatCols = 1;
					}
					int[] subcatLinksPerCol = new int[subcatCols];
					int col = 0;
					for (int i = 0; i < subCategories.size(); i++) {
						subcatLinksPerCol[col++] += 1;
						if (col == subcatCols)
							col = 0;
					}
					myModel.put("subcatLinksPerCol", subcatLinksPerCol);
				}

				if (subcatLevels > 1) {
					Map<Integer, int[]> subcatLinksPerColMap = new HashMap<Integer, int[]>();

					Iterator<Category> iter = subCategories.iterator();
					while (iter.hasNext()) {
						Category category = iter.next();
						getSubCategoryLinks(category, request, 0, 1);

						if (siteConfig.get("SUBCAT_DISPLAY").getValue().equals("onePerRow")) {
							// subs by column
							int subcatCols = thisCategory.getSubcatCols();
							if (subcatCols < 1) {
								subcatCols = 1;
							}
							int[] subcatLinksPerCol = new int[subcatCols];
							int col = 0;
							for (int i = 0; i < category.getSubCategories().size(); i++) {
								subcatLinksPerCol[col++] += 1;
								if (col == subcatCols)
									col = 0;
							}
							subcatLinksPerColMap.put(category.getId(), subcatLinksPerCol);
						}

					}
					myModel.put("subcatLinksPerColMap", subcatLinksPerColMap);
				}
			}
			// product fields filter
			List<ProductField> searchProductField = null;
			List<ProductField> productFields;
			List<NameValues> dropDownList = null;

			if (thisCategory.getProductFieldSearchType().equals("0")) {
				thisCategory.setProductFieldSearchType(siteConfig.get("PRODUCT_FIELDS_SEARCH_TYPE").getValue());
			}

			if ((Integer) gSiteConfig.get("gPRODUCT_FIELDS") > 0 && (Boolean) gSiteConfig.get("gPRODUCT_FIELD_SEARCH")) {
				productSearch.setgPRODUCT_FIELDS((Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
			}

	        if ((Integer) gSiteConfig.get("gPRODUCT_FIELDS") > 0 && (Boolean) gSiteConfig.get("gPRODUCT_FIELD_SEARCH")) {
				searchProductField = new ArrayList<ProductField>();
				dropDownList = this.webJaguar.getProductFields(thisCategory, request);
				Map<String, ProductField> productFieldMap = new HashMap<String, ProductField>();
				this.webJaguar.getProductFields(null, productSearch.getgPRODUCT_FIELDS());
				boolean setNewMapping = false;

				for (int i = 1; i <= productSearch.getgPRODUCT_FIELDS(); i++) {
					Double minValue = null, maxValue = null;
					if ((request.getParameter("field_" + i + "_min") != null && !request.getParameter("field_" + i + "_min").isEmpty())
							|| (request.getParameter("field_" + i + "_max") != null && !request.getParameter("field_" + i + "_max").isEmpty())) {
						setNewMapping = true;
						if (request.getParameter("field_" + i + "_min") != null && !request.getParameter("field_" + i + "_min").isEmpty()) {
							minValue = Double.parseDouble(request.getParameter("field_" + i + "_min"));
							myModel.put("minValue_" + i, request.getParameter("field_" + i + "_min"));
						}
						if (request.getParameter("field_" + i + "_max") != null && !request.getParameter("field_" + i + "_max").isEmpty()) {
							maxValue = Double.parseDouble(request.getParameter("field_" + i + "_max"));
							myModel.put("maxValue_" + i, request.getParameter("field_" + i + "_max"));
						}
						productFieldMap.put("field_" + i, new ProductField(i, minValue, maxValue));
					}
					if (request.getParameter("field_" + i) != null) {
						setNewMapping = true;
						List<String> selectedValues = new ArrayList<String>();
						String[] values = ServletRequestUtils.getStringParameters(request, "field_" + i);
						for (int x = 0; x < values.length; x++) {
							if (values[x].trim().length() > 0) {
								selectedValues.add(values[x].trim());
							}
						}
						if (selectedValues.size() > 0) {
							productFieldMap.put("field_" + i, new ProductField(i, selectedValues));
						}
					}
				}
				if (setNewMapping) {
					if (request.getSession().getAttribute("catProductFieldMap") == null) {
						request.getSession().setAttribute("catProductFieldMap", new HashMap<String, Map<String, ProductField>>());
					}
					((Map<String, Map<String, ProductField>>) request.getSession().getAttribute("catProductFieldMap")).put("cat_" + thisCategory.getId(), productFieldMap);
				}
				if (request.getSession().getAttribute("catProductFieldMap") != null
						&& ((Map<String, Map<String, ProductField>>) request.getSession().getAttribute("catProductFieldMap")).get("cat_" + thisCategory.getId()) != null) {
					searchProductField.addAll(((Map<String, Map<String, ProductField>>) request.getSession().getAttribute("catProductFieldMap")).get("cat_" + thisCategory.getId()).values());
				}
			}
	        
	        myModel.put("catProductFieldMap", (Map<String, Map<String, ProductField>>) request.getSession().getAttribute("catProductFieldMap"));
	        
	        boolean emptyField = true;
	        for (int i = 1; i <= productSearch.getgPRODUCT_FIELDS(); i++) {
	        	if (request.getParameter("field_" + i) != null){
	        		emptyField = false;
	        	}
	        }
	        
	        if (request.getParameter("reset") != null && request.getParameter("reset") != "") {
	        	emptyField = true;
	        }
	        if(emptyField){
		        myModel.put("catProductFieldMap", null);	
	        }
	        
			myModel.put("productFieldSearchType", thisCategory.getProductFieldSearchType());
			// sku filter
			if (request.getParameter("skuStart") != null) {
				if (request.getParameter("skuValue") != null) {
					String productSku = request.getParameter("skuStart") + request.getParameter("skuValue");
					productSearch.setSku(productSku);
					myModel.put("skuValue", request.getParameter("skuValue"));
				} else {
					productSearch.setSku(request.getParameter("skuStart"));
				}
				myModel.put("skuStart", request.getParameter("skuStart"));
			}
			// canonical link
			if (gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1")) {
				if (siteConfig.get("CATEGORY_OVERRIDE_CANONICAL").getValue().length() > 0) {
					// category fields
					Class<Category> c = Category.class;
					Method m = null;
					Object arglist[] = null;
					m = c.getMethod("getField" + siteConfig.get("CATEGORY_OVERRIDE_CANONICAL").getValue());
					if(m.invoke(thisCategory, arglist) != null && !m.invoke(thisCategory, arglist).toString().trim().isEmpty()) {
						myModel.put("canonicalLink", m.invoke(thisCategory, arglist));
					} else {
						myModel.put("canonicalLink",
								request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/" + siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/" + thisCategory.getId() + "/"
										  + thisCategory.getEncodedName() + ".html");
					}
					
				} else {
					myModel.put("canonicalLink",
							request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/" + siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/" + thisCategory.getId() + "/"
									  + thisCategory.getEncodedName() + ".html");
				}
				if (thisCategory.isHomePage()) {
					myModel.put("canonicalLink", request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/");
				}
			}
			
			
			int count = 0;			
			// If Left bar is set to have Lucene Search Filters
			Map<String, Object> lCategoryModel = null;
			if((thisCategory.getLayoutId() != 7) && siteConfig.get("SHOW_LUCENE_SEARCH_FILTER").getValue().equals("true") && ((thisCategory.getLeftbarType() != null && thisCategory.getLeftbarType().equalsIgnoreCase("6"))  || siteConfig.get("LEFTBAR_DEFAULT_TYPE").getValue().equalsIgnoreCase("6"))) {
				LuceneCategery lCategory = new LuceneCategery();
				
				File indexFile;
				File productsTaxoDir;

				indexFile = new File(getServletContext().getRealPath("/lucene_index/products"));
				productsTaxoDir = new File(getServletContext().getRealPath("/lucene_index/productsTaxoDir" ));
				boolean includeSubCategoriesProducts = false;
							
				lCategoryModel = lCategory.getLuceneProduct(request, gSiteConfig, siteConfig, webJaguar, customer, indexFile, myModel, productsTaxoDir, thisCategory.getId(), thisCategory.getName(), includeSubCategoriesProducts);
				if(lCategoryModel != null && !lCategoryModel.isEmpty()) {
						count = Integer.parseInt(lCategoryModel.get("count").toString());
				}
			} else {
				if(emptyField){
					searchProductField = null;	
		        }
				count = this.webJaguar.getProductsByCategoryIdCount(thisCategory, searchProductField, request, productSearch);

			}	
			
			// set layout
			setLayout(thisCategory, request, response, myModel, count, mobile, homePage, cart);
			
			if (thisCategory.getLimit() != null) {
				count = thisCategory.getLimit();
			}

			if (count < productSearch.getOffset() - 1) {
				productSearch.setPage(1);
			}
			productSearch.setLimit(productSearch.getPageSize());
			productSearch.setOffset((productSearch.getPage() - 1) * productSearch.getPageSize());

			if ((productSearch.getOffset() + productSearch.getLimit()) > count) {
				productSearch.setLimit(Math.abs(productSearch.getOffset() - count));
			}

			// via quickmode2 default sort can move to VIATRADING Branch later
			if (siteConfig.get("SITE_URL").getValue().contains("viatrading") || siteConfig.get("SITE_URL").getValue().contains("test.wjserver430.com") || siteConfig.get("SITE_URL").getValue().contains("localhost")) {
				if (productSearch.getSort().isEmpty() && thisCategory.getDisplayMode().equals("quick2")) {
					productSearch.setSort("created DESC , sku ASC ");
				}
			}

			// get Product List
			List<Product> productList = new ArrayList<Product>();
			if((thisCategory.getLayoutId() != 7) && siteConfig.get("SHOW_LUCENE_SEARCH_FILTER").getValue().equals("true") && (((thisCategory.getLeftbarType() != null && thisCategory.getLeftbarType().equalsIgnoreCase("6"))  || siteConfig.get("LEFTBAR_DEFAULT_TYPE").getValue().equalsIgnoreCase("6")) && lCategoryModel != null && !lCategoryModel.isEmpty())){
				productList = (List<Product>) lCategoryModel.get("results");
				myModel.put("categorySearchFilter", true);
				myModel.put("cid", thisCategory.getId());
			} else {
				productList = this.webJaguar.getProductsByCategoryId(thisCategory, searchProductField, request, productSearch, false);
				int pageCount = count / productSearch.getPageSize();
				if (count % productSearch.getPageSize() > 0) {
					pageCount++;
				}
				myModel.put("pageCount", pageCount);
				myModel.put("pageEnd", (productSearch.getOffset() + productList.size() > count) ? count : productSearch.getOffset() + productList.size());
				myModel.put("count", count);
				
			}
		    //myModel.put("userData", "userInfo.toString()");

			// Custom Search Product not found
			if (request.getParameter("customSearch") != null && productList.isEmpty()) {
				return new ModelAndView(new RedirectView(siteConfig.get("PRODUCT_SEARCH_NOT_FOUND_URL").getValue()));
			}
			
			if(thisCategory.getLayoutId() == 7 && emptyField){
				productList = this.webJaguar.getProductsByCategoryId(thisCategory, null, request, productSearch, false);
	        }

			myModel.put("sortBy", "Please Select");
			if (thisCategory.getSortBy() != null) {
				if (thisCategory.getSortBy().equals("viewed desc") || productSearch.getSort().equalsIgnoreCase("viewed desc")) {
					myModel.put("sortBy", "Most Viewed");
				} else if (thisCategory.getSortBy().contains("best_seller")) {
					myModel.put("sortBy", "Best Seller");
				} else if (thisCategory.getSortBy().equals("created desc") || productSearch.getSort().equalsIgnoreCase("created desc")) {
					myModel.put("sortBy", "New Arrival");
				}
			}
			
			// special pricing
			Map<String, SpecialPricing> productSpecialPrice = null;
			if ((Boolean) gSiteConfig.get("gSPECIAL_PRICING") && customer != null) {
				productSpecialPrice = this.webJaguar.getSpecialPricingByCustomerIDPerPage(customer.getId(), productList);
			}
			if (!siteConfig.get("PRODUCT_FIELDS_SEARCH_TYPE").getValue().equals("0") && thisCategory.isProductFieldSearch()) {
				myModel.put("dropDownList", dropDownList);
			}

			if ((siteConfig.get("SITE_URL").getValue().contains("viatrading") || siteConfig.get("SITE_URL").getValue().contains("test.wjserver180.com"))
					&& (thisCategory.getDisplayMode() != null && thisCategory.getDisplayMode().equals("quick2"))) {
				productFields = this.webJaguar.getProductFields(request, productList, false);
			} else {
				productFields = this.webJaguar.getProductFields(request, productList, true);
			}

			if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
				if (customer != null && customer.getPriceTable() == null) {
					// by default all US customer see PriceTable1
					customer.setPriceTable(1);
				}
			}

			boolean showPriceColumn = false;
			boolean showQtyColumn = false;
			boolean setEtilize = false;
			boolean setProductQuoteColumn = false;
			Map<Integer, Product> alsoConsiderMap = null;
			 List<String> invoiceSku = new ArrayList<String>();

			for (Product product : productList) {
				if ((Boolean) gSiteConfig.get("gSALES_PROMOTIONS")) {
					if (product.getSalesTag() != null) {
						File promoSalesTagFile = new File(baseImageFile, "PromoSalesTag/salestag_" + product.getSalesTag().getTagId() + ".gif");
						product.getSalesTag().setImage(promoSalesTagFile.exists());
					}
				} else {
					product.setSalesTag(null);
				}
				product.setProductOptions(this.webJaguar.getProductOptionsByOptionCode(product.getOptionCode(), siteConfig.get("PRODUCT_MULTI_OPTION").getValue().equals("true"),
						product.getSalesTag(), protectedAccess));
				if ((Boolean) gSiteConfig.get("gBOX")) {
					for (ProductOption op : product.getProductOptions()) {
						if (op.getType().equals("box") && product.getBoxSize() != null) {
							product.setType("box");
							product.setProductOptions(null);
							break;
						}
					}
				}
				// ProductQuoteColumn
				if (siteConfig.get("PRODUCT_QUOTE").getValue().equals("true")) {
					if (product.isQuote()) {
						setProductQuoteColumn = true;
					}
				}

				// hide price
				if (product.isHidePrice() && !customerSeeHiddendPrice) {
					product.setPrice(null);
				}
				// hide msrp
				if (product.isHideMsrp()) {
					product.setMsrp(null);
				}
				// special pricing
				if (productSpecialPrice != null && !productSpecialPrice.isEmpty() && product.getSku() != null) {
					SpecialPricing sp = productSpecialPrice.get(product.getSku().toLowerCase());
					if (sp != null && sp.getPrice() != null) {
						List<Price> priceList = new ArrayList<Price>();
						priceList.add(new Price(sp.getPrice(), null, null, null, null, null));
						product.setPrice(priceList);
						product.setSalesTag(null);
					}
				}
				// also consider
				if ((Boolean) gSiteConfig.get("gALSO_CONSIDER") && siteConfig.get("ALSO_CONSIDER_DETAILS_ONLY").getValue().equals("false")) {
					Product alsoConsider = this.webJaguar.getAlsoConsiderBySku(product.getAlsoConsider(), request);
					if (alsoConsider != null) {
						if (alsoConsiderMap == null) {
							alsoConsiderMap = new HashMap<Integer, Product>();
						}
						alsoConsiderMap.put(product.getId(), alsoConsider);
					}
				}
				// case content
				if (!(Boolean) gSiteConfig.get("gCASE_CONTENT")) {
					product.setCaseContent(null);
				}
				// custom lines
				if (!(Boolean) gSiteConfig.get("gCUSTOM_LINES")) {
					product.setNumCustomLines(null);
				}
				// product Review
				if (!(Boolean) gSiteConfig.get("gPRODUCT_REVIEW") && !siteConfig.get("PRODUCT_RATE").getValue().equals("true")) {
					product.setRate(null);
				}

				if (!showPriceColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) || product.getMsrp() != null)) {
					showPriceColumn = true;
				}
				if (!showQtyColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) && (!product.isLoginRequire() || (product.isLoginRequire() && userSession != null)))) {
					showQtyColumn = true;
				}
				// i18n is on WebImpl (getProductsByCategoryId)
				// master sku
				if ((Boolean) gSiteConfig.get("gMASTER_SKU")) {
					this.webJaguar.setMasterSkuDetails(product, protectedAccess, (Boolean) gSiteConfig.get("gINVENTORY"));
					if (product.getSlaveCount() != null && !showPriceColumn) {
						showPriceColumn = true;
					}
				}
				// check thumbnail
				if (product.getThumbnail() == null && (product.getFeed() != null || product.getEtilizeId() != null || (product.getUpc() != null && product.getUpc().trim().length() > 0))) {
					// get etilize thumbnail if necessary
					setEtilize = true;
				}
				

				//Only for LoadCentre category- show skus present in invoice

				 if(thisCategory.getId() == 738){
					OrderSearch search = new OrderSearch();
					if(userId != null && !userId.equals("")){
						search.setUserId(userId);
						search.setSku(product.getSku());
						List<Order> orders = this.webJaguar.getOrdersList(search);	
						for (Order order:orders){
							if(!order.getStatus().equals("x")){
								invoiceSku.add(product.getSku());
	                          }
	                        break;		
						}	
					 }
				  }
				 
			}
					
			
			// budget by products
			if ((Boolean) gSiteConfig.get("gBUDGET_PRODUCT") && customer != null) {
				myModel.put("budgetProduct", this.webJaguar.getBudgetProduct(customer.getId(), productList));
			}
			// Etilize
			String appId = siteConfig.get("ETILIZE").getValue();
			if (appId.trim().length() > 0 && setEtilize) {
				setEtilize(appId, productList, request.getParameter("debug") != null);
			}
			myModel.put("invoiceSku", invoiceSku);
			myModel.put("showPriceColumn", showPriceColumn);
			myModel.put("showQtyColumn", showQtyColumn);
			myModel.put("alsoConsiderMap", alsoConsiderMap);

			// QUOTE
			if (siteConfig.get("PRODUCT_QUOTE").getValue().equals("true")) {
				myModel.put("showProductQuoteColumn", setProductQuoteColumn);
			}

			// product fields
			myModel.put("productFields", productFields);
			myModel.put("products", productList);

			// accounts
			/*
			 * My tradeZone PagedListHolder customerList = new PagedListHolder(this.webJaguar.getCustomerList(thisCategory, request)); customerList.setPageSize(10); customerList.setPage(pageCount);
			 * 
			 * myModel.put("accounts", customerList);
			 */
			TreeSet<Integer> productPerPageList = new TreeSet<Integer>();
			if (siteConfig.get("PRODUCT_PAGE_SIZE").getValue().length() > 0) {
				String[] productPageSize = siteConfig.get("PRODUCT_PAGE_SIZE").getValue().split(",");
				for (int i = 0; i < productPageSize.length; i++) {
					productPerPageList.add(Integer.parseInt(productPageSize[i].trim()));
				}
			}
			if (thisCategory.getProductPerPage() != null) {
				productPerPageList.add(thisCategory.getProductPerPage());
			} else {
				productPerPageList.add(new Integer(siteConfig.get("NUMBER_PRODUCT_PER_PAGE").getValue()));
			}
			if((thisCategory.getLayoutId() == 7)){
				productPerPageList = new TreeSet<Integer>();
				productPerPageList.add(25);
				productPerPageList.add(50);
				productPerPageList.add(100);
				productPerPageList.add(200);
				productPerPageList.add(500);
				productPerPageList.add(1000);	
			}
			myModel.put("productPerPageList", productPerPageList);
		} else {
			if (!siteConfig.get("CATEGORY_NOT_FOUND_URL").getValue().isEmpty()) {
				logger.info("NULL categoryId=" + categoryId);
				return new ModelAndView(new RedirectView(siteConfig.get("CATEGORY_NOT_FOUND_URL").getValue()));
			}
			// Left Bar
			String leftBarType = this.webJaguar.getLeftBarType(request, null);
			if (leftBarType.equals("3")) {
				myModel.put("mainCategories", this.webJaguar.getMainsAndSubsCategoryLinks(request));
			} else {
				myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request, leftBarType));
			}
		}
		if ((Boolean) gSiteConfig.get("gCOMPARISON") && siteConfig.get("COMPARISON_ON_CATEGORY").getValue().equals("true")) {
			List<Integer> comparisonList = (List) request.getSession().getAttribute("comparisonList");
			Map<Integer, Object> comparisonMap;
			if (comparisonList != null) {
				comparisonMap = new HashMap<Integer, Object>();
				for (Integer id : comparisonList) {
					comparisonMap.put(id, true);
				}
				myModel.put("comparisonMap", comparisonMap);
			}

			myModel.put("maxComparisons", new Integer(((Configuration) siteConfig.get("COMPARISON_MAX")).getValue()));
		}
				
		// check if protected feature is enabled
		protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		
		
		url = request.getContextPath() + request.getServletPath();
		String qry = request.getQueryString();
		if(qry != null) {
			url = url + "?" + qry;
		}
		// continue shopping
		request.getSession().setAttribute("continueShoppingUrl", url);
        // required to set to request directly as frontend jsps are disabled to access session
		// sometimes jsp creates multiple jsessionid
		request.setAttribute("continueShoppingUrl", url);
		// TO BE DELETED
		//search.setConjunction(siteConfig.get("SEARCH_DEFAULT_KEYWORDS_CONJUNCTION").getValue());

		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		List<Product> results = new ArrayList<Product>();

		boolean showPriceColumn = false;
		boolean showQtyColumn = false;
		boolean minCharsMet = false;
		

		// default proximity set to 50. If it is greater than 100, set it back to 50
		Integer proximity = Integer.parseInt(siteConfig.get("LUCENE_PROXIMITY_VALUE").getValue());
		if (ServletRequestUtils.getIntParameter(request, "pr") != null) {
			proximity = ServletRequestUtils.getIntParameter(request, "pr", proximity) <= 100 ? ServletRequestUtils.getIntParameter(request, "pr", proximity) : proximity;
		} else if (request.getSession().getAttribute("proximity") != null) {
			proximity = (Integer) request.getSession().getAttribute("proximity");
		}
		request.getSession().setAttribute("proximity", proximity);
		// required to set to request directly as frontend jsps are disabled to access session
		// sometimes jsp creates multiple jsessionid
		request.setAttribute("proximity", proximity);
	
        // Either keywords are required or must have noKeywords=true
		myModel.put("showPriceColumn", showPriceColumn);
		myModel.put("showQtyColumn", showQtyColumn);
		
		if (siteConfig.get("SEARCH_DISPLAY_MODE").getValue().equals("quick2")) {
			myModel.put("productFields", this.webJaguar.getProductFields(request, results, false));
		} else {
			myModel.put("productFields", this.webJaguar.getProductFields(request, results, true));
		}
		
		// bread crumbs
		if (request.getParameter("cid") != null && !request.getParameter("cid").isEmpty()) {
			List<Category> breadCrumb = this.webJaguar.getCategoryTree(ServletRequestUtils.getIntParameter(request, "cid"), true, request);
			myModel.put("breadCrumbs", breadCrumb);
			myModel.put("cid", ServletRequestUtils.getIntParameter(request, "cid"));

			protectedAccess = "0";
			// check if protected feature is enabled

			if (protectedLevels == 0) {
				protectedAccess = Constants.FULL_PROTECTED_ACCESS;
			} else {
				if (customer != null)  {
					protectedAccess = customer.getProtectedAccess();
				} else {
					protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
				}
			}
			categoryId = ServletRequestUtils.getIntParameter(request, "cid", -1);
			thisCategory = this.webJaguar.getCategoryById(categoryId, protectedAccess);
			
			Layout productSearchLayout = this.webJaguar.getSystemLayout("productSearch", request.getHeader("host"), request);

			if (thisCategory!=null && thisCategory.getHeadTag() != null && !thisCategory.getHeadTag().equals("")) {
				thisCategory.setHeadTag(thisCategory.getHeadTag().replace("#name#", thisCategory.getName()));
				thisCategory.setHeadTag(thisCategory.getHeadTag().replace("#field1#", (thisCategory.getField1() == null) ? "" : thisCategory.getField1()));
				thisCategory.setHeadTag(thisCategory.getHeadTag().replace("#field2#", (thisCategory.getField2() == null) ? "" : thisCategory.getField2()));
				thisCategory.setHeadTag(thisCategory.getHeadTag().replace("#field3#", (thisCategory.getField3() == null) ? "" : thisCategory.getField3()));
				thisCategory.setHeadTag(thisCategory.getHeadTag().replace("#field4#", (thisCategory.getField4() == null) ? "" : thisCategory.getField4()));
				thisCategory.setHeadTag(thisCategory.getHeadTag().replace("#field5#", (thisCategory.getField5() == null) ? "" : thisCategory.getField5()));
				productSearchLayout.setHeadTag(thisCategory.getHeadTag());
			}
			
			if(thisCategory!=null)
			productSearchLayout.setHeaderHtml(thisCategory.getHtmlCode());
			// get protected html
			if (protectedLevels > 0) {
				if (thisCategory!=null && !thisCategory.getProtectedLevel().equals("0") && thisCategory.isProtect()) {
					if (thisCategory!=null && thisCategory.getProtectedHtmlCode() != null && thisCategory.getProtectedHtmlCode().trim().length() > 0) {
						productSearchLayout.setHeaderHtml(thisCategory.getProtectedHtmlCode());
					}
				}
			}
			
			if(thisCategory!=null)
			productSearchLayout.setFooterHtml(thisCategory.getFooterHtmlCode());
			myModel.put("productSearchLayout", productSearchLayout);
			
		} else {
			Layout productSearchLayout = this.webJaguar.getSystemLayout("productSearch", request.getHeader("host"), request);
			productSearchLayout.setHideLeftBar((siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) ? true : false);
			myModel.put("productSearchLayout", productSearchLayout);
		}
		
		myModel.put("results", results);
		myModel.put("hideSearchBox", ServletRequestUtils.getBooleanParameter(request, "hideSearchBox", false));
		myModel.put("lucene2", true);
		myModel.put("luceneSearch", false);
		
		//Add to COmparioson for Gajoodle layout
		if ((Boolean) gSiteConfig.get("gCOMPARISON")) {
			List<Integer> comparisonList = (List) request.getSession().getAttribute("comparisonList");
			Map<Integer, Object> comparisonMap;
			if (comparisonList != null) {
				comparisonMap = new HashMap<Integer, Object>();
				for (Integer id : comparisonList) {
					comparisonMap.put(id, true);
				}
				myModel.put("comparisonMap", comparisonMap);
			}

			myModel.put("maxComparisons", new Integer(((Configuration) siteConfig.get("COMPARISON_MAX")).getValue()));
		}
		
		// Show add presentation only when salesrep cookie is there.
		if ((Boolean) gSiteConfig.get("gPRESENTATION")) {
			myModel.put("showAddPresentation", (request.getAttribute("cookieSalesRep") != null && customer == null) ? true : false);
		}
		Boolean modifiedDirectory = false;
		if (siteConfig.get("CATEGORY1_DIRECTORY").getValue().length() > 0) {
			modifiedDirectory = true;
			myModel.put("modifiedDirectory", modifiedDirectory);
			myModel.put("directory", siteConfig.get("CATEGORY1_DIRECTORY").getValue());
			return new ModelAndView(siteConfig.get("CATEGORY1_DIRECTORY").getValue() + "/category", "model", myModel);
		} else if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/category/category", "model", myModel);
		} else {
			return new ModelAndView("frontend/category", "model", myModel);
		}
	}
	

	private void getSubCategoryLinks(Category category, HttpServletRequest request, int depth, int maxDepth) {
		List<Category> subCategories = this.webJaguar.getCategoryLinks(category.getId(), request);
		category.setSubCategories(subCategories);
		Iterator<Category> iter = subCategories.iterator();
		while (iter.hasNext()) {
			Category subCategory = iter.next();
			if (depth < maxDepth) {
				getSubCategoryLinks(subCategory, request, depth + 1, maxDepth);
			}
		}
	}

	private boolean startWith(List<String> fieldsSearch, int num) {
		Iterator<String> iter = fieldsSearch.iterator();
		while (iter.hasNext()) {
			String field = iter.next();
			if (field.startsWith("field_" + num))
				return true;
		}

		return false;
	}

	private void setLayout(Category category, HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, Integer count, String mobile, boolean homePage, Cart cart) {
		Layout layout = this.webJaguar.getLayoutByCategoryId(category.getId(), request, response);
		if(homePage){
			if(layout!=null && layout.getTopBarHtml()!=null && i18nCategory!=null){
				layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", i18nCategory.getI18nName()));
			}
		}else{
			if(lang!=null && lang.equalsIgnoreCase("es") && i18nCategory!=null){
				layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", i18nCategory.getI18nName()));
			}else{
				layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", category.getName()));
			}
		}

		Integer qnt = new Integer(0);
		if(cart!=null){
			for(CartItem cartItem : cart.getCartItems()) {
				Product p = null;
				if(cartItem!=null && cartItem.getProduct()!=null){
					p = this.webJaguar.getProductById(cartItem.getProduct().getId(), 0, false, null);
					if(p!=null && p.isActive()){
						qnt = qnt + cartItem.getQuantity();
					}
				}
			}
		}
		
		if(layout!=null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#cartItemQuantity#", qnt.toString()));
		}
		request.setAttribute("layout", layout);

		if (category.isHideHeader())
			layout.setHeaderHtml(null);
		if (category.isHideTopBar()) {
			layout.setTopBarHtml(null);
		} else {
			layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#cid#", category.getId().toString()));
		}
		
		//ShowFullWidth
		layout.setShowFullWidth(category.isShowFullWidth());
		
		if (category.isHideLeftBar()) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} else {
			request.setAttribute("categoryId", category.getId());
			// Left Bar
			String leftBarType = this.webJaguar.getLeftBarType(request, category.getLeftbarType());
			// In case of filters on left bar, as only layout2 has filters set 'leftBarType' as '2'
			if(category.getProductFieldSearchPosition() != null && category.getProductFieldSearchType() != null && (category.getProductFieldSearchType().equals("2") || category.getProductFieldSearchType().equals("0")) && category.getProductFieldSearchPosition().equalsIgnoreCase("left") && category.isProductFieldSearch()) {
				leftBarType = "2";
			}
			request.setAttribute("_leftBar", leftBarType);
			
			if (leftBarType.equals("3")) {
				model.put("mainCategories", this.webJaguar.getMainsAndSubsCategoryLinks(request));
			} else {
				model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request, leftBarType));
			}
			layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#parentId#", (category.getParent() != null) ? category.getParent().toString() : "-1"));
			layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#cid#", category.getId().toString()));
			
		}
		if (category.isHideRightBar()) {
			layout.setRightBarTopHtml("");
			layout.setRightBarBottomHtml("");
		} else {
			if (category.getRightBarTopHtmlCode() != null && category.getRightBarTopHtmlCode().trim().length() > 0) {
				layout.setRightBarTopHtml(category.getRightBarTopHtmlCode());
			}
			if (category.getRightBarBottomHtmlCode() != null && category.getRightBarBottomHtmlCode().trim().length() > 0) {
				layout.setRightBarBottomHtml(category.getRightBarBottomHtmlCode());
			}
		}

		if (category.isHideLeftBar()) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
		} else {
			if (category.getLeftBarTopHtmlCode() != null && category.getLeftBarTopHtmlCode().trim().length() > 0) {
				layout.setLeftBarTopHtml(category.getLeftBarTopHtmlCode());
			}
			if (category.getLeftBarBottomHtmlCode() != null && category.getLeftBarBottomHtmlCode().trim().length() > 0) {
				layout.setLeftBarBottomHtml(category.getLeftBarBottomHtmlCode());
			}
		}
		if (category.isHideFooter())
			layout.setFooterHtml(null);

		// bread crumbs
		List<Category> breadCrumb = null;
		if (!category.isHideBreadCrumbs()) {
			breadCrumb = this.webJaguar.getCategoryTree(category.getId(), true, request);
			model.put("breadCrumbs", breadCrumb);
		}

		// headTag
		Layout categoryLayout = this.webJaguar.getSystemLayout("category", request.getHeader("host"), request);
		if (categoryLayout.getHeaderHtml() != null && !categoryLayout.getHeaderHtml().trim().equals("")) {
			categoryLayout.setHeaderHtml(categoryLayout.getHeaderHtml().replace("#name#", category.getName().toString()));
		}
		model.put("categoryLayout", categoryLayout);
		if (category.getHeadTag() != null && !category.getHeadTag().trim().equals("")) {
			layout.setHeadTag(category.getHeadTag());
			if(mobile.equalsIgnoreCase("_mobile")){
				layout.setHeadTagMobile(category.getHeadTag());
			}
		} else if (categoryLayout.getHeadTag() != null && !categoryLayout.getHeadTag().equals("")) {
			categoryLayout.setHeadTag(categoryLayout.getHeadTag().replace("#name#", category.getName()));
			categoryLayout.setHeadTag(categoryLayout.getHeadTag().replace("#breadcrumb#", (breadCrumb == null) ? "" : Constants.showBreadCrumb(breadCrumb)));
			categoryLayout.setHeadTag(categoryLayout.getHeadTag().replace("#field1#", (category.getField1() == null) ? "" : category.getField1()));
			categoryLayout.setHeadTag(categoryLayout.getHeadTag().replace("#field2#", (category.getField2() == null) ? "" : category.getField2()));
			categoryLayout.setHeadTag(categoryLayout.getHeadTag().replace("#field3#", (category.getField3() == null) ? "" : category.getField3()));
			categoryLayout.setHeadTag(categoryLayout.getHeadTag().replace("#field4#", (category.getField4() == null) ? "" : category.getField4()));
			categoryLayout.setHeadTag(categoryLayout.getHeadTag().replace("#field5#", (category.getField5() == null) ? "" : category.getField5()));
			layout.setHeadTag(categoryLayout.getHeadTag());
		}
		// on Mobile layout we dont want to see content if there is no product associated to the category
//		if (mobile.equalsIgnoreCase("_mobile")) {
//			if (count < 1) {
//				layout.setHideContent(true);
//				layout.setHideLeftBar(false);
//			} else {
//				layout.setHideContent(false);
//				layout.setHideLeftBar(true);
//			}
//		}		
	}

	private void setEtilize(String appId, List<Product> productList, boolean debug) {
		try {
			GetProduct getProduct = new GetProduct();
			getProduct.setCatalog("na");
			getProduct.setSiteId(0);

			String wsdlLocation = "http://ws.spexlive.net/service/soap/catalog?appId=" + appId + "&wsdl";
			Catalog service = new Catalog(new URL(wsdlLocation), new QName("http://com.etilize.spexlive", "catalog"));
			CatalogServiceIntf port = service.getCatalogHttpPort();

			// SelectProductFields
			SelectProductFields selectProductFields = new SelectProductFields();
			selectProductFields.getResourceType().add("Thumbnail");
			getProduct.setSelectProductFields(selectProductFields);

			// subcatalog
			SubcatalogFilter subcatalogFilter = new SubcatalogFilter();
			subcatalogFilter.setContent("allitems");
			getProduct.setSubcatalogFilter(subcatalogFilter);

			for (Product product : productList) {
				if (product.getThumbnail() == null) {
					// search by sku
					Sku sku = new Sku();
					sku.setNumber(product.getSku());
					if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("SYNNEX")) {
						sku.setType("Synnex");
						if (product.getSku().startsWith("SYN-")) {
							sku.setNumber(product.getSku().substring(4)); // remove leading SYN-
						}
					} else if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("INGRAM")) {
						sku.setType("Ingram Micro USA");
						if (product.getSku().startsWith("IM-")) {
							sku.setNumber(product.getSku().substring(3)); // remove leading IM-
						}
					} else if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("TECHDATA")) {
						sku.setType("Tech Data");
						if (product.getSku().startsWith("TD-")) {
							sku.setNumber(product.getSku().substring(3)); // remove leading TD-
						}
					} else if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("DSI")) {
						sku.setType("DSI");
						if (product.getSku().startsWith("DSI-")) {
							sku.setNumber(product.getSku().substring(4)); // remove leading DSI-
						}
					} else if (product.getEtilizeId() != null) {
						ProductId productId = new ProductId();
						productId.setId(product.getEtilizeId());
						getProduct.setProductId(productId);
					} else if (product.getUpc() != null && product.getUpc().trim().length() > 0) {
						sku.setType("UPC");
						sku.setNumber(product.getUpc()); // use UPC
					}
					if (sku.getType() != null) {
						getProduct.setSku(sku);
					} else if (getProduct.getProductId() != null) {
						// use etilize id
					} else {
						return;
					}

					try {
						spexlive.etilize.com.Product eProduct = port.getProduct(getProduct);

						// thumbnail
						if (eProduct.getResources() != null && !eProduct.getResources().getResource().isEmpty()) {
							ProductImage thumbnail = new ProductImage();
							thumbnail.setImageUrl(eProduct.getResources().getResource().get(0).getUrl());
							product.setThumbnail(thumbnail);
						}
					} catch (SOAPFaultException e) {
						// item not found, jump to next item
						if (debug) {
							//System.out.println(e.toString());
						}
					}
				}
			}
		} catch (Exception e) {
			if (debug) {
				//System.out.println(e.toString());
			}
		}
	}

	private ProductSearch getProductSearch(HttpServletRequest request, Category category, Map<String, Object> gSiteConfig) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		ProductSearch productSearch = (ProductSearch) request.getSession().getAttribute("frontEndProductSearch" + category.getId());
		if (productSearch == null) {
			productSearch = new ProductSearch();
			productSearch.setSort((category.getSortBy() == null || category.getSortBy().isEmpty()) ? siteConfig.get("PRODUCT_SORTING").getValue() : category.getSortBy());
			productSearch.setPageSize((category.getProductPerPage() == null) ? new Integer(siteConfig.get("NUMBER_PRODUCT_PER_PAGE").getValue()) : category.getProductPerPage());
			request.getSession().setAttribute("frontEndProductSearch" + category.getId(), productSearch);

		}
		Integer i = null;
		try {
			i = Integer.parseInt(productSearch.getSort());
		} catch (Exception e) {
		}

		if (i != null && i > 200) {
			productSearch.setSort("DESC");
		} else {
			if (i != null && i > 100)
				productSearch.setSort("ASC");
		}
		request.setAttribute("frontEndProductSearch", productSearch);
		// sorting
		if (request.getParameter("sort") != null && request.getParameter("sort") != "") {
			int sortValue = ServletRequestUtils.getIntParameter(request, "sort", 0);
			switch (sortValue) {
			case 0:
				productSearch.setSort("");
				break;
			case 10:
				if(request.getParameter("sorttype") != null && request.getParameter("sorttype").equalsIgnoreCase("desc")){
					productSearch.setSort("name DESC");
				}else{
					productSearch.setSort("name ASC");
				}
				break;
			case 20:
				productSearch.setSort("price_1 ASC");
				break;
			case 30:
				productSearch.setSort("price_1 DESC");
				break;
			case 40:
				productSearch.setSort("rate DESC");
				break;
			case 50:
				if(request.getParameter("sorttype") != null && request.getParameter("sorttype").equalsIgnoreCase("desc")){
					productSearch.setSort("sku" + " desc");
				}else{
					productSearch.setSort("sku" + " asc");
				}
				break;
			case 60:
				productSearch.setSort("sku ASC");
				break;
			case 70:
				productSearch.setSort("name DESC");
				break;
			case 80:
				productSearch.setSort("name ASC");
				break;
			case 112:
				if(request.getParameter("sorttype") != null && request.getParameter("sorttype").equalsIgnoreCase("desc")){
					productSearch.setSort("price_1 DESC");
				}else{
					productSearch.setSort("price_1 ASC");
				}
				break;
			}
			if (sortValue > 200) {
				productSearch.setSort("field_" + (sortValue % 200) + " DESC");
			}
			if (sortValue > 100 && sortValue < 200) {
				productSearch.setSort("field_" + (sortValue % 100) + " ASC");
			}
			else if(sortValue!=50 && sortValue!=10 && sortValue!=112){
				if(request.getParameter("sorttype") != null && request.getParameter("sorttype").equalsIgnoreCase("desc")){
					productSearch.setSort("field_" + (sortValue % 100) + " DESC");
				}else{
					productSearch.setSort("field_" + (sortValue % 100) + " ASC");
				}
			}
		}

		if (request.getParameter("fsort") != null && request.getParameter("fsort") != "") {
			int sortValue = ServletRequestUtils.getIntParameter(request, "fsort", 0);
			switch (sortValue) {
			case 0:
				productSearch.setSort("");
				break;
			case 10:
				if(request.getParameter("sorttype") != null && request.getParameter("sorttype").equalsIgnoreCase("desc")){
					productSearch.setSort("name DESC");
				}else{
					productSearch.setSort("name ASC");
				}
				break;
			case 20:
				productSearch.setSort("price_1 ASC");
				break;
			case 30:
				productSearch.setSort("price_1 DESC");
				break;
			case 40:
				productSearch.setSort("rate DESC");
				break;
			case 50:
				if(request.getParameter("sorttype") != null && request.getParameter("sorttype").equalsIgnoreCase("desc")){
					productSearch.setSort("sku" + " desc");
				}else{
					productSearch.setSort("sku" + " asc");
					
				}
				break;
			case 60:
				productSearch.setSort("sku ASC");
				break;
			case 70:
				productSearch.setSort("name DESC");
				break;
			case 80:
				productSearch.setSort("name ASC");
				break;
			case 112:
				if(request.getParameter("sorttype") != null && request.getParameter("sorttype").equalsIgnoreCase("desc")){
					productSearch.setSort("price_1 DESC");
				}else{
					productSearch.setSort("price_1 ASC");
				}
				break;
			}
		}
			
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				productSearch.setPage(1);
			} else {
				productSearch.setPage(page);
			}
		}

		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			productSearch.setPageSize(size);
		}

		return productSearch;
	}
	
	private String getSlidShowCode(HttpServletRequest  request,  Integer categoryId) throws UnsupportedEncodingException {
		StringBuffer sb = new StringBuffer();
		Category thisCategory = this.webJaguar.getCategoryById(categoryId, "");
		List<Product> products = null;
		if (thisCategory != null) {
			products = this.webJaguar.getProductsByCategoryId( thisCategory, null, request, new ProductSearch(), false );
		}
		if(products == null) {
			return sb.toString();
		}
		
		Iterator<Product> itr = products.iterator();
		while(itr.hasNext()) {
			Product product = itr.next();
			if (product.getThumbnail() == null) {
				itr.remove();
			}
		}
		if(products.isEmpty()) {
			return sb.toString();
		}
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		ApplicationContext context = getApplicationContext();
		String currencySymbol = context.getMessage(siteConfig.get("CURRENCY").getValue(), new Object[0], null);
		sb.append("<div class=\"products_carousel owl-carousel\">");
		
		for(Product product : products) {
				
			if(product.getProductLayout().equalsIgnoreCase("047")){
				String productLink = null;		

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}


				sb.append("<div class=\"product_wrapper\">");		
				
				
				String saleTag = "";
				if(product.getSalesTag() != null){
					saleTag = product.getSalesTag().getTitle();
				}
				
				if(product.getField32()!=null && !product.getField32().equalsIgnoreCase("")){
					sb.append("<div class=\"product_bordered_box is_ribboned\">");
					sb.append("<div class=\"ribbon_wrapper\">");
				if(product.getField32().equalsIgnoreCase("New_Item")){
					sb.append("<div class=\"ribbon ribbon_6\">NEW ARRIVAL</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Out_of_Stock")){
					sb.append("<div class=\"ribbon ribbon_4\">OUT OF STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Back_in_Stock")){
					sb.append("<div class=\"ribbon ribbon_3\">BACK IN STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Load_Added")){
					sb.append("<div class=\"ribbon ribbon_5\">NEW LOAD ADDED</div>");
				}
				else{
					sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				}
				sb.append("</div>");
				}else{
					sb.append("<div class=\"product_bordered_box\">");
				}

				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}

				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
		
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
				
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price\">");
				sb.append(product.getField8());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"total_price\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getPrice1() != null) {
					sb.append(currencySymbol);
					sb.append(numFormat.format(product.getPrice1()));
				}
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}
			
			if(product.getProductLayout().equalsIgnoreCase("048")){
				String productLink = null;
				
				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");
				
				if(product.getField32()!=null && !product.getField32().equalsIgnoreCase("")){
					sb.append("<div class=\"product_bordered_box is_ribboned\">");
					sb.append("<div class=\"ribbon_wrapper\">");
				if(product.getField32().equalsIgnoreCase("New_Item")){
					sb.append("<div class=\"ribbon ribbon_6\">NEW ARRIVAL</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Out_of_Stock")){
					sb.append("<div class=\"ribbon ribbon_4\">OUT OF STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Back_in_Stock")){
					sb.append("<div class=\"ribbon ribbon_3\">BACK IN STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Load_Added")){
					sb.append("<div class=\"ribbon ribbon_5\">NEW LOAD ADDED</div>");
				}
				else{
					sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				}
				sb.append("</div>");
				}else{
					sb.append("<div class=\"product_bordered_box\">");
				}
			
				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
				
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price\">");
				sb.append(product.getField8());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"total_price\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(currencySymbol);
				sb.append((product.getPriceRangeMinimum() != null ? numFormat.format(product.getPriceRangeMinimum()):0.0));
				sb.append("-");	
				sb.append(currencySymbol);
				sb.append((product.getPriceRangeMaximum()!=null?numFormat.format(product.getPriceRangeMaximum()):0.0));
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}

			if(product.getProductLayout().equalsIgnoreCase("049")){
				String productLink = null;
				

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");
				
				if(product.getField32()!=null && !product.getField32().equalsIgnoreCase("")){
					sb.append("<div class=\"product_bordered_box is_ribboned\">");
					sb.append("<div class=\"ribbon_wrapper\">");
				if(product.getField32().equalsIgnoreCase("New_Item")){
					sb.append("<div class=\"ribbon ribbon_6\">NEW ARRIVAL</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Out_of_Stock")){
					sb.append("<div class=\"ribbon ribbon_4\">OUT OF STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Back_in_Stock")){
					sb.append("<div class=\"ribbon ribbon_3\">BACK IN STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Load_Added")){
					sb.append("<div class=\"ribbon ribbon_5\">NEW LOAD ADDED</div>");
				}
				else{
					sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				}
				sb.append("</div>");
				}else{
					sb.append("<div class=\"product_bordered_box\">");
				}
				
				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");			
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");	
				
				sb.append("<div class=\"product_manifest_status\">");
				sb.append(product.getField14()!=null?product.getField14().replace(",", ""):"");
				sb.append("</div>");
				sb.append("<div class=\"price_percentage\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getField11());
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}

			if(product.getProductLayout().equalsIgnoreCase("050")){
				String productLink = null;
				

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");		
				
				String saleTag = "";
				if(product.getSalesTag() != null){
					saleTag = product.getSalesTag().getTitle();
				}
				
				sb.append("<div class=\"product_bordered_box is_ribboned\">");
				sb.append("<div class=\"ribbon_wrapper\">");
				sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				sb.append("</div>");

				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}

				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
								
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
		
				Double discount = 0.0;
				if(product.getSalesTag() != null && product.getSalesTag().getDiscount() != 0.0){
					if(product.getSalesTag().isPercent()) {
						discount = product.getPrice1() * (100 - product.getSalesTag().getDiscount()) / 100;
					} else {
						discount = product.getPrice1() - product.getSalesTag().getDiscount();
					}
				}else{
					discount = product.getPrice1();
				}
				
				discount = Utilities.roundFactory(discount,2,BigDecimal.ROUND_HALF_UP);
	
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price discount\">");
				sb.append("<span class=\"new_price\">");sb.append(product.getField8());sb.append("</span>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"total_price discount\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append("<span class=\"strikethrough\">");
				sb.append("<span class=\"old_price\">");				
				if(product.getPrice1() != null) {
					sb.append(currencySymbol);
					sb.append(numFormat.format(product.getPrice1()));
				}
				sb.append("</span>");
				sb.append("</span>");
				sb.append("<span class=\"new_price\">");

				if(discount != null) {
					sb.append(currencySymbol);
					sb.append(numFormat.format(discount));
				}
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</span>");
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}

			if(product.getProductLayout().equalsIgnoreCase("051")){
				String productLink = null;
				

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");				

				sb.append("<div class=\"product_bordered_box is_ribboned\">");
				sb.append("<div class=\"ribbon_wrapper\">");
				sb.append("<div class=\"ribbon ribbon_2\">ONE TIME DEAL</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}

				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");	
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
			
				sb.append("<div class=\"clearfix\">");
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("<div class=\"pull-right\">");
				sb.append("	<div class=\"product_manifest_status\">");
				sb.append(product.getField14()!=null?product.getField14().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price\">");
				sb.append(product.getField8());
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("<div class=\"total_price\">");
				sb.append("<a href=\""+productLink+"\">");
				
				if(product.getPrice1() != null) {
					sb.append(currencySymbol);
					sb.append(numFormat.format(product.getPrice1()));
				}
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}		
			
			
			if(!product.getProductLayout().equalsIgnoreCase("047") && !product.getProductLayout().equalsIgnoreCase("048") && !product.getProductLayout().equalsIgnoreCase("049") && !product.getProductLayout().equalsIgnoreCase("050") && !product.getProductLayout().equalsIgnoreCase("051")){
				String productLink = null;
				

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}
				

				sb.append("<div class=\"product_wrapper\">");
				
				if(product.getField32()!=null && !product.getField32().equalsIgnoreCase("")){
					sb.append("<div class=\"product_bordered_box is_ribboned\">");
					sb.append("<div class=\"ribbon_wrapper\">");
				if(product.getField32().equalsIgnoreCase("New_Item")){
					sb.append("<div class=\"ribbon ribbon_6\">NEW ARRIVAL</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Out_of_Stock")){
					sb.append("<div class=\"ribbon ribbon_4\">OUT OF STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Back_in_Stock")){
					sb.append("<div class=\"ribbon ribbon_3\">BACK IN STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Load_Added")){
					sb.append("<div class=\"ribbon ribbon_5\">NEW LOAD ADDED</div>");
				}
				else{
					sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				}
				sb.append("</div>");
				}else{
					sb.append("<div class=\"product_bordered_box\">");
				}
				
				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}
				
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
				
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price\">");
				sb.append(product.getField8());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"total_price\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getPrice1() != null) {
					sb.append(currencySymbol);
					sb.append(numFormat.format(product.getPrice1()));
				}
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");
					sb.append(product.getField6());
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");		
			}
			
		}
		sb.append("</div>");
		return sb.toString();
	}
	
}