/* Copyright 2010 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 04.07.2010
 */

package com.webjaguar.web.frontend.crm;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UserSession;
import com.webjaguar.model.crm.CrmAccount;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactField;
import com.webjaguar.model.crm.CrmContactGroup;
import com.webjaguar.model.crm.CrmForm;
import com.webjaguar.model.crm.CrmFormField;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.Encoder;
import com.webjaguar.web.form.crm.CrmFormProcessorForm;

public class FormProcessorController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar; 
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	private Properties site;
	public void setSite(Properties site) { this.site = site; }
	
	private File baseFile;
	public FormProcessorController() {
		setSessionForm(true);
		setCommandName("crmFormProcessorForm");
		setCommandClass(CrmFormProcessorForm.class);
		setFormView("frontend/crm/formValidation");
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		String oldCountry = null;
		
    	CrmFormProcessorForm crmFormProcessorForm = (CrmFormProcessorForm) command;
    	CrmForm	crmForm = this.webJaguar.getCrmFormByFormNumber(ServletRequestUtils.getStringParameter(request, "formNumber"));
    	
    	if(checkDuplicateEntries(crmFormProcessorForm)){
    		if(ServletRequestUtils.getStringParameter(request, "returnUrl") != null) {
    	    	crmForm.setReturnUrl(ServletRequestUtils.getStringParameter(request, "returnUrl"));
    	    }
    		    		
    		//return new ModelAndView(new RedirectView( crmForm.getReturnUrl(), false));  

    		//return new ModelAndView(new RedirectView(request.getScheme()+"://www.viatrading.com/lp/crmTimeout.html", false));  
    	}
    	
    	
    	request.getSession(false);
    	
    	if(crmForm.isDoubleOptIn()) {
    		crmFormProcessorForm.getCrmContact().setVerified(false);
    	} 
    	CrmAccount crmAccount = this.webJaguar.getCrmAccountById(ServletRequestUtils.getIntParameter(request, "accountId", 1));
    	if(crmAccount != null) {
    		crmFormProcessorForm.getCrmContact().setAccountId(crmAccount.getId());
    	    crmFormProcessorForm.getCrmContact().setAccountName(crmAccount.getAccountName());
    	} else {
    		crmFormProcessorForm.getCrmContact().setAccountId(1);
            crmFormProcessorForm.getCrmContact().setAccountName("Leads");
        }
    	crmFormProcessorForm.getCrmContact().setIpAddress(request.getRemoteAddr());
        if(crmFormProcessorForm.getCrmContact().getCrmContactFields() != null){
        	CrmContactField crmContactField = null;
            for(int i=0; i<crmFormProcessorForm.getCrmContact().getCrmContactFields().size(); i++){
            	crmContactField = crmFormProcessorForm.getCrmContact().getCrmContactFields().get(i);
            	crmContactField.setId(ServletRequestUtils.getIntParameter(request, "field"+i));
            	if(crmContactField.getId()!=null && crmContactField.getId()==30){
            		if(crmContactField.getFieldValue()!=null && crmContactField.getFieldValue().equalsIgnoreCase("Yes")){
                		crmFormProcessorForm.getCrmContact().setUnsubscribe(false);
                	}else{
                		crmFormProcessorForm.getCrmContact().setUnsubscribe(true);
                	}
            	}
            }
        }
        
        // trackcode
        if(crmFormProcessorForm.getCrmContact().getTrackcode() == null || crmFormProcessorForm.getCrmContact().getTrackcode().isEmpty()) {
        	if(request.getAttribute("trackcode") != null) {
        		crmFormProcessorForm.getCrmContact().setTrackcode(request.getAttribute("trackcode").toString());
        	}
        }
        UserSession userSession = this.webJaguar.getUserSession(request);
		
      
        oldCountry = crmFormProcessorForm.getCrmContact().getCountry();
		crmFormProcessorForm.getCrmContact().setCountry(getCountryCodes(crmFormProcessorForm.getCrmContact().getCountry()));
		 
        CrmContact exisitngContact = null;
    	if(ServletRequestUtils.getStringParameter(request, "updateIfExist") != null && ServletRequestUtils.getStringParameter(request, "updateIfExist").equalsIgnoreCase("true")) {
    		Customer customer = null;
    		if (userSession != null) {
    			customer = this.webJaguar.getCustomerById(userSession.getUserid());
    		}
    		if(customer != null && customer.getCrmContactId() != null) {
        		exisitngContact = this.webJaguar.getCrmContactById(customer.getCrmContactId());
            }
        	
        	if(exisitngContact == null) {
        		String contactEmail = crmFormProcessorForm.getCrmContact().getEmail1() != null ? crmFormProcessorForm.getCrmContact().getEmail1() : crmFormProcessorForm.getCrmContact().getEmail2();
        		if(contactEmail != null) {
            		List<CrmContact> exisitngContactList = this.webJaguar.getCrmContactByEmail1(contactEmail);
            		if(exisitngContactList != null && !exisitngContactList.isEmpty()) {
            			exisitngContact = exisitngContactList.get(0);
            		}
        		}
        	}
        } 
        
      	if(exisitngContact != null){
      		crmFormProcessorForm.getCrmContact().setId(exisitngContact.getId());
      		this.webJaguar.updateCrmContact(crmFormProcessorForm.getCrmContact());
        } else {
        	
        	// SearchEngine Prospect
    		if ((Boolean) gSiteConfig.get("gSEARCH_ENGINE_PROSPECT")) {
    			String cookieName = site.getProperty("track.prospect.cookie.name");
    			Cookie prospectCookie = WebUtils.getCookie(request, cookieName);
    			if (prospectCookie != null) {
    				crmFormProcessorForm.getCrmContact().setProspectId(prospectCookie.getValue());
    				logger.info("Prospect Id = " + prospectCookie.getValue());
    				// delete Cookie after order placed.
    				prospectCookie.setMaxAge(0);
    				prospectCookie.setPath("/");
    				prospectCookie.setValue("");
    				response.addCookie(prospectCookie);
    			}
    		}
    		if(crmFormProcessorForm.getCrmContact().getLanguage()==null){
    			crmFormProcessorForm.getCrmContact().setLanguage("");
    		}
    		
    		if(this.webJaguar.getDuplicateEntry(crmFormProcessorForm.getCrmContact().getEmail1()) > 0){
    			crmFormProcessorForm.getCrmContact().setDuplicateContact(1);
    		}else{
    			crmFormProcessorForm.getCrmContact().setDuplicateContact(0);
    		}
    		
      		this.webJaguar.insertCrmContact(crmFormProcessorForm.getCrmContact());
        }
      	
  		crmFormProcessorForm.getCrmContact().setCountry(oldCountry);
        
        //create a folder to save attachment
    	// For example, ROOT Directory/temp/CRM/contact_CONTACTID
    	try {
			baseFile = new File( baseFile, "/contact_"+crmFormProcessorForm.getCrmContact().getId()+"/" );
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
		} catch ( Exception e ){ e.printStackTrace(); }
		
		//save attachment
        boolean uploadedAttachment = false;
		MultipartFile attachment = null;
		if ( baseFile.canWrite() ) {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			attachment = multipartRequest.getFile( "attachment" );
			if ( attachment != null && !attachment.isEmpty() ) {
				uploadedAttachment = true;
			}
		}
		
		File newFile = null;
		if ( uploadedAttachment ) {
			newFile = new File( baseFile, attachment.getOriginalFilename() );
			try {
				attachment.transferTo( newFile );
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
		
		//save attachment
        boolean uploadedAttachment1 = false;
		MultipartFile attachment1 = null;
		if ( baseFile.canWrite() ) {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			attachment1 = multipartRequest.getFile( "attachment1" );
			if ( attachment1 != null && !attachment1.isEmpty() ) {
				uploadedAttachment1 = true;
			}
		}
		
		File newFile1 = null;
		if ( uploadedAttachment1 ) {
			newFile1 = new File( baseFile, attachment1.getOriginalFilename() );
			try {
				attachment1.transferTo( newFile1 );
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
		
		//save attachment
        boolean uploadedAttachment2 = false;
		MultipartFile attachment2 = null;
		if ( baseFile.canWrite() ) {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			attachment2 = multipartRequest.getFile( "attachment2" );
			if ( attachment2 != null && !attachment2.isEmpty() ) {
				uploadedAttachment2 = true;
			}
		}
		
		File newFile2 = null;
		if ( uploadedAttachment2 ) {
			newFile2 = new File( baseFile, attachment2.getOriginalFilename() );
			try {
				attachment2.transferTo( newFile2 );
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
		
		//save attachment
        boolean uploadedAttachment3 = false;
		MultipartFile attachment3 = null;
		if ( baseFile.canWrite() ) {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			attachment3 = multipartRequest.getFile( "attachment3" );
			if ( attachment3 != null && !attachment3.isEmpty() ) {
				uploadedAttachment3 = true;
			}
		}
		
		File newFile3 = null;
		if ( uploadedAttachment3 ) {
			newFile3 = new File( baseFile, attachment3.getOriginalFilename() );
			try {
				attachment3.transferTo( newFile3 );
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
		
		//save attachment
        boolean uploadedAttachment4 = false;
		MultipartFile attachment4 = null;
		if ( baseFile.canWrite() ) {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			attachment4 = multipartRequest.getFile( "attachment4" );
			if ( attachment4 != null && !attachment4.isEmpty() ) {
				uploadedAttachment4 = true;
			}
		}
		
		File newFile4 = null;
		if ( uploadedAttachment4 ) {
			newFile4 = new File( baseFile, attachment4.getOriginalFilename() );
			try {
				attachment4.transferTo( newFile4 );
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
		
      	Set<Integer> groups = null;
        if (ServletRequestUtils.getStringParameter(request, "groupId") != null) {
     		groups = new HashSet<Integer>();
     		String[] groupIds = ServletRequestUtils.getStringParameter(request, "groupId").split( "," );
			for ( int x = 0; x < groupIds.length; x++ ) {
				if ( !("".equals( groupIds[x].trim() )) ) {
					try {
						groups.add( Integer.valueOf( groupIds[x].trim() ) );
					}catch ( Exception e ) {
						// do nothing
					}
				}
			}
			this.webJaguar.batchCrmContactGroupIds( crmFormProcessorForm.getCrmContact().getId(), groups, "add");
		}
		String token = this.webJaguar.createToken(crmFormProcessorForm.getCrmContact().getId());
    	   	
    	if(crmForm.isDoubleOptIn() && crmFormProcessorForm.getCrmContact().getEmail1() != null) {
    		sendEmail(crmFormProcessorForm.getCrmContact(), token, crmFormProcessorForm.getCrmContact().getId(), request);
        } else if( !crmForm.isDoubleOptIn() && crmFormProcessorForm.getCrmContact().getEmail1() != null ) {
        	
        	//Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
        	Iterator<Integer> iter = groups.iterator();
        	List<Integer> messageIds = new ArrayList<Integer>();
        	while(iter.hasNext()){
        		
        		CrmContactGroup crmContactGroup = this.webJaguar.getContactGroupById(iter.next());
        		if( crmContactGroup != null && crmContactGroup.getMessageId() != null && !messageIds.contains(crmContactGroup.getMessageId())) {
        		
        			messageIds.add(crmContactGroup.getMessageId());
        			SiteMessage message = this.webJaguar.getSiteMessageById(crmContactGroup.getMessageId());
        			this.webJaguar.sendAutoResponse(message, crmFormProcessorForm.getCrmContact(), mailSender, siteConfig);
                
        		}
        	}
        }
    	
        this.webJaguar.newLeadEmail(this.webJaguar.getCrmContactById(crmFormProcessorForm.getCrmContact().getId()), crmForm.getRecipientEmail(), crmForm.getRecipientCcEmail(), crmForm.getRecipientBccEmail(), request, crmForm, newFile,  newFile1, newFile2, newFile3, newFile4, mailSender);  

    	// Sync CRM form values (***ONLY CUSTOM FILEDS***) with Customer if 'CRM_CUSTOMER_SYNCH' is true
    	if ((Boolean) gSiteConfig.get("gCRM") && siteConfig.get("CRM_CUSTOMER_SYNCH").getValue().equals("true")) {
    		if( crmFormProcessorForm.getCrmContact().getEmail1() != null ) {
				Customer customer = this.webJaguar.getCustomerByUsername(crmFormProcessorForm.getCrmContact().getEmail1());
				if(customer != null) {
					try {
						this.webJaguar.crmContactToCustomerSynch(crmFormProcessorForm.getCrmContact(), customer, true);
					} catch (Exception e) {}
					this.webJaguar.updateCustomer(customer, false, false);
				} else {
					crmFormProcessorForm.getCrmContact().setUserId(null);
				}
			}
    	}
    	
    	if(ServletRequestUtils.getStringParameter(request, "returnUrl") != null) {
    		crmForm.setReturnUrl(ServletRequestUtils.getStringParameter(request, "returnUrl"));
    	}
    	
    	String path = request.getContextPath().equals("") ? "/" : request.getContextPath();
    	if(crmForm.getFormName()!=null && crmFormProcessorForm!=null && crmFormProcessorForm.getCrmContact()!=null && crmFormProcessorForm.getCrmContact().getId()!=null){
			Cookie saleRepAccountTrackCodeCookie = new Cookie("LPF", crmFormProcessorForm.getCrmContact().getId().toString());
			saleRepAccountTrackCodeCookie.setMaxAge(Constants.SECONDS_IN_DAY * 365);
			saleRepAccountTrackCodeCookie.setPath(path);
			response.addCookie(saleRepAccountTrackCodeCookie);	
		}
    	
    	if(crmForm.getReturnUrl() != null){
        	return new ModelAndView(new RedirectView( crmForm.getReturnUrl()));  
        }
    return null;  
    }
    
    private synchronized boolean checkDuplicateEntries(CrmFormProcessorForm crmFormProcessorForm){
 
    	Calendar now = Calendar.getInstance();
        Long currentTime = now.getTimeInMillis();
        Long getTime = 0L;
      
        if(this.webJaguar.getCrmTime(crmFormProcessorForm.getCrmContact().getEmail1())!=null){
            getTime = this.webJaguar.getCrmTime(crmFormProcessorForm.getCrmContact().getEmail1());
        }else { 
        	if(crmFormProcessorForm.getCrmContact().getEmail1()!=null){                                      
        		this.webJaguar.insertCrmTime(crmFormProcessorForm.getCrmContact().getEmail1(),currentTime);  
        	}                                                                                                 
        	if(crmFormProcessorForm.getCrmContact().getEmail2()!=null){                                      
        		this.webJaguar.insertCrmTime(crmFormProcessorForm.getCrmContact().getEmail2(),currentTime);  
        	}                                                                                                
        	return false;
        }

        if(currentTime - getTime < 300000){
        	return true;
        }
       
        return false;
    }
    
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("crmForm", this.webJaguar.getCrmFormByFormNumber(ServletRequestUtils.getStringParameter(request, "formNumber")) );
    	map.put("groupId", ServletRequestUtils.getStringParameter(request, "groupId") );
    	map.put("returnUrl", ServletRequestUtils.getStringParameter(request, "returnUrl") );
    	map.put("updateIfExist", ServletRequestUtils.getStringParameter(request, "updateIfExist"));
    	map.put("adminEmailSubject", ServletRequestUtils.getStringParameter(request, "adminEmailSubject"));
    	return map;
    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
    	CrmFormProcessorForm crmFormProcessorForm = new CrmFormProcessorForm();
    	CrmForm crmForm = null;
    	if(ServletRequestUtils.getStringParameter(request, "formNumber") != null) {
    		crmForm = this.webJaguar.getCrmFormByFormNumber(ServletRequestUtils.getStringParameter(request, "formNumber"));
    	}
    	if(crmForm != null) {
    		CrmContactField crmContactField = null;
        	int i=0;
        	List<CrmContactField> list = new ArrayList<CrmContactField>();
        	
    		for(CrmFormField crmFormField: crmForm.getCrmFields()){
    			switch (crmFormField.getContactFieldId()) {
					case 1010:
						crmFormProcessorForm.getCrmContact().setFirstName(ServletRequestUtils.getStringParameter(request, "crmContact.firstName"));
			        	break;
					case 1020:
						crmFormProcessorForm.getCrmContact().setLastName(ServletRequestUtils.getStringParameter(request, "crmContact.lastName"));
			        	break;
					case 1030:
						crmFormProcessorForm.getCrmContact().setEmail1(ServletRequestUtils.getStringParameter(request, "crmContact.email1"));
			        	break;
					case 1040:
						crmFormProcessorForm.getCrmContact().setEmail2(ServletRequestUtils.getStringParameter(request, "crmContact.email2"));
			        	break;
					case 1050:
						crmFormProcessorForm.getCrmContact().setPhone1(ServletRequestUtils.getStringParameter(request, "crmContact.phone1"));
			        	break;
					case 1060:
						crmFormProcessorForm.getCrmContact().setPhone2(ServletRequestUtils.getStringParameter(request, "crmContact.phone2"));
			        	break;
					case 1070:
						crmFormProcessorForm.getCrmContact().setFax(ServletRequestUtils.getStringParameter(request, "crmContact.fax"));
			        	break;
					case 1080:
						crmFormProcessorForm.getCrmContact().setStreet(ServletRequestUtils.getStringParameter(request, "crmContact.street"));
			        	break;
					case 1090:
						crmFormProcessorForm.getCrmContact().setCity(ServletRequestUtils.getStringParameter(request, "crmContact.city"));
			        	break;
					case 1100:
						crmFormProcessorForm.getCrmContact().setStateProvince(ServletRequestUtils.getStringParameter(request, "crmContact.stateProvince"));
			        	break;
					case 1110:
						crmFormProcessorForm.getCrmContact().setZip(ServletRequestUtils.getStringParameter(request, "crmContact.zip"));
			        	break;
					case 1120:
						crmFormProcessorForm.getCrmContact().setCountry(ServletRequestUtils.getStringParameter(request, "crmContact.country"));
						break;
					case 1130:
						crmFormProcessorForm.getCrmContact().setDescription(ServletRequestUtils.getStringParameter(request, "crmContact.description"));
			        	break;
					case 1140:
						crmFormProcessorForm.getCrmContact().setAddressType(ServletRequestUtils.getStringParameter(request, "crmContact.addressType"));
			        	break;
					case 1160:
						crmFormProcessorForm.getCrmContact().setLanguage(ServletRequestUtils.getStringParameter(request, "crmContact.language"));
			        	break;
					case 1150:
					 	// Do nothing for attachment
						break;
					default:
						crmContactField = new CrmContactField();
		    			
						if(crmFormField.getType() == 3){
		    				StringBuffer multiValues = new StringBuffer();
							for(int j=0; j< ServletRequestUtils.getStringParameters(request, "crmContact.crmContactFields["+i+"].fieldValue").length; j++){
								multiValues.append(ServletRequestUtils.getStringParameters(request, "crmContact.crmContactFields["+i+"].fieldValue")[j]);
								if(j != ServletRequestUtils.getStringParameters(request, "crmContact.crmContactFields["+i+"].fieldValue").length-1){
									multiValues.append(",");
								}
							}crmContactField.setFieldValue(multiValues.toString());
		    			} else {
		    				crmContactField.setFieldValue(ServletRequestUtils.getStringParameter(request, "crmContact.crmContactFields["+i+"].fieldValue"));
		        		}
		    			
		    			list.add(new CrmContactField());
		    			i++;
		    			crmFormProcessorForm.getCrmContact().setCrmContactFields(list);
		    	    	break;
				}
    		}
    	}
    	
    	baseFile = new File( getServletContext().getRealPath( "/assets/CRM/" ) );
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				baseFile = new File( (String) prop.get( "site.root" ) + "/assets/CRM/" );
			}
			if(!baseFile.exists()) {
				baseFile.mkdir();
			}
		} catch ( Exception e ){
			
		}
		
    	return crmFormProcessorForm;
    }
    
    @SuppressWarnings("unused")
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
    	CrmFormProcessorForm crmFormProcessorForm = (CrmFormProcessorForm) command;
    	CrmForm crmForm = null;
    	if(ServletRequestUtils.getStringParameter(request, "aAns") != null && !ServletRequestUtils.getStringParameter(request, "aAns").isEmpty()) {
    		if(ServletRequestUtils.getStringParameter(request, "uAns") != null && !ServletRequestUtils.getStringParameter(request, "uAns").isEmpty()) {
    			if(!ServletRequestUtils.getStringParameter(request, "aAns").equals(ServletRequestUtils.getStringParameter(request, "uAns"))) {
    				errors.rejectValue("uAns", "form.invalidInput", "Invalid Input");
        		}
    		} else {
    			errors.rejectValue("uAns", "form.invalidInput", "Invalid Input");
        	}
    	}
        if(crmFormProcessorForm.getFormNumber() != null) {
    		crmForm = this.webJaguar.getCrmFormByFormNumber(crmFormProcessorForm.getFormNumber());
    	}
        
    	if(crmForm != null) {
    		int i=0;
    		for(CrmFormField crmFormField: crmForm.getCrmFields()){
    		
    			if(crmFormField.isRequired()){
    				switch (crmFormField.getContactFieldId()) {
					case 1010:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.firstName", "form.required", "required");
						break;
					case 1020:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.lastName", "form.required", "required");
						break;
					case 1030:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.email1", "form.required", "required");
						break;
					case 1040:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.email2", "form.required", "required");
						break;
					case 1050:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.phone1", "form.required", "required");
						break;
					case 1060:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.phone2", "form.required", "required");
						break;
					case 1070:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.fax", "form.required", "required");
						break;
					case 1080:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.street", "form.required", "required");
						break;
					case 1090:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.city", "form.required", "required");
						break;
					case 1100:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.stateProvince", "form.required", "required");
						break;
					case 1110:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.zip", "form.required", "required");
						break;
					case 1120:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.country", "form.required", "required");
						break;
					case 1130:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.description", "form.required", "required");
						break;
					case 1140:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.addressType", "form.required", "required");
						break;
					case 1150:
						// No need to validate if attachment is not there at present
						break;
					case 1160:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.language", "form.required", "required");
						break;
					default:
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "crmContact.crmContactFields["+i+"].fieldValue", "form.required", "required");
						break;
    				}
    			}
    			// this is custom validation just for glosoweatherstation
    			if(crmFormField.getType() == 7) {
    				if(ServletRequestUtils.getStringParameter(request, "crmContact.crmContactFields["+i+"].fieldValue") != null && !ServletRequestUtils.getStringParameter(request, "crmContact.crmContactFields["+i+"].fieldValue").trim().isEmpty()){
    					
    					CrmContactField crmContactField = this.webJaguar.getCrmContactFieldById(crmFormField.getContactFieldId());
        				if(crmContactField != null && crmContactField.getOptions() != null && !crmContactField.getOptions().trim().isEmpty()) {
        					
        					String[] optionValues = crmContactField.getOptions().split(",");
            				Set<String> optionValuesSet = new HashSet<String>();
            				
            				for(String value : optionValues){
            					optionValuesSet.add(value.trim());
            				}
            				
            				String fieldValue = ServletRequestUtils.getStringParameter(request, "crmContact.crmContactFields["+i+"].fieldValue");
            				if(!optionValuesSet.contains(fieldValue)) {
            					errors.rejectValue("crmContact.crmContactFields["+i+"].fieldValue", "form.invalidInput", "invalid"+crmFormField.getFieldName());
            	        	}
            				
            				if(this.webJaguar.getFieldValueCountByFieldIdAndValue(crmFormField.getContactFieldId(), fieldValue.trim()) > 0){
            					errors.rejectValue("crmContact.crmContactFields["+i+"].fieldValue", "form.invalidInput", "invalid"+crmFormField.getFieldName());
                	        }
            			}
    				}
    			}
    			if(crmFormField.getContactFieldId() == 1030 && !Constants.validateEmail(crmFormProcessorForm.getCrmContact().getEmail1(), null)) {
    				errors.rejectValue("crmContact.email1", "form.invalidEmail", "invalid email");
    			}
    			if(crmFormField.getContactFieldId() == 1040 && !Constants.validateEmail(crmFormProcessorForm.getCrmContact().getEmail2(), null)) {
    				errors.rejectValue("crmContact.email2", "form.invalidEmail", "invalid email");
    			}
    			if(crmFormField.getContactFieldId() == 1150) {
    				//check the file type of an attachment
    				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
					MultipartFile attachment = multipartRequest.getFile( "attachment" );;
					if(attachment != null && !attachment.isEmpty()) {
						if (!checkFileContentType(attachment.getContentType())){
	    					errors.rejectValue("crmContact.attachment", "form.invalidFileType", "Only gif,jpg,png,doc,xls,pdf,zip,xml,mpeg and text files are allowed.");
			    		}
		    		}
				}
    			if(crmFormField.getContactFieldId() < 1000){
    				i++;
    			}
    		}
    	} else {
    		errors.reject("NOT_WJFORM", "This form does not belong to us");
    	}
    }
    
    private boolean checkFileContentType(String contentType){

    	String[] fileContentType = {
    			"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/vnd.openxmlformats-officedocument.wordprocessingml.template","application/vnd.ms-word.document.macroEnabled.12","application/vnd.ms-word.template.macroEnabled.12","application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    			"application/vnd.openxmlformats-officedocument.spreadsheetml.template","application/vnd.ms-excel.sheet.macroEnabled.12","application/vnd.ms-excel.addin.macroEnabled.12","application/vnd.ms-excel.sheet.binary.macroEnabled.12","application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation","application/vnd.openxmlformats-officedocument.presentationml.template",
    			"application/vnd.openxmlformats-officedocument.presentationml.slideshow","application/vnd.ms-powerpoint.addin.macroEnabled.12","application/vnd.ms-powerpoint.presentation.macroEnabled.12","application/vnd.ms-powerpoint.slideshow.macroEnabled.12","image/gif","image/jpeg","image/jpg","image/png","image/tiff","doc","xls","pdf","application/pdf","application/zip","text/html",
    			"application/xml","text/css","text/htm","text/plain","text/txt","image/bmp","image/vnd.microsoft.icon","application/x-rar-compressed","audio/mpeg","video/quicktime","image/vnd.adobe.photoshop","application/rtf","application/vnd.oasis.opendocument.text","application/vnd.oasis.opendocument.spreadsheet"};
    	
    	for(String type : fileContentType){
    		if(type.equalsIgnoreCase(contentType)){
    			return true;
    		}
    	}

    	return false;
    }
    
    private String getCountryCodes(String countryCode){
    	
    	Map<String,String> countryCodeMap = new HashMap<String,String>();
    	Map<String,String> countryMap = new HashMap<String,String>();

    	countryCodeMap.put("Andorra", "AD");
    	countryCodeMap.put("United Arab Emirates", "AE");
    	countryCodeMap.put("Afghanistan", "AF");
    	countryCodeMap.put("Antigua and Barbuda", "AG");
    	countryCodeMap.put("Anguilla", "AI");
    	countryCodeMap.put("Albania", "AL");
    	countryCodeMap.put("Armenia", "AM");
    	countryCodeMap.put("Netherlands Antilles", "AN");
    	countryCodeMap.put("Angola", "AO");
    	countryCodeMap.put("Antarctica", "AQ");
    	countryCodeMap.put("Argentina", "AR");
    	countryCodeMap.put("American Samoa", "AS");
    	countryCodeMap.put("Austria", "AT");
    	countryCodeMap.put("Australia", "AU");
    	countryCodeMap.put("Aruba", "AW");
    	countryCodeMap.put("Azerbaijan", "AZ");
    	countryCodeMap.put("Bosnia and Herzegovina", "BA");
    	countryCodeMap.put("Barbados", "BB");
    	countryCodeMap.put("Bangladesh", "BD");
    	countryCodeMap.put("Belgium", "BE");
    	countryCodeMap.put("Burkina Faso", "BF");
    	countryCodeMap.put("Bulgaria", "BG");
    	countryCodeMap.put("Bahrain", "BH");
    	countryCodeMap.put("Burundi", "BI");
    	countryCodeMap.put("Benin", "BJ");
    	countryCodeMap.put("Bermuda", "BM");
    	countryCodeMap.put("Brunei Darussalam", "BN");
    	countryCodeMap.put("Bolivia", "BO");
    	countryCodeMap.put("Brazil", "BR");
    	countryCodeMap.put("Bahamas", "BS");
    	countryCodeMap.put("Bhutan", "BT");
    	countryCodeMap.put("Bouvet Island", "BV");
    	countryCodeMap.put("Botswana", "BW");
    	countryCodeMap.put("Belarus", "BY");
    	countryCodeMap.put("Belize", "BZ");
    	countryCodeMap.put("Canada", "CA");
    	countryCodeMap.put("Cocos Islands", "CC");
    	countryCodeMap.put("Central African Republic", "CF");
    	countryCodeMap.put("Congo", "CG");
    	countryCodeMap.put("Switzerland", "CH");
    	countryCodeMap.put("Cote D'Ivoire", "CI");
    	countryCodeMap.put("Cook Islands", "CK");
    	countryCodeMap.put("Chile", "CL");
    	countryCodeMap.put("Cameroon", "CM");
    	countryCodeMap.put("China", "CN");
    	countryCodeMap.put("Colombia", "CO");
    	countryCodeMap.put("US Commercial", "COM");
    	countryCodeMap.put("Costa Rica", "CR");
    	countryCodeMap.put("Czechoslovakia", "CS");
    	countryCodeMap.put("Cuba", "CU");
    	countryCodeMap.put("Cape Verde", "CV");
    	countryCodeMap.put("Christmas Island", "CX");
    	countryCodeMap.put("Cyprus", "CY");
    	countryCodeMap.put("Czech Republic", "CZ");
    	countryCodeMap.put("Germany", "DE");
    	countryCodeMap.put("Djibouti", "DJ");
    	countryCodeMap.put("Denmark", "DK");
    	countryCodeMap.put("Dominica", "DM");
    	countryCodeMap.put("Dominican Republic", "DO");
    	countryCodeMap.put("Algeria", "DZ");
    	countryCodeMap.put("Ecuador", "EC");
    	countryCodeMap.put("US Educational", "EDU");
    	countryCodeMap.put("Estonia", "EE");
    	countryCodeMap.put("Egypt", "EG");
    	countryCodeMap.put("Western Sahara", "EH");
    	countryCodeMap.put("Eritrea", "ER");
    	countryCodeMap.put("Spain", "SP");
    	countryCodeMap.put("Ethiopia", "ET");
    	countryCodeMap.put("Finland", "FI");
    	countryCodeMap.put("Fiji", "FJ");
    	countryCodeMap.put("Falkland Islands", "FK");
    	countryCodeMap.put("Micronesia", "FM");
    	countryCodeMap.put("Faroe Islands", "FO");
    	countryCodeMap.put("France", "FR");
    	countryCodeMap.put("France, Metropolitan", "FX");
    	countryCodeMap.put("Gabon", "GA");
    	countryCodeMap.put("Great Britain", "GB");
    	countryCodeMap.put("Grenada", "GD");
    	countryCodeMap.put("Georgia", "GE");
    	countryCodeMap.put("French Guiana", "GF");
    	countryCodeMap.put("Ghana", "GH");
    	countryCodeMap.put("Gibraltar", "GI");
    	countryCodeMap.put("Greenland", "GL");
    	countryCodeMap.put("Gambia", "GM");
    	countryCodeMap.put("Guinea", "GN");
    	countryCodeMap.put("US Government", "GOV");
    	countryCodeMap.put("Guadeloupe", "GP");
    	countryCodeMap.put("Guadeloupe", "GO");
    	countryCodeMap.put("Greece", "GR");
    	countryCodeMap.put("S. Georgia and S. Sandwich Isls.", "GS");
    	countryCodeMap.put("Guatemala", "GT");
    	countryCodeMap.put("Guam", "GU");
    	countryCodeMap.put("Guinea-Bissau", "GW");
    	countryCodeMap.put("Guyana", "GN");
    	countryCodeMap.put("Hong Kong", "HK");
    	countryCodeMap.put("Heard and McDonald Islands", "HM");
    	countryCodeMap.put("Honduras", "HN");
    	countryCodeMap.put("Croatia", "HR");
    	countryCodeMap.put("Haiti", "HT");
    	countryCodeMap.put("Hungary", "HU");
    	countryCodeMap.put("Indonesia", "ID");
    	countryCodeMap.put("Ireland", "IE");
    	countryCodeMap.put("Israel", "IL");
    	countryCodeMap.put("India", "IN");
    	countryCodeMap.put("International", "INT");
    	countryCodeMap.put("British Indian Ocean Territory", "IO");
    	countryCodeMap.put("Iraq", "IQ");
    	countryCodeMap.put("Iran", "IR");
    	countryCodeMap.put("Iceland", "IS");
    	countryCodeMap.put("Italy", "IT");
    	countryCodeMap.put("Jamaica", "JM");
    	countryCodeMap.put("Jordan", "JO");
    	countryCodeMap.put("Japan", "JP");
    	countryCodeMap.put("Kenya", "KE");
    	countryCodeMap.put("Kyrgyzstan", "KG");
    	countryCodeMap.put("Cambodia", "KH");
    	countryCodeMap.put("Kiribati", "KI");
    	countryCodeMap.put("Comoros", "KM");
    	countryCodeMap.put("Saint Kitts and Nevis", "KN");
    	countryCodeMap.put("Korea (North)", "KP");
    	countryCodeMap.put("Korea (South)", "KR");
    	countryCodeMap.put("Kuwait", "KW");
    	countryCodeMap.put("Cayman Islands", "KY");
    	countryCodeMap.put("Kazakhstan", "KZ");
    	countryCodeMap.put("Laos", "JO");
    	countryCodeMap.put("Lebanon", "JO");
    	countryCodeMap.put("Saint Lucia", "JO");
    	countryCodeMap.put("Liechtenstein", "LI");
    	countryCodeMap.put("Sri Lanka", "LK");
    	countryCodeMap.put("Liberia", "LR");
    	countryCodeMap.put("Lesotho", "LS");
    	countryCodeMap.put("Lithuania", "LI");
    	countryCodeMap.put("Luxembourg", "LU");
    	countryCodeMap.put("Latvia", "LV");
    	countryCodeMap.put("Libya", "LY");
    	countryCodeMap.put("Morocco", "MA");
    	countryCodeMap.put("Monaco", "MC");
    	countryCodeMap.put("Moldova", "MD");
    	countryCodeMap.put("Madagascar", "MG");
    	countryCodeMap.put("Marshall Islands", "MH");
    	countryCodeMap.put("US Military", "MIL");
    	countryCodeMap.put("Macedonia", "MK");
    	countryCodeMap.put("Mali", "ML");
    	countryCodeMap.put("Myanmar", "MM");
    	countryCodeMap.put("Mongolia", "MN");
    	countryCodeMap.put("Macau", "MO");
    	countryCodeMap.put("Northern Mariana Islands", "MP");
    	countryCodeMap.put("Martinique", "MQ");
    	countryCodeMap.put("Mauritania", "MR");
    	countryCodeMap.put("Montserrat", "MS");
    	countryCodeMap.put("Malta", "MT");
    	countryCodeMap.put("Mauritius", "MU");
    	countryCodeMap.put("Maldives", "MV");
    	countryCodeMap.put("Malawi", "MW");
    	countryCodeMap.put("Mexico", "MX");
    	countryCodeMap.put("Malaysia", "MY");
    	countryCodeMap.put("Mozambique", "MZ");
    	countryCodeMap.put("Namibia", "MT");
    	countryCodeMap.put("Nato field", "MT");
    	countryCodeMap.put("New Caledonia", "MT");
    	countryCodeMap.put("Niger", "NG");
    	countryCodeMap.put("Norfolk Island", "NL");
    	countryCodeMap.put("Nigeria", "NO");
    	countryCodeMap.put("Nicaragua", "NI");
    	countryCodeMap.put("Netherlands", "NL");
    	countryCodeMap.put("Norway", "NO");
    	countryCodeMap.put("Nepal", "NP");
    	countryCodeMap.put("Nauru", "NR");
    	countryCodeMap.put("Neutral Zone", "NT");
    	countryCodeMap.put("Niue", "NU");
    	countryCodeMap.put("New Zealand", "NZ");
    	countryCodeMap.put("Oman", "OM");
    	countryCodeMap.put("Non-Profit Organization", "ORG");
    	countryCodeMap.put("Panama", "PA");
    	countryCodeMap.put("Peru", "PE");
    	countryCodeMap.put("French Polynesia", "PF");
    	countryCodeMap.put("Papua New Guinea", "PG");
    	countryCodeMap.put("Philippines", "PH");
    	countryCodeMap.put("Pakistan", "PK");
    	countryCodeMap.put("Poland", "PL");
    	countryCodeMap.put("St. Pierre and Miquelon", "PM");
    	countryCodeMap.put("Pitcairn", "PN");
    	countryCodeMap.put("Pitcairn", "PN");
    	countryCodeMap.put("Puerto Rico", "PR");
    	countryCodeMap.put("Portugal", "PT");
    	countryCodeMap.put("Palau", "PW");
    	countryCodeMap.put("Paraguay", "PY");
    	countryCodeMap.put("Qatar", "QA");
    	countryCodeMap.put("Reunion", "RE");
    	countryCodeMap.put("Romania", "RO");
    	countryCodeMap.put("Russian Federation", "RU");
    	countryCodeMap.put("Rwanda", "RW");
    	countryCodeMap.put("Saudi Arabia", "SA");
    	countryCodeMap.put("Seychelles", "SC");
    	countryCodeMap.put("Solomon Islands", "SB");
    	countryCodeMap.put("Sudan", "SD");
    	countryCodeMap.put("Sweden", "SE");
    	countryCodeMap.put("Singapore", "SG");
    	countryCodeMap.put("St. Helena", "SH");
    	countryCodeMap.put("Slovenia", "NI");
    	countryCodeMap.put("Svalbard and Jan Mayen Islands", "SJ");
    	countryCodeMap.put("Slovak Republic", "SK");
    	countryCodeMap.put("Sierra Leone", "SL");
    	countryCodeMap.put("San Marino", "SM");
    	countryCodeMap.put("Senegal", "SN");
    	countryCodeMap.put("Somalia", "SO");
    	countryCodeMap.put("Suriname", "SR");
    	countryCodeMap.put("Sao Tome and Principe", "ST");
    	countryCodeMap.put("USSR", "SU");
    	countryCodeMap.put("El Salvador", "SV");
    	countryCodeMap.put("Syria", "SY");
    	countryCodeMap.put("Swaziland", "SZ");
    	countryCodeMap.put("Suriname", "SR");
    	countryCodeMap.put("Turks and Caicos Islands", "TC");
    	countryCodeMap.put("Chad", "TD");
    	countryCodeMap.put("French Southern Territories", "TF");
    	countryCodeMap.put("Togo", "TG");
    	countryCodeMap.put("Thailand", "TH");
    	countryCodeMap.put("Thailand", "TJ");
    	countryCodeMap.put("Tajikistan", "SR");
    	countryCodeMap.put("Tokelau", "TK");
    	countryCodeMap.put("Turkmenistan", "Turkmenistan");
    	countryCodeMap.put("Tunisia", "TN");
    	countryCodeMap.put("Tonga", "TO");
    	countryCodeMap.put("East Timor", "TP");
    	countryCodeMap.put("Turkey", "TR");
    	countryCodeMap.put("Trinidad and Tobago", "TT");
    	countryCodeMap.put("Tuvalu", "TV");
    	countryCodeMap.put("Taiwan", "TW");
    	countryCodeMap.put("Tanzania", "TZ");
    	countryCodeMap.put("Thailand", "SR");
    	countryCodeMap.put("Ukraine", "UA");
    	countryCodeMap.put("Uganda", "UG");
    	countryCodeMap.put("United Kingdom", "UK");
    	countryCodeMap.put("US Minor Outlying Islands", "UM");
    	countryCodeMap.put("United States", "US");
    	countryCodeMap.put("Uruguay", "UY");
    	countryCodeMap.put("Uzbekistan", "UZ");
    	countryCodeMap.put("Vatican City State", "VA");
    	countryCodeMap.put("Saint Vincent and the Grenadines", "VC");
    	countryCodeMap.put("Venezuela", "VE");
    	countryCodeMap.put("Virgin Islands", "VG");
    	countryCodeMap.put("Virgin Islands (U.S.)", "VI");
    	countryCodeMap.put("Viet Nam", "VN");
    	countryCodeMap.put("Vanuatu", "VU");
    	countryCodeMap.put("Wallis and Futuna Islands", "WF");
    	countryCodeMap.put("Samoa", "WS");
    	countryCodeMap.put("Yemen", "YE");
    	countryCodeMap.put("Mayotte", "YT");
    	countryCodeMap.put("Yugoslavia", "YU");
    	countryCodeMap.put("South Africa", "ZA");
    	countryCodeMap.put("Zambia", "ZM");
    	countryCodeMap.put("Zaire", "ZR");
    	countryCodeMap.put("Zimbabwe", "ZW");
    	


    	for (Map.Entry<String, String> entry : countryCodeMap.entrySet())
    	{
    		countryMap.put(entry.getValue(), entry.getKey());
    	}


    	
    	return countryCodeMap.get(countryCode);
    }
    
    private void sendEmail(CrmContact crmContact, String token, Integer contactId, HttpServletRequest request) throws Exception {
		
    	MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	
		helper.setTo(crmContact.getEmail1());
		// cc emails
		if(siteConfig.get("CONTACT_EMAIL") != null && !siteConfig.get("CONTACT_EMAIL").getValue().equals("")) {
    		helper.setFrom( siteConfig.get("CONTACT_EMAIL").getValue());	
    	} else {
    		helper.setFrom("noreply@noreply.com");	
    	}
		helper.setSubject("Email Verification");
		String link = siteConfig.get("SITE_URL").getValue()+"formConfirm.jhtm?tkn="+Encoder.encode(token)+"&cid="+Encoder.encode(contactId.toString());
		if(ServletRequestUtils.getStringParameter(request, "groupId") != null && !ServletRequestUtils.getStringParameter(request, "groupId").equals("")) {
			link = link+"&gId="+Encoder.encode(ServletRequestUtils.getStringParameter(request, "groupId"));
		}
		StringBuffer message = new StringBuffer();
		
		message.append("Hello " + crmContact.getFirstName()+" "+crmContact.getLastName());
		message.append(",\n\n");
		message.append("Thank you for your registration at our website. \n" );
		message.append(" Please click on "+link+"  to verify this email address.  \n\n");	
		message.append("Thank you \n");
		
    	helper.setText(message.toString());
		mailSender.send(mms);
		
    }	
    
}