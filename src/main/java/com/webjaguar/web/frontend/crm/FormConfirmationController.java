package com.webjaguar.web.frontend.crm;



import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactGroup;
import com.webjaguar.web.domain.Encoder;

public class FormConfirmationController implements Controller{

	
	private WebJaguarFacade webJaguar; 
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
					throws Exception {
		
		Integer contactId = Integer.parseInt( Encoder.decode(ServletRequestUtils.getStringParameter(request, "cid")));
		String token = Encoder.decode(ServletRequestUtils.getStringParameter(request, "tkn"));
		CrmContact crmContact = null;
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	if(contactId != null){
			crmContact = this.webJaguar.getCrmContactById(contactId);
		}
		
		if(crmContact != null && crmContact.getToken().equals(token)){
			if(crmContact.isVerified()) {
				return new ModelAndView(new RedirectView(siteConfig.get("SITE_URL").getValue(), false));
			}
			crmContact.setVerified(true);
			this.webJaguar.updateCrmContact(crmContact);
		}
		
		if(crmContact.isVerified() && ServletRequestUtils.getStringParameter(request, "gId") != null) {
			Set<Integer> groups = new HashSet<Integer>();
			String[] groupIds = Encoder.decode( ServletRequestUtils.getStringParameter(request, "gId") ).split( "," );
			for ( int x = 0; x < groupIds.length; x++ ) {
				if ( !("".equals( groupIds[x].trim() )) ) {
					try {
						groups.add( Integer.valueOf( groupIds[x].trim() ) );
					}catch ( Exception e ) {
						// do nothing
					}
				}
			}
			this.webJaguar.batchCrmContactGroupIds( crmContact.getId(), groups, "add");
			
			Iterator<Integer> iter = groups.iterator();
        	List<Integer> messageIds = new ArrayList<Integer>();
        	while(iter.hasNext()){
        		
        		CrmContactGroup crmContactGroup = this.webJaguar.getContactGroupById(iter.next());
        		if( crmContactGroup != null && crmContactGroup.getMessageId() != null && !messageIds.contains(crmContactGroup.getMessageId())) {
        		
        			messageIds.add(crmContactGroup.getMessageId());
        			SiteMessage message = this.webJaguar.getSiteMessageById(crmContactGroup.getMessageId());
        			this.webJaguar.sendAutoResponse(message, crmContact, mailSender, siteConfig);
                
        		}
        	}
        }
		
		return new ModelAndView(new RedirectView(siteConfig.get("SITE_URL").getValue(), false));
	}

}
