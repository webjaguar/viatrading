/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.web.frontend;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import org.json.JSONObject;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import spexlive.etilize.com.Attribute;
import spexlive.etilize.com.AttributeGroup;
import spexlive.etilize.com.Catalog;
import spexlive.etilize.com.CatalogServiceIntf;
import spexlive.etilize.com.Description;
import spexlive.etilize.com.GetCategoryTree;
import spexlive.etilize.com.GetProduct;
import spexlive.etilize.com.ProductId;
import spexlive.etilize.com.Resource;
import spexlive.etilize.com.SelectProductFields;
import spexlive.etilize.com.Sku;
import spexlive.etilize.com.SubcatalogFilter;
import asiProduct.CriteriaSet;
import asiProduct.CriteriaSet.Options.Option;
import asiProduct.Dependencies.Dependency;
import asiProduct.PriceGrid;


import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductOptionValue;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.model.ProductTab;
import com.webjaguar.model.ReviewSearch;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.UserSession;
import com.webjaguar.thirdparty.asi.asiAPI.ProductAttributeValue;
import com.webjaguar.thirdparty.asi.asiAPI.ProductOptionSet;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.KeyBean;
import com.webjaguar.web.domain.Utilities;


public class ProductController extends WebApplicationObjectSupport implements Controller
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	private String shipping;
	private Double totalCharges;
	private String message;
	private Double shippingCost;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();

		boolean showPriceColumn = false;
		boolean showQtyColumn = false;
		
		UserSession userSession = this.webJaguar.getUserSession(request);
		Customer customer = null;
		if (userSession != null)
			customer = this.webJaguar.getCustomerById(userSession.getUserid());
		boolean customerSeeHiddendPrice = (customer == null || !customer.isSeeHiddenPrice()) ? false : true ;
		// Show add presentation only when salesrep cookie is there.
		SalesRep salesRep = null;
		
		if((Boolean)gSiteConfig.get("gPRESENTATION")) {
			salesRep = this.webJaguar.getSalesRep(request);
			model.put( "showAddPresentation", (request.getAttribute( "cookieSalesRep" ) != null && customer == null) ?  true  : false );
		}
		model.put("userId", customer!=null?customer.getId():"");
		
		Boolean adminLogin = (Boolean) request.getSession().getAttribute("adminLogin");
		if(adminLogin!=null && adminLogin){
			model.put("adminLogin", true);
		}
		//System.out.println("adminLogin------------------------------------------------" + adminLogin);
		// protected access feature
		String protectedAccess = "0";
		if (customer != null) {
			protectedAccess = customer.getProtectedAccess();
		} else if(salesRep != null) { // check for sales rep log in also
			protectedAccess = salesRep.getProtectedAccess();
		} else {
			protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			protectedAccess = Constants.FULL_PROTECTED_ACCESS;
		}
		
		// i18n
		String lang = null;
		if (request != null) {
			lang = RequestContextUtils.getLocale(request).getLanguage();
			if(lang.equalsIgnoreCase(LanguageCode.en.toString())) {
				lang = null;
			}
		}
		
		model.put("lang", lang);
		
		String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();
		String prodLayout =  siteConfig.get("PRODUCT_DEFAULT_DETAIL_LAYOUT").getValue();
		
		Boolean softLink = false;
		
		if (siteConfig.get("LEFTBAR_DEFAULT_TYPE").getValue().equals("3")) {
			model.put( "mainCategories", this.webJaguar.getMainsAndSubsCategoryLinks( request ) );
		} else {
			model.put( "mainCategories", this.webJaguar.getMainCategoryLinks( request, null) );
		}
		model.put("url", url + "?" + query);

		Product product = null;
		String skuParam = ServletRequestUtils.getStringParameter(request, "sku", "").trim();
		Integer idParam = ServletRequestUtils.getIntParameter(request, "id", 0);
		
		
		if (request.getParameter("id") != null || !skuParam.equals("")) {

			if (!skuParam.equals("")) {
				product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(skuParam), request);								
			} else {
				product = this.webJaguar.getProductById(idParam, request);				
			}
	
			if (product != null) {				
				if ((Boolean) gSiteConfig.get("gSALES_PROMOTIONS") && siteConfig.get("SALES_TAG_COUNTDOWN").getValue().equals("true") &&product.getSalesTag() != null) {
					Calendar salesTagEndDate = Calendar.getInstance();
					salesTagEndDate.setTime(product.getSalesTag().getEndDate());
					model.put("salesTagEndDate", salesTagEndDate);								
				}
				// redirect to parent sku if child sku is not allowed to be displayed.
				if ((Boolean) gSiteConfig.get("gMASTER_SKU") && Integer.parseInt(siteConfig.get("PARENT_CHILDERN_DISPLAY").getValue()) == 0) {
					if(product.getMasterSku() != null && !product.getMasterSku().trim().isEmpty()){
						Integer masterProductId = this.webJaguar.getProductIdBySku(product.getMasterSku());
						if(masterProductId != null) {
							Product masterProduct = this.webJaguar.getProductById(masterProductId, 0, false, null);
							if(masterProduct != null) {
								response.setStatus(301);
								// if mod rewrite is enabled
								if (gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && masterProduct.getSku() != null && !masterProduct.getSku().contains("/")) {
									response.setHeader("Location", request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/" + siteConfig.get("MOD_REWRITE_PRODUCT").getValue() + "/" + masterProduct.getEncodedSku() + "/" + masterProduct.getEncodedName() + ".html");
								} else {
									response.setHeader("Location", request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/product.jhtm?id=" + masterProduct.getId());
								}
								response.setHeader("Connection", "close");
							}
						}
					}
				}
				
				String morePicture = product.getImageFolder();
				File baseFile2;
				baseFile2 = new File(getServletContext().getRealPath("/assets/Image/Product/detailsbig/" + morePicture));
				
				Properties prop2 = new Properties();
				try {
					prop2.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
						if (prop2.get("site.root") != null) {
							baseFile2 = new File((String) prop2.get("site.root") + "/assets/Image/Product/detailsbig/" + morePicture);
						}
					} catch (Exception e) {
				}
				File listOfFiles[] = baseFile2.listFiles();
				
			    model.put("listOfFiles", listOfFiles);
			    
			   /* if(listOfFiles!=null){
			    	for(File filename : listOfFiles){
			    		javaxt.io.Image image = new javaxt.io.Image(filename.getPath());
			    		   
			    		final IImageMetadata metadata = Imaging.getMetadata(filename);
			    		final JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
			    		if (null != jpegMetadata) {
			    			final TiffImageMetadata exif = jpegMetadata.getExif();	   
			    		}
			    	}
			    }*/
	            
			    model.put("morePicture", morePicture); 

			    if(listOfFiles!=null)
			    for (int i = 0; i < listOfFiles.length; i++) {
			      if (listOfFiles[i].isFile()) {
			        
			      } else if (listOfFiles[i].isDirectory()) {
			        
			      }
			    }
				
				// update statistics
				this.webJaguar.nonTransactionSafeStats(product);
				model.put("recommendedSkuList", recommendedSkuList(product, model, request));
				
				// product fields
				Class<Product> c = Product.class;
				Method m = null;
				Object arglist[] = null;
				
				// product layout
				if(product.getProductLayout() != null && !product.getProductLayout().equals("")){
					prodLayout = product.getProductLayout();
				}
				
				// ASI end quantity pricing
				/*if (siteConfig.get("ASI").getValue().length() > 0 && siteConfig.get("ASI_ENDQTYPRICING").getValue().equals("true") 
						&& siteConfig.get("ASI_ENDQTYPRICING_CUSTOMER").getValue().equals("true")) {
					product.setEndQtyPricing(this.webJaguar.isEndQtyPricing(product.getSku()));
				}*/
				if (!gSiteConfig.get("gASI").equals("") && ((String) gSiteConfig.get("gASI")).equals("File") && product.getAsiXML()!=null) {
					
					if(prodLayout.equalsIgnoreCase("012") || prodLayout.equalsIgnoreCase("021") || prodLayout.equalsIgnoreCase("022") ) {
						JAXBContext context = JAXBContext.newInstance(com.webjaguar.thirdparty.asi.asiAPI.Product.class);
						Unmarshaller unmarshaller = context.createUnmarshaller();
						StringBuffer sb = new StringBuffer(product.getAsiXML());
						ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
					    com.webjaguar.thirdparty.asi.asiAPI.Product asiProduct = (com.webjaguar.thirdparty.asi.asiAPI.Product) unmarshaller.unmarshal(bis);
					    this.getAsiAttributes(asiProduct, model);
						
					    // set ASI Price with markup, if any
					    if(siteConfig.get("ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE").getValue().equals("true")){
						    this.webJaguar.applyPriceDifference(null, asiProduct,siteConfig.get("ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS").getValue(),  Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_TYPE").getValue()), Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_FORMULA").getValue()));
					    } else {
					    	com.webjaguar.thirdparty.asi.asiAPI.Product asiProduct2 = (com.webjaguar.thirdparty.asi.asiAPI.Product) unmarshaller.unmarshal(new ByteArrayInputStream(sb.toString().getBytes("UTF-8")));
					    	boolean  markUpApplied = this.webJaguar.applyPriceDifference(null, asiProduct2,siteConfig.get("ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS").getValue(),  Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_TYPE").getValue()), Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_FORMULA").getValue()));
					    	if(markUpApplied) {
					    		model.put("asiProduct2", asiProduct2);
						    }
					    }
					} else if(prodLayout.equalsIgnoreCase("005") || prodLayout.equalsIgnoreCase("007")){
						JAXBContext context = JAXBContext.newInstance(asiProduct.Product.class);
						Unmarshaller unmarshaller = context.createUnmarshaller();
						StringBuffer sb = new StringBuffer(product.getAsiXML());
						ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
					    asiProduct.Product prod = (asiProduct.Product) unmarshaller.unmarshal(bis);
					    
					    // set ASI Price with markup, if any
					    if(siteConfig.get("ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE").getValue().equals("true")){
						    this.webJaguar.applyPriceDifference(prod, null,siteConfig.get("ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS").getValue(),  Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_TYPE").getValue()), Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_FORMULA").getValue()));
					    }
					    model.put("asiProduct", prod); 
					    setAsiInfo(prod, model);
					}
				}
				
				for (ProductField productField : this.webJaguar.getProductFieldsRanked(protectedAccess, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"), lang)) {
					if (productField.isDetailsField()) {
						m = c.getMethod("getField" + productField.getId());
						productField.setValue((String) m.invoke(product, arglist));
						if (productField.getValue() != null && productField.getValue().length() > 0) {
							product.addProductField(productField);
						}
					}
				}
				//	hide price
				if (product.isHidePrice() && !customerSeeHiddendPrice) {
					product.setPrice(null);
				}
				//	hide msrp
				if (product.isHideMsrp()) {
					product.setMsrp(null);
				}
				// recommended list
				List <Product> recommendedList = null;
				if ((Boolean) gSiteConfig.get("gRECOMMENDED_LIST")) {
					if (product.getRecommendedList() != null) {
						recommendedList = new ArrayList<Product>();
						String[] skus = product.getRecommendedList().split("[,\n]"); // split by line breaks or commas
						for (int x = 0; x < skus.length; x++)
						{
							String sku = skus[x].trim();
							if (!("".equals(sku))) {
								Product recommendedProduct = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(sku), request);
								if (recommendedProduct != null) {
									recommendedList.add(recommendedProduct);
									if (!showPriceColumn && ((recommendedProduct.getPrice() != null && !recommendedProduct.getPrice().isEmpty())|| recommendedProduct.getMsrp() != null)) {
										showPriceColumn = true;					
									}
									if (!showQtyColumn && ((recommendedProduct.getPrice() != null && !recommendedProduct.getPrice().isEmpty())&& (!recommendedProduct.isLoginRequire() || (recommendedProduct.isLoginRequire() && userSession != null)))) {
										showQtyColumn = true;					
									}
								}
							}
						}
						if (!recommendedList.isEmpty()) {	// not empty
							model.put("recommendedList", recommendedList);		
							// product fields
							model.put("productFields", this.webJaguar.getProductFields(request, recommendedList, true));
						}
					}
					model.put("showPriceColumn", showPriceColumn);
					model.put("showQtyColumn", showQtyColumn);
				}
				
				// master sku
				if ((Boolean) gSiteConfig.get("gMASTER_SKU")) {
					boolean slavesShowQtyColumn = false;
					if (prodLayout.equalsIgnoreCase("014") || prodLayout.equalsIgnoreCase("015") || prodLayout.equalsIgnoreCase("018")) {
						// PremierPromotionalGroup Layout
						List <Product> slavesList = new ArrayList<Product>();
						ProductSearch search = new ProductSearch();
						search.setSku(product.getSku());
						search.setProtectedAccess(protectedAccess);
						search.setSort("price_1");
						search.setCheckInventory((Boolean) gSiteConfig.get("gINVENTORY"));
						boolean basedOnHand = false;
						if (siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly") ) {
							basedOnHand = true;
						}
						search.setBasedOnHand(basedOnHand);
						m = null;
						try {
							m = c.getMethod("getField" + siteConfig.get("MASTER_SKU_PRODUCT_FIELD").getValue());
						} catch (Exception e) {
							m = c.getMethod("getField1");
						}
												
						for (Integer id: this.webJaguar.getSlaves(search)) {
							Product slave = this.webJaguar.getProductById(id, request);
							if (slave != null) {
								ProductField productField = new ProductField();
								productField.setValue((String) m.invoke(slave, arglist));
								slave.addProductField(productField);
								slavesList.add(slave);
								if (!slavesShowQtyColumn && ((slave.getPrice() != null && !slave.getPrice().isEmpty()) && (!slave.isLoginRequire() || (slave.isLoginRequire() && userSession != null)))) {
									slavesShowQtyColumn = true;					
								}
								if (!showPriceColumn && ((slave.getPrice() != null && !slave.getPrice().isEmpty())|| slave.getMsrp() != null)) {
									showPriceColumn = true;					
								}
							}
						}
						// For sorting product filed values (if value is size)- create map
						Map<Integer, Product> sizeAscMap = new TreeMap<Integer, Product>();
						int i=0;
						for (Product slaveProduct: slavesList) {
							for(ProductField slaveProductField: slaveProduct.getProductFields()) {
								try {
									sizeAscMap.put(Integer.parseInt(Constants.getApparelSize().get(slaveProductField.getValue()).toString()), slaveProduct);								
								} catch (Exception e) {
									sizeAscMap.put(i, slaveProduct);
								}
							}
							++i;
						}
						// If product is a slave
						if(product.getMasterSku() != null && !product.getMasterSku().isEmpty()) {
							if (!siteConfig.get("MASTER_SKU_PRODUCT_FIELD").getValue().isEmpty()) {
								model.put("masterProductFieldName", this.webJaguar.getProductFieldValue(product.getMasterSku(), "field_"+siteConfig.get("MASTER_SKU_PRODUCT_FIELD").getValue()));
							}
							model.put("slaveFieldValue",(String) m.invoke(product, arglist));
						} else {
							model.put("masterProductFieldName", (String) m.invoke(product, arglist));
						}
						
						List <Product> sortedSlavesList = new ArrayList(sizeAscMap.values());
						model.put("slavesList", sortedSlavesList);
					} else if (prodLayout.equalsIgnoreCase("016")) {
						// themaxxshop layout
						
						List <Product> slavesList = new ArrayList<Product>();
						ProductSearch search = new ProductSearch();
						search.setSku(product.getSku());
						search.setProtectedAccess(protectedAccess);
						search.setCheckInventory((Boolean) gSiteConfig.get("gINVENTORY"));
						boolean basedOnHand = false;
						if (siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly") ) {
							basedOnHand = true;
						}
						search.setBasedOnHand(basedOnHand);
						
						String defualtColor = null;
						String level1SlaveSku = null;
						Map<String, Integer> colorsProductMap = new HashMap<String, Integer>();
						for (Integer id: this.webJaguar.getSlaves(search)) {
							Product level1Slave = this.webJaguar.getProductById(id, request);
							
							String colorField = null;
							try {
								m = c.getMethod("getField" + siteConfig.get("COLOR_FIELD_ID").getValue() );
								colorField = (String) m.invoke(level1Slave, arglist);
							}catch(Exception e){
								e.printStackTrace();
							}
							
							if(defualtColor == null && level1Slave != null && colorField != null && !colorField.trim().isEmpty()){
								defualtColor = colorField;
								level1SlaveSku = level1Slave.getSku();
								model.put("masterProduct", level1Slave);		
							}
							if(colorField != null){
								colorsProductMap.put(colorField.trim().toUpperCase(), id);
							}
						}
						search.setSku(level1SlaveSku);
						// product field 1 setting color
						List<ProductField> productFieldList = new ArrayList<ProductField>();
						// set default color
						// TODO
						// on kassirco the value is empty. this will NumberFormatException
						productFieldList.add( new ProductField(Integer.parseInt(siteConfig.get("COLOR_FIELD_ID").getValue()), defualtColor) );
						search.setProductField(productFieldList);
						Map<Integer, String> sizeMap = new HashMap<Integer, String>();
						for (Integer id: this.webJaguar.getSlaves(search)) {
							Product level2Slave = this.webJaguar.getProductById(id, request);
							if (level2Slave != null) {
								slavesList.add(level2Slave);
								
								try {
									// TODO
									// on kassirco the value is empty. this will NumberFormatException
									m = c.getMethod("getField" + siteConfig.get("SIZE_FIELD_ID").getValue() );
									sizeMap.put(id, (String) m.invoke(level2Slave, arglist));
								}catch(Exception e){
									e.printStackTrace();
								}
								
								if (!slavesShowQtyColumn && ((level2Slave.getPrice() != null && !level2Slave.getPrice().isEmpty()) && (!level2Slave.isLoginRequire() || (level2Slave.isLoginRequire() && userSession != null)))) {
									slavesShowQtyColumn = true;					
								}
							}
						}
						model.put("sizeMap", sizeMap);
						Map<Integer, KeyBean> colorsMap = new TreeMap<Integer, KeyBean>();
						for(KeyBean colorCode : Constants.COLOR_CODE) {
							if(colorsProductMap.get(colorCode.getName()) != null) {
								colorsMap.put(colorsProductMap.get(colorCode.getName()), colorCode);
							}
						}
						model.put("colorsMap", colorsMap);
						if(defualtColor != null){
							model.put("selectedColor", defualtColor.toUpperCase());		
						}
						List<ProductField> productFields = this.webJaguar.getProductFields(request, slavesList, true);
						if (!slavesList.isEmpty()) {	// not empty
							// For sorting product filed values (if value is size)- create map
							Map<Integer, Product> sizeAscMap = new TreeMap<Integer, Product>();
							int i=0;
							for (Product slaveProduct: slavesList) {
								for(ProductField slaveProductField: slaveProduct.getProductFields()) {
									if (slaveProductField.getId()== Integer.parseInt(siteConfig.get("SIZE_FIELD_ID").getValue())){
										try {
											sizeAscMap.put(Integer.parseInt(Constants.getApparelSize().get(slaveProductField.getValue()).toString()), slaveProduct);								
										} catch (Exception e) {
											sizeAscMap.put(i, slaveProduct);
										}										
									}
								}
								++i;
							}	
							
							List <Product> sortedSlavesList = new ArrayList(sizeAscMap.values());						
							model.put("slavesList", sortedSlavesList);
							// product fields
							model.put("slavesProductFields", productFields);
							
						}
						model.put("slavesSearch", search);
						model.put("slavesShowQtyColumn", slavesShowQtyColumn);
					} else if (prodLayout.equalsIgnoreCase("017")) {
						// Framestore Layout
						List <Product> childProducts = new ArrayList<Product>();
						ProductSearch search = new ProductSearch();
						search.setSku(product.getSku());
						search.setProtectedAccess(protectedAccess);
						search.setSort("price_1");
						search.setCheckInventory((Boolean) gSiteConfig.get("gINVENTORY"));
						boolean basedOnHand = false;
						if (siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly") ) {
							basedOnHand = true;
						}
						search.setBasedOnHand(basedOnHand);
												
						for (Integer id: this.webJaguar.getSlaves(search)) {
							Product slave = this.webJaguar.getProductById(id, request);
							if (slave != null) {
								childProducts.add(slave);
							}
						}
						model.put("childProducts", childProducts);
					} else {
						// default Master SKU layout
						List <Product> slavesList = new ArrayList<Product>();
						ProductSearch search = new ProductSearch();
						search.setSku(product.getSku());
						search.setProtectedAccess(protectedAccess);
						search.setSort("price_1");
						search.setCheckInventory((Boolean) gSiteConfig.get("gINVENTORY"));
						boolean basedOnHand = false;
						if (siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly") ) {
							basedOnHand = true;
						}
						search.setBasedOnHand(basedOnHand);
						// product fields
						int gPRODUCT_FIELDS = (Integer) ((Map<String, Object>) request.getAttribute("gSiteConfig")).get("gPRODUCT_FIELDS");
						for (int i=1; i<=gPRODUCT_FIELDS; i++) {
							if (request.getParameter("field_" + i) != null) {
								if (search.getProductFieldMap() == null) {
									search.setProductFieldMap(new HashMap<String, ProductField>());
								}
								ProductField pField = new ProductField(i, request.getParameter("field_" + i));
								search.getProductFieldMap().put("field_" + i, pField);
							}
						}
						if (search.getProductFieldMap() != null) {
							search.setProductField(new ArrayList<ProductField>());
							search.getProductField().addAll(search.getProductFieldMap().values());
						}
						
						for (Integer id: this.webJaguar.getSlaves(search)) {
							Product slave = this.webJaguar.getProductById(id, request);
							if (slave != null) {
								slavesList.add(slave);
								if (!slavesShowQtyColumn && ((slave.getPrice() != null && !slave.getPrice().isEmpty()) && (!slave.isLoginRequire() || (slave.isLoginRequire() && userSession != null)))) {
									slavesShowQtyColumn = true;
								}
								if (!showPriceColumn && ((slave.getPrice() != null && !slave.getPrice().isEmpty())|| slave.getMsrp() != null)) {
									showPriceColumn = true;					
								}
							}
						}
								
						if (!slavesList.isEmpty()) {	// not empty
							model.put("slavesList", slavesList);		
							// product fields
							model.put("slavesProductFields", this.webJaguar.getProductFields(request, slavesList, true));
						}
						model.put("slavesSearch", search);
					}
					model.put("showPriceColumn", showPriceColumn);
					model.put("slavesShowQtyColumn", slavesShowQtyColumn);					
				}
				
				// box
				List <Product> boxContentsList = null;
				if ((Boolean) gSiteConfig.get("gBOX") && product.getBoxSize() != null) {
					if (product.getProductOptions() != null) {
						boxContentsList = new ArrayList<Product>();
						for (ProductOption op:product.getProductOptions()) {
							if (op.getType() != null && op.getType().equals("box")) {		
								Product boxContentProduct = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(op.getName().trim()), request);
								if (boxContentProduct != null) {
									boxContentProduct.setType("box");
									boxContentProduct.setOptionIndex(op.getIndex());
									boxContentProduct.setOptionCode(op.getOptionCode());
									boxContentsList.add(boxContentProduct);	
								}		
							}
						}
					}
					if (boxContentsList != null && !boxContentsList.isEmpty()) {	// not empty
						model.put("boxContentsList", boxContentsList);	
						model.put("boxSize", product.getBoxSize());
						model.put("boxType", true);
					}
				}
				// initial ImageUrl
				Set<String> imageUrlSet = null;
				if (product.getProductOptions() != null) {
					imageUrlSet = new HashSet<String>();
					for (ProductOption op:product.getProductOptions()) {
						for(ProductOptionValue value : op.getValues()) {
							if(value.getIndex() == 0) {
								if(value.getImageUrl() != null && !value.getImageUrl().isEmpty()) {
									imageUrlSet.add(value.getImageUrl());
								}
							}
						}
					}
				}
				model.put("imageUrlSet", imageUrlSet);	
				
				// also consider
				if ((Boolean) gSiteConfig.get("gALSO_CONSIDER")) {
					Map<Integer, Product> alsoConsiderMap = new HashMap<Integer, Product>();
					Product alsoConsider = this.webJaguar.getAlsoConsiderBySku(product.getAlsoConsider(), request);
					if (alsoConsider != null) {
						alsoConsiderMap.put(product.getId(), alsoConsider);
					}
					if (recommendedList != null && !recommendedList.isEmpty() && 
							siteConfig.get("ALSO_CONSIDER_DETAILS_ONLY").getValue().equals("false")) {
						// get also consider for recommended list				
						for (Product recommended:recommendedList) {
							alsoConsider = this.webJaguar.getAlsoConsiderBySku(recommended.getAlsoConsider(), request);
							if (alsoConsider != null) {
								alsoConsiderMap.put(recommended.getId(), alsoConsider);
							}								
						}
					}
					if (!alsoConsiderMap.isEmpty()) {	// not empty
						model.put("alsoConsiderMap", alsoConsiderMap);								
					}
				}
				// tab list
				List <ProductTab> tabList = null;
				Method m1 = null;
				Method m2 = null;
				int numTab = (Integer) gSiteConfig.get("gTAB");
				if (numTab > 0) {
					int i=0;
					tabList = new ArrayList<ProductTab>();
					while (i++ < numTab)
					{
						m1 = c.getMethod("getTab" + i);
						m2 = c.getMethod("getTab" + i + "Content");
						String tabName = ((String) m1.invoke(product, arglist));
						String tabContent = ((String) m2.invoke(product, arglist));
						if (tabName != null && tabName.length() > 0) {
							tabList.add(new ProductTab(tabName, tabContent, i));
						}
					}
					model.put("tabList", tabList);	
				}
				
				if(product!=null && product.getLongDesc()!=null && product.getLongDesc().indexOf("<!---->")>0 && userSession!=null){
					product.setLongDesc(product.getLongDesc().substring(product.getLongDesc().indexOf("<!---->")));
				}
				if ((Boolean) gSiteConfig.get("gPRODUCT_REVIEW")) {
					
					ReviewSearch search = new ReviewSearch();
					// page
					int page = 1;
					if (request.getParameter("page") != null) {
						page = ServletRequestUtils.getIntParameter(request, "page", 1);
						if (page < 1) {
							search.setPage(1);
						} else {
							search.setPage(page);				
						}
					}

					search.setProductSku(product.getSku());
					PagedListHolder productReviewList = new PagedListHolder(this.webJaguar.getProductReviewListByProductSku(product.getSku(), true, null));
					productReviewList.setPageSize(20);
					productReviewList.setPage(page - 1);
					model.put("productReviewList", productReviewList);
					if (prodLayout.equalsIgnoreCase("006")) {
						model.put("productReviewListLast3", this.webJaguar.getProductReviewListByProductSku(product.getSku(), true, 3));
						model.put("flashSource", ((String) c.getMethod("getField1").invoke(product, arglist)));
					}
					model.put("cid", ServletRequestUtils.getIntParameter(request, "cid", -1));
					if (siteConfig.get("PRODUCT_REVIEW_TYPE").getValue().equals("anonymous")) {
						model.put("productRate", product.getRate());
					}
				}
				// canonical link
				if (gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null && !product.getSku().contains("/")) {
					model.put("canonicalLink", request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/" + siteConfig.get("MOD_REWRITE_PRODUCT").getValue() + "/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html");
				}
				// set layout
				setLayout(userSession, product, request, response, model);
				
				// slide show
				if (product.getImageLayout() != null && product.getImageLayout().equals("ss") &&
						siteConfig.get("PRODUCT_IMAGE_LAYOUTS").getValue().contains("ss")) {
					product.setImages(null);
				} else {
					// get images from custom folder
					if (product.getImageFolder() != null && (product.getImageFolder().trim().equals("") || product.getImageFolder().startsWith(".")))
						product.setImageFolder(null);
					if (product.getImageFolder() != null) {
						File baseFile = new File(getServletContext().getRealPath("/assets/Image/Product"));
						Properties prop = new Properties();
						try
						{
							prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
							if (prop.get("site.root") != null)
							{
								baseFile = new File((String) prop.get("site.root") + "/assets/Image/Product");
							}
						}
						catch (Exception e)
						{}
						File detailsbig = new File(baseFile, "detailsbig");
						File imageFolder = new File (detailsbig, product.getImageFolder());
						if (imageFolder.exists() && imageFolder.isDirectory()) {
							for (File image : imageFolder.listFiles()) {
								if (!image.isHidden() && !image.isDirectory() && 
										(image.getName().toLowerCase().endsWith(".jpg") || image.getName().toLowerCase().endsWith(".gif"))) {
									//ProductImage pi = new ProductImage();
									//pi.setImageUrl(product.getImageFolder() + "/" + image.getName());
									//product.addImage(pi);
								}
							}
						}
					}
				}

				// long description
				if (siteConfig.get("DETAILS_LONG_DESC_HTML").getValue().equals("false") && product.getLongDesc() != null) {
					product.setLongDesc(product.getLongDesc().replace("\n", "<br>"));					
				}

				// Etilize
				String appId = siteConfig.get("ETILIZE").getValue();
		    	if (appId.trim().length() > 0) {
					setEtilize(appId, product, tabList, model, request.getParameter("hack") != null, request.getParameter("debug") != null);
				}
		    	
				// budget by  products
				if ((Boolean) gSiteConfig.get("gBUDGET_PRODUCT") && customer != null) {
					List<Product> productList = new ArrayList<Product>();
					productList.add(product);
					model.put("budgetProduct", this.webJaguar.getBudgetProduct(customer.getId(), productList));
				}
				
				// product variant
				if ((Boolean) gSiteConfig.get("gPRODUCT_VARIANTS")) {
					product.setVariants(this.webJaguar.getProductVariant(product.getId(), null, false));
				}
				
				// doors2go pre-hanging
				if (gSiteConfig.get("gSITE_DOMAIN") != null && gSiteConfig.get("gSITE_DOMAIN").equals("doors2gostore.com") 
						&& product.getField1() != null && product.getField1().toLowerCase().contains("door") && !product.getProductOptions().isEmpty() && !product.getOptionCode().trim().toUpperCase().equals("GLOBAL")) {
					model.put("preHangingDoorOptions", this.webJaguar.getProductOptionsByOptionCode("GLOBAL", false, product.getSalesTag(), null));
					model.put("preHanging", this.webJaguar.getPreHanging(product.getField1(), null, null, product.getOptionCode().trim()));
				}
				
				if ((Boolean) gSiteConfig.get("gINVENTORY")) {
					// check type on inventory for frontend
					if(siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly") ) {
						product.setInventory(product.getInventoryAFS());	
					}
					if(siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("ship") ) {
						product.setInventory(product.getInventory());	
					}
				}
			} else {
				// check redirect sku

				if (siteConfig.get("PRODUCT_INACTIVE_REDIRECT_FIELD").getValue().length() > 0) {
					String redirectURL = this.webJaguar.getProductRedirectUrl(skuParam, idParam, protectedAccess, "field_" + siteConfig.get("PRODUCT_INACTIVE_REDIRECT_FIELD").getValue());
					if (redirectURL != null && redirectURL.trim().length() > 0) {
						response.setStatus(301);
						response.setHeader("Location", redirectURL);
						response.setHeader("Connection", "close");
					}
				}
				
				Layout productLayout = this.webJaguar.getSystemLayout("productNotFound", request.getHeader("host"), request);
				model.put("productLayout", productLayout);
				model.put("message", "product.exception.notfound");
			}
			//System.out.println("size: "  +product.getProductFields());
			model.put("product", product);
			getMootoolsStatus(model, request, product);
		}
		
		String activeSoftLink = this.webJaguar.isProductSoftLink(idParam);		
		if(activeSoftLink==null || activeSoftLink.length()<=0){
			activeSoftLink = this.webJaguar.isProductSoftLinkSku(skuParam);
		}
		
		if(activeSoftLink!=null && (activeSoftLink.equalsIgnoreCase("true") || activeSoftLink.equalsIgnoreCase("1"))){
			
			softLink = true;
			StringBuilder sUrl = new StringBuilder();
			sUrl.append(request.getScheme()+"://www.viatrading.com/wholesale/283/ALL-PRODUCTS.html?hideSearchBox=false&keywords=&cid=283");
			
			try{
				String field2 = null;
				
				if(product!=null){
					field2 = product.getField2();
				}
				
				if(field2!=null && field2.startsWith(",")){
					field2 = field2.replaceFirst(",","");
				}
				
				if(field2!=null && field2.charAt(field2.length()-1)==','){
					field2 = field2.substring(0, field2.length()-1);
				}
				
				String[] categories = null;
				if(field2!=null){
					categories = field2.split(",");
				}
				
				if(categories!=null){
					for(String category : categories){
						if(!(category.equalsIgnoreCase(",")) && category.length()>0){
							sUrl.append("&facetNameValue=Category_value_+");
							sUrl.append(category);
						}
					}
				}
			}
			catch(Exception ex){
			}
			model.put("softLinkUrl", sUrl.toString());
		}
		
		Cart cart = null;
		Integer userId = null;
		Double subTotal = 00.0;
		Double grandTotal = 00.0;
		Double discount = 00.0;
		Double tax = 00.0;
		String[] skuArray = null;
		List<String> skuList = new ArrayList<String>();

		if(userSession!=null){
	    	userSession = (UserSession) request.getAttribute( "userSession" );
		}
		if (userSession != null) { 
			userId = userSession.getUserid();
			customer = this.webJaguar.getCustomerById(userId);
			cart = this.webJaguar.getUserCart(userId, null);
		}else { 
			cart = (Cart) WebUtils.getSessionAttribute(request, "sessionCart");
		}
		if(cart!=null){
			for(CartItem cartItem : cart.getCartItems()) {
				cartItem.setProduct(this.webJaguar.getProductById(cartItem.getProduct().getId(), request));
			}
			String message = this.webJaguar.updateCart(cart, (Map<String, Object>) request.getAttribute("gSiteConfig"), (Map<String, Configuration>) request.getAttribute("siteConfig"), RequestContextUtils.getLocale(request).getLanguage(), request.getHeader("host"), null);
			for(CartItem cartItem: cart.getCartItems()) {
				
				if(cartItem.getUnitPrice()==null){
					cartItem.setUnitPrice(0.0);
				}
				
				subTotal += (cartItem.getQuantity()*cartItem.getUnitPrice());
				discount += (cartItem.getDiscount());
				
				URL url2 = new URL(request.getScheme()+"://www.viatrading.com/dv/liveapps/showrelatedskus.php?sku="+cartItem.getProduct().getSku());
				HttpURLConnection con = (HttpURLConnection) url2.openConnection();
			    con.setRequestMethod("GET");
			    con.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			    con.addRequestProperty("User-Agent", "Mozilla");
			    con.addRequestProperty("Referer", "google.com");
			    con.setRequestProperty("Accept", "text/html, text/*");
			    con.addRequestProperty("Content-Type","application/json; charset=UTF-8"); 
				
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				 
				while((inputLine = in.readLine()) != null)
				{
				    if(inputLine!=null && !inputLine.isEmpty()){
				    	skuArray = inputLine.split(",");
				    }
				}
				if(skuArray!=null){
					for(String tmp : skuArray){
						skuList.add(tmp);
					}
				}
				in.close();
			}
		}
		
		model.put("cart", cart);
		model.put("subTotal", subTotal);
		model.put("discount", discount);
		model.put("tax", tax);

		String shippingType = "";
		String shippingMessage = "";
		
		shippingCost = 0.0;
		
		//shippingCalculate(model, request);

		if(shippingCost==null){
			shippingCost = 0.0;
		}
		grandTotal = subTotal - discount + shippingCost + tax;

		model.put("shippingCost", shippingCost);
		model.put("shippingType", shippingType);
		model.put("shippingMessage", shippingMessage);
		model.put("grandTotal", grandTotal);
		
		
		String loadUrl1 = request.getScheme()+"://www.viatrading.com/wholesale/738/Load-Center.html?cid=738&sort=&sorttype=&state=asc&userId=1&field_39=" + (product!=null?product.getSku():"") + "&reset=&fsort=&size=25&page=1&redirectSku=" + (product!=null?product.getSku():"");	
		String loadUrl2 = request.getScheme()+"://www.viatrading.com/login.jhtm?forwardAction=http%3A%2F%2Fwww.viatrading.com%2Fcategory.jhtm%3Fcid%3D738&viewManifest=true&redirectSku=" + (product!=null?product.getSku():"");
		
		model.put("loadUrl1", loadUrl1);
		model.put("loadUrl2", loadUrl2);
		
		StringBuilder sb2 = new StringBuilder();
		if(skuList!=null){
			for(String sku : skuList){
				Integer productId = this.webJaguar.getProductIdBySku(sku);
				product = this.webJaguar.getProductById(productId, request);
				if(product!=null){
					sb2.append("<div class=\"sc_product\">");
					sb2.append("<div class=\"sc_product_image\">");
	
					String productLink = null;		
	
					if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
						productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
					} else {
						productLink = "product.jhtm?id=" + product.getId();
					}
					
					sb2.append("<a href=\""+productLink+"\">");
					if(product.getThumbnail()!=null){
						sb2.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
					}else{
						sb2.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
					}
					sb2.append("</a>");
					
					sb2.append("</div>");
					sb2.append("<div class=\"sc_product_name\">");
					sb2.append("<a href=\""+productLink+"\">");sb2.append(product.getName());sb2.append("</a>");
					sb2.append("</div>");
					sb2.append("<div class=\"sc_product_sku\">");
					sb2.append(product.getSku());
					sb2.append("</div>");
					sb2.append("<div class=\"sc_product_price\">");
					if(product.getProductLayout()!=null && product.getProductLayout().equalsIgnoreCase("049")){
						sb2.append((product.getField11()!=null)?product.getField11():"");
					}else{
						sb2.append((product.getPrice1()!=null)?product.getPrice1():"");
					}
					sb2.append("</div>");
					sb2.append("</div>");
				}
			}
		}
		model.put("sb", sb2); 	
		lang = RequestContextUtils.getLocale(request).getLanguage();

		
		// if product is inactive and LOC not checked redirect to -
	   if (product == null && !softLink) {
	          return new ModelAndView(new RedirectView("https://www.viatrading.com/wholesale-products/"));
       }
       else  if (product != null && (lang.equalsIgnoreCase(LanguageCode.es.toString())) && (product.getRedirectUrlEs() != null && product.getRedirectUrlEs().length() > 0)) {
		          return new ModelAndView(new RedirectView(product.getRedirectUrlEs()));
	   } else if(product != null &&  (lang.equalsIgnoreCase(LanguageCode.en.toString()) && (product.getRedirectUrlEn() != null && product.getRedirectUrlEn().length() > 0))) {
		         return new ModelAndView(new RedirectView(product.getRedirectUrlEn()));
	   } else {
		
		if (siteConfig.get("PRODUCT1_DIRECTORY").getValue().length() > 0) {
			int layout = Integer.parseInt(prodLayout);		
			if(softLink!=null && softLink){
				layout = 52;
			}
			
			String view = null;
			switch (layout) {
			case 47:
				view = "frontend/product1a";							
				break;
			case 48:
				view = "frontend/product1b";
				break;
			case 49:
				view = "frontend/product1c";
				break;
			case 50:
				view = "frontend/product1d";
				break;
			case 51:
				view = "frontend/product1e";							
				break;
			}
			return new ModelAndView(siteConfig.get("PRODUCT1_DIRECTORY").getValue() + "/" + view, "model", model);	
		} else {
			int layout = Integer.parseInt(prodLayout);
			String view = null;
			
			if(softLink!=null && softLink){
				switch (layout) {
				case 47:
					view="frontend/layout/template6/viaresponsive/product1as";   //Viatrading layout
					break;	
				case 48:
					view="frontend/layout/template6/viaresponsive/product1bs";   //Viatrading layout
					break;	
				case 49:
					view="frontend/layout/template6/viaresponsive/product1cs";   //Viatrading layout
					break;	
				case 50:
					view="frontend/layout/template6/viaresponsive/product1ds";   //Viatrading layout
					break;	
				case 51:
					view="frontend/layout/template6/viaresponsive/product1es";   //Viatrading layout
					break;
				default:				
					view = "frontend/product";							// default
				break;
				}
			}
			else{ 
				switch (layout) {
				case 1:
					view = "frontend/product2";							
					break;
				case 2:
					view = "frontend/product3";
					break;
				case 3:
					view = "frontend/product4";
					break;
				case 4:
					view = "frontend/product5";
					break;
				case 5:
					view = "frontend/product6";							//ASI Very Old (< 2011)
					break;
				case 6:
					view = "frontend/product7";
					break;
				case 7:
					view = "frontend/product8";							//ASI Old (2011 - 2012)
					break;
				case 8:
					view = "frontend/viatrading/product9";				// Viatrading
					break;
				case 9:
					view = "frontend/product10";						// Hometheatergear
					break;
				case 10:
					view = "frontend/product11";						// ROI	
					break;
				case 11:
					view = "frontend/product12";						// Framestore- Configurator
					break;
				case 12:
					view = "frontend/asi/product19";					//ASI (>Aug 2012)
					break;
				case 13:
					view = "frontend/framestore/product14";				//Framestore- Old (Feb 2012 - August 2012)
					break;
				case 14:
					view = "frontend/product15";						// Premier Group - Apparel
					break;
				case 15:
					view = "frontend/product16";						// Brandit (Parent-Child)
					break;
				case 16:
					view = "frontend/product17";						// The Maxxshop
					break;
				case 17:
					view = "frontend/framestore/product18";				// Framestore- New (> Aug 2012)
					break;
				case 18:
					view = "frontend/product15";						// Premier Group - Non Apparel
					break;
				case 19:
					view = "frontend/fantasia/product";					// Fantasia
					break;	
				case 20:
					view = "frontend/cosmetix/product";				// Cosmetix
					break;
				case 21:
					view = "frontend/asi/directpromoshop/product";		// ASI - directpromoshop
					break;	
				case 22:
					view = "frontend/bnoticed/product";				// Bnoticed
					break;	
				case 23:
					view="frontend/layout/template6/adi/product3";   //Yayme layout
					break;
				case 41:
					view="frontend/layout/template6/adi/product2";     // adi Product details page
					break;
				case 45:
					view="frontend/layout/template6/adi/product3";   //Yayme layout
					break;	
				case 46:
					view="frontend/layout/template6/viaresponsive/product1";   //Viatrading layout
					break;	
				case 47:
					view="frontend/layout/template6/viaresponsive/product1a";   //Viatrading layout
					break;	
				case 48:
					view="frontend/layout/template6/viaresponsive/product1b";   //Viatrading layout
					break;	
				case 49:
					view="frontend/layout/template6/viaresponsive/product1c";   //Viatrading layout
					break;	
				case 50:
					view="frontend/layout/template6/viaresponsive/product1d";   //Viatrading layout
					break;	
				case 51:
					view="frontend/layout/template6/viaresponsive/product1e";   //Viatrading layout
					break;		
				default:				
					view = "frontend/product";							// default
					break;
				}
			}
			return new ModelAndView(view, "model", model);
		}
		
	  }
	}
	
	private void getMootoolsStatus(Map<String, Object> model, HttpServletRequest request, Product product) {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		String imageLayout = "";
		if (product != null && product.getImageLayout() != null && product.getImageLayout().length() > 0) {
			imageLayout = product.getImageLayout();
		}
		if (imageLayout.equals("")) imageLayout = siteConfig.get("PRODUCT_IMAGE_LAYOUT").getValue();
		List<ProductTab> tabList = (List<ProductTab>) model.get("tabList");
		
		if ((siteConfig.get("MOOTOOLS").getValue().equalsIgnoreCase("false")) &&
				((siteConfig.get("PRODUCT_IMAGE_LAYOUTS").getValue().contains("s2") && imageLayout.equals("s2")) ||
				(siteConfig.get("PRODUCT_IMAGE_LAYOUTS").getValue().contains("mb") && imageLayout.equals("mb")) ||
				(siteConfig.get("PRODUCT_IMAGE_LAYOUTS").getValue().contains("qb") && imageLayout.equals("qb")) ||
				siteConfig.get("GOOGLE_CURRENCY_CONVERTER").getValue().equals("true") ||
				((Integer) gSiteConfig.get("gTAB") > 0 && tabList != null && !tabList.isEmpty()))) {
			model.put("mootools", true);
		}
	}
	
	private String recommendedSkuList(Product productTemp, Map<String, Object> model, HttpServletRequest request) throws Exception{
		
		StringBuffer sb = new StringBuffer();
		
		BufferedReader in = null;
		URLConnection connection = null;
		String inputLine = null;
		String[] skuArray = null;
		String sku = null; 
		sku = productTemp.getSku();
		try{
			URL url2 = new URL(request.getScheme()+"://www.viatrading.com/dv/liveapps/showrelatedskus.php");
			connection = url2.openConnection();
			connection.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
			connection.connect();
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			inputLine = in.readLine();
		}
		catch(Exception ex){
		}

		if(inputLine!=null && !inputLine.isEmpty()){
			skuArray = inputLine.split(",");
		}

		if(skuArray==null || skuArray.length<=0)
			return null;
		
		List<Product> products = new LinkedList<Product>();
		
		for(String sku2: skuArray){
			Product pro = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(sku2), request);
			if(pro!=null){
				products.add(pro);
			}
		}
	
		if(products.size()<=0) {
			return sb.toString();
		}
		
		Iterator<Product> itr = products.iterator();
		while(itr.hasNext()) {
			Product product = itr.next();
			if (product!=null && product.getThumbnail() == null) {
				itr.remove();
			}
		}
		if(products.isEmpty()) {
			return sb.toString();
		}
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		ApplicationContext context = getApplicationContext();
		String currencySymbol = "$";
				
		sb.append("<div class=\"products_carousel_wrapper\">");
		sb.append("<div id=\"products_carousel\" class=\"products_carousel owl-carousel\">");
		
		for(Product product : products) {
			//System.out.println(product.getId());
			if(product.getProductLayout().equalsIgnoreCase("047")){
				String productLink = null;		

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");		
					

				String saleTag = "";
				if(product.getSalesTag() != null){
					saleTag = product.getSalesTag().getTitle();
				}
				
				if(product.getField32()!=null && !product.getField32().equalsIgnoreCase("")){
					sb.append("<div class=\"product_bordered_box is_ribboned\">");
					sb.append("<div class=\"ribbon_wrapper\">");
				if(product.getField32().equalsIgnoreCase("New_Item")){
					sb.append("<div class=\"ribbon ribbon_6\">NEW ARRIVAL</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Out_of_Stock")){
					sb.append("<div class=\"ribbon ribbon_4\">OUT OF STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Back_in_Stock")){
					sb.append("<div class=\"ribbon ribbon_3\">BACK IN STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Load_Added")){
					sb.append("<div class=\"ribbon ribbon_5\">NEW LOAD ADDED</div>");
				}
				else{
					sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				}
				sb.append("</div>");
				}else{
					sb.append("<div class=\"product_bordered_box\">");
				}

				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}

				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
		
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
				
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price\">");
				sb.append(product.getField8());
				sb.append("</div>");
				sb.append("</div>");
				//System.out.println("1102------------------------------------------------------------------------------>>>>>>");
				sb.append("</div>");
				
				sb.append("<div class=\"total_price\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(currencySymbol);
				sb.append(valueFormat(product.getPrice1()));
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}
			
			if(product.getProductLayout().equalsIgnoreCase("048")){
				String productLink = null;
				
				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");
				
				if(product.getField32()!=null && !product.getField32().equalsIgnoreCase("")){
					sb.append("<div class=\"product_bordered_box is_ribboned\">");
					sb.append("<div class=\"ribbon_wrapper\">");
				if(product.getField32().equalsIgnoreCase("New_Item")){
					sb.append("<div class=\"ribbon ribbon_6\">NEW ARRIVAL</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Out_of_Stock")){
					sb.append("<div class=\"ribbon ribbon_4\">OUT OF STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Back_in_Stock")){
					sb.append("<div class=\"ribbon ribbon_3\">BACK IN STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Load_Added")){
					sb.append("<div class=\"ribbon ribbon_5\">NEW LOAD ADDED</div>");
				}
				else{
					sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				}
				sb.append("</div>");
				}else{
					sb.append("<div class=\"product_bordered_box\">");
				}
			
				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
				
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price\">");
				sb.append(product.getField8());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"total_price\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(currencySymbol);
				sb.append((product.getPriceRangeMinimum()!=null?valueFormat(product.getPriceRangeMinimum()):0.0));
				sb.append("-");	
				sb.append(currencySymbol);
				sb.append((product.getPriceRangeMaximum()!=null?valueFormat(product.getPriceRangeMaximum()):0.0));
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}

			if(product.getProductLayout().equalsIgnoreCase("049")){
				String productLink = null;
				//System.out.println("1184------------------------------------------------------------------------------>>>>>>");

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");
				
				if(product.getField32()!=null && !product.getField32().equalsIgnoreCase("")){
					sb.append("<div class=\"product_bordered_box is_ribboned\">");
					sb.append("<div class=\"ribbon_wrapper\">");
				if(product.getField32().equalsIgnoreCase("New_Item")){
					sb.append("<div class=\"ribbon ribbon_6\">NEW ARRIVAL</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Out_of_Stock")){
					sb.append("<div class=\"ribbon ribbon_4\">OUT OF STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Back_in_Stock")){
					sb.append("<div class=\"ribbon ribbon_3\">BACK IN STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Load_Added")){
					sb.append("<div class=\"ribbon ribbon_5\">NEW LOAD ADDED</div>");
				}
				else{
					sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				}
				sb.append("</div>");
				}else{
					sb.append("<div class=\"product_bordered_box\">");
				}
				
				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");			
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");	
				
				sb.append("<div class=\"product_manifest_status\">");
				sb.append(product.getField14()!=null?product.getField14().replace(",", ""):"");
				sb.append("</div>");
				sb.append("<div class=\"price_percentage\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getField11());
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}

			if(product.getProductLayout().equalsIgnoreCase("050")){
				String productLink = null;
				//System.out.println("1239------------------------------------------------------------------------------>>>>>>");

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");		
				
				String saleTag = "";
				if(product.getSalesTag() != null){
					saleTag = product.getSalesTag().getTitle();
				}
				
				sb.append("<div class=\"product_bordered_box is_ribboned\">");
				sb.append("<div class=\"ribbon_wrapper\">");
				sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				sb.append("</div>");

				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}

				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
								
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
		
				Double discount = 0.0;
				if(product.getSalesTag() != null && product.getSalesTag().getDiscount() != 0.0){
					if(product.getSalesTag().isPercent()) {
						discount = product.getPrice1() * (100 - product.getSalesTag().getDiscount()) / 100;
					} else {
						discount = product.getPrice1() - product.getSalesTag().getDiscount();
					}
				}else{
					discount = product.getPrice1();
				}
				
				discount = Utilities.roundFactory(discount,2,BigDecimal.ROUND_HALF_UP);
	
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price discount\">");
				sb.append("<span class=\"new_price\">");sb.append(product.getField8());sb.append("</span>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"total_price discount\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append("<span class=\"strikethrough\">");
				sb.append("<span class=\"old_price\">");				
				sb.append(currencySymbol);
				sb.append(valueFormat(product.getPrice1()));
				sb.append("</span>");
				sb.append("</span>");
				sb.append("<span class=\"new_price\">");
				sb.append(currencySymbol);
				sb.append(valueFormat(discount));
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</span>");
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}

			if(product.getProductLayout().equalsIgnoreCase("051")){
				String productLink = null;
				//System.out.println("1336------------------------------------------------------------------------------>>>>>>");

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}

				sb.append("<div class=\"product_wrapper\">");				

				sb.append("<div class=\"product_bordered_box is_ribboned\">");
				sb.append("<div class=\"ribbon_wrapper\">");
				sb.append("<div class=\"ribbon ribbon_2\">ONE TIME DEAL</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}

				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");	
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
			
				sb.append("<div class=\"clearfix\">");
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("<div class=\"pull-right\">");
				sb.append("	<div class=\"product_manifest_status\">");
				sb.append(product.getField14()!=null?product.getField14().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price\">");
				sb.append(product.getField8());
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("<div class=\"total_price\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(currencySymbol);
				sb.append(valueFormat(product.getPrice1()));
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
			}		
			
			
			if(!product.getProductLayout().equalsIgnoreCase("047") && !product.getProductLayout().equalsIgnoreCase("048") && !product.getProductLayout().equalsIgnoreCase("049") && !product.getProductLayout().equalsIgnoreCase("050") && !product.getProductLayout().equalsIgnoreCase("051")){
				String productLink = null;
				//System.out.println("1417------------------------------------------------------------------------------>>>>>>");

				if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
					productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
				} else {
					productLink = "product.jhtm?id=" + product.getId();
				}
				//System.out.println("---------------------------------product------------------------------------->" + product.getId());

				sb.append("<div class=\"product_wrapper\">");
				
				if(product.getField32()!=null && !product.getField32().equalsIgnoreCase("")){
					sb.append("<div class=\"product_bordered_box is_ribboned\">");
					sb.append("<div class=\"ribbon_wrapper\">");
				if(product.getField32().equalsIgnoreCase("New_Item")){
					sb.append("<div class=\"ribbon ribbon_6\">NEW ARRIVAL</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Out_of_Stock")){
					sb.append("<div class=\"ribbon ribbon_4\">OUT OF STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Back_in_Stock")){
					sb.append("<div class=\"ribbon ribbon_3\">BACK IN STOCK</div>");
				}
				else if(product.getField32().equalsIgnoreCase("Load_Added")){
					sb.append("<div class=\"ribbon ribbon_5\">NEW LOAD ADDED</div>");
				}
				else{
					sb.append("<div class=\"ribbon ribbon_1\">ON SALE</div>");
				}
				sb.append("</div>");
				}else{
					sb.append("<div class=\"product_bordered_box\">");
				}
				
				sb.append("<div class=\"product_sku_wrapper\">");
				sb.append("<div class=\"product_sku\">");
				sb.append(product.getSku());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_image_wrapper\">");
				sb.append("<div class=\"product_image\">");
				sb.append("<a href=\""+productLink+"\">");
				
				if(product.getThumbnail()!=null){
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb/" + product.getThumbnail().getImageUrl() + "\">");
				}else{
					sb.append("<img class=\"img-responsive center-block\" src=\"" + request.getContextPath() + "/assets/Image/Product/thumb\">");
				}
				
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_name_wrapper\">");
				sb.append("<div class=\"product_name\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(product.getName());
				sb.append("</a>");
				sb.append("</div>");
				
				sb.append("<div class=\"product_condition\">");
				sb.append(product.getField24()!=null?product.getField24().replace(",", ""):"");
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"clearfix\">");
				
				sb.append("<div class=\"pull-left\">");
				sb.append("<div class=\"nb_units\">");
				sb.append(product.getField29());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("<div class=\"pull-right\">");
				sb.append("<div class=\"unit_price\">");
				sb.append(product.getField8());
				sb.append("</div>");
				sb.append("</div>");
				
				sb.append("</div>");
				
				sb.append("<div class=\"total_price\">");
				sb.append("<a href=\""+productLink+"\">");
				sb.append(currencySymbol);
				sb.append(valueFormat(product.getPrice1()));
				if (product.getField6() != null && product.getField6().length()>0){
					sb.append(" /");sb.append(product.getField6());
				}
				sb.append("</a>");
				sb.append("</div>");
				sb.append("</div>");		
			}
			
		}
		sb.append("</div>");
		sb.append("</div>");
		
		sb.append("</div>");
		return sb.toString();
	}
	

	
	private String valueFormat(Double val){
		DecimalFormat numFormat;
		String number;
		//System.out.println("-------------val----------" + val);
		if(val!=null){
			numFormat = new DecimalFormat("#,###.00");
			number = numFormat.format(val);
			//System.out.println("3. DecimalFormat with ,: " + number);
			return number;
		}else{
			return "";
		}
	}

	private void setLayout(UserSession userSession,Product product, HttpServletRequest request, HttpServletResponse response, Map<String, Object> model)
	{
		Layout layout = (Layout) request.getAttribute("layout");
		Layout productLayout = this.webJaguar.getSystemLayout("product", request.getHeader("host"), request);
		
		if(userSession!=null){
			if(layout!=null && productLayout!=null){ 
				if(productLayout.getHeadTag()!=null && productLayout.getHeadTag().length()>0){
					layout.setHeadTag(productLayout.getHeadTag());
				}	
				if(productLayout.getHeaderHtmlLogin()!=null && productLayout.getHeaderHtmlLogin().length()>0){
					layout.setHeaderHtmlLogin(productLayout.getHeaderHtmlLogin());
				}
				if(productLayout.getFooterHtmlLogin()!=null && productLayout.getFooterHtmlLogin().length()>0){
					layout.setFooterHtmlLogin(productLayout.getFooterHtmlLogin());
				}
			}
		}else{
			if(layout!=null && productLayout!=null){ 
				if(productLayout.getHeadTag()!=null && productLayout.getHeadTag().length()>0){
					layout.setHeadTag(productLayout.getHeadTag());
				}	
				if(productLayout.getHeaderHtml()!=null && productLayout.getHeaderHtml().length()>0){
					layout.setHeaderHtml(productLayout.getHeaderHtml());
				}
				if(productLayout.getFooterHtml()!=null && productLayout.getFooterHtml().length()>0){
					layout.setFooterHtml(productLayout.getFooterHtml());
				}
			}
		}
		if(layout!=null && product.getHeadTag()!=null && product.getHeadTag().length()>0){
			layout.setHeadTag(product.getHeadTag());
		}
		
		// A/B Testing
		if (siteConfig.get("PRODUCT1_DIRECTORY").getValue().length() > 0) {
			Integer view = ServletRequestUtils.getIntParameter(request, "view", 0);
			if (view >= 1 && view < 3) {
				productLayout = this.webJaguar.getSystemLayout("product"+view, request.getHeader("host"), request);
			}
		}
		int cid = ServletRequestUtils.getIntParameter(request, "cid", -1);
		if (cid > -1) {
			//layout = this.webJaguar.getLayoutByCategoryId(cid, request, response);
		}
		request.setAttribute("layout", layout);
		layout.setTopBarHtml(layout.getTopBarHtml().replace("#pageName#", product.getName()));
		
		if (product.isHideHeader())
			layout.setHeaderHtml(null);
		if (product.isHideTopBar())
			layout.setTopBarHtml(null);
		if (product.isHideLeftBar()) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} else {
			if (cid > -1) {
				Category category = this.webJaguar.getCategoryById(cid, "");
				if (category!=null) {
				
					String leftBarType = this.webJaguar.getLeftBarType(request, category.getLeftbarType());
					request.setAttribute("_leftBar", leftBarType);
					model.put("leftBarType",leftBarType );
					
					// LEFT BAR from category
					if (leftBarType.equals("3")) {
						model.put( "mainCategories", this.webJaguar.getMainsAndSubsCategoryLinks( request ) );
					} else {
						model.put( "mainCategories", this.webJaguar.getMainCategoryLinks( request, leftBarType ));
					}
					
					layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#parentId#", (category.getParent()!=null) ? category.getParent().toString() : "-1"));
					layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#cid#", category.getId().toString()));
				} else {
					if (siteConfig.get("LEFTBAR_DEFAULT_TYPE").getValue().equals("3")) {
						model.put( "mainCategories", this.webJaguar.getMainsAndSubsCategoryLinks( request ) );
					} else {
						model.put( "mainCategories", this.webJaguar.getMainCategoryLinks( request, null) );
					}
					layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#parentId#", "-1"));
					layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#cid#", "-1"));
				}
			} else {
				if (siteConfig.get("LEFTBAR_DEFAULT_TYPE").getValue().equals("3")) {
					model.put( "mainCategories", this.webJaguar.getMainsAndSubsCategoryLinks( request ) );
				} else {
					model.put( "mainCategories", this.webJaguar.getMainCategoryLinks( request, null) );
				}
				layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#parentId#", "-1"));
				layout.setLeftBarBottomHtml(layout.getLeftBarBottomHtml().replaceAll("#cid#", "-1"));
			}
			if (productLayout.getLeftBarTopHtml() != null && productLayout.getLeftBarTopHtml().trim().length() > 0) {
				layout.setLeftBarTopHtml(productLayout.getLeftBarTopHtml());				
			}
		}
		if (product.isHideRightBar()) {
			layout.setRightBarTopHtml("");
			layout.setRightBarBottomHtml("");
		} else {
			if (productLayout.getRightBarTopHtml() != null && productLayout.getRightBarTopHtml().trim().length() > 0) {
				layout.setRightBarTopHtml(productLayout.getRightBarTopHtml());				
			}
		}
		if (product.isHideFooter())
			layout.setFooterHtml(null);

		// bread crumbs
		List<Category> breadCrumb = null;
		if (!product.isHideBreadCrumbs()) {
			breadCrumb = this.webJaguar.getCategoryTree(ServletRequestUtils.getIntParameter(request, "cid", -1), true, request);
			model.put("breadCrumbs", breadCrumb);				
		}
		
		model.put("productLayout", productLayout);
		if (product.getHeadTag() != null && !product.getHeadTag().equals("")) {
			layout.setHeadTag(product.getHeadTag());
		} else if (productLayout.getHeadTag() != null && !productLayout.getHeadTag().equals("")) {
			// replaceAll does not work if a dollar sign happen to exist on string (?i)
			productLayout.setHeadTag(productLayout.getHeadTag().replace("#sku#", product.getSku()));
			productLayout.setHeadTag(productLayout.getHeadTag().replace("#id#", product.getId().toString()));
			productLayout.setHeadTag(productLayout.getHeadTag().replace("#name#", product.getName()));
			productLayout.setHeadTag(productLayout.getHeadTag().replace("#namewithoutquotes#", (product.getName() != null) ? product.getName().replaceAll("\"", "") : ""));
			productLayout.setHeadTag(productLayout.getHeadTag().replace("#msrp#", (product.getMsrp() != null) ? product.getMsrp().toString() : ""));
			productLayout.setHeadTag(productLayout.getHeadTag().replace("#price1#", (product.getPrice1() != null) ? product.getPrice1().toString() : ""));
			productLayout.setHeadTag(productLayout.getHeadTag().replace("#shortdesc#", (product.getShortDesc() != null) ? product.getShortDesc() : ""));
			productLayout.setHeadTag(productLayout.getHeadTag().replace("#keyword#", product.getKeywords()));
			productLayout.setHeadTag(productLayout.getHeadTag().replace("#weight#", product.getWeight().toString()));
			productLayout.setHeadTag(productLayout.getHeadTag().replace("#breadcrumb#", (breadCrumb != null) ? Constants.showBreadCrumb(breadCrumb) : ""));
			int gPRODUCT_FIELDS = (Integer) ((Map<String, Object>) request.getAttribute("gSiteConfig")).get("gPRODUCT_FIELDS");
			// product fields
			Class<Product> c = Product.class;
			Method m = null;
			Object arglist[] = null;
			for (int i=1; i<=gPRODUCT_FIELDS; i++) {
				try {
					m = c.getMethod("getField" + i);
					productLayout.setHeadTag(productLayout.getHeadTag().replace("#field"+i+"#", ((String) m.invoke(product, arglist) != null) ? (String) m.invoke(product, arglist) : ""));
				} catch (Exception e)  {
					// do nothing
				}
			}
			layout.setHeadTag(productLayout.getHeadTag());
		}
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		//tileshowroom tiles calculator
		if(((String) ((Map<String, Object>) request.getAttribute("gSiteConfig")).get("gSITE_DOMAIN")).equalsIgnoreCase("tileshowroom.com")) {
			if (productLayout.getFooterHtml() != null && !productLayout.getFooterHtml().equals("")) {
				// calculate tile calculation factor
				double length = 12;
				double width = 12;
				if(product.getField29() != null && !product.getField29().isEmpty()) {
					length = Double.parseDouble(product.getField29());
				}
				if(product.getField30() != null && !product.getField30().isEmpty()) {
					width = Double.parseDouble(product.getField30());
				}
				Double tilesFactor = ( 144 )  / ( length * width ) ;
				productLayout.setFooterHtml(productLayout.getFooterHtml().replaceAll("#tilesFactor#", tilesFactor.toString()));
			}
		}
	}
	
	private void setEtilize(String appId, Product product, List <ProductTab> tabList, Map<String, Object> model, boolean hackImages, boolean debug) {
    	try {
    		GetProduct getProduct = new GetProduct();
    		getProduct.setCatalog("na");
    		getProduct.setSiteId(0);
    		
    		// search by sku
    		Sku sku = new Sku();
    		sku.setNumber(product.getSku());
    		if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("SYNNEX")) {
    			sku.setType("Synnex");
    			if (product.getSku().startsWith("SYN-")) {
            		sku.setNumber(product.getSku().substring(4)); // remove leading SYN-    				    				
    			}
    		} else if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("INGRAM")) {
    			sku.setType("Ingram Micro USA");
    			if (product.getSku().startsWith("IM-")) {
    				sku.setNumber(product.getSku().substring(3)); // remove leading IM-
    			}
    		} else if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("TECHDATA")) {
    			sku.setType("Tech Data");
    			if (product.getSku().startsWith("TD-")) {
        			sku.setNumber(product.getSku().substring(3)); // remove leading TD-    				
    			}
    		} else if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("DSI")) {
    			sku.setType("DSI");
    			if (product.getSku().startsWith("DSI-")) {
        			sku.setNumber(product.getSku().substring(4)); // remove leading DSI-    				
    			}
    		} else if (product.getEtilizeId() != null) {
    			ProductId productId = new ProductId();
    			productId.setId(product.getEtilizeId());
    			getProduct.setProductId(productId);
    		} else if (product.getUpc() != null && product.getUpc().trim().length() > 0) {
    			sku.setType("UPC");
        		sku.setNumber(product.getUpc()); // use UPC  				
    		}
    		if (sku.getType() != null) {
        		getProduct.setSku(sku);
    		} else if (getProduct.getProductId() != null) {
    			// use etilize id
    		} else {
    			return;    			
    		}
    		
    		String wsdlLocation = "http://ws.spexlive.net/service/soap/catalog?appId=" + appId + "&wsdl";
    		Catalog service = new Catalog(new URL(wsdlLocation), new QName("http://com.etilize.spexlive", "catalog"));
    		CatalogServiceIntf port = service.getCatalogHttpPort();
    		
    		// SelectProductFields
    		SelectProductFields selectProductFields = new SelectProductFields();
    		selectProductFields.setDescriptions("all");
    		selectProductFields.setManufacturer("default");
    		selectProductFields.getResourceType().add("all"); 		
    		selectProductFields.getSkuType().add("all");
    		selectProductFields.setDataSheet("basic");
    		selectProductFields.setCategory("default");
    		getProduct.setSelectProductFields(selectProductFields);
    		
    		// subcatalog
    		SubcatalogFilter subcatalogFilter = new SubcatalogFilter();
    		subcatalogFilter.setContent("allitems");
    		getProduct.setSubcatalogFilter(subcatalogFilter);
    		
    		spexlive.etilize.com.Product eProduct = port.getProduct(getProduct);
    		
    		// descriptions
    		if (eProduct.getDescriptions() != null) {
        		for (Description description: eProduct.getDescriptions().getDescription()) {
        			if (description.getType() == 2 && (product.getName() == null || product.getName().trim().equals(""))) {
        				// change name if empty
        				product.setName(description.getContent());        				
        			} else if (description.getType() == 3) {
        				product.setShortDesc(description.getContent());        				
        			}
        		}    			
    		}
    		
    		// images
    		if (eProduct.getResources() != null) {
    			// images
    			Set<String> imageTypes = new HashSet<String>();
    			imageTypes.add("Top");
    			imageTypes.add("Left");
    			imageTypes.add("Right");
    			imageTypes.add("Front");
    			imageTypes.add("Rear");
    			imageTypes.add("Bottom");
    			imageTypes.add("Jack-Pack");

    			// pdfs
    			Set<String> pdfTypes = new HashSet<String>();
    			pdfTypes.add("Manufacturer-Brochure");
    			pdfTypes.add("Assembly-Instructions");
    			pdfTypes.add("User-Manual");
    			
    			ProductImage large = null;
    			ProductImage thumb = null;
        		for (Resource resource: eProduct.getResources().getResource()) {
        			if (resource.getType().equalsIgnoreCase("Large")) {
        				large = new ProductImage();
        				large.setImageUrl(resource.getUrl());
        			} else if (imageTypes.contains(resource.getType())) {
    					ProductImage pi = new ProductImage();
    					pi.setImageUrl(resource.getUrl());
    					product.addImage(pi);
        			} else if (pdfTypes.contains(resource.getType())) {
        				product.addPdf(resource.getType(), resource.getUrl());
        			} else if (resource.getType().equalsIgnoreCase("Thumbnail")) {
        				thumb = new ProductImage();
        				thumb.setImageUrl(resource.getUrl());
        			}
        		}
        		if (large != null) {
        			if (product.getImages() == null) product.setImages(new ArrayList<ProductImage>());
        			product.getImages().add(0, large);
        		} else if ((product.getImages() == null || product.getImages().isEmpty()) && thumb != null) {
        			product.addImage(thumb);
        		}
    		}
    		if (hackImages) {
        		// hack to their images
        		String[] types  = { "Top", "Left", "Right", "Front", "Rear" };
        		for (String type: types) {
    				ProductImage pi = new ProductImage();
    				// If image does not exist Etilize will show "Image Not Available or Brand Logo" by adding ?noimage=logo
    				pi.setImageUrl("http://content.etilize.com/" + type + "/" + eProduct.getProductId() + ".jpg?noimage=logo");
    				product.addImage(pi);        			
        		}    			
    		}
    		
    		// tab list
			if (tabList == null) {
				tabList = new ArrayList<ProductTab>();
			} else {
				Iterator<ProductTab> productTabIter = tabList.iterator();
				while (productTabIter.hasNext()) {
					ProductTab productTab = productTabIter.next();
					if (productTab.getTabName().equalsIgnoreCase("Overview")) {
						productTabIter.remove();
					} else if (productTab.getTabName().equalsIgnoreCase("Technical Specifications")) {
						productTabIter.remove();			
					}
				}
			}
    		if (eProduct.getDatasheet() != null) {

    			String marketingInfo = "";
    			StringBuffer sbuff = new StringBuffer("");
    			// Overview
    			sbuff.append("<table class='tabAttr'>");
				sbuff.append("<tr>");
				sbuff.append("<td class='tabAttrMain'>Main Features</td>");
				sbuff.append("</tr>");
				sbuff.append("<tr><td><ul>");
    			for (AttributeGroup attributeGroup: eProduct.getDatasheet().getAttributeGroup()) {
    				if (attributeGroup.getName().equalsIgnoreCase("General Information")) {
    					for (Attribute attribute: attributeGroup.getAttribute()) {
    						if (attribute.getName().equalsIgnoreCase("Marketing Information")) {
    							marketingInfo = attribute.getContent();
    						}
    					}
    				} else {
        				for (Attribute attribute: attributeGroup.getAttribute()) {
        					sbuff.append("<li>" + attribute.getName() + ": ");
        					sbuff.append(attribute.getContent().replaceAll("<ul>", "").replaceAll("</ul>", "").replaceAll("<li>", "").replaceAll("</li>", ", ") + "</li>");    					
        				}    					
    				}
    			}
				sbuff.append("</ul></td></tr>");
    			if (marketingInfo.length() > 0) {
    				sbuff.append("<tr>");
    				sbuff.append("<td class='tabAttrMarketing'>Marketing Information</td>");
    				sbuff.append("</tr>");
    				sbuff.append("<tr>");
    				sbuff.append("<td>" + marketingInfo + "</td>");
    				sbuff.append("</tr>");    				
    			}
    			sbuff.append("</table>");
    			tabList.add(new ProductTab("Overview", sbuff.toString(), 1));
    			
    			// Technical Specifications
    			sbuff = new StringBuffer("");
    			sbuff.append("<table class='tabAttr'>");
    			for (AttributeGroup attributeGroup: eProduct.getDatasheet().getAttributeGroup()) {
    				sbuff.append("<tr>");
					sbuff.append("<td colspan='2' class='tabAttr0'>" + attributeGroup.getName() + "</td>");
    				sbuff.append("</tr>");
    				for (Attribute attribute: attributeGroup.getAttribute()) {
    					sbuff.append("<tr>");
    					sbuff.append("<td class='tabAttr1 " + "ts_" + attribute.getName().replaceAll(" ", "_") + "'>" + attribute.getName() + ":</td>");
    					sbuff.append("<td class='tabAttr2 " + "ts_" + attribute.getName().replaceAll(" ", "_") + "'>" + attribute.getContent().replaceAll("<ul>", "").replaceAll("</ul>", "") + "</td>");    					
    					sbuff.append("</tr>");
    				}
    			}
    			sbuff.append("</table>");
    			tabList.add(new ProductTab("Technical Specifications", sbuff.toString(), 2));
    			
    			if (product.getPdfs() != null && !product.getPdfs().isEmpty()) {
        			sbuff = new StringBuffer("");
        			sbuff.append("<table class='tabAttr'>");
        			for (String type: product.getPdfs().keySet()) {
    					sbuff.append("<tr>");
    					sbuff.append("<td class='tabAttr1'>" + type + ":</td>");
    					sbuff.append("<td class='tabAttr2'><a href=\"" + 
    									product.getPdfs().get(type) + 
    									"\" target=\"_blank\"><img src=\"assets/Image/Layout/pdf.gif\" border=\"0\"></a></td>");    					
    					sbuff.append("</tr>");
        			}
        			sbuff.append("</table>");
        			tabList.add(new ProductTab("Resources", sbuff.toString(), 3));
    			}
    		}
     		
    		// category tree
    		GetCategoryTree getCategoryTree = new GetCategoryTree();
    		getCategoryTree.setCatalog("na");
    		getCategoryTree.setSiteId(0);
    		getCategoryTree.setChildLevels(-1);
    		getCategoryTree.setSubcatalogFilter(subcatalogFilter);
    		getCategoryTree.setCategoryId(eProduct.getCategory().getId());
    		spexlive.etilize.com.Category category = port.getCategoryTree(getCategoryTree);
    		List<spexlive.etilize.com.Category> etilizeBreadCrumbs = new ArrayList<spexlive.etilize.com.Category>();    		
    		etilizeBreadCrumbs.add(category);
    		while (category.getParent() != null) {
    			category = category.getParent();
    			if (category.getId() > 1) {
        			etilizeBreadCrumbs.add(0, category);    				
    			}
    		}
    		model.put("etilizeBreadCrumbs", etilizeBreadCrumbs);

    	} catch (Exception e) {
    		if (debug) {
        					
    		}
    	} 
	}
	
	private void getAsiAttributes(com.webjaguar.thirdparty.asi.asiAPI.Product asiProduct, Map<String, Object> model) {
		
		model.put("asiProduct", asiProduct); 
	    
		// get all attributes : Color, Material, Size and Shape 
		if(asiProduct.getAttributes() != null) {
	    	List<String> productColors = null;
			if(asiProduct.getAttributes().getColors() != null) {
				if(asiProduct.getAttributes().getColors().getValues() != null) {
					productColors = new ArrayList<String>();
					if(!asiProduct.getAttributes().getColors().getValues().getProductAttributeValue().isEmpty()) {
						for(ProductAttributeValue attributeValue : asiProduct.getAttributes().getColors().getValues().getProductAttributeValue()) {
							productColors.add(attributeValue.getName());
						}
					} else {
						productColors.addAll(asiProduct.getAttributes().getColors().getValues().getString());
					}
					model.put("productColors", productColors);
				}
			}
		
			List<String> materials = null;
			if(asiProduct.getAttributes().getMaterials() != null) {
				if(asiProduct.getAttributes().getMaterials().getValues() != null) {
					materials = new ArrayList<String>();
					if(!asiProduct.getAttributes().getMaterials().getValues().getProductAttributeValue().isEmpty()) {
						for(ProductAttributeValue attributeValue : asiProduct.getAttributes().getMaterials().getValues().getProductAttributeValue()) {
							materials.add(attributeValue.getName());
						}
					} else {
						materials.addAll(asiProduct.getAttributes().getMaterials().getValues().getString());
					}
					model.put("materials", materials);
				}
			}
			
			List<String> sizes = null;
			if(asiProduct.getAttributes().getSizes() != null) {
				if(asiProduct.getAttributes().getSizes().getValues() != null) {
					sizes = new ArrayList<String>();
					if(!asiProduct.getAttributes().getSizes().getValues().getProductAttributeValue().isEmpty()) {
						for(ProductAttributeValue attributeValue : asiProduct.getAttributes().getSizes().getValues().getProductAttributeValue()) {
							sizes.add(attributeValue.getName());
						}
					} else {
						sizes.addAll(asiProduct.getAttributes().getSizes().getValues().getString());
					}
					model.put("sizes", sizes);
				}
			}
			
			List<String> shapes = null;
			if(asiProduct.getAttributes().getShapes() != null) {
				if(asiProduct.getAttributes().getShapes().getValues() != null) {
					shapes = new ArrayList<String>();
					if(!asiProduct.getAttributes().getShapes().getValues().getProductAttributeValue().isEmpty()) {
						for(ProductAttributeValue attributeValue : asiProduct.getAttributes().getShapes().getValues().getProductAttributeValue()) {
							shapes.add(attributeValue.getName());
						}
					} else {
						shapes.addAll(asiProduct.getAttributes().getShapes().getValues().getString());
					}
					model.put("shapes", shapes);
				}
			}
		}
		
		if(asiProduct.getImprinting() != null) {
			if(asiProduct.getImprinting().getMethods() != null && asiProduct.getImprinting().getMethods().getValues() != null) {
				if(asiProduct.getImprinting().getMethods().getValues().getProductOption() != null) {
					List<String> imprintMethods = new ArrayList<String>();
					for(com.webjaguar.thirdparty.asi.asiAPI.ProductOption productOption : asiProduct.getImprinting().getMethods().getValues().getProductOption()) {
						imprintMethods.add(productOption.getName());
					}
					model.put("imprintMethods", imprintMethods); 
				    
				}	
			}
		}
		model.put("priceIncludes", asiProduct.getPriceIncludes()); 
	    
	    if(asiProduct.getPackaging() != null && asiProduct.getPackaging().getOption() != null){
	    	StringBuffer sbPackaging = new StringBuffer(); 
	    	for(ProductOptionSet productOptionSet : asiProduct.getPackaging().getOption()){
	    		if(productOptionSet.getDescription() != null) {
	    			sbPackaging.append(productOptionSet.getDescription());
	    		}
	    		if(productOptionSet.getValues() != null && productOptionSet.getValues().getProductOptionValue() != null) {
	    			for(ProductAttributeValue productOptionValue : productOptionSet.getValues().getProductOptionValue()) {
	    				if(productOptionValue.getName() != null) {
	    					sbPackaging.append(productOptionValue.getName()+", ");
	    			    }
	    			}
	    			//	sbPackaging.delete(sbPackaging.length() - 2, sbPackaging.length());
	    		}
	    		if(productOptionSet.getGroups() != null && productOptionSet.getGroups().getGroup() != null){
	    			for(com.webjaguar.thirdparty.asi.asiAPI.ProductOption iProductOption : productOptionSet.getGroups().getGroup()){
	    				sbPackaging.append(iProductOption.getName()+", ");
	    			}
	    			sbPackaging.delete(sbPackaging.length() - 2, sbPackaging.length());
	    		}
	    	}
			model.put("packaging", sbPackaging.toString()); 
		}
	    if(asiProduct.getProductionTime() != null && asiProduct.getProductionTime().getValue() != null){
	    	model.put("productionTime", asiProduct.getProductionTime().getValue().get(0).getName()); 
	    }
	    model.put("rushService", asiProduct.isHasRushService()); 
	    //imprint options
	    List<String> imprintOptions = null;
		if(asiProduct.getImprinting() != null && asiProduct.getImprinting().getMethods() != null) {
			if(asiProduct.getImprinting().getMethods().getValues() != null ) {
				if(asiProduct.getImprinting().getMethods().getValues().getProductOption() != null) {
					if(asiProduct.getImprinting().getMethods().getValues().getProductOption() != null) {
						if(imprintOptions == null) {
							imprintOptions = new ArrayList<String>();
						}
						for(com.webjaguar.thirdparty.asi.asiAPI.ProductOption productOption : asiProduct.getImprinting().getMethods().getValues().getProductOption()) {
							imprintOptions.add(productOption.getName());
						}
					}
				}
				
				if(asiProduct.getImprinting().getMethods().getValues().getString() != null) {
					if(imprintOptions == null) {
						imprintOptions = new ArrayList<String>();
					}
					for(String value : asiProduct.getImprinting().getMethods().getValues().getString()) {
						if(!imprintOptions.contains(value)) {
							imprintOptions.add(value);
						}
					}
				}
			}
		}
		model.put("imprintOptions", imprintOptions);
		//shipping
		if(asiProduct.getShipping() != null && asiProduct.getShipping().getFOBPoints() != null) {
			if(asiProduct.getShipping().getFOBPoints().getValues() != null && asiProduct.getShipping().getFOBPoints().getValues().getString() != null) {
			    List<String> shippingFobPoints = new ArrayList<String>();
			    shippingFobPoints.addAll(asiProduct.getShipping().getFOBPoints().getValues().getString());
			    if(!shippingFobPoints.isEmpty()) {
			    	model.put("shippingFobPoints", shippingFobPoints);
				}
			}
		}
	}
	private Map<String, List<String>> setAttributes(String optionName, CriteriaSet criteria, Map<String, List<String>> optionValues) {
		List<String> optionList = new ArrayList<String>();
		
		if(criteria.getOptions() != null && criteria.getOptions().getOption() != null) {
			for(Option option: criteria.getOptions().getOption()){
				if(optionValues.get(optionName) != null) {
					optionValues.get(optionName).add(option.getValue());
				} else {
					optionList = new ArrayList<String>();
					optionList.add(option.getValue());
					optionValues.put(optionName, optionList);
				}
			}
		}
		return optionValues;
	}
	
	private void setAsiInfo(asiProduct.Product product, Map<String, Object> model) {
		
		List<String> optionList = new ArrayList<String>();
		Map<String, List<String>> productAttributes = new HashMap<String, List<String>>();
		Map<String, List<String>> imprintInfo = new HashMap<String, List<String>>();
		Map<String, String> additionalCharges = new HashMap<String, String>();
		
		if(product.getCategories() != null && product.getCategories().getCategory() != null) {
			for(asiProduct.Categories.Category category : product.getCategories().getCategory()) {
				if(productAttributes.get("Category") != null) {
					productAttributes.get("Category").add(category.getName());
				} else {
					optionList = new ArrayList<String>();
					optionList.add(category.getName());
					productAttributes.put("Category", optionList);
				}
			}
		}
		
		if(product.getProductOptions() != null && product.getProductOptions().getCriteriaSet() != null) {
			for(CriteriaSet criteria : product.getProductOptions().getCriteriaSet()) {
				if(criteria.getName().equalsIgnoreCase("Material")) {
					this.setAttributes("Material", criteria, productAttributes);
				}
				if(criteria.getName().equalsIgnoreCase("ProductColor") || criteria.getName().equalsIgnoreCase("Color")) {
					this.setAttributes("Color", criteria, productAttributes);
				}
				if(criteria.getName().equalsIgnoreCase("Size")) {
					this.setAttributes("Size", criteria, productAttributes);
				}
			}
		}
		
		
		if(product.getImprintOptions() != null && product.getImprintOptions().getCriteriaSet() != null) {
			for(CriteriaSet criteria : product.getImprintOptions().getCriteriaSet()) { 
				if(criteria.getName().equalsIgnoreCase("ImprintColor")) {
					this.setAttributes("Imprint Color", criteria, imprintInfo);
				}
				if(criteria.getName().equalsIgnoreCase("ImprintMethod")) {
					this.setAttributes("Imprint Method", criteria, imprintInfo);
				}
				if(criteria.getName().equalsIgnoreCase("ImprintSize")) {
					this.setAttributes("Imprint Size", criteria, imprintInfo);
				}
			}
		}
		model.put("productAttributes", productAttributes);
		model.put("imprintInfo", imprintInfo);
		

		for(PriceGrid priceGrid : product.getPricing().getPriceGrid()) {
			if( !priceGrid.isBase() && priceGrid.getDependencies() != null && priceGrid.getDependencies().getDependency() != null) {
				for(Dependency dependency : priceGrid.getDependencies().getDependency()) {
					if(dependency.getValue() != null && ( dependency.getValue().equalsIgnoreCase("Set Up Charge") || dependency.getValue().equalsIgnoreCase("Running Charge"))) {
						if(priceGrid.getPrice() != null && priceGrid.getPrice().get(0) != null) {
							additionalCharges.put(dependency.getValue() ,  priceGrid.getCurrency()+" "+priceGrid.getPrice().get(0).getValue());
						}
					}
				}
			}
		}
		model.put("additionalCharges", additionalCharges);
	}
}