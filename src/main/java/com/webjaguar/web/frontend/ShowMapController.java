/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.22.2009
 */

package com.webjaguar.web.frontend;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

public class ShowMapController implements Controller {


	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {	
		
		Map<String, Object> model = new HashMap<String, Object>();
		String address = ServletRequestUtils.getStringParameter( request, "add", "" );
		model.put( "address", address );
		
		return new ModelAndView("frontend/location/map", "model", model);    	
    }
}