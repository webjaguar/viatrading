/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.shoppingCart;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.buySafe.*;
import com.webjaguar.listener.SessionListener;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Brand;
import com.webjaguar.model.BudgetProduct;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.Promo;
import com.webjaguar.model.UserSession;
import com.webjaguar.thirdparty.cXml.cXmlAPI.BuyerCookie;
import com.webjaguar.thirdparty.cXml.cXmlAPI.CXML;
import com.webjaguar.thirdparty.cXml.cXmlAPI.Classification;
import com.webjaguar.thirdparty.cXml.cXmlAPI.Description;
import com.webjaguar.thirdparty.cXml.cXmlAPI.From;
import com.webjaguar.thirdparty.cXml.cXmlAPI.Header;
import com.webjaguar.thirdparty.cXml.cXmlAPI.ItemDetail;
import com.webjaguar.thirdparty.cXml.cXmlAPI.ItemID;
import com.webjaguar.thirdparty.cXml.cXmlAPI.ItemIn;
import com.webjaguar.thirdparty.cXml.cXmlAPI.Message;
import com.webjaguar.thirdparty.cXml.cXmlAPI.Money;
import com.webjaguar.thirdparty.cXml.cXmlAPI.PunchOutOrderMessage;
import com.webjaguar.thirdparty.cXml.cXmlAPI.PunchOutOrderMessageHeader;
import com.webjaguar.thirdparty.cXml.cXmlAPI.Sender;
import com.webjaguar.thirdparty.cXml.cXmlAPI.SupplierPartAuxiliaryID;
import com.webjaguar.thirdparty.cXml.cXmlAPI.To;
import com.webjaguar.thirdparty.cXml.cXmlAPI.Total;
import com.webjaguar.thirdparty.cXml.cXmlAPI.UnitPrice;
import com.webjaguar.web.domain.Utilities;

public class ViewCartController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
    	Map<String, Object> model = new HashMap<String, Object>();
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	UserSession userSession = (UserSession) request.getAttribute( "userSession" );
    	Cart cart = new Cart();
    	
    	String promocode = ServletRequestUtils.getStringParameter(request, "promocode", "");
    	request.getSession().setAttribute("pro2", promocode);
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	
    	if(userSession!=null){
    		userSession.setPromocode(promocode);
    		
    	}
    	
    	
		if (userSession == null && promocode!=null && promocode.length()>0) {
			
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("billingShipping.jhtm")));
		}
    	
		// check if anything was removed
		String remove[] = request.getParameterValues("remove");
		File customImageDir = null;
		if (remove != null) {
			customImageDir = new File(getServletContext().getRealPath("/framer/pictureframer/images/FRAMED"));
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
				if (prop.get("site.root") != null) {
					customImageDir = new File((String) prop.get("site.root") + "/framer/pictureframer/images/FRAMED");
				}
			} catch (Exception e) {}			
		}
    	
		Integer userId = null;
		if (userSession != null) { // cart stored in database
			userId = userSession.getUserid();		
			// check if update button was clicked
			if (request.getParameter("_update") != null) {
				String index[] = request.getParameterValues("index");
				if (index != null) {
					for (int i=0; i<index.length; i++) {
						try {
							int quantity = Integer.parseInt(request.getParameter("qty_" + index[i]));
							Double priceByUser = ServletRequestUtils.getDoubleParameter( request, "_price_by_user_" + index[i] );
							CartItem cartItem = new CartItem(quantity, priceByUser, userId, Integer.parseInt(index[i]));
							if (request.getParameter("customLine_qty_" + index[i]) != null) {
								// custom lines
								quantity = updateCustomLines(request, cartItem, index[i]);
								cartItem.setQuantity(quantity);
							}
							if (quantity > 0) {
								this.webJaguar.updateCartItem( cartItem );
								cartItem.setInventoryMessage( null );
							}
						} catch (NumberFormatException e) {
							// ignore on purpose 
						} catch (Exception e) { e.printStackTrace(); }						
					}					
				}

				if (remove != null) {
					for (int i=0; i<remove.length; i++) {
						try {
							String customImageUrl = this.webJaguar.removeCartItem(Integer.parseInt(remove[i]), userId);
							if (customImageUrl != null && customImageUrl.trim().length() > 0) {
								File customeImageFile = new File(customImageDir, customImageUrl);
								customeImageFile.delete();
							}
						} catch (Exception e) {}
					}
				}
			}
			cart = this.webJaguar.getUserCart(userId, null);
			cart.setUserId( userId );
			model.put("email", this.webJaguar.getCustomerById(userId).getUsername());
		} else { // cart stored in session
			Cart sessionCart = (Cart) WebUtils.getSessionAttribute(request, "sessionCart");
			if (sessionCart != null) cart = sessionCart;
			// check if update button was clicked
			if (request.getParameter("_update") != null) {
				Iterator<CartItem> iter = cart.getCartItems().iterator();
				int index = 0;
				while (iter.hasNext()) {
					CartItem cartItem = iter.next();
					try {
						int quantity = Integer.parseInt(request.getParameter("qty_" + index));
						if (request.getParameter("customLine_qty_" + index) != null) {
							// custom lines
							quantity = updateCustomLines(request, cartItem, String.valueOf(index));
						}
						if (quantity > 0) {
							cartItem.setQuantity(quantity);
						}
						Double priceByUser = ServletRequestUtils.getDoubleParameter( request, "_price_by_user_" + index );
						cartItem.setVariableUnitPrice( priceByUser );
						index++;
					} catch (NumberFormatException e) {
						// ignore on purpose 
					}
				}				

				if (remove != null) {
					for (int i=remove.length-1; i>=0; i--) {
						try {
							String customImageUrl = null;
							if (cart.getCartItems().get(i) != null
									&& cart.getCartItems().get(i).getCustomImageUrl() != null
									&& cart.getCartItems().get(i).getCustomImageUrl().trim().length() > 0) {
								customImageUrl = cart.getCartItems().get(i).getCustomImageUrl();
							}
							cart.removeCartItem(Integer.parseInt(remove[i]));
							// delete custom image
							if (customImageUrl != null) {
								File customeImageFile = new File(customImageDir, customImageUrl);
								customeImageFile.delete();
							}
						} catch (Exception e) {}
					}
				}
			}
			model.put("sessionId", request.getSession().getId());					
		}
		
		this.updateAssignedSkus(cart, model);
		String message = this.webJaguar.updateCart(cart, (Map<String, Object>) request.getAttribute("gSiteConfig"), (Map<String, Configuration>) request.getAttribute("siteConfig"), RequestContextUtils.getLocale(request).getLanguage(), request.getHeader("host"), model);
		this.updateMasterSkus(cart, model, request);
		
		if (userSession != null) {
			request.getSession().setAttribute("userCart", cart);
			if ((Boolean) gSiteConfig.get("gBUDGET_BRAND")) {
				Map<Brand, Double> brandsMap = new HashMap<Brand, Double>();
				if (this.webJaguar.getSubTotalPerBrand(cart, brandsMap)) {
					model.put("isOverBudget", true);
				}
				if (brandsMap.size() > 0) {
					model.put("brandsMap", brandsMap);					
				}
			}
			if ((Boolean) gSiteConfig.get("gBUDGET_PRODUCT")) {
				Map<String, BudgetProduct> budgetProduct = new HashMap<String, BudgetProduct>();
				if (this.webJaguar.getBudgetByCart(cart, budgetProduct)) {
					model.put("isOverBudget", true);
				}
				model.put("budgetProduct", budgetProduct);
			}
			
			// save cart total per line item to filter customer on admin
			Double tempCartTotal = cart.getSubTotal();
			for(CartItem cartItem: cart.getCartItems()) {
				if(tempCartTotal != null && cartItem.getTempCartTotal() != null && !tempCartTotal.equals(cartItem.getTempCartTotal())) {
					cartItem.setTempCartTotal(tempCartTotal);
					this.webJaguar.updateCartItem(cartItem);
				}
			}
		}
		
		// promo code
		if(siteConfig.get("PROMO_ON_SHOPPING_CART").getValue().equalsIgnoreCase("TRUE")) {
			this.validatePromo(request, cart, gSiteConfig, userSession);
		}  
			
		// Recommended List
		List <Product> recommendedList = new  ArrayList<Product>();
		List <String> masterSku = new ArrayList<String>();
		Set<String> recommendedSku = new TreeSet<String>();
		for(CartItem cartItem: cart.getCartItems()) {
			masterSku.add(cartItem.getProduct().getSku());
		}
		if ((Boolean) gSiteConfig.get( "gRECOMMENDED_LIST" ) && ( siteConfig.get( "SHOPPING_CART_RECOMMENDED_LIST" ).getValue().equals( "true" ) )) {
			for(CartItem cartItem: cart.getCartItems()) {
				if (cartItem.getProduct()!=null && cartItem.getProduct().getRecommendedList() != null && !cartItem.getProduct().getRecommendedList().isEmpty() ) {
					String[] skus = cartItem.getProduct().getRecommendedList().split( "[,\n]" ); 
					for ( int x = 0; x < skus.length; x++ ) {
						String sku = skus[x].trim();
						if ( !("".equals(sku)) && !masterSku.contains(sku)) {
							recommendedSku.add(sku);
						}
					}	
				}
			}
		}	
		if(siteConfig.get( "UPSELL_RECOMMENDED_LIST" ).getValue().equals( "true" ) && !siteConfig.get( "UPSELL_DEFAULT_RECOMMENDED_LIST" ).getValue().equals( " " ) ){
			String[] upsellSkus = siteConfig.get( "UPSELL_DEFAULT_RECOMMENDED_LIST" ).getValue().split( "[,\n]" );
			String upsellSku = "";
			for ( int x = 0; x < upsellSkus.length; x++ ) {
				
				// check if max number of skus to be displayed is set
				if(x == upsellSkus.length-1 && upsellSkus[x].contains("#")) {
					String[] skuWithLimit = upsellSkus[x].split("#");
					upsellSku = skuWithLimit[0].trim();
					try{
						model.put("limit", Integer.parseInt(skuWithLimit[1].trim()));
					} catch(Exception e){}
				} else {
					upsellSku = upsellSkus[x].trim();
				}
				if( !("".equals(upsellSku)) && !masterSku.contains(upsellSku) ){
					recommendedSku.add(upsellSku);
				}
			}
		}
		Iterator<String> iter = recommendedSku.iterator();
		while(iter.hasNext()){
			Product recommendedProduct = this.webJaguar.getProductById( this.webJaguar.getProductIdBySku( iter.next() ), request );
			if(recommendedProduct != null){
				recommendedList.add(recommendedProduct);	
			}	
		}
		if (!recommendedList.isEmpty()) {	
			model.put( "recommendedList", recommendedList );
			if(model.get("limit") == null) {
				model.put("limit", recommendedList.size());
			}
		}
		
		// Manufacturer
		if ((Boolean) gSiteConfig.get("gMANUFACTURER")) {
			if (!this.webJaguar.checkManufacturerMinOrder(cart, request)) {
				model.put("isUnderManufactureMin", true);
			}
		} 

		Map<String, Object> params = new HashMap<String, Object>();
		// check Buy safe
		if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") && cart.getCartItems().size() > 0) {
			cart.setWantsBond(ServletRequestUtils.getBooleanParameter(request, "WantsBondField"));
			if(request.getSession().getAttribute("buySafeCartId") == null){
				request.getSession().setAttribute("buySafeCartId", UUID.randomUUID().toString().toUpperCase().replace("-", "").substring(0, 4)+new Date().getTime());
			}
			
			ShoppingCartAddUpdateRS addUpdateRS = null;
			
			try{
				addUpdateRS = this.webJaguar.addUpdateShoppingCartRequest(cart, null, request, userId, mailSender);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(addUpdateRS != null && addUpdateRS.getBondCostDisplayText() != null && !addUpdateRS.getBondCostDisplayText().equals("")){
				cart.setWantsBond(true);
			} else {
				cart.setWantsBond(false);
			}
			model.put("buySafeResponse", addUpdateRS);
		}
		// check if checkout button was pressed
		if (request.getParameter("_checkout.x") != null || request.getParameter("_checkout1.x") != null || request.getParameter("_checkout1.jhtm") != null) {

			request.setAttribute("userSession", userSession);
			
			String manufacturerName = ServletRequestUtils.getStringParameter(request, "manufacturerName", "");
			if (cart.isContinueCart() || manufacturerName.trim().length() > 0) {
				if (siteConfig.get("SHOPATRON").getValue().length() > 0 && !siteConfig.get("SHOPATRON_REQUIRE_LOGIN").getValue().equals("true") && userSession == null
						&& manufacturerName.trim().equals("")) { // manufacturerName changes cart contents
					return shopatronRPC(request, gSiteConfig, siteConfig, cart);
				}
				request.getSession().setAttribute("manufacturerName", manufacturerName);
				request.getSession().setAttribute("userSession", userSession);
				
				Random randomGenerator = new Random();
			    int randomInt = randomGenerator.nextInt(100);
			    if (randomInt < Integer.parseInt(siteConfig.get("CHECKOUT2_VISIBILITY").getValue())) {
			    	return new ModelAndView(new RedirectView("checkout2.jhtm"), params);
			    } else if(request.getParameter("_checkout1.x") != null) {
			    	return new ModelAndView(new RedirectView("checkout1.jhtm")); 
			    } else {
			    	return new ModelAndView(new RedirectView("checkout.jhtm"), params);
			    }
			}
		}	
		
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		if (message != null ) model.put("message", message); 
		if (request.getSession().getAttribute( "continueShoppingUrl" ) != null) model.put("continueShoppingUrl", request.getSession().getAttribute( "continueShoppingUrl" )); 

		model.put("cart", cart);
		Layout layout = (Layout) request.getAttribute( "layout" );
		layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Cart"));
		Layout cartLayout = this.webJaguar.getSystemLayout("cart", request.getHeader("host"), request);
		// rightBar
		if (cartLayout.getRightBarTopHtml() != null && cartLayout.getRightBarTopHtml().trim().length() > 0) {
			layout.setRightBarTopHtml(cartLayout.getRightBarTopHtml());				
		 }
		// leftBar
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} else {
			if (cartLayout.getLeftBarTopHtml() != null && cartLayout.getLeftBarTopHtml().trim().length() > 0) {
				layout.setLeftBarTopHtml(cartLayout.getLeftBarTopHtml());				
			 }
		}
		updateCartQuantity(userSession, request, layout , cart);
		model.put("cartLayout", cartLayout);
		model.put("layout", layout);
		
		// CXML: encoded cart for order request
		if(request.getSession().getAttribute("jsId") != null && SessionListener.getHttpSessionIdMap().get(request.getSession().getAttribute("jsId")) != null && SessionListener.getHttpSessionIdMap().get(request.getSession().getAttribute("jsId")).getAttribute("browserFormPost") != null) {
			model.put("browserFormPost", (String) SessionListener.getHttpSessionIdMap().get(request.getSession().getAttribute("jsId")).getAttribute("browserFormPost"));
			model.put("cXmlEncodedCart", this.encodedCart(SessionListener.getHttpSessionIdMap().get(request.getSession().getAttribute("jsId")), cart, siteConfig));
		}
		
		if((Boolean) gSiteConfig.get("gSHOPPING_CART_1") && siteConfig.get("CHECKOUT1_DIRECTORY").getValue().length() > 0 ) {
			return new ModelAndView(siteConfig.get("CHECKOUT1_DIRECTORY").getValue() + "/checkout1/cart", "model", model);
		} else if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/cart", "model", model);
		} else {
			return new ModelAndView("frontend/cart", "model", model);
		}
	}
    
	public void updateCartQuantity(UserSession userSession, HttpServletRequest request, Layout layout, Cart cart) {
		Integer qnt = new Integer(0);
		if(cart!=null){
			for(CartItem cartItem : cart.getCartItems()) {
				Product p = null;
				if(cartItem!=null && cartItem.getProduct()!=null){
					p = this.webJaguar.getProductById(cartItem.getProduct().getId(), 0, false, null);
					if(p!=null && p.isActive()){
						qnt = qnt + cartItem.getQuantity();
					}
				}
			}
		}
		if(layout!=null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#cartItemQuantity#", qnt.toString()));
		}
	}
    
    private String encodedCart(HttpSession httpSession, Cart cart, Map<String, Configuration> siteConfig) {
    	
    	CXML responseCXml = new CXML();
		responseCXml.setPayloadID(""+new Date().getTime());
		java.text.Format formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss-hh:mm");
		
		responseCXml.setTimestamp(""+formatter.format(new Date()));
		responseCXml.setVersion("1.2.014");
		responseCXml.setXmlLang("en-US");
		
		Header requestHeader = (Header) httpSession.getAttribute("cXmlHeader");
	
		From from = new From();
		from.getCredential().addAll(requestHeader.getTo().getCredential());
		
		Sender sender = new Sender();
		sender.getCredential().addAll(requestHeader.getTo().getCredential());
		
		To to = new To(); 
		to.getCredential().addAll(requestHeader.getFrom().getCredential());
		
		Header header = new Header();
		header.setFrom(from);
		header.setTo(to);
		header.setSender(sender);
		responseCXml.getHeaderOrMessageOrRequestOrResponse().add(header);
		
		Message message = new Message();
		
		PunchOutOrderMessage punchOutOrderMessage = new PunchOutOrderMessage();
		message.getPunchOutOrderMessageOrProviderDoneMessageOrSubscriptionChangeMessageOrDataAvailableMessageOrSupplierChangeMessageOrOrganizationChangeMessage().add(punchOutOrderMessage);
		
		responseCXml.getHeaderOrMessageOrRequestOrResponse().add(message);
		BuyerCookie buyerCookie = new BuyerCookie();
		buyerCookie.getContent().add(httpSession.getAttribute("buyerCookie"));
		punchOutOrderMessage.setBuyerCookie(buyerCookie);
		
		PunchOutOrderMessageHeader punchOutMsgHdr = new PunchOutOrderMessageHeader();
		punchOutMsgHdr.setOperationAllowed("Edit");
		punchOutOrderMessage.setPunchOutOrderMessageHeader(punchOutMsgHdr);
		
		Money money = new Money();
		money.setvalue(new Double(cart.getSubTotal()).toString());
		
		Total total = new Total();
		total.setMoney(money);
		
		punchOutMsgHdr.setTotal(total);
		
		for(CartItem  cartItem : cart.getCartItems()) {
			ItemIn itemIn = new ItemIn();
			itemIn.setQuantity(new Integer(cartItem.getQuantity()).toString());
			
			ItemID itemId = new ItemID();
			itemId.setSupplierPartID(cartItem.getProduct().getSku());
			
			SupplierPartAuxiliaryID supplierauxId = new SupplierPartAuxiliaryID();
			supplierauxId.getContent().add(cartItem.getProduct().getId());
			itemId.setSupplierPartAuxiliaryID(supplierauxId);
			itemIn.setItemID(itemId);
			
			ItemDetail itemDetail = new ItemDetail();
			
			UnitPrice unitPrice = new UnitPrice();
			Money mny = new Money();
			mny.setvalue(new Double(cartItem.getUnitPrice()).toString());
			mny.setCurrency(siteConfig.get( "CURRENCY" ).getValue());
			unitPrice.setMoney(mny);
			itemDetail.setUnitPrice(unitPrice);
			
			Description desc = new Description();
			desc.setvalue(cartItem.getProduct().getName());
			itemDetail.getDescription().add(desc);
			itemIn.setItemDetail(itemDetail);
			
			//Unit of Measure (EA for each item)
			itemDetail.setUnitOfMeasure("EA");
			
			if(siteConfig.get("CXML_UNSPSC").getValue() != null){
				Classification classification = new Classification();
				classification.setDomain("UNSPSC");
				classification.setvalue(siteConfig.get("CXML_UNSPSC").getValue());
				itemDetail.getClassification().add(classification);
			}
			punchOutOrderMessage.getItemIn().add(itemIn);
		}
		
		JAXBContext context;
	    OutputStream oStream = new ByteArrayOutputStream();
		
		try {
			context = JAXBContext.newInstance(CXML.class);
			context.createMarshaller().marshal(responseCXml, oStream);
			
			String encodedCart = URLEncoder.encode(oStream.toString(), "UTF-8");
			
			return Utilities.replaceString(new StringBuffer(encodedCart), "+", "%20", false).toString();
			
		} catch (Exception e) {
		    e.printStackTrace();
			return null;
		}
    }
    private int updateCustomLines(HttpServletRequest request, CartItem cartItem, String index) {
    	cartItem.setCustomLines(null);
    	String quantities[] = ServletRequestUtils.getStringParameters(request, "customLine_qty_" + index);
    	int numCustomLines = ServletRequestUtils.getStringParameters(request, "customLine_" + index + "_0").length;
    	for (int key=0; key<quantities.length; key++) {
    		try {
        		int quantity = Integer.parseInt(quantities[key]);
        		if (quantity > 0) {
        			StringBuffer sb = new StringBuffer();
        			String lines[] = ServletRequestUtils.getStringParameters(request, "customLine_" + index + "_" + key);
        			for (int line = 0; line<lines.length; line++) {
        				sb.append(lines[line].trim() + " \n");
        			}
        			cartItem.addCustomLine(sb.toString(), quantity);
        		}
    		} catch (NumberFormatException e) {
    			// do nothing
    		}
    	}
    	int total = 0;
    	if (cartItem.getCustomLines() != null) {
        	for (Integer qty: cartItem.getCustomLines().values()) {
        		total = total + qty;
        	}
    	} else {
    		StringBuffer sb = new StringBuffer();
    		for (int line = 0; line<numCustomLines; line++) {
				sb.append(" \n");
			}
			cartItem.addCustomLine(sb.toString(), 1);
    		total = 1;
    	}
    	return total;
    }
    
	private ModelAndView shopatronRPC(HttpServletRequest request, Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig, Cart cart) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		ModelAndView modelAndView = new ModelAndView("frontend/checkout/shopatron");
		modelAndView.addObject("model", model);
		
		List<Object> params = new ArrayList<Object>();
		params.add(siteConfig.get("SHOPATRON").getValue());		// mfg_id.cat_id
		params.add(cart.getNumberOfItems());					// num_items

		Map<String, Map<String, Object>> order_data = new HashMap<String, Map<String, Object>>();
		int line = 0;
		for (CartItem lineItem: cart.getCartItems()) {
			Map<String, Object> line_item = new HashMap<String, Object>();
			line_item.put("product_id", lineItem.getProduct().getSku());
			line_item.put("name", lineItem.getProduct().getName());
			line_item.put("price", lineItem.getUnitPrice());
			line_item.put("quantity", lineItem.getQuantity());
			if (lineItem.getProduct().getWeight() != null) {
				line_item.put("weight", lineItem.getProduct().getWeight());				
			}
			
			order_data.put("item_" + ++line, line_item);			
		}
		params.add(order_data);

		try {
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
			config.setServerURL(new URL("https://xml.shopatron.com/xmlServer.php"));
			XmlRpcClient client = new XmlRpcClient();
			client.setConfig(config);
			
			model.put("order_id", client.execute("examples.loadOrder", params));
		} catch (XmlRpcException e) {
			model.put("exception", e); 			
		} catch (MalformedURLException e) {
			model.put("exception", e); 
		}
		
		if (model.get("order_id") != null) {
			// email order details
	    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
	    	String siteURL = siteConfig.get("SITE_URL").getValue();
			// multi store
			List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
			MultiStore multiStore = null;
			if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
				multiStore = multiStores.get(0);
				contactEmail = multiStore.getContactEmail();
				siteURL = request.getScheme()+"://" + multiStore.getHost() + request.getContextPath() + "/";
			}
	    	
	    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
	    	
			SimpleMailMessage msg = new SimpleMailMessage();
			msg.setTo(contactEmail);			
			msg.setFrom(contactEmail);
			msg.setSubject("Shopatron Order ID: " + model.get("order_id") + " placed at " + siteURL);
			try {
				StringBuffer message = new StringBuffer();
				message.append(dateFormatter.format(new Date()));
				message.append("\n\n");
				
				message.append("Number of items: " + cart.getNumberOfItems() + "\n");
				message.append("------------------------------" + "\n");
				for (CartItem lineItem: cart.getCartItems()) {
					message.append("sku = " + lineItem.getProduct().getSku() + "\n");
					message.append("desc = " + lineItem.getProduct().getName() + "\n");
					message.append("price = " + lineItem.getUnitPrice() + "\n");
					message.append("quantity = " + lineItem.getQuantity() + "\n");
					message.append("------------------------------" + "\n");
				}
				
				msg.setText(message.toString());
				mailSender.send(msg);
			} catch (MailException ex) {
				// do nothing
			}	
		}
		return modelAndView;
	}
	
	private void updateAssignedSkus(Cart cart, Map<String, Object> model){
		Map<String, Integer> groupItems = null;
		for(CartItem cartItem : cart.getCartItems()) {
			if(cartItem.getItemGroup() != null && cartItem.isItemGroupMainItem()){
				if(groupItems == null){
					groupItems = new HashMap<String, Integer>();
				}
					
				if(groupItems.get(cartItem.getItemGroup()) != null) {
					groupItems.put(cartItem.getItemGroup(), groupItems.get(cartItem.getItemGroup()) + cartItem.getQuantity());
				} else {
					groupItems.put(cartItem.getItemGroup(), cartItem.getQuantity());
				}
			}
		}
		// remove decorative sku or change quantity based on group or parent sku
		Iterator<CartItem> iterator = cart.getCartItems().iterator();
		List<Integer> removeItemsIndex = null; 
		while(iterator.hasNext()) {
			CartItem cartItem = iterator.next();
			// set quantity for group items ( for decorative skus )
			if(cartItem.getItemGroup() != null && !cartItem.isItemGroupMainItem()){
				if(groupItems != null && groupItems.get(cartItem.getItemGroup()) != null) {
					cartItem.setQuantity((cartItem.getQtyForPriceBreak() != null ? 1 : groupItems.get(cartItem.getItemGroup())) * cartItem.getQtyMultiplier());
					if(cartItem.getQtyForPriceBreak() != null) {
						cartItem.setQtyForPriceBreak(groupItems.get(cartItem.getItemGroup()) * cartItem.getQtyMultiplier());
					}
				} else {
					if(cart.getUserId() != null) {
						// remove from database
						this.webJaguar.removeCartItem(cartItem.getIndex(), cart.getUserId());
						// remove from current cartitems list
						iterator.remove();
					} else {
						if(removeItemsIndex == null) {
							removeItemsIndex = new ArrayList<Integer>();
						}
						removeItemsIndex.add(cartItem.getIndex());
					}
				}
			}
		}
		if(removeItemsIndex != null) {
			for(Integer index : removeItemsIndex) {
				try {
					cart.removeCartItem(index);
				} catch (Exception e) {	e.printStackTrace(); }	
				
			}
		}
		
	}
	
	private void updateMasterSkus(Cart cart, Map<String, Object> model, HttpServletRequest request){
		Map<String, CartItem> groupItems = null;
		for(CartItem cartItem : cart.getCartItems()) {
			if(cartItem.getItemGroup() != null){
				if(groupItems == null){
					groupItems = new HashMap<String, CartItem>();
				}
				CartItem ci = null;
				if(groupItems.get(cartItem.getItemGroup()) != null) {
					ci = groupItems.get(cartItem.getItemGroup());
					ci.setTempItemTotal(ci.getTempItemTotal() + cartItem.getTotal());
					
					// add attributes, if item attributes are empty
					// this may happen, if first item in the list is decorative item and not the main item  
					if(cartItem.isItemGroupMainItem() && cartItem.getProductAttributes() != null && (ci.getProductAttributes() == null || ci.getProductAttributes().isEmpty())) {
						for(ProductAttribute attr : cartItem.getProductAttributes()){
							ci.addProductAttribute(attr);
						}
					}
				} else {
					ci = new CartItem();
					ci.setTempItemTotal(cartItem.getTotal());
					
					// add attributes, if item is main item in the group
					if(cartItem.isItemGroupMainItem() && cartItem.getProductAttributes() != null) {
						for(ProductAttribute attr : cartItem.getProductAttributes()){
							ci.addProductAttribute(attr);
						}
					} 
				}
				
				// set master product only if it is not set before and current item is main item in the group
				// parent product can not be set for 2 different products having different parents.
				// remove decorative products ( not the main products in the group.
				if(ci.getProduct().getSku() == null && cartItem.isItemGroupMainItem()) {
					Product product = this.webJaguar.getProductById(cartItem.getProduct().getId(), request);
					if(product.getMasterSku() != null && !product.getMasterSku().trim().isEmpty()) {
						Integer masterProductId = this.webJaguar.getProductIdBySku(product.getMasterSku());
						if(masterProductId != null) {
							Product parentProduct = this.webJaguar.getProductById(masterProductId, request);
							ci.setProduct(parentProduct);
						}	
					} else {
						// for products which are stand alone
						ci.setProduct(product);
					}
				}
				// add quantity only if item is main item and not decorative (added) sku
				if(cartItem.isItemGroupMainItem()) {
					ci.setQuantity(ci.getQuantity() + cartItem.getQuantity());
				}
				groupItems.put(cartItem.getItemGroup(), ci);
			}
		}
		if(model != null) {
			model.put("groupItems", groupItems);
		}
		this.arrangeCartItems(cart);
	}
	
	private void arrangeCartItems(Cart cart) {
		for(int i =0 ; i<cart.getCartItems().size(); i++){
			// group for assigned (decorative) products
			if(cart.getCartItems().get(i).getItemGroup() != null){
				for (int j = i+1 ; j<cart.getCartItems().size(); j++) {
					if(cart.getCartItems().get(j).getItemGroup() != null && cart.getCartItems().get(j).getItemGroup().equalsIgnoreCase(cart.getCartItems().get(i).getItemGroup())){
						CartItem tempItem = cart.getCartItems().get(j);
						try {
							if(cart.getCartItems().get(i).isItemGroupMainItem()) {
								cart.removeCartItem(j);
								cart.addCartItem(i+1, tempItem);
								i++;
							} else {
								cart.removeCartItem(j);
								if(tempItem.isItemGroupMainItem()) {
									cart.addCartItem(i, tempItem);
								} else {
									cart.addCartItem(i+1, tempItem);
								}
							}	
						}catch(Exception e){ }
					}
				}				
			}
		}
	}
	
	private void validatePromo(HttpServletRequest request, Cart cart, Map<String, Object> gSiteConfig, UserSession userSession) throws ServletRequestBindingException{
		cart.setTempPromoErrorMessage(null);
		String tempPromoCode = null;
		if(request.getSession().getAttribute("promocode") != null){
			tempPromoCode = (String) request.getSession().getAttribute("promocode");
			request.getSession().removeAttribute("promocode");
		}
		if(ServletRequestUtils.getStringParameter(request, "promoCode") != null && !ServletRequestUtils.getStringParameter(request, "promoCode").trim().isEmpty()){
			tempPromoCode = ServletRequestUtils.getStringParameter(request, "promoCode");
		}
		cart.setPromoCode(tempPromoCode);
		
		if ( cart.getPromoCode() != null && !cart.getPromoCode().equals( "" )) {
			
			Promo promo = this.webJaguar.getPromoByName(cart.getPromoCode());
			if (promo != null) {
				// return if promo is designed for shipping or per product
	    		if(promo.getDiscountType().equalsIgnoreCase("shipping") || promo.getDiscountType().equalsIgnoreCase("product")) {
					cart.setPromo( null );
		    		cart.setTempPromoErrorMessage( "f_cart_invalidPromoOnCart" );
		    		return;
				}
	    		
	    		// return if promo is designed for specific states or customer groups and customer is not logged in
	    		if( userSession == null && (!promo.getGroupIdSet().isEmpty() || (promo.getStates() != null  && !promo.getStates().trim().isEmpty()))){
	    			cart.setPromo( null );
		    		cart.setTempPromoErrorMessage( "f_cart_invalidPromoOnCart" );
		    	    return;
				}
				
	    		Map<String, Object> conditionPromo = new HashMap<String, Object>();
				conditionPromo.put("tempPromoCode", cart.getPromoCode());
				conditionPromo.put("minOrder",cart.getSubTotal());
				conditionPromo.put("userId",cart.getUserId());
				
	    		Set<String> skuSet = new HashSet<String>();
	    		for(CartItem item : cart.getCartItems()) {
	    			if(item.getProduct() != null) {
		    			skuSet.add(item.getProduct().getSku());
		    			if(item.getProduct().getManufactureName() != null) {
		    				skuSet.add(item.getProduct().getManufactureName());
		    			}
	    			}
	    		}
	    		conditionPromo.put("cartSkuSet",skuSet.toArray());
				if( (Boolean)gSiteConfig.get("gSALES_PROMOTIONS_ADVANCED") ){
					conditionPromo.put("groupType",promo.getGroupType());
		    		conditionPromo.put("stateType",promo.getStateType());
		    		conditionPromo.put("customShippingId",promo.getShippingIdSet());
				}
				if(this.webJaguar.getPromoWithCondition( promo, conditionPromo ) != null){
	    			if( promo.getDiscountType().equalsIgnoreCase("product") ) {
						
	    				/* If required, need to develop this so that promo per line item is also available on shopping cart
	    				 * 
	    				Map<String, Double> brandTotalMap = new HashMap<String, Double>();
	    				for(CartItem lineItem : cart.getCartItems()) {
	    					if(lineItem.getProduct().getManufactureName() != null && !lineItem.getProduct().getManufactureName().equals("")) {
	    						Double newTotal = lineItem.getTotal();
	    						if(brandTotalMap.get(lineItem.getProduct().getManufactureName()) != null) {
	    							newTotal = newTotal + brandTotalMap.get(lineItem.getProduct().getManufactureName());
	    						}
	    						brandTotalMap.put(lineItem.getProduct().getManufactureName(), newTotal);
	    					} 
	    				}
	    				
	    				Boolean validPromo = false;
	    				for(CartItem lineItem: cart.getCartItems()){
	    					if( promo.getSkuSet().contains( lineItem.getProduct().getSku() )  || ( lineItem.getProduct().getManufactureName() != null && promo.getBrandSet().contains( lineItem.getProduct().getManufactureName()) )) {
	    						if(lineItem.getProduct().getManufactureName() != null && brandTotalMap.get(lineItem.getProduct().getManufactureName()) != null && promo.getMinOrderPerBrand() != null && brandTotalMap.get(lineItem.getProduct().getManufactureName()) < promo.getMinOrderPerBrand()) {
		    						break;
			    				}
			    			} else {
			    				validPromo = validPromo || false;
			    			}
			    		}
	    				if(!validPromo){
	    					cart.setTempPromoErrorMessage( "f_cart_invalidPromo" );
	    				}*/
	    			} else {
	    				cart.setPromo(promo);
	    				request.getSession().setAttribute("promocode", cart.getPromoCode());
	    			}
	    			
		    	} else {
		    		cart.setPromo( null );
		    		cart.setTempPromoErrorMessage( "f_cart_invalidPromo" );
				}
	    	} else{
	    		cart.setPromo( null );
			    cart.setTempPromoErrorMessage("f_cart_invalidPromo");
	    	}
		}
	}
}