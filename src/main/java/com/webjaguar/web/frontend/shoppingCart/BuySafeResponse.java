/* Copyright 2009 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 07.26.2010
 */
package com.webjaguar.web.frontend.shoppingCart;

import com.buySafe.BuySafeAPISoap;
import com.buySafe.ShoppingCartAddUpdateRQ;
import com.buySafe.ShoppingCartAddUpdateRS;
import com.buySafe.ShoppingCartCheckoutRQ;
import com.buySafe.ShoppingCartCheckoutRS;

public class BuySafeResponse implements Runnable{
	private BuySafeAPISoap port;
	
	private ShoppingCartAddUpdateRQ addUpdateRQ;
	private ShoppingCartAddUpdateRS addUpdateRS;
	
	private ShoppingCartCheckoutRQ checkOutRQ;
	private ShoppingCartCheckoutRS checkOutRS;
	
	public BuySafeResponse( BuySafeAPISoap port, ShoppingCartAddUpdateRQ addUpdateRQ, ShoppingCartCheckoutRQ checkOutRQ  ) {
		this.addUpdateRQ = addUpdateRQ;
		this.checkOutRQ = checkOutRQ;
		this.port = port;
	}
	
	public void run() {
		if(addUpdateRQ != null){
			this.addUpdateRS = port.addUpdateShoppingCart(addUpdateRQ);
		} else if(checkOutRQ != null){
			this.checkOutRS = port.setShoppingCartCheckout(checkOutRQ);
		}
	}
	
	public ShoppingCartAddUpdateRS getAddUpdateRS () { return this.addUpdateRS; }

	public  ShoppingCartCheckoutRS getCheckOutRS () { return this.checkOutRS; }

}
