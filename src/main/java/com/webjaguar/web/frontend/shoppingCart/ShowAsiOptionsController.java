/* Copyright 2005, 2008 Advanced E-Media Solutions
 * author: Jwalant Patel
 */

package com.webjaguar.web.frontend.shoppingCart;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.web.servlet.mvc.Controller;

import asiProduct.CriteriaSet;
import asiProduct.PriceGrid;
import asiProduct.CriteriaSet.Options.Option;
import asiProduct.Dependencies.Dependency;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductOptionValue;

public class ShowAsiOptionsController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, String> optionValueMap = new HashMap<String, String>();
		String[] optionCriteriaIds = ServletRequestUtils.getStringParameters(request, "criteriaId");
		String[] optionValues = ServletRequestUtils.getStringParameters(request, "value");
		if(optionValues != null) {
			for(int i=0; i< optionValues.length; i++) {
				optionValueMap.put(optionCriteriaIds[i], optionValues[i]);
			}
		}
		Map<String, Object> myModel = new HashMap<String, Object>();
		if( ServletRequestUtils.getStringParameter(request, "initialDependencyId") !=  null) {
			optionValueMap.put(ServletRequestUtils.getStringParameter(request, "initialDependencyId"), ServletRequestUtils.getStringParameter(request, "initialDependencyValue"));
			optionValueMap.put("initialDependencyId", ServletRequestUtils.getStringParameter(request, "initialDependencyId"));
		}
		if( ServletRequestUtils.getStringParameter(request, "priceGridId") !=  null) {
			optionValueMap.put("priceGridId", ServletRequestUtils.getStringParameter(request, "priceGridId"));
		}
		
		Product product = this.webJaguar.getProductById(ServletRequestUtils.getIntParameter(request, "id"), request);
		if (product != null && product.getAsiXML()!=null) {
			
			myModel.put("product", product);
			myModel.put("optionValueMap", optionValueMap);
			JAXBContext context = JAXBContext.newInstance(asiProduct.Product.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			StringBuffer sb = new StringBuffer(product.getAsiXML());
			ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
		    asiProduct.Product prod = (asiProduct.Product) unmarshaller.unmarshal(bis);
		    
			// set ASI Price with markup, if any
		    boolean isMarkUpApplied = this.webJaguar.applyPriceDifference(prod, null, siteConfig.get("ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS").getValue(),  Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_TYPE").getValue()), Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_FORMULA").getValue()));
		    
		    // set ASI Price with salesTag, if any
		    if((siteConfig.get("ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE").getValue().equals("true") || !isMarkUpApplied) && product.getSalesTag() != null) {
		    	this.webJaguar.setASIPriceWithSalesTag(prod, null, product.getSalesTag());
			}
		    
		    int minQuantity = this.getMinQty(prod, ServletRequestUtils.getStringParameter(request, "priceGridId"));
		    int quantity = 1;
			
		    try {
				quantity = ServletRequestUtils.getIntParameter(request, "quantity");
			} catch ( Exception e) {
				quantity = minQuantity;
			}
			
			if(minQuantity > quantity) {
				quantity = minQuantity;
			}
			myModel.put("quantity", quantity);
			if(product.isAsiIgnorePrice()) {
				myModel.put("unitPrice", this.webJaguar.unitPriceByQty(quantity, product));
			} else {
				myModel.put("unitPrice", this.setPrice(quantity, ServletRequestUtils.getStringParameter(request, "priceGridId"), prod));
			}
			
			// remove last parameter when asi provides xml with clear indication for 'criteria having information only'. 
			myModel.put("asiProductOption", getAsiOptions(prod, optionValueMap, this.getDependencies(prod)));
			
			// pass this value to cart so that customer know which option they selected
			myModel.put("asiInitialDependencyOption", getInitialDependecyOption(prod, optionValueMap));
			
			// default selected options
			List<String> fixedChargeList = new ArrayList<String>();
			fixedChargeList.add("screen charge");
			fixedChargeList.add("set up charge");
			fixedChargeList.add("plate charge");
			fixedChargeList.add("die charge");
			fixedChargeList.add("digitizing charge");
			fixedChargeList.add("mold charge");
			fixedChargeList.add("tape charge");
			fixedChargeList.add("tooling charge");
			fixedChargeList.add("cut charge");
			
			/*fixedChargeList.add("film charge");
			fixedChargeList.add("rushservice charge");*/
			
			Map<String, String> priceDependecyMap = this.setAdditionalPrice(prod, optionValueMap);
			if(priceDependecyMap != null) {
				Double additionalCharge = 0.0;
				for(String charge : priceDependecyMap.keySet()){
					if(optionValueMap.get("initialDependencyId") != null && optionValueMap.get("initialDependencyId").equals(charge)) {
						continue;
					}
					if( !priceDependecyMap.get(charge).equals("QUR") ) {
						boolean fixedCharge = false;
						Double price = Double.parseDouble(priceDependecyMap.get(charge));
						for(int i=0; i< fixedChargeList.size(); i++) {
							if(optionValueMap.get(charge).toLowerCase().contains( fixedChargeList.get(i) ) ) {
								fixedCharge = true;
								break;
							}
						}
						additionalCharge = additionalCharge +  ( fixedCharge  ? price : ( price * quantity));
					}
				}
				if(additionalCharge > 0.0) {
					myModel.put("additionalCharge", additionalCharge);
				}
			}
			myModel.put("priceDependecyMap", this.setAdditionalPrice(prod, optionValueMap));
			
			// default selected options
			List<String> defaultSelectionList = new ArrayList<String>();
			defaultSelectionList.add("ProductColor");
			defaultSelectionList.add("Material");
			defaultSelectionList.add("Size");
			defaultSelectionList.add("Shape");
			defaultSelectionList.add("ImprintSize");
			defaultSelectionList.add("ImprintColor");
			defaultSelectionList.add("FOBPoint");
			defaultSelectionList.add("ProductionTime");
			myModel.put("defaultSelectionList", defaultSelectionList);
			
			
			// default drop down list
			List<String> defaultDropDownList = new ArrayList<String>();
			defaultDropDownList.add("ProductColor");
			defaultDropDownList.add("Size");
			defaultDropDownList.add("ImprintMethod");
			defaultDropDownList.add("ImprintColor");
			defaultDropDownList.add("Packaging");
			myModel.put("defaultDropDownList", defaultDropDownList);
			
		}
		
        return new ModelAndView("frontend/asi/asiOptions", "model", myModel);
	}
	
	// remove this function when asi provides xml with clear indication for 'criteria having information only'. 
	private Set<String> getDependencies(asiProduct.Product product){
		
		Set<String> dependencies = new HashSet<String>();
		
		if(product.getPricing() != null && product.getPricing().getPriceGrid() != null) {
			for(PriceGrid priceGrid : product.getPricing().getPriceGrid()) {
				if(priceGrid.getDependencies() != null && priceGrid.getDependencies().getDependency() != null){
					for(Dependency dependency : priceGrid.getDependencies().getDependency()) {
						dependencies.add(dependency.getId());
					}
				}
			}
		}
		
		if(product.getProductOptions() != null && product.getProductOptions().getCriteriaSet() != null) {
			this.getCriteriaSetWithDependecies(product.getProductOptions().getCriteriaSet(), dependencies);
		}
		
		if(product.getGeneralOptions() != null && product.getGeneralOptions().getCriteriaSet() != null) {
			this.getCriteriaSetWithDependecies(product.getGeneralOptions().getCriteriaSet(), dependencies);
		}
		
		if(product.getImprintOptions() != null && product.getImprintOptions().getCriteriaSet() != null) {
			this.getCriteriaSetWithDependecies(product.getImprintOptions().getCriteriaSet(), dependencies);
		}
		
		return dependencies;
	}
	
	// remove this function when asi provides xml with clear indication for 'criteria having information only'. 
	private void getCriteriaSetWithDependecies(List<asiProduct.CriteriaSet> citeriaSet, Set<String> dependencies){
		
		for(CriteriaSet criteria : citeriaSet) { 
			if(criteria.getOptions() != null && criteria.getOptions().getOption() != null) {
				for(Option option: criteria.getOptions().getOption()) { 
					if(option.getDependencies() != null && option.getDependencies().getDependency() != null) {
						for(Dependency dependency : option.getDependencies().getDependency()) {
							dependencies.add(dependency.getId());
						}
					} 
			  	}
			} else if( criteria.getDependencies() != null && criteria.getDependencies().getDependency() != null){
				for(Dependency dependency : criteria.getDependencies().getDependency()) {
					dependencies.add(dependency.getId());
				}
			}
		}
	}
	
	private List<ProductOption> getAsiOptions(asiProduct.Product product, Map<String, String> optionValueMap, Set<String> dependencies) {
		List<ProductOption> asiProductOptions = new ArrayList<ProductOption>();
		
		if(product.getProductOptions() != null && product.getProductOptions().getCriteriaSet() != null) {
			this.addOptions(product, product.getProductOptions().getCriteriaSet(), optionValueMap, asiProductOptions, dependencies);
		}
		
		if(product.getGeneralOptions() != null && product.getGeneralOptions().getCriteriaSet() != null) {
			this.addOptions(product, product.getGeneralOptions().getCriteriaSet(), optionValueMap, asiProductOptions, dependencies);
		}
		
		if(product.getImprintOptions() != null && product.getImprintOptions().getCriteriaSet() != null) {
			this.addOptions(product, product.getImprintOptions().getCriteriaSet(), optionValueMap, asiProductOptions, dependencies);
		}
		
		return asiProductOptions;
	}
	
	
	private void addOptions(asiProduct.Product product , List<asiProduct.CriteriaSet> citeriaSet, Map<String, String> optionValueMap, List<ProductOption> asiProductOptions, Set<String> dependencies) {
		List<String> invisibleOptions = new ArrayList<String>();
		invisibleOptions.add("Keywords");
		invisibleOptions.add("LineName");
		invisibleOptions.add("ProductionTime");
		invisibleOptions.add("Material");
		invisibleOptions.add("TradeName");
		invisibleOptions.add("Theme");
		invisibleOptions.add("PublicationKeyword");
		invisibleOptions.add("Origin");
		
		for(CriteriaSet criteria : citeriaSet) { 
			if(invisibleOptions.contains(criteria.getName())) {
				continue;
			}
			
			if( optionValueMap.get("initialDependencyId") == null || !criteria.getId().equals(optionValueMap.get("initialDependencyId"))) {
				ProductOption productOption = new ProductOption();
				productOption.setName(criteria.getName());	
				productOption.setAsiCriteriaSetId(criteria.getId());	
				productOption.setAsiDescription(criteria.getDescription());	
				
				boolean disableOption = false;
				if(criteria.getOptions() != null && criteria.getOptions().getOption() != null) {
					
					// remove this block when asi provides xml with clear indication for 'criteria having information only'. 
					if(criteria.getOptions().getOption().size() == 1 && !dependencies.contains(criteria.getId()) && criteria.getOptions().getOption().get(0).getValue().length() > 30) {
						invisibleOptions.add(criteria.getName());
						continue;
					}
					
					for(Option option: criteria.getOptions().getOption()) { 
						disableOption = false;
						if(option.getDependencies() != null && option.getDependencies().getDependency() != null ) {
							
							// check for implicitly applied options
							if(criteria.getOptions().getOption().size() == 1) {
								this.checkImplicitOption(criteria, option, option.getDependencies().getDependency(), optionValueMap);
							}
							
							disableOption = this.isDisabled(option.getDependencies().getDependency(), optionValueMap);
						} 
				  		if(!disableOption) {
				  			ProductOptionValue optionValue = new ProductOptionValue();
					  		optionValue.setName(option.getValue());
					  		productOption.addValue(optionValue);
					 	} else {
					 		optionValueMap.remove(criteria.getId());
					 	}
				  	}
				} else if( criteria.getDependencies() != null && criteria.getDependencies().getDependency() != null){
					// remove this block when asi provides xml with clear indication for 'criteria having information only'. 
					if(!dependencies.contains(criteria.getId()) && criteria.getDescription().length() > 30) {
						invisibleOptions.add(criteria.getName());
						continue;
					}
					
					disableOption = this.isDisabled(criteria.getDependencies().getDependency(), optionValueMap);
					if(!disableOption) { 
			  			ProductOptionValue optionValue = new ProductOptionValue();
				  		optionValue.setName(criteria.getDescription());
				  		productOption.addValue(optionValue);
				  	}  else {
				 		optionValueMap.remove(criteria.getId());
				 	}
			  	} else if( criteria.getDescription() != null ){
			  		
			  		// remove this block when asi provides xml with clear indication for 'criteria having information only'. 
					if(!dependencies.contains(criteria.getId()) && criteria.getDescription().length() > 30) {
						invisibleOptions.add(criteria.getName());
						continue;
					}
					
			  		ProductOptionValue optionValue = new ProductOptionValue();
			  		optionValue.setName(criteria.getDescription());
			  		productOption.addValue(optionValue);
			  	}
				if(productOption.getValues() != null && productOption.getValues().size() > 0) {
				 	asiProductOptions.add(productOption);
				}
			} 
		}
	}
	
	private ProductOption getInitialDependecyOption(asiProduct.Product product , Map<String, String> optionValueMap) {
		ProductOption productOption = null;
		List<CriteriaSet> criteriaSet = new ArrayList<CriteriaSet>();
		if(product.getProductOptions() != null && product.getProductOptions().getCriteriaSet() != null) {
			criteriaSet.addAll(product.getProductOptions().getCriteriaSet());
		}
		
		if(product.getGeneralOptions() != null && product.getGeneralOptions().getCriteriaSet() != null) {
			criteriaSet.addAll(product.getGeneralOptions().getCriteriaSet());
		}
		
		if(product.getImprintOptions() != null && product.getImprintOptions().getCriteriaSet() != null) {
			criteriaSet.addAll(product.getImprintOptions().getCriteriaSet());
		}
		
		for(CriteriaSet criteria : criteriaSet) { 
			if( optionValueMap.get("initialDependencyId") != null && criteria.getId().equals(optionValueMap.get("initialDependencyId"))) {
				productOption = new ProductOption();
				productOption.setName(criteria.getName());	
				productOption.setAsiCriteriaSetId(criteria.getId());	
				productOption.setAsiDescription(criteria.getDescription());	
				
				ProductOptionValue optionValue = new ProductOptionValue();
				optionValue.setName(optionValueMap.get("initialDependencyValue"));
		  		productOption.addValue(optionValue);
			}
		}
		return productOption;
	}
	
	private boolean isDisabled(List<Dependency> dependecies, Map<String, String> optionValueMap) {
		boolean disableOption = false;
		for(Dependency dependency : dependecies) {
			if(optionValueMap.get(dependency.getId()) == null || optionValueMap.get(dependency.getId()).equals("") || !optionValueMap.get(dependency.getId()).equals(dependency.getValue())) {
				disableOption = true;
				break;
			}
		}
		return disableOption;
	}
	
	private void checkImplicitOption(CriteriaSet criteria, Option option, List<Dependency> dependecies, Map<String, String> optionValueMap) {
		for(Dependency dependency : dependecies) {
			if(optionValueMap.get(dependency.getId()) != null && optionValueMap.get(dependency.getId()).equals(dependency.getValue())) {
				optionValueMap.put(criteria.getId(), option.getValue());
				break;
			}
			
		}
	}
	
	private Double setPrice(int quantity, String priceGridId, asiProduct.Product product) throws Exception {
		
		Double unitPrice = null;
		if(product.getPricing() != null && product.getPricing().getPriceGrid() != null) {
			for(PriceGrid priceGrid : product.getPricing().getPriceGrid()) { 
				if(priceGrid.getId().equals(priceGridId)) {
					
					for(int i=0; i<priceGrid.getPrice().size(); i++) {
						asiProduct.PriceGrid.Price asiPrice = priceGrid.getPrice().get(i);
						int quantMin = 1;
						int quantMax = 1;
						
						if(i == 0) {
							quantMin = 1;
							if(priceGrid.getPrice().size() == 1) {
								quantMax = Integer.parseInt(asiPrice.getQuantity().toString()) + 1;
							} else {
								quantMax = Integer.parseInt(priceGrid.getPrice().get(i+1).getQuantity().toString());
							}
						} else if (i == priceGrid.getPrice().size()-1) {
							quantMin = Integer.parseInt(asiPrice.getQuantity().toString());
						} else {
							quantMin = Integer.parseInt(asiPrice.getQuantity().toString());
							quantMax = Integer.parseInt(priceGrid.getPrice().get(i+1).getQuantity().toString());
						}
						
						if( quantity >= quantMin && quantity < quantMax) {
							unitPrice = Double.parseDouble(asiPrice.getValue());
							break;
						} else if( quantity >= quantMin && quantMax == 1) {
							unitPrice = Double.parseDouble(asiPrice.getValue());
						}
					}
				}
			}
		}
		return unitPrice;
	}
	
	private int getMinQty(asiProduct.Product product, String priceGridId) throws Exception {
		int minQty = 1;
		if(product.getPricing() != null && product.getPricing().getPriceGrid() != null) {
			for(PriceGrid priceGrid : product.getPricing().getPriceGrid()) { 
				if(priceGrid.getId().equals(priceGridId)) {
					minQty = Integer.parseInt(priceGrid.getPrice().get(0).getQuantity().toString());
				}
			}
		}
		return minQty;
	}
	
	private Map<String, String> setAdditionalPrice(asiProduct.Product product, Map<String, String> optionValueMap) throws Exception {
		Map<String, String> priceDependecyMap = new HashMap<String, String>();
		if(product.getPricing() != null && product.getPricing().getPriceGrid() != null) {
			for(PriceGrid priceGrid : product.getPricing().getPriceGrid()) { 
				if(priceGrid.getDependencies() != null && priceGrid.getDependencies().getDependency() != null) {
					for(Dependency dependency : priceGrid.getDependencies().getDependency()) {
						if(optionValueMap.get(dependency.getId()) != null && ( optionValueMap.get(dependency.getId()).equals(dependency.getValue()) || dependency.getValue() == null )){
							if( priceGrid.getId().equals("pricegrid-99") ) {
								priceDependecyMap.put(dependency.getId(), "QUR");
							} else {
								Double price = this.setPrice(1, priceGrid.getId(), product);
								priceDependecyMap.put(dependency.getId(), price.toString());
							}
						}
					}
				}
			}
		}
		return priceDependecyMap;
	}
}