/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 */

package com.webjaguar.web.frontend.shoppingCart;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Option;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductOptionValue;
import com.webjaguar.model.IncludedProduct;
import com.webjaguar.model.UserSession;

public class AddToCartController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar( WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest( HttpServletRequest request, HttpServletResponse response ) throws Exception
	{
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		UserSession userSession = this.webJaguar.getUserSession( request );
		Integer userId = null;
		String productLink = null;
		List<CartItem> productsWithOptions = new ArrayList<CartItem>();
		if ( userSession != null ) {
			userId = userSession.getUserid();
		}
		// set group for items. Group has to be same for all children products.
		String group = new Integer(new Random().nextInt(1000)).toString();
		
		String ids[] = request.getParameterValues( "product.id" );
		
		String loadcenter = ServletRequestUtils.getStringParameter( request, "loadcenter", null );
		if(loadcenter!=null && loadcenter.equalsIgnoreCase("true")){
			ids = updateIds(ids, request);
		}	
		
		if ( ids != null ) {
			for ( int i = 0; i < ids.length; i++ ) {
				// get quantity
				String stringQuantity = ServletRequestUtils.getStringParameter( request, "quantity_" + ids[i], null );
				String includePro = ServletRequestUtils.getStringParameter( request, "include_" + ids[i], null );
				try {
					// get Product
					Product product = this.webJaguar.getProductById( Integer.parseInt( ids[i] ), request );
					if(includePro!=null && includePro.equalsIgnoreCase("1")){
						if(product!=null){
							product.setIncludeRetailDisplay(true);
						}
					}else{
						if(product!=null){
							product.setIncludeRetailDisplay(false);
						}
					}		
					if ( stringQuantity != null ) {
						if ( product != null && Integer.parseInt( stringQuantity ) > 0 ) {
							if ( (Boolean) gSiteConfig.get( "gBOX" ) && product.getType().equals( "box" ) && product.getBoxSize() != null ) { // check for null totalQty
								if ( ( request.getParameter( "box_total_qty" ) == null || request.getParameter( "box_total_qty" ).equals( "" ) ) || 
										( Integer.parseInt( request.getParameter( "box_total_qty" ) ) != product.getBoxSize() && product.getBoxExtraAmt() == null || 
										  Integer.parseInt( request.getParameter( "box_total_qty" ) ) < product.getBoxSize() && product.getBoxExtraAmt() != null)) {
									Map<String, Object> myModel = new HashMap<String, Object>();
									// box
									List <Product> boxContentsList = null;
									if ( product.getProductOptions() != null ) {
										boxContentsList = new ArrayList<Product>();
										for ( ProductOption op:product.getProductOptions()) {
											if ( op.getType() != null && op.getType().equals( "box" ) ) {		
												Product boxContentProduct = this.webJaguar.getProductById( this.webJaguar.getProductIdBySku( op.getName().trim() ), request );
												if ( boxContentProduct != null ) {
													boxContentProduct.setType( "box" );
													boxContentProduct.setOptionIndex( op.getIndex() );
													boxContentsList.add( boxContentProduct );	
												}		
											}
										}
										if (boxContentsList != null && !boxContentsList.isEmpty()) {	// not empty
											myModel.put( "boxContentsList", boxContentsList );	
											myModel.put( "boxSize", product.getBoxSize() );
											myModel.put( "boxType", true );
										}
									}
									myModel.put( "var", product.getBoxSize());
									if ( request.getParameter( "box_total_qty" ) == null || request.getParameter( "box_total_qty" ).equals( "" ) )
										myModel.put( "boxErrorMessage", "This page requires Javascript enable!");
									else if ( Integer.parseInt( request.getParameter( "box_total_qty" ) ) != product.getBoxSize() && product.getBoxExtraAmt() == null )
										myModel.put( "boxErrorMessage", "Please add " + product.getBoxSize() + " item(s).");
									else if ( Integer.parseInt( request.getParameter( "box_total_qty" ) ) < product.getBoxSize() && product.getBoxExtraAmt() != null )
										myModel.put( "boxErrorMessage", "Please add " + product.getBoxSize() + " item(s).( $" + product.getBoxExtraAmt()  + " extra for each additional item. )");
									myModel.put( "mainCategories", this.webJaguar.getMainCategoryLinks( request, null) );
									myModel.put( "product", product );
									return new ModelAndView("frontend/product", "model", myModel);
								}
							}

							CartItem cartItem = new CartItem();
							cartItem.setIncludeRetailDisplay(product.getIncludeRetailDisplay());
							cartItem.getProduct().setId( product.getId() );
							cartItem.getProduct().setCaseContent(product.getCaseContent() );
							if (product.isPriceCasePack()) {
								cartItem.setPriceCasePackQty(product.getPriceCasePackQty());
							} else {
								cartItem.setPriceCasePackQty(null);
							}
							cartItem.getProduct().setPacking( product.getPacking() );
							cartItem.getProduct().setPriceByCustomer( product.isPriceByCustomer() ); 
							cartItem.getProduct().setBoxSize( product.getBoxSize() ); 
							cartItem.getProduct().setBoxExtraAmt( product.getBoxExtraAmt() ); 
							cartItem.getProduct().setNumCustomLines( product.getNumCustomLines() );
							cartItem.setUnitWeight( product.getWeight() );
							if (siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly")) {
								cartItem.getProduct().setInventory( product.getInventoryAFS() );
							} else {
								cartItem.getProduct().setInventory( product.getInventory() );
							}							
							cartItem.getProduct().setNegInventory( product.isNegInventory() );
							cartItem.getProduct().setMinimumQty( product.getMinimumQty() );
							cartItem.getProduct().setIncrementalQty( product.getIncrementalQty() );
							cartItem.getProduct().setDefaultSupplierId( product.getDefaultSupplierId() );
							cartItem.setQuantity( Integer.parseInt( stringQuantity ) );
							cartItem.getProduct().setManufactureName( product.getManufactureName() );
							
							if ( (Boolean) gSiteConfig.get( "gVARIABLE_PRICE" ) ) {
								if ( product.isPriceByCustomer() )
									cartItem.setVariableUnitPrice( 0.0 );
							} else {
								cartItem.setVariableUnitPrice( null );
							}
							if ((Boolean) gSiteConfig.get("gSUBSCRIPTION")) {
								String subscriptionInterval = ServletRequestUtils.getStringParameter(request, "subscriptionInterval_" + ids[i], "");
								if (subscriptionInterval.equals("m1") || subscriptionInterval.equals("m2")) {
									cartItem.setSubscriptionInterval(subscriptionInterval);
								}
							}
							//for PPG layout
							// master sku
							Integer masterProductId = null;
							if(siteConfig.get("APPLY_PARENT_SKU_OPTION").getValue().equals("true")){
								// clear the child sku option code, if it exist
								// this will make sure that intermediate cart does not appear.
								product.getProductOptions().clear();
							}
							if ((Boolean) gSiteConfig.get("gMASTER_SKU") && product.getMasterSku() != null && siteConfig.get("APPLY_PARENT_SKU_OPTION").getValue().equals("true") ) {
								masterProductId = this.webJaguar.getProductIdBySku(product.getMasterSku());
							}
							
							// Set Attributes to the cart item
							Set<String> optionCodes = new HashSet<String>();
							for (String optionCode: ServletRequestUtils.getStringParameters(request, "optionCode_" + (masterProductId != null ? masterProductId.toString().trim() : ids[i]))) {
								optionCode = optionCode.trim();
								if (optionCode.length() > 0) {
									// get product attributes return an empty array if not found.
									String options[] = ServletRequestUtils.getStringParameters(request, optionCode + "-option_" + (masterProductId != null ? masterProductId.toString() : ids[i]));
									if (options.length != 0) {
										
										for (int option_index = 0, optionCount = 0; option_index < options.length; option_index++) {
											// Read only one option at a time. Do not read all options at a time belongs to same option code
											// This is required to maintain the order of attributes on cart, specially when dependent option appears based on selection of one option value
											if(optionCount > 0 || !optionCodes.add(optionCode.toLowerCase()+"-"+option_index)) {
												continue;
											}
											optionCount++;
											
											String option_values[] = request.getParameterValues(optionCode + "-option_values_" + (masterProductId != null ? masterProductId.toString() : ids[i]) + "_" + options[option_index]);
											if (option_values != null) {
												for (int values_index = 0; values_index < option_values.length; values_index++) {	
													cartItem.addProductAttribute(new ProductAttribute(optionCode, Integer.parseInt(options[option_index]), Integer.parseInt(option_values[values_index]), ServletRequestUtils.getStringParameter(request, optionCode + "-image_" +( masterProductId != null ? masterProductId.toString() : ids[i] )+ "_" + options[option_index] + "_" + option_values[values_index], null )));
													boolean productIncluded = this.addIncludedProductToCart(cartItem, group, optionCode, Integer.parseInt(options[option_index]), Integer.parseInt(option_values[values_index]), userId, request, cartItem.getQuantity());
													if(ids.length > 1 || productIncluded) {
														cartItem.setItemGroup(group);
														cartItem.setItemGroupMainItem(true);
													}
												}
											}
											// one value for custom text
											String option_values_cus[] = request.getParameterValues(optionCode + "-option_values_cus_" + (masterProductId != null ? masterProductId.toString() : ids[i] )+ "_" + options[option_index]);
											if ( option_values_cus != null ) {
												cartItem.addProductAttribute(new ProductAttribute(optionCode, Integer.parseInt(options[option_index]), option_values_cus[0].toString(), 0));
											}
											
											// option value is different than default value for that index
											String option_values_cus_same_index[] = request.getParameterValues(optionCode + "-option_values_cus_with_index_" + (masterProductId != null ? masterProductId.toString() : ids[i] )+ "_" + options[option_index]);
											
											if ( option_values_cus_same_index != null ) {
												try {
													if(option_values_cus_same_index[0].toString().split("_").length == 2) {
														cartItem.addProductAttribute(new ProductAttribute(optionCode, Integer.parseInt(options[option_index]), option_values_cus_same_index[0].toString().split("_")[1], Integer.parseInt(option_values_cus_same_index[0].split("_")[0])));
													}
												}catch(Exception e){ e.printStackTrace(); }
											}
											// one value for box content
											String option_values_box[] = request.getParameterValues(optionCode + "-option_values_box_" + (masterProductId != null ? masterProductId.toString() : ids[i]) + "_" + options[option_index]);
											if ((Boolean) gSiteConfig.get("gBOX" ) && request.getParameter("box_total_qty") != null && option_values_box != null && product.getBoxSize() != null) {	
												if (!option_values_box[0].toString().equals("")) {
													cartItem.addProductAttribute(new ProductAttribute(
															optionCode
															, Integer.parseInt(options[option_index]) 
															, ServletRequestUtils.getStringParameter(request, optionCode + "-product_name_" + masterProductId != null ? masterProductId.toString() : ids[i] + "_" + options[option_index], null).concat(" Qty: " + option_values_box[0].toString())
															, ServletRequestUtils.getStringParameter(request, optionCode + "-product_name_" + masterProductId != null ? masterProductId.toString() : ids[i] + "_" + options[option_index], null)));
												}
											}
										}
									}		
								}
							}
							
							
							// setting box content quantity and box content weight
							if ( (Boolean) gSiteConfig.get( "gBOX" ) && product.getBoxSize() != null ) {
								//create new optionCodes set, so that it can be used in next iteration
								optionCodes = new HashSet<String>();
								for (String optionCode: ServletRequestUtils.getStringParameters(request, "optionCode_" + (masterProductId != null ? masterProductId.toString().trim() : ids[i]))) {
									optionCode = optionCode.trim();
									if (optionCode.length() > 0 && optionCodes.add(optionCode.toLowerCase())) {
										String options[] = ServletRequestUtils.getStringParameters(request, optionCode + "-option_" + (masterProductId != null ? masterProductId.toString() : ids[i]));
										if (options.length != 0) {
											int boxContentQuantity = 0;
											double boxContentWeight = 0.0;
											for (int option_index = 0; option_index < options.length; option_index++) {
												// one value for box content
												String option_values_box[] = request.getParameterValues(optionCode + "-option_values_box_" + (masterProductId != null ? masterProductId.toString() : ids[i]) + "_" + options[option_index]);
												if (request.getParameter("box_total_qty") != null && option_values_box != null && product.getBoxSize() != null) {	
													if (!option_values_box[0].toString().equals("")) {
														boxContentQuantity += Integer.parseInt(option_values_box[0].toString());
														boxContentWeight += Integer.parseInt(option_values_box[0].toString()) * ServletRequestUtils.getDoubleParameter(request, optionCode + "-option_weight_" + masterProductId != null ? masterProductId.toString() : ids[i] + "_" + options[option_index], 0.0);
													}
												}
											}
											boxContentQuantity = Math.abs( boxContentQuantity - product.getBoxSize() );
											cartItem.setBoxExtraQuantity( boxContentQuantity  );
											cartItem.setBoxContentWeight( boxContentWeight );
										}		
									}
								}
							}
							/* 
							 * 
							 * attributes/options with name-value pair without having index on database
							 * request object must contain one or more parameter with name optionNVPair
							 * value of the parameter optionNVPair must contain name and value of the option/attribute separated by _value_
							 * i.e optionNVPair=XYZName_value_XYZValue
							 *
							 */
							if ( ServletRequestUtils.getStringParameter(request, "optionNVPair") != null ) {
								String optionNVPairs[] = ServletRequestUtils.getStringParameters(request, "optionNVPair");
								for ( String optionNV : optionNVPairs)
								{
									try {
										cartItem.addProductAttribute( new ProductAttribute( optionNV.split("_value_")[0], optionNV.split("_value_")[1], true ) );
									}catch(Exception e){ }
								}
								if(ServletRequestUtils.getDoubleParameter(request, "additionalCharge") != null){
									cartItem.setAsiAdditionalCharge(ServletRequestUtils.getDoubleParameter(request, "additionalCharge"));
								}
							}
							// get custom line
							if ( (Boolean) gSiteConfig.get( "gCUSTOM_LINES" ) && cartItem.getProduct().getNumCustomLines() != null ) {
								loadCustomLines(request, cartItem);
							}

							if ( product.getProductOptions().isEmpty() || cartItem.getProductAttributes() != null || product.getNumCustomLines() != null ) {

								if (request.getParameter("mainProductId") != null && !request.getParameter("mainProductId").equalsIgnoreCase("")){
									product = this.webJaguar.getProductById(Integer.parseInt(request.getParameter("mainProductId")), request);
								}
								
								if(gSiteConfig.get("gMOD_REWRITE") != null && gSiteConfig.get("gMOD_REWRITE").equals("1") && product.getSku() != null) {
									productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ siteConfig.get("MOD_REWRITE_CATEGORY").getValue() + "/product/" + product.getEncodedSku() + "/" + product.getEncodedName() + ".html";
								} else {
									productLink =  request.getScheme()+"://" + request.getHeader("host") + request.getContextPath() + "/"+ "product.jhtm?id=" + product.getId();
								}
								
								if (userId != null) { 
								    // save on database
									if(request.getParameter("decrease")!=null){
										cartItem.setQuantity(-1);
									}
									this.webJaguar.addToCart(cartItem, userId);
								}
								else { // store in session
									Cart cart = (Cart) WebUtils.getOrCreateSessionAttribute( request.getSession(), "sessionCart", Cart.class );
									if ( cart.containsCartItem( cartItem ) ) {
										if(request.getParameter("decrease")!=null){
											cartItem.setQuantity(-1);
											cart.incrementQuantityByCartItem2(cartItem);
										}else{
											cart.incrementQuantityByCartItem(cartItem);
										}
									}
									else {
										// add item
										cart.addCartItem( cartItem );
									}
								}
							} else	{
								cartItem.setProduct( product );
								productsWithOptions.add( cartItem );								
							}							
						}
					}
				} catch ( NumberFormatException e ) { 
					// increase log file.
					// e.printStackTrace(); 
					}
			}
		}
		
		
		if (request.getParameter("addToCartCategory") != null && request.getParameter("addToCartCategory").equalsIgnoreCase("true")) {
			return new ModelAndView(new RedirectView("category.jhtm?cid="+request.getParameter("cid")));
		}else if(request.getParameter("addToCartProduct") != null && request.getParameter("addToCartProduct").equalsIgnoreCase("true")){
			return new ModelAndView(new RedirectView(productLink));
		}else if(request.getParameter("addToCartLucene") != null && request.getParameter("addToCartLucene").equalsIgnoreCase("true")){
			return new ModelAndView(new RedirectView("lsearch.jhtm?keywords="+request.getParameter("searchKeyword")+"&searchOption=Products"));
		}

		if ( productsWithOptions.isEmpty() ) {
			return new ModelAndView( new RedirectView( "viewCart.jhtm" ) );
		} else {
			Layout layout = (Layout) request.getAttribute( "layout" );
			if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" )) {
				layout.setLeftBarTopHtml( "" );
				layout.setLeftBarBottomHtml( "" );
				layout.setHideLeftBar( true );
			} 
			Map<String, Object> myModel = new HashMap<String, Object>();
			myModel.put("productsWithOptions", productsWithOptions);
			myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
			myModel.put("showProductQuoteColumn", false);
			if(siteConfig.get("TEMPLATE").getValue()!=null && siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")){
				return new ModelAndView("frontend/layout/template6/intermediateCart", "model", myModel);
			}else{
				return new ModelAndView("frontend/intermediateCart", "model", myModel);
			}					
		}
	}
	
	private String[] updateIds(String[] ids, HttpServletRequest request) {
		String id[] = request.getParameterValues("hidden.id");
		return id;
	}

	private void loadCustomLines(HttpServletRequest request, CartItem cartItem){
		for ( int itemNum = 1; itemNum <= cartItem.getQuantity(); itemNum++ ) {
			StringBuffer sb = new StringBuffer();
			for ( int lineNum = 1; lineNum <= cartItem.getProduct().getNumCustomLines(); lineNum++ ) {
				String lineValue = ServletRequestUtils.getStringParameter( request, "line_" + itemNum + "_" + lineNum, "" );
				sb.append(lineValue.trim() + " \n");
			}
			cartItem.addCustomLine(sb.toString(), 1);
		}
	}
	
	private boolean addIncludedProductToCart(CartItem parentItem, String group, String optioncode, int optionIndex, int valueIndex, Integer userId, HttpServletRequest request, int quantity){
		boolean productIncluded = false;
		
		Option option = this.webJaguar.getOption(null, optioncode);
		if(option != null){
			for(ProductOption po : option.getProductOptions()) {
				if(po.getIndex() == optionIndex){
					for(ProductOptionValue attr : po.getValues()){
						if(attr.getIndex() == valueIndex && attr.getIncludedProducts() != null) {
							for(IncludedProduct includedProduct :  attr.getIncludedProductList()) {
								Integer productId = this.webJaguar.getProductIdBySku(includedProduct.getSku());
								Product product = this.webJaguar.getProductById(productId, 5, false, null);
								if(product == null){
									continue;
								}
								Product parentProduct = this.webJaguar.getProductById(parentItem.getProduct().getId(), 5, false, null);
								productIncluded = true;
								CartItem cartItem = new CartItem();
								cartItem.setProduct(product);
								cartItem.setQtyMultiplier(includedProduct.getQtyMultiplier());
								cartItem.setQtyForPriceBreak(includedProduct.getOneTimePrice() ? parentItem.getQuantity() * includedProduct.getQtyMultiplier()  : null);
								cartItem.setQuantity((includedProduct.getOneTimePrice() ? 1 : parentItem.getQuantity()) * includedProduct.getQtyMultiplier());
								cartItem.setItemGroup(group);
								cartItem.setItemGroupMainItem(false);
								
								if(includedProduct.getAttachment()) {
									//upload an image
									//save it on session
									MultipartFile attachment = null;
									try {
										MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
										attachment = multipartRequest.getFile( "attachment_"+cartItem.getProduct().getSku() );
										
										if ( attachment != null && !attachment.isEmpty() ) {
											File baseFile = new File( getServletContext().getRealPath("temp"));
											if(!baseFile.exists()){
												baseFile.mkdir();
											}
											baseFile = new File( baseFile, "/Cart/" );
											if(!baseFile.exists()){
												baseFile.mkdir();
											}
										
											baseFile = new File( baseFile, (userId != null ? "/customer/" : "/session/") );
											if(!baseFile.exists()){
												baseFile.mkdir();
												baseFile.setWritable(true, true);
											}
											
											baseFile = new File( baseFile, (userId != null ? "/"+userId+"/" : "/"+request.getSession().getId()+"/") );
											if(!baseFile.exists()){
												baseFile.mkdir();
												baseFile.setWritable(true, true);
											}
											
											// save attachment
											if ( baseFile.canWrite() ) {
												File newFile = new File( baseFile, attachment.getOriginalFilename() );
												attachment.transferTo( newFile );
												cartItem.setAttachment(attachment.getOriginalFilename());
											}
										}
									} catch ( Exception e ){ e.printStackTrace(); }
								}
								
								try {
									if(parentProduct.getMasterSku() != null && !parentProduct.getMasterSku().trim().isEmpty()) {
										cartItem.addProductAttribute( new ProductAttribute( "Added With Product", this.webJaguar.getProductNameBySku(parentProduct.getMasterSku()), true ) );
									} else {
										cartItem.addProductAttribute( new ProductAttribute( "Added With Product", parentProduct.getName(), true ) );
									}
									for (String optionCode: ServletRequestUtils.getStringParameters(request, "optionCode_" + productId)) {
										optionCode = optionCode.trim();
										if (optionCode.length() > 0) {
											// get product attributes return an empty array if not found.
											String options[] = ServletRequestUtils.getStringParameters(request, optionCode + "-option_" + productId);
											if (options.length != 0) {
												for (int option_index = 0; option_index < options.length; option_index++) {
													String option_values[] = request.getParameterValues(optionCode + "-option_values_" + productId + "_" + options[option_index]);
													if (option_values != null) {
														for (int values_index = 0; values_index < option_values.length; values_index++) {	
															cartItem.addProductAttribute(new ProductAttribute(optionCode, Integer.parseInt(options[option_index]), Integer.parseInt(option_values[values_index]), ServletRequestUtils.getStringParameter(request, optionCode + "-image_" +productId+ "_" + options[option_index] + "_" + option_values[values_index], null )));
														}
													}
													// one value for custom text
													String option_values_cus[] = request.getParameterValues(optionCode + "-option_values_cus_" + productId + "_" + options[option_index]);
													if ( option_values_cus != null ) {
														cartItem.addProductAttribute(new ProductAttribute(optionCode, Integer.parseInt(options[option_index]), option_values_cus[0].toString(), 0));
													}
													
													// option value is different than default value for that index
													String option_values_cus_same_index[] = request.getParameterValues(optionCode + "-option_values_cus_with_index_" + productId+ "_" + options[option_index]);
													
													if ( option_values_cus_same_index != null ) {
														try {
															if(option_values_cus_same_index[0].toString().split("_").length == 2) {
																cartItem.addProductAttribute(new ProductAttribute(optionCode, Integer.parseInt(options[option_index]), option_values_cus_same_index[0].toString().split("_")[1], Integer.parseInt(option_values_cus_same_index[0].split("_")[0])));
															}
														}catch(Exception e){ e.printStackTrace(); }
													}
												}
											}		
										}
									}
								}catch(Exception e){ e.printStackTrace(); }
								
								if ( userId != null ) { 
									// save on database
									this.webJaguar.addToCart( cartItem, userId );
								} else { // store in session
									Cart cart = (Cart) WebUtils.getOrCreateSessionAttribute( request.getSession(), "sessionCart", Cart.class );
									try {
										if ( cart.containsCartItem( cartItem ) ) {
											cart.incrementQuantityByCartItem( cartItem );
										} else {
											cart.addCartItem( cartItem );
										}	
									}catch(Exception e){ e.printStackTrace(); }
								}
							}
						}
					}
				}
			}
		}
		return productIncluded;
	}
}