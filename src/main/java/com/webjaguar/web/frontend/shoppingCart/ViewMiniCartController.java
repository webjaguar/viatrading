/* Copyright 2005, 2010 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.shoppingCart;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;

import com.webjaguar.model.Cart;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.UserSession;

public class ViewMiniCartController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
    	Map<String, Object> model = new HashMap<String, Object>();
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	if (!siteConfig.get("MINI_CART").getValue().equalsIgnoreCase("true")) {
    		
    		return new ModelAndView("");
    	}
    	
    	UserSession userSession = (UserSession) request.getAttribute( "userSession" );
    	Cart cart = new Cart();
    	
		Integer userId = null;
		if (userSession != null) { // cart stored in database
			userId = userSession.getUserid();		
			// no update in floating cart
			
			cart = this.webJaguar.getUserCart(userId, null);
			cart.setUserId( userId );
		} else { // cart stored in session
			Cart sessionCart = (Cart) WebUtils.getSessionAttribute(request, "sessionCart");
			if (sessionCart != null) cart = sessionCart;
			// no update in floating cart
		}
		
		String message = this.webJaguar.updateCart(cart, request);
		if (userSession != null) {
			request.getSession().setAttribute("userCart", cart);
		}
		
		Map<String, Object> params = new HashMap<String, Object>();

		// check if checkout button was pressed
		if (request.getParameter("_checkout.x") != null) {
			if (cart.isContinueCart()) {
				return new ModelAndView(new RedirectView("checkout.jhtm"), params);
			}
		}		

		if (message != null ) model.put("message", message); 

		model.put("cart", cart);
		return new ModelAndView("frontend/ajax/miniCart", "model", model);
	}
}