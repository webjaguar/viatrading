/* Copyright 2005, 2010 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.shoppingCart;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.model.Cart;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.UserSession;

public class ViewMiniCartLinkController extends WebApplicationObjectSupport implements Controller {
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	if (!siteConfig.get("MINI_CART").getValue().equalsIgnoreCase("true")) {
    		return new ModelAndView("");
    	}
    	Map<String, Object> model = new HashMap<String, Object>();
    	String quickViewCart = "";
    	
    	NumberFormat nf = NumberFormat.getInstance( );
    	 nf.setMaximumFractionDigits( 2 );
    	 nf.setMinimumFractionDigits( 2 );
    	 Cart sessionCart = (Cart) request.getSession().getAttribute( "sessionCart" );
    	 Cart userCart = (Cart) request.getSession().getAttribute( "userCart" );
    	 UserSession userSession = (UserSession) request.getAttribute( "userSession" );
    	 if (userSession == null ) {
    	 	if (request.getSession().getAttribute( "sessionCart" ) == null ) {
    	 		quickViewCart = "0 Items $0.00";
    	 	} else {
    	 		quickViewCart = sessionCart.getQuantity() + " Items " + "$" +  nf.format(sessionCart.getSubTotal());
    	 	}
    	 } else {
    	 	if (request.getSession().getAttribute( "userCart" ) == null ) {
    	 		quickViewCart = "0 Items $0.00";
    	 	} else {
    	 		quickViewCart = userCart.getQuantity() + " Items " + "$" +  nf.format(userCart.getSubTotal());
    	 	}
    	 }

		model.put("quickViewCart", quickViewCart);
		return new ModelAndView("frontend/ajax/miniCartLink", "model", model);
	}
}