/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.26.2009
 */

package com.webjaguar.web.frontend.shoppingCart;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Price;
import com.webjaguar.model.UserSession;

public class AddFrameToCartController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest( HttpServletRequest request, HttpServletResponse response ) throws Exception {
		
		UserSession userSession = this.webJaguar.getUserSession(request);
		Integer userId = null;
		if (userSession != null) {
			userId = userSession.getUserid();
		}
		
		String xml = ServletRequestUtils.getStringParameter(request, "xml", "").trim();
		
		File framerDir = new File(getServletContext().getRealPath("/framer/pictureframer/"));
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				framerDir = new File((String) prop.get("site.root") + "/framer/pictureframer/");
			}
		} catch (Exception e) {}
		File xmlFile = new File(framerDir, "xml/" + xml + ".xml");
		
		if (xmlFile.length() > 0 && xmlFile.exists()) {
			
			CartItem cartItem = parseXML(xmlFile);
			
			if (userId != null) { 
				// save on database
				this.webJaguar.addToCart(cartItem, userId);
			}
			else{ 
				// store in session
				Cart cart = (Cart) WebUtils.getOrCreateSessionAttribute(request.getSession(), "sessionCart", Cart.class);
				cart.addCartItem(cartItem);				
			}
			
			xmlFile.delete();
		}
		
		return new ModelAndView(new RedirectView("viewCart.jhtm"));
	}
	
	private CartItem parseXML(File xmlFile) throws Exception {
		CartItem cartItem = new CartItem();
		
		SAXBuilder builder = new SAXBuilder(false);	
		
		Document doc = builder.build(xmlFile);
		
		// name
		cartItem.getProduct().setName(doc.getRootElement().getChild("productName").getValue());
		
		// quantity
		cartItem.setQuantity(Integer.parseInt(doc.getRootElement().getChild("quantity").getValue()));

		// price
		List<Price> prices = new ArrayList<Price>();
		prices.add(new Price(Double.parseDouble(doc.getRootElement().getChild("totalPrice").getValue()), null, null, null, null, null));
		cartItem.getProduct().setPrice(prices);
		
		// image
		String productURL = doc.getRootElement().getChild("productURL").getValue();
		cartItem.setCustomImageUrl(productURL.substring(productURL.lastIndexOf("/")+1));
		
		cartItem.setCustomXml(FileUtils.readFileToString(xmlFile, "UTF-8"));
		
		return cartItem;
	}
}
