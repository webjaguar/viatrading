/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 09.14.2010
 */

package com.webjaguar.web.frontend.shoppingCart;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.ListIterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import asiProduct.PriceGrid;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.UserSession;

public class AddASIToCartController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest( HttpServletRequest request, HttpServletResponse response ) throws Exception {
		UserSession userSession = this.webJaguar.getUserSession(request);
		Integer userId = null;
		if (userSession != null) {
			userId = userSession.getUserid();
		}
		int productId = ServletRequestUtils.getIntParameter(request,"product.id",-1);
		if ( productId != -1 )
		{
				siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
				// get quantity
				String stringQuantity = ServletRequestUtils.getStringParameter( request, "quantity_" + productId, null );
				try
				{ 
					// get Product
					Product product = this.webJaguar.getProductById(productId, request);
					asiProduct.Product prod = null;
					com.webjaguar.thirdparty.asi.asiAPI.Product asiProduct = null;
					boolean isMarkUpApplied = false;
					if (product != null && product.getAsiXML()!=null) {
					
						if(product.getProductLayout().equalsIgnoreCase("012")) {
							JAXBContext context = JAXBContext.newInstance(com.webjaguar.thirdparty.asi.asiAPI.Product.class);
							Unmarshaller unmarshaller = context.createUnmarshaller();
							StringBuffer sb = new StringBuffer(product.getAsiXML());
							ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
						    asiProduct = (com.webjaguar.thirdparty.asi.asiAPI.Product) unmarshaller.unmarshal(bis);
						} else if(product.getProductLayout().equalsIgnoreCase("005") || product.getProductLayout().equalsIgnoreCase("007")){
							JAXBContext context = JAXBContext.newInstance( asiProduct.Product.class);
							Unmarshaller unmarshaller = context.createUnmarshaller();
							StringBuffer sb = new StringBuffer(product.getAsiXML());
							ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
							prod = (asiProduct.Product) unmarshaller.unmarshal(bis);
						}
					    // set ASI Price with markup, if any
					    isMarkUpApplied = this.webJaguar.applyPriceDifference(prod, asiProduct, siteConfig.get("ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS").getValue(),  Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_TYPE").getValue()), Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_FORMULA").getValue()));
					}
					
					if ( product != null && stringQuantity != null && Integer.parseInt( stringQuantity ) > 0 ) {
						CartItem cartItem = new CartItem();
						cartItem.setQuantity( Integer.parseInt( stringQuantity ) );
						try {
							cartItem.setAsiAdditionalCharge(ServletRequestUtils.getDoubleParameter(request, "additionalCharge"));
						} catch(Exception e){}
						
						String priceGridId = ServletRequestUtils.getStringParameter(request, "priceGridId");
						
						if((siteConfig.get("ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE").getValue().equals("true") || !isMarkUpApplied) && product.getSalesTag() != null) {
							cartItem.getProduct().setSalesTag(product.getSalesTag());
						}
						if(product.isAsiIgnorePrice()) {
							cartItem.setUnitPrice(this.webJaguar.unitPriceByQty(cartItem.getQuantity(), product));
						} else if(prod != null){
							this.setPrice(cartItem, priceGridId, prod);
						} else if(asiProduct != null){
							cartItem.setAsiUnitPrice(ServletRequestUtils.getDoubleParameter(request, "asiUnitPrice"));
							cartItem.setAsiOriginalPrice(ServletRequestUtils.getDoubleParameter(request, "asiOriginalPrice"));
						}
						cartItem.getProduct().setId(product.getId());
						cartItem.getProduct().setSku(product.getSku());
						cartItem.getProduct().setAsiId(product.getAsiId());
						
						if (prod != null && product.getAsiId()!=null) {
							// get product attributes return an empty array if not found.
							String asiOptions[] = ServletRequestUtils.getStringParameters(request, "asi_option");
							String asiCriteriaSetIds[] = ServletRequestUtils.getStringParameters(request, "asi_criteria_id");
							if ( asiOptions.length != 0 ) {
								for ( int option_index = 0; option_index < asiOptions.length; option_index++ ) {
									String option_values[] = request.getParameterValues( "asi_option_values_"+asiCriteriaSetIds[option_index]);
									String option_price = ServletRequestUtils.getStringParameter(request, "asi_option_price_"+asiCriteriaSetIds[option_index]);
									if ( option_values != null ) {
										for ( int values_index = 0; values_index < option_values.length; values_index++ ) {	
											if(option_values[values_index] != null && !option_values[values_index].equals("")) {
												cartItem.addAsiProductAttribute( new ProductAttribute( asiOptions[option_index].toString(), option_values[values_index]+(option_price == null ? "" : " ( "+( option_price.equals("QUR") ? option_price : "$ "+option_price )+" )"), option_index ));
											}
										}
									}
								}
							}
						}
						
						if(asiProduct != null) {
							if ( ServletRequestUtils.getStringParameter(request, "optionNVPair") != null ) {
								String optionNVPairs[] = ServletRequestUtils.getStringParameters(request, "optionNVPair");
								for ( String optionNV : optionNVPairs) {
									try {
										cartItem.addProductAttribute( new ProductAttribute( optionNV.split("_value_")[0], optionNV.split("_value_")[1], true ) );
									}catch(Exception e){ }
								}
								if(ServletRequestUtils.getDoubleParameter(request, "additionalCharge") != null){
									cartItem.setAsiAdditionalCharge(ServletRequestUtils.getDoubleParameter(request, "additionalCharge"));
								}
							}
							/* UNDER DEVELOPMENT
							//attachment
							MultipartFile attachment = null;
							try {
								MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
								attachment = multipartRequest.getFile( "attachment_"+cartItem.getProduct().getSku() );
								
								if ( attachment != null && !attachment.isEmpty() ) {
									File baseFile = new File( getServletContext().getRealPath("temp"));
									if(!baseFile.exists()){
										baseFile.mkdir();
									}
									baseFile = new File( baseFile, "/Cart/" );
									if(!baseFile.exists()){
										baseFile.mkdir();
									}
								
									baseFile = new File( baseFile, (userId != null ? "/customer/" : "/session/") );
									if(!baseFile.exists()){
										baseFile.mkdir();
										baseFile.setWritable(true, true);
									}
									
									baseFile = new File( baseFile, (userId != null ? "/"+userId+"/" : "/"+request.getSession().getId()+"/") );
									if(!baseFile.exists()){
										baseFile.mkdir();
										baseFile.setWritable(true, true);
									}
									
									// save attachment
									if ( baseFile.canWrite() ) {
										File newFile = new File( baseFile, attachment.getOriginalFilename() );
										attachment.transferTo( newFile );
										cartItem.setAttachment(attachment.getOriginalFilename());
									}
								}
							} catch ( Exception e ){ e.printStackTrace(); }
							*/
						}
						
						if (userId != null) { 
							// save on database
							this.webJaguar.addToCart(cartItem, userId);
						}
						else{ 
							// store in session
							Cart cart = (Cart) WebUtils.getOrCreateSessionAttribute(request.getSession(), "sessionCart", Cart.class);
							cart.addCartItem(cartItem);	
						}
					}
					
		
				} catch (Exception e) { e.printStackTrace();}
		}
		
    	return new ModelAndView(new RedirectView("viewCart.jhtm"));
	}
	
	private void setPrice(CartItem cartItem, String priceGridId, asiProduct.Product product) throws Exception {
		
		Double unitPrice = null;
		if(product.getPricing() != null && product.getPricing().getPriceGrid() != null) {
			for(PriceGrid priceGrid : product.getPricing().getPriceGrid()) { 
				if(priceGrid.getId().equals(priceGridId)) {
					ListIterator<asiProduct.PriceGrid.Price> iter = priceGrid.getPrice().listIterator();
					while(iter.hasNext()) {
						asiProduct.PriceGrid.Price asiPrice = iter.next();
						try {
							int quantMin = Integer.parseInt(asiPrice.getQuantity().toString());
							int quantMax = Integer.parseInt(iter.next().getQuantity().toString());
							iter.previous();		
							
							if( cartItem.getQuantity() >= quantMin && cartItem.getQuantity() < quantMax) {
								unitPrice = Double.parseDouble(asiPrice.getValue());
								break;
							}
						} catch (Exception e) {
							int quantMin = Integer.parseInt(asiPrice.getQuantity().toString());
							if( cartItem.getQuantity() >= quantMin) {
								unitPrice = Double.parseDouble(asiPrice.getValue());
							}
						}
					}
				}
			}
		}
		//set original price before applying sales tag
		cartItem.setAsiOriginalPrice(unitPrice);
		
		// salesTag
		if(cartItem.getProduct().getSalesTag() != null && cartItem.getProduct().getSalesTag().getDiscount() > 0) {
			if(cartItem.getProduct().getSalesTag().isPercent()) {
				unitPrice = unitPrice * (100 - cartItem.getProduct().getSalesTag().getDiscount()) / 100;
			} else {
				unitPrice = unitPrice - cartItem.getProduct().getSalesTag().getDiscount();
			}
		}
		cartItem.setAsiUnitPrice(unitPrice);
	}	
}