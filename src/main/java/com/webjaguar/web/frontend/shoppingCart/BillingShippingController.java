/* 
 * @author Eduardo Asprec
 * @since 03.09.2011
 */

package com.webjaguar.web.frontend.shoppingCart;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.listener.SessionListener;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Cart;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UserSession;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.thirdparty.evergreen.EvergreenApi;
import com.webjaguar.web.form.CustomerForm;

public class BillingShippingController extends SimpleFormController {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;

	private JavaMailSenderImpl mailSender;

	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}

	public BillingShippingController() {
		setCommandName("customerForm");
		setCommandClass(CustomerForm.class);
		setSuccessView("checkout1.jhtm");
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {

		// check if login button was pressed
		if (request.getParameter("_login") != null) {
			if (checkLogin(request, response)) {
				return new ModelAndView(new RedirectView(getSuccessView()));
			} else {
				return showForm(request, response, errors);
			}
		}

		SalesRep salesRep = null;

		CustomerForm customerForm = (CustomerForm) command;
		customerForm.getCustomer().setUnsubscribe(!customerForm.isSubscribeEmail());
		customerForm.getCustomer().setTrackcode((String) request.getAttribute("trackcode"));
		customerForm.getCustomer().setRegisteredBy("frontend");

		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);
			customerForm.getCustomer().setHost(request.getHeader("host"));
		}

		if ((Integer) gSiteConfig.get("gAFFILIATE") > 0) {
			String promocodeLink = (String) request.getAttribute("promocode");
			if (promocodeLink != null && !promocodeLink.equals("")) {
				Integer ownerId = this.webJaguar.getUserIdByPromoCode(promocodeLink);
				if (ownerId != null) {
					customerForm.getCustomer().setAffiliateParent(ownerId);
				} else {
					customerForm.getCustomer().setAffiliateParent(0);
				}
			} else {
				customerForm.getCustomer().setAffiliateParent(0);
			}
		} else {
			customerForm.getCustomer().setAffiliateParent(0); // by default new customer connect to root.
		}
		try {
			if (siteConfig.get("CUSTOMER_IP_ADDRESS").getValue().equals("true")) {
				customerForm.getCustomer().setNote(
						"--START-- \nOn " + new Date() + " \n\"" + customerForm.getCustomer().getAddress().getFirstName() + " " + customerForm.getCustomer().getAddress().getLastName()
								+ "\" was trying to register with this IP ADDRESS: " + request.getRemoteAddr() + "\n--END--\n\n");
			}
			customerForm.getCustomer().setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_NEW_REGISTRANT").getValue());
			if (customerForm.getCustomer().getTrackcode() != null && siteConfig.get("TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT").getValue().trim().length() > 0) {
				if (!siteConfig.get("TRACK_CODE").getValue().equals("")
						&& siteConfig.get("TRACK_CODE").getValue().trim().toLowerCase().equals(customerForm.getCustomer().getTrackcode().trim().toLowerCase())) {
					customerForm.getCustomer().setProtectedAccess(siteConfig.get("TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT").getValue());
				}
			}
			if ((Boolean) gSiteConfig.get("gSALES_REP") && siteConfig.get("TERRITORY_ZIPCODE").getValue().equals("true")) {
				salesRep = this.webJaguar.getSalesRepByTerritoryZipcode(customerForm.getCustomer().getAddress().getZip());
				customerForm.getCustomer().setSalesRepId((salesRep == null) ? null : salesRep.getId());
			}
			// generate random cardID
			generateRandomCardID(customerForm.getCustomer());

			if (customerForm.isSameAsBilling()) {
				customerForm.setShipping(null);
			}
			if (siteConfig.get("CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER").getValue().equals("true")) {
				customerForm.getCustomer().setAutoGenerateAccountNumber(true);
			} else {
				customerForm.getCustomer().setAutoGenerateAccountNumber(false);
			}
			
			String parameterTrackcode = (String)request.getSession().getAttribute("parameterTrackcode");
			String crmTrackcode = (String)request.getSession().getAttribute("crmTrackcode");
				
			String cId = (String)request.getSession().getAttribute("ccid");
			Integer crmid = -1;
			try{
					crmid = Integer.parseInt(cId);
				}catch(Exception ex){	
			}
			
			CrmContact crmContact = this.webJaguar.getCrmContactById(crmid);
			
			if(crmContact==null){
				try{
					crmid = (Integer)request.getSession().getAttribute("crmid");
					crmContact = this.webJaguar.getCrmContactById(crmid);
				}catch(Exception ex){
				}
				
			}
			
			if(crmContact!=null && customerForm.getCustomer()!=null){
				if(crmContact.getTrackcode()!=null && crmContact.getTrackcode().length()>0 && crmContact.getEmail1().trim().equalsIgnoreCase(customerForm.getCustomer().getUsername().trim())){
					String trk = crmContact.getTrackcode() + ">>" + customerForm.getCustomer().getTrackcode();
					customerForm.getCustomer().setTrackcode(trk);
					crmContact.setTrackcode(trk);
					this.webJaguar.updateCrmContact(crmContact);
				}	
			}else{
				if(customerForm.getCustomer()!=null && crmTrackcode!=null && crmTrackcode.length()>0 && parameterTrackcode!=null && parameterTrackcode.length()>0){
					String finalTrackcode = crmTrackcode + ">>" + parameterTrackcode;
					crmContact.setTrackcode(finalTrackcode);
					this.webJaguar.updateCrmContact(crmContact);
					customerForm.getCustomer().setTrackcode(finalTrackcode);
				}else{
					try{
						List<CrmContact> list = this.webJaguar.getCrmContactByEmail1(customerForm.getCustomer().getUsername());
						if(list!=null && list.size()>0){
							CrmContact tmp = list.get(0);
							String finalTrackcode = null;
							if(tmp!=null && tmp.getTrackcode()!=null && tmp.getTrackcode().length()>0 && parameterTrackcode!=null && parameterTrackcode.length()>0){
								finalTrackcode = tmp.getTrackcode() + ">>" + parameterTrackcode;
								tmp.setTrackcode(finalTrackcode);
								this.webJaguar.updateCrmContact(tmp);
							}else{
								finalTrackcode = parameterTrackcode;
							}
							customerForm.getCustomer().setTrackcode(finalTrackcode);
						}else{
							customerForm.getCustomer().setTrackcode((String) request.getAttribute("trackcode"));
						}
					}catch(Exception ex){
						customerForm.getCustomer().setTrackcode((String) request.getAttribute("trackcode"));
					}
				}
			}
			
			this.webJaguar.insertCustomer(customerForm.getCustomer(), customerForm.getShipping(), (Boolean) gSiteConfig.get("gCRM"));
		} catch (DataIntegrityViolationException ex) {
			errors.rejectValue("customer.username", "USER_ID_ALREADY_EXISTS", "An account using this email address already exists.");
			customerForm.setShipping(new Address());
			return showForm(request, response, errors);
		}

		SiteMessage siteMessage = null;
		try {
			Integer messageId = null;
			if (multiStore != null) {
				messageId = multiStore.getMidNewRegistration();
			} else {
				messageId = Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_NEW_REGISTRATION").getValue());
			}
			siteMessage = this.webJaguar.getSiteMessageById(messageId);
			if (siteMessage != null) {
				// send email
				sendEmail(siteMessage, request, customerForm.getCustomer(), multiStore, salesRep);
			}
		} catch (NumberFormatException e) {
			// do nothing
		}

		// evergreen
		if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
			// submit customer to evergreen
			EvergreenApi evergreen = new EvergreenApi();
			try {
				this.webJaguar.updateEvergreenCustomerSuc(customerForm.getCustomer().getId(), evergreen.SetClient(customerForm.getCustomer(), customerForm.getCustomer().getAddress()));
			} catch (Exception e) {
				
				e.printStackTrace();
				notifyAdmin(siteConfig, "Error submitting customer " + customerForm.getCustomer().getId() + " to evergreen webservice.\n" + e.toString());
			}
		}

		UserSession userSession = new UserSession();
		userSession.setUsername(customerForm.getCustomer().getUsername());
		userSession.setFirstName(customerForm.getCustomer().getAddress().getFirstName());
		userSession.setLastName(customerForm.getCustomer().getAddress().getLastName());
		userSession.setUserid(customerForm.getCustomer().getId());

		this.webJaguar.setUserSession(request, userSession);

		SessionListener.getActiveSessions().put(request.getSession().getId(), userSession.getUserid());

		Cart cart = (Cart) request.getSession().getAttribute("sessionCart");
		if (cart != null) {
			this.webJaguar.mergeCart(cart, userSession.getUserid());
			request.getSession().setAttribute("userCart", cart);
			request.getSession().removeAttribute("sessionCart");
		}
		return new ModelAndView(new RedirectView(getSuccessView()));
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {

		Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>();
		Map<String, Object> model = new HashMap<String, Object>();

		// get a list of enabled country code from database
		model.put("countrylist", this.webJaguar.getEnabledCountryList());
		model.put("statelist", this.webJaguar.getStateList("US"));
		model.put("caProvinceList", this.webJaguar.getStateList("CA"));
		map.put("model", model);

		Layout layout = (Layout) request.getAttribute("layout");
		layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Billing Shipping Information"));
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} else {
			model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		}
		this.webJaguar.updateCartQuantity(null, request, layout);
		model.put("billingShippingLayout", this.webJaguar.getSystemLayout("billingShipping", request.getHeader("host"), request));

		return map;
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_login") != null) {
			return true;
		}
		return false;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		CustomerForm customerForm = (CustomerForm) command;

		// password is required
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.password", "form.required", "required");

		if ((Boolean) gSiteConfig.get("gTAX_EXEMPTION") && siteConfig.get("TAX_ID_REQUIRED").getValue().equalsIgnoreCase("true")) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.taxId", "form.required", "required");
		}

		if (!customerForm.isSameAsBilling()) {
			// check shipping
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.firstName", "form.required", "required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.lastName", "form.required", "required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.addr1", "form.required", "required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.city", "form.required", "required");
			if (customerForm.getShipping().isStateProvinceNA()) {
				customerForm.getShipping().setStateProvince("");
			} else {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.stateProvince", "form.required", "required");
			}
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.zip", "form.required", "required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.country", "form.required", "required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.phone", "form.required", "required");
			if (customerForm.isCompanyRequired()) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.company", "form.required", "required");
			}
		}

		if (errors.getErrorCount() == 0) {
			// check email against database.
			if (this.webJaguar.getCustomerByUsername(customerForm.getCustomer().getUsername()) != null) {
				errors.rejectValue("customer.username", "USER_ID_ALREADY_EXISTS", "An account using this email address already exists.");
			}
		}
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		setFormView("frontend/checkout1/billingShipping");
		if (siteConfig.get("CHECKOUT1_DIRECTORY").getValue().length() > 0) {
			setFormView(siteConfig.get("CHECKOUT1_DIRECTORY").getValue() + "/checkout1/billingShipping");
		}

		if (!(Boolean) gSiteConfig.get("gSHOPPING_CART_1")) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("viewCart.jhtm")));
		}

		// check if customer is logged in
		if (this.webJaguar.getUserSession(request) != null) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("checkout1.jhtm")));
		}

		CustomerForm customerForm = new CustomerForm();

		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			setFormView("frontend/layout/template6/checkout1/billingShipping");
		}
		
		if (siteConfig.get("CUSTOMER_MASS_EMAIL_SUBSCRIBE_DEFAULT").getValue().equals("true")) {
			customerForm.setSubscribeEmail(true);
		}

		if (siteConfig.get("CUSTOMERS_REQUIRED_COMPANY").getValue().equals("true")) {
			customerForm.setCompanyRequired(true);
		}

		customerForm.setShipping(new Address());
		customerForm.getCustomer().getAddress().setCountry("US");
		customerForm.getShipping().setCountry("US");
		customerForm.setSameAsBilling(true);

		return customerForm;
	}

	private void sendEmail(SiteMessage siteMessage, HttpServletRequest request, Customer customer, MultiStore multiStore, SalesRep salesRep) {

		// send email
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		if (multiStore != null) {
			contactEmail = multiStore.getContactEmail();
		}
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));
		Format formatter2 = new SimpleDateFormat("MM/dd/yyyy");

		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append(dateFormatter.format(new Date()) + "\n\n");

		String firstName = customer.getAddress().getFirstName();
		if (firstName == null) {
			firstName = "";
		}
		String lastname = customer.getAddress().getLastName();
		if (lastname == null) {
			lastname = "";
		}

		// subject
		siteMessage.setSubject(siteMessage.getSubject().replace("#email#", customer.getUsername()));
		siteMessage.setSubject(siteMessage.getSubject().replace("#password#", customer.getPassword()));
		siteMessage.setSubject(siteMessage.getSubject().replace("#firstname#", firstName));
		siteMessage.setSubject(siteMessage.getSubject().replace("#lastname#", lastname));
		siteMessage.setSubject(siteMessage.getSubject().replace("#date#", formatter2.format(new Date()).toString()));

		if (salesRep != null) {
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepName#", salesRep.getName()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepEmail#", salesRep.getEmail()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		// message
		siteMessage.setMessage(siteMessage.getMessage().replace("#email#", customer.getUsername()));
		siteMessage.setMessage(siteMessage.getMessage().replace("#password#", customer.getPassword()));
		siteMessage.setMessage(siteMessage.getMessage().replace("#firstname#", firstName));
		siteMessage.setMessage(siteMessage.getMessage().replace("#lastname#", lastname));
		siteMessage.setMessage(siteMessage.getMessage().replace("#date#", formatter2.format(new Date()).toString()));

		if (salesRep != null) {
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepName#", salesRep.getName()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepEmail#", salesRep.getEmail()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		messageBuffer.append(siteMessage.getMessage());

		try {
			// construct email message
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.addBcc(contactEmail);
			helper.setSubject(siteMessage.getSubject());
			helper.setText(messageBuffer.toString(), siteMessage.isHtml());
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing
		}
		if (salesRep != null && ((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
			messageBuffer.append("<br /><br />New Customer Info:<br />Name:<b>" + customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName() + "</b>");
			messageBuffer.append("<br />Company Name:<b>" + customer.getAddress().getCompany() + "</b>");
			messageBuffer.append("<br />Address1:<b>" + customer.getAddress().getAddr1() + "</b>");
			messageBuffer.append("<br />Address2:<b>" + ((customer.getAddress().getAddr2() == null) ? "" : customer.getAddress().getAddr2()) + "</b>");
			messageBuffer.append("<br />City:<b>" + customer.getAddress().getCity() + "</b> State:<b> " + customer.getAddress().getStateProvince() + "</b> Zipcode:<b> "
					+ customer.getAddress().getZip() + "</b>");
			messageBuffer.append("<br />Phone:<b>" + customer.getAddress().getPhone() + "</b>");
			messageBuffer.append("<br />Email:<b>" + customer.getUsername() + "</b>");
			try {
				// construct email message
				MimeMessage mms = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
				helper.setTo(salesRep.getEmail());
				helper.setFrom(contactEmail);
				helper.setCc(contactEmail);

				helper.setSubject(siteMessage.getSubject());
				helper.setText(messageBuffer.toString(), siteMessage.isHtml());
				mailSender.send(mms);
			} catch (Exception ex) {
				// do nothing
			}
		}
	}

	private void notifyAdmin(Map<String, Configuration> siteConfig, String message) {
		// send email notification
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		String siteURL = siteConfig.get("SITE_URL").getValue();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setBcc("developers@advancedemedia.com");
		msg.setSubject("Registering on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}

	private void generateRandomCardID(Customer customer) {
		String cardID = UUID.randomUUID().toString().toUpperCase().replace("-", "").substring(0, 9);
		if (this.webJaguar.getCustomerByCardID(cardID) == null) {
			customer.setCardId(cardID);
		} else
			generateRandomCardID(customer);
	}

	private boolean checkLogin(HttpServletRequest request, HttpServletResponse response) {
		
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		String username = ServletRequestUtils.getStringParameter( request, "username", "" );
		String username2 = ServletRequestUtils.getStringParameter( request, "username", "" );
		String password = ServletRequestUtils.getStringParameter(request, "password", "");

		Customer customer = null;
		String accountNum = request.getParameter("accountNum");
		String cardId = ServletRequestUtils.getStringParameter( request, "cardid", "" );
		
		if (username.length() > 0 && password.length() > 0) {
			customer = this.webJaguar.getCustomerByUsernameAndPassword(username, password);
		}
		
		if(customer == null){
			if (username2.length() > 0 && password.length() > 0) {
				customer = this.webJaguar.getCellPhoneByUsernameAndPassword(username2, password);
			}else if ((Boolean) gSiteConfig.get( "gREGISTRATION_TOUCH_SCREEN" ) && cardId.length() > 0) {
				customer = this.webJaguar.getCustomerByCardIdAndPassword( cardId, password );
			}
		}

		boolean validIpAddress = true;
		if (siteConfig.get("CUSTOMER_IP_ADDRESS").getValue().equals("true") && customer != null && customer.getIpAddress() != null) {
			String[] ips = customer.getIpAddress().split("[,\n]"); // split by line breaks or commas
			for (int x = 0; x < ips.length; x++) {
				String ip = ips[x].trim();
				if (!("".equals(ip))) {
					if (x == 0) {
						validIpAddress = false;
					}
					if (request.getRemoteAddr().equals(ip)) {
						validIpAddress = true;
						break;
					}
				}
			}
		}
		boolean hasProtectedAccess = true;
		if ((Integer) gSiteConfig.get("gPROTECTED") > 0 && siteConfig.get("PROTECTED_HOST").getValue().equals(request.getHeader("host"))) {
			if (customer != null) {
				hasProtectedAccess = hasProtectedAccess(siteConfig.get("PROTECTED_HOST_PROTECTED_LEVEL").getValue(), customer.getProtectedAccess());
			}
		}

		if (customer == null || (customer != null && customer.isSuspended()) || (!validIpAddress) || !hasProtectedAccess) {
			if ((customer != null && customer.isSuspended())) {
				request.setAttribute("message", "LOGIN_SUSPENDED");
			} else if (!validIpAddress) {
				request.setAttribute("message", "LOGIN_IPADDRESS_NOTVALID");
				customer.setNote(customer.getNote().concat(
						"--START-- \nOn " + new Date() + " \n\"" + customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName() + "\" was trying to login with this IP ADDRESS: "
								+ request.getRemoteAddr() + "\n--END--\n\n"));
				this.webJaguar.updateCustomerNote(customer);
				notifyCustomer(request, customer);
			} else if (!hasProtectedAccess) {
				request.setAttribute("message", "LOGIN_INVALID_FOR_DOMAIN");
			} else {
				request.setAttribute("message", "LOGIN_INVALID");
			}

			return false;
		} else {
			// update login statistics
			this.webJaguar.nonTransactionSafeLoginStats(customer);

			// set user session
			UserSession userSession = new UserSession();
			userSession.setUsername(customer.getUsername());
			userSession.setFirstName(customer.getAddress().getFirstName());
			userSession.setLastName(customer.getAddress().getLastName());
			userSession.setUserid(customer.getId());
			this.webJaguar.setUserSession(request, userSession);

			SessionListener.getActiveSessions().put(request.getSession().getId(), customer.getId());

			Cart cart = (Cart) request.getSession().getAttribute("sessionCart");
			if (cart != null) {
				// clear saved cart to avoid confusion and surprises
				// this.webJaguar.deleteShoppingCart(userSession.getUserid(), null);
				this.webJaguar.mergeCart(cart, userSession.getUserid());
				request.getSession().setAttribute("userCart", cart);
				request.getSession().removeAttribute("sessionCart");
			}
			return true;
		}
	}

	private BitSet getBitSet(String bitString) {
		BitSet bitSet = new BitSet();
		StringBuffer sbuff = new StringBuffer(bitString);
		sbuff.reverse();
		for (int i = 0; i < sbuff.length(); i++) {
			if (sbuff.charAt(i) == '1') {
				bitSet.set(i);
			}
		}
		return bitSet;
	}

	private boolean hasProtectedAccess(String protectedLevel, String protectedAccess) {
		BitSet oldPaBitSet = getBitSet(protectedAccess);
		BitSet newPaBitSet = getBitSet(protectedAccess);
		BitSet plBitSet = getBitSet(protectedLevel);
		newPaBitSet.or(plBitSet);

		return (oldPaBitSet.equals(newPaBitSet));
	}

	private void notifyCustomer(HttpServletRequest request, Customer customer) {

		// send email
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));

		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(customer.getUsername());
		msg.setSubject("Customer Access Denied!");
		msg.setText("" + customer.getAddress().getFirstName() + ", " + customer.getAddress().getLastName() + " Access denied.\n\n" + "On:\t" + dateFormatter.format(new Date()) + "\n"
				+ "A note has been added to the customer note section.\n");
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}
