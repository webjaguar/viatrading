package com.webjaguar.web.frontend.shoppingCart;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.SimpleFormController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.thirdparty.payment.paypalpro.PayPalProApi;
import com.webjaguar.thirdparty.payment.paypalpro.PayPalProResponse;
import com.webjaguar.web.form.OrderForm;


public class PayPalProAccountCheckoutController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;	
	public void setWebJaguar( WebJaguarFacade webJaguar ) { this.webJaguar = webJaguar; }
	
	private MailSender mailSender;
	public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
	public String token;
	public String payerId;
	public Integer orderId;
	
	public PayPalProAccountCheckoutController() {
		setCommandName("orderForm");
		setCommandClass( OrderForm.class );
		setFormView("frontend/checkout/payPalProUseMyAccount/preliminary");
	}	
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors)  throws Exception {
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map<String, Object> model = new HashMap<String, Object>();
		
		OrderForm orderForm =(OrderForm) command;
		PayPalProApi payPalProApi = new PayPalProApi();
		
		PayPalProResponse payPalResponse = payPalProApi.DoExpressCheckout(token,payerId, siteConfig, orderForm.getOrder().getGrandTotal());
		OrderStatus orderStatus = new OrderStatus();
		orderForm.getOrder().setPaymentMethod("PayPalAccount");
		orderStatus.setOrderId(orderId);
		
		if (payPalResponse.getSuccess().equals(true)) {
			orderStatus.setStatus("p");
			if (payPalResponse.getTransactionId() != null) {			
				orderStatus.setComments("Transaction ID:" +payPalResponse.getTransactionId());
			} 
			model.put("invoiceLayout", this.webJaguar.getSystemLayout("finvoice", request.getHeader("host"), request));
			model.put("order", orderForm.getOrder());
			model.put("orderForm", orderForm);
			model.put("username", orderForm.getCustomer().getUsername());
			this.webJaguar.updateOrder(orderForm.getOrder());
			this.webJaguar.insertOrderStatus(orderStatus);
			return new ModelAndView ("frontend/checkout/payPalProUseMyAccount/finalinvoice", model);	
		} else {
			orderStatus.setStatus("xp");
			if (payPalResponse.getPendingReason() != null) {
				orderStatus.setComments("Pending Reason:" +payPalResponse.getPendingReason());
			} else if (payPalResponse.getLLongMessage() != null) {
				orderStatus.setComments("Error Reason:" +payPalResponse.getLLongMessage());				
			}
			this.webJaguar.updateOrder(orderForm.getOrder());
			this.webJaguar.insertOrderStatus(orderStatus);
			notifyAdmin("PayPalProAccountErrorMessage " + siteConfig.get("SITE_URL").getValue(), payPalResponse.getLLongMessage() +".Error Code: "+payPalResponse.getLErrorCode() );
			model.put("message", "Payment is not completed. PayPal din't accept the transaction");
			return showForm(request, response, errors, model);	
		}		
	}
	
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("invoiceLayout", this.webJaguar.getSystemLayout("pinvoice", request.getHeader("host"), request));
		map.put("payPalToken", token);
		map.put("payPalPayerId", payerId);
		map.put("payPalOrderId", orderId);
		return map;
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws ModelAndViewDefiningException {
		
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();	
		
		PayPalProApi payPalProApi = new PayPalProApi();	
		token = request.getParameter("token");
		payerId = request.getParameter("PayerID");
		orderId =Integer.parseInt( request.getParameter("order_id"));
		
		OrderForm orderForm = new OrderForm();
		
		if (token != null && payerId != null && orderId != null) {
			
			Address shippingAddress = payPalProApi.getExpressCheckout(token, siteConfig);
			
			Order order = this.webJaguar.getOrder(orderId, null);	
			Customer customer = this.webJaguar.getCustomerById(order.getUserId());		
			orderForm.setCustomer(customer);
			orderForm.getOrder().setLineItems(order.getLineItems());		
			orderForm.getOrder().payPalProOrder( customer, order);
			orderForm.getOrder().setShipping(shippingAddress);
			orderForm.setUsername(customer.getUsername());		
		}
		return orderForm;
	}
	
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
}
