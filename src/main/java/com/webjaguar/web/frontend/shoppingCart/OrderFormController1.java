/* 
 * @author Eduardo Asprec
 * @since 03.07.2011
 */

package com.webjaguar.web.frontend.shoppingCart;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.internet.MimeMessage;
import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.buySafe.ShoppingCartAddUpdateRS;
import com.buySafe.ShoppingCartCheckoutRS;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Brand;
import com.webjaguar.model.BudgetProduct;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CreditCard;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Payment;
import com.webjaguar.model.Product;
import com.webjaguar.model.Promo;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.ShippingObj;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.Supplier;
import com.webjaguar.model.UserSession;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.thirdparty.evergreen.EvergreenApi;
import com.webjaguar.thirdparty.linkShare.LinkShare;
import com.webjaguar.thirdparty.payment.authorizenet.AuthorizeNetApi;
import com.webjaguar.thirdparty.payment.cardinal.CentinelApi;
import com.webjaguar.thirdparty.payment.eBizCharge.EBizChargePaymentApi;
import com.webjaguar.thirdparty.payment.ezic.EzicApi;
import com.webjaguar.thirdparty.payment.geMoney.GEMoney;
import com.webjaguar.thirdparty.payment.geMoney.GEMoneyValidator;
import com.webjaguar.thirdparty.payment.paymentech.PaymentechApi;
import com.webjaguar.thirdparty.payment.paypalpro.PayPalProApi;
import com.webjaguar.thirdparty.payment.payrover.PayRoverApi;
import com.webjaguar.web.admin.customer.PaymentImp;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.Encoder;
import com.webjaguar.web.form.OrderForm;
import com.webjaguar.web.validator.OrderFormValidator;

public class OrderFormController1 extends AbstractWizardFormController {
	private static final Logger logger = LoggerFactory.getLogger(OrderFormController1.class);

	private GlobalDao globalDao;

	public void setGlobalDao(GlobalDao globalDao) {
		this.globalDao = globalDao;
	}

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private JavaMailSenderImpl mailSender;

	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}

	private Properties site;

	public void setSite(Properties site) {
		this.site = site;
	}
	
	private String shipping;
	private Double totalCharges;
	private String message;

	private Map<String, Object> gSiteConfig;
	private Map<String, Configuration> siteConfig;
	private HashMap<String, String> err = new HashMap<String, String>();

	public OrderFormController1() {
		setCommandName("orderForm");
		setCommandClass(OrderForm.class);
	}

	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
		SimpleDateFormat df = new SimpleDateFormat("MM-dd-yyyy", RequestContextUtils.getLocale(request));
		binder.registerCustomEditor(Date.class, new CustomDateEditor(df, true, 10));
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		OrderForm orderForm = (OrderForm) command;

		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> myModel = new HashMap<String, Object>();

		if (ServletRequestUtils.getStringParameter(request, "_modify") != null) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("viewCart.jhtm")));
		}

		Layout layout = (Layout) request.getAttribute("layout");
		layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Preliminary Invoice"));
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} else {
			myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request, null));
		}
		// add master sku before child skus; just for display purpose
		this.updateMasterSkus(orderForm.getOrder(), myModel);

		// hide right bar
		layout.setRightBarTopHtml("");
		layout.setRightBarBottomHtml("");

		if (ServletRequestUtils.getStringParameter(request, "_finish") != null) {
			myModel.put("finish", true);
		}

		switch (page) {
		case 0:
			myModel.put("countrylist", this.webJaguar.getEnabledCountryList());
			myModel.put("statelist", this.webJaguar.getStateList("US"));
			myModel.put("caProvinceList", this.webJaguar.getStateList("CA"));
			myModel.put("addressBook", this.webJaguar.getAddressListByUserid(orderForm.getOrder().getUserId()));
			myModel.put("shippingLayout", this.webJaguar.getSystemLayout("shipping1", request.getHeader("host"), request));
			break;
		case 1:
			myModel.put("countrylist", this.webJaguar.getEnabledCountryList());
			myModel.put("statelist", this.webJaguar.getStateList("US"));
			myModel.put("caProvinceList", this.webJaguar.getStateList("CA"));
			myModel.put("addressBook", this.webJaguar.getAddressListByUserid(orderForm.getOrder().getUserId()));
			myModel.put("billingLayout", this.webJaguar.getSystemLayout("billing1", request.getHeader("host"), request));
			break;
		case 2:

			// custom payments
			Integer custId = null;
			if ((Boolean) gSiteConfig.get("gGROUP_CUSTOMER")) {
				custId = orderForm.getOrder().getUserId();
			}
			myModel.put("customPayments", this.webJaguar.getCustomPaymentMethodList(custId));

			// multi store
			// map.put("invoiceLayout", this.webJaguar.getSystemLayout("pinvoice1", request.getHeader("host"), request));
			layout = this.webJaguar.getSystemLayout("pinvoice1", request.getHeader("host"), request);
			if (layout != null && layout.getTopBarHtml() != null)
				layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Sales Invoice"));
			this.webJaguar.updateCartQuantity(null, request, layout);
			map.put("invoiceLayout", layout);

			if ((Boolean) gSiteConfig.get("gSHOW_ELIGIBLE_PROMOS")) {
				map.put("eligiblePromos", this.webJaguar.getEligiblePromoList(orderForm.getCustomer(), ((Boolean) gSiteConfig.get("gMANUFACTURER") ? orderForm.getOrder() : null)));
			}

			// expireYears
			myModel.put("expireYears", Constants.getYears(10));

			// eBizCharge
			if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ebizcharge") && siteConfig.get("PCI_FRIENDLY_EBIZCHARGE").getValue().equalsIgnoreCase("true")
					&& orderForm.getCustomer().getGatewayToken() != null) {
				EBizChargePaymentApi eBizChargePayment = new EBizChargePaymentApi(siteConfig, orderForm.getOrder().getIpAddress());
				eBizChargePayment.bankCardGetCustomer(orderForm.getCustomer(), orderForm.getOrder(), false);

				if (orderForm.getCustomer().getGatewayPaymentMethods() != null && orderForm.getCustomer().getGatewayPaymentMethods().size() > 0) {
					myModel.put("gatewayPaymentMethods", orderForm.getCustomer().getGatewayPaymentMethods());
				}

				if (request.getParameter("__remove_payment") != null && !orderForm.getOrder().getCcToken().equalsIgnoreCase("0") && !orderForm.getOrder().getCcToken().equalsIgnoreCase("-2")) {
					eBizChargePayment.bankCardDelCustomerPaymentMethod(orderForm.getCustomer(), orderForm.getOrder(), true);
					myModel.put("gatewayPaymentMethods", orderForm.getCustomer().getGatewayPaymentMethods());
					CreditCard creditCard = new CreditCard();
					orderForm.getOrder().setCreditCard(creditCard);
				}
			}
			// for affilaiet promo
			err.put("orderError", "");
			this.webJaguar.validatePromoCode(orderForm.getOrder(), err, orderForm.getCustomer(), orderForm.getPromocodeLink(), gSiteConfig);
			if (err.get("orderError") != null && !err.get("orderError").equals("")) {
				orderForm.setTempPromoErrorMessage(err.get("orderError"));

			}

			// check paymentMethod via trading only use customer paymentMethod & if is "paypal" or "credit card", add 3% CC Fee
			String paymentMethod = orderForm.getOrder().getPaymentMethod();
			if (paymentMethod != null && !paymentMethod.trim().equals("") && (paymentMethod.toLowerCase().contains("paypal") || paymentMethod.toLowerCase().contains("credit card"))) {
				orderForm.getOrder().setCcFeePercent(true);
				orderForm.getOrder().setCcFeeRate(3.00);
				orderForm.getOrder().setCcFeeAmount();
				orderForm.getOrder().setGrandTotal();
			}

			if (orderForm.getOrder().isPromoAvailable()) {
				orderForm.setTempPromoErrorMessage(null);
				this.webJaguar.updateGrandTotal(gSiteConfig, orderForm.getOrder());
			}
			this.webJaguar.updateTaxAndTotal(gSiteConfig, globalDao, orderForm.getOrder());
			// this code make sure that links appear on preliminary invoice for all attachemnts
			// needs some work as same code appears in case 3
			if (orderForm.getCustomerFolder() != null) {
				File file[] = orderForm.getCustomerFolder().listFiles();
				List<Map<String, Object>> attachedFiles = new ArrayList<Map<String, Object>>();
				for (int f = 0; f < file.length; f++) {
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					long size = file[f].length();
					fileMap.put("size", size);
					attachedFiles.add(fileMap);
				}
				if (!attachedFiles.isEmpty()) {
					map.put("attachedFiles", attachedFiles);
				}
			}
			break;
		case 3:

			layout = this.webJaguar.getSystemLayout("attachFiles", request.getHeader("host"), request);
			if (layout != null && layout.getTopBarHtml() != null)
				layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Sales Invoice"));
			this.webJaguar.updateCartQuantity(null, request, layout);
			myModel.put("attachFilesLayout", layout);

			// this code make sure that links appear on page for edit attachments
			// needs some work as same code appears in case 2
			if (orderForm.getCustomerFolder() != null) {
				File file[] = orderForm.getCustomerFolder().listFiles();
				List<Map<String, Object>> attachedFiles = new ArrayList<Map<String, Object>>();
				for (int f = 0; f < file.length; f++) {
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					long size = file[f].length();
					fileMap.put("size", size);
					attachedFiles.add(fileMap);
				}
				if (!attachedFiles.isEmpty()) {
					map.put("attachedFiles", attachedFiles);
				}
			}
			break;
		case 4:
			layout = this.webJaguar.getSystemLayout("cardinalCentinel", request.getHeader("host"), request);
			if (layout != null && layout.getTopBarHtml() != null)
				layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Sales Invoice"));
			this.webJaguar.updateCartQuantity(null, request, layout);
			myModel.put("cardinalCentinelLayout", layout);
			break;
		}
		map.put("countries", this.webJaguar.getCountryMap());
		shippingCalculate(myModel, request, orderForm);
		updateShipping(orderForm);
		this.webJaguar.updateTaxAndTotal(gSiteConfig, globalDao, orderForm.getOrder());
				
    	
		if((String) request.getSession().getAttribute("pro2")!=null){
	    	
			map.put("promoCode", (String) request.getSession().getAttribute("pro2"));
		}
		
		UserSession userSession = (UserSession) request.getAttribute("userSession");
		if(userSession!=null){
			map.put("promoCode", userSession.getPromocode());
			
		}

		map.put("model", myModel);
		return map;
	}
	
	private void shippingCalculate(Map<String, Object> map, HttpServletRequest request, OrderForm orderForm){
		
		UserSession userSession = (UserSession) request.getAttribute("userSession");
		Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());

		String manufacturerName = (String) request.getSession().getAttribute("manufacturerName");
		Cart cart = this.webJaguar.getUserCart(userSession.getUserid(), manufacturerName);
		cart.setUserId(userSession.getUserid());
		cart.setPromoCode((String) request.getSession().getAttribute("promocode"));
		this.webJaguar.updateCart(cart, request);
		
		List<ShippingObj> shippingList = new ArrayList<ShippingObj>();
		
		map.put("shipping", null);
		map.put("totalCharges", null);
		map.put("message", null);
		
		StringBuilder userInfo = null;
		if (customer != null && customer.getAddress() != null) {
			
			userInfo = new StringBuilder();

			StringBuilder load = new StringBuilder();
			StringBuilder loadQty = new StringBuilder();
			StringBuilder loadTotalWeight = new StringBuilder();
			StringBuilder loadTotalPallets = new StringBuilder();
			StringBuilder loadTotalCases = new StringBuilder();

			userInfo.append("Email=");
			userInfo.append(customer.getUsername());
			userInfo.append("&Address1=");
			userInfo.append(customer.getAddress().getAddr1());
			userInfo.append("&Address2=");
			userInfo.append(customer.getAddress().getAddr2());
			userInfo.append("&City=");
			userInfo.append(customer.getAddress().getCity());
			userInfo.append("&State=");
			userInfo.append(customer.getAddress().getStateProvince());
			userInfo.append("&Zip=");
			userInfo.append(customer.getAddress().getZip());
			userInfo.append("&Country=");
			userInfo.append(customer.getAddress().getCountry());
			userInfo.append("&Residential=");
			userInfo.append(customer.getAddress().isResidential() ? "1" : "0");
			userInfo.append("&Liftgate=");
			userInfo.append(customer.getAddress().isLiftGate() ? "1" : "0");
			userInfo.append("&DeclaredValue=1");
			userInfo.append("&Show=All");

			load.append("&Load=[\"");
			loadQty.append("&LoadQty=[\"");
			loadTotalWeight.append("&LoadTotalWeight=[\"");
			loadTotalPallets.append("&LoadTotalPallets=[\"");
			loadTotalCases.append("&LoadTotalCases=[\"");
			int index = 0;

			for (CartItem cartItem : cart.getCartItems()) {
				cartItem.setProduct(this.webJaguar.getProductById(cartItem.getProduct().getId(), request));
				if (index > 0) {
					load.append("\",\"");
					loadQty.append("\",\"");
					loadTotalWeight.append("\",\"");
					loadTotalPallets.append("\",\"");
					loadTotalCases.append("\",\"");
				}
				index++;
				load.append(cartItem.getProduct().getSku());
				loadQty.append(cartItem.getQuantity());
				loadTotalWeight.append(cartItem.getProduct().getWeight()*cartItem.getQuantity());

				Integer pallets = 0;
				try {
					pallets = Integer.parseInt(cartItem.getProduct().getField15()) * cartItem.getQuantity();
				} catch (Exception ex) {
				}

				loadTotalPallets.append(pallets);

				Double upsMaxItemsInPackage = 0.0;
				try {
					upsMaxItemsInPackage = cartItem.getProduct().getUpsMaxItemsInPackage() * cartItem.getQuantity();
				} catch (Exception ex) {
				}
				loadTotalCases.append(upsMaxItemsInPackage);
			}

			load.append("\"]");
			loadQty.append("\"]");
			loadTotalWeight.append("\"]");
			loadTotalPallets.append("\"]");
			loadTotalCases.append("\"]");

			userInfo.append(load.toString()).append(loadQty.toString()).append(loadTotalWeight.toString()).append(loadTotalPallets.toString()).append(loadTotalCases.toString());
			String urlRes = null;
			
			
			try {
						
				String urlParameters = userInfo.toString();
				urlParameters=urlParameters.replaceAll("\"", "%22");
				urlParameters=urlParameters.replaceAll(" ", "%20");
				URL url = new URL(request.getScheme()+"://www.viatrading.com/dv/liveapps/shippingquotes.php?" + urlParameters);
				//System.out.println("orderformcontrol000000000000000000000000000------------------------------>" + "https://www.viatrading.com/dv/liveapps/shippingquotes.php?" + urlParameters);

				
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
			    con.setRequestMethod("GET");
			    con.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			    con.addRequestProperty("User-Agent", "Mozilla");
			    con.addRequestProperty("Referer", "google.com");
			    con.setRequestProperty("Accept", "text/html, text/*");
			    con.addRequestProperty("Content-Type","application/json; charset=UTF-8"); 
			    
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;

				while ((inputLine = in.readLine()) != null) {
					if (inputLine != null && !inputLine.isEmpty()) {
						urlRes = inputLine;
					}
				}
				//urlRes  = "{\"0\":{\"Shipping\":\"FedEx International Economy\",\"TotalCharges\":758.74,\"Message\":\"Estimated delivery date: November 3, 2015\"},\"1\":{\"Shipping\":\"FedEx International Priority\",\"TotalCharges\":914.36,\"Message\":\"Estimated delivery date: November 2, 2015\"},\"2\":{\"Shipping\":\"USPS Global Express Guaranteed\",\"TotalCharges\":1089.2,\"Message\":\"1 - 3 business days to many major markets\"},\"Note\":\"Please note, shipping more than one case at the same time can significantly reduce your shipping cost per case.If you have over 12 cases in your shipment, please call or email to see if we can reduce your shipping costs by shipping with an LTL carrier.\"}";
				 
				String delims4 = "[{}]+";
				String[] tokens4 = urlRes.split(delims4);
				String shipping = null;
				String note = "{"+tokens4[tokens4.length-1]+"}";
				
				int len = (tokens4.length-1)/2;
				JSONObject obj;
				for(int i=0;i<len;i++){
					shipping = "{"+tokens4[i*2+2]+"}";
					
					obj = new JSONObject(shipping);
					Double totalCharges = 0.0;
					shipping = obj.getString("Shipping");
					try{
						totalCharges = Double.parseDouble(obj.getString("TotalCharges"));
					}catch(Exception ex){
						totalCharges = 0.0;
					}
					String message = obj.getString("Message");
					
					ShippingObj object = new ShippingObj();
					object.setTitle(shipping);
					object.setTotalCharges(totalCharges);
					object.setMessage(message);
					shippingList.add(object);
					
					//System.out.println("Shipping################" + shipping);
					//System.out.println("totalCharges###########" + totalCharges);
					//System.out.println("message################" + message);
				}
				map.put("shippingList", shippingList);
				delims4 = "[:]";
				tokens4 = note.split(delims4);
				note = tokens4[1].replaceAll("}", "");
				note = note.replaceAll("\"", "");
				map.put("note", note);
				}catch (Exception ex) {
					ShippingObj object = new ShippingObj();
					object.setTitle("To be Determined");
					object.setTotalCharges(0.0);
					shippingList.add(object);
				}
		}
		
		List<ShippingRate> ratesList = new ArrayList<ShippingRate>();
		
		ShippingRate shippingRate = null;
		
		for(ShippingObj shippingObj : shippingList){
			shippingRate = new ShippingRate();
			shippingRate.setTitle(shippingObj.getTitle());
			shippingRate.setPrice(shippingObj.getTotalCharges().toString());
			shippingRate.setMessage(shippingObj.getMessage());
			ratesList.add(shippingRate);
		}
		orderForm.setRatesList(ratesList);	
	}

	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		OrderForm orderForm = (OrderForm) command;

		switch (page) {
		case 0:
			if (!errors.hasErrors()) {
				if (orderForm.isNewShippingAddress()) {
					orderForm.getOrder().getShipping().setUserId(orderForm.getOrder().getUserId());
					this.webJaguar.insertAddress(orderForm.getOrder().getShipping());
					orderForm.setNewShippingAddress(false);
				} else {
					if (ServletRequestUtils.getBooleanParameter(request, "save", false)) {
						// update address
						Address address = orderForm.getOrder().getShipping();
						address.setUserId(orderForm.getOrder().getUserId());
						this.webJaguar.updateAddress(address);
					}
				}
			}
			if (siteConfig.get("NO_SHIPPING").getValue().equals("true") || (orderForm.getCustomerShippingTitle() != null && orderForm.getCustomerShippingTitle().trim().length() > 0)) {
				List<ShippingRate> shippingRates = new ArrayList<ShippingRate>();
				shippingRates.add(new ShippingRate("No Shipping", "0.0"));
				orderForm.setRatesList(shippingRates);
			} else {
				orderForm.setRatesList(this.webJaguar.calculateShippingHandling(orderForm.getOrder(), null, request, gSiteConfig));
			}
			orderForm.setShippingRateIndex(null);
			updateShipping(orderForm);
			this.webJaguar.updateTaxAndTotal(gSiteConfig, globalDao, orderForm.getOrder());
			break;
		case 1:
			if (!errors.hasErrors()) {
				if (orderForm.isNewBillingAddress()) {
					orderForm.getOrder().getBilling().setUserId(orderForm.getOrder().getUserId());
					this.webJaguar.insertAddress(orderForm.getOrder().getBilling());
					orderForm.setNewBillingAddress(false);
				} else {
					if (ServletRequestUtils.getBooleanParameter(request, "save", false)) {
						// update address
						Address address = orderForm.getOrder().getBilling();
						address.setUserId(orderForm.getOrder().getUserId());
						this.webJaguar.updateAddress(address);
					}
				}
			}

			break;
		/*
		 * case 3:
		 * 
		 * if (orderForm.getTempGiftCardCode() != null && !orderForm.getTempGiftCardCode().equals("")) { GiftCard giftCard = this.webJaguar.getGiftCardByCode(orderForm.getTempGiftCardCode(), true); if (giftCard != null) { UserSession userSession = (UserSession)
		 * request.getAttribute("userSession"); this.webJaguar.redeemGiftCardByCode(giftCard, userSession); orderForm.setCustomerCreditHistory(orderForm.getCustomerCreditHistory() + giftCard.getAmount()); } else { orderForm.setErrorMessage("giftCard.giftCardNotFound"); } }
		 * this.webJaguar.updateTaxAndTotal(gSiteConfig, globalDao, orderForm.getOrder()); break;
		 */
		case 2:
			// these will not be called when customer clicks purchase button
			// use validatePage() instead if code needs to be run when purchase button is clicked

			if (orderForm.isNewBillingAddress()) {
				orderForm.getOrder().setBilling(new Address());
			}
			if (orderForm.isNewShippingAddress()) {
				orderForm.getOrder().setShipping(new Address());
			}

			break;
		case 3:
			if (orderForm.getCustomerFolder() != null) {
				File files[] = orderForm.getCustomerFolder().listFiles();
				for (int i = 0; i < files.length; i++) {
					if (request.getParameter("remove_" + i) != null) {
						files[i].delete();
					}
				}
				int allowed = (Integer) gSiteConfig.get("gORDER_FILEUPLOAD") - orderForm.getCustomerFolder().listFiles().length;
				for (int i = 0; i < allowed; i++) {
					try {
						MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
						MultipartFile file = multipartRequest.getFile("file_" + i);
						if (file != null && !file.isEmpty()) {
							File tempFile = new File(orderForm.getCustomerFolder(), file.getOriginalFilename());
							file.transferTo(tempFile);
						}
					} catch (Exception e) {
						// do nothing
					}
				}
			}
			break;
		}
	}

	protected Object formBackingObject(HttpServletRequest request) throws ModelAndViewDefiningException {
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		setPages(new String[] { "frontend/checkout1/shipping", "frontend/checkout1/billing", "frontend/checkout1/preliminary", "frontend/checkout1/attach", "frontend/checkout1/centinel" });
		if (siteConfig.get("CHECKOUT1_DIRECTORY").getValue().length() > 0) {
			setPages(new String[] { siteConfig.get("CHECKOUT1_DIRECTORY").getValue() + "/checkout1/shipping", siteConfig.get("CHECKOUT1_DIRECTORY").getValue() + "/checkout1/billing",
					siteConfig.get("CHECKOUT1_DIRECTORY").getValue() + "/checkout1/preliminary", "frontend/checkout1/attach", "frontend/checkout1/centinel" });
		}

		if (siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			setPages(new String[] { "frontend/layout/template6/checkout1/shipping", "frontend/layout/template6/checkout1/billing", "frontend/layout/template6/checkout1/preliminary",
					"frontend/layout/template6/checkout1/attach", "frontend/layout/template6/checkout1/centinel" });
		}

		if (!(Boolean) gSiteConfig.get("gSHOPPING_CART_1")) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("viewCart.jhtm")));
		}

		UserSession userSession = (UserSession) request.getAttribute("userSession");

		// International Checkout.
		if (!siteConfig.get("INTERNATIONAL_CHECKOUT").getValue().isEmpty() && request.getParameter("internationalCheckout") != null && request.getParameter("internationalCheckout").equals("true")) {
			Cart cartInternational = (Cart) WebUtils.getSessionAttribute(request, "sessionCart");
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("cart", cartInternational);
			model.put("frame", "false");
			throw new ModelAndViewDefiningException(new ModelAndView("frontend/checkout1/internationalCheckout", model));
		}

		if (userSession == null) {
			// not logged in
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("billingShipping.jhtm")));
		}

		// Check the update customer new feature flag is true or not
		Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());
		if (customer.isUpdateNewInformation() && request.getParameter("updateInformation") == null) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("account_information_edit.jhtm?forwardAction=checkout1.jhtm")));
		}

		String manufacturerName = (String) request.getSession().getAttribute("manufacturerName");
		Cart cart = this.webJaguar.getUserCart(userSession.getUserid(), manufacturerName);
		cart.setUserId(userSession.getUserid());
		cart.setPromoCode((String) request.getSession().getAttribute("promocode"));
		this.webJaguar.updateCart(cart, request);

		if (!cart.isContinueCart() ||
		// check manufacturers
				((Boolean) gSiteConfig.get("gMANUFACTURER") && !this.webJaguar.checkManufacturerMinOrder(cart, request)) ||
				// budget by brands
				((Boolean) gSiteConfig.get("gBUDGET_PRODUCT") && this.webJaguar.getBudgetByCart(cart, new HashMap<String, BudgetProduct>())) ||
				// budget by brands
				((Boolean) gSiteConfig.get("gBUDGET_BRAND") && this.webJaguar.getSubTotalPerBrand(cart, new HashMap<Brand, Double>()))) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("viewCart.jhtm")));
		}
		if (cart != null && cart.getNumberOfItems() > 0) {
			OrderForm orderForm = new OrderForm();
			orderForm.setCustomer((Customer) request.getAttribute("sessionCustomer"));
			// set language
			if (!((String) gSiteConfig.get("gI18N")).isEmpty() && orderForm.getCustomer() != null) {
				orderForm.getOrder().setLanguageCode(orderForm.getCustomer().getLanguageCode());
			}
			if (orderForm.getCustomer().getAddress().getStateProvince() != null && orderForm.getCustomer().getAddress().getStateProvince().trim().equals("")) {
				orderForm.getCustomer().getAddress().setStateProvinceNA(true);
			}
			orderForm.setCustomerShippingTitle(orderForm.getCustomer().getShippingTitle());
			// To add items stored previously
			if (!siteConfig.get("BUY_SAFE_URL").getValue().equals("") && cart.getCartItems().size() > 0) {
				ShoppingCartAddUpdateRS addUpdateRS = null;
				if (request.getSession().getAttribute("buySafeCartId") == null) {
					request.getSession().setAttribute("buySafeCartId", UUID.randomUUID().toString().toUpperCase().replace("-", "").substring(0, 4) + new Date().getTime());
				}
				try {
					addUpdateRS = this.webJaguar.addUpdateShoppingCartRequest(cart, null, request, (userSession != null ? userSession.getUserid() : null), mailSender);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (addUpdateRS != null && addUpdateRS.getBondCostDisplayText() != null && !addUpdateRS.getBondCostDisplayText().equals("")) {
					orderForm.getOrder().setWantsBond(true);
					orderForm.getOrder().setBondCost(addUpdateRS.getTotalBondCost());
				} else {
					orderForm.getOrder().setWantsBond(false);
					orderForm.getOrder().setBondCost(null);
				}
				orderForm.getOrder().setCartId(request.getSession().getAttribute("buySafeCartId").toString());
			}

			orderForm.getOrder().initOrder(orderForm.getCustomer(), cart);
			for (LineItem lineItem : orderForm.getOrder().getLineItems()) {
				//System.out.println("lineItem.getParentSkuDiscount() " + lineItem.getParentSkuDiscount());
				if (lineItem.getParentSkuDiscount() != null && lineItem.getParentSkuDiscount() < 0.0) {
					orderForm.getOrder().setHasParentDeal(true);
					break;
				}
			}
			// move cartitem attachment to customer folder if it is in temp
			this.moveImageToCustomerFolder(cart.getCartItems(), request, cart.getUserId(), orderForm.getOrder().getOrderId());

			// shipping
			Address shipping = this.webJaguar.getDefaultShippingAddressByUserid(userSession.getUserid());
			if (shipping != null) {
				orderForm.getOrder().setShipping(shipping);
			}

			if (siteConfig.get("SHOPATRON").getValue().length() > 0 && orderForm.getCustomer().getPriceTable() == 0) {
				// regular customer
				throw new ModelAndViewDefiningException(shopatronRPC(request, siteConfig, orderForm.getOrder(), orderForm.getCustomer()));
			}
			orderForm.setPoRequired(orderForm.getCustomer().getPoRequired());
			orderForm.setCustPayment(orderForm.getCustomer().getPayment());
			orderForm.setCustomerCredit(orderForm.getCustomer().getCredit());
			orderForm.getOrder().setTurnOverday(Integer.parseInt(siteConfig.get("SHIPPING_TURNOVER_TIME").getValue()));
			orderForm.setUsername(orderForm.getCustomer().getUsername());
			if (siteConfig.get("CUSTOMERS_REQUIRED_COMPANY").getValue().equals("true")) {
				orderForm.setCompanyRequired(true);
			}
			if (request.getAttribute("promocode") != null && !request.getAttribute("promocode").equals("")) {
				orderForm.setPromocodeLink((String) request.getAttribute("promocode"));
			}

			// customer's temp folder
			if ((Integer) gSiteConfig.get("gORDER_FILEUPLOAD") > 0) {
				File tempFolder = new File(getServletContext().getRealPath("temp"));
				if (!tempFolder.exists()) {
					tempFolder.mkdir();
				}
				orderForm.setCustomerFolder(new File(tempFolder, "customer_" + orderForm.getOrder().getUserId()));
				if (!orderForm.getCustomerFolder().exists()) {
					if (orderForm.getCustomerFolder().mkdir() == false) {
						// folder not created
						orderForm.setCustomerFolder(null);
					}
				}
			}

			boolean enableCustomShipping = false;
			if (siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue().length() > 0) {
				enableCustomShipping = true;
			}
			if (siteConfig.get("SITE_URL").getValue().contains("tileshowroom")) {

				List<String> outsideUSStates = new ArrayList<String>();
				outsideUSStates.add("AK");
				outsideUSStates.add("HI");
				outsideUSStates.add("AS");
				outsideUSStates.add("GU");
				outsideUSStates.add("MP");
				outsideUSStates.add("PR");
				outsideUSStates.add("VI");
				outsideUSStates.add("FM");
				outsideUSStates.add("PW");

				if ((!orderForm.getOrder().getShipping().getCountry().equalsIgnoreCase("US"))
						|| (orderForm.getOrder().getShipping().getCountry().equalsIgnoreCase("US") && outsideUSStates.contains(orderForm.getOrder().getShipping().getStateProvince()))) {
					enableCustomShipping = false;
				}
			}
			if (enableCustomShipping) {
				initCustomShippingRates(orderForm);
			}

			// GE Money
			orderForm.getOrder().setGeMoney(new GEMoney());

			// product fields
			try {
				orderForm.setProductFieldsHeader(this.webJaguar.getProductFieldsHeader(orderForm.getOrder(), (Integer) gSiteConfig.get("gPRODUCT_FIELDS")));
			} catch (Exception e) {
			}

			if (siteConfig.get("NO_SHIPPING").getValue().equals("true") || (orderForm.getCustomerShippingTitle() != null && orderForm.getCustomerShippingTitle().trim().length() > 0)) {
				List<ShippingRate> shippingRates = new ArrayList<ShippingRate>();
				shippingRates.add(new ShippingRate("No Shipping", "0.0"));
				orderForm.setRatesList(shippingRates);
			} else {
				// orderForm.setRatesList(calculateShippingHandling(orderForm, request, gSiteConfig));
				orderForm.setRatesList(this.webJaguar.calculateShippingHandling(orderForm.getOrder(), orderForm.getCustomer(), request, gSiteConfig));
			}

			// manufacturer selection
			orderForm.getOrder().setManufacturerName((String) request.getSession().getAttribute("manufacturerName"));

			// Consignment
			if ((Boolean) gSiteConfig.get("gCUSTOMER_SUPPLIER")) {
				for (LineItem lineItem : orderForm.getOrder().getLineItems()) {
					// primary supplier is not done yet.
					Supplier supplier = this.webJaguar.getProductSupplierPrimaryBySku(lineItem.getProduct().getSku());
					if (supplier != null) {
						lineItem.setSupplier(supplier);
						lineItem.setCost(supplier.getPrice());
						lineItem.setCostPercent(supplier.isPercent());
						lineItem.getProduct().setDefaultSupplierId(supplier.getId());
					}
				}
			}

			return orderForm;
		} else {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("viewCart.jhtm")));
		}
	}

	protected int getInitialPage(HttpServletRequest request, Object command) {
		return 2;
	}

	public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		OrderForm orderForm = (OrderForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("order", orderForm.getOrder());
		model.put("orderForm", orderForm);
		model.put("username", orderForm.getCustomer().getUsername());

		Layout layout = (Layout) request.getAttribute("layout");
		if (layout != null && layout.getTopBarHtml() != null)
			layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Sales Invoice"));
		model.put("invoiceLayout", layout);
		model.put("layout", layout);

		if (orderForm != null && orderForm.getOrder() != null && orderForm.getCustomer() != null) {
			orderForm.getOrder().setQualifier(orderForm.getCustomer().getQualifier());
		}

		orderForm.getOrder().setTrackcode((String) request.getAttribute("trackcode"));
		orderForm.getOrder().setOrderType("internet");
		orderForm.getOrder().setIpAddress(request.getRemoteAddr());
		// order quantity and inventory management
		if ((Boolean) gSiteConfig.get("gINVENTORY")) {
			for (LineItem lineItem : orderForm.getOrder().getLineItems()) {
				if (lineItem.getProduct().getInventory() != null && !lineItem.getProduct().isNegInventory()) {
					Inventory inventory = new Inventory();
					inventory.setSku(lineItem.getProduct().getSku());
					inventory = this.webJaguar.getInventory(inventory);
					//
					if (siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly")) {
						if (inventory.getInventoryAFS() != null && (inventory.getInventoryAFS() < lineItem.getQuantity())) {
							return new ModelAndView(new RedirectView("viewCart.jhtm"));
						}
					} else {
						if (inventory.getInventory() != null && (inventory.getInventory() < lineItem.getQuantity())) {
							return new ModelAndView(new RedirectView("viewCart.jhtm"));
						}
					}
				}
			}
		}
		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);
			orderForm.getOrder().setHost(request.getHeader("host"));
		}

		// LinkShare
		Cookie linkShareSiteID = WebUtils.getCookie(request, "linkShareSiteID");
		if (linkShareSiteID != null) {
			LinkShare linkShare = new LinkShare();
			linkShare.setSiteID(linkShareSiteID.getValue());

			Cookie linkShareDate = WebUtils.getCookie(request, "linkShareDate");
			if (linkShareDate != null) {
				SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd/kk:mm:ss");
				dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));
				try {
					linkShare.setDateEntered(dateFormatter.parse(linkShareDate.getValue()));
				} catch (Exception e) {
					// do nothing
				}
			}
			orderForm.getOrder().setLinkShare(linkShare);
		}

		// SearchEngine Prospect
		if ((Boolean) gSiteConfig.get("gSEARCH_ENGINE_PROSPECT")) {
			String cookieName = site.getProperty("track.prospect.cookie.name");
			Cookie prospectCookie = WebUtils.getCookie(request, cookieName);
			if (prospectCookie != null) {
				orderForm.getOrder().setProspectId(prospectCookie.getValue());
				logger.info("Prospect Id = " + prospectCookie.getValue());
				// delete Cookie after order placed.
				prospectCookie.setMaxAge(0);
				prospectCookie.setPath("/");
				prospectCookie.setValue("");
				response.addCookie(prospectCookie);
			}
		}

		OrderStatus orderStatus = new OrderStatus();

		// PayPal
		if ((Integer) gSiteConfig.get("gPAYPAL") > 0 && orderForm.getOrder().getPaymentMethod().contains("paypal")) {
			orderStatus.setStatus("xp"); // paypal cancelled
			this.webJaguar.insertOrder(orderForm, orderStatus, request, true);
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			this.moveImageToCustomerFolder(orderForm.getOrder().getLineItems(), request, orderForm.getOrder().getUserId(), orderForm.getOrder().getOrderId());
			this.updateMasterSkus(orderForm.getOrder(), model);
			if (!siteConfig.get("BUY_SAFE_URL").getValue().equals("")) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView("frontend/checkout/paypalPost", model);
		}
		// NetCommerce
		if (orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("netcommerce") && gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("netcommerce")) {
			orderStatus.setStatus("xnc"); // netcommerce cancelled
			this.webJaguar.insertOrder(orderForm, orderStatus, request, true);
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			this.moveImageToCustomerFolder(orderForm.getOrder().getLineItems(), request, orderForm.getOrder().getUserId(), orderForm.getOrder().getOrderId());
			this.updateMasterSkus(orderForm.getOrder(), model);

			if (!siteConfig.get("BUY_SAFE_URL").getValue().equals("")) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
			String signatureTxt = orderForm.getOrder().getGrandTotal() + "&" + "840" + "&" + orderForm.getOrder().getOrderId() + "&" + siteConfig.get("NET_COMMERCE_MD5_KEY").getValue() + "&"
					+ secureUrl + "netcommerce.jhtm" + "&" + siteConfig.get("NET_COMMERCE_MD5_KEY").getValue();
			model.put("signatureTxt", signatureTxt);
			return new ModelAndView("frontend/checkout/netCommercePost", model);
		}
		// BankAudi
		if (orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("bankaudi") && gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("bankaudi")) {
			orderStatus.setStatus("xba"); // bankaudi cancelled
			this.webJaguar.insertOrder(orderForm, orderStatus, request, true);
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			this.moveImageToCustomerFolder(orderForm.getOrder().getLineItems(), request, orderForm.getOrder().getUserId(), orderForm.getOrder().getOrderId());
			this.updateMasterSkus(orderForm.getOrder(), model);

			if (!siteConfig.get("BUY_SAFE_URL").getValue().equals("")) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView("frontend/checkout/bankAudiPost", model);
		}
		// eBillme
		if (orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("ebillme") && siteConfig.get("EBILLME_URL").getValue().trim().length() > 0
				&& siteConfig.get("EBILLME_MERCHANTTOKEN").getValue().trim().length() > 0 && siteConfig.get("EBILLME_CANCEL_URL").getValue().trim().length() > 0
				&& siteConfig.get("EBILLME_ERROR_URL").getValue().trim().length() > 0) {
			orderStatus.setStatus("xeb"); // ebillme cancelled
			this.webJaguar.insertOrder(orderForm, orderStatus, request, false);
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			this.moveImageToCustomerFolder(orderForm.getOrder().getLineItems(), request, orderForm.getOrder().getUserId(), orderForm.getOrder().getOrderId());
			this.updateMasterSkus(orderForm.getOrder(), model);

			if (!siteConfig.get("BUY_SAFE_URL").getValue().equals("")) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView("frontend/checkout/eBillmePost", model);
		}
		// google checkout
		if (orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("Google") && !siteConfig.get("GOOGLE_CHECKOUT_URL").getValue().isEmpty()) {
			orderStatus.setStatus("xg"); // google cancelled
			this.webJaguar.insertOrder(orderForm, orderStatus, request, true);
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			this.moveImageToCustomerFolder(orderForm.getOrder().getLineItems(), request, orderForm.getOrder().getUserId(), orderForm.getOrder().getOrderId());
			this.updateMasterSkus(orderForm.getOrder(), model);

			orderForm.getOrder().setOrderId(orderForm.getOrder().getOrderId());
			String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
			Document doc = createXMLCart(orderForm, secureUrl + "gcoCalculations.jhtm");
			Map<String, String> encodedCart = encodeCart(doc, siteConfig.get("GOOGLE_MERCHANT_KEY").getValue());
			model.put("encodedCart", encodedCart);
			model.put("checkOutUrl", siteConfig.get("GOOGLE_CHECKOUT_URL").getValue() + siteConfig.get("GOOGLE_MERCHANT_ID").getValue());

			if (!siteConfig.get("BUY_SAFE_URL").getValue().equals("")) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView("frontend/checkout/googlePost", model);
		}

		// Amazon Checkout Payment
		if (orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("amazon") && siteConfig.get("AMAZON_URL").getValue().trim().length() > 0
				&& siteConfig.get("AMAZON_MERCHANT_ID").getValue().trim().length() > 0 && siteConfig.get("AMAZON_ACCESS_KEY").getValue().trim().length() > 0
				&& siteConfig.get("AMAZON_SECRET_KEY").getValue().trim().length() > 0) {
			orderStatus.setStatus("xap"); // amazon payment cancelled
			this.webJaguar.insertOrder(orderForm, orderStatus, request, false);
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			this.moveImageToCustomerFolder(orderForm.getOrder().getLineItems(), request, orderForm.getOrder().getUserId(), orderForm.getOrder().getOrderId());
			this.updateMasterSkus(orderForm.getOrder(), model);

			if (!siteConfig.get("BUY_SAFE_URL").getValue().equals("")) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView("frontend/checkout/amazonPaymentPost", model);
		}

		// GE Money
		if (orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("GE Money") && siteConfig.get("GEMONEY_DOMAIN").getValue().trim().length() > 0) {
			HttpClient client = new HttpClient();
			client.getState().setCredentials(new AuthScope(siteConfig.get("GEMONEY_DOMAIN").getValue(), 443, "myrealm"),
					new UsernamePasswordCredentials(siteConfig.get("GEMONEY_MERCHANTID").getValue().trim(), siteConfig.get("GEMONEY_PASSWORD").getValue().trim()));
			PostMethod post = new PostMethod(request.getScheme()+"://" + siteConfig.get("GEMONEY_DOMAIN").getValue() + "/process/login.do");
			post.addParameter("shopperId=", request.getSession().getId());
			post.setDoAuthentication(true);
			try {
				client.executeMethod(post);
				model.put("geMoneyStrToken", post.getResponseBodyAsString().trim());
			} catch (Exception e) {
				errors.rejectValue("order.creditCard.number", "----", "Error");
				model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
				return showPage(request, errors, 2);
			} finally {
				post.releaseConnection();
			}

			orderStatus.setStatus("xge"); // ge money initiated
			this.webJaguar.insertOrder(orderForm, orderStatus, request, false); // retain cart
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			this.moveImageToCustomerFolder(orderForm.getOrder().getLineItems(), request, orderForm.getOrder().getUserId(), orderForm.getOrder().getOrderId());
			this.updateMasterSkus(orderForm.getOrder(), model);

			if (!siteConfig.get("BUY_SAFE_URL").getValue().equals("")) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView("frontend/checkout/geMoneyPost", model);
		}

		// EbizCharge
		if (siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("")
				&& (orderForm.getOrder().getCreditCard().getNumber() != null || orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("Credit Card"))
				&& gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ebizcharge") && siteConfig.get("PCI_FRIENDLY_EBIZCHARGE").getValue().equalsIgnoreCase("true")) {
			try {
				EBizChargePaymentApi eBizChargePayment = new EBizChargePaymentApi(siteConfig, orderForm.getOrder().getIpAddress());
				if (orderForm.getOrder().getCcToken().equalsIgnoreCase("-1") || orderForm.getOrder().getCcToken().equalsIgnoreCase("-2")) {
					if (orderForm.getCustomer().getGatewayToken() != null && !orderForm.getCustomer().getGatewayToken().isEmpty()) {
						eBizChargePayment.bankCardCustomerPaymentMethod(orderForm.getCustomer(), orderForm.getOrder());
					} else {
						eBizChargePayment.bankCardAddCustomer(orderForm.getCustomer(), orderForm.getOrder());
					}
				}
				// If eBizCharge returns a custId then update users table
				if (orderForm.getCustomer().getGatewayToken() != null && !orderForm.getCustomer().getGatewayToken().isEmpty()) {
					this.webJaguar.updateGatewayToken(orderForm.getCustomer().getGatewayToken(), orderForm.getCustomer().getId());
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		boolean autoCharge = false;
		if (!siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("")
				&& (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ezic") || gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("payrover")
						|| gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("authorizenet") || gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ebizcharge") || gSiteConfig.get("gCREDIT_CARD_PAYMENT")
						.equals("paypalpro")) && (orderForm.getOrder().getCreditCard().getNumber() != null || orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("Credit Card"))) {

			// credit card attempts
			if (orderForm.getCreditCardAttempts() > 3) {
				errors.rejectValue("order.creditCard.number", "order.exception.creditcard.attempts");
				model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
				return showPage(request, errors, 2);
			}

			// Ezic
			if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ezic")) {
				orderForm.setCreditCardAttempts(orderForm.getCreditCardAttempts() + 1);
				try {
					EzicApi ezic = new EzicApi();
					autoCharge = ezic.process(orderForm.getOrder(), orderForm.getUsername(), siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth"), siteConfig);
				} catch (Exception e) {
					errors.rejectValue("order.creditCard.number", "----", "Error");
					model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
					return showPage(request, errors, 2);
				}
			}

			// Authorize.Net Or EbizCharge
			if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("authorizenet") || gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ebizcharge")) {

				if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ebizcharge") && siteConfig.get("PCI_FRIENDLY_EBIZCHARGE").getValue().equalsIgnoreCase("true")) {

					try {
						EBizChargePaymentApi eBizChargePayment = new EBizChargePaymentApi(siteConfig, orderForm.getOrder().getIpAddress());
						if (orderForm.getOrder().getCcToken().equalsIgnoreCase("-1") || orderForm.getOrder().getCcToken().equalsIgnoreCase("-2")) {
							if (orderForm.isSaveCC()) {
								if (orderForm.getCustomer().getGatewayToken() != null && !orderForm.getCustomer().getGatewayToken().isEmpty()) {
									eBizChargePayment.bankCardCustomerPaymentMethod(orderForm.getCustomer(), orderForm.getOrder());
								} else {
									eBizChargePayment.bankCardAddCustomer(orderForm.getCustomer(), orderForm.getOrder());

									// If eBizCharge returns a custId then update users table
									if (orderForm.getCustomer().getGatewayToken() != null && !orderForm.getCustomer().getGatewayToken().isEmpty()) {
										this.webJaguar.updateGatewayToken(orderForm.getCustomer().getGatewayToken(), orderForm.getCustomer().getId());
									}
								}
							}
						}
						if (orderForm.getCustomer().getGatewayToken() != null
								&& (!orderForm.getOrder().getCcToken().equalsIgnoreCase("-1") && !orderForm.getOrder().getCcToken().equalsIgnoreCase("-2"))) {
							autoCharge = eBizChargePayment.CustomerBankCardTransactions(orderForm.getOrder(), orderForm.getCustomer(), siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth"),
									false, siteConfig);
						} else {
							autoCharge = eBizChargePayment.BankCardTransactions(orderForm.getOrder(), siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth"), false, siteConfig);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					// cardinal showPage(request, errors, 2)
					if (!siteConfig.get("CENTINEL_URL").getValue().equals("") && siteConfig.get("CENTINEL_PROCESSORID").getValue().trim().length() > 0
							&& siteConfig.get("CENTINEL_MERCHANTID").getValue().trim().length() > 0 && siteConfig.get("CENTINEL_TRANSACTIONPWD").getValue().trim().length() > 0) {

						String cardType = orderForm.getOrder().getCreditCard().getType();

						if (cardType.equalsIgnoreCase("VISA") || cardType.equalsIgnoreCase("MC") || cardType.equalsIgnoreCase("JCB")) {
							CentinelApi centinelApi = new CentinelApi();

							if (request.getParameter("_PaRes") == null) {
								// reset values
								orderForm.getOrder().getCreditCard().setEci(null);
								orderForm.getOrder().getCreditCard().setCavv(null);
								orderForm.getOrder().getCreditCard().setXid(null);

								if (centinelApi.lookup(request, orderForm.getOrder(), siteConfig, mailSender)) {
									// card enrolled
									return showPage(request, errors, 4);
								}
							} else {
								try {
									boolean failure = centinelApi.authenticate(request, orderForm.getOrder(), siteConfig, request.getParameter("centinelOrderId"), request.getParameter("_PaRes"));
									if (failure) {
										// authentication failed
										request.setAttribute("centinelAuthenticationFailure", true);
										model.put("cardinalCentinelLayout", this.webJaguar.getSystemLayout("cardinalCentinelFailed", request.getHeader("host"), request));
										return showPage(request, errors, 2);
									}
								} catch (Exception e) {
									// transaction should not be authorized
									request.setAttribute("centinelAuthenticationFailure", true);
									model.put("cardinalCentinelLayout", this.webJaguar.getSystemLayout("cardinalCentinelFailed", request.getHeader("host"), request));
									return showPage(request, errors, 2);
								}
							}
						} else {
							// reset values
							orderForm.getOrder().getCreditCard().setEci(null);
							orderForm.getOrder().getCreditCard().setCavv(null);
							orderForm.getOrder().getCreditCard().setXid(null);
						}
					}

					orderForm.setCreditCardAttempts(orderForm.getCreditCardAttempts() + 1);
					try {
						AuthorizeNetApi authorizeNet = new AuthorizeNetApi();
						autoCharge = authorizeNet.authorize(orderForm.getOrder(), siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth"), false, siteConfig, gSiteConfig);
					} catch (Exception e) {
						errors.rejectValue("order.creditCard.number", "----", "Error");
						model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
						return showPage(request, errors, 2);
					}
				}
			}

			// PayPal-Pro
			if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("paypalpro")) {
				try {
					PayPalProApi payPalPro = new PayPalProApi();
					autoCharge = payPalPro.DoDirectPayment(orderForm.getOrder(), siteConfig, null);
				} catch (Exception e) {
					errors.rejectValue("order.creditCard.number", "----", "Error");
					model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
					return showPage(request, errors, 3);
				}
			}

			// PayRover
			if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("payrover")) {
				orderForm.setCreditCardAttempts(orderForm.getCreditCardAttempts() + 1);
				try {
					PayRoverApi payRover = new PayRoverApi();
					autoCharge = payRover.authorize(orderForm.getOrder(), siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth"), siteConfig);
					return showPage(request, errors, 2);
				} catch (Exception e) {
					errors.rejectValue("order.creditCard.number", "----", "Error");
					model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
					return showPage(request, errors, 2);
				}
			}

			if (!autoCharge) {
				// credit card failed
				errors.rejectValue("order.creditCard.number", "order.exception.creditcard.declined");
				errors.rejectValue("order.creditCard.expireMonth", "----", ""); // additional info
				model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
				return showPage(request, errors, 2);
			}
		}

		orderStatus.setStatus("p"); // pending

		SiteMessage siteMessage = null;
		if (orderForm.getCustomer().isEmailNotify()) {
			// notify email
			try {
				Integer messageId = null;
				Integer newOrderId;
				if (multiStore != null) {
					messageId = multiStore.getMidNewOrders();
				} else {
					String languageCode = orderForm.getOrder().getLanguageCode().toString();
					try {
						newOrderId = this.webJaguar.getLanguageSetting(languageCode).getNewOrderId();
					} catch (Exception e) {
						newOrderId = null;
					}
					if (languageCode.equalsIgnoreCase((LanguageCode.en).toString()) || newOrderId == null) {
						messageId = Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_NEW_ORDERS").getValue());
					} else {
						messageId = Integer.parseInt(newOrderId.toString());
					}
				}
				siteMessage = this.webJaguar.getSiteMessageById(messageId);
				if (siteMessage != null) {
					orderStatus.setUserNotified(true);
				}
			} catch (NumberFormatException e) {
				// do nothing
			}
		}

		this.webJaguar.insertOrder(orderForm, orderStatus, request, true);
		this.moveImageToCustomerFolder(orderForm.getOrder().getLineItems(), request, orderForm.getOrder().getUserId(), orderForm.getOrder().getOrderId());
		this.updateMasterSkus(orderForm.getOrder(), model);

		if (autoCharge) {
			this.webJaguar.updateCreditCardPayment(orderForm.getOrder());
			if (orderForm.getOrder().getCreditCard().getEci() != null) {
				this.webJaguar.insertCreditCardHolderAuthentication(orderForm.getOrder());
			}
		}

		if (!siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("") && gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("paymentech") && gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("paymentech")
				&& (orderForm.getOrder().getCreditCard().getNumber() != null || orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("Credit Card"))) {
			// Paymentech
			try {
				PaymentechApi paymentech = new PaymentechApi();
				boolean result = paymentech.process(orderForm.getOrder(), siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth"), siteConfig);
				if (!result) {
					orderForm.getOrder().getCreditCard().setPaymentStatus("FAIL");
				}
			} catch (Exception e) {
				orderForm.getOrder().getCreditCard().setPaymentStatus("FAITH");
			}
			this.webJaguar.updateCreditCardPayment(orderForm.getOrder());
		}

		if (orderForm.getCustomerFolder() != null) {
			createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());
		}
		// custom frames
		moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());

		// clear user cart from session
		request.getSession().removeAttribute("userCart");

		if (siteMessage != null) {
			// send email
			orderStatus.setSubject(siteMessage.getSubject());
			orderStatus.setMessage(siteMessage.getMessage());
			orderStatus.setHtmlMessage(siteMessage.isHtml());
			notifyCustomer(orderStatus, request, orderForm.getOrder(), multiStore, orderForm.getCustomer());

		}

		// buy safe
		if (!siteConfig.get("BUY_SAFE_URL").getValue().equals("")) {
			ShoppingCartCheckoutRS checkOutResponse = null;
			try {
				checkOutResponse = this.webJaguar.setShoppingCartCheckout(orderForm.getOrder(), mailSender);
			} catch (Exception e) {
				e.printStackTrace();
				OrderStatus status = new OrderStatus();
				status.setOrderId(orderForm.getOrder().getOrderId());
				status.setStatus("p"); // pending
				if ((orderForm.getOrder().getWantsBond() != null) && (orderForm.getOrder().getWantsBond() == true)) {
					status.setComments("BuySafe Bond was selected but cancelled as system failed to connect Buysafe. Please check the amount charged and refund the bond cost to Customer.");
				} else {
					status.setComments("BuySafe Bond was not selected. System failed to connect Buysafe");
				}
				orderForm.getOrder().setWantsBond(false);
				orderForm.getOrder().setBondCost(null);
				orderForm.getOrder().setGrandTotal();

				this.webJaguar.cancelBuySafeBond(orderForm.getOrder(), status);

			}
			model.put("buySafeResponse", checkOutResponse);
			request.getSession().removeAttribute("buySafeCartId");
		}

		model.put("countries", this.webJaguar.getCountryMap());
		if (siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/checkout1/finalinvoice", model);
		}
		return new ModelAndView("frontend/checkout/finalinvoice", model);
	}
	
	private void updateCustomShipping(OrderForm orderForm) {
		ShippingRate shippingRate = orderForm.getCustomRatesList().get(orderForm.getCustomShippingRateIndex());
		orderForm.getOrder().setCustomShippingTitle(shippingRate.getTitle());
		orderForm.getOrder().setCustomShippingCost(Double.parseDouble(shippingRate.getPrice()));

		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		for (LineItem lineItem : orderForm.getOrder().getLineItems()) {
			Product product = this.webJaguar.getProductById(lineItem.getProductId(), 0, false, null);
			try {
				m = c.getMethod("getField" + siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue());
				if (((String) m.invoke(product, arglist)).equals("0")) {
					for (int i = 1; i < 3; i++) {
						if (siteConfig.get("CUSTOM_SHIPPING_TITLE" + i).getValue().length() > 0
								&& siteConfig.get("CUSTOM_SHIPPING_TITLE" + i).getValue().equals(orderForm.getOrder().getCustomShippingTitle())) {
							m = c.getMethod("getField" + siteConfig.get("CUSTOM_SHIPPING_FIELD" + i).getValue());
							String value = (String) m.invoke(product, arglist);
							lineItem.setCustomShippingCost(Double.parseDouble(value) * (siteConfig.get("CUSTOM_SHIPPING_FIXED_COST").getValue().length() > 0 ? 1 : lineItem.getQuantity()));
						}
					}
				}
			} catch (Exception e) {
				// do nothing
			}
		}
	}

	protected void validatePage(Object command, Errors errors, int page) {
		OrderFormValidator validator = (OrderFormValidator) getValidator();
		OrderForm orderForm = (OrderForm) command;

		switch (page) {
		case 0:
			validator.validateShippingAddress(command, errors);
			break;
		case 1:
			validator.validateBillingAddress(command, errors);
			break;
		case 2:
			// these are guaranteed to be called

			if (siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue().length() > 0 && orderForm.getCustomShippingRateIndex() != null) {
				updateCustomShipping(orderForm);
			}
			
			// payment method
			validator.validatePaymentMethod(this.webJaguar.getCustomPaymentMethodList(orderForm.getOrder().getUserId()), command, errors);
			
			// shipping method
			if (orderForm.getOrder().ishasRegularShipping()) {
				validator.validateShippingSelection(errors);
			}
			if (orderForm.getCustomRatesList() != null && orderForm.getCustomRatesList().size() > 0) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customShippingRateIndex", "shipping.required");
			}

			orderForm.getOrder().setCcFeeRate(null);
			if (orderForm.getOrder().getPaymentMethod() != null && orderForm.getOrder().getPaymentMethod().contains("Credit Card")) {
				orderForm.getOrder().setCcFeeRate(3.0);
			}

			if (orderForm.getOrder().getPaymentMethod() != null && orderForm.getOrder().getPaymentMethod().contains("Paypal")) {
				orderForm.getOrder().setCcFeeRate(3.0);
			}
			
			updateShipping(orderForm);

			// promo code
			err.put("orderError", "");
			this.webJaguar.validatePromoCode(orderForm.getOrder(), err, orderForm.getCustomer(), orderForm.getPromocodeLink(), gSiteConfig);
			if (err.get("orderError") != null && !err.get("orderError").equals("")) {
				orderForm.setTempPromoErrorMessage(err.get("orderError"));

			}
			if (orderForm.getOrder().isPromoAvailable()) {
				orderForm.setTempPromoErrorMessage(null);
				this.webJaguar.updateGrandTotal(gSiteConfig, orderForm.getOrder());
				// change payment method if grand total is zero
				if (orderForm.getOrder().getGrandTotal() <= 0) {
					validator.validatePaymentMethod(this.webJaguar.getCustomPaymentMethodList(orderForm.getOrder().getUserId()), orderForm, errors);
				}
			}

			// purchase order
			if (orderForm.getPoRequired() != null && orderForm.getPoRequired().equals("required")) {
				validator.validatePurchaseOrder(command, errors);
			}

			// GE Money
			if (orderForm.getOrder().getPaymentMethod() != null && orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("GE Money") && siteConfig.get("GEMONEY_DOMAIN").getValue().length() > 0
					&& siteConfig.get("GEMONEY_MERCHANTID").getValue().trim().length() > 0 && siteConfig.get("GEMONEY_PASSWORD").getValue().trim().length() > 0) {
				GEMoneyValidator.validateGEMoneyOrder(orderForm.getOrder(), errors);
			} else {
				// reset ge money
				orderForm.getOrder().setGeMoney(new GEMoney());
			}

			if (orderForm.getOrder().getPaymentMethod() != null && orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("Credit Card")
					&& siteConfig.get("CREDIT_CARD_PAYMENT").getValue().length() > 0) {
				// eBizCharge credit card
				if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ebizcharge") && siteConfig.get("PCI_FRIENDLY_EBIZCHARGE").getValue().equalsIgnoreCase("true")) {
					if (orderForm.getCustomer().getGatewayToken() != null && !orderForm.getOrder().getCcToken().equals("-1") && !orderForm.getOrder().getCcToken().equals("-2")) {
						EBizChargePaymentApi eBizChargePayment = new EBizChargePaymentApi(siteConfig, orderForm.getOrder().getIpAddress());
						try {
							eBizChargePayment.bankCardGetCustomer(orderForm.getCustomer(), orderForm.getOrder(), true);
						} catch (ServiceException e) {
							e.printStackTrace();
						}
					}
					if (orderForm.getOrder().getCcToken().equalsIgnoreCase("-2")
							|| (orderForm.getTempCreditCard() != null && orderForm.getTempCreditCard().getNumber() != null && !orderForm.getTempCreditCard().getNumber().isEmpty())) {
						CreditCard tempCard = orderForm.getTempCreditCard();
						orderForm.getOrder().setCreditCard(tempCard);
						validator.validateCC(orderForm, errors, true);
					}
				} else {
					validator.validateCC(orderForm, errors, false);
				}
			} else {
				orderForm.getOrder().setCreditCard(new CreditCard());
			}

			// radio button
			orderForm.getOrder().setCreditUsed(null);
			if (orderForm.getOrder().getPaymentMethod() != null && orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("vba")) {
				orderForm.getOrder().setCreditUsed(orderForm.getOrder().getGrandTotal());
			} else {
				// checkbox
				if (orderForm.isApplyUserPoint()) {
					orderForm.getOrder().setCreditUsed(orderForm.getCustomerCredit());
				} else if (orderForm.getOrder().getCreditUsed() != null) {
					// put grand total back
					orderForm.getOrder().setCreditUsed(null);
				}
			}
			this.webJaguar.updateTaxAndTotal(gSiteConfig, globalDao, orderForm.getOrder());
			// If customer uses 'Credit' it is one type of payment, so update the amountPaid to credit used.
			orderForm.getOrder().setAmountPaid(orderForm.getOrder().getCreditUsed());

			orderForm.getOrder().setupDueDate(orderForm.getOrder().getTurnOverday(), orderForm.getOrder().getShippingPeriod());

			break;
		}
	}

	protected void updateShipping(OrderForm orderForm) {
		if (orderForm.getShippingRateIndex() == null) {
			// reset shipping
			orderForm.getOrder().setShippingMethod(null);
			orderForm.getOrder().setCustomShippingId(null);
			orderForm.getOrder().setShippingPeriod(null);
			orderForm.getOrder().setDueDate(null);
			orderForm.getOrder().setShippingCost(null);
			return;
		}
		ShippingRate shippingRate = orderForm.getRatesList().get(Integer.parseInt(orderForm.getShippingRateIndex()));
		orderForm.getOrder().setShippingMethod(shippingRate.getTitle());
		orderForm.getOrder().setCustomShippingId(shippingRate.getId());
		try {
			orderForm.getOrder().setShippingPeriod(Integer.parseInt(shippingRate.getShippingPeriod()));
		} catch (Exception e) {
		}
		orderForm.getOrder().setDueDate(shippingRate.getDeliveryDate());
		if (shippingRate.getPrice() != null) {
			orderForm.getOrder().setShippingCost(Double.parseDouble(shippingRate.getPrice()));
		} else {
			orderForm.getOrder().setShippingCost(null);
		}		
	}

	private void notifyCustomer(OrderStatus orderStatus, HttpServletRequest request, Order order, MultiStore multiStore, Customer customer) {
		SalesRep salesRep = null;
		if ((Boolean) gSiteConfig.get("gSALES_REP") && order.getSalesRepId() != null) {
			salesRep = this.webJaguar.getSalesRepById(order.getSalesRepId());
		}
		// send email
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));

		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append(dateFormatter.format(new Date()) + "\n\n");

		// replace dyamic elements
		try {
			this.webJaguar.replaceDynamicElement(orderStatus, customer, salesRep, order, secureUrl, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		messageBuffer.append(orderStatus.getMessage());

		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File pdfFile = new File(tempFolder, "order_" + order.getOrderId() + ".pdf");
		try {
			// construct email message
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.setBcc(contactEmail);
			if (salesRep != null)
				helper.setCc(salesRep.getEmail());
			helper.setSubject(orderStatus.getSubject());
			if (orderStatus.isHtmlMessage()) {
				helper.setText(messageBuffer.toString(), true);
			} else {
				helper.setText(messageBuffer.toString());
			}
			if ((Boolean) gSiteConfig.get("gPDF_INVOICE") && siteConfig.get("ATTACHE_PDF_ON_PLACE_ORDER").getValue().equals("true")) {
				// attach pdf invoice
				if (this.webJaguar.createPdfInvoice(pdfFile, order.getOrderId(), request)) {
					helper.addAttachment("order_" + order.getOrderId() + ".pdf", pdfFile);
				}
			}
			mailSender.send(mms);
		} catch (Exception ex) {
			if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
				ex.printStackTrace();
			}
			ex.printStackTrace();
		}
		if (pdfFile.exists()) {
			pdfFile.delete();
		}
	}

	// notifySuppliers moved to webImplementation

	private void createZipFile(int orderId, File customerFolder) {
		if (customerFolder == null) {
			return;
		} else if (customerFolder.listFiles().length == 0) {
			customerFolder.delete();
			return;
		}

		// Create a buffer for reading the files
		byte[] buf = new byte[1024];

		File zipFolder = new File(getServletContext().getRealPath("/assets/orders_zip/"));
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				zipFolder = new File((String) prop.get("site.root"), "assets/orders_zip");
			}
		} catch (Exception e) {
		}
		if (!zipFolder.exists()) {
			zipFolder.mkdir();
		}

		try {
			// Create the ZIP file
			File zipFile = new File(zipFolder, orderId + ".zip");
			ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile));

			// Compress the files
			File file[] = customerFolder.listFiles();
			for (int i = 0; i < file.length; i++) {
				FileInputStream in = new FileInputStream(file[i]);

				// Add ZIP entry to output stream.
				out.putNextEntry(new ZipEntry(file[i].getName()));

				// Transfer bytes from the file to the ZIP file
				int len;
				while ((len = in.read(buf)) > 0) {
					out.write(buf, 0, len);
				}

				// Complete the entry
				out.closeEntry();
				in.close();
			}

			// Complete the ZIP file
			out.close();

			// delete temp files and folder
			for (int i = 0; i < file.length; i++) {
				file[i].delete();
			}
			customerFolder.delete();
		} catch (IOException e) {
			// do nothing
		}
	}

	private void moveCustomImages(int orderId, List<LineItem> lineItems) {

		boolean hasCustomImage = false;
		for (LineItem lineItem : lineItems) {
			if (lineItem.getCustomImageUrl() != null && lineItem.getCustomImageUrl().trim().length() > 0) {
				hasCustomImage = true;
				break;
			}
		}

		if (!hasCustomImage)
			return;

		File ordersCustomImageDir = new File(getServletContext().getRealPath("/assets/orders_customImage/"));
		File customImageDir = new File(getServletContext().getRealPath("/framer/pictureframer/images/FRAMED"));
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				ordersCustomImageDir = new File((String) prop.get("site.root"), "assets/customImages");
				customImageDir = new File((String) prop.get("site.root") + "/framer/pictureframer/images/FRAMED");
			}
		} catch (Exception e) {
		}
		if (!ordersCustomImageDir.exists()) {
			ordersCustomImageDir.mkdir();
		}
		ordersCustomImageDir = new File(ordersCustomImageDir, "" + orderId);
		if (!ordersCustomImageDir.exists()) {
			ordersCustomImageDir.mkdir();
		}

		for (LineItem lineItem : lineItems) {
			if (lineItem.getCustomImageUrl() != null && lineItem.getCustomImageUrl().trim().length() > 0) {
				// move file
				File customImageFile = new File(customImageDir, lineItem.getCustomImageUrl());
				customImageFile.renameTo(new File(ordersCustomImageDir, lineItem.getCustomImageUrl()));
			}
		}

	}

	private void initCustomShippingRates(OrderForm orderForm) {
		Map<String, Double> customShippingRateMap = new HashMap<String, Double>();
		orderForm.getOrder().setCustomShippingTotalWeight(0.0);
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		for (LineItem lineItem : orderForm.getOrder().getLineItems()) {
			Product product = this.webJaguar.getProductById(lineItem.getProductId(), 0, false, null);
			try {
				m = c.getMethod("getField" + siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue());
				if (((String) m.invoke(product, arglist)).equals("0")) {
					// get custom shipping
					for (int i = 1; i < 3; i++) {
						if (siteConfig.get("CUSTOM_SHIPPING_FIELD" + i).getValue().length() > 0) {
							m = c.getMethod("getField" + siteConfig.get("CUSTOM_SHIPPING_FIELD" + i).getValue());
							String value = (String) m.invoke(product, arglist);
							int qty = (siteConfig.get("CUSTOM_SHIPPING_FIXED_COST").getValue().length() > 0 ? 1 : lineItem.getQuantity());
							lineItem.setCustomShippingCost(Double.parseDouble(value) * qty);
							if (i == 1) {
								orderForm.getOrder().setCustomShippingTotalWeight(orderForm.getOrder().getCustomShippingTotalWeight() + (lineItem.getProduct().getWeight() * lineItem.getQuantity()));
							}
							if (customShippingRateMap.containsKey(siteConfig.get("CUSTOM_SHIPPING_TITLE" + i).getValue())) {
								customShippingRateMap.put(siteConfig.get("CUSTOM_SHIPPING_TITLE" + i).getValue(), customShippingRateMap.get(siteConfig.get("CUSTOM_SHIPPING_TITLE" + i).getValue())
										.doubleValue() + (Double.parseDouble(value) * qty));
							} else {
								customShippingRateMap.put(siteConfig.get("CUSTOM_SHIPPING_TITLE" + i).getValue(), (Double.parseDouble(value) * qty));
							}
						}
					}
				}
			} catch (Exception e) {
				// do nothing
			}
		}
		List<ShippingRate> customRatesList = new ArrayList<ShippingRate>();
		for (int i = 1; i < 3; i++) {
			if (customShippingRateMap.containsKey(siteConfig.get("CUSTOM_SHIPPING_TITLE" + i).getValue())) {
				customRatesList.add(new ShippingRate(siteConfig.get("CUSTOM_SHIPPING_TITLE" + i).getValue(), customShippingRateMap.get(siteConfig.get("CUSTOM_SHIPPING_TITLE" + i).getValue())
						.toString()));
			}
		}
		if (customRatesList.size() > 0) {
			orderForm.setCustomRatesList(customRatesList);
		}
	}

	private void notifyAdmin(Map<String, Configuration> siteConfig, String message, boolean informContactEmail) {
		// send email notification
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		String siteURL = siteConfig.get("SITE_URL").getValue();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

		SimpleMailMessage msg = new SimpleMailMessage();
		if (informContactEmail) {
			msg.setTo(contactEmail);
			msg.setBcc("developers@advancedemedia.com");
		} else {
			msg.setTo("developers@advancedemedia.com");
		}
		msg.setFrom(contactEmail);
		msg.setSubject("Ordering on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}

	private ModelAndView shopatronRPC(HttpServletRequest request, Map<String, Configuration> siteConfig, Order order, Customer customer) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request, null));
		ModelAndView modelAndView = new ModelAndView("frontend/checkout/shopatron");
		modelAndView.addObject("model", model);

		List<Object> params = new ArrayList<Object>();
		params.add(siteConfig.get("SHOPATRON").getValue()); // mfg_id.cat_id
		params.add(order.getLineItems().size()); // num_items

		Map<String, Map<String, Object>> order_data = new HashMap<String, Map<String, Object>>();
		int line = 0;
		for (LineItem lineItem : order.getLineItems()) {
			Map<String, Object> line_item = new HashMap<String, Object>();
			line_item.put("product_id", lineItem.getProduct().getSku());
			line_item.put("name", lineItem.getProduct().getName());
			line_item.put("price", lineItem.getUnitPrice());
			line_item.put("quantity", lineItem.getQuantity());
			if (lineItem.getProduct().getWeight() != null) {
				line_item.put("weight", lineItem.getProduct().getWeight());
			}

			order_data.put("item_" + ++line, line_item);
		}
		params.add(order_data);

		try {
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
			config.setServerURL(new URL("https://xml.shopatron.com/xmlServer.php"));
			XmlRpcClient client = new XmlRpcClient();
			client.setConfig(config);

			model.put("order_id", client.execute("examples.loadOrder", params));
		} catch (XmlRpcException e) {
			model.put("exception", e);
			notifyAdmin(siteConfig, "Error submitting customer " + order.getUserId() + " cart to shopatron.\n" + e.toString(), false);
		} catch (MalformedURLException e) {
			model.put("exception", e);
		}

		if (model.get("order_id") != null) {
			// email order details
			String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
			String siteURL = siteConfig.get("SITE_URL").getValue();

			// multi store
			List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
			MultiStore multiStore = null;
			if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
				multiStore = multiStores.get(0);
				contactEmail = multiStore.getContactEmail();
				siteURL = request.getScheme()+"://" + multiStore.getHost() + request.getContextPath() + "/";
			}
			String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
			SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

			SimpleMailMessage msg = new SimpleMailMessage();
			msg.setTo(contactEmail);
			msg.setFrom(contactEmail);
			msg.setSubject("Shopatron Order ID: " + model.get("order_id") + " placed at " + siteURL);
			try {
				StringBuffer message = new StringBuffer();
				message.append(dateFormatter.format(new Date()));
				message.append("\n\n");
				message.append("Customer " + customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName());
				message.append("\n\n");
				message.append(secureUrl + "admin/customers/customer.jhtm?id=" + customer.getId());
				message.append("\n\n");

				message.append("Number of items: " + order.getLineItems().size() + "\n");
				message.append("------------------------------" + "\n");
				for (LineItem lineItem : order.getLineItems()) {
					message.append("sku = " + lineItem.getProduct().getSku() + "\n");
					message.append("desc = " + lineItem.getProduct().getName() + "\n");
					message.append("price = " + lineItem.getUnitPrice() + "\n");
					message.append("quantity = " + lineItem.getQuantity() + "\n");
					message.append("------------------------------" + "\n");
				}

				msg.setText(message.toString());
				mailSender.send(msg);
			} catch (MailException ex) {
				// do nothing
			}

			// clear user cart from session and database
			request.getSession().removeAttribute("userCart");
			this.webJaguar.deleteShoppingCart(customer.getId(), order.getManufacturerName());
		}

		return modelAndView;
	}

	private Document createXMLCart(OrderForm orderForm, String merchantCalculationUrl) throws Exception {

		List<LineItem> lineItems = orderForm.getOrder().getLineItems();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Document doc = null;
		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.newDocument();
		} catch (ParserConfigurationException pce) {
			System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
			System.exit(1);
		}

		// root element
		Element root = doc.createElement("checkout-shopping-cart");
		root.setAttribute("xmlns", "http://checkout.google.com/schema/2");
		doc.appendChild(root);

		// shopping cart
		Element shoppingCart = doc.createElement("shopping-cart");
		root.appendChild(shoppingCart);

		// private data to send order Id
		Element merchantPrivate = doc.createElement("merchant-private-data");
		shoppingCart.appendChild(merchantPrivate);

		Element merchantNote = doc.createElement("merchant-note");
		merchantPrivate.appendChild(merchantNote);
		merchantNote.appendChild(doc.createTextNode("" + orderForm.getOrder().getOrderId()));

		// line items
		Element items = doc.createElement("items");
		shoppingCart.appendChild(items);

		Iterator<LineItem> iter = lineItems.iterator();
		while (iter.hasNext()) {
			LineItem lineItem = iter.next();
			Element item = createItemElement(lineItem, doc, orderForm.getOrder(), "item");
			items.appendChild(item);
		}

		// discount if any
		if (orderForm.getOrder().getPromo() != null) {
			Element discount = createItemElement(null, doc, orderForm.getOrder(), "promo");
			items.appendChild(discount);
		}

		// buy safe
		if (orderForm.getOrder().getBondCost() != null) {
			Element bondCost = createItemElement(null, doc, orderForm.getOrder(), "buySafe");
			items.appendChild(bondCost);
		}

		// check out flow support
		Element flowSupport = doc.createElement("checkout-flow-support");
		root.appendChild(flowSupport);

		// merchant flow support
		Element merchantFlowSupport = doc.createElement("merchant-checkout-flow-support");
		flowSupport.appendChild(merchantFlowSupport);

		// shipping methods
		Element shippingMethods = doc.createElement("shipping-methods");
		merchantFlowSupport.appendChild(shippingMethods);

		// merchant calculated shipping
		Element merchantCalculatedShipping = doc.createElement("merchant-calculated-shipping");
		if (orderForm.getOrder().getShippingMethod() != null) {
			merchantCalculatedShipping.setAttribute("name", orderForm.getOrder().getShippingMethod());
		} else if (orderForm.getOrder().getCustomShippingTitle() != null) {
			merchantCalculatedShipping.setAttribute("name", orderForm.getOrder().getCustomShippingTitle());
		}
		shippingMethods.appendChild(merchantCalculatedShipping);

		// merchant calculations
		Element merchantCalculations = doc.createElement("merchant-calculations");
		merchantFlowSupport.appendChild(merchantCalculations);

		// merchant calculations url
		Element merchantCalculationsUrl = doc.createElement("merchant-calculations-url");
		merchantCalculationsUrl.appendChild(doc.createTextNode(merchantCalculationUrl));
		merchantCalculations.appendChild(merchantCalculationsUrl);

		// price
		Element price = doc.createElement("price");
		price.setAttribute("currency", "USD");
		Double shippingCost = 0.0;
		if (orderForm.getOrder().getShippingCost() != null) {
			shippingCost = shippingCost + orderForm.getOrder().getShippingCost();
		}
		if (orderForm.getOrder().getCustomShippingCost() != null) {
			shippingCost = shippingCost + orderForm.getOrder().getCustomShippingCost();
		}
		price.appendChild(doc.createTextNode("" + shippingCost));
		merchantCalculatedShipping.appendChild(price);

		// tax tables
		Element taxTables = doc.createElement("tax-tables");
		taxTables.setAttribute("merchant-calculated", "true");
		merchantFlowSupport.appendChild(taxTables);

		// default tax table
		Element defaultTaxTable = doc.createElement("default-tax-table");
		taxTables.appendChild(defaultTaxTable);

		// tax rules
		Element taxRules = doc.createElement("tax-rules");
		defaultTaxTable.appendChild(taxRules);

		// default tax rule
		Element defaultTaxRule = doc.createElement("default-tax-rule");
		taxRules.appendChild(defaultTaxRule);

		// exempt shipping from tax
		Element shippingTaxed = doc.createElement("shipping-taxed");
		Text shippingTax = doc.createTextNode("false");
		shippingTaxed.appendChild(shippingTax);
		defaultTaxRule.appendChild(shippingTaxed);

		// tax rate
		Element rate = doc.createElement("rate");
		rate.appendChild(doc.createTextNode("" + orderForm.getOrder().getTaxRate() / 100));
		defaultTaxRule.appendChild(rate);

		// tax area
		Element taxArea = doc.createElement("tax-area");
		defaultTaxRule.appendChild(taxArea);

		// us state area for tax
		Element usStateArea = doc.createElement("us-state-area");
		taxArea.appendChild(usStateArea);

		// us state for tax
		Element state = doc.createElement("state");
		state.appendChild(doc.createTextNode("" + orderForm.getOrder().getShipping().getStateProvince()));
		usStateArea.appendChild(state);

		return doc;
	}

	private Element createItemElement(LineItem lineItem, Document doc, Order order, String type) {

		Element item = doc.createElement("item");

		Element name = doc.createElement("item-name");
		if (type.equalsIgnoreCase("item")) {
			name.appendChild(doc.createTextNode(lineItem.getProduct().getSku()));
		} else if (type.equalsIgnoreCase("promo")) {
			name.appendChild(doc.createTextNode("Discount"));
		}
		item.appendChild(name);

		Element disc = doc.createElement("item-description");
		if (lineItem != null) {
			disc.appendChild(doc.createTextNode(lineItem.getProduct().getShortDesc()));
		} else if (type.equalsIgnoreCase("promo")) {
			disc.appendChild(doc.createTextNode("Promo code " + order.getPromoCode()));
		} else {
			disc.appendChild(doc.createTextNode("BuySafe Bond Guarantee "));
		}
		item.appendChild(disc);

		Element price = doc.createElement("unit-price");
		if (type.equalsIgnoreCase("item")) {
			price.appendChild(doc.createTextNode(lineItem.getUnitPrice().toString()));
		} else if (type.equalsIgnoreCase("promo")) {
			price.appendChild(doc.createTextNode("-" + (order.getPromoAmount() + order.getLineItemPromoAmount())));
		} else {
			price.appendChild(doc.createTextNode(order.getBondCost().toString()));
		}
		price.setAttribute("currency", "USD");
		item.appendChild(price);

		Element qty = doc.createElement("quantity");
		if (type.equalsIgnoreCase("item")) {
			qty.appendChild(doc.createTextNode("" + lineItem.getQuantity()));
		} else {
			qty.appendChild(doc.createTextNode("" + 1));
		}
		item.appendChild(qty);

		if (type.equalsIgnoreCase("item")) {
			Element weight = doc.createElement("item-weight");
			weight.setAttribute("unit", "LB");
			weight.setAttribute("value", "" + lineItem.getProduct().getWeight() * lineItem.getQuantity());
			item.appendChild(weight);
		}

		return item;
	}

	private Map<String, String> encodeCart(Document dom, String key) throws Exception {

		Map<String, String> encryptedCart = new HashMap<String, String>();
		try {
			StringBuilder stringBuilder = null;

			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			OutputFormat outputformat = new OutputFormat();
			outputformat.setIndenting(true);
			outputformat.setPreserveSpace(false);
			XMLSerializer serializer = new XMLSerializer();
			serializer.setOutputFormat(outputformat);
			serializer.setOutputByteStream(stream);
			serializer.asDOMSerializer();
			serializer.serialize(dom.getDocumentElement());

			stringBuilder = new StringBuilder(stream.toString());
			Encoder encoder = new Encoder();
			String encodedCart = encoder.encode(stringBuilder.toString());
			String sign = encoder.sign(stringBuilder.toString(), key);

			encryptedCart.put("encodedCart", encodedCart);
			encryptedCart.put("sign", sign);

		} catch (Exception e) {
		}

		return encryptedCart;
	}

	private void moveImageToCustomerFolder(List<?> cartItems, HttpServletRequest request, int userId, Integer orderId) {

		File baseFile = new File(getServletContext().getRealPath("temp/Cart/"));
		if (!baseFile.exists()) {
			return;
		}
		File attachment = null;
		for (Object item : cartItems) {

			String attach = null;
			if (item.getClass().isInstance(new CartItem())) {
				attach = ((CartItem) item).getAttachment();
			} else if (item.getClass().isInstance(new LineItem())) {
				attach = ((LineItem) item).getAttachment();
			}

			if (attach == null) {
				continue;
			}
			if (item.getClass().isInstance(new CartItem())) {
				try {
					attachment = new File(baseFile, "/session/" + request.getSession().getId() + "/" + attach + "/");
					if (attachment != null) {
						baseFile = new File(baseFile, ("/customer/"));
						if (!baseFile.exists()) {
							baseFile.mkdir();
						}

						baseFile = new File(baseFile, ("/" + userId + "/"));
						if (!baseFile.exists()) {
							baseFile.mkdir();
						}

						// save attachment
						if (baseFile.canWrite()) {
							File newFile = new File(baseFile, attach);
							attachment.renameTo(newFile);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if (item.getClass().isInstance(new LineItem()) && orderId != null) {
				try {
					attachment = new File(baseFile, "/customer/" + userId + "/" + attach + "/");
					if (attachment != null) {
						baseFile = new File(getServletContext().getRealPath("temp"));

						baseFile = new File(baseFile, "/Order/");
						if (!baseFile.exists()) {
							baseFile.mkdir();
						}

						baseFile = new File(baseFile, ("/" + orderId + "/"));
						if (!baseFile.exists()) {
							baseFile.mkdir();
						}

						// save attachment
						if (baseFile.canWrite()) {
							File newFile = new File(baseFile, attach);
							attachment.renameTo(newFile);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void updateMasterSkus(Order order, Map<String, Object> model) {
		Map<String, CartItem> groupItems = null;
		for (LineItem lineItem : order.getLineItems()) {
			if (groupItems == null) {
				groupItems = new HashMap<String, CartItem>();
			}
			if (lineItem.getItemGroup() != null) {
				CartItem ci = null;
				if (groupItems.get(lineItem.getItemGroup()) != null) {
					ci = groupItems.get(lineItem.getItemGroup());
					ci.setTempItemTotal(ci.getTempItemTotal() + lineItem.getTotalPrice());
				} else {
					ci = new CartItem();
					ci.setTempItemTotal(lineItem.getTotalPrice());
				}
				if (ci.getProduct().getSku() == null) {
					Product product = this.webJaguar.getProductById(lineItem.getProduct().getId(), 0, false, null);
					if (product.getMasterSku() != null) {
						Integer masterProductId = this.webJaguar.getProductIdBySku(product.getMasterSku());
						if (masterProductId != null) {
							Product parentProduct = this.webJaguar.getProductById(masterProductId, 0, false, null);
							ci.setProduct(parentProduct);
						}
					}
				}
				// add quantity only if item is main item and not decorative (added) sku
				if (lineItem.isItemGroupMainItem()) {
					ci.setQuantity(ci.getQuantity() + lineItem.getQuantity());
				}
				groupItems.put(lineItem.getItemGroup(), ci);
			}
		}
		if (model != null) {
			model.put("groupItems", groupItems);
			model.put("viewInvoice", true);
		}
	}

}