/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.12.2010
 */

package com.webjaguar.web.frontend.shoppingCart;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Product;
import com.webjaguar.model.UserSession;

public class AddVariantToCartController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	public ModelAndView handleRequest( HttpServletRequest request, HttpServletResponse response ) throws Exception {
		
		UserSession userSession = this.webJaguar.getUserSession(request);
		Integer userId = null;
		if (userSession != null) {
			userId = userSession.getUserid();
		}

		Cart cart = null;
		boolean updateCart = false;
		
		for (String variantId: ServletRequestUtils.getStringParameters(request, "variantId")) {
			String variantSku[] = ServletRequestUtils.getStringParameters(request, "variant_" + variantId + "_sku");
			String variantQty[] = ServletRequestUtils.getStringParameters(request, "variant_" + variantId + "_qty");
			for (int i=0; i<variantQty.length; i++) {
				try {
					int qty = Integer.parseInt(variantQty[i]);
					if (qty > 0) {
						List<Product> variants = this.webJaguar.getProductVariant(Integer.parseInt(variantId), variantSku[i], false);
						if (!variants.isEmpty()) {
							CartItem cartItem = new CartItem();
							cartItem.getProduct().setId(variants.get(0).getId());
							cartItem.setVariantSku(variants.get(0).getSku());
							cartItem.setQuantity(qty);
							
							if (userId != null) { 
								cart = this.webJaguar.getUserCart(userId, null);
								cart.setUserId(userId);
								
								// save on database
								this.webJaguar.addToCart(cartItem, userId);
							}
							else{ 
								// store in session
								cart = (Cart) WebUtils.getOrCreateSessionAttribute(request.getSession(), "sessionCart", Cart.class);
								if (cart.containsCartItem(cartItem)) {
									cart.incrementQuantityByCartItem(cartItem);
								} else {
									// add item
									cart.addCartItem(cartItem);
								}			
							}
							
							updateCart = true;
						}
					}
				} catch (NumberFormatException e) {
					// do nothing
					if (request.getParameter("debug") != null) e.printStackTrace();
				}
			}			
		}
		
		if (updateCart) {
			this.webJaguar.updateCart(cart, request);			
		}
		
		return new ModelAndView(new RedirectView("viewCart.jhtm"));
	}
}
