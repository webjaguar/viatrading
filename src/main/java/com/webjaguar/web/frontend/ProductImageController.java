/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 01.02.2008
 */

package com.webjaguar.web.frontend;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductImage;

public class ProductImageController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {	
    	String layout = ServletRequestUtils.getStringParameter( request, "layout", "" );
    	
    	Product product = this.webJaguar.getProductById( ServletRequestUtils.getIntParameter( request, "id", 0 ), request );
		if ( product != null ) {
			// get images from custom folder
			if ( product.getImageFolder() != null && (product.getImageFolder().trim().equals( "" ) || product.getImageFolder().startsWith( "." )) )
				product.setImageFolder( null );
			if ( product.getImageFolder() != null ) {
				File baseFile = new File( getServletContext().getRealPath( "/assets/Image/Product" ) );
				Properties prop = new Properties();
				try
				{
					prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
					if ( prop.get( "site.root" ) != null )
					{
						baseFile = new File( (String) prop.get( "site.root" ) + "/assets/Image/Product" );
					}
				}
				catch ( Exception e )
				{}
				File detailsbig = new File( baseFile, "detailsbig" );
				File imageFolder = new File ( detailsbig, product.getImageFolder() );
				if ( imageFolder.exists() && imageFolder.isDirectory() ) {
					for ( File image : imageFolder.listFiles()) {
						if ( !image.isHidden() && !image.isDirectory() && 
								(image.getName().toLowerCase().endsWith( ".jpg" ) || image.getName().toLowerCase().endsWith( ".gif" ))) {
							ProductImage pi = new ProductImage();
							pi.setImageUrl( product.getImageFolder() + "/" + image.getName() );
							product.addImage( pi );
						}
					}
				}
			}
		}

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("imgUrl", ServletRequestUtils.getStringParameter( request, "imgUrl" ));
		model.put("product", product);
		
		return new ModelAndView("frontend/layout/template1/productImages"+layout, "model", model);    	
    }

}
