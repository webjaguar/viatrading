/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 9.10.2008
 */

package com.webjaguar.web.frontend.rma;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.Rma;
import com.webjaguar.model.UserSession;

public class RmaWizardController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }

	private Map<String, Configuration> siteConfig;
	
	public RmaWizardController() {
		setCommandName("rma");
		setCommandClass(Rma.class);
		setPages(new String[] {"frontend/rma/step1", "frontend/rma/step2", "frontend/rma/step3"});
	}
	
    public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {	
    	Rma rma = (Rma) command;
    	
    	// check serial number
    	boolean valid = false;
    	
    	Integer orderId = this.webJaguar.getLastOrderId(rma.getSerialNum());
    	if (orderId != null) {
			valid = true;
		}
    	
    	if (valid) {
        	this.webJaguar.insertRma(rma);   
        	notifyAdmin(rma, request);
    	} else {
    		errors.rejectValue("serialNum", "INVALID_SERIAL_NUM");
        	return showForm(request, response, errors);
    	}
		
    	return new ModelAndView(new RedirectView("account_rmas.jhtm"));
    }
 
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
    	throws Exception {
    	
       	Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		map.put("model", model); 
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		return map;
    }
    
    protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {

    	UserSession userSession = this.webJaguar.getUserSession(request);
    	Rma rma = (Rma) command;
    	
		switch ( page ) {
			case 0:	
				if (!errors.hasErrors()) {
					Integer lastOrderId = null;
					if (rma.getSerialNum().length() > 0) {
						lastOrderId = this.webJaguar.getLastOrderId(rma.getSerialNum());
						if (lastOrderId == null) {
							errors.rejectValue("serialNum", "INVALID_SERIAL_NUM");
							break;						
						}
					}
					if (rma.getOrderId() != null) {
						if (lastOrderId != null &&
								rma.getOrderId().compareTo(lastOrderId) != 0) {
							errors.rejectValue("orderId", "order.exception.notfound");
							break;	
						}
					}
					
					// check if customer is allowed to submit an rma for this serialnum
					Integer orderId = null;
					if (rma.getOrderId() != null) {
						orderId = rma.getOrderId();
					} else {
						orderId = lastOrderId;
					}
					Order order = this.webJaguar.getOrder(orderId, null);
					boolean valid = false;
					
					if (order != null && !order.getStatus().equals("x")) {
						for (Customer customer:this.webJaguar.getFamilyTree(order.getUserId(), "up")) {
							if (customer.getId().compareTo(userSession.getUserid()) == 0) {
								valid = true;
								break;
							}
						}						
					}
					if (valid) {
						rma.setOrderId(orderId);
						rma.setSerialNums(new TreeSet<String>());
						for (LineItem lineItem: order.getLineItems()) {
							if (lineItem.getSerialNums() != null) {
								rma.getSerialNums().addAll(lineItem.getSerialNums());
							}
						}							
					} else {
						errors.rejectValue("orderId", "order.exception.notfound");							
					}
				}
				break;
			case 1:	
				if (!errors.hasErrors()) {
					Integer lastOrderId = this.webJaguar.getLastOrderId(rma.getSerialNum());
					if (rma.getOrderId().compareTo(lastOrderId) != 0) {
						errors.rejectValue("serialNum", "INVALID_SERIAL_NUM");
					}
				}
				rma.setLineItem(this.webJaguar.getLineItem(rma.getOrderId(), rma.getSerialNum()));
				
				// check if there is an existing rma
				Rma lastRma = this.webJaguar.getRma(new Rma(rma.getOrderId(), rma.getSerialNum()));
				if (lastRma != null) {
					errors.rejectValue("serialNum", "EXISTING_REQUEST");
					request.setAttribute("lastRma", lastRma);
				}				
				
				break;
		}
    }
    
    protected void validatePage(Object command, Errors errors, int page) {
    	
    	Rma rma = (Rma) command;
		
    	// trim serial number
    	rma.setSerialNum(rma.getSerialNum().trim());
    	
		switch (page) {
			case 0:
				if (!errors.hasErrors()) {
					if (rma.getSerialNum().length() == 0) {
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "orderId", "form.required");
					}
				}
				break;
			case 1:
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "serialNum", "form.required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "problem", "form.required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "action", "form.required");
				break;
		}
    }
    
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );

		return new Rma();
	}

	private void notifyAdmin(Rma rma, HttpServletRequest request) {
		// send email notification
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("New RMA Request on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" +
					"You have a new RMA Request on your site.\n\n" +
					siteURL + "admin/rma/rma.jhtm?num=" + rma.getRmaNum());
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
}
