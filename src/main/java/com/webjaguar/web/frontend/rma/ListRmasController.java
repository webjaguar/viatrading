/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.16.2008
 */

package com.webjaguar.web.frontend.rma;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.RmaSearch;
import com.webjaguar.model.UserSession;

public class ListRmasController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {	

		// check if add button was pressed
		if (request.getParameter("__add") != null) {
			return new ModelAndView(new RedirectView("newRma.jhtm"));				
		}
		
    	UserSession userSession = this.webJaguar.getUserSession(request);
		    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
				
		// get service requests
		RmaSearch search = getSearch(request);
		for (Customer customer:this.webJaguar.getFamilyTree(userSession.getUserid(), "down")) {
			search.addUserId(customer.getId());
		}
		model.put("rmaSearch", search);
		
		PagedListHolder rmaList = new PagedListHolder(this.webJaguar.getRmaList(search));
		rmaList.setPageSize(search.getPageSize());
		rmaList.setPage(search.getPage()-1);
		model.put("rmas", rmaList);
		
		setLayout(request); 
		
		return new ModelAndView("frontend/account/rmas", "model", model);    	
    }

	private void setLayout(HttpServletRequest request) {
		Layout layout = (Layout) request.getAttribute( "layout" );
		layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "RMA"));
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true" )) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		}
		this.webJaguar.updateCartQuantity(null, request, layout);
	}
	
	private RmaSearch getSearch(HttpServletRequest request) {
		RmaSearch search = new RmaSearch();
		
		// status
		if (request.getParameter("status") != null) {
			search.setStatus(ServletRequestUtils.getStringParameter( request, "status", "" ));
		}
		
		// rma number
		if (request.getParameter("rmaNum") != null) {
			search.setRmaNum(ServletRequestUtils.getStringParameter( request, "rmaNum", "" ));
		}

		// order number
		if (request.getParameter("orderId") != null) {
			search.setOrderId(ServletRequestUtils.getStringParameter( request, "orderId", "" ));
		}
		
		// serial num
		if (request.getParameter("serialNum") != null) {
			search.setSerialNum(ServletRequestUtils.getStringParameter( request, "serialNum", "" ));
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}	
		return search;
	}
	
}
