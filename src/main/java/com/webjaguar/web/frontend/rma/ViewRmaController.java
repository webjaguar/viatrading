/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.16.2008
 */

package com.webjaguar.web.frontend.rma;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Order;
import com.webjaguar.model.Rma;
import com.webjaguar.model.UserSession;

public class ViewRmaController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {
    	
    	UserSession userSession = this.webJaguar.getUserSession(request);

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		Rma rma = this.webJaguar.getRma(new Rma(ServletRequestUtils.getIntParameter(request, "num", -1)));
		boolean valid = false;
		if (rma != null) {
			Order order = this.webJaguar.getOrder(rma.getOrderId(), null);
			for (Customer customer:this.webJaguar.getFamilyTree(order.getUserId(), "up" )) {
				if (customer.getId().compareTo( userSession.getUserid() ) == 0) {
					valid = true;
					break;
				}
			}
		}
		
		if (rma != null && valid) {
			model.put("rma", rma);
		} else {
      	  	model.put("message", "rma.exception.notfound");			
		}
		
		setLayout(request); 
    	
		return new ModelAndView("frontend/account/rma", "model", model);
	}

	private void setLayout(HttpServletRequest request) {
		Layout layout = (Layout) request.getAttribute("layout");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if ( siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		}		
	}
}
