/* Copyright 2005, 2011 Advanced E-Media Solutions
 * author: Jwalant Patel
 */

package com.webjaguar.web.frontend.product;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Option;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductOptionValue;

public class AjaxCustomFrameOptionsController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	List<ProductOption> optionsList;
	private static double price;
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{	
		optionsList = new ArrayList<ProductOption>();	
		Map<String, Map<Integer, Integer>> valueMap = new HashMap<String, Map<Integer,Integer>>();
		Map<String, Integer> originalIndexSelection = new HashMap<String, Integer>();
		Map<String, Object> originalValueSelection = new HashMap<String, Object>();
		Map<String, String> selectedOptions = new HashMap<String, String>();
		Product product = this.webJaguar.getProductById(ServletRequestUtils.getIntParameter(request, "productId"), request);
		Option baseOpt =  null;
		String cusValue = null;
		if(product != null) {
			baseOpt =  this.webJaguar.getOption(null, product.getOptionCode());
			if(baseOpt != null) {
				for(ProductOption productOption : baseOpt.getProductOptions()) {
					  optionsList.add(productOption);
				}
			}	
		}
		if(ServletRequestUtils.getStringParameter(request, "option") != null) {
			for(String option : ServletRequestUtils.getStringParameters(request, "option")) {
				String[] data = option.split("-");
				
				String optionCode = null;
				Integer optionIndex = null;
				Integer valueIndex = null;
				try {
					optionCode = data[0];
					if(!data[1].contains("cus_with_index_")) {
						optionIndex = Integer.parseInt( data[1].split("_")[3] );
						originalIndexSelection.put(data[0]+"-"+data[1], Integer.parseInt( data[2] ));
						valueIndex = Integer.parseInt( data[2] );
					} else {
						optionIndex = Integer.parseInt( data[1].split("_")[6] );
						valueIndex = this.getNearestOptionIndexWithHigherArea(data[2].split("_")[1]);
						originalValueSelection.put(data[0]+"-"+data[1], valueIndex+"_"+data[2].split("_")[1]);
						selectedOptions.put(data[0].trim(), data[2].split("_")[1]);
						cusValue = data[2].split("_")[1];
					}
					if(valueMap.get(optionCode) != null) {
						valueMap.get(optionCode).put(optionIndex, valueIndex);
					} else {
						Map<Integer, Integer> tempMap = new HashMap<Integer, Integer>();
						tempMap.put(optionIndex, valueIndex);
						valueMap.put(optionCode, tempMap);
					}
				} catch(Exception e) { e.printStackTrace(); }
				
			}
		}
		Set<String> imageUrlSet = new HashSet<String>();
		
		// change the price1 only and add only price per item. Need to add more code, if customer want to change all prices or price range.
		// Option price that is one time only is not added to Price1.
		price = product.getPrice1();
		
		this.getDependingOptions(baseOpt.getCode(), product.getId(), valueMap, imageUrlSet, optionsList, selectedOptions, originalValueSelection);
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		for(ProductOption option : optionsList) {
			
			// new dropdown's first options' imageUrl 
			if(valueMap.get(option.getOptionCode()) == null) {
				for(ProductOptionValue value : option.getValues()) {
					if(value.getIndex() == 0) {
						if(value.getImageUrl() != null && !value.getImageUrl().isEmpty()) {
							imageUrlSet.add(value.getImageUrl());
						}
					}
				}
			}
		}
		
		myModel.put("product", product);
		myModel.put("optionList", optionsList);
		myModel.put("imageUrlSet", imageUrlSet);
		myModel.put("productId", ServletRequestUtils.getStringParameter(request, "productId"));
		myModel.put("originalIndexSelection", originalIndexSelection);
		myModel.put("originalValueSelection", originalValueSelection);
		
		// apply sales tag
		if(product.getSalesTag() != null && product.getSalesTag().isInEffect()) {
			double discountedPrice = price;
			if(product.getSalesTag().isPercent()) {
				discountedPrice = ( (100 - product.getSalesTag().getDiscount()) /100 ) * price;
			} else {
				discountedPrice = price - (( product.getSalesTag().getDiscount() > price ) ? product.getSalesTag().getDiscount() : price);
			}
			myModel.put("discountedPrice", discountedPrice);
			myModel.put("salesTag", product.getSalesTag());
		}
		myModel.put("price", price);
		myModel.put("selectedOptions", selectedOptions);
		
		
		
		//get assigned Product
		if(optionsList != null) {
			// get last option from all available options as we want to assign to a PRODUCT on last step
			List<ProductOptionValue> optValues = optionsList.get(optionsList.size() - 1).getValues();
			if(optValues != null) {
				for(ProductOptionValue optValue : optValues) {
					// get value that is selected by user and has assigned product id
					if(selectedOptions.containsValue(optValue.getName().trim()) && optValue.getAssignedProductId() != null) {
						
						Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
						Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
						
						Product assignedProduct = this.webJaguar.getProductById(optValue.getAssignedProductId(), 5, false, null);
						myModel.put("assignedProduct", assignedProduct);
						
						
						// add product to comparison
						if((Boolean) gSiteConfig.get( "gCOMPARISON" )) {
							
							List<CartItem> cartItemsComparisonList = (List) request.getSession().getAttribute( "cartItemsComparisonList" );
							if (cartItemsComparisonList == null) {
								cartItemsComparisonList = new ArrayList<CartItem>();
								request.getSession().setAttribute( "cartItemsComparisonList", cartItemsComparisonList );
							}
							
							CartItem item = new CartItem();
							item.setProduct(assignedProduct);
							List<ProductAttribute> attributeList = new ArrayList<ProductAttribute>();
							ProductAttribute attribute;
							for(String key : valueMap.keySet()) {
								Integer optionIndex = -1;
								for(Integer index : valueMap.get(key).keySet()) {
									optionIndex = index;
								}
								if(valueMap.get(key).get(optionIndex) != null) {
									if(!key.contains("Choose Your Diploma Size") || cusValue == null) {
										attribute = new ProductAttribute(key, optionIndex, valueMap.get(key).get(optionIndex), null);
									} else {
										attribute = new ProductAttribute(key, optionIndex, cusValue, -1);
									}
									attributeList.add(attribute);
								} 
							}
							item.setProductAttributes(attributeList);
							
							myModel.put("cartItemsComparisonList", cartItemsComparisonList );
							int maxComparisons = new Integer( ((Configuration) siteConfig.get( "COMPARISON_MAX" )).getValue() );
							myModel.put("maxComparisons", maxComparisons );

							if(maxComparisons > cartItemsComparisonList.size()) {
								boolean isItemAddedToList = false;
								for(CartItem cartItem : cartItemsComparisonList) {
									if(cartItem.toString().equals(item.toString())) {
										isItemAddedToList = true;
										break;
									}
								}
								myModel.put("isItemAddedToList", isItemAddedToList );
							}
						}
					}
				}
				
			}
		}
		request.getSession().setAttribute( "continueShoppingUrl", "product.jhtm?id="+product.getId() );

		return new ModelAndView("frontend/framestore/smartOptions", "model", myModel);
	}
	
	//function with hard coded values just for custom framestore
	private int getNearestOptionIndexWithHigherArea(String size){
		double width = 0;
		double height = 0;
		
		try {
			width = Double.parseDouble(size.split("X")[0]);
		}catch(Exception e) {e.printStackTrace();}

		try {
			height = Double.parseDouble(size.split("X")[1]);
		}catch(Exception e) {e.printStackTrace();}
		
		if(width > 0 && height > 0){
			
			if(width + height <= 14) {
				return 0;
			} else if((width + height) > 14 && (width + height) <= 19.5) {
				return 1;
			} else if((width + height) > 19.5 && (width + height) <= 25) {
				return 2;
			} else if((width + height) > 25 && (width + height) <= 36) {
				return 3;
			} 
		}
		
		return -1;
	}
	
	//recursive function to get all options based on selection
	public void getDependingOptions(String optionCode, int productId, Map<String, Map<Integer, Integer>> valueMap, Set<String> imageUrlSet, List<ProductOption> optionsList, Map<String, String> selectedOptions, Map<String, Object> originalValueSelection ){

		if(optionCode != null && valueMap.get(optionCode) != null) {
			Option opt =  this.webJaguar.getOption(null, optionCode);
			
			if(opt != null) {
				for(ProductOption productOption : opt.getProductOptions()) {
					
					String dependencyId = null;
					for(ProductOptionValue value : productOption.getValues()) {
						if(valueMap.get(optionCode).get(productOption.getIndex()) != null && value.getIndex() == valueMap.get(optionCode).get(productOption.getIndex())) {
							dependencyId = value.getDependingOptionIds();
							if(value.getOptionPrice() != null) {
								price = price + value.getOptionPrice();
							}
							 
							if(value.getImageUrl() != null && !value.getImageUrl().isEmpty()) {
								imageUrlSet.add(value.getImageUrl());
							}
							//selectedOptions
							if(originalValueSelection.get(opt.getCode().trim()+"-option_values_cus_with_index_"+productId+"_"+productOption.getIndex()) == null) {
								selectedOptions.put(productOption.getName().trim(), value.getName());
							}
						}
					}
					
					if(dependencyId != null) {
						Set<Integer> optionIdSet = new HashSet<Integer>();
						for(String id : dependencyId.split(",")){
							try {
								optionIdSet.add(Integer.parseInt(id));
							} catch (Exception e) { e.printStackTrace(); }
						}
						
						for(Integer id : optionIdSet) {
							Option newOption = this.webJaguar.getOption(id, null);
							if(newOption != null) {
								for(ProductOption option : newOption.getProductOptions()) {
									int index = -1;
									for(ProductOption tempOpt : optionsList) {
										if(tempOpt.getOptionCode().equalsIgnoreCase(productOption.getOptionCode()) && tempOpt.getName().equalsIgnoreCase(productOption.getName())) {
											index = optionsList.indexOf(tempOpt);
											break;
										}
									}
									if(index != -1) {
										optionsList.add(index+1, option);
									} else {
										optionsList.add(option);
									}
								}
								getDependingOptions(newOption.getCode(), productId, valueMap, imageUrlSet, optionsList, selectedOptions, originalValueSelection);
							}
						}
					}
				}
			}
		}
	}
	
}