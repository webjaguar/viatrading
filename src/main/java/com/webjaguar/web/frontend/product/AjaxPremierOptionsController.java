/* Copyright 2005, 2011 Advanced E-Media Solutions
 * author: Jwalant Patel
 */

package com.webjaguar.web.frontend.product;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.IncludedProduct;
import com.webjaguar.model.Option;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductOptionValue;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.web.domain.Constants;

public class AjaxPremierOptionsController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	Map<String, Configuration> siteConfig;
	Map<String, Object> gSiteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {	
		double newPrice;
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		Product product = this.webJaguar.getProductById(ServletRequestUtils.getIntParameter(request, "productId"), request);
		
		// get child skus
		this.getChildSkus(product, request, myModel);
		
		List<ProductOption> optionsList = new ArrayList<ProductOption>();	
			
		Map<String, Map<Integer, Integer>> valueMap = new HashMap<String, Map<Integer,Integer>>();
		Map<String, Integer> originalSelection = new HashMap<String, Integer>();
		if(ServletRequestUtils.getStringParameter(request, "option") != null) {
			for(String option : ServletRequestUtils.getStringParameters(request, "option")) {
				
				/*
				 * example, option=117Screenprint-option_values_55232_0-1
				 * where optionCode = 117Screenprint,
				 * option index = 0
				 * value index = 1
				 * product id = 55232
				 * 
				 * */
				String[] data = option.split("-");
				
				String optionCode = null;
				Integer optionIndex = null;
				Integer valueIndex = null;
				try {
					originalSelection.put(data[0]+"-"+data[1], Integer.parseInt( data[2] ));
					optionCode = data[0];
					optionIndex = Integer.parseInt( data[1].split("_")[3] );
					valueIndex = Integer.parseInt( data[2] );
					
					if(valueMap.get(optionCode) != null) {
						valueMap.get(optionCode).put(optionIndex, valueIndex);
					} else {
						Map<Integer, Integer> tempMap = new HashMap<Integer, Integer>();
						tempMap.put(optionIndex, valueIndex);
						valueMap.put(optionCode, tempMap);
					}
				} catch(Exception e) { 
					//e.printStackTrace();
				}
				
			}
		}
		
		// one value for custom text
		if(ServletRequestUtils.getStringParameter(request, "option_cus") != null) {
			Map<String, String> originalCustomText = new HashMap<String, String>();
			for(String option : ServletRequestUtils.getStringParameters(request, "option_cus")) {
				String[] data = option.split("-");
				try {
					originalCustomText.put(data[0]+"-"+data[1],  data[2].trim());
				} catch(Exception e) {
					//e.printStackTrace(); 
				}
				myModel.put("originalCustomText", originalCustomText);
			}
		}
		Set<String> imageUrlSet = new HashSet<String>();
		
		// change the price1 only and add only price per item. Need to add more code, if customer want to change all prices or price range.
		// Option price that is one time only is not added to Price1.
		newPrice = product.getPrice1();
		
		String[] optionCodeList = null;
		if(product != null && product.getOptionCode() != null) {
			optionCodeList = product.getOptionCode().split(",");
			for(String optionCode : optionCodeList) {
				Option baseOpt =  this.webJaguar.getOption(null, optionCode.trim());
				if(baseOpt != null) {
					for(ProductOption productOption : baseOpt.getProductOptions()) {
						productOption.setProductId(product.getId());
						optionsList.add(productOption);
						this.getDependingOptions(productOption, product.getId(), optionCode.trim(), valueMap, imageUrlSet, optionsList, newPrice);
					}
				}	
			} 
		}
			
		for(ProductOption option : optionsList) {
			// new dropdown's first options' imageUrl 
			if(valueMap.get(option.getOptionCode()) == null) {
				for(ProductOptionValue value : option.getValues()) {
					if(value.getIndex() == 0) {
						if(value.getImageUrl() != null && !value.getImageUrl().isEmpty()) {
							imageUrlSet.add(value.getImageUrl());
						}
					}
				}
			}
			// get colors map
			if(option.getType().equalsIgnoreCase("colorSwatch") && myModel.get("colors") == null) {
				myModel.put("colors", Constants.COLOR_CODE);
			}
		}
		// for non apparel, it is hard to get value from map. so pass it as another parameter
		
		if(ServletRequestUtils.getIntParameter(request, "selectedChild") != null) {
			myModel.put("selectedChild", ServletRequestUtils.getStringParameter(request, "selectedChild"));
			myModel.put("productQty", ServletRequestUtils.getIntParameter(request, "quantity_"+ServletRequestUtils.getIntParameter(request, "selectedChild")));
		}
			
		myModel.put("product", product);
		myModel.put("optionList", optionsList);
		myModel.put("imageUrlSet", imageUrlSet);
		myModel.put("productId", ServletRequestUtils.getStringParameter(request, "productId"));
		myModel.put("originalSelection", originalSelection);
		myModel.put("newPrice", newPrice);
		
		if(ServletRequestUtils.getStringParameter(request, "colorSwatch") != null) {
			String[] colorSwatches = ServletRequestUtils.getStringParameters(request, "colorSwatch");
			List<String> colorSwatchList = new ArrayList<String>(); 
			List<String> selectedImprintColorsList; 
			Map<String, List> locationSelectedColorMap = new HashMap<String, List>();
			for(String colorSwatch : colorSwatches) {
				if(!colorSwatch.trim().isEmpty()) {
					// save selected colors
					if(ServletRequestUtils.getStringParameter(request, "selectedImprintColors"+colorSwatch) != null) {
						String[] selectedImprintColors = ServletRequestUtils.getStringParameter(request, "selectedImprintColors"+colorSwatch).split(",");
						selectedImprintColorsList = new ArrayList<String>(); 
						for(String color : selectedImprintColors) {
							if(!color.trim().isEmpty()) {
								selectedImprintColorsList.add(color.trim());
							}
						}
						locationSelectedColorMap.put(colorSwatch, selectedImprintColorsList);
						myModel.put("selectedImprintColors"+colorSwatch, ServletRequestUtils.getStringParameter(request, "selectedImprintColors"+colorSwatch));
						myModel.put("locationSelectedColorMap", locationSelectedColorMap);
					}
					colorSwatchList.add(colorSwatch.trim());
				}
			}
		}
		return new ModelAndView("frontend/premier/smartOptions", "model", myModel);
	}
	
	//function to get child skus
	private void getChildSkus(Product product, HttpServletRequest request, Map<String, Object> myModel ) throws Exception{
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		
		// PremierPromotionalGroup Layout
		List <Product> slavesList = new ArrayList<Product>();
		ProductSearch search = new ProductSearch();
		search.setSku(product.getSku());
		search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		search.setSort("price_1");
		search.setCheckInventory((Boolean) gSiteConfig.get("gINVENTORY"));
		boolean basedOnHand = false;
		if (siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly") ) {
			basedOnHand = true;
		}
		search.setBasedOnHand(basedOnHand);
		m = null;
		try {
			m = c.getMethod("getField" + siteConfig.get("MASTER_SKU_PRODUCT_FIELD").getValue());
		} catch (Exception e) {
			m = c.getMethod("getField1");
		}
								
		for (Integer id: this.webJaguar.getSlaves(search)) {
			Product slave = this.webJaguar.getProductById(id, request);
			if (slave != null) {
				ProductField productField = new ProductField();
				productField.setValue((String) m.invoke(slave, arglist));
				slave.addProductField(productField);
				slavesList.add(slave);
			}
		}
		
		
		// For sorting product filed values (if value is size)- create map
		Map<Integer, Product> sizeAscMap = new TreeMap<Integer, Product>();
		int i=0;
		for (Product slaveProduct: slavesList) {
			for(ProductField slaveProductField: slaveProduct.getProductFields()) {
				try {
					sizeAscMap.put(Integer.parseInt(Constants.getApparelSize().get(slaveProductField.getValue()).toString()), slaveProduct);								
				} catch (Exception e) {
					sizeAscMap.put(i, slaveProduct);
				}
			}
			++i;
		}
		// If product is a slave
		if(product.getMasterSku() != null && !product.getMasterSku().isEmpty()) {
			myModel.put("masterProductFieldName", this.webJaguar.getProductFieldValue(product.getMasterSku(), "field_"+siteConfig.get("MASTER_SKU_PRODUCT_FIELD").getValue()));
			myModel.put("slaveFieldValue",(String) m.invoke(product, arglist));
		} else {
			myModel.put("masterProductFieldName", (String) m.invoke(product, arglist));
		}
		
		List<Product> sortedSlavesList = new ArrayList(sizeAscMap.values());
		myModel.put("childProductsList", sortedSlavesList);
		
		//get quantity
		Map<Integer, Integer> productQtyMap = new HashMap<Integer, Integer>();
		if(!sortedSlavesList.isEmpty()) {
			// if product has  child skus
			for(Product childProduct : sortedSlavesList) {
				if(ServletRequestUtils.getIntParameter(request, "quantity_"+childProduct.getId(), 0)  > 0) {
					productQtyMap.put(childProduct.getId(), ServletRequestUtils.getIntParameter(request, "quantity_"+childProduct.getId()) );
				}
			}
		} else {
			// if product is stand alone product 
			productQtyMap.put(product.getId(), ServletRequestUtils.getIntParameter(request, "quantity_"+product.getId(), (product.getMinimumQty() != null) ? product.getMinimumQty() : 1 ) );
		}
		myModel.put("productQtyMap", productQtyMap);
		
	}
	
	//recursive function to get all options based on selection
	public void getDependingOptions(ProductOption productOption, Integer productId, String optionCode, Map<String, Map<Integer, Integer>> valueMap, Set<String> imageUrlSet, List<ProductOption> optionsList, double newPrice){
		//if option value is open input field or option name is different than parameter passed to this function, than dont do anything
		if(productOption.getValues().isEmpty()) {
			return;
		}
		if(optionCode != null && valueMap.get(optionCode) != null) {
			//ProductOption productOption = this.webJaguar.getProductOptionByOptionCodeAndOptionName(optionCode, productOptionName);
					
			String dependencyId = null;
			for(ProductOptionValue value : productOption.getValues()) {
				if(value.getIndex() == valueMap.get(optionCode).get(productOption.getIndex())) {
					dependencyId = value.getDependingOptionIds();
					if(value.getOptionPrice() != null) {
						newPrice = newPrice + value.getOptionPrice();
					}
					 
					if(value.getImageUrl() != null && !value.getImageUrl().isEmpty()) {
						imageUrlSet.add(value.getImageUrl());
					}
					
					if(value.getIncludedProductList() != null) {
						this.getIncludedProductOptions(value.getIncludedProductList(), valueMap, imageUrlSet, optionsList, newPrice);
					}
				}
			}
					
			if(dependencyId != null) {
				Set<Integer> optionIdSet = new HashSet<Integer>();
				for(String id : dependencyId.split(",")){
					try {
						optionIdSet.add(Integer.parseInt(id));
					} catch (Exception e) { e.printStackTrace(); }
				}
				
				for(Integer id : optionIdSet) {
					Option newOption = this.webJaguar.getOption(id, null);
					if(newOption != null) {
						// This variable totalOptions is required to maintain the order of newly added options
						int totalOptions = 0;
						for(ProductOption option : newOption.getProductOptions()) {
							option.setProductId(productId);
							int index = -1;
							for(ProductOption tempOpt : optionsList) {
								if(tempOpt.getOptionCode().equalsIgnoreCase(productOption.getOptionCode()) && tempOpt.getName().equalsIgnoreCase(productOption.getName())) {
									index = optionsList.indexOf(tempOpt);
									break;
								}
							}
							if(index != -1) {
								optionsList.add(index+totalOptions+1, option);
							} else {
								optionsList.add(option);
							}
							totalOptions++;
							getDependingOptions(option, productId, newOption.getCode(), valueMap, imageUrlSet, optionsList, newPrice);
						}
					}
				}
			}
		}
	}
	
	
	private void getIncludedProductOptions(List<IncludedProduct> includedProductList, Map<String, Map<Integer, Integer>> valueMap, Set<String> imageUrlSet, List<ProductOption> optionsList, double newPrice){
		
		for(Product includedProduct : includedProductList){
			Product product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(includedProduct.getSku()), 0, false, null);
			if(product != null && product.getOptionCode() != null && !product.getOptionCode().trim().isEmpty()) {
				String[] optionCodes = product.getOptionCode().split(",");
				for(String optionCode : optionCodes){
					Option opt =  this.webJaguar.getOption(null, optionCode);
					if(opt != null) {
						for(ProductOption productOption : opt.getProductOptions()) {
							productOption.setProductId(product.getId());
							optionsList.add(productOption);
							this.getDependingOptions(productOption, product.getId(), optionCode, valueMap, imageUrlSet, optionsList, newPrice);
						}
					}
				}	
			}
		}
	}
	
}