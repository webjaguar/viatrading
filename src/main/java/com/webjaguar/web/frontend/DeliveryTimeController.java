/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.City;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.ShippingMethod;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.model.State;
import com.webjaguar.shipping.ShippingRateQuote;
import com.webjaguar.thirdparty.shipping.ups.UpsRateQuote;
import com.webjaguar.thirdparty.shipping.ups.UpsTransitQuote;

public class DeliveryTimeController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	String upsGroundTransitDay = null;
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		Integer turnOverTime = Integer.parseInt( siteConfig.get( "SHIPPING_TURNOVER_TIME" ).getValue());
		myModel.put("turnOverTime", turnOverTime);
		String zipcode = null;
		if (request.getParameter("_zipcode") != null) {
			zipcode = request.getParameter("_zipcode");
		}
		myModel.put("zipcode", zipcode);
		String stateProvince = null;
				
		if(!siteConfig.get("UPS_SHIPPER_NUMBER").getValue().isEmpty()) { 
			if(request.getParameter("_stateProvince") != null && request.getParameter("_stateProvince").equalsIgnoreCase("0")) {
				myModel.put("message","Please select State"); 
			} else {
				stateProvince = request.getParameter("_stateProvince"); 
			}
		} else {
			stateProvince = null;
		}
		myModel.put("stateProvince", stateProvince);
		
		Double weight = 1.0;
		/*if (request.getParameter("_weight") != null) {
			weight = Double.parseDouble( request.getParameter("_weight") );
		}
		myModel.put("weight", weight);*/
		Date shippingTime = setupDueDate(turnOverTime, Calendar.getInstance(), true);
		Calendar shipTimeCalendar = new GregorianCalendar();
		shipTimeCalendar.setTime( shippingTime );
		
		List<ShippingRate> ratesList = new ArrayList<ShippingRate>();
		try {
			UpsRateQuote upsRateQuote = new UpsRateQuote();
			ratesList.addAll( upsRateQuote.getRates( siteConfig.get( "SOURCE_ZIP" ).getValue(),siteConfig.get("SOURCE_STATEPROVINCE").getValue(), siteConfig.get( "SOURCE_COUNTRY" ).getValue(), siteConfig.get( "UPS_USERID" ).getValue(), siteConfig.get( "UPS_PASSWORD" ).getValue(), siteConfig.get( "UPS_ACCESS_LICENSE_NUMBER" ).getValue(), zipcode, stateProvince, "US", weight, false, true, null, null, siteConfig.get("UPS_PICKUPTYPE").getValue(), siteConfig.get("UPS_SHIPPER_NUMBER").getValue()) );
		}
		catch ( Exception e ) {
			// do nothing
		}
		try
		{
			UpsTransitQuote upsTransitQuote = new UpsTransitQuote();
			upsGroundTransitDay = upsTransitQuote.getTransit( siteConfig.get( "SOURCE_ZIP" ).getValue(), siteConfig.get( "SOURCE_COUNTRY" ).getValue(), siteConfig.get( "UPS_USERID" ).getValue(), siteConfig.get( "UPS_PASSWORD" ).getValue(), siteConfig.get( "UPS_ACCESS_LICENSE_NUMBER" ).getValue(), zipcode, "US", weight, false);
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}
		List<ShippingRate> finalList = new ArrayList<ShippingRate>();
		// set shipping title for each shipping rate object, remove DISABLED shipping rate also
		for ( Iterator it = ratesList.iterator(); it.hasNext(); )
		{
			ShippingRate shippingRate = (ShippingRate) it.next();
			ShippingMethod shippingMethod = webJaguar.getShippingMethodByCarrierCode( shippingRate );
			if ( shippingMethod != null && shippingMethod.getShippingActive() )
			{
				shippingRate.setTitle( shippingMethod.getShippingTitle() );
				shippingRate.setRank( shippingMethod.getShippingRank() );
				if (!shippingRate.getCode().equals( "03" )) { // UPS Ground code is 03
					shippingRate.setDeliveryDate( setupDueDate(Integer.parseInt( shippingRate.getGuaranteedDaysToDelivery() ), shipTimeCalendar, false));
				} else if ( shippingRate.getCarrier().equals( "ups" ) && shippingRate.getCode().equals( "03" ) && upsGroundTransitDay != null) {
					shippingRate.setDeliveryDate( setupDueDate(Integer.parseInt( upsGroundTransitDay ), shipTimeCalendar, false));
				}
				finalList.add( shippingRate );
			}
		}
		myModel.put("ratesList", finalList);
		myModel.put("shippingTime", shippingTime);
		myModel.put("statelist", this.webJaguar.getStateList("US")); 
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		}
		myModel.put("deliveryTimeLayout", this.webJaguar.getSystemLayout("deliveryTime", request.getHeader("host"), request));
		
		return new ModelAndView("frontend/deliveryTime", "model", myModel);    	
    }

	private Date setupDueDate(int turnOverTime, Calendar startTime, boolean AMPM) {
		Calendar now = new GregorianCalendar();
		now.setTime( startTime.getTime() );
		if ( AMPM && now.get( Calendar.AM_PM ) == Calendar.PM ) {
			now.add( Calendar.DAY_OF_MONTH, 1 );
		}
		if ( now.get( GregorianCalendar.DAY_OF_WEEK ) == 1 ) {
			now.add( Calendar.DAY_OF_MONTH, 1 );
		}
		else if ( now.get( GregorianCalendar.DAY_OF_WEEK ) == 7 ) {
			now.add( Calendar.DAY_OF_MONTH, 2 );
		}
		for (int i=0; i < turnOverTime ; i++) {
			now.add( Calendar.DAY_OF_MONTH, 1 );
			if ( now.get( GregorianCalendar.DAY_OF_WEEK ) == 1 ) {
				now.add( Calendar.DAY_OF_MONTH, 1 );
			}
			else if ( now.get( GregorianCalendar.DAY_OF_WEEK ) == 7 ) {
				now.add( Calendar.DAY_OF_MONTH, 2 );
			}
		}
		return now.getTime();
	}

}
