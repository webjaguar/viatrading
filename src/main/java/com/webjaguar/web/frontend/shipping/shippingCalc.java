package com.webjaguar.web.frontend.shipping;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.support.WebApplicationObjectSupport;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Customer;
import com.webjaguar.model.UserSession;


@Controller
public class shippingCalc extends WebApplicationObjectSupport{
	@Autowired
	private WebJaguarFacade webJaguar;
	
	
	@RequestMapping(value="/calcShipping.jhtm", method = RequestMethod.POST)
	private @ResponseBody Map shippingCalculate(HttpServletRequest request, HttpServletResponse response) throws Exception{
		Map<String, Object> data = new HashMap<String, Object>();
		
		String shipping;
		Double subTotal = 00.0;
		Double grandTotal = 00.0;
		Double discount = 00.0;
		Double tax = 00.0;

		 Double shippingCost = 0.0;
		UserSession userSession = (UserSession) request.getAttribute("userSession");

		Customer customer =  null;Cart cart = null;
		if(userSession!=null){

			customer =  this.webJaguar.getCustomerById(userSession.getUserid());
			String manufacturerName = (String) request.getSession().getAttribute("manufacturerName");
			cart = this.webJaguar.getUserCart(userSession.getUserid(), manufacturerName);
			cart.setUserId(userSession.getUserid());
			cart.setPromoCode((String) request.getSession().getAttribute("promocode"));
			this.webJaguar.updateCart(cart, request);
		}
		
		data.put("shipping", null);
		data.put("totalCharges", null);
		data.put("message", "");
		

		StringBuilder userInfo = null;
		if (customer != null && customer.getAddress() != null) {
			
			userInfo = new StringBuilder();

			StringBuilder load = new StringBuilder();
			StringBuilder loadQty = new StringBuilder();
			StringBuilder loadTotalWeight = new StringBuilder();
			StringBuilder loadTotalPallets = new StringBuilder();
			StringBuilder loadTotalCases = new StringBuilder();

			userInfo.append("Email=");
			userInfo.append(customer.getUsername());
			userInfo.append("&Address1=");
			userInfo.append(customer.getAddress().getAddr1());
			userInfo.append("&Address2=");
			userInfo.append(customer.getAddress().getAddr2());
			userInfo.append("&City=");
			userInfo.append(customer.getAddress().getCity());
			userInfo.append("&State=");
			userInfo.append(customer.getAddress().getStateProvince());
			userInfo.append("&Zip=");
			userInfo.append(customer.getAddress().getZip());
			userInfo.append("&Country=");
			userInfo.append(customer.getAddress().getCountry());
			userInfo.append("&Residential=");
			userInfo.append(customer.getAddress().isResidential() ? "1" : "0");
			userInfo.append("&Liftgate=");
			userInfo.append(customer.getAddress().isLiftGate() ? "1" : "0");
			userInfo.append("&DeclaredValue=1");

			load.append("&Load=[\"");
			loadQty.append("&LoadQty=[\"");
			loadTotalWeight.append("&LoadTotalWeight=[\"");
			loadTotalPallets.append("&LoadTotalPallets=[\"");
			loadTotalCases.append("&LoadTotalCases=[\"");
			int index = 0;

			for (CartItem cartItem : cart.getCartItems()) {

				cartItem.setProduct(this.webJaguar.getProductById(cartItem.getProduct().getId(), request));
				
				subTotal += (cartItem.getQuantity()*cartItem.getUnitPrice());
				discount += (cartItem.getDiscount());

				if (index > 0) {
					load.append("\",\"");
					loadQty.append("\",\"");
					loadTotalWeight.append("\",\"");
					loadTotalPallets.append("\",\"");
					loadTotalCases.append("\",\"");
				}
				index++;
				load.append(cartItem.getProduct().getSku());
				loadQty.append(cartItem.getQuantity());
				loadTotalWeight.append(cartItem.getProduct().getWeight()*cartItem.getQuantity());

				Integer pallets = 0;
				try {
					pallets = Integer.parseInt(cartItem.getProduct().getField15()) * cartItem.getQuantity();
				} catch (Exception ex) {
				}

				loadTotalPallets.append(pallets);

				Double upsMaxItemsInPackage = 0.0;
				try {
					upsMaxItemsInPackage = cartItem.getProduct().getUpsMaxItemsInPackage() * cartItem.getQuantity();
				} catch (Exception ex) {
				}
				loadTotalCases.append(upsMaxItemsInPackage);
			}

			load.append("\"]");
			loadQty.append("\"]");
			loadTotalWeight.append("\"]");
			loadTotalPallets.append("\"]");
			loadTotalCases.append("\"]");

			userInfo.append(load.toString()).append(loadQty.toString()).append(loadTotalWeight.toString()).append(loadTotalPallets.toString()).append(loadTotalCases.toString());
			String urlRes = null;
			
			try {

				String urlParameters = userInfo.toString();
				urlParameters=urlParameters.replaceAll("\"", "%22");
				urlParameters=urlParameters.replaceAll(" ", "%20");

				URL url = new URL(request.getScheme()+"://www.viatrading.com/dv/liveapps/shippingquotes.php?" + urlParameters);

				HttpURLConnection con = (HttpURLConnection) url.openConnection();
			    con.setRequestMethod("GET");
			    con.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			    con.addRequestProperty("User-Agent", "Mozilla");
			    con.addRequestProperty("Referer", "google.com");
			    con.setRequestProperty("Accept", "text/html, text/*");
			    con.addRequestProperty("Content-Type","application/json; charset=UTF-8"); 
			    
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;

				while ((inputLine = in.readLine()) != null) {

					if (inputLine != null && !inputLine.isEmpty()) {

						urlRes = inputLine;		
					}
				}
				
				Double totalCharges = 0.0;

				JSONObject obj = new JSONObject(urlRes);
				shipping = obj.getString("Shipping");
				try{

					totalCharges = Double.parseDouble(obj.getString("TotalCharges"));
				}catch(Exception ex){
					totalCharges = 0.0;
				}
				String message = obj.getString("Message");
				data.put("shipping", this.webJaguar.shippingTitle(shipping));
				System.out.println(this.webJaguar.shippingTitle(shipping));
				System.out.println(totalCharges);

				shippingCost = totalCharges;
				data.put("totalCharges", totalCharges);
					if(message!=null && !message.equalsIgnoreCase("null")){
						data.put("message", message);
					}
				} 
				catch (Exception ex) {
					System.out.println("Exception");
					ex.printStackTrace();

				}
		}
		
		grandTotal = subTotal - discount + shippingCost + tax;

		data.put("shippingCost", shippingCost);
		//data.put("shippingType", shippingType);
		//data.put("shippingMessage", shippingMessage);
		data.put("grandTotal", grandTotal);
		
		return data;

	}

}
