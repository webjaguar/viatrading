/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.register;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.EventMember;
import com.webjaguar.model.EventSearch;
import com.webjaguar.model.Layout;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.form.CustomerForm;
import com.webjaguar.web.form.EventForm;

public class EventRegisterWizardController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	private String type;
	public void setType(String type) { this.type = type; }
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	public EventRegisterWizardController() {
		setCommandName("eventForm");
		setCommandClass(EventForm.class);
	}
	
    public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {

    	EventForm eventForm = (EventForm) command;
    	
    	if (eventForm.getCustomer() != null) {
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#cardid#", eventForm.getCustomer().getCardId()));
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#firstname#", eventForm.getCustomer().getAddress().getFirstName()));
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#lastname#", eventForm.getCustomer().getAddress().getLastName()));
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#accountNumber#", (eventForm.getCustomer().getAccountNumber()==null) ? "" : eventForm.getCustomer().getAccountNumber()));
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#eventname#", eventForm.getEvent().getEventName()));
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#eventdate#", eventForm.getEvent().getStartDate().toString()));
			try {
				eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#salesRepName#", this.webJaguar.getSalesRepById(eventForm.getCustomer().getSalesRepId()).getName()));
			} catch (Exception e) {
				eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#salesRepName#", ""));
			}
			// customer fields
			Class<Customer> c1 = Customer.class;
			Method m1 = null;
			Object arglist1[] = null;
			for ( CustomerField customerField : this.webJaguar.getCustomerFields() ) {
				if ( customerField.isEnabled() ) {
					m1 = c1.getMethod( "getField" + customerField.getId() );
					customerField.setValue( (String) m1.invoke( eventForm.getCustomer(), arglist1 ) );
					eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#customerfield"+ customerField.getId() +"#", ( customerField.getValue() == null ) ? "" :  customerField.getValue() ));
				}
			}
		}
    	
    	try {
    		EventMember member = new EventMember();
    		member.setUserId(eventForm.getCustomer().getId());
    		member.setEventId(eventForm.getEvent().getId());
    		member.setUsername(eventForm.getCustomer().getUsername());
    		member.setFirstName(eventForm.getCustomer().getAddress().getFirstName());
    		member.setLastName(eventForm.getCustomer().getAddress().getLastName());
    		member.setCardId(eventForm.getCustomer().getCardId());
    		member.setSalesRepId(eventForm.getCustomer().getSalesRepId());
    		member.setHtmlCode(eventForm.getEvent().getHtmlCode());
			this.webJaguar.insertEventMember(member);
			
			// insert queue
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#queue#", member.getUserIndex().toString()));
			member.setHtmlCode(eventForm.getEvent().getHtmlCode());
			this.webJaguar.updateEventMember(member);
			
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			errors.rejectValue("cardId", "form.event.memberExist", "You are already registered to this event.");				
			return showPage(request, errors, 1);
		}
		
		Map<String, Object> model = new HashMap<String, Object>();

		model.put( "eventForm", eventForm );	
		setLayout(request);
		
		String dir = "";
		if (siteConfig.get("TSR_DIRECTORY").getValue().length() > 0) {
			dir = siteConfig.get("TSR_DIRECTORY").getValue() + "/";
		} 
		
		if (eventForm.getLanguage().equalsIgnoreCase("es")) {
			return new ModelAndView(dir+"frontend/register/event/es/step2", model );
		} else
			return new ModelAndView(dir+"frontend/register/event/step2", model );
    }
 
    @SuppressWarnings("deprecation")
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
    	throws Exception {
    	EventForm eventForm = (EventForm) command;
    	
       	Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		String dir = "";
		if (siteConfig.get("TSR_DIRECTORY").getValue().length() > 0) {
			dir = siteConfig.get("TSR_DIRECTORY").getValue() + "/";
		} 
		
			switch (page) {
			case 0:
				EventSearch search = new EventSearch();
				search.setActive("1");
				//search.setInEffect("1");
				myModel.put("events", this.webJaguar.getEventList(search));
				if(type==null) {
				if (eventForm.getLanguage().equals( "es" )) {
					setPages( new String[] {dir+"frontend/register/event/es/step0", dir+"frontend/register/event/es/step1", dir+"frontend/register/event/es/step2"});
				} else {
					setPages( new String[] {dir+"frontend/register/event/step0", dir+"frontend/register/event/step1", dir+"frontend/register/event/step2"});
				}
				} else if(type.equals("login")){
					if (eventForm.getLanguage().equals( "es" )) {
						setPages( new String[] {dir+"frontend/register/event/login/es/step0", dir+"frontend/register/event/login/es/step1", dir+"frontend/register/event/login/es/step2"});
					} else {
						setPages( new String[] {dir+"frontend/register/event/login/step0", dir+"frontend/register/event/login/step1", dir+"frontend/register/event/login/step2"});
						
						
					}
				}
				break;
			case 1:
				if(type==null){
					eventForm.setEvent(this.webJaguar.getEventById(eventForm.getEvent().getId()));
					break;
				}else if(type.equals("login")){
					AccessUser accessUser = this.webJaguar.getAccessUser(request);
					if(accessUser==null){
					}
					UserSession userSession = this.webJaguar.getUserSession(request);
			    	Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());
			    	
					myModel.put("customer", customer);					
					eventForm.setEvent(this.webJaguar.getEventById(eventForm.getEvent().getId()));
					break;
				}
				
			case 2:
				break;
		}	
		EventSearch search = new EventSearch();
		search.setActive("1");
		search.setSort("start_date");
		myModel.put("events", this.webJaguar.getEventList(search));
	
		if(type == null || !type.equals("login")){
			setLayout(request);
		}else{
			setLayoutForLoginPage(request);
		}
		
		
		Customer customer = this.webJaguar.getCustomerByCardID(eventForm.getCardId());
		if(eventForm.getEventId()!=null && !eventForm.getEventId().equalsIgnoreCase(""))
		eventForm.setEvent(this.webJaguar.getEventById(Integer.valueOf(eventForm.getEventId())));
		eventForm.setCustomer(customer);
    	if (eventForm.getCustomer() != null && eventForm.getEvent()!=null && eventForm.getCustomer().getAddress()!=null && eventForm.getEvent().getHtmlCode()!=null) {
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#cardid#", eventForm.getCustomer().getCardId()));
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#firstname#", eventForm.getCustomer().getAddress().getFirstName()));
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#lastname#", eventForm.getCustomer().getAddress().getLastName()));
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#accountNumber#", (eventForm.getCustomer().getAccountNumber()==null) ? "" : eventForm.getCustomer().getAccountNumber()));
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#eventname#", eventForm.getEvent().getEventName()));
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#eventdate#", eventForm.getEvent().getStartDate().toString()));
			try {
				eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#salesRepName#", this.webJaguar.getSalesRepById(eventForm.getCustomer().getSalesRepId()).getName()));
			} catch (Exception e) {
				eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#salesRepName#", ""));
			}
			// customer fields
			Class<Customer> c1 = Customer.class;
			Method m1 = null;
			Object arglist1[] = null;
			for ( CustomerField customerField : this.webJaguar.getCustomerFields() ) {
				if ( customerField.isEnabled() ) {
					m1 = c1.getMethod( "getField" + customerField.getId() );
					customerField.setValue( (String) m1.invoke( eventForm.getCustomer(), arglist1 ) );
					eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#customerfield"+ customerField.getId() +"#", ( customerField.getValue() == null ) ? "" :  customerField.getValue() ));
				}
			}
		
    	
    	try {
    		EventMember member = new EventMember();
    		member.setUserId(eventForm.getCustomer().getId());
    		member.setEventId(eventForm.getEvent().getId());
    		member.setUsername(eventForm.getCustomer().getUsername());
    		member.setFirstName(eventForm.getCustomer().getAddress().getFirstName());
    		member.setLastName(eventForm.getCustomer().getAddress().getLastName());
    		member.setCardId(eventForm.getCustomer().getCardId());
    		member.setSalesRepId(eventForm.getCustomer().getSalesRepId());
    		member.setHtmlCode(eventForm.getEvent().getHtmlCode());
			this.webJaguar.insertEventMember(member);
			map.put("eventStatus", "Registration Success!!!"); 
			// insert queue
			eventForm.getEvent().setHtmlCode(eventForm.getEvent().getHtmlCode().replace("#queue#", member.getUserIndex().toString()));
			member.setHtmlCode(eventForm.getEvent().getHtmlCode());
			this.webJaguar.updateEventMember(member);
			
			} catch (DataIntegrityViolationException ex) {
				map.put("eventStatus", "You are already registered to this event.!!!"); 
				ex.printStackTrace();
				errors.rejectValue("cardId", "form.event.memberExist", "You are already registered to this event.");				
			}
    	}
    	else if(eventForm.getCardId()!=null && eventForm.getCardId().length()>0){
    		//map.put("eventStatus", "Card id is not correct!!!"); 
    	}
    	
		String userId = ServletRequestUtils.getStringParameter(request,"userid","-1");		
		map.put("userid", userId);
    	map.put("model", myModel); 
		return map;
    }
    
    protected void validatePage(Object command, Errors errors, int page) {
    	EventForm eventForm = (EventForm) command;
		
		switch (page) {
			case 1:
				Customer customer = this.webJaguar.getCustomerByCardID(eventForm.getCardId());
				eventForm.setCustomer(customer);
				if (customer == null) {
					errors.rejectValue( "cardId", "form.event.invalidCardId", "Invalid Card ID" );
					break;
				}
				if (customer.isSuspendedEvent()) {
					errors.reject( "cardId" );
					break;
				}
			}
    }
    
	@SuppressWarnings("deprecation")
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );	
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		
		String dir = "";
		if (siteConfig.get("TSR_DIRECTORY").getValue().length() > 0) {
			dir = siteConfig.get("TSR_DIRECTORY").getValue() + "/";
		} 
		
		if(type==null){
			if(ServletRequestUtils.getStringParameter(request,"language","es").equals("es")){
				if(ServletRequestUtils.getStringParameter(request,"regEvent","true").equals("true")){
					setPages( new String[] {dir+"frontend/register/event/es/step0", dir+"frontend/register/event/es/step1", dir+"frontend/register/event/es/step2"});
				}else{
					setPages( new String[] {dir+"frontend/register/event/es/step0", dir+"frontend/register/event/es/step1", dir+"frontend/register/event/es/step2"});
				}
			}else{
				if(ServletRequestUtils.getStringParameter(request,"regEvent","true").equals("true")){
					setPages( new String[] {dir+"frontend/register/event/step0", dir+"frontend/register/event/step1", dir+"frontend/register/event/step2"});
				}else{
					setPages( new String[] {dir+"frontend/register/event/step0", dir+"frontend/register/event/step1", dir+"frontend/register/event/step2"});
				}
			}
		}else if(type.equals("login")){
			setPages( new String[] {dir+"frontend/register/event/login/step0", dir+"frontend/register/event/login/step1", dir+"frontend/register/event/login/step2"});
		}
		
		// check if customer is logged in
		if(type==null){
			if (this.webJaguar.getUserSession(request) != null) {
				
				throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("account.jhtm")));	
			}
		}	
		EventForm eventForm = new EventForm();
		
		Customer customer = null;
		String userId = ServletRequestUtils.getStringParameter(request,"userid","-1");
		
		
		if(userId!=null && userId.length()>0){
			Integer id = null;
			try{
				id = Integer.parseInt(userId);
			}catch(Exception ex){
			}
			customer = this.webJaguar.getCustomerById(id);
			
		}
		
		if(ServletRequestUtils.getStringParameter(request,"language","es").equals("es")){
			eventForm.setLanguage("es");
		}else{
			eventForm.setLanguage("en");
		}
		
		eventForm.setCardId((ServletRequestUtils.getStringParameter(request,"cartNo","")));
		if(customer!=null){
			if(eventForm.getCardId()==null || eventForm.getCardId().length()<=0){
				
				eventForm.setCardId(customer.getCardId());
			}
		}
		eventForm.setEventId((ServletRequestUtils.getStringParameter(request,"event.id","")));


		return eventForm;
	}
	
	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		EventForm eventForm = (EventForm) command;

		switch (page) {
			case 0:
				break;
		}		
	}
	
	private void sendEmail(SiteMessage siteMessage, HttpServletRequest request, Customer customer, SalesRep salesRep) {
		
		// send email
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));

    	StringBuffer messageBuffer = new StringBuffer();
    	messageBuffer.append( dateFormatter.format(new Date()) + "\n\n" );
    	
    	String firstName = customer.getAddress().getFirstName();  
    	if (firstName == null) {
    		firstName = "";
    	}
    	String lastname = customer.getAddress().getLastName();
    	if (lastname == null) {
    		lastname = "";
    	}
    
    	// subject
    	siteMessage.setSubject( siteMessage.getSubject().replace( "#email#", customer.getUsername() ) );
    	siteMessage.setSubject( siteMessage.getSubject().replace( "#password#", customer.getPassword() ) );
    	siteMessage.setSubject( siteMessage.getSubject().replace( "#firstname#", firstName ) );
    	siteMessage.setSubject( siteMessage.getSubject().replace( "#lastname#", lastname ) );
    	if ( salesRep != null ) {
    		siteMessage.setSubject( siteMessage.getSubject().replace( "#salesRepName#", salesRep.getName() ) );
    		siteMessage.setSubject( siteMessage.getSubject().replace( "#salesRepEmail#", salesRep.getEmail() ) );
    		siteMessage.setSubject( siteMessage.getSubject().replace( "#salesRepPhone#", ( salesRep.getPhone() == null ) ? "" : salesRep.getPhone() ) );
    		siteMessage.setSubject( siteMessage.getSubject().replace( "#salesRepCellPhone#", ( salesRep.getCellPhone() == null ) ? "" : salesRep.getCellPhone() ) );
    	}
    	// message
    	siteMessage.setMessage( siteMessage.getMessage().replace( "#email#", customer.getUsername() ) );
    	siteMessage.setMessage( siteMessage.getMessage().replace( "#password#", customer.getPassword() ) );
    	siteMessage.setMessage( siteMessage.getMessage().replace( "#firstname#", firstName ) );
    	siteMessage.setMessage( siteMessage.getMessage().replace( "#lastname#", lastname ) );
    	if ( salesRep != null ) {
    		siteMessage.setMessage( siteMessage.getMessage().replace( "#salesRepName#", salesRep.getName() ) );
    		siteMessage.setMessage( siteMessage.getMessage().replace( "#salesRepEmail#", salesRep.getEmail() ) );
    		siteMessage.setMessage( siteMessage.getMessage().replace( "#salesRepPhone#", ( salesRep.getPhone() == null ) ? "" : salesRep.getPhone() ) );
    		siteMessage.setMessage( siteMessage.getMessage().replace( "#salesRepCellPhone#", ( salesRep.getCellPhone() == null ) ? "" : salesRep.getCellPhone() ) );
    	}
    	messageBuffer.append( siteMessage.getMessage() );
    	
		try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.addBcc(contactEmail);
			helper.setSubject(siteMessage.getSubject());
		    helper.setText(messageBuffer.toString(), siteMessage.isHtml());
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing
		}  
		if (salesRep != null && ((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
			messageBuffer.append( "<br /><br />New Customer Info:<br />Name:<b>" + customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName() + "</b>" );
			messageBuffer.append( "<br />Company Name:<b>" + customer.getAddress().getCompany() + "</b>" );
			messageBuffer.append( "<br />Address1:<b>" + customer.getAddress().getAddr1() + "</b>" );
			messageBuffer.append( "<br />Address2:<b>" + (( customer.getAddress().getAddr2() == null ) ? "" : customer.getAddress().getAddr2()) + "</b>" );
			messageBuffer.append( "<br />City:<b>" + customer.getAddress().getCity() + "</b> State:<b> " + customer.getAddress().getStateProvince() + "</b> Zipcode:<b> " + customer.getAddress().getZip() + "</b>" );
			messageBuffer.append( "<br />Phone:<b>" + customer.getAddress().getPhone() + "</b>" );
			messageBuffer.append( "<br />Email:<b>" + customer.getUsername() + "</b>" );
			try {
		    	// construct email message
		       	MimeMessage mms = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
				helper.setTo(salesRep.getEmail());
				helper.setFrom(contactEmail);
				helper.setCc( contactEmail );

				helper.setSubject(siteMessage.getSubject());
			    helper.setText(messageBuffer.toString(), siteMessage.isHtml());
				mailSender.send(mms);
			} catch (Exception ex) {
				// do nothing
			}  
		}
	}
	
	private void setLayout(HttpServletRequest request) {
		Layout layout = null;
		//layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Register For Events"));
		layout = this.webJaguar.getSystemLayout("eventRegister", request.getHeader("host"), request);
		if(layout != null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#pageName#", "Register For Events"));
		}
		if(layout == null){
			layout = (Layout) request.getAttribute("layout");
		}
		request.setAttribute("layout", layout);
		layout.setHeaderHtml( "" );
		layout.setTopBarHtml( "" );
		layout.setRightBarBottomHtml("");
		layout.setRightBarBottomHtmlLogin("");
		layout.setRightBarTopHtml("");
		layout.setRightBarTopHtmlLogin("");
		layout.setLeftBarTopHtml( "" );
		layout.setLeftBarBottomHtml( "" );
		layout.setHideLeftBar( true );
		//layout.setFooterHtml( "" );
	}
	
	private void setLayoutForLoginPage(HttpServletRequest request) {
		Layout layout = null;
		//layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Register For Events"));
		layout = this.webJaguar.getSystemLayout("eventRegister", request.getHeader("host"), request);
		if(layout != null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#pageName#", "Register For Events"));
		}
		if(layout == null){
			layout = (Layout) request.getAttribute("layout");
		}
		request.setAttribute("layout", layout);
		layout.setRightBarBottomHtml("");
		layout.setRightBarBottomHtmlLogin("");
		layout.setRightBarTopHtml("");
		layout.setRightBarTopHtmlLogin("");
		layout.setLeftBarTopHtml( "" );
		layout.setLeftBarBottomHtml( "" );
		layout.setHideLeftBar( true );
		//layout.setFooterHtml( "" );
	}
}
