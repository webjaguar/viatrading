package com.webjaguar.web.frontend.register;

import java.io.File;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.SalesRep;
import com.webjaguar.thirdparty.itext.ITextApi;

public class TouchScreenPrintLabelController extends WebApplicationObjectSupport implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
				
		// check if __printPdf button was clicked
		if (request.getParameter("__printPdf") != null && request.getParameter("__cardId") != null) {
			Customer customer = this.webJaguar.getCustomerByCardID(request.getParameter("__cardId"));
			Integer quantity =Integer.parseInt(ServletRequestUtils.getStringParameter( request, "__qty", "1" ).trim());
			SalesRep salesRep = null;
			if(customer != null) {
				salesRep = this.webJaguar.getSalesRepById(customer.getSalesRepId());
			}
			if(printPdf(request, response, customer, quantity,salesRep)) {
				return null;
			}
		}
		setLayout(request);
		return new ModelAndView("frontend/register/touchScreenPrintLabel");
	}
	
	
	private boolean printPdf(HttpServletRequest request, HttpServletResponse response, Customer customer, Integer quantity, SalesRep salesRep) {
		File tempFolder = new File(getServletContext().getRealPath("temp"));
		
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File pdfFile = new File(tempFolder, "customerLabels.pdf");
		boolean result = false;

		if(customer != null) {	
			try {
				ITextApi iText = new ITextApi();
				
				iText.createCustomerLabels(pdfFile, request, quantity, customer, salesRep);
				result = true;
	
				byte[] b = this.webJaguar.getBytesFromFile(pdfFile);
			
			response.addHeader("Content-Disposition", "attachment; filename="+pdfFile.getName());
	        response.setBufferSize((int) pdfFile.length());
			response.setContentType("application/pdf");
			response.setContentLength((int) pdfFile.length());
			
			// output stream
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(b, 0,(int) pdfFile.length());
			ouputStream.flush();
			ouputStream.close();
	        
			// delete file before exit
			pdfFile.delete();
			
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	private void setLayout(HttpServletRequest request) {
		Layout layout = (Layout) request.getAttribute( "layout" );
		layout.setHeaderHtml( "" );
		layout.setTopBarHtml( "" );
		layout.setRightBarBottomHtml("");
		layout.setRightBarBottomHtmlLogin("");
		layout.setRightBarTopHtml("");
		layout.setRightBarTopHtmlLogin("");
		layout.setLeftBarTopHtml( "" );
		layout.setLeftBarBottomHtml( "" );
		layout.setHideLeftBar( true );
		layout.setFooterHtml( "" );
	}

}
