/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.register;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.web.form.CustomerForm;
import com.webjaguar.web.validator.CustomerFormValidator;

public class StepSignCPWizardController extends WebApplicationObjectSupport implements Controller {
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	
    	Map<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView( "frontend/register/touchScreen/stepSignCP", model );
    }
	
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
        	throws Exception {    	
        	
        	CustomerForm customerForm = (CustomerForm) command;
        	
        	Boolean spanish = false;
        	
        	if(customerForm.getLanguage()!=null && customerForm.getLanguage().equalsIgnoreCase("es")){
        		spanish = true;
            	customerForm.getCustomer().setLanguageCode(LanguageCode.es);
            	customerForm.getCustomer().setLanguage(customerForm.getLanguage());
        	}else{
            	customerForm.getCustomer().setLanguageCode(LanguageCode.en);
            	customerForm.getCustomer().setLanguage(customerForm.getLanguage());
        	}
        	
           	Map<String, Object> map = new HashMap<String, Object>();
    		Map<String, Object> myModel = new HashMap<String, Object>();
    		switch (page) {
    			case 2:
    				if(customerForm.getCustomer()!=null && customerForm.getCustomer().getId()!=null){
    					sendCustomerIdToPhp(customerForm.getCustomer().getId(), spanish);
    				}
    				break;
    			case 3:
    				if ( customerForm.getShipping() == null || customerForm.getCustomer().getAddress().compareTo( customerForm.getShipping() ) == 1) {
    					Address shippingAddress = new Address();
    					shippingAddress.setFirstName( customerForm.getCustomer().getAddress().getFirstName() );
    					shippingAddress.setLastName( customerForm.getCustomer().getAddress().getLastName() );
    					shippingAddress.setAddr1( customerForm.getCustomer().getAddress().getAddr1() );
    					shippingAddress.setCountry( customerForm.getCustomer().getAddress().getCountry() );
    					shippingAddress.setCity( customerForm.getCustomer().getAddress().getCity() );
    					shippingAddress.setStateProvince( customerForm.getCustomer().getAddress().getStateProvince() );
    					shippingAddress.setZip( customerForm.getCustomer().getAddress().getZip() );
    					shippingAddress.setPhone( customerForm.getCustomer().getAddress().getPhone() );
    					shippingAddress.setLiftGate( customerForm.getCustomer().getAddress().isLiftGate() );
    					shippingAddress.setResidential( customerForm.getCustomer().getAddress().isResidential() );
    					customerForm.setShipping( shippingAddress );
    				}
    				break;	
    			case 4:
    				break;
    			case 5:
    				customerForm.setConfirmPassword(customerForm.getCustomer().getPassword());
    				break;
    			case 7:
    				if (customerForm.getCustomer().getUsername().equalsIgnoreCase("")) {
    					customerForm.getCustomer().setUsername(customerForm.getCustomer().getCardId()+"@viatrading.com");
    					customerForm.getCustomer().setEmailNotify(false);
    					customerForm.getCustomer().setUnsubscribe(true);
    				}
    				break;	
    		}	
    		
    		myModel.put("cust", customerForm.getCustomer()); 
    		map.put("model", myModel); 
    		
    		return map;
        }
        
        public void sendCustomerIdToPhp(Integer customerId, Boolean spanish) { 
        	try{
        		
        		URL url = null;
        		
        		if(spanish){
        			url = new URL("/dv/tsr/agreement-es.php?id=" + customerId);
        		}else{
        			url = new URL("/dv/tsr/agreement.php?id=" + customerId);
        		}
        		
        		
        		
        		HttpURLConnection con = (HttpURLConnection) url.openConnection();
        		con.setRequestMethod("GET");
      			con.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
      			con.addRequestProperty("User-Agent", "Mozilla");
      			con.addRequestProperty("Referer", "google.com");
      			con.setRequestProperty("Accept", "text/html, text/*");
      			con.addRequestProperty("Content-Type","application/json; charset=UTF-8"); 
      			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
      			String inputLine;
      			StringBuilder sb = new StringBuilder(); 
      			while((inputLine = in.readLine()) != null)
      			{
      				if(inputLine!=null && !inputLine.isEmpty()){
      					sb.append(inputLine);
      					
      				}
      			}
        	}catch(Exception ex){	
        	}   
        }
        
        protected void validatePage(Object command, Errors errors, int page) {
    		CustomerForm customerForm = (CustomerForm) command;
        }
        
    	protected Object formBackingObject(HttpServletRequest request) throws Exception {
    		CustomerForm customerForm = new CustomerForm();
    		customerForm.setTouchScreen(true);
    		customerForm.setLanguage( ServletRequestUtils.getStringParameter( request, "language", "en" ));
    		return customerForm;
    	}
    	
    	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
    		CustomerForm customerForm = (CustomerForm) command;
    	}


}
