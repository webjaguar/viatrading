/* Copyright 2010 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 04.07.2010
 */

package com.webjaguar.web.frontend.register.viatrading;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.DialingNote;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SalesRepSearch;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.CustomerForm;

public class RegisterFormController extends SimpleFormController {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}
	
	private JavaMailSenderImpl mailSender;

	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}

	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;

	public RegisterFormController() {
		setSessionForm(true);
		setCommandName("customerForm");
		setCommandClass(CustomerForm.class);
		setFormView("frontend/register/viatrading/form");
	}

	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		SalesRep salesRep = null;

		CustomerForm customerForm = (CustomerForm) command;
		if(request.getParameter("reset") != null) {
			return new ModelAndView(new RedirectView("viaRegister.jhtm"));
		}

		try {
			if (siteConfig.get("CUSTOMER_IP_ADDRESS").getValue().equals("true")) {
				customerForm.getCustomer().setNote(
						"--START-- \nOn " + new Date() + " \n\"" + customerForm.getCustomer().getAddress().getFirstName() + " " + customerForm.getCustomer().getAddress().getLastName()
								+ "\" was trying to register with this IP ADDRESS: " + request.getRemoteAddr() + "\n--END--\n\n");
			}
			customerForm.getCustomer().setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_NEW_REGISTRANT").getValue());
			if (customerForm.getCustomer().getTrackcode() != null && siteConfig.get("TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT").getValue().trim().length() > 0) {
				if (!siteConfig.get("TRACK_CODE").getValue().equals("")
						&& siteConfig.get("TRACK_CODE").getValue().trim().toLowerCase().equals(customerForm.getCustomer().getTrackcode().trim().toLowerCase())) {
					customerForm.getCustomer().setProtectedAccess(siteConfig.get("TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT").getValue());
				}
			}

			if ((Boolean) gSiteConfig.get("gSALES_REP")) {
				/*
				 * If territory zipcode module is on and customer belongs to a zipcode, that is assigned to a salesrep, that salesrep will be assigned to a customer first.
				 * 
				 * If territory zipcode module is not on or customer zipcode does not match with any of the salesrep than look for the next available sales rep in the cycle.
				 */
				if (siteConfig.get("TERRITORY_ZIPCODE").getValue().equals("true")) {
					salesRep = this.webJaguar.getSalesRepByTerritoryZipcode(customerForm.getCustomer().getAddress().getZip());
				}

				if (salesRep == null && siteConfig.get("SALES_REP_SEQUENTIAL_ASSIGNEMENT").getValue().equals("true")) {
					salesRep = this.webJaguar.getNextSalesRepInQueue();
				}

				if (salesRep != null) {
					// mark sales rep as assigned to customer in the current cycle
					this.webJaguar.markSalesRepAssignedToCustomerInCycle(salesRep.getId(), true);
				}
				// assign customer to a sales rep
				if(!(customerForm!=null && customerForm.getCustomer()!=null && customerForm.getCustomer().getSalesRepId()>-1))
					customerForm.getCustomer().setSalesRepId((salesRep == null) ? null : salesRep.getId());
			}

			// generate random cardID
			generateRandomCardID(customerForm.getCustomer());

			this.webJaguar.insertCustomer(customerForm.getCustomer(), customerForm.getShipping(), (Boolean) gSiteConfig.get("gCRM"));
			
			DialingNote note = new DialingNote();
			Integer customerId = (customerForm!=null && customerForm.getCustomer()!=null)?customerForm.getCustomer().getId():-1;
			note.setCustomerId(customerId);
			note.setNote((customerForm!=null && customerForm.getCustomer()!=null && customerForm.getCustomer().getNewDialingNote()!=null)?customerForm.getCustomer().getNewDialingNote().getNote():null);
			if((customerId!=-1) && (note.getNote()!=null && note.getNote().length()>0)){
				this.webJaguar.insertDialingNoteHistory(note);
			}
		} catch (DataIntegrityViolationException ex) {
			errors.rejectValue("customer.username", "USER_ID_ALREADY_EXISTS", "An account using this email address already exists.");
			return showForm(request, response, errors);
		}
		
		//send email
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);
			customerForm.getCustomer().setHost(request.getHeader("host"));
		}
		SiteMessage siteMessage = null;
		try {
			Integer messageId = null;
			Integer newRegistrationId;
			if (multiStore != null) {
				messageId = multiStore.getMidNewRegistration();
			} else {
				String languageCode = customerForm.getCustomer().getLanguageCode().toString();
				try {
					newRegistrationId = this.webJaguar.getLanguageSetting(languageCode).getNewRegistrationId();
				} catch (Exception e) {
					newRegistrationId = null;
				}
				if (languageCode.equalsIgnoreCase((LanguageCode.en).toString()) || newRegistrationId == null) {
					messageId = Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_NEW_REGISTRATION").getValue());
				} else {
					messageId = Integer.parseInt(newRegistrationId.toString());
				}
			}
			siteMessage = this.webJaguar.getSiteMessageById(messageId);
			if (siteMessage != null) {
				// send email
				sendEmail(siteMessage, request, customerForm.getCustomer(), multiStore, salesRep);
			}
		} catch (NumberFormatException e) {
			// do nothing
			e.printStackTrace();
		}

		return new ModelAndView(new RedirectView("category.jhtm?cid=569"));
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("countrylist", this.webJaguar.getEnabledCountryList());
		map.put("statelist", this.webJaguar.getStateList("US"));
		map.put("caProvinceList", this.webJaguar.getStateList("CA"));
		map.put("mobileCarriers", this.webJaguar.getMobileCarrierList());
		map.put("languageCodes", this.webJaguar.getLanguageSettings());
		SalesRepSearch salesRepSearch = new SalesRepSearch();;
		salesRepSearch.setShowActiveSalesRep(true);
		map.put("salesRepList", this.webJaguar.getSalesRepList(salesRepSearch));
		setLayout(request);
		return map;
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {

		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		CustomerForm customerForm = new CustomerForm();

		// Customer Fields
		List<CustomerField> customerFields = this.webJaguar.getCustomerFields();
		Iterator<CustomerField> iter = customerFields.iterator();
		while (iter.hasNext()) {
			CustomerField customerField = iter.next();
			if (customerField.getId() != 2 && customerField.getId() != 8 && customerField.getId() != 3 && customerField.getId() != 1) {
				iter.remove();
			}
		}
		if (!customerFields.isEmpty()) {
			customerForm.setCustomerFields(customerFields);
		}

		if (ServletRequestUtils.getStringParameter(request, "trackcode") != null) {
			customerForm.getCustomer().setTrackcode(ServletRequestUtils.getStringParameter(request, "trackcode"));
		}

		if (ServletRequestUtils.getStringParameter(request, "registeredby") != null) {
			customerForm.getCustomer().setRegisteredBy(ServletRequestUtils.getStringParameter(request, "registeredby"));
		}

		if (siteConfig.get("ADDRESS_RESIDENTIAL_COMMERCIAL").getValue().equals("1") || siteConfig.get("ADDRESS_RESIDENTIAL_COMMERCIAL").getValue().equals("3")) {
			customerForm.getCustomer().getAddress().setResidential(true);
		}
		// remove this code whemn viatading is moved to its own branch
		if (siteConfig.get("SITE_URL").getValue().contains("viatrading") || siteConfig.get("SITE_URL").getValue().contains("test.wjserver180.com")) {
			customerForm.getCustomer().getAddress().setLiftGate(true);
		}
		return customerForm;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		CustomerForm customerForm = (CustomerForm) command;
		
		if(request.getParameter("reset") == null) {

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.password", "form.required", "required");
		if (customerForm.getCustomerFields() != null) {
			for (CustomerField customerField : customerForm.getCustomerFields()) {
				if (customerField.isRequired())
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.field" + customerField.getId(), "form.required", "required");
			}
		}

		// password length
		if (customerForm.getCustomer().getPassword().length() > 0) {
			if (customerForm.getCustomer().getPassword().length() < 5) {
				Object[] errorArgs = { new Integer(5) };
				errors.rejectValue("customer.password", "form.charLessMin", errorArgs, "should be at least 5 chars");
			}
		}

		// address
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.firstName", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.lastName", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.addr1", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.city", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.stateProvince", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.country", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.zip", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.phone", "form.required", "required");

		// email
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.username", "form.required", "required");

		//customer field
		
		if (customerForm.getCustomerFields() != null ) {
    		for ( CustomerField customerField : customerForm.getCustomerFields() ) {
    			if ( customerField.isRequired() )
	    		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.field" + customerField.getId(), "form.required", "required");
	    	}
    	}
		
		if (!Constants.validateEmail(customerForm.getCustomer().getUsername(), null)) {
			errors.rejectValue("customer.username", "invalid email", "Invalid Email");
		}
		}
	}

	private void generateRandomCardID(Customer customer) {
		String cardID = UUID.randomUUID().toString().toUpperCase().replace("-", "").substring(0, 9);
		if (this.webJaguar.getCustomerByCardID(cardID) == null) {
			customer.setCardId(cardID);
		} else
			generateRandomCardID(customer);
	}

	private void setLayout(HttpServletRequest request) {
		Layout layout = (Layout) request.getAttribute("layout");
		layout.setHeaderHtml("");
		layout.setTopBarHtml("");
		layout.setRightBarBottomHtml("");
		layout.setRightBarBottomHtmlLogin("");
		layout.setRightBarTopHtml("");
		layout.setRightBarTopHtmlLogin("");
		layout.setLeftBarTopHtml("");
		layout.setLeftBarBottomHtml("");
		layout.setHideLeftBar(true);
		layout.setFooterHtml("");
	}
	
	private void sendEmail(SiteMessage siteMessage, HttpServletRequest request, Customer customer, MultiStore multiStore, SalesRep salesRep) {

		// send email
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		if (multiStore != null) {
			contactEmail = multiStore.getContactEmail();
		}
		Format formatter2 = new SimpleDateFormat("MM/dd/yyyy");

		StringBuffer messageBuffer = new StringBuffer();

		String firstName = customer.getAddress().getFirstName();
		if (firstName == null) {
			firstName = "";
		}
		String lastname = customer.getAddress().getLastName();
		if (lastname == null) {
			lastname = "";
		}
		String note = customer.getNote();
		if (note == null) {
			note = "";
		}

		// subject
		siteMessage.setSubject(siteMessage.getSubject().replace("#email#", customer.getUsername()));
		siteMessage.setSubject(siteMessage.getSubject().replace("#password#", customer.getPassword()));
		siteMessage.setSubject(siteMessage.getSubject().replace("#firstname#", firstName));
		siteMessage.setSubject(siteMessage.getSubject().replace("#lastname#", lastname));
		siteMessage.setSubject(siteMessage.getSubject().replace("#note#", note));
		siteMessage.setSubject(siteMessage.getSubject().replace("#date#", formatter2.format(new Date()).toString()));

		if (salesRep != null) {
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepName#", salesRep.getName()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepEmail#", salesRep.getEmail()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		// message
		siteMessage.setMessage(siteMessage.getMessage().replace("#email#", customer.getUsername()));
		siteMessage.setMessage(siteMessage.getMessage().replace("#password#", customer.getPassword()));
		siteMessage.setMessage(siteMessage.getMessage().replace("#firstname#", firstName));
		siteMessage.setMessage(siteMessage.getMessage().replace("#lastname#", lastname));
		siteMessage.setMessage(siteMessage.getMessage().replace("#note#", note));
		siteMessage.setMessage(siteMessage.getMessage().replace("#date#", formatter2.format(new Date()).toString()));

		if (salesRep != null) {
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepName#", salesRep.getName()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepEmail#", salesRep.getEmail()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		messageBuffer.append(siteMessage.getMessage());

		try {
			// construct email message
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.addBcc(contactEmail);
			helper.setSubject(siteMessage.getSubject());
			helper.setText(messageBuffer.toString(), siteMessage.isHtml());
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing
		}
		// salesRep is not null is TERRITORY_ZIPCODE is ON
		// TODO if message is not HTML we send html message to SalesRep
		if (salesRep != null) {
			messageBuffer.append("<br /><br />New Customer Info:<br />Name:<b>" + customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName() + "</b>");
			messageBuffer.append("<br />Company Name:<b>" + customer.getAddress().getCompany() + "</b>");
			messageBuffer.append("<br />Address1:<b>" + customer.getAddress().getAddr1() + "</b>");
			messageBuffer.append("<br />Address2:<b>" + ((customer.getAddress().getAddr2() == null) ? "" : customer.getAddress().getAddr2()) + "</b>");
			messageBuffer.append("<br />City:<b>" + customer.getAddress().getCity() + "</b> State:<b> " + customer.getAddress().getStateProvince() + "</b> Zipcode:<b> "
					+ customer.getAddress().getZip() + "</b>");
			messageBuffer.append("<br />Phone:<b>" + customer.getAddress().getPhone() + "</b>");
			messageBuffer.append("<br />Email:<b>" + customer.getUsername() + "</b>");
			messageBuffer.append("<br />Note:<b>" + customer.getNote() + "</b>");
			try {
				// construct email message
				MimeMessage mms = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
				helper.setTo(salesRep.getEmail());
				helper.setFrom(contactEmail);
				helper.setCc(contactEmail);

				helper.setSubject(siteMessage.getSubject());
				helper.setText(messageBuffer.toString(), siteMessage.isHtml());
				mailSender.send(mms);
			} catch (Exception ex) {
				// do nothing
			}
		}
	}
}