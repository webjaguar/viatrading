package com.webjaguar.web.frontend.register;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.view.RedirectView;
import com.webjaguar.listener.SessionListener;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UserSession;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.web.form.CustomerForm;
import com.webjaguar.web.validator.CustomerFormValidator;
import org.apache.commons.codec.binary.Base64;

public class TouchLoginWizardController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) {this.mailSender = mailSender;}
	
	public Boolean cellPhoneAlreadyExist;
	public Boolean emailPhoneAlreadyExist;
	public Boolean errorExist;
	
	public TouchLoginWizardController() {
		setCommandName("customerForm");
		setCommandClass(CustomerForm.class);
	}
 
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {    	
    	
    	CustomerForm customerForm = (CustomerForm) command;
       	Map<String, Object> map = new HashMap<String, Object>();
       	
		String language = ServletRequestUtils.getStringParameter(request, "language");
		String error = null;
		
		if(language==null || language.isEmpty() || language.equalsIgnoreCase("en")){
			error = "Please enter correct user credentials to login";
		}else{
			error = "Por favor, introduzca las credenciales de usuario correctos para iniciar sesi�n";
		}
		
		if (ServletRequestUtils.getStringParameter(request, "error") != null){
			map.put("error", error);
		}
       	
		return map;
    }
    
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );	
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		String dir = "";
		if (siteConfig.get("TSR_DIRECTORY").getValue().length() > 0) {
			dir = siteConfig.get("TSR_DIRECTORY").getValue() + "/";
		} 
		
		String language = ServletRequestUtils.getStringParameter(request, "language");
		String lng = null;
		
		if(language==null || language.isEmpty() || language.equalsIgnoreCase("en")){
			lng = "en";
			setPages( new String[] {dir+"frontend/register/touchScreen/stepLogin",dir+"frontend/register/touchScreen/stepLogin",dir+"frontend/register/touchScreen/stepLogin",dir+"frontend/register/touchScreen/stepLogin",dir+"frontend/register/touchScreen/stepLogin"});
		}else{
			lng = "es";
			setPages( new String[] {dir+"frontend/register/touchScreen/es/stepLogin",dir+"frontend/register/touchScreen/es/stepLogin",dir+"frontend/register/touchScreen/es/stepLogin",dir+"frontend/register/touchScreen/es/stepLogin",dir+"frontend/register/touchScreen/es/stepLogin"});
		}
		
		CustomerForm customerForm = new CustomerForm();
		return customerForm;
	}
	
	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		
		CustomerForm customerForm = (CustomerForm) command;
    	
		String language = ServletRequestUtils.getStringParameter(request, "language");
		String lng = null;
		
		String error = null;
		
		String dir = "";
		if (siteConfig.get("TSR_DIRECTORY").getValue().length() > 0) {
			dir = siteConfig.get("TSR_DIRECTORY").getValue() + "/";
		} 
		
		if(language==null || language.isEmpty() || language.equalsIgnoreCase("en")){
			lng = "en";
			
			error = "Please enter correct user credentials to login";
			
			setPages( new String[] {dir+"frontend/register/touchScreen/stepLogin",dir+"frontend/register/touchScreen/stepLogin",dir+"frontend/register/touchScreen/stepLogin",dir+"frontend/register/touchScreen/stepLogin",dir+"frontend/register/touchScreen/stepLogin"});
		}else{
			lng = "es";
			
			error = "Por favor, introduzca las credenciales de usuario correctos para iniciar sesi�n";
			
			setPages( new String[] {dir+"frontend/register/touchScreen/es/stepLogin",dir+"frontend/register/touchScreen/es/stepLogin",dir+"frontend/register/touchScreen/es/stepLogin",dir+"frontend/register/touchScreen/es/stepLogin",dir+"frontend/register/touchScreen/es/stepLogin"});
		}

		String forwardAction = request.getParameter("forwardAction");
		Customer customer = null;
		
		if (this.webJaguar.getUserSession(request) != null) {
			try{
				UserSession userSession = this.webJaguar.getUserSession(request);
				Integer userId = userSession.getUserid();
				customer = this.webJaguar.getCustomerById(userId);
			}catch(Exception ex){
			}
		}
		
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		
		if(customer==null){
			
			Boolean registerCellPhone = false;
			if(customerForm!=null && customerForm.getCustomer()!=null && customerForm.getCustomer().getUsername()!=null && customerForm.getCustomer().getUsername().contains("@")){
				registerCellPhone = false;
			}else{
				registerCellPhone = true;
			}
			
			String username = customerForm.getCustomer().getUsername();
			String password = customerForm.getCustomer().getPassword();
			String cellPhone = customerForm.getCustomer().getUsername();
		
			if(registerCellPhone){
				if (cellPhone!=null && cellPhone.length() > 0 && password!=null && password.length() > 0) {
					customer = this.webJaguar.getCellPhoneByUsernameAndPassword(cellPhone, password);
				}
			}
			else{
				if (username!=null && username.length() > 0 && password!=null && password.length() > 0) {
					customer = this.webJaguar.getCustomerByUsernameAndPassword(username, password);
				} 
			}
		}
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		String siteURL = siteConfig.get("SITE_URL").getValue();
		System.out.println("------------------siteURL------------------" + siteURL);
		
		if(customer!=null){
			//UserSession userSession = new UserSession();
			//userSession.setUsername(customer.getUsername());
			//userSession.setFirstName(customer.getAddress().getFirstName());
			//userSession.setLastName(customer.getAddress().getLastName());
			//userSession.setUserid(customer.getId());
			//this.webJaguar.setUserSession(request, userSession);
			//SessionListener.getActiveSessions().put(request.getSession().getId(), userSession.getUserid());
			
			ModelAndView modelAndView = null;
			
			if(lng!=null && lng.equalsIgnoreCase("es")){
				modelAndView = new ModelAndView(new RedirectView("/dv/tsr/myaccount-es.php?language=" + lng + "&id=" + customer.getId()));
			}else{
				modelAndView = new ModelAndView(new RedirectView("/dv/tsr/myaccount.php?language=" + lng + "&id=" + customer.getId()));
			}			
			throw new ModelAndViewDefiningException(modelAndView);
		}else{
			myModel.put("error", error);
			ModelAndView modelAndView = new ModelAndView(new RedirectView(siteURL + "/touchLogin.jhtm?error=true&language=" + lng));
			throw new ModelAndViewDefiningException(modelAndView);
		}
		
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		return null;
	}

}
