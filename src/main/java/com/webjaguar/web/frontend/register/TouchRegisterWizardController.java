/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.register;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.listener.SessionListener;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UserSession;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.PasswordByte;
import com.webjaguar.web.form.CustomerForm;
import com.webjaguar.web.validator.CustomerFormValidator;
import org.apache.commons.codec.binary.Base64;

public class TouchRegisterWizardController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) {this.mailSender = mailSender;}
	
	public Boolean cellPhoneAlreadyExist;
	public Boolean emailPhoneAlreadyExist;
	public Boolean errorExist;
	public Boolean errorExistPasswordMatch=false;
	
	public TouchRegisterWizardController() {
		setCommandName("customerForm");
		setCommandClass(CustomerForm.class);
	}
	
    public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {	
    	SalesRep salesRep = null;

    	CustomerForm customerForm = (CustomerForm) command;  	
    	customerForm.getCustomer().setRegisteredBy("touchScreen");
    	customerForm.getCustomer().setTrackcode((String) request.getAttribute("trackcode"));
    	
    	// Protected Access
    	customerForm.getCustomer().setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_NEW_REGISTRANT").getValue());
    	//if (customerForm.getCustomer().getTrackcode() != null && siteConfig.get("TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT").getValue().trim().length() > 0) {
		//	if (!siteConfig.get("TRACK_CODE").getValue().equals("") && siteConfig.get("TRACK_CODE").getValue().trim().toLowerCase().equals(customerForm.getCustomer().getTrackcode().trim().toLowerCase())) {
		//		customerForm.getCustomer().setProtectedAccess(siteConfig.get("TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT").getValue());
		//		System.out.println("TRACK_CODE_PROTECTED_ACCESS " +customerForm.getCustomer().getProtectedAccess());
		//	}
		//}
    	
    	customerForm.getCustomer().setAffiliateParent(0); // by default new customer connect to root.
    	if (customerForm.getCustomer().getUsername().equalsIgnoreCase("")) {
			customerForm.getCustomer().setUsername(customerForm.getCustomer().getCardId()+"@viatrading.com");
			customerForm.getCustomer().setEmailNotify(false);
			customerForm.getCustomer().setUnsubscribe(true);
    	}
    	
    	try {
    		
			this.webJaguar.insertCustomer(customerForm.getCustomer(), null, (Boolean) gSiteConfig.get("gCRM"));
		} catch (DataIntegrityViolationException ex) {
			errors.rejectValue("customer.username", "USER_ID_ALREADY_EXISTS", "An account using this email address already exists.");
			return showForm(request, response, errors);
		}
		if ( customerForm.getShipping().compareTo( customerForm.getCustomer().getAddress() ) == 0 ){
			// add shipping address.
			customerForm.getShipping().setUserId( customerForm.getCustomer().getId() );
			customerForm.getShipping().setPrimary( true );
			this.webJaguar.insertAddress(customerForm.getShipping());
		}
		
		//send email
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);
			customerForm.getCustomer().setHost(request.getHeader("host"));
		}		
		SiteMessage siteMessage = null;
		try {
			Integer messageId = null;
			Integer newRegistrationId;
			if (multiStore != null) {
				messageId = multiStore.getMidNewRegistration();
			} else {
				String languageCode = customerForm.getCustomer().getLanguageCode().toString();
				try {
					newRegistrationId = this.webJaguar.getLanguageSetting(languageCode).getNewRegistrationId();
				} catch (Exception e) {
					newRegistrationId = null;
				}
				if (languageCode.equalsIgnoreCase((LanguageCode.en).toString()) || newRegistrationId == null) {
					messageId = Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_NEW_REGISTRATION").getValue());
				} else {
					messageId = Integer.parseInt(newRegistrationId.toString());
				}
			}
			siteMessage = this.webJaguar.getSiteMessageById(messageId);
			if (siteMessage != null) {
				// send email
				sendEmail(siteMessage, request, customerForm.getCustomer(), multiStore, salesRep);
			}
		} catch (NumberFormatException e) {
			// do nothing
			e.printStackTrace();
		}
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put( "customerForm", customerForm );	
		setLayout(request);
		
		// log customer in (for widgets)
		UserSession userSession = new UserSession();
		userSession.setUsername(customerForm.getCustomer().getUsername());
		userSession.setFirstName( customerForm.getCustomer().getAddress().getFirstName() );
		userSession.setLastName( customerForm.getCustomer().getAddress().getLastName() );
		userSession.setUserid(customerForm.getCustomer().getId());
		this.webJaguar.setUserSession(request, userSession);
		SessionListener.getActiveSessions().put(request.getSession().getId(), userSession.getUserid());
		
		if(customerForm.getLanguage().equalsIgnoreCase("es")){
			return new ModelAndView(new RedirectView("category.jhtm?cid=556"));
		}else{
			return new ModelAndView(new RedirectView("category.jhtm?cid=557"));
		}
    }
 
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
    	throws Exception {    	
    	
    	CustomerForm customerForm = (CustomerForm) command;
    	
    	if(customerForm!=null && customerForm.getCustomer()!=null && customerForm.getCustomer().getUsername()==null){
    			HttpSession session = request.getSession();
    		    Customer customer = (Customer)session.getAttribute("customer");
    		    if(customer!=null){
    		    	customerForm.setCustomer(customer);
    		    }
    		    Address shipping = (Address)session.getAttribute("shipping");
    		    if(shipping!=null){
    		    	customerForm.setShipping(shipping);
    		    }
    	}
    	
    	if(customerForm.getLanguage()!=null && customerForm.getLanguage().equalsIgnoreCase("es")){
        	customerForm.getCustomer().setLanguageCode(LanguageCode.es);
        	customerForm.getCustomer().setLanguage(customerForm.getLanguage());
    	}else{
        	customerForm.getCustomer().setLanguageCode(LanguageCode.en);
        	customerForm.getCustomer().setLanguage(customerForm.getLanguage());
    	}
    	
       	Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> myModel = new HashMap<String, Object>();
		// get a list of enabled country code from database
		myModel.put("countrylist", this.webJaguar.getEnabledCountryList());
		myModel.put("statelist",this.webJaguar.getStateList("US"));
		myModel.put("mobileCarriers",this.webJaguar.getMobileCarrierList());
		myModel.put("languageCodes", this.webJaguar.getLanguageSettings());
		myModel.put("caProvinceList", this.webJaguar.getStateList("CA"));
		switch (page) {
			case 2:
				if(customerForm.getCustomer()!=null && customerForm.getCustomer().getId()!=null){
				}
				break;
			case 3:
				if (customerForm.getShipping() == null || customerForm.getCustomer().getAddress().compareTo(customerForm.getShipping()) == 1) {
					Address shippingAddress = new Address();
					shippingAddress.setFirstName( customerForm.getCustomer().getAddress().getFirstName() );
					shippingAddress.setLastName( customerForm.getCustomer().getAddress().getLastName() );
					shippingAddress.setAddr1( customerForm.getCustomer().getAddress().getAddr1() );
					shippingAddress.setAddr2(customerForm.getCustomer().getAddress().getAddr2());
					shippingAddress.setCountry( customerForm.getCustomer().getAddress().getCountry() );
					shippingAddress.setCity( customerForm.getCustomer().getAddress().getCity() );
					shippingAddress.setStateProvince( customerForm.getCustomer().getAddress().getStateProvince() );
					shippingAddress.setZip( customerForm.getCustomer().getAddress().getZip() );
					shippingAddress.setPhone( customerForm.getCustomer().getAddress().getPhone() );
					shippingAddress.setLiftGate( customerForm.getCustomer().getAddress().isLiftGate() );
					shippingAddress.setResidential( customerForm.getCustomer().getAddress().isResidential() );
					customerForm.setShipping( shippingAddress );
				}
				break;	
			case 4:
				break;
			case 5:
				customerForm.setConfirmPassword(customerForm.getCustomer().getPassword());
				break;
			case 7:
				if (customerForm.getCustomer().getUsername().equalsIgnoreCase("")){
					customerForm.getCustomer().setUsername(customerForm.getCustomer().getCardId()+"@viatrading.com");
					customerForm.getCustomer().setEmailNotify(false);
					customerForm.getCustomer().setUnsubscribe(true);
				}
				break;	
		}	
		
		myModel.put("cust", customerForm.getCustomer()); 
		map.put("model", myModel); 
		
		if(customerForm!=null && customerForm.getCustomer()!=null && customerForm.getCustomer().getAddress()!=null){
			map.put("countrySelected", customerForm.getCustomer().getAddress().getCountry()); 
			
			String states = customerForm.getCustomer().getAddress().getStateProvince()!=null?customerForm.getCustomer().getAddress().getStateProvince():"";
			String[] stateArray = states.split(",,");
			if(stateArray.length>0){
				map.put("stateSelected", stateArray[0]); 
				customerForm.getCustomer().getAddress().setStateProvince(stateArray[0]);
			}
		}else{
			map.put("countrySelected", ""); 
			map.put("stateSelected", ""); 
		}
		map.put("cellPhoneAlreadyExist", cellPhoneAlreadyExist);
		map.put("emailPhoneAlreadyExist", emailPhoneAlreadyExist);
		map.put("errorExist", errorExist);
		
		setLayout(request);
		map.put("countryCodes", getCountryCodes());
		return map;
    }
    
    protected void validatePage(Object command, Errors errors, int page) {
		CustomerForm customerForm = (CustomerForm) command;
		CustomerFormValidator validator = (CustomerFormValidator) getValidator();
		switch (page) {
			case 2:
				Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
				validate(customerForm, errors);
				
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.password", "form.required", "Required");
				if(!errorExistPasswordMatch){
					ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "form.required", "Required");
				}
				
				Boolean regCellPhone = false;
				Boolean regCellRequired = false;
				if(customerForm!=null && customerForm.getCustomer()!=null && customerForm.getCustomer().getUsernameDisabled()!=null && customerForm.getCustomer().getAddress().getCellPhone()!=null && customerForm.getCustomer().getAddress().getCellPhone().length()>0){
					regCellPhone = true;
				}
				
				if(!regCellPhone && customerForm.getCustomer().isTextMessageNotify()){
					regCellRequired = true;
					errors.rejectValue("customer.address.cellPhone", "form.required", "Cell phone number can not be empty");
				}
				
				final String US_PHONE_REGEXP = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})(\\sExt-\\d{1,5})?$";
				final String New_format = "^\\((\\d{3})\\)(\\d{3})[- ](\\d{4})(\\s Ext-\\d{1,5})?$";

				if(regCellPhone){
					if (!(Pattern.compile(New_format).matcher(customerForm.getCustomer().getAddress().getCellPhone()).matches())) {
						errors.rejectValue("customer.address.cellPhone", "form.usPhone", "invalid USA phone");
				    }else if (this.webJaguar.checkUserByCellPhone(customerForm.getCustomer().getAddress().getCellPhone())!= null) {
						 errors.rejectValue("customer.address.cellPhone", "form.exist", "An account using this cell phone number already");
						 cellPhoneAlreadyExist = true;
		    		}else if(!errors.hasErrors() && (customerForm.getCustomer().getUsername()==null || customerForm.getCustomer().getUsername().length()<=0)){
						 this.webJaguar.generateUsername(customerForm.getCustomer());
					}
				}else if(customerForm!=null && customerForm.getCustomer()!=null && customerForm.getCustomer().getUsernameDisabled()!=null && !customerForm.getCustomer().getUsernameDisabled() && customerForm.getCustomer().getUsername()!= null && customerForm.getCustomer().getUsername().length()>0){
					if (this.webJaguar.getCustomerByUsername(customerForm.getCustomer().getUsername()) != null) {
						errors.rejectValue("customer.username", "form.required", "An account using this email address already exists.");
						emailPhoneAlreadyExist = true;
					}	
				}else{
					if(!regCellRequired){
						errors.rejectValue("customer.address.cellPhone", "form.required", "Cell phone number can not be empty");
					}
					errors.rejectValue("customer.username", "form.required", "Email Address can not be empty.");
				}		
				if(customerForm.getCustomer()!=null && customerForm.getCustomer().getAddress()!=null && customerForm.getCustomer().getAddress().getCountry()!=null && customerForm.getCustomer().getAddress().getCountry().equalsIgnoreCase("US") && (customerForm.getCustomer().isTextMessageNotify()) && (customerForm.getCustomer().getAddress().getMobileCarrierId()==null)){
					errors.rejectValue("customer.address.mobileCarrierId", "form.select", "Mobile Carrier should be selected.");
				}
				
				
				break;
			case 3:		
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.addr1", "form.required", "Required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.city", "form.required", "Required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.stateProvince", "form.required", "Required");			
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.zip", "form.required", "Required");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "shipping.phone", "form.required", "Required");
		    	break;	
		    case 4:
		    	if (customerForm.getCustomerFields() != null ) {
		    		for ( CustomerField customerField : customerForm.getCustomerFields() ) {
		    			if ( customerField.isRequired() )
			    		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.field" + customerField.getId(), "form.required", "Required");
			    	}
		    	}
		    	break;
			}
		
			if(errors.hasErrors()){
				errorExist = true;
			}
    }
    
	public void validate(Object obj, Errors errors) {
		CustomerForm customerForm = (CustomerForm) obj;
		if (customerForm.isTouchScreen() && customerForm.getCustomer().getUsername() != "") {
			validateEmail(customerForm, errors);
			validatePassword(customerForm, errors);
		} else if (!customerForm.isTouchScreen()) {
			validateEmail(customerForm, errors);
			validatePassword(customerForm, errors);
		}
		validateAddress(customerForm.getCustomer().getAddress(), errors);
		if (customerForm.isCompanyRequired()) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.company", "form.required", "required");
		}

		Class<Customer> c = Customer.class;
		Method m = null;
		Object arglist[] = null;

		// custom notes
		// TEXT is limited to 65,535 characters. MEDIUMTEXT is 16,777,215
		try {
			for (int i = 1; i <= 3; i++) {
				m = c.getMethod("getCustomNote" + i);
				String customNote = (String) m.invoke(customerForm.getCustomer(), arglist);
				if (customNote != null && customNote.length() > 1000000) {
					Object[] errorArgs = { customNote.length(), new Integer(1000000) };
					errors.rejectValue("customer.customNote" + i, "form.charOverLimit", errorArgs, customNote.length() + " chars (1,000,000 limit)");
				}
			}
		} catch (Exception e) {
			// do nothing
		}
	}

	public void validatePassword(CustomerForm customerForm, Errors errors) {
		// Strong password require at least 1 special character and 1 Digit in Password.
		if (customerForm.isStrongPassword()) {
			PasswordByte password = new PasswordByte(customerForm.getCustomer().getPassword());
			if ((password.getLength() < customerForm.getMinPasswordLength()) || (password.getNumOfDigits() < 1 || password.getNumOfSpecial() < 1)) {
				Object[] errorArgs = { new Integer(customerForm.getMinPasswordLength()) };
				errors.rejectValue("customer.password", "form.charLessMinSpDig", errorArgs, "should be at least " + customerForm.getMinPasswordLength() + " chars");
			} else if (!customerForm.getCustomer().getPassword().equals(customerForm.getConfirmPassword())) {
				errorExistPasswordMatch=true;
				errors.rejectValue("confirmPassword", "form.passwordMismatch", "matching passwords required");
			}
		} else {
			if (customerForm!=null && customerForm.getCustomer()!=null && customerForm.getCustomer().getPassword()!=null && customerForm.getCustomer().getPassword().length() > 0) {
				if (customerForm.getCustomer().getPassword().length() < customerForm.getMinPasswordLength()) {
					Object[] errorArgs = { new Integer(customerForm.getMinPasswordLength()) };
					errors.rejectValue("customer.password", "form.charLessMin", errorArgs, "should be at least " + customerForm.getMinPasswordLength() + " chars");
				} else if (!customerForm.getCustomer().getPassword().equals(customerForm.getConfirmPassword())) {
					errorExistPasswordMatch=true;
					errors.rejectValue("confirmPassword", "form.passwordMismatch", "matching passwords required");
				}
			}
		}
	}

	private void validateEmail(CustomerForm customerForm, Errors errors) {
		
		Customer customer = customerForm.getCustomer();
		
		if(customer.getUsernameDisabled()==null){
			if (customer.getUsername() == null || customer.getUsername().isEmpty()) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.username", "form.required", "required");
			} else {
				if (!Constants.validateEmail(customer.getUsername(), customerForm.getEmailRegExp())) {
					errors.rejectValue("customer.username", "form.invalidEmail", "invalid email");
				}
			}
		}
		else if(customer.getUsernameDisabled()!=null && !customer.getUsernameDisabled()){
			if (customer.getUsername() == null || customer.getUsername().isEmpty()) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.username", "form.required", "required");
			} else {
				if (!Constants.validateEmail(customer.getUsername(), customerForm.getEmailRegExp())) {
					errors.rejectValue("customer.username", "form.invalidEmail", "invalid email");
				}
			}
		}

		if (customer.getExtraEmail() != null) {
			StringTokenizer st = new StringTokenizer(customer.getExtraEmail(), ", ", false);
			while (st.hasMoreTokens()) {
				String tempToken = st.nextToken();
				if (!Constants.validateEmail(tempToken, customerForm.getEmailRegExp())) {
					errors.rejectValue("customer.extraEmail", "form.invalidEmail", "invalid email");
				}
			}
		}
	}
	
	public void validateAddress(Address address, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.firstName", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.lastName", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.addr1", "form.required", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.city", "form.required", "required");
		if (address.isStateProvinceNA()) {
			address.setStateProvince("");
		} else {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.stateProvince", "form.required", "required");
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.country", "form.required", "required");

		if (address.getCountry().equalsIgnoreCase("us")) {
			// (123)456-7890, 123-456-7890, 1234567890, (123)-456-7890
			// final String US_PHONE_REGEXP = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$";
			// with ext (xxx) xxx-xxxx Ext-xxxxx
			final String US_PHONE_REGEXP = "^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})(\\sExt-\\d{1,5})?$";
			final String US_ZIP_REGEXP = "^\\d{5}(-\\d{4})?$";
			final String New_format = "^\\((\\d{3})\\)(\\d{3})[- ](\\d{4})(\\s Ext-\\d{1,5})?$";

			// if added as 10 digit number we add (xxx) xxx-xxxx format to it.
			if (Constants.isDigit(address.getPhone().trim()) && address.getPhone().trim().length() == 10) {
				address.setPhone("(" + address.getPhone().trim().substring(0, 3) + ") " + address.getPhone().trim().substring(3, 6) + "-" + address.getPhone().trim().substring(6));
			}
			if (!(Pattern.compile(New_format).matcher(address.getPhone().trim()).matches())) {
				errors.rejectValue("customer.address.phone", "form.usPhone", "invalid USA phone");
			}
			if (address.getZip() == null || address.getZip().isEmpty()) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.zip", "form.required", "required");
			} else {
				if (!(Pattern.compile(US_ZIP_REGEXP).matcher(address.getZip().trim()).matches())) {
					errors.rejectValue("customer.address.zip", "form.usZipcode", "invalid USA zip code");
				}
			}
			address.setPhone(address.getPhone().trim());
			address.setZip(address.getZip().trim());
		} else if (address.getCountry().equalsIgnoreCase("ca")) {
			final String CA_ZIP_REGEXP_SPACE = "[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]\\d[a-zA-Z] \\d[a-zA-Z]\\d";
			final String CA_ZIP_REGEXP = "[abceghjklmnprstvxyABCEGHJKLMNPRSTVXY]\\d[a-zA-Z]\\d[a-zA-Z]\\d";
			if (address.getZip() == null || address.getZip().isEmpty()) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.zip", "form.required", "required");
			} else {
				if (!Pattern.compile(CA_ZIP_REGEXP_SPACE).matcher(address.getZip()).matches() && Pattern.compile(CA_ZIP_REGEXP).matcher(address.getZip()).matches()) {
					address.setZip(address.getZip().substring(0, 3) + " " + address.getZip().substring(3, 6));
				}
				if (!(Pattern.compile(CA_ZIP_REGEXP).matcher(address.getZip()).matches() || Pattern.compile(CA_ZIP_REGEXP_SPACE).matcher(address.getZip()).matches())) {
					errors.rejectValue("customer.address.zip", "form.canadaZipcode", "invalid Canada zipcode");
				}
			}
		} else {
//			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.zip", "form.required", "required");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.phone", "form.required", "required");
		}
	}
    
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );	
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		
		String siteURL = siteConfig.get("SITE_URL").getValue();
		
		
		HttpSession session = request.getSession();		
		
		String lang = null;
		if(ServletRequestUtils.getStringParameter(request, "language")!=null && ServletRequestUtils.getStringParameter(request, "language").equalsIgnoreCase("es")){
			lang = "es";
		    session.setAttribute("language", lang);
		    
		}else{
			lang = "en";
		    session.setAttribute("language", lang);
		    
		}
		
		if(ServletRequestUtils.getStringParameter(request, "type")!=null && ServletRequestUtils.getStringParameter(request, "type").equalsIgnoreCase("login")){
			ModelAndView modelAndView = new ModelAndView(new RedirectView(siteURL + "/touchRegister.jhtm?trackcode=&language="+ lang + "&target=01"));
			throw new ModelAndViewDefiningException(modelAndView);
		}else if(ServletRequestUtils.getStringParameter(request, "type")!=null && ServletRequestUtils.getStringParameter(request, "type").equalsIgnoreCase("event")){
			String userId = ServletRequestUtils.getStringParameter(request,"userid","-1");
			ModelAndView modelAndView = new ModelAndView(new RedirectView(siteURL + "/eventRegister.jhtm?language="+ lang + "&regEvent=false&userid=" + userId));
			throw new ModelAndViewDefiningException(modelAndView);
		}
		
		String dir = "";
		if (siteConfig.get("TSR_DIRECTORY").getValue().length() > 0) {
			dir = siteConfig.get("TSR_DIRECTORY").getValue() + "/";
		} 
		
		if(ServletRequestUtils.getStringParameter(request, "language") != null) {
			if(ServletRequestUtils.getStringParameter(request, "language").equals("es")) {
				if(ServletRequestUtils.getStringParameter(request, "register")!=null && ServletRequestUtils.getStringParameter(request, "register").equals("true")){
					setPages( new String[] {dir+"frontend/register/touchScreen/es/step1", dir+"frontend/register/touchScreen/es/step1", dir+"frontend/register/touchScreen/es/step2", dir+"frontend/register/touchScreen/es/step3", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step6"});
				}else{
					if(ServletRequestUtils.getStringParameter(request, "target").equals("01")) {
						setPages( new String[] {dir+"frontend/register/touchScreen/es/step01", dir+"frontend/register/touchScreen/es/step1", dir+"frontend/register/touchScreen/es/step1", dir+"frontend/register/touchScreen/es/step2", dir+"frontend/register/touchScreen/es/step3", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step6", dir+"frontend/register/touchScreen/es/step6"});
					}else if(ServletRequestUtils.getStringParameter(request, "target").equals("1")) {
						
						setPages( new String[] {dir+"frontend/register/touchScreen/es/step1", dir+"frontend/register/touchScreen/es/step1", dir+"frontend/register/touchScreen/es/step1", dir+"frontend/register/touchScreen/es/step2", dir+"frontend/register/touchScreen/es/step3", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step6", dir+"frontend/register/touchScreen/es/step6"});
					}else if(ServletRequestUtils.getStringParameter(request, "target").equals("2")) {
						
						setPages( new String[] {dir+"frontend/register/touchScreen/es/step2", dir+"frontend/register/touchScreen/es/step1", dir+"frontend/register/touchScreen/es/step1", dir+"frontend/register/touchScreen/es/step2", dir+"frontend/register/touchScreen/es/step3", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step6", dir+"frontend/register/touchScreen/es/step6"});
					}else if(ServletRequestUtils.getStringParameter(request, "target").equals("3")) {
						
						setPages( new String[] {dir+"frontend/register/touchScreen/es/step3", dir+"frontend/register/touchScreen/es/step1", dir+"frontend/register/touchScreen/es/step1", dir+"frontend/register/touchScreen/es/step2", dir+"frontend/register/touchScreen/es/step3", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step6", dir+"frontend/register/touchScreen/es/step6"});
					}else if(ServletRequestUtils.getStringParameter(request, "target").equals("4")) {
						
						setPages( new String[] {dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step6", dir+"frontend/register/touchScreen/es/step6"});
					}else if(ServletRequestUtils.getStringParameter(request, "target").equals("5")) {
						
						setPages( new String[] {dir+"frontend/register/touchScreen/es/step6", dir+"frontend/register/touchScreen/es/step6", dir+"frontend/register/touchScreen/es/step6", dir+"frontend/register/touchScreen/es/step6", dir+"frontend/register/touchScreen/es/step6", dir+"frontend/register/touchScreen/es/step6", dir+"frontend/register/touchScreen/es/step6", dir+"frontend/register/touchScreen/es/step6"});
					}					}
			} else {
				if(ServletRequestUtils.getStringParameter(request, "target").equals("01")) {
					setPages( new String[] {dir+"frontend/register/touchScreen/step01", dir+"frontend/register/touchScreen/step1", dir+"frontend/register/touchScreen/step1", dir+"frontend/register/touchScreen/step2", dir+"frontend/register/touchScreen/step3", dir+"frontend/register/touchScreen/step4", dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6"});
				}else if(ServletRequestUtils.getStringParameter(request, "target").equals("1")) {
					
					setPages( new String[] {dir+"frontend/register/touchScreen/step1", dir+"frontend/register/touchScreen/step1", dir+"frontend/register/touchScreen/step1", dir+"frontend/register/touchScreen/step2", dir+"frontend/register/touchScreen/step3", dir+"frontend/register/touchScreen/step4", dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6"});
				}else if(ServletRequestUtils.getStringParameter(request, "target").equals("2")) {
					
					setPages( new String[] {dir+"frontend/register/touchScreen/step2", dir+"frontend/register/touchScreen/step1", dir+"frontend/register/touchScreen/step1", dir+"frontend/register/touchScreen/step2", dir+"frontend/register/touchScreen/step3", dir+"frontend/register/touchScreen/step4", dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6"});
				}else if(ServletRequestUtils.getStringParameter(request, "target").equals("3")) {
					
					setPages( new String[] {dir+"frontend/register/touchScreen/step3", dir+"frontend/register/touchScreen/step1", dir+"frontend/register/touchScreen/step1", dir+"frontend/register/touchScreen/step2", dir+"frontend/register/touchScreen/step3", dir+"frontend/register/touchScreen/step4", dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6"});
				}else if(ServletRequestUtils.getStringParameter(request, "target").equals("4")) {
					
					setPages( new String[] {dir+"frontend/register/touchScreen/step4", dir+"frontend/register/touchScreen/step4", dir+"frontend/register/touchScreen/step4", dir+"frontend/register/touchScreen/step4", dir+"frontend/register/touchScreen/step4", dir+"frontend/register/touchScreen/step4", dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6"});
				}else if(ServletRequestUtils.getStringParameter(request, "target").equals("5")) {
					
					setPages( new String[] {dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6"});
				}	
			}
		} else {
			setPages( new String[] {dir+"frontend/register/touchScreen/step0", dir+"frontend/register/touchScreen/step01", dir+"frontend/register/touchScreen/step1", dir+"frontend/register/touchScreen/step2", dir+"frontend/register/touchScreen/step3", dir+"frontend/register/touchScreen/step4", dir+"frontend/register/touchScreen/step6", dir+"frontend/register/touchScreen/step6"});
		}
		
		// check if customer is logged in
		if (this.webJaguar.getUserSession(request) != null) {
			// remove session
			request.getSession().invalidate();
		}
		if (!ServletRequestUtils.getStringParameter( request, "module", "touch" ).equals("touch")) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("eventRegister.jhtm"), "lang", ServletRequestUtils.getStringParameter( request, "language", "en" ) ));	
		}
		CustomerForm customerForm = new CustomerForm();
		customerForm.setTouchScreen(true);
		customerForm.setLanguage( ServletRequestUtils.getStringParameter( request, "language", "en" ));
		
		if ( siteConfig.get( "CUSTOMERS_REQUIRED_COMPANY" ).getValue().equals( "true" ) ) {
			customerForm.setCompanyRequired( true );
		}

		generateRandomCode(customerForm.getCustomer());
		
		// Customer Fields
		List<CustomerField> customerFields = this.webJaguar.getCustomerFields();
		Iterator<CustomerField> iter = customerFields.iterator();
		while ( iter.hasNext() ) {
			CustomerField customerField = iter.next();
			if (!customerField.isEnabled() || !customerField.isPublicField()) {
				iter.remove();
			}
		} 
		if (!customerFields.isEmpty()) {
			customerForm.setCustomerFields( customerFields );						
		}
		
		if ( siteConfig.get( "ADDRESS_RESIDENTIAL_COMMERCIAL" ).getValue().equals( "1" ) ||  siteConfig.get( "ADDRESS_RESIDENTIAL_COMMERCIAL" ).getValue().equals( "3" )) {
			customerForm.getCustomer().getAddress().setResidential(true);
		}
		//remove this code whemn viatading is moved to its own branch
		if(siteConfig.get( "SITE_URL" ).getValue().contains("viatrading") || siteConfig.get( "SITE_URL" ).getValue().contains("test.wjserver180.com")){
			customerForm.getCustomer().getAddress().setLiftGate(true);
		}
		return customerForm;
	}
	
	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		CustomerForm customerForm = (CustomerForm) command;
    	
		String dir = "";
		if (siteConfig.get("TSR_DIRECTORY").getValue().length() > 0) {
			dir = siteConfig.get("TSR_DIRECTORY").getValue() + "/";
		} 
		
		if(page == 2 || page == 3){
			HttpSession session = request.getSession();
		    session.setAttribute("customer", customerForm.getCustomer());
		    session.setAttribute("shipping", customerForm.getShipping());
		}

		switch (page) {
			case 0:
				if (customerForm.getLanguage().equals( "es" )) {
					setPages( new String[] {dir+"frontend/register/touchScreen/es/step0", dir+"frontend/register/touchScreen/es/step01", dir+"frontend/register/touchScreen/es/step1", dir+"frontend/register/touchScreen/es/step2", dir+"frontend/register/touchScreen/es/step3", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step4", dir+"frontend/register/touchScreen/es/step6"});
				}
				break;
			case 2:							
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				break;
			case 7:
			   	customerForm.getCustomer().setRegisteredBy("touchScreen");
		    	customerForm.getCustomer().setTrackcode((String) request.getAttribute("trackcode"));
		    	customerForm.getCustomer().setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_NEW_REGISTRANT").getValue());
		    	customerForm.getCustomer().setAffiliateParent(0);
		    	customerForm.getCustomer().setLanguage(customerForm.getLanguage());
		    	if (customerForm.getCustomer().getUsername().equalsIgnoreCase("")) {
					customerForm.getCustomer().setUsername(customerForm.getCustomer().getCardId()+"@viatrading.com");
					customerForm.getCustomer().setEmailNotify(false);
					customerForm.getCustomer().setUnsubscribe(true);
		    	}
		    	try {
		    		
					this.webJaguar.insertCustomer(customerForm.getCustomer(), null, (Boolean) gSiteConfig.get("gCRM"));
		    	}catch(Exception ex){
		    		ex.printStackTrace();
		    	}
				String urlStr = "";
				String customerid = "";
				if(customerForm!=null && customerForm.getCustomer()!=null){
					try{
						customerid = customerForm.getCustomer().getId().toString();
					}catch(Exception ex){
					}
				}
				Random randomGenerator = new Random();
			    int randomInt = randomGenerator.nextInt(100);
				urlStr = customerid + "-" + randomInt;
			    byte[] encoded = Base64.encodeBase64(urlStr.getBytes());         
			    urlStr = new String(encoded);		   
			    
			    HttpSession session = request.getSession();
			    String lang = (String)session.getAttribute("language");
			    
				String url = null;
			    if(lang!=null && lang.equalsIgnoreCase("es")){
			    	url = "/dv/tsr/agreement-es.php?language=es&id=" + urlStr;
				}else{
					url = "/dv/tsr/agreement.php?language=en&id=" + urlStr;
				}
			    
				ModelAndView mv = new ModelAndView(new RedirectView(url));
			    throw new ModelAndViewDefiningException(mv);
		}		
	}
	
	private void sendEmail(SiteMessage siteMessage, HttpServletRequest request, Customer customer, MultiStore multiStore, SalesRep salesRep) {

		// send email
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		if (multiStore != null) {
			contactEmail = multiStore.getContactEmail();
		}
		Format formatter2 = new SimpleDateFormat("MM/dd/yyyy");

		StringBuffer messageBuffer = new StringBuffer();

		String firstName = customer.getAddress().getFirstName();
		if (firstName == null) {
			firstName = "";
		}
		String lastname = customer.getAddress().getLastName();
		if (lastname == null) {
			lastname = "";
		}
		String note = customer.getNote();
		if (note == null) {
			note = "";
		}

		// subject
		siteMessage.setSubject(siteMessage.getSubject().replace("#email#", customer.getUsername()));
		siteMessage.setSubject(siteMessage.getSubject().replace("#password#", customer.getPassword()));
		siteMessage.setSubject(siteMessage.getSubject().replace("#firstname#", firstName));
		siteMessage.setSubject(siteMessage.getSubject().replace("#lastname#", lastname));
		siteMessage.setSubject(siteMessage.getSubject().replace("#note#", note));
		siteMessage.setSubject(siteMessage.getSubject().replace("#date#", formatter2.format(new Date()).toString()));

		if (salesRep != null) {
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepName#", salesRep.getName()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepEmail#", salesRep.getEmail()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		// message
		siteMessage.setMessage(siteMessage.getMessage().replace("#email#", customer.getUsername()));
		siteMessage.setMessage(siteMessage.getMessage().replace("#password#", customer.getPassword()));
		siteMessage.setMessage(siteMessage.getMessage().replace("#firstname#", firstName));
		siteMessage.setMessage(siteMessage.getMessage().replace("#lastname#", lastname));
		siteMessage.setMessage(siteMessage.getMessage().replace("#note#", note));
		siteMessage.setMessage(siteMessage.getMessage().replace("#date#", formatter2.format(new Date()).toString()));

		if (salesRep != null) {
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepName#", salesRep.getName()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepEmail#", salesRep.getEmail()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		messageBuffer.append(siteMessage.getMessage());

		try {
			// construct email message
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.addBcc(contactEmail);
			helper.setSubject(siteMessage.getSubject());
			helper.setText(messageBuffer.toString(), siteMessage.isHtml());
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing
		}
		// salesRep is not null is TERRITORY_ZIPCODE is ON
		// TODO if message is not HTML we send html message to SalesRep
		if (salesRep != null) {
			messageBuffer.append("<br /><br />New Customer Info:<br />Name:<b>" + customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName() + "</b>");
			messageBuffer.append("<br />Company Name:<b>" + customer.getAddress().getCompany() + "</b>");
			messageBuffer.append("<br />Address1:<b>" + customer.getAddress().getAddr1() + "</b>");
			messageBuffer.append("<br />Address2:<b>" + ((customer.getAddress().getAddr2() == null) ? "" : customer.getAddress().getAddr2()) + "</b>");
			messageBuffer.append("<br />City:<b>" + customer.getAddress().getCity() + "</b> State:<b> " + customer.getAddress().getStateProvince() + "</b> Zipcode:<b> "
					+ customer.getAddress().getZip() + "</b>");
			messageBuffer.append("<br />Phone:<b>" + customer.getAddress().getPhone() + "</b>");
			messageBuffer.append("<br />Email:<b>" + customer.getUsername() + "</b>");
			messageBuffer.append("<br />Note:<b>" + customer.getNote() + "</b>");
			try {
				// construct email message
				MimeMessage mms = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
				helper.setTo(salesRep.getEmail());
				helper.setFrom(contactEmail);
				helper.setCc(contactEmail);

				helper.setSubject(siteMessage.getSubject());
				helper.setText(messageBuffer.toString(), siteMessage.isHtml());
				mailSender.send(mms);
			} catch (Exception ex) {
				// do nothing
			}
		}
	}
	
	private void generateRandomCode(Customer customer) {
		String cardID = UUID.randomUUID().toString().toUpperCase().replace("-","").substring(0, 9);
		if (this.webJaguar.getCustomerByCardID(cardID) == null) {
			customer.setCardId(cardID);
		} else
			generateRandomCode(customer);
	}
	
    private Map<String,String> getCountryCodes(){
    	
    	Map<String,String> countryCodeMap = new HashMap<String,String>();
    	Map<String,String> countryMap = new HashMap<String,String>();

    	countryCodeMap.put("Andorra", "AD");
    	countryCodeMap.put("United Arab Emirates", "AE");
    	countryCodeMap.put("Afghanistan", "AF");
    	countryCodeMap.put("Antigua and Barbuda", "AG");
    	countryCodeMap.put("Anguilla", "AI");
    	countryCodeMap.put("Albania", "AL");
    	countryCodeMap.put("Armenia", "AM");
    	countryCodeMap.put("Netherlands Antilles", "AN");
    	countryCodeMap.put("Angola", "AO");
    	countryCodeMap.put("Antarctica", "AQ");
    	countryCodeMap.put("Argentina", "AR");
    	countryCodeMap.put("American Samoa", "AS");
    	countryCodeMap.put("Austria", "AT");
    	countryCodeMap.put("Australia", "AU");
    	countryCodeMap.put("Aruba", "AW");
    	countryCodeMap.put("Azerbaijan", "AZ");
    	countryCodeMap.put("Bosnia and Herzegovina", "BA");
    	countryCodeMap.put("Barbados", "BB");
    	countryCodeMap.put("Bangladesh", "BD");
    	countryCodeMap.put("Belgium", "BE");
    	countryCodeMap.put("Burkina Faso", "BF");
    	countryCodeMap.put("Bulgaria", "BG");
    	countryCodeMap.put("Bahrain", "BH");
    	countryCodeMap.put("Burundi", "BI");
    	countryCodeMap.put("Benin", "BJ");
    	countryCodeMap.put("Bermuda", "BM");
    	countryCodeMap.put("Brunei Darussalam", "BN");
    	countryCodeMap.put("Bolivia", "BO");
    	countryCodeMap.put("Brazil", "BR");
    	countryCodeMap.put("Bahamas", "BS");
    	countryCodeMap.put("Bhutan", "BT");
    	countryCodeMap.put("Bouvet Island", "BV");
    	countryCodeMap.put("Botswana", "BW");
    	countryCodeMap.put("Belarus", "BY");
    	countryCodeMap.put("Belize", "BZ");
    	countryCodeMap.put("Canada", "CA");
    	countryCodeMap.put("Cocos Islands", "CC");
    	countryCodeMap.put("Central African Republic", "CF");
    	countryCodeMap.put("Congo", "CG");
    	countryCodeMap.put("Switzerland", "CH");
    	countryCodeMap.put("Cote D'Ivoire", "CI");
    	countryCodeMap.put("Cook Islands", "CK");
    	countryCodeMap.put("Chile", "CL");
    	countryCodeMap.put("Cameroon", "CM");
    	countryCodeMap.put("China", "CN");
    	countryCodeMap.put("Colombia", "CO");
    	countryCodeMap.put("US Commercial", "COM");
    	countryCodeMap.put("Costa Rica", "CR");
    	countryCodeMap.put("Czechoslovakia", "CS");
    	countryCodeMap.put("Cuba", "CU");
    	countryCodeMap.put("Cape Verde", "CV");
    	countryCodeMap.put("Christmas Island", "CX");
    	countryCodeMap.put("Cyprus", "CY");
    	countryCodeMap.put("Czech Republic", "CZ");
    	countryCodeMap.put("Germany", "DE");
    	countryCodeMap.put("Djibouti", "DJ");
    	countryCodeMap.put("Denmark", "DK");
    	countryCodeMap.put("Dominica", "DM");
    	countryCodeMap.put("Dominican Republic", "DO");
    	countryCodeMap.put("Algeria", "DZ");
    	countryCodeMap.put("Ecuador", "EC");
    	countryCodeMap.put("US Educational", "EDU");
    	countryCodeMap.put("Estonia", "EE");
    	countryCodeMap.put("Egypt", "EG");
    	countryCodeMap.put("Western Sahara", "EH");
    	countryCodeMap.put("Eritrea", "ER");
    	countryCodeMap.put("Spain", "SP");
    	countryCodeMap.put("Ethiopia", "ET");
    	countryCodeMap.put("Finland", "FI");
    	countryCodeMap.put("Fiji", "FJ");
    	countryCodeMap.put("Falkland Islands", "FK");
    	countryCodeMap.put("Micronesia", "FM");
    	countryCodeMap.put("Faroe Islands", "FO");
    	countryCodeMap.put("France", "FR");
    	countryCodeMap.put("France, Metropolitan", "FX");
    	countryCodeMap.put("Gabon", "GA");
    	countryCodeMap.put("Great Britain", "GB");
    	countryCodeMap.put("Grenada", "GD");
    	countryCodeMap.put("Georgia", "GE");
    	countryCodeMap.put("French Guiana", "GF");
    	countryCodeMap.put("Ghana", "GH");
    	countryCodeMap.put("Gibraltar", "GI");
    	countryCodeMap.put("Greenland", "GL");
    	countryCodeMap.put("Gambia", "GM");
    	countryCodeMap.put("Guinea", "GN");
    	countryCodeMap.put("US Government", "GOV");
    	countryCodeMap.put("Guadeloupe", "GP");
    	countryCodeMap.put("Guadeloupe", "GO");
    	countryCodeMap.put("Greece", "GR");
    	countryCodeMap.put("S. Georgia and S. Sandwich Isls.", "GS");
    	countryCodeMap.put("Guatemala", "GT");
    	countryCodeMap.put("Guam", "GU");
    	countryCodeMap.put("Guinea-Bissau", "GW");
    	countryCodeMap.put("Guyana", "GN");
    	countryCodeMap.put("Hong Kong", "HK");
    	countryCodeMap.put("Heard and McDonald Islands", "HM");
    	countryCodeMap.put("Honduras", "HN");
    	countryCodeMap.put("Croatia", "HR");
    	countryCodeMap.put("Haiti", "HT");
    	countryCodeMap.put("Hungary", "HU");
    	countryCodeMap.put("Indonesia", "ID");
    	countryCodeMap.put("Ireland", "IE");
    	countryCodeMap.put("Israel", "IL");
    	countryCodeMap.put("India", "IN");
    	countryCodeMap.put("International", "INT");
    	countryCodeMap.put("British Indian Ocean Territory", "IO");
    	countryCodeMap.put("Iraq", "IQ");
    	countryCodeMap.put("Iran", "IR");
    	countryCodeMap.put("Iceland", "IS");
    	countryCodeMap.put("Italy", "IT");
    	countryCodeMap.put("Jamaica", "JM");
    	countryCodeMap.put("Jordan", "JO");
    	countryCodeMap.put("Japan", "JP");
    	countryCodeMap.put("Kenya", "KE");
    	countryCodeMap.put("Kyrgyzstan", "KG");
    	countryCodeMap.put("Cambodia", "KH");
    	countryCodeMap.put("Kiribati", "KI");
    	countryCodeMap.put("Comoros", "KM");
    	countryCodeMap.put("Saint Kitts and Nevis", "KN");
    	countryCodeMap.put("Korea (North)", "KP");
    	countryCodeMap.put("Korea (South)", "KR");
    	countryCodeMap.put("Kuwait", "KW");
    	countryCodeMap.put("Cayman Islands", "KY");
    	countryCodeMap.put("Kazakhstan", "KZ");
    	countryCodeMap.put("Laos", "JO");
    	countryCodeMap.put("Lebanon", "JO");
    	countryCodeMap.put("Saint Lucia", "JO");
    	countryCodeMap.put("Liechtenstein", "LI");
    	countryCodeMap.put("Sri Lanka", "LK");
    	countryCodeMap.put("Liberia", "LR");
    	countryCodeMap.put("Lesotho", "LS");
    	countryCodeMap.put("Lithuania", "LI");
    	countryCodeMap.put("Luxembourg", "LU");
    	countryCodeMap.put("Latvia", "LV");
    	countryCodeMap.put("Libya", "LY");
    	countryCodeMap.put("Morocco", "MA");
    	countryCodeMap.put("Monaco", "MC");
    	countryCodeMap.put("Moldova", "MD");
    	countryCodeMap.put("Madagascar", "MG");
    	countryCodeMap.put("Marshall Islands", "MH");
    	countryCodeMap.put("US Military", "MIL");
    	countryCodeMap.put("Macedonia", "MK");
    	countryCodeMap.put("Mali", "ML");
    	countryCodeMap.put("Myanmar", "MM");
    	countryCodeMap.put("Mongolia", "MN");
    	countryCodeMap.put("Macau", "MO");
    	countryCodeMap.put("Northern Mariana Islands", "MP");
    	countryCodeMap.put("Martinique", "MQ");
    	countryCodeMap.put("Mauritania", "MR");
    	countryCodeMap.put("Montserrat", "MS");
    	countryCodeMap.put("Malta", "MT");
    	countryCodeMap.put("Mauritius", "MU");
    	countryCodeMap.put("Maldives", "MV");
    	countryCodeMap.put("Malawi", "MW");
    	countryCodeMap.put("Mexico", "MX");
    	countryCodeMap.put("Malaysia", "MY");
    	countryCodeMap.put("Mozambique", "MZ");
    	countryCodeMap.put("Namibia", "MT");
    	countryCodeMap.put("Nato field", "MT");
    	countryCodeMap.put("New Caledonia", "MT");
    	countryCodeMap.put("Niger", "NG");
    	countryCodeMap.put("Norfolk Island", "NL");
    	countryCodeMap.put("Nigeria", "NO");
    	countryCodeMap.put("Nicaragua", "NI");
    	countryCodeMap.put("Netherlands", "NL");
    	countryCodeMap.put("Norway", "NO");
    	countryCodeMap.put("Nepal", "NP");
    	countryCodeMap.put("Nauru", "NR");
    	countryCodeMap.put("Neutral Zone", "NT");
    	countryCodeMap.put("Niue", "NU");
    	countryCodeMap.put("New Zealand", "NZ");
    	countryCodeMap.put("Oman", "OM");
    	countryCodeMap.put("Non-Profit Organization", "ORG");
    	countryCodeMap.put("Panama", "PA");
    	countryCodeMap.put("Peru", "PE");
    	countryCodeMap.put("French Polynesia", "PF");
    	countryCodeMap.put("Papua New Guinea", "PG");
    	countryCodeMap.put("Philippines", "PH");
    	countryCodeMap.put("Pakistan", "PK");
    	countryCodeMap.put("Poland", "PL");
    	countryCodeMap.put("St. Pierre and Miquelon", "PM");
    	countryCodeMap.put("Pitcairn", "PN");
    	countryCodeMap.put("Pitcairn", "PN");
    	countryCodeMap.put("Puerto Rico", "PR");
    	countryCodeMap.put("Portugal", "PT");
    	countryCodeMap.put("Palau", "PW");
    	countryCodeMap.put("Paraguay", "PY");
    	countryCodeMap.put("Qatar", "QA");
    	countryCodeMap.put("Reunion", "RE");
    	countryCodeMap.put("Romania", "RO");
    	countryCodeMap.put("Russian Federation", "RU");
    	countryCodeMap.put("Rwanda", "RW");
    	countryCodeMap.put("Saudi Arabia", "SA");
    	countryCodeMap.put("Seychelles", "SC");
    	countryCodeMap.put("Solomon Islands", "SB");
    	countryCodeMap.put("Sudan", "SD");
    	countryCodeMap.put("Sweden", "SE");
    	countryCodeMap.put("Singapore", "SG");
    	countryCodeMap.put("St. Helena", "SH");
    	countryCodeMap.put("Slovenia", "NI");
    	countryCodeMap.put("Svalbard and Jan Mayen Islands", "SJ");
    	countryCodeMap.put("Slovak Republic", "SK");
    	countryCodeMap.put("Sierra Leone", "SL");
    	countryCodeMap.put("San Marino", "SM");
    	countryCodeMap.put("Senegal", "SN");
    	countryCodeMap.put("Somalia", "SO");
    	countryCodeMap.put("Suriname", "SR");
    	countryCodeMap.put("Sao Tome and Principe", "ST");
    	countryCodeMap.put("USSR", "SU");
    	countryCodeMap.put("El Salvador", "SV");
    	countryCodeMap.put("Syria", "SY");
    	countryCodeMap.put("Swaziland", "SZ");
    	countryCodeMap.put("Suriname", "SR");
    	countryCodeMap.put("Turks and Caicos Islands", "TC");
    	countryCodeMap.put("Chad", "TD");
    	countryCodeMap.put("French Southern Territories", "TF");
    	countryCodeMap.put("Togo", "TG");
    	countryCodeMap.put("Thailand", "TH");
    	countryCodeMap.put("Thailand", "TJ");
    	countryCodeMap.put("Tajikistan", "SR");
    	countryCodeMap.put("Tokelau", "TK");
    	countryCodeMap.put("Turkmenistan", "Turkmenistan");
    	countryCodeMap.put("Tunisia", "TN");
    	countryCodeMap.put("Tonga", "TO");
    	countryCodeMap.put("East Timor", "TP");
    	countryCodeMap.put("Turkey", "TR");
    	countryCodeMap.put("Trinidad and Tobago", "TT");
    	countryCodeMap.put("Tuvalu", "TV");
    	countryCodeMap.put("Taiwan", "TW");
    	countryCodeMap.put("Tanzania", "TZ");
    	countryCodeMap.put("Thailand", "SR");
    	countryCodeMap.put("Ukraine", "UA");
    	countryCodeMap.put("Uganda", "UG");
    	countryCodeMap.put("United Kingdom", "UK");
    	countryCodeMap.put("US Minor Outlying Islands", "UM");
    	countryCodeMap.put("United States", "US");
    	countryCodeMap.put("Uruguay", "UY");
    	countryCodeMap.put("Uzbekistan", "UZ");
    	countryCodeMap.put("Vatican City State", "VA");
    	countryCodeMap.put("Saint Vincent and the Grenadines", "VC");
    	countryCodeMap.put("Venezuela", "VE");
    	countryCodeMap.put("Virgin Islands", "VG");
    	countryCodeMap.put("Virgin Islands (U.S.)", "VI");
    	countryCodeMap.put("Viet Nam", "VN");
    	countryCodeMap.put("Vanuatu", "VU");
    	countryCodeMap.put("Wallis and Futuna Islands", "WF");
    	countryCodeMap.put("Samoa", "WS");
    	countryCodeMap.put("Yemen", "YE");
    	countryCodeMap.put("Mayotte", "YT");
    	countryCodeMap.put("Yugoslavia", "YU");
    	countryCodeMap.put("South Africa", "ZA");
    	countryCodeMap.put("Zambia", "ZM");
    	countryCodeMap.put("Zaire", "ZR");
    	countryCodeMap.put("Zimbabwe", "ZW");
    	
    	for (Map.Entry<String, String> entry : countryCodeMap.entrySet())
    	{
    		countryMap.put(entry.getValue(), entry.getKey());
    	}
    	
    	return countryMap;
    }
	
	private void setLayout(HttpServletRequest request) {
		Layout layout = (Layout) request.getAttribute( "layout" );
		layout.setHeaderHtml( "" );
		layout.setTopBarHtml( "" );
		layout.setRightBarBottomHtml("");
		layout.setRightBarBottomHtmlLogin("");
		layout.setRightBarTopHtml("");
		layout.setRightBarTopHtmlLogin("");
		layout.setLeftBarTopHtml( "" );
		layout.setLeftBarBottomHtml( "" );
		layout.setHideLeftBar( true );
		layout.setFooterHtml( "" );
	}
}
