/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.register;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.AccessUser;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.EventMember;
import com.webjaguar.model.EventSearch;
import com.webjaguar.model.Layout;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.form.CustomerForm;
import com.webjaguar.web.form.EventForm;

public class SurveyRegisterWizardController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	private String type;
	public void setType(String type) { this.type = type; }
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	public SurveyRegisterWizardController() {
	}
	
    @SuppressWarnings("deprecation")
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
    	throws Exception {
    	
       	Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		if(ServletRequestUtils.getStringParameter(request,"language","es").equals("es")){
			if(ServletRequestUtils.getStringParameter(request,"type","new").equals("new")){
				setPages( new String[] {"frontend/register/survey/surveyEsNew"});
			}else{
				setPages( new String[] {"frontend/register/survey/surveyEsOld"});
			}
		}else{
			if(ServletRequestUtils.getStringParameter(request,"type","new").equals("new")){
				setPages( new String[] {"frontend/register/survey/surveyEnNew"});
			}else{
				setPages( new String[] {"frontend/register/survey/surveyEnOld"});
			}
		}
		
		map.put("model", myModel); 
		
		return map;
    }
    
    protected void validatePage(Object command, Errors errors, int page) {			
    }
    
	@SuppressWarnings("deprecation")
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );	
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		System.out.println("test");

		if(ServletRequestUtils.getStringParameter(request,"language","es").equals("es")){
			if(ServletRequestUtils.getStringParameter(request,"type","new").equals("new")){
				setPages( new String[] {"frontend/register/survey/surveyEsNew"});
			}else{
				setPages( new String[] {"frontend/register/survey/surveyEsOld"});
			}
		}else{
			if(ServletRequestUtils.getStringParameter(request,"type","new").equals("new")){
				setPages( new String[] {"frontend/register/survey/surveyEnNew"});
			}else{
				setPages( new String[] {"frontend/register/survey/surveyEnOld"});
			}
		}
		
		EventForm eventForm = new EventForm();
		return eventForm;
	}
	
	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		EventForm eventForm = (EventForm) command;

		switch (page) {
			case 0:
				break;
		}		
	}

	@Override
	protected ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		return null;
	}
}
