/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.register;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.NumberUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.webjaguar.listener.SessionListener;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Cart;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CountryCode;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UserSession;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactField;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.form.CustomerForm;
import com.webjaguar.web.validator.CustomerFormValidator;

public class RegisterWizardController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;

	private JavaMailSenderImpl mailSender;

	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}

	private Properties site;

	public void setSite(Properties site) {
		this.site = site;
	}

	public RegisterWizardController() {
		setCommandName("customerForm");
		setCommandClass(CustomerForm.class);
	}

	public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
		System.out.println("processFinish");
		String lang ="";
		if (request != null) {
			 lang = RequestContextUtils.getLocale(request).getLanguage();
		}
		System.out.println("lang"+lang);
		if (lang.equals("en")) {
			return new ModelAndView(new RedirectView("https://www.viatrading.com/reg/index.php?language=en"));
		} else {
			
		
				

		SalesRep salesRep = null;

		CustomerForm customerForm = (CustomerForm) command;
		customerForm.getCustomer().setUnsubscribe(!customerForm.isSubscribeEmail());
		
		String parameterTrackcode = (String)request.getSession().getAttribute("parameterTrackcode");
		String crmTrackcode = (String)request.getSession().getAttribute("crmTrackcode");
		Integer crmid = null;
				
		CrmContact crmContact = null;
		try{
			crmid = (Integer)request.getSession().getAttribute("crmid");
			crmContact = this.webJaguar.getCrmContactById(crmid);
		}catch(Exception ex){
		}
		
		if(customerForm.getCustomer()!=null && crmTrackcode!=null && crmTrackcode.length()>0 && parameterTrackcode!=null && parameterTrackcode.length()>0){
			String finalTrackcode = crmTrackcode + ">>" + parameterTrackcode;
			crmContact.setTrackcode(finalTrackcode);
			this.webJaguar.updateCrmContact(crmContact);
			customerForm.getCustomer().setTrackcode(finalTrackcode);
		}else{
			try{
				List<CrmContact> list = this.webJaguar.getCrmContactByEmail1(customerForm.getCustomer().getUsername());
				if(list!=null && list.size()>0){
					CrmContact tmp = list.get(0);
					String finalTrackcode = null;
					if(tmp!=null && tmp.getTrackcode()!=null && tmp.getTrackcode().length()>0 && parameterTrackcode!=null && parameterTrackcode.length()>0){
						finalTrackcode = tmp.getTrackcode() + ">>" + parameterTrackcode;
						tmp.setTrackcode(finalTrackcode);
						this.webJaguar.updateCrmContact(tmp);
					}else{
						finalTrackcode = parameterTrackcode;
					}
					customerForm.getCustomer().setTrackcode(finalTrackcode);
				}else{
					customerForm.getCustomer().setTrackcode((String) request.getAttribute("trackcode"));
				}
			}catch(Exception ex){
				customerForm.getCustomer().setTrackcode((String) request.getAttribute("trackcode"));
			}
		}
		
		customerForm.getCustomer().setRegisteredBy("frontend");
		// force the Address type and don't Ask Customer
		if (siteConfig.get("ADDRESS_RESIDENTIAL_COMMERCIAL").getValue().equals("0")) { // commercial
			customerForm.getCustomer().getAddress().setResidential(false);
		} else if (siteConfig.get("ADDRESS_RESIDENTIAL_COMMERCIAL").getValue().equals("1")) { // residential
			customerForm.getCustomer().getAddress().setResidential(true);
		}
		// category ids
		if (customerForm.getCategoryIds() != null) {
			Set<Object> categories = new HashSet<Object>();
			for (Object id : customerForm.getCategoryIds()) {
				if (!"".equals(id)) {
					categories.add(new Integer(id.toString()));
				}
			}
			customerForm.getCustomer().setCatIds(categories);
		}

		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);
			customerForm.getCustomer().setHost(request.getHeader("host"));
		}

		if ((Integer) gSiteConfig.get("gAFFILIATE") > 0) {
			String promocodeLink = (String) request.getAttribute("promocode");
			if (promocodeLink != null && !promocodeLink.equals("")) {
				Integer ownerId = this.webJaguar.getUserIdByPromoCode(promocodeLink);
				if (ownerId != null) {
					customerForm.getCustomer().setAffiliateParent(ownerId);
				} else
					customerForm.getCustomer().setAffiliateParent(0);
			} else {
				customerForm.getCustomer().setAffiliateParent(0);
			}

		} else
			customerForm.getCustomer().setAffiliateParent(0); // by default new customer connect to root.
		try {
			if (siteConfig.get("CUSTOMER_IP_ADDRESS").getValue().equals("true")) {
				customerForm.getCustomer().setNote(
						"--START-- \nOn " + new Date() + " \n\"" + customerForm.getCustomer().getAddress().getFirstName() + " " + customerForm.getCustomer().getAddress().getLastName()
								+ "\" was trying to register with this IP ADDRESS: " + request.getRemoteAddr() + "\n--END--\n\n");
			}
			customerForm.getCustomer().setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_NEW_REGISTRANT").getValue());
			customerForm.getCustomer().setPriceTable(Integer.parseInt(siteConfig.get("NEW_REGISTRANT_PRICE_PROTECTED_ACCESS").getValue()));
			if (customerForm.getCustomer().getTrackcode() != null && siteConfig.get("TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT").getValue().trim().length() > 0) {
				if (!siteConfig.get("TRACK_CODE").getValue().equals("")
						&& siteConfig.get("TRACK_CODE").getValue().trim().toLowerCase().equals(customerForm.getCustomer().getTrackcode().trim().toLowerCase())) {
					customerForm.getCustomer().setProtectedAccess(siteConfig.get("TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT").getValue());
				}
			}
			if ((Boolean) gSiteConfig.get("gSALES_REP")) {
				/*
				 * If territory zipcode module is on and customer belongs to a zipcode, that is assigned to a salesrep, that salesrep will be assigned to a customer first.
				 * 
				 * If territory zipcode module is not on or customer zipcode does not match with any of the salesrep than look for the next available sales rep in the cycle.
				 */
				if (siteConfig.get("TERRITORY_ZIPCODE").getValue().equals("true")) {
					salesRep = this.webJaguar.getSalesRepByTerritoryZipcode(customerForm.getCustomer().getAddress().getZip());
				}

				if (salesRep == null && siteConfig.get("SALES_REP_SEQUENTIAL_ASSIGNEMENT").getValue().equals("true")) {
					salesRep = this.webJaguar.getNextSalesRepInQueue();
				}

				if (salesRep != null) {
					// mark sales rep as assigned to customer in the current cycle
					this.webJaguar.markSalesRepAssignedToCustomerInCycle(salesRep.getId(), true);
				}
				// assign customer to a sales rep
				customerForm.getCustomer().setSalesRepId((salesRep == null) ? null : salesRep.getId());
			}
			// generate random cardID
			generateRandomCardID(customerForm.getCustomer());

			// SearchEngine Prospect
			if ((Boolean) gSiteConfig.get("gSEARCH_ENGINE_PROSPECT")) {
				String cookieName = site.getProperty("track.prospect.cookie.name");
				Cookie prospectCookie = WebUtils.getCookie(request, cookieName);
				if (prospectCookie != null) {
					customerForm.getCustomer().setProspectId(prospectCookie.getValue());
					logger.info("Prospect Id = " + prospectCookie.getValue());
					// delete Cookie after order placed.
					prospectCookie.setMaxAge(0);
					prospectCookie.setPath("/");
					prospectCookie.setValue("");
					response.addCookie(prospectCookie);
				}
			}
			if (siteConfig.get("CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER").getValue().equals("true")) {
				customerForm.getCustomer().setAutoGenerateAccountNumber(true);
			} else {
				customerForm.getCustomer().setAutoGenerateAccountNumber(false);
			}	
			String languageCode = customerForm.getCustomer().getLanguageCode().toString();
			if("es".equalsIgnoreCase(languageCode)){
				customerForm.getCustomer().setLanguage("es");
			}else if("fr".equalsIgnoreCase(languageCode)){
				customerForm.getCustomer().setLanguage("fr");
			}else{
				customerForm.getCustomer().setLanguage("en");
			}
			this.webJaguar.insertCustomer(customerForm.getCustomer(), customerForm.getShipping(), (Boolean) gSiteConfig.get("gCRM"));
		} catch (DataIntegrityViolationException ex) {
			ex.printStackTrace();
			errors.rejectValue("customer.username", "USER_ID_ALREADY_EXISTS", "An account using this email address already exists.");
			return showForm(request, response, errors);
		}

		SiteMessage siteMessage = null;
		try {
			Integer messageId = null;
			Integer newRegistrationId;
			if (multiStore != null) {
				messageId = multiStore.getMidNewRegistration();
			} else {
				String languageCode = customerForm.getCustomer().getLanguageCode().toString();
				try {
					newRegistrationId = this.webJaguar.getLanguageSetting(languageCode).getNewRegistrationId();
				} catch (Exception e) {
					newRegistrationId = null;
				}
				if (languageCode.equalsIgnoreCase((LanguageCode.en).toString()) || newRegistrationId == null) {
					messageId = Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_NEW_REGISTRATION").getValue());
				} else {
					messageId = Integer.parseInt(newRegistrationId.toString());
				}
			}
			siteMessage = this.webJaguar.getSiteMessageById(messageId);
			if (siteMessage != null) {
				// send email
				sendEmail(siteMessage, request, customerForm.getCustomer(), multiStore, salesRep);
			}
		} catch (NumberFormatException e) {
			// do nothing
			e.printStackTrace();
		}

		UserSession userSession = new UserSession();
		userSession.setUsername(customerForm.getCustomer().getUsername());
		userSession.setFirstName(customerForm.getCustomer().getAddress().getFirstName());
		userSession.setLastName(customerForm.getCustomer().getAddress().getLastName());
		userSession.setUserid(customerForm.getCustomer().getId());
		this.webJaguar.setUserSession(request, userSession);

		SessionListener.getActiveSessions().put(request.getSession().getId(), userSession.getUserid());

		Cart cart = (Cart) request.getSession().getAttribute("sessionCart");
		if (cart != null) {
			this.webJaguar.mergeCart(cart, userSession.getUserid());
			request.getSession().setAttribute("userCart", cart);
			request.getSession().removeAttribute("sessionCart");
		}
		if (!customerForm.getForwardAction().equals("")) {
			response.sendRedirect(customerForm.getForwardAction());
			return null;
		} else {
			String view = siteConfig.get("REGISTRATION_SUCCESS_URL").getValue().trim();
			if (multiStore != null && multiStore.getRegistrationSuccessUrl() != null) {
				view = multiStore.getRegistrationSuccessUrl().trim();
			}
			return new ModelAndView(new RedirectView(view));
		}
	  }
	}
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception{
		System.out.println("handleRequest");
		String lang ="";
		if (request != null) {
			 lang = RequestContextUtils.getLocale(request).getLanguage();
		}
		System.out.println("lang"+lang);
		if (lang.equals("es")) {
			return new ModelAndView(new RedirectView("https://www.viatrading.com/reg/index.php?language=es"));
		} else {
		return new ModelAndView(new RedirectView("https://www.viatrading.com/reg/index.php?language=en"));
		}
		
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {

		CustomerForm customerForm = (CustomerForm) command;

		Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();

		switch (page) {
		case 0:
			// get a list of enabled country code from database
			myModel.put("countrylist", this.webJaguar.getEnabledCountryList());
			myModel.put("statelist", this.webJaguar.getStateList("US"));
			myModel.put("caProvinceList", this.webJaguar.getStateList("CA"));
			myModel.put("createAccount", this.webJaguar.getSystemLayout("createAccount", request.getHeader("host"), request));
			myModel.put("languageCodes", this.webJaguar.getLanguageSettings());
			myModel.put("mobileCarrierList", this.webJaguar.getMobileCarrierList());
			break;
		case 1:
			if (customerForm.getParentIds() != null) {
				Map<Integer, Object> categories = new HashMap<Integer, Object>();
				Map<Integer, Object> tree = new HashMap<Integer, Object>();
				for (int i = 0; i < customerForm.getParentIds().length; i++) {
					categories.put(i, this.webJaguar.getCategoryLinks(new Integer(customerForm.getParentIds()[i].toString()), request));
					myModel.put("categories", categories);
					tree.put(i, this.webJaguar.getCategoryTree(new Integer(customerForm.getParentIds()[i].toString()), true, null));
					myModel.put("tree", tree);
				}
			}
			// just to carry over the HeadTag, We don't show Header and footer of CreateAccount
			myModel.put("createAccount", this.webJaguar.getSystemLayout("createAccount", request.getHeader("host"), request));
			break;
		case 2:
			myModel.put("verifyAccount", this.webJaguar.getSystemLayout("verifyAccount", request.getHeader("host"), request));
		}

		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		map.put("countries", this.webJaguar.getCountryMap());
		map.put("countryCodeMap", getCountryCodeMap());
		myModel.put("countryCodeList", getCountryCodeList());
		
		// i18n
		String lang = null;
		if (request != null) {
			lang = RequestContextUtils.getLocale(request).getLanguage();
		}
		
		if(customerForm!=null && customerForm.getCustomer()!=null && customerForm.getCustomer().getAddress()!=null && customerForm.getCustomer().getAddress().getCountry()!=null){
			myModel.put("countrySelected", getCountryCodeIndex(customerForm.getCustomer().getAddress().getCountry()));	
		}
		
		myModel.put("customFieldValue", getCountryCodeIndex(customerForm.getCustomer().getAddress().getCountry()));	
		
		
		myModel.put("lang", lang);	
		map.put("model", myModel);

		Layout layout = (Layout) request.getAttribute("layout");
		layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "Register"));
		layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#PAGENAME#", "Register"));
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		}

		return map;
	 }
	
	
	 private Map<String,String> getCountryCodeMap(){
	    	
	    	Map<String,String> countryCodeMap = new HashMap<String,String>();
	    	Map<String,String> countryMap = new HashMap<String,String>();
	    	
	    	countryCodeMap.put("Andorra", "AD");
	    	countryCodeMap.put("United Arab Emirates", "AE");
	    	countryCodeMap.put("Afghanistan", "AF");
	    	countryCodeMap.put("Antigua and Barbuda", "AG");
	    	countryCodeMap.put("Anguilla", "AI");
	    	countryCodeMap.put("Albania", "AL");
	    	countryCodeMap.put("Armenia", "AM");
	    	countryCodeMap.put("Netherlands Antilles", "AN");
	    	countryCodeMap.put("Angola", "AO");
	    	countryCodeMap.put("Antarctica", "AQ");
	    	countryCodeMap.put("Argentina", "AR");
	    	countryCodeMap.put("American Samoa", "AS");
	    	countryCodeMap.put("Austria", "AT");
	    	countryCodeMap.put("Australia", "AU");
	    	countryCodeMap.put("Aruba", "AW");
	    	countryCodeMap.put("Azerbaijan", "AZ");
	    	countryCodeMap.put("Bosnia and Herzegovina", "BA");
	    	countryCodeMap.put("Barbados", "BB");
	    	countryCodeMap.put("Bangladesh", "BD");
	    	countryCodeMap.put("Belgium", "BE");
	    	countryCodeMap.put("Burkina Faso", "BF");
	    	countryCodeMap.put("Bulgaria", "BG");
	    	countryCodeMap.put("Bahrain", "BH");
	    	countryCodeMap.put("Burundi", "BI");
	    	countryCodeMap.put("Benin", "BJ");
	    	countryCodeMap.put("Bermuda", "BM");
	    	countryCodeMap.put("Brunei Darussalam", "BN");
	    	countryCodeMap.put("Bolivia", "BO");
	    	countryCodeMap.put("Brazil", "BR");
	    	countryCodeMap.put("Bahamas", "BS");
	    	countryCodeMap.put("Bhutan", "BT");
	    	countryCodeMap.put("Bouvet Island", "BV");
	    	countryCodeMap.put("Botswana", "BW");
	    	countryCodeMap.put("Belarus", "BY");
	    	countryCodeMap.put("Belize", "BZ");
	    	countryCodeMap.put("Canada", "CA");
	    	countryCodeMap.put("Cocos Islands", "CC");
	    	countryCodeMap.put("Central African Republic", "CF");
	    	countryCodeMap.put("Congo", "CG");
	    	countryCodeMap.put("Switzerland", "CH");
	    	countryCodeMap.put("Cote D'Ivoire", "CI");
	    	countryCodeMap.put("Cook Islands", "CK");
	    	countryCodeMap.put("Chile", "CL");
	    	countryCodeMap.put("Cameroon", "CM");
	    	countryCodeMap.put("China", "CN");
	    	countryCodeMap.put("Colombia", "CO");
	    	countryCodeMap.put("US Commercial", "COM");
	    	countryCodeMap.put("Costa Rica", "CR");
	    	countryCodeMap.put("Czechoslovakia", "CS");
	    	countryCodeMap.put("Cuba", "CU");
	    	countryCodeMap.put("Cape Verde", "CV");
	    	countryCodeMap.put("Christmas Island", "CX");
	    	countryCodeMap.put("Cyprus", "CY");
	    	countryCodeMap.put("Czech Republic", "CZ");
	    	countryCodeMap.put("Germany", "DE");
	    	countryCodeMap.put("Djibouti", "DJ");
	    	countryCodeMap.put("Denmark", "DK");
	    	countryCodeMap.put("Dominica", "DM");
	    	countryCodeMap.put("Dominican Republic", "DO");
	    	countryCodeMap.put("Algeria", "DZ");
	    	countryCodeMap.put("Ecuador", "EC");
	    	countryCodeMap.put("US Educational", "EDU");
	    	countryCodeMap.put("Estonia", "EE");
	    	countryCodeMap.put("Egypt", "EG");
	    	countryCodeMap.put("Western Sahara", "EH");
	    	countryCodeMap.put("Eritrea", "ER");
	    	countryCodeMap.put("Spain", "SP");
	    	countryCodeMap.put("Ethiopia", "ET");
	    	countryCodeMap.put("Finland", "FI");
	    	countryCodeMap.put("Fiji", "FJ");
	    	countryCodeMap.put("Falkland Islands", "FK");
	    	countryCodeMap.put("Micronesia", "FM");
	    	countryCodeMap.put("Faroe Islands", "FO");
	    	countryCodeMap.put("France", "FR");
	    	countryCodeMap.put("France, Metropolitan", "FX");
	    	countryCodeMap.put("Gabon", "GA");
	    	countryCodeMap.put("Great Britain", "GB");
	    	countryCodeMap.put("Grenada", "GD");
	    	countryCodeMap.put("Georgia", "GE");
	    	countryCodeMap.put("French Guiana", "GF");
	    	countryCodeMap.put("Ghana", "GH");
	    	countryCodeMap.put("Gibraltar", "GI");
	    	countryCodeMap.put("Greenland", "GL");
	    	countryCodeMap.put("Gambia", "GM");
	    	countryCodeMap.put("Guinea", "GN");
	    	countryCodeMap.put("US Government", "GOV");
	    	countryCodeMap.put("Guadeloupe", "GP");
	    	countryCodeMap.put("Guadeloupe", "GO");
	    	countryCodeMap.put("Greece", "GR");
	    	countryCodeMap.put("S. Georgia and S. Sandwich Isls.", "GS");
	    	countryCodeMap.put("Guatemala", "GT");
	    	countryCodeMap.put("Guam", "GU");
	    	countryCodeMap.put("Guinea-Bissau", "GW");
	    	countryCodeMap.put("Guyana", "GN");
	    	countryCodeMap.put("Hong Kong", "HK");
	    	countryCodeMap.put("Heard and McDonald Islands", "HM");
	    	countryCodeMap.put("Honduras", "HN");
	    	countryCodeMap.put("Croatia", "HR");
	    	countryCodeMap.put("Haiti", "HT");
	    	countryCodeMap.put("Hungary", "HU");
	    	countryCodeMap.put("Indonesia", "ID");
	    	countryCodeMap.put("Ireland", "IE");
	    	countryCodeMap.put("Israel", "IL");
	    	countryCodeMap.put("India", "IN");
	    	countryCodeMap.put("International", "INT");
	    	countryCodeMap.put("British Indian Ocean Territory", "IO");
	    	countryCodeMap.put("Iraq", "IQ");
	    	countryCodeMap.put("Iran", "IR");
	    	countryCodeMap.put("Iceland", "IS");
	    	countryCodeMap.put("Italy", "IT");
	    	countryCodeMap.put("Jamaica", "JM");
	    	countryCodeMap.put("Jordan", "JO");
	    	countryCodeMap.put("Japan", "JP");
	    	countryCodeMap.put("Kenya", "KE");
	    	countryCodeMap.put("Kyrgyzstan", "KG");
	    	countryCodeMap.put("Cambodia", "KH");
	    	countryCodeMap.put("Kiribati", "KI");
	    	countryCodeMap.put("Comoros", "KM");
	    	countryCodeMap.put("Saint Kitts and Nevis", "KN");
	    	countryCodeMap.put("Korea (North)", "KP");
	    	countryCodeMap.put("Korea (South)", "KR");
	    	countryCodeMap.put("Kuwait", "KW");
	    	countryCodeMap.put("Cayman Islands", "KY");
	    	countryCodeMap.put("Kazakhstan", "KZ");
	    	countryCodeMap.put("Laos", "JO");
	    	countryCodeMap.put("Lebanon", "JO");
	    	countryCodeMap.put("Saint Lucia", "JO");
	    	countryCodeMap.put("Liechtenstein", "LI");
	    	countryCodeMap.put("Sri Lanka", "LK");
	    	countryCodeMap.put("Liberia", "LR");
	    	countryCodeMap.put("Lesotho", "LS");
	    	countryCodeMap.put("Lithuania", "LI");
	    	countryCodeMap.put("Luxembourg", "LU");
	    	countryCodeMap.put("Latvia", "LV");
	    	countryCodeMap.put("Libya", "LY");
	    	countryCodeMap.put("Morocco", "MA");
	    	countryCodeMap.put("Monaco", "MC");
	    	countryCodeMap.put("Moldova", "MD");
	    	countryCodeMap.put("Madagascar", "MG");
	    	countryCodeMap.put("Marshall Islands", "MH");
	    	countryCodeMap.put("US Military", "MIL");
	    	countryCodeMap.put("Macedonia", "MK");
	    	countryCodeMap.put("Mali", "ML");
	    	countryCodeMap.put("Myanmar", "MM");
	    	countryCodeMap.put("Mongolia", "MN");
	    	countryCodeMap.put("Macau", "MO");
	    	countryCodeMap.put("Northern Mariana Islands", "MP");
	    	countryCodeMap.put("Martinique", "MQ");
	    	countryCodeMap.put("Mauritania", "MR");
	    	countryCodeMap.put("Montserrat", "MS");
	    	countryCodeMap.put("Malta", "MT");
	    	countryCodeMap.put("Mauritius", "MU");
	    	countryCodeMap.put("Maldives", "MV");
	    	countryCodeMap.put("Malawi", "MW");
	    	countryCodeMap.put("Mexico", "MX");
	    	countryCodeMap.put("Malaysia", "MY");
	    	countryCodeMap.put("Mozambique", "MZ");
	    	countryCodeMap.put("Namibia", "MT");
	    	countryCodeMap.put("Nato field", "MT");
	    	countryCodeMap.put("New Caledonia", "MT");
	    	countryCodeMap.put("Niger", "NG");
	    	countryCodeMap.put("Norfolk Island", "NL");
	    	countryCodeMap.put("Nigeria", "NO");
	    	countryCodeMap.put("Nicaragua", "NI");
	    	countryCodeMap.put("Netherlands", "NL");
	    	countryCodeMap.put("Norway", "NO");
	    	countryCodeMap.put("Nepal", "NP");
	    	countryCodeMap.put("Nauru", "NR");
	    	countryCodeMap.put("Neutral Zone", "NT");
	    	countryCodeMap.put("Niue", "NU");
	    	countryCodeMap.put("New Zealand", "NZ");
	    	countryCodeMap.put("Oman", "OM");
	    	countryCodeMap.put("Non-Profit Organization", "ORG");
	    	countryCodeMap.put("Panama", "PA");
	    	countryCodeMap.put("Peru", "PE");
	    	countryCodeMap.put("French Polynesia", "PF");
	    	countryCodeMap.put("Papua New Guinea", "PG");
	    	countryCodeMap.put("Philippines", "PH");
	    	countryCodeMap.put("Pakistan", "PK");
	    	countryCodeMap.put("Poland", "PL");
	    	countryCodeMap.put("St. Pierre and Miquelon", "PM");
	    	countryCodeMap.put("Pitcairn", "PN");
	    	countryCodeMap.put("Pitcairn", "PN");
	    	countryCodeMap.put("Puerto Rico", "PR");
	    	countryCodeMap.put("Portugal", "PT");
	    	countryCodeMap.put("Palau", "PW");
	    	countryCodeMap.put("Paraguay", "PY");
	    	countryCodeMap.put("Qatar", "QA");
	    	countryCodeMap.put("Reunion", "RE");
	    	countryCodeMap.put("Romania", "RO");
	    	countryCodeMap.put("Russian Federation", "RU");
	    	countryCodeMap.put("Rwanda", "RW");
	    	countryCodeMap.put("Saudi Arabia", "SA");
	    	countryCodeMap.put("Seychelles", "SC");
	    	countryCodeMap.put("Solomon Islands", "SB");
	    	countryCodeMap.put("Sudan", "SD");
	    	countryCodeMap.put("Sweden", "SE");
	    	countryCodeMap.put("Singapore", "SG");
	    	countryCodeMap.put("St. Helena", "SH");
	    	countryCodeMap.put("Slovenia", "NI");
	    	countryCodeMap.put("Svalbard and Jan Mayen Islands", "SJ");
	    	countryCodeMap.put("Slovak Republic", "SK");
	    	countryCodeMap.put("Sierra Leone", "SL");
	    	countryCodeMap.put("San Marino", "SM");
	    	countryCodeMap.put("Senegal", "SN");
	    	countryCodeMap.put("Somalia", "SO");
	    	countryCodeMap.put("Suriname", "SR");
	    	countryCodeMap.put("Sao Tome and Principe", "ST");
	    	countryCodeMap.put("USSR", "SU");
	    	countryCodeMap.put("El Salvador", "SV");
	    	countryCodeMap.put("Syria", "SY");
	    	countryCodeMap.put("Swaziland", "SZ");
	    	countryCodeMap.put("Suriname", "SR");
	    	countryCodeMap.put("Turks and Caicos Islands", "TC");
	    	countryCodeMap.put("Chad", "TD");
	    	countryCodeMap.put("French Southern Territories", "TF");
	    	countryCodeMap.put("Togo", "TG");
	    	countryCodeMap.put("Thailand", "TH");
	    	countryCodeMap.put("Thailand", "TJ");
	    	countryCodeMap.put("Tajikistan", "SR");
	    	countryCodeMap.put("Tokelau", "TK");
	    	countryCodeMap.put("Turkmenistan", "Turkmenistan");
	    	countryCodeMap.put("Tunisia", "TN");
	    	countryCodeMap.put("Tonga", "TO");
	    	countryCodeMap.put("East Timor", "TP");
	    	countryCodeMap.put("Turkey", "TR");
	    	countryCodeMap.put("Trinidad and Tobago", "TT");
	    	countryCodeMap.put("Tuvalu", "TV");
	    	countryCodeMap.put("Taiwan", "TW");
	    	countryCodeMap.put("Tanzania", "TZ");
	    	countryCodeMap.put("Thailand", "SR");
	    	countryCodeMap.put("Ukraine", "UA");
	    	countryCodeMap.put("Uganda", "UG");
	    	countryCodeMap.put("United Kingdom", "UK");
	    	countryCodeMap.put("US Minor Outlying Islands", "UM");
	    	countryCodeMap.put("Uruguay", "UY");
	    	countryCodeMap.put("Uzbekistan", "UZ");
	    	countryCodeMap.put("Vatican City State", "VA");
	    	countryCodeMap.put("Saint Vincent and the Grenadines", "VC");
	    	countryCodeMap.put("Venezuela", "VE");
	    	countryCodeMap.put("Virgin Islands", "VG");
	    	countryCodeMap.put("Virgin Islands (U.S.)", "VI");
	    	countryCodeMap.put("Viet Nam", "VN");
	    	countryCodeMap.put("Vanuatu", "VU");
	    	countryCodeMap.put("Wallis and Futuna Islands", "WF");
	    	countryCodeMap.put("Samoa", "WS");
	    	countryCodeMap.put("Yemen", "YE");
	    	countryCodeMap.put("Mayotte", "YT");
	    	countryCodeMap.put("Yugoslavia", "YU");
	    	countryCodeMap.put("South Africa", "ZA");
	    	countryCodeMap.put("Zambia", "ZM");
	    	countryCodeMap.put("Zaire", "ZR");
	    	countryCodeMap.put("Zimbabwe", "ZW");


	    	for (Map.Entry<String, String> entry : countryCodeMap.entrySet())
	    	{
	    		countryMap.put(entry.getValue(), entry.getKey());
	    	}
	    	return countryCodeMap;
	 }
	 
	 private List<CountryCode> getCountryCodeList(){
	    	
	    	Map<String,String> countryCodeMap = new TreeMap<String,String>();
	    	List<CountryCode> list = new LinkedList<CountryCode>();
	    	
	    	countryCodeMap.put("Andorra", "AD");
	    	countryCodeMap.put("United Arab Emirates", "AE");
	    	countryCodeMap.put("Afghanistan", "AF");
	    	countryCodeMap.put("Antigua and Barbuda", "AG");
	    	countryCodeMap.put("Anguilla", "AI");
	    	countryCodeMap.put("Albania", "AL");
	    	countryCodeMap.put("Armenia", "AM");
	    	countryCodeMap.put("Netherlands Antilles", "AN");
	    	countryCodeMap.put("Angola", "AO");
	    	countryCodeMap.put("Antarctica", "AQ");
	    	countryCodeMap.put("Argentina", "AR");
	    	countryCodeMap.put("American Samoa", "AS");
	    	countryCodeMap.put("Austria", "AT");
	    	countryCodeMap.put("Australia", "AU");
	    	countryCodeMap.put("Aruba", "AW");
	    	countryCodeMap.put("Azerbaijan", "AZ");
	    	countryCodeMap.put("Bosnia and Herzegovina", "BA");
	    	countryCodeMap.put("Barbados", "BB");
	    	countryCodeMap.put("Bangladesh", "BD");
	    	countryCodeMap.put("Belgium", "BE");
	    	countryCodeMap.put("Burkina Faso", "BF");
	    	countryCodeMap.put("Bulgaria", "BG");
	    	countryCodeMap.put("Bahrain", "BH");
	    	countryCodeMap.put("Burundi", "BI");
	    	countryCodeMap.put("Benin", "BJ");
	    	countryCodeMap.put("Bermuda", "BM");
	    	countryCodeMap.put("Brunei Darussalam", "BN");
	    	countryCodeMap.put("Bolivia", "BO");
	    	countryCodeMap.put("Brazil", "BR");
	    	countryCodeMap.put("Bahamas", "BS");
	    	countryCodeMap.put("Bhutan", "BT");
	    	countryCodeMap.put("Bouvet Island", "BV");
	    	countryCodeMap.put("Botswana", "BW");
	    	countryCodeMap.put("Belarus", "BY");
	    	countryCodeMap.put("Belize", "BZ");
	    	countryCodeMap.put("Canada", "CA");
	    	countryCodeMap.put("Cocos Islands", "CC");
	    	countryCodeMap.put("Central African Republic", "CF");
	    	countryCodeMap.put("Congo", "CG");
	    	countryCodeMap.put("Switzerland", "CH");
	    	countryCodeMap.put("Cote D'Ivoire", "CI");
	    	countryCodeMap.put("Cook Islands", "CK");
	    	countryCodeMap.put("Chile", "CL");
	    	countryCodeMap.put("Cameroon", "CM");
	    	countryCodeMap.put("China", "CN");
	    	countryCodeMap.put("Colombia", "CO");
	    	countryCodeMap.put("US Commercial", "COM");
	    	countryCodeMap.put("Costa Rica", "CR");
	    	countryCodeMap.put("Czechoslovakia", "CS");
	    	countryCodeMap.put("Cuba", "CU");
	    	countryCodeMap.put("Cape Verde", "CV");
	    	countryCodeMap.put("Christmas Island", "CX");
	    	countryCodeMap.put("Cyprus", "CY");
	    	countryCodeMap.put("Czech Republic", "CZ");
	    	countryCodeMap.put("Germany", "DE");
	    	countryCodeMap.put("Djibouti", "DJ");
	    	countryCodeMap.put("Denmark", "DK");
	    	countryCodeMap.put("Dominica", "DM");
	    	countryCodeMap.put("Dominican Republic", "DO");
	    	countryCodeMap.put("Algeria", "DZ");
	    	countryCodeMap.put("Ecuador", "EC");
	    	countryCodeMap.put("US Educational", "EDU");
	    	countryCodeMap.put("Estonia", "EE");
	    	countryCodeMap.put("Egypt", "EG");
	    	countryCodeMap.put("Western Sahara", "EH");
	    	countryCodeMap.put("Eritrea", "ER");
	    	countryCodeMap.put("Spain", "SP");
	    	countryCodeMap.put("Ethiopia", "ET");
	    	countryCodeMap.put("Finland", "FI");
	    	countryCodeMap.put("Fiji", "FJ");
	    	countryCodeMap.put("Falkland Islands", "FK");
	    	countryCodeMap.put("Micronesia", "FM");
	    	countryCodeMap.put("Faroe Islands", "FO");
	    	countryCodeMap.put("France", "FR");
	    	countryCodeMap.put("France, Metropolitan", "FX");
	    	countryCodeMap.put("Gabon", "GA");
	    	countryCodeMap.put("Great Britain", "GB");
	    	countryCodeMap.put("Grenada", "GD");
	    	countryCodeMap.put("Georgia", "GE");
	    	countryCodeMap.put("French Guiana", "GF");
	    	countryCodeMap.put("Ghana", "GH");
	    	countryCodeMap.put("Gibraltar", "GI");
	    	countryCodeMap.put("Greenland", "GL");
	    	countryCodeMap.put("Gambia", "GM");
	    	countryCodeMap.put("Guinea", "GN");
	    	countryCodeMap.put("US Government", "GOV");
	    	countryCodeMap.put("Guadeloupe", "GP");
	    	countryCodeMap.put("Guadeloupe", "GO");
	    	countryCodeMap.put("Greece", "GR");
	    	countryCodeMap.put("S. Georgia and S. Sandwich Isls.", "GS");
	    	countryCodeMap.put("Guatemala", "GT");
	    	countryCodeMap.put("Guam", "GU");
	    	countryCodeMap.put("Guinea-Bissau", "GW");
	    	countryCodeMap.put("Guyana", "GN");
	    	countryCodeMap.put("Hong Kong", "HK");
	    	countryCodeMap.put("Heard and McDonald Islands", "HM");
	    	countryCodeMap.put("Honduras", "HN");
	    	countryCodeMap.put("Croatia", "HR");
	    	countryCodeMap.put("Haiti", "HT");
	    	countryCodeMap.put("Hungary", "HU");
	    	countryCodeMap.put("Indonesia", "ID");
	    	countryCodeMap.put("Ireland", "IE");
	    	countryCodeMap.put("Israel", "IL");
	    	countryCodeMap.put("India", "IN");
	    	countryCodeMap.put("International", "INT");
	    	countryCodeMap.put("British Indian Ocean Territory", "IO");
	    	countryCodeMap.put("Iraq", "IQ");
	    	countryCodeMap.put("Iran", "IR");
	    	countryCodeMap.put("Iceland", "IS");
	    	countryCodeMap.put("Italy", "IT");
	    	countryCodeMap.put("Jamaica", "JM");
	    	countryCodeMap.put("Jordan", "JO");
	    	countryCodeMap.put("Japan", "JP");
	    	countryCodeMap.put("Kenya", "KE");
	    	countryCodeMap.put("Kyrgyzstan", "KG");
	    	countryCodeMap.put("Cambodia", "KH");
	    	countryCodeMap.put("Kiribati", "KI");
	    	countryCodeMap.put("Comoros", "KM");
	    	countryCodeMap.put("Saint Kitts and Nevis", "KN");
	    	countryCodeMap.put("Korea (North)", "KP");
	    	countryCodeMap.put("Korea (South)", "KR");
	    	countryCodeMap.put("Kuwait", "KW");
	    	countryCodeMap.put("Cayman Islands", "KY");
	    	countryCodeMap.put("Kazakhstan", "KZ");
	    	countryCodeMap.put("Laos", "JO");
	    	countryCodeMap.put("Lebanon", "JO");
	    	countryCodeMap.put("Saint Lucia", "JO");
	    	countryCodeMap.put("Liechtenstein", "LI");
	    	countryCodeMap.put("Sri Lanka", "LK");
	    	countryCodeMap.put("Liberia", "LR");
	    	countryCodeMap.put("Lesotho", "LS");
	    	countryCodeMap.put("Lithuania", "LI");
	    	countryCodeMap.put("Luxembourg", "LU");
	    	countryCodeMap.put("Latvia", "LV");
	    	countryCodeMap.put("Libya", "LY");
	    	countryCodeMap.put("Morocco", "MA");
	    	countryCodeMap.put("Monaco", "MC");
	    	countryCodeMap.put("Moldova", "MD");
	    	countryCodeMap.put("Madagascar", "MG");
	    	countryCodeMap.put("Marshall Islands", "MH");
	    	countryCodeMap.put("US Military", "MIL");
	    	countryCodeMap.put("Macedonia", "MK");
	    	countryCodeMap.put("Mali", "ML");
	    	countryCodeMap.put("Myanmar", "MM");
	    	countryCodeMap.put("Mongolia", "MN");
	    	countryCodeMap.put("Macau", "MO");
	    	countryCodeMap.put("Northern Mariana Islands", "MP");
	    	countryCodeMap.put("Martinique", "MQ");
	    	countryCodeMap.put("Mauritania", "MR");
	    	countryCodeMap.put("Montserrat", "MS");
	    	countryCodeMap.put("Malta", "MT");
	    	countryCodeMap.put("Mauritius", "MU");
	    	countryCodeMap.put("Maldives", "MV");
	    	countryCodeMap.put("Malawi", "MW");
	    	countryCodeMap.put("Mexico", "MX");
	    	countryCodeMap.put("Malaysia", "MY");
	    	countryCodeMap.put("Mozambique", "MZ");
	    	countryCodeMap.put("Namibia", "MT");
	    	countryCodeMap.put("Nato field", "MT");
	    	countryCodeMap.put("New Caledonia", "MT");
	    	countryCodeMap.put("Niger", "NG");
	    	countryCodeMap.put("Norfolk Island", "NL");
	    	countryCodeMap.put("Nigeria", "NO");
	    	countryCodeMap.put("Nicaragua", "NI");
	    	countryCodeMap.put("Netherlands", "NL");
	    	countryCodeMap.put("Norway", "NO");
	    	countryCodeMap.put("Nepal", "NP");
	    	countryCodeMap.put("Nauru", "NR");
	    	countryCodeMap.put("Neutral Zone", "NT");
	    	countryCodeMap.put("Niue", "NU");
	    	countryCodeMap.put("New Zealand", "NZ");
	    	countryCodeMap.put("Oman", "OM");
	    	countryCodeMap.put("Non-Profit Organization", "ORG");
	    	countryCodeMap.put("Panama", "PA");
	    	countryCodeMap.put("Peru", "PE");
	    	countryCodeMap.put("French Polynesia", "PF");
	    	countryCodeMap.put("Papua New Guinea", "PG");
	    	countryCodeMap.put("Philippines", "PH");
	    	countryCodeMap.put("Pakistan", "PK");
	    	countryCodeMap.put("Poland", "PL");
	    	countryCodeMap.put("St. Pierre and Miquelon", "PM");
	    	countryCodeMap.put("Pitcairn", "PN");
	    	countryCodeMap.put("Pitcairn", "PN");
	    	countryCodeMap.put("Puerto Rico", "PR");
	    	countryCodeMap.put("Portugal", "PT");
	    	countryCodeMap.put("Palau", "PW");
	    	countryCodeMap.put("Paraguay", "PY");
	    	countryCodeMap.put("Qatar", "QA");
	    	countryCodeMap.put("Reunion", "RE");
	    	countryCodeMap.put("Romania", "RO");
	    	countryCodeMap.put("Russian Federation", "RU");
	    	countryCodeMap.put("Rwanda", "RW");
	    	countryCodeMap.put("Saudi Arabia", "SA");
	    	countryCodeMap.put("Seychelles", "SC");
	    	countryCodeMap.put("Solomon Islands", "SB");
	    	countryCodeMap.put("Sudan", "SD");
	    	countryCodeMap.put("Sweden", "SE");
	    	countryCodeMap.put("Singapore", "SG");
	    	countryCodeMap.put("St. Helena", "SH");
	    	countryCodeMap.put("Slovenia", "NI");
	    	countryCodeMap.put("Svalbard and Jan Mayen Islands", "SJ");
	    	countryCodeMap.put("Slovak Republic", "SK");
	    	countryCodeMap.put("Sierra Leone", "SL");
	    	countryCodeMap.put("San Marino", "SM");
	    	countryCodeMap.put("Senegal", "SN");
	    	countryCodeMap.put("Somalia", "SO");
	    	countryCodeMap.put("Suriname", "SR");
	    	countryCodeMap.put("Sao Tome and Principe", "ST");
	    	countryCodeMap.put("USSR", "SU");
	    	countryCodeMap.put("El Salvador", "SV");
	    	countryCodeMap.put("Syria", "SY");
	    	countryCodeMap.put("Swaziland", "SZ");
	    	countryCodeMap.put("Suriname", "SR");
	    	countryCodeMap.put("Turks and Caicos Islands", "TC");
	    	countryCodeMap.put("Chad", "TD");
	    	countryCodeMap.put("French Southern Territories", "TF");
	    	countryCodeMap.put("Togo", "TG");
	    	countryCodeMap.put("Thailand", "TH");
	    	countryCodeMap.put("Thailand", "TJ");
	    	countryCodeMap.put("Tajikistan", "SR");
	    	countryCodeMap.put("Tokelau", "TK");
	    	countryCodeMap.put("Turkmenistan", "Turkmenistan");
	    	countryCodeMap.put("Tunisia", "TN");
	    	countryCodeMap.put("Tonga", "TO");
	    	countryCodeMap.put("East Timor", "TP");
	    	countryCodeMap.put("Turkey", "TR");
	    	countryCodeMap.put("Trinidad and Tobago", "TT");
	    	countryCodeMap.put("Tuvalu", "TV");
	    	countryCodeMap.put("Taiwan", "TW");
	    	countryCodeMap.put("Tanzania", "TZ");
	    	countryCodeMap.put("Thailand", "SR");
	    	countryCodeMap.put("Ukraine", "UA");
	    	countryCodeMap.put("Uganda", "UG");
	    	countryCodeMap.put("United Kingdom", "UK");
	    	countryCodeMap.put("US Minor Outlying Islands", "UM");
	    	countryCodeMap.put("Uruguay", "UY");
	    	countryCodeMap.put("Uzbekistan", "UZ");
	    	countryCodeMap.put("Vatican City State", "VA");
	    	countryCodeMap.put("Saint Vincent and the Grenadines", "VC");
	    	countryCodeMap.put("Venezuela", "VE");
	    	countryCodeMap.put("Virgin Islands", "VG");
	    	countryCodeMap.put("Virgin Islands (U.S.)", "VI");
	    	countryCodeMap.put("Viet Nam", "VN");
	    	countryCodeMap.put("Vanuatu", "VU");
	    	countryCodeMap.put("Wallis and Futuna Islands", "WF");
	    	countryCodeMap.put("Samoa", "WS");
	    	countryCodeMap.put("Yemen", "YE");
	    	countryCodeMap.put("Mayotte", "YT");
	    	countryCodeMap.put("Yugoslavia", "YU");
	    	countryCodeMap.put("South Africa", "ZA");
	    	countryCodeMap.put("Zambia", "ZM");
	    	countryCodeMap.put("Zaire", "ZR");
	    	countryCodeMap.put("Zimbabwe", "ZW");
	    	
	    	CountryCode cp = new CountryCode();
    		cp.setName("United States");
    		cp.setCode("US");
    		list.add(cp);

	    	for (Map.Entry<String, String> entry : countryCodeMap.entrySet())
	    	{
	    		cp = new CountryCode();
	    		cp.setName(entry.getKey());
	    		cp.setCode(entry.getValue());
	    		list.add(cp);
	    		
	    	}
	    	return list;
	 }
	 
	 private Integer getCountryCodeIndex(String code){
	    	
	    	Map<String,String> countryCodeMap = new TreeMap<String,String>();
	    	List<CountryCode> list = new LinkedList<CountryCode>();
	    	
	    	countryCodeMap.put("United States", "US");
	    	countryCodeMap.put("Andorra", "AD");
	    	countryCodeMap.put("United Arab Emirates", "AE");
	    	countryCodeMap.put("Afghanistan", "AF");
	    	countryCodeMap.put("Antigua and Barbuda", "AG");
	    	countryCodeMap.put("Anguilla", "AI");
	    	countryCodeMap.put("Albania", "AL");
	    	countryCodeMap.put("Armenia", "AM");
	    	countryCodeMap.put("Netherlands Antilles", "AN");
	    	countryCodeMap.put("Angola", "AO");
	    	countryCodeMap.put("Antarctica", "AQ");
	    	countryCodeMap.put("Argentina", "AR");
	    	countryCodeMap.put("American Samoa", "AS");
	    	countryCodeMap.put("Austria", "AT");
	    	countryCodeMap.put("Australia", "AU");
	    	countryCodeMap.put("Aruba", "AW");
	    	countryCodeMap.put("Azerbaijan", "AZ");
	    	countryCodeMap.put("Bosnia and Herzegovina", "BA");
	    	countryCodeMap.put("Barbados", "BB");
	    	countryCodeMap.put("Bangladesh", "BD");
	    	countryCodeMap.put("Belgium", "BE");
	    	countryCodeMap.put("Burkina Faso", "BF");
	    	countryCodeMap.put("Bulgaria", "BG");
	    	countryCodeMap.put("Bahrain", "BH");
	    	countryCodeMap.put("Burundi", "BI");
	    	countryCodeMap.put("Benin", "BJ");
	    	countryCodeMap.put("Bermuda", "BM");
	    	countryCodeMap.put("Brunei Darussalam", "BN");
	    	countryCodeMap.put("Bolivia", "BO");
	    	countryCodeMap.put("Brazil", "BR");
	    	countryCodeMap.put("Bahamas", "BS");
	    	countryCodeMap.put("Bhutan", "BT");
	    	countryCodeMap.put("Bouvet Island", "BV");
	    	countryCodeMap.put("Botswana", "BW");
	    	countryCodeMap.put("Belarus", "BY");
	    	countryCodeMap.put("Belize", "BZ");
	    	countryCodeMap.put("Canada", "CA");
	    	countryCodeMap.put("Cocos Islands", "CC");
	    	countryCodeMap.put("Central African Republic", "CF");
	    	countryCodeMap.put("Congo", "CG");
	    	countryCodeMap.put("Switzerland", "CH");
	    	countryCodeMap.put("Cote D'Ivoire", "CI");
	    	countryCodeMap.put("Cook Islands", "CK");
	    	countryCodeMap.put("Chile", "CL");
	    	countryCodeMap.put("Cameroon", "CM");
	    	countryCodeMap.put("China", "CN");
	    	countryCodeMap.put("Colombia", "CO");
	    	countryCodeMap.put("US Commercial", "COM");
	    	countryCodeMap.put("Costa Rica", "CR");
	    	countryCodeMap.put("Czechoslovakia", "CS");
	    	countryCodeMap.put("Cuba", "CU");
	    	countryCodeMap.put("Cape Verde", "CV");
	    	countryCodeMap.put("Christmas Island", "CX");
	    	countryCodeMap.put("Cyprus", "CY");
	    	countryCodeMap.put("Czech Republic", "CZ");
	    	countryCodeMap.put("Germany", "DE");
	    	countryCodeMap.put("Djibouti", "DJ");
	    	countryCodeMap.put("Denmark", "DK");
	    	countryCodeMap.put("Dominica", "DM");
	    	countryCodeMap.put("Dominican Republic", "DO");
	    	countryCodeMap.put("Algeria", "DZ");
	    	countryCodeMap.put("Ecuador", "EC");
	    	countryCodeMap.put("US Educational", "EDU");
	    	countryCodeMap.put("Estonia", "EE");
	    	countryCodeMap.put("Egypt", "EG");
	    	countryCodeMap.put("Western Sahara", "EH");
	    	countryCodeMap.put("Eritrea", "ER");
	    	countryCodeMap.put("Spain", "SP");
	    	countryCodeMap.put("Ethiopia", "ET");
	    	countryCodeMap.put("Finland", "FI");
	    	countryCodeMap.put("Fiji", "FJ");
	    	countryCodeMap.put("Falkland Islands", "FK");
	    	countryCodeMap.put("Micronesia", "FM");
	    	countryCodeMap.put("Faroe Islands", "FO");
	    	countryCodeMap.put("France", "FR");
	    	countryCodeMap.put("France, Metropolitan", "FX");
	    	countryCodeMap.put("Gabon", "GA");
	    	countryCodeMap.put("Great Britain", "GB");
	    	countryCodeMap.put("Grenada", "GD");
	    	countryCodeMap.put("Georgia", "GE");
	    	countryCodeMap.put("French Guiana", "GF");
	    	countryCodeMap.put("Ghana", "GH");
	    	countryCodeMap.put("Gibraltar", "GI");
	    	countryCodeMap.put("Greenland", "GL");
	    	countryCodeMap.put("Gambia", "GM");
	    	countryCodeMap.put("Guinea", "GN");
	    	countryCodeMap.put("US Government", "GOV");
	    	countryCodeMap.put("Guadeloupe", "GP");
	    	countryCodeMap.put("Guadeloupe", "GO");
	    	countryCodeMap.put("Greece", "GR");
	    	countryCodeMap.put("S. Georgia and S. Sandwich Isls.", "GS");
	    	countryCodeMap.put("Guatemala", "GT");
	    	countryCodeMap.put("Guam", "GU");
	    	countryCodeMap.put("Guinea-Bissau", "GW");
	    	countryCodeMap.put("Guyana", "GN");
	    	countryCodeMap.put("Hong Kong", "HK");
	    	countryCodeMap.put("Heard and McDonald Islands", "HM");
	    	countryCodeMap.put("Honduras", "HN");
	    	countryCodeMap.put("Croatia", "HR");
	    	countryCodeMap.put("Haiti", "HT");
	    	countryCodeMap.put("Hungary", "HU");
	    	countryCodeMap.put("Indonesia", "ID");
	    	countryCodeMap.put("Ireland", "IE");
	    	countryCodeMap.put("Israel", "IL");
	    	countryCodeMap.put("India", "IN");
	    	countryCodeMap.put("International", "INT");
	    	countryCodeMap.put("British Indian Ocean Territory", "IO");
	    	countryCodeMap.put("Iraq", "IQ");
	    	countryCodeMap.put("Iran", "IR");
	    	countryCodeMap.put("Iceland", "IS");
	    	countryCodeMap.put("Italy", "IT");
	    	countryCodeMap.put("Jamaica", "JM");
	    	countryCodeMap.put("Jordan", "JO");
	    	countryCodeMap.put("Japan", "JP");
	    	countryCodeMap.put("Kenya", "KE");
	    	countryCodeMap.put("Kyrgyzstan", "KG");
	    	countryCodeMap.put("Cambodia", "KH");
	    	countryCodeMap.put("Kiribati", "KI");
	    	countryCodeMap.put("Comoros", "KM");
	    	countryCodeMap.put("Saint Kitts and Nevis", "KN");
	    	countryCodeMap.put("Korea (North)", "KP");
	    	countryCodeMap.put("Korea (South)", "KR");
	    	countryCodeMap.put("Kuwait", "KW");
	    	countryCodeMap.put("Cayman Islands", "KY");
	    	countryCodeMap.put("Kazakhstan", "KZ");
	    	countryCodeMap.put("Laos", "JO");
	    	countryCodeMap.put("Lebanon", "JO");
	    	countryCodeMap.put("Saint Lucia", "JO");
	    	countryCodeMap.put("Liechtenstein", "LI");
	    	countryCodeMap.put("Sri Lanka", "LK");
	    	countryCodeMap.put("Liberia", "LR");
	    	countryCodeMap.put("Lesotho", "LS");
	    	countryCodeMap.put("Lithuania", "LI");
	    	countryCodeMap.put("Luxembourg", "LU");
	    	countryCodeMap.put("Latvia", "LV");
	    	countryCodeMap.put("Libya", "LY");
	    	countryCodeMap.put("Morocco", "MA");
	    	countryCodeMap.put("Monaco", "MC");
	    	countryCodeMap.put("Moldova", "MD");
	    	countryCodeMap.put("Madagascar", "MG");
	    	countryCodeMap.put("Marshall Islands", "MH");
	    	countryCodeMap.put("US Military", "MIL");
	    	countryCodeMap.put("Macedonia", "MK");
	    	countryCodeMap.put("Mali", "ML");
	    	countryCodeMap.put("Myanmar", "MM");
	    	countryCodeMap.put("Mongolia", "MN");
	    	countryCodeMap.put("Macau", "MO");
	    	countryCodeMap.put("Northern Mariana Islands", "MP");
	    	countryCodeMap.put("Martinique", "MQ");
	    	countryCodeMap.put("Mauritania", "MR");
	    	countryCodeMap.put("Montserrat", "MS");
	    	countryCodeMap.put("Malta", "MT");
	    	countryCodeMap.put("Mauritius", "MU");
	    	countryCodeMap.put("Maldives", "MV");
	    	countryCodeMap.put("Malawi", "MW");
	    	countryCodeMap.put("Mexico", "MX");
	    	countryCodeMap.put("Malaysia", "MY");
	    	countryCodeMap.put("Mozambique", "MZ");
	    	countryCodeMap.put("Namibia", "MT");
	    	countryCodeMap.put("Nato field", "MT");
	    	countryCodeMap.put("New Caledonia", "MT");
	    	countryCodeMap.put("Niger", "NG");
	    	countryCodeMap.put("Norfolk Island", "NL");
	    	countryCodeMap.put("Nigeria", "NO");
	    	countryCodeMap.put("Nicaragua", "NI");
	    	countryCodeMap.put("Netherlands", "NL");
	    	countryCodeMap.put("Norway", "NO");
	    	countryCodeMap.put("Nepal", "NP");
	    	countryCodeMap.put("Nauru", "NR");
	    	countryCodeMap.put("Neutral Zone", "NT");
	    	countryCodeMap.put("Niue", "NU");
	    	countryCodeMap.put("New Zealand", "NZ");
	    	countryCodeMap.put("Oman", "OM");
	    	countryCodeMap.put("Non-Profit Organization", "ORG");
	    	countryCodeMap.put("Panama", "PA");
	    	countryCodeMap.put("Peru", "PE");
	    	countryCodeMap.put("French Polynesia", "PF");
	    	countryCodeMap.put("Papua New Guinea", "PG");
	    	countryCodeMap.put("Philippines", "PH");
	    	countryCodeMap.put("Pakistan", "PK");
	    	countryCodeMap.put("Poland", "PL");
	    	countryCodeMap.put("St. Pierre and Miquelon", "PM");
	    	countryCodeMap.put("Pitcairn", "PN");
	    	countryCodeMap.put("Pitcairn", "PN");
	    	countryCodeMap.put("Puerto Rico", "PR");
	    	countryCodeMap.put("Portugal", "PT");
	    	countryCodeMap.put("Palau", "PW");
	    	countryCodeMap.put("Paraguay", "PY");
	    	countryCodeMap.put("Qatar", "QA");
	    	countryCodeMap.put("Reunion", "RE");
	    	countryCodeMap.put("Romania", "RO");
	    	countryCodeMap.put("Russian Federation", "RU");
	    	countryCodeMap.put("Rwanda", "RW");
	    	countryCodeMap.put("Saudi Arabia", "SA");
	    	countryCodeMap.put("Seychelles", "SC");
	    	countryCodeMap.put("Solomon Islands", "SB");
	    	countryCodeMap.put("Sudan", "SD");
	    	countryCodeMap.put("Sweden", "SE");
	    	countryCodeMap.put("Singapore", "SG");
	    	countryCodeMap.put("St. Helena", "SH");
	    	countryCodeMap.put("Slovenia", "NI");
	    	countryCodeMap.put("Svalbard and Jan Mayen Islands", "SJ");
	    	countryCodeMap.put("Slovak Republic", "SK");
	    	countryCodeMap.put("Sierra Leone", "SL");
	    	countryCodeMap.put("San Marino", "SM");
	    	countryCodeMap.put("Senegal", "SN");
	    	countryCodeMap.put("Somalia", "SO");
	    	countryCodeMap.put("Suriname", "SR");
	    	countryCodeMap.put("Sao Tome and Principe", "ST");
	    	countryCodeMap.put("USSR", "SU");
	    	countryCodeMap.put("El Salvador", "SV");
	    	countryCodeMap.put("Syria", "SY");
	    	countryCodeMap.put("Swaziland", "SZ");
	    	countryCodeMap.put("Suriname", "SR");
	    	countryCodeMap.put("Turks and Caicos Islands", "TC");
	    	countryCodeMap.put("Chad", "TD");
	    	countryCodeMap.put("French Southern Territories", "TF");
	    	countryCodeMap.put("Togo", "TG");
	    	countryCodeMap.put("Thailand", "TH");
	    	countryCodeMap.put("Thailand", "TJ");
	    	countryCodeMap.put("Tajikistan", "SR");
	    	countryCodeMap.put("Tokelau", "TK");
	    	countryCodeMap.put("Turkmenistan", "Turkmenistan");
	    	countryCodeMap.put("Tunisia", "TN");
	    	countryCodeMap.put("Tonga", "TO");
	    	countryCodeMap.put("East Timor", "TP");
	    	countryCodeMap.put("Turkey", "TR");
	    	countryCodeMap.put("Trinidad and Tobago", "TT");
	    	countryCodeMap.put("Tuvalu", "TV");
	    	countryCodeMap.put("Taiwan", "TW");
	    	countryCodeMap.put("Tanzania", "TZ");
	    	countryCodeMap.put("Thailand", "SR");
	    	countryCodeMap.put("Ukraine", "UA");
	    	countryCodeMap.put("Uganda", "UG");
	    	countryCodeMap.put("United Kingdom", "UK");
	    	countryCodeMap.put("US Minor Outlying Islands", "UM");
	    	countryCodeMap.put("Uruguay", "UY");
	    	countryCodeMap.put("Uzbekistan", "UZ");
	    	countryCodeMap.put("Vatican City State", "VA");
	    	countryCodeMap.put("Saint Vincent and the Grenadines", "VC");
	    	countryCodeMap.put("Venezuela", "VE");
	    	countryCodeMap.put("Virgin Islands", "VG");
	    	countryCodeMap.put("Virgin Islands (U.S.)", "VI");
	    	countryCodeMap.put("Viet Nam", "VN");
	    	countryCodeMap.put("Vanuatu", "VU");
	    	countryCodeMap.put("Wallis and Futuna Islands", "WF");
	    	countryCodeMap.put("Samoa", "WS");
	    	countryCodeMap.put("Yemen", "YE");
	    	countryCodeMap.put("Mayotte", "YT");
	    	countryCodeMap.put("Yugoslavia", "YU");
	    	countryCodeMap.put("South Africa", "ZA");
	    	countryCodeMap.put("Zambia", "ZM");
	    	countryCodeMap.put("Zaire", "ZR");
	    	countryCodeMap.put("Zimbabwe", "ZW");
	    	
	    	CountryCode cp = new CountryCode();
    		cp.setName("United States");
    		cp.setCode("US");
    		list.add(cp);

	    	for (Map.Entry<String, String> entry : countryCodeMap.entrySet())
	    	{
	    		cp = new CountryCode();
	    		cp.setName(entry.getKey());
	    		cp.setCode(entry.getValue());
	    		list.add(cp);
	    		
	    	}

	    	int index = -1;
	    	boolean found = false;
	    	for (CountryCode cd : list)
	    	{
	    		index++;
	    		if(cd.getCode().equalsIgnoreCase(code)){
	    			found = true;
	    			break;
	    		}	
	    	}
	    	
	    	if(found){
	    		return index;
	    	}else{
	    		return 0;
	    	}
	 }

	protected void validatePage(Object command, Errors errors, int page) {
		CustomerForm customerForm = (CustomerForm) command;
		CustomerFormValidator validator = (CustomerFormValidator) getValidator();
		switch (page) {
		case 0:
			
			
			
			
			Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
			validator.validate(customerForm, errors);

			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.password", "form.required", "required");
			
			if((customerForm.getCustomer().isRegisterCellPhone()) && (customerForm.getCustomer().getAddress().getCellPhone()==null || customerForm.getCustomer().getAddress().getCellPhone().length()<8)){
				errors.rejectValue("customer.address.cellPhone", "CELL_PHONE_IS_REQUIRED", "A cell phone is required for registration.");	
			}
			
			if (errors.getErrorCount() == 0) {
				if(customerForm.getCustomer().isRegisterCellPhone() && this.webJaguar.checkUserByCellPhone(this.webJaguar.removeCellPhone(customerForm.getCustomer().getAddress().getCellPhone())) != null){
					errors.rejectValue("customer.address.cellPhone", "CELL_PHONE_IS_ALREADY_EXIST", "An account using this cell phone already exists.");	
				}else{
					if (!customerForm.getCustomer().isRegisterCellPhone() && this.webJaguar.getCustomerByUsername(customerForm.getCustomer().getUsername()) != null) {
						errors.rejectValue("customer.username", "USER_ID_ALREADY_EXISTS", "An account using this email address already exists.");
					}
				}
			}
			//Force Carrier selection if opted for Text msg notifications
			if(customerForm.getCustomer().isTextMessageNotify() && (customerForm.getCustomer().getAddress().getCellPhone()!=null || !customerForm.getCustomer().getAddress().getCellPhone().isEmpty())) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.mobileCarrierId", "form.required", "required");
			}
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.address.country", "form.required", "required");
			if (!customerForm.getCustomer().isRegisterCellPhone() && siteConfig.get("CONFIRM_USERNAME").getValue().equals("true") && !customerForm.getCustomer().getUsername().equals(customerForm.getConfirmUsername())) {
				errors.rejectValue("confirmUsername", "form.usernameMismatch", "matching username required");
			}

			if ((Boolean) gSiteConfig.get("gTAX_EXEMPTION") && siteConfig.get("TAX_ID_REQUIRED").getValue().equalsIgnoreCase("true")) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.taxId", "form.required", "required");
			}
			break;
		case 1:
			if (customerForm.getCustomerFields() != null) {
				for (CustomerField customerField : customerForm.getCustomerFields()) {
					if (customerField.isRequired())
						ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customer.field" + customerField.getId(), "form.required", "required");
				}
			}
			break;
		}
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		CrmContact tmp = null;
		String crmContactId = ServletRequestUtils.getStringParameter(request, "crmcontactid");
		Integer crmid = -1;
		boolean found = false;
		
		try{
			crmid = Integer.parseInt(crmContactId);
			}catch(Exception ex){	
		}
		
		CrmContact crmContact = this.webJaguar.getCrmContactById(crmid);
		
		if(crmContact!=null){
			found = true;
			tmp = crmContact;
		}
		
		// check if customer is logged in
		if (this.webJaguar.getUserSession(request) != null) {
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("account.jhtm")));
		}
		String forwardAction = request.getParameter("forwardAction");
		CustomerForm customerForm = new CustomerForm();
		if (forwardAction != null) {
			customerForm = new CustomerForm(forwardAction);
		}
		
		if(crmContact!=null && customerForm.getCustomer()!=null && customerForm.getCustomer().getAddress()!=null){
			
			customerForm.getCustomer().setUsername(crmContact.getEmail1());
			customerForm.getCustomer().getAddress().setFirstName(crmContact.getFirstName());
			customerForm.getCustomer().getAddress().setLastName(crmContact.getLastName());
			customerForm.getCustomer().getAddress().setCountry(crmContact.getCountry());
			customerForm.getCustomer().getAddress().setZip(crmContact.getZip());
			customerForm.getCustomer().getAddress().setCity(crmContact.getCity());
			customerForm.getCustomer().getAddress().setStateProvince(crmContact.getStateProvince());
			customerForm.getCustomer().getAddress().setPhone(crmContact.getPhone1());
			customerForm.getCustomer().getAddress().setCellPhone(crmContact.getPhone2());
			customerForm.getCustomer().getAddress().setFax(crmContact.getFax());
			customerForm.getCustomer().getAddress().setAddr1(crmContact.getStreet());
			customerForm.getCustomer().setNote(crmContact.getDescription());
			try{
				customerForm.getCustomer().getAddress().setCompany(crmContact.getCrmContactFields().get(13).getFieldValue());
			}catch(Exception ex){
			}
			if(crmContact.getTrackcode()!=null && crmContact.getTrackcode().length()>0){
				if(crmContact.getEmail1().trim().equalsIgnoreCase(customerForm.getCustomer().getUsername().trim())){
					//String trkcd = (String)request.getSession().getAttribute("trkcd");
					request.getSession().setAttribute("crmTrackcode",crmContact.getTrackcode());
					request.getSession().setAttribute("crmid",crmContact.getId());
					
					
					 
					//customerForm.getCustomer().setTrackcode(trk);
					//request.getSession().setAttribute("trkcd2",trkcd);	
				}
			}	
		}
		
		customerForm.setMinPasswordLength(Integer.parseInt(siteConfig.get("CUSTOMER_PASSWORD_LENGTH").getValue()));
		customerForm.setStrongPassword(Boolean.parseBoolean(siteConfig.get("CUSTOMER_PASSWORD_STRONG").getValue()));
		customerForm.setEmailRegExp(siteConfig.get("CUSTOMER_REG_EXP_EMAIL").getValue());

		if (gSiteConfig.get("gSITE_DOMAIN") != null && (gSiteConfig.get("gSITE_DOMAIN").equals("besthandbagwholesale.com") || gSiteConfig.get("gSITE_DOMAIN").equals("handbagdistributor-temp.com") || gSiteConfig.get("gSITE_DOMAIN").equals("wholesalehandbagsusa-temp.com"))) {
			setPages(new String[] { "frontend/register/step1", "frontend/register/step2", "frontend/register/step3", "frontend/register/besthandbagwholesale" });
		} else if (siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {	
			if(siteConfig.get("FRONTEND_REGISTRATION_DIRECTORY").getValue()!=null && siteConfig.get("FRONTEND_REGISTRATION_DIRECTORY").getValue().length()>0){
				setPages(new String[] {siteConfig.get("FRONTEND_REGISTRATION_DIRECTORY").getValue()+"/step1", siteConfig.get("FRONTEND_REGISTRATION_DIRECTORY").getValue()+"/step2", siteConfig.get("FRONTEND_REGISTRATION_DIRECTORY").getValue()+"/step3"});
			}else{
				setPages(new String[] {"frontend/layout/template6/register/step1", "frontend/layout/template6/register/step2", "frontend/layout/template6/register/step3"});
			}	
		}else {
			setPages(new String[] { "frontend/register/step1", "frontend/register/step2", "frontend/register/step3" });
		}

		if (siteConfig.get("CUSTOMERS_REQUIRED_COMPANY").getValue().equals("true")) {
			customerForm.setCompanyRequired(true);
		}

		if (siteConfig.get("CUSTOMER_MASS_EMAIL_SUBSCRIBE_DEFAULT").getValue().equals("true")) {
			customerForm.setSubscribeEmail(true);
		}

		// Customer Fields
		List<CustomerField> customerFields = this.webJaguar.getCustomerFields();
		Iterator<CustomerField> iter = customerFields.iterator();
		while (iter.hasNext()) {
			CustomerField customerField = iter.next();
			if (!customerField.isEnabled() || !customerField.isPublicField()) {
				iter.remove();
			}
		}
		
		if (!customerFields.isEmpty()) {
			customerForm.setCustomerFields(customerFields);
		}
		
		if (siteConfig.get("ADDRESS_RESIDENTIAL_COMMERCIAL").getValue().equals("1") || siteConfig.get("ADDRESS_RESIDENTIAL_COMMERCIAL").getValue().equals("3")) {
			customerForm.getCustomer().getAddress().setResidential(true);
		}
		// remove this code when viatading is moved to its own branch
		if (siteConfig.get("SITE_URL").getValue().contains("viatrading") || siteConfig.get("SITE_URL").getValue().contains("test.wjserver180.com")) {
			customerForm.getCustomer().getAddress().setLiftGate(true);
		}

		if (gSiteConfig.get("gCUSTOMER_CATEGORY") != null) {
			// category associations
			List<Object> catIds = new ArrayList<Object>();
			for (int i = catIds.size(); i < 5; i++) {
				catIds.add("");
			}
			customerForm.setCategoryIds(catIds.toArray());
			List<Object> parentIds = new ArrayList<Object>();
			for (int i = 0; i < catIds.size(); i++) {
				parentIds.add(gSiteConfig.get("gCUSTOMER_CATEGORY"));
			}
			customerForm.setParentIds(parentIds.toArray());
		}
		
		String path = request.getContextPath().equals("") ? "/" : request.getContextPath();
		Integer crmId = null;
		Cookie saleRepAccountTrackcodeCookie = WebUtils.getCookie(request, "LPF");
		if (saleRepAccountTrackcodeCookie != null){
			crmId = Integer.valueOf(saleRepAccountTrackcodeCookie.getValue());
		}
		
		CrmContact user = null;
		if(!found && crmId!=null && customerForm!=null && customerForm.getCustomer()!=null && customerForm.getCustomer().getAddress()!=null){
			user = this.webJaguar.getCrmContactById(crmId);	
			if(user!=null){
				tmp = user;
				crmContactToCustomerSynch(user, customerForm.getCustomer());	
			}
		}
		
		List<CrmContactField> list = null;
		
		if(tmp!=null){
			list = this.webJaguar.getCrmContactFieldsByContactId(tmp.getId());
		}
		
		String customFieldValue = null;
		
		if(list!=null){
			for(CrmContactField field : list){
				if(field.getId()==2){
					customFieldValue = field.getFieldValue();
				}
			}	
		}

		if(customFieldValue!=null){
			customerForm.getCustomer().setCustomFieldValue(customFieldValue);
		}
		
		return customerForm;
	}
	
	
	
	public void crmContactToCustomerSynch(CrmContact crmContact, Customer customer) throws IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		Map<String, String> maps = this.webJaguar.getContcatCustomerMap("customer");
		
		Map<Integer, String> contactFields = new HashMap<Integer, String>();
		for (CrmContactField crmContactField : crmContact.getCrmContactFields()) {
			if (crmContactField.getId() <= 20) {
				contactFields.put(crmContactField.getId(), crmContactField.getFieldValue());
			}
		}
		
		Class c = CrmContact.class;
		String methodName = null;
		
		customer.setUsername(crmContact.getEmail1());
		customer.getAddress().setFirstName(crmContact.getFirstName());
		customer.getAddress().setLastName(crmContact.getLastName());
		customer.getAddress().setCountry(crmContact.getCountry());
		customer.getAddress().setZip(crmContact.getZip());
		customer.getAddress().setCity(crmContact.getCity());
		customer.getAddress().setStateProvince(crmContact.getStateProvince());
		customer.getAddress().setPhone(crmContact.getPhone1());
		customer.getAddress().setCellPhone(crmContact.getPhone2());
		customer.getAddress().setFax(crmContact.getFax());

		customer.setCrmContactId(crmContact.getId());
		customer.setCrmAccountId(crmContact.getAccountId());		

		customer.getAddress().setCompany(crmContact.getAccountName());
		customer.setUnsubscribe(crmContact.isUnsubscribe());
		customer.setSalesRepId(crmContact.getAccessUserId() != null ? this.webJaguar.getAccessUserById(crmContact.getAccessUserId()).getSalesRepId() : null);

		// First Name
		methodName = maps.get("getAddress_getFirstName");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.getAddress().setFirstName((String) c.getMethod(methodName).invoke(crmContact));
			} else {
			customer.getAddress().setFirstName(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
		}
		
		// Last Name
		methodName = maps.get("getAddress_getLastName");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.getAddress().setLastName((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.getAddress().setLastName(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// addr1
		methodName = maps.get("getAddress_getAddr1");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.getAddress().setAddr1((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.getAddress().setAddr1(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// addr2
		methodName = maps.get("getAddress_getAddr2");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.getAddress().setAddr2((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.getAddress().setAddr2(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// city
		methodName = maps.get("getAddress_getCity");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.getAddress().setCity((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.getAddress().setCity(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// stateProvince
		methodName = maps.get("getAddress_getStateProvince");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.getAddress().setStateProvince((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.getAddress().setStateProvince(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// zipCode
		methodName = maps.get("getAddress_getZip");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.getAddress().setZip((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.getAddress().setZip(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// country
		methodName = maps.get("getAddress_getCountry");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.getAddress().setCountry((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.getAddress().setCountry(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// address type
		customer.getAddress().setResidential((crmContact.getAddressType() != null && crmContact.getAddressType().equalsIgnoreCase("Residential")) ? true : false);
		
		// phone
		methodName = maps.get("getAddress_getPhone");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.getAddress().setPhone((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.getAddress().setPhone(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// cellPhone
		methodName = maps.get("getAddress_getCellPhone");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.getAddress().setCellPhone((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.getAddress().setCellPhone(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// fax
		methodName = maps.get("getAddress_getFax");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.getAddress().setFax((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.getAddress().setFax(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// email to username
		methodName = maps.get("getUsername");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.setUsername((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.setUsername(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// taxId
		methodName = maps.get("getTaxId");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.setTaxId((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.setTaxId(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// note
		methodName = maps.get("getNote");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.setNote((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.setNote(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// trackcode
		methodName = maps.get("getTrackcode");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.setTrackcode((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.setTrackcode(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// unsubscribe
		methodName = maps.get("isUnsubscribe");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				if((Integer) c.getMethod(methodName).invoke(crmContact)==1){
					customer.setUnsubscribe(true);
				}else{
					customer.setUnsubscribe(false);
				}
			} 
		}
		
		// qualifier
		methodName = maps.get("getQualifier");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.setQualifier((Integer) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.setQualifier(Integer.parseInt(contactFields.get(Integer.parseInt(methodName.split("_")[1]))));
			}
		}
		
		// language
		methodName = maps.get("getLanguageCode");
		if (methodName != null && !methodName.equals("")) {
			String langCode = crmContact.getLanguage();
			if (!methodName.startsWith("getCrmContactFields")) {
				if(langCode!=null){
					if(langCode.equals("en")) {
						customer.setLanguageCode(LanguageCode.en);
					} else if(langCode.equals("es")) {
						customer.setLanguageCode(LanguageCode.es);
					} else if(langCode.equals("fr")) {
						customer.setLanguageCode(LanguageCode.fr);
					} else if(langCode.equals("ar")) {
						customer.setLanguageCode(LanguageCode.ar);
					} else if(langCode.equals("de")) {
						customer.setLanguageCode(LanguageCode.de);
					} else{
						customer.setLanguageCode(LanguageCode.en);
					}
				}
				else{
					customer.setLanguageCode(LanguageCode.en);
				}
			} 
		}
		
		// Registered By
		methodName = maps.get("getRegisteredBy");
		if (methodName != null && !methodName.equals("")) {
			String leadSource = null;
			if (!methodName.startsWith("getCrmContactFields")) {
				leadSource = (String) c.getMethod(methodName).invoke(crmContact);
				if (leadSource != null && !leadSource.isEmpty()) {
					customer.setRegisteredBy(leadSource);
				}
			} else {
				leadSource = contactFields.get(Integer.parseInt(methodName.split("_")[1]));
				if (leadSource != null && !leadSource.isEmpty()) {
					customer.setRegisteredBy(leadSource);
				}
			}
		}
		
		// getRating1
		methodName = maps.get("getRating1");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.setRating1((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.setRating1(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// getRating2
		methodName = maps.get("getRating2");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.setRating2((String) c.getMethod(methodName).invoke(crmContact));
			} else {
				customer.setRating2(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
			}
		}
		
		// isDoNotCall
		methodName = maps.get("isDoNotCall");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getCrmContactFields")) {
				customer.setDoNotCall((Boolean) c.getMethod(methodName).invoke(crmContact));
				}
			}
		}
	
		// test
		Class c2 = Customer.class;
		Method m2 = null;
		Class[] arglist = new Class[1];
		arglist[0] = String.class;
	
		// field1 to field20
		for (int i = 1; i <= 20; i++) {
			methodName = maps.get("getField" + i);
			if (methodName != null && !methodName.equals("")) {
				m2 = c2.getMethod("setField" + i, arglist);
				if (!methodName.startsWith("getCrmContactFields")) {
					m2.invoke(customer, (String) c.getMethod(methodName).invoke(crmContact));
				} else {
					m2.invoke(customer, contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
		}
		
		
	}


	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
		CustomerForm customerForm = (CustomerForm) command;

		switch (page) {
		case 1:
			if (customerForm.getParentIds() != null) {
				for (int i = 0; i < customerForm.getParentIds().length; i++) {
					customerForm.getCategoryIds()[i] = ServletRequestUtils.getStringParameter(request, "cat" + i + "_categoryIds", "");
				}
			}
			break;
		}
	}

	private void sendEmail(SiteMessage siteMessage, HttpServletRequest request, Customer customer, MultiStore multiStore, SalesRep salesRep) {

		// send email
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		if (multiStore != null) {
			contactEmail = multiStore.getContactEmail();
		}
		Format formatter2 = new SimpleDateFormat("MM/dd/yyyy");

		StringBuffer messageBuffer = new StringBuffer();

		String firstName = customer.getAddress().getFirstName();
		if (firstName == null) {
			firstName = "";
		}
		String lastname = customer.getAddress().getLastName();
		if (lastname == null) {
			lastname = "";
		}
		String note = customer.getNote();
		if (note == null) {
			note = "";
		}

		// subject
		siteMessage.setSubject(siteMessage.getSubject().replace("#email#", customer.getUsername()));
		siteMessage.setSubject(siteMessage.getSubject().replace("#password#", customer.getPassword()));
		siteMessage.setSubject(siteMessage.getSubject().replace("#firstname#", firstName));
		siteMessage.setSubject(siteMessage.getSubject().replace("#lastname#", lastname));
		siteMessage.setSubject(siteMessage.getSubject().replace("#note#", note));
		siteMessage.setSubject(siteMessage.getSubject().replace("#date#", formatter2.format(new Date()).toString()));

		if (salesRep != null) {
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepName#", salesRep.getName()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepEmail#", salesRep.getEmail()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			siteMessage.setSubject(siteMessage.getSubject().replace("#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		// message
		siteMessage.setMessage(siteMessage.getMessage().replace("#email#", customer.getUsername()));
		siteMessage.setMessage(siteMessage.getMessage().replace("#password#", customer.getPassword()));
		siteMessage.setMessage(siteMessage.getMessage().replace("#firstname#", firstName));
		siteMessage.setMessage(siteMessage.getMessage().replace("#lastname#", lastname));
		siteMessage.setMessage(siteMessage.getMessage().replace("#note#", note));
		siteMessage.setMessage(siteMessage.getMessage().replace("#date#", formatter2.format(new Date()).toString()));

		if (salesRep != null) {
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepName#", salesRep.getName()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepEmail#", salesRep.getEmail()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			siteMessage.setMessage(siteMessage.getMessage().replace("#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		messageBuffer.append(siteMessage.getMessage());

		try {
			// construct email message
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.addBcc(contactEmail);
			helper.setSubject(siteMessage.getSubject());
			helper.setText(messageBuffer.toString(), siteMessage.isHtml());
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing
		}
		// salesRep is not null is TERRITORY_ZIPCODE is ON
		// TODO if message is not HTML we send html message to SalesRep
		if (salesRep != null) {
			messageBuffer.append("<br /><br />New Customer Info:<br />Name:<b>" + customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName() + "</b>");
			messageBuffer.append("<br />Company Name:<b>" + customer.getAddress().getCompany() + "</b>");
			messageBuffer.append("<br />Address1:<b>" + customer.getAddress().getAddr1() + "</b>");
			messageBuffer.append("<br />Address2:<b>" + ((customer.getAddress().getAddr2() == null) ? "" : customer.getAddress().getAddr2()) + "</b>");
			messageBuffer.append("<br />City:<b>" + customer.getAddress().getCity() + "</b> State:<b> " + customer.getAddress().getStateProvince() + "</b> Zipcode:<b> "
					+ customer.getAddress().getZip() + "</b>");
			messageBuffer.append("<br />Phone:<b>" + customer.getAddress().getPhone() + "</b>");
			messageBuffer.append("<br />Email:<b>" + customer.getUsername() + "</b>");
			messageBuffer.append("<br />Note:<b>" + customer.getNote() + "</b>");
			try {
				// construct email message
				MimeMessage mms = mailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
				helper.setTo(salesRep.getEmail());
				helper.setFrom(contactEmail);
				helper.setCc(contactEmail);

				helper.setSubject(siteMessage.getSubject());
				helper.setText(messageBuffer.toString(), siteMessage.isHtml());
				mailSender.send(mms);
			} catch (Exception ex) {
				// do nothing
			}
		}
	}

	private void notifyAdmin(Map<String, Configuration> siteConfig, String message) {
		// send email notification
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		String siteURL = siteConfig.get("SITE_URL").getValue();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setBcc("developers@advancedemedia.com");
		msg.setSubject("Registering on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}

	private void generateRandomCardID(Customer customer) {
		String cardID = UUID.randomUUID().toString().toUpperCase().replace("-", "").substring(0, 9);
		if (this.webJaguar.getCustomerByCardID(cardID) == null) {
			customer.setCardId(cardID);
		} else
			generateRandomCardID(customer);
	}
}
