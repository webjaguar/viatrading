/*
 * Copyright 2005, 2011 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.web.frontend;


import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;

public class AjaxAddToQuoteListController implements Controller
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{		
		List<Integer> quoteListCount = (List) request.getSession().getAttribute( "quoteListCount" );
		if (quoteListCount == null) {
			quoteListCount = new ArrayList<Integer>();
			request.getSession().setAttribute( "quoteListCount", quoteListCount );
		}
		if (ServletRequestUtils.getIntParameter(request, "productId") != null && !quoteListCount.contains(ServletRequestUtils.getIntParameter(request, "productId"))) {
			quoteListCount.add( ServletRequestUtils.getIntParameter(request, "productId") );	
		} 
		
		return null;
	}
}
