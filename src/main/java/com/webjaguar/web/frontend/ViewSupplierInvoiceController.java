/* Copyright 2006 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.24.2009
 */

package com.webjaguar.web.frontend;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.PackingList;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.form.PackingListForm;

public class ViewSupplierInvoiceController extends SimpleFormController{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private MailSender mailSender;
    public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
    
    private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
    public ViewSupplierInvoiceController() {
		setSessionForm(true);
		setCommandName("packingListForm");
		setCommandClass(PackingListForm.class);
		setFormView("frontend/account/supplierSaleDetail");
		setSuccessView("account_sales.jhtm");
	}
    
    protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(request, binder);
		SimpleDateFormat df = new SimpleDateFormat( "MM/dd/yyyy", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, true, 10 ) );	
	}
    
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
		PackingListForm packingListForm = (PackingListForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
		Pattern p = Pattern.compile("\\d*-");
		if ( packingListForm.getOrder() != null ){
			if ( packingListForm.isNewPackingList() ) {
				Matcher matcher = p.matcher(packingListForm.getCustomer().getSupplierPrefix());
				packingListForm.getOrder().setLineItems( getSupplierLineItems( matcher.replaceFirst( "" ), packingListForm.getOrder().getOrderId() ));
			}
			for ( LineItem lineItem : packingListForm.getOrder().getLineItems()) {
				packingListForm.getPackingList().getLineItems().add( new LineItem(lineItem, packingListForm.getCustomer().getSupplierPrefix()) );
			}
		}
		model.put("order", packingListForm.getOrder());
		model.put("customer", packingListForm.getCustomer() );
		model.put("supplierId", packingListForm.getCustomer().getSupplierId() );
		// multi store
		model.put("invoiceLayout", this.webJaguar.getSystemLayout("invoice", packingListForm.getOrder().getHost(), request));	
		return model;
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors)  
	throws Exception {
		PackingListForm packingListForm = (PackingListForm) command;
		if ( packingListForm.getOrderStatus().getTrackNum().trim().length() == 0 ) {
			packingListForm.getOrderStatus().setTrackNum( null );
		}
		packingListForm.getPackingList().setTracking( packingListForm.getOrderStatus().getTrackNum() );
		
		updatePackingListLineItems( request, packingListForm.getPackingList() );
		if ( packingListForm.isNewPackingList() ) {
			try {
				boolean inventoryHistory = false;
				if(siteConfig.get("INVENTORY_HISTORY").getValue().equals("true")) {
					inventoryHistory = true;
				}
				this.webJaguar.insertPackingList( packingListForm.getPackingList(), null, inventoryHistory );
			}
			catch ( Exception e ) {}
		}

		boolean fulfilled = true;
		for (LineItem lineItem : this.webJaguar.getLineItemByOrderId( packingListForm.getPackingList().getOrderId() )) {
			if (lineItem.getQuantity() != lineItem.getProcessed()) {
				fulfilled = false;
				break;
			}
		}
		
		
		if ( fulfilled ) {
			packingListForm.getOrderStatus().setStatus( "s" );
		} else {
			packingListForm.getOrderStatus().setStatus( "pr" );
		}
		String supplierComment = packingListForm.getOrderStatus().getComments();
		if ( supplierComment != null ) {
			String supplierPrefix = "Added by (Sprefix " + packingListForm.getCustomer().getSupplierPrefix() +  " Sid " + packingListForm.getCustomer().getSupplierId() + "): ";
			packingListForm.getOrderStatus().setComments( supplierPrefix + packingListForm.getOrderStatus().getComments() );
		}
				
		this.webJaguar.insertOrderStatus( packingListForm.getOrderStatus() );
		
		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(packingListForm.getOrder().getHost());
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);
		}
		notify(siteConfig, packingListForm.getOrder(), multiStore);
		
		if (errors.hasErrors()) {
			return showForm(request, response, errors);				
		} else {
			return new ModelAndView( new RedirectView( getSuccessView() ));
		}
		
	}
	
	private void updatePackingListLineItems(HttpServletRequest request, PackingList packingList) throws ServletRequestBindingException 
	{
		for (LineItem lineItem : packingList.getLineItems()) {
			// get quantity
			lineItem.setQuantity( ServletRequestUtils.getIntParameter( request, "__quantity_" + lineItem.getLineNumber(), lineItem.getQuantity() ) );
			lineItem.setProcessed( lineItem.getQuantity() );
		}	
	}
    
	public Object formBackingObject(HttpServletRequest request) throws Exception {
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
		UserSession userSession = this.webJaguar.getUserSession(request);
		PackingListForm packingListForm = new PackingListForm();
		int orderId = ServletRequestUtils.getIntParameter(request, "orderId", 0);
		Order order = this.webJaguar.getOrder(orderId, siteConfig.get( "INVOICE_LINEITEMS_SORTING" ).getValue());
		Customer customer = this.webJaguar.getCustomerById( userSession.getUserid() );
		packingListForm.setCustomer( customer );
		packingListForm.getOrderStatus().setOrderId( orderId );
		if ( order != null ) {
			packingListForm.setOrder( order );
		    packingListForm.setPackingList( new PackingList() );
		    packingListForm.getPackingList().setPackingListNumber( orderId + "-" + customer.getSupplierPrefix() + this.webJaguar.packingListCount( orderId ) );
		    packingListForm.getPackingList().setOrderId( orderId );	
		}
		return packingListForm;
	}
	
	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception
	{
		PackingListForm packingListForm = (PackingListForm) command;
	}
	
	private List<LineItem> getSupplierLineItems(String supplierPrefix, Integer orderId)
	{
		Order order = this.webJaguar.getOrder( orderId, "" );
		Iterator<LineItem> lineItemsIterator = order.getLineItems().iterator();
		while ( lineItemsIterator.hasNext() ) {
   			LineItem lineItem = lineItemsIterator.next();
   			if ( !lineItem.getProduct().getSku().startsWith( supplierPrefix ) ) {
   				lineItemsIterator.remove();
   				continue;
   			}
		}
		return order.getLineItems();
	}
    
	
	
	private boolean notify(Map<String, Configuration> siteConfig, Order order, MultiStore multiStore) {

		Customer customer = this.webJaguar.getCustomerById(order.getUserId());
		
		if (!customer.isEmailNotify()) {
			// do not send email
			return false;
		}
		
		// send email
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	
    	
    	String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

    	StringBuffer messageBuffer = new StringBuffer();
    	messageBuffer.append( dateFormatter.format(new Date()) + "\n\n" );

    	messageBuffer.append( "The Order has been updated.\n Please check your order at \n" + secureUrl + "invoice.jhtm?order=" + order.getOrderId() + "\n\n " );
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(customer.getUsername());
		msg.setFrom( contactEmail );
		msg.setBcc( contactEmail );
		msg.setSubject( "order #" + order.getOrderId() + " " +  siteConfig.get("SITE_NAME").getValue());
		msg.setText( messageBuffer.toString() );
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
		return true;
	}
}
