/* Copyright 2005, 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 03.02.2009
 */

package com.webjaguar.web.frontend.buyRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.BuyRequest;
import com.webjaguar.model.BuyRequestSearch;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.UserSession;

public class BuyRequestSearchController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		
		BuyRequestSearch search = getBuyRequestSearch(request);

		search.setDelimiter( siteConfig.get( "SEARCH_KEYWORDS_DELIMITER" ).getValue());
		search.setKeywordsMinChars(Integer.parseInt(siteConfig.get("SEARCH_KEYWORDS_MINIMUM_CHARACTERS").getValue()));
    	search.setSort(siteConfig.get("SEARCH_SORT" ).getValue());
		
    	UserSession userSession = this.webJaguar.getUserSession( request );

		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null)); 
		
		Customer customer = null;
		if ( userSession != null )
			customer = this.webJaguar.getCustomerById(userSession.getUserid());
		if ( customer != null )
		{
			search.setProtectedAccess( customer.getProtectedAccess() );
		} else {
			search.setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue());
		}
 		
		// check number of characters
		boolean minCharsMet = false;
		if (search.getKeywords().trim().length() > 0) {
			for (String token: search.getKeywords().split(" ")) {
				if (token.length() >= search.getKeywordsMinChars()) {
					minCharsMet = true;
				} else {
					myModel.put("hasKeywordsLessMinChars", true);
				}
			}			
		} else if (search.getKeywordsMinChars() == 0) {
			minCharsMet = true;
		}
		myModel.put("minCharsMet", minCharsMet);
		
		int count = 0;
		if (minCharsMet) {
			count = this.webJaguar.searchBuyRequestsCount( search );
		}
		if (count < search.getOffset()-1) {
			search.setPage(1);
		}
		
		search.setLimit(search.getPageSize());
		search.setOffset((search.getPage()-1)*search.getPageSize());
		
		List<BuyRequest> buyRequestList = new ArrayList<BuyRequest>();
		if (minCharsMet) {
			buyRequestList = this.webJaguar.searchBuyRequests( search );		
		}
		
		int pageCount = count/search.getPageSize();
		if (count%search.getPageSize() > 0) {
			pageCount++;
		}
		
		myModel.put("pageCount", pageCount);
		myModel.put("pageEnd", search.getOffset()+buyRequestList.size());
		myModel.put("count", count);
		myModel.put("buyRequests", buyRequestList);
		// search tree
		List<Category> searchTree = new ArrayList<Category>();
		Integer categoryId = null;
		// multi store
		Integer hostCategoryId = this.webJaguar.getHomePageByHost(request.getHeader("host"));
		if (hostCategoryId != null) {
			if (this.webJaguar.getCategoryById(hostCategoryId, "").isShowSubcats()) {
				categoryId = hostCategoryId; 				
			}
		}
		for (Category category: this.webJaguar.getCategoryTree(categoryId, search.getProtectedAccess(), true, true, null, null)) {
			if (categoryId != null) {
				traverseSearchTree(searchTree, category, 0);
			} else {
				searchTree.add(category);
				traverseSearchTree(searchTree, category, 1);		
			}
		}
		if (searchTree.size() > 0) {
			myModel.put("searchTree", searchTree);			
		}
    	
        return new ModelAndView("frontend/buyRequest/search", "model", myModel);
	}
	
	private void traverseTree(List<Integer> categoryIds, Category category) {
		for (Category thisCategory: category.getSubCategories()) {
			categoryIds.add(thisCategory.getId());
			traverseTree(categoryIds, thisCategory);				
		}
	}
	
	private void traverseSearchTree(List<Category> categories, Category category, int level) {
		for (Category thisCategory: category.getSubCategories()) {
			StringBuffer cName = new StringBuffer(thisCategory.getName());
			for (int i=0; i<level; i++) {
				cName.insert(0, "&nbsp;&nbsp;");
			}
			thisCategory.setName(cName.toString());
			categories.add(thisCategory);
			traverseSearchTree(categories, thisCategory, level+1);				
		}
	}

	private BuyRequestSearch getBuyRequestSearch(HttpServletRequest request) {
		BuyRequestSearch search = (BuyRequestSearch) WebUtils.getOrCreateSessionAttribute(request.getSession(), "buyRequestSearch", BuyRequestSearch.class);
		
		if (request.getParameter("keywords") != null) {
			search.setKeywords(ServletRequestUtils.getStringParameter(request, "keywords", ""));
		}
		
		if (request.getParameter("category") != null) { 
			int categoryId = ServletRequestUtils.getIntParameter(request, "category", -1);
			if (categoryId > -1) {
				search.setCategory(categoryId);
			} else {
				search.setCategory(null);
			}
			search.setCategoryIds(null);
			// include subs
			List<Category> categories = this.webJaguar.getCategoryTree(search.getCategory(), null, true, null, null, null);	// include all categories
			if (!categories.isEmpty()) {
				List<Integer> categoryIds = new ArrayList<Integer>();
				categoryIds.add(categories.get(0).getId());
				// get subcategory id's
				traverseTree(categoryIds, categories.get(0));
				search.setCategoryIds(categoryIds);
			}
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);				
			}
		}			
		
		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				search.setPageSize(10);
			} else {
				search.setPageSize(size);				
			}
		}
		
		return search;
	}
}