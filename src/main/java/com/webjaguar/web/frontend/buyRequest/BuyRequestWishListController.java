/* Copyright 2005, 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 04.09.2009
 */

package com.webjaguar.web.frontend.buyRequest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.UserSession;

public class BuyRequestWishListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {		
		
    	UserSession userSession = this.webJaguar.getUserSession(request);			    	
    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();
		model.put( "url", url + "?" + query );
        // continue shopping
		request.getSession().setAttribute( "continueShoppingUrl", model.get( "url" ) );
		
		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			String ids[] = request.getParameterValues("__selected_id");
			if (ids != null) {
				this.webJaguar.removeFromBuyRequestWishList( userSession.getUserid(), ids );
			}
		}

		// products
		PagedListHolder buyRequestWishList = 
			new PagedListHolder(this.webJaguar.getBuyRequestWishListByUserid(userSession.getUserid(), request));
		buyRequestWishList.setPageSize(10);
		String page = request.getParameter("page");
		if (page != null) {
			buyRequestWishList.setPage(Integer.parseInt(page)-1);
		}
		model.put("buyRequestWishList", buyRequestWishList);
		Iterator<Product> plistIter = buyRequestWishList.getPageList().iterator();
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		return new ModelAndView("frontend/account/buyRequestWishList", "model", model);    	
    }
}
