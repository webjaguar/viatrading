/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 04.01.2009
 */

package com.webjaguar.web.frontend.buyRequest;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.BuyRequestSearch;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;

public class BuyRequestListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {	
		
    	UserSession userSession = this.webJaguar.getUserSession(request);
    	BuyRequestSearch buyRequestSearch = getBuyRequestSearch( request );    	
    	buyRequestSearch.setUserId( userSession.getUserid() );
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
				
		PagedListHolder buyRequestList = 
			new PagedListHolder(this.webJaguar.getBuyRequestListByUserid(buyRequestSearch));
		buyRequestList.setPageSize( 20 );
		buyRequestList.setPage( ServletRequestUtils.getIntParameter( request, "page", 1) - 1 );
		model.put("buyRequestList", buyRequestList);
		
		setLayout(request); 
		
		return new ModelAndView("frontend/account/buyRequestList", "model", model);    	
    }

	private void setLayout(HttpServletRequest request) {
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		}		
	}
	
	private BuyRequestSearch getBuyRequestSearch(HttpServletRequest request) {
		BuyRequestSearch buyRequestSearch = (BuyRequestSearch) request.getSession().getAttribute( "buyRequestSearch" );
		if (buyRequestSearch == null) {
			buyRequestSearch = new BuyRequestSearch();
			request.getSession().setAttribute( "buyRequestSearch", buyRequestSearch );
		}
		
		// sorting
		String sortBy = request.getParameter("sortBy");
		if (sortBy != null) {
			switch (ServletRequestUtils.getIntParameter( request, "sortBy", 10 )) {
				case 1:
					buyRequestSearch.setSort("created");
					break;
				default:
					buyRequestSearch.setSort("created DESC");
					break;
			}
		}
		
		// buy request Id
		if (request.getParameter("buyrequest_id") != null) {
			buyRequestSearch.setBuyRequestId(ServletRequestUtils.getStringParameter( request, "buyrequest_id", "" ));
		}		
		
		// status
		if (request.getParameter("status") != null) {
			buyRequestSearch.setStatus(ServletRequestUtils.getStringParameter( request, "status", "" ));
		}	
		
		// name
		if (request.getParameter("name") != null) {
			buyRequestSearch.setName( ServletRequestUtils.getStringParameter( request, "name", "" ));
		}	

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				buyRequestSearch.setPage( 1 );
			} else {
				buyRequestSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 20 );
			if (size < 10) {
				buyRequestSearch.setPageSize( 20 );
			} else {
				buyRequestSearch.setPageSize( size );				
			}
		}	
		return buyRequestSearch;
	}
}