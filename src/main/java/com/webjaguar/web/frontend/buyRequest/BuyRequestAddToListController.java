/* Copyright 2005, 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 04.01.2009
 */

package com.webjaguar.web.frontend.buyRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.UserSession;

public class BuyRequestAddToListController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {

    	UserSession userSession = this.webJaguar.getUserSession(request);
    	String ids[] = request.getParameterValues("br_id");
    	if (ids != null) {
    		this.webJaguar.addToBuyRequestWishList( userSession.getUserid(), ids );
    	}

		return new ModelAndView(new RedirectView("buyRequestWishList.jhtm"));
	}
}
