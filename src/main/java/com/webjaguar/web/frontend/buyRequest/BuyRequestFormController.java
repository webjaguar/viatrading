/* Copyright 2005, 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 04.01.2009
 */

package com.webjaguar.web.frontend.buyRequest;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.BuyRequest;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.form.BuyRequestForm;

public class BuyRequestFormController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	public BuyRequestFormController() {
		setSessionForm(true);
		setCommandName("buyRequestForm");
		setCommandClass(BuyRequestForm.class);
		setFormView("frontend/buyRequest/buyRequestForm");
		setSuccessView("buyRequestList.jhtm");
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
    	UserSession userSession = this.webJaguar.getUserSession(request);
    	BuyRequestForm buyRequestForm = (BuyRequestForm) command;
    	// check if delete button was pressed
		if ( request.getParameter( "_delete" ) != null ) {
			// not let them to delete?
			this.webJaguar.deleteBuyRequestById( buyRequestForm.getBuyRequest().getId() );
			return new ModelAndView(new RedirectView("buyRequestList.jhtm"));
		}
		if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("account.jhtm"));
    	}
    	buyRequestForm.getBuyRequest().setOwnerId( userSession.getUserid() );
    	buyRequestForm.getBuyRequest().setStatus( "open" ); 
		
		// get category ids ajax
		Set<Object> categories = new HashSet<Object>();
		for (int i=0; i<buyRequestForm.getParentIds().length; i++) {
			String id = ServletRequestUtils.getStringParameter(request, "cat" + i + "_categoryIds", "");
            if (!"".equals(id)) {
            	categories.add(new Integer(id));                                        
            }                                        		
		}
		buyRequestForm.getBuyRequest().setCatIdsSet(categories);
		// hard coded to level 0
		buyRequestForm.getBuyRequest().setProtectedLevel( "0" );
		if ( buyRequestForm.isNewBuyRequest() ) {
			
			this.webJaguar.insertBuyRequest( buyRequestForm.getBuyRequest() );
		} else {
			// not let them to update?
			this.webJaguar.updateBuyRequest( buyRequestForm.getBuyRequest() );
		}

		// construct email message
		MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms, false, "UTF-8");
		try {
			constructMessage(helper, buyRequestForm, request, userSession);
			mailSender.send(mms);
		} catch (Exception e) {
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));    	
    }
    
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors) {
    	BuyRequestForm buyRequestForm = (BuyRequestForm) command;
    	
       	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();
	    myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
	    map.put("model", myModel);    	
	    
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map<String, Configuration> siteConfig =  (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		Map <Integer, Object> categories = new HashMap<Integer, Object>();
		Map <Integer, Object> tree = new HashMap<Integer, Object>();
		for (int i=0; i<buyRequestForm.getParentIds().length; i++) {
			categories.put(i, this.webJaguar.getCategoryLinks(new Integer(buyRequestForm.getParentIds()[i].toString()), request));						
			myModel.put("categories", categories);
			tree.put(i, this.webJaguar.getCategoryTree(new Integer(buyRequestForm.getParentIds()[i].toString()), true, null));						
			myModel.put("tree", tree);
		}
	    
	    return map;
    }  
    
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig"); 
		
		UserSession userSession = this.webJaguar.getUserSession(request);
		BuyRequestForm buyRequestForm = new BuyRequestForm();
		if ( request.getParameter( "id" ) != null )
		{
			Integer buyRequestId = new Integer( request.getParameter( "id" ) );
			BuyRequest buyRequest = this.webJaguar.getBuyRequestById( buyRequestId, userSession.getUserid() );
			buyRequestForm.setNewBuyRequest( false );
			
			List<Object> catIds = new ArrayList<Object>();
			catIds.addAll(buyRequest.getCatIdsSet());	
			for (int i=catIds.size(); i<5; i++) {
				catIds.add("");				
			}
			buyRequestForm.setCategoryIds(catIds.toArray());
			List<Object> parentIds = new ArrayList<Object>();
			for (int i=0; i<catIds.size(); i++) {
				Integer catId = null;
				try {					
					catId = new Integer(catIds.get(i).toString());
				} catch (NumberFormatException e) {}
				Category category = this.webJaguar.getCategoryById(catId, "");
				if (category == null || category.getParent() == null) {
					parentIds.add(siteConfig.get("BUY_REQUEST_CATEGORY_ID").getValue());					
				} else {
					parentIds.add(category.getParent());					
				}
			}
			buyRequestForm.setParentIds(parentIds.toArray());
			
			buyRequestForm.setBuyRequest( buyRequest );
		} else {

			Calendar now = Calendar.getInstance();
			buyRequestForm.getBuyRequest().setActiveFrom( now.getTime() );
			// valid for 3 month
			now.add( Calendar.MONTH, 3 );
			buyRequestForm.getBuyRequest().setActiveTo( now.getTime() );
			List<Object> parentIds = new ArrayList<Object>();
			for (int i=0; i<5; i++) {
				parentIds.add(siteConfig.get("BUY_REQUEST_CATEGORY_ID").getValue());	
			}
			buyRequestForm.setParentIds(parentIds.toArray());
		}
		
		return buyRequestForm;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		BuyRequestForm buyRequestForm = (BuyRequestForm) command;
		if (buyRequestForm.getBuyRequest().getName() == null || buyRequestForm.getBuyRequest().getName().equals( "" )) {
			errors.rejectValue("buyRequest.name", "form.required", "required");
			errors.rejectValue("buyRequest.shortDesc", "form.required", "required");
			errors.rejectValue("buyRequest.keywords", "form.required", "required");
		}
	}
	
	private void constructMessage(MimeMessageHelper helper, BuyRequestForm buyRequestForm, HttpServletRequest request, UserSession userSession) throws Exception {
		// send email
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
    	String contactEmail = ((Configuration) siteConfig.get("CONTACT_EMAIL")).getValue();
    	String siteName = ((Configuration) siteConfig.get("SITE_NAME")).getValue();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));
    	
		helper.setTo(contactEmail);
		helper.setFrom(userSession.getUsername());	
		// Bcc emails
		helper.addBcc( "developers@advancedemedia.com" );
		
		helper.setSubject("Buy Request created on " + siteName + "  ID:" + buyRequestForm.getBuyRequest().getId());
		StringBuffer message = new StringBuffer();
		message.append( dateFormatter.format(new Date()) + "<br /><br />A Buy Request has been created to the Buy Request list.<br /><br />" + 
				"Customer ID: " + buyRequestForm.getBuyRequest().getOwnerId() + "<br />" +
				"Buy Request ID: " + buyRequestForm.getBuyRequest().getId() + "<br />" +
				"You can view this by searching:<br /><b>" + buyRequestForm.getBuyRequest().getKeywords() + "</b><br /><br />");
		helper.setText(message.toString(), true);

 	}
}