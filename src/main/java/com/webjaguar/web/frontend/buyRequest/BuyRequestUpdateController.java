/* Copyright 2005, 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 03.02.2009
 */

package com.webjaguar.web.frontend.buyRequest;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.form.BuyRequestForm;

public class BuyRequestUpdateController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	public BuyRequestUpdateController() {
		setSessionForm(true);
		setCommandName("buyRequestForm");
		setCommandClass(BuyRequestForm.class);
		setFormView("frontend/buyRequest/buyRequestUpdate");
		setSuccessView("buyRequestUpdate.jhtm");
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
    	UserSession userSession = this.webJaguar.getUserSession(request);
    	BuyRequestForm buyRequestForm = (BuyRequestForm) command;
    	buyRequestForm.getBuyRequestVersion().setUserId( userSession.getUserid() );

		// check if update button was pressed
		if (request.getParameter("__update") != null && request.getParameter("oid") != null) {
			buyRequestForm.getBuyRequestVersion().setOwnerId( Integer.parseInt( request.getParameter("oid") ));
			buyRequestForm.getBuyRequestVersion().setBuyRequestId( buyRequestForm.getBuyRequest().getId() );
			this.webJaguar.insertBuyRequestVesrion( buyRequestForm.getBuyRequestVersion() );

			Customer recipient = this.webJaguar.getCustomerById( buyRequestForm.getBuyRequestVersion().getOwnerId() );
			
			// construct email message
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, false, "UTF-8");
			try {
				constructMessage(helper, buyRequestForm, request, userSession, recipient.getUsername());
				mailSender.send(mms);
			} catch (Exception e) {
			}
		}
		// check if close button was pressed
		if (request.getParameter("__close") != null) {
			buyRequestForm.getBuyRequest().setStatus( "close" );
			this.webJaguar.updateBuyRequest( buyRequestForm.getBuyRequest() );
			
			// send message and inform the close.
			// TODO
		}
		return new ModelAndView(new RedirectView( getSuccessView() ), "id", buyRequestForm.getBuyRequest().getId() );
    }
    
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("__close_buyRequest") != null) {
			return true;			
		}
		return false;
	}

    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
    	BuyRequestForm buyRequestForm = (BuyRequestForm) command;
    	UserSession userSession = this.webJaguar.getUserSession(request);
       	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();

	    myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
	    Customer owner = this.webJaguar.getCustomerById( buyRequestForm.getBuyRequest().getOwnerId() );
	    myModel.put("owner", owner);
	    List versionList = this.webJaguar.getBuyRequestVesrionList( buyRequestForm.getBuyRequestVersion());
	    myModel.put("buyRequestVersions", versionList);
	    myModel.put("buyRequestVersionsSize", versionList.size());
	    myModel.put("userSession", userSession);
	    map.put("model", myModel);    	
	    
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
	    return map;
    }  

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		BuyRequestForm buyRequestForm = new BuyRequestForm();
		
		UserSession userSession = this.webJaguar.getUserSession(request);
		if ( request.getParameter("id") != null && userSession.getUserid() != null ) {
			buyRequestForm.setBuyRequest( this.webJaguar.getBuyRequestById( Integer.parseInt( request.getParameter("id") ), null ) );
			buyRequestForm.getBuyRequestVersion().setBuyRequestId( buyRequestForm.getBuyRequest().getId() );
			if ( userSession.getUserid() == buyRequestForm.getBuyRequest().getOwnerId()) {
				buyRequestForm.getBuyRequestVersion().setUserId( null );
			} else
				buyRequestForm.getBuyRequestVersion().setUserId( userSession.getUserid() );
		} 
		return buyRequestForm;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		BuyRequestForm buyRequestForm = (BuyRequestForm) command;
		if (buyRequestForm.getBuyRequestVersion().getDialog() == null || buyRequestForm.getBuyRequestVersion().getDialog().equals( "" )) {
			errors.rejectValue("buyRequestVersion.dialog", "form.required", "required");    					
		}
	}
	
	private void constructMessage(MimeMessageHelper helper, BuyRequestForm buyRequestForm, HttpServletRequest request, UserSession userSession, String recipientEmail) throws Exception {
		// send email
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
    	String contactEmail = ((Configuration) siteConfig.get("CONTACT_EMAIL")).getValue();
    	String siteName = ((Configuration) siteConfig.get("SITE_NAME")).getValue();
    	
    	List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0) {
			multiStore = multiStores.get(0);
		}
    	String siteURL = this.webJaguar.getSecureUrl(gSiteConfig, multiStore);
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));
    	
		helper.setTo(recipientEmail);
		// for now give them mytradezone email
		helper.setFrom(contactEmail);	
		// cc emails
		helper.addBcc( "developers@advancedemedia.com" );
		
		helper.setSubject("Buy Request updated on " + siteName + "  ID:" + buyRequestForm.getBuyRequest().getId());
		StringBuffer message = new StringBuffer();
		message.append(dateFormatter.format(new Date()) + "<br /><br />A comment has been added to the following buy request.<br />" + 
				"You can view and updat this Buy Request :<a href=\"" + siteURL + "/buyRequestUpdate.jhtm?id=" + buyRequestForm.getBuyRequest().getId() + "\">here</a><br />");
		
		helper.setText(message.toString(), true);
 	}
}