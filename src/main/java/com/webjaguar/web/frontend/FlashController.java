/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 08.16.2007
 */

package com.webjaguar.web.frontend;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.web.domain.Constants;

public class FlashController extends MultiActionController {
	
	private WebJaguarFacade webJaguar;
	
    public ModelAndView thumbnailer1(HttpServletRequest request, HttpServletResponse response)
    			throws Exception {				    	
    	
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		Map<String, Product> model = new HashMap<String, Product>();

		Product product = this.webJaguar.getProductById( ServletRequestUtils.getIntParameter( request, "id", 0 ), request );
		if (product != null && product.getImageLayout() != null && product.getImageLayout().equals( "ss" ) &&
				siteConfig.get( "PRODUCT_IMAGE_LAYOUTS" ).getValue().contains( "ss" )) {
			model.put("product", product);			
		}
		
		return new ModelAndView("frontend/flash/thumbnailer1", model);    	
    }
    
    public ModelAndView thumbnailer2(HttpServletRequest request, HttpServletResponse response) {
		Map<String, List<Category>> model = new HashMap<String, List<Category>>();
		
		// return list of sub-categories
		List<Category> subCategories = this.webJaguar.getCategoryLinks( ServletRequestUtils.getIntParameter( request, "cid", -1 ), request );
		// base image file
		File baseImageFile = new File(getServletContext().getRealPath("/assets/Image"));
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
			if ( prop.get( "site.root" ) != null ) {
				baseImageFile = new File( (String) prop.get( "site.root" ), "/assets/Image" );
			}
		}
		catch ( Exception e ) {}
		File baseCategoryFile = new File(baseImageFile, "Category");
		// check if image exists
		for ( Iterator<Category> i = subCategories.iterator(); i.hasNext(); ) {
			Category cat = i.next();
			File image = new File(baseCategoryFile, "cat_" + cat.getId() + ".gif");
			if ( !image.exists() ) {
				i.remove();
			}
		}
		model.put("subCategories", subCategories);
		
		return new ModelAndView("frontend/flash/thumbnailer2", model);        	
    }
 
    public ModelAndView thumbnailer3(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> model = new HashMap<String, Object>();
		
		// return list of products
		Category thisCategory = this.webJaguar.getCategoryById( ServletRequestUtils.getIntParameter( request, "cid", -1 ), Constants.FULL_PROTECTED_ACCESS );
		List<Product> featuredProducts = new ArrayList<Product>();
		if (thisCategory != null) {
			List<Product> products = this.webJaguar.getProductsByCategoryId( thisCategory, null, request, new ProductSearch(), false );
			for (Product thisProduct:products) {
				if (thisProduct.getThumbnail() != null) {
					featuredProducts.add( thisProduct );
				}
				if (featuredProducts.size() == 10) {
					break;
				}
			}			
		}
				
		model.put( "featuredProducts", featuredProducts );
		
		return new ModelAndView("frontend/flash/thumbnailer3", model);        	
    }

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}
}
