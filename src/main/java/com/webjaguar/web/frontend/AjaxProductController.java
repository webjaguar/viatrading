/*
 * Copyright 2005, 2012 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.web.frontend;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.web.domain.Constants;

public class AjaxProductController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		Product product = null;
		if(ServletRequestUtils.getStringParameter(request, "sku") != null) {
			Integer productId = this.webJaguar.getProductIdBySku(ServletRequestUtils.getStringParameter(request, "sku"));
			if(productId != null){
				product = this.webJaguar.getProductById(productId, request);
				myModel.put("product", product);
			}
		}
		Product parentProduct = null;
		if(product != null) {
			Integer parentProductId = product.getMasterSku() != null ? this.webJaguar.getProductIdBySku(product.getMasterSku()) : null;
			if(parentProductId != null) {
				parentProduct = this.webJaguar.getProductById(parentProductId, 5, false, null);
			}
			if(parentProduct != null && parentProduct.getProductLayout().equalsIgnoreCase("017")) {
				this.updateChildProduct(product, myModel, request);
			}
		}
		
		if(parentProduct != null && parentProduct.getProductLayout().equalsIgnoreCase("017")) {
			return new ModelAndView("frontend/framestore/ajaxUpdate", "model", myModel);
		} else {
			// need to find rule for this view
			return new ModelAndView( "frontend/viatrading/quickmode/product", "model", myModel );
		}
		
	}
	
	private void updateChildProduct(Product product, Map<String, Object> myModel, HttpServletRequest request){
		List <Product> childProducts = new ArrayList<Product>();
		ProductSearch search = new ProductSearch();
		search.setSku(product.getMasterSku());
		search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		search.setSort("price_1");
								
		for (Integer id: this.webJaguar.getSlaves(search)) {
			Product slave = this.webJaguar.getProductById(id, request);
			if (slave != null) {
				childProducts.add(slave);
			}
		}
		myModel.put("childProducts", childProducts);
	
	}
}