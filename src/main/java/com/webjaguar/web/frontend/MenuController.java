package com.webjaguar.web.frontend;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.domain.Constants;

public class MenuController extends WebApplicationObjectSupport implements Controller{
	
	private static final Logger logger = LoggerFactory.getLogger(MenuController.class);

	private WebJaguarFacade webJaguar;
	
	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}
	
	private String catName;
	
	public void setCatName(String catName) {
		this.catName = catName;
	}

	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		UserSession userSession = this.webJaguar.getUserSession(request);
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();

		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("url", url + "?" + query);
		
		Customer customer = null;
		if (userSession != null)
			customer = this.webJaguar.getCustomerById(userSession.getUserid());
		// Show add presentation only when salesrep cookie is there.
		if ((Boolean) gSiteConfig.get("gPRESENTATION")) {
			myModel.put("showAddPresentation", (request.getAttribute("cookieSalesRep") != null && customer == null) ? true : false);
		}
		
		String protectedAccess = "0";
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			protectedAccess = Constants.FULL_PROTECTED_ACCESS;
		} else {
			if (customer != null) {
				protectedAccess = customer.getProtectedAccess();
			} else {
				protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
			}
		}
		
		Category thisCategory = null;
		Integer categoryId = null;
		if (request.getParameter("sid") == null || ((String) gSiteConfig.get("gSHARED_CATEGORIES")).equals("")) {
			if (request.getParameter("cid") != null) {
				categoryId = ServletRequestUtils.getIntParameter(request, "cid", -1);
				thisCategory = this.webJaguar.getCategoryById(categoryId, protectedAccess);
			} else {
				if (this.catName == null) {
					categoryId = this.webJaguar.getHomePageByHost(request.getHeader("host"));
					if (categoryId == null) {
						categoryId = this.webJaguar.getHomePageByHost(null);
					}
				} else {
					try {
						if (this.catName.equals("contactUs")) {// get contact us category id
							categoryId = Integer.parseInt(siteConfig.get("CONTACT_US_ID").getValue());
						} else if (this.catName.equals("aboutUs")) {// get about us category id
							categoryId = Integer.parseInt(siteConfig.get("ABOUT_US_ID").getValue());
						}
					} catch (Exception e) {
					}
				}
				thisCategory = this.webJaguar.getCategoryById(categoryId, protectedAccess);
			}
		}
		
		
		// sub categories
					Integer subcatLevels = new Integer(siteConfig.get("SUBCAT_LEVELS").getValue());
					File baseImageFile = new File(getServletContext().getRealPath("/assets/Image"));
					File baseCategoryFile = new File(baseImageFile, "Category");
					if (thisCategory.getSubcatLevels() != null) {
						subcatLevels = thisCategory.getSubcatLevels();
					}
					if (subcatLevels > 0) {
						List<Category> subCategories = this.webJaguar.getCategoryLinks(thisCategory.getId(), request);
						// check if image exist. This is done just for Safari
						// other browsers do not show, no image.
						for (Iterator<Category> i = subCategories.iterator(); i.hasNext();) {
							Category cat = i.next();
							File image = new File(baseCategoryFile, "cat_" + cat.getId() + ".gif");
							cat.setHasImage(image.exists());
						}

						myModel.put("subCategories", subCategories);
						if (siteConfig.get("SUBCAT_DISPLAY").getValue().equals("") || siteConfig.get("SUBCAT_DISPLAY").getValue().equals("onePerRow")) {
							// subs by column
							int subcatCols = thisCategory.getSubcatCols();
							if (subcatCols < 1) {
								subcatCols = 1;
							}
							int[] subcatLinksPerCol = new int[subcatCols];
							int col = 0;
							for (int i = 0; i < subCategories.size(); i++) {
								subcatLinksPerCol[col++] += 1;
								if (col == subcatCols)
									col = 0;
							}
							myModel.put("subcatLinksPerCol", subcatLinksPerCol);
						}

						if (subcatLevels > 1) {
							Map<Integer, int[]> subcatLinksPerColMap = new HashMap<Integer, int[]>();

							Iterator<Category> iter = subCategories.iterator();
							while (iter.hasNext()) {
								Category category = iter.next();
								getSubCategoryLinks(category, request, 0, 1);

								if (siteConfig.get("SUBCAT_DISPLAY").getValue().equals("onePerRow")) {
									// subs by column
									int subcatCols = thisCategory.getSubcatCols();
									if (subcatCols < 1) {
										subcatCols = 1;
									}
									int[] subcatLinksPerCol = new int[subcatCols];
									int col = 0;
									for (int i = 0; i < category.getSubCategories().size(); i++) {
										subcatLinksPerCol[col++] += 1;
										if (col == subcatCols)
											col = 0;
									}
									subcatLinksPerColMap.put(category.getId(), subcatLinksPerCol);
								}

							}
							myModel.put("subcatLinksPerColMap", subcatLinksPerColMap);
						}
					}
					
					
				
		myModel.put("thisCategory", thisCategory);
		System.out.println("!--this is cid: "+categoryId);
	
		myModel.put("controller","this is from MenuController");
		return new ModelAndView("frontend/menu", "model", myModel);
	}
	private void getSubCategoryLinks(Category category, HttpServletRequest request, int depth, int maxDepth) {
		List<Category> subCategories = this.webJaguar.getCategoryLinks(category.getId(), request);
		category.setSubCategories(subCategories);
		Iterator<Category> iter = subCategories.iterator();
		while (iter.hasNext()) {
			Category subCategory = iter.next();
			if (depth < maxDepth) {
				getSubCategoryLinks(subCategory, request, depth + 1, maxDepth);
			}
		}
	}

}
