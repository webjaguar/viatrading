/* Copyright 2005, 2011 Advanced E-Media Solutions
 * author: Jwalant Patel
 */

package com.webjaguar.web.frontend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.IncludedProduct;
import com.webjaguar.model.Option;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductOptionValue;

public class ShowAjaxOptionsController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	List<ProductOption> optionsList;
	
	private static double newPrice;
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {	
		optionsList = new ArrayList<ProductOption>();	
		
		Map<String, Map<Integer, Integer>> valueMap = new HashMap<String, Map<Integer,Integer>>();
		Map<String, Integer> originalSelection = new HashMap<String, Integer>();
		if(ServletRequestUtils.getStringParameter(request, "option") != null) {
			for(String option : ServletRequestUtils.getStringParameters(request, "option")) {
				
				String[] data = option.split("-");
				
				String optionCode = null;
				Integer optionIndex = null;
				Integer valueIndex = null;
				try {
					originalSelection.put(data[0]+"-"+data[1], Integer.parseInt( data[2] ));
					optionCode = data[0];
					optionIndex = Integer.parseInt( data[1].split("_")[3] );
					valueIndex = Integer.parseInt( data[2] );
					
					if(valueMap.get(optionCode) != null) {
						valueMap.get(optionCode).put(optionIndex, valueIndex);
					} else {
						Map<Integer, Integer> tempMap = new HashMap<Integer, Integer>();
						tempMap.put(optionIndex, valueIndex);
						valueMap.put(optionCode, tempMap);
					}
				} catch(Exception e) { e.printStackTrace(); }
				
			}
		}
		Set<String> imageUrlSet = new HashSet<String>();
		
		// change the price1 only and add only price per item. Need to add more code, if customer want to change all prices or price range.
		// Option price that is one time only is not added to Price1.
		Product product = this.webJaguar.getProductById(ServletRequestUtils.getIntParameter(request, "productId"), request);
		newPrice = product.getPrice1();
		
		String[] optionCodeList = null;
		if(product != null && product.getOptionCode() != null) {
			optionCodeList = product.getOptionCode().split(",");
			for(String optionCode : optionCodeList) {
				Option baseOpt =  this.webJaguar.getOption(null, optionCode.trim());
				if(baseOpt != null) {
					for(ProductOption productOption : baseOpt.getProductOptions()) {
						productOption.setProductId(product.getId());
						optionsList.add(productOption);
						this.getDependingOptions(productOption, product.getId(), optionCode.trim(), valueMap, imageUrlSet, optionsList);
					}
				}	
			} 
				
		}
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		for(ProductOption option : optionsList) {
				
			// new dropdown's first options' imageUrl 
			if(valueMap.get(option.getOptionCode()) == null) {
				for(ProductOptionValue value : option.getValues()) {
					if(value.getIndex() == 0) {
						if(value.getImageUrl() != null && !value.getImageUrl().isEmpty()) {
							imageUrlSet.add(value.getImageUrl());
						}
					}
				}
			}
		}
		
		myModel.put("product", product);
		myModel.put("optionList", optionsList);
		myModel.put("imageUrlSet", imageUrlSet);
		myModel.put("productId", ServletRequestUtils.getStringParameter(request, "productId"));
		myModel.put("originalSelection", originalSelection);
		myModel.put("newPrice", newPrice);
		return new ModelAndView("frontend/common/smartOptions", "model", myModel);
	}
	
	
	//recursive function to get all options based on selection
	public void getDependingOptions(ProductOption productOption, Integer productId, String optionCode, Map<String, Map<Integer, Integer>> valueMap, Set<String> imageUrlSet, List<ProductOption> optionsList){
		//if option value is open input field or option name is different than parameter passed to this function, than dont do anything
		if(productOption.getValues().isEmpty()) {
			return;
		}
		if(optionCode != null && valueMap.get(optionCode) != null) {
			//ProductOption productOption = this.webJaguar.getProductOptionByOptionCodeAndOptionName(optionCode, productOptionName);
					
			String dependencyId = null;
			for(ProductOptionValue value : productOption.getValues()) {
				if(value.getIndex() == valueMap.get(optionCode).get(productOption.getIndex())) {
					dependencyId = value.getDependingOptionIds();
					if(value.getOptionPrice() != null) {
						newPrice = newPrice + value.getOptionPrice();
					}
					 
					if(value.getImageUrl() != null && !value.getImageUrl().isEmpty()) {
						imageUrlSet.add(value.getImageUrl());
					}
					
					if(value.getIncludedProductList() != null) {
						this.getIncludedProductOptions(value.getIncludedProductList(), valueMap, imageUrlSet, optionsList);
					}
				}
			}
					
			if(dependencyId != null) {
				Set<Integer> optionIdSet = new HashSet<Integer>();
				for(String id : dependencyId.split(",")){
					try {
						optionIdSet.add(Integer.parseInt(id));
					} catch (Exception e) { e.printStackTrace(); }
				}
				
				for(Integer id : optionIdSet) {
					Option newOption = this.webJaguar.getOption(id, null);
					if(newOption != null) {
						for(ProductOption option : newOption.getProductOptions()) {
							option.setProductId(productId);
							int index = -1;
							for(ProductOption tempOpt : optionsList) {
								if(tempOpt.getOptionCode().equalsIgnoreCase(productOption.getOptionCode()) && tempOpt.getName().equalsIgnoreCase(productOption.getName())) {
									index = optionsList.indexOf(tempOpt);
									break;
								}
							}
							if(index != -1) {
								optionsList.add(index+1, option);
							} else {
								optionsList.add(option);
							}
							getDependingOptions(option, productId, newOption.getCode(), valueMap, imageUrlSet, optionsList);
						}
					}
				}
			}
		}
	}
	
	
	private void getIncludedProductOptions(List<IncludedProduct> includedProductList, Map<String, Map<Integer, Integer>> valueMap, Set<String> imageUrlSet, List<ProductOption> optionsList){
		
		for(Product includedProduct : includedProductList){
			Product product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(includedProduct.getSku()), 0, false, null);
			if(product != null && product.getOptionCode() != null && !product.getOptionCode().trim().isEmpty()) {
				String[] optionCodes = product.getOptionCode().split(",");
				for(String optionCode : optionCodes){
					Option opt =  this.webJaguar.getOption(null, optionCode);
					if(opt != null) {
						for(ProductOption productOption : opt.getProductOptions()) {
							productOption.setProductId(product.getId());
							optionsList.add(productOption);
							this.getDependingOptions(productOption, product.getId(), optionCode, valueMap, imageUrlSet, optionsList);
						}
					}
				}	
			}
		}
	}
}