/*
 * Copyright 2005, 2010 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.web.frontend.quickView;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.UserSession;
import com.webjaguar.thirdparty.evergreen.EvergreenApi;
import com.webjaguar.web.domain.Constants;

public class QuickViewProductController extends WebApplicationObjectSupport implements Controller
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		
		UserSession userSession = this.webJaguar.getUserSession(request);
		Customer customer = null;
		if (userSession != null)
			customer = this.webJaguar.getCustomerById(userSession.getUserid());
		boolean customerSeeHiddendPrice = (customer == null || !customer.isSeeHiddenPrice()) ? false : true ;
		
		// protected access feature
		String protectedAccess = "0";
		if (customer != null) {
			protectedAccess = customer.getProtectedAccess();
		} else {
			protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			protectedAccess = Constants.FULL_PROTECTED_ACCESS;
		}
		
		// i18n
		String lang = null;
		if (request != null) {
			lang = RequestContextUtils.getLocale(request).getLanguage();
			if(lang.equalsIgnoreCase(LanguageCode.en.toString())) {
				lang = null;
			}
		}
		
		String prodLayout =  siteConfig.get("PRODUCT_DEFAULT_DETAIL_LAYOUT").getValue();
		

		String skuParam = ServletRequestUtils.getStringParameter(request, "sku", "").trim();
		Integer idParam = ServletRequestUtils.getIntParameter(request, "id", 0);
		if (request.getParameter("id") != null || !skuParam.equals("")) {
			Product product = null;
			if (!skuParam.equals("")) {
				product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(skuParam), request);								
			} else {
				product = this.webJaguar.getProductById(idParam, request);				
			}
			
			if (product != null) {
				// update statistics
				this.webJaguar.nonTransactionSafeStats(product);
				
				// product fields
				Class<Product> c = Product.class;
				Method m = null;
				Object arglist[] = null;
				
				// product layout
				if(product.getProductLayout() != null && !product.getProductLayout().equals("")){
					prodLayout = product.getProductLayout();
				}
				
				// evergreen
				if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
					// get inventory status
					EvergreenApi evergreen = new EvergreenApi();
					String evergreenStatus = evergreen.GetInvStatus(product.getSku());
					if (evergreenStatus != null) {
						product.setField6(evergreenStatus);						
					}
					
					if (customer != null && customer.getPriceTable() == null) {
						// by default all US customer see PriceTable1
						customer.setPriceTable(1);
					} else if ((customer != null && customer.getPriceTable() == 6) && product.getPriceTable6() == null) {
						product.setPrice( null );
						product.setMsrp( null );
						product.setLoginRequire(true);
						product.setAltLoginRequire(true);
					}
				}
				for (ProductField productField : this.webJaguar.getProductFieldsRanked(protectedAccess, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"), lang)) {
					if (productField.isDetailsField()) {
						m = c.getMethod("getField" + productField.getId());
						productField.setValue((String) m.invoke(product, arglist));
						if (productField.getValue() != null && productField.getValue().length() > 0) {
							product.addProductField(productField);
						}
					}
				}
				//	hide price
				if (product.isHidePrice() && !customerSeeHiddendPrice) {
					product.setPrice(null);
				}
				//	hide msrp
				if (product.isHideMsrp()) {
					product.setMsrp(null);
				}
				// recommended list
				List <Product> recommendedList = null;
				if ((Boolean) gSiteConfig.get("gRECOMMENDED_LIST")) {
					if (product.getRecommendedList() != null) {
						recommendedList = new ArrayList<Product>();
						String[] skus = product.getRecommendedList().split("[,\n]"); // split by line breaks or commas
						for (int x = 0; x < skus.length; x++) {
							String sku = skus[x].trim();
							if (!("".equals(sku))) {
								Product recommendedProduct = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(sku), request);
								if (recommendedProduct != null) {
									recommendedList.add(recommendedProduct);
								}
							}
						}
						if (!recommendedList.isEmpty()) {	// not empty
							model.put("recommendedList", recommendedList);		
						}
					}
				}
				
				if ((Boolean) gSiteConfig.get("gPRODUCT_REVIEW")) {
					
				}
				

				// get images from custom folder
				if (product.getImageFolder() != null && (product.getImageFolder().trim().equals("") || product.getImageFolder().startsWith(".")))
					product.setImageFolder(null);
				if (product.getImageFolder() != null) {
					File baseFile = new File(getServletContext().getRealPath("/assets/Image/Product"));
					Properties prop = new Properties();
					try
					{
						prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
						if (prop.get("site.root") != null)
						{
							baseFile = new File((String) prop.get("site.root") + "/assets/Image/Product");
						}
					}
					catch (Exception e)
					{}
					File detailsbig = new File(baseFile, "detailsbig");
					File imageFolder = new File (detailsbig, product.getImageFolder());
					if (imageFolder.exists() && imageFolder.isDirectory()) {
						for (File image : imageFolder.listFiles()) {
							if (!image.isHidden() && !image.isDirectory() && 
									(image.getName().toLowerCase().endsWith(".jpg") || image.getName().toLowerCase().endsWith(".gif"))) {
								ProductImage pi = new ProductImage();
								pi.setImageUrl(product.getImageFolder() + "/" + image.getName());
								product.addImage(pi);
							}
						}
					}
				}
			}
			model.put("product", product);
		} 
		if (siteConfig.get( "SITE_URL" ).getValue().contains("viatrading") || siteConfig.get( "SITE_URL" ).getValue().contains("test.wjserver180.com")) {
			return new ModelAndView("frontend/viatrading/productQuickView", "model", model);
		}
		else {
			return new ModelAndView("frontend/common/quickView/productQuickView", "model", model);
		}		
	}
}
