/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 06.19.2007
 */

package com.webjaguar.web.frontend;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.webjaguar.dao.CategoryDao;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductSearch;


public class XmlServicesController extends MultiActionController {

	WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }
	
    private GlobalDao globalDao;
    public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	CategoryDao categoryDao;
	public void setCategoryDao(CategoryDao categoryDao){ this.categoryDao = categoryDao; }
	
    public ModelAndView category(HttpServletRequest request, HttpServletResponse response) {
    	
		// site configuration
		Map<String, Configuration> siteConfig = this.webJaguar.getSiteConfig();
		Map<String, Object> gSiteConfig = this.globalDao.getGlobalSiteConfig();
    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	int sid = ServletRequestUtils.getIntParameter( request, "sid", 0 );
    	Category category = this.webJaguar.getCategoryById( sid, "" );
    	if (category != null) {
        	if (category.getSubcatLevels() == null) {
        		category.setSubcatLevels( new Integer( siteConfig.get( "SUBCAT_LEVELS" ).getValue() ) );
        	}
        	if (category.getProductPerPage() == null) {
        		category.setProductPerPage( new Integer( siteConfig.get( "NUMBER_PRODUCT_PER_PAGE" ).getValue() ) );
        	}
        	
        	// base image file
			File baseImageFile = new File(getServletContext().getRealPath("/assets/Image"));
			Properties prop = new Properties();
			try {
				prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
				if ( prop.get( "site.root" ) != null ) {
					baseImageFile = new File( (String) prop.get( "site.root" ), "/assets/Image" );
				}
			}
			catch ( Exception e ) {}
			File baseCategoryFile = new File(baseImageFile, "Category");
			File image = new File(baseCategoryFile, "cat_" + category.getId() + "_big.gif");
			category.setHasImage( image.exists() );
			category.setProductPerRow( Integer.parseInt(siteConfig.get( "NUMBER_PRODUCT_PER_ROW" ).getValue()) );
        	
    		if (category.getSubcatLevels() > 0) {
    			List<Category> subCategories = this.categoryDao.getCategoryLinks( category.getId(), "0", null, false );	
    			
				// check if image exists
				for ( Iterator<Category> i = subCategories.iterator(); i.hasNext(); ) {
					Category cat = i.next();
					image = new File(baseCategoryFile, "cat_" + cat.getId() + ".gif");
					cat.setHasImage( image.exists() );
				}
    			map.put( "subCategories", subCategories );
    			if (category.getSubcatLevels() > 1) {
    				Iterator<Category> iter = subCategories.iterator();
    				while (iter.hasNext()) {
    					Category subCategory = iter.next();
    					getSubCategoryLinks( subCategory, request, 0, 1 );
    				}
    			}
    		}
    		
    		// get layout
    		if (category.getDisplayMode() == null || category.getDisplayMode().equals("")) {
    			category.setDisplayMode( siteConfig.get( "CATEGORY_DISPLAY_MODE" ).getValue() );
    		}
    		
    		ProductSearch productSearch = new ProductSearch();
    		productSearch.setCategory( sid );
    		productSearch.setSort( siteConfig.get( "PRODUCT_SORTING" ).getValue() );
    		
    		// product fields
    		productSearch.setProductField(new ArrayList<ProductField>());
    		int gPRODUCT_FIELDS = (Integer) gSiteConfig.get("gPRODUCT_FIELDS");									
    		for (int i=1; i<=gPRODUCT_FIELDS; i++) {
    			if (request.getParameter("field_" + i) != null) {
    				ProductField pField = new ProductField(i, request.getParameter("field_" + i));
    				productSearch.getProductField().add(pField);
    			}
    		}
    		
    		map.put("SUBCAT_DISPLAY", siteConfig.get( "SUBCAT_DISPLAY" ).getValue());
    		map.put("category", category);
    		map.put("breadCrumbs", this.webJaguar.getCategoryTree(sid, true, null));
    		map.put( "products", this.webJaguar.getProductList(productSearch) );    		
    	}
    	return new ModelAndView("xml/category", map);
    }
    
    public ModelAndView html(HttpServletRequest request, HttpServletResponse response) {
    	
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	int sid = ServletRequestUtils.getIntParameter( request, "sid", 0 );
    	Category category = this.webJaguar.getCategoryById( sid, "" );
    	if (category != null) {
    		map.put("category", category);
    	}
    	return new ModelAndView("xml/html", map);
    }
    
	private void getSubCategoryLinks(Category category, HttpServletRequest request, int depth, int maxDepth)
	{
		List<Category> subCategories = this.categoryDao.getCategoryLinks( category.getId(), "0", null, false );
		category.setSubCategories( subCategories );
		Iterator<Category> iter = subCategories.iterator();
		while (iter.hasNext()) {
			Category subCategory = iter.next();
			if (depth < maxDepth) {
				getSubCategoryLinks( subCategory, request, depth + 1, maxDepth );
			}
		}
	}
    
}
