/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ClickDetail;
import com.webjaguar.model.Click;
import com.webjaguar.web.domain.Utilities;
import com.sun.image.codec.jpeg.JPEGCodec;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class OpenRateController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private final static String KEY = "AECCF9008EDBF4535CCE2345435364FA";
	private static final int WIDTH = 1;
	private static final int HEIGHT = 1;
	private static final Color BACKGROUND_COLOR = new Color(255,255,255);
	private static final Color COLOR = new Color(255,255,255);
	private static final Font FONT = new Font("Times New Roman", Font.BOLD, 46);
	private static final Font FOOT_FONT = new Font("Courier", Font.ITALIC, 14);
	private static final Color FOOT_COLOR = Color.BLACK;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {	    	

		Map<String, Object> model = new HashMap<String, Object>();
		// <img src="/image.jhtm?uid=XXXX&cid=XXXX&campId=XXXX"
		//  Save this for the Campaign as opened.
		
		response.setContentType("image/jpg");
		ServletOutputStream out = response.getOutputStream();
		BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_BYTE_INDEXED);
		Graphics graphics = image.getGraphics();
		graphics.setColor(BACKGROUND_COLOR);
		graphics.fillRect(0, 0, image.getWidth(), image.getHeight());
		graphics.setColor(COLOR);
		graphics.setFont(FONT);
		graphics.drawString("", 10, HEIGHT/2);
		graphics.setFont(FOOT_FONT);
		graphics.setColor(FOOT_COLOR);
		graphics.drawString("" , 10, HEIGHT - 30);
		JPEGCodec.createJPEGEncoder(out).encode(image);
		
    	if(request.getParameter("value") != null) {    		
			try {
				String value = new String(request.getParameter("value"));
				String ori = Utilities.AESdecrypt(KEY, value);
				String delim = "[,|]+";								
				String [] values = ori.split(delim);
				
				Click open = new Click();
				ClickDetail openDetail = new ClickDetail();
				
				String email = null;
				
				if(!values[0].equals("-")) {
					openDetail.setUserId(Integer.parseInt(values[0]));			
					email = this.webJaguar.getUserName(openDetail.getUserId(), null);
				} else { 
					openDetail.setContactId(Integer.parseInt(values[1]));
					email = this.webJaguar.getUserName(null, openDetail.getContactId());
				}
				
				openDetail.setEmail(email);
				open.setCampaignId(Integer.parseInt(values[2]));
				open.setClickDetail(openDetail);
				this.webJaguar.updateOpenCount(open);
				
			} catch ( Exception e){
				//System.out.println("Exc: " +e);
			}
    	}
        return null;
    }
}