/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 06.02.2009
 */

package com.webjaguar.web.frontend.rss;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.io.SyndFeedOutput;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
public class RSSView extends AbstractView{
	
	public RSSView() {}

	protected void renderMergedOutputModel(Map model, HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		SyndFeed feed = createFeed(siteConfig);
		List<SyndEntry> entries = new ArrayList<SyndEntry>();
		List<Product> products = (List<Product>) model.get( "products" );
		
		for(Product product: products) {
			entries.add( createEntry(product, siteConfig) );
		}
		feed.setEntries( entries );

		SyndFeedOutput output = new SyndFeedOutput(); // Create the outputter
		output.output(feed,response.getWriter()); // write the feed to the output
		
	}
	
	private SyndFeed createFeed(Map<String, Configuration> siteConfig) {
		SyndFeed feed = new SyndFeedImpl();
		feed.setFeedType( "rss_2.0" );
		feed.setAuthor( siteConfig.get("SITE_URL").getValue() );
		feed.setTitle( "Product" );
		feed.setDescription( "description" );
		feed.setLink( siteConfig.get("SITE_URL").getValue() );
		return feed;
	}
	
	private SyndEntry createEntry(Product product, Map<String, Configuration> siteConfig) {
		SyndEntry entry = new SyndEntryImpl();
		entry.setTitle( "Product Name " + product.getName() );
		entry.setLink( siteConfig.get("SITE_URL").getValue() + "product.jhtm?id=" + product.getId() );
		entry.setPublishedDate( product.getCreated() );
		SyndContent content = new SyndContentImpl();
		content.setType( "text/html" );
		content.setValue( product.getLongDesc() );
		entry.setDescription( content );
		return entry;
	}
}
