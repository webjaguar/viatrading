/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 06.02.2009
 */

package com.webjaguar.web.frontend.rss;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.web.domain.Constants;

	public class RSSController extends WebApplicationObjectSupport implements Controller {
		
		private WebJaguarFacade webJaguar;
		public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
			
	    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
	    	if ( !(Boolean) gSiteConfig.get( "gRSS" ) ) {
	    		return null;
	    	}
	    	Map<String, Object> model = new HashMap<String, Object>();
	    	String type = null;
	    	String path = request.getServletPath();		// pattern /category.rss
			try {
				type = path.substring(1, path.length()-4);			
			} catch (Exception e) {
				type = null;
			}
	    	if ( type.equals( "category" ) ) {
	    		Integer categoryId = ServletRequestUtils.getIntParameter( request, "cid", -1 );
	    		Category category = this.webJaguar.getCategoryById( categoryId, Constants.FULL_PROTECTED_ACCESS );
	    		// set the category limit inside the category form.
	    		List<Product> products = this.webJaguar.getProductsByCategoryId( category, null, request, new ProductSearch(), false );
	    		model.put("products", products);
	    	} else 
	    		return null;
	    	model.put("request", request);	
	    	model.put("context", getApplicationContext());	
			return new ModelAndView("rssView", model);
		}
	}

