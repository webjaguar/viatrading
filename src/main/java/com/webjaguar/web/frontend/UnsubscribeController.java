/* Copyright 2005, 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 12.16.2008
 */

package com.webjaguar.web.frontend;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.crm.CrmContact;

public class UnsubscribeController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private MailSender mailSender;
    public void setMailSender(MailSender mailSender) { this.mailSender = mailSender; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException, IOException {
    	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
    	Map<String, Object> myModel = new HashMap<String, Object>();
		
		if(request.getParameter("__send") != null){
			
			String email = ServletRequestUtils.getStringParameter( request, "email", null ); 
			String token = ServletRequestUtils.getStringParameter( request, "token", null ); 
			String crmToken = ServletRequestUtils.getStringParameter( request, "crmToken", null );
			String unsubscribeReason = ServletRequestUtils.getStringParameter( request, "unsubscribeReason", null );
			String otherReasons = ServletRequestUtils.getStringParameter( request, "otherReasons", null );
			String campaignId = ServletRequestUtils.getStringParameter( request, "campaignId", null );
			if (email == null) {
				return new ModelAndView(new RedirectView("home.jhtm"));
			} else if (token != null && crmToken==null) {
				// customer
				Customer customer = this.webJaguar.getCustomerByUsername( email );
				if ( customer == null || !customer.getToken().equals( token )) {
					return new ModelAndView(new RedirectView("home.jhtm"));
				} else {
					// set unsubscribe reason
					if(unsubscribeReason != null && !unsubscribeReason.equals("")){
						customer.setUnsubscribeReason(unsubscribeReason);
					}else if(otherReasons != null){
						customer.setUnsubscribeReason(otherReasons);
						
					}
					// update customer unsubscribe flag
					this.webJaguar.unsubscribeByCustomerId( customer.getId(), customer.getUnsubscribeReason() );
					this.webJaguar.insertUnsubscribeDetails(campaignId, customer.getId(), email);
					if ( (Boolean) gSiteConfig.get("gCRM")) {
						List<CrmContact> crmContactList = this.webJaguar.getCrmContactByEmail1( email );
						for (CrmContact crmContact:crmContactList) {
							if (crmContact.getToken().equals( crmToken )) {
								// update crmContact unsubscribe flag
								this.webJaguar.unsubscribeByCrmContactId(crmContact.getId());
								this.webJaguar.insertUnsubscribeDetails(campaignId, crmContact.getId(), email);
							}
						}
					}
					myModel.put("message", "Your email has been successfully removed from our list.");
//					notifyAdmin(siteConfig, email);
				}
			} else if (token == null && crmToken!=null) {
				// crm
				List<CrmContact> crmContactList = this.webJaguar.getCrmContactByEmail1( email );
				boolean crmEmailUnsubscribed = false;
				for (CrmContact crmContact:crmContactList) {
					if (crmContact.getToken().equals( crmToken )) {
						// update crmContact unsubscribe flag
						this.webJaguar.unsubscribeByCrmContactId(crmContact.getId());
						this.webJaguar.insertUnsubscribeDetails(campaignId, crmContact.getId(), email);
						crmEmailUnsubscribed = true;
					}
				}
				if (crmEmailUnsubscribed) {
					Customer customer = this.webJaguar.getCustomerByUsername( email );
					if (customer != null) {
						// set unsubscribe reason
						if(unsubscribeReason != null && !unsubscribeReason.equals("")){
							customer.setUnsubscribeReason(unsubscribeReason);
						}else if(otherReasons != null){
							customer.setUnsubscribeReason(otherReasons);
						}
						// update customer unsubscribe flag
						this.webJaguar.unsubscribeByCustomerId( customer.getId(), customer.getUnsubscribeReason() );
						this.webJaguar.insertUnsubscribeDetails(campaignId, customer.getId(), email);
					}
					myModel.put("message", "Your email has been successfully removed from our list.");
//					notifyAdmin(siteConfig, email);
				}
			} else {
				return new ModelAndView(new RedirectView("home.jhtm"));
			}
		}
		
    	
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 

		return new ModelAndView("frontend/unsubscribe", "model", myModel);    	
    }

    private void notifyAdmin(Map<String, Configuration> siteConfig, String email) {
		// send email notification
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Unsubscribe email on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" +
					"The following email address has been unsubscribed from your email list.\n\n" + email);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}
}