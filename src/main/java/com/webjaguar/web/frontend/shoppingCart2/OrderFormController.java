/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 */

package com.webjaguar.web.frontend.shoppingCart2;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import com.buySafe.ShoppingCartAddUpdateRS;
import com.buySafe.ShoppingCartCheckoutRS;
import com.sun.org.apache.xml.internal.serialize.OutputFormat;
import com.sun.org.apache.xml.internal.serialize.XMLSerializer;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.Brand;
import com.webjaguar.model.BudgetProduct;
import com.webjaguar.model.Cart;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CreditCard;
import com.webjaguar.model.Customer;
import com.webjaguar.model.GiftCard;
import com.webjaguar.model.GiftCardStatus;
import com.webjaguar.model.Inventory;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Product;
import com.webjaguar.model.Promo;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.Supplier;
import com.webjaguar.model.UserSession;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.thirdparty.evergreen.EvergreenApi;
import com.webjaguar.thirdparty.linkShare.LinkShare;
import com.webjaguar.thirdparty.payment.authorizenet.AuthorizeNetApi;
import com.webjaguar.thirdparty.payment.cardinal.CentinelApi;
import com.webjaguar.thirdparty.payment.ezic.EzicApi;
import com.webjaguar.thirdparty.payment.geMoney.GEMoney;
import com.webjaguar.thirdparty.payment.geMoney.GEMoneyValidator;
import com.webjaguar.thirdparty.payment.paymentech.PaymentechApi;
import com.webjaguar.thirdparty.payment.payrover.PayRoverApi;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.Encoder;
import com.webjaguar.web.form.OrderForm;
import com.webjaguar.web.validator.OrderFormValidator;

public class OrderFormController extends AbstractWizardFormController
{
	private GlobalDao globalDao;
	public void setGlobalDao(GlobalDao globalDao) { this.globalDao = globalDao; }
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	private Map<String, Object> gSiteConfig;
	private Map<String, Configuration> siteConfig;
	
	public OrderFormController()
	{
		setCommandName( "orderForm" );
		setCommandClass( OrderForm.class );
		setPages( new String[] { "frontend/checkout2/shipping", "frontend/checkout2/shippingmethod", "frontend/checkout2/billing", "frontend/checkout2/billingmethod",
				"frontend/checkout2/preliminary", "frontend/checkout2/attach", "frontend/checkout2/centinel" } );
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {
		super.initBinder( request, binder );
		SimpleDateFormat df = new SimpleDateFormat( "MM-dd-yyyy", RequestContextUtils.getLocale( request ) );
		binder.registerCustomEditor( Date.class, new CustomDateEditor( df, true, 10 ) );	
	}

	protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) throws Exception
	{
		OrderForm orderForm = (OrderForm) command;

		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> myModel = new HashMap<String, Object>();

		Layout layout = (Layout) request.getAttribute( "layout" );
		layout.setHideLeftBar( true );
		// hide right bar
		layout.setRightBarTopHtml( "" );
		layout.setRightBarBottomHtml( "" );

		switch ( page )
		{
		case 0:
			myModel.put( "countrylist", this.webJaguar.getEnabledCountryList() );
			myModel.put( "statelist", this.webJaguar.getStateList("US") );
			myModel.put( "caProvinceList", this.webJaguar.getStateList("CA") );
			myModel.put( "addressBook", this.webJaguar.getAddressListByUserid( orderForm.getOrder().getUserId() ) );
			myModel.put("shippingLayout", this.webJaguar.getSystemLayout("shipping2", request.getHeader("host"), request));
			break;
		case 1:
			if (request.getParameter("_viewcart.x") != null) {
				throw new ModelAndViewDefiningException( new ModelAndView( new RedirectView("viewCart.jhtm") ) );
			}
			myModel.put("shippingLayout", this.webJaguar.getSystemLayout("shipping2", request.getHeader("host"), request));
			if ( siteConfig.get( "NO_SHIPPING" ).getValue().equals( "true" ) || ( orderForm.getCustomerShippingTitle() != null && orderForm.getCustomerShippingTitle().trim().length() > 0 ) )
			{
				List<ShippingRate> shippingRates = new ArrayList<ShippingRate>();
				shippingRates.add( new ShippingRate( "No Shipping", "0.0" ) );
				orderForm.setRatesList( shippingRates );
			}
			else
			{
				orderForm.setRatesList( this.webJaguar.calculateShippingHandling( orderForm.getOrder(), orderForm.getCustomer(),request, gSiteConfig ) );
			}
			break;
		case 2:
			myModel.put( "countrylist", this.webJaguar.getEnabledCountryList() );
			myModel.put( "statelist", this.webJaguar.getStateList("US") );
			myModel.put( "caProvinceList", this.webJaguar.getStateList("CA") );
			myModel.put( "addressBook", this.webJaguar.getAddressListByUserid( orderForm.getOrder().getUserId() ) );
			myModel.put("billingLayout", this.webJaguar.getSystemLayout("billing2", request.getHeader("host"), request));
			break;
		case 3:
			orderForm.getOrder().setCcFeeRate( null );
			myModel.put("billingLayout", this.webJaguar.getSystemLayout("billing2", request.getHeader("host"), request));
			Integer custId = null;
			if ((Boolean) gSiteConfig.get("gGROUP_CUSTOMER")) {
				custId = orderForm.getOrder().getUserId();
			}
			myModel.put("customPayments", this.webJaguar.getCustomPaymentMethodList(custId));
			if (orderForm.getShippingRateIndex() != null) {
				updateShipping( orderForm );				
			}
			this.webJaguar.updateTaxAndTotal( gSiteConfig, globalDao, orderForm.getOrder() );
			orderForm.getOrder().setCreditUsed( null );
			// expireYears
			myModel.put("expireYears", Constants.getYears(10));
			break;
		case 4:
			// multi store
			map.put("invoiceLayout", this.webJaguar.getSystemLayout("pinvoice2", request.getHeader("host"), request));
			// radio button
			if ( orderForm.getOrder().getPaymentMethod() != null && orderForm.getOrder().getPaymentMethod().equalsIgnoreCase( "vba" )) {
				orderForm.getOrder().setCreditUsed( orderForm.getOrder().getGrandTotal());
				//orderForm.getOrder().setGrandTotal(0.0);
			} else {
				// checkbox
				if ( orderForm.isApplyUserPoint() ) {
					orderForm.getOrder().setCreditUsed( orderForm.getCustomerCredit() );
				} else if (orderForm.getOrder().getCreditUsed() != null){
					// put grand total back
					orderForm.getOrder().setCreditUsed( null );
				}
				this.webJaguar.updateTaxAndTotal( gSiteConfig, globalDao, orderForm.getOrder() );
			}
			if ( (Boolean) gSiteConfig.get( "gSHOW_ELIGIBLE_PROMOS" )) {
				map.put("eligiblePromos", this.webJaguar.getEligiblePromoList( orderForm.getCustomer(), ( (Boolean) gSiteConfig.get("gMANUFACTURER") ? orderForm.getOrder() : null)));
			}
			orderForm.getOrder().setupDueDate( orderForm.getOrder().getTurnOverday(), orderForm.getOrder().getShippingPeriod() );
		case 5:
			myModel.put("attachFilesLayout", this.webJaguar.getSystemLayout("attachFiles", request.getHeader("host"), request));
			if (orderForm.getCustomerFolder() != null) {
				File file[] = orderForm.getCustomerFolder().listFiles();
				List<Map<String, Object>> attachedFiles = new ArrayList<Map<String, Object>>();
				for (int f=0; f<file.length; f++) {
					HashMap<String, Object> fileMap = new HashMap<String, Object>();
					fileMap.put("file", file[f]);
					fileMap.put("lastModified", new Date(file[f].lastModified()));
					long size = file[f].length();
					fileMap.put("size", size);
					attachedFiles.add( fileMap );				
				}
				if (!attachedFiles.isEmpty()) {
					map.put( "attachedFiles", attachedFiles);					
				}
			}
			break;
		case 6:
			myModel.put("cardinalCentinelLayout", this.webJaguar.getSystemLayout("cardinalCentinel", request.getHeader("host"), request));
			break;			
		}

		map.put( "countries", this.webJaguar.getCountryMap() );
		map.put( "model", myModel );
		return map;
	}

	protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception
	{
		OrderForm orderForm = (OrderForm) command;

		switch ( page )
		{
		case 0:
			if ( !errors.hasErrors() )
			{
				if ( orderForm.isNewShippingAddress() )
				{
					orderForm.getOrder().getShipping().setUserId( orderForm.getOrder().getUserId() );
					this.webJaguar.insertAddress( orderForm.getOrder().getShipping() );
					orderForm.setNewShippingAddress( false );
				}
				else
				{
					if ( ServletRequestUtils.getBooleanParameter( request, "save", false ) )
					{
						// update address
						Address address = orderForm.getOrder().getShipping();
						address.setUserId( orderForm.getOrder().getUserId() );
						this.webJaguar.updateAddress( address );
					}
				}
			}
			orderForm.setShippingRateIndex( null );
			break;
		case 1:
			if ( siteConfig.get("DSI").getValue().length() > 0 && 
					siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue().length() > 0 && orderForm.getCustomShippingRateIndex() != null) {
				updateCustomShipping(orderForm);
			}
			if ( orderForm.isNewShippingAddress() )
			{
				orderForm.getOrder().setShipping( new Address() );
			}
			break;
		case 2:
			if ( !errors.hasErrors() )
			{
				if ( orderForm.isNewBillingAddress() )
				{
					orderForm.getOrder().getBilling().setUserId( orderForm.getOrder().getUserId() );
					this.webJaguar.insertAddress( orderForm.getOrder().getBilling() );
					orderForm.setNewBillingAddress( false );
				}
				else
				{
					if ( ServletRequestUtils.getBooleanParameter( request, "save", false ) )
					{
						// update address
						Address address = orderForm.getOrder().getBilling();
						address.setUserId( orderForm.getOrder().getUserId() );
						this.webJaguar.updateAddress( address );
					}
				}
			}
			break;
		case 3:
			if ( orderForm.isNewBillingAddress() )
			{
				orderForm.getOrder().setBilling( new Address() );
			}
			if (request.getParameter( "order.creditCard.number" ) != null) {
				OrderFormValidator validator = (OrderFormValidator) getValidator();
				validator.validateCC( orderForm, errors, false );				
			} else {
				orderForm.getOrder().setCreditCard(new CreditCard());
				if (orderForm.getOrder().getPaymentMethod() != null && orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("Credit Card")
						&& siteConfig.get("CREDIT_CARD_PAYMENT").getValue().length() > 0) {
					// happens when javascript is disabled
					errors.rejectValue("order.creditCard.number", "order.exception.creditcard.number.please");					
				}			
			}
			if (orderForm.getOrder().getPaymentMethod() != null && orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("Credit Card")) {
				orderForm.getOrder().setCcFeeRate( Double.parseDouble( siteConfig.get( "DEFAULT_CREDITCARD_FEE_RATE" ).getValue() ));
			}
			if ( orderForm.getTempGiftCardCode() != null && !orderForm.getTempGiftCardCode().equals( "" )) {
				GiftCard giftCard = this.webJaguar.getGiftCardByCode( orderForm.getTempGiftCardCode(), true );
				if (giftCard != null ) {
					UserSession userSession = (UserSession) request.getAttribute( "userSession" );
					//Change giftCardStatus to shipped if card is redeemed
					GiftCardStatus giftCardStatus = new GiftCardStatus();
					giftCardStatus.setStatus("p");
					giftCardStatus.setGiftCardOrderId(giftCard.getGiftCardOrderId());
					giftCardStatus.setComments("Gift Card redeemed by: "+ userSession.getUsername());
					this.webJaguar.redeemGiftCardByCode( giftCard, userSession, giftCardStatus );
					orderForm.setCustomerCredit( orderForm.getCustomerCredit() + giftCard.getAmount() );
				} else
				{
					orderForm.setErrorMessage( "giftCard.giftCardNotFound" );
				}
			}
			if (orderForm.getOrder().getPaymentMethod() != null && orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("GE Money")
						&& siteConfig.get("GEMONEY_DOMAIN").getValue().length() > 0
						&& siteConfig.get("GEMONEY_MERCHANTID").getValue().trim().length() > 0
						&& siteConfig.get("GEMONEY_PASSWORD").getValue().trim().length() > 0
						) {
				GEMoneyValidator.validateGEMoneyOrder(orderForm.getOrder(), errors);
			} else {
				// reset ge money
				orderForm.getOrder().setGeMoney(new GEMoney());
			}
			this.webJaguar.updateTaxAndTotal( gSiteConfig, globalDao, orderForm.getOrder() );
			break;
		case 4:
			if ( orderForm.isNewBillingAddress() )
			{
				orderForm.getOrder().setBilling( new Address() );
			}
			if ( orderForm.isNewShippingAddress() )
			{
				orderForm.getOrder().setShipping( new Address() );
			}
			if ( orderForm.getOrder().getPromo() != null )
			{
				orderForm.setTempPromoErrorMessage( null );
				this.webJaguar.updateGrandTotal(gSiteConfig, orderForm.getOrder());
			}
			break;
		case 5:
			if (orderForm.getCustomerFolder() != null) {
				File files[] = orderForm.getCustomerFolder().listFiles();
				for (int i=0; i<files.length; i++) {
					if (request.getParameter("remove_" + i) != null) {
						files[i].delete();
					}
				}
				int allowed = (Integer) gSiteConfig.get("gORDER_FILEUPLOAD") - orderForm.getCustomerFolder().listFiles().length;
				for (int i=0; i<allowed; i++) {
					try {
						MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			    		MultipartFile file = multipartRequest.getFile("file_" + i);
			    		if (file != null && !file.isEmpty()) {
							File tempFile = new File(orderForm.getCustomerFolder(), file.getOriginalFilename());
							file.transferTo(tempFile);
			    		}
					} catch (Exception e) {
						// do nothing
					}
				}		
			}
			break;
		}
	}
	
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter( "_viewcart.x" ) != null) {
			return true;
		}
		return false;
	}

	protected Object formBackingObject(HttpServletRequest request) throws ModelAndViewDefiningException
	{
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		siteConfig = this.webJaguar.getSiteConfig();

		UserSession userSession = (UserSession) request.getAttribute( "userSession" );
		
		String manufacturerName = (String) request.getSession().getAttribute("manufacturerName");
		Cart cart = this.webJaguar.getUserCart(userSession.getUserid(), manufacturerName);
		cart.setUserId( userSession.getUserid() );
		this.webJaguar.updateCart( cart, request );
		if ( !cart.isContinueCart() || 
				// check manufacturers
				((Boolean) gSiteConfig.get("gMANUFACTURER") && !this.webJaguar.checkManufacturerMinOrder(cart, request)) ||
				// budget by brands
				((Boolean) gSiteConfig.get("gBUDGET_PRODUCT") && this.webJaguar.getBudgetByCart(cart, new HashMap<String, BudgetProduct>())) ||
				// budget by brands
				((Boolean) gSiteConfig.get("gBUDGET_BRAND") && this.webJaguar.getSubTotalPerBrand(cart, new HashMap<Brand, Double>())) ) {
			throw new ModelAndViewDefiningException( new ModelAndView( new RedirectView("viewCart.jhtm") )  );
		}
		if ( cart != null && cart.getNumberOfItems() > 0 )
		{
			OrderForm orderForm = new OrderForm();
			orderForm.setCustomer((Customer) request.getAttribute("sessionCustomer"));
			// set language
			if(!((String)gSiteConfig.get("gI18N")).isEmpty() && orderForm.getCustomer() != null){
				orderForm.getOrder().setLanguageCode(orderForm.getCustomer().getLanguageCode());
			}
			if (orderForm.getCustomer().getAddress().getStateProvince() != null && orderForm.getCustomer().getAddress().getStateProvince().trim().equals("")) {
				orderForm.getCustomer().getAddress().setStateProvinceNA(true);
			}
			orderForm.setCustomerShippingTitle( orderForm.getCustomer().getShippingTitle() );
			// To add items stored previously
			if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") && cart.getCartItems().size() > 0) {
				ShoppingCartAddUpdateRS addUpdateRS = null;
				if(request.getSession().getAttribute("buySafeCartId") == null){
					request.getSession().setAttribute("buySafeCartId", UUID.randomUUID().toString().toUpperCase().replace("-", "").substring(0, 4)+new Date().getTime());
				}
				try {
					addUpdateRS = this.webJaguar.addUpdateShoppingCartRequest(cart, null, request, ( userSession != null ? userSession.getUserid() : null), mailSender);
				} catch (Exception e) { e.printStackTrace(); } 
			
				if(addUpdateRS != null && addUpdateRS.getBondCostDisplayText() != null && !addUpdateRS.getBondCostDisplayText().equals("")){
					orderForm.getOrder().setWantsBond(true);
					orderForm.getOrder().setBondCost(addUpdateRS.getTotalBondCost());
				} else {
					orderForm.getOrder().setWantsBond(false);
					orderForm.getOrder().setBondCost(null);
				}
				orderForm.getOrder().setCartId(request.getSession().getAttribute("buySafeCartId").toString());
			}
			
			orderForm.getOrder().initOrder( orderForm.getCustomer(), cart );
			if (siteConfig.get("SHOPATRON").getValue().length() > 0 && orderForm.getCustomer().getPriceTable() == 0) {
				// regular customer
				throw new ModelAndViewDefiningException(shopatronRPC(request, siteConfig, orderForm.getOrder(), orderForm.getCustomer()));
			}
			orderForm.setPoRequired( orderForm.getCustomer().getPoRequired() );
			orderForm.setCustPayment( orderForm.getCustomer().getPayment() );
			orderForm.setCustomerCredit( orderForm.getCustomer().getCredit() );
			orderForm.getOrder().setTurnOverday( Integer.parseInt( siteConfig.get( "SHIPPING_TURNOVER_TIME" ).getValue() ) );
			orderForm.setUsername( orderForm.getCustomer().getUsername() );
			if ( siteConfig.get( "CUSTOMERS_REQUIRED_COMPANY" ).getValue().equals( "true" ) )
			{
				orderForm.setCompanyRequired( true );
			}
			if ( request.getAttribute( "promocode" ) != null && !request.getAttribute( "promocode" ).equals( "" ) ) 
			{
				orderForm.setPromocodeLink( (String) request.getAttribute( "promocode" ) );
			}
			
			// customer's temp folder
			if ((Integer) gSiteConfig.get("gORDER_FILEUPLOAD") > 0) {
				File tempFolder = new File(getServletContext().getRealPath("temp"));	
				if (!tempFolder.exists()) {
					tempFolder.mkdir();
				}
				orderForm.setCustomerFolder(new File(tempFolder, "customer_" + orderForm.getOrder().getUserId()));
				if (!orderForm.getCustomerFolder().exists()) {
					if (orderForm.getCustomerFolder().mkdir() == false) {
						// folder not created
						orderForm.setCustomerFolder(null);
					}
				}
			}

			if ( siteConfig.get("DSI").getValue().length() > 0 && siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue().length() > 0 ) {
				initCustomShippingRates(orderForm);
			}
			
			// GE Money
			orderForm.getOrder().setGeMoney(new GEMoney());
			
			// product fields
			try {
				orderForm.setProductFieldsHeader(this.webJaguar.getProductFieldsHeader(orderForm.getOrder(), (Integer) gSiteConfig.get("gPRODUCT_FIELDS")));				
			} catch (Exception e) {}
			
			// manufacturer selection
			orderForm.getOrder().setManufacturerName((String) request.getSession().getAttribute("manufacturerName"));
			
			return orderForm;
		}
		else
		{
			Map<String, Object> myModel = new HashMap<String, Object>();
			myModel.put( "mainCategories", this.webJaguar.getMainCategoryLinks( request, null) );
			myModel.put( "message", "Cart is empty." );
			ModelAndView modelAndView = new ModelAndView( "frontend/error" );
			modelAndView.addObject( "model", myModel );
			throw new ModelAndViewDefiningException( modelAndView );
		}
	}

	protected int getInitialPage(HttpServletRequest request, Object command)
	{
		return 1;
	}

	public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception
	{
		OrderForm orderForm = (OrderForm) command;
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("order", orderForm.getOrder());
		model.put("orderForm", orderForm);
		model.put("username", orderForm.getCustomer().getUsername());
		model.put("invoiceLayout", this.webJaguar.getSystemLayout("finvoice2", request.getHeader("host"), request));
		
		if(orderForm!=null && orderForm.getOrder()!=null && orderForm.getCustomer()!=null){
			orderForm.getOrder().setQualifier(orderForm.getCustomer().getQualifier());
		}
		
		if (orderForm.getOrder().getPromo() != null) {
			// promo code bug fix
			// happens when customer puts a promo code without clicking "apply promo code"
			orderForm.setTempPromoErrorMessage(null);
			this.webJaguar.updateGrandTotal(gSiteConfig, orderForm.getOrder());
		}
		
		orderForm.getOrder().setTrackcode( (String) request.getAttribute( "trackcode" ) );
		orderForm.getOrder().setOrderType( "internet" );
		orderForm.getOrder().setIpAddress( request.getRemoteAddr() );
		// order quantity and inventory management
		if ((Boolean) gSiteConfig.get("gINVENTORY")) {
			for (LineItem lineItem : orderForm.getOrder().getLineItems()) {
				if (lineItem.getProduct().getInventory() != null && !lineItem.getProduct().isNegInventory()) {
					Inventory inventory = new Inventory();
					inventory.setSku(lineItem.getProduct().getSku());
					inventory = this.webJaguar.getInventory(inventory);
					// 
					if (siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly")) {
						if (inventory.getInventoryAFS() != null && (inventory.getInventoryAFS() < lineItem.getQuantity())) {
							return new ModelAndView( new RedirectView("viewCart.jhtm"));
						}
					}
					else {
						if (inventory.getInventory() != null && (inventory.getInventory() < lineItem.getQuantity())) {
							return new ModelAndView( new RedirectView("viewCart.jhtm"));
						}
					}
				}
			}
		}
		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);
			orderForm.getOrder().setHost(request.getHeader("host"));			
		}
		
		// LinkShare
		Cookie linkShareSiteID = WebUtils.getCookie(request, "linkShareSiteID");
		if (linkShareSiteID != null) {
			LinkShare linkShare = new LinkShare();
			linkShare.setSiteID(linkShareSiteID.getValue());
			
			Cookie linkShareDate = WebUtils.getCookie(request, "linkShareDate");
			if (linkShareDate != null) {
				SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd/kk:mm:ss");
				dateFormatter.setTimeZone(TimeZone.getTimeZone("GMT"));	
				try {
					linkShare.setDateEntered(dateFormatter.parse(linkShareDate.getValue()));
				} catch (Exception e) {
					// do nothing
				}
			}
			orderForm.getOrder().setLinkShare(linkShare);
		}
		
		OrderStatus orderStatus = new OrderStatus();

		// PayPal
		if ((Integer) gSiteConfig.get( "gPAYPAL" ) > 0 && orderForm.getOrder().getPaymentMethod().equalsIgnoreCase( "paypal" ) )
		{
			orderStatus.setStatus( "xp" ); // paypal cancelled
			this.webJaguar.insertOrder( orderForm, orderStatus, request, true );
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());				
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView( "frontend/checkout2/paypalPost", model);
		}
		// NetCommerce
		if (orderForm.getOrder().getPaymentMethod().equalsIgnoreCase( "netcommerce" ) && gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals( "netcommerce" ))
		{
			orderStatus.setStatus( "xnc" ); // netcommerce cancelled
			this.webJaguar.insertOrder( orderForm, orderStatus, request, true );
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			
			if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView( "frontend/checkout2/netCommercePost", model);
		}
		// BankAudi
		if (orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("bankaudi") && gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("bankaudi")) {
			orderStatus.setStatus("xba"); // bankaudi cancelled
			this.webJaguar.insertOrder(orderForm, orderStatus, request, true);
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			
			if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView( "frontend/checkout2/bankAudiPost", model);
		}		
		// eBillme
		if (orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("ebillme")
				&& siteConfig.get("EBILLME_URL").getValue().trim().length() > 0
				&& siteConfig.get("EBILLME_MERCHANTTOKEN").getValue().trim().length() > 0
				&& siteConfig.get("EBILLME_CANCEL_URL").getValue().trim().length() > 0
				&& siteConfig.get("EBILLME_ERROR_URL").getValue().trim().length() > 0
			) {
			orderStatus.setStatus("xeb"); // ebillme cancelled
			this.webJaguar.insertOrder(orderForm, orderStatus, request, false);
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			
			if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView("frontend/checkout2/eBillmePost", model);
		}
		// google checkout
		if (orderForm.getOrder().getPaymentMethod().equalsIgnoreCase( "Google" ) && !siteConfig.get("GOOGLE_CHECKOUT_URL").getValue().isEmpty())
		{
			orderStatus.setStatus( "xg" ); // google cancelled
			this.webJaguar.insertOrder( orderForm, orderStatus, request, true );
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());				
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			
			orderForm.getOrder().setOrderId(orderForm.getOrder().getOrderId());
			Document doc = createXMLCart(orderForm, siteConfig.get("SECURE_URL").getValue()+"gcoCalculations.jhtm");
			Map<String, String> encodedCart = encodeCart(doc, siteConfig.get("GOOGLE_MERCHANT_KEY").getValue());
			model.put("encodedCart", encodedCart);
			model.put("checkOutUrl", siteConfig.get("GOOGLE_CHECKOUT_URL").getValue()+siteConfig.get("GOOGLE_MERCHANT_ID").getValue());
			
			if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView( "frontend/checkout2/googlePost", model);
		}

		// Amazon Checkout Payment 
		if (orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("amazon")
				&& siteConfig.get("AMAZON_URL").getValue().trim().length() > 0
				&& siteConfig.get("AMAZON_MERCHANT_ID").getValue().trim().length() > 0
				&& siteConfig.get("AMAZON_ACCESS_KEY").getValue().trim().length() > 0				
				&& siteConfig.get("AMAZON_SECRET_KEY").getValue().trim().length() > 0				
			) {
			orderStatus.setStatus("xap"); // amazon payment cancelled
			this.webJaguar.insertOrder(orderForm, orderStatus, request, false);
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());				
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			
			if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView("frontend/checkout2/amazonPaymentPost", model);
		}	
		
		// GE Money
		if (orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("GE Money")
				&& siteConfig.get("GEMONEY_DOMAIN").getValue().trim().length() > 0
			) {
			HttpClient client = new HttpClient(); 
			client.getState().setCredentials(
					new AuthScope(siteConfig.get("GEMONEY_DOMAIN").getValue(), 443, "myrealm"),
					new UsernamePasswordCredentials(siteConfig.get("GEMONEY_MERCHANTID").getValue().trim(), siteConfig.get("GEMONEY_PASSWORD").getValue().trim())); 
			PostMethod post = new PostMethod(request.getScheme()+"://" + siteConfig.get("GEMONEY_DOMAIN").getValue() + "/process/login.do"); 
			post.addParameter("shopperId=", request.getSession().getId()); 
			post.setDoAuthentication(true); 
			try {
				client.executeMethod(post); 
				model.put("geMoneyStrToken", post.getResponseBodyAsString().trim()); 
			} catch (Exception e) {
				errors.rejectValue("order.creditCard.number", "----", "Error");					
				model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
				return showPage(request, errors, 3);
			} finally {
				post.releaseConnection(); 
			}
			
			orderStatus.setStatus("xge"); // ge money initiated
			this.webJaguar.insertOrder(orderForm, orderStatus, request, false);			// retain cart
			if (orderForm.getCustomerFolder() != null) {
				createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());				
			}
			// custom frames
			moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());
			
			if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
				request.getSession().removeAttribute("buySafeCartId");
			}
			return new ModelAndView("frontend/checkout2/geMoneyPost", model);
		}
		
		boolean autoCharge = false;		
		if (!siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("") 
				&& ( gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ezic") 
						|| gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("payrover")
						|| gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("authorizenet")
						|| gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ebizcharge") )
				&& (orderForm.getOrder().getCreditCard().getNumber() != null
						|| orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("Credit Card"))
						) {

			// credit card attempts
			if (orderForm.getCreditCardAttempts() > 3) {
				errors.rejectValue("order.creditCard.number",  "order.exception.creditcard.attempts");					
				model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
				return showPage(request, errors, 3);
			}
			
			// Ezic
			if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ezic")) {
				orderForm.setCreditCardAttempts(orderForm.getCreditCardAttempts() + 1);
				try {
					EzicApi ezic = new EzicApi();
					autoCharge = ezic.process(orderForm.getOrder(), orderForm.getUsername(), siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth"), siteConfig);				
				} catch (Exception e) {
					errors.rejectValue("order.creditCard.number", "----", "Error");					
					model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
					return showPage(request, errors, 3);				
				}
			}
			
			// Authorize.Net
			if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("authorizenet") || gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ebizcharge")) {
				
				// cardinal centinel
				if (!siteConfig.get("CENTINEL_URL").getValue().equals("")
						&& siteConfig.get("CENTINEL_PROCESSORID").getValue().trim().length() > 0
						&& siteConfig.get("CENTINEL_MERCHANTID").getValue().trim().length() > 0				
						&& siteConfig.get("CENTINEL_TRANSACTIONPWD").getValue().trim().length() > 0) {
					
					String cardType = orderForm.getOrder().getCreditCard().getType();
					
					if (cardType.equalsIgnoreCase("VISA") || cardType.equalsIgnoreCase("MC") || cardType.equalsIgnoreCase("JCB")) {
						CentinelApi centinelApi = new CentinelApi();
						
						if (request.getParameter("_PaRes") == null) {
							// reset values
							orderForm.getOrder().getCreditCard().setEci(null);
							orderForm.getOrder().getCreditCard().setCavv(null);
							orderForm.getOrder().getCreditCard().setXid(null);
							
							if (centinelApi.lookup(request, orderForm.getOrder(), siteConfig, mailSender)) {
								// card enrolled
								return showPage(request, errors, 6);						
							}
						} else {
							try {
								boolean failure = centinelApi.authenticate(request, orderForm.getOrder(), siteConfig, request.getParameter("centinelOrderId"), request.getParameter("_PaRes"));
								if (failure) {
									// authentication failed
									request.setAttribute("centinelAuthenticationFailure", true);
									model.put("billingLayout", this.webJaguar.getSystemLayout("cardinalCentinelFailed", request.getHeader("host"), request));
									return showPage(request, errors, 3);								
								}
							} catch (Exception e) {
								// transaction should not be authorized
								request.setAttribute("centinelAuthenticationFailure", true);
								model.put("billingLayout", this.webJaguar.getSystemLayout("cardinalCentinelFailed", request.getHeader("host"), request));
								return showPage(request, errors, 3);							
							}
						}
					} else {
						// reset values
						orderForm.getOrder().getCreditCard().setEci(null);
						orderForm.getOrder().getCreditCard().setCavv(null);
						orderForm.getOrder().getCreditCard().setXid(null);
					}
				}
				
				orderForm.setCreditCardAttempts(orderForm.getCreditCardAttempts() + 1);
				try {
					AuthorizeNetApi authorizeNet = new AuthorizeNetApi();
					autoCharge = authorizeNet.authorize(orderForm.getOrder(), siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth"), false, siteConfig, gSiteConfig);
				} catch (Exception e) {
					errors.rejectValue("order.creditCard.number", "----", "Error");					
					model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
					return showPage(request, errors, 3);				
				}
			}						

			// PayRover
			if (gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("payrover")) {
				orderForm.setCreditCardAttempts(orderForm.getCreditCardAttempts() + 1);
				try {
					PayRoverApi payRover = new PayRoverApi();
					autoCharge = payRover.authorize(orderForm.getOrder(), siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth"), siteConfig);
					return showPage(request, errors, 3);
				} catch (Exception e) {
					errors.rejectValue("order.creditCard.number", "----", "Error");					
					model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
					return showPage(request, errors, 3);				
				}
			}
			
			if (!autoCharge) {
				// credit card failed
				errors.rejectValue("order.creditCard.number", "order.exception.creditcard.declined");					
				errors.rejectValue("order.creditCard.expireMonth", "----", "");	// additional info			
				model.put("billingLayout", this.webJaguar.getSystemLayout("ccfailed", request.getHeader("host"), request));
				return showPage(request, errors, 3);
			}				
		}
				
		orderStatus.setStatus( "p" ); // pending

		SiteMessage siteMessage = null;
		if (orderForm.getCustomer().isEmailNotify()) {
			// notify email
			try
			{
				Integer messageId = null;
				Integer newOrderId;
				if (multiStore != null) {
					messageId = multiStore.getMidNewOrders();
				} else {
					String languageCode =orderForm.getOrder().getLanguageCode().toString();					
					try {
						newOrderId = this.webJaguar.getLanguageSetting(languageCode).getNewOrderId();
					} catch (Exception e) {
						newOrderId= null;
					}
				    if(languageCode.equalsIgnoreCase((LanguageCode.en).toString()) || newOrderId == null ) {
				    	messageId = Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_NEW_ORDERS" ).getValue() );
				    } else {
						messageId = Integer.parseInt(newOrderId.toString());
					}					
				}
				siteMessage = this.webJaguar.getSiteMessageById( messageId );
				if ( siteMessage != null )
				{
					orderStatus.setUserNotified( true );
				}
			}
			catch ( NumberFormatException e )
			{
				// do nothing
			}
		}

		this.webJaguar.insertOrder( orderForm, orderStatus, request, true );
		if (autoCharge) {
			this.webJaguar.updateCreditCardPayment(orderForm.getOrder());
			if (orderForm.getOrder().getCreditCard().getEci() != null) {
				this.webJaguar.insertCreditCardHolderAuthentication(orderForm.getOrder());
			}
		}
		
		if (!siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("") && gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("paymentech") 
				&& gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("paymentech")
				&& (orderForm.getOrder().getCreditCard().getNumber() != null
						|| orderForm.getOrder().getPaymentMethod().equalsIgnoreCase("Credit Card"))
						) {
			// Paymentech
			try {
				PaymentechApi paymentech = new PaymentechApi();
				boolean result = paymentech.process(orderForm.getOrder(), siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth"), siteConfig);
				if (!result) {
					orderForm.getOrder().getCreditCard().setPaymentStatus("FAIL");
				}
			} catch (Exception e) {
				orderForm.getOrder().getCreditCard().setPaymentStatus("FAITH");
			}
			this.webJaguar.updateCreditCardPayment(orderForm.getOrder());
		}
		
		if (orderForm.getCustomerFolder() != null) {
			createZipFile(orderForm.getOrder().getOrderId(), orderForm.getCustomerFolder());
		}
		// custom frames
		moveCustomImages(orderForm.getOrder().getOrderId(), orderForm.getOrder().getLineItems());

		// clear user cart from session
		request.getSession().removeAttribute( "userCart" );

		if ( siteMessage != null )
		{
			// send email
			orderStatus.setSubject( siteMessage.getSubject() );
			orderStatus.setMessage( siteMessage.getMessage() );
			orderStatus.setHtmlMessage( siteMessage.isHtml() );
			notifyCustomer( orderStatus, request, orderForm.getOrder(), multiStore, orderForm.getCustomer() );

		}

		/* Moved to Admin (Invoice Controller) email should be sent only after the order is shipped
		if ( (Boolean) gSiteConfig.get("gCUSTOMER_SUPPLIER") ) {
			SiteMessage supplierSiteMessage = null;
			try {
				supplierSiteMessage = this.webJaguar.getSiteMessageById(Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_SUPPLIER_ORDERS").getValue()));				
			} catch (NumberFormatException e) { 
				// do nothing
			}
			if (supplierSiteMessage != null) {
				Set<String> supplierEmails = new HashSet();
				for (LineItem lineItem : orderForm.getOrder().getLineItems()) {
					OrderStatus tempOrderStatus = new OrderStatus(orderForm.getOrder().getOrderId(), "p", supplierSiteMessage.getSubject(), supplierSiteMessage.getMessage() );
					Supplier supplier = this.webJaguar.getProductSupplierPrimaryBySku(lineItem.getProduct().getSku());
					//Supplier supplier = this.webJaguar.getSupplierById( lineItem.getSupplier().getId() );
					if ( supplier != null && supplier.getAddress() != null && !supplierEmails.contains( supplier.getAddress().getEmail() ) ) {
						supplierEmails.add( supplier.getAddress().getEmail() );
						notifySuppliers( tempOrderStatus, request, orderForm.getOrder(), supplier );
					}
				}
			}
		} */
		
		// evergreen
		if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
			// submit order to evergeen
			EvergreenApi evergreen = new EvergreenApi();
			if (orderForm.getCustomer().getAccountNumber() == null || orderForm.getCustomer().getAccountNumber().trim().equals("")) {
				// new customer
				try {
					this.webJaguar.updateEvergreenCustomerSuc(orderForm.getCustomer().getId(),
							evergreen.SetClient(orderForm.getCustomer(), orderForm.getOrder().getShipping()));				
				} catch (Exception e) {
					
					e.printStackTrace();
					notifyAdmin(siteConfig, "Error submitting customer " + orderForm.getCustomer().getId() + " with order " + orderForm.getOrder().getOrderId() 
							+ " to evergreen webservice.\nn" + e.toString(), true);
				}				
			}
			try {
				for (LineItem lineItem: orderForm.getOrder().getLineItems()) {
					evergreen.SetOrderDetail(lineItem, false);					
				}
				this.webJaguar.updateEvergreenOrderSuc(orderForm.getOrder().getOrderId(), 
							evergreen.SetOrderHeader(orderForm.getOrder(), orderForm.getCustomer(), false));
			} catch (Exception e) {
				
				e.printStackTrace();
				notifyAdmin(siteConfig, "Error submitting order " + orderForm.getOrder().getOrderId() + " to evergreen webservice.\n" + e.toString(), true);
			}
		}
		
		// buy safe
		if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
			ShoppingCartCheckoutRS checkOutResponse = null;
			try {
				checkOutResponse = this.webJaguar.setShoppingCartCheckout( orderForm.getOrder(), mailSender );
			} catch (Exception e) { 
				e.printStackTrace();
				OrderStatus status = new OrderStatus();
				status.setOrderId( orderForm.getOrder().getOrderId() );
				status.setStatus( "p" ); // pending
				if( (orderForm.getOrder().getWantsBond() != null) && (orderForm.getOrder().getWantsBond() == true) ) {
					status.setComments("BuySafe Bond was selected but cancelled as system failed to connect Buysafe. Please check the amount charged and refund the bond cost to Customer.");
				} else {
					status.setComments("BuySafe Bond was not selected. System failed to connect Buysafe");
				}
				orderForm.getOrder().setWantsBond(false);
				orderForm.getOrder().setBondCost(null);
				orderForm.getOrder().setGrandTotal();
				
				this.webJaguar.cancelBuySafeBond(orderForm.getOrder(), status);
				
			}
			model.put("buySafeResponse", checkOutResponse);
			request.getSession().removeAttribute("buySafeCartId");
		}
		
		model.put( "countries", this.webJaguar.getCountryMap() );
		return new ModelAndView( "frontend/checkout2/finalinvoice", model );
	}

	protected void validatePage(Object command, Errors errors, int page)
	{
		OrderFormValidator validator = (OrderFormValidator) getValidator();
		OrderForm orderForm = (OrderForm) command;
		switch ( page )
		{
		case 0:
			validator.validateShippingAddress( command, errors );
			break;
		case 1:
			if (orderForm.getOrder().ishasRegularShipping()) {
				validator.validateShippingSelection( errors );				
			}
			if (orderForm.getCustomRatesList() != null && orderForm.getCustomRatesList().size() > 0) {
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "customShippingRateIndex", "shipping.required");				
			}
			break;
		case 2:
			validator.validateBillingAddress( command, errors );
			break;
		case 3:
			validator.validatePaymentMethod(this.webJaguar.getCustomPaymentMethodList(orderForm.getOrder().getUserId()), command, errors);
			break;
		case 4:
			validatePromoCode( command, errors );
			if ( orderForm.getPoRequired() != null && orderForm.getPoRequired().equals( "required" )) {
				validator.validatePurchaseOrder( command, errors );
			}
			break;
		}
	}

	private void validatePromoCode(Object command, org.springframework.validation.Errors errors)
	{
		OrderForm orderForm = (OrderForm) command;
		String tempPromoCode = null;
		orderForm.setTempPromoErrorMessage(null);

		if ( (orderForm.getOrder().getPromoCode() != null && !orderForm.getOrder().getPromoCode().equals( "" )) || orderForm.getPromocodeLink() != null )
		{
			if ( orderForm.getOrder().getPromoCode() == null || orderForm.getOrder().getPromoCode().equals( "" )) {
				tempPromoCode = orderForm.getPromocodeLink();
			}
			else
				tempPromoCode = orderForm.getOrder().getPromoCode();

			Promo promo = this.webJaguar.getPromoByName(tempPromoCode);
	    	if (promo != null) {
				
				Map<String, Object> conditionPromo = new HashMap<String, Object>();
				conditionPromo.put("tempPromoCode", tempPromoCode);
				conditionPromo.put("minOrder",orderForm.getOrder().getSubTotal());
				conditionPromo.put("userId",orderForm.getOrder().getUserId());
				conditionPromo.put("orderShippingId",orderForm.getOrder().getCustomShippingId());
				
				if( (Boolean)gSiteConfig.get("gSALES_PROMOTIONS_ADVANCED") ){
					conditionPromo.put("state", orderForm.getCustomer().getAddress().getStateProvince());
					conditionPromo.put("groupType",promo.getGroupType());
		    		conditionPromo.put("stateType",promo.getStateType());
		    		conditionPromo.put("customShippingId",promo.getShippingIdSet());
				}
				if(this.webJaguar.getPromoWithCondition( promo, conditionPromo ) != null){
	    			if( promo.getDiscountType().equalsIgnoreCase("product") ) {
						
	    				Map<String, Double> brandTotalMap = new HashMap<String, Double>();
	    				for(LineItem lineItem : orderForm.getOrder().getLineItems()) {
	    					if(lineItem.getManufactureName() != null && !lineItem.getManufactureName().equals("")) {
	    						Double newTotal = lineItem.getTotalPrice();
	    						if(brandTotalMap.get(lineItem.getManufactureName()) != null) {
	    							newTotal = newTotal + brandTotalMap.get(lineItem.getManufactureName());
	    						}
	    						brandTotalMap.put(lineItem.getManufactureName(), newTotal);
	    					} 
	    				}
	    				
	    				Boolean validPromo = false;
	    				for(LineItem lineItem: orderForm.getOrder().getLineItems()){
	    					if( promo.getSkuSet().contains( lineItem.getProduct().getSku() )  || ( lineItem.getProduct().getManufactureName() != null && promo.getBrandSet().contains( lineItem.getProduct().getManufactureName()) )) {
	    						if(lineItem.getManufactureName() != null && brandTotalMap.get(lineItem.getManufactureName()) != null && promo.getMinOrderPerBrand() != null && brandTotalMap.get(lineItem.getManufactureName()) < promo.getMinOrderPerBrand()) {
		    						break;
			    				}
			    				
			    				if( !promo.isProductAvailability() ) {
			    					lineItem.setPromo(promo);
			    					validPromo = true;
						    	} else {
						    		lineItem.getProduct().setField6(this.webJaguar.getProductAvialabilityById(lineItem.getProduct().getId()));
				    				if( lineItem.getProduct().getField6() != null && lineItem.getProduct().getField6().equalsIgnoreCase("Available") ) {
				    					lineItem.setPromo(promo);
				    					validPromo = true;
								    } else {
								    	validPromo = validPromo || false;
								    }	
						    	}
			    			} else {
			    				validPromo = validPromo || false;
			    			}
			    			
			    		}
	    				if(!validPromo){
	    					orderForm.setTempPromoErrorMessage( "form.invalidPromo" );
	    				}
	    			} else {
	    				orderForm.getOrder().setPromo(promo);
	    			}
	    			
		    	} else {
		    		orderForm.getOrder().setPromo( null );
					orderForm.setTempPromoErrorMessage( "form.invalidPromo" );
				}
	    	}
	    	else{
	    		orderForm.setTempPromoErrorMessage("form.invalidPromo");
	    	}
		} 
	}

	protected void updateShipping(OrderForm orderForm)
	{
		ShippingRate shippingRate = orderForm.getRatesList().get( (new Integer( orderForm.getShippingRateIndex() ).intValue()) );
		orderForm.getOrder().setShippingMethod( shippingRate.getTitle() );
		orderForm.getOrder().setShippingCarrier(shippingRate.getCarrier());
		orderForm.getOrder().setCustomShippingId( shippingRate.getId() );
		try {
			orderForm.getOrder().setShippingPeriod( Integer.parseInt( shippingRate.getShippingPeriod() ) );
		} catch (Exception e) { }
		orderForm.getOrder().setDueDate( shippingRate.getDeliveryDate() );
		if (shippingRate.getPrice() != null) {
			orderForm.getOrder().setShippingCost( Double.parseDouble( shippingRate.getPrice() ) );			
		} else {
			orderForm.getOrder().setShippingCost(null);
		}
	}

	private void notifyCustomer(OrderStatus orderStatus, HttpServletRequest request, Order order, MultiStore multiStore, Customer customer)
	{

		SalesRep salesRep = null;
		if ( (Boolean) gSiteConfig.get( "gSALES_REP" ) && order.getSalesRepId() != null ) {
			salesRep = this.webJaguar.getSalesRepById( order.getSalesRepId() );
		}
		// send email
		String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
		String secureUrl = siteConfig.get( "SECURE_URL" ).getValue();
		if (multiStore != null) {
			contactEmail = multiStore.getContactEmail();
			secureUrl = request.getScheme()+"://" + multiStore.getHost() + request.getContextPath() + "/";
		}
		SimpleDateFormat dateFormatter = new SimpleDateFormat( "MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale( request ) );

		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append( dateFormatter.format( new Date() ) + "\n\n" );

		// replace dyamic elements
		try {
			this.webJaguar.replaceDynamicElement( orderStatus, customer, salesRep, order, secureUrl, null);
		} catch (Exception e) {e.printStackTrace();}
		messageBuffer.append( orderStatus.getMessage() );

		File tempFolder = new File(getServletContext().getRealPath("temp"));
		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}
		File pdfFile = new File(tempFolder, "order_" + order.getOrderId() + ".pdf");
		try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.setBcc(contactEmail);
			if ( salesRep != null )
				helper.setCc( salesRep.getEmail() );
			helper.setSubject( orderStatus.getSubject() );
			if ( orderStatus.isHtmlMessage() ) { 
	    		helper.setText( messageBuffer.toString(), true );
	    	} else {
	    		helper.setText(messageBuffer.toString());
	    	}
			if ((Boolean) gSiteConfig.get("gPDF_INVOICE") && siteConfig.get( "ATTACHE_PDF_ON_PLACE_ORDER" ).getValue().equals( "true" )) {
				// attach pdf invoice
				if (this.webJaguar.createPdfInvoice(pdfFile, order.getOrderId(), request)) {
					helper.addAttachment("order_" + order.getOrderId() + ".pdf", pdfFile);    						
				}
			}
			mailSender.send(mms);
		} catch (Exception ex) {
			if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
				ex.printStackTrace();				
			}
		}    			
		if (pdfFile.exists()) {
			pdfFile.delete();
		}
	}

	private void notifySuppliers(OrderStatus orderStatus, HttpServletRequest request, Order order, Supplier supplier)
	{
		// send email
		String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
		String secureUrl = siteConfig.get( "SECURE_URL" ).getValue();
		SimpleDateFormat dateFormatter = new SimpleDateFormat( "MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale( request ) );

		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append( dateFormatter.format( new Date() ) + "\n\n" );

		// subject
		orderStatus.setSubject( orderStatus.getSubject().replace( "#firstname#", (supplier.getAddress().getFirstName() != null) ? supplier.getAddress().getFirstName() : "" ) );
		orderStatus.setSubject( orderStatus.getSubject().replace( "#lastname#", (supplier.getAddress().getLastName() != null) ? supplier.getAddress().getLastName() : "" ) );
		orderStatus.setSubject( orderStatus.getSubject().replace( "#email#", (supplier.getAddress().getEmail() != null) ? supplier.getAddress().getEmail() : "" ) );
		orderStatus.setSubject( orderStatus.getSubject().replace( "#order#", order.getOrderId().toString() ) );
		orderStatus.setSubject( orderStatus.getSubject().replace( "#status#", getMessageSourceAccessor().getMessage( "orderStatus_" + orderStatus.getStatus() ) ) );
		orderStatus.setSubject( orderStatus.getSubject().replace( "#tracknum#", (orderStatus.getTrackNum() != null) ? orderStatus.getTrackNum() : "" ) );
		orderStatus.setSubject( orderStatus.getSubject().replace( "#orderlink#", secureUrl + "supplierInvoice.jhtm?orderId=" + orderStatus.getOrderId() ) );

		// message
		orderStatus.setMessage( orderStatus.getMessage().replace( "#firstname#", (supplier.getAddress().getFirstName() != null) ? supplier.getAddress().getFirstName() : "" ) );
		orderStatus.setMessage( orderStatus.getMessage().replace( "#lastname#", (supplier.getAddress().getLastName() != null) ? supplier.getAddress().getLastName() : "" ) );
		orderStatus.setMessage( orderStatus.getMessage().replace( "#email#", (supplier.getAddress().getEmail() != null) ? supplier.getAddress().getEmail() : "" ) );
		orderStatus.setMessage( orderStatus.getMessage().replace( "#order#", order.getOrderId().toString() ) );
		orderStatus.setMessage( orderStatus.getMessage().replace( "#status#", getMessageSourceAccessor().getMessage( "orderStatus_" + orderStatus.getStatus() ) ) );
		orderStatus.setMessage( orderStatus.getMessage().replace( "#tracknum#", (orderStatus.getTrackNum() != null) ? orderStatus.getTrackNum() : "" ) );
		orderStatus.setMessage( orderStatus.getMessage().replace( "#orderlink#", secureUrl + "supplierInvoice.jhtm?orderId=" + orderStatus.getOrderId() ) );

		messageBuffer.append( orderStatus.getMessage() );

		try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(supplier.getAddress().getEmail());
			helper.setFrom(contactEmail);
			helper.setBcc( contactEmail );
			helper.setSubject( orderStatus.getSubject() );
			if ( orderStatus.isHtmlMessage() ) { 
	    		helper.setText( messageBuffer.toString(), true );
	    	} else {
	    		helper.setText(messageBuffer.toString());
	    	}
			
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing
			ex.printStackTrace();
		}    			
	}

	private void createZipFile(int orderId, File customerFolder) {
		if (customerFolder == null) {
			return;
		} else if (customerFolder.listFiles().length == 0) {
			customerFolder.delete();
			return;
		}
		
	    // Create a buffer for reading the files
	    byte[] buf = new byte[1024];

	    File zipFolder = new File(getServletContext().getRealPath("/assets/orders_zip/"));
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				zipFolder = new File((String) prop.get("site.root"), "assets/orders_zip");
			}
		} catch (Exception e) {}   	
		if (!zipFolder.exists()) {
			zipFolder.mkdir();
		}
		
	    try {
	        // Create the ZIP file
	        File zipFile = new File(zipFolder, orderId  + ".zip");
	        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipFile));
	    
	        // Compress the files
	        File file[] = customerFolder.listFiles();
	        for (int i=0; i<file.length; i++) {
	            FileInputStream in = new FileInputStream(file[i]);
	    
	            // Add ZIP entry to output stream.
	            out.putNextEntry(new ZipEntry(file[i].getName()));
	    
	            // Transfer bytes from the file to the ZIP file
	            int len;
	            while ((len = in.read(buf)) > 0) {
	                out.write(buf, 0, len);
	            }
	    
	            // Complete the entry
	            out.closeEntry();
	            in.close();
	        }
	    
	        // Complete the ZIP file
	        out.close();
	        
	        // delete temp files and folder
	        for (int i=0; i<file.length; i++) {
	        	file[i].delete();	        	
	        }
	        customerFolder.delete();
	    } catch (IOException e) {
	    	// do nothing
	    }
	}
	
	private void moveCustomImages(int orderId, List <LineItem> lineItems) {
		
		boolean hasCustomImage = false;
		for (LineItem lineItem: lineItems) {
			if (lineItem.getCustomImageUrl() != null && lineItem.getCustomImageUrl().trim().length() > 0) {
				hasCustomImage =  true;
				break;
			}
		}

		if (!hasCustomImage) return;
		
	    File ordersCustomImageDir = new File(getServletContext().getRealPath("/assets/orders_customImage/"));
		File customImageDir = new File(getServletContext().getRealPath("/framer/pictureframer/images/FRAMED"));
		Properties prop = new Properties();
		try {
			prop.load( new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				ordersCustomImageDir = new File((String) prop.get("site.root"), "assets/customImages");
				customImageDir = new File((String) prop.get("site.root") + "/framer/pictureframer/images/FRAMED");
			}
		} catch (Exception e) {}   	
		if (!ordersCustomImageDir.exists()) {
			ordersCustomImageDir.mkdir();
		}
		ordersCustomImageDir = new File(ordersCustomImageDir, "" + orderId);
		if (!ordersCustomImageDir.exists()) {
			ordersCustomImageDir.mkdir();
		}		
		
		for (LineItem lineItem: lineItems) {
			if (lineItem.getCustomImageUrl() != null && lineItem.getCustomImageUrl().trim().length() > 0) {
				// move file
				File customImageFile = new File(customImageDir, lineItem.getCustomImageUrl());
				customImageFile.renameTo(new File(ordersCustomImageDir, lineItem.getCustomImageUrl()));
			}
		}

	}
	
	private void initCustomShippingRates(OrderForm orderForm) {
		Map<String, Double> customShippingRateMap = new HashMap<String, Double>();
		orderForm.getOrder().setCustomShippingTotalWeight( 0.0 );
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		for (LineItem lineItem : orderForm.getOrder().getLineItems()) {
			Product product = this.webJaguar.getProductById( lineItem.getProductId(), 0, false, null );
			try {
				m = c.getMethod("getField" + siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue());
				if (((String) m.invoke(product, arglist)).equals("0")) {
					// get custom shipping 
					for (int i=1; i<3; i++ ) {
						if ( siteConfig.get( "CUSTOM_SHIPPING_FIELD" + i ).getValue().length() > 0 ) {
							m = c.getMethod( "getField" + siteConfig.get( "CUSTOM_SHIPPING_FIELD" + i ).getValue() );
							String value = (String) m.invoke( product, arglist );
							int qty = (siteConfig.get("CUSTOM_SHIPPING_FIXED_COST").getValue().length() > 0 ? 1 : lineItem.getQuantity());
							lineItem.setCustomShippingCost( Double.parseDouble(value) * qty );
							if (  i == 1 ) {
								orderForm.getOrder().setCustomShippingTotalWeight( orderForm.getOrder().getCustomShippingTotalWeight() + (lineItem.getProduct().getWeight() * lineItem.getQuantity()));
							}
							if (customShippingRateMap.containsKey(siteConfig.get( "CUSTOM_SHIPPING_TITLE" + i ).getValue())) {
								customShippingRateMap.put(siteConfig.get( "CUSTOM_SHIPPING_TITLE" + i ).getValue(), customShippingRateMap.get(siteConfig.get( "CUSTOM_SHIPPING_TITLE" + i ).getValue()).doubleValue() + (Double.parseDouble(value) * qty));
							} else {
								customShippingRateMap.put(siteConfig.get( "CUSTOM_SHIPPING_TITLE" + i ).getValue(), (Double.parseDouble(value) * qty));
							}
						}
					}					
				}
			} catch (Exception e) {
				// do nothing
			}
		}
		List<ShippingRate> customRatesList = new ArrayList<ShippingRate>();
		for (int i=1; i<3; i++ ) {
			if (customShippingRateMap.containsKey(siteConfig.get("CUSTOM_SHIPPING_TITLE" + i).getValue())) {
				customRatesList.add(new ShippingRate(siteConfig.get("CUSTOM_SHIPPING_TITLE" + i).getValue(), 
						customShippingRateMap.get(siteConfig.get("CUSTOM_SHIPPING_TITLE" + i).getValue()).toString()));
			}
		}
		if (customRatesList.size() > 0) {
			orderForm.setCustomRatesList(customRatesList);
		}
	}
	
	private void updateCustomShipping(OrderForm orderForm) {
		ShippingRate shippingRate = orderForm.getCustomRatesList().get(orderForm.getCustomShippingRateIndex());
		orderForm.getOrder().setCustomShippingTitle(shippingRate.getTitle());
		orderForm.getOrder().setCustomShippingCost(Double.parseDouble(shippingRate.getPrice()));
		
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		for (LineItem lineItem : orderForm.getOrder().getLineItems()) {
			Product product = this.webJaguar.getProductById( lineItem.getProductId(), 0, false, null );
			try {
				m = c.getMethod("getField" + siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue());
				if (((String) m.invoke(product, arglist)).equals("0")) {
					for (int i=1; i<3; i++ ) {
						if ( siteConfig.get( "CUSTOM_SHIPPING_TITLE" + i ).getValue().length() > 0 && siteConfig.get( "CUSTOM_SHIPPING_TITLE" + i ).getValue().equals( orderForm.getOrder().getCustomShippingTitle() ) ) {
							m = c.getMethod( "getField" + siteConfig.get( "CUSTOM_SHIPPING_FIELD" + i ).getValue() );
							String value = (String) m.invoke( product, arglist );
							lineItem.setCustomShippingCost( Double.parseDouble(value) * (siteConfig.get("CUSTOM_SHIPPING_FIXED_COST").getValue().length() > 0 ? 1 : lineItem.getQuantity()) );
						}
					}
				}
			} catch (Exception e) {
				// do nothing
			}
		}
	}
	
	private void notifyAdmin(Map<String, Configuration> siteConfig, String message, boolean informContactEmail) {
		// send email notification
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String siteURL = siteConfig.get("SITE_URL").getValue();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		if (informContactEmail) {
			msg.setTo(contactEmail);			
			msg.setBcc("developers@advancedemedia.com");
		} else {
			msg.setTo("developers@advancedemedia.com");						
		}
		msg.setFrom(contactEmail);
		msg.setSubject("Ordering on " + siteURL);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	} 	
	
	private ModelAndView shopatronRPC(HttpServletRequest request, Map<String, Configuration> siteConfig, Order order, Customer customer) {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		ModelAndView modelAndView = new ModelAndView("frontend/checkout2/shopatron");
		modelAndView.addObject("model", model);
		
		List<Object> params = new ArrayList<Object>();
		params.add(siteConfig.get("SHOPATRON").getValue());		// mfg_id.cat_id
		params.add(order.getLineItems().size());				// num_items

		Map<String, Map<String, Object>> order_data = new HashMap<String, Map<String, Object>>();
		int line = 0;
		for (LineItem lineItem: order.getLineItems()) {
			Map<String, Object> line_item = new HashMap<String, Object>();
			line_item.put("product_id", lineItem.getProduct().getSku());
			line_item.put("name", lineItem.getProduct().getName());
			line_item.put("price", lineItem.getUnitPrice());
			line_item.put("quantity", lineItem.getQuantity());
			if (lineItem.getProduct().getWeight() != null) {
				line_item.put("weight", lineItem.getProduct().getWeight());				
			}
			
			order_data.put("item_" + ++line, line_item);			
		}
		params.add(order_data);

		try {
			XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
			config.setServerURL(new URL("https://xml.shopatron.com/xmlServer.php"));
			XmlRpcClient client = new XmlRpcClient();
			client.setConfig(config);
			
			model.put("order_id", client.execute("examples.loadOrder", params));
		} catch (XmlRpcException e) {
			model.put("exception", e); 
			notifyAdmin(siteConfig, "Error submitting customer " + order.getUserId() + " cart to shopatron.\n" + e.toString(), false);
		} catch (MalformedURLException e) {
			model.put("exception", e); 
		}
		
		if (model.get("order_id") != null) {
			// email order details
	    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
	    	String secureUrl = siteConfig.get("SECURE_URL").getValue();
	    	String siteURL = siteConfig.get("SITE_URL").getValue();
			// multi store
			List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
			MultiStore multiStore = null;
			if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
				multiStore = multiStores.get(0);
				contactEmail = multiStore.getContactEmail();
				siteURL = request.getScheme()+"://" + multiStore.getHost() + request.getContextPath() + "/";
			}
	    	
	    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
	    	
			SimpleMailMessage msg = new SimpleMailMessage();
			msg.setTo(contactEmail);			
			msg.setFrom(contactEmail);
			msg.setSubject("Shopatron Order ID: " + model.get("order_id") + " placed at " + siteURL);
			try {
				StringBuffer message = new StringBuffer();
				message.append(dateFormatter.format(new Date()));
				message.append("\n\n");
				message.append("Customer " + customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName());
				message.append("\n\n");
				message.append(secureUrl + "admin/customers/customer.jhtm?id=" + customer.getId());
				message.append("\n\n");
				
				message.append("Number of items: " + order.getLineItems().size() + "\n");
				message.append("------------------------------" + "\n");
				for (LineItem lineItem: order.getLineItems()) {
					message.append("sku = " + lineItem.getProduct().getSku() + "\n");
					message.append("desc = " + lineItem.getProduct().getName() + "\n");
					message.append("price = " + lineItem.getUnitPrice() + "\n");
					message.append("quantity = " + lineItem.getQuantity() + "\n");
					message.append("------------------------------" + "\n");
				}
				
				msg.setText(message.toString());
				mailSender.send(msg);
			} catch (MailException ex) {
				// do nothing
			}	
			
			// clear user cart from session and database
			request.getSession().removeAttribute("userCart");
			this.webJaguar.deleteShoppingCart(customer.getId(), order.getManufacturerName());
		}
		
		return modelAndView;
	}
	
	 private Document createXMLCart( OrderForm orderForm, String merchantCalculationUrl) throws Exception {
		 
			List<LineItem> lineItems = orderForm.getOrder().getLineItems();
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			Document  doc = null ;
			try {
				DocumentBuilder db = dbf.newDocumentBuilder();
				doc = db.newDocument();
			}catch(ParserConfigurationException pce) {
				System.exit(1);
			}
			
			// root element
			Element root = doc.createElement("checkout-shopping-cart");
			root.setAttribute("xmlns", "http://checkout.google.com/schema/2");
			doc.appendChild(root);
			
			// shopping cart
			Element shoppingCart = doc.createElement("shopping-cart");
			root.appendChild(shoppingCart);
			
			// private data to send order Id
			Element merchantPrivate = doc.createElement("merchant-private-data");
			shoppingCart.appendChild(merchantPrivate);
			
			Element merchantNote = doc.createElement("merchant-note");
			merchantPrivate.appendChild(merchantNote);
			merchantNote.appendChild(doc.createTextNode(""+orderForm.getOrder().getOrderId()));
			
			// line items
			Element items = doc.createElement("items");
			shoppingCart.appendChild(items);
			
			Iterator<LineItem> iter  = lineItems.iterator();
			while(iter.hasNext()) {
				LineItem lineItem = iter.next();
				Element item = createItemElement(lineItem, doc, orderForm.getOrder(), "item");
				items.appendChild(item);
			}
			
			// discount if any
			if(orderForm.getOrder().getPromo() != null){
				Element discount = createItemElement(null, doc, orderForm.getOrder(), "promo");
				items.appendChild(discount);
			}
			
			// buy safe
			if(orderForm.getOrder().getBondCost() != null){
				Element bondCost = createItemElement(null, doc, orderForm.getOrder(), "buySafe");
				items.appendChild(bondCost);
			}
			
			// check out flow support
			Element flowSupport = doc.createElement("checkout-flow-support");
			root.appendChild(flowSupport);
			
			// merchant flow support
			Element merchantFlowSupport = doc.createElement("merchant-checkout-flow-support");
			flowSupport.appendChild(merchantFlowSupport);
			
			// shipping methods
			Element shippingMethods = doc.createElement("shipping-methods");
			merchantFlowSupport.appendChild(shippingMethods);
			
			// merchant calculated shipping
			Element merchantCalculatedShipping = doc.createElement("merchant-calculated-shipping");
			if(orderForm.getOrder().getShippingMethod() != null) {
				merchantCalculatedShipping.setAttribute("name", orderForm.getOrder().getShippingMethod());
			} else if(orderForm.getOrder().getCustomShippingTitle() != null){
				merchantCalculatedShipping.setAttribute("name", orderForm.getOrder().getCustomShippingTitle());
			}
			shippingMethods.appendChild(merchantCalculatedShipping);
			
			// merchant calculations
			Element merchantCalculations = doc.createElement("merchant-calculations");
			merchantFlowSupport.appendChild(merchantCalculations);
			
			// merchant calculations url
			Element merchantCalculationsUrl = doc.createElement("merchant-calculations-url");
			merchantCalculationsUrl.appendChild(doc.createTextNode(merchantCalculationUrl));
			merchantCalculations.appendChild(merchantCalculationsUrl);
			
			// price
			Element price = doc.createElement("price");
			price.setAttribute("currency", "USD");
			Double shippingCost = 0.0;
			if(orderForm.getOrder().getShippingCost() != null) {
				shippingCost = shippingCost + orderForm.getOrder().getShippingCost();
			} 
			if( orderForm.getOrder().getCustomShippingCost() != null ){
				shippingCost = shippingCost + orderForm.getOrder().getCustomShippingCost();
			}
			price.appendChild(doc.createTextNode(""+shippingCost));
			merchantCalculatedShipping.appendChild(price);
			
			// tax tables
			Element taxTables = doc.createElement("tax-tables");
			taxTables.setAttribute("merchant-calculated", "true");
			merchantFlowSupport.appendChild(taxTables);
			
			// default tax table
			Element defaultTaxTable = doc.createElement("default-tax-table");
			taxTables.appendChild(defaultTaxTable);
			
			// tax rules
			Element taxRules = doc.createElement("tax-rules");
			defaultTaxTable.appendChild(taxRules);
			
			// default tax rule
			Element defaultTaxRule = doc.createElement("default-tax-rule");
			taxRules.appendChild(defaultTaxRule);
			
			// exempt shipping from tax
			Element shippingTaxed = doc.createElement("shipping-taxed");
			Text shippingTax =   doc.createTextNode("false");
			shippingTaxed.appendChild(shippingTax);
			defaultTaxRule.appendChild(shippingTaxed);
			
			// tax rate
			Element rate = doc.createElement("rate");
			rate.appendChild(doc.createTextNode(""+orderForm.getOrder().getTaxRate()/100));
			defaultTaxRule.appendChild(rate);
			
			// tax area
			Element taxArea = doc.createElement("tax-area");
			defaultTaxRule.appendChild(taxArea);
			
			// us state area for tax
			Element usStateArea = doc.createElement("us-state-area");
			taxArea.appendChild(usStateArea);
			
			// us state for tax
			Element state = doc.createElement("state");
			state.appendChild(doc.createTextNode(""+orderForm.getOrder().getShipping().getStateProvince()));
			usStateArea.appendChild(state);
			
			return doc;
		}

		private Element createItemElement(LineItem lineItem, Document doc, Order order, String type){

			Element item = doc.createElement("item");
			
			Element name = doc.createElement("item-name");
			if(type.equalsIgnoreCase("item")){
				name.appendChild(doc.createTextNode(lineItem.getProduct().getSku()));
			} else if(type.equalsIgnoreCase("promo")){
				name.appendChild(doc.createTextNode("Discount"));
			}
			item.appendChild(name);
			
			Element disc = doc.createElement("item-description");
			if(lineItem != null){
				disc.appendChild(doc.createTextNode(lineItem.getProduct().getShortDesc()));
			}else if(type.equalsIgnoreCase("promo")){
				disc.appendChild(doc.createTextNode("Promo code "+order.getPromoCode()));
			}else {
				disc.appendChild(doc.createTextNode("BuySafe Bond Guarantee "));
			}
			item.appendChild(disc);
			
			Element price = doc.createElement("unit-price");
			if(type.equalsIgnoreCase("item")){
				price.appendChild(doc.createTextNode(lineItem.getUnitPrice().toString()));
			}else if(type.equalsIgnoreCase("promo")){
				price.appendChild(doc.createTextNode("-"+(order.getPromoAmount()+order.getLineItemPromoAmount())));
			}else {
				price.appendChild(doc.createTextNode(order.getBondCost().toString()));
			}
			price.setAttribute("currency", "USD");
			item.appendChild(price);
			
			Element qty = doc.createElement("quantity");
			if(type.equalsIgnoreCase("item")){
				qty.appendChild(doc.createTextNode(""+lineItem.getQuantity()));
			}else {
				qty.appendChild(doc.createTextNode(""+1));
			}
			item.appendChild(qty);
			
			if(type.equalsIgnoreCase("item")){
				Element weight = doc.createElement("item-weight");
				weight.setAttribute("unit", "LB");
				weight.setAttribute("value", ""+lineItem.getProduct().getWeight()*lineItem.getQuantity());
				item.appendChild(weight);
			}
			
			return item;
		}
		
		private Map<String, String> encodeCart(Document dom, String key) throws Exception{
			
			Map<String, String> encryptedCart = new HashMap<String, String>();
			try
			{
				StringBuilder stringBuilder = null;
				
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				OutputFormat outputformat = new OutputFormat();
				outputformat.setIndenting(true);
				outputformat.setPreserveSpace(false);
				XMLSerializer serializer = new XMLSerializer();
				serializer.setOutputFormat(outputformat);
				serializer.setOutputByteStream(stream);
				serializer.asDOMSerializer();
				serializer.serialize(dom.getDocumentElement());

				stringBuilder = new StringBuilder(stream.toString());	
				Encoder encoder = new Encoder();
				String encodedCart = encoder.encode(stringBuilder.toString());
				String sign = encoder.sign(stringBuilder.toString(), key);
				     
				encryptedCart.put("encodedCart", encodedCart);
				encryptedCart.put("sign", sign);
				     
			}catch (Exception e){ }
		
		return encryptedCart;
		}
}