/* Copyright 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 11.14.2007
 */

package com.webjaguar.web.frontend.ticket;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.TicketSearch;
import com.webjaguar.model.UserSession;

public class TicketListController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
    			throws ServletException {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
    	UserSession userSession = this.webJaguar.getUserSession(request);
    	TicketSearch ticketSearch = getTicketSearch( request );    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
				
		PagedListHolder ticketList = 
			new PagedListHolder(this.webJaguar.getTicketsListByUserid(userSession.getUserid(), ticketSearch));
		ticketList.setPageSize( 10 );
		ticketList.setPage( ServletRequestUtils.getIntParameter( request, "page", 1) - 1 );
		model.put("ticketList", ticketList);
		
		setLayout(userSession, request, model); 
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/ticket/ticketList", "model", model);
		}
		return new ModelAndView("frontend/ticket/ticketList", "model", model);    	
    }

	private void setLayout(UserSession userSession, HttpServletRequest request, Map<String, Object> model) {
		Layout layout = null;
		layout = this.webJaguar.getSystemLayout("tickets", request.getHeader("host"), request);
		if(layout != null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#pageName#", "My Tickets"));
		}
		if(layout == null){
			layout = (Layout) request.getAttribute("layout");
		}
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		request.setAttribute("layout", layout);
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		}		
		//model.put("ticketsLayout", this.webJaguar.getSystemLayout("tickets", request.getHeader("host"), request));
	}
	
	private TicketSearch getTicketSearch(HttpServletRequest request) {
		TicketSearch ticketSearch = (TicketSearch) request.getSession().getAttribute( "ticketSearch" );
		if (ticketSearch == null) {
			ticketSearch = new TicketSearch();
			request.getSession().setAttribute( "ticketSearch", ticketSearch );
		}
		
		// sorting
		String sortBy = request.getParameter("sortBy");
		if (sortBy != null) {
			switch (ServletRequestUtils.getIntParameter( request, "sortBy", 10 )) {
				case 1:
					ticketSearch.setSort("created");
					break;
				default:
					ticketSearch.setSort("created DESC");
					break;
			}
		}			

		// subject
		if (request.getParameter("subject") != null) {
			ticketSearch.setSubject(ServletRequestUtils.getStringParameter( request, "subject", "" ));
		}
		
		// ticket Id
		if (request.getParameter("ticket_id") != null) {
			ticketSearch.setTicketId(ServletRequestUtils.getStringParameter( request, "ticket_id", "" ));
		}		
		
		// status
		if (request.getParameter("status") != null) {
			ticketSearch.setStatus(ServletRequestUtils.getStringParameter( request, "status", "" ));
		}	

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				ticketSearch.setPage( 1 );
			} else {
				ticketSearch.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				ticketSearch.setPageSize( 10 );
			} else {
				ticketSearch.setPageSize( size );				
			}
		}	
		return ticketSearch;
	}
}
