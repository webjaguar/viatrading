/* Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 11.16.2007
 */

package com.webjaguar.web.frontend.ticket;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.form.TicketForm;

public class TicketUpdateController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
    private File baseFile;
    
	public TicketUpdateController() {
		setSessionForm(true);
		setCommandName("ticketForm");
		setCommandClass(TicketForm.class);
		setFormView("frontend/ticket/ticketUpdate");
		setSuccessView("account_ticketUpdate.jhtm");
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
    	UserSession userSession = this.webJaguar.getUserSession(request);
    	Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());
    	TicketForm ticketForm = (TicketForm) command;
    	ticketForm.getTicket().setCompanyName(customer.getAddress().getCompany());
    	ticketForm.getTicket().setUserId( userSession.getUserid() );

		// check if update button was pressed
		if (request.getParameter("__update_ticket") != null) {
			ticketForm.getTicketVersion().setCreatedByType( "User" );
			ticketForm.getTicketVersion().setOwnerId( ticketForm.getTicket().getTicketId() );
			this.webJaguar.insertTicketVersion( ticketForm.getTicketVersion() );
			this.webJaguar.updateTicketLastModified( ticketForm.getTicketVersion().getOwnerId(), ticketForm.getTicketVersion().getCreatedByType() );
			
			// save attachement
			if ( baseFile.canWrite() ) {
				boolean uploadedAttachment = false;
				MultipartFile attachment = null;
				
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
				attachment = multipartRequest.getFile( "attachment" );
				if ( attachment != null && !attachment.isEmpty() ) {
					uploadedAttachment = true;
					if ( uploadedAttachment ) {
						File newFile = new File( baseFile, ticketForm.getTicket().getTicketId()+"_"+ ticketForm.getTicketVersion().getTicketVersionId()+"_"+ attachment.getOriginalFilename() );
						try {
							attachment.transferTo( newFile );
						} catch ( IOException e ) {
							e.printStackTrace();
						}
					}
				}
			}
			
			// construct email message
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, false, "UTF-8");
			try {
				constructMessage(helper, ticketForm, request, userSession);
				mailSender.send(mms);
			} catch (Exception e) { }
		}
		// check if close button was pressed
		if (request.getParameter("__close_ticket") != null) {
			ticketForm.getTicket().setStatus( "close" );
			this.webJaguar.updateTicketStatus( ticketForm.getTicket() );
		}
		return new ModelAndView(new RedirectView( getSuccessView() ), "id", ticketForm.getTicket().getTicketId() );
    }
    
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("__close_ticket") != null) {
			return true;			
		}
		return false;
	}

    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
    	TicketForm ticketForm = (TicketForm) command;
    	UserSession userSession = this.webJaguar.getUserSession(request);
       	Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		baseFile = new File( getServletContext().getRealPath("temp"));
		try {
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
			baseFile = new File( baseFile, "/Ticket/" );
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
			baseFile = new File( baseFile, "/customer_"+userSession.getUserid()+"/" );
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
		} catch ( Exception e ){ e.printStackTrace(); }
		
		List<TicketForm> ticketList  = this.webJaguar.getTicketVersionListById(ticketForm.getTicket().getTicketId(), userSession.getUserid());
		for(TicketForm tForm : ticketList){
			// check ticket has attachment
			File[] listOfFiles = baseFile.listFiles();
			if(listOfFiles != null) {
				for(int i=0; i<listOfFiles.length; i++){
					if(listOfFiles[i].isFile() && listOfFiles[i].getName().startsWith(tForm.getTicket().getTicketId()+"_"+tForm.getTicketVersion().getTicketVersionId() + "_")){
						tForm.getTicketVersion().setAttachment(listOfFiles[i].getName());
					}
				}	
			}
		}
		
		model.put("questionAnswerList", ticketList);
		model.put("firstName", userSession.getFirstName());
	    map.put("model", model);    	
	    
		Layout layout = (Layout) request.getAttribute("layout");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		model.put("updateTicketLayout", this.webJaguar.getSystemLayout("updateTicket", request.getHeader("host"), request));
		
	    return map;
    }  

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		TicketForm ticketForm = new TicketForm();
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if ( request.getParameter("id") != null ) {
			ticketForm = new TicketForm(this.webJaguar.getTicketById(new Integer(request.getParameter("id"))));
		} 
		
		baseFile = new File( getServletContext().getRealPath("temp"));
		try {
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
			baseFile = new File( baseFile, "/Ticket/" );
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
			baseFile = new File( baseFile, "/customer_"+ticketForm.getTicket().getUserId()+"/" );
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
		} catch ( Exception e ){ e.printStackTrace(); }
		
		//check ticket has attachment
		File[] listOfFiles = baseFile.listFiles();
		if(listOfFiles != null) {
			for(int i=0; i<listOfFiles.length; i++){
				if(listOfFiles[i].isFile() && listOfFiles[i].getName().startsWith(ticketForm.getTicket().getTicketId()+"_"+ticketForm.getTicketVersion().getTicketVersionId() + "_")){
					ticketForm.getTicketVersion().setAttachment(listOfFiles[i].getName());
				}
			}	
		}
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			setFormView("frontend/layout/template6/ticket/ticketUpdate");
		}
		return ticketForm;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		TicketForm ticketForm = (TicketForm) command;
		if (ticketForm.getTicketVersion().getQuestionDescription() == null || ticketForm.getTicketVersion().getQuestionDescription().equals( "" )) {
			errors.rejectValue("ticketVersion.questionDescription", "form.required", "required");    					
		}
	}
	
	private void constructMessage(MimeMessageHelper helper, TicketForm ticketForm, HttpServletRequest request, UserSession userSession) throws Exception {
		// send email
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		if (!((Configuration) siteConfig.get("TICKET_MESSAGE_ON_EMAIL")).getValue().equals( "true" )) ticketForm.getTicketVersion().setQuestionDescription( "" );
    	String contactEmail = ((Configuration) siteConfig.get("CONTACT_EMAIL")).getValue();
    	String ticketContactEmail = ((Configuration) siteConfig.get("TICKET_CONTACT_EMAIL")).getValue();
    	if(ticketContactEmail == null || ticketContactEmail.isEmpty()){
    		return;
    	}
    	
    	String siteName = ((Configuration) siteConfig.get("SITE_NAME")).getValue();

    	// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(ticketForm.getTicket().getHost());
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);	
		}
		
    	String siteURL = this.webJaguar.getSecureUrl(gSiteConfig, multiStore);
    	
    	helper.setTo(ticketContactEmail);
		helper.setFrom(userSession.getUsername() );	
		// cc emails

		if ( (Boolean) gSiteConfig.get("gSHOPPING_CART") && (Boolean) gSiteConfig.get("gSALES_REP")){
			SalesRep salesRep = this.webJaguar.getSalesRepByUserId( userSession.getUserid() );
			if ( salesRep != null ) {
				helper.setBcc(  new String[] { contactEmail, salesRep.getEmail() } );
			} else {
				helper.setBcc( contactEmail );
			}
		}
		else {
			helper.setBcc( contactEmail );
		}
		helper.setSubject("Updated " + siteName + " Ticket #" + ticketForm.getTicket().getTicketId());
		StringBuffer message = new StringBuffer();
		message.append("The following support ticket has been updated:<br /><br />A comment has been added to the following support ticket.<br /><br />" +
				"" + ((ticketForm.getTicket().getCompanyName()!=null && !ticketForm.getTicket().getCompanyName().isEmpty()) ? "Company:  "+ ticketForm.getTicket().getCompanyName() + "<br />" : "" ) +
				"Ticket#:  "+ticketForm.getTicket().getTicketId()+"<br />Subject: " + ticketForm.getTicket().getSubject() +
				"" + ((ticketForm.getTicket().getType() != null && !ticketForm.getTicket().getType().trim().isEmpty()) ? ("<br />Type:" + ticketForm.getTicket().getType().trim()) : "" )+
				"<br /><br />" +
				"You can view this ticket here:<br />  " + siteURL + "admin/ticket/ticketUpdate.jhtm?id=" + ticketForm.getTicket().getTicketId() + "<br /><br />" +
				"Thank you,<br />  " + siteName + "<br />" + "<a href=\"" +siteURL + "\" >" +siteURL+ "</a><br /><br />"+
				ticketForm.getTicketVersion().getQuestionDescription() );
		helper.setText(message.toString(), true);
 	}
}