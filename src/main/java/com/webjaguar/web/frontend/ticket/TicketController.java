/* Copyright 2005, 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 11.16.2007
 */

package com.webjaguar.web.frontend.ticket;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.SalesRep;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.form.TicketForm;

public class TicketController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
    private Map<String, Object> gSiteConfig;
    Map<String, Configuration> siteConfig;
    private File baseFile;
	
	public TicketController() {
		setSessionForm(true);
		setCommandName("ticketForm");
		setCommandClass(TicketForm.class);
		setFormView("frontend/ticket/ticketForm");
		setSuccessView("account.jhtm");
	}	
	
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws Exception {
    	UserSession userSession = this.webJaguar.getUserSession(request);
    	Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());
    	TicketForm ticketForm = (TicketForm) command;
    	ticketForm.getTicket().setCompanyName(customer.getAddress().getCompany());
    	if (request.getParameter("_cancel") != null) {
    		return new ModelAndView(new RedirectView("account.jhtm"));
    	}
    	// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);
			ticketForm.getTicket().setHost(request.getHeader("host"));		
		}
		
		// save attachement
		boolean uploadedAttachment = false;
		MultipartFile attachment = null;
		if ( baseFile.canWrite() ) {
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			attachment = multipartRequest.getFile( "attachment" );
			if ( attachment != null && !attachment.isEmpty() ) {
				uploadedAttachment = true;
			}
		}
		
		ticketForm.getTicket().setUserId( userSession.getUserid() );
		ticketForm.getTicket().setStatus( "open" );
		int ticketNumber = this.webJaguar.insertTicket( ticketForm.getTicket() );

		if ( uploadedAttachment ) {
			File newFile = new File( baseFile, ticketNumber+"_0_"+attachment.getOriginalFilename() );
			try {
				attachment.transferTo( newFile );
			} catch ( IOException e ) {
				e.printStackTrace();
			}
		}
		
		// construct email message
		MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms, false, "UTF-8");
		try {
			constructMessage(helper, ticketForm, request, userSession, ticketNumber, multiStore);
			mailSender.send(mms);
		} catch (Exception e) {
		}
		
		return new ModelAndView(new RedirectView(getSuccessView()));    	
    }
    
	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null) {
			return true;			
		}
		return false;
	}

    protected Map referenceData(HttpServletRequest request)  throws Exception {
	
       	Map<String, Map<String, Object>> map = new HashMap<String, Map<String, Object>>();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
	    map.put("model", model);    	
	    
		Layout layout = null;
		layout = this.webJaguar.getSystemLayout("newTicket", request.getHeader("host"), request);
		if(layout != null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#pageName#", "Submit New Ticket"));
		}
		if(layout == null){
			layout = (Layout) request.getAttribute("layout");
		}
		this.webJaguar.updateCartQuantity(null, request, layout);
		request.setAttribute("layout", layout);
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		//model.put("newTicketLayout", this.webJaguar.getSystemLayout("newTicket", request.getHeader("host"), request));
		if (!siteConfig.get("TICKET_TYPE").getValue().isEmpty()) {
			model.put("ticketType", new ArrayList<String>(Arrays.asList(siteConfig.get("TICKET_TYPE").getValue().split(","))));
		}
	    
	    return map;
    }  
    
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		gSiteConfig = (Map<String, Object>) request.getAttribute( "gSiteConfig" );
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		
		UserSession userSession = this.webJaguar.getUserSession(request);
		TicketForm ticketForm = new TicketForm();
		baseFile = new File( getServletContext().getRealPath("temp"));
		try {
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
			baseFile = new File( baseFile, "/Ticket/" );
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
			baseFile = new File( baseFile, "/customer_"+userSession.getUserid()+"/" );
			if(!baseFile.exists()){
				baseFile.mkdir();
			}
		} catch ( Exception e ){ e.printStackTrace(); }
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			setFormView("frontend/layout/template6/ticket/ticketForm");
		}
		return ticketForm;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors) throws Exception {
		TicketForm ticketForm = (TicketForm) command;
		if (ticketForm.getTicket().getSubject() == null || ticketForm.getTicket().getSubject().isEmpty()) {
			errors.rejectValue("ticket.subject", "form.required", "required");    					
		}
		if (ticketForm.getTicket().getQuestion() == null || ticketForm.getTicket().getQuestion().isEmpty()) {
			errors.rejectValue("ticket.question", "form.required", "required");    					
		}
		if (request.getHeader("host").contains("advancedemedia.com") && ( ticketForm.getTicket().getType() == null || ticketForm.getTicket().getType().isEmpty() )) {
			errors.rejectValue("ticket.type", "form.required", "required");    					
		}
	}
	
	private void constructMessage(MimeMessageHelper helper, TicketForm ticketForm, HttpServletRequest request, UserSession userSession, int ticketNumber, MultiStore multiStores) throws Exception {
		// send email
		Map siteConfig = (Map) request.getAttribute( "siteConfig" );
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		if (!((Configuration) siteConfig.get("TICKET_MESSAGE_ON_EMAIL")).getValue().equals( "true" )) ticketForm.getTicket().setQuestion( "" );
    	String contactEmail = ((Configuration) siteConfig.get("CONTACT_EMAIL")).getValue();
    	String ticketContactEmail = ((Configuration) siteConfig.get("TICKET_CONTACT_EMAIL")).getValue();
    	if(ticketContactEmail == null || ticketContactEmail.isEmpty()){
    		return;
    	}
    	
    	String siteName = ((Configuration) siteConfig.get("SITE_NAME")).getValue();
    	String siteURL = this.webJaguar.getSecureUrl(gSiteConfig, multiStores);
    	
		helper.setTo(ticketContactEmail);
		helper.setFrom(userSession.getUsername() );	
		// cc emails

		if ( (Boolean) gSiteConfig.get("gSHOPPING_CART") && (Boolean) gSiteConfig.get("gSALES_REP")){
			SalesRep salesRep = this.webJaguar.getSalesRepByUserId( userSession.getUserid() );
			if ( salesRep != null ) {
				helper.setBcc(  new String[] { contactEmail, salesRep.getEmail() } );
			} else {
				helper.setBcc( contactEmail );
			}
		}
		else {
			helper.setBcc( contactEmail );
		}

		helper.setSubject("Created " + siteName + " Ticket #" + ticketNumber);
		StringBuffer message = new StringBuffer();
		message.append("The following support ticket has been created:<br /><br />A comment has been added to the following support ticket.<br /><br />" +
				"" + ((ticketForm.getTicket().getCompanyName()!=null && !ticketForm.getTicket().getCompanyName().isEmpty()) ? "Company:  "+ ticketForm.getTicket().getCompanyName() + "<br />" : "" ) +
				"Ticket#:  "+ticketNumber+"<br />Subject: " + ticketForm.getTicket().getSubject() + 
				"" + ((ticketForm.getTicket().getType() != null && !ticketForm.getTicket().getType().trim().isEmpty()) ? ("<br />Type:" + ticketForm.getTicket().getType().trim()) : "" )+
				"<br /><br />" +
				"You can view this ticket here:<br />  " + siteURL + "admin/ticket/ticketUpdate.jhtm?id=" + ticketNumber + "<br /><br />" +
				"Thank you,<br />  " + siteName + "<br />" + siteURL + "<br /><br />"+
				ticketForm.getTicket().getQuestion().toString());
		helper.setText(message.toString(), true);
 	}
}
