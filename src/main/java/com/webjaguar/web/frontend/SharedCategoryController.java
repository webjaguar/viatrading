/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 06.19.2007
 */

package com.webjaguar.web.frontend;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.UserSession;

public class SharedCategoryController extends WebApplicationObjectSupport implements Controller {

	WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		Map<String, Object> model = new HashMap<String, Object>();

		if (((String) gSiteConfig.get("gSHARED_CATEGORIES")).equals("")) {
			return new ModelAndView(new RedirectView("home.jhtm"));
		}

		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();
		model.put("url", url + "?" + query);
		// continue shopping
		request.getSession().setAttribute("continueShoppingUrl", model.get("url"));

		Integer sid = ServletRequestUtils.getIntParameter(request, "sid", 0);

		List<Category> breadCrumbs = new ArrayList<Category>();
		List<Product> sharedProductList = new ArrayList<Product>();
		List<Product> products = new ArrayList<Product>();
		Map<Integer, Product> alsoConsiderMap = new HashMap<Integer, Product>();

		UserSession userSession = this.webJaguar.getUserSession(request);

		// product fields
		Map<String, ProductField> productFieldMap = new HashMap<String, ProductField>();
		int gPRODUCT_FIELDS = (Integer) gSiteConfig.get("gPRODUCT_FIELDS");
		boolean setNewMapping = false;
		for (int i = 1; i <= gPRODUCT_FIELDS; i++) {
			if (request.getParameter("field_" + i) != null) {
				setNewMapping = true;
				productFieldMap.put("field_" + i, new ProductField(i, request.getParameter("field_" + i)));
			}
		}
		if (setNewMapping) {
			if (request.getSession().getAttribute("sharedProductFieldMap") == null) {
				request.getSession().setAttribute("sharedProductFieldMap", new HashMap<String, Map<String, ProductField>>());
			}
			((Map<String, Map<String, ProductField>>) request.getSession().getAttribute("sharedProductFieldMap")).put("cat_" + sid, productFieldMap);
		}
		List<ProductField> productFields = new ArrayList<ProductField>();
		if (request.getSession().getAttribute("sharedProductFieldMap") != null
				&& ((Map<String, Map<String, ProductField>>) request.getSession().getAttribute("sharedProductFieldMap")).get("cat_" + sid) != null) {
			productFields.addAll(((Map<String, Map<String, ProductField>>) request.getSession().getAttribute("sharedProductFieldMap")).get("cat_" + sid).values());
		}

		Category category = getCategory((String) gSiteConfig.get("gSHARED_CATEGORIES"), sid, productFields, breadCrumbs, sharedProductList);
		String[] configProductSync = siteConfig.get("PRODUCT_SYNC").getValue().split(",");

		if (category != null) {
			boolean showPriceColumn = false;
			boolean showQtyColumn = false;
			for (Product product : sharedProductList) {
				String sku = product.getSku().trim();
				if (!sku.equals("")) {
					product = this.webJaguar.getProductById(this.webJaguar.getProductIdBySku(product.getSku()), request);
					if (product != null) {
						products.add(product);
						// also consider
						if ((Boolean) gSiteConfig.get("gALSO_CONSIDER") && siteConfig.get("ALSO_CONSIDER_DETAILS_ONLY").getValue().equals("false")) {
							Product alsoConsider = this.webJaguar.getAlsoConsiderBySku(product.getAlsoConsider(), request);
							if (alsoConsider != null) {
								alsoConsiderMap.put(product.getId(), alsoConsider);
							}
						}
						if (!showPriceColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) || product.getMsrp() != null)) {
							showPriceColumn = true;
						}
						if (!showQtyColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) && (!product.isLoginRequire() || (product.isLoginRequire() && userSession != null)))) {
							showQtyColumn = true;
						}
						if (configProductSync[0].length() > 0 && product.getThumbnail() != null && product.getThumbnail().isAbsolute()) {
							// thumbnails
							product.getThumbnail().setImageUrl(
									product.getThumbnail().getImageUrl().replace(configProductSync[0] + "assets/Image/Product/detailsbig/", configProductSync[0] + "assets/Image/Product/thumb/"));
						}
					}
				}
			}
			model.put("showPriceColumn", showPriceColumn);
			model.put("showQtyColumn", showQtyColumn);

			PagedListHolder productList = new PagedListHolder(products);

			model.put("category", category);
			model.put("breadCrumbs", breadCrumbs);
			model.put("sharedProductList", sharedProductList);
			model.put("products", productList);
			if (!alsoConsiderMap.isEmpty()) { // not empty
				model.put("alsoConsiderMap", alsoConsiderMap);
			}
			double subcatTotal = category.getSubCategories().size();
			model.put("subcatLinksPerCol", ((int) (Math.round((float) subcatTotal / 4)) == 0) ? 1 : (int) (Math.round((float) subcatTotal / 4)));
			if (siteConfig.get("SHARED_CATEGORIES_LAYOUT").getValue().equals("2")) {
				if (category.getSubcatDisplay().equals("row")) {
					model.put("subcatLinksPerCol", 1);
				} else {
					model.put("subcatLinksPerCol", (int) (Math.ceil(subcatTotal / category.getSubcatCols())));
				}
				model.put("subcatCols", category.getSubcatCols());
			}

			productList.setPageSize(products.size());
			model.put("productFields", this.webJaguar.getProductFields(request, productList.getPageList(), true));
			productList.setPageSize(new Integer(siteConfig.get("NUMBER_PRODUCT_PER_PAGE").getValue()));
			productList.setPage(ServletRequestUtils.getIntParameter(request, "page", 1) - 1);

		}

		return new ModelAndView("frontend/sharedCategory", "model", model);
	}

	private Category getCategory(String urlString, Integer cid, List<ProductField> productFields, List<Category> breadCrumbs, List<Product> sharedProductList) {
		Category category = new Category();

		try {
			URL url = new URL(urlString + "xmlService.jhtm");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("action=category");
			request.append("&sid=" + cid);
			for (ProductField pField : productFields) {
				request.append("&field_" + pField.getId() + "=" + URLEncoder.encode(pField.getValue(), "UTF-8"));
			}

			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();

			SAXBuilder builder = new SAXBuilder(false);

			Document doc = builder.build(connection.getInputStream());

			Element root = doc.getRootElement();

			category.setId(cid);
			category.setDisplayMode(root.getAttributeValue("displayMode"));
			category.setProductPerRow(Integer.parseInt(root.getAttributeValue("productPerRow")));
			category.setHasImage(root.getAttributeValue("hasImage").equals("true"));
			category.setSubcatCols(Integer.parseInt(root.getChild("SubCategories").getAttributeValue("columns")));
			category.setSubcatLevels(Integer.parseInt(root.getChild("SubCategories").getAttributeValue("levels")));
			category.setSubcatDisplay(root.getChild("SubCategories").getAttributeValue("display"));
			category.setProductPerPage(Integer.parseInt(root.getChild("SubCategories").getAttributeValue("productPerPage")));
			category.setSubcatLocation(root.getChild("SubCategories").getAttributeValue("location"));

			Iterator results = root.getChild("SubCategories").getChildren().iterator();
			while (results.hasNext()) {
				Element thisCategory = (Element) results.next();
				Category newCategory = new Category();
				newCategory.setId(Integer.parseInt(thisCategory.getAttributeValue("id")));
				newCategory.setName(thisCategory.getChild("Name").getText());
				newCategory.setHasImage(thisCategory.getAttributeValue("hasImage").equals("true"));
				Iterator subs = thisCategory.getChild("SubCategories").getChildren().iterator();
				while (subs.hasNext()) {
					Element thisSubCategory = (Element) subs.next();
					Category newSubCategory = new Category();
					newSubCategory.setId(Integer.parseInt(thisSubCategory.getAttributeValue("id")));
					newSubCategory.setName(thisSubCategory.getChild("Name").getText());
					newCategory.getSubCategories().add(newSubCategory);
				}
				category.getSubCategories().add(newCategory);
			}

			// breadcrumbs
			results = root.getChild("BreadCrumbs").getChildren().iterator();
			while (results.hasNext()) {
				Element thisCategory = (Element) results.next();
				Category newCategory = new Category();
				newCategory.setId(Integer.parseInt(thisCategory.getAttributeValue("id")));
				newCategory.setName(thisCategory.getChild("Name").getText());
				breadCrumbs.add(newCategory);
			}

			// products
			results = root.getChild("Products").getChildren().iterator();
			while (results.hasNext()) {
				Element thisProduct = (Element) results.next();
				Product newProduct = new Product();
				newProduct.setId(Integer.parseInt(thisProduct.getAttributeValue("id")));
				newProduct.setSku(thisProduct.getChild("Sku").getText());
				sharedProductList.add(newProduct);
			}

		} catch (Exception e) {
			return null;
		}

		return category;
	}

}
