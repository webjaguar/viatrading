package com.webjaguar.web.frontend;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Click;
import com.webjaguar.model.ClickDetail;
import com.webjaguar.web.domain.Utilities;

public class ClickStatController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private final static String KEY = "AECCF9008EDBF4535CCE2345435364FA";
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		if(request.getParameter("value") != null) {    		
			try {	
				
				String value = new String(request.getParameter("value"));
				String ori = Utilities.AESdecrypt(KEY, value);
				
				/*
				 * customerId,-ContactId|clickId
				 * Customer: x,-|x Contact: -,x|x
				 */
				String delim = "[,|]+";		
				String [] values = ori.split(delim);
				
				String query = request.getQueryString();
				String url =  StringUtils.substringAfterLast(query, "url=");
				Integer userId = null;
				Integer contactId = null;
				
				// userId / contactId
				if(!values[0].equals("-")) {
					userId = Integer.parseInt(values[0]);
				} else {
					contactId = Integer.parseInt(values[1]);
				}
				int clickId = Integer.parseInt(values[2]);
				Click click = new Click();		
				click.setId(clickId);
				click.setUrl(url);
				ClickDetail clickDetail = new ClickDetail();				
				clickDetail.setClickId(clickId);		
				clickDetail.setUserId(userId);
				clickDetail.setContactId(contactId);		
				clickDetail.setIpAddress(request.getRemoteAddr());
				click.setClickDetail(clickDetail);		
				
				this.webJaguar.updateClickCount(click);

				return new ModelAndView(new RedirectView(click.getUrl()));
				
			} catch (Exception e) {
				//System.out.println("Exception: " +e);
				e.printStackTrace();
			}
		}
		return new ModelAndView(new RedirectView(request.getParameter("url")));
	}
}