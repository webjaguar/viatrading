/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 08.14.2007
 */

package com.webjaguar.web.frontend;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductOptionValue;

public class ProductComparisonController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		Map siteConfig = (Map) request.getAttribute("siteConfig");

		String url = request.getContextPath() + request.getServletPath();
		String query = request.getQueryString();
		myModel.put("url", url + "?" + query);
		// continue shopping
		request.getSession().setAttribute("continueShoppingUrl", myModel.get("url"));

		setLayout(request);

		if (ServletRequestUtils.getStringParameter(request, "optionComparison") != null) {
			// items with options
			this.getCartItemsList(myModel, request);
			return new ModelAndView("frontend/framestore/comparison", "model", myModel);
		} else {
			int maxComparisons = new Integer(((Configuration) siteConfig.get("COMPARISON_MAX")).getValue());
			if(((Configuration)siteConfig.get("TEMPLATE")).getValue().equalsIgnoreCase("template6")) {
				maxComparisons = 3;
			}
			this.getProductsList(myModel, request, maxComparisons);
		}

		String forwardAction = request.getParameter("forwardAction");
		if (forwardAction != null) {
			response.sendRedirect(forwardAction);
			return null;
		}

		if(((Map<String,Configuration>)siteConfig).get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/comparison", "model", myModel);
		} else {
			return new ModelAndView("frontend/comparison", "model", myModel);
		}
	}

	private void getCartItemsList(Map<String, Object> myModel, HttpServletRequest request) {

		List<CartItem> cartItems = (List<CartItem>) request.getSession().getAttribute("cartItemsComparisonList");
		if (cartItems == null || cartItems.isEmpty()) {
			return;
		}
		myModel.put("cartItems", cartItems);

		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			int[] ids = ServletRequestUtils.getIntParameters(request, "__selected_id");
			for (int i = 0; i < ids.length; i++) {
				cartItems.remove(ids[i]);
			}
		}

		List<Product> products = new ArrayList<Product>();
		for (CartItem cartItem : cartItems) {
			Product product = this.webJaguar.getProductById(cartItem.getProduct().getId(), request);
			products.add(product);
			cartItem.setProduct(product);

			Map<String, List<ProductOption>> productOptionsMap = new HashMap<String, List<ProductOption>>();

			for (ProductAttribute attribute : cartItem.getProductAttributes()) {

				if (!productOptionsMap.containsKey(attribute.getOptionCode())) {
					productOptionsMap.put(attribute.getOptionCode(), this.webJaguar.getProductOptionsByOptionCode(attribute.getOptionCode(), false, product.getSalesTag(), null));
				}
				try {
					attribute.setOptionName(productOptionsMap.get(attribute.getOptionCode()).get(attribute.getOptionIndex()).getName());
				} catch (Exception e) {
					continue;
				}

				if (attribute.getValueIndex() != -1) {
					try {
						ProductOptionValue productOptionValue = productOptionsMap.get(attribute.getOptionCode()).get(attribute.getOptionIndex()).getValues().get(attribute.getValueIndex());
						if (attribute.isOneTimePrice()) {
							attribute.setValueName(productOptionValue.getName() + " $" + new DecimalFormat("#,###.00").format(attribute.getOptionPrice()));
						} else {
							attribute.setValueName(productOptionValue.getName());
						}
					} catch (Exception e) {
					}
				} else {
					attribute.setValueName(attribute.getOptionCusValue());
				}
			}
		}

		try {
			myModel.put("productFields", this.webJaguar.getProductFields(request, products, true));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getProductsList(Map<String, Object> myModel, HttpServletRequest request, int maxComparisons) {
		// comparison list
		List<Product> products = new ArrayList<Product>();

		List<Integer> comparisonList = (List) request.getSession().getAttribute("comparisonList");
		if (comparisonList == null) {
			comparisonList = new ArrayList<Integer>();
			request.getSession().setAttribute("comparisonList", comparisonList);
		}

		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			int[] ids = ServletRequestUtils.getIntParameters(request, "__selected_id");
			for (int i = 0; i < ids.length; i++) {
				comparisonList.remove(comparisonList.indexOf(ids[i]));
			}
		}

		int[] ids = ServletRequestUtils.getIntParameters(request, "product.id");
		for (int i = 0; i < ids.length; i++) {
			if (!comparisonList.contains(ids[i])) {
				comparisonList.add(ids[i]);
			}
		}

		Iterator iter = comparisonList.iterator();
		while (iter.hasNext()) {
			Product product = this.webJaguar.getProductById((Integer) iter.next(), request);
			if (product != null) {
				if (products.size() < maxComparisons) {
					products.add(product);
				} else {
					myModel.put("message", "comparison.max");
					iter.remove();
				}
			} else {
				iter.remove();
			}
		}

		myModel.put("products", products);
		try {
			myModel.put("productFields", this.webJaguar.getProductFields(request, products, true));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void setLayout(HttpServletRequest request) {
		Layout layout = (Layout) request.getAttribute("layout");
		Map siteConfig = (Map) request.getAttribute("siteConfig");
		if (((Configuration) siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES")).getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		}
	}
}