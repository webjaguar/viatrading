/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.review;


import java.util.*;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.CompanyReview;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.form.ReviewForm;

public class CompanyReviewWizardController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	public CompanyReviewWizardController() {
		setCommandName("companyReviewForm");
		setCommandClass(ReviewForm.class);
		setPages( new String[] { "frontend/review/company/step1", "frontend/review/company/step2" } );
	}
	
    public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {	
    	Map<String, Integer> map = new HashMap<String, Integer>();
    	
    	ReviewForm reviewForm = (ReviewForm) command;
    	this.webJaguar.insertCompanyReview( reviewForm.getCompanyReview() );
    	return new ModelAndView(new RedirectView(reviewForm.getReturnUrl()));
    }
 
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
    	throws Exception {    	
    	
    	ReviewForm reviewForm = (ReviewForm) command;
    	
       	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put( "account", this.webJaguar.getCustomerById(reviewForm.getCompanyReview().getCompanyId()) );
		
		switch (page) {
			case 1:
				myModel.put("now", new Date());
				break;
		}	
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		myModel.put("companyReviewLayout", this.webJaguar.getSystemLayout("companyReview", request.getHeader("host"), request));
		map.put("model", myModel); 
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		return map;
    }
    
    protected void validatePage(Object command, Errors errors, int page) {
    	ReviewForm reviewForm = (ReviewForm) command;

		switch (page) {
			case 0 :
				if (reviewForm.getCompanyReview().getRate() == 0) {
					errors.rejectValue("companyReview.rate", "form.required", "Required field");
				}
				if (reviewForm.getCompanyReview().getText().length() < 1 || reviewForm.getCompanyReview().getText().length() > 5000) {
					errors.rejectValue("companyReview.text", "Max 5000 character.", "Max 5000 character.");
				}
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "companyReview.userName", "form.required", "Required field");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "companyReview.title", "form.required", "Required field");
				break;
			}
    }
    
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );	
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		UserSession userSession = this.webJaguar.getUserSession(request);
		ReviewForm reviewForm = new ReviewForm();
		CompanyReview companyReview = new CompanyReview();
		reviewForm.setCompanyReview( companyReview );
		
		Integer userId = ServletRequestUtils.getIntParameter( request, "id", -1 ); 

		if ( userId == -1 && userSession == null){
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("account.jhtm")));			
		}
		reviewForm.getCompanyReview().setUserId( userSession.getUserid() );
		reviewForm.getCompanyReview().setUserName( userSession.getFirstName() + " " + userSession.getLastName() );
		reviewForm.getCompanyReview().setCompanyId( userId );
		
		reviewForm.setReturnUrl( ServletRequestUtils.getStringParameter( request, "url", "home.jhtm" ) );

		return reviewForm;
	}
	
	protected int getInitialPage(HttpServletRequest request, Object command) {
		return 0;
	}
}