/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.review;


import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;


import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;

import spexlive.etilize.com.Catalog;
import spexlive.etilize.com.CatalogServiceIntf;
import spexlive.etilize.com.GetProduct;
import spexlive.etilize.com.ProductId;
import spexlive.etilize.com.Resource;
import spexlive.etilize.com.SelectProductFields;
import spexlive.etilize.com.Sku;
import spexlive.etilize.com.SubcatalogFilter;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Layout;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.form.ReviewForm;

public class ProductReviewWizardController extends AbstractWizardFormController {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	private JavaMailSenderImpl mailSender;
    public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
    
	private Map<String, Configuration> siteConfig;
	private Map<String, Object> gSiteConfig;
	
	public ProductReviewWizardController() {
		setCommandName("productReviewForm");
		setCommandClass(ReviewForm.class);
		setPages( new String[] { "frontend/review/product/step1", "frontend/review/product/step2" } );
	}
	
    public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {	
    	Map<String, Integer> map = new HashMap<String, Integer>();
    	
    	ReviewForm reviewForm = (ReviewForm) command;
    	if (reviewForm.getProductReview().getRate() > 5 ) {
    		reviewForm.getProductReview().setRate(5);
    	}
    	this.webJaguar.insertProductReview( reviewForm.getProductReview() );
    	map.put( "id", reviewForm.getProductReview().getProductId() );
    	map.put( "cid", reviewForm.getProductReview().getCategoryId() );
    	//email send to admin
    	if(! siteConfig.get("PRODUCT_REVIEW_EMAIL").getValue().isEmpty()) {
    		MimeMessage mms = mailSender.createMimeMessage();
    		MimeMessageHelper helper = new MimeMessageHelper(mms, false, "UTF-8");
    		try {
    			String contactEmail = siteConfig.get("PRODUCT_REVIEW_EMAIL").getValue();
    	    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));
    	    	helper.setFrom(siteConfig.get("CONTACT_EMAIL").getValue());
    	    	helper.setTo(contactEmail);
    			// Bcc emails
    			helper.addBcc( "rewatiraman@advancedemedia.com" );			
    			helper.setSubject("Product Review was added for product sku: " + reviewForm.getProductReview().getProductSku());
    			StringBuffer message = new StringBuffer();
    			message.append( dateFormatter.format(new Date()) + "<br /><br />A product review was added by a customer<br /><br />" + 
    					"Product sku: " + reviewForm.getProductReview().getProductSku() + "<br />" +
    					"Product Id: " + reviewForm.getProductReview().getProductId() + "<br />" +
    					"Product category Id: " + reviewForm.getProductReview().getCategoryId() + "<br />" +
    					"By UserName: " + reviewForm.getProductReview().getUserName() + "<br />" +
    					"By UserId: " + reviewForm.getProductReview().getUserId() + "<br />" +
    					"Review Title: " + reviewForm.getProductReview().getTitle() + "<br />" +
    					"Review: " + reviewForm.getProductReview().getText() );
    			helper.setText(message.toString(), true);		
    			mailSender.send(mms);
    		} catch (Exception e) {
    		}
    	}
    	    	
    	return new ModelAndView(new RedirectView("product.jhtm"), map);
    }
 
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
    	throws Exception {    	
    	
    	ReviewForm reviewForm = (ReviewForm) command;
    	
       	Map<String, Map> map = new HashMap<String, Map>();
		Map<String, Object> myModel = new HashMap<String, Object>();
		Product product = this.webJaguar.getProductById( reviewForm.getProductReview().getProductId(), request );
		
		
		switch (page) {
			case 1:
				// Etilize
				String appId = siteConfig.get("ETILIZE").getValue();
		    	if (appId.trim().length() > 0) {
					setEtilize(appId, product, myModel, request.getParameter("hack") != null, request.getParameter("debug") != null);
				}
				myModel.put("now", new Date());
				break;
		}	
		myModel.put("product", product);
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		myModel.put("productReviewLayout", this.webJaguar.getSystemLayout("productReview", request.getHeader("host"), request));
		map.put("model", myModel); 
		Layout layout = (Layout) request.getAttribute( "layout" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		
		return map;
    }
    
    protected void validatePage(Object command, Errors errors, int page) {
    	ReviewForm reviewForm = (ReviewForm) command;

		switch (page) {
			case 0 :
				if (reviewForm.getProductReview().getRate() < 0) {
					errors.rejectValue("productReview.rate", "form.required", "Required field");
				}
				if (reviewForm.getProductReview().getText().length() < 1 || reviewForm.getProductReview().getText().length() > 5000) {
					errors.rejectValue("productReview.text", "Max 5000 character.", "Max 5000 character.");
				}
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productReview.userName", "form.required", "Required field");
				ValidationUtils.rejectIfEmptyOrWhitespace(errors, "productReview.title", "form.required", "Required field");
				break;
			}
    }
    
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );	
		gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		UserSession userSession = this.webJaguar.getUserSession(request);
		ReviewForm reviewForm = new ReviewForm();
		String productSku = ServletRequestUtils.getStringParameter(request, "sku", "");
		Integer categoryId = ServletRequestUtils.getIntParameter( request, "cid", -1 ); 
		Integer productId = this.webJaguar.getProductIdBySku(productSku) ; 
		if ( productId == null ){
			throw new ModelAndViewDefiningException(new ModelAndView(new RedirectView("home.jhtm")));			
		}
		if (siteConfig.get( "PRODUCT_REVIEW_TYPE" ).getValue().equals("anonymous")) {
			reviewForm.getProductReview().setReviewType("anonymous");
		} else {
			reviewForm.getProductReview().setUserId( userSession.getUserid() );
			reviewForm.getProductReview().setUserName( userSession.getFirstName() + " " + userSession.getLastName() );
			
			reviewForm.getProductReview().setReviewType("login");
			reviewForm.getProductReview().setIpAddress( request.getRemoteAddr());
		}
		reviewForm.getProductReview().setProductId( productId );
		reviewForm.getProductReview().setCategoryId( categoryId );
		reviewForm.getProductReview().setProductSku( productSku );
		
		if (siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			setPages(new String[] { "frontend/layout/template6/review/product/step1", "frontend/layout/template6/review/product/step2"});
		} 
		return reviewForm;
	}
	
	protected int getInitialPage(HttpServletRequest request, Object command) {
		return 0;
	}
	
	private void setEtilize(String appId, Product product, Map<String, Object> model, boolean hackImages, boolean debug) {
		try {
    		GetProduct getProduct = new GetProduct();
    		getProduct.setCatalog("na");
    		getProduct.setSiteId(0);
    		
    		// search by sku
    		Sku sku = new Sku();
    		sku.setNumber(product.getSku());
    		if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("SYNNEX")) {
    			sku.setType("Synnex");
    			if (product.getSku().startsWith("SYN-")) {
            		sku.setNumber(product.getSku().substring(4)); // remove leading SYN-    				    				
    			}
    		} else if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("INGRAM")) {
    			sku.setType("Ingram Micro USA");
    			if (product.getSku().startsWith("IM-")) {
    				sku.setNumber(product.getSku().substring(3)); // remove leading IM-
    			}
    		} else if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("TECHDATA")) {
    			sku.setType("Tech Data");
    			if (product.getSku().startsWith("TD-")) {
        			sku.setNumber(product.getSku().substring(3)); // remove leading TD-    				
    			}
    		} else if (product.getFeed() != null && product.getFeed().equalsIgnoreCase("DSI")) {
    			sku.setType("DSI");
    			if (product.getSku().startsWith("DSI-")) {
        			sku.setNumber(product.getSku().substring(4)); // remove leading DSI-    				
    			}
    		} else if (product.getEtilizeId() != null) {
    			ProductId productId = new ProductId();
    			productId.setId(product.getEtilizeId());
    			getProduct.setProductId(productId);
    		} else if (product.getUpc() != null && product.getUpc().trim().length() > 0) {
    			sku.setType("UPC");
        		sku.setNumber(product.getUpc()); // use UPC  				
    		}
    		if (sku.getType() != null) {
        		getProduct.setSku(sku);
    		} else if (getProduct.getProductId() != null) {
    			// use etilize id
    		} else {
    			return;    			
    		}
    		
    		String wsdlLocation = "http://ws.spexlive.net/service/soap/catalog?appId=" + appId + "&wsdl";
    		Catalog service = new Catalog(new URL(wsdlLocation), new QName("http://com.etilize.spexlive", "catalog"));
    		CatalogServiceIntf port = service.getCatalogHttpPort();
    		
    		// SelectProductFields
    		SelectProductFields selectProductFields = new SelectProductFields();
    		selectProductFields.setDescriptions("all");
    		selectProductFields.setManufacturer("default");
    		selectProductFields.getResourceType().add("all"); 		
    		selectProductFields.getSkuType().add("all");
    		selectProductFields.setDataSheet("basic");
    		selectProductFields.setCategory("default");
    		getProduct.setSelectProductFields(selectProductFields);
    		
    		// subcatalog
    		SubcatalogFilter subcatalogFilter = new SubcatalogFilter();
    		subcatalogFilter.setContent("allitems");
    		getProduct.setSubcatalogFilter(subcatalogFilter);
    		
    		spexlive.etilize.com.Product eProduct = port.getProduct(getProduct);
    		
    		// images
    		if (eProduct.getResources() != null) {
    			// images
    			Set<String> imageTypes = new HashSet<String>();
    			imageTypes.add("Top");
    			imageTypes.add("Left");
    			imageTypes.add("Right");
    			imageTypes.add("Front");
    			imageTypes.add("Rear");
    			imageTypes.add("Bottom");
    			imageTypes.add("Jack-Pack");
    			
    			ProductImage large = null;
    			ProductImage thumb = null;
        		for (Resource resource: eProduct.getResources().getResource()) {
        			if (resource.getType().equalsIgnoreCase("Large")) {
        				large = new ProductImage();
        				large.setImageUrl(resource.getUrl());
        			} else if (imageTypes.contains(resource.getType())) {
    					ProductImage pi = new ProductImage();
    					pi.setImageUrl(resource.getUrl());
    					product.addImage(pi);
        			} else if (resource.getType().equalsIgnoreCase("Thumbnail")) {
        				thumb = new ProductImage();
        				thumb.setImageUrl(resource.getUrl());
        			}
        		}
        		if (large != null) {
        			if (product.getImages() == null) product.setImages(new ArrayList<ProductImage>());
        			product.getImages().add(0, large);
        		} else if ((product.getImages() == null || product.getImages().isEmpty()) && thumb != null) {
        			product.addImage(thumb);
        		}
    		}
    		if (hackImages) {
        		// hack to their images
        		String[] types  = { "Top", "Left", "Right", "Front", "Rear" };
        		for (String type: types) {
    				ProductImage pi = new ProductImage();
    				pi.setImageUrl("http://content.etilize.com/" + type + "/" + eProduct.getProductId() + ".jpg");
    				product.addImage(pi);        			
        		}    			
    		}

    	} catch (Exception e) {
    		if (debug) {
        		System.out.println(e.toString());    			
    		}
    	} 
	}
}