/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend.review;


import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.ProductReview;
import com.webjaguar.model.UserSession;
import com.webjaguar.web.form.ReviewForm;

public class ProductRateController implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) 
        throws Exception {
			Map<String, Object> model = new HashMap<String, Object>();
			UserSession userSession = this.webJaguar.getUserSession(request);
			ReviewForm reviewForm = new ReviewForm();
			
			String productSku = ServletRequestUtils.getStringParameter(request, "sku", " ");
			Integer rate = ServletRequestUtils.getIntParameter(request, "rate", 0);
			
			reviewForm.getProductReview().setProductSku( productSku );
			
			// Review Type is rating
			reviewForm.getProductReview().setReviewType( "rating" );
			reviewForm.getProductReview().setRate( rate );
			reviewForm.getProductReview().setIpAddress( request.getRemoteAddr() );
			
			if(userSession == null){
				reviewForm.getProductReview().setUserId( -1 );
				reviewForm.getProductReview().setUserName( "Anonymous" );
				reviewForm.getProductReview().setTitle("Anonymous");
				reviewForm.getProductReview().setText( "" );
				reviewForm.getProductReview().setActive(true);
			}
			else{
				reviewForm.getProductReview().setTitle("Anonymous");
				reviewForm.getProductReview().setUserId( userSession.getUserid() );
				reviewForm.getProductReview().setUserName( userSession.getFirstName() + " " + userSession.getLastName() );
			}
			reviewForm.setMessage("error");
			
			// Save Product Review
			if(this.webJaguar.checkPreviousReview(reviewForm.getProductReview()) == false){
				this.webJaguar.insertProductReview(reviewForm.getProductReview());
				reviewForm.setMessage(null);
			}
				
			// Get Average Review
			ProductReview review = this.webJaguar.getProductReviewAverage(productSku);
		    reviewForm.getProductReview().setAverage(review.getAverage());
		    reviewForm.getProductReview().setNumReview(review.getNumReview());
			model.put("jsonObject", reviewForm);
		
		return new ModelAndView("frontend/review/product/showRate", model);
		}
	}