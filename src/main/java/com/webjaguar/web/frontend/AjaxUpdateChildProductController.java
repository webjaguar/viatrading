/*
 * Copyright 2005, 2011 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.web.frontend;


import java.lang.reflect.Method;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductSearch;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.KeyBean;

public class AjaxUpdateChildProductController implements Controller
{

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	Map<String, Configuration> siteConfig;
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception
	{
		siteConfig = (Map) request.getAttribute( "siteConfig" );
		Map<String, Object> model = new HashMap<String, Object>();
		
		Product level1Slave = this.webJaguar.getProductById(ServletRequestUtils.getIntParameter(request, "id", -1), request);
		Product product = null;
		model.put("masterProduct", level1Slave);	
		if(level1Slave != null){
			int mainProductId = this.webJaguar.getProductIdBySku(level1Slave.getMasterSku());
			product = this.webJaguar.getProductById(mainProductId, request);
			model.put("product", product);		
		}
		
		List <Product> slavesList = new ArrayList<Product>();
		ProductSearch search = new ProductSearch();
		search.setSku(product.getSku());
		search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		boolean basedOnHand = false;
		if (siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly") ) {
			basedOnHand = true;
		}
		search.setBasedOnHand(basedOnHand);
		
		Map<String, Integer> colorsProductMap = new HashMap<String, Integer>();
		// product fields
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		for (Integer id: this.webJaguar.getSlaves(search)) {
			Product temp = this.webJaguar.getProductById(id, request);
			
			String colorField = null;
			try {
				m = c.getMethod("getField" + siteConfig.get("COLOR_FIELD_ID").getValue() );
				colorField = (String) m.invoke(temp, arglist);
			}catch(Exception e){
				e.printStackTrace();
			}
			if(colorField != null){
				colorsProductMap.put(colorField.trim().toUpperCase(), id);
			}
		}
		
		search.setSku(level1Slave.getSku());
		// product field 1 setting color
		List<ProductField> productFieldList = new ArrayList<ProductField>();
		
		String colorField = null;
		try {
			m = c.getMethod("getField" + siteConfig.get("COLOR_FIELD_ID").getValue() );
			colorField = (String) m.invoke(level1Slave, arglist);
		}catch(Exception e){
			e.printStackTrace();
		}
		productFieldList.add( new ProductField( Integer.parseInt(siteConfig.get("COLOR_FIELD_ID").getValue()), colorField) );
		search.setProductField(productFieldList);
		Map<Integer, String> sizeMap = new HashMap<Integer, String>();
		for (Integer id: this.webJaguar.getSlaves(search)) {
			Product level2Slave = this.webJaguar.getProductById(id, request);
			if (level2Slave != null) {
				slavesList.add(level2Slave);
				
				try {
					m = c.getMethod("getField" + siteConfig.get("SIZE_FIELD_ID").getValue() );
					sizeMap.put(id, (String) m.invoke(level2Slave, arglist));
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
		model.put("sizeMap", sizeMap);
		
		Map<Integer, KeyBean> colorsMap = new TreeMap<Integer, KeyBean>();
		for(KeyBean colorCode : Constants.COLOR_CODE) {
			if(colorsProductMap.get(colorCode.getName()) != null) {
				colorsMap.put(colorsProductMap.get(colorCode.getName()), colorCode);
			}
		}
		model.put("colorsMap", colorsMap);		
		model.put("selectedColor", colorField.toUpperCase());	
		List<ProductField> productFields = this.webJaguar.getProductFields(request, slavesList, true);	
		if (!slavesList.isEmpty()) {	// not empty
			
			// For sorting product filed values (if value is size)- create map
			Map<Integer, Product> sizeAscMap = new TreeMap<Integer, Product>();
			int i=0;
			for (Product slaveProduct: slavesList) {
				for(ProductField slaveProductField: slaveProduct.getProductFields()) {
					if (slaveProductField.getId()== Integer.parseInt(siteConfig.get("SIZE_FIELD_ID").getValue())){
						try {
							sizeAscMap.put(Integer.parseInt(Constants.getApparelSize().get(slaveProductField.getValue()).toString()), slaveProduct);								
						} catch (Exception e) {
							sizeAscMap.put(i, slaveProduct);
						}
					}
				}
				++i;
			}		
			
			
			List <Product> sortedSlavesList = new ArrayList(sizeAscMap.values());
			model.put("slavesList", sortedSlavesList);
			model.put("slavesProductFields",productFields);
		}
		model.put("slavesSearch", search);
		model.put("slavesShowQtyColumn", true);
		return new ModelAndView( "frontend/ajax/updateChildProduct", "model", model );
	}
}