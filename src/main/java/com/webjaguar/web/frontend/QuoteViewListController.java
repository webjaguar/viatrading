/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 08.14.2007
 */

package com.webjaguar.web.frontend;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Address;
import com.webjaguar.model.CartItem;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Option;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductAttribute;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductOptionValue;
import com.webjaguar.model.Promo;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UserSession;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.web.form.InvoiceForm;
import com.webjaguar.web.frontend.shoppingCart.OrderFormController;

public class QuoteViewListController extends WebApplicationObjectSupport implements Controller {
	
	private static final Logger logger = LoggerFactory.getLogger(OrderFormController.class);
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	private Properties site;
	public void setSite(Properties site) { this.site = site; }
	
	Map<String, Configuration> siteConfig;
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {				
    	    	
    	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	Map<String, Object> myModel = new HashMap<String, Object>();
		myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		boolean showPriceColumn = false;
		boolean showOptionColumn = false;
		UserSession userSession = (UserSession) request.getAttribute( "userSession" );
		 
		if(userSession != null) {
			Customer customer = this.webJaguar.getCustomerById(userSession.getUserid());
			myModel.put("customer", customer);
		}
		
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		
		
        // continue shopping
		request.getSession().setAttribute( "continueShoppingUrl", myModel.get( "url" ) );
		
		// Quote list
		List<Product> products = new ArrayList<Product>();
		
		List<Integer> quoteViewList = (List) request.getSession().getAttribute( "quoteListCount" );
		if (quoteViewList == null) {
			quoteViewList = new ArrayList<Integer>();
			request.getSession().setAttribute( "quoteListCount", quoteViewList );
		}

		// check if delete button was clicked
		if (request.getParameter("__delete") != null) {
			int[] ids = ServletRequestUtils.getIntParameters( request, "__selected_id" );
			for (int i=0; i<ids.length; i++) {
				quoteViewList.remove(quoteViewList.indexOf(ids[i]));
			}
		}
		
		int[] ids = ServletRequestUtils.getIntParameters( request, "product_Id" );		
		for (int i=0; i<ids.length; i++) {
			if (!quoteViewList.contains(ids[i]) && request.getParameter("__delete") == null ) {
				quoteViewList.add( ids[i] );	
			}
		}
		
		Iterator iter = quoteViewList.iterator();
		while (iter.hasNext()) {
			Product product = this.webJaguar.getProductById( (Integer) iter.next(), request );			
			if (product != null) {				
				products.add( product );
			} else {
				iter.remove();
			}
		}

		// Grouping of Products
		Map<String, List<ProductField>> groupListMap = new HashMap<String, List<ProductField>>();
		Map<String, List<Product>> groupProductMap = new HashMap<String, List<Product>>();
		if(siteConfig.get("GROUP_PRODUCTS_QUOTE").getValue().equals("true")) {			
			for(Product product :products) {
				if(product.getField50() == null && groupProductMap.get("No Group") != null){
					if (groupProductMap.get("No Group").size() < 10) {
						groupProductMap.get("No Group").add(product);
					}
				} else if(product.getField50() != null && groupProductMap.get(product.getField50()) != null){
					if (groupProductMap.get(product.getField50()).size() < 10) {
						groupProductMap.get(product.getField50()).add(product);
					}
				} else {
					List<Product> tempList = new ArrayList<Product>();
					tempList.add(product);
					groupProductMap.put(product.getField50() != null ? product.getField50() : "No Group",tempList);
				}
			}
			
			for(String key : groupProductMap.keySet()) {
				groupListMap.put(key, this.webJaguar.getProductFields(request, groupProductMap.get(key), true));
			}
			myModel.put("productFieldsMap", groupListMap);
			myModel.put("groupProductsMap", groupProductMap);
		}  else {
			groupProductMap.put("products", products);
			for(String key : groupProductMap.keySet()) {
				groupListMap.put(key, this.webJaguar.getProductFields(request, groupProductMap.get(key), true));
			}
			myModel.put("groupProductsMap",groupProductMap);			
			myModel.put("productFieldsMap", groupListMap);
		}
		
		
		for(Product product: products) {
			if (!showPriceColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) || product.getMsrp() != null)) {
				showPriceColumn = true;					
			}
			if (!showOptionColumn && (product.getOptionCode() != null && !product.getOptionCode().isEmpty())) {
				showOptionColumn = true;			
				Integer productWithOptions = product.getId();
			}
			
		}
		myModel.put("showPriceColumn", showPriceColumn);
		myModel.put("showOptionColumn", showOptionColumn);
		
		PagedListHolder productList = new PagedListHolder(products);
		productList.setPageSize(products.size());
		myModel.put("products", productList);
		myModel.put("size", products.size());
			
		setLayout(request);
		myModel.put("quoteListLayout", this.webJaguar.getSystemLayout("quoteList", request.getHeader("host"), request));
		
		if (request.getParameter("__delete") != null) {
			for ( int i = 0; i < ids.length; i++ ) {						
				//Option Code
				setProductOptions(ids[i],  request, null, myModel);
			}			
		}
		
		// multi store
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);
		}
		
		if (request.getParameter("send") != null) {
			
			Customer customer = new Customer();
			CrmContact crmContact = new CrmContact();
			StringBuffer message = new StringBuffer();
			Product product = new Product();
			
			// CRM Message
			message.append("----QUOTES requested for Products---- <br>");
			if ( ids != null ) {

				String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
				for ( int i = 0; i < ids.length; i++ ) {					
					product = this.webJaguar.getProductById(ids[i], request);
					String stringQuantity = ServletRequestUtils.getStringParameter( request, "quantity_" + ids[i], null );
					myModel.put("quantity_"+product.getId(), stringQuantity);					
					//message.append("Product Details : \n PRODUCT NAME: " +product.getName() + "\n SKU: " +product.getSku() + "\n PRODUCT_ID: " +product.getId() +  "\n QUANTITY: " +stringQuantity+ "\n"  ); // need to more code to make the email dynamic	(check abandoned shopping cart email )			
					
					
					// product options
					Set<String> optionCodes = new HashSet<String>();
					if(request.getParameter("optionCode_" + ids[i]) != null) {
						for (String optionCode: ServletRequestUtils.getStringParameters(request, "optionCode_" + ids[i])) {
							optionCode = optionCode.trim();						
							if (optionCode.length() > 0 && optionCodes.add(optionCode.toLowerCase())) {
								Option option = this.webJaguar.getOption(null, optionCode);
								// get product attributes return an empty array if not found.
								String options[] = ServletRequestUtils.getStringParameters(request, optionCode + "-option_" + ids[i]);
								if (options.length != 0) {
									for (int option_index = 0; option_index < options.length; option_index++) {
										String option_values[] = request.getParameterValues(optionCode + "-option_values_" + ids[i] + "_" + options[option_index]);
										if (option_values != null) {
											for (int values_index = 0; values_index < option_values.length; values_index++) {
												if(option.getProductOptions() != null){
													for(ProductOption productOption: option.getProductOptions()){
														for(ProductOptionValue productOptionValue :productOption.getValues()){
															if(productOptionValue.getIndex() == Integer.parseInt(option_values[values_index])) {
																message.append("Product Details : " + "<br> PRODUCT NAME: " +product.getName() + "<br> SHORT DESCRIPTION: " +product.getShortDesc() + "<br> SKU: " +product.getSku() + "<br> PRODUCT_ID: " +product.getId() +  
																		"<br> QUANTITY: " +stringQuantity+ "<br> OPTIONS: " +productOptionValue.getName() + "<br> LINK: " + secureUrl + "product.jhtm?id=" + product.getId() + "<br><br><br>");
															}
														}
													}
												}
											}
										}
										// one value for custom text
										String option_values_cus[] = request.getParameterValues(optionCode + "-option_values_cus_" + ids[i] + "_" + options[option_index]);
										if ( option_values_cus != null ) {		
											message.append("Product Details : " + "<br> PRODUCT NAME: " +product.getName() + "<br> SHORT DESCRIPTION: " +product.getShortDesc() + "<br> SKU: " +product.getSku() + "<br> PRODUCT_ID: " +product.getId() +  
													"<br> QUANTITY: " +stringQuantity + "<br> OPTIONS: "  +option_values_cus[0].toString() + "<br> LINK: " + secureUrl + "product.jhtm?id=" + product.getId() +  "<br><br><br>");
										}
									}
								}
							}
						}
					} else {
						message.append("Product Details : <br> PRODUCT NAME: " +product.getName() + "<br> SHORT DESCRIPTION: " +product.getShortDesc() + "<br> SKU: " +product.getSku() + "<br> PRODUCT_ID: " +product.getId() +  "<br> QUANTITY: " +stringQuantity + "<br> LINK: " + secureUrl + "product.jhtm?id=" + product.getId() + "<br><br><br>"  );
					}
				}
			}	
			
			if(ServletRequestUtils.getStringParameter(request, "company") != null) {
				message.append("Company : "+ServletRequestUtils.getStringParameter(request, "company")+" \n");
			}
			if(ServletRequestUtils.getStringParameter(request, "note") != null) {
				message.append("Note from Customer : "+ServletRequestUtils.getStringParameter(request, "note")+" \n");
				myModel.put("note", ServletRequestUtils.getStringParameter(request, "note"));
			}	

			if (!ServletRequestUtils.getStringParameter(request, "email").isEmpty() && (this.webJaguar.getCustomerByUsername(ServletRequestUtils.getStringParameter(request, "email")) != null)) {	
				customer = this.webJaguar.getCustomerByUsername(ServletRequestUtils.getStringParameter(request, "email"));
				
				//Existing Customer 
					crmContact.setAccountId(1);
					crmContact.setAccountName("Leads");
					if(ServletRequestUtils.getStringParameter(request, "leadSource") != null) {
						crmContact.setLeadSource(ServletRequestUtils.getStringParameter(request, "leadSource")); 
					}
					crmContact.setFirstName(ServletRequestUtils.getStringParameter(request, "firstName"));
					crmContact.setLastName(ServletRequestUtils.getStringParameter(request, "lastName"));
					crmContact.setStreet(ServletRequestUtils.getStringParameter(request, "address"));
					crmContact.setCity(ServletRequestUtils.getStringParameter(request, "city"));
					crmContact.setStateProvince(ServletRequestUtils.getStringParameter(request, "state"));
					crmContact.setZip(ServletRequestUtils.getStringParameter(request, "zipCode"));
					crmContact.setCountry(ServletRequestUtils.getStringParameter(request, "country"));
					crmContact.setEmail1(ServletRequestUtils.getStringParameter(request, "email"));
					crmContact.setPhone1(ServletRequestUtils.getStringParameter(request, "phone"));
					crmContact.setDescription(message.toString());	
				
			} else {

				//Creating New Customer
				customer.setUsername(ServletRequestUtils.getStringParameter(request, "email"));
				// password
				customer.setPassword(UUID.randomUUID().toString().substring(0,5));  
				// address
				Address address = new Address();				
				address.setFirstName(ServletRequestUtils.getStringParameter(request, "firstName"));
				address.setLastName(ServletRequestUtils.getStringParameter(request, "lastName"));
				address.setEmail(ServletRequestUtils.getStringParameter(request, "email"));
				address.setPhone(ServletRequestUtils.getStringParameter(request, "phone"));				

				if(ServletRequestUtils.getStringParameter(request, "address") != null && !ServletRequestUtils.getStringParameter(request, "address").isEmpty()) {
					address.setAddr1(ServletRequestUtils.getStringParameter(request, "address"));
				} else {
					address.setAddr1("address_Quote");
				}
				if(ServletRequestUtils.getStringParameter(request, "city") != null && !ServletRequestUtils.getStringParameter(request, "city").isEmpty()) {
					address.setCity(ServletRequestUtils.getStringParameter(request, "city"));
				} else {
					address.setCity("city_Quote");
				}
				if (ServletRequestUtils.getStringParameter(request, "state") != null && !ServletRequestUtils.getStringParameter(request, "state").isEmpty()) {
					address.setStateProvince(ServletRequestUtils.getStringParameter(request, "state"));
				} else {
					address.setStateProvince("state_Quote");
				}
				if (ServletRequestUtils.getStringParameter(request, "zipCode") != null && !ServletRequestUtils.getStringParameter(request, "zipCode").isEmpty()) {
					address.setZip(ServletRequestUtils.getStringParameter(request, "zipCode"));
				} else {
					address.setZip("zipCode_Quote");
				}
				if (ServletRequestUtils.getStringParameter(request, "country") != null && !ServletRequestUtils.getStringParameter(request, "country").isEmpty()) {
					address.setCountry(ServletRequestUtils.getStringParameter(request, "country"));
				} else {
					address.setCountry("country_Quote");
				}
				if (ServletRequestUtils.getStringParameter(request, "company") != null && !ServletRequestUtils.getStringParameter(request, "company").isEmpty()) {
					address.setCompany(ServletRequestUtils.getStringParameter(request, "company"));					
				} else {
					address.setCompany("company_Quote");
				}
				
				customer.setAddress(address);
				// Generating Customer CardID
				generateRandomCardID(customer);
				
				// Error Message when inserting new Customer
				if (ServletRequestUtils.getStringParameter(request, "email") == null || ServletRequestUtils.getStringParameter(request, "email").isEmpty() || ServletRequestUtils.getStringParameter(request, "firstName") == null || ServletRequestUtils.getStringParameter(request, "firstName").isEmpty() || ServletRequestUtils.getStringParameter(request, "lastName") == null || ServletRequestUtils.getStringParameter(request, "lastName").isEmpty()) {
					for ( int i = 0; i < ids.length; i++ ) {						
						//Option Code
						setProductOptions(ids[i],  request, null, myModel);
					}
					myModel.put("message", "f_quote_errorMessage");
					return new ModelAndView("frontend/quoteViewList", "model", myModel);					
				} else {
					this.webJaguar.insertCustomer(customer, null, (Boolean) gSiteConfig.get("gCRM"));
					crmContact = this.webJaguar.getCrmContactById(customer.getCrmContactId());
					crmContact.setAccountId(1);
					crmContact.setDescription(message.toString());
					this.webJaguar.updateCrmContact(crmContact);
				}						
			}			
			// Creating Invoice
			InvoiceForm invoiceForm = new InvoiceForm();
			invoiceForm.setNewInvoice( true );

		    if (customer != null) {
		    	invoiceForm.getOrder().setUserId(customer.getId());
		    	invoiceForm.getOrder().setBilling( setAddress(customer.getAddress()) );
			    invoiceForm.getOrder().setShipping( setAddress(customer.getAddress()) );
			    invoiceForm.getOrder().setSalesRepId( customer.getSalesRepId() );
			    invoiceForm.getOrder().setPromo( new Promo() );	
			    invoiceForm.getOrder().setSubTotal(0.0);
			    invoiceForm.getOrder().setLanguageCode(customer.getLanguageCode());
			    invoiceForm.getOrder().setInsertedLineItems(new ArrayList<LineItem>());
		    }
		    invoiceForm.getOrder().setOrderType( siteConfig.get( "DEFAULT_ORDER_TYPE" ).getValue() );
		    invoiceForm.getOrder().setTrackcode( (String) request.getAttribute( "trackcode" ) );
		    invoiceForm.getOrder().setStatus("xq");
	    	Address source = new Address();
	    	source.setCountry(siteConfig.get( "SOURCE_COUNTRY" ).getValue());
	    	source.setStateProvince( siteConfig.get( "SOURCE_STATEPROVINCE" ).getValue());
			source.setZip( siteConfig.get( "SOURCE_ZIP" ).getValue());
			invoiceForm.getOrder().setSource( setAddress(source) );
			
			//Adding LineItems	
			List<LineItem> lineItems = new ArrayList<LineItem>();
			for(int i =0; i < ids.length ; i++) {
				
				product = this.webJaguar.getProductById(ids[i], request);
				
				CartItem cartItem = new CartItem();
				cartItem.getProduct().setId(product.getId());		
				
				setProductOptions(product.getId(),  request, cartItem, myModel);			
				
				//Product Atrributes
				if(cartItem.getProductAttributes() != null) {
					
					Iterator<ProductAttribute> prodAttrIter = cartItem.getProductAttributes().iterator();					
					
					Map<String, List<ProductOption>> productOptionsMap = new HashMap<String, List<ProductOption>>();
				
					while (prodAttrIter.hasNext()) {
						ProductAttribute attribute = prodAttrIter.next();
						
						if (!productOptionsMap.containsKey(attribute.getOptionCode())) {							
							productOptionsMap.put(attribute.getOptionCode(),this.webJaguar.getProductOptionsByOptionCode(attribute.getOptionCode(), false, product.getSalesTag(), null));
						}
						try {
							attribute.setOptionName(productOptionsMap.get(attribute.getOptionCode()).get(attribute.getOptionIndex()).getName());
							
						} catch (Exception e) {
							//do nothing
						}
						 try {							 
							 ProductOptionValue productOptionValue = productOptionsMap.get(attribute.getOptionCode()).get(attribute.getOptionIndex()).getValues().get(attribute.getValueIndex());
							 attribute.setOptionPriceOriginal(productOptionValue.getOptionPriceOriginal());
							 attribute.setOptionPrice(productOptionValue.getOptionPrice());
							 attribute.setOneTimePrice(productOptionValue.isOneTimePrice());
							 if(attribute.isOneTimePrice()) {
								attribute.setValueName(productOptionValue.getName()+" $"+new DecimalFormat("#,###.00").format( attribute.getOptionPrice()));
							 } else {
								attribute.setValueName(productOptionValue.getName());
							 }
							 attribute.setOptionWeight(productOptionValue.getOptionWeight());
							 attribute.setImageUrl(productOptionValue.getImageUrl());							
						}
						catch (Exception e) {
							// do nothing 
						}
					}
				}

				// Line items
				LineItem lineItem = new LineItem(lineItems.size()+1,cartItem);
	
				lineItems.add(lineItem);
				product = this.webJaguar.getProductById( product.getId(), 0, false, null );
				String stringQuantity = ServletRequestUtils.getStringParameter( request, "quantity_" + ids[i], null );	
				lineItem.setProduct(product);
				lineItem.setProductId( product.getId() );
				lineItem.setUnitPrice(this.webJaguar.getUnitPrice(gSiteConfig, siteConfig, invoiceForm.getOrder().getUserId(), product.getSku(), 1));
				lineItem.setOriginalPrice( lineItem.getUnitPrice() );
				
				//Quantity
				try {
					lineItem.setQuantity(Integer.parseInt(stringQuantity));
				} catch (Exception e) {
					//do nothing 
				}				
				invoiceForm.getOrder().getInsertedLineItems().add(lineItem);
			}
			
			// Calculating SubTotal and grandTotal
			double totalWeight = 0;
			double subTotal = 0;
			invoiceForm.getOrder().getLineItems().addAll( invoiceForm.getOrder().getInsertedLineItems() );
			for (LineItem lineItem:invoiceForm.getOrder().getLineItems()) {
				if (lineItem.getPackageWeight() != null ) {
					totalWeight = totalWeight + (lineItem.getPackageWeight().doubleValue() * lineItem.getQuantity()) ;
				}
				subTotal = subTotal + ( lineItem.getTotalPrice() != null ? lineItem.getTotalPrice() : 0.0);
			}
			invoiceForm.getOrder().setTotalWeight(totalWeight);
			invoiceForm.getOrder().setSubTotal(subTotal);			
			this.webJaguar.updateGrandTotal(gSiteConfig, invoiceForm.getOrder());			
			
			// Insert order
			OrderStatus orderStatus = new OrderStatus();
			orderStatus.setStatus( "xq" );

			// Notify email
			SiteMessage siteMessage = null;			
			try {
				if(siteConfig.get("SITE_MESSAGE_ID_FOR_NEW_QUOTE") != null && !siteConfig.get("SITE_MESSAGE_ID_FOR_NEW_QUOTE").getValue().equals("")) {
					siteMessage = this.webJaguar.getSiteMessageById(Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_NEW_QUOTE" ).getValue()));
				}
				if ( siteMessage != null ) {
					orderStatus.setUserNotified( true );
				}
			}
			catch ( NumberFormatException e ) {
				// do nothing
			}			
			
			// LineNumber
			int lastLineNum = 0;
			for (LineItem lineItem1: invoiceForm.getOrder().getInsertedLineItems()) {
				lineItem1.setLineNumber( ++lastLineNum );
			}				
			
			// SearchEngine Prospect
			if ((Boolean) gSiteConfig.get("gSEARCH_ENGINE_PROSPECT")) {
				String cookieName = site.getProperty("track.prospect.cookie.name");
				Cookie prospectCookie = WebUtils.getCookie(request, cookieName);
				if (prospectCookie != null) {
					invoiceForm.getOrder().setProspectId(prospectCookie.getValue());
					logger.info("Prospect Id = " + prospectCookie.getValue());
					// delete Cookie after order placed.
					prospectCookie.setMaxAge(0);
					prospectCookie.setPath("/");
					prospectCookie.setValue("");
					response.addCookie(prospectCookie);
				}
			}
			// insert order
			this.webJaguar.insertOrder( invoiceForm.getOrder(), orderStatus, false, false, 0, siteConfig, gSiteConfig ); 
			
			// Error Message when inserting into CRM
			if(crmContact.getFirstName() == null || crmContact.getFirstName().isEmpty() || crmContact.getLastName() == null || crmContact.getLastName().isEmpty() || ((crmContact.getEmail1() == null || crmContact.getEmail1().isEmpty()) && (crmContact.getPhone1() == null || crmContact.getPhone1().isEmpty()))) {
				myModel.put("message", "f_quote_errorMessage");
				return new ModelAndView("frontend/quoteViewList", "model", myModel);  				
			} else {
				this.webJaguar.insertCrmContact(crmContact);
			}
			
			//Email to customer 
			if ( siteMessage != null ) 	{
				// send email
				orderStatus.setSubject( siteMessage.getSubject() );
				orderStatus.setMessage( siteMessage.getMessage());
				orderStatus.setHtmlMessage( siteMessage.isHtml() );
				notifyCustomer( orderStatus, request, invoiceForm.getOrder(), customer, crmContact, multiStore);
			}
			
			// Email to admin about quote to different id
			if(siteConfig.get("RFQ_EMAIL") != null && !siteConfig.get("RFQ_EMAIL").getValue().equals("")) {
				this.webJaguar.newLeadEmail(crmContact, siteConfig.get("RFQ_EMAIL").getValue(), null, null, request, null, null, mailSender);
		    } else { //Email to admin
				this.webJaguar.newLeadEmail(crmContact, siteConfig.get("CONTACT_EMAIL").getValue(), null, null, request, null, null, mailSender);
		    }

			myModel.put("customer", customer);
			
			// Thank you Page
			if(siteConfig.get("QUOTE_VIEW_LIST_THANK_YOU_ID") != null && !siteConfig.get("QUOTE_VIEW_LIST_THANK_YOU_ID").getValue().equals("")){
				String cid= siteConfig.get("QUOTE_VIEW_LIST_THANK_YOU_ID").getValue();				
				return new ModelAndView( new RedirectView("category.jhtm?cid="+cid));
			}
			
			myModel.put("successMessage", "f_quote_successMessage");			
			if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
				return new ModelAndView("frontend/layout/template6/quoteViewList", "model", myModel);		
			}
			return new ModelAndView("frontend/quoteViewList", "model", myModel);		
			
		} else {			
			if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
				return new ModelAndView("frontend/layout/template6/quoteViewList", "model", myModel);		
			}
			return new ModelAndView("frontend/quoteViewList", "model", myModel);    	
	    }
	} 
    
    
    private void generateRandomCardID(Customer customer) {
		String cardID = UUID.randomUUID().toString().toUpperCase().replace( "-", "" ).substring( 0, 9 );
		if (this.webJaguar.getCustomerByCardID( cardID ) == null) {
			customer.setCardId( cardID );
		} else
			generateRandomCardID(customer);
	}

    private void setProductOptions(Integer id, HttpServletRequest request, CartItem cartItem, Map<String, Object> myModel) {
    	
    	//Option Code
		Set<String> optionCodes = new HashSet<String>();
		for (String optionCode: ServletRequestUtils.getStringParameters(request, "optionCode_" + id)) {
			optionCode = optionCode.trim();
			if (optionCode.length() > 0 && optionCodes.add(optionCode.toLowerCase())) {
				// get product attributes return an empty array if not found.
				String options[] = ServletRequestUtils.getStringParameters(request, optionCode + "-option_" + id);
				if (options.length != 0) {
					for (int option_index = 0; option_index < options.length; option_index++) {
						String option_values[] = request.getParameterValues(optionCode + "-option_values_" + id + "_" + options[option_index]);
						if (option_values != null) {
							for (int values_index = 0; values_index < option_values.length; values_index++) {	
								cartItem.addProductAttribute(new ProductAttribute(optionCode, Integer.parseInt(options[option_index]), Integer.parseInt(option_values[values_index]), ServletRequestUtils.getStringParameter(request, optionCode + "-image_" + id + "_" + options[option_index] + "_" + option_values[values_index], null )));			
								myModel.put("optionValue_"+id, Integer.parseInt(option_values[values_index]));
							}
						}
						// one value for custom text
						String option_values_cus[] = request.getParameterValues(optionCode + "-option_values_cus_" + id + "_" + options[option_index]);
						if ( option_values_cus != null ) {		
							if (cartItem != null) {
								cartItem.addProductAttribute(new ProductAttribute(optionCode, Integer.parseInt(options[option_index]), option_values_cus[0].toString(), 0));							
							}
							myModel.put("optionValueCus_"+id , option_values_cus[0].toString());
						}
					}
				}
			}
		}
    }
    
    private Address setAddress(Address address) {
		Address newAddress = new Address();
		newAddress.setFirstName( address.getFirstName() );
		newAddress.setLastName( address.getLastName() );		
		newAddress.setCompany( address.getCompany() );
		newAddress.setAddr1( address.getAddr1() );
		newAddress.setAddr2( address.getAddr2() );
		newAddress.setCity( address.getCity() );
		newAddress.setStateProvince( address.getStateProvince() );
		newAddress.setZip( address.getZip() );
		newAddress.setCountry( address.getCountry() );
		newAddress.setPhone( address.getPhone() );
		newAddress.setCellPhone( address.getCellPhone() );
		newAddress.setFax( address.getFax() );
		newAddress.setResidential( address.isResidential() );
		newAddress.setLiftGate( address.isLiftGate() );
		newAddress.setCode(address.getCode());
		return newAddress;
	}
    
	private void setLayout(HttpServletRequest request) {
		Layout layout = (Layout) request.getAttribute( "layout" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 		
	}
	
	private void notifyCustomer(OrderStatus orderStatus, HttpServletRequest request, Order order, Customer customer, CrmContact crmContact, MultiStore multiStore)
	{
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
		String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);

		// send email
		String contactEmail = null;
		if(siteConfig.get("CONTACT_EMAIL") != null && !siteConfig.get("CONTACT_EMAIL").getValue().equals("")) {
			contactEmail =  siteConfig.get("CONTACT_EMAIL").getValue();
		}
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat( "MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale( request ) );

		StringBuffer messageBuffer = new StringBuffer();
		messageBuffer.append( dateFormatter.format( new Date() ) + "\n\n" );
		
		// replace dyamic elements
		try {
			this.webJaguar.replaceDynamicElement( orderStatus, customer, null, order, secureUrl, null);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#itemDesc#", (crmContact == null ? "" :crmContact.getDescription())));
		messageBuffer.append( orderStatus.getMessage() + "\n\n");
		
		try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			// bcc is removed because an email is already sent to ADMIN (newLeadEmail)
			
			helper.setSubject( orderStatus.getSubject() );
	    	helper.setText( messageBuffer.toString(), true );
	    	
			mailSender.send(mms);
		} catch (Exception ex) {
			//System.out.println("Exception " +ex);
		} 
	}
}