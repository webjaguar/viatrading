/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.frontend;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.support.PagedListHolder;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.ConsignmentReport;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.UserSession;
import com.webjaguar.thirdparty.echosign.EchoSignSearch;

public class ListConsingmentSalesController implements Controller {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	

    	UserSession userSession = this.webJaguar.getUserSession(request);	
    	Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		int custId = userSession.getUserid();
		model.put("userId", userSession.getUserid());
		Customer customer = this.webJaguar.getCustomerById( custId );

		ConsignmentReport salesReport = getSearch(request);
		salesReport.setNote(siteConfig.get("CONSIGNMENT_NOTE_FIELD").getValue());
		salesReport.setSupplierId(customer.getSupplierId());
		
		// set end date to end of day
		if (salesReport.getEndDate() != null) {
			salesReport.getEndDate().setTime(salesReport.getEndDate().getTime() 
					+ 23*60*60*1000 // hours
					+ 59*60*1000 	// minutes
					+ 59*1000);		// seconds			
		}
		
		PagedListHolder salesList = new PagedListHolder(this.webJaguar.getConsignmentSales(salesReport));
		salesList.setPageSize(salesReport.getPageSize());
		salesList.setPage(salesReport.getPage()-1);		
		
		Layout layout = (Layout) request.getAttribute( "layout" );
		layout.setTopBarHtml(layout.getTopBarHtml().replaceAll("#pageName#", "My Sales"));
		if ( siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" ).getValue().equals( "true" ) )
		{
			layout.setLeftBarTopHtml( "" );
			layout.setLeftBarBottomHtml( "" );
			layout.setHideLeftBar( true );
		} 
		this.webJaguar.updateCartQuantity(userSession, request, layout);
		model.put("salesList", salesList);			
		model.put("supplierId", customer.getSupplierId());
		model.put("viewMySalesLayout", this.webJaguar.getSystemLayout("viewMySalesLayout", request.getHeader("host"), request));
		return new ModelAndView("frontend/account/consignmentSales", "model", model);    	
    }

	private ConsignmentReport getSearch(HttpServletRequest request) {
		ConsignmentReport search = (ConsignmentReport) request.getSession().getAttribute("consignmentSalesSearch");
		if (search == null) {
			search = new ConsignmentReport();
			request.getSession().setAttribute("consignmentSalesSearch", search);
		}
			
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter( request, "page", 1 );
			if (page < 1) {
				search.setPage( 1 );
			} else {
				search.setPage( page );				
			}
		}
		
		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter( request, "size", 10 );
			if (size < 10) {
				search.setPageSize( 10 );
			} else {
				search.setPageSize( size );				
			}
		}	
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy" , RequestContextUtils.getLocale( request ));
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				search.setStartDate(df.parse( request.getParameter("startDate")));
			} catch (ParseException e) {
				search.setStartDate(null);
	        }
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				search.setEndDate(df.parse( request.getParameter("endDate")));
			} catch (ParseException e) {
				search.setEndDate(null);
	        }
		}
		
		return search;
	}

}
