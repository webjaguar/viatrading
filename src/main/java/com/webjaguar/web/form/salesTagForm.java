package com.webjaguar.web.form;

import java.util.Calendar;

import com.webjaguar.model.SalesTag;

public class salesTagForm {
	
	private com.webjaguar.model.SalesTag salesTag;
	private boolean newSalesTag = true;
	
	public salesTagForm() {
		this.salesTag = new SalesTag();
		Calendar now = Calendar.getInstance();
		now.set( Calendar.AM_PM, 0 );
		now.set( Calendar.HOUR, 6 );
		now.set( Calendar.MINUTE, 0 );
		this.getSalesTag().setStartDate( now.getTime() );

		now.set( Calendar.HOUR, 23 );
		this.getSalesTag().setEndDate( now.getTime() );
	}

	public boolean isNewSalesTag()
	{
		return newSalesTag;
	}

	public void setNewSalesTag(boolean newSalesTag)
	{
		this.newSalesTag = newSalesTag;
	}

	public com.webjaguar.model.SalesTag getSalesTag()
	{
		return salesTag;
	}

	public void setSalesTag(com.webjaguar.model.SalesTag salesTag)
	{
		this.salesTag = salesTag;
	}
}
