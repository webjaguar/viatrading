package com.webjaguar.web.form;

import com.webjaguar.model.FaqGroup;

public class FaqGroupForm {
	
    private FaqGroup faqGroup;
	
	private boolean newFaqGroup;
	
	public FaqGroupForm(FaqGroup faqGroup) {
		this.faqGroup = faqGroup;
		this.newFaqGroup = false;
	}
	
	public FaqGroupForm() {
		this.faqGroup = new FaqGroup();
		this.newFaqGroup = true;
	}

	public FaqGroup getFaqGroup() {
		return faqGroup;
	}

	public boolean isNewFaqGroup() {
		return newFaqGroup;
	}

}
