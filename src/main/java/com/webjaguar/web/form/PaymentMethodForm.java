/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.01.2007
 */

package com.webjaguar.web.form;

import java.util.List;

import com.webjaguar.model.PaymentMethod;

public class PaymentMethodForm {
	
	private List<PaymentMethod> customPaymentMethods;

	public List<PaymentMethod> getCustomPaymentMethods()
	{
		return customPaymentMethods;
	}

	public void setCustomPaymentMethods(List<PaymentMethod> customPaymentMethods)
	{
		this.customPaymentMethods = customPaymentMethods;
	}
}
