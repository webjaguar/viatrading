/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import java.io.File;
import java.util.List;

import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductImage;

public class ProductForm {
	
	private Product product;
	private Integer category;
	private boolean newProduct;
	private int priceTiers;
    private String message;
    private String skuPrefix;
    
	private Object categoryIds[];	
	private Object parentIds[];	
	
	// product fields
	List<ProductField> productFields;
	
	// image upload
	List<ProductImage> productImages;
	
	// supplier's image folder
	private File[] supplierFolder;
	
	private ProductImage selectedImage;
	
	public ProductForm(Product product) {
		this.product = product;
		this.newProduct = false;
	}
	
	public ProductForm() {
		this.product = new Product();
		this.newProduct = true;
	}

	public Product getProduct() {    		
		return product;
	}

	public boolean isNewProduct() {
		return newProduct;
	}

	public Integer getCategory() {
		return category;
	}

	public void setCategory(Integer category) {
		this.category = category;
	}

	public Object[] getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(Object[] categoryIds) {
		this.categoryIds = categoryIds;
	}

	public List<ProductField> getProductFields() { return productFields; }
	public void setProductFields(List<ProductField> productFields) {
		this.productFields = productFields;
	}

	public List<ProductImage> getProductImages() {
		return productImages;
	}

	public void setProductImages(List<ProductImage> productImages) {
		this.productImages = productImages;
	}

	public int getPriceTiers()
	{
		return priceTiers;
	}

	public void setPriceTiers(int priceTiers)
	{
		this.priceTiers = priceTiers;
	}

	public void setNewProduct(boolean newProduct)
	{
		this.newProduct = newProduct;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public String getSkuPrefix()
	{
		return skuPrefix;
	}

	public void setSkuPrefix(String skuPrefix)
	{
		this.skuPrefix = skuPrefix;
	}

	public File[] getSupplierFolder()
	{
		return supplierFolder;
	}

	public void setSupplierFolder(File[] supplierFolder)
	{
		this.supplierFolder = supplierFolder;
	}

	public ProductImage getSelectedImage()
	{
		return selectedImage;
	}

	public void setSelectedImage(ProductImage selectedImage)
	{
		this.selectedImage = selectedImage;
	}

	public Object[] getParentIds()
	{
		return parentIds;
	}

	public void setParentIds(Object[] parentIds)
	{
		this.parentIds = parentIds;
	}	

}
