/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.Location;

public class LocationForm {
	
	private Location location;
	private boolean newLocation;
	public LocationForm()
	{
		super();
		location = new Location();
		this.newLocation = true;
	}
	public LocationForm( Location location, boolean newLocation )
	{
		super();
		this.location = location;
		this.newLocation = newLocation;
	}
	public Location getLocation()
	{
		return location;
	}
	public void setLocation(Location location)
	{
		this.location = location;
	}
	public boolean isNewLocation()
	{
		return newLocation;
	}
	public void setNewLocation(boolean newLocation)
	{
		this.newLocation = newLocation;
	}
	

}
