package com.webjaguar.web.form;

import com.webjaguar.model.EventMember;

public class EventMemberForm {

	private EventMember eventMember;
	private boolean newEventMember;
	private Integer eventId;
	
	public EventMemberForm(EventMember eventMember) {
		super();
		this.eventMember = eventMember;
	}
	
	public EventMemberForm() {
		super();
		this.eventMember = new EventMember();
		newEventMember = true;
	}

	public EventMember getEventMember() {
		return eventMember;
	}
	public void setEventMember(EventMember eventMember) {
		this.eventMember = eventMember;
	}
	public boolean isNewEventMember() {
		return newEventMember;
	}
	public void setNewEventMember(boolean newEventMember) {
		this.newEventMember = newEventMember;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	
}
