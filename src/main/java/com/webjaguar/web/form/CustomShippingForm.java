package com.webjaguar.web.form;


import com.webjaguar.model.ShippingRate;

public class CustomShippingForm {
	private ShippingRate shippingRate;
	private boolean newCustomShipping = true;

	public CustomShippingForm()
	{
		this.setShippingRate(new ShippingRate());
	}

	public CustomShippingForm( ShippingRate shippingRate )
	{
		super();
		this.setShippingRate(shippingRate);
	}

	public void setNewCustomShipping(boolean newCustomShipping) {
		this.newCustomShipping = newCustomShipping;
	}

	public boolean isNewCustomShipping() {
		return newCustomShipping;
	}

	public void setShippingRate(ShippingRate shippingRate) {
		this.shippingRate = shippingRate;
	}

	public ShippingRate getShippingRate() {
		return shippingRate;
	}

}
