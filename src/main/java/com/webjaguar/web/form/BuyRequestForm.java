/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.BuyRequest;
import com.webjaguar.model.BuyRequestVersion;


public class BuyRequestForm {

	private BuyRequest buyRequest;
	private BuyRequestVersion buyRequestVersion;
	private boolean newBuyRequest = true;
	
	private Object categoryIds[] = {"", ""};	
	private Object parentIds[] = {"", ""};	
	
	public BuyRequestForm()
	{
		this.buyRequest = new BuyRequest();
		this.buyRequestVersion = new BuyRequestVersion();
		
	}
	public BuyRequestForm(BuyRequest buyRequest)
	{
		this.buyRequest = buyRequest;
		this.buyRequestVersion = new BuyRequestVersion();
	}
	public BuyRequest getBuyRequest()
	{
		return buyRequest;
	}
	public void setBuyRequest(BuyRequest buyRequest)
	{
		this.buyRequest = buyRequest;
	}
	public BuyRequestVersion getBuyRequestVersion()
	{
		return buyRequestVersion;
	}
	public void setBuyRequestVersion(BuyRequestVersion buyRequestVersion)
	{
		this.buyRequestVersion = buyRequestVersion;
	}
	public boolean isNewBuyRequest()
	{
		return newBuyRequest;
	}
	public void setNewBuyRequest(boolean newBuyRequest)
	{
		this.newBuyRequest = newBuyRequest;
	}
	public Object[] getCategoryIds()
	{
		return categoryIds;
	}
	public void setCategoryIds(Object[] categoryIds)
	{
		this.categoryIds = categoryIds;
	}
	public Object[] getParentIds()
	{
		return parentIds;
	}
	public void setParentIds(Object[] parentIds)
	{
		this.parentIds = parentIds;
	}
	
}
