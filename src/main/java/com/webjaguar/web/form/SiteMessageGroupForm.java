package com.webjaguar.web.form;

import com.webjaguar.model.CustomerGroup;
import com.webjaguar.model.SiteMessageGroup;

public class SiteMessageGroupForm {
	
	private SiteMessageGroup siteMessageGroup;
	private boolean newSiteMessageGroup;
	
	public SiteMessageGroupForm(SiteMessageGroup siteMessageGroup) {
		this.setSiteMessageGroup(siteMessageGroup);
		this.setNewSiteMessageGroup(false);
	}
	
	public SiteMessageGroupForm() {
		this.setSiteMessageGroup(new SiteMessageGroup());
		this.setNewSiteMessageGroup(true);
	}

	public SiteMessageGroup getSiteMessageGroup() {
		return siteMessageGroup;
	}

	public void setSiteMessageGroup(SiteMessageGroup siteMessageGroup) {
		this.siteMessageGroup = siteMessageGroup;
	}

	public boolean isNewSiteMessageGroup() {
		return newSiteMessageGroup;
	}

	public void setNewSiteMessageGroup(boolean newSiteMessageGroup) {
		this.newSiteMessageGroup = newSiteMessageGroup;
	}
}
