/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.CompanyReview;
import com.webjaguar.model.ProductReview;


public class ReviewForm {

	private ProductReview productReview;
	private CompanyReview companyReview;
	private boolean newReview = true;
	private String returnUrl;
	private String message;
	
	
	public ReviewForm()
	{
		this.productReview = new ProductReview();
	}
	public ProductReview getProductReview()
	{
		return productReview;
	}
	public void setProductReview(ProductReview productReview)
	{
		this.productReview = productReview;
	}
	public CompanyReview getCompanyReview()
	{
		return companyReview;
	}
	public void setCompanyReview(CompanyReview companyReview)
	{
		this.companyReview = companyReview;
	}
	public boolean isNewReview()
	{
		return newReview;
	}
	public void setNewReview(boolean newReview)
	{
		this.newReview = newReview;
	}
	public String getReturnUrl()
	{
		return returnUrl;
	}
	public void setReturnUrl(String returnUrl)
	{
		this.returnUrl = returnUrl;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
