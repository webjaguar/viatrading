/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.Ticket;
import com.webjaguar.model.TicketVersion;


public class TicketForm {

	private Ticket ticket;
	private TicketVersion ticketVersion;
	private boolean onEmail = true;
	
	
	public TicketForm()
	{
		this.ticket = new Ticket();
		this.ticketVersion = new TicketVersion();
		
	}
	public TicketForm(Ticket ticket)
	{
		this.ticket = ticket;
		this.ticketVersion = new TicketVersion();
	}
	public Ticket getTicket()
	{
		return ticket;
	}
	public void setTicket(Ticket ticket)
	{
		this.ticket = ticket;
	}
	public TicketVersion getTicketVersion()
	{
		return ticketVersion;
	}
	public void setTicketVersion(TicketVersion ticketVersion)
	{
		this.ticketVersion = ticketVersion;
	}
	public boolean isOnEmail() {
		return onEmail;
	}
	public void setOnEmail(boolean onEmail) {
		this.onEmail = onEmail;
	}
}
