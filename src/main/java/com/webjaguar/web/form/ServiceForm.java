/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.28.2007
 */

package com.webjaguar.web.form;

import com.webjaguar.model.ServiceWork;

public class ServiceForm {
	
	private ServiceWork service;

	public ServiceWork getService()
	{
		return service;
	}

	public void setService(ServiceWork service)
	{
		this.service = service;
	}

}
