/* Copyright 2005, 2007 Advanced E-Media Solutions
 * Author Shahin Naji
 */

package com.webjaguar.web.form;

import java.util.ArrayList;

import com.webjaguar.model.Order;
import com.webjaguar.model.PurchaseOrder;
import com.webjaguar.model.PurchaseOrderLineItem;
import com.webjaguar.model.PurchaseOrderStatus;


public class PurchaseOrderForm {
	
	private PurchaseOrder purchaseOrder;
	private PurchaseOrderStatus purchaseOrderStatus;
	private boolean newPurchaseOrder;
	private String errorMessage;
	
	private Order order;

	public PurchaseOrderStatus getPurchaseOrderStatus()
	{
		return purchaseOrderStatus;
	}

	public void setPurchaseOrderStatus(PurchaseOrderStatus purchaseOrderStatus)
	{
		this.purchaseOrderStatus = purchaseOrderStatus;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public PurchaseOrderForm()
	{
		this.purchaseOrder = new PurchaseOrder();
		this.purchaseOrderStatus = new PurchaseOrderStatus();
		this.purchaseOrder.setStatus( this.purchaseOrderStatus.getStatus() );
		this.newPurchaseOrder = true;
	}
	
	public PurchaseOrderForm( PurchaseOrder purchaseOrder )
	{
		super();
		this.purchaseOrder = purchaseOrder;
		
	}

	public PurchaseOrder getPurchaseOrder()
	{
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder)
	{
		this.purchaseOrder = purchaseOrder;
	}

	public boolean isNewPurchaseOrder()
	{
		return newPurchaseOrder;
	}

	public void setNewPurchaseOrder(boolean newPurchaseOrder)
	{
		this.newPurchaseOrder = newPurchaseOrder;
	}

	public Order getOrder()
	{
		return order;
	}

	public void setOrder(Order order)
	{
		this.order = order;
	}
	

}
