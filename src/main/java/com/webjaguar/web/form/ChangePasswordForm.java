/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.AccessUser;

public class ChangePasswordForm
{
    private AccessUser user;
	private String confirmPassword;
	private String currentPassword;
	
	public AccessUser getUser()
	{
		return user;
	}
	public void setUser(AccessUser user)
	{
		this.user = user;
	}
	public String getConfirmPassword()
	{
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword)
	{
		this.confirmPassword = confirmPassword;
	}
	public String getCurrentPassword()
	{
		return currentPassword;
	}
	public void setCurrentPassword(String currentPassword)
	{
		this.currentPassword = currentPassword;
	}
	public ChangePasswordForm()
	{
		this.user = new AccessUser();
	}
}
