package com.webjaguar.web.form;

import com.webjaguar.model.SalesRep;

public class SalesRepForm
{

	private SalesRep salesRep;
	private boolean newSalesRep = true;
	private boolean protectedAccess[];
	
	public SalesRepForm()
	{
		this.salesRep = new SalesRep();
	}
	public boolean isNewSalesRep()
	{
		return newSalesRep;
	}
	public void setNewSalesRep(boolean newSalesRep)
	{
		this.newSalesRep = newSalesRep;
	}
	public SalesRep getSalesRep()
	{
		return salesRep;
	}
	public void setSalesRep(SalesRep salesRep)
	{
		this.salesRep = salesRep;
	}

	public boolean[] getProtectedAccess() {
		return protectedAccess;
	}

	public void setProtectedAccess(boolean[] protectedAccess) {
		this.protectedAccess = protectedAccess;
	}
	
}
