/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import java.util.List;

import com.webjaguar.model.Address;
import com.webjaguar.model.BrokerImage;
import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerField;

public class CustomerForm {

	private Customer customer;
	private String confirmUsername;
	private String existingUsername;
	private String emailRegExp;
	private String currentPassword;
	private String confirmPassword;
	private int minPasswordLength;
	private boolean strongPassword;
	private String forwardAction;
	private boolean protectedAccess[];
	private boolean newCustomer;
	private boolean companyRequired;
	//private boolean evergreenClient;
	private boolean currentIsAfiliate;
	private Address shipping;
	
	private Object categoryIds[];
	private Object parentIds[];
	
	// customer fields
	List<CustomerField> customerFields;
	private String language = "en";

	private boolean touchScreen;
	private boolean subscribeEmail;
	
	private boolean sameAsBilling;
	
	/* Constructors */	

	public CustomerForm() {
		this.customer = new Customer();
		this.customer.setAddress(new Address());
	}	
	
	public CustomerForm(Customer customer) {
		this.customer = customer;
		this.existingUsername = customer.getUsername();
	}
	
	public CustomerForm(String forwardAction) {
		this.customer = new Customer();
		this.customer.setAddress(new Address());
		this.forwardAction = forwardAction;
	}	

	public Customer getCustomer() { return customer; }
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getExistingUsername() { return existingUsername; }
	
	public String getConfirmPassword() { return confirmPassword; }
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getCurrentPassword() { return currentPassword; }
	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public int getMinPasswordLength() {
		return minPasswordLength;
	}

	public void setMinPasswordLength(int minPasswordLength) {
		this.minPasswordLength = minPasswordLength;
	}

	public boolean isStrongPassword() {
		return strongPassword;
	}

	public void setStrongPassword(boolean strongPassword) {
		this.strongPassword = strongPassword;
	}

	public String getForwardAction() { return forwardAction; }
	public void setForwardAction(String forwardAction) {
		this.forwardAction = forwardAction;
	}

	public boolean[] getProtectedAccess() {
		return protectedAccess;
	}

	public void setProtectedAccess(boolean[] protectedAccess) {
		this.protectedAccess = protectedAccess;
	}

	public boolean isNewCustomer()
	{
		return newCustomer;
	}

	public void setNewCustomer(boolean newCustomer)
	{
		this.newCustomer = newCustomer;
	}

	public boolean isCompanyRequired()
	{
		return companyRequired;
	}

	public void setCompanyRequired(boolean companyRequired)
	{
		this.companyRequired = companyRequired;
	}

	public List<CustomerField> getCustomerFields()
	{
		return customerFields;
	}

	public void setCustomerFields(List<CustomerField> customerFields)
	{
		this.customerFields = customerFields;
	}

	public String getConfirmUsername()
	{
		return confirmUsername;
	}

	public void setConfirmUsername(String confirmUsername)
	{
		this.confirmUsername = confirmUsername;
	}

//	public boolean isEvergreenClient()
//	{
//		return evergreenClient;
//	}
//
//	public void setEvergreenClient(boolean evergreenClient)
//	{
//		this.evergreenClient = evergreenClient;
//	}

	public Address getShipping()
	{
		return shipping;
	}

	public void setShipping(Address shipping)
	{
		this.shipping = shipping;
	}

	public Object[] getCategoryIds()
	{
		return categoryIds;
	}

	public void setCategoryIds(Object[] categoryIds)
	{
		this.categoryIds = categoryIds;
	}

	public Object[] getParentIds()
	{
		return parentIds;
	}

	public void setParentIds(Object[] parentIds)
	{
		this.parentIds = parentIds;
	}
	
	public String getLanguage()
	{
		return language;
	}

	public void setLanguage(String language)
	{
		this.language = language;
	}
	
	public boolean isEqualAddress() {
		if ( this.customer.getAddress().compareTo( this.shipping ) == 1)
			return true;
		else
			return false;
	}

	public boolean isTouchScreen() {
		return touchScreen;
	}

	public void setTouchScreen(boolean touchScreen) {
		this.touchScreen = touchScreen;
	}
	public boolean isCurrentIsAfiliate() {
		return currentIsAfiliate;
	}

	public void setCurrentIsAfiliate(boolean currentIsAfiliate) {
		this.currentIsAfiliate = currentIsAfiliate;
	}

	public boolean isSubscribeEmail() {
		return subscribeEmail;
	}

	public void setSubscribeEmail(boolean subscribeEmail) {
		this.subscribeEmail = subscribeEmail;
	}

	public boolean isSameAsBilling() {
		return sameAsBilling;
	}

	public void setSameAsBilling(boolean sameAsBilling) {
		this.sameAsBilling = sameAsBilling;
	}

	public String getEmailRegExp() {
		return emailRegExp;
	}

	public void setEmailRegExp(String emailRegExp) {
		this.emailRegExp = emailRegExp;
	}

}
