package com.webjaguar.web.form;

import com.webjaguar.model.Contact;

public class ContactForm {
	private Contact contact;
	private boolean newContact;
	
	public ContactForm(Contact contact) {
		this.contact = contact;
	}
	
	public ContactForm() {
		this.contact = new Contact();
		this.newContact = true;
	}

	public Contact getContact() {
		return contact;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	public boolean isNewContact() {
		return newContact;
	}
	public void setNewContact(boolean newContact) {
		this.newContact = newContact;
	}
}
