/* Copyright 2005, 2007 Advanced E-Media Solutions
 * Author Shahin Naji
 */

package com.webjaguar.web.form;

import com.webjaguar.model.Customer;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.PackingList;

public class PackingListForm {
	
	private PackingList packingList;
	private EmailMessageForm emailMessageForm;
	private String errorMessage;
	private boolean newPackingList = true;
	
	private Order order;
	private Customer customer;
	private OrderStatus orderStatus = new OrderStatus();
	
	public Customer getCustomer()
	{
		return customer;
	}
	public void setCustomer(Customer customer)
	{
		this.customer = customer;
	}
	public Order getOrder()
	{
		return order;
	}
	public void setOrder(Order order)
	{
		this.order = order;
	}
	public OrderStatus getOrderStatus()
	{
		return orderStatus;
	}
	public void setOrderStatus(OrderStatus orderStatus)
	{
		this.orderStatus = orderStatus;
	}
	public PackingList getPackingList()
	{
		return packingList;
	}
	public void setPackingList(PackingList packingList)
	{
		this.packingList = packingList;
	}
	public boolean isNewPackingList()
	{
		return newPackingList;
	}
	public void setNewPackingList(boolean newPackingList)
	{
		this.newPackingList = newPackingList;
	}
	public EmailMessageForm getEmailMessageForm()
	{
		return emailMessageForm;
	}
	public void setEmailMessageForm(EmailMessageForm emailMessageForm)
	{
		this.emailMessageForm = emailMessageForm;
	}
	public String getErrorMessage()
	{
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}
}
