/* Copyright 2011 Advanced E-Media Solutions
 *
 */
package com.webjaguar.web.form;

import java.util.Date;

import com.webjaguar.model.TruckLoadProcess;


public class TruckLoadProcessForm {

	private String name;
	private TruckLoadProcess truckLoadProcess;
	private Boolean newTruckLoadProcess;
	private String custom1;
	private String custom2;
	private String custom3;
	private String custom4;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TruckLoadProcess getTruckLoadProcess() {
		return truckLoadProcess;
	}

	public void setTruckLoadProcess(TruckLoadProcess truckLoadProcess) {
		this.truckLoadProcess = truckLoadProcess;
	}

	public String getCustom1() {
		return custom1;
	}

	public void setCustom1(String custom1) {
		this.custom1 = custom1;
	}

	public String getCustom2() {
		return custom2;
	}

	public void setCustom2(String custom2) {
		this.custom2 = custom2;
	}

	public String getCustom3() {
		return custom3;
	}

	public void setCustom3(String custom3) {
		this.custom3 = custom3;
	}

	public String getCustom4() {
		return custom4;
	}

	public void setCustom4(String custom4) {
		this.custom4 = custom4;
	}

	public Boolean getNewTruckLoadProcess() {
		return newTruckLoadProcess;
	}

	public void setNewTruckLoadProcess(Boolean newTruckLoadProcess) {
		this.newTruckLoadProcess = newTruckLoadProcess;
	}

}
