package com.webjaguar.web.form;

import java.util.List;

import com.webjaguar.model.Presentation;
import com.webjaguar.model.Product;

public class ProductPresentationForm {
	
	private List<Product> product;
	private List<Presentation> presentationList;
	private List<Integer> productId;
	private Presentation presentation;
	private boolean added;
	
	public List<Presentation> getPresentationList() {
		return presentationList;
	}
	public void setPresentationList(List<Presentation> presentationList) {
		this.presentationList = presentationList;
	}
	public Presentation getPresentation() {
		return presentation;
	}
	public void setPresentation(Presentation presentation) {
		this.presentation = presentation;
	}
	public boolean isAdded() {
		return added;
	}
	public void setAdded(boolean added) {
		this.added = added;
	}
	public List<Integer> getProductId() {
		return productId;
	}
	public void setProductId(List<Integer> productId) {
		this.productId = productId;
	}
	public List<Product> getProduct() {
		return product;
	}
	public void setProduct(List<Product> product) {
		this.product = product;
	}

}
