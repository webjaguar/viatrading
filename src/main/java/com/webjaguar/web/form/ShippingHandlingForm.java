/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 06.06.2007
 */

package com.webjaguar.web.form;

import java.util.List;

import com.webjaguar.model.ShippingHandling;
import com.webjaguar.model.ShippingRate;

public class ShippingHandlingForm {
	
	private ShippingHandling shippingHandling;
	private List<ShippingRate> customShippingRates;

	public List<ShippingRate> getCustomShippingRates() {
		return customShippingRates;
	}

	public void setCustomShippingRates(List<ShippingRate> customShippingRates) {
		this.customShippingRates = customShippingRates;
	}

	public ShippingHandling getShippingHandling()
	{
		return shippingHandling;
	}

	public void setShippingHandling(ShippingHandling shippingHandling)
	{
		this.shippingHandling = shippingHandling;
	}
}
