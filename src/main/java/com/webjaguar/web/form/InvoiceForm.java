package com.webjaguar.web.form;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.webjaguar.model.CustomerBudgetPartner;
import com.webjaguar.model.Order;

public class InvoiceForm {
	
	private Order order;
	private String errorMessage;
	private boolean newInvoice;
	private Date orderDate;
	private boolean applyUserPoint;	
	private List<CustomerBudgetPartner> partnersList;
	
	// Budget Earned Credits
	private Map<Integer, Double>  budgetPartnerHistory;

	public Date getOrderDate()
	{
		return orderDate;
	}

	public void setOrderDate(Date orderDate)
	{
		this.orderDate = orderDate;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public InvoiceForm() {
		this.order = new Order();
	}
	
	public InvoiceForm(Order order){
		this.order = order;
	}
    
	// setter and getter
	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public boolean isNewInvoice()
	{
		return newInvoice;
	}

	public void setNewInvoice(boolean newInvoice)
	{
		this.newInvoice = newInvoice;
	}

	public boolean isApplyUserPoint() {
		return applyUserPoint;
	}

	public void setApplyUserPoint(boolean applyUserPoint) {
		this.applyUserPoint = applyUserPoint;
	}

	public List<CustomerBudgetPartner> getPartnersList() {
		return partnersList;
	}

	public void setPartnersList(List<CustomerBudgetPartner> partnersList) {
		this.partnersList = partnersList;
	}

	public Map<Integer, Double> getBudgetPartnerHistory() {
		return budgetPartnerHistory;
	}

	public void setBudgetPartnerHistory(Map<Integer, Double> budgetPartnerHistory) {
		this.budgetPartnerHistory = budgetPartnerHistory;
	}
}