/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.Policy;

public class PolicyForm {
	
	private Policy policy;
	
	private boolean newPolicy;
	
	public PolicyForm(Policy policy) {
		this.policy = policy;
		this.newPolicy = false;
	}
	
	public PolicyForm() {
		this.policy = new Policy();
		this.newPolicy = true;
	}

	public Policy getPolicy() {
		return policy;
	}

	public boolean isNewPolicy() {
		return newPolicy;
	}

}
