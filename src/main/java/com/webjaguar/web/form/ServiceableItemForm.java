/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.24.2007
 */

package com.webjaguar.web.form;

import com.webjaguar.model.Address;
import com.webjaguar.model.ServiceableItem;

public class ServiceableItemForm {
	
	private ServiceableItem item;
	private boolean newItem;

	public ServiceableItemForm() {
		this.item = new ServiceableItem();
		this.newItem = true;
		this.item.setAddress(new Address());
	}

	public ServiceableItemForm(ServiceableItem item) {
		this.item = item;
		this.newItem = false;
	}
	
	public ServiceableItem getItem()
	{
		return item;
	}

	public void setItem(ServiceableItem item)
	{
		this.item = item;
	}

	public boolean isNewItem()
	{
		return newItem;
	}

	public void setNewItem(boolean newItem)
	{
		this.newItem = newItem;
	}


}
