/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

public class UploadForm {

	// file upload
	private byte[] file;


	public byte[] getFile() { return file; }
	public void setFile(byte[] file) {
		this.file = file;
	}
}
