package com.webjaguar.web.form;

import com.webjaguar.model.PresentationTemplate;

public class TemplateForm {
	private PresentationTemplate template;
	private boolean newTemplate;

	public PresentationTemplate getTemplate() {
		return template;
	}

	public void setTemplate(PresentationTemplate template) {
		this.template = template;
	}

	public boolean isNewTemplate() {
		return newTemplate;
	}

	public void setNewTemplate(boolean newTemplate) {
		this.newTemplate = newTemplate;
	}

}
