/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 08.01.2007
 */

package com.webjaguar.web.form;

import java.math.BigDecimal;
import java.util.*;

import com.webjaguar.model.Customer;
import com.webjaguar.model.Order;
import com.webjaguar.model.Payment;

public class PaymentForm {

	private Payment payment;
	private Map<Integer, Order> invoices;
	private boolean newPayment;

	public PaymentForm() {}
	
	public PaymentForm(Customer customer) {
		payment = new Payment();
		payment.setCustomer( customer );
		newPayment = true;
		payment.setDate( new Date() );
	}
	
	public Payment getPayment()
	{
		return payment;
	}

	public void setPayment(Payment payment)
	{
		this.payment = payment;
	}

	public Map<Integer, Order> getInvoices()
	{
		return invoices;
	}

	public void setInvoices(Map<Integer, Order> invoices)
	{
		this.invoices = invoices;
	}

	public Collection<Order> getInvoiceList() {
		return invoices.values();
	}
	
	public Double getTotalPayments()
	{
		double total = 0;
		for (Order order: invoices.values()) {
			if (order.getPayment() != null) total = order.getPayment() + total;
		}
		if (total > 0) {
			BigDecimal bd = new BigDecimal(total);
			return bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		}
		return null;
	}

	public boolean isNewPayment()
	{
		return newPayment;
	}

	public void setNewPayment(boolean newPayment)
	{
		this.newPayment = newPayment;
	}
}
