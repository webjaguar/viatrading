package com.webjaguar.web.form;

import com.webjaguar.model.Address;
import com.webjaguar.model.Supplier;

public class ProductSupplierForm {
	
	private Supplier supplier;
	private String sku;
	private Integer supplierId;
	private boolean newSupplier = true;
	private boolean canDelete = true;
	private boolean setAsPrimary;
	
	public ProductSupplierForm() {
		this.supplier = new Supplier();
		this.supplier.setAddress( new Address() );
		newSupplier = true;
	}
	
	public ProductSupplierForm(Supplier supplier) {
		this.supplier = supplier;
		newSupplier = false;
	}

	public boolean isNewSupplier()
	{
		return newSupplier;
	}

	public void setNewSupplier(boolean newSupplier)
	{
		this.newSupplier = newSupplier;
	}

	public Integer getSupplierId()
	{
		return supplierId;
	}

	public void setSupplierId(Integer supplierId)
	{
		this.supplierId = supplierId;
	}

	public Supplier getSupplier()
	{
		return supplier;
	}

	public void setSupplier(Supplier supplier)
	{
		this.supplier = supplier;
	}

	public String getSku()
	{
		return sku;
	}

	public void setSku(String sku)
	{
		this.sku = sku;
	}

	public boolean isCanDelete()
	{
		return canDelete;
	}

	public void setCanDelete(boolean canDelete)
	{
		this.canDelete = canDelete;
	}

	public boolean isSetAsPrimary() {
		return setAsPrimary;
	}

	public void setSetAsPrimary(boolean setAsPrimary) {
		this.setAsPrimary = setAsPrimary;
	}

}
