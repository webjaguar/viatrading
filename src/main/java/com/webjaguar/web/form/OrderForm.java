/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.webjaguar.model.CreditCard;
import com.webjaguar.model.Customer;
import com.webjaguar.model.GiftCard;
import com.webjaguar.model.Order;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ShippingRate;

public class OrderForm {

	private final Order order = new Order();
	private String username;
	private String password;
	private boolean newCustomer;
	private String message;
	private String errorMessage;
	private boolean newShippingAddress;
	private boolean newBillingAddress;
	private String shippingRateIndex;
	private List<ShippingRate> ratesList;
	private String tempPromoErrorMessage;
	private boolean promoValidated = true;
	private String shippingCarrier;
	private boolean companyRequired;
	private List<Integer> parentsId;
	private String customerShippingTitle;
	// Affiliate promocode link
	private String promocodeLink;
	private String poRequired;

	private String custPayment;
	private Double customerCredit;
	private GiftCard tempGiftCard;
	private String tempGiftCardCode;
	private boolean applyUserPoint;	
	
	// order file upload
	private File customerFolder;
	
	private int creditCardAttempts;
	private List<ShippingRate> customRatesList;
	private Integer customShippingRateIndex;

	private boolean treshold = false;
	
	private Customer customer;
	
	List<ProductField> productFieldsHeader;
	
	//PayPalPro UseMyaacount
	private boolean payPalProUseMyAccount=false;
	
	// Budget Earned Credits
	private Map<Integer, Double>  budgetPartnerHistory;
	
	// EbizCredit Card 
	private boolean saveCC = false;
	private CreditCard tempCreditCard;

	public boolean isApplyUserPoint()
	{
		return applyUserPoint;
	}

	public void setApplyUserPoint(boolean applyUserPoint)
	{
		this.applyUserPoint = applyUserPoint;
	}

	public Double getCustomerCredit()
	{
		return customerCredit;
	}

	public void setCustomerCredit(Double customerCredit)
	{
		this.customerCredit = customerCredit;
	}

	public List<Integer> getParentsId()
	{
		return parentsId;
	}

	public void setParentsId(List<Integer> parentsId)
	{
		this.parentsId = parentsId;
	}

	public boolean isPromoValidated()
	{
		return promoValidated;
	}

	public void setPromoValidated(boolean promoValidated)
	{
		this.promoValidated = promoValidated;
	}

	public Order getOrder() {
		return order;
	}
	
	public String getUsername() { return username; }
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() { return password; }
	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isNewCustomer() { return newCustomer; }
	public void setNewCustomer(boolean newCustomer) {
		this.newCustomer = newCustomer;
	}
	
	public String getMessage() { return message; }
	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isNewShippingAddress() { return newShippingAddress; }
	public void setNewShippingAddress(boolean newShippingAddress) {
		this.newShippingAddress = newShippingAddress;
	}

	public boolean isNewBillingAddress() { return newBillingAddress; }
	public void setNewBillingAddress(boolean newBillingAddress) {
		this.newBillingAddress = newBillingAddress;
	}

	public String getShippingRateIndex() { return shippingRateIndex; }
    public void setShippingRateIndex(String shippingRateIndex) { 
		this.shippingRateIndex = shippingRateIndex;
	}

	public List<ShippingRate> getRatesList() { return ratesList; }

	public void setRatesList(List<ShippingRate> ratesList) {
		this.ratesList = ratesList;
	}

	public String getErrorMessage() { return errorMessage; }

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getTempPromoErrorMessage()
	{
		return tempPromoErrorMessage;
	}

	public void setTempPromoErrorMessage(String tempPromoErrorMessage)
	{
		this.tempPromoErrorMessage = tempPromoErrorMessage;
	}

	public boolean isCompanyRequired()
	{
		return companyRequired;
	}

	public void setCompanyRequired(boolean companyRequired)
	{
		this.companyRequired = companyRequired;
	}

	public String getPromocodeLink()
	{
		return promocodeLink;
	}
	public String getCustPayment()
	{
		return custPayment;
	}

	public void setPromocodeLink(String promocodeLink)
	{
		this.promocodeLink = promocodeLink;
	}

	public void setCustPayment(String custPayment)
	{
		this.custPayment = custPayment;
	}

	public String getCustomerShippingTitle()
	{
		return customerShippingTitle;
	}

	public void setCustomerShippingTitle(String customerShippingTitle)
	{
		this.customerShippingTitle = customerShippingTitle;
	}

	public String getPoRequired()
	{
		return poRequired;
	}

	public void setPoRequired(String poRequired)
	{
		this.poRequired = poRequired;
	}

	public String getTempGiftCardCode()
	{
		return tempGiftCardCode;
	}

	public void setTempGiftCardCode(String tempGiftCardCode)
	{
		this.tempGiftCardCode = tempGiftCardCode;
	}

	public GiftCard getTempGiftCard()
	{
		return tempGiftCard;
	}

	public void setTempGiftCard(GiftCard tempGiftCard)
	{
		this.tempGiftCard = tempGiftCard;
	}

	public File getCustomerFolder()
	{
		return customerFolder;
	}

	public void setCustomerFolder(File customerFolder)
	{
		this.customerFolder = customerFolder;
	}

	public int getCreditCardAttempts()
	{
		return creditCardAttempts;
	}

	public void setCreditCardAttempts(int creditCardAttempts)
	{
		this.creditCardAttempts = creditCardAttempts;
	}

	public List<ShippingRate> getCustomRatesList()
	{
		return customRatesList;
	}

	public void setCustomRatesList(List<ShippingRate> customRatesList)
	{
		this.customRatesList = customRatesList;
	}

	public Integer getCustomShippingRateIndex()
	{
		return customShippingRateIndex;
	}

	public void setCustomShippingRateIndex(Integer customShippingRateIndex)
	{
		this.customShippingRateIndex = customShippingRateIndex;
	}

	public Customer getCustomer()
	{
		return customer;
	}

	public void setCustomer(Customer customer)
	{
		this.customer = customer;
	}

	public List<ProductField> getProductFieldsHeader() {
		return productFieldsHeader;
	}

	public void setProductFieldsHeader(List<ProductField> productFieldsHeader) {
		this.productFieldsHeader = productFieldsHeader;
	}

	public void setShippingCarrier(String shippingCarrier) {
		this.shippingCarrier = shippingCarrier;
	}

	public String getShippingCarrier() {
		return shippingCarrier;
	}

	public void setTreshold(boolean treshold) {
		this.treshold = treshold;
	}

	public boolean isTreshold() {
		return treshold;
	}

	public boolean isPayPalProUseMyAccount() {
		return payPalProUseMyAccount;
	}

	public void setPayPalProUseMyAccount(boolean payPalProUseMyAccount) {
		this.payPalProUseMyAccount = payPalProUseMyAccount;
	}

	public boolean isSaveCC() {
		return saveCC;
	}

	public void setSaveCC(boolean saveCC) {
		this.saveCC = saveCC;
	}


	public CreditCard getTempCreditCard() {
		return tempCreditCard;
	}

	public void setTempCreditCard(CreditCard tempCreditCard) {
		this.tempCreditCard = tempCreditCard;
	}

	public Map<Integer, Double> getBudgetPartnerHistory() {
		return budgetPartnerHistory;
	}

	public void setBudgetPartnerHistory(Map<Integer, Double> budgetPartnerHistory) {
		this.budgetPartnerHistory = budgetPartnerHistory;
	}
	
	public Double getSubTotalByPlan(){
		Double value = null;
		if(this.order.getBudgetEarnedCredits() != null && this.customer.getBudgetPlan() != null && this.customer.getBudgetPlan().equalsIgnoreCase("plan-b")){				
			value= this.order.getSubTotal() - this.order.getBudgetEarnedCredits();
		} else {
			value= this.order.getSubTotal();
		}
		return value;
	}
}
