package com.webjaguar.web.form;

import java.util.List;

import com.webjaguar.model.Presentation;
import com.webjaguar.model.PresentationTemplate;


public class PresentationForm {
	private Integer id;
	private String name;
	private List<PresentationTemplate> templateList;
	private Presentation presentation;
	private boolean newPresentation;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<PresentationTemplate> getTemplateList() {
		return templateList;
	}
	public void setTemplateList(List<PresentationTemplate> templateList) {
		this.templateList = templateList;
	}
	public Presentation getPresentation() {
		return presentation;
	}
	public void setPresentation(Presentation presentation) {
		this.presentation = presentation;
	}
	public boolean isNewPresentation() {
		return newPresentation;
	}
	public void setNewPresentation(boolean newPresentation) {
		this.newPresentation = newPresentation;
	}
	
}
