/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.Faq;

public class FaqForm {
	
	private Faq faq;
	
	private boolean newFaq;
	
	public FaqForm(Faq faq) {
		this.faq = faq;
		this.newFaq = false;
	}
	
	public FaqForm() {
		this.faq = new Faq();
		this.newFaq = true;
	}

	public Faq getFaq() {
		return faq;
	}

	public boolean isNewFaq() {
		return newFaq;
	}

	public void setNewFaq(boolean newFaq)
	{
		this.newFaq = newFaq;
	}

}
