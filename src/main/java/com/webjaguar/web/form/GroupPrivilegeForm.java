/* Copyright 2005, 2010 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.AccessGroupPrivilege;

public class GroupPrivilegeForm {
	
	private AccessGroupPrivilege privilege;
	private boolean newPrivilege;

	public GroupPrivilegeForm(AccessGroupPrivilege privilege) {
		this.privilege = privilege;
		this.newPrivilege = false;
	}
	
	public GroupPrivilegeForm() {
		this.privilege = new AccessGroupPrivilege();
		this.newPrivilege = true;
	}

	public boolean isNewPrivilege()
	{
		return newPrivilege;
	}

	public void setNewPrivilege(boolean newPrivilege)
	{
		this.newPrivilege = newPrivilege;
	}

	public AccessGroupPrivilege getPrivilege()
	{
		return privilege;
	}

	public void setPrivilege(AccessGroupPrivilege privilege)
	{
		this.privilege = privilege;
	}
}
