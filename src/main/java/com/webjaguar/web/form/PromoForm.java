package com.webjaguar.web.form;

import java.util.Calendar;

import com.webjaguar.model.Promo;

public class PromoForm
{

	private Promo promo;
	private boolean newPromo = true;

	public PromoForm()
	{
		this.promo = new com.webjaguar.model.Promo();
		Calendar now = Calendar.getInstance();
		now.set( Calendar.AM_PM, 0 );
		now.set( Calendar.HOUR, 6 );
		now.set( Calendar.MINUTE, 0 );
		this.getPromo().setStartDate( now.getTime() );

		now.set( Calendar.HOUR, 23 );
		this.getPromo().setEndDate( now.getTime() );
	}

	public PromoForm( Promo promo )
	{
		super();
		this.promo = promo;
	}

	public boolean isNewPromo()
	{
		return newPromo;
	}

	public void setNewPromo(boolean newPromo)
	{
		this.newPromo = newPromo;
	}

	public com.webjaguar.model.Promo getPromo()
	{
		return promo;
	}

	public void setPromo(com.webjaguar.model.Promo promo)
	{
		this.promo = promo;

	}

}
