/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.CustomerExtContact;

public class CustomerExtContactForm {
	
	private CustomerExtContact customerExtContact;
	private boolean newCustomerExtContact;

	
	public CustomerExtContactForm(CustomerExtContact customerExtContact) {
		this.customerExtContact = customerExtContact;
		this.newCustomerExtContact = false;
	}
	
	public CustomerExtContactForm() {
		this.customerExtContact = new CustomerExtContact();
		this.newCustomerExtContact = true;
	}

	public CustomerExtContact getCustomerExtContact() {
		return customerExtContact;
	}

	public boolean isNewCustomerExtContact() {
		return newCustomerExtContact;
	}

}
