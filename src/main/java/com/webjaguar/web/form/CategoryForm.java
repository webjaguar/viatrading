/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import java.util.HashMap;
import java.util.Map;

import com.webjaguar.model.Category;

public class CategoryForm {
	
	private Category category;
	
	private boolean newCategory;
	
	private Integer oldParent;
	
	private Map<String, Category> i18nCategory;
	
	public CategoryForm(Category category) {
		this.category = category;
		this.newCategory = false;
	}
	
	public CategoryForm() {
		this.category = new Category();
		this.newCategory = true;
	}

	public Category getCategory() {
		return category;
	}

	public boolean isNewCategory() {
		return newCategory;
	}

	public Integer getOldParent()
	{
		return oldParent;
	}

	public void setOldParent(Integer oldParent)
	{
		this.oldParent = oldParent;
	}

	public void setCategory(Category category)
	{
		this.category = category;
	}

	public void setNewCategory(boolean newCategory)
	{
		this.newCategory = newCategory;
	}

	public Map<String, Category> getI18nCategory()
	{
		return i18nCategory;
	}

	public void setI18nCategory(Map<String, Category> category)
	{
		i18nCategory = category;
	}
	
	public void addI18nCategory(String lang, Category category) {
		if (i18nCategory == null) {
			i18nCategory = new HashMap<String, Category>();
		}
		i18nCategory.put(lang, category);
	}

}
