package com.webjaguar.web.form;

import com.webjaguar.model.Address;
import com.webjaguar.model.Supplier;

public class SupplierForm {
	
	private Supplier supplier;
	private Double price;
	private boolean newSupplier = true;
	private boolean activeStatus = true;
	
	public SupplierForm() {
		this.supplier = new Supplier();
		this.supplier.setAddress( new Address() );
		newSupplier = true;
	}
	
	public SupplierForm(Supplier supplier) {
		this.supplier = supplier;
		newSupplier = false;
	}

	public boolean isNewSupplier() {
		return newSupplier;
	}

	public void setNewSupplier(boolean newSupplier) {
		this.newSupplier = newSupplier;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public void setActiveStatus(boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public boolean isActiveStatus() {
		return activeStatus;
	}
	
}