/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.Address;

public class AddressForm {
	
	private Address address;
	private boolean newAddress;
	private boolean setAsPrimary;
	private boolean companyRequired;
	
	public AddressForm(Address address) {
		this.address = address;
		this.newAddress = false;
	}
	
	public AddressForm() {
		this.address = new Address();
		this.newAddress = true;
	}

	public Address getAddress() {
		return address;
	}

	public boolean isNewAddress() {
		return newAddress;
	}

	public boolean isSetAsPrimary() {
		return setAsPrimary;
	}

	public void setSetAsPrimary(boolean setAsPrimary) {
		this.setAsPrimary = setAsPrimary;
	}

	public boolean isCompanyRequired()
	{
		return companyRequired;
	}

	public void setCompanyRequired(boolean companyRequired)
	{
		this.companyRequired = companyRequired;
	}

}
