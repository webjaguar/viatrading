package com.webjaguar.web.form;

import com.webjaguar.model.CustomerGroup;

public class CustomerGroupForm {
	
    private CustomerGroup customerGroup;
	private boolean newCustomerGroup;
	
	public CustomerGroupForm(CustomerGroup customerGroup) {
		this.customerGroup = customerGroup;
		this.newCustomerGroup = false;
	}
	
	public CustomerGroupForm() {
		this.customerGroup = new CustomerGroup();
		this.newCustomerGroup = true;
	}

	public CustomerGroup getCustomerGroup()
	{
		return customerGroup;
	}

	public void setCustomerGroup(CustomerGroup customerGroup)
	{
		this.customerGroup = customerGroup;
	}

	public boolean isNewCustomerGroup()
	{
		return newCustomerGroup;
	}

	public void setNewCustomerGroup(boolean newCustomerGroup)
	{
		this.newCustomerGroup = newCustomerGroup;
	}

}
