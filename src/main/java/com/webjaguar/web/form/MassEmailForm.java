/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 12.11.2008
 */

package com.webjaguar.web.form;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import com.webjaguar.model.CustomerSearch;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderSearch;

public class MassEmailForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String subject;	
	private String message;
	private boolean html;
	private String from;
	private String to;
	private String cc;
	private Integer numEmails;
	private Integer numEmailsTemp;
	private String type;
	private boolean uniquePerOrder;
	private Boolean cartNotEmpty;
	private Vector<Integer> orderIds = new Vector<Integer>();
	private Set<Integer> userIds = new HashSet<Integer>();
	private Set<Integer> crmContactIds = new HashSet<Integer>();
	private List<Order> orders;
	private OrderSearch orderSearch;
	private CustomerSearch customerSearch;
	private Date scheduleTime;
	private String campaignName;
	private Integer campaignId;
	private Integer messageId;
	
	
	public OrderSearch getOrderSearch()
	{
		return orderSearch;
	}
	public void setOrderSearch(OrderSearch orderSearch)
	{
		this.orderSearch = orderSearch;
	}
	public void setCustomerSearch(CustomerSearch customerSearch) {
		this.customerSearch = customerSearch;
	}
	public CustomerSearch getCustomerSearch() {
		return customerSearch;
	}
	public String getSubject()
	{
		return subject;
	}
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public String getFrom()
	{
		return from;
	}
	public void setFrom(String from)
	{
		this.from = from;
	}
	public String getTo()
	{
		return to;
	}
	public void setTo(String to)
	{
		this.to = to;
	}
	public String getCc()
	{
		return cc;
	}
	public void setCc(String cc)
	{
		this.cc = cc;
	}
	public boolean isHtml()
	{
		return html;
	}
	public void setHtml(boolean html)
	{
		this.html = html;
	}
	public Integer getNumEmails()
	{
		return numEmails;
	}
	public void setNumEmails(Integer numEmails)
	{
		this.numEmails = numEmails;
	}
	public Integer getNumEmailsTemp() {
		return numEmailsTemp;
	}
	public void setNumEmailsTemp(Integer numEmailsTemp) {
		this.numEmailsTemp = numEmailsTemp;
	}
	public Set<Integer> getUserIds()
	{
		return userIds;
	}
	public void setUserIds(Set<Integer> userIds)
	{
		this.userIds = userIds;
	}
	public Set<Integer> getCrmContactIds() {
		return crmContactIds;
	}
	public void setCrmContactIds(Set<Integer> crmContactIds) {
		this.crmContactIds = crmContactIds;
	}
	public List<Order> getOrders()
	{
		return orders;
	}
	public void setOrders(List<Order> orders)
	{
		this.orders = orders;
	}
	public Date getScheduleTime()
	{
		return scheduleTime;
	}
	public void setScheduleTime(Date scheduleTime)
	{
		this.scheduleTime = scheduleTime;
	}
	public String getCampaignName() {
		return campaignName;
	}
	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}
	public Integer getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(Integer campaignId) {
		this.campaignId = campaignId;
	}
	public Vector<Integer> getOrderIds() {
		return orderIds;
	}
	public void setOrderIds(Vector<Integer> orderIds) {
		this.orderIds = orderIds;
	}
	public boolean isUniquePerOrder() {
		return uniquePerOrder;
	}
	public void setUniquePerOrder(boolean uniquePerOrder) {
		this.uniquePerOrder = uniquePerOrder;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Boolean getCartNotEmpty() {
		return cartNotEmpty;
	}
	public void setCartNotEmpty(Boolean cartNotEmpty) {
		this.cartNotEmpty = cartNotEmpty;
	}
	public Integer getMessageId() {
		return messageId;
	}
	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}
	
}
