/* Copyright 2005, 2010 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form.crm;

import com.webjaguar.model.crm.CrmForm;

public class CrmFormBuilderForm {
	
	private CrmForm crmForm;
	private boolean newCrmForm;
	
	public CrmFormBuilderForm() {
		this.crmForm = new CrmForm();
		this.newCrmForm = true;
	}
	public CrmFormBuilderForm(CrmForm crmForm) {
		this.crmForm = crmForm;
		this.newCrmForm = false;
	}
	
	public CrmForm getCrmForm() {
		return crmForm;
	}

	public void setCrmForm(CrmForm crmForm) {
		this.crmForm = crmForm;
	}

	public boolean isNewCrmForm() {
		return newCrmForm;
	}

	public void setNewCrmForm(boolean newCrmForm) {
		this.newCrmForm = newCrmForm;
	}
	
}
