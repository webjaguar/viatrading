/* Copyright 2005, 2010 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form.crm;

import com.webjaguar.model.crm.CrmQualifier;

public class CrmQualifierForm {
	
	private CrmQualifier crmQualifier;
	private boolean newCrmQualifier;
	
	public CrmQualifierForm() {
		this.crmQualifier = new CrmQualifier();
		this.newCrmQualifier = true;
	}
	
	public CrmQualifierForm(CrmQualifier crmQualifier) {
		this.crmQualifier = crmQualifier;
		this.newCrmQualifier = false;
	}
	
	public CrmQualifier getCrmQualifier() {
		return crmQualifier;
	}
	
	public void setCrmQualifier(CrmQualifier crmQualifier) {
		this.crmQualifier = crmQualifier;
	}
	
	public boolean isNewCrmQualifier() {
		return newCrmQualifier;
	}
	
	public void setNewCrmQualifier(boolean newCrmQualifier) {
		this.newCrmQualifier = newCrmQualifier;
	}
	
}
