package com.webjaguar.web.form.crm;

import com.webjaguar.model.crm.CrmContactField;

public class CrmContactFieldForm {

	private CrmContactField crmContactField;
	private boolean newCrmContactField;
	
	public CrmContactFieldForm() {
		this.crmContactField = new CrmContactField();
		this.newCrmContactField = true;
	}
	public CrmContactFieldForm(CrmContactField crmContactField) {
		this.crmContactField = crmContactField;
		this.newCrmContactField = false;
	}
	
	public CrmContactField getCrmContactField() {
		return crmContactField;
	}

	public void setCrmContactField(CrmContactField crmContactField) {
		this.crmContactField = crmContactField;
	}

	public boolean isNewCrmContactField() {
		return newCrmContactField;
	}

	public void setNewCrmContactField(boolean newCrmContactField) {
		this.newCrmContactField = newCrmContactField;
	}
	
}
