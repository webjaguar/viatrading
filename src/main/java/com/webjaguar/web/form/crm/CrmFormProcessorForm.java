/* Copyright 2005, 2010 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form.crm;

import java.util.List;

import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactField;


public class CrmFormProcessorForm {
	
	private CrmContact crmContact;
	private String formNumber;
	private String aQuest;
	private String aAns;
	private String uAns;
	
	public CrmFormProcessorForm() {
		this.crmContact = new CrmContact();
	}
	public CrmFormProcessorForm(CrmContact crmContact, List<CrmContactField> crmContactFields) {
		this.crmContact = crmContact;
	}
	
	public CrmContact getCrmContact() {
		return crmContact;
	}
	public void setCrmContact(CrmContact crmContact) {
		this.crmContact = crmContact;
	}
	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}
	public String getFormNumber() {
		return formNumber;
	}
	public void setaQuest(String aQuest) {
		this.aQuest = aQuest;
	}
	public String getaQuest() {
		return aQuest;
	}
	public void setaAns(String aAns) {
		this.aAns = aAns;
	}
	public String getaAns() {
		return aAns;
	}
	public void setuAns(String uAns) {
		this.uAns = uAns;
	}
	public String getuAns() {
		return uAns;
	}
	
	
}
