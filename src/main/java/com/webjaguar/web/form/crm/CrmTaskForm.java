package com.webjaguar.web.form.crm;

import java.io.Serializable;
import java.util.Calendar;

import com.webjaguar.model.crm.CrmTask;

public class CrmTaskForm implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CrmTask crmTask;
	private boolean newCrmTask;
	
	public CrmTaskForm(CrmTask contact) {
		this.crmTask = contact;
	}
	
	public CrmTaskForm() {
		this.crmTask = new CrmTask();
		this.newCrmTask = true;
		this.getCrmTask().setCreated( Calendar.getInstance().getTime());
	}

	public CrmTask getCrmTask() {
		return crmTask;
	}

	public void setCrmTask(CrmTask crmTask) {
		this.crmTask = crmTask;
	}

	public boolean isNewCrmTask() {
		return newCrmTask;
	}

	public void setNewCrmTask(boolean newCrmTask) {
		this.newCrmTask = newCrmTask;
	}
}
