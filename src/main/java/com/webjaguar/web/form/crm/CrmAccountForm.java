/* Copyright 2005, 2010 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form.crm;

import com.webjaguar.model.crm.CrmAccount;

public class CrmAccountForm {
	
	private CrmAccount crmAccount;
	private boolean newCrmAccount;
	
	public CrmAccountForm() {
		this.crmAccount = new CrmAccount();
		this.newCrmAccount = true;
	}
	
	public CrmAccountForm(CrmAccount crmAccount) {
		this.crmAccount = crmAccount;
		this.newCrmAccount = false;
	}
	
	public CrmAccount getCrmAccount() {
		return crmAccount;
	}
	public void setCrmAccount(CrmAccount crmAccount) {
		this.crmAccount = crmAccount;
	}
	public boolean isNewCrmAccount() {
		return newCrmAccount;
	}
	public void setNewCrmAccount(boolean newCrmAccount) {
		this.newCrmAccount = newCrmAccount;
	}
	
}
