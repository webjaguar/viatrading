/* Copyright 2005, 2010 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form.crm;

import com.webjaguar.model.Customer;
import com.webjaguar.model.crm.CrmContact;

public class CrmContactForm {
	
	private CrmContact crmContact;
	private Customer customer;
	private boolean newCrmContact;
	private boolean customerUpdate;
	private boolean NewCustomer;
	private boolean crmUpdate;
	
	public CrmContactForm() {
		this.crmContact = new CrmContact();
		this.newCrmContact = true;
	}
	public CrmContactForm(CrmContact crmContact) {
		this.crmContact = crmContact;
		this.newCrmContact = false;
	}
	
	public CrmContact getCrmContact() {
		return crmContact;
	}

	public void setCrmContact(CrmContact crmContact) {
		this.crmContact = crmContact;
	}

	public boolean isNewCrmContact() {
		return newCrmContact;
	}

	public void setNewCrmContact(boolean newCrmContact) {
		this.newCrmContact = newCrmContact;
	}
	
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public boolean isCustomerUpdate() {
		return customerUpdate;
	}
	public void setCustomerUpdate(boolean customerUpdate) {
		this.customerUpdate = customerUpdate;
	}
	public boolean isNewCustomer() {
		return NewCustomer;
	}
	public void setNewCustomer(boolean newCustomer) {
		NewCustomer = newCustomer;
	}
	public boolean isCrmUpdate() {
		return crmUpdate;
	}
	public void setCrmUpdate(boolean crmUpdate) {
		this.crmUpdate = crmUpdate;
	}
	

}
