package com.webjaguar.web.form.crm;

import com.webjaguar.model.crm.CrmContactGroup;

public class CrmContactGroupForm {
	
    private CrmContactGroup crmContactGroup;
	private boolean newCrmContactGroup;
	
	public CrmContactGroupForm(CrmContactGroup crmContactGroup) {
		this.crmContactGroup = crmContactGroup;
		this.newCrmContactGroup = false;
	}
	
	public CrmContactGroupForm() {
		this.crmContactGroup = new CrmContactGroup();
		this.newCrmContactGroup = true;
	}

	public CrmContactGroup getCrmContactGroup() {
		return crmContactGroup;
	}

	public void setCrmContactGroup(CrmContactGroup crmContactGroup) {
		this.crmContactGroup = crmContactGroup;
	}

	public boolean isNewCrmContactGroup() {
		return newCrmContactGroup;
	}

	public void setNewCrmContactGroup(boolean newCrmContactGroup) {
		this.newCrmContactGroup = newCrmContactGroup;
	}
}