/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.CommissionReport;

public class CommissionForm {
	
	private CommissionReport commissionReport;

	//helper
	private Integer level;
	private Integer userId;
	private Integer orderId;

	public CommissionReport getCommissionReport()
	{
		return commissionReport;
	}
	public void setCommissionReport(CommissionReport commissionReport)
	{
		this.commissionReport = commissionReport;
	}
	public Integer getLevel()
	{
		return level;
	}
	public void setLevel(Integer level)
	{
		this.level = level;
	}
	public Integer getOrderId()
	{
		return orderId;
	}
	public void setOrderId(Integer orderId)
	{
		this.orderId = orderId;
	}
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

}
