package com.webjaguar.web.form;

import com.webjaguar.model.SiteMessage;

public class SiteMessageForm
{
	private SiteMessage siteMessage;
	private boolean newSiteMessage;
	
	public SiteMessageForm()
	{
		super();
		this.siteMessage = new SiteMessage();
		this.newSiteMessage = true;
	}
	public boolean isNewSiteMessage()
	{
		return newSiteMessage;
	}
	public void setNewSiteMessage(boolean newSiteMessage)
	{
		this.newSiteMessage = newSiteMessage;
	}
	public SiteMessage getSiteMessage()
	{
		return siteMessage;
	}
	public void setSiteMessage(SiteMessage siteMessage)
	{
		this.siteMessage = siteMessage;
	}
	
	
}
