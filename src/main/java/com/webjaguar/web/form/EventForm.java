package com.webjaguar.web.form;

import com.webjaguar.model.Customer;
import com.webjaguar.model.Event;

public class EventForm {

	private Event event;
	private boolean newEvent;
	
	private String cardId; 
	private String language = "en";
	private boolean registerEvent;
	private Customer customer;
	
	private String eventId; 
	public EventForm(Event event) {
		this.event = event;
		this.newEvent = false;
	}
	
	public EventForm() {
		this.event = new Event();
		this.newEvent = true;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public boolean isNewEvent() {
		return newEvent;
	}

	public void setNewEvent(boolean newEvent) {
		this.newEvent = newEvent;
	}

	public String getLanguage() {
		return language;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public boolean isRegisterEvent() {
		return registerEvent;
	}

	public void setRegisterEvent(boolean registerEvent) {
		this.registerEvent = registerEvent;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
}
