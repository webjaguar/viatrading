/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.29.2006
 */

package com.webjaguar.web.form;

import com.webjaguar.model.Option;

public class ProductOptionsForm {
	
	private Option option;
	private boolean newOption;
		
	public ProductOptionsForm() {
		this.option = new Option();
		this.newOption = true;
	}

	public ProductOptionsForm(Option option) {
		this.option = option;
		this.newOption = false;
	}
	
	public Option getOption()
	{
		return option;
	}

	public void setOption(Option option)
	{
		this.option = option;
	}

	public boolean isNewOption()
	{
		return newOption;
	}

	public void setNewOption(boolean newOption)
	{
		this.newOption = newOption;
	}
}
