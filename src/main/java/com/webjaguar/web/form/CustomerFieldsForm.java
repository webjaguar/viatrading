/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.12.2007
 */

package com.webjaguar.web.form;

import java.util.*;

import com.webjaguar.model.CustomerField;

public class CustomerFieldsForm {
	
	List<CustomerField> customerFields;

	public List<CustomerField> getCustomerFields()
	{
		return customerFields;
	}

	public void setCustomerFields(List<CustomerField> customerFields)
	{
		this.customerFields = customerFields;
	}

}
