package com.webjaguar.web.form;

import java.util.Calendar;

import com.webjaguar.model.MailInRebate;

public class MailInRebateForm {

	private MailInRebate mailInRebate;
	private boolean newMailInRebate = true;

	public MailInRebateForm()
	{
		this.mailInRebate = new com.webjaguar.model.MailInRebate();
		Calendar now = Calendar.getInstance();
		now.set( Calendar.AM_PM, 0 );
		now.set( Calendar.HOUR, 6 );
		now.set( Calendar.MINUTE, 0 );
		this.getMailInRebate().setStartDate( now.getTime() );

		now.set( Calendar.HOUR, 23 );
		this.getMailInRebate().setEndDate( now.getTime() );
	}

	public MailInRebateForm( MailInRebate mailInRebate )
	{
		super();
		this.mailInRebate = mailInRebate;
	}

	public boolean isNewMailInRebate()
	{
		return newMailInRebate;
	}

	public void setNewMailInRebate(boolean newMailInRebate)
	{
		this.newMailInRebate = newMailInRebate;
	}

	public MailInRebate getMailInRebate()
	{
		return mailInRebate;
	}

	public void setMailInRebate(MailInRebate mailInRebate)
	{
		this.mailInRebate = mailInRebate;

	}
}