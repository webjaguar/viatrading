/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import java.util.Date;

import com.webjaguar.model.Address;
import com.webjaguar.model.CreditCard;

public class BuyCreditForm {

	private Double amount;
	private Integer point;
	private CreditCard creditCard = new CreditCard();
	private Address billing = new Address();
	private String user;
	private String note;
	private Date dateCreated;
	private boolean aemUser;
	
	public boolean isAemUser()
	{
		return aemUser;
	}
	public void setAemUser(boolean aemUser)
	{
		this.aemUser = aemUser;
	}
	public Double getAmount()
	{
		return amount;
	}
	public void setAmount(Double amount)
	{
		this.amount = amount;
	}
	public CreditCard getCreditCard()
	{
		return creditCard;
	}
	public void setCreditCard(CreditCard creditCard)
	{
		this.creditCard = creditCard;
	}
	public Address getBilling()
	{
		return billing;
	}
	public void setBilling(Address billing)
	{
		this.billing = billing;
	}
	public String getUser()
	{
		return user;
	}
	public void setUser(String user)
	{
		this.user = user;
	}
	public Date getDateCreated()
	{
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated)
	{
		this.dateCreated = dateCreated;
	}
	public Integer getPoint()
	{
		return point;
	}
	public void setPoint(Integer point)
	{
		this.point = point;
	}
	public String getNote()
	{
		return note;
	}
	public void setNote(String note)
	{
		this.note = note;
	}
}
