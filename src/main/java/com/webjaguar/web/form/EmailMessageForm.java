/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.28.2007
 */

package com.webjaguar.web.form;

import com.webjaguar.model.Order;

public class EmailMessageForm {
	
	private Integer recipientId;
	private String subject;	
	private String message;
	private boolean html;
	private String from;
	private String to;
	private String cc;
	private String bcc;
	private Order order;
	private Integer supplierId;
	private String username;
	
	public Integer getRecipientId() {
		return recipientId;
	}
	public void setRecipientId(Integer recipientId) {
		this.recipientId = recipientId;
	}
	public String getSubject()
	{
		return subject;
	}
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public String getFrom()
	{
		return from;
	}
	public void setFrom(String from)
	{
		this.from = from;
	}
	public String getTo()
	{
		return to;
	}
	public void setTo(String to)
	{
		this.to = to;
	}
	public String getCc()
	{
		return cc;
	}
	public void setCc(String cc)
	{
		this.cc = cc;
	}
	public String getBcc()
	{
		return bcc;
	}
	public void setBcc(String bcc)
	{
		this.bcc = bcc;
	}
	public Order getOrder()
	{
		return order;
	}
	public void setOrder(Order order)
	{
		this.order = order;
	}
	public Integer getSupplierId()
	{
		return supplierId;
	}
	public void setSupplierId(Integer supplierId)
	{
		this.supplierId = supplierId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public boolean isHtml()
	{
		return html;
	}
	public void setHtml(boolean html)
	{
		this.html = html;
	}
	
}
