/* Copyright 2005, 2011 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.WebjaguarDataFeed;

public class WebjaguarDataFeedForm {
	
	private WebjaguarDataFeed webjaguarDataFeed;
	private boolean newWebjaguarDataFeed;
	
	public WebjaguarDataFeedForm() {
		this.webjaguarDataFeed = new WebjaguarDataFeed();
		this.newWebjaguarDataFeed = true;
	}
	public WebjaguarDataFeedForm(WebjaguarDataFeed webjaguarDataFeed) {
		this.webjaguarDataFeed = webjaguarDataFeed;
		this.newWebjaguarDataFeed = false;
	}
	
	public WebjaguarDataFeed getWebjaguarDataFeed() {
		return webjaguarDataFeed;
	}

	public void setWebjaguarDataFeed(WebjaguarDataFeed webjaguarDataFeed) {
		this.webjaguarDataFeed = webjaguarDataFeed;
	}

	public boolean isNewWebjaguarDataFeed() {
		return newWebjaguarDataFeed;
	}

	public void setNewDataFeedConfig(boolean newWebjaguarDataFeed) {
		this.newWebjaguarDataFeed = newWebjaguarDataFeed;
	}
	
}
