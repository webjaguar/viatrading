/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import java.util.*;

import com.webjaguar.model.ProductField;

public class ProductFieldsForm {
	
	List<ProductField> productFields;

	public List<ProductField> getProductFields() { return productFields; }
	public void setProductFields(List<ProductField> productFields) {
		this.productFields = productFields;
	}

}
