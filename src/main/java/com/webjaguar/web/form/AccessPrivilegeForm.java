/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import com.webjaguar.model.AccessPrivilege;

public class AccessPrivilegeForm {
	
	private AccessPrivilege privilege;
	private boolean newPrivilege;
	private String confirmPassword;
	private String currentPassword;
	private String existingUsername;


	public String getExistingUsername()
	{
		return existingUsername;
	}

	public void setExistingUsername(String existingUsername)
	{
		this.existingUsername = existingUsername;
	}

	public String getConfirmPassword()
	{
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword)
	{
		this.confirmPassword = confirmPassword;
	}

	public AccessPrivilegeForm(AccessPrivilege privilege) {
		this.privilege = privilege;
		this.newPrivilege = false;
	}
	
	public AccessPrivilegeForm() {
		this.privilege = new AccessPrivilege();
		this.newPrivilege = true;
	}

	public boolean isNewPrivilege()
	{
		return newPrivilege;
	}

	public void setNewPrivilege(boolean newPrivilege)
	{
		this.newPrivilege = newPrivilege;
	}

	public AccessPrivilege getPrivilege()
	{
		return privilege;
	}

	public void setPrivilege(AccessPrivilege privilege)
	{
		this.privilege = privilege;
	}

	public String getCurrentPassword()
	{
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword)
	{
		this.currentPassword = currentPassword;
	}

	
}
