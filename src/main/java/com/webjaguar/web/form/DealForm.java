package com.webjaguar.web.form;

import java.util.Calendar;

import com.webjaguar.model.Deal;

public class DealForm {

	private Deal deal;
	private boolean newDeal = true;

	public DealForm()
	{
		this.deal = new com.webjaguar.model.Deal();
		Calendar now = Calendar.getInstance();
		now.set( Calendar.AM_PM, 0 );
		now.set( Calendar.HOUR, 6 );
		now.set( Calendar.MINUTE, 0 );
		this.getDeal().setStartDate( now.getTime() );

		now.set( Calendar.HOUR, 23 );
		this.getDeal().setEndDate( now.getTime() );
	}

	public DealForm( Deal deal )
	{
		super();
		this.deal = deal;
	}

	public boolean isNewDeal()
	{
		return newDeal;
	}

	public void setNewDeal(boolean newDeal)
	{
		this.newDeal = newDeal;
	}

	public Deal getDeal()
	{
		return deal;
	}

	public void setDeal(Deal deal)
	{
		this.deal = deal;

	}

}
