package com.webjaguar.web.form;

import java.util.Calendar;

import com.webjaguar.model.Deal;
import com.webjaguar.model.ViaDeal;

public class ViaDealForm {

	private ViaDeal viaDeal;
	private boolean newDeal = true;

	public ViaDealForm()
	{
		System.out.println("testing viaDealForm constructor");
		this.viaDeal = new com.webjaguar.model.ViaDeal();
		Calendar now = Calendar.getInstance();
		now.set( Calendar.AM_PM, 0 );
		now.set( Calendar.HOUR, 6 );
		now.set( Calendar.MINUTE, 0 );
		this.getViaDeal().setStartDate( now.getTime() );

		now.set( Calendar.HOUR, 23 );
		this.getViaDeal().setEndDate( now.getTime() );
	}

	public ViaDealForm( ViaDeal viaDeal )
	{
		super();
		this.viaDeal = viaDeal;
	}

	public boolean isNewDeal()
	{
		return newDeal;
	}

	public void setNewDeal(boolean newDeal)
	{
		this.newDeal = newDeal;
	}

	public ViaDeal getViaDeal()
	{
		return viaDeal;
	}

	public void setViaDeal(ViaDeal viaDeal)
	{
		this.viaDeal = viaDeal;

	}

}
