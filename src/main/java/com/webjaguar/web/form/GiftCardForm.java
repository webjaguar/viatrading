/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.web.form;

import java.util.List;

import com.webjaguar.model.GiftCard;
import com.webjaguar.model.GiftCardOrder;
import com.webjaguar.model.GiftCardStatus;

public class GiftCardForm {

	private GiftCardOrder giftCardOrder;
	private GiftCard giftCard;
	private String confirmEmail;
	private String errorMessage;
	private GiftCardStatus statusHistory;
	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}

	public GiftCardForm()
	{
		super();
		this.giftCard = new GiftCard();
		this.giftCardOrder = new GiftCardOrder();
	}
	
	public GiftCardForm( GiftCard giftCard )
	{
		super();
		this.giftCard = giftCard;
	}

	public GiftCard getGiftCard()
	{
		return giftCard;
	}

	public void setGiftCard(GiftCard giftCard)
	{
		this.giftCard = giftCard;
	}

	public String getConfirmEmail()
	{
		return confirmEmail;
	}

	public void setConfirmEmail(String confirmEmail)
	{
		this.confirmEmail = confirmEmail;
	}

	public GiftCardOrder getGiftCardOrder()
	{
		return giftCardOrder;
	}

	public void setGiftCardOrder(GiftCardOrder giftCardOrder)
	{
		this.giftCardOrder = giftCardOrder;
	}

	public GiftCardStatus getStatusHistory() {
		return statusHistory;
	}

	public void setStatusHistory(GiftCardStatus statusHistory) {
		this.statusHistory = statusHistory;
	}

	
	
}
