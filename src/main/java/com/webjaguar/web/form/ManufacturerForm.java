package com.webjaguar.web.form;
import com.webjaguar.model.Manufacturer;

public class ManufacturerForm {
	
	private Manufacturer manufacturer;
	private boolean newManufacturer;

	
	public ManufacturerForm(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
		this.newManufacturer = false;
	}
	
	public ManufacturerForm() {
		this.manufacturer= new Manufacturer();
		this.newManufacturer = true;
	}
	

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Manufacturer getManufacturer() {
		return manufacturer;
	}

	public void setNewManufacturer(boolean newManufacturer) {
		this.newManufacturer = newManufacturer;
	}

	public boolean isNewManufacturer() {
		return newManufacturer;
	}
}