package com.webjaguar.shipping;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.Product;
import com.webjaguar.model.ShippingMethod;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.model.Supplier;
import com.webjaguar.model.Weather;
import com.webjaguar.thirdparty.shipping.dhl.DhlRateQuote;
import com.webjaguar.thirdparty.shipping.fedex.rating.Address;
import com.webjaguar.thirdparty.shipping.fedex.rating.FedexRateQuote;
import com.webjaguar.thirdparty.shipping.ups.UpsRateQuote;
import com.webjaguar.thirdparty.shipping.ups.UpsTransitQuote;
import com.webjaguar.thirdparty.shipping.usps.UspsIntRateQuote;
import com.webjaguar.thirdparty.shipping.usps.UspsRateQuote;
import com.webjaguar.thirdparty.weatherchannel.WeatherChannelApi;

public class ShippingRateQuotePerSupplier
{
	//public List<ShippingRate> getShippingRates(HttpServletRequest request, Order order, WebJaguarFacade webJaguar, double handlingTotal, List<LineItem> lineItems, Address originAddress, String sourceStreetAddr1, String sourceCity, String sourceState, String sourceZipCode, String sourceCountry)
	public List<ShippingRate> getShippingRates(HttpServletRequest request, Order order, WebJaguarFacade webJaguar, double handlingTotal)
	{
		String upsGroundTransitDay = null;
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		
		
		
		
		Map<Integer, List<LineItem>> supplierLineItemsMap = new HashMap<Integer, List<LineItem>>();
		for(LineItem lineItem : order.getLineItems()) {
			if(lineItem.getProduct().getDefaultSupplierId() != 0) {
				if(supplierLineItemsMap.get(lineItem.getProduct().getDefaultSupplierId()) != null){
					supplierLineItemsMap.get(lineItem.getProduct().getDefaultSupplierId()).add(lineItem);
				} else {
					List<LineItem> lineItems = new ArrayList<LineItem>();
					lineItems.add(lineItem);
					supplierLineItemsMap.put(lineItem.getProduct().getDefaultSupplierId(), lineItems);
				}
			} else {
				if(supplierLineItemsMap.get(-1) != null){
					supplierLineItemsMap.get(-1).add(lineItem);
				} else {
					List<LineItem> lineItems = new ArrayList<LineItem>();
					lineItems.add(lineItem);
					supplierLineItemsMap.put(-1, lineItems);
				}
			}
		}
		
		
		
		//System.out.println("Map "+supplierLineItemsMap);
		com.webjaguar.model.Address sourceAddress = null;
		
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		boolean addressType = webJaguar.getAddressType(order.getShipping());
		
		// list to be returned
		List<ShippingRate> ratesList = new ArrayList<ShippingRate>();
		
		Map<String, Integer> rateCodeFrequencyMap = new HashMap<String, Integer>();
		Map<String, Double> rateCodeTotalMap = new HashMap<String, Double>();
		Map<String, ShippingRate> rateCodeShippingMap = new HashMap<String, ShippingRate>();
		for(Integer supplierId : supplierLineItemsMap.keySet()){
			if(supplierId != -1){
				Supplier supplier = webJaguar.getSupplierById(supplierId);
				sourceAddress = supplier.getAddress();
			} else  if(order.getSource() != null) {
				sourceAddress = order.getSource();
			} else {
				sourceAddress = new com.webjaguar.model.Address();
				sourceAddress.setAddr1(siteConfig.get( "SOURCE_ADDRESS" ).getValue());
				sourceAddress.setCity(siteConfig.get( "SOURCE_CITY" ).getValue());
				sourceAddress.setStateProvince(siteConfig.get( "SOURCE_STATEPROVINCE" ).getValue());
				sourceAddress.setZip(siteConfig.get( "SOURCE_ZIP" ).getValue());
				sourceAddress.setCountry(siteConfig.get( "SOURCE_COUNTRY" ).getValue());
			}
			
			
			// Get the weight of lineItems
			Double totalWeight = 0.0;
			for (LineItem lineItem : supplierLineItemsMap.get(supplierId)) {
				Product product = webJaguar.getProductById(lineItem.getProductId(), 0, false, null);
				try {
					m = c.getMethod("getField" + siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue());
					if (!((String) m.invoke(product, arglist)).equals("0")) {
						totalWeight = totalWeight + (lineItem.getProduct().getWeight() * lineItem.getQuantity());
					}
				} catch (Exception e) {
					// do nothing
				}
			}
			if(totalWeight <= 0.0) {
				totalWeight = 1.0;
			}
			
			
			List<ShippingRate> tempList = null;
			// Get FedEx Rates
			if ( webJaguar.isActiveCarrier( "fedex" ) ) {
				tempList = this.getFedExRates(sourceAddress, webJaguar, order, siteConfig, totalWeight, addressType);
				if(tempList != null){
					for(ShippingRate sRate : tempList){
						rateCodeTotalMap.put(sRate.getCode(), Double.parseDouble(sRate.getPrice()) + (rateCodeTotalMap.get(sRate.getCode()) != null ? rateCodeTotalMap.get(sRate.getCode()) : 0.0));
						rateCodeFrequencyMap.put(sRate.getCode(), 1 + (rateCodeFrequencyMap.get(sRate.getCode()) != null ? rateCodeFrequencyMap.get(sRate.getCode()) : 0));
						rateCodeShippingMap.put(sRate.getCode(), sRate);
					}
				}
			}
			// Get USPS Rates
			if ( webJaguar.isActiveCarrier( "usps" ) && siteConfig.get( "USPS_USERID" ).getValue().length() > 0 ) {
				tempList = this.getUspsRates(sourceAddress, webJaguar, order, supplierLineItemsMap.get(supplierId), siteConfig, totalWeight, addressType);
				if(tempList != null){
					for(ShippingRate sRate : tempList){
						rateCodeTotalMap.put(sRate.getCode(), Double.parseDouble(sRate.getPrice()) + (rateCodeTotalMap.get(sRate.getCode()) != null ? rateCodeTotalMap.get(sRate.getCode()) : 0.0));
						rateCodeFrequencyMap.put(sRate.getCode(), 1 + (rateCodeFrequencyMap.get(sRate.getCode()) != null ? rateCodeFrequencyMap.get(sRate.getCode()) : 0));
						rateCodeShippingMap.put(sRate.getCode(), sRate);
					}
				}
				//System.out.println("USPS Temp List "+tempList);
			}
			
			// Get DHL Rates
			if ( webJaguar.isActiveCarrier( "dhl" ) ) {
				tempList = this.getDHLRates(order, totalWeight);
				if(tempList != null){
					for(ShippingRate sRate : tempList){
						rateCodeTotalMap.put(sRate.getCode(), Double.parseDouble(sRate.getPrice()) + (rateCodeTotalMap.get(sRate.getCode()) != null ? rateCodeTotalMap.get(sRate.getCode()) : 0.0));
						rateCodeFrequencyMap.put(sRate.getCode(), 1 + (rateCodeFrequencyMap.get(sRate.getCode()) != null ? rateCodeFrequencyMap.get(sRate.getCode()) : 0));
						rateCodeShippingMap.put(sRate.getCode(), sRate);
					}
				}
			}
			
			// Get UPS Rates
			if ( webJaguar.isActiveCarrier( "ups" ) ) {
				tempList = this.getUPSRates(sourceAddress, webJaguar, order, supplierLineItemsMap.get(supplierId), siteConfig, totalWeight, addressType);
				if(tempList != null){
					for(ShippingRate sRate : tempList){
						rateCodeTotalMap.put(sRate.getCode(), Double.parseDouble(sRate.getPrice()) + (rateCodeTotalMap.get(sRate.getCode()) != null ? rateCodeTotalMap.get(sRate.getCode()) : 0.0));
						rateCodeFrequencyMap.put(sRate.getCode(), 1 + (rateCodeFrequencyMap.get(sRate.getCode()) != null ? rateCodeFrequencyMap.get(sRate.getCode()) : 0));
						rateCodeShippingMap.put(sRate.getCode(), sRate);
					}
				}
				
				try {
					UpsTransitQuote upsTransitQuote = new UpsTransitQuote();
					upsGroundTransitDay = upsTransitQuote.getTransit( sourceAddress.getZip(), sourceAddress.getCountry(), siteConfig.get( "UPS_USERID" ).getValue(), siteConfig.get( "UPS_PASSWORD" ).getValue(), siteConfig.get( "UPS_ACCESS_LICENSE_NUMBER" ).getValue(), order.getShipping().getZip(), order.getShipping().getCountry(), totalWeight, addressType);
				} catch ( Exception e ) {
					e.printStackTrace();
				}
			}
		}
		//System.out.println("rateCodeTotalMap "+rateCodeTotalMap);
		//System.out.println("rateCodeFrequencyMap "+rateCodeFrequencyMap);
		for(String shippingCode : rateCodeFrequencyMap.keySet()) {
			if(rateCodeFrequencyMap.get(shippingCode) < supplierLineItemsMap.size()) {
				rateCodeTotalMap.remove(shippingCode);
			}
		}
		//System.out.println("rateCodeTotalMap "+rateCodeTotalMap);
		for(String rateCode : rateCodeTotalMap.keySet()) {
			ShippingRate sRate = rateCodeShippingMap.get(rateCode);
			sRate.setPrice(rateCodeTotalMap.get(rateCode).toString());
			ratesList.add(sRate);
		}
		//System.out.println("ratesList "+ratesList);
		
		
		Weather temp = null;
		if ( (Boolean) gSiteConfig.get( "gWEATHER_CHANNEL" ) ) {
			WeatherChannelApi wc = new WeatherChannelApi();
			// get one day forecast
			temp = wc.ModelGetTemp( order.getShipping().getZip(), "2" );
			order.setWeather( temp );
		}

		List<ShippingRate> finalList = new ArrayList<ShippingRate>();
		// set shipping title for each shipping rate object, remove DISABLED shipping rate also
		for ( Iterator it = ratesList.iterator(); it.hasNext(); ) {
			Date shippingTime = setupDueDate(Integer.parseInt( siteConfig.get( "SHIPPING_TURNOVER_TIME" ).getValue()), Calendar.getInstance(), true);
			Calendar shipTimeCalendar = new GregorianCalendar();
			shipTimeCalendar.setTime( shippingTime );
			ShippingRate shippingRate = (ShippingRate) it.next();
			ShippingMethod shippingMethod = webJaguar.getShippingMethodByCarrierCode( shippingRate );
			if ( shippingMethod != null && shippingMethod.getShippingActive() ) {
				shippingRate.setTitle( shippingMethod.getShippingTitle() );
				shippingRate.setRank( shippingMethod.getShippingRank() );
				shippingRate.setMinPrice(shippingMethod.getMinPrice());
				shippingRate.setMaxPrice(shippingMethod.getMaxPrice());
				shippingRate.setId(shippingMethod.getId());
				try {
					if (shippingRate.getCarrier().equals( "ups" ) && !shippingRate.getCode().equals( "03" )) { // UPS Ground code is 03
						shippingRate.setDeliveryDate( setupDueDate(Integer.parseInt( shippingRate.getGuaranteedDaysToDelivery() ), shipTimeCalendar, false));
						shippingRate.setShippingPeriod( shippingRate.getGuaranteedDaysToDelivery() );
					} else if ( shippingRate.getCarrier().equals( "ups" ) && shippingRate.getCode().equals( "03" )) {
						shippingRate.setDeliveryDate( setupDueDate(Integer.parseInt( upsGroundTransitDay ), shipTimeCalendar, false));
						shippingRate.setShippingPeriod( upsGroundTransitDay );
					}
				} catch ( Exception e) {}
				shippingRate.setPrice( roundShippingRate (Double.parseDouble(shippingRate.getPrice()) + handlingTotal).toString() );
				if ( temp == null || !order.isTemperatureEnable() || sourceAddress.getStateProvince().equals( order.getShipping().getStateProvince()) ) {
					finalList.add( shippingRate );
				}
				else {
					// if I get N/A I assume the high temperature 
					if ( temp.getHigh() == null || temp.getHigh().equals( "N/A" ) || Integer.parseInt( temp.getHigh() ) > order.getLowestTemp() ) {
						if ( shippingMethod.getWeatherTemp() < order.getLowestTemp() )
							finalList.add( shippingRate );
					} else
						finalList.add( shippingRate );
				}	
			}
		}
		
		// sort the list by rank
		Collections.sort( finalList, RANK_ORDER );

		// return the rates list
		return finalList;
	}

	private Double roundShippingRate(Double shippingRateDouble)	{
		int decimalPlace = 2;
		BigDecimal bd = new BigDecimal(shippingRateDouble);
	    bd = bd.setScale(decimalPlace,BigDecimal.ROUND_HALF_UP);
	    shippingRateDouble = bd.doubleValue();
	    return shippingRateDouble;
	}

	private Comparator<ShippingRate> RANK_ORDER = new Comparator<ShippingRate>() {
		public int compare(ShippingRate s1, ShippingRate s2)
		{
			return s1.getRank().compareTo( s2.getRank() );
		}
	};
	
	private Date setupDueDate(int turnOverTime, Calendar startTime, boolean AMPM) {
		Calendar now = new GregorianCalendar();
		now.setTime( startTime.getTime() );
		if ( AMPM && now.get( Calendar.AM_PM ) == Calendar.PM ) {
			now.add( Calendar.DAY_OF_MONTH, 1 );
		}
		if ( now.get( GregorianCalendar.DAY_OF_WEEK ) == 1 ) {
			now.add( Calendar.DAY_OF_MONTH, 1 );
		}
		else if ( now.get( GregorianCalendar.DAY_OF_WEEK ) == 7 ) {
			now.add( Calendar.DAY_OF_MONTH, 2 );
		}
		for (int i=0; i < turnOverTime ; i++) {
			now.add( Calendar.DAY_OF_MONTH, 1 );
			if ( now.get( GregorianCalendar.DAY_OF_WEEK ) == 1 ) {
				now.add( Calendar.DAY_OF_MONTH, 1 );
			}
			else if ( now.get( GregorianCalendar.DAY_OF_WEEK ) == 7 ) {
				now.add( Calendar.DAY_OF_MONTH, 2 );
			}
		}
		return now.getTime();
	}
	
	private String getFiveDigitZipcode(String zipcode) {
		String FiveDigitZipcode;
		if (zipcode == null) {
			FiveDigitZipcode = "";
		} else if ( zipcode.trim().length() > 5) {
			FiveDigitZipcode = zipcode.trim().substring( 0, 5 ).trim();
		} else {
			FiveDigitZipcode = zipcode.trim();
		}
		return FiveDigitZipcode;
	}
	
	private List<ShippingRate> getFedExRates(com.webjaguar.model.Address sourceAddress, WebJaguarFacade webJaguar, Order order, Map<String, Configuration> siteConfig, Double totalWeight, boolean addressType){
		try {
			FedexRateQuote fedexRateQuote = new FedexRateQuote();
			Address originAddress = new Address(new String[] { sourceAddress.getAddr1() }, sourceAddress.getCity(), sourceAddress.getStateProvince(), sourceAddress.getZip(), "", sourceAddress.getCountry(), true);
			Address destinAddress = new Address(new String[] { order.getShipping().getAddr1() }, order.getShipping().getCity(), (order.getShipping().getStateProvince() == null || order.getShipping().getStateProvince().length() < 2) ? order.getShipping().getStateProvince() : order.getShipping().getStateProvince().substring( 0, 2 ), order.getShipping().getZip(), "", order.getShipping().getCountry(), addressType);
			return fedexRateQuote.getRates( originAddress, destinAddress, totalWeight.toString(),
					siteConfig.get( "FEDEX_USER_KEY" ).getValue(), siteConfig.get( "FEDEX_USER_PASSWORD" ).getValue(), siteConfig.get( "FEDEX_USER_METERNUMBER" ).getValue(),  siteConfig.get( "FEDEX_ACCOUNT_NUMBER" ).getValue(),
					siteConfig.get( "FEDEX_SIGNATURE_TYPE" ).getValue(), siteConfig.get( "FEDEX_PACKAGING_TYPE" ).getValue(), siteConfig.get( "FEDEX_DROP_OFF_TYPE" ).getValue(),
					siteConfig.get( "PACKAGING_DIMENSION_LENGTH" ).getValue(), siteConfig.get( "PACKAGING_DIMENSION_WIDTH" ).getValue(), siteConfig.get( "PACKAGING_DIMENSION_HEIGHT" ).getValue(), siteConfig.get( "PACKAGING_DIMENSION_UNIT" ).getValue(), Boolean.getBoolean( siteConfig.get( "FEDEX_SATURDAY_DELIVERY" ).getValue()) );
		} catch ( Exception e ) {
			e.printStackTrace();
			return null;
		}
	} 

	private List<ShippingRate> getUspsRates(com.webjaguar.model.Address sourceAddress, WebJaguarFacade webJaguar, Order order, List<LineItem> lineItems, Map<String, Configuration> siteConfig, Double totalWeight, boolean addressType){
			
		double unknownPackageWeight = 0;	
		boolean hasUnknownPackage = false;
		int packageNumber = 0;
		Map<Integer, Double> independentPackages = new HashMap<Integer, Double>();
		
		for (LineItem lineItem: lineItems) {
			if (lineItem.getProduct().getWeight() == null || lineItem.getProduct().getWeight() <= 0) {
				hasUnknownPackage = true;
			} else if (lineItem.getCustomShippingCost() != null) {
				// ignore custom shipping items
			} else if (lineItem.getProduct().getUspsMaxItemsInPackage() != null){	
				int seperatePackage = (new Double(Math.floor(lineItem.getQuantity() * lineItem.getCaseContentQty() * lineItem.getCasePackQty() / lineItem.getProduct().getUspsMaxItemsInPackage()))).intValue();
				
				// packages with max quantity
				for(int i=0; i< seperatePackage ; i++) {
					independentPackages.put(packageNumber++, lineItem.getProduct().getUspsMaxItemsInPackage() * lineItem.getProduct().getWeight());
			    }
			    double remainingWeight = ((lineItem.getQuantity() * lineItem.getCaseContentQty() * lineItem.getCasePackQty()) - (seperatePackage * lineItem.getProduct().getUspsMaxItemsInPackage())) * lineItem.getProduct().getWeight();
				if(remainingWeight > 0) {
			    	independentPackages.put(packageNumber++, remainingWeight);
			    }
			} else {	
				hasUnknownPackage = true;
				unknownPackageWeight += lineItem.getPackageWeight() * lineItem.getQuantity();
			}
		}
		
		if ( order.getShipping().getCountry().equalsIgnoreCase( "US" )) {
			try {
				UspsRateQuote uspsRateQuote = new UspsRateQuote();
				//System.out.println("Returning Rates from USPS");
				return uspsRateQuote.getRates( siteConfig.get( "USPS_USERID" ).getValue(), "All", sourceAddress.getZip(), getFiveDigitZipcode(order.getShipping().getZip()), hasUnknownPackage, unknownPackageWeight, independentPackages,"REGULAR" );
			} catch ( Exception e )	{
				e.printStackTrace();
				return null;
			}
		} else {
			try {
				UspsIntRateQuote uspsIntRateQuote = new UspsIntRateQuote();
				String countryName = webJaguar.getCountryByCode( order.getShipping().getCountry() ).getName();
				if (countryName.equals( "United Kingdom" )) {
					// USPS does not accept 'United Kingdom'
					countryName = "Great Britain";
				} else if (countryName.equals("Russian Federation")) {
					// USPS does not accept 'Russian Federation'
					countryName = "Russia";						
				} else if(countryName.equals("Slovakia (Slovak Republic)")) {
					// USPS does not accept 'Slovakia (Slovak Republic)'
					countryName="Slovakia";
				} else if (countryName.equals("Bosnia and Herzegowina")) {
					countryName="Bosnia-Herzegovina";
				} else if (countryName.equals("Falkland Islands (Malvinas)")) {
					countryName="Falkland Islands";
				} else if (countryName.equals("South Georgia and the South Sandwich Islands")) {
					countryName="South Georgia";
				} else if (countryName.equals("Iran (Islamic Republic of)")) {
					countryName="Iran";
				} else if (countryName.equals("Korea, Democratic People\'s Republic of")) {
					countryName="North Korea";
				} else if (countryName.equals("Korea, Republic of")) {
					countryName="South Korea";
				} else if (countryName.equals("Lao People\'s Democratic Republic")) {
					countryName="Laos";//not sure
				} else if (countryName.equals("Macedonia, The Former Yugoslav Republic of")) {
					countryName="Macedonia, Republic of";
				} else if (countryName.equals("Tanzania, United Republic of")) {
					countryName="Tanzania";
				} else if (countryName.equals("Vatican City State (Holy See)")) {
					countryName="Vatican City";
				} else if (countryName.equals("Viet Nam")) {
					countryName="Vietnam";
				}					
				return uspsIntRateQuote.getRates( siteConfig.get( "USPS_USERID" ).getValue(), countryName, hasUnknownPackage, unknownPackageWeight, independentPackages, null );
			} catch ( Exception e ) {
				e.printStackTrace();
				return null;
			}
		}
	} 

	@SuppressWarnings("unchecked")
	private List<ShippingRate> getDHLRates(Order order,Double totalWeight){
		// get DHL rate quote
		try {
			DhlRateQuote dhlRateQuote = new DhlRateQuote();
			return dhlRateQuote.getRates( "P", "" + totalWeight.intValue(), "1", "1", "1", "AP", "0", order.getShipping().getZip(), order.getShipping().getStateProvince() );
		} catch ( Exception e ) {
			e.printStackTrace();
			return null;
		}
		
	} 

	private List<ShippingRate> getUPSRates(com.webjaguar.model.Address sourceAddress, WebJaguarFacade webJaguar, Order order, List<LineItem> lineItems, Map<String, Configuration> siteConfig, Double totalWeight, boolean addressType){
		
		try {
			UpsRateQuote upsRateQuote = new UpsRateQuote();
			// getRates(String fromZip, String fromCountry, String toZip, String toCountry, double unknownPackageWeight, String addressType, boolean hasUnknownPackage, List<Map <String, Object>> packages, String pickupType)

			List<Map <String, Object>> packages = new ArrayList<Map <String, Object>>();
			double unknownPackageWeight = 0;	
			boolean hasUnknownPackage = false;
			int packageNumber = 0;
			Map<Integer, Double> independentPackages = new HashMap<Integer, Double>();
			for (LineItem lineItem: lineItems) {
				if (lineItem.getProduct().getWeight() == null || lineItem.getProduct().getWeight() <= 0) {
					hasUnknownPackage = true;
				} else if (lineItem.getCustomShippingCost() != null) {
					// ignore custom shipping items
				} else if (lineItem.getCubicSize() != null) {
					Map<String, Object> packageMap = new HashMap<String, Object>();
					packageMap.put("packageWeight", Math.ceil(lineItem.getPackageWeight()));
					packageMap.put("qty", lineItem.getQuantity());
					packageMap.put("length", lineItem.getProduct().getPackageL());
					packageMap.put("width", lineItem.getProduct().getPackageW());
					packageMap.put("height", lineItem.getProduct().getPackageH());
					
					packages.add(packageMap);
				} else if (lineItem.getProduct().getUpsMaxItemsInPackage() != null){	
					int seperatePackage = (new Double(Math.floor(lineItem.getQuantity() * lineItem.getCaseContentQty() * lineItem.getCasePackQty() / lineItem.getProduct().getUpsMaxItemsInPackage()))).intValue();
					
					// packages with max quantity
					for(int i=0; i< seperatePackage ; i++) {
						independentPackages.put(packageNumber++, lineItem.getProduct().getUpsMaxItemsInPackage() * lineItem.getProduct().getWeight());
				    }
				    double remainingWeight = ((lineItem.getQuantity() * lineItem.getCaseContentQty() * lineItem.getCasePackQty()) - (seperatePackage * lineItem.getProduct().getUpsMaxItemsInPackage())) * lineItem.getProduct().getWeight();
					if(remainingWeight > 0) {
				    	independentPackages.put(packageNumber++, remainingWeight);
				    }
				} else {	
					hasUnknownPackage = true;
					unknownPackageWeight += lineItem.getPackageWeight() * lineItem.getQuantity();
				}
			}
			return upsRateQuote.getRates(sourceAddress.getZip(), sourceAddress.getStateProvince(), sourceAddress.getCountry(), siteConfig.get("UPS_USERID").getValue(), siteConfig.get("UPS_PASSWORD").getValue(), siteConfig.get("UPS_ACCESS_LICENSE_NUMBER").getValue(), order.getShipping().getZip(),order.getShipping().getStateProvince(), order.getShipping().getCountry(), 
									unknownPackageWeight, addressType, hasUnknownPackage, packages, independentPackages, siteConfig.get("UPS_PICKUPTYPE").getValue(), siteConfig.get("UPS_SHIPPER_NUMBER").getValue());
		} catch ( Exception e ) {
			e.printStackTrace();
			return null;
		}
		
	}
	
}
