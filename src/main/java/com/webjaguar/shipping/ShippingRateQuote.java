package com.webjaguar.shipping;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.model.ShippingMethod;
import com.webjaguar.model.ShippingRate;
import com.webjaguar.model.Weather;
import com.webjaguar.thirdparty.shipping.dhl.DhlRateQuote;
import com.webjaguar.thirdparty.shipping.fedex.rating.Address;
import com.webjaguar.thirdparty.shipping.fedex.rating.FedexRateQuote;
import com.webjaguar.thirdparty.shipping.ups.UpsRateQuote;
import com.webjaguar.thirdparty.shipping.ups.UpsTransitQuote;
import com.webjaguar.thirdparty.shipping.usps.UspsIntRateQuote;
import com.webjaguar.thirdparty.shipping.usps.UspsRateQuote;
import com.webjaguar.thirdparty.weatherchannel.WeatherChannelApi;

public class ShippingRateQuote
{
	public List<ShippingRate> getShippingRates(HttpServletRequest request, Order order, WebJaguarFacade webJaguar, double handlingTotal)
	{
		String upsGroundTransitDay = null;
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();
		Map gSiteConfig = (Map) request.getAttribute( "gSiteConfig" );
		Double orderWeight = order.getTotalWeight() - order.getCustomShippingTotalWeight();
		
		//if order source zip code is different than site config, use order source zip code
		String sourceZipCode = siteConfig.get( "SOURCE_ZIP" ).getValue();
		if(order.getSource() != null && order.getSource().getZip() != null && !order.getSource().getZip().isEmpty()) {
			sourceZipCode = order.getSource().getZip();
		}
		String sourceStateProvince = siteConfig.get( "SOURCE_STATEPROVINCE" ).getValue();
		if(order.getSource() != null && order.getSource().getStateProvince() != null && !order.getSource().getStateProvince().isEmpty()) {
			sourceStateProvince = order.getSource().getStateProvince();
		}
		String sourceCountry = siteConfig.get( "SOURCE_COUNTRY" ).getValue();
		if(order.getSource() != null && order.getSource().getCountry() != null && !order.getSource().getCountry().isEmpty()) {
			sourceCountry = order.getSource().getCountry();
		}
		// list to be returned
		List<ShippingRate> ratesList = new ArrayList<ShippingRate>();
        boolean addressType = webJaguar.getAddressType(order.getShipping());
		// get Fedex rate quotes
		if ( webJaguar.isActiveCarrier( "fedex" ) )
		{
			try
			{
				FedexRateQuote fedexRateQuote = new FedexRateQuote();
				Address originAddress = new Address(new String[] { siteConfig.get( "SOURCE_ADDRESS" ).getValue() }, siteConfig.get( "SOURCE_CITY" ).getValue(), sourceStateProvince, sourceZipCode, "", sourceCountry, true);
				Address destinAddress = new Address(new String[] { order.getShipping().getAddr1() }, order.getShipping().getCity(), (order.getShipping().getStateProvince() == null || order.getShipping().getStateProvince().length() < 2) ? order.getShipping().getStateProvince() : order.getShipping().getStateProvince().substring( 0, 2 ), order.getShipping().getZip(), "", order.getShipping().getCountry(), addressType);
				ratesList = fedexRateQuote.getRates( originAddress, destinAddress, orderWeight.toString(),
						siteConfig.get( "FEDEX_USER_KEY" ).getValue(), siteConfig.get( "FEDEX_USER_PASSWORD" ).getValue(), siteConfig.get( "FEDEX_USER_METERNUMBER" ).getValue(),  siteConfig.get( "FEDEX_ACCOUNT_NUMBER" ).getValue(),
						siteConfig.get( "FEDEX_SIGNATURE_TYPE" ).getValue(), siteConfig.get( "FEDEX_PACKAGING_TYPE" ).getValue(), siteConfig.get( "FEDEX_DROP_OFF_TYPE" ).getValue(),
						siteConfig.get( "PACKAGING_DIMENSION_LENGTH" ).getValue(), siteConfig.get( "PACKAGING_DIMENSION_WIDTH" ).getValue(), siteConfig.get( "PACKAGING_DIMENSION_HEIGHT" ).getValue(), siteConfig.get( "PACKAGING_DIMENSION_UNIT" ).getValue(), Boolean.getBoolean( siteConfig.get( "FEDEX_SATURDAY_DELIVERY" ).getValue()) );
			}
			catch ( Exception e )
			{
				e.printStackTrace();
			}
		}

		// get USPS rate quotes
		if ( webJaguar.isActiveCarrier( "usps" ) && siteConfig.get( "USPS_USERID" ).getValue().length() > 0 )
		{
			
			double unknownPackageWeight = 0;	
			boolean hasUnknownPackage = false;
			int packageNumber = 0;
			Map<Integer, Double> independentPackages = new HashMap<Integer, Double>();
			
			for (LineItem lineItem: order.getLineItems()) {
				if (lineItem.getProduct().getWeight() == null || lineItem.getProduct().getWeight() <= 0) {
					hasUnknownPackage = true;
				} else if (lineItem.getCustomShippingCost() != null) {
					// ignore custom shipping items
				} else if (lineItem.getProduct().getUspsMaxItemsInPackage() != null){	
					int seperatePackage = (new Double(Math.floor(lineItem.getQuantity() * lineItem.getCaseContentQty() * lineItem.getCasePackQty() / lineItem.getProduct().getUspsMaxItemsInPackage()))).intValue();
					
					// packages with max quantity
					for(int i=0; i< seperatePackage ; i++) {
						independentPackages.put(packageNumber++, lineItem.getProduct().getUspsMaxItemsInPackage() * lineItem.getProduct().getWeight());
				    }
				    double remainingWeight = ((lineItem.getQuantity() * lineItem.getCaseContentQty() * lineItem.getCasePackQty()) - (seperatePackage * lineItem.getProduct().getUspsMaxItemsInPackage())) * lineItem.getProduct().getWeight();
					if(remainingWeight > 0) {
				    	independentPackages.put(packageNumber++, remainingWeight);
				    }
				} else {	
					hasUnknownPackage = true;
					unknownPackageWeight += lineItem.getPackageWeight() * lineItem.getQuantity();
				}
			}
			
			
			if ( order.getShipping().getCountry().equalsIgnoreCase( "US" )) {
				try
				{
					UspsRateQuote uspsRateQuote = new UspsRateQuote();
					ratesList.addAll( uspsRateQuote.getRates( siteConfig.get( "USPS_USERID" ).getValue(), "All", sourceZipCode, getFiveDigitZipcode(order.getShipping().getZip()), hasUnknownPackage, unknownPackageWeight, independentPackages,"REGULAR" ) );
				}
				catch ( Exception e )
				{
					e.printStackTrace();
				}
			}
			else {
				try
				{
					UspsIntRateQuote uspsIntRateQuote = new UspsIntRateQuote();
					String countryName = webJaguar.getCountryByCode( order.getShipping().getCountry() ).getName();
					if (countryName.equals( "United Kingdom" )) {
						// USPS does not accept 'United Kingdom'
						countryName = "Great Britain";
					} else if (countryName.equals("Russian Federation")) {
						// USPS does not accept 'Russian Federation'
						countryName = "Russia";						
					} else if(countryName.equals("Slovakia (Slovak Republic)")) {
						// USPS does not accept 'Slovakia (Slovak Republic)'
						countryName="Slovakia";
					} else if (countryName.equals("Bosnia and Herzegowina")) {
						countryName="Bosnia-Herzegovina";
					} else if (countryName.equals("Falkland Islands (Malvinas)")) {
						countryName="Falkland Islands";
					} else if (countryName.equals("South Georgia and the South Sandwich Islands")) {
						countryName="South Georgia";
					} else if (countryName.equals("Iran (Islamic Republic of)")) {
						countryName="Iran";
					} else if (countryName.equals("Korea, Democratic People\'s Republic of")) {
						countryName="North Korea";
					} else if (countryName.equals("Korea, Republic of")) {
						countryName="South Korea";
					} else if (countryName.equals("Lao People\'s Democratic Republic")) {
						countryName="Laos";//not sure
					} else if (countryName.equals("Macedonia, The Former Yugoslav Republic of")) {
						countryName="Macedonia, Republic of";
					} else if (countryName.equals("Tanzania, United Republic of")) {
						countryName="Tanzania";
					} else if (countryName.equals("Vatican City State (Holy See)")) {
						countryName="Vatican City";
					} else if (countryName.equals("Viet Nam")) {
						countryName="Vietnam";
					}					
					ratesList.addAll( uspsIntRateQuote.getRates( siteConfig.get( "USPS_USERID" ).getValue(), countryName, hasUnknownPackage, unknownPackageWeight, independentPackages, null ) );
				}
				catch ( Exception e )
				{
					e.printStackTrace();
				}
			}
		}

		// get DHL rate quote
		if ( webJaguar.isActiveCarrier( "dhl" ) )
		{
			try
			{
				DhlRateQuote dhlRateQuote = new DhlRateQuote();
				ratesList.addAll( dhlRateQuote.getRates( "P", "" + orderWeight.intValue(), "1", "1", "1", "AP", "0", order.getShipping().getZip(), order.getShipping().getStateProvince() ) );
			}
			catch ( Exception e )
			{
				e.printStackTrace();
			}
		}

		// get UPS rate quote
		if ( webJaguar.isActiveCarrier( "ups" ) )
		{
			try
			{
				UpsRateQuote upsRateQuote = new UpsRateQuote();
				// getRates(String fromZip, String fromCountry, String toZip, String toCountry, double unknownPackageWeight, String addressType, boolean hasUnknownPackage, List<Map <String, Object>> packages, String pickupType)

				List<Map <String, Object>> packages = new ArrayList<Map <String, Object>>();
				double unknownPackageWeight = 0;	
				boolean hasUnknownPackage = false;
				int packageNumber = 0;
				Map<Integer, Double> independentPackages = new HashMap<Integer, Double>();
				for (LineItem lineItem: order.getLineItems()) {
					if (lineItem.getProduct().getWeight() == null || lineItem.getProduct().getWeight() <= 0) {
						hasUnknownPackage = true;
					} else if (lineItem.getCustomShippingCost() != null) {
						// ignore custom shipping items
					} else if (lineItem.getCubicSize() != null) {
						Map<String, Object> packageMap = new HashMap<String, Object>();
						packageMap.put("packageWeight", Math.ceil(lineItem.getPackageWeight()));
						packageMap.put("qty", lineItem.getQuantity());
						packageMap.put("length", lineItem.getProduct().getPackageL());
						packageMap.put("width", lineItem.getProduct().getPackageW());
						packageMap.put("height", lineItem.getProduct().getPackageH());
						
						packages.add(packageMap);
					} else if (lineItem.getProduct().getUpsMaxItemsInPackage() != null){	
						int seperatePackage = (new Double(Math.floor(lineItem.getQuantity() * lineItem.getCaseContentQty() * lineItem.getCasePackQty() / lineItem.getProduct().getUpsMaxItemsInPackage()))).intValue();
						
						// packages with max quantity
						for(int i=0; i< seperatePackage ; i++) {
							independentPackages.put(packageNumber++, lineItem.getProduct().getUpsMaxItemsInPackage() * lineItem.getProduct().getWeight());
					    }
					    double remainingWeight = ((lineItem.getQuantity() * lineItem.getCaseContentQty() * lineItem.getCasePackQty()) - (seperatePackage * lineItem.getProduct().getUpsMaxItemsInPackage())) * lineItem.getProduct().getWeight();
						if(remainingWeight > 0) {
					    	independentPackages.put(packageNumber++, remainingWeight);
					    }
					} else {	
						hasUnknownPackage = true;
						unknownPackageWeight += lineItem.getPackageWeight() * lineItem.getQuantity();
					}
				}
				ratesList.addAll(upsRateQuote.getRates(sourceZipCode, sourceStateProvince, sourceCountry, siteConfig.get("UPS_USERID").getValue(), siteConfig.get("UPS_PASSWORD").getValue(), siteConfig.get("UPS_ACCESS_LICENSE_NUMBER").getValue(), order.getShipping().getZip(),order.getShipping().getStateProvince(), order.getShipping().getCountry(), 
										unknownPackageWeight, addressType, hasUnknownPackage, packages, independentPackages, siteConfig.get("UPS_PICKUPTYPE").getValue(), siteConfig.get("UPS_SHIPPER_NUMBER").getValue()));
			}
			catch ( Exception e )
			{
				e.printStackTrace();
			}
			try
			{
				UpsTransitQuote upsTransitQuote = new UpsTransitQuote();
				upsGroundTransitDay = upsTransitQuote.getTransit( sourceZipCode, sourceCountry, siteConfig.get( "UPS_USERID" ).getValue(), siteConfig.get( "UPS_PASSWORD" ).getValue(), siteConfig.get( "UPS_ACCESS_LICENSE_NUMBER" ).getValue(), order.getShipping().getZip(), order.getShipping().getCountry(), orderWeight, addressType);
			}
			catch ( Exception e )
			{
				e.printStackTrace();
			}
		}
		
		Weather temp = null;
		if ( (Boolean) gSiteConfig.get( "gWEATHER_CHANNEL" ) ) {
			WeatherChannelApi wc = new WeatherChannelApi();
			// get one day forecast
			temp = wc.ModelGetTemp( order.getShipping().getZip(), "2" );
			order.setWeather( temp );
		}

		List<ShippingRate> finalList = new ArrayList<ShippingRate>();
		// set shipping title for each shipping rate object, remove DISABLED shipping rate also
		for ( Iterator it = ratesList.iterator(); it.hasNext(); )
		{
			Date shippingTime = setupDueDate(Integer.parseInt( siteConfig.get( "SHIPPING_TURNOVER_TIME" ).getValue()), Calendar.getInstance(), true);
			Calendar shipTimeCalendar = new GregorianCalendar();
			shipTimeCalendar.setTime( shippingTime );
			ShippingRate shippingRate = (ShippingRate) it.next();
			ShippingMethod shippingMethod = webJaguar.getShippingMethodByCarrierCode( shippingRate );
			if ( shippingMethod != null && shippingMethod.getShippingActive() )
			{
				shippingRate.setTitle( shippingMethod.getShippingTitle() );
				shippingRate.setRank( shippingMethod.getShippingRank() );
				shippingRate.setMinPrice(shippingMethod.getMinPrice());
				shippingRate.setMaxPrice(shippingMethod.getMaxPrice());
				shippingRate.setId(shippingMethod.getId());
				try {
					if (shippingRate.getCarrier().equals( "ups" ) && !shippingRate.getCode().equals( "03" )) { // UPS Ground code is 03
						shippingRate.setDeliveryDate( setupDueDate(Integer.parseInt( shippingRate.getGuaranteedDaysToDelivery() ), shipTimeCalendar, false));
						shippingRate.setShippingPeriod( shippingRate.getGuaranteedDaysToDelivery() );
					} else if ( shippingRate.getCarrier().equals( "ups" ) && shippingRate.getCode().equals( "03" )) {
						shippingRate.setDeliveryDate( setupDueDate(Integer.parseInt( upsGroundTransitDay ), shipTimeCalendar, false));
						shippingRate.setShippingPeriod( upsGroundTransitDay );
					}
				} catch ( Exception e) {}
				shippingRate.setPrice( roundShippingRate (Double.parseDouble(shippingRate.getPrice()) + handlingTotal).toString() );
				if ( temp == null || !order.isTemperatureEnable() || sourceStateProvince.equals( order.getShipping().getStateProvince()) ) {
					finalList.add( shippingRate );
				}
				else {
					// if I get N/A I assume the high temperature 
					if ( temp.getHigh() == null || temp.getHigh().equals( "N/A" ) || Integer.parseInt( temp.getHigh() ) > order.getLowestTemp() ) {
						if ( shippingMethod.getWeatherTemp() < order.getLowestTemp() )
							finalList.add( shippingRate );
					} else
						finalList.add( shippingRate );
				}	
			}
		}
		
		// sort the list by rank
		Collections.sort( finalList, RANK_ORDER );

		// return the rates list
		return finalList;
	}

	private Double roundShippingRate(Double shippingRateDouble)
	{
		int decimalPlace = 2;
		BigDecimal bd = new BigDecimal(shippingRateDouble);
	    bd = bd.setScale(decimalPlace,BigDecimal.ROUND_HALF_UP);
	    shippingRateDouble = bd.doubleValue();
	    return shippingRateDouble;
	}

	private Comparator<ShippingRate> RANK_ORDER = new Comparator<ShippingRate>()
	{
		public int compare(ShippingRate s1, ShippingRate s2)
		{
			return s1.getRank().compareTo( s2.getRank() );
		}
	};
	
	private Date setupDueDate(int turnOverTime, Calendar startTime, boolean AMPM) {
		Calendar now = new GregorianCalendar();
		now.setTime( startTime.getTime() );
		if ( AMPM && now.get( Calendar.AM_PM ) == Calendar.PM ) {
			now.add( Calendar.DAY_OF_MONTH, 1 );
		}
		if ( now.get( GregorianCalendar.DAY_OF_WEEK ) == 1 ) {
			now.add( Calendar.DAY_OF_MONTH, 1 );
		}
		else if ( now.get( GregorianCalendar.DAY_OF_WEEK ) == 7 ) {
			now.add( Calendar.DAY_OF_MONTH, 2 );
		}
		for (int i=0; i < turnOverTime ; i++) {
			now.add( Calendar.DAY_OF_MONTH, 1 );
			if ( now.get( GregorianCalendar.DAY_OF_WEEK ) == 1 ) {
				now.add( Calendar.DAY_OF_MONTH, 1 );
			}
			else if ( now.get( GregorianCalendar.DAY_OF_WEEK ) == 7 ) {
				now.add( Calendar.DAY_OF_MONTH, 2 );
			}
		}
		return now.getTime();
	}
	
	private String getFiveDigitZipcode(String zipcode) {
		String FiveDigitZipcode;
		if (zipcode == null) {
			FiveDigitZipcode = "";
		} else if ( zipcode.trim().length() > 5) {
			FiveDigitZipcode = zipcode.trim().substring( 0, 5 ).trim();
		} else {
			FiveDigitZipcode = zipcode.trim();
		}
		return FiveDigitZipcode;
	}

}
