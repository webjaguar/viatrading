/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.logic;

import https.webservices_rrts_com.ratequote.ArrayOfServiceOptions;
import https.webservices_rrts_com.ratequote.ArrayOfShipmentDetail;
import https.webservices_rrts_com.ratequote.AuthenticationHeader;
import https.webservices_rrts_com.ratequote.QuoteRequest;
import https.webservices_rrts_com.ratequote.QuoteResponse;
import https.webservices_rrts_com.ratequote.RateQuoteSoap;
import https.webservices_rrts_com.ratequote.RateQuote_Service;
import https.webservices_rrts_com.ratequote.ServiceOptions;
import https.webservices_rrts_com.ratequote.ShipmentDetail;

import java.awt.Color;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.poi.hssf.record.formula.functions.And;
import org.apache.poi.hssf.record.formula.functions.Code;
import org.apache.xmlbeans.impl.common.IdentityConstraint.IdState;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.support.RequestContextUtils;
import org.springframework.web.util.WebUtils;

import asiProduct.PriceGrid;

import com.buySafe.Amount;
import com.buySafe.ArrayOfShoppingCartItem;
import com.buySafe.ArrayOfString;
import com.buySafe.BuySafeAPI;
import com.buySafe.BuySafeAPISoap;
import com.buySafe.BuySafeUserCredentials;
import com.buySafe.BuySafeWSHeader;
import com.buySafe.BuyerInformation;
import com.buySafe.CountryCode;
import com.buySafe.CurrencyCode;
import com.buySafe.CustomBoolean;
import com.buySafe.ItemURL;
import com.buySafe.MerchantServiceProviderCredentials;
import com.buySafe.PricingDetails;
import com.buySafe.ShoppingCartAddUpdateRQ;
import com.buySafe.ShoppingCartAddUpdateRS;
import com.buySafe.ShoppingCartAddress;
import com.buySafe.ShoppingCartCheckoutRQ;
import com.buySafe.ShoppingCartCheckoutRS;
import com.buySafe.ShoppingCartItem;
import com.sun.xml.ws.developer.WSBindingProvider;
import com.sun.xml.ws.developer.MemberSubmissionAddressing.Validation;
import com.webjaguar.dao.AccessPrivilegeDao;
import com.webjaguar.dao.AffiliateDao;
import com.webjaguar.dao.BudgetDao;
import com.webjaguar.dao.BuyRequestDao;
import com.webjaguar.dao.CategoryDao;
import com.webjaguar.dao.ConfigDao;
import com.webjaguar.dao.CrmDao;
import com.webjaguar.dao.CustomerDao;
import com.webjaguar.dao.DataFeedDao;
import com.webjaguar.dao.DealDao;
import com.webjaguar.dao.EchoSignDao;
import com.webjaguar.dao.EventDao;
import com.webjaguar.dao.FaqDao;
import com.webjaguar.dao.GiftCardDao;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.dao.InventoryDao;
import com.webjaguar.dao.LayoutDao;
import com.webjaguar.dao.LocationDao;
import com.webjaguar.dao.MailInRebateDao;
import com.webjaguar.dao.ManufacturerDao;
import com.webjaguar.dao.MassEmailDao;
import com.webjaguar.dao.MultiStoreDao;
import com.webjaguar.dao.OrderDao;
import com.webjaguar.dao.PaymentDao;
import com.webjaguar.dao.PolicyDao;
import com.webjaguar.dao.PresentationDao;
import com.webjaguar.dao.ProductDao;
import com.webjaguar.dao.ProductImportDao;
import com.webjaguar.dao.PromoDao;
import com.webjaguar.dao.ReportDao;
import com.webjaguar.dao.ReviewDao;
import com.webjaguar.dao.RmaDao;
import com.webjaguar.dao.SalesRepDao;
import com.webjaguar.dao.SearchEngineProspectDao;
import com.webjaguar.dao.ServiceDao;
import com.webjaguar.dao.ShoppingCartDao;
import com.webjaguar.dao.SubscriptionDao;
import com.webjaguar.dao.SupplierDao;
import com.webjaguar.dao.TicketDao;
import com.webjaguar.dao.TruckLoadProcessDao;
import com.webjaguar.dao.VendorDao;
import com.webjaguar.listener.SessionListener;
import com.webjaguar.model.*;
import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.model.crm.CrmAccount;
import com.webjaguar.model.crm.CrmAccountSearch;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactField;
import com.webjaguar.model.crm.CrmContactGroup;
import com.webjaguar.model.crm.CrmContactGroupSearch;
import com.webjaguar.model.crm.CrmContactSearch;
import com.webjaguar.model.crm.CrmForm;
import com.webjaguar.model.crm.CrmFormField;
import com.webjaguar.model.crm.CrmQualifier;
import com.webjaguar.model.crm.CrmQualifierSearch;
import com.webjaguar.model.crm.CrmTask;
import com.webjaguar.model.crm.CrmTaskSearch;
import com.webjaguar.shipping.ShippingRateQuote;
import com.webjaguar.shipping.ShippingRateQuotePerSupplier;
import com.webjaguar.thirdparty.asi.asiAPI.Product.Variants.Variant;
import com.webjaguar.thirdparty.dsdi.DsdiCategory;
import com.webjaguar.thirdparty.dsi.DsiCategory;
import com.webjaguar.thirdparty.echosign.EchoSign;
import com.webjaguar.thirdparty.echosign.EchoSignSearch;
import com.webjaguar.thirdparty.echosign.EchoSignWidgetSearch;
import com.webjaguar.thirdparty.echosign.Widget;
import com.webjaguar.thirdparty.imagej.ImageManipulator;
import com.webjaguar.thirdparty.ingrammicro.IngramMicroCategory;
import com.webjaguar.thirdparty.itext.ITextApi;
import com.webjaguar.thirdparty.koleImports.KoleImportsCategory;
import com.webjaguar.thirdparty.payment.amazon.Iopn;
import com.webjaguar.thirdparty.payment.ebillme.EBillme;
import com.webjaguar.thirdparty.payment.geMoney.GEMoney;
import com.webjaguar.thirdparty.synnex.SynnexCategory;
import com.webjaguar.thirdparty.techdata.TechdataCategory;
import com.webjaguar.thirdparty.techdata.TechdataManufacturer;
import com.webjaguar.web.admin.customer.CreditImp;
import com.webjaguar.web.admin.customer.PaymentImp;
import com.webjaguar.web.admin.datafeed.WebjaguarCategory;
import com.webjaguar.web.domain.Constants;
import com.webjaguar.web.domain.Utilities;
import com.webjaguar.web.form.AccessPrivilegeForm;
import com.webjaguar.web.form.EmailMessageForm;
import com.webjaguar.web.form.GroupPrivilegeForm;
import com.webjaguar.web.form.OrderForm;
import com.webjaguar.web.form.PurchaseOrderForm;
import com.webjaguar.web.form.TicketForm;
import com.webjaguar.web.form.crm.CrmFormBuilderForm;
import com.webjaguar.web.frontend.shoppingCart.BuySafeResponse;


public class WebJaguarImpl extends WebApplicationObjectSupport implements WebJaguarFacade {

	private AffiliateDao affiliateDao;
	private CustomerDao customerDao;
	private CategoryDao categoryDao;
	private ProductDao productDao;
	private ManufacturerDao manufacturerDao;
	private TruckLoadProcessDao truckLoadProcessDao;
	private PolicyDao policyDao;
	private FaqDao faqDao;
	private ConfigDao configDao;
	private SupplierDao supplierDao;
	private ShoppingCartDao shoppingCartDao;
	private OrderDao orderDao;
	private LayoutDao layoutDao;
	private PromoDao promoDao;
	private SalesRepDao salesRepDao;
	private AccessPrivilegeDao accessPrivilegeDao;
	private PaymentDao paymentDao;
	private ServiceDao serviceDao;
	private DataFeedDao dataFeedDao;
	private TicketDao ticketDao;
	private InventoryDao inventoryDao;
	private ReportDao reportDao;
	private MultiStoreDao multiStoreDao;
	private GiftCardDao giftCardDao;
	private SubscriptionDao subscriptionDao;
	private RmaDao rmaDao;
	private BudgetDao budgetDao;
	private MassEmailDao massEmailDao;
	private LocationDao locationDao;
	private BuyRequestDao buyRequestDao;
	private ReviewDao reviewDao;
	private VendorDao vendorDao;
	private ImageManipulator imageManipulator = new ImageManipulator();
	private ITextApi iText = new ITextApi();
	private EventDao eventDao;
	private DealDao dealDao;
	private CrmDao crmDao;
	private MailInRebateDao mailInRebateDao;
	private EchoSignDao echoSignDao;
	private GlobalDao globalDao;
	private PresentationDao presentationDao;
	private SearchEngineProspectDao searchEngineProspectDao;
	private ProductImportDao productImportDao;

	// -------------------------------------------------------------------------
	// Setter methods for dependency injection
	// -------------------------------------------------------------------------

	public void setAffiliateDao(AffiliateDao affiliateDao) {
		this.affiliateDao = affiliateDao;
	}

	public void setCustomerDao(CustomerDao customerDao) {
		this.customerDao = customerDao;
	}

	public void setProductDao(ProductDao productDao) {
		this.productDao = productDao;
	}

	public void setCategoryDao(CategoryDao categoryDao) {
		this.categoryDao = categoryDao;
	}

	public void setManufacturerDao(ManufacturerDao manufacturerDao) {
		this.manufacturerDao = manufacturerDao;
	}

	public void setTruckLoadProcessDao(TruckLoadProcessDao truckLoadProcessDao) {
		this.truckLoadProcessDao = truckLoadProcessDao;
	}

	public void setPolicyDao(PolicyDao policyDao) {
		this.policyDao = policyDao;
	}

	public void setFaqDao(FaqDao faqDao) {
		this.faqDao = faqDao;
	}

	public void setSupplierDao(SupplierDao supplierDao) {
		this.supplierDao = supplierDao;
	}

	public void setShoppingCartDao(ShoppingCartDao shoppingCartDao) {
		this.shoppingCartDao = shoppingCartDao;
	}

	public void setOrderDao(OrderDao orderDao) {
		this.orderDao = orderDao;
	}

	public void setConfigDao(ConfigDao configDao) {
		this.configDao = configDao;
	}
	
	public ConfigDao getConfigDao() {
		return configDao;
	}

	public void setLayoutDao(LayoutDao layoutDao) {
		this.layoutDao = layoutDao;
	}

	public void setPromoDao(PromoDao promoDao) {
		this.promoDao = promoDao;
	}

	public void setDealDao(DealDao dealDao) {
		this.dealDao = dealDao;
	}

	public void setSalesRepDao(SalesRepDao salesRepDao) {
		this.salesRepDao = salesRepDao;
	}

	public void setPresentationDao(PresentationDao presentationDao) {
		this.presentationDao = presentationDao;
	}

	public void setAccessPrivilegeDao(AccessPrivilegeDao accessPrivilegeDao) {
		this.accessPrivilegeDao = accessPrivilegeDao;
	}

	public void setPaymentDao(PaymentDao paymentDao) {
		this.paymentDao = paymentDao;
	}

	public void setServiceDao(ServiceDao serviceDao) {
		this.serviceDao = serviceDao;
	}

	public void setDataFeedDao(DataFeedDao dataFeedDao) {
		this.dataFeedDao = dataFeedDao;
	}

	public void setTicketDao(TicketDao ticketDao) {
		this.ticketDao = ticketDao;
	}

	public void setInventoryDao(InventoryDao inventoryDao) {
		this.inventoryDao = inventoryDao;
	}

	public void setReportDao(ReportDao reportDao) {
		this.reportDao = reportDao;
	}

	public void setMultiStoreDao(MultiStoreDao multiStoreDao) {
		this.multiStoreDao = multiStoreDao;
	}

	public void setGiftCardDao(GiftCardDao giftCardDao) {
		this.giftCardDao = giftCardDao;
	}

	public void setSubscriptionDao(SubscriptionDao subscriptionDao) {
		this.subscriptionDao = subscriptionDao;
	}

	public void setRmaDao(RmaDao rmaDao) {
		this.rmaDao = rmaDao;
	}

	public void setBudgetDao(BudgetDao budgetDao) {
		this.budgetDao = budgetDao;
	}

	public void setMassEmailDao(MassEmailDao massEmailDao) {
		this.massEmailDao = massEmailDao;
	}

	public void setLocationDao(LocationDao locationDao) {
		this.locationDao = locationDao;
	}

	public void setBuyRequestDao(BuyRequestDao buyRequestDao) {
		this.buyRequestDao = buyRequestDao;
	}

	public void setReviewDao(ReviewDao reviewDao) {
		this.reviewDao = reviewDao;
	}

	public void setVendorDao(VendorDao vendorDao) {
		this.vendorDao = vendorDao;
	}

	public UserSession getUserSession(HttpServletRequest request) {

		// return session, if session id is available
		// developed for CXML
		String jsId = null;
		if (request.getParameter("jsId") != null) {
			jsId = request.getParameter("jsId");
		} else if (request.getSession().getAttribute("jsId") != null) {
			jsId = (String) request.getSession().getAttribute("jsId");
		}
		if (jsId != null) {
			Integer userId = SessionListener.getActiveSessions().get(jsId);
			if (userId != null) {
				UserSession userSession = new UserSession();
				userSession.setUserid(userId);
				this.setUserSession(request, userSession);
				request.getSession().setAttribute("jsId", jsId);
				return userSession;
			}
		}
		return (UserSession) request.getAttribute("userSession");
	}

	public void setEventDao(EventDao eventDao) {
		this.eventDao = eventDao;
	}

	public void setCrmDao(CrmDao crmDao) {
		this.crmDao = crmDao;
	}

	public void setMailInRebateDao(MailInRebateDao mailInRebateDao) {
		this.mailInRebateDao = mailInRebateDao;
	}

	public void setEchoSignDao(EchoSignDao echoSignDao) {
		this.echoSignDao = echoSignDao;
	}

	public void setGlobalDao(GlobalDao globalDao) {
		this.globalDao = globalDao;
	}

	public void setSearchEngineProspectDao(SearchEngineProspectDao searchEngineProspectDao) {
		this.searchEngineProspectDao = searchEngineProspectDao;
	}

	// -------------------------------------------------------------------------
	// Operation methods, implementing the WebJaguarFacade interface
	// -------------------------------------------------------------------------

	public ProductImportDao getProductImportDao() {
		return productImportDao;
	}

	public void setProductImportDao(ProductImportDao productImportDao) {
		this.productImportDao = productImportDao;
	}

	public void setUserSession(HttpServletRequest request, UserSession userSession) {
		WebUtils.setSessionAttribute(request, "userSession", userSession);
	}

	public Integer getHomePageByHost(String host) {
		return this.categoryDao.getHomePageByHost(host);
	}

	public Map<String, Category> getI18nCategory(int cid, String lang) {
		return this.categoryDao.getI18nCategory(cid, lang);
	}

	public void updateI18nCategory(Collection<Category> categories) {
		this.categoryDao.updateI18nCategory(categories);
	}

	public List<Category> getCategoryTree(Integer catId, String protectedAccess, boolean showHidden, Boolean showOnSearch, Set<Integer> extraIds, String lang) {
		List<Category> categories = this.categoryDao.getCategories(protectedAccess, showHidden, showOnSearch, lang);
		List<Category> mainCategories = new ArrayList<Category>();
		Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
		// create category map
		for (Category category : categories) {
			categoryMap.put(category.getId(), category);
		}
		// associate categories to parents
		for (Category category : categories) {
			if (category.getParent() == null) {
				mainCategories.add(category);
			} else {
				if (categoryMap.containsKey(category.getParent())) {
					categoryMap.get(category.getParent()).getSubCategories().add(category);
				}
			}
		}
		if (catId != null || extraIds != null) {
			List<Category> thisCategory = new ArrayList<Category>();
			if (catId != null && categoryMap.containsKey(catId)) {
				thisCategory.add(categoryMap.get(catId));
			}
			if (extraIds != null) {
				for (Integer extraId : extraIds) {
					if (categoryMap.containsKey(extraId)) {
						thisCategory.add(categoryMap.get(extraId));
					}
				}
			}
			return thisCategory;
		}
		return mainCategories;
	}

	public Set<Integer> getParentCategories(Integer categoryId) {
		return this.categoryDao.getParentCategories(categoryId);
	}

	public Map<String, Long> getCategoryExternalIdMap() {
		return this.categoryDao.getCategoryExternalIdMap();
	}

	public void insertCategoryTree(Integer parentId, List<Category> categories) {
		int rank = 10;
		for (Category thisCategory : categories) {
			thisCategory.setParent(parentId);
			thisCategory.setRank(rank);
			rank = rank + 10;
			this.categoryDao.insertCategory(thisCategory);

			insertCategoryTree(thisCategory.getId(), thisCategory.getSubCategories());
		}
	}

	public void updateAddress(Address address) {
		this.customerDao.updateAddress(address);
	}

	public void deleteAddress(Address address) {
		this.customerDao.deleteAddress(address);
	}

	public void insertAddress(Address address) {
		this.customerDao.insertAddress(address);
	}

	public String createPasswordToken(Integer userId) {
		return this.customerDao.createPasswordToken(userId);
	}

	public boolean validToken(String token) {
		return this.customerDao.validToken(token);
	}

	public Integer getUserIdByToken(String token) {
		return this.customerDao.getUserIdByToken(token);
	}

	public Integer getCustomerIdByAccountNumber(String accountNumber) {
		return this.customerDao.getCustomerIdByAccountNumber(accountNumber);
	}

	public void updatePassword(String newPassword, String token) {
		this.customerDao.updatePassword(newPassword, token);
	}

	public List<Customer> getCustomerListWithDate(CustomerSearch search) {
		return this.customerDao.getCustomerListWithDate(search);
	}
	
	public ZipCode getZipCode(String zipcode) {
		return this.globalDao.getZipCode(zipcode);
	}
	
	public List<ZipCode> getZipCodeList(LocationSearch locationSearch) {
		return this.globalDao.getZipCodeList(locationSearch);
	}

	public int getCustomerListWithDateCount(CustomerSearch search) {
		return this.customerDao.getCustomerListWithDateCount(search);
	}

	public List<Integer> getCustomerIdsList(CustomerSearch search) {
		return this.customerDao.getCustomerIdsList(search);
	}

	public List<Customer> getCustomers(CustomerSearch search) {
		return this.customerDao.getCustomers(search);
	}

	public int getCustomersCount() {
		return this.customerDao.getCustomersCount();
	}

	public List<Customer> getCustomerListByZipCode(LocationSearch search) {
		return this.customerDao.getCustomerListByZipCode(search);
	}

	public int getCustomerListByZipCodeCount(LocationSearch search) {
		return this.customerDao.getCustomerListByZipCodeCount(search);
	}

	public Customer getCustomerById(Integer userId) {
		return this.customerDao.getCustomerById(userId);
	}

	public List<Integer> getUserIdByCrmId(List<Integer> userIds) {
		return this.customerDao.getUserIdByCrmId(userIds);
	}
	
	public Integer getUserIdByCrmId(Integer crmContactId) {
		return this.customerDao.getUserIdByCrmId(crmContactId);
	}
	
	public Integer getCrmIdByUsername(String username) {
		return this.customerDao.getCrmIdByUsername(username);
	}
	
	public Map<String,CsvMap> getAvailableUserList(List<String> username) {
		return this.customerDao.getAvailableUserList(username);
	}
	
	public List<String> getUserIdListByUsername(List<String> username) {
		return this.customerDao.getUserIdListByUsername(username);
	}
	
	public Map<String,Long> getUserIdsMapByUsername(List<String> username) {
		return this.customerDao.getUserIdsMapByUsername(username);
	}
	
	public String getTaxIdByUserId(Integer userId) {
		return this.customerDao.getTaxIdByUserId(userId);
	}
	
	public String getTextMessageServerById(Integer textMessageServerId) {
		return this.customerDao.getTextMessageServerById(textMessageServerId);
	}
	
	public String checkUserByCellPhone(String username2) {
		return this.customerDao.checkUserByCellPhone(username2);
	}
	
	public Customer getCellPhoneByUsernameAndPassword(String username2, String password) {
		return this.customerDao.getCellPhoneByUsernameAndPassword(username2,  password);
	}

	public Customer getCustomerByUsername(String username) {
		return this.customerDao.getCustomerByUsername(username);
	}

	public Customer getCustomerByUsernameAndPassword(String username, String password) {
		return this.customerDao.getCustomerByUsernameAndPassword(username, password);
	}

	public Customer getCustomerByCardIdAndPassword(String cardId, String password) {
		return this.customerDao.getCustomerByCardIdAndPassword(cardId, password);
	}

	public Customer getCustomerByCardID(String cardID) {
		return this.customerDao.getCustomerByCardID(cardID);
	}

	public void updateCustomer(Customer customer, boolean changeUsername, boolean changePassword) {
		this.customerDao.updateCustomer(customer, changeUsername, changePassword);
		if (customer.isConvertToSupplier() || customer.getSupplierId() != null) {
			// update/insert supplier
			this.supplierDao.insertSupplierByCustomer(customer);
			this.locationDao.insertLocationByCustomer(customer);
		}
		if(customer.getBrokerImage() != null && !customer.isRemoveBrokerImage()){
			this.customerDao.updateCustomerBrokerImage(customer);
		}
		if(customer.isRemoveBrokerImage()){
			this.customerDao.removeCustomerBrokerImage(customer);
		}
	}
	
	public int insertDialingNoteHistory(DialingNote note){
		return this.customerDao.insertDialingNoteHistory(note);
	}

	public void updateCardIdCount(Integer userId) {
		this.customerDao.updateCardIdCount(userId);
	}

	public boolean getAddressType(Address address) {
		if (getSiteConfig().get("ADDRESS_RESIDENTIAL_COMMERCIAL").getValue().equals("1")) {
			return true;
		} else if (Integer.parseInt(getSiteConfig().get("ADDRESS_RESIDENTIAL_COMMERCIAL").getValue()) >= 2) {
			if (address.isResidential()) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public void generateUsername(Customer customer) {
		String username = UUID.randomUUID().toString().toUpperCase().replace("-", "").substring(0, 9);
		if (getUserByUserName(username) == null) {
			customer.setUsername((username+"@noemail.com").toLowerCase());
		} else {
			generateUsername(customer);
		}
	}
	
	public String removeCellPhone(String cellPhone) {
		if(cellPhone!=null && cellPhone.length()>0){
			cellPhone = cellPhone.replaceAll("\\(", "");
			cellPhone = cellPhone.replaceAll("\\)", "");
			cellPhone = cellPhone.replaceAll("\\-", "");
			cellPhone = cellPhone.replaceAll("\\+", "");
			cellPhone = cellPhone.replaceAll(" ", "");
		}
		return cellPhone;
	}
	

	public void insertCustomer(Customer customer, Address shipping, boolean insertCrmAccountContact) {
		customer.getAddress().setResidential(getAddressType(customer.getAddress()));
		
		if (customer.isRegisterCellPhone()) {
			generateUsername(customer);
		}
		
    	System.out.println("PASS final :"+customer.getPassword());

		this.customerDao.insertCustomer(customer);
		if (customer.isConvertToSupplier() || customer.getSupplierId() != null) {
			// update/insert supplier
			this.supplierDao.insertSupplierByCustomer(customer);
			this.locationDao.insertLocationByCustomer(customer);
		}
		if (shipping != null) {
			shipping.setUserId(customer.getId());
			shipping.setDefaultShipping(true);
			if (shipping.getLastName() == null) {
				shipping.setLastName(customer.getAddress().getLastName());
			}
			insertAddress(shipping);
		}

		if (insertCrmAccountContact) {
			insertCrmAccountContact(customer, shipping);
		}
		//Broker Logo
		if(customer.getBrokerImage() != null && !customer.isRemoveBrokerImage()){
			this.customerDao.updateCustomerBrokerImage(customer);
		}
		if(customer.isRemoveBrokerImage()){
			this.customerDao.removeCustomerBrokerImage(customer);
		}
	}

	public void insertCrmAccountContact(Customer customer, Address shipping) {

		String crmAccountName = null;
		CrmAccount crmAccount = null;

		if (customer.getAddress().getCompany() != null && !customer.getAddress().getCompany().equals("")) {
			crmAccountName = customer.getAddress().getCompany();
		} else {
			crmAccountName = customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName();
		}
		crmAccount = this.crmDao.getCrmAccountByName(crmAccountName);

		/*
		 * if Account does not exist then create CRM Account for user who fill out the CRM Form
		 */
		if (crmAccount == null) {
			crmAccount = new CrmAccount();

			crmAccount.setAccountName(crmAccountName);
			crmAccount.setAccountOwner("Admin");
			crmAccount.setAccountType("Customer");

			crmAccount.setBillingStreet(customer.getAddress().getAddr1() + " " + customer.getAddress().getAddr2());
			crmAccount.setBillingCity(customer.getAddress().getCity());
			crmAccount.setBillingStateProvince(customer.getAddress().getStateProvince());
			crmAccount.setBillingCountry(customer.getAddress().getCountry());
			crmAccount.setBillingZip(customer.getAddress().getZip());

			if (shipping != null) {
				crmAccount.setShippingStreet(shipping.getAddr1() + " " + shipping.getAddr2());
				crmAccount.setShippingCity(shipping.getCity());
				crmAccount.setShippingStateProvince(shipping.getStateProvince());
				crmAccount.setShippingZip(shipping.getCountry());
				crmAccount.setShippingCountry(shipping.getCountry());
			}
			this.crmDao.insertCrmAccount(crmAccount);
		}

		/*
		 * Check if Contact exsist on CRM Contact If NO: Create new Contact If YES: We update couple fields on Customer and
		 */
		List<CrmContact> crmContactList = this.crmDao.getCrmContactByEmail1(customer.getUsername());
		if (crmContactList == null || crmContactList.isEmpty()) {
			CrmContact crmContact = new CrmContact();

			crmContact.setAccountId(crmAccount.getId());
			crmContact.setAccountName(crmAccountName);
			crmContact.setProspectId(customer.getProspectId());
			try {
				customerToCrmContactSynch(crmContact, customer);
			} catch (Exception e) {
			}

			this.crmDao.insertCrmContact(crmContact);
			customer.setCrmContactId(crmContact.getId());
			customer.setCrmAccountId(crmContact.getAccountId());
			this.customerDao.updateCrmIds(customer);
		} else {
			for (CrmContact contact : crmContactList) {
				customer.setCrmContactId(contact.getId());
				customer.setCrmAccountId(contact.getAccountId());
				customer.setRegisteredBy((customer.getRegisteredBy() != null ? customer.getRegisteredBy() : "") + (contact.getLeadSource() != null ? contact.getLeadSource() : ""));
				if (customer.getTrackcode() == null || customer.getTrackcode().isEmpty()) {
					customer.setTrackcode(contact.getTrackcode() != null ? contact.getTrackcode() : "");
				}
				if (contact.getAccessUserId() != null) {
					AccessUser user = getAccessUserById(contact.getAccessUserId());
					if (user != null) {
						SalesRep salesRep = getSalesRepById(user.getSalesRepId());
						if (salesRep != null) {
							customer.setSalesRepId(salesRep.getId());
						}
					}
				}
				// update customer (contactId, accountId, RegisteredBy, Trackcode, SalesRep)
				// if Customer has lets' say 10 contact on CRM then we update Customer base on last Contact pulling from This crmContactList
				this.customerDao.updateCrmIds(customer);
				// assigne userId to CRN contact
				this.crmDao.convertCrmContactToCustomer(contact.getId(), customer.getId(), customer.getRegisteredBy());

				// convert only one contact to customer with same customer id
				break;
			}
		}
	}

	public void deleteCustomer(Customer customer) {
		this.customerDao.deleteCustomer(customer);
	}

	public void nonTransactionSafeLoginStats(Customer customer) {
		this.customerDao.nonTransactionSafeUpdateLoginStats(customer);
	}

	public Customer getCustomerOrderReportByUserID(Integer userId) {
		return this.customerDao.getCustomerOrderReportByUserID(userId);
	}

	public void importCustomers(List<Customer> customerList, boolean insertCrmAccountContact) {
		for (Customer customer : customerList) {
			if (customer.getId() == null) {
				// new customer
				customer.setImported(true);
				insertCustomer(customer, null, insertCrmAccountContact);
			} else {
				// old customer
				if (customer.getPassword() != null) {
					// reset password
					updateCustomer(customer, false, true);
				} else {
					updateCustomer(customer, false, false);
				}
			}

			// CRM synch
			if (insertCrmAccountContact) {
				List<CrmContact> crmContactList = this.getCrmContactByEmail1(customer.getUsername());
				if (crmContactList != null && !crmContactList.isEmpty()) {
					// update only first contact
					CrmContact crmContact = crmContactList.get(0);
					try {
						this.customerToCrmContactSynch(crmContact, customer);
						this.updateCrmContact(crmContact);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public List<Customer> getCustomerExportList(CustomerSearch search) {
		return this.customerDao.getCustomerExportList(search);
	}

	public int customerCount(CustomerSearch search) {
		return customerDao.customerCount(search);
	}

	public List<Address> getAddressListByUserid(Integer userId) {
		return this.customerDao.getAddressListByUserid(userId);
	}
	
	public List<Address> getAllAddressList(Integer userId) {
		return this.customerDao.getAllAddressList(userId);
	}

	public Address getAddressById(Integer addressId) {
		return this.customerDao.getAddressById(addressId);
	}

	public Address getDefaultAddressByUserid(Integer userId) {
		return this.customerDao.getDefaultAddressByUserid(userId);
	}

	public Address getDefaultShippingAddressByUserid(int userId) {
		return this.customerDao.getDefaultShippingAddressByUserid(userId);
	}

	public List<CustomerField> getCustomerFields() {
		return this.customerDao.getCustomerFields();
	}
	
	public List<DialingNote> getDialingNoteHistoryById(int id, boolean isCustomer){
		return this.customerDao.getDialingNoteHistoryById(id, isCustomer);
	}
	
	public void updateDialingNotesCrmToCustomer(Integer crmId, Integer customerId){
		this.customerDao.updateDialingNotesCrmToCustomer(crmId, customerId);
	}

	public void updateCustomerFields(List<CustomerField> customerFields) {
		this.customerDao.updateCustomerFields(customerFields);
	}

	public void updateCustomerNote(Customer customer) {
		this.customerDao.updateCustomerNote(customer);
	}

	public void updateCredit(CustomerCreditHistory credit) {
		this.customerDao.updateCredit(credit);
	}

	public void updatePointsToCustomer(int customerId, double loyaltyPoints) {
		this.customerDao.updatePointsToCustomer(customerId, loyaltyPoints);
	}

	public Integer getCustomerIdBySupplierPrefix(String prefix) {
		return this.customerDao.getCustomerIdBySupplierPrefix(prefix);
	}

	public int productCountByCustomerSupplierId(int supplierId) {
		return this.customerDao.productCountByCustomerSupplierId(supplierId);
	}

	public boolean isExistingCustomer(String accountNum) {
		return this.customerDao.isExistingCustomer(accountNum);
	}

	public List<Customer> getCustomerList(Category category, HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		CustomerSearch search = new CustomerSearch();
		search.setCategory(category.getId());
		search.setSetType(category.getSetType());

		// get category ids
		if (siteConfig.get("CATEGORY_INTERSECTION").getValue().equals("true")) {
			List<Integer> categoryIdsList = new ArrayList<Integer>();
			if (category.getCategoryIds() != null) {
				String[] categoryIds = category.getCategoryIds().split(",");
				for (int x = 0; x < categoryIds.length; x++) {
					if (!("".equals(categoryIds[x].trim()))) {
						categoryIdsList.add(Integer.valueOf(categoryIds[x].trim()));
					}
				}
				if (!categoryIdsList.isEmpty()) {
					search.setCategoryIds(categoryIdsList);
				}
			}
		}

		return this.customerDao.getCustomerList(search);
	}

	public void updateCategoryIdsByUser(int userId, Set<Object> categoryIds) {
		this.customerDao.updateCategoryIds(userId, categoryIds);
	}

	public Set<Integer> getCategoryIdsByUser(int userId) {
		return this.customerDao.getCategoryIds(userId);
	}

	public void updateUserDescription(Customer customer) {
		this.customerDao.updateUserDescription(customer);
	}

	public List<Customer> getCustomersAjax(CustomerSearch search) {
		return this.customerDao.getCustomersAjax(search);
	}

	public List<CustomerGroup> getCustomerGroupList(CustomerGroupSearch search) {
		return this.customerDao.getCustomerGroupList(search);
	}

	public List<CustomerGroup> getCustomerGroupNameList() {
		return this.customerDao.getCustomerGroupNameList();
	}

	public void insertCustomerGroup(CustomerGroup group) throws DataAccessException {
		this.customerDao.insertCustomerGroup(group);
	}

	public void updateCustomerGroup(CustomerGroup group) {
		this.customerDao.updateCustomerGroup(group);
	}

	public CustomerGroup getCustomerGroupById(Integer groupId) {
		return this.customerDao.getCustomerGroupById(groupId);
	}

	public List<Integer> getCustomerGroupIdList(Integer customerId) {
		return this.customerDao.getCustomerGroupIdList(customerId);
	}
	
	public List<Integer> getInactiveSalesRepIdList() {
		return this.salesRepDao.getInactiveSalesRepIdList();
	}

	public List<Integer> getCustomerIdByGroupId(Integer groupId) {
		return this.customerDao.getCustomerIdByGroupId(groupId);
	}

	public boolean isCustomerInGroup(Integer customerId, Set groupIds, String groupType) {

		List<Integer> groupIdList = new ArrayList<Integer>();
		ArrayList<Integer> gIdList = new ArrayList<Integer>();
		Boolean gIdMatch = false;
		Boolean temp = true;

		groupIdList = this.getCustomerGroupIdList(customerId);

		Iterator itr = groupIds.iterator();
		while (itr.hasNext()) {
			gIdList.add(Integer.parseInt(itr.next().toString()));
		}

		Iterator<Integer> gIter = groupIdList.iterator();
		while (gIter.hasNext()) {
			Integer number = gIter.next();
			if (gIdList.contains(number) && groupType.equalsIgnoreCase("include")) {
				gIdMatch = true;
			}
			if (!gIdList.contains(number) && groupType.equalsIgnoreCase("exclude")) {
				gIdMatch = temp && true;
			} else if (gIdList.contains(number) && groupType.equalsIgnoreCase("exclude")) {
				temp = false;
			}
		}
		if (groupIdList == null || groupIdList.size() == 0) {
			if (groupType.equalsIgnoreCase("exclude")) {
				gIdMatch = true;
			}
		}
		if (gIdList == null || gIdList.size() == 0) {
			gIdMatch = true;
		}
		if ((groupIdList == null || groupIdList.size() == 0) && (gIdList == null || gIdList.size() == 0)) {
			gIdMatch = true;
		}

		return gIdMatch;
	}

	public void deleteCustomerGroupById(Integer groupId) {
		this.customerDao.deleteCustomerGroupById(groupId);
	}

	public void batchGroupIds(Integer customerId, Set groupIds, String action) {
		this.customerDao.batchGroupIds(customerId, groupIds, action);
	}

	public boolean isGroupCustomerExist(Integer groupId) {
		return this.customerDao.isGroupCustomerExist(groupId);
	}

	public void updateSuspension(Integer customerId, String action) {
		this.customerDao.updateSuspension(customerId, action);
	}
	
	public void updateQualifier(Integer customerId, String action) {
		this.customerDao.updateQualifier(customerId, action);
	}
	
	public void updateTrackCode(Integer customerId, String trackCode) {
		this.customerDao.updateTrackCode(customerId, trackCode);
	}
	
	public void updateNewInformation(Integer customerId, Integer action) {
		this.customerDao.updateNewInformation(customerId, action);
	}
	
	public void updateCrmTrackCode(Integer customerId, String trackCode) {
		this.crmDao.updateCrmTrackCode(customerId, trackCode);
	}
	
	public void updateCrmQualifier(Integer customerId, String qualifier) {
		this.crmDao.updateCrmQualifier(customerId, qualifier);
	}
	
	public void updateMainSource(Integer customerId, String mainSource) {
		this.customerDao.updateMainSource(customerId, mainSource);
	}
	
	public void updatePaid(Integer customerId, String paid) {
		this.customerDao.updatePaid(customerId, paid);
	}
	
	public void updateMedium(Integer customerId, String medium) {
		this.customerDao.updateMedium(customerId, medium);
	}
	
	public void updateLanguageField(Integer customerId, String languageField) {
		this.customerDao.updateLanguageField(customerId, languageField);
	}

	public Address getAddress(int userId, String addressCode) {
		return this.customerDao.getAddress(userId, addressCode);
	}
	
	public Integer getCustomerIdByUsername(String username) {
		return this.customerDao.getCustomerIdByUsername(username);
	}

	public List<Payment> getCustomerPaymentList(PaymentSearch search) {
		return this.paymentDao.getCustomerPaymentList(search);
	}

	public List<Payment> getCustomerPaymentListByOrder(PaymentSearch search) {
		return this.paymentDao.getCustomerPaymentListByOrder(search);
	}

	public List<Payment> getPaymentExportList(PaymentSearch search) {
		return this.paymentDao.getPaymentExportList(search);
	}

	public void updatePaymentExported(Set<Integer> paymentIds) {
		this.paymentDao.updatePaymentExported(paymentIds);
	}

	public Payment getCustomerPaymentById(int id) {
		return this.paymentDao.getCustomerPaymentById(id);
	}

	public void updateCustomerPayment(Payment payment, Collection<Order> invoices) {
		this.paymentDao.updateCustomerPayment(payment, invoices);
	}

	public void insertCustomerPayment(Payment payment, Collection<Order> invoices) {
		this.paymentDao.insertCustomerPayment(payment, invoices);
	}
	
	public void insertOrderPayment(Payment payment, Collection<Order> invoices) {
		this.paymentDao.insertOrderPayment(payment, invoices);
	}
	
	public void insertPayments(Payment payment) {
		this.paymentDao.insertPayments(payment);
	}

	public void insertCustomerPayment(Payment payment) {
		this.paymentDao.insertCustomerPayment(payment);
	}

	public Map<Integer, Order> getInvoices(PaymentSearch search, String searchType) {
		return this.paymentDao.getInvoices(search, searchType);
	}

	public Integer getFirstPaymentId(Integer order_id) {
		return this.paymentDao.getFirstPaymentId(order_id);
	}

	public void updateCrmIds(Customer customer) {
		this.customerDao.updateCrmIds(customer);
	}

	public Double getPaymentAmount(Integer paymentId, Integer orderId) {
		return this.paymentDao.getPaymentAmount(paymentId, orderId);
	}

	// Service
	public List<ServiceableItem> getServiceableItemList(ServiceableItemSearch search) {
		return this.serviceDao.getServiceableItemList(search);
	}

	public ServiceableItem getServiceableItemById(int id) {
		return this.serviceDao.getServiceableItemById(id);
	}

	public void updateServiceableItem(ServiceableItem item) {
		this.serviceDao.updateServiceableItem(item);
	}

	public void insertServiceableItem(ServiceableItem item) {
		this.serviceDao.insertServiceableItem(item);
	}

	public void deleteServiceableItem(ServiceableItem item) {
		this.serviceDao.deleteServiceableItem(item);
	}

	public List<ServiceWork> getServiceList(ServiceSearch search) {
		return this.serviceDao.getServiceList(search);
	}

	public ServiceWork getService(int serviceNum) {
		return this.serviceDao.getService(serviceNum);
	}

	public void updateService(ServiceWork service) {
		this.serviceDao.updateService(service);
	}

	public Integer getServiceableItemId(String itemId, String serialNum) {
		return this.serviceDao.getServiceableItemId(itemId, serialNum);
	}

	public void insertService(ServiceWork service) {
		this.serviceDao.insertService(service);
	}

	public WorkOrder getWorkOrder(int serviceNum) {
		return this.serviceDao.getWorkOrder(serviceNum);
	}

	public void updateWorkOrder(WorkOrder workOrder) {
		this.serviceDao.updateWorkOrder(workOrder);
	}

	public ServiceWork getLastService(ServiceableItem item) {
		return this.serviceDao.getLastService(item);
	}

	public List<WorkOrder> getServiceHistory(ServiceableItem item) {
		return this.serviceDao.getServiceHistory(item);
	}

	public int getServiceCount(ServiceSearch search, String column) {
		return this.serviceDao.getServiceCount(search, column);
	}

	public int updateServiceableItems(final List<ServiceableItem> items) {
		return this.serviceDao.updateServiceableItems(items);
	}

	public void updateWorkOrderPrinted(int serviceNum) {
		this.serviceDao.updateWorkOrderPrinted(serviceNum);
	}

	public void updatePdfService(ServiceWork service, String type) {
		this.serviceDao.updatePdfService(service, type);
	}

	public Map<Integer, ServiceWork> getServiceMap() {
		return this.serviceDao.getServiceMap();
	}

	public void getServiceHistory(ServiceableItem item, Map<String, Object> model, WebJaguarFacade webJaguar, Map<String, Object> gSiteConfig) {
		model.put("item", item);

		// get product
		Product product = getProductById(getProductIdBySku(item.getSku()), 0, false, null);
		if (product == null) {
			product = new Product();
		}
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		for (ProductField productField : getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"))) {
			if (productField.isEnabled() && productField.isServiceField()) {
				try {
					m = c.getMethod("getField" + productField.getId());
					productField.setValue((String) m.invoke(product, arglist));
				} catch (Exception e) {
					// do nothing
				}
				product.addProductField(productField);
			}
		}
		model.put("product", product);

		List<WorkOrder> workOrders = getServiceHistory(item);
		model.put("workOrders", workOrders);
		// cpp
		Map<String, Map<String, Object>> cpp = new HashMap<String, Map<String, Object>>();
		Integer bwStartCount = null, colorStartCount = null;
		Integer bwEndCount = null, colorEndCount = null;
		Double tco = 0.0;

		for (WorkOrder wo : workOrders) {
			if (wo.getService().getStatus().equals("Completed")) {
				// B&W
				if (bwEndCount == null)
					bwEndCount = wo.getBwCount();
				bwStartCount = wo.getBwCount();
				// Color
				if (colorEndCount == null)
					colorEndCount = wo.getColorCount();
				colorStartCount = wo.getColorCount();
			}
			for (WorkOrderLineItem wol : wo.getLineItems()) {
				if (wol.getInSN() != null && !wol.getInSN().equals("")) {
					Map<String, Object> map = (Map<String, Object>) cpp.get(wol.getInSN());
					if (map == null) {
						map = new HashMap<String, Object>();
					}
					map.put("inBwCount", wo.getBwCount());
					map.put("price", wol.getUnitPrice());
					cpp.put(wol.getInSN(), map);
				}
				if (wol.getOutSN() != null && !wol.getOutSN().equals("")) {
					Map<String, Object> map = (Map<String, Object>) cpp.get(wol.getOutSN());
					if (map == null) {
						map = new HashMap<String, Object>();
					}
					map.put("outBwCount", wo.getBwCount());
					cpp.put(wol.getOutSN(), map);
				}
				// tco
				if (wo.getService().getStatus().equals("Completed")) {
					if (wol.getTotalPrice() != null) {
						tco += wol.getTotalPrice();
					}
				}
			}
			for (WorkOrderLineItem wol : wo.getLineItems()) {
				Map<String, Object> map = cpp.get(wol.getInSN());
				if (map != null && map.containsKey("outBwCount")) {
					wol.setBwEndCount((Integer) map.get("outBwCount"));
				}
				if (wol.getBwEndCount() != null && wo.getBwCount() != null) {
					wol.setBwTotalCount(wol.getBwEndCount() - wo.getBwCount());
				}
			}
		}
		if (bwStartCount != null && bwEndCount != null) {
			int bwTotal = bwEndCount - bwStartCount;
			if (bwTotal > 0) {
				model.put("bwTotal", bwTotal);
			}
		}
		if (colorStartCount != null && colorEndCount != null) {
			int colorTotal = colorEndCount - colorStartCount;
			if (colorTotal > 0) {
				model.put("colorTotal", colorTotal);
			}
		}
		model.put("tco", tco);
		model.put("countries", getCountryMap());
	}

	public List<WorkOrder> getIncompleteWorkOrders(String customerField, int defaultInterval) {
		return this.serviceDao.getIncompleteWorkOrders(customerField, defaultInterval);
	}

	// Data Feed
	public List<DataFeed> getDataFeedList(String feed) {
		return this.dataFeedDao.getDataFeedList(feed);
	}

	public List<Product> getProductDataFeedList(DataFeedSearch search) {
		return this.dataFeedDao.getProductDataFeedList(search);
	}

	public List<Product> getWebjaguarDataFeedProductList(DataFeedSearch search, int numImages, String imagePath, boolean includeCategories) {
		return this.productDao.getWebjaguarDataFeedProductList(search, numImages, imagePath, includeCategories);
	}

	public void updateDataFeed(List<DataFeed> list, Map<String, Object> config) {
		this.dataFeedDao.updateDataFeed(list, config);
	}

	public void updateDataFeedList(List<DataFeed> list, List<WebjaguarDataFeed> configList) {
		this.dataFeedDao.updateDataFeedList(list, configList);
	}

	public Map<String, Object> getDataFeedConfig(String feed) {
		return this.dataFeedDao.getDataFeedConfig(feed);
	}

	public int productDataFeedListCount(DataFeedSearch search) {
		return this.dataFeedDao.productDataFeedListCount(search);
	}

	public int webjaguarDataFeedProductListCount(DataFeedSearch search) {
		return this.dataFeedDao.webjaguarDataFeedProductListCount(search);
	}

	public void nonTransactionSafeUpdateWebjaguarCategories(List<WebjaguarCategory> categories) {
		this.dataFeedDao.nonTransactionSafeUpdateWebjaguarCategories(categories);
	}

	public List<WebjaguarCategory> getWebjguarCategories() {
		return this.dataFeedDao.getWebjaguarCategories();
	}

	public Map<String, WebjaguarCategory> getWebjaguarCategoryMap() {
		return this.dataFeedDao.getWebjaguarCategoryMap();
	}

	public void markAndInactiveOldProducts(String feed, Integer dataFeedId, boolean endDocument) {
		this.productDao.markAndInactiveOldProducts(feed, dataFeedId, endDocument);
	}

	public Integer getWebjaguarDataFeedListCount(DataFeedSearch search) {
		return this.dataFeedDao.getWebjaguarDataFeedListCount(search);
	}

	public List<WebjaguarDataFeed> getWebjaguarDataFeedList(DataFeedSearch search) {
		return this.dataFeedDao.getWebjaguarDataFeedList(search);
	}

	public WebjaguarDataFeed getWebjaguarDataFeed(Integer id, String token, String feedType) {
		return this.dataFeedDao.getWebjaguarDataFeed(id, token, feedType);
	}

	public void insertWebjaguarDataFeed(WebjaguarDataFeed dataFeed) {
		this.dataFeedDao.insertWebjaguarDataFeed(dataFeed);
	}

	public void updateWebjaguarDataFeed(WebjaguarDataFeed dataFeed) {
		this.dataFeedDao.updateWebjaguarDataFeed(dataFeed);
	}

	public void deleteWebjaguarDataFeed(Integer id) {
		this.dataFeedDao.deleteWebjaguarDataFeed(id);
	}

	// CSV
	public Map<String, CsvFeed> getCsvFeedMapping(String feed) {
		return this.configDao.getCsvFeedMapping(feed);
	}

	public List<CsvFeed> getCsvFeed(String feed) {
		return this.configDao.getCsvFeed(feed);
	}
	
	public Map<String, CsvData> getCsvFeedMap(String feed) {
		return this.configDao.getCsvFeedMap(feed);
	}

	public int[] updateProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data, Set<String> deletedSkus) {
		if (deletedSkus != null) {
			for (String sku : deletedSkus) {
				this.productDao.deleteProductBySku(sku);
			}
		}
		return this.productDao.updateProduct(csvFeedList, data);
	}

	public void updateProductImage(Map<Long, List<String>> data) {
		this.productDao.updateProductImage(data);
	}

	public void nonTransactionSafeUpdateFreeShipping(String option_code, String field) {
		this.productDao.updateFreeShipping(option_code, field);
	}

	// Concord
	public void updateProductCategory(Map<Long, List<Long>> data) {
		this.productDao.updateProductCategory(data);
	}

	public void updateConcordProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		Map<String, Long> productMap = getProductSkuMap();
		boolean getNewMapping = false;
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (!productMap.containsKey(sku.toLowerCase())) {
				// add new skus
				try {
					getNewMapping = true;
					insertProduct(new Product(sku, "Concord Feed"), null);
					productMap.put(sku.toLowerCase(), null);
				} catch (Exception e) {
					// do nothing. happens with duplicate skus
				}
			}
		}

		// images
		Map<Long, List<String>> imageData = new HashMap<Long, List<String>>();
		if (getNewMapping) {
			productMap = getProductSkuMap(); // get new mapping
		}
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (productMap.containsKey(sku.toLowerCase())) {
				// valid item
				Long productId = productMap.get(sku.toLowerCase());
				List<String> images = new ArrayList<String>();

				if (map.get("image") != null) {
					images.add((String) map.get("image"));
				}
				imageData.put(productId, images);
			}
		}
		this.productDao.updateProductImage(imageData);

		this.productDao.updateProduct(csvFeedList, data);

		// delete old concord items
		this.productDao.deleteOldProducts("CONCORD", false);
	}

	public Map<String, Map<String, String>> getCategoryExternalIdNewEggMap() {
		return this.categoryDao.getCategoryExternalIdNewEggMap();
	}

	// Evergreen
	// public void updateEvergreenProduct(final List <CsvFeed> csvFeedList, final List<Map <String, Object>> data) {
	// Map <String, Long> productMap = getProductSkuMap();
	// boolean getNewMapping = false;
	// for (Map<String, Object> map: data) {
	// String sku = map.get("sku").toString();
	// if (!productMap.containsKey(sku.toLowerCase())) {
	// // add new skus
	// try {
	// getNewMapping = true;
	// insertProduct(new Product(sku), null);
	// productMap.put(sku.toLowerCase(), null);
	// } catch (Exception e) {
	// // do nothing. happens with duplicate skus
	// }
	// }
	// }
	//
	// if (data.get(0).containsKey("images")) {
	// // images
	// Map <Long, List<String>> imageData = new HashMap<Long, List<String>>();
	// if (getNewMapping) {
	// productMap = getProductSkuMap(); // get new mapping
	// getNewMapping = false;
	// }
	// for (Map<String, Object> map: data) {
	// String sku = map.get("sku").toString();
	// if (productMap.containsKey(sku.toLowerCase())) {
	// // valid item
	// Long productId = productMap.get(sku.toLowerCase());
	// List<String> images = new ArrayList<String>();
	//
	// String[] image = ((String) map.get("images")).split(",");
	// for (int i=0; i<image.length; i++) {
	// if (image[i].trim().length() > 0) {
	// images.add(image[i]);
	// }
	// }
	// imageData.put(productId, images);
	// }
	// }
	// this.productDao.updateProductImage(imageData);
	// }
	//
	// if (data.get(0).containsKey("categories")) {
	// // categories
	// Map <Long, List<Long>> categoryData = new HashMap<Long, List<Long>>();
	// if (getNewMapping) {
	// productMap = getProductSkuMap(); // get new mapping
	// getNewMapping = false;
	// }
	// for (Map<String, Object> map: data) {
	// String sku = map.get("sku").toString();
	// if (productMap.containsKey(sku.toLowerCase())) {
	// // valid item
	// Long productId = productMap.get(sku.toLowerCase());
	// List<Long> categoryIds = new ArrayList<Long>();
	//
	// String[] categoryId = ((String) map.get("categories")).split(",");
	// for (int i=0; i<categoryId.length; i++) {
	// if (categoryId[i].trim().length() > 0) {
	// try {
	// categoryIds.add(Long.parseLong(categoryId[i]));
	// } catch (NumberFormatException e) {
	// // do nothing
	// }
	// }
	// }
	// categoryData.put(productId, categoryIds);
	// }
	// }
	// updateProductCategory(categoryData);
	// }
	//
	// this.productDao.updateProduct(csvFeedList, data);
	// }

	public void updateEvergreenOrderSuc(int orderId, String suc) {
		this.orderDao.updateEvergreenOrderSuc(orderId, suc);
	}

	public void updateEvergreenCustomerSuc(int customerId, String suc) {
		this.customerDao.updateEvergreenCustomerSuc(customerId, suc);
	}

	public String getEvergreenOrderSuc(int orderId) {
		return this.orderDao.getEvergreenOrderSuc(orderId);
	}

	public void updateEvergreenSalesRep(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data, Map<String, String> customerSalesRepMap) {
		Map<String, Long> salesRepMap = salesRepDao.getSaleRepAccountNumMap();
		for (Map<String, Object> map : data) {
			String sales_rep_account_num = map.get("sales_rep_account_num").toString();
			if (!salesRepMap.containsKey(sales_rep_account_num.toLowerCase())) {
				// add new sales rep
				insertSalesRep(new SalesRep(sales_rep_account_num));
				salesRepMap.put(sales_rep_account_num.toLowerCase(), null);
			}
		}

		this.salesRepDao.updateSalesRep(csvFeedList, data);

		// customer --> sales rep mapping
		if (customerSalesRepMap.size() > 0) {
			List<Map<String, Object>> customerSalesRepData = new ArrayList<Map<String, Object>>();
			salesRepMap = salesRepDao.getSaleRepAccountNumMap();
			for (String account_number : customerSalesRepMap.keySet()) {
				HashMap<String, Object> map = new HashMap<String, Object>();
				map.put("account_number", account_number);
				map.put("sales_rep_id", salesRepMap.get(customerSalesRepMap.get(account_number)));
				customerSalesRepData.add(map);
				if (customerSalesRepData.size() == 5000) {
					this.salesRepDao.updateCustomerSalesRepAssoc(customerSalesRepData);
					customerSalesRepData.clear();
					map.clear();
				}
			}
			this.salesRepDao.updateCustomerSalesRepAssoc(customerSalesRepData);
		}

		this.salesRepDao.deleteOldSalesReps();
	}
	
	

	// Fragrancenet
	public void updateFragrancenetProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		Map<String, Long> productMap = getProductSkuMap();
		boolean getNewMapping = false;
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (!productMap.containsKey(sku.toLowerCase())) {
				// add new skus
				try {
					getNewMapping = true;
					insertProduct(new Product(sku, "Fragrancenet Feed"), null);
					productMap.put(sku.toLowerCase(), null);
				} catch (Exception e) {
					// do nothing. happens with duplicate skus
				}
			}
		}

		if (data.get(0).containsKey("categories")) {
			// categories
			Map<String, List<Long>> categoryData = new HashMap<String, List<Long>>();
			for (Map<String, Object> map : data) {
				String sku = map.get("sku").toString();
				List<Long> categoryIds = new ArrayList<Long>();
				String[] categoryId = ((String) map.get("categories")).split(",");
				for (int i = 0; i < categoryId.length; i++) {
					if (categoryId[i].trim().length() > 0) {
						categoryIds.add(Long.parseLong(categoryId[i]));
					}
				}
				if (categoryIds.size() > 0) {
					categoryData.put(sku, categoryIds);
				}
			}
			insertProductCategory(categoryData);
		}

		// images
		Map<Long, List<String>> imageData = new HashMap<Long, List<String>>();
		if (getNewMapping) {
			productMap = getProductSkuMap(); // get new mapping
		}
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (productMap.containsKey(sku.toLowerCase())) {
				// valid item
				Long productId = productMap.get(sku.toLowerCase());
				List<String> images = new ArrayList<String>();

				if (map.get("image") != null) {
					images.add((String) map.get("image"));
				}
				imageData.put(productId, images);
			}
		}
		this.productDao.updateProductImage(imageData);

		this.productDao.updateProduct(csvFeedList, data);

		// make old fragrancenet items inactive
		this.productDao.deleteOldProducts("FRAGRANCENET", true);
	}

	// Jgoodin
	public void updateJgoodinProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		Map<String, Long> productMap = getProductSkuMap();
		boolean getNewMapping = false;
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (!productMap.containsKey(sku.toLowerCase())) {
				// add new skus
				try {
					getNewMapping = true;
					insertProduct(new Product(sku, "Jgoodin Feed"), null);
					productMap.put(sku.toLowerCase(), null);
				} catch (Exception e) {
					// do nothing. happens with duplicate skus
				}
			}
		}

		// images
		Map<Long, List<String>> imageData = new HashMap<Long, List<String>>();
		if (getNewMapping) {
			productMap = getProductSkuMap(); // get new mapping
		}
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (productMap.containsKey(sku.toLowerCase())) {
				// valid item
				Long productId = productMap.get(sku.toLowerCase());
				List<String> images = new ArrayList<String>();
				for (int i = 1; i <= 5; i++) {
					if (map.get("image_" + i) != null) {
						images.add((String) map.get("image_" + i));
					}
				}
				imageData.put(productId, images);
			}
		}
		this.productDao.updateProductImage(imageData);

		this.productDao.updateProduct(csvFeedList, data);

	}

	public void insertProductCategory(Map<String, List<Long>> data) {
		this.productDao.insertProductCategory(data);
	}

	// number1Appliance
	public void updateNum1ApplainceProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		Map<String, Long> productMap = getProductSkuMap();
		boolean getNewMapping = false;
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (!productMap.containsKey(sku.toLowerCase())) {
				// add new skus
				try {
					getNewMapping = true;
					Product product = new Product(sku, "Applience Feed");
					product.setActive(false);
					product.setCompare(true);
					product.setHideLeftBar(true);
					insertProduct(product, null);
					productMap.put(sku.toLowerCase(), null);
				} catch (Exception e) {
					// do nothing. happens with duplicate skus
				}
			}
		}

		// images
		Map<Long, List<String>> imageData = new HashMap<Long, List<String>>();
		if (getNewMapping) {
			productMap = getProductSkuMap(); // get new mapping
		}
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (productMap.containsKey(sku.toLowerCase())) {
				// valid item
				Long productId = productMap.get(sku.toLowerCase());
				List<String> images = new ArrayList<String>();

				for (int index = 1; index <= 10; index++) {
					if (map.get("image" + index) != null) {
						images.add((String) map.get("image" + index));
					}
				}
				imageData.put(productId, images);
			}
		}
		this.productDao.updateProductImage(imageData);

		this.productDao.updateProduct(csvFeedList, data);
	}

	// Budget
	public List<Brand> getBrands(Integer userId) {
		return this.budgetDao.getBrands(userId);
	}

	public Double getOrderTotalByBrand(Brand brand, int userId, int year) {
		return this.budgetDao.getOrderTotalByBrand(brand, userId, year);
	}

	public void updateBudget(int userId, List<Brand> brands) {
		this.budgetDao.updateBudget(userId, brands);
	}

	public boolean getSubTotalPerBrand(Cart cart, Map<Brand, Double> brandsMap) {
		// returns true if any brand is over quota
		boolean overQuota = false;

		// get budget by brands
		List<Brand> brands = getBrands(cart.getUserId());
		nextItem: for (CartItem cartItem : cart.getCartItems()) {
			String sku = cartItem.getProduct().getSku();
			for (Brand brand : brands) {
				if (sku != null && sku.toLowerCase().startsWith(brand.getSkuPrefix().toLowerCase())) {
					if (brand.getOptionName() == null) {
						cartItem.setBrand(brand);
						if (brandsMap.get(brand) == null) {
							brandsMap.put(brand, 0.0);
						}
						brandsMap.put(brand, brandsMap.get(brand) + cartItem.getTotal());
						continue nextItem;
					} else if (cartItem.getProductAttributes() != null) {
						for (ProductAttribute pa : cartItem.getProductAttributes()) {
							if (pa.getOptionName().equalsIgnoreCase(brand.getOptionName()) && pa.getValueName().equalsIgnoreCase(brand.getValueName())) {
								cartItem.setBrand(brand);
								if (brandsMap.get(brand) == null) {
									brandsMap.put(brand, 0.0);
								}
								brandsMap.put(brand, brandsMap.get(brand) + cartItem.getTotal());
								continue nextItem;
							}
						}
					}
					cartItem.setBrand(null); // reset brand to null if not a match (possible when options changes)
				}
			}
		}
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		for (Brand brand : brandsMap.keySet()) {
			Double total = getOrderTotalByBrand(brand, cart.getUserId(), currentYear);
			if (total == null) {
				brand.setTotal(0.0);
			} else {
				brand.setTotal(Utilities.roundFactory(total, 2, BigDecimal.ROUND_HALF_UP));
			}
			if (brand.getBalance() < brandsMap.get(brand)) {
				overQuota = true;
			}
		}
		return overQuota;
	}

	public List<Product> getProductExportByBrands(String protectedAccess) {
		return this.budgetDao.getProductExportByBrands(protectedAccess);
	}

	public List<Product> getProductListByBrand(Brand brand) {
		return this.budgetDao.getProductListByBrand(brand);
	}

	public Map<String, Object> getOrderReportByBrand(Brand brand) {
		return this.budgetDao.getOrderReportByBrand(brand);
	}

	public Map<String, Brand> getBrandMap() {
		Map<String, Brand> brandMap = new TreeMap<String, Brand>();
		for (Brand brand : this.budgetDao.getBrands(null)) {
			brandMap.put(brand.getSkuPrefix(), brand);
		}
		return brandMap;
	}

	public List<Map<String, Object>> getOrderReportByBrand(Brand brand, String sku) {
		return this.budgetDao.getOrderReportByBrand(brand, sku);
	}

	public boolean getBudgetByCart(Cart cart, Map<String, BudgetProduct> budgetProduct) {
		// returns true if any product is over quota
		boolean overQuota = false;

		int currentYear = Calendar.getInstance().get(Calendar.YEAR);

		// get budget by product
		budgetProduct.putAll(this.budgetDao.getBudgetProduct(cart.getUserId(), currentYear, true));
		for (CartItem cartItem : cart.getCartItems()) {
			String sku = cartItem.getProduct().getSku();
			if (budgetProduct.containsKey(sku)) {
				if (budgetProduct.get(sku).addQty(cartItem.getQuantity())) {
					overQuota = true;
				}
			} else {
				budgetProduct.put(sku, new BudgetProduct(sku, cartItem.getQuantity()));
				overQuota = true;
			}
		}

		return overQuota;
	}

	public List<BudgetProduct> getBudgetProductList(BudgetProductSearch search) {
		return this.budgetDao.getBudgetProductList(search);
	}

	public BudgetProduct getBudgetProduct(int id) {
		return this.budgetDao.getBudgetProduct(id);
	}

	public void updateBudgetProduct(BudgetProduct budgetProduct) {
		this.budgetDao.updateBudgetProduct(budgetProduct);
	}

	public List<Integer> getBudgetProductYears(BudgetProductSearch search) {
		return this.budgetDao.getBudgetProductYears(search);
	}

	public void insertBudgetProduct(BudgetProduct budgetProduct) {
		this.budgetDao.insertBudgetProduct(budgetProduct);
	}

	public void deleteBudgetProduct(int id) {
		this.budgetDao.deleteBudgetProduct(id);
	}

	public Map<String, BudgetProduct> getBudgetProduct(int userId, List<Product> productList) {
		return this.budgetDao.getBudgetProduct(userId, Calendar.getInstance().get(Calendar.YEAR), productList);
	}

	// Lucene
	
	public List<Map<String, Object>> getLuceneProduct(int limit, int offset, boolean onlyParent, String parentSku, String[] searchFieldsList, boolean adminIndex) {
		return this.productDao.getLuceneProduct(limit, offset, onlyParent, parentSku, searchFieldsList, adminIndex);
	}

	public int getLuceneProductCount(boolean onlyParent) {
		return this.productDao.getLuceneProductCount(onlyParent);
	}

	// i18n
	public Map<String, Product> getI18nProduct(int id) {
		return this.productDao.getI18nProduct(id);
	}

	public List<ProductImage> getImages(Integer productId, int numImages) {
		return this.productDao.getImages(productId, numImages);
	}

	// Power Review
	public int[] updatePowerReview(final List<Map<String, Object>> data) {
		Iterator<Map<String, Object>> iter = data.iterator();
		while (iter.hasNext()) {
			Map<String, Object> map = iter.next();
			String sku = map.get("sku").toString();
			Integer productId = this.getProductIdBySku(sku);
			if (productId != null) {
				map.put("id", productId);
			} else {
				iter.remove();
			}
		}

		return this.productDao.updatePowerReview(data);
	}

	// Synchronize
	// public void updateSyncProduct(final List <CsvFeed> csvFeedList, final List<Map <String, Object>> data, String source) {
	// Map <String, Long> productMap = getProductSkuMap();
	// boolean getNewMapping = false;
	// for (Map<String, Object> map: data) {
	// String sku = map.get("sku").toString();
	// if (!productMap.containsKey(sku.toLowerCase())) {
	// // add new skus
	// try {
	// getNewMapping = true;
	// insertProduct(new Product(sku, ));
	// productMap.put(sku.toLowerCase(), null);
	// } catch (Exception e) {
	// // do nothing. happens with duplicate skus
	// }
	// }
	// }
	//
	// // images
	// Map <Long, List<String>> imageData = new HashMap<Long, List<String>>();
	// if (getNewMapping) {
	// productMap = getProductSkuMap(); // get new mapping
	// }
	// for (Map<String, Object> map: data) {
	// String sku = map.get("sku").toString();
	// if (productMap.containsKey(sku.toLowerCase())) {
	// // valid item
	// Long productId = productMap.get(sku.toLowerCase());
	// List<String> images = new ArrayList<String>();
	// for(int index=1; index <=10; index++ ) {
	// if (map.get("image"+index) != null) {
	// images.add((String) map.get("image"+index));
	// }
	// }
	// imageData.put(productId, images);
	// }
	// }
	// this.productDao.updateProductImage(imageData);
	//
	// this.productDao.updateProduct(csvFeedList, data);
	//
	// // delete old items
	// this.productDao.deleteOldProducts(source, false);
	// }

	// Synchronize ASI
	public void nonTransactionSafeUpdateASIProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		Map<String, Long> productMap = getProductSkuMap();
		boolean getNewMapping = false;
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();

			// Integer supplierId = this.getSupplierIdByAccountNumber(map.get("asi_id").toString());
			if (!productMap.containsKey(sku.toLowerCase())) {
				// add new skus
				try {
					getNewMapping = true;
					insertProduct(new Product(sku, "ASI Feed"), null);
					productMap.put(sku.toLowerCase(), null);
				} catch (Exception e) {
					// do nothing. happens with duplicate skus
				}
			}
		}

		// images
		Map<Long, List<String>> imageData = new HashMap<Long, List<String>>();
		if (getNewMapping) {
			productMap = getProductSkuMap(); // get new mapping
		}
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (productMap.containsKey(sku.toLowerCase())) {
				// valid item
				Long productId = productMap.get(sku.toLowerCase());
				List<String> images = new ArrayList<String>();
				for (int index = 1; index <= 10; index++) {
					if (map.get("image" + index) != null) {
						images.add((String) map.get("image" + index));
					}
				}
				imageData.put(productId, images);
			}
		}
		this.productDao.updateProductImage(imageData);

		this.productDao.updateProduct(csvFeedList, data);

		this.updateASIProductSupplier(data);
	}

	// Synchronize Webjaguar
	public void nonTransactionSafeUpdateWebjaguarProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		Map<String, Long> productMap = getProductSkuMap();
		boolean getNewMapping = false;
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (!productMap.containsKey(sku.toLowerCase())) {
				// add new skus
				try {
					getNewMapping = true;
					insertProduct(new Product(sku, "Feed"), null);
					productMap.put(sku.toLowerCase(), null);
				} catch (Exception e) {
					e.printStackTrace();
					// do nothing. happens with duplicate skus
				}
			}
		}

		// images
		Map<Long, List<String>> imageData = new HashMap<Long, List<String>>();
		if (getNewMapping) {
			productMap = getProductSkuMap(); // get new mapping
		}
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (productMap.containsKey(sku.toLowerCase())) {
				// valid item
				Long productId = productMap.get(sku.toLowerCase());
				List<String> images = new ArrayList<String>();
				for (int index = 1; index <= 10; index++) {
					if (map.get("image" + index) != null) {
						images.add((String) map.get("image" + index));
					}
				}
				imageData.put(productId, images);
			}
		}
		this.productDao.updateProductImage(imageData);

		this.productDao.updateWebjaguarProduct(csvFeedList, data);

		// will need to consider supplier later
		// this.updateASIFileFeedProductSupplier(data);
	}

	public void nonTransactionSafeInsertWebjaguarCategories(final List<WebjaguarCategory> categoryList) {
		this.productDao.insertWebjaguarCategory(categoryList);
	}

	// Synchronize Premier
	public void nonTransactionSafeUpdatePremierProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		Map<String, Long> productMap = getProductSkuMap();
		boolean getNewMapping = false;
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (!productMap.containsKey(sku.toLowerCase())) {
				// add new skus
				try {
					getNewMapping = true;
					insertProduct(new Product(sku, "Feed"), null);
					productMap.put(sku.toLowerCase(), null);
				} catch (Exception e) {
					e.printStackTrace();
					// do nothing. happens with duplicate skus
				}
			}
		}

		// images
		Map<Long, List<String>> imageData = new HashMap<Long, List<String>>();
		if (getNewMapping) {
			productMap = getProductSkuMap(); // get new mapping
		}
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			if (productMap.containsKey(sku.toLowerCase())) {
				// valid item
				Long productId = productMap.get(sku.toLowerCase());
				List<String> images = new ArrayList<String>();
				for (int index = 1; index <= 10; index++) {
					if (map.get("image" + index) != null && !map.get("image" + index).toString().trim().isEmpty()) {
						images.add((String) map.get("image" + index));
					}
				}
				imageData.put(productId, images);
			}
		}
		this.productDao.updateProductImage(imageData);
		this.productDao.updateWebjaguarProduct(csvFeedList, data);

		// will need to consider supplier later
	}

	// Synchronize cdsanddvds
	public void nonTransactionSafeUpdateCdsDvdsProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data, boolean insertNewProduct) {
		Map<String, Long> productMap = new HashMap<String, Long>();

		if (insertNewProduct) {
			productMap = getProductSkuMap();
			for (Map<String, Object> map : data) {
				String sku = map.get("sku").toString();
				if (!productMap.containsKey(sku.toLowerCase())) {
					// add new skus
					try {
						Integer id = this.productDao.insertProductWithSkuOnly(sku);
						productMap.put(sku.toLowerCase(), Long.parseLong(id.toString()));
					} catch (Exception e) {
						e.printStackTrace();
						// do nothing. happens with duplicate skus
					}
				}
			}

			// images and categories
			Map<Long, List<String>> imageData = new HashMap<Long, List<String>>();
			Map<Long, Integer> categoryData = new HashMap<Long, Integer>();

			for (Map<String, Object> map : data) {
				String sku = map.get("sku").toString();
				if (productMap.containsKey(sku.toLowerCase())) {
					// valid item
					Long productId = productMap.get(sku.toLowerCase());
					List<String> images = new ArrayList<String>();
					for (int index = 1; index <= 10; index++) {
						if (map.get("image" + index) != null) {
							images.add((String) map.get("image" + index));
						}
					}
					imageData.put(productId, images);
					if (map.get("categoryId") != null) {
						categoryData.put(productId, (Integer) map.get("categoryId"));
					}
				}
			}
			this.productDao.updateProductImage(imageData);
			this.productDao.updateCategoryData(categoryData);

		}

		this.productDao.updatedCdsAndDvdsProduct(csvFeedList, data);
	}

	// Synchronize ASI
	public void nonTransactionSafeUpdateASIFileFeedProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		Map<String, Long> productMap = getProductSkuMap();
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();

			if (!productMap.containsKey(sku.toLowerCase())) {
				// add new skus
				try {
					Product product = new Product(sku, "ASI Feed");
					insertProduct(product, null);
					productMap.put(sku.toLowerCase(), product.getId().longValue());
				} catch (Exception e) {
					// do nothing. happens with duplicate skus
				}
			}
		}

		// images
		Map<Long, List<String>> imageData = new HashMap<Long, List<String>>();
		String sku = null;
		Long productId = null;
		List<String> images = null;
		for (Map<String, Object> map : data) {
			sku = map.get("sku").toString();
			productId = productMap.get(sku.toLowerCase());

			if (productId != null) {
				images = new ArrayList<String>();
				for (int index = 1; index <= 10; index++) {
					if (map.get("image" + index) == null) {
						break;
					}
					images.add((String) map.get("image" + index));
				}
				imageData.put(productId, images);
			}
		}

		this.productDao.updateProductImage(imageData);
		this.productDao.updateASIProduct(csvFeedList, data);
		this.updateASIFileFeedProductSupplier(data);

		imageData.clear();
		productMap.clear();
	}

	// Synchronize ASI
	public void nonTransactionSafeInactiveASIFileFeedProduct(final List<String> data, String column) {
		this.productDao.inactiveProduct(data, column);
	}

	private void updateASIProductSupplier(final List<Map<String, Object>> data) {
		for (Map<String, Object> map : data) {
			String sku = map.get("sku").toString();
			Integer supplierId = this.getSupplierIdByAccountNumber(map.get("asi_id").toString());
			if (supplierId != null) {
				Supplier supplier = new Supplier();
				supplier.setSku(sku);
				supplier.setId(supplierId);
				if (map.get("cost1") != null) {
					String cost1 = map.get("cost1").toString();
					supplier.setPrice(Double.parseDouble(cost1));
				}
				insertProductSupplier(supplier);
			}
		}
	}

	// rename this methode later (not just for ASI)
	private void updateASIFileFeedProductSupplier(final List<Map<String, Object>> data) {
		for (Map<String, Object> map : data) {
			// Number
			String supplierSku = null;
			if (map.get("field_42") != null) {
				supplierSku = map.get("field_42").toString();
			}
			String productSku = map.get("sku").toString();
			Integer supplierId = this.getSupplierIdByAccountNumber(map.get("asi_id").toString());
			if (supplierId != null) {
				Supplier supplier = new Supplier();
				supplier.setSku(productSku);
				supplier.setId(supplierId);
				supplier.setSupplierSku(supplierSku);
				if (map.get("cost1") != null) {
					String cost1 = map.get("cost1").toString();
					supplier.setPrice(Double.parseDouble(cost1));
				}
				insertProductSupplier(supplier);
			}
		}
	}

	public void insertProductSupplier(final List<Map<String, Object>> data, Integer supplierId) {
		for (Map<String, Object> map : data) {
			// Number
			String supplierSku = (map.get("supplier_sku") != null) ? map.get("supplier_sku").toString() : null;
			String productSku = map.get("sku").toString();
			if (supplierId != null) {
				Supplier supplier = new Supplier();
				supplier.setSku(productSku);
				supplier.setId(supplierId);
				supplier.setSupplierSku(supplierSku);
				if (map.get("cost1") != null) {
					String cost1 = map.get("cost1").toString();
					supplier.setPrice(Double.parseDouble(cost1));
				}
				insertProductSupplier(supplier);
			}
		}
	}

	public void updateSyncSalesTag(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		this.promoDao.insertSalesTag(csvFeedList, data);
	}

	public boolean applyPriceDifference(asiProduct.Product product, com.webjaguar.thirdparty.asi.asiAPI.Product iProduct, String globalMarkup, Integer globalMarkupType, int globalMarkUpFormula) {

		List<Double> discountList = new ArrayList<Double>();
		int markupType = 0;
		int markUpFormula = 0;
		boolean isMarkupAvailable = false;

		// check if markup per supplier is available
		Class<Supplier> c2 = Supplier.class;
		Method m2 = null;
		Supplier supplier = null;
		if (product != null) {
			supplier = getSupplierByAccountNumber(product.getSupplier().getASI().toString());
		} else if (iProduct != null) {
			supplier = getSupplierByAccountNumber(iProduct.getSupplier().getAsiNumber().toString());
		}
		if (supplier != null) {
			for (int i = 1; i < 10; i++) {
				Double value = null;
				try {
					m2 = c2.getMethod("getMarkup" + i);
					value = (Double) m2.invoke(supplier, null);
				} catch (Exception e) {
				}

				if (value != null) {
					isMarkupAvailable = true;
				}
				discountList.add(value);
			}
			markupType = supplier.getMarkupType();
			markUpFormula = supplier.getMarkupFormula();
		}

		// if markup per supplier is not available , check for global markup
		if (!isMarkupAvailable) {
			if (globalMarkup != null && !globalMarkup.isEmpty()) {
				discountList = new ArrayList<Double>();
				for (String disc : globalMarkup.split(",")) {
					try {
						discountList.add(Double.parseDouble(disc));
						isMarkupAvailable = true;
					} catch (Exception e) {
					}
				}
			}
			markupType = globalMarkupType;
			markUpFormula = globalMarkUpFormula;

		}

		if (product != null && product.getPricing() != null && product.getPricing().getPriceGrid() != null && isMarkupAvailable && discountList != null && !discountList.isEmpty()) {
			for (PriceGrid priceGrid : product.getPricing().getPriceGrid()) {
				// apply margin only on base price and with no QUR.
				if (priceGrid.isBase() && !priceGrid.getId().equalsIgnoreCase("pricegrid-99")) {
					Double endQtyPrice = null;
					if (markupType == 1) {
						endQtyPrice = this.getEndQtyPriceOrCost(priceGrid, null, true);
					} else if (markupType == 3) {
						endQtyPrice = this.getEndQtyPriceOrCost(priceGrid, null, false);
					}
					for (int i = 0; i < (discountList.size() < priceGrid.getPrice().size() ? discountList.size() : priceGrid.getPrice().size()); i++) {
						if (discountList.get(i) != null) {
							BigDecimal newPrice = null;
							switch (markupType) {
							case 1:
								// Based on End Qty Price
								if (markUpFormula == 0) {
									newPrice = new BigDecimal(endQtyPrice * (100 + discountList.get(i)) / 100);
								} else {
									newPrice = new BigDecimal(endQtyPrice * 100 / (100 - discountList.get(i)));
								}
								break;
							case 2:
								// Based on Regualr Cost
								if (markUpFormula == 0) {
									newPrice = new BigDecimal(Double.parseDouble(priceGrid.getPrice().get(i).getCost()) * (100 + discountList.get(i)) / 100);
								} else {
									newPrice = new BigDecimal(Double.parseDouble(priceGrid.getPrice().get(i).getCost()) * 100 / (100 - discountList.get(i)));
								}
								break;
							case 3:
								// Based on End Qty Cost
								if (markUpFormula == 0) {
									newPrice = new BigDecimal(endQtyPrice * (100 + discountList.get(i)) / 100);
								} else {
									newPrice = new BigDecimal(endQtyPrice * 100 / (100 - discountList.get(i)));
								}
								break;
							default:
								// Based on Regular Price
								if (markUpFormula == 0) {
									newPrice = new BigDecimal(Double.parseDouble(priceGrid.getPrice().get(i).getValue()) * (100 + discountList.get(i)) / 100);
								} else {
									newPrice = new BigDecimal(Double.parseDouble(priceGrid.getPrice().get(i).getValue()) * 100 / (100 - discountList.get(i)));
								}
								break;
							}
							newPrice = newPrice.setScale(2, BigDecimal.ROUND_UP);
							priceGrid.getPrice().get(i).setValue(newPrice.toString());
						}
					}
				}
			}
		} else if (iProduct != null) {
			// FOR PRODUCT WITH VARIANTS
			if (iProduct.getVariants() != null && iProduct.getVariants().getVariant() != null) {
				for (Variant variant : iProduct.getVariants().getVariant()) {
					if (variant.getPrices() != null && variant.getPrices().getPrice() != null) {
						this.applyMarkupOnPrice(discountList, variant.getPrices().getPrice(), markupType, markUpFormula);
					}
				}
			} else if (iProduct.getPrices() != null && iProduct.getPrices().getPrice() != null) {

				// FOR PRODUCT WITH OUT VARIANTS
				this.applyMarkupOnPrice(discountList, iProduct.getPrices().getPrice(), markupType, markUpFormula);
			}
		}
		return isMarkupAvailable;
	}

	private void applyMarkupOnPrice(List<Double> discountList, List<com.webjaguar.thirdparty.asi.asiAPI.Price> priceList, int markupType, int markUpFormula) {

		Double endQtyPrice = null;
		if (markupType == 1) {
			endQtyPrice = this.getEndQtyPriceOrCost(null, priceList, true);
		} else if (markupType == 3) {
			endQtyPrice = this.getEndQtyPriceOrCost(null, priceList, false);
		}

		for (int i = 0; i < (discountList.size() < priceList.size() ? discountList.size() : priceList.size()); i++) {
			if (priceList.get(i).getIsUndefined() != null && priceList.get(i).getIsUndefined()) {
				continue;
			}
			if (discountList.get(i) != null) {
				BigDecimal newPrice = null;
				switch (markupType) {
				case 1:
					// Based on End Qty Price
					if (markUpFormula == 0) {
						newPrice = new BigDecimal(endQtyPrice * (100 + discountList.get(i)) / 100);
					} else {
						newPrice = new BigDecimal(endQtyPrice * 100 / (100 - discountList.get(i)));
					}
					break;
				case 2:
					// Based on Regular Cost
					if (markUpFormula == 0) {
						newPrice = new BigDecimal(priceList.get(i).getCost().doubleValue() * (100 + discountList.get(i)) / 100);
					} else {
						newPrice = new BigDecimal(priceList.get(i).getCost().doubleValue() * 100 / (100 - discountList.get(i)));
					}
					break;
				case 3:
					// Based on End Qty Cost
					if (markUpFormula == 0) {
						newPrice = new BigDecimal(endQtyPrice * (100 + discountList.get(i)) / 100);
					} else {
						newPrice = new BigDecimal(endQtyPrice * 100 / (100 - discountList.get(i)));
					}
					break;
				default:
					// Based on Regular Price
					if (markUpFormula == 0) {
						newPrice = new BigDecimal(priceList.get(i).getPrice().doubleValue() * (100 + discountList.get(i)) / 100);
					} else {
						newPrice = new BigDecimal(priceList.get(i).getPrice().doubleValue() * 100 / (100 - discountList.get(i)));
					}
					break;
				}
				newPrice = newPrice.setScale(2, BigDecimal.ROUND_UP);
				priceList.get(i).setPrice(newPrice);
			}
		}
	}

	public Double getEndQtyPriceOrCost(asiProduct.PriceGrid asiPriceGrid, List<com.webjaguar.thirdparty.asi.asiAPI.Price> prices, boolean getPrice) {
		Double endQtyPrice = null;

		if (asiPriceGrid != null) {
			for (int i = 0; i < asiPriceGrid.getPrice().size(); i++) {
				Double newPrice = Double.parseDouble(getPrice ? asiPriceGrid.getPrice().get(i).getValue() : asiPriceGrid.getPrice().get(i).getCost());
				if (endQtyPrice == null || (endQtyPrice != null && endQtyPrice > newPrice)) {
					endQtyPrice = newPrice;
				}
			}
		} else if (prices != null) {
			for (int i = 0; i < prices.size(); i++) {
				if (prices.get(i).getIsUndefined() != null && prices.get(i).getIsUndefined()) {
					continue;
				}
				Double newPrice = getPrice ? prices.get(i).getPrice().doubleValue() : prices.get(i).getCost().doubleValue();
				if (endQtyPrice == null || (endQtyPrice != null && endQtyPrice > newPrice)) {
					endQtyPrice = newPrice;
				}
			}
		}
		return endQtyPrice;
	}

	public void setASIPriceWithSalesTag(asiProduct.Product product, com.webjaguar.thirdparty.asi.asiAPI.Product asiProduct, SalesTag salesTag) {

		if (product != null && product.getPricing() != null && product.getPricing().getPriceGrid() != null && salesTag != null && salesTag.getDiscount() > 0) {
			for (PriceGrid priceGrid : product.getPricing().getPriceGrid()) {
				// apply margin only on base price and with no QUR.
				if (priceGrid.isBase() && !priceGrid.getId().equalsIgnoreCase("pricegrid-99")) {
					for (int i = 0; i < priceGrid.getPrice().size(); i++) {
						BigDecimal newPrice = null;
						if (salesTag.isPercent()) {
							newPrice = new BigDecimal(Double.parseDouble(priceGrid.getPrice().get(i).getValue()) * (100 - salesTag.getDiscount()) / 100);
						} else {
							newPrice = new BigDecimal(Double.parseDouble(priceGrid.getPrice().get(i).getValue()) - salesTag.getDiscount());
						}
						newPrice = newPrice.setScale(2, BigDecimal.ROUND_UP);
						priceGrid.getPrice().get(i).setValue(newPrice.toString());
					}
				}
			}
		} else if (asiProduct != null) {

			// simple products without variants
			if (asiProduct.getVariants() == null && asiProduct.getPrices() != null && asiProduct.getPrices().getPrice() != null) {

				for (int i = 0; i < asiProduct.getPrices().getPrice().size(); i++) {
					BigDecimal newPrice = null;
					if (salesTag.isPercent()) {
						newPrice = new BigDecimal(asiProduct.getPrices().getPrice().get(i).getPrice().doubleValue() * (100 - salesTag.getDiscount()) / 100);
					} else {
						newPrice = new BigDecimal(asiProduct.getPrices().getPrice().get(i).getPrice().doubleValue() - salesTag.getDiscount());
					}
					newPrice = newPrice.setScale(2, BigDecimal.ROUND_UP);
					asiProduct.getPrices().getPrice().get(i).setPrice(newPrice);
				}
			}

			// products with variants
			if (asiProduct.getVariants() != null && asiProduct.getVariants().getVariant() != null) {
				for (Variant loopVariant : asiProduct.getVariants().getVariant()) {
					if (loopVariant.getPrices() != null && loopVariant.getPrices().getPrice() != null) {
						for (int i = 0; i < loopVariant.getPrices().getPrice().size(); i++) {
							BigDecimal newPrice = null;
							if (salesTag.isPercent()) {
								newPrice = new BigDecimal(loopVariant.getPrices().getPrice().get(i).getPrice().doubleValue() * (100 - salesTag.getDiscount()) / 100);
							} else {
								newPrice = new BigDecimal(loopVariant.getPrices().getPrice().get(i).getPrice().doubleValue() - salesTag.getDiscount());
							}
							newPrice = newPrice.setScale(2, BigDecimal.ROUND_UP);
							loopVariant.getPrices().getPrice().get(i).setPrice(newPrice);
						}
					}
				}
			}
		}
	}

	public void getProductFromAsiXml(String productXml, Product product) {

		try {
			JAXBContext context;
			Unmarshaller unmarshaller = null;
			ByteArrayInputStream bis;
			com.webjaguar.thirdparty.asi.asiAPI.Product asiProd = null;
			try {
				context = JAXBContext.newInstance(com.webjaguar.thirdparty.asi.asiAPI.Product.class);
				unmarshaller = context.createUnmarshaller();
				StringBuffer sb = new StringBuffer(productXml.toString());
				bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
				asiProd = (com.webjaguar.thirdparty.asi.asiAPI.Product) unmarshaller.unmarshal(bis);
			} catch (Exception e) {
				return;
			}

			Integer newSku = Integer.rotateLeft(asiProd.getId().intValue(), 2);
			product.setSku(newSku.toString());
			product.setCreatedBy("ASI Feed");
			product.setName(asiProd.getName());
			product.setKeywords(newSku.toString());
			product.setAsiXML(productXml.toString());
			product.setShortDesc(asiProd.getShortDescription() != null && asiProd.getShortDescription().length() >= 255 ? asiProd.getShortDescription().substring(0, 255) : asiProd
					.getShortDescription());
			product.setLongDesc(asiProd.getDescription() != null && asiProd.getDescription().length() >= 510 ? asiProd.getDescription().substring(0, 510) : asiProd.getDescription());

			try {
				// Supplier Number is String
				product.setAsiId(Integer.parseInt(asiProd.getSupplier().getAsiNumber().toString()));
				product.setAsiUniqueId(asiProd.getId().intValue());
			} catch (Exception e) {
			}

			// Supplier Number
			product.setField34((asiProd.getSupplier() != null) ? asiProd.getSupplier().getAsiNumber() : "");
			// Theme
			product.setField35((asiProd.getThemes() != null) ? Utilities.toString(asiProd.getThemes().getTheme(), ", ") : "");
			if (product.getField35() != null && product.getField35().length() > 80) {
				product.setField35(product.getField35().substring(0, 80));
			}
			// Origin
			product.setField36((asiProd.getOrigin() != null && asiProd.getOrigin().getOrigin() != null) ? Utilities.toString(asiProd.getOrigin().getOrigin(), ", ") : "");

			// Material
			if (asiProd.getAttributes() != null && asiProd.getAttributes().getMaterials() != null) {
				if (asiProd.getAttributes().getMaterials().getValues() != null) {
					if (asiProd.getAttributes().getMaterials().getValues().getString() != null) {
						try {
							String tempMaterials = Utilities.toString(asiProd.getAttributes().getMaterials().getValues().getString(), " ,");
							product.setField37(tempMaterials.length() >= 255 ? tempMaterials.substring(0, 255) : tempMaterials);
						} catch (Exception e) {
						}
					}
				}
			}
			// Product Name
			product.setField38((asiProd.getName() != null) ? asiProd.getName() : "");
			// Rush Service
			product.setField39((asiProd.isHasRushService() != null) ? "Yes" : "No");

			// Product Id
			product.setField40(asiProd.getId() + "");

			// Color
			if (asiProd.getAttributes() != null && asiProd.getAttributes().getColors() != null) {
				if (asiProd.getAttributes().getColors().getValues() != null) {
					if (asiProd.getAttributes().getColors().getValues().getString() != null) {
						try {
							String tempColors = Utilities.toString(asiProd.getAttributes().getColors().getValues().getString(), " ,");
							product.setField41(tempColors.length() >= 255 ? tempColors.substring(0, 255) : tempColors);
						} catch (Exception e) {
						}
					}
				}
			}
			// ASI Product Number
			product.setField42((asiProd.getNumber() != null) ? asiProd.getNumber() : "");

			// Supplier Name
			product.setField43((asiProd.getSupplier() != null) ? asiProd.getSupplier().getName() : "");
			// Trade Name
			product.setField44((asiProd.getTradeNames() != null) ? Utilities.toString(asiProd.getTradeNames().getTradeName(), ", ") : "");

			// Production Time Not required for retail
			/*
			 * if (asiProd.getProductionTime() != null && asiProd.getProductionTime().getValue() != null) { if (asiProd.getProductionTime().getValue().get(0) != null) { product.setField46(asiProd.getProductionTime().getValue().get(0).getName()); } }
			 */

			// field45 is kept reserved for indicator to show Added or Modified with date and time

			// Rush Time
			if (asiProd.getRushTime() != null && asiProd.getRushTime().getValue() != null) {
				if (asiProd.getRushTime().getValue() != null) {
					product.setField46(asiProd.getRushTime().getValue().get(0).getName());
				}
			}
			// Eco Friendly
			if (asiProd.isIsEcoFriendly() != null) {
				product.setField54(asiProd.isIsEcoFriendly().toString());
			}

			Map<String, Configuration> siteConfig = configDao.getSiteConfig();

			if (siteConfig.get("ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE").getValue().equals("true")) {
				this.applyPriceDifference(null, asiProd, siteConfig.get("ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS").getValue(), Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_TYPE").getValue()),
						Integer.parseInt(siteConfig.get("ASI_PRICE_MARK_UP_FORMULA").getValue()));
			}
			// get Prices
			Class<Product> c = Product.class;
			Class paramTypeDouble[] = { Double.class };
			Class paramTypeInteger[] = { Integer.class };
			if (asiProd.getPrices() != null) {
				int pr = 1;
				for (com.webjaguar.thirdparty.asi.asiAPI.Price asiPrice : asiProd.getPrices().getPrice()) {
					// product.setPrice1(price1);
					if (asiPrice.getIsUndefined() != null && asiPrice.getIsUndefined()) {
						pr++;
						continue;
					}
					try {
						Method mPrice = c.getMethod("setPrice" + pr, paramTypeDouble);
						Object arglist[] = { asiPrice.getPrice().doubleValue() };
						mPrice.invoke(product, arglist);

						Method mPrceBreak = c.getMethod("setQtyBreak" + (pr - 1), paramTypeInteger);
						Object arglist2[] = { asiPrice.getQuantity().getRange().getFrom() };
						mPrceBreak.invoke(product, arglist2);
					} catch (Exception e) {
						// do nothing
						// e.printStackTrace();
					}
					if (pr == 1) {
						product.setMinimumQty(asiPrice.getQuantity().getRange().getFrom());
					}
					pr++;
				}
			} else if (asiProd.getVariants() != null && asiProd.getVariants().getVariant() != null && !asiProd.getVariants().getVariant().isEmpty()) {
				// For products with variant
				Variant variant = asiProd.getVariants().getVariant().get(0);
				int pr = 1;
				for (com.webjaguar.thirdparty.asi.asiAPI.Price asiPrice : variant.getPrices().getPrice()) {
					// product.setPrice1(price1);
					if (asiPrice.getIsUndefined() != null && asiPrice.getIsUndefined()) {
						pr++;
						continue;
					}
					try {
						Method mPrice = c.getMethod("setPrice" + pr, paramTypeDouble);
						Object arglist[] = { asiPrice.getPrice().doubleValue() };
						mPrice.invoke(product, arglist);

						Method mPrceBreak = c.getMethod("setQtyBreak" + (pr - 1), paramTypeInteger);
						Object arglist2[] = { asiPrice.getQuantity().getRange().getFrom() };
						mPrceBreak.invoke(product, arglist2);
					} catch (Exception e) {
						// do nothing
						// e.printStackTrace();
					}
					if (pr == 1) {
						product.setMinimumQty(asiPrice.getQuantity().getRange().getFrom());
					}
					pr++;
				}
			}

			// set cost
			if (asiProd.getLowestPrice() != null && asiProd.getLowestPrice().getCost() != null) {
				product.setCost1(asiProd.getLowestPrice().getCost().doubleValue());
			}

			// get Category names
			if (asiProd.getCategories() != null) {
				int index = 47;
				for (com.webjaguar.thirdparty.asi.asiAPI.Product.Categories.Category cat : asiProd.getCategories().getCategory()) {
					switch (index) {
					case 47:
						product.setField47(cat.getName());
						break;
					case 48:
						product.setField48(cat.getName());
						break;
					case 49:
						product.setField49(cat.getName());
						break;
					}
					index++;
					if (index > 49)
						break;
				}
			}

			// Shapes
			if (asiProd.getAttributes() != null && asiProd.getAttributes().getShapes() != null) {
				if (asiProd.getAttributes().getShapes().getValues() != null) {
					if (asiProd.getAttributes().getShapes().getValues().getString() != null) {
						try {
							String tempShapes = Utilities.toString(asiProd.getAttributes().getShapes().getValues().getString(), " ,");
							product.setField51(tempShapes.length() >= 255 ? tempShapes.substring(0, 255) : tempShapes);
						} catch (Exception e) {
						}
					}
				}
			}
			// Sizes
			if (asiProd.getAttributes() != null && asiProd.getAttributes().getSizes() != null) {
				if (asiProd.getAttributes().getSizes().getValues() != null) {
					if (asiProd.getAttributes().getSizes().getValues().getString() != null) {
						try {
							String tempSizes = Utilities.toString(asiProd.getAttributes().getSizes().getValues().getString(), " ,");
							product.setField52(tempSizes.length() >= 255 ? tempSizes.substring(0, 255) : tempSizes);
						} catch (Exception e) {
						}
					}
				}
			}

			// Weight
			if (asiProd.getShipping() != null && asiProd.getShipping().getWeight() != null && asiProd.getShipping().getWeight().getValues() != null) {
				if (asiProd.getShipping().getWeight().getValues().getString() != null) {
					try {
						String tempWeight = Utilities.toString(asiProd.getShipping().getWeight().getValues().getString(), " ,");
						product.setField53(tempWeight.length() >= 255 ? tempWeight.substring(0, 255) : tempWeight);
					} catch (Exception e) {
					}
				}
			}
			// get image
			List<ProductImage> productImages = new ArrayList<ProductImage>();
			byte index = 1;
			if (asiProd.getImageUrl() != null) {
				ProductImage image = new ProductImage();
				image.setImageUrl("http://api.asicentral.com/v1/" + asiProd.getImageUrl() + "?size=large");
				image.setIndex(index++);
				productImages.add(image);
			}

			if (asiProd.getImages() != null) {
				for (String imageString : asiProd.getImages().getImage()) {
					ProductImage image = new ProductImage();
					image.setImageUrl("http://api.asicentral.com/v1/" + imageString + "?size=large");
					image.setIndex(index++);
					productImages.add(image);
				}
			}
			if (!productImages.isEmpty()) {
				product.setThumbnail(productImages.get(0));
			}
			product.setImages(productImages);
			// set layout and hide leftbar
			product.setHideLeftBar(true);
			product.setProductLayout("012");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Ticket
	public List<Ticket> getTicketsListByUserid(Integer userId, TicketSearch ticketSearch) {
		return this.ticketDao.getTicketsListByUserid(userId, ticketSearch);
	}

	public List<Ticket> getTicketsList(TicketSearch ticketSearch) {
		return this.ticketDao.getTicketsList(ticketSearch);
	}

	public List<TicketForm> getTicketVersionListById(Integer ticketId, Integer userId) {
		return this.ticketDao.getTicketVersionListById(ticketId, userId);
	}

	public Ticket getTicketById(Integer ticketId) {
		return this.ticketDao.getTicketById(ticketId);
	}

	public int insertTicket(Ticket ticket) {
		return this.ticketDao.insertTicket(ticket);
	}

	public void insertTicketVersion(TicketVersion ticketVersion) {
		this.ticketDao.insertTicketVersion(ticketVersion);
	}

	public void updateTicketLastModified(Integer ticketId, String versionCreatedByType) {
		this.ticketDao.updateTicketLastModified(ticketId, versionCreatedByType);
	}

	public void updateTicketStatus(Ticket ticket) {
		this.ticketDao.updateTicketStatus(ticket);
	}

	public List<Category> getCategoryTree(Integer categoryId, boolean showHidden, HttpServletRequest request) {
		// i18n
		String lang = null;
		if (request != null) {
			Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
			lang = RequestContextUtils.getLocale(request).getLanguage();
			// default language is English and set lang = null
			if (lang.equalsIgnoreCase(LanguageCode.en.toString())) {
				lang = null;
			}
		}

		return this.categoryDao.getCategoryTree(categoryId, showHidden, lang);
	}

	public List<Category> getCategoryList(CategorySearch search) {
		return this.categoryDao.getCategoryList(search);
	}

	public List<Category> getSiteMapCategoryList(CategorySearch search) {
		return this.categoryDao.getSiteMapCategoryList(search);
	}

	public Category getCategoryById(Integer categoryId, String protectedAccess) {
		return this.categoryDao.getCategoryById(categoryId, protectedAccess);
	}

	public void nonTransactionSafeStats(Category category) {
		this.categoryDao.updateStats(category);
	}

	public int insertCategory(Category category) {
		return this.categoryDao.insertCategory(category);
	}

	public void updateCategory(Category category) {
		this.categoryDao.updateCategory(category);
	}

	public AccessUser getAccessUser(HttpServletRequest request) {
		AccessUser accessUser = null;
		if (request != null && request.getAttribute("accessUser") != null) {
			return (AccessUser) request.getAttribute("accessUser");
		} else {
			accessUser = new AccessUser();
			accessUser.setUsername("Admin");
			return accessUser;
		}
	}

	public void insertProduct(Product product, HttpServletRequest request) {
		if (request != null || product.getCreatedBy() == null) {
			product.setCreatedBy(this.getAccessUser(request).getUsername());
		}

		this.productDao.insertProduct(product);
		/*
		 * MTZ Need to work more when product is added form front-end.
		 */
		if (product.getDefaultSupplierId() != 0) {
			Supplier supplier = new Supplier();
			supplier.setSku(product.getSku());
			supplier.setId(product.getDefaultSupplierId());
			insertProductSupplier(supplier);
		}
	}

	public void importProducts(List<Product> productList) {
		for (Product product : productList) {
			if (product.getId() == null) { // new product
				insertProduct(product, null);
				product.setId(null); // reset ID
			} else { // old product
				updateProduct(product, null);
			}
		}
	}

	public void updateProduct(Product product, HttpServletRequest request) {
		if (request != null || product.getLastModifiedBy() == null) {
			product.setLastModifiedBy(this.getAccessUser(request).getUsername());
		}

		this.productDao.updateProduct(product);
	}
	
	public void updateApiProduct(Product product, HttpServletRequest request) {
		if (request != null || product.getLastModifiedBy() == null) {
			product.setLastModifiedBy(this.getAccessUser(request).getUsername());
		}

		this.productDao.updateApiProduct(product);
	}

	public void batchCategoryIds(Integer productId, Set categoryIds, String action) {
		this.productDao.batchCategoryIds(productId, categoryIds, action);
	}

	public void batchFieldUpdate(Set productIds, Integer fieldId, String fieldValue) {
		this.productDao.batchFieldUpdate(productIds, fieldId, fieldValue);
	}

	public void deleteProduct(Product product) {
		this.productDao.deleteProduct(product);
	}

	public void deleteProductById(Integer productId) {
		this.productDao.deleteProductById(productId);
	}

	public Product getProductById(Integer productId, int numImages, boolean showPrice, Customer customer) {
		Product product = this.productDao.getProductById(productId, numImages);
		if (showPrice) {
			// get price
			Map<String, Object> gSiteConfig = globalDao.getGlobalSiteConfig();
			Map<String, Configuration> siteConfig = configDao.getSiteConfig();
			int priceTiers = (Integer) gSiteConfig.get("gPRICE_TIERS");
			if (customer != null) {
				if ((Boolean) gSiteConfig.get("gSPECIAL_PRICING") && product.getSku() != null) {
					SpecialPricing sp = null;
					sp = this.getSpecialPricingByCustomerIdSku(new SpecialPricing(product.getSku().toLowerCase(), customer.getId(), null, false));
					if (sp != null) {
						List<Price> prices = getPrices(product, sp.getPrice());
						product.setPrice(prices);
					} else {
						List<Price> prices = getPrices(product, customer.getPriceTable(), siteConfig);
						if (prices.isEmpty())
							prices = getPrices(product, priceTiers);
						product.setPrice(prices);
					}
				} else {
					List<Price> prices = getPrices(product, customer.getPriceTable(), siteConfig);
					// return regular price if pricetable is null
					if (prices.isEmpty())
						prices = getPrices(product, priceTiers);
					product.setPrice(prices);
				}
			} else {
				product.setPrice(getPrices(product, priceTiers));
			}
		}
		return product;
	}

	public Product getProductById(Integer productId, HttpServletRequest request) {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		boolean basedOnHand = false;
		if ((Boolean) gSiteConfig.get("gINVENTORY") && siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly")) {
			basedOnHand = true;
		}

		boolean priceCasePack = false;
		if ((Boolean) gSiteConfig.get("gPRICE_CASEPACK")) {
			if (ServletRequestUtils.getBooleanParameter(request, "casePrice_" + productId, false)) {
				priceCasePack = true;
			}
		}

		return getProductByIdPrivate(productId, gSiteConfig, siteConfig, getCustomerByRequest(request), getSalesRep(request), RequestContextUtils.getLocale(request).getLanguage(), priceCasePack,
				basedOnHand);
	}

	private Product getProductByIdPrivate(Integer productId, Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig, Customer customer, SalesRep salesRep, String lang,
			boolean priceCasePack, boolean basedOnHand) {
		String protectedAccess = "0";

		if (customer != null) {
			protectedAccess = customer.getProtectedAccess();
		} else if (salesRep != null) { // check for sales rep log in also
			protectedAccess = salesRep.getProtectedAccess();
		} else {
			protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			protectedAccess = Constants.FULL_PROTECTED_ACCESS;
		}
		// i18n
		if (lang != null && lang.equalsIgnoreCase(LanguageCode.en.toString())) {
			lang = null;
		}

		Product thisProduct = this.productDao.getProductById(productId, (Integer) gSiteConfig.get("gPRODUCT_IMAGES"), protectedAccess, lang, basedOnHand);
		if (thisProduct == null)
			return thisProduct;

		// i18n
		if (thisProduct.getI18nName() != null && thisProduct.getI18nName().length() > 0) {
			thisProduct.setName(thisProduct.getI18nName());
		}
		if (thisProduct.getI18nShortDesc() != null && thisProduct.getI18nShortDesc().length() > 0) {
			thisProduct.setShortDesc(thisProduct.getI18nShortDesc());
		}
		if (thisProduct.getI18nLongDesc() != null && thisProduct.getI18nLongDesc().length() > 0) {
			thisProduct.setLongDesc(thisProduct.getI18nLongDesc());
		}
		if (lang != null) {
			thisProduct = getMissingLangProductField(thisProduct, gSiteConfig, siteConfig);
		}
		File salesTagImageFile = new File(getServletContext().getRealPath("/assets/Image/PromoSalesTag/"));
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				salesTagImageFile = new File((String) prop.get("site.root") + "/assets/Image/PromoSalesTag/");
			}
		} catch (Exception e) {
		}

		if ((Boolean) gSiteConfig.get("gSALES_PROMOTIONS")) {
			if (thisProduct.getSalesTag() != null) {
				File baseFile = new File(salesTagImageFile.getAbsolutePath() + "/salestag_" + thisProduct.getSalesTag().getTagId() + ".gif");
				thisProduct.getSalesTag().setImage(baseFile.exists());
			}
		} else {
			thisProduct.setSalesTag(null);
		}

		if (!(Boolean) gSiteConfig.get("gCASE_CONTENT")) {
			thisProduct.setCaseContent(null);
		}
		if ((Integer) gSiteConfig.get("gAFFILIATE") < 1) {
			thisProduct.setCommissionTables(null);
		}
		if (!(Boolean) gSiteConfig.get("gCUSTOM_LINES")) {
			thisProduct.setNumCustomLines(null);
		}
		if ((Boolean) gSiteConfig.get("gPRODUCT_REVIEW") || siteConfig.get("PRODUCT_RATE").getValue().equals("true")) {
			thisProduct.setRate(this.reviewDao.getProductReviewAverage(thisProduct.getSku()));
		}
		// hide price
		boolean showPrice = true;
		if (thisProduct.isHidePrice()) {
			if (customer == null || !customer.isSeeHiddenPrice()) {
				showPrice = false;
			}
		}

		if (showPrice) {
			if (priceCasePack && thisProduct.getPriceCasePackQty() != null) {
				thisProduct.setPriceCasePack(true);
			}
			// get price
			int priceTiers = (Integer) gSiteConfig.get("gPRICE_TIERS");
			if (customer != null) {
				if ((Boolean) gSiteConfig.get("gSPECIAL_PRICING") && thisProduct.getSku() != null) {
					SpecialPricing sp = null;
					sp = this.getSpecialPricingByCustomerIdSku(new SpecialPricing(thisProduct.getSku().toLowerCase(), customer.getId(), null, false));
					if (sp != null) {
						List<Price> prices = getPrices(thisProduct, sp.getPrice());
						thisProduct.setPrice(prices);
					} else {
						List<Price> prices = getPrices(thisProduct, customer.getPriceTable(), siteConfig);
						if (prices.isEmpty())
							prices = getPrices(thisProduct, priceTiers);
						thisProduct.setPrice(prices);
					}

				} else {
					List<Price> prices = getPrices(thisProduct, customer.getPriceTable(), siteConfig);
					// return regular price if pricetable is null
					if (prices.isEmpty())
						prices = getPrices(thisProduct, priceTiers);
					thisProduct.setPrice(prices);
				}
			} else {
				// be default view Price Table 1
				if (siteConfig.get("PRICE_PROTECTED_ACCESS") != null && !siteConfig.get("PRICE_PROTECTED_ACCESS").getValue().equals("0")) {
					Integer defaultPriceTable = Integer.parseInt(siteConfig.get("PRICE_PROTECTED_ACCESS").getValue());
					List<Price> prices = new ArrayList<Price>();
					if (defaultPriceTable != null) {
						prices = getPrices(thisProduct, defaultPriceTable, siteConfig);
					}
					if (prices.isEmpty())
						prices = getPrices(thisProduct, priceTiers);
					thisProduct.setPrice(prices);
				} else {
					thisProduct.setPrice(getPrices(thisProduct, priceTiers));
				}
			}
		}

		thisProduct.setProductOptions(getProductOptionsByOptionCode(thisProduct.getOptionCode(), siteConfig.get("PRODUCT_MULTI_OPTION").getValue().equals("true"), thisProduct.getSalesTag(),
				protectedAccess));
		if ((Boolean) gSiteConfig.get("gBOX")) {
			for (ProductOption op : thisProduct.getProductOptions()) {
				if (op.getType().equals("box")) {
					thisProduct.setType("box");
					break;
				}
			}
		}

		return thisProduct;

	}

	public Double getProductCost(String sku, Integer quantity, HttpServletRequest request) {

		Integer productId = getProductIdBySku(sku);
		Product product = getProductById(productId, request);
		Supplier supplier = getProductSupplierBySIdSku(sku, product.getDefaultSupplierId());
		if (supplier != null && supplier.getPrice() != null) {
			return supplier.getPrice();
		} else {

			if ((product.getQtyBreak1() == null) || (product.getQtyBreak1() != null && quantity < product.getQtyBreak1())) {
				return product.getCost1();
			}

			if ((product.getQtyBreak2() == null) || (product.getQtyBreak2() != null && quantity >= product.getQtyBreak1() && quantity < product.getQtyBreak2())) {
				return product.getCost2();
			}

			if ((product.getQtyBreak3() == null) || (product.getQtyBreak3() != null && quantity >= product.getQtyBreak2() && quantity < product.getQtyBreak3())) {
				return product.getCost3();
			}

			if ((product.getQtyBreak4() == null) || (product.getQtyBreak4() != null && quantity >= product.getQtyBreak3() && quantity < product.getQtyBreak4())) {
				return product.getCost4();
			}

			if ((product.getQtyBreak5() == null) || (product.getQtyBreak5() != null && quantity >= product.getQtyBreak4() && quantity < product.getQtyBreak5())) {
				return product.getCost5();
			}

			if ((product.getQtyBreak6() == null) || (product.getQtyBreak6() != null && quantity >= product.getQtyBreak5() && quantity < product.getQtyBreak6())) {
				return product.getCost6();
			}

			if ((product.getQtyBreak7() == null) || (product.getQtyBreak7() != null && quantity >= product.getQtyBreak6() && quantity < product.getQtyBreak7())) {
				return product.getCost7();
			}

			if ((product.getQtyBreak8() == null) || (product.getQtyBreak8() != null && quantity >= product.getQtyBreak7() && quantity < product.getQtyBreak8())) {
				return product.getCost8();
			}

			if ((product.getQtyBreak9() == null) || (product.getQtyBreak9() != null && quantity >= product.getQtyBreak8() && quantity < product.getQtyBreak9())) {
				return product.getCost9();
			}
		}
		return null;
	}

	public Set<Object> getCategoryIdsByProductId(Integer productId) {
		return this.productDao.getCategoryIdsByProductId(productId);
	}

	public Product getAlsoConsiderBySku(String sku, HttpServletRequest request) {
		if (sku == null || sku.trim().equals("")) {
			return null;
		}
		return getProductById(getProductIdBySku(sku.trim()), request);
	}

	public boolean duplicateOptionName(String productId, String name) {
		return productDao.duplicateOptionName(productId, name);
	}

	public int productCount() {
		return productDao.productCount();
	}

	public int productCount(ProductSearch search) {
		return this.productDao.productCount(search);
	}

	public int productSupplierCount() {
		return this.productDao.productSupplierCount();
	}

	public Integer getProductIdBySku(String sku) {
		return productDao.getProductIdBySku(sku);
	}
	
	public String getProductSkuById(Integer id) {
		return productDao.getProductSkuById(id);
	}

	public boolean isValidProductId(int id) {
		return productDao.isValidProductId(id);
	}

	public void deleteCategory(Category category) {
		this.categoryDao.deleteCategory(category);
	}

	public List<Category> getCategoryLinks(Integer parentId, HttpServletRequest request) {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		String protectedAccess = null;

		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels > 0) {
			Customer customer = getCustomerByRequest(request);
			if (customer != null) {
				protectedAccess = customer.getProtectedAccess();
			} else {
				protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
			}
		}
		// i18n
		String lang = RequestContextUtils.getLocale(request).getLanguage();
		if (lang.equalsIgnoreCase(LanguageCode.en.toString())) {
			lang = null;
		}
		List<Category> categories = this.categoryDao.getCategoryLinks(parentId, protectedAccess, lang, (Boolean) gSiteConfig.get("gINVENTORY"));
		if (lang != null) {
			for (Category category : categories) {
				if (category.getI18nName() != null && category.getI18nName().trim().length() > 0) {
					category.setName(category.getI18nName());
				}
			}
		}

		return categories;
	}

	public Customer getCustomerByRequest(HttpServletRequest request) {
		Customer customer = null;
		UserSession userSession = (UserSession) request.getAttribute("userSession");
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		if (userSession != null) {
			customer = this.getCustomerById(userSession.getUserid());
			int numOfPriceTables = (Integer) gSiteConfig.get("gPRICE_TABLE");
			if (customer.getPriceTable() > numOfPriceTables) {
				// adjust price table
				customer.setPriceTable(0);
			}
			if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
				if (customer.getPriceTable() == null) {
					// by default all US customer see PriceTable1
					customer.setPriceTable(1);
				}
			}
		}
		return customer;
	}

	public int getProductsByCategoryIdCount(Category category, List<ProductField> searchProductField, HttpServletRequest request, ProductSearch search) {
		Customer customer = getCustomerByRequest(request);
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		search.setCategory(category.getId());
		SalesRep salesRepLogIn = null;

		if (siteConfig.get("SALESREP_LOGIN").getValue().equals("true")) {
			salesRepLogIn = getSalesRep(request);
		}
		if (customer != null) {
			search.setProtectedAccess(customer.getProtectedAccess());
		} else if (salesRepLogIn != null) { // check for salesrep login protected access
			search.setProtectedAccess(salesRepLogIn.getProtectedAccess());
		} else {
			search.setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue());
		}

		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		}
		search.setCategorySort((category.getSortBy() == null || category.getSortBy().equals("")) ? siteConfig.get("PRODUCT_SORTING").getValue() : category.getSortBy());
		search.setProductField(searchProductField);
		search.setStartDate(category.getStartDate());
		search.setEndDate(category.getEndDate());
		search.setSetType(category.getSetType());
		search.setCheckInventory((Boolean) gSiteConfig.get("gINVENTORY"));
		if ((Boolean) gSiteConfig.get("gSALES_PROMOTIONS")) {
			search.setSalesTagTitle(category.getSalesTagTitle());
		}
		// get category ids
		if (siteConfig.get("CATEGORY_INTERSECTION").getValue().equals("true")) {
			search.setCategoryIds(null);
			List<Integer> categoryIdsList = null;
			if (category.getCategoryIds() != null && !category.getCategoryIds().isEmpty()) {
				String[] categoryIds = category.getCategoryIds().split(",");
				categoryIdsList = new ArrayList<Integer>();
				for (int x = 0; x < categoryIds.length; x++) {
					if (!("".equals(categoryIds[x].trim()))) {
						categoryIdsList.add(Integer.valueOf(categoryIds[x].trim()));
					}
				}
				if (!categoryIdsList.isEmpty())
					search.setCategoryIds(categoryIdsList);
			}
		}
		if (siteConfig.get("SEARCH_FIELDS_MULTIPLE").getValue().equals("true")) {
			search.setSearchFieldsMultiple(true);
		}
		return this.productDao.getProductsByCategoryIdCount(search);
	}

	public List<Product> getProductsByCategoryId(Category category, List<ProductField> searchProductField, HttpServletRequest request, ProductSearch search, boolean fieldFilter) {
		Customer customer = getCustomerByRequest(request);
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		int priceTiers = (Integer) gSiteConfig.get("gPRICE_TIERS");
		search.setCategory(category.getId());
		SalesRep salesRepLogIn = null;

		if (siteConfig.get("SALESREP_LOGIN").getValue().equals("true")) {
			salesRepLogIn = getSalesRep(request);
		}
		if (siteConfig.get("SEARCH_FIELDS_MULTIPLE").getValue().equals("true")) {
			search.setSearchFieldsMultiple(true);
		}
		if (customer != null) {
			search.setProtectedAccess(customer.getProtectedAccess());

		} else if (salesRepLogIn != null) {// check for sales rep log in also
			search.setProtectedAccess(salesRepLogIn.getProtectedAccess());
		} else {
			search.setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue());
		}

		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		}
		search.setCategorySort((category.getSortBy() == null || category.getSortBy().equals("")) ? siteConfig.get("PRODUCT_SORTING").getValue() : category.getSortBy());
		search.setProductField(searchProductField);
		search.setStartDate(category.getStartDate());
		search.setEndDate(category.getEndDate());
		search.setSetType(category.getSetType());
		search.setCheckInventory((Boolean) gSiteConfig.get("gINVENTORY"));
		if ((Boolean) gSiteConfig.get("gSALES_PROMOTIONS")) {
			search.setSalesTagTitle(category.getSalesTagTitle());
		}
		// set Product Review, Mail In Rebate and deals
		search.setProductReview((Boolean) gSiteConfig.get("gPRODUCT_REVIEW"));
		search.setMailInRebate((Integer) gSiteConfig.get("gMAIL_IN_REBATES") > 0);
		search.setDeals((Integer) gSiteConfig.get("gDEALS") > 0);

		// get category ids
		if (siteConfig.get("CATEGORY_INTERSECTION").getValue().equals("true")) {
			search.setCategoryIds(null);
			List<Integer> categoryIdsList = null;
			if (category.getCategoryIds() != null && !category.getCategoryIds().isEmpty()) {
				String[] categoryIds = category.getCategoryIds().split(",");
				categoryIdsList = new ArrayList<Integer>();
				for (int x = 0; x < categoryIds.length; x++) {
					if (!("".equals(categoryIds[x].trim()))) {
						categoryIdsList.add(Integer.valueOf(categoryIds[x].trim()));
					}
				}
				if (!categoryIdsList.isEmpty())
					search.setCategoryIds(categoryIdsList);
			}
		}

		// i18n
		search.setLang(RequestContextUtils.getLocale(request).getLanguage());
		if (search.getLang().equalsIgnoreCase(LanguageCode.en.toString())) {
			search.setLang(null);
		}

		boolean basedOnHand = false;
		if (siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly")) {
			basedOnHand = true;
		}
		search.setBasedOnHand(basedOnHand);

		List<Product> products;
		if (fieldFilter) {
			// this product object has only Field value
			products = this.productDao.getProductsFieldByCategoryId(search);
		} else {
			// this product object has more detail value
			products = this.productDao.getProductsByCategoryId(search);
		}
		for (Product product : products) {
			if (customer != null) {
				List<Price> prices = getPrices(product, customer.getPriceTable(), siteConfig);
				// return regular price if pricetable is null
				if (prices.isEmpty())
					prices = getPrices(product, priceTiers);
				product.setPrice(prices);
			} else {
				// by default view Price Table 1
				if (siteConfig.get("PRICE_PROTECTED_ACCESS") != null && !siteConfig.get("PRICE_PROTECTED_ACCESS").getValue().equals("0")) {
					Integer defaultPriceTable = Integer.parseInt(siteConfig.get("PRICE_PROTECTED_ACCESS").getValue());
					List<Price> prices = new ArrayList<Price>();
					if (defaultPriceTable != null) {
						prices = getPrices(product, defaultPriceTable, siteConfig);
					}
					if (prices.isEmpty())
						prices = getPrices(product, priceTiers);
					product.setPrice(prices);
				} else {
					product.setPrice(getPrices(product, priceTiers));
				}
			}
			// i18n
			if (product.getI18nName() != null && product.getI18nName().length() > 0) {
				product.setName(product.getI18nName());
			}
			if (product.getI18nShortDesc() != null && product.getI18nShortDesc().length() > 0) {
				product.setShortDesc(product.getI18nShortDesc());
			}
			if (product.getI18nLongDesc() != null && product.getI18nLongDesc().length() > 0) {
				product.setLongDesc(product.getI18nLongDesc());
			}
			if (search.getLang() != null) {
				product = getMissingLangProductField(product, gSiteConfig, siteConfig);
			}

		}
		return products;
	}

	private Product getMissingLangProductField(Product product, Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig) {
		Product thisProductLangEn = this.productDao.getProductById(product.getId(), (Integer) gSiteConfig.get("gPRODUCT_IMAGES"), siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue(), null, false);
		Class<Product> obj = Product.class;
		Method method = null;
		Object arglist[] = null;
		Class<String> parameterType = String.class;
		for (int i = 1; i <= (Integer) gSiteConfig.get("gPRODUCT_FIELDS") && (Integer) gSiteConfig.get("gPRODUCT_FIELDS") > 0; i++) {
			try {
				method = obj.getMethod("getField" + i);
				if (((String) method.invoke(product, arglist)) != null && ((String) method.invoke(product, arglist)).isEmpty()) {
					String field = (String) method.invoke(thisProductLangEn, arglist);
					if (field != null && !field.isEmpty()) {
						method = obj.getMethod("setField" + i, parameterType);
						method.invoke(product, field);
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return product;
	}

	public List<Product> getShoppingListByUserid(Integer userId, int groupId, HttpServletRequest request) {
		Customer customer = getCustomerByRequest(request);
		MyListSearch search = new MyListSearch();
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		int priceTiers = (Integer) gSiteConfig.get("gPRICE_TIERS");
		search.setUserId(userId);
		search.setGroupId(groupId);
		if (customer != null) {
			search.setProtectedAccess(customer.getProtectedAccess());
		} else {
			search.setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue());
		}
		// set sorting
		search.setSort("name");

		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		}
		List<Product> newProducts = new ArrayList<Product>();
		List<Product> products = this.productDao.getShoppingListByUserid(search);
		Iterator<Product> iter = products.iterator();
		while (iter.hasNext()) {
			Product product = iter.next();

			if (customer != null) {
				if ((Boolean) gSiteConfig.get("gSPECIAL_PRICING")) {
					SpecialPricing sp = null;
					sp = this.getSpecialPricingByCustomerIdSku(new SpecialPricing(product.getSku(), customer.getId(), null, false));
					if (sp != null) {
						List<Price> prices = getPrices(product, sp.getPrice());
						product.setPrice(prices);
					} else {
						List<Price> prices = getPrices(product, customer.getPriceTable(), siteConfig);
						if (prices.isEmpty())
							prices = getPrices(product, priceTiers);
						product.setPrice(prices);
					}

				} else {
					List<Price> prices = getPrices(product, customer.getPriceTable(), siteConfig);
					// return regular price if pricetable is null
					if (prices.isEmpty())
						prices = getPrices(product, priceTiers);
					product.setPrice(prices);
				}

			} else {
				product.setPrice(getPrices(product, priceTiers));
			}
			// hide price
			if (product.isHidePrice()) {
				if (customer == null || !customer.isSeeHiddenPrice()) {
					product.setPrice(null);
				}
			}
			// hide msrp
			if (product.isHideMsrp()) {
				product.setMsrp(null);
			}
			newProducts.add(product);
		}
		return newProducts;
	}

	public List<Product> searchProducts(ProductSearch search, HttpServletRequest request, Customer customer) {
		Map<String, Object> gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		int priceTiers = (Integer) gSiteConfig.get("gPRICE_TIERS");
		search.setSort((search.getSort() == null || search.getSort().equals("")) ? siteConfig.get("PRODUCT_SORTING").getValue() : search.getSort());
		// inventory
		search.setCheckInventory((Boolean) gSiteConfig.get("gINVENTORY"));

		SalesRep salesRepLogIn = null;
		if (siteConfig.get("SALESREP_LOGIN").getValue().equals("true")) {
			salesRepLogIn = getSalesRep(request);
		}

		// protected access
		if (customer != null) {
			search.setProtectedAccess(customer.getProtectedAccess());
		} else if (salesRepLogIn != null) {
			search.setProtectedAccess(salesRepLogIn.getProtectedAccess());
		} else {
			search.setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue());
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		}

		File salesTagImageFile = new File(getServletContext().getRealPath("/assets/Image/PromoSalesTag/"));
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				salesTagImageFile = new File((String) prop.get("site.root") + "/assets/Image/PromoSalesTag/");
			}
		} catch (Exception e) {
		}

		boolean customerSeeHiddendPrice = (customer == null || !customer.isSeeHiddenPrice()) ? false : true;

		// i18n
		search.setLang(RequestContextUtils.getLocale(request).getLanguage());
		if (search.getLang().equalsIgnoreCase(LanguageCode.en.toString())) {
			search.setLang(null);
		}
		search.setInflector(Boolean.parseBoolean(siteConfig.get("SEARCH_INFLECTOR").getValue()));

		if (siteConfig.get("GROUP_SEARCH").getValue().equals("true")) {
			search.setGroupSearch("field_50");
		}
		List<Product> products = this.productDao.searchProducts(search);

		// special pricing
		Map<String, SpecialPricing> productSpecialPrice = null;
		if ((Boolean) gSiteConfig.get("gSPECIAL_PRICING") && customer != null) {
			productSpecialPrice = getSpecialPricingByCustomerIDPerPage(customer.getId(), products);
		}

		for (Product product : products) {
			if (customer != null) {
				List<Price> prices = getPrices(product, customer.getPriceTable(), siteConfig);
				// return regular price if pricetable is null
				if (prices.isEmpty())
					prices = getPrices(product, priceTiers);
				product.setPrice(prices);
			} else {
				product.setPrice(getPrices(product, priceTiers));
			}

			if ((Boolean) gSiteConfig.get("gSALES_PROMOTIONS")) {
				if (product.getSalesTag() != null) {
					File baseFile = new File(salesTagImageFile.getAbsolutePath() + "/salestag_" + product.getSalesTag().getTagId() + ".gif");
					product.getSalesTag().setImage(baseFile.exists());
				}
			} else {
				product.setSalesTag(null);
			}

			if ((Boolean) gSiteConfig.get("gBOX")) {
				product.setProductOptions(getProductOptionsByOptionCode(product.getOptionCode(), siteConfig.get("PRODUCT_MULTI_OPTION").getValue().equals("true"), product.getSalesTag(),
						search.getProtectedAccess()));
				for (ProductOption op : product.getProductOptions()) {
					if (op.getType().equals("box") && product.getBoxSize() != null) {
						product.setType("box");
						product.setProductOptions(null);
						break;
					}
				}
			}

			// hide price
			if (product.isHidePrice() && !customerSeeHiddendPrice) {
				product.setPrice(null);
			}

			// hide msrp
			if (product.isHideMsrp()) {
				product.setMsrp(null);
			}

			// special pricing
			if (productSpecialPrice != null && !productSpecialPrice.isEmpty() && product.getSku() != null) {
				SpecialPricing sp = productSpecialPrice.get(product.getSku().toLowerCase());
				if (sp != null) {
					List<Price> priceList = new ArrayList<Price>();
					priceList.add(new Price(sp.getPrice(), null, null, null, null, null));
					product.setPrice(priceList);
					product.setSalesTag(null);
				}
			}

			// case content
			if (!(Boolean) gSiteConfig.get("gCASE_CONTENT")) {
				product.setCaseContent(null);
			}
			// custom lines
			if (!(Boolean) gSiteConfig.get("gCUSTOM_LINES")) {
				product.setNumCustomLines(null);
			}
			// i18n
			if (product.getI18nName() != null && product.getI18nName().length() > 0) {
				product.setName(product.getI18nName());
			}
			if (product.getI18nShortDesc() != null && product.getI18nShortDesc().length() > 0) {
				product.setShortDesc(product.getI18nShortDesc());
			}
			if (product.getI18nLongDesc() != null && product.getI18nLongDesc().length() > 0) {
				product.setLongDesc(product.getI18nLongDesc());
			}
			if (search.getLang() != null) {
				product = getMissingLangProductField(product, gSiteConfig, siteConfig);
			}
		}

		return products;
	}

	public int searchProductsCount(ProductSearch search, HttpServletRequest request, Customer customer) {
		Map<String, Object> gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		SalesRep salesRepLogIn = null;

		if (siteConfig.get("SALESREP_LOGIN").getValue().equals("true")) {
			salesRepLogIn = getSalesRep(request);
		}
		// inventory
		search.setCheckInventory((Boolean) gSiteConfig.get("gINVENTORY"));

		// protected access
		if (customer != null) {
			search.setProtectedAccess(customer.getProtectedAccess());
		} else if (salesRepLogIn != null) { // if sales rep log in check for protected access
			search.setProtectedAccess(salesRepLogIn.getProtectedAccess());
		} else {
			search.setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue());
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		}
		search.setInflector(Boolean.parseBoolean(siteConfig.get("SEARCH_INFLECTOR").getValue()));

		if (siteConfig.get("GROUP_SEARCH").getValue().equals("true")) {
			search.setGroupSearch("field_50");
		}
		return this.productDao.searchProductsCount(search);
	}

	public List<Supplier> getProductSupplierListBySku(String sku) {
		return this.productDao.getProductSupplierListBySku(sku);
	}
	
	public Integer getProductSupplierCost(String sku, Integer defaultSupplierId){
		return this.productDao.getProductSupplierCost(sku,defaultSupplierId);
	}

	public void updateProductSupplier(Supplier supplier) {
		this.productDao.updateProductSupplier(supplier);
	}

	public void insertProductSupplier(Supplier supplier) {
		// This is to check if a product has defaultSUpplierId, if not return the supplierId from 'product_supplier' based on the lowest price.
		if (supplier.getSku() != null) {
			Integer defaultSupplierId = getDefaultSupplierIdBySku(supplier.getSku());
			if (!(defaultSupplierId > 0)) {
				Supplier defaultSupplier = this.productDao.getProductSupplierPrimaryBySku(supplier.getSku());
				if (defaultSupplier != null) {
					updateDefaultSupplierId(defaultSupplier.getId(), defaultSupplier.getSku());
				} else {
					updateDefaultSupplierId(supplier.getId(), supplier.getSku());
				}
			}
		}
		this.productDao.insertProductSupplier(supplier);
	}

	public void updateDefaultSupplierId(Integer supplierId, String productSku) {
		this.productDao.updateDefaultSupplierId(supplierId, productSku);
	}

	public Supplier getProductSupplierBySIdSku(String sku, Integer supplierId) {
		return this.productDao.getProductSupplierBySIdSku(sku, supplierId);
	}

	public Supplier getProductSupplierPrimaryBySku(String sku) {
		return this.productDao.getProductSupplierPrimaryBySku(sku);
	}

	public boolean isValidSupplierId(int id) {
		return this.productDao.isValidSupplierId(id);
	}

	public void importProductSuppliers(List<Supplier> suppliers) {
		this.productDao.importProductSuppliers(suppliers);
	}

	public List getProductSkuBySId(Integer supplierId) {
		return this.productDao.getProductSkuBySId(supplierId);
	}

	public void deleteProductSupplier(String sku, Integer supplierId) {
		this.productDao.deleteProductSupplier(sku, supplierId);
	}

	public void nonTransactionSafeStats(Product product) {
		this.productDao.nonTransactionSafeUpdateStats(product);
	}

	public ProductLabelTemplate getProductLabelTemplate(Integer id) {
		return this.productDao.getProductLabelTemplate(id);
	}

	public List<ProductLabelTemplate> getProductLabelTemplates(ProductLabelTemplateSearch search) {
		return this.productDao.getProductLabelTemplates(search);
	}

	public Customer getCustomerByToken(String token) {
		return this.customerDao.getCustomerByToken(token);
	}

	public String getTokenByUserId(Integer userId) {
		return this.customerDao.getTokenByUserId(userId);
	}

	public String changeCustomerToken(Integer userId) {
		return this.customerDao.changeCustomerToken(userId);
	}

	public List<Product> getProductsOnFrontendBySupplier(Integer supplierId) {
		return this.productDao.getProductsOnFrontendBySupplier(supplierId);
	}

	public List<Product> getProductsBySupplierId(Integer supplierId) {
		return this.productDao.getProductsBySupplierId(supplierId);
	}

	public List<Product> getProductList(ProductSearch search) {
		return this.productDao.getProductList(search);
	}

	public int getProductListCount(ProductSearch search) {
		return this.productDao.getProductListCount(search);
	}

	public List<Integer> getProductIdListBySearch(ProductSearch search) {
		return this.productDao.getProductIdListBySearch(search);
	}

	public List<Product> getStoneEdgeProductList(ProductSearch search) {

		Map<String, Object> gSiteConfig = globalDao.getGlobalSiteConfig();
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		List<ProductField> thisProductFields = getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
		List<Product> products = this.productDao.getStoneEdgeProductList(search);
		for (Product product : products) {
			List<ProductField> productFields = new ArrayList<ProductField>();
			// product fields
			for (ProductField productField : thisProductFields) {
				if (productField.isEnabled()) {
					ProductField newProductField = new ProductField();
					newProductField.setId(productField.getId());
					newProductField.setName(productField.getName());
					try {
						m = c.getMethod("getField" + productField.getId());
						newProductField.setValue((String) m.invoke(product, arglist));
					} catch (Exception e) {
						e.printStackTrace();
					}
					productFields.add(newProductField);
				}
			}
			product.setProductFields(productFields);
		}

		return products;
	}

	public List<Product> getProductExportList(int limit, int offset) {
		return this.productDao.getProductExportList(limit, offset);
	}

	public List<Product> getProductExportList(ProductSearch search, int numImages) {
		return this.productDao.getProductExportList(search, numImages);
	}

	public List<Product> getSiteMapProductList(ProductSearch search) {
		return this.productDao.getSiteMapProductList(search);
	}

	public List<Supplier> getProductSupplierExportList(int limit, int offset) {
		return this.productDao.getProductSupplierExportList(limit, offset);
	}

	public void updateProductFields(List<ProductField> productFields) {
		this.productDao.updateProductFields(productFields);
	}

	public void updateI18nProductFields(List<ProductField> productFields) {
		this.productDao.updateI18nProductFields(productFields);
	}

	public List<String> getProductSkuListByCategoryId(Integer cid) {
		return this.productDao.getProductSkuListByCategoryId(cid);
	}

	public List<ProductField> getProductFields(String protectedAccess, int numProdFields) {
		return this.productDao.getProductFields(protectedAccess, numProdFields);
	}

	public Map<String, Map<Integer, ProductField>> getI18nProductFields(int numProdFields) {
		return this.productDao.getI18nProductFields(numProdFields);
	}

	public List<ProductField> getProductFieldsRanked(String protectedAccess, int numProdFields, String lang) {
		return this.productDao.getProductFieldsRanked(protectedAccess, numProdFields, lang);
	}

	public void updateProduct(String updateStatement, String whereStatement) {
		this.productDao.updateProduct(updateStatement, whereStatement);
	}
	public void updateParentProduct(String parentSku, Integer productId){
		this.productDao.updateParentProduct(parentSku, productId);
	}
	
	public List<RangeValue> getRangeValues(String type) {
		return this.productDao.getRangeValues(type);
	}

	public List<ProductOption> getProductOptionsByOptionCode(String optionCode, boolean multiOption, SalesTag salesTag, String protectedAccess) {

		// make sure sales tag id is not passed if sales tag was set to null due to price table or special pricing
		Integer salesTagId = null;
		if (salesTag != null)
			salesTagId = salesTag.getTagId();

		List<ProductOption> options = new ArrayList<ProductOption>();
		if (multiOption && optionCode != null && optionCode.length() > 0) {
			Set<String> codeSet = new HashSet<String>();
			for (String code : optionCode.split("[,]")) {
				code = code.trim();
				if (code.length() > 0 && codeSet.add(code.toLowerCase())) {
					options.addAll(productDao.getProductOptionsByOptionCode(code, salesTagId, protectedAccess));
				}
			}
		} else {
			options.addAll(productDao.getProductOptionsByOptionCode(optionCode, salesTagId, protectedAccess));
		}
		return options;
	}

	public void updateProductOptions(Option option) {
		this.productDao.updateProductOptions(option);
	}

	public List<Option> getOptionsList(OptionSearch search) {
		return this.productDao.getOptionsList(search);
	}

	public int getOptionsListCount() {
		return this.productDao.getOptionsListCount();
	}

	public Option getOption(Integer id, String code) {
		return this.productDao.getOption(id, code);
	}

	public void deleteOption(Option option) {
		this.productDao.deleteOption(option);
	}

	public void updateSalesTagToProductId(int productId, Integer salesTagId) {
		this.productDao.updateSalesTagToProductId(productId, salesTagId);
	}

	public void updateManufacturerToProductId(int productId, Integer manufacturerId) {
		this.productDao.updateManufacturerToProductId(productId, manufacturerId);
	}

	public Double getUnitPrice(Map gSiteConfig, Map siteConfig, int customerId, String sku, int qty) {

		if ((Boolean) gSiteConfig.get("gSPECIAL_PRICING")) {
			SpecialPricing sp = this.getSpecialPricingByCustomerIdSku(new SpecialPricing(sku, customerId, null, false));
			if (sp != null) {
				return sp.getPrice();
			}
		}
		Product product = getProductById(getProductIdBySku(sku), 0, false, null);
		Customer customer = getCustomerById(customerId);
		if (customer.getPriceTable() > (Integer) gSiteConfig.get("gPRICE_TABLE")) {
			// adjust price table
			customer.setPriceTable(0);
		}

		if (product != null) {
			List<Price> prices = getPrices(product, customer.getPriceTable(), siteConfig);
			// return regular price if pricetable is null
			if (prices.isEmpty()) {
				int priceTiers = (Integer) gSiteConfig.get("gPRICE_TIERS");
				prices = getPrices(product, priceTiers);
			}
			product.setPrice(prices);

			return Cart.unitPriceByQty(qty, product);
		}

		return null;
	}

	public List<ProductField> getProductFields(HttpServletRequest request, List<Product> productList, boolean hideEmptyColumn) throws Exception {
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Customer customer = getCustomerByRequest(request);
		String protectedAccess = "0";
		if (customer != null) {
			protectedAccess = customer.getProtectedAccess();
		} else {
			protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			protectedAccess = Constants.FULL_PROTECTED_ACCESS;
		}
		// i18n
		String lang = null;
		if (request != null) {
			lang = RequestContextUtils.getLocale(request).getLanguage();
			if (lang.equalsIgnoreCase(LanguageCode.en.toString())) {
				lang = null;
			}
		}

		List<ProductField> productFields = getProductFieldsRanked(protectedAccess, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"), lang);

		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		productField: for (ProductField productField : productFields) {
			Iterator<Product> productListIterator = productList.iterator();
			while (productListIterator.hasNext()) {
				Product product = productListIterator.next();
				m = c.getMethod("getField" + productField.getId());
				String value = (String) m.invoke(product, arglist);
				if (value != null && value.length() > 0) {
					productField.setEmpty(false);
					continue productField;
				}
			}
		}
		Iterator<Product> productListIterator = productList.iterator();
		while (productListIterator.hasNext()) {
			Product product = productListIterator.next();
			Iterator<ProductField> pfIter = productFields.iterator();
			while (pfIter.hasNext()) {
				ProductField productField = pfIter.next();
				if (productField.isEmpty() && hideEmptyColumn) {
					pfIter.remove();
				} else {
					m = c.getMethod("getField" + productField.getId());
					ProductField newProductField = new ProductField();
					newProductField.setId(productField.getId());
					newProductField.setQuickModeField(productField.isQuickModeField());
					newProductField.setQuickMode2Field(productField.isQuickMode2Field());
					newProductField.setComparisonField(productField.isComparisonField());
					newProductField.setShowOnMyList(productField.isShowOnMyList());
					newProductField.setValue((String) m.invoke(product, arglist));
					newProductField.setFieldType(productField.getFieldType());
					newProductField.setFormatType(productField.getFormatType());
					newProductField.setShowOnPresentation(productField.isShowOnPresentation());
					product.addProductField(newProductField);
					if (newProductField.getValue() != null && newProductField.getValue().trim().length() > 0) {
						productField.getValues().add(newProductField.getValue());
					}
				}
			}
		}
		return productFields;
	}

	public Product getProductByAsiId(Integer asiId, HttpServletRequest request) {

		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		int priceTiers = (Integer) gSiteConfig.get("gPRICE_TIERS");
		Product product = this.productDao.getProductByAsiId(asiId);
		if (product != null) {
			product.setPrice(getPrices(product, priceTiers));
		}
		return product;
	}

	public List<NameValues> getProductFields(Category category, HttpServletRequest request) throws Exception {
		List<NameValues> filterList = new ArrayList<NameValues>();

		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Customer customer = getCustomerByRequest(request);
		String protectedAccess = "0";
		if (customer != null) {
			protectedAccess = customer.getProtectedAccess();
		} else {
			protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			protectedAccess = Constants.FULL_PROTECTED_ACCESS;
		}

		// i18n
		String lang = null;
		if (request != null) {
			lang = RequestContextUtils.getLocale(request).getLanguage();
			if (lang.equalsIgnoreCase(LanguageCode.en.toString())) {
				lang = null;
			}
		}

		List<ProductField> productFields = getProductFieldsRanked(protectedAccess, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"), lang);
		List<Product> productList = getProductsByCategoryId(category, new ArrayList<ProductField>(), request, new ProductSearch(), true);
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		NameValues nv;
		productField: for (ProductField productField : productFields) {
			Iterator<Product> productListIterator = productList.iterator();
			while (productListIterator.hasNext()) {
				Product product = productListIterator.next();
				m = c.getMethod("getField" + productField.getId());
				String value = (String) m.invoke(product, arglist);
				if (value != null && value.trim().length() > 0 && (category.getDisplayMode().equals("quick2") ? productField.isSearch2() : productField.isSearch())) {
					productField.setEmpty(false);
					nv = new NameValues();
					nv.setName(productField.getName().trim());
					nv.setFieldId(productField.getId());
					filterList.add(nv);
					continue productField;
				}
			}
		}
		Iterator<Product> productListIterator = productList.iterator();
		while (productListIterator.hasNext()) {
			Product product = productListIterator.next();
			Iterator<ProductField> pfIter = productFields.iterator();
			int index = 0;
			while (pfIter.hasNext()) {
				ProductField productField = pfIter.next();
				if (productField.isEmpty()) {
					pfIter.remove();
				} else {
					m = c.getMethod("getField" + productField.getId());
					String value = (String) m.invoke(product, arglist);
					if (value != null && !value.trim().equals("") && (category.getDisplayMode().equals("quick2") ? productField.isSearch2() : productField.isSearch())) {
						if (siteConfig.get("SEARCH_FIELDS_MULTIPLE").getValue().equals("true")) {
							String[] array = value.split(",");
							for (int i = 0; i < array.length; i++) {
								if (!array[i].trim().equals(""))
									((NameValues) filterList.get(index)).getValue().add(array[i].trim().toString());
							}
						} else {
							((NameValues) filterList.get(index)).getValue().add(value.trim());
						}

					}
					index++;
				}
			}
		}
		return filterList;
	}

	public List<ProductField> getProductFields(HttpServletRequest request, List<Product> productList, List<NameValues> dropDownList) throws Exception {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Customer customer = getCustomerByRequest(request);
		String protectedAccess = "0";
		if (customer != null) {
			protectedAccess = customer.getProtectedAccess();
		} else {
			protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			protectedAccess = Constants.FULL_PROTECTED_ACCESS;
		}
		List<ProductField> productFields = new ArrayList<ProductField>();
		for (ProductField productField : getProductFields(protectedAccess, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"))) {
			if (productField.isEnabled()) {
				productFields.add(productField);
			}
		}
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		NameValues nv;
		productField: for (ProductField productField : productFields) {
			Iterator<Product> productListIterator = productList.iterator();
			while (productListIterator.hasNext()) {
				Product product = productListIterator.next();
				m = c.getMethod("getField" + productField.getId());
				String value = (String) m.invoke(product, arglist);
				if (value != null && value.length() > 0 && productField.isSearch()) {
					productField.setEmpty(false);
					nv = new NameValues();
					nv.setName(productField.getName());
					nv.setFieldId(productField.getId());
					dropDownList.add(nv);
					continue productField;
				}
			}
		}
		Iterator<Product> productListIterator = productList.iterator();
		while (productListIterator.hasNext()) {
			Product product = productListIterator.next();
			Iterator<ProductField> pfIter = productFields.iterator();
			int index = 0;
			while (pfIter.hasNext()) {
				ProductField productField = pfIter.next();
				if (productField.isEmpty()) {
					pfIter.remove();
				} else {
					m = c.getMethod("getField" + productField.getId());
					ProductField newProductField = new ProductField();
					newProductField.setId(productField.getId());
					newProductField.setQuickModeField(productField.isQuickModeField());
					newProductField.setComparisonField(productField.isComparisonField());
					newProductField.setValue((String) m.invoke(product, arglist));
					String value = (String) m.invoke(product, arglist);
					if (value != null && !value.trim().equals("") && productField.isSearch()) {
						((NameValues) dropDownList.get(index)).getValue().add(value.trim());
					}

					index++;
					product.addProductField(newProductField);
				}
			}
		}
		return productFields;
	}

	public String getProductAvialabilityById(Integer productId) {
		return this.productDao.getProductAvialabilityById(productId);
	}

	public String getProductRedirectUrl(String sku, Integer productId, String protectedAccess, String field) {
		return this.productDao.getProductRedirectUrl(sku, productId, protectedAccess, field);
	}

	public String getProductAsiXMLById(String sku) {
		return this.productDao.getProductAsiXMLById(sku);
	}

	public String getProductNoteBySku(String sku) {
		return this.productDao.getProductNoteBySku(sku);
	}

	public boolean isEndQtyPricing(String sku) {
		return this.productDao.isEndQtyPricing(sku);
	}

	public List<Product> getCustomerShoppingCartProductInfo(Integer userId) {
		return this.productDao.getCustomerShoppingCartProductInfo(userId);
	}

	public void updateCategoryProductRanking(List<Map<String, Integer>> data) {
		this.categoryDao.updateCategoryProductRanking(data);
	}

	public String getCategoryName(Integer categoryId) {
		return this.categoryDao.getCategoryName(categoryId);
	}

	/* categoryLeftBar if null get from Site Config, if empty regular left bar, if 3 accordion effect, if 4 dynamic menu */
	public List<Category> getMainCategoryLinks(HttpServletRequest request, String categoryLeftBar) {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		int LEFTBAR_LAYOUT = Integer.parseInt(siteConfig.get("LEFTBAR_LAYOUT").getValue());

		Integer parentId[] = new Integer[LEFTBAR_LAYOUT + 1];

		int cid = ServletRequestUtils.getIntParameter(request, "cid", -1);
		if (request.getAttribute("categoryId") != null) {
			cid = (Integer) request.getAttribute("categoryId");
		}
		String url = request.getServletPath();
		try {
			if (url.equals("/aboutus.jhtm"))
				cid = Integer.parseInt(siteConfig.get("ABOUT_US_ID").getValue());
			else if (url.equals("/contactus.jhtm"))
				cid = Integer.parseInt(siteConfig.get("CONTACT_US_ID").getValue());
		} catch (Exception e) {
		}

		if (cid > -1) {
			List<Category> categoryLinksTree = this.categoryDao.getCategoryLinksTree(cid);
			if (!categoryLinksTree.isEmpty()) {
				parentId[0] = categoryLinksTree.get(0).getId();
				for (int i = 1; i <= LEFTBAR_LAYOUT; i++) {
					if (categoryLinksTree.size() > i) {
						parentId[i] = categoryLinksTree.get(i).getId();
					}
				}
			}
		}

		// multi store
		Integer hostCategoryId = getHomePageByHost(request.getHeader("host"));
		if (parentId[0] == null && hostCategoryId != null) {
			if (getCategoryById(hostCategoryId, "").isShowSubcats()) {
				parentId[0] = hostCategoryId;
			}
		}

		File baseFile = new File(getServletContext().getRealPath("/assets/Image/Category/"));
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				baseFile = new File((String) prop.get("site.root") + "/assets/Image/Category/");
			}
		} catch (Exception e) {
		}

		List<Category> categoryLinks = null;
		String leftBarType = siteConfig.get("LEFTBAR_DEFAULT_TYPE").getValue();
		if (categoryLeftBar != null && categoryLeftBar != "") { // dynamic Menu
			leftBarType = categoryLeftBar;
		}

		if (leftBarType.equals("4")) { // dynamic menus
			String protectedAccess = null;
			// check if protected feature is enabled
			int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
			if (protectedLevels > 0) {
				Customer customer = getCustomerByRequest(request);
				if (customer != null) {
					protectedAccess = customer.getProtectedAccess();
				} else {
					protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
				}
			}
			// i18n
			String lang = RequestContextUtils.getLocale(request).getLanguage();
			if (lang.equalsIgnoreCase(LanguageCode.en.toString())) {
				lang = null;
			}
			categoryLinks = getCategoryTree(parentId[0], protectedAccess, false, null, null, lang);

		} else {
			categoryLinks = getCategoryLinks(parentId[0], request);
			Iterator<Category> clIter = categoryLinks.iterator();
			while (clIter.hasNext()) {
				Category category = clIter.next();
				if (baseFile.canRead()) {
					// check only if directory exists
					category.setHasLinkImage((new File(baseFile, "catlink_" + category.getId() + ".gif")).exists());
					category.setHasLinkImageOver((new File(baseFile, "catlink_over_" + category.getId() + ".gif")).exists());
				}
				if (category.getId().equals(parentId[0]) || category.getId().equals(parentId[1])) {
					category.setSelected(true);
					getCategoryLinks(category, request, baseFile, parentId, 1, LEFTBAR_LAYOUT);
				}
			}
		}
		return categoryLinks;
	}

	private void getCategoryLinks(Category category, HttpServletRequest request, File baseFile, Integer parentId[], int index, int LEFTBAR_LAYOUT) {
		if (LEFTBAR_LAYOUT > index) {
			category.setSubCategories(getCategoryLinks(category.getId(), request));
			for (Category subcat : category.getSubCategories()) {
				if (baseFile.canRead()) {
					// check only if directory exists
					subcat.setHasLinkImage((new File(baseFile, "catlink_" + subcat.getId() + ".gif")).exists());
					subcat.setHasLinkImageOver((new File(baseFile, "catlink_over_" + subcat.getId() + ".gif")).exists());
				}
				if (subcat.getId().equals(parentId[index + 1])) {
					subcat.setSelected(true);
					getCategoryLinks(subcat, request, baseFile, parentId, index + 1, LEFTBAR_LAYOUT);
				}
			}
		}
	}

	public List<Category> getMainsAndSubsCategoryLinks(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		int LEFTBAR_LAYOUT = Integer.parseInt(siteConfig.get("LEFTBAR_LAYOUT").getValue());

		Integer parentId[] = { null, null, null, null };

		int cid = ServletRequestUtils.getIntParameter(request, "cid", -1);
		if (request.getAttribute("categoryId") != null) {
			cid = (Integer) request.getAttribute("categoryId");
		}
		String url = request.getServletPath();
		try {
			if (url.equals("/aboutus.jhtm"))
				cid = Integer.parseInt(siteConfig.get("ABOUT_US_ID").getValue());
			else if (url.equals("/contactus.jhtm"))
				cid = Integer.parseInt(siteConfig.get("CONTACT_US_ID").getValue());
		} catch (Exception e) {
		}

		if (cid > -1) {
			List<Category> categoryLinksTree = this.categoryDao.getCategoryLinksTree(cid);
			// if (!categoryLinksTree.isEmpty())
			// {
			// parentId[0] = categoryLinksTree.get( 0 ).getId();
			// if (categoryLinksTree.size() > 1)
			// {
			// parentId[1] = categoryLinksTree.get( 1 ).getId();
			// }
			// if (categoryLinksTree.size() > 2)
			// {
			// parentId[2] = categoryLinksTree.get( 2 ).getId();
			// }
			// if (categoryLinksTree.size() > 3) {
			// parentId[3] = categoryLinksTree.get(3).getId();
			// }
			// }
		}
		// multi store
		// Integer hostCategoryId = getHomePageByHost(request.getHeader("host"));
		// if (parentId[0] == null && hostCategoryId != null) {
		// if (getCategoryById(hostCategoryId, "").isShowSubcats()) {
		// parentId[0] = hostCategoryId;
		// }
		// }

		// File baseFile = new File( getServletContext().getRealPath( "/assets/Image/Category/" ) );
		// Properties prop = new Properties();
		// try
		// {
		// prop.load( new FileInputStream( getServletContext().getRealPath( "WEB-INF/properties/site.properties" ) ) );
		// if ( prop.get( "site.root" ) != null )
		// {
		// baseFile = new File( (String) prop.get( "site.root" ) + "/assets/Image/Category/" );
		// }
		// }
		// catch ( Exception e )
		// {}

		List<Category> categoryLinks = getCategoryLinks(parentId[0], request);
		Iterator<Category> clIter = categoryLinks.iterator();
		while (clIter.hasNext()) {
			Category category = clIter.next();
			// if ( baseFile.canRead() )
			// { // check only if directory exists
			// category.setHasLinkImage( (new File( baseFile, "catlink_" + category.getId() + ".gif" )).exists() );
			// category.setHasLinkImageOver( (new File( baseFile, "catlink_over_" + category.getId() + ".gif" )).exists() );
			// }
			// if (category.getId().equals( parentId[0] ) | category.getId().equals( parentId[1] ))
			// {
			category.setSelected(true);
			// if (LEFTBAR_LAYOUT == 3) {
			List<Category> subCategories = getCategoryLinks(category.getId(), request);
			category.setSubCategories(subCategories);
			// for (Category subcat: category.getSubCategories()) {
			// if (baseFile.canRead()) {
			// // check only if directory exists
			// subcat.setHasLinkImage((new File(baseFile, "catlink_" + subcat.getId() + ".gif")).exists());
			// subcat.setHasLinkImageOver((new File(baseFile, "catlink_over_" + subcat.getId() + ".gif")).exists());
			// }
			// if (subcat.getId().equals(parentId[2])) {
			// subcat.setSelected(true);
			// if (LEFTBAR_LAYOUT > 2) {
			// List<Category> subSubCategories = getCategoryLinks(subcat.getId(), request);
			// subcat.setSubCategories(subSubCategories);
			// for (Category subSub: subSubCategories) {
			// // if (baseFile.canRead()) {
			// // // check only if directory exists
			// // subSub.setHasLinkImage((new File(baseFile, "catlink_" + subSub.getId() + ".gif")).exists());
			// // subSub.setHasLinkImageOver((new File(baseFile, "catlink_over_" + subSub.getId() + ".gif")).exists());
			// // }
			// if (subSub.getId().equals(parentId[3])) {
			// subSub.setSelected(true);
			// }
			// }
			// }
			// }
			// }
			// }
			// }
		}
		return categoryLinks;
	}

	public void updateCategoryRanking(List<Map<String, Integer>> data) {
		this.categoryDao.updateRanking(data);
	}

	// RMA
	public Integer getLastOrderId(String serialNum) {
		return this.rmaDao.getLastOrderId(serialNum);
	}

	public void insertRma(Rma rma) {
		this.rmaDao.insertRma(rma);
	}

	public List<Rma> getRmaList(RmaSearch search) {
		return this.rmaDao.getRmaList(search);
	}

	public Rma getRma(Rma rma) {
		return this.rmaDao.getRma(rma);
	}

	public void updateRma(Rma rma) {
		this.rmaDao.updateRma(rma);
	}

	public LineItem getLineItem(int orderId, String serialNum) {
		return this.rmaDao.getLineItem(orderId, serialNum);
	}

	// inventory
	public void insertPurchaseOrder(PurchaseOrderForm purchaseOrderForm) {
		this.inventoryDao.insertPurchaseOrder(purchaseOrderForm);
	}

	public void updatePurchaseOrder(PurchaseOrderForm purchaseOrderForm) {
		this.inventoryDao.updatePurchaseOrder(purchaseOrderForm);
	}

	public PurchaseOrder getPurchaseOrder(int poId) {
		return this.inventoryDao.getPurchaseOrder(poId);
	}

	public void insertPurchaseOrderStatus(PurchaseOrderForm purchaseOrderForm, boolean inventoryHistory) {
		this.inventoryDao.insertPurchaseOrderStatus(purchaseOrderForm, inventoryHistory);
	}

	public List<PurchaseOrderStatus> getPurchaseOrderStatusHistory(int poId) {
		return this.inventoryDao.getPurchaseOrderStatusHistory(poId);
	}

	public List<PurchaseOrder> getPurchaseOrderList(PurchaseOrderSearch purchaseOrderSearch) {
		return this.inventoryDao.getPurchaseOrderList(purchaseOrderSearch);
	}
	
	public List<String>  getPoPTList(){
		return this.inventoryDao.getPoPTList();
	}
	
	public void updatePoPtById(int poId, String pt){
		this.inventoryDao.updatePoPtById(poId, pt);
	}
	
	public void updatePoStatusById(int poId, String pt){
		this.inventoryDao.updatePoStatusById(poId, pt);
	}

	public Integer getPurchaseOrderIdByName(String poNumber) {
		return this.inventoryDao.getPurchaseOrderIdByName(poNumber);
	}

	public void updateOnHand(String sku, Integer quantity) {
		Inventory inventory = new Inventory();
		inventory.setSku(sku);
		inventory = getInventory(inventory);
		if (inventory != null && inventory.getInventory() != null) {
			inventory.setSku(sku);
			inventory.setInventoryAFS(quantity);
			inventory.setInventory(0);
			updateInventory(inventory);
		}
	}

	public void updateInventory(List<LineItem> lineItems) {
		this.productDao.updateInventory(lineItems);
	}
	
	//get LineItem sku
	public String getLineItemSku(String sku){
		 return this.productDao.getLineItemSku(sku);
	}

	public void deductInventory(List<LineItem> lineItems) {
		this.productDao.deductInventory(lineItems);
	}

	public void addInventory(List<LineItem> lineItems) {
		this.productDao.addInventory(lineItems);
	}

	public void importInventory(InventoryActivity inventoryActivity) {
		this.inventoryDao.importInventory(inventoryActivity);
	}

	public int purchaseOrderCount(Integer orderId) {
		return this.inventoryDao.purchaseOrderCount(orderId);
	}

	public List<PurchaseOrderLineItem> getPOLineItemsQuantity(Integer orderId, boolean dropShip) {
		return this.inventoryDao.getPOLineItemsQuantity(orderId, dropShip);
	}

	public PurchaseOrderLineItem getLineItemByPOId(PurchaseOrderLineItem lineItem) {
		return this.inventoryDao.getLineItemByPOId(lineItem);
	}

	public void deletePurchaseOrder(Integer poId) {
		this.inventoryDao.deletePurchaseOrder(poId);
	}

	public void updateInventoryBySkus(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data) {
		this.inventoryDao.updateInventoryBySkus(csvFeedList, data);
	}

	public void adjustInventoryBySku(InventoryActivity inventoryActivity) {
		this.inventoryDao.adjustInventoryBySku(inventoryActivity);
	}

	public Inventory getInventory(Inventory inventory) {
		return this.inventoryDao.getInventory(inventory);
	}

	public void updateInventory(Inventory inventory) {
		this.inventoryDao.updateInventory(inventory);
	}

	// report
	public List<ImportExportHistory> getImportExportHistoryByType(ImportExportHistorySearch search) {
		return this.reportDao.getImportExportHistoryByType(search);
	}

	public void insertImportExportHistory(ImportExportHistory imEx) {
		this.reportDao.insertImportExportHistory(imEx);
	}

	public List<Report> getOrdersSaleReport(ReportFilter filter) {
		List<Report> salesReports = this.reportDao.getOrdersSaleReport(filter);
		for (int i = 0; i < 12; i++) {
			try {
				if (salesReports.get(i).getMonth() != i + 1) {
					Report r = new Report(i + 1, 0.0);
					salesReports.add(i, r);
				}
			} catch (IndexOutOfBoundsException e) {
				Report r = new Report(i + 1, 0.0);
				salesReports.add(i, r);
			}
		}
		return salesReports;
	}

	public List<Report> getOrdersSaleDetailReport(ReportFilter filter) {
		List<Report> salesDetailReports = this.reportDao.getOrdersSaleDetailReport(filter);
		for (int i = 0; i < 12; i++) {
			Integer activated = getNumCustomerActivated(filter.getYear(), i + 1);
			Integer retained = getNumCustomerRetained(filter.getYear(), i + 1);
			try {
				salesDetailReports.get(i).setNumCustomerRetained(retained);
				salesDetailReports.get(i).setNumCustomerActivated(activated);
				if (salesDetailReports.get(i).getMonth() != i + 1) {
					Report r = new Report(i + 1, 0.0, 0, 0, 0.0, 0.0, 0, 0.0, 0.0, retained, activated);
					salesDetailReports.add(i, r);
				}
			} catch (IndexOutOfBoundsException e) {
				Report r = new Report(i + 1, 0.0, 0, 0, 0.0, 0.0, 0, 0.0, 0.0, retained, activated);
				salesDetailReports.add(i, r);
			}
		}
		return salesDetailReports;
	}

	public List<MetricReport> getMetricReport(ReportFilter filter) {
		return this.reportDao.getMetricReport(filter);
	}

	public Integer getNumCustomerActivated(String year, int month) {
		return this.reportDao.getNumCustomerActivated(year, month);
	}

	public Integer getNumCustomerRetained(String year, int month) {
		return this.reportDao.getNumCustomerRetained(year, month);
	}

	public List<Report> getOrdersSaleReportDaily(ReportFilter filter) {
		return this.reportDao.getOrdersSaleReportDaily(filter);
	}

	public Integer getOrdersSaleReportDailyCount(ReportFilter filter) {
		return this.reportDao.getOrdersSaleReportDailyCount(filter);
	}

	public List<Report> getCustomerQuickViewPurchase(Integer userId) {
		List<Report> purchaseReports = this.reportDao.getCustomerQuickViewPurchase(userId);
		List<Report> purchaseReportsLastYear = this.reportDao.getCustomerQuickViewPurchaseLastYear(userId);
		for (int i = 0; i < 12; i++) {
			try {
				if (purchaseReports.get(i).getMonth() != i + 1) {
					Report r = new Report(i + 1, 0.0);
					purchaseReports.add(i, r);
				}
			} catch (IndexOutOfBoundsException e) {
				Report r = new Report(i + 1, 0.0);
				purchaseReports.add(i, r);
			}
		}
		return purchaseReports;
	}

	public List<Report> getCustomerQuickViewPurchaseLastYear(Integer userId) {

		List<Report> purchaseReportsLastYear = this.reportDao.getCustomerQuickViewPurchaseLastYear(userId);
		for (int i = 0; i < 12; i++) {
			try {
				if (purchaseReportsLastYear.get(i).getMonth() != i + 1) {
					Report r = new Report(i + 1, 0.0);
					purchaseReportsLastYear.add(i, r);
				}
			} catch (IndexOutOfBoundsException e) {
				Report r = new Report(i + 1, 0.0);
				purchaseReportsLastYear.add(i, r);
			}
		}
		return purchaseReportsLastYear;
	}

	public Integer getCountPendingProcessingOrder() {
		return this.reportDao.getCountPendingProcessingOrder();
	}

	public List<Order> getPendingProcessingOrder() {
		return this.reportDao.getPendingProcessingOrder();
	}

	public List<Integer> getInactiveCustomerIds(ReportFilter reportFilter) {
		return this.reportDao.getInactiveCustomerIds(reportFilter);
	}

	public List<CustomerReport> getInactiveCustomers(ReportFilter reportFilter) {
		return this.reportDao.getInactiveCustomers(reportFilter);
	}

	public List<Report> getTrackcodeReport(ReportFilter reportFilter) {
		return this.reportDao.getTrackcodeReport(reportFilter);
	}

	public List<Report> getSalesRepDetailReport(ReportFilter filter) {
		return this.reportDao.getSalesRepDetailReport(filter);
	}

	public List<Report> getSalesRepDetailReport2(ReportFilter filter) {

		return this.reportDao.getSalesRepDetailReport2(filter);
	}

	public List<SalesRepReport> getSalesRepReport(Integer salesRepId, ReportFilter salesRepFilter) {
		return this.reportDao.getSalesRepReport(salesRepId, salesRepFilter);
	}

	public List<CustomerReport> getCustomerReportOverview(ReportFilter filter) {
		return this.reportDao.getCustomerReportOverview(filter);
	}

	public List<SalesRepReport> getSalesRepReportDaily(Integer salesRepId, ReportFilter salesRepFilter) {
		return this.reportDao.getSalesRepReportDaily(salesRepId, salesRepFilter);
	}

	public List<EmailCampaign> getEmailCampaigns(ReportFilter reportFilter) {
		return this.reportDao.getEmailCampaigns(reportFilter);
	}

	public List<ProductReport> getProductListReport(ReportFilter reportFilter) {
		return this.reportDao.getProductListReport(reportFilter);
	}

	public int getProductListReportCount(ReportFilter reportFilter) {
		return this.reportDao.getProductListReportCount(reportFilter);
	}

	public List<Report> getPromoCodeReport(ReportFilter reportFilter) {
		return this.reportDao.getPromoCodeReport(reportFilter);
	}

	public List<InventoryActivity> getInventoryActivity(ReportFilter reportFilter) {
		return this.reportDao.getInventoryActivity(reportFilter);
	}

	public Integer getInventoryActivityCount(ReportFilter reportFilter) {
		return this.reportDao.getInventoryActivityCount(reportFilter);
	}

	public List<InventoryReport> getInventoryReport(ReportFilter reportFilter) {
		return this.reportDao.getInventoryReport(reportFilter);
	}

	public Integer getInventoryReportCount(ReportFilter reportFilter) {
		return this.reportDao.getInventoryReportCount(reportFilter);
	}

	public List<Product> getQuantityPending(List<Product> productList) {
		return this.productDao.getQuantityPending(productList);
	}
	
	public List<AbandonedShoppingCart> getAbandonedShoppingCartList(AbandonedShoppingCartSearch abandonedShoppingCartSearch) {
		return this.reportDao.getAbandonedShoppingCartList(abandonedShoppingCartSearch);
	}
	
	public List<TicketReport> getTicketReport(ReportFilter reportFilter) {
		return this.reportDao.getTicketReport(reportFilter);
	}
	
	public List<Report> getCustomerReportDaily(ReportFilter reportFilter) {
		return this.reportDao.getCustomeReportDaily(reportFilter);
	}
	
	public List<ProductReport> getBestSellingProducts(ReportFilter reportFilter) {
		return this.reportDao.getBestSellingProducts(reportFilter);
	}

	public List<ProductReport> getTopAbandonedProducts(ReportFilter reportFilter) {
		return this.reportDao.getTopAbandonedProducts(reportFilter);
	}
	
	public List<CityReport> getTopShippedCities(ReportFilter reportFilter) {
		return this.reportDao.getTopShippedCities(reportFilter);
	}

	public List<StateReport> getTopShippedStates(ReportFilter reportFilter) {
		return this.reportDao.getTopShippedStates(reportFilter);
	}
	
	public List<SalesRepReport> getTopSalesReps(ReportFilter reportFilter) {
		return this.reportDao.getTopSalesReps(reportFilter);
	}
	
	public AbandonedShoppingCartSearch getAbandonedShoppingCartSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		AbandonedShoppingCartSearch abandonedShoppingCartSearch = (AbandonedShoppingCartSearch) request.getSession().getAttribute("abandonedShoppingCartSearch");
		// 'emptyFilter' is used to reset all the filters to show the sub-accounts of 'parent'
		if (abandonedShoppingCartSearch == null || request.getParameter("emptyFilter") != null) {
			abandonedShoppingCartSearch = new AbandonedShoppingCartSearch();
			abandonedShoppingCartSearch.setPageSize(Integer.parseInt(siteConfig.get("DEFAULT_LIST_SIZE").getValue()));
			request.getSession().setAttribute("abandonedShoppingCartSearch", abandonedShoppingCartSearch);
		}
		// sales rep
		if (request.getParameter("sales_rep_id") != null) {
			abandonedShoppingCartSearch.setSalesRepId(ServletRequestUtils.getIntParameter(request, "sales_rep_id", -1));
		}
		
		
		// sorting
		if (request.getParameter("sort") != null) {
			abandonedShoppingCartSearch.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				abandonedShoppingCartSearch.setStartDate(df.parse(request.getParameter("startDate")));
			} catch (ParseException e) {
				abandonedShoppingCartSearch.setStartDate(null);
			}
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				abandonedShoppingCartSearch.setEndDate(df.parse(request.getParameter("endDate")));
			} catch (ParseException e) {
				abandonedShoppingCartSearch.setEndDate(null);
			}
		}
		
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				abandonedShoppingCartSearch.setPage(1);
			} else {
				abandonedShoppingCartSearch.setPage(page);
			}
		}

		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				abandonedShoppingCartSearch.setPageSize(10);
			} else {
				abandonedShoppingCartSearch.setPageSize(size);
			}
		}


		return abandonedShoppingCartSearch;
	}
	
	// supplier
	public List<Supplier> getSuppliers(Integer categoryId) {
		return this.supplierDao.getSuppliers(categoryId);
	}
	
	public void importSuppliers(List<Supplier> suppliers) {
		this.supplierDao.importSuppliers(suppliers);
	}
	
	public int supplierCount() {
		return this.supplierDao.supplierCount();
	}
	
	public List<Supplier> getSupplierExportList(int limit, int offset) {
		return this.supplierDao.getSupplierExportList(limit, offset);
	}

	public List<Supplier> getSuppliers(Search search) {
		return this.supplierDao.getSuppliers(search);
	}

	public Integer getProductsCountBySupplierId(Integer supplierId) {
		return this.supplierDao.getProductsCountBySupplierId(supplierId);
	}

	public Supplier getSupplierByUserid(Integer userId) {
		return this.supplierDao.getSupplierByUserid(userId);
	}

	public Integer getSupplierIdByCompany(String company) {
		return this.supplierDao.getSupplierIdByCompany(company);
	}

	public String getSupplierCompanyBySupplierId(Integer suppId) {
		return this.supplierDao.getSupplierCompanyBySupplierId(suppId);
	}

	public Integer getSupplierIdByAccountNumber(String accountNumber) {
		return this.supplierDao.getSupplierIdByAccountNumber(accountNumber);
	}

	public Supplier getSupplierByAccountNumber(String accountNumber) {
		return this.supplierDao.getSupplierByAccountNumber(accountNumber);
	}

	public Supplier getSupplierById(Integer supplierId) {
		return this.supplierDao.getSupplierById(supplierId);
	}

	public void insertSupplier(Supplier supplier) throws DataAccessException {
		this.supplierDao.insertSupplier(supplier);
	}

	public void updateSupplier(Supplier supplier) throws DataAccessException {
		this.supplierDao.updateSupplier(supplier);
	}

	public void updateSupplierById(Supplier supplier) throws DataAccessException {
		this.supplierDao.updateSupplierById(supplier);
	}

	public void updateSupplierByIdWithMarkup(Supplier supplier) throws DataAccessException {
		this.supplierDao.updateSupplierByIdWithMarkup(supplier);
	}

	public void updateSupplierStatusById(Integer id, boolean active) throws DataAccessException {
		this.supplierDao.updateSupplierStatusById(id, active);
	}

	public void deleteSupplierById(Integer supplierId) {
		this.supplierDao.deleteSupplierById(supplierId);
	}

	public List<Supplier> getProductSupplierListByOrderId(int orderId, String filter) {
		return this.supplierDao.getProductSupplierListByOrderId(orderId, filter);
	}

	public void nonTransactionSafeUpdateProductSupplier(final int supplierId, final List<Map<String, Object>> data) {
		this.supplierDao.updateProductSupplier(supplierId, data);
	}

	public List<Address> getAddressListBySupplierId(Integer supplierId) {
		return this.supplierDao.getAddressListBySupplierId(supplierId);
	}

	public Address getDefaultAddressBySupplierId(Integer supplierId) {
		return this.supplierDao.getDefaultAddressBySupplierId(supplierId);
	}

	public void updateSupplierAddress(Address address) {
		this.supplierDao.updateSupplierAddress(address);
	}

	public void insertSupplierAddress(Address address) {
		this.supplierDao.insertSupplierAddress(address);
	}

	public Address getSupplierAddressById(Integer addressId) {
		return this.supplierDao.getSupplierAddressById(addressId);
	}

	public void deleteSupplierAddress(Address address) {
		this.supplierDao.deleteSupplierAddress(address);
	}

	public void updateSupplierPrimeAddressBySupplierId(Address address) {
		this.supplierDao.updateSupplierPrimeAddressBySupplierId(address);
	}

	public Map<Integer, String> getSupplierNameMap(List<Integer> suppIdList) {
		return this.supplierDao.getSupplierNameMap(suppIdList);
	}

	// Orders
	public boolean isValidOrderId(String orderId) {
		return this.orderDao.isValidOrderId(orderId);
	}

	public List<LineItem> getLineItemByOrderId(Integer OrderId) {
		return this.orderDao.getLineItemByOrderId(OrderId);
	}

	public List<Order> getOrdersListByUser(OrderSearch search, boolean quote) {
		return this.orderDao.getOrdersListByUser(search, quote);
	}
	
	public Integer isThereAnOrderUseThisPromo(String promoCode) {
		return this.orderDao.isThereAnOrderUseThisPromo(promoCode);
	}
	
	public Integer isThereAnOrderUseThisPromoByCustomer(String promoCode, Integer customerId) {
		return this.orderDao.isThereAnOrderUseThisPromoByCustomer(promoCode, customerId);
	}
	
	public Map<Integer, Integer> getFirstOrderAllUser() {
		return this.orderDao.getFirstOrderAllUser();
	}
	
	public List<Order> getManifestsReportByUser(OrderSearch search, boolean quote) {
		return this.orderDao.getManifestsReportByUser(search, quote);
	}

	public List<Order> getQuotesListByUser(OrderSearch search) {
		return this.orderDao.getQuotesListByUser(search);
	}

	public Order getOrder(int orderId, String sort) {
		return this.orderDao.getOrder(orderId, sort);
	}

	public Integer getOrderIdByGoogleOrderId(Long googleOrderId) {
		return this.orderDao.getOrderIdByGoogleOrderId(googleOrderId);
	}

	public List<Order> getQBOrdersList(OrderSearch search, String sort) {
		return this.orderDao.getQBOrdersList(search, sort);
	}

	public List<Order> getOrdersList(OrderSearch search) {
		return this.orderDao.getOrdersList(search);
	}
	
	

	public int getOrdersListCount(OrderSearch search) {
		return this.orderDao.getOrdersListCount(search);
	}
	
	public int getOrdersCountByStatus(String  status) {
		return this.orderDao.getOrdersCountByStatus(status);
	}

	public OrderStatus getLatestStatusHistory(Integer orderId) {
		return this.orderDao.getLatestStatusHistory(orderId);
	}

	public List<Order> getOrdersListBySupplier(OrderSearch search) {
		return this.orderDao.getOrdersListBySupplier(search);
	}

	public List<ConsignmentReport> getConsignmentReport(Integer supplierId, String sort) {
		return this.reportDao.getConsignmentReport(supplierId, sort);
	}

	public List<ConsignmentReport> getConsignmentSales(ConsignmentReport salesReport) {
		return this.reportDao.getConsignmentSales(salesReport);
	}

	public List<CustomerCreditHistory> getCustomerCreditHistory(Integer supplierId, Integer userId) {
		return this.reportDao.getCustomerCreditHistory(supplierId, userId);
	}

	public Order getLastOrderByUserId(Integer userId) {
		return this.orderDao.getLastOrderByUserId(userId);
	}
	
	public Integer getCustomerOrdersNumber(Integer userId) {
		return this.orderDao.getCustomerOrdersNumber(userId);
	}

	// This is called when order is placed from Back- end
	public void insertOrder(Order order, OrderStatus orderStatus, boolean clearUserCart, boolean backendAddedOrder, int gAFFILIATE, Map siteConfig, Map gSiteConfig) {
		//if it is the first order of customer, enable is_first
		try{
			if(getCustomerOrdersNumber(order.getUserId()) == 0){
				order.setFirstOrder(true);
			}
		}catch(Exception e){	
		}
		if (gAFFILIATE > 0) {
			int numCommTable = Integer.parseInt(((Configuration) siteConfig.get("NUMBER_PRODUCT_COMMISSION_TABLE")).getValue());
			if (order.getPromo() != null) {
				Integer userId = this.getUserIdByPromoCode(order.getPromo().getTitle());
				Integer currentParent = this.getParentId(order.getUserId());
				if (userId != null && (currentParent == null || currentParent == 0))
					this.customerDao.updateEdge(order.getUserId(), userId);
			}
			computeCommission(order, this.getParentsId(order.getUserId(), gAFFILIATE), numCommTable);
		}
		
		this.orderDao.insertOrder(order, orderStatus, backendAddedOrder);

		// credit
		Customer customer = getCustomerById(order.getUserId());
		if ((order.getStatus() != "xq" && order.getStatus() != "x")
				&& ((order.getCreditUsed() != null && order.getCreditUsed() > 0) || (order.getPaymentMethod() != null && order.getPaymentMethod().equalsIgnoreCase("credit card")))) {

			// update customer credit
			if (order.getCreditUsed() != null && order.getCreditUsed() > 0) {
				CreditImp credit = new CreditImp();
				CustomerCreditHistory customerCredit = new CustomerCreditHistory();
				if (order.isBackendOrder()) {
					customerCredit = credit.updateCustomerCreditByOrderPayment(order, "payment", order.getCreditUsed(), orderStatus.getUsername());
				} else {
					customerCredit = credit.updateCustomerCreditByOrderPayment(order, "payment", order.getCreditUsed(), customer.getUsername());
				}
				updateCredit(customerCredit);
			}
			// adding payments
			if ((Boolean) gSiteConfig.get("gPAYMENTS")) {

				Collection<Order> invoices = new ArrayList<Order>();
				PaymentImp customerPaymentImp = new PaymentImp();
				invoices.add(order);
				Payment payment = new Payment();

				// add Payment (Back-end Order)
				if (order.isBackendOrder() && order.getCreditUsed() != null && order.getCreditUsed() > 0) {
					payment = customerPaymentImp.createPayemnts(customer, "VBA Credit", order.getCreditUsed(), "Customer used VBA Credit");
					insertCustomerPayment(payment, invoices);
				} else {
					// add Payment (Front-end Order)
					if (order.getPaymentMethod() != null
							&& ((order.getPaymentMethod().equalsIgnoreCase("credit card") && !((Configuration) siteConfig.get("CREDIT_AUTO_CHARGE")).getValue().equals("") && ((Configuration) siteConfig
									.get("CREDIT_AUTO_CHARGE")).getValue().equals("charge")) || (order.getCreditUsed() != null && order.getCreditUsed() > 0))) {

						// if customer paid by partial payments and one payment is CREDIT, then adding payment to credit amount.
						if (order.getCreditUsed() != null && (order.getCreditUsed() > 0 && order.getCreditUsed() < order.getGrandTotal())) {
							payment = customerPaymentImp.createPayemnts(getCustomerById(order.getUserId()), "VBA Credit", order.getCreditUsed(), "Auto generated: " + order.getOrderId()
									+ "/Paid using: Credit");
							for (Order invoice : invoices) {
								invoice.setPayment(order.getCreditUsed());
							}
							insertCustomerPayment(payment, invoices);
						}

						// EITHER ONE: if customer paid using his credit OR his credit card (only if it is 'CHARGE CARD').
						if ((order.getPaymentMethod().equalsIgnoreCase("credit card") && !((Configuration) siteConfig.get("CREDIT_AUTO_CHARGE")).getValue().equals("") && ((Configuration) siteConfig
								.get("CREDIT_AUTO_CHARGE")).getValue().equals("charge")) || order.getPaymentMethod().equalsIgnoreCase("vba")) {

							if (order.getPayment() == null) {
								order.setPayment(0.00);
							}

							if (order.getPaymentMethod().equalsIgnoreCase("credit card")) {
								payment = customerPaymentImp.createPayemnts(getCustomerById(order.getUserId()), "Front End", (order.getGrandTotal() - order.getPayment()),
										"Auto generated: " + order.getOrderId() + "/Paid using: Credit Card" + "/TransactionId:" + order.getCreditCard().getTransId());
							} else {
								payment = customerPaymentImp.createPayemnts(getCustomerById(order.getUserId()), "VBA Credit", (order.getGrandTotal() - order.getPayment()),
										"Auto generated: " + order.getOrderId() + "/Paid using: Credit");
							}

							for (Order invoice : invoices) {
								if (order.getPayment() != null) {
									invoice.setPayment(order.getGrandTotal() - order.getPayment());
								} else {
									invoice.setPayment(order.getGrandTotal());
								}
							}
							insertCustomerPayment(payment, invoices);
						}
					}
				}
			}
		}

		if (clearUserCart) {
			// clear shopping cart items from database
			deleteShoppingCart(order.getUserId(), order.getManufacturerName());
		}
		// deduct inventory on hand
		deductInventoryAFS(order);

		// payments
		if (order.getPayments() != null) {
			Collection<Order> invoices = new ArrayList<Order>();
			invoices.add(order);
			PaymentImp customerPaymentImp = new PaymentImp();

			for (Payment orderPayment : order.getPayments()) {
				if (orderPayment.getAmount() != null) {
					order.setPayment(orderPayment.getAmount());
					Payment payment = customerPaymentImp.createPayemnts(getCustomerById(order.getUserId()), orderPayment.getPaymentMethod(), orderPayment.getAmount(), orderPayment.getMemo());
 					payment.setDate(orderPayment.getDate());
					insertCustomerPayment(payment, invoices);
				}
			}
		}
		// check for the credit alert && (order.getAmountPaid() == null || (order.getAmountPaid() < order.getGrandTotal()))
		if ((Boolean) gSiteConfig.get("gPAYMENTS") && customer.getPaymentAlert() != null && customer.getPaymentAlert() > 0) {
			Double amountPaid = getOrderAmountPaid(order.getOrderId());
			if (amountPaid == null || (amountPaid != null && (order.getGrandTotal().compareTo(amountPaid)) != 0)) {
				Double balance = getCustomerOrderBalance(customer.getId(), null);
				if (balance != null && balance > customer.getPaymentAlert()) {
					order.setPaymentAlert(true);
					updateOrderPaymentAlert(order.isPaymentAlert(), order.getOrderId());
				}
			}
		}
	}

	public List getAffiliateChild() {
		return this.getAffiliateChild();
	}

	// This is called when order is placed from Front- end
	public void insertOrder(OrderForm orderForm, OrderStatus orderStatus, HttpServletRequest request, boolean clearUserCart) {
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map siteConfig = (Map) request.getAttribute("siteConfig");
		int gAFFILIATE = (Integer) gSiteConfig.get("gAFFILIATE");
		//if it is the first order of customer, enable is_first
		try{
			if(getCustomerOrdersNumber(orderForm.getOrder().getUserId()) == 0){
				orderForm.getOrder().setFirstOrder(true);
			}
		}catch(Exception e){	
		}
		
		insertOrder(orderForm.getOrder(), orderStatus, clearUserCart, false, gAFFILIATE, siteConfig, gSiteConfig);
	}

	public void generatePackingList(Order order, boolean backendAddedOrder, Date shipDate, boolean inventoryHistory) {
		PackingList packingList = new PackingList();
		packingList.setOrderId(order.getOrderId());
		packingList.setPackingListNumber(order.getOrderId() + "-0");
		if (shipDate != null) {
			packingList.setShipped(new Date());
		}
		if (backendAddedOrder) {
			for (LineItem lineItem : order.getInsertedLineItems()) {
				lineItem.setPackingListNumber(packingList.getPackingListNumber());
				lineItem.setProcessed(lineItem.getQuantity());
				packingList.getLineItems().add(lineItem);
			}
		} else {
			for (LineItem lineItem : order.getLineItems()) {
				lineItem.setPackingListNumber(packingList.getPackingListNumber());
				lineItem.setProcessed(lineItem.getQuantity());
				packingList.getLineItems().add(lineItem);
			}
		}
		// insert packingList & deduct inventory
		insertPackingList(packingList, order.getAccessUserId(), inventoryHistory);
	}

	public void deductInventoryAFS(Order order) {
		
		for (LineItem lineItem : order.getLineItems()) {
			if (lineItem.getProduct().getInventory() != null) {
				Inventory inventory = new Inventory();
				inventory.setInventoryAFS(lineItem.getQuantity() * (-1));
				inventory.setSku(lineItem.getProduct().getSku());
				this.inventoryDao.updateInventory(inventory);
				System.out.println("update inventory AFSafter Order placed" + inventory.getInventoryAFS());
				System.out.println("update inventory after Order placed" + inventory.getInventory());
				System.out.println("Order Id" + order.getOrderId());

				InventoryActivity inventoryActivity = new InventoryActivity();
				

				inventoryActivity.setSku(lineItem.getProduct().getSku());
				inventoryActivity.setReference(order.getOrderId().toString());
				inventoryActivity.setAccessUserId(order.getAccessUserId());
				inventoryActivity.setType("Order Placed");
				inventoryActivity.setQuantity(inventory.getInventoryAFS());
				inventory.setSku(lineItem.getProduct().getSku());
				inventory = this.getInventory(inventory);
				inventoryActivity.setInventory(inventory.getInventory());
			    this.addKitPartsHistory(inventoryActivity);
			}
		}
	}

	public void updateOrder(Order order) {
		this.orderDao.updateOrder(order);
	}

	public void cancelBuySafeBond(Order order, OrderStatus orderStatus) {
		this.orderDao.cancelBuySafeBond(order, orderStatus);
	}

	public void updateCreditCardPayment(Order order) {
		this.orderDao.updateCreditCardPayment(order);
	}

	public void insertCreditCardHolderAuthentication(Order order) {
		this.orderDao.insertCreditCardHolderAuthentication(order);
	}

	public void insertOrderStatus(OrderStatus orderStatus) {
		this.orderDao.insertOrderStatus(orderStatus);
	}
	
	public void insertOrderUserDueDate(Order order) {
		this.orderDao.insertOrderUserDueDate(order);
	}
	
	public void updateIsFirstOrder(Order order) {
		this.orderDao.updateIsFirstOrder(order);
	}
	
	public void updateOrderSubStatus(Order order) {
		this.orderDao.updateOrderSubStatus(order);
	}

	public boolean isUserUsedPromoCode(String promoCode, Integer userId) {
		return this.orderDao.isUserUsedPromoCode(promoCode, userId);
	}

	public List<String> getSKUsByUserId(Integer userId) {
		return this.orderDao.getSKUsByUserId(userId);
	}

	public int invoiceCount(OrderSearch search) {
		return this.orderDao.invoiceCount(search);
	}

	public List<Order> getInvoiceExportList(OrderSearch search, String sort) {
		return this.orderDao.getInvoiceExportList(search, sort);
	}

	public List<PackingList> getExportPackingList(PackingListSearch search) {
		return this.orderDao.getExportPackingList(search);
	}

	public void updatePdfOrder(Order order) {
		this.orderDao.updatePdfOrder(order);
	}

	public List<Integer> getCustomersWithOrderCount(OrderSearch search) {
		return this.orderDao.getCustomersWithOrderCount(search);
	}

	public List<String> getPackisListNumByOrderId(int orderId) {
		return this.orderDao.getPackisListNumByOrderId(orderId);
	}

	public void exportXMLInvoices(List<Order> ordersList, Map<Integer, SalesRep> salesRepMap, List<ProductField> productFields, PrintWriter pw, Set<Integer> orderIds, boolean paymentOption) {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MM/dd/yyyy");

		pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		pw.println("<orders>");
		Class<Product> product = Product.class;
		Method m = null;
		for (Order order : ordersList) {
			pw.println("<order>");
			pw.println("<orderId>" + order.getOrderId() + "</orderId>");
			pw.println("<orderType>" + order.getOrderType() + "</orderType>");
			pw.println("<orderDate>" + dateFormatter.format(order.getDateOrdered()) + "</orderDate>");
			pw.println("<email>" + order.getUserEmail() + "</email>");
			pw.println("<purchaseOrder>" + (order.getPurchaseOrder() != null ? order.getPurchaseOrder() : "") + "</purchaseOrder>");
			pw.println("<salesrepId>" + ((order.getSalesRepId() == null) ? "" : salesRepMap.get(order.getSalesRepId()).getName()) + "</salesrepId>");
			pw.println("<invoiceNote>" + (order.getInvoiceNote() != null ? StringEscapeUtils.escapeXml(order.getInvoiceNote()) : "") + "</invoiceNote>");
			pw.println("<tax>" + order.getTax() + "</tax>");
			pw.println("<promoAmount>" + order.getPromoAmount() + "</promoAmount>");
			pw.println("<grandTotal>" + order.getGrandTotal() + "</grandTotal>");
			pw.println("<billing>");
			pw.println("<billingFirstName>" + (order.getBilling().getFirstName() != null ? StringEscapeUtils.escapeXml(order.getBilling().getFirstName()) : "") + "</billingFirstName>");
			pw.println("<billingLastName>" + (order.getBilling().getLastName() != null ? StringEscapeUtils.escapeXml(order.getBilling().getLastName()) : "") + "</billingLastName>");
			pw.println("<billingPhone>" + order.getBilling().getPhone() + "</billingPhone>");
			pw.println("<billingCellPhone>" + (order.getBilling().getCellPhone() != null ? order.getBilling().getCellPhone() : "") + "</billingCellPhone>");
			pw.println("<billingFax>" + (order.getBilling().getFax() != null ? order.getBilling().getFax() : "") + "</billingFax>");
			pw.println("<billingEmail>" + (order.getBilling().getEmail() != null ? order.getBilling().getEmail() : "") + "</billingEmail>");
			pw.println("<billingCompany>" + (order.getBilling().getCompany() != null ? StringEscapeUtils.escapeXml(order.getBilling().getCompany()) : "") + "</billingCompany>");
			pw.println("<billingAddress1>" + (order.getBilling().getAddr1() != null ? StringEscapeUtils.escapeXml(order.getBilling().getAddr1()) : "") + "</billingAddress1>");
			pw.println("<billingAddress2>" + (order.getBilling().getAddr2() != null ? StringEscapeUtils.escapeXml(order.getBilling().getAddr2()) : "") + "</billingAddress2>");
			pw.println("<billingCity>" + (order.getBilling().getCity() != null ? StringEscapeUtils.escapeXml(order.getBilling().getCity()) : "") + "</billingCity>");
			pw.println("<billingState>" + order.getBilling().getStateProvince() + "</billingState>");
			pw.println("<billingZip>" + order.getBilling().getZip() + "</billingZip>");
			pw.println("<billingCountry>" + order.getBilling().getCountry() + "</billingCountry>");
			pw.println("</billing>");
			pw.println("<shipping>");
			pw.println("<shippingMethod>" + (order.getShippingMethod() != null ? StringEscapeUtils.escapeXml(order.getShippingMethod()) : "") + "</shippingMethod>");
			pw.println("<shippingCost>" + order.getShippingCost() + "</shippingCost>");
			pw.println("<shippingFirstName>" + (order.getShipping().getFirstName() != null ? StringEscapeUtils.escapeXml(order.getShipping().getFirstName()) : "") + "</shippingFirstName>");
			pw.println("<shippingLastName>" + (order.getShipping().getLastName() != null ? StringEscapeUtils.escapeXml(order.getShipping().getLastName()) : "") + "</shippingLastName>");
			pw.println("<shippingPhone>" + order.getShipping().getPhone() + "</shippingPhone>");
			pw.println("<shippingCellPhone>" + (order.getShipping().getCellPhone() != null ? order.getShipping().getCellPhone() : "") + "</shippingCellPhone>");
			pw.println("<shippingFax>" + (order.getShipping().getFax() != null ? order.getShipping().getFax() : "") + "</shippingFax>");
			pw.println("<shippingEmail>" + (order.getShipping().getEmail() != null ? order.getShipping().getEmail() : "") + "</shippingEmail>");
			pw.println("<shippingCompany>" + (order.getShipping().getCompany() != null ? StringEscapeUtils.escapeXml(order.getShipping().getCompany()) : "") + "</shippingCompany>");
			pw.println("<shippingAddress1>" + (order.getShipping().getAddr1() != null ? StringEscapeUtils.escapeXml(order.getShipping().getAddr1()) : "") + "</shippingAddress1>");
			pw.println("<shippingAdress2>" + (order.getShipping().getAddr2() != null ? StringEscapeUtils.escapeXml(order.getShipping().getAddr2()) : "") + "</shippingAdress2>");
			pw.println("<shippingCity>" + (order.getShipping().getCity() != null ? StringEscapeUtils.escapeXml(order.getShipping().getCity()) : "") + "</shippingCity>");
			pw.println("<shippingState>" + order.getShipping().getStateProvince() + "</shippingState>");
			pw.println("<shippingZip>" + order.getShipping().getZip() + "</shippingZip>");
			pw.println("<shippingCountry>" + order.getShipping().getCountry() + "</shippingCountry>");
			pw.println("</shipping>");
			pw.println("<items>");
			for (LineItem lineItem : order.getLineItems()) {
				pw.println("<item>");
				pw.println("<sku>" + (lineItem.getProduct().getSku() != null ? StringEscapeUtils.escapeXml(lineItem.getProduct().getSku()) : "") + "</sku>");
				pw.println("<name>" + (lineItem.getProduct().getName() != null ? StringEscapeUtils.escapeXml(lineItem.getProduct().getName()) : "") + "</name>");
				pw.println("<quantity>" + lineItem.getQuantity() + "</quantity>");
				if (!lineItem.getProductAttributes().isEmpty()) {
					pw.println("<options>");
					int attributeIndex = 0;
					for (ProductAttribute attribute : lineItem.getProductAttributes()) {
						++attributeIndex;
						pw.println("<option" + attributeIndex + ">");
						pw.println("<optionName>" + attribute.getOptionName() + "</optionName>");
						pw.println("<optionValue>" + attribute.getValueString() + "</optionValue>");
						pw.println("</option" + attributeIndex + ">");
					}
					pw.println("</options>");
				}
				pw.println("<unitPrice>" + lineItem.getUnitPrice() + "</unitPrice>");
				pw.println("<totalPrice>" + lineItem.getTotalPrice() + "</totalPrice>");
				if (productFields != null && productFields.size() > 0) {
					lineItem.setProductFields(productFields);
					for (ProductField field : productFields) {
						try {
							m = product.getMethod("getField" + field.getId());
							pw.println("<field_" + field.getId() + ">" + (m.invoke(lineItem.getProduct()) == null ? "" : StringEscapeUtils.escapeXml(m.invoke(lineItem.getProduct()).toString()))
									+ "</field_" + field.getId() + ">");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				pw.println("</item>");
			}
			pw.println("</items>");
			if (paymentOption) {
				pw.println("<payment>");
				if (order.getPaymentMethod().equalsIgnoreCase("Credit Card") && order.getCreditCard() != null) {
					pw.println("<creditcard>");
					pw.println("<creditCardType>" + order.getCreditCard().getType() + "</creditCardType>");
					pw.println("<creditCardNumber>" + order.getCreditCard().getNumber() + "</creditCardNumber>");
					pw.println("<creditCardCode>" + order.getCreditCard().getCardCode() + "</creditCardCode>");
					pw.println("<creditCardExpireDate>" + order.getCreditCard().getExpireMonth() + "/" + order.getCreditCard().getExpireYear() + "</creditCardExpireDate>");
					pw.println("</creditcard>");
				} else if (order.getPaymentMethod().equalsIgnoreCase("paypal") && order.getPaypal() != null) {
					pw.println("<paypal>");
					pw.println("<transID>" + order.getPaypal().getTxn_id() + "</transID>");
					pw.println("<status>" + order.getPaypal().getPayment_status() + "</status>");
					pw.println("<transDate>" + order.getPaypal().getPayment_date() + "</transDate>");
					pw.println("</paypal>");
				}
				pw.println("</payment>");
			}
			pw.println("</order>");

			if (orderIds != null) {
				orderIds.add(order.getOrderId());
			}
		}
		pw.println("</orders>");

		pw.println();
		pw.close();
	}

	public void updateInvoiceExported(Set<Integer> orderIds) {
		this.orderDao.updateInvoiceExported(orderIds);
	}

	public Double getCustomerOrderBalance(Integer userId, Date dateOrdered) {
		return this.orderDao.getCustomerOrderBalance(userId, dateOrdered);
	}

	public Double getOrderAmountPaid(Integer orderId) {
		return this.orderDao.getOrderAmountPaid(orderId);
	}

	public void updateOrderPaymentAlert(boolean paymentAlert, Integer orderId) {
		this.orderDao.updateOrderPaymentAlert(paymentAlert, orderId);
	}

	public List<CustomerBudgetPartner> getCustomerPartnerHistory(CustomerBudgetPartner partner) {
		return this.orderDao.getCustomerPartnerHistory(partner);
	}

	public void updateCustomerPartnerHistory(CustomerBudgetPartner partner) {
		this.orderDao.updateCustomerPartnerHistory(partner);
	}

	// eBizCharge update customer ID
	public void updateGatewayToken(String gatewayToken, Integer id) {
		this.orderDao.updateGatewayToken(gatewayToken, id);
	}

	// Subscriptions
	public List<Subscription> getDueSubscriptions() {
		return this.subscriptionDao.getDueSubscriptions();
	}

	public List<Subscription> getSubscriptions(SubscriptionSearch search) {
		return this.subscriptionDao.getSubscriptions(search);
	}

	public Subscription getSubscription(String code) {
		return this.subscriptionDao.getSubscription(code);
	}

	public void updateSubscription(Subscription subscription) {
		this.subscriptionDao.updateSubscription(subscription);
	}

	public void deleteSubscription(Subscription subscription) {
		this.subscriptionDao.deleteSubscription(subscription);
	}

	// packing List
	public void insertPackingList(PackingList packingList, Integer accessUserId, boolean inventoryHistory) {
		this.orderDao.insertPackingList(packingList, accessUserId, inventoryHistory);
	}
	
	public void insertPackingList(PackingList packingList, Integer accessUserId, boolean inventoryHistory, Order order) {
		this.orderDao.insertPackingList(packingList, accessUserId, inventoryHistory, order);
	}

	public List<PackingList> getPackingListByOrderId(PackingListSearch packingListSearch) {
		return this.orderDao.getPackingListByOrderId(packingListSearch);
	}

	public PackingList getPackingList(Integer orderId, String packingNumber) {
		return this.orderDao.getPackingList(orderId, packingNumber);
	}

	public void updatePackingList(PackingList packingList) {
		this.orderDao.updatePackingList(packingList);
	}

	public void ensurePackingListShipDate(Order order, boolean inventoryHistory, Integer accessUserId) {
		this.orderDao.ensurePackingListShipDate(order, inventoryHistory, accessUserId);
	}

	public void updateLineItemOnHand(PackingList packingList, boolean inventoryHistory, Integer accessUserId) {
		this.orderDao.updateLineItemOnHand(packingList, inventoryHistory, accessUserId);
	}

	public void productIventoryHistory(InventoryActivity inventoryActivity) {
		this.orderDao.productIventoryHistory(inventoryActivity);
	}

	public List<LineItem> getPackingListLineItemsQuantity(Integer orderId) {
		return this.orderDao.getPackingListLineItemsQuantity(orderId);
	}

	public List<LineItem> getPackingListLineItemsByPackingNum(String packingNumber) {
		return this.orderDao.getPackingListLineItemsByPackingNum(packingNumber);
	}

	public List<LineItem> getNotProcessedLineItems(Integer orderId, String packingNumber) {
		Order order = this.orderDao.getOrder(orderId, "");
		//
		/**
		 * Iterator<LineItem> lineItemsIterator = order.getLineItems().iterator(); while ( lineItemsIterator.hasNext() ) { LineItem lineItem = lineItemsIterator.next(); //lineItem.setQuantity( lineItem.getQuantity() - lineItem.getProcessed() ); if ( lineItem.getQuantity() -
		 * lineItem.getProcessed() == 0 ) { lineItemsIterator.remove(); continue; } }
		 */

		if (packingNumber != null) {
			PackingList packingList = getPackingList(orderId, packingNumber);
			Iterator<LineItem> lineItemsIterator = order.getLineItems().iterator();
			while (lineItemsIterator.hasNext()) {
				LineItem orderLineItem = lineItemsIterator.next();
				for (LineItem plLineItem : packingList.getLineItems()) {
					if (orderLineItem.getLineNumber() == plLineItem.getLineNumber()) {
						orderLineItem.setToBeShipQty(plLineItem.getQuantity());
						continue;
					}
				}
			}
		}

		return order.getLineItems();

	}

	public int packingListCount(Integer orderId) {
		return this.orderDao.packingListCount(orderId);
	}

	public void deletePackingList(Integer orderId, String packingNumber, Integer accessUserId, boolean inventoryHistory) {
		this.orderDao.deletePackingList(orderId, packingNumber, accessUserId, inventoryHistory);
	}

	public Date getShipDateByOrderId(Integer orderId) {
		return this.orderDao.getShipDateByOrderId(orderId);
	}

	// cart
	public String updateCart(Cart cart, HttpServletRequest request) {
		return updateCart(cart, (Map<String, Object>) request.getAttribute("gSiteConfig"), (Map<String, Configuration>) request.getAttribute("siteConfig"), RequestContextUtils.getLocale(request)
				.getLanguage(), request.getHeader("host"), null);
	}

	public String updateCart(Cart cart, Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig, String lang, String host, Map<String, Object> model) {
		cart.setContinueCart(true);
		cart.setMinIncMessage(null);
		// multi store
		List<MultiStore> multiStores = getMultiStore(host);
		MultiStore multiStore = null;
		if (multiStores.size() > 0 && (Integer) gSiteConfig.get("gMULTI_STORE") > 0) {
			multiStore = multiStores.get(0);
		}
		String message = null;
		Map<String, Integer> crossItems = new HashMap<String, Integer>();
		List<CartItem> cartItems = cart.getCartItems();
		Iterator<CartItem> iter = cartItems.iterator();
		Customer customer = this.getCustomerById(cart.getUserId());
		for (CartItem cartItem : cartItems) {
			Product product = null;
			if(cartItem!=null && cartItem.getProduct()!=null){
				product = getProductById(cartItem.getProduct().getId(), 0, false, null);
			}
			if (product!=null && product.getCrossItemsCode() != null && product.getCrossItemsCode().trim().length() > 0) {
				String crossItemsCode = product.getCrossItemsCode().toLowerCase().trim();
				cartItem.getProduct().setCrossItemsCode(crossItemsCode);
				if (crossItems.containsKey(crossItemsCode)) {
					crossItems.put(crossItemsCode, crossItems.get(crossItemsCode) + cartItem.getQuantity());
				} else {
					crossItems.put(crossItemsCode, cartItem.getQuantity());
				}
			}
		}

		boolean checkEndQty = !gSiteConfig.get("gASI").equals("") && siteConfig.get("ASI_ENDQTYPRICING").getValue().equals("true")
				&& siteConfig.get("ASI_ENDQTYPRICING_CUSTOMER").getValue().equals("true");

		while (iter.hasNext()) {
			CartItem existingCartItem = iter.next();
			if (existingCartItem!=null && existingCartItem.getProduct()!=null && existingCartItem.getProduct().getId() == null) {

				// custom frame
				if (existingCartItem.getCustomXml() != null) {
					// weight - hardcode to 5 lbs
					existingCartItem.getProduct().setWeight(5.0);
					existingCartItem.setUnitWeight(5.0);
				}

				// custom item
				continue;
			}

			boolean basedOnHand = false;
			if ((Boolean) gSiteConfig.get("gINVENTORY") && siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("fly")) {
				basedOnHand = true;
			}

			// get Product
			try {
				if(existingCartItem!=null && existingCartItem.getProduct()!=null){
				
				Product product = getProductByIdPrivate(existingCartItem.getProduct().getId(), gSiteConfig, siteConfig, customer, null, lang, (existingCartItem.getPriceCasePackQty() != null ? true
						: false), basedOnHand);
				existingCartItem.getProduct().setName(product.getName());
				// append extra field name to the name of sku that uniquely identifies the
				// child sku, if all child sku has same name
				if (existingCartItem.getItemGroup() != null && existingCartItem.isItemGroupMainItem()) {
					Integer masterSkuProductFieldId = (siteConfig.get("MASTER_SKU_PRODUCT_FIELD").getValue() != null && !siteConfig.get("MASTER_SKU_PRODUCT_FIELD").getValue().trim().isEmpty()) ? Integer
							.parseInt(siteConfig.get("MASTER_SKU_PRODUCT_FIELD").getValue()) : 1;
					String masterSkuProductField = this.getProductFieldById(existingCartItem.getProduct().getId(), masterSkuProductFieldId);
					if (masterSkuProductField != null && !masterSkuProductField.isEmpty()) {
						existingCartItem.getProduct().setName(existingCartItem.getProduct().getName() + "-" + masterSkuProductField);
					}
				}
				existingCartItem.getProduct().setSku(product.getSku());
				existingCartItem.getProduct().setMasterSku(product.getMasterSku());
				existingCartItem.getProduct().setFeed(product.getFeed());
				existingCartItem.getProduct().setPrice(product.getPrice());

				// temp fix for price case pack qty, if product is updated and pricecasepackqty is different than saved value on cart database.
				// We might need to remove this and force customers to click on "Do Not Retain Cart"
				if (product.isPriceCasePack()) {
					existingCartItem.setPriceCasePackQty(product.getPriceCasePackQty());
				}

				// product variant
				if (existingCartItem.getVariantSku() != null) {
					List<Product> variants = getProductVariant(existingCartItem.getProduct().getId(), existingCartItem.getVariantSku(), false);
					if (variants.isEmpty()) {
						message = "shoppingcart.product.deleted";
						iter.remove();
						continue;
					}

					Product variant = variants.get(0);
					existingCartItem.getProduct().setSku(variant.getSku());
					existingCartItem.getProduct().setShortDesc(variant.getShortDesc());

					if (variant.getName() != null) {
						existingCartItem.getProduct().setName(variant.getName());
					}

					// price
					List<Price> prices = new ArrayList<Price>();
					prices.add(new Price(variant.getPrice1(), null, null, null, null, null));
					existingCartItem.getProduct().setPrice(prices);
				}

				existingCartItem.getProduct().setSalesTag(product.getSalesTag());
				Double weight = product.getWeight();
				if ((Boolean) gSiteConfig.get("gMASTER_SKU") && (weight == null || weight == 0) && product.getMasterSku() != null && product.getMasterSku().length() > 0) {
					// get master sku's weight
					weight = getProductWeightBySku(product.getMasterSku());
					if (weight == null) {
						weight = 0.0;
					}
				}
				existingCartItem.getProduct().setWeight(weight);
				existingCartItem.setUnitWeight(weight);

				// package dimensions (for shipping calculations)
				existingCartItem.getProduct().setField7(product.getField7());
				existingCartItem.getProduct().setPackageH(product.getPackageH());
				existingCartItem.getProduct().setPackageW(product.getPackageW());
				existingCartItem.getProduct().setPackageL(product.getPackageL());
				existingCartItem.getProduct().setPacking(product.getPacking());
				existingCartItem.getProduct().setUpsMaxItemsInPackage(product.getUpsMaxItemsInPackage());
				existingCartItem.getProduct().setUspsMaxItemsInPackage(product.getUspsMaxItemsInPackage());
				existingCartItem.getProduct().setCaseContent(product.getCaseContent());
				existingCartItem.getProduct().setPriceCasePack(product.isPriceCasePack());
				existingCartItem.getProduct().setPriceCasePackQty(product.getPriceCasePackQty());
				existingCartItem.getProduct().setTemperature(product.getTemperature());
				existingCartItem.getProduct().setTaxable(product.isTaxable());
				existingCartItem.getProduct().setPriceByCustomer(product.isPriceByCustomer());
				existingCartItem.getProduct().setBoxSize(product.getBoxSize());
				existingCartItem.getProduct().setBoxExtraAmt(product.getBoxExtraAmt());
				existingCartItem.getProduct().setLoyaltyPoint(product.getLoyaltyPoint());
				existingCartItem.getProduct().setCustomShippingEnabled(product.isCustomShippingEnabled());
				existingCartItem.getProduct().setCustomLineCharacter(product.getCustomLineCharacter());
				existingCartItem.getProduct().setInventory(product.getInventory());
				existingCartItem.getProduct().setInventoryAFS(product.getInventoryAFS());
				existingCartItem.getProduct().setNegInventory(product.isNegInventory());
				existingCartItem.getProduct().setMinimumQty(product.getMinimumQty());
				existingCartItem.getProduct().setIncrementalQty(product.getIncrementalQty());
				existingCartItem.getProduct().setDefaultSupplierId(product.getDefaultSupplierId());
				existingCartItem.getProduct().setManufactureName(product.getManufactureName());
				if (product.getRecommendedList() != null) {
					existingCartItem.getProduct().setRecommendedList(product.getRecommendedList());
				}
				if (existingCartItem.getSubscriptionInterval() != null) {
					existingCartItem.getProduct().setSubscriptionDiscount(product.getSubscriptionDiscount());
				}
				if (siteConfig.get("SHOW_IMAGE_ON_INVOICE").getValue().equals("true")) {
					existingCartItem.getProduct().setThumbnail(product.getThumbnail());
				}

				if (product.getCommissionTables() != null) {
					ProductAffiliateCommission commissionTables = new ProductAffiliateCommission();
					commissionTables.setCommissionTable1(product.getCommissionTables().getCommissionTable1());
					commissionTables.setCommissionTable2(product.getCommissionTables().getCommissionTable2());
					commissionTables.setCommissionTable3(product.getCommissionTables().getCommissionTable3());
					commissionTables.setCommissionTable4(product.getCommissionTables().getCommissionTable4());
					commissionTables.setCommissionTable5(product.getCommissionTables().getCommissionTable5());
					commissionTables.setCommissionTable6(product.getCommissionTables().getCommissionTable6());
					commissionTables.setCommissionTable7(product.getCommissionTables().getCommissionTable7());
					commissionTables.setCommissionTable8(product.getCommissionTables().getCommissionTable8());
					commissionTables.setCommissionTable9(product.getCommissionTables().getCommissionTable9());
					commissionTables.setCommissionTable10(product.getCommissionTables().getCommissionTable10());

					existingCartItem.getProduct().setCommissionTables(commissionTables);
				}
				// get product attributes
				if (existingCartItem.getProductAttributes() != null) {
					Iterator<ProductAttribute> prodAttrIter = existingCartItem.getProductAttributes().iterator();

					Map<String, List<ProductOption>> productOptionsMap = new HashMap<String, List<ProductOption>>();
					while (prodAttrIter.hasNext()) {
						ProductAttribute attribute = prodAttrIter.next();
						if (attribute.isCustomAttribute()) {
							continue;
						}

						if (!productOptionsMap.containsKey(attribute.getOptionCode())) {
							productOptionsMap.put(attribute.getOptionCode(), getProductOptionsByOptionCode(attribute.getOptionCode(), false, product.getSalesTag(), null));
						}
						try {
							if (!attribute.isDealItem()) {
								attribute.setOptionName(productOptionsMap.get(attribute.getOptionCode()).get(attribute.getOptionIndex()).getName());
							}
						} catch (Exception e) {
							// happens when option code is changed/cleared from product via excel import
							prodAttrIter.remove();
							continue;
						}
						try {
							if (!attribute.isDealItem()) {
								ProductOptionValue productOptionValue = productOptionsMap.get(attribute.getOptionCode()).get(attribute.getOptionIndex()).getValues().get(attribute.getValueIndex());
								attribute.setOptionPriceOriginal(productOptionValue.getOptionPriceOriginal());
								attribute.setOptionPrice(productOptionValue.getOptionPrice());
								attribute.setOneTimePrice(productOptionValue.isOneTimePrice());
								if (attribute.isOneTimePrice()) {
									attribute.setValueName(productOptionValue.getName() + " $" + new DecimalFormat("#,###.00").format(attribute.getOptionPrice()));
								} else {
									attribute.setValueName(productOptionValue.getName());
								}
								attribute.setOptionWeight(productOptionValue.getOptionWeight());
								attribute.setImageUrl(productOptionValue.getImageUrl());
							}
						} catch (Exception e) {
							// do nothing - customText and boxContent does not have optionValue
						}
					}
					// pre-hanging
					if (gSiteConfig.get("gSITE_DOMAIN") != null && gSiteConfig.get("gSITE_DOMAIN").equals("doors2gostore.com") && product.getField1() != null
							&& product.getField1().toLowerCase().contains("door")) {
						boolean isPreHanging = false;
						String species = null, widthHeight = null, jamb = null;
						for (ProductAttribute attribute : existingCartItem.getProductAttributes()) {
							if (attribute.getOptionName().equalsIgnoreCase("Pre-Hanging"))
								isPreHanging = attribute.getValueIndex() == 1;
							if (attribute.getOptionName().equalsIgnoreCase("Species"))
								species = attribute.getValueName();
							if (attribute.getOptionName().equalsIgnoreCase("Width & Height"))
								widthHeight = attribute.getValueName();
							if (attribute.getOptionName().equalsIgnoreCase("Jamb"))
								jamb = attribute.getValueName();
						}
						List<Map<String, Object>> preHangList = getPreHanging(product.getField1(), species, widthHeight, null);
						if (isPreHanging && preHangList.size() > 0) {
							ProductAttribute preHang = new ProductAttribute();
							preHang.setOptionName("Net Frame Dimension");
							preHang.setValueName((String) preHangList.get(0).get("dimensions"));
							preHang.setOptionPrice((Double) preHangList.get(0).get("price"));
							if (jamb != null && jamb.contains("10%") && preHang.getOptionPrice() != null) {
								preHang.setOptionPrice(preHang.getOptionPrice() * 1.1); // add 10%
							}
							existingCartItem.addProductAttribute(preHang);
						}
					}
				}
				if (!checkCartItemQty(iter, existingCartItem, product, customer, siteConfig, gSiteConfig, crossItems)) {
					cart.setContinueCart(false);
					if (existingCartItem.getMinimumIncrementalQtyMessage() != null) {
						cart.setMinIncMessage("f_cartMinIncMessage");
					}
				}
				
				}

			} catch (Exception e) {
				message = "shoppingcart.product.deleted";
				iter.remove();
			}
		}
		// set unit prices
		iter = cartItems.iterator();
		while (iter.hasNext()) {
			CartItem cartItem = iter.next();
			if (cartItem.getAsiUnitPrice() != null) {
				if (cartItem.getAsiOriginalPrice() == null) {
					// for backward integration where asi original price is null;
					// this if statement can be removed after all ASI sites upgraded to version 6.1.6 or later.
					cartItem.setUnitPrice(cartItem.getAsiUnitPrice());
				} else {
					if (cartItem.getProduct().getSalesTag() != null) {
						if (cartItem.getProduct().getSalesTag().isInEffect()) {
							cartItem.setUnitPrice(cartItem.getAsiUnitPrice());
						} else {
							cartItem.setUnitPrice(cartItem.getAsiOriginalPrice());
						}
					} else {
						cartItem.setUnitPrice(cartItem.getAsiOriginalPrice());
					}
				}
				continue;
			}
			if (cartItem.getProduct()!=null && checkEndQty) {
				cartItem.getProduct().setEndQtyPricing(isEndQtyPricing(cartItem.getProduct().getSku()));
			}
			int quantity = cartItem.getQtyForPriceBreak() != null ? cartItem.getQtyForPriceBreak() : cartItem.getQuantity();
			if (cartItem.getProduct()!=null && cartItem.getProduct().getCrossItemsCode() != null && !cartItem.getProduct().getCrossItemsCode().equalsIgnoreCase("")) {
				quantity = crossItems.get(cartItem.getProduct().getCrossItemsCode());
			}
			cartItem.setCrossItemsQty(quantity);
			try {
				cartItem.setUnitPrice(Cart.unitPriceByQty(quantity, cartItem.getProduct()));
			} catch (Exception e) {
				message = "shoppingcart.product.deleted";
				iter.remove();
			}
		}

		// Login required to see deal items in order
		if ((Integer) gSiteConfig.get("gDEALS") > 0) {
			cartItems = this.updateCartItemsWithDeals(cartItems, cart, gSiteConfig, siteConfig, lang, customer);
			if (cart.getUserId() != null) {
				arrangeCartItems(cartItems);
			}
			for(CartItem cartItem : cartItems){
				if(cartItem.getDiscount()<0){
					cart.setHasParentDeal(true);
					break;
				}
			}
		}
		if (!checkCartMinimumOrder(customer, cart, siteConfig, gSiteConfig, multiStore)) {
			cart.setContinueCart(false);
		}
		return message;
	}

	public Double unitPriceByQty(int qty, Product product) {
		Double unitPriceByQty = null;

		Iterator<Price> iter = product.getPrice().iterator();
		while (iter.hasNext()) {
			Price price = iter.next();
			if (price.getDiscountAmt() == null) {
				unitPriceByQty = price.getAmt();
			} else {
				unitPriceByQty = price.getDiscountAmt();
			}
			if (!product.isEndQtyPricing() && (price.getQtyTo() == null || (price.getQtyTo().intValue() >= qty))) {
				break;
			}
		}
		if (unitPriceByQty != null && product.getSubscriptionDiscount() != null) {
			unitPriceByQty = unitPriceByQty - (unitPriceByQty * product.getSubscriptionDiscount() / 100);
		}

		return unitPriceByQty;
	}

	public List<CartItem> updateCartItemsWithDeals(List<CartItem> cartItems, Cart cart, Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig, String lang, Customer customer) {
		DealSearch dealSearch = new DealSearch();
		dealSearch.setActiveOnly(true);
		//get normal deals(without check parent sku).
		dealSearch.setParentSku(false);
		List<Deal> deals = getDealList(dealSearch);
		
		//get parent sku deal
		DealSearch dealParentSearch = new DealSearch();
		dealParentSearch.setActiveOnly(true);
		dealParentSearch.setParentSku(true);
		List<Deal> parentDeals = getDealList(dealParentSearch);
		
		Map<String, Integer> getTotalQuantity = new HashMap<String, Integer>();
		List<CartItem> removeCartItems = new ArrayList<CartItem>();
		
		// get total quantity per item
		for (CartItem cartItem : cartItems) {
			cartItem.setOriginalPrice(cartItem.getUnitPrice());
			cartItem.setDiscount(0.0);
			cartItem.setDealMainItem(false);
			int quant = cartItem.getQuantity();
			if (getTotalQuantity.get(cartItem.getProduct().getSku()) != null) {
				quant = quant + getTotalQuantity.get(cartItem.getProduct().getSku());
			}
			getTotalQuantity.put(cartItem.getProduct().getSku(), quant);

			if (cartItem.getProductAttributes() != null) {
				List<ProductAttribute> productAttrList = cartItem.getProductAttributes();
				Iterator<ProductAttribute> iter = productAttrList.iterator();
				while (iter.hasNext()) {
					ProductAttribute attr = iter.next();
					if (attr.isDealItem()) {
						if (getTotalQuantity.get(cartItem.getProduct().getSku()) == cartItem.getQuantity()) {
							iter.remove();
							continue;
						}
						removeCartItems.add(cartItem);
						break;
					}
				}
			}
		}

		// remove deal item
		for (CartItem cartItem : removeCartItems) {
			cartItems.remove(cartItem);
			removeCartItem(cartItem.getIndex(), cartItem.getUserId());
		}
		
		
		// Based on differnt deal type to edit lineItem
		if(deals != null){
			Map<String, Object> editCartItem = new HashMap<String, Object>();
			List<String> skuList = new ArrayList<String>();
			List<CartItem> addCartItems = new ArrayList<CartItem>();
			Map<String, Deal> dealsBySku = new HashMap<String, Deal>();
			double subTotal = cart.getSubTotal();
			
			

			// put sku last in case of deal on subtotal
			for (Deal deal : deals) {
				dealsBySku.put(deal.getGetSku(), deal);
				if (deal.getBuyAmount() != null) {
					for (int i = 0; i < cartItems.size(); i++) {
						if (cartItems.get(i).getProduct().getSku().equalsIgnoreCase(deal.getGetSku())) {
							CartItem tempCartItem = cartItems.get(i);

							if (cart.getUserId() != null) {
								try {
									cartItems.remove(i);
									cartItems.add(cartItems.size(), tempCartItem);
								} catch (Exception e) {
								}
							} else {
								try {
									cart.removeCartItem(i);
									cart.addCartItem(tempCartItem);
								} catch (Exception e) {
								}
							}

						}
					}
				}
			}
			// edit, add, delete cart item
			for (CartItem cartItem : cartItems) {
				
				if (skuList.contains(cartItem.getProduct().getSku())) {
					continue;
				}
				skuList.add(cartItem.getProduct().getSku());
				Deal deal = dealsBySku.get(cartItem.getProduct().getSku());
				if (deal == null) {
					continue;
				}
				editCartItem =  validateCartItem(cartItems, deal, subTotal, cartItem, getTotalQuantity);

				int itemTotalQuant = Integer.parseInt(getTotalQuantity.get(cartItem.getProduct().getSku()).toString());
				int itemQualifiedQuant = Integer.parseInt(editCartItem.get("qualifiedQuantity").toString());
				Double discountedPrice = Double.parseDouble(editCartItem.get("price").toString());

				if (itemTotalQuant > 0 && itemQualifiedQuant > 0) {
					List<ProductAttribute> productAttrList = new ArrayList<ProductAttribute>();
					ProductAttribute productAttr = new ProductAttribute();
					productAttr.setOptionIndex(100);
					productAttr.setOptionName("Deal");
					productAttr.setValueIndex(100);
					productAttr.setValueName(editCartItem.get("title").toString());
					productAttr.setDealItem(true);
					productAttrList.add(productAttr);

					if (itemTotalQuant <= itemQualifiedQuant) {
						cartItem.setUnitPrice(discountedPrice);
						cartItem.setQuantity(itemTotalQuant);
						if (cartItem.getProductAttributes() != null) {
							cartItem.getProductAttributes().add(productAttr);
						} else {
							cartItem.setProductAttributes(productAttrList);
						}
						updateCartItem(cartItem);
						subTotal = subTotal - (cartItem.getUnitPrice() - discountedPrice) * cartItem.getQuantity();
					}
					if (itemTotalQuant > itemQualifiedQuant) {
						cartItem.setQuantity(itemTotalQuant - itemQualifiedQuant);
						cartItem.setDealMainItem(true);
						updateCartItem(cartItem);

						CartItem newCartItem = new CartItem();
						newCartItem.setProduct(getProductByIdPrivate(cartItem.getProduct().getId(), gSiteConfig, siteConfig, customer, null, lang, (newCartItem.getPriceCasePackQty() != null ? true
								: false), false));
						newCartItem.setUnitPrice(discountedPrice);
						newCartItem.setQuantity(itemQualifiedQuant);
						newCartItem.setUserId(cartItem.getUserId());
						newCartItem.setProductAttributes(productAttrList);
						newCartItem.setDealGroup(cartItem.getDealGroup());
						if (cartItem.getProductAttributes() != null) {
							newCartItem.getProductAttributes().addAll(cartItem.getProductAttributes());
						}
						addCartItems.add(newCartItem);
						subTotal = subTotal - (cartItem.getUnitPrice() - discountedPrice) * newCartItem.getQuantity();
					}
				}
			}
			for (CartItem cartItem : addCartItems) {
				if (cart.getUserId() != null) { // save on database
					cartItems.add(cartItem);
					addToCart(cartItem, cartItem.getUserId());
				} else {
					cart.addCartItem(cartItem);
				}
			}
		}
		
		// edit, add, delete cart item For Parent SKU deal
		if(parentDeals != null){

			Map<String, Deal> dealsByParentSku = new HashMap<String, Deal>();
			Map<String, Integer> quantityByMasterSKU = new HashMap<String, Integer>();
			Map<String, Double> amountByMasterSKU = new HashMap<String, Double>();
			
			for(CartItem cartItem : cartItems){

				String masterSKU = cartItem.getProduct().getMasterSku();
				
				// based on masterSKU caculate totoal quantity
				if(!quantityByMasterSKU.containsKey(masterSKU)){
					quantityByMasterSKU.put(masterSKU, cartItem.getQuantity());
				}else{
					quantityByMasterSKU.put(masterSKU, quantityByMasterSKU.get(masterSKU)+cartItem.getQuantity());
				}
				
				// based on masterSKU caculate totoal amount
				if(!amountByMasterSKU.containsKey(masterSKU)){
					amountByMasterSKU.put(masterSKU, cartItem.getTotal());
				}else{
					if(amountByMasterSKU!=null){
						Double d1 = 0.0;
						Double d2 = 0.0;
						Double d3 = 0.0;
						try{
							if(masterSKU!=null && amountByMasterSKU.get(masterSKU)!=null){
								d1 = amountByMasterSKU.get(masterSKU);
							}
							if(cartItem!=null && cartItem.getTotal()!=null){
								d2 = cartItem.getTotal();
							}
							d3 = d1 + d2;
						}catch(Exception ex){
							d3 = 0.0;
						}
						amountByMasterSKU.put(masterSKU, d3);
					}
				}
			}
			
			for(Deal deal : parentDeals){
				//check buy parent sku by quantity
				if(deal.isDiscountType()){
					if(deal.getBuySku() != null && !deal.getBuySku().equals("") && deal.getBuyQuantity()!= null && deal.getBuyQuantity()>0){
						
						String masterSKUKey = deal.getBuySku();
						if(!quantityByMasterSKU.containsKey(masterSKUKey) || quantityByMasterSKU.get(masterSKUKey)<deal.getBuyQuantity()){
							continue;
						}else{
							Integer newId = null;
							double minPrice =Double.MAX_VALUE;
							for(CartItem cartItem : cartItems){
								String masterSKU = cartItem.getProduct().getMasterSku();
								if(!masterSKU.equals(masterSKUKey) ){
									continue;
								}else if(deal.isDiscountType() && deal.getDiscount()==100.00){
									if(cartItem.getUnitPrice() < minPrice){
										
										minPrice = cartItem.getUnitPrice();
										newId = cartItem.getProduct().getId();
									}
									if(cartItem.getUnitPrice() == 0.0){
										break;
									}
									continue;
								}else{
									double price = 0.0;
									if (deal.isDiscountType()) {
										price = cartItem.getUnitPrice() * (1 - (deal.getDiscount() / 100));
									}
									cartItem.setUnitPrice(price);
									cartItem.setDiscount((cartItem.getOriginalPrice()-cartItem.getUnitPrice())*(-1));
									cartItem.setDealMainItem(true);
									
									List<ProductAttribute> productAttrList = new ArrayList<ProductAttribute>();
									ProductAttribute productAttr = new ProductAttribute();
									productAttr.setOptionIndex(100);
									productAttr.setOptionName("Deal");
									productAttr.setValueIndex(100);
									productAttr.setValueName(deal.getTitle());
									productAttr.setDealItem(true);
									productAttrList.add(productAttr);
									cartItem.setProductAttributes(productAttrList);
									
									updateCartItem(cartItem);
								}
								
							}
							if(newId != null && deal.getDiscount()==100.00 && minPrice != 0.0){
								List<ProductAttribute> productAttrList = new ArrayList<ProductAttribute>();
								ProductAttribute productAttr = new ProductAttribute();
								productAttr.setOptionIndex(100);
								productAttr.setOptionName("Deal");
								productAttr.setValueIndex(100);
								productAttr.setValueName(deal.getTitle());
								productAttr.setDealItem(true);
								productAttrList.add(productAttr);
								
								CartItem newCartItem = new CartItem();
								newCartItem.setProduct(getProductByIdPrivate(newId, gSiteConfig, siteConfig, customer, null, lang, (newCartItem.getPriceCasePackQty() != null ? true
										: false), false));
								newCartItem.setUnitPrice(0.0);
								newCartItem.setOriginalPrice(minPrice);
								newCartItem.setDiscount(minPrice*(-1));
								newCartItem.setQuantity(1);
								newCartItem.setProductAttributes(productAttrList);
								newCartItem.setUserId(cartItems.get(0).getUserId());
								if (newCartItem.getUserId() != null) { // save on database
									cartItems.add(newCartItem);
									addToCart(newCartItem, newCartItem.getUserId());
								} else {
									cart.addCartItem(newCartItem);
								}
								
							}
						}
					}else if(deal.getBuyAmount() != null && deal.getBuyAmount()>0){
						Double buyAmount = deal.getBuyAmount();
						String masterSKUKey = deal.getGetSku();
						if(!amountByMasterSKU.containsKey(masterSKUKey) || amountByMasterSKU.get(masterSKUKey)<buyAmount){
							continue;
						}else{
							Integer newId = null;
							double minPrice =Double.MAX_VALUE;
							for(CartItem cartItem : cartItems){
								String masterSKU = cartItem.getProduct().getMasterSku();
								if(!masterSKU.equals(masterSKUKey) ){
									continue;
								}else if(deal.isDiscountType() && deal.getDiscount()==100.00){
									if(cartItem.getUnitPrice() < minPrice){
										
										minPrice = cartItem.getUnitPrice();
										newId = cartItem.getProduct().getId();
									}
									if(cartItem.getUnitPrice() == 0.0){
										break;
									}
									continue;
								}else{
									double price = 0.0;
									if (deal.isDiscountType()) {
										price = cartItem.getUnitPrice() * (1 - (deal.getDiscount() / 100));
									}
									cartItem.setUnitPrice(price);
									cartItem.setDiscount((cartItem.getOriginalPrice() - cartItem.getUnitPrice())*(-1));
									cartItem.setDealMainItem(true);
									List<ProductAttribute> productAttrList = new ArrayList<ProductAttribute>();
									ProductAttribute productAttr = new ProductAttribute();
									productAttr.setOptionIndex(100);
									productAttr.setOptionName("Deal");
									productAttr.setValueIndex(100);
									productAttr.setValueName(deal.getTitle());
									productAttr.setDealItem(true);
									productAttrList.add(productAttr);
									cartItem.setProductAttributes(productAttrList);
									
									updateCartItem(cartItem);
								}
								
							}
							if(newId != null && deal.getDiscount()==100.00 && minPrice != 0.0){
								List<ProductAttribute> productAttrList = new ArrayList<ProductAttribute>();
								ProductAttribute productAttr = new ProductAttribute();
								productAttr.setOptionIndex(100);
								productAttr.setOptionName("Deal");
								productAttr.setValueIndex(100);
								productAttr.setValueName(deal.getTitle());
								productAttr.setDealItem(true);
								productAttrList.add(productAttr);
								
								CartItem newCartItem = new CartItem();
								newCartItem.setProduct(getProductByIdPrivate(newId, gSiteConfig, siteConfig, customer, null, lang, (newCartItem.getPriceCasePackQty() != null ? true
										: false), false));
								newCartItem.setOriginalPrice(minPrice);
								newCartItem.setUnitPrice(0.0);
								newCartItem.setDiscount(minPrice*(-1));
								newCartItem.setQuantity(1);
								newCartItem.setProductAttributes(productAttrList);
								newCartItem.setUserId(cartItems.get(0).getUserId());
								if (newCartItem.getUserId() != null) { // save on database
									cartItems.add(newCartItem);
									addToCart(newCartItem, newCartItem.getUserId());
								} else {
									cart.addCartItem(newCartItem);
								}
							}
						}
					}
				}else{
					double discount = deal.getDiscount();
					if(deal.getBuySku() != null && !deal.getBuySku().equals("") && deal.getBuyQuantity()!= null && deal.getBuyQuantity()>0){					
						String masterSKUKey = deal.getBuySku();
						if(!quantityByMasterSKU.containsKey(masterSKUKey) || quantityByMasterSKU.get(masterSKUKey)<deal.getBuyQuantity()){
							continue;
						}else{
							String newSku = "";
							double minPrice =Double.MAX_VALUE;
							for(CartItem cartItem : cartItems){
								String masterSKU = cartItem.getProduct().getMasterSku();
								if(!masterSKU.equals(masterSKUKey) ){
									continue;
								}else{
									double price = 0.0;
									if(discount <= cartItem.getTotal()){
										price = cartItem.getUnitPrice() - discount/(cartItem.getTotal()/cartItem.getUnitPrice());
										discount =0.0;
									}else{
										price =0.0;
										discount -= cartItem.getTotal();
									}
									cartItem.setUnitPrice(price);
									cartItem.setDiscount((cartItem.getOriginalPrice() - cartItem.getUnitPrice())*(-1));
									cartItem.setDealMainItem(true);
									
									List<ProductAttribute> productAttrList = new ArrayList<ProductAttribute>();
									ProductAttribute productAttr = new ProductAttribute();
									productAttr.setOptionIndex(100);
									productAttr.setOptionName("Deal");
									productAttr.setValueIndex(100);
									productAttr.setValueName(deal.getTitle());
									productAttr.setDealItem(true);
									productAttrList.add(productAttr);
									cartItem.setProductAttributes(productAttrList);
									
									updateCartItem(cartItem);
									if(discount == 0){
										break;
									}
								}
							}
						}
					}else if(deal.getBuyAmount() != null && deal.getBuyAmount()>0){
						Double buyAmount = deal.getBuyAmount();
						String masterSKUKey = deal.getGetSku();
						if(!amountByMasterSKU.containsKey(masterSKUKey) || amountByMasterSKU.get(masterSKUKey)<buyAmount){
							continue;
						}else{
							String newSku = "";
							double minPrice =Double.MAX_VALUE;
							for(CartItem cartItem : cartItems){
								String masterSKU = cartItem.getProduct().getMasterSku();
								if(!masterSKU.equals(masterSKUKey) ){
									continue;
								}else{
									double price = 0.0;
									if(discount <= cartItem.getTotal()){
										price = cartItem.getUnitPrice() - discount/(cartItem.getTotal()/cartItem.getUnitPrice());
										discount =0.0;
									}else{
										price =0.0;
										discount -= cartItem.getTotal();
									}
									cartItem.setUnitPrice(price);
									cartItem.setDiscount((cartItem.getOriginalPrice() - cartItem.getUnitPrice())*(-1));
									cartItem.setDealMainItem(true);
									
									List<ProductAttribute> productAttrList = new ArrayList<ProductAttribute>();
									ProductAttribute productAttr = new ProductAttribute();
									productAttr.setOptionIndex(100);
									productAttr.setOptionName("Deal");
									productAttr.setValueIndex(100);
									productAttr.setValueName(deal.getTitle());
									productAttr.setDealItem(true);
									productAttrList.add(productAttr);
									cartItem.setProductAttributes(productAttrList);
									
									updateCartItem(cartItem);
									if(discount == 0){
										break;
									}
								}
							}
						}
					}
				}			
			}
		}
		
		
		
				
		
		
		
		return cartItems;
	}

	public Map<String, Object> validateCartItem(List<CartItem> cartItems, Deal deal, Double subTotal, CartItem currentItem, Map<String, Integer> getQuantity) {
		int qualifiedQuantity = 0;
		Double price = 0.0;
		String title = "";

		if (deal.getGetSku().equalsIgnoreCase(currentItem.getProduct().getSku())) {
			// check if deal is on subtotal
			if (deal.getBuyAmount() != null && deal.getBuyAmount() != 0.0) {
				if (deal.getBuyAmount() <= subTotal) {
					double newSubtotal = 0;
					title = deal.getTitle();
					if (deal.isDiscountType()) {
						price = currentItem.getUnitPrice() * (1 - (deal.getDiscount() / 100));
					} else {
						price = currentItem.getUnitPrice() - deal.getDiscount();
					}
					for (int i = 1; i <= getQuantity.get(currentItem.getProduct().getSku()); i++) {
						qualifiedQuantity = i;
						newSubtotal = subTotal - qualifiedQuantity * (currentItem.getUnitPrice() - price);
						if ((int) (newSubtotal / (deal.getBuyAmount() * deal.getGetQuantity())) >= qualifiedQuantity) {
							continue;
						} else if ((int) (newSubtotal / (deal.getBuyAmount() * deal.getGetQuantity())) < qualifiedQuantity) {
							qualifiedQuantity = qualifiedQuantity - 1;
							break;
						}
					}
					currentItem.setDealGroup("All");
				}
				// check if deal is on sku
			} else if (deal.getBuySku() != null && !deal.getBuySku().equals("")) {
				for (CartItem cartItem : cartItems) {
					if (cartItem.getProduct().getSku().equalsIgnoreCase(deal.getBuySku())) {
						title = deal.getTitle();
						if (deal.getBuySku().equalsIgnoreCase(deal.getGetSku())) {
							qualifiedQuantity = getQuantity.get(deal.getBuySku()) * deal.getGetQuantity() / (deal.getBuyQuantity() + deal.getGetQuantity());
						} else {
							qualifiedQuantity = getQuantity.get(deal.getBuySku()) * deal.getGetQuantity() / deal.getBuyQuantity();
						}
						if (deal.isDiscountType()) {
							price = currentItem.getUnitPrice() * (1 - (deal.getDiscount() / 100));
						} else {
							price = currentItem.getUnitPrice() - deal.getDiscount();
						}
						if (qualifiedQuantity > 0) {
							currentItem.setDealGroup(cartItem.getProduct().getSku());
							cartItem.setDealGroup(cartItem.getProduct().getSku());
							updateCartItem(cartItem);
						}
					}
				}
			}
		}
		Map<String, Object> cartItemData = new HashMap<String, Object>();
		cartItemData.put("title", title);
		cartItemData.put("qualifiedQuantity", qualifiedQuantity);
		cartItemData.put("price", price);

		return cartItemData;

	}

	public void arrangeCartItems(List<CartItem> cartItems) {

		for (int i = 0; i < cartItems.size(); i++) {
			if (cartItems.get(i).getDealGroup() != null && cartItems.get(i).getDealGroup().equalsIgnoreCase(cartItems.get(i).getProduct().getSku())) {
				boolean dealItem = false;
				if (cartItems.get(i).getProductAttributes() != null) {
					List<ProductAttribute> productAttr = cartItems.get(i).getProductAttributes();
					Iterator<ProductAttribute> iter = productAttr.iterator();
					while (iter.hasNext()) {
						ProductAttribute attr = iter.next();
						if (attr.isDealItem()) {
							dealItem = true;
							break;
						}
					}
				}

				if (dealItem == false) {
					for (int j = 0; j < cartItems.size(); j++) {
						if (i == j) {
							continue;
						}
						if (cartItems.get(j).getDealGroup() != null && cartItems.get(j).getDealGroup().equalsIgnoreCase(cartItems.get(i).getDealGroup())) {
							CartItem tempItem = cartItems.get(j);
							cartItems.remove(j);
							if (j < i) {
								cartItems.add(i, tempItem);
							} else {
								cartItems.add(i + 1, tempItem);
							}
						}
					}
				}
			}
		}
	}

	public boolean checkManufacturerMinOrder(Cart cart, HttpServletRequest request) {
		boolean continueCheckout = true;
		Map<String, Manufacturer> manufacturerMap = this.getManufacturerMap();
		Set<String> manufacturerPass = new HashSet<String>();
		Set<String> manufacturerCart = new HashSet<String>();
		for (CartItem cartItem : cart.getCartItems()) {
			cartItem.setManufacturer(null);
			if (cartItem.getProduct().getManufactureName() != null && cartItem.getProduct().getManufactureName().length() > 0) {
				manufacturerCart.add(cartItem.getProduct().getManufactureName());
			}
		}
		for (CartItem cartItem : cart.getCartItems()) {
			cartItem.setManufacturer(null);
			if (cartItem.getProduct().getManufactureName() != null && cartItem.getProduct().getManufactureName().length() > 0
					&& manufacturerMap.get(cartItem.getProduct().getManufactureName()) != null) {
				cart.setHasManufacturer(true);
				manufacturerMap.get(cartItem.getProduct().getManufactureName()).setAccumulator(
						manufacturerMap.get(cartItem.getProduct().getManufactureName()).getAccumulator() + ((cartItem.getTotal() == null) ? 0.0 : cartItem.getTotal()));
				if (manufacturerMap.get(cartItem.getProduct().getManufactureName()).getMinOrder() != null
						&& manufacturerMap.get(cartItem.getProduct().getManufactureName()).getAccumulator() < manufacturerMap.get(cartItem.getProduct().getManufactureName()).getMinOrder()) {
					cartItem.setManufacturer(manufacturerMap.get(cartItem.getProduct().getManufactureName()));
				} else {
					manufacturerPass.add(cartItem.getProduct().getManufactureName());
				}
			}
		}
		if (manufacturerPass.size() != manufacturerCart.size()) {
			continueCheckout = false;
			cart.setContinueCart(false);
		}
		if (cart.getCartItems().size() > 1 && manufacturerPass.size() > 0) {
			request.setAttribute("manufacturerPass", manufacturerPass);
		}
		return continueCheckout;
	}

	private boolean checkCartItemQty(Iterator<CartItem> iter, CartItem cartItem, Product product, Customer customer, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig,
			Map<String, Integer> crossItems) {
		int quantity = cartItem.getQuantity();
		if (quantity > 0) {
			cartItem.setQuantity(quantity);
		}
		int minimumQty = (cartItem.getProduct().getMinimumQty() == null) ? 0 : cartItem.getProduct().getMinimumQty();
		boolean continueCheckout = true;

		cartItem.setMinimumIncrementalQtyMessage(null);
		// minimum / incremental
		if ((Boolean) gSiteConfig.get("gMINIMUM_INCREMENTAL_QTY")) {
			boolean checkMinInc = false;
			if (siteConfig.get("APPLY_MIN_INC_TO_CUSTOMER").getValue().equals("1") && (Integer) gSiteConfig.get("gPRICE_TABLE") > 0 && customer != null && customer.getPriceTable() > 0) {
				checkMinInc = true;
			} else if (siteConfig.get("APPLY_MIN_INC_TO_CUSTOMER").getValue().equals("0") && customer != null && customer.getPriceTable() == 0) {
				checkMinInc = true;
			} else if (siteConfig.get("APPLY_MIN_INC_TO_CUSTOMER").getValue().length() < 1) {
				checkMinInc = true;
			}
			cartItem.setShowMinIncMessage(checkMinInc);
			// if CasePackPrice is selected then don't worry about the Min/Inc
			if (checkMinInc && cartItem.getPriceCasePackQty() == null) {
				int qty = cartItem.getQuantity();
				String crossItemCode = null;
				if (cartItem.getProduct().getCrossItemsCode() != null) {
					crossItemCode = cartItem.getProduct().getCrossItemsCode().trim().toLowerCase();
				}
				if (crossItems != null && crossItemCode != null && crossItems.get(crossItemCode) != null) {
					qty = crossItems.get(crossItemCode);
				}
				if (cartItem.getProduct().getMinimumQty() != null && cartItem.getProduct().getMinimumQty() > qty && !cartItem.getProduct().isPriceCasePack()) {
					cartItem.setMinimumIncrementalQtyMessage("minimum is " + cartItem.getProduct().getMinimumQty());
					continueCheckout = false;
				}
				if (cartItem.getProduct().getIncrementalQty() != null && cartItem.getQuantity() != (minimumQty)
						&& ((cartItem.getQuantity() - minimumQty) % cartItem.getProduct().getIncrementalQty() != 0) && !cartItem.getProduct().isPriceCasePack()) {
					cartItem.setMinimumIncrementalQtyMessage("incremental is " + cartItem.getProduct().getIncrementalQty());
					continueCheckout = false;
				}
			}
		}

		cartItem.setInventoryMessage(null);
		// inventory check for quantity available.
		if ((Boolean) gSiteConfig.get("gINVENTORY")) {
			if (siteConfig.get("INVENTORY_DEDUCT_TYPE").getValue().equals("ship")) {
				cartItem.getProduct().setInventoryAFS(cartItem.getProduct().getInventory());
				product.setInventoryAFS(cartItem.getProduct().getInventory());
			}
			if (cartItem.getProduct().getInventory() != null && !cartItem.getProduct().isNegInventory() && cartItem.getProduct().getInventoryAFS() < quantity) {
				cartItem.setInventoryMessage(siteConfig.get("INVENTORY_ADJUST_QTY_TITLE").getValue().toString());// configurable Message
				if (product.getInventoryAFS() > 0) {
					cartItem.setQuantity(product.getInventoryAFS());
					continueCheckout = false;
				} else {
					iter.remove();
					return continueCheckout;
				}
			}
			if (product != null) {
				if (product.getInventory() != null && !product.isNegInventory() && product.getInventoryAFS() < cartItem.getQuantity()) {
					cartItem.setInventoryMessage(siteConfig.get("INVENTORY_ADJUST_QTY_TITLE").getValue().toString());// configurable Message
					if (product.getInventoryAFS() > 0) {
						cartItem.setQuantity(product.getInventoryAFS());
						continueCheckout = false;
					} else {
						iter.remove();
						return continueCheckout;
					}
				}
				if (product.getInventory() != null && !product.isNegInventory() && product.getLowInventory() != null && product.getLowInventory() > product.getInventoryAFS()) {
					cartItem.setLowInventoryMessage(siteConfig.get("LOW_STOCK_TITLE").getValue().toString());
				}
			}
		}
		return continueCheckout;
	}

	private boolean checkCartMinimumOrder(Customer customer, Cart cart, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig, MultiStore multiStore) {
		boolean continueCheckout = true;
		cart.setMinOrderMessage(null);
		cart.setMinOrderAmt(null);
		if (siteConfig.get("MINIMUM_ORDER").getValue().equals("true")) {
			double MINIMUM_ORDER_AMOUNT_WHOLESALE = 0.0;
			double MINIMUM_ORDER_AMOUNT = 0.0;
			try {
				MINIMUM_ORDER_AMOUNT = Double.parseDouble(siteConfig.get("MINIMUM_ORDER_AMOUNT").getValue());
			} catch (NumberFormatException e) {
			}
			try {
				MINIMUM_ORDER_AMOUNT_WHOLESALE = Double.parseDouble(siteConfig.get("MINIMUM_ORDER_AMOUNT_WHOLESALE").getValue());
			} catch (NumberFormatException e) {
			}
			if (multiStore != null) {
				if (multiStore.getMinOrderAmt() != null)
					MINIMUM_ORDER_AMOUNT = multiStore.getMinOrderAmt();
				if (multiStore.getMinOrderAmtWholesale() != null)
					MINIMUM_ORDER_AMOUNT_WHOLESALE = multiStore.getMinOrderAmtWholesale();
			}
			if ((Integer) gSiteConfig.get("gPRICE_TABLE") > 0 && customer != null && customer.getPriceTable() > 0) {
				if (cart.getSubTotal() > 0.0 && cart.getSubTotal() < MINIMUM_ORDER_AMOUNT_WHOLESALE) {
					cart.setMinOrderMessage("shoppingcart.minimum.order");
					cart.setMinOrderAmt(MINIMUM_ORDER_AMOUNT_WHOLESALE);
					continueCheckout = false;
				}
			} else {
				if (cart.getSubTotal() > 0.0 && cart.getSubTotal() < MINIMUM_ORDER_AMOUNT) {
					cart.setMinOrderMessage("shoppingcart.minimum.order");
					cart.setMinOrderAmt(MINIMUM_ORDER_AMOUNT);
					continueCheckout = false;
				}
			}
		}
		return continueCheckout;
	}

	public void addToCart(CartItem cartItem, Integer userId) {
		cartItem.setUserId(userId);
		if (getUserCart(userId, null).containsCartItem(cartItem) && cartItem.getProduct().getId() != null) {
			this.shoppingCartDao.incrementQuantityByCartItem(cartItem);
			Cart cart = getUserCart(userId, null);
			for(CartItem item : cart.getCartItems()){
				if(item.getIndex()==cartItem.getIndex() && item.getQuantity()<1){
					removeCartItem(item.getIndex(), userId);
				}
			}
		} else {
			this.shoppingCartDao.addToCart(cartItem);
		}
	}

	public String removeCartItem(int index, Integer userId) {
		CartItem cartItem = new CartItem();
		cartItem.setIndex(index);
		cartItem.setUserId(userId);
		String customImageUrl = this.shoppingCartDao.getCustomImageUrl(cartItem);
		this.shoppingCartDao.removeCartItem(cartItem);
		return customImageUrl;
	}
	
	public void removeCartItembyApiIndex(String apiIndex, Integer userId, Integer productId) {
		this.shoppingCartDao.removeCartItembyApiIndex(apiIndex,userId, productId);
	}

	public void updateCartItem(CartItem cartItem) {
		this.shoppingCartDao.updateCartItem(cartItem);
	}
	
	public void updateCartItemByApiIndex(CartItem cartItem, Integer productId)  {
		this.shoppingCartDao.updateCartItemByApiIndex(cartItem, productId);
	}
	

	public void mergeCart(Cart cart, Integer userId) {
		Cart userCart = getUserCart(userId, null);

		Iterator<CartItem> iter = cart.getCartItems().iterator();
		while (iter.hasNext()) {
			CartItem cartItem = iter.next();
			cartItem.setUserId(userId);
			if (userCart.containsCartItem(cartItem) && cartItem.getProduct().getId() != null) {
				this.shoppingCartDao.incrementQuantityByCartItem(cartItem);
			} else {
				this.shoppingCartDao.addToCart(cartItem);
			}
		}
	}

	public Cart getUserCart(int userId, String selectedManufacturer) {
		return this.shoppingCartDao.getCart(userId, selectedManufacturer);
	}

	public void deleteShoppingCart(int userid, String manufacturerName) {
		this.shoppingCartDao.deleteShoppingCart(userid, manufacturerName);
	}

	// list
	public void addToList(String ids[], Integer userId) {
		Set<Integer> productIds = new HashSet<Integer>();
		for (String id : ids) {
			productIds.add(Integer.parseInt(id));
		}
		this.productDao.addToList(userId, productIds);
	}

	public void removeFromList(String ids[], Integer userId) {
		Set<Integer> productIds = new HashSet<Integer>();
		for (int i = 0; i < ids.length; i++) {
			productIds.add(Integer.parseInt(ids[i]));
		}
		this.productDao.removeFromList(userId, productIds);
	}

	public void addToWishList(Integer userId, Set productIds) {
		this.productDao.addToList(userId, productIds);
	}

	public List getShoppingListGroupByUserid(Integer userId) {
		return this.productDao.getShoppingListGroupByUserid(userId);
	}

	public void insertShoppingGroupList(String groupName, Integer userId) {
		this.productDao.insertShoppingGroupList(groupName, userId);
	}

	public ShoppingListGroup getShoppingListGroupByName(String groupName) {
		return this.productDao.getShoppingListGroupByName(groupName);
	}

	public void addToShoppingGroupList(String[] ids, Integer userId, Integer groupId) {
		Set<Integer> productIds = new HashSet<Integer>();
		for (int i = 0; i < ids.length; i++) {
			productIds.add(Integer.parseInt(ids[i]));
		}
		this.productDao.addToShoppingGroupList(productIds, userId, groupId);
	}

	public void removeMyListGroup(String[] ids, Integer userId) {
		Set<Integer> myListGroupIds = new HashSet<Integer>();
		for (int i = 0; i < ids.length; i++) {
			myListGroupIds.add(Integer.parseInt(ids[i]));
		}
		this.productDao.removeMyListGroup(myListGroupIds, userId);

	}

	// Policy

	public List<Policy> getPolicyList() {
		return this.policyDao.getPolicyList();
	}

	public void insertPolicy(Policy policy) {
		this.policyDao.insertPolicy(policy);
	}

	public void updatePolicy(Policy policy) {
		this.policyDao.updatePolicy(policy);
	}

	public void deletePolicy(Integer policyId) {
		this.policyDao.deletePolicy(policyId);
	}

	public Policy getPolicyById(Integer policyId) {
		return (Policy) this.policyDao.getPolicyById(policyId);
	}

	public List<Policy> getPolicies() {
		return this.policyDao.getPolicies();
	}

	public void updatePolicyRanking(List data) {
		this.policyDao.updateRanking(data);
	}

	// Promo

	public List<Promo> getPromoList(PromoSearch promoSearch) {
		return this.promoDao.getPromosList(promoSearch);
	}
	public List<Promo> getDynamicPromoList(PromoSearch promoSearch) {
		return this.promoDao.getDynamicPromosList(promoSearch);
	}
	public List<Promo> getCampaignList(PromoSearch promoSearch) {
		return this.promoDao.getCampaignList(promoSearch);
	}

	public Promo getPromoById(Integer promoId) {
		return this.promoDao.getPromoById(promoId);
	}

	public Promo getPromoByName(String promoName) {
		return this.promoDao.getPromoByName(promoName);
	}

	
	public Promo getPromoWithCondition(Promo promo, Map<String, Object> conditionPromo) {
		return this.promoDao.getPromoWithCondition(promo, conditionPromo);
	}
	
	public Promo getAdminPromoWithCondition(Promo promo, Map<String, Object> conditionPromo) {
		return this.promoDao.getAdminPromoWithCondition(promo, conditionPromo);
	}

	public Integer getPromoIdByName(String promoName) {
		return this.promoDao.getPromoIdByName(promoName);
	}

	public void insertPromo(com.webjaguar.model.Promo promo) {
		this.promoDao.insertPromo(promo);
	}


	public Promo getPromo(String promoName) {
		return this.promoDao.getPromo(promoName);
	}

	public Integer getPromoCount(PromoSearch promoSearch) {
		return this.promoDao.getPromoCount(promoSearch);
	}

	public void updatePromo(com.webjaguar.model.Promo promo) {
		this.promoDao.updatePromo(promo);
	}

	public void deletePromoById(Integer promoId) {
		this.promoDao.deletePromoById(promoId);
	}
	public void deletePromoByIds(int[] promoIds) {
		this.promoDao.deletePromoByIds(promoIds);
	}

	public List<Promo> getEligiblePromoList(Customer customer, Order order) {
		return this.promoDao.getEligiblePromoList(customer, order);
	}

	public void updatePromoRanking(List<Map<String, Integer>> data) {
		this.promoDao.updateRanking(data);
	}

	// GiftCard
	public int insertGiftCardOrder(GiftCardOrder giftCardOrder, GiftCardStatus giftCardStatus) {
		return this.giftCardDao.insertGiftCardOrder(giftCardOrder, giftCardStatus);
	}

	public List<GiftCard> getGiftCardList(GiftCardSearch search, Integer customerId) {
		return this.giftCardDao.getGiftCardList(search, customerId);
	}

	public List<GiftCardOrder> getGiftCardOrderList(GiftCardSearch search, Integer customerId) {
		return this.giftCardDao.getGiftCardOrderList(search, customerId);
	}

	public void insertGiftCard(GiftCard giftCard) {
		this.giftCardDao.insertGiftCard(giftCard);
	}

	public GiftCard getGiftCardByCode(String code, Boolean active) {
		return this.giftCardDao.getGiftCardByCode(code, active);
	}

	public void updateGiftCard(GiftCard giftCard) {
		this.giftCardDao.updateGiftCard(giftCard);
	}

	public void redeemGiftCardByCode(GiftCard giftCard, UserSession userSession, GiftCardStatus giftCardStatus) {
		this.insertGiftCardStatus(giftCardStatus);
		this.giftCardDao.redeemGiftCardByCode(giftCard.getCode(), userSession);
		CreditImp credit = new CreditImp();
		CustomerCreditHistory customerCredit = credit.updateCustomerCreditByGiftCard(giftCard, userSession.getUserid());
		updateCredit(customerCredit);
	}

	public void updateGiftCardOrderCreditCardPayment(GiftCardOrder giftCardOrder) {
		this.giftCardDao.updateGiftCardOrderCreditCardPayment(giftCardOrder);
	}

	public GiftCardOrder getGiftCardOrderByGiftCardOrderId(String giftCardOrderId) {
		return this.giftCardDao.getGiftCardOrderByGiftCardOrderId(giftCardOrderId);
	}

	public void updateGiftCardByPayPal(GiftCard giftCard) {
		this.giftCardDao.updateGiftCardByPayPal(giftCard);
	}

	public GiftCard getGiftCardById(Integer giftCardOrderId) {
		return this.giftCardDao.getGiftCardById(giftCardOrderId);
	}

	public void updateGiftCardOrderByPayPal(GiftCardOrder giftCardOrder, GiftCardStatus giftCardStatus) {
		this.giftCardDao.updateGiftCardOrderByPayPal(giftCardOrder, giftCardStatus);
	}

	public List<GiftCardStatus> getGiftCardStatusHistory(Integer giftCardOrderId) {
		return this.giftCardDao.getGiftCardStatusHistory(giftCardOrderId);
	}

	public void insertGiftCardStatus(GiftCardStatus giftCardStatus) {
		this.giftCardDao.insertGiftCardStatus(giftCardStatus);
	}

	// Deal

	public List<Deal> getDealList(DealSearch search) {
		return this.dealDao.getDealsList(search);
	}

	public Deal getDealById(Integer dealId) {
		return this.dealDao.getDealById(dealId);
	}

	public Deal getDealBySku(String getSku) {
		return this.dealDao.getDealBySku(getSku);
	}

	public void insertDeal(com.webjaguar.model.Deal deal) {
		this.dealDao.insertDeal(deal);
	}

	public void updateDeal(com.webjaguar.model.Deal deal) {
		this.dealDao.updateDeal(deal);
	}

	public void deleteDealById(Integer dealId) {
		this.dealDao.deleteDealById(dealId);
	}
	
	//via Deal
	
	public List<ViaDeal> getViaDealsList(DealSearch search) {
		return this.dealDao.getViaDealsList(search);
	}
	
	public ViaDeal getViaDealById(Integer dealId) {
		return this.dealDao.getViaDealById(dealId);
	}
	
	public ViaDeal getViaDealBySku(String getSku) {
		return this.dealDao.getViaDealBySku(getSku);
	}
	
	public void insertViaDeal(com.webjaguar.model.ViaDeal viaDeal) {
		this.dealDao.insertViaDeal(viaDeal);
	}

	public void updateViaDeal(com.webjaguar.model.ViaDeal viaDeal) {
		this.dealDao.updateViaDeal(viaDeal);
	}

	public void deleteViaDealById(Integer dealId) {
		this.dealDao.deleteViaDealById(dealId);
	}

	// Mail In Rebate
	public List<MailInRebate> getMailInRebateList(MailInRebateSearch search) {
		return this.mailInRebateDao.getMailInRebateList(search);
	}

	public MailInRebate getMailInRebateById(Integer rebateId) {
		return this.mailInRebateDao.getMailInRebateById(rebateId);
	}

	public MailInRebate getMailInRebateByName(String name) {
		return this.mailInRebateDao.getMailInRebateByName(name);
	}

	public void insertMailInRebate(com.webjaguar.model.MailInRebate mailInRebate) {
		this.mailInRebateDao.insertMailInRebate(mailInRebate);
	}

	public void updateMailInRebate(com.webjaguar.model.MailInRebate mailInRebate) {
		this.mailInRebateDao.updateMailInRebate(mailInRebate);
	}

	public void deleteMailInRebateById(Integer rebateId) {
		this.mailInRebateDao.deleteMailInRebateById(rebateId);
	}

	// Affiliate
	public List<Affiliate> getAffiliates(Integer root, Integer list_type) {
		return this.customerDao.getAffiliates(root, list_type);
	}

	public Affiliate getAffiliatesCommission(Integer nodeId) {
		return this.customerDao.getAffiliatesCommission(nodeId);
	}

	public void updateEdge(Integer customerId, Integer parentId) {
		this.customerDao.updateEdge(customerId, parentId);
	}

	public Integer getParentId(Integer childId) {
		return this.customerDao.getParentId(childId);
	}

	public Integer getUserIdByPromoCode(String promoCode) {
		return this.customerDao.getUserIdByPromoCode(promoCode);
	}

	public int affiliateCount() {
		return this.customerDao.affiliateCount();
	}

	public List<Customer> getCustomerListByParent(CustomerSearch search) {
		return this.customerDao.getCustomerListByParent(search);
	}

	public int getCustomerListByParentCount(CustomerSearch search) {
		return this.customerDao.getCustomerListByParentCount(search);
	}

	public void updateParent(Customer customer) {
		this.customerDao.updateParent(customer);
	}

	public void unsubscribeByCustomerId(Integer customerId, String unsubscibeReason) {
		this.customerDao.unsubscribeByCustomerId(customerId, unsubscibeReason);
	}

	public List<Customer> getFamilyTree(int customerId, String direction) {
		List<Customer> results = this.customerDao.getFamilyTree(customerId, direction);
		if (direction.equals("up")) {
			List<Customer> customers = new ArrayList<Customer>();
			for (Customer customer : results) {
				if (customer.getId() != null) {
					customers.add(0, customer);
				} else {
					break;
				}
			}
			results = customers;
		}
		return results;
	}

	public void insertPartnerCustomer(CustomerBudgetPartner partner) {
		this.customerDao.insertPartnerCustomer(partner);
	}

	public void updatePartnerCustomer(CustomerBudgetPartner partner, boolean addHistory) {
		this.customerDao.updatePartnerCustomer(partner, addHistory);
	}

	public List<CustomerBudgetPartner> getPartnersListByCustomerId(Integer userId, Boolean active) {
		return this.customerDao.getPartnersListByCustomerId(userId, active);
	}

	public void insertPartnerCustomerHistory(CustomerBudgetPartner partner) {
		this.customerDao.insertPartnerCustomerHistory(partner);
	}

	public Boolean getPartnerByName(String name) {
		return this.customerDao.getPartnerByName(name);
	}

	public void insertBudgetPartners(BudgetPartner partner) {
		this.customerDao.insertBudgetPartners(partner);
	}

	public void updatePartners(BudgetPartner partner) {
		this.customerDao.updatePartners(partner);
	}

	public String getPartnerNameById(Integer id) {
		return this.customerDao.getPartnerNameById(id);
	}

	public List<BudgetPartner> getPartnersList(Boolean active) {
		return this.customerDao.getPartnersList(active);
	}

	public BudgetPartner getPartnerById(BudgetPartner partner) {
		return this.customerDao.getPartnerById(partner);
	}

	public List<Integer> getParentsId(Integer childId, int level) {
		List<Integer> parentsId = new Vector<Integer>();
		Integer temp = childId;
		Integer temp1;
		while (level-- > 0) {
			temp1 = getParentId(temp);
			if (temp1 != null && temp1 != 0)
				parentsId.add(temp1);
			else
				return parentsId;
			temp = temp1;
		}

		return parentsId;
	}
	
	// Lucene Customer
	public int getLuceneCustomerListCount(CustomerSearch search) {
		return this.customerDao.getLuceneCustomerListCount(search);
	}
	
	public int updateUserRatings(StagingUser user) {
		return this.customerDao.updateUserRatings(user);
	}
		
	public List<Map<String, Object>> getLuceneCustomer(int offset, int limit, Integer userId) {
		return this.customerDao.getLuceneCustomer(offset, limit, userId);
	}

	public CommissionReport getCommissionsByUserIdOrderId(Integer userId, Integer orderId, Integer level) {
		return this.affiliateDao.getCommissionsByUserIdOrderId(userId, orderId, level);
	}

	public List getCommissionsByUserId(Integer userId, Integer level, CommissionSearch search) {
		return this.affiliateDao.getCommissionsByUserId(userId, level, search);
	}

	public void updateCommissionStatus(CommissionReport comReport) {
		this.affiliateDao.updateCommissionStatus(comReport);
	}

	public CommissionReport getCommissionTotalByUserId(Integer userId, Integer level) {
		return this.affiliateDao.getCommissionTotalByUserId(userId, level);
	}

	public CommissionReport getCommissionTotal(Integer userId) {
		return this.affiliateDao.getCommissionTotal(userId);
	}

	public List<CommissionReport> getCommissionReport() {
		List<CommissionReport> affiliateReport = new ArrayList<CommissionReport>();
		for (Customer affiliate : this.getAffiliateNameList()) {
			CommissionReport commissionReport = new CommissionReport();
			commissionReport = this.affiliateDao.getCommissionTotal(affiliate.getId());
			commissionReport.setUserId(affiliate.getId());
			commissionReport.setFirstName(affiliate.getAddress().getFirstName());
			commissionReport.setLastName(affiliate.getAddress().getLastName());
			affiliateReport.add(commissionReport);
		}
		return affiliateReport;
	}

	public int getAffiliateListCount(CustomerSearch search) {
		return this.affiliateDao.getAffiliateListCount(search);
	}

	public List<Customer> getAffiliateList(CustomerSearch search) {
		return this.affiliateDao.getAffiliateList(search);
	}

	public List<Customer> getAffiliateNameList() {
		return this.affiliateDao.getAffiliateNameList();
	}

	// Access Privilege
	public List<AccessUser> getUserPrivilegeList(AccessPrivilegeSearch accessPrivilegeSearch) {
		return this.accessPrivilegeDao.getUserPrivilegeList(accessPrivilegeSearch);
	}

	public AccessPrivilege getPrivilegeById(Integer accessUserId) {
		return this.accessPrivilegeDao.getPrivilegeById(accessUserId);
	}

	public AccessUser getUserByUserName(String userName) {
		return this.accessPrivilegeDao.getUserByUserName(userName);
	}

	public AccessUser getAccessUserById(Integer accessUserId) {
		return this.accessPrivilegeDao.getAccessUserById(accessUserId);
	}

	public void insertAccessPrivilegeUser(AccessPrivilegeForm privilegeForm) {
		this.accessPrivilegeDao.insertAccessPrivilegeUser(privilegeForm);
	}

	public void disableAccessPrivilege() {
		this.accessPrivilegeDao.disableAccessPrivilege();
	}

	public void updateAccessPrivilegeUser(AccessUser user, boolean changeUsername, boolean changePassword) {
		this.accessPrivilegeDao.updateAccessPrivilegeUser(user, changeUsername, changePassword);
	}

	public void updateAccessPrivilege(List<Configuration> data) {
		this.accessPrivilegeDao.updateAccessPrivilege(data);
	}

	public void deleteAccessPrivilege(String username) {
		this.accessPrivilegeDao.deleteAccessPrivilege(username);
	}

	public void deleteAccessPrivilegeUser(String username) {
		this.accessPrivilegeDao.deleteAccessPrivilegeUser(username);
	}

	public boolean isUserExist(String username) {
		return this.accessPrivilegeDao.isUserExist(username);
	}

	public List getUserAuditList(String username) {
		List<AccessUserAudit> auditList = this.accessPrivilegeDao.getUserAuditList(username);
		Calendar in = new GregorianCalendar();
		Calendar out = new GregorianCalendar();
		int index = 0;
		for (AccessUserAudit accessUserAudit : auditList) {
			try {
				in.setTime(auditList.get(index).getPageVisitDate());
				out.setTime(auditList.get(index++ + 1).getPageVisitDate());
				long seconds = (in.getTimeInMillis() - out.getTimeInMillis()) / 1000;
				accessUserAudit.setPageWorkTime("" + String.format("%1$02d:%2$02d:%3$02d", seconds / (60 * 60), (seconds / 60) % 60, seconds % 60));
			} catch (IndexOutOfBoundsException e) {
				// do nothing
			}
		}
		return auditList;
	}

	public void insertUserAudit(AccessUserAudit accessUserAudit) {
		this.accessPrivilegeDao.insertUserAudit(accessUserAudit);
	}

	public void deleteAccessUserAudit(Integer id) {
		this.accessPrivilegeDao.deleteAccessUserAudit(id);
	}

	public AccessUser getAccessUserBySalesRepId(Integer salesRepId) {
		return this.accessPrivilegeDao.getAccessUserBySalesRepId(salesRepId);
	}

	public List<AccessGroup> getGroupPrivilegeList(AccessPrivilegeSearch accessPrivilegeSearch) {
		return this.accessPrivilegeDao.getGroupPrivilegeList(accessPrivilegeSearch);
	}

	public AccessGroupPrivilege getPrivilegeByGroupId(Integer groupId) {
		return this.accessPrivilegeDao.getPrivilegeByGroupId(groupId);
	}

	public boolean isGroupUserExist(Integer groupId) {
		return this.accessPrivilegeDao.isGroupUserExist(groupId);
	}

	public void insertAccessGroup(GroupPrivilegeForm privilegeForm) throws DataAccessException {
		this.accessPrivilegeDao.insertAccessGroup(privilegeForm);
	}

	public void updateGroupAccessPrivilege(final List<Configuration> data) {
		this.accessPrivilegeDao.updateGroupAccessPrivilege(data);
	}

	public void deleteAccessPrivilegeGroup(Integer groupId) {
		this.accessPrivilegeDao.deleteAccessPrivilegeGroup(groupId);
	}

	public void updateAccessGroup(AccessGroup group) {
		this.accessPrivilegeDao.updateAccessGroup(group);
	}

	public void deleteAccessGroup(Integer groupId) {
		this.accessPrivilegeDao.deleteAccessGroup(groupId);
	}

	public List<Configuration> getGroupPrivileges(Integer groupId, Integer userId) {
		return this.accessPrivilegeDao.getGroupPrivileges(groupId, userId);
	}

	// Event
	public Event getEventById(Integer eventId) {
		return this.eventDao.getEventById(eventId);
	}

	public List<Event> getEventList(EventSearch search) {
		return this.eventDao.getEventList(search);
	}

	public void insertEvent(Event event) {
		this.eventDao.insertEvent(event);
	}

	public void updateEvent(Event event) {
		this.eventDao.updateEvent(event);
	}

	public void deleteEvent(Integer eventId) {
		this.eventDao.deleteEvent(eventId);
	}

	public int getEventMemberListCount(EventSearch search) {
		return this.eventDao.getEventMemberListCount(search);
	}
	
	public int getAllEventMemberListCount(EventSearch search){
		return this.eventDao.getAllEventMemberListCount(search);
	}

	public List<EventMember> getEventMemberList(EventSearch search) {
		return this.eventDao.getEventMemberList(search);
	}
	
	public List<EventMember> getAllEventMemberList(EventSearch search){
		return this.eventDao.getAllEventMemberList(search);
	}

	public void insertEventMember(EventMember event) {
		this.eventDao.insertEventMember(event);
	}

	public EventMember getEventMemberByEventIdUserId(Integer eventId, Integer userId) {
		return this.eventDao.getEventMemberByEventIdUserId(eventId, userId);
	}

	public void updateEventMember(EventMember eventMember) throws DataAccessException {
		this.eventDao.updateEventMember(eventMember);
	}

	public void deleteEventMemberById(Integer eventId, Integer userId) {
		this.eventDao.deleteEventMemberById(eventId, userId);
	}

	// Sales Tag
	public List<SalesTag> getSalesTagList() {
		return this.promoDao.getSalesTagList();
	}

	public com.webjaguar.model.SalesTag getSalesTagById(Integer salesTagId) {
		return this.promoDao.getSalesTagById(salesTagId);
	}

	public int insertSalesTag(SalesTag salesTag) {
		return this.promoDao.insertSalesTag(salesTag);
	}

	public void updateSalesTag(SalesTag salesTag) {
		this.promoDao.updateSalesTag(salesTag);
	}

	public void deleteSalesTagById(Integer salesTagId) {
		this.promoDao.deleteSalesTagById(salesTagId);
	}

	public Map<String, Long> getSalesTagCodeMap() {
		return this.promoDao.getSalesTagCodeMap();
	}
	
	public List<CrmQualifier> getQualifierList() {
		return this.salesRepDao.getQualifierList(new QualifierSearch());
	}

	// Sales Rep
	public List<SalesRep> getSalesRepList() {
		return this.salesRepDao.getSalesRepList(new SalesRepSearch());
	}

	public List<SalesRep> getSalesRepList(SalesRepSearch search) {
		return this.salesRepDao.getSalesRepList(search);
	}
	
	public List<CrmQualifier> getQualifierList(QualifierSearch search) {
		return this.salesRepDao.getQualifierList(search);
	}

	public SalesRep getSalesRepByUserId(Integer userId) {
		return this.salesRepDao.getSalesRepByUserId(userId);
	}

	public SalesRep getSalesRep(Integer userId, Integer accessUserId) {
		SalesRep salesRep = null;
		if (userId != null) {
			salesRep = this.salesRepDao.getSalesRepByUserId(userId);

		} else if (salesRep == null && accessUserId != null) {
			AccessUser accessUser = this.getAccessUserById(accessUserId);
			if (accessUser != null && accessUser.getSalesRepId() != null) {
				salesRep = this.getSalesRepById(accessUser.getSalesRepId());
			}
		}
		return salesRep;
	}

	public void insertSalesRep(SalesRep salesRep) {
		this.salesRepDao.insertSalesRep(salesRep);
	}

	public void updateSalesRep(SalesRep salesRep) {
		this.salesRepDao.updateSalesRep(salesRep);
	}

	public SalesRep getSalesRepById(Integer salesRepId) {
		return this.salesRepDao.getSalesRepById(salesRepId);
	}

	public void deleteSalesRep(Integer salesRepId) {
		this.salesRepDao.deleteSalesRep(salesRepId);
	}
	
	public boolean salesRepHasCustomer(Integer salesRepId){
		return this.salesRepDao.salesRepHasCustomer(salesRepId);
	}

	public SalesRep getSalesRepByTerritoryZipcode(String zipcode) {
		return this.salesRepDao.getSalesRepByTerritoryZipcode(zipcode);
	}

	public SalesRep getNextSalesRepInQueue() {
		SalesRep salesRep = this.salesRepDao.getNextSalesRepInQueue();
		if (salesRep == null) {
			// if it returns null, than either sales rep list is empty
			// or all salesreps are already assigned in this cycle and need to start from beginning.
			this.markSalesRepAssignedToCustomerInCycle(null, false);
			salesRep = this.salesRepDao.getNextSalesRepInQueue();
		}
		return salesRep;
	}

	public void markSalesRepAssignedToCustomerInCycle(Integer salesRepId, boolean isAssigned) {
		this.salesRepDao.markSalesRepAssignedToCustomerInCycle(salesRepId, isAssigned);
	}

	public List<SalesRep> getSalesRepListByTerritoryZipcode(LocationSearch search) {
		return this.salesRepDao.getSalesRepListByTerritoryZipcode(search);
	}

	public SalesRep getSalesRep(SalesRep salesRep) {
		return this.salesRepDao.getSalesRep(salesRep);
	}

	public SalesRep getSalesRep(HttpServletRequest request) {
		Cookie salesRepCookie = WebUtils.getCookie(request, "salesrep");
		if (salesRepCookie != null) {
			SalesRep salesRep = new SalesRep();
			salesRep.setToken(salesRepCookie.getValue());
			return this.salesRepDao.getSalesRep(salesRep);
		} else {
			return null;
		}
	}

	public List<Customer> getCustomerListBySalesRep(int salesRepId, String sort) {
		return this.salesRepDao.getCustomerListBySalesRep(salesRepId, sort);
	}

	public void nonTransactionSafeLoginStats(SalesRep salesRep) {
		this.salesRepDao.updateLoginStats(salesRep);
	}

	public List<SalesRep> getSalesRepTree(int salesRepId) {
		return this.salesRepDao.getSalesRepTree(salesRepId);
	}
	
	public List<String> getSalesRepGroupList(){
		return this.salesRepDao.getSalesRepGroupList();
	}

	public List<SalesRep> getSalesRepList(int salesRepId) {
		return this.salesRepDao.getSalesRepList(salesRepId);
	}

	// Presentation
	public List<PresentationProduct> getPresentationProductList(Presentation presentation) {
		return this.presentationDao.getPresentationProductList(presentation);
	}

	public List<Presentation> getPresentationList(Presentation presentation) {
		return this.presentationDao.getPresentationList(presentation);
	}

	public Presentation getPresentation(Integer presentationId) {
		return this.presentationDao.getPresentation(presentationId);
	}

	public void addProductToPresentation(String[] sku, Integer presentationId, Integer saleRepId) {
		this.presentationDao.addProductToPresentation(sku, presentationId, saleRepId);
	}

	public List<PresentationTemplate> getPresentationTemplateList(PresentationTemplateSearch templateSearch) {
		return this.presentationDao.getPresentationTemplateList(templateSearch);
	}

	public void insertPresentation(Presentation presentation) {
		this.presentationDao.insertPresentation(presentation);
	}

	public void updateNoOfViewToPresentation(Integer presentationId) {
		this.presentationDao.updateNoOfViewToPresentation(presentationId);
	}

	public void updatePresentationPrice(String sku, double price, Integer presentationId, Integer salesRepId, boolean remove) {
		this.presentationDao.updatePresentationPrice(sku, price, presentationId, salesRepId, remove);
	}

	public void updatePresentation(Presentation presentation) {
		this.presentationDao.updatePresentation(presentation);
	}

	public void deletePresentation(Presentation presentation) {
		this.presentationDao.deletePresentation(presentation);
	}

	public void insertTemplate(PresentationTemplate template) {
		this.presentationDao.insertTemplate(template);
	}

	public void updateTemplate(PresentationTemplate template) {
		this.presentationDao.updateTemplate(template);
	}

	public void deleteTemplate(Integer templateId) {
		this.presentationDao.deleteTemplate(templateId);
	}

	public PresentationTemplate getTemplateById(Integer templateId) {
		return this.presentationDao.getTemplateById(templateId);
	}

	public void removeProductFromPresentationList(String sku[], Integer saleRepId) {
		this.presentationDao.removeProductFromPresentationList(sku, saleRepId);
	}

	// Special Pricing
	public void updateSpecialPricingActive(SpecialPricing specialPricing) {
		this.customerDao.updateSpecialPricingActive(specialPricing);
	}

	public SpecialPricing getSpecialPricingByCustomerIdSku(SpecialPricing specialPricing) {
		return this.customerDao.getSpecialPricingByCustomerIdSku(specialPricing);
	}

	public List<SpecialPricing> getSpecialPricingByCustomerID(SpecialPricingSearch search) {
		return this.customerDao.getSpecialPricingByCustomerID(search);
	}

	public void insertSpecialPricing(SpecialPricing specialPricing) {
		this.customerDao.insertSpecialPricing(specialPricing);
	}

	public void deleteSpecialPricingById(Integer id) {
		this.customerDao.deleteSpecialPricingById(id);
	}

	public Map<String, SpecialPricing> getSpecialPricingByCustomerIDPerPage(Integer customerId, List productList) {
		return this.customerDao.getSpecialPricingByCustomerIDPerPage(customerId, productList);
	}

	public Map<Integer, Customer> getQuoteTotalByUserIDPerPage(List customerList) {
		return this.customerDao.getQuoteTotalByUserIDPerPage(customerList);
	}

	// Faq, FaqGroup
	public List<Faq> getFaqList(FaqSearch search) {
		return this.faqDao.getFaqList(search);
	}

	public Faq getFaqByQuestion(FaqSearch search) {
		return this.faqDao.getFaqByQuestion(search);
	}

	public void insertFaq(Faq faq) {
		this.faqDao.insertFaq(faq);
	}

	public void updateFaq(Faq faq) {
		this.faqDao.updateFaq(faq);
	}

	public void deleteFaq(Integer faqId) {
		this.faqDao.deleteFaq(faqId);
	}

	public Faq getFaqById(Integer faqId) {
		return this.faqDao.getFaqById(faqId);
	}

	public void updateFaqRanking(List data) {
		this.faqDao.updateRanking(data);
	}

	public void insertFaqGroup(FaqGroup faqGroup) {
		this.faqDao.insertFaqGroup(faqGroup);
	}

	public void updateFaqGroup(FaqGroup faqGroup) {
		this.faqDao.updateFaqGroup(faqGroup);
	}

	public void deleteFaqGroup(Integer groupId) {
		this.faqDao.deleteFaqGroup(groupId);
	}

	public FaqGroup getFaqGroupById(Integer groupId) {
		return this.faqDao.getFaqGroupById(groupId);
	}

	public void updateFaqGroupRanking(List data) {
		this.faqDao.updateFaqGroupRanking(data);
	}

	public List<FaqGroup> getFaqGroupList(String protectedAccess) {
		return this.faqDao.getFaqGroupList(protectedAccess);
	}

	// Image
	public boolean resizeImage(String iPath, String oPath, int maxWidth, int maxHeight, String contentType) {
		return imageManipulator.resize(iPath, oPath, maxWidth, maxHeight, contentType);
	}

	public void watermarkImage(String iPath, String oPath, String watermarkText, int r, int g, int b, int watermarkSize, String contentType) {
		imageManipulator.watermark(iPath, oPath, watermarkText, new Font("SansSerif", Font.PLAIN, watermarkSize), new Color(r, g, b), contentType);
	}

	// PDF
	public boolean createPdfInvoice(File pdfFile, int orderId, HttpServletRequest request) {
		List<Integer> orderIds = new ArrayList<Integer>();
		orderIds.add(orderId);
		return createPdfInvoice(pdfFile, orderIds, request);
	}

	public boolean createPdfInvoice(File pdfFile, List<Integer> orderIds, HttpServletRequest request) {
		boolean created = false;

		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("countryMap", getCountryMap());
		map.put("siteConfig", siteConfig);
		map.put("gSiteConfig", gSiteConfig);

		Layout pdfLayout = getSystemLayout("invoicePDF", request.getHeader("host"), request);
		String headerText = pdfLayout.getHeaderHtml();
		if (headerText != null) {
			for (Integer orderId : orderIds) {
				Order order = getOrder(orderId, siteConfig.get("INVOICE_LINEITEMS_SORTING").getValue());
				Customer customer = getCustomerById(order.getUserId());
				Class<Customer> c = Customer.class;
				Method m = null;
				Object arglist[] = null;
				for (int i = 1; i <= 20; i++) {
					try {
						m = c.getMethod("getField" + i);
						pdfLayout.replace("#customerField" + i + "#", ((String) m.invoke(customer, arglist) == null) ? "" : (String) m.invoke(customer, arglist));
					} catch (Exception e) {
						// do nothing
					}
				}
			}
			headerText = pdfLayout.getHeaderHtml();
			map.put("headerText", headerText);
		}

		List<Customer> customers = new ArrayList<Customer>();
		List<CustomerField> getCustomerFields = getCustomerFields();
		List<ProductField> thisProductFields = getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));

		// get order
		List<Order> orders = new ArrayList<Order>();
		int index = 0;
		for (Integer orderId : orderIds) {
			Order order = getOrder(orderId, siteConfig.get("INVOICE_LINEITEMS_SORTING").getValue());
			// To show action history on PDF set the action history on order
			if ((Boolean) gSiteConfig.get("gBUDGET")) {
				List<Order> actionOrdersList = getOrderActionHistory(order.getOrderId());
				List<Integer> ids = new ArrayList();
				for (Order id : actionOrdersList) {
					ids.add(id.getActionBy());
				}
				// to get all the managers IDs
				if (order.getStatus() != null && order.getStatus().equals("xq")) {
					for (Customer node : getFamilyTree(order.getUserId(), "up")) {
						if (order.getUserId().compareTo(node.getId()) != 0) {
							Order orderAction = new Order();
							orderAction.setActionBy(node.getId());
							if (!(ids.contains(orderAction.getActionBy()))) {
								actionOrdersList.add(orderAction);
							}
						}
					}
				}
				order.setActionHistory(actionOrdersList);
				List<Integer> userIdList = new ArrayList();
				for (Order actionById : order.getActionHistory()) {
					userIdList.add(actionById.getActionBy());
				}
				Map<Integer, String> nameMap = getCustomerNameMap(userIdList);

				for (Order actionByName : order.getActionHistory()) {
					if (nameMap.containsKey((long) actionByName.getActionBy())) {
						actionByName.setActionByName(nameMap.get((long) actionByName.getActionBy()));
					}
				}
			}
			orders.add(order);

			// set tax on shipping
			State state = this.getStateByCode(order.getShipping().getCountry(), order.getShipping().getStateProvince());
			if (state != null) {
				order.setTaxOnShipping(state.isApplyShippingTax());
			}

			// get sales rep
			if ((Boolean) gSiteConfig.get("gSALES_REP")) {
				map.put("salesRep" + index, getSalesRepById(order.getSalesRepId()));
				map.put("salesRepProcessedBy" + index, getSalesRepById(order.getSalesRepProcessedById()));
			}

			// get customer
			Customer customer = getCustomerById(order.getUserId());
			customers.add(customer);

			// customer fields
			List<CustomerField> customerFields = new ArrayList<CustomerField>();
			Class<Customer> c1 = Customer.class;
			Method m1 = null;
			Object arglist1[] = null;
			if (customer != null) {
				for (CustomerField customerField : getCustomerFields) {
					if (customerField.isEnabled() && customerField.isShowOnInvoice()) {
						try {
							m1 = c1.getMethod("getField" + customerField.getId());
							customerField.setValue((String) m1.invoke(customer, arglist1));
							if (customerField.getValue() != null && customerField.getValue().length() > 0) {
								customerFields.add(customerField);
							}
						} catch (Exception e) {
							// do nothing
						}
					}
				}
			}
			map.put("customerFields" + index, customerFields);

			Class<Product> c = Product.class;
			Method m = null;
			Object arglist[] = null;
			List<ProductField> productFieldsHeader = new ArrayList<ProductField>();

			for (LineItem lineItem : order.getLineItems()) {
				List<ProductField> productFields = new ArrayList<ProductField>();
				Product product = getProductById(getProductIdBySku(lineItem.getProduct().getSku()), 0, false, null);
				if (product != null) {
					// product fields
					for (ProductField productField : thisProductFields) {
						if (productField.isEnabled() && productField.isShowOnInvoice()) {
							ProductField newProductField = new ProductField();
							newProductField.setId(productField.getId());
							try {
								m = c.getMethod("getField" + productField.getId());
								newProductField.setValue((String) m.invoke(product, arglist));
							} catch (Exception e) {
								return false;
							}
							if (newProductField.getValue() != null && newProductField.getValue().length() > 0) {
								if (!productFieldsHeader.toString().contains(productField.toString())) {
									productFieldsHeader.add(productField);
								}
								productFields.add(newProductField);
							}
						}
					}
				}
				lineItem.setProductFields(productFields);
			}
			map.put("productFieldsHeader" + index, productFieldsHeader);

			index++;
		}

		File baseImageDir = new File(getServletContext().getRealPath("/assets/Image"));
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				baseImageDir = new File((String) prop.get("site.root"), "/assets/Image");
			}
		} catch (Exception e) {
		}
		try {
			iText.createPdfInvoice(pdfFile, orders, customers, request, map, baseImageDir, getApplicationContext());
			created = true;
		} catch (Exception e) {
			// do nothing
		}
		return created;
	}
	
	
	// PDF
	public boolean createPdfInvoice(File pdfFile, int orderId, HttpServletRequest request, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) {
		List<Integer> orderIds = new ArrayList<Integer>();
		orderIds.add(orderId);
		return createPdfInvoice(pdfFile, orderIds, request, siteConfig, gSiteConfig);
	}
	
	public boolean createPdfInvoice(File pdfFile, List<Integer> orderIds, HttpServletRequest request, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) {
		boolean created = false;
		
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("countryMap", getCountryMap());
		map.put("siteConfig", siteConfig);
		map.put("gSiteConfig", gSiteConfig);
		
		String host = "";
		if(request!=null){
			host = request.getHeader("host");
		}
		String headerText = null;
		Layout pdfLayout = getSystemLayout2("invoicePDF", host, request, gSiteConfig);
		if(pdfLayout!=null){
			headerText = pdfLayout.getHeaderHtml();
		}
		if (headerText != null) {
			for (Integer orderId : orderIds) {
				Order order = getOrder(orderId, siteConfig.get("INVOICE_LINEITEMS_SORTING").getValue());
				Customer customer = getCustomerById(order.getUserId());
				Class<Customer> c = Customer.class;
				Method m = null;
				Object arglist[] = null;
				for (int i = 1; i <= 20; i++) {
					try {
						m = c.getMethod("getField" + i);
						pdfLayout.replace("#customerField" + i + "#", ((String) m.invoke(customer, arglist) == null) ? "" : (String) m.invoke(customer, arglist));
					} catch (Exception e) {
						// do nothing
					}
				}
			}
			headerText = pdfLayout.getHeaderHtml();
			map.put("headerText", headerText);
		}

		List<Customer> customers = new ArrayList<Customer>();
		List<CustomerField> getCustomerFields = getCustomerFields();
		List<ProductField> thisProductFields = getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));

		// get order
		List<Order> orders = new ArrayList<Order>();
		int index = 0;
		for (Integer orderId : orderIds) {
			Order order = getOrder(orderId, siteConfig.get("INVOICE_LINEITEMS_SORTING").getValue());
			// To show action history on PDF set the action history on order
			if ((Boolean) gSiteConfig.get("gBUDGET")) {
				List<Order> actionOrdersList = getOrderActionHistory(order.getOrderId());
				List<Integer> ids = new ArrayList();
				for (Order id : actionOrdersList) {
					ids.add(id.getActionBy());
				}
				// to get all the managers IDs
				if (order.getStatus() != null && order.getStatus().equals("xq")) {
					for (Customer node : getFamilyTree(order.getUserId(), "up")) {
						if (order.getUserId().compareTo(node.getId()) != 0) {
							Order orderAction = new Order();
							orderAction.setActionBy(node.getId());
							if (!(ids.contains(orderAction.getActionBy()))) {
								actionOrdersList.add(orderAction);
							}
						}
					}
				}
				order.setActionHistory(actionOrdersList);
				List<Integer> userIdList = new ArrayList();
				for (Order actionById : order.getActionHistory()) {
					userIdList.add(actionById.getActionBy());
				}
				Map<Integer, String> nameMap = getCustomerNameMap(userIdList);

				for (Order actionByName : order.getActionHistory()) {
					if (nameMap.containsKey((long) actionByName.getActionBy())) {
						actionByName.setActionByName(nameMap.get((long) actionByName.getActionBy()));
					}
				}
			}
			orders.add(order);

			// set tax on shipping
			State state = this.getStateByCode(order.getShipping().getCountry(), order.getShipping().getStateProvince());
			if (state != null) {
				order.setTaxOnShipping(state.isApplyShippingTax());
			}

			// get sales rep
			if ((Boolean) gSiteConfig.get("gSALES_REP")) {
				map.put("salesRep" + index, getSalesRepById(order.getSalesRepId()));
				map.put("salesRepProcessedBy" + index, getSalesRepById(order.getSalesRepProcessedById()));
			}

			// get customer
			Customer customer = getCustomerById(order.getUserId());
			customers.add(customer);

			// customer fields
			List<CustomerField> customerFields = new ArrayList<CustomerField>();
			Class<Customer> c1 = Customer.class;
			Method m1 = null;
			Object arglist1[] = null;
			if (customer != null) {
				for (CustomerField customerField : getCustomerFields) {
					if (customerField.isEnabled() && customerField.isShowOnInvoice()) {
						try {
							m1 = c1.getMethod("getField" + customerField.getId());
							customerField.setValue((String) m1.invoke(customer, arglist1));
							if (customerField.getValue() != null && customerField.getValue().length() > 0) {
								customerFields.add(customerField);
							}
						} catch (Exception e) {
							// do nothing
						}
					}
				}
			}
			map.put("customerFields" + index, customerFields);

			Class<Product> c = Product.class;
			Method m = null;
			Object arglist[] = null;
			List<ProductField> productFieldsHeader = new ArrayList<ProductField>();

			for (LineItem lineItem : order.getLineItems()) {
				List<ProductField> productFields = new ArrayList<ProductField>();
				Product product = getProductById(getProductIdBySku(lineItem.getProduct().getSku()), 0, false, null);
				if (product != null) {
					// product fields
					for (ProductField productField : thisProductFields) {
						if (productField.isEnabled() && productField.isShowOnInvoice()) {
							ProductField newProductField = new ProductField();
							newProductField.setId(productField.getId());
							try {
								m = c.getMethod("getField" + productField.getId());
								newProductField.setValue((String) m.invoke(product, arglist));
							} catch (Exception e) {
								return false;
							}
							if (newProductField.getValue() != null && newProductField.getValue().length() > 0) {
								if (!productFieldsHeader.toString().contains(productField.toString())) {
									productFieldsHeader.add(productField);
								}
								productFields.add(newProductField);
							}
						}
					}
				}
				lineItem.setProductFields(productFields);
			}
			map.put("productFieldsHeader" + index, productFieldsHeader);

			index++;
		}

		File baseImageDir = new File(getServletContext().getRealPath("/assets/Image"));
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				baseImageDir = new File((String) prop.get("site.root"), "/assets/Image");
			}
		} catch (Exception e) {
		}
		try {
			iText.createPdfInvoice(pdfFile, orders, customers, request, map, baseImageDir, getApplicationContext());
			created = true;
		} catch (Exception e) {
			// do nothing
		}
		return created;
	}

	// Presentation PDF
	public boolean createPdfPresentation(HttpServletRequest request, HttpServletResponse response, List<Integer> productIds, Integer noOfProduct, Presentation presentation, SalesRep salesrep,
			Map<String, Object> gSiteConfig, Map<Integer, Double> presentationProductPrice, boolean admin) {
		File tempFolder = new File(getServletContext().getRealPath("temp"));

		if (!tempFolder.exists()) {
			tempFolder.mkdir();
		}

		File pdfFile = new File(tempFolder, "presentation.pdf");
		List<ProductField> thisProductFields = getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
		List<ProductField> productFieldsHeader = new ArrayList<ProductField>();

		boolean result = false;
		Map<String, Object> map = new HashMap<String, Object>();
		List<Product> products = new ArrayList<Product>();
		map.put("gSiteConfig", gSiteConfig);
		Method m = null;
		Object arglist[] = null;
		int index = 0;
		Class<Product> c = Product.class;
		try {
			ITextApi iText = new ITextApi();
			for (Integer productId : productIds) {
				Product product = getProductById(productId, (Integer) gSiteConfig.get("gPRODUCT_IMAGES"), false, null);
				if (product != null) {
					List<ProductField> productFields = new ArrayList<ProductField>();
					Iterator pfIter = productFields.iterator();
					while (pfIter.hasNext()) {
						ProductField productField = (ProductField) pfIter.next();
						if (!productField.isEnabled()) {
							pfIter.remove();
						}
					}
					product.setProductFields(productFields);
					for (ProductField productField : thisProductFields) {
						if (productField.isEnabled() && productField.isShowOnPresentation()) {
							ProductField newProductField = new ProductField();
							newProductField.setId(productField.getId());
							try {
								m = c.getMethod("getField" + productField.getId());
								newProductField.setValue((String) m.invoke(product, arglist));
							} catch (Exception e) {
								return false;
							}
							if (newProductField.getValue() != null && newProductField.getValue().length() > 0) {
								if (!productFieldsHeader.toString().contains(productField.toString())) {
									productFieldsHeader.add(productField);
								}
								productFields.add(newProductField);
							}
						}
					}

					products.add(product);

				}
				map.put("productFieldsHeader" + index, productFieldsHeader);
				index++;
			}

			File baseImageDir = new File(getServletContext().getRealPath("/assets/Image"));
			Properties prop = new Properties();
			try {
				prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
				if (prop.get("site.root") != null) {
					baseImageDir = new File((String) prop.get("site.root"), "/assets/Image");
				}
			} catch (Exception e) {
			}

			iText.createSalesRepPdfPresentation(pdfFile, request, products, baseImageDir, noOfProduct, presentation, salesrep, map, presentationProductPrice, admin);
			result = true;

			byte[] b = getBytesFromFile(pdfFile);

			response.addHeader("Content-Disposition", "attachment; filename=" + pdfFile.getName());
			response.setBufferSize((int) pdfFile.length());
			response.setContentType("application/pdf");
			response.setContentLength((int) pdfFile.length());

			// output stream
			ServletOutputStream ouputStream = response.getOutputStream();
			ouputStream.write(b, 0, (int) pdfFile.length());
			ouputStream.flush();
			ouputStream.close();

			// delete file before exit
			pdfFile.delete();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	// PDF
	public boolean createPdfPO(File pdfFile, PurchaseOrder purchaseOrder, HttpServletRequest request) {
		boolean created = false;

		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("countryMap", getCountryMap());
		map.put("siteConfig", siteConfig);
		map.put("gSiteConfig", gSiteConfig);
		map.put("purchaseOrder", purchaseOrder);

		File baseImageDir = new File(getServletContext().getRealPath("/assets/Image"));
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				baseImageDir = new File((String) prop.get("site.root"), "/assets/Image");
			}
		} catch (Exception e) {
		}
		try {
			iText.createPdfPO(pdfFile, purchaseOrder, request, map, baseImageDir, getApplicationContext());
			created = true;
		} catch (Exception e) {
			e.printStackTrace();
			// do nothing
		}
		return created;
	}

	public boolean createPdfPackingList(File pdfFile, int orderId, HttpServletRequest request, Integer supplierId) {
		boolean created = false;

		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("countryMap", getCountryMap());
		map.put("siteConfig", siteConfig);

		// get order
		Order order = getOrder(orderId, siteConfig.get("INVOICE_LINEITEMS_SORTING").getValue());

		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		List<ProductField> thisProductFields = getProductFields(null, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"));
		List<ProductField> productFieldsHeader = new ArrayList<ProductField>();
		Iterator<LineItem> lineItemsIterator = order.getLineItems().iterator();
		while (lineItemsIterator.hasNext()) {
			LineItem lineItem = lineItemsIterator.next();
			List<ProductField> productFields = new ArrayList<ProductField>();
			Product product = getProductById(lineItem.getProductId(), 0, false, null);
			if (supplierId != null) {
				Supplier supplier = getProductSupplierBySIdSku(lineItem.getProduct().getSku(), supplierId);
				if (supplier != null) {
					lineItem.setSupplier(supplier);
					if (supplier.getSupplierSku() != null && supplier.getSupplierSku().trim().length() > 0) {
						map.put("hasSupplierSku", true);
					}
				} else {
					lineItemsIterator.remove();
					continue;
				}
			}
			if (product != null) {
				// product fields
				for (ProductField productField : thisProductFields) {
					if (productField.isEnabled() && productField.isPackingField()) {
						ProductField newProductField = new ProductField();
						newProductField.setId(productField.getId());
						try {
							m = c.getMethod("getField" + productField.getId());
							newProductField.setValue((String) m.invoke(product, arglist));
						} catch (Exception e) {
							return false;
						}
						if (newProductField.getValue() != null && newProductField.getValue().length() > 0) {
							if (!productFieldsHeader.toString().contains(productField.toString())) {
								productFieldsHeader.add(productField);
							}
							productFields.add(newProductField);
						}
					}
				}
			}
			lineItem.setProductFields(productFields);
		}
		map.put("productFieldsHeader", productFieldsHeader);

		File baseImageDir = new File(getServletContext().getRealPath("/assets/Image"));
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.root") != null) {
				baseImageDir = new File((String) prop.get("site.root"), "/assets/Image");
			}
		} catch (Exception e) {
		}
		try {
			iText.createPdfPackingList(pdfFile, order, request, map, baseImageDir, getApplicationContext());
			created = true;
		} catch (Exception e) {
			// do nothing
		}
		return created;
	}

	// admin config: Country, State,ShippingMethod

	// Country

	public Country getCountryByCode(String code) {
		return this.configDao.getCountryByCode(code);
	}
	
	public String getCountryCodeByName(String name){
		return this.configDao.getCountryCodeByName(name);
	}

	public void updateCountryList(List data) {
		this.configDao.updateCountryList(data);
	}

	public List<Country> getCountryList(boolean stats) {
		return this.configDao.getCountryList(stats);
	}

	public List<Country> getEnabledCountryList() {
		return this.configDao.getEnabledCountryList();
	}
	
	public List<MobileCarrier> getMobileCarrierList(){
		return this.customerDao.getMobileCarrierList();
	}

	public Map<String, String> getCountryMap() {
		Map<String, String> map = new HashMap<String, String>();
		Iterator<Country> iter = getCountryList(false).iterator();
		while (iter.hasNext()) {
			Country country = iter.next();
			map.put(country.getCode(), country.getName());
		}
		return map;
	}

	// State

	public State getStateByCode(String country, String code) {
		return this.configDao.getStateByCode(country, code);
	}

	public List<State> getStateList(String country) {
		return this.configDao.getStateList(country);
	}
	
	public List<State> getNationalRegionList() {
		return this.configDao.getNationalRegionList();
	}

	public void updateStateList(List data) {
		this.configDao.updateStateList(data);
	}

	public Map<String, String> getStateMap(String country) {
		Map<String, String> map = new HashMap<String, String>();
		Iterator<State> iter = getStateList(country).iterator();
		while (iter.hasNext()) {
			State state = iter.next();
			map.put(state.getCode(), state.getName());
		}
		return map;
	}

	// ShippingMethod

	public ShippingMethod getShippingMethodById(Integer id) {
		return this.configDao.getShippingMethodById(id);
	}

	public ShippingMethod getShippingMethodByCarrierCode(ShippingRate shippingRate) {
		return this.configDao.getShippingMethodByCarrierCode(shippingRate);
	}

	public void updateShippingMethodList(List<ShippingMethod> data) {
		this.configDao.updateShippingMethodList(data);
	}

	public List<ShippingMethod> getShippingMethodList(Boolean active) {
		return this.configDao.getShippingMethodList(active);
	}

	public List<ShippingRate> getCustomShippingRateList(boolean all, boolean internal) {
		List<ShippingRate> customList = new ArrayList<ShippingRate>();
		Iterator<ShippingRate> iter = this.configDao.getCustomShippingRateList().iterator();
		while (iter.hasNext()) {
			ShippingRate shipRate = iter.next();
			if (!shipRate.getTitle().trim().equals("") || all) {
				if (internal) {
					customList.add(shipRate);
				} else if (!shipRate.isInternal()) {
					customList.add(shipRate);
				}
			}
		}
		return customList;
	}

	public List<Contact> getCustomShippingContactList(Search search) {
		return this.configDao.getCustomShippingContactList(search);
	}
	
	public List<Contact> getActiveCustomShippingContactList(Search search){
		return this.configDao.getActiveCustomShippingContactList(search);

	}


	public Contact getCustomShippingContactById(Integer id) {
		return this.configDao.getCustomShippingContactById(id);
	}

	public void insertCustomShippingContact(Contact contact) {
		this.configDao.insertCustomShippingContact(contact);
	}

	public void updateCustomShippingContact(Contact contact) {
		this.configDao.updateCustomShippingContact(contact);
	}

	public void deleteCustomShippingContact(Integer contactId) {
		this.configDao.deleteCustomShippingContact(contactId);
	}

	public void updateCustomShipping(ShippingRate shippingRate) {
		this.configDao.updateCustomShipping(shippingRate);
	}

	public void insertCustomShipping(ShippingRate shippingRate) {
		this.configDao.insertCustomShipping(shippingRate);
	}

	public ShippingRate getCustomShippingById(Integer customShippingId) {
		return this.configDao.getCustomShippingById(customShippingId);
	}

	public boolean isActiveCarrier(String carrierCode) {
		return this.configDao.isActiveCarrier(carrierCode);
	}

	public ShippingHandling getShippingHandling() {
		return this.configDao.getShippingHandling();
	}

	public void updateShippingHandling(ShippingHandling shippingHandling) {
		this.configDao.updateShippingHandling(shippingHandling);
	}

	public Double getHandlingByZipcode(String country, String zipcode) {
		return this.configDao.getHandlingByZipcode(country, zipcode);
	}

	public void getCustomShippingWithMaxValue(List<ShippingRate> customList, List<ShippingRate> regularList, Order order) {
		int customShippingIdWithMaxValue = -1;
		double maxValue = 0.0;
		Double customShippingPrice = null;
		Iterator<ShippingRate> iter = customList.iterator();

		// get max value of custom shipping
		// only those custom shipping will be considered which has category id
		while (iter.hasNext()) {
			ShippingRate cRate = iter.next();
			if (cRate.getCategoryId() != null) {
				if (cRate.getCarrierId() != null) {
					for (ShippingRate sRate : regularList) {
						if (sRate.getId() == cRate.getCarrierId()) {
							if (cRate.getPercent() == 0 && (cRate.getPrice() != null || cRate.getHandling() != null)) {
								customShippingPrice = Double.parseDouble(cRate.getPrice() != null ? cRate.getPrice() : "0.0") + (cRate.getHandling() != null ? cRate.getHandling() : 0.0);
							} else if (cRate.getPercent() == 1 && (cRate.getPrice() != null || cRate.getHandling() != null)) {
								customShippingPrice = ((100 - Double.parseDouble(cRate.getPrice() != null ? cRate.getPrice() : "0.0")) / 100) * Double.parseDouble(sRate.getPrice())
										+ (cRate.getHandling() != null ? cRate.getHandling() : 0.0);
							}
						}
					}
				} else if (cRate.getPercent() == 0 && (cRate.getPrice() != null || cRate.getHandling() != null)) {
					customShippingPrice = Double.parseDouble(cRate.getPrice() != null ? cRate.getPrice() : "0.0") + (cRate.getHandling() != null ? cRate.getHandling() : 0.0);
				} else if (cRate.getPercent() == 2 && (cRate.getPrice() != null || cRate.getHandling() != null)) {
					customShippingPrice = (Double.parseDouble(cRate.getPrice() != null ? cRate.getPrice() : "0.0") / 100) * order.getSubTotal()
							+ (cRate.getHandling() != null ? cRate.getHandling() : 0.0);
				} else if (cRate.getPercent() == 3 && (cRate.getPrice() != null || cRate.getHandling() != null)) {
					customShippingPrice = (Double.parseDouble(cRate.getPrice() != null ? cRate.getPrice() : "0.0")) * (order.getTotalWeight() != null ? order.getTotalWeight() : 0)
							+ (cRate.getHandling() != null ? cRate.getHandling() : 0.0);
				}
				if (customShippingPrice != null && customShippingPrice >= maxValue) {
					maxValue = customShippingPrice;
					customShippingIdWithMaxValue = cRate.getId();
				}
			}
		}

		// remove custom shipping who has category id and values is less than max value
		Iterator<ShippingRate> secondIter = customList.iterator();
		while (secondIter.hasNext()) {
			ShippingRate cRate = secondIter.next();
			if (cRate.getCategoryId() != null && cRate.getId() != customShippingIdWithMaxValue) {
				secondIter.remove();
			}
		}

	}

	// counties
	public List<County> getCounties(String country, String state) {
		return this.configDao.getCounties(country, state);
	}

	public void updateCountyList(List<County> counties) {
		this.configDao.updateCountyList(counties);
	}

	public County getTaxRateByCounty(County county) {
		return this.configDao.getTaxRateByCounty(county);
	}

	// cities
	public List<City> getCities(City city) {
		return this.configDao.getCities(city);
	}

	public void updateCityList(List<City> cities) {
		this.configDao.updateCityList(cities);
	}

	public City getTaxRateByCity(City city) {
		return this.configDao.getTaxRateByCity(city);
	}

	// Payment Method
	public List<PaymentMethod> getCustomPaymentMethodList(Integer custId) {
		return this.configDao.getCustomPaymentMethodList(custId);
	}

	public void updateCustomPaymentMethods(List<PaymentMethod> customPayments) {
		this.configDao.updateCustomPaymentMethods(customPayments);
	}

	// Site Configuration
	public Map<String, Configuration> getSiteConfig() {
		return this.configDao.getSiteConfig();
	}

	public void updateSiteConfig(List<Configuration> data) {
		this.configDao.updateSiteConfig(data);
	}

	public void updateSiteConfig(String key, String value) {
		List<Configuration> data = new ArrayList<Configuration>();
		Configuration config = new Configuration();
		config.setKey(key);
		config.setValue(value);
		data.add(config);
		updateSiteConfig(data);
	}

	public String getSiteId() {
		Properties prop = new Properties();
		String siteId = "none";
		try {
			prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
			if (prop.get("site.id") != null) {
				siteId = prop.get("site.id").toString();
			}
		} catch (Exception e) {
		}
		return siteId;
	}

	public int getNextIndexByName(String accountName) {
		return this.customerDao.getNextIndexByName(accountName);
	}

	// Site Message
	public List<SiteMessage> getSiteMessageList() {
		return this.configDao.getSiteMessageList();
	}

	public void deleteSiteMessage(Integer messageId) {
		this.configDao.deleteSiteMessage(messageId);
	}

	public void insertSiteMessage(SiteMessage siteMessage) {
		this.configDao.insertSiteMessage(siteMessage);
	}

	public void insertEmailHistory(Integer userId, Integer contactId, Integer messageId, Date created) {
		this.configDao.insertEmailHistory(userId, contactId, messageId, created);
	}
	
	public void updateSiteMessage(SiteMessage siteMessage) {
		this.configDao.updateSiteMessage(siteMessage);
	}

	public SiteMessage getSiteMessageById(Integer messageId) {
		return this.configDao.getSiteMessageById(messageId);
	}

	public SiteMessage getSiteMessageByName(String messageName) {
		return this.configDao.getSiteMessageByName(messageName);
	}
	
	public List<SiteMessage> getSiteMessageList(SiteMessageSearch siteMessageSearch) {
		return this.configDao.getSiteMessageList(siteMessageSearch);
	}
	
	public SiteMessageSearch getSiteMessageSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		SiteMessageSearch siteMessageSearch = (SiteMessageSearch) request.getSession().getAttribute("siteMessageSearch");
		// 'emptyFilter' is used to reset all the filters to show the sub-accounts of 'parent'
		if (siteMessageSearch == null || request.getParameter("emptyFilter") != null) {
			siteMessageSearch = new SiteMessageSearch();
			siteMessageSearch.setPageSize(Integer.parseInt(siteConfig.get("DEFAULT_LIST_SIZE").getValue()));
			request.getSession().setAttribute("siteMessageSearch", siteMessageSearch);
		}

		// sorting
		if (request.getParameter("sort") != null) {
			siteMessageSearch.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}
		
		if (request.getParameter("messageId") != null) {
			siteMessageSearch.setMessageId(ServletRequestUtils.getIntParameter(request, "messageId", -1));
		}
		
		if (request.getParameter("messageName") != null) {
			siteMessageSearch.setMessageName(ServletRequestUtils.getStringParameter(request, "messageName", ""));
		}
		
		if (request.getParameter("message") != null) {
			siteMessageSearch.setMessage(ServletRequestUtils.getStringParameter(request, "message", ""));
		}
		
		if (request.getParameter("groupId") != null) {
			siteMessageSearch.setGroupId(ServletRequestUtils.getIntParameter(request, "groupId", -1));
		}
		

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				siteMessageSearch.setPage(1);
			} else {
				siteMessageSearch.setPage(page);
			}
		}

		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				siteMessageSearch.setPageSize(10);
			} else {
				siteMessageSearch.setPageSize(size);
			}
		}


		return siteMessageSearch;
	}
	
	// Site Message Group
	public List<SiteMessageGroup> getSiteMessageGroupList() {
		return this.configDao.getSiteMessageGroupList();
	}
	
	public void insertSiteMessageGroup(SiteMessageGroup group) throws DataAccessException {
		this.configDao.insertSiteMessageGroup(group);
	}
	
	public void updateSiteMessageGroup(SiteMessageGroup group) {
		this.configDao.updateSiteMessageGroup(group);
	}
	
	public SiteMessageGroup getSiteMessageGroupById(Integer groupId) {
		return this.configDao.getSiteMessageGroupById(groupId);
	}
	
	public void deleteSiteMessageGroupById(Integer groupId) {
		this.configDao.deleteSiteMessageGroupById(groupId);
	}
	
	public List<SiteMessageGroup> getSiteMessageGroupList(SiteMessageGroupSearch search) {
		return this.configDao.getSiteMessageGroupList(search);
	}
	
	
	// language
	public void updateSiteMessageLanguage(Language language) {
		this.configDao.updateSiteMessageLanguage(language);
	}

	public Language getLanguageSetting(String languageCode) {
		return this.configDao.getLanguageSetting(languageCode);
	}

	public List<Language> getLanguageSettings() {
		return this.configDao.getLanguageSettings();
	}

	public Language getLanguage(String languageCode) {
		// TODO Auto-generated method stub
		return null;
	}

	public void insertLanguage(String languageCode) {
		this.configDao.insertLanguage(languageCode);
	}

	public void deleteLanguage(String languageCode) {
		this.configDao.deleteLanguage(languageCode);
	}

	// Admin Message
	public SiteMessage getAdminMessageById(int messageId) {
		return this.configDao.getAdminMessageById(messageId);
	}

	public void updateAdminMessage(SiteMessage adminMessage) {
		this.configDao.updateAdminMessage(adminMessage);
	}

	// Layout
	public Layout getLayout(int id, HttpServletRequest request) {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		String host = request.getHeader("host");
		String mobile = "";
		if (request.getAttribute("_mobile") !=null && (request.getAttribute("_mobile").toString().equalsIgnoreCase("_mobile"))) {
			mobile = request.getAttribute("_mobile").toString();
		}
		Layout layout = this.layoutDao.getLayout(new Layout(id, host, ""));
		if (layout == null) {
			layout = this.layoutDao.getLayout(new Layout(id, "", ""));
		}
		String lang = RequestContextUtils.getLocale(request).getLanguage();
		if (((String) gSiteConfig.get("gI18N")).length() > 0 && this.configDao.getLanguageSetting(lang) != null) {
			layout.setLang(lang);
			Layout i18n = this.layoutDao.getLayout(layout);
			if (i18n != null) {
				if (i18n.getHeadTag().trim().length() > 0) {
					layout.setHeadTag(i18n.getHeadTag());
				}
				if (i18n.getHeaderHtml().trim().length() > 0) {
					layout.setHeaderHtml(i18n.getHeaderHtml());
				}
				if (i18n.getHeaderHtmlLogin().trim().length() > 0) {
					layout.setHeaderHtmlLogin(i18n.getHeaderHtmlLogin());
				}
				if (i18n.getTopBarHtml().trim().length() > 0) {
					layout.setTopBarHtml(i18n.getTopBarHtml());
				}
				if (i18n.getTopBarHtmlLogin().trim().length() > 0) {
					layout.setTopBarHtmlLogin(i18n.getTopBarHtmlLogin());
				}
				if (i18n.getLeftBarTopHtml().trim().length() > 0) {
					layout.setLeftBarTopHtml(i18n.getLeftBarTopHtml());
				}
				if (i18n.getLeftBarTopHtmlLogin().trim().length() > 0) {
					layout.setLeftBarTopHtmlLogin(i18n.getLeftBarTopHtmlLogin());
				}
				if (i18n.getLeftBarBottomHtml().trim().length() > 0) {
					layout.setLeftBarBottomHtml(i18n.getLeftBarBottomHtml());
				}
				if (i18n.getLeftBarBottomHtmlLogin().trim().length() > 0) {
					layout.setLeftBarBottomHtmlLogin(i18n.getLeftBarBottomHtmlLogin());
				}
				if (i18n.getRightBarTopHtml().trim().length() > 0) {
					layout.setRightBarTopHtml(i18n.getRightBarTopHtml());
				}
				if (i18n.getRightBarTopHtmlLogin().trim().length() > 0) {
					layout.setRightBarTopHtmlLogin(i18n.getRightBarTopHtmlLogin());
				}
				if (i18n.getRightBarBottomHtml().trim().length() > 0) {
					layout.setRightBarBottomHtml(i18n.getRightBarBottomHtml());
				}
				if (i18n.getRightBarBottomHtmlLogin().trim().length() > 0) {
					layout.setRightBarBottomHtmlLogin(i18n.getRightBarBottomHtmlLogin());
				}
				if (i18n.getFooterHtml().trim().length() > 0) {
					layout.setFooterHtml(i18n.getFooterHtml());
				}
				if (i18n.getFooterHtmlLogin().trim().length() > 0) {
					layout.setFooterHtmlLogin(i18n.getFooterHtmlLogin());
				}
			}
		}
		if (((Boolean) gSiteConfig.get( "gMOBILE_LAYOUT" ) && mobile.equalsIgnoreCase("_mobile"))) {
			// TODO replace layout with Mobile layout
			if (!layout.getHeaderHtmlMobile().trim().isEmpty()) {
				layout.setHeaderHtml(layout.getHeaderHtmlMobile());
			}
			if (!layout.getFooterHtmlMobile().trim().isEmpty()) {
				layout.setFooterHtml(layout.getFooterHtmlMobile());
			}
			if (!layout.getHeadTagMobile().trim().isEmpty()) {
				layout.setHeadTag(layout.getHeadTagMobile());
			}
			if(!layout.getTopBarHtmlMobile().trim().isEmpty()){
				layout.setTopBarHtml(layout.getTopBarHtmlMobile());
			}
			if (((String) gSiteConfig.get("gI18N")).length() > 0 && this.configDao.getLanguageSetting(lang) != null) {
				layout.setLang(lang);
				Layout i18n = this.layoutDao.getLayout(layout);
				if (i18n != null) {
					if (i18n.getHeadTagMobile().trim().length() > 0) {
						layout.setHeadTag(i18n.getHeadTagMobile());
						layout.setHeadTagMobile(i18n.getHeadTagMobile());
					}
					if (i18n.getHeaderHtmlMobile().trim().length() > 0) {
						layout.setHeaderHtml(i18n.getHeaderHtmlMobile());
						layout.setHeaderHtmlMobile(i18n.getHeaderHtmlMobile());
					}
					if (i18n.getFooterHtmlMobile().trim().length() > 0) {
						layout.setFooterHtml(i18n.getFooterHtmlMobile());
						layout.setFooterHtmlMobile(i18n.getFooterHtmlMobile());
					}
					if (i18n.getTopBarHtmlMobile().trim().length() > 0) {
						layout.setTopBarHtml(i18n.getTopBarHtmlMobile());
						layout.setTopBarHtmlMobile(i18n.getTopBarHtmlMobile());
					}
				}
			}
		}
		
		return layout;
	}

	public List<Layout> getLayoutList() {
		return this.layoutDao.getLayoutList();
	}

	public Layout getLayoutByCategoryId(int cid, HttpServletRequest request, HttpServletResponse response) {

		Layout layout = this.layoutDao.getLayoutByCategoryId(cid);

		layout = getLayout(layout.getId(), request);
		UserSession userSession = (UserSession) request.getAttribute("userSession");
		if (userSession != null) {
			layout.initLayout(request.getAttribute("_mobile").toString().equalsIgnoreCase("_mobile"));
			layout.replace("#firstname#", userSession.getFirstName());
			layout.replace("#lastname#", userSession.getLastName());
			layout.replace("#email#", userSession.getUsername());
		} else {
			layout.replace("#firstname#", "");
			layout.replace("#lastname#", "");
			layout.replace("#email#", "");
		}
		layout.replace("#trackcode#", (String) request.getAttribute("trackcode"));

		Cookie layoutCookie = new Cookie("layout", "" + layout.getId());
		layoutCookie.setMaxAge(60 * 60 * 24 * 365 * 50);
		// without this mod rewrite pages will save multiple cookies with different paths
		layoutCookie.setPath(request.getContextPath().equals("") ? "/" : request.getContextPath());
		response.addCookie(layoutCookie);

		return layout;
	}

	public Layout getSystemLayout(String type, String host, HttpServletRequest request) {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		Layout layout = this.layoutDao.getSystemLayout(new Layout(type, host, ""));
		if (layout == null) {
			layout = this.layoutDao.getSystemLayout(new Layout(type, "", ""));
		}
		String lang = RequestContextUtils.getLocale(request).getLanguage();
		if (((String) gSiteConfig.get("gI18N")).length() > 0 && this.configDao.getLanguageSetting(lang) != null) {
			layout.setLang(lang);
			Layout i18n = this.layoutDao.getSystemLayout(layout);
			if (i18n != null) {
				if (i18n.getHeaderHtml().trim().length() > 0) {
					layout.setHeaderHtml(i18n.getHeaderHtml());
				}
				if (i18n.getFooterHtml().trim().length() > 0) {
					layout.setFooterHtml(i18n.getFooterHtml());
				}
				if (i18n.getHeadTag().trim().length() > 0) {
					layout.setHeadTag(i18n.getHeadTag());
				}
			}
		}

		Layout masterLayout = (Layout) request.getAttribute("layout");
		if (layout!=null && masterLayout != null && layout.getHeadTag().trim().length() > 0) {
			masterLayout.setHeadTag(layout.getHeadTag());
		}
		UserSession userSession = (UserSession) request.getAttribute("userSession");

		if (layout!=null){
				if (userSession != null) {
					layout.initLayout(request.getAttribute("_mobile").toString().equalsIgnoreCase("_mobile"));
					layout.replace("#email#", userSession.getUsername());
				} else {
					layout.replace("#email#", "");
				}
		}

		return layout;
	}
	
	public Layout getSystemLayout2(String type, String host, HttpServletRequest request, Map<String, Object> gSiteConfig) {

		Layout layout = this.layoutDao.getSystemLayout(new Layout(type, host, ""));
		if (layout == null) {
			layout = this.layoutDao.getSystemLayout(new Layout(type, "", ""));
		}
		String lang = "en";
		if (((String) gSiteConfig.get("gI18N")).length() > 0 && this.configDao.getLanguageSetting(lang) != null) {
			layout.setLang(lang);
			Layout i18n = this.layoutDao.getSystemLayout(layout);
			if (i18n != null) {
				if (i18n.getHeaderHtml().trim().length() > 0) {
					layout.setHeaderHtml(i18n.getHeaderHtml());
				}
				if (i18n.getFooterHtml().trim().length() > 0) {
					layout.setFooterHtml(i18n.getFooterHtml());
				}
				if (i18n.getHeadTag().trim().length() > 0) {
					layout.setHeadTag(i18n.getHeadTag());
				}
			}
		}

		Layout masterLayout = null;
		if (masterLayout != null && layout.getHeadTag().trim().length() > 0) {
			masterLayout.setHeadTag(layout.getHeadTag());
		}
		UserSession userSession = null;

		if (userSession != null) {
			layout.initLayout(request.getAttribute("_mobile").toString().equalsIgnoreCase("_mobile"));
			layout.replace("#email#", userSession.getUsername());
		} else {
			if(layout!=null){
				layout.replace("#email#", "");
			}
		}

		return layout;
	}

	public List<Layout> getSystemLayoutList(String type) {
		return this.layoutDao.getSystemLayoutList(type);
	}

	// Multi Store
	public void updateMultiStoreConfig(List<MultiStore> multiStores) {
		this.multiStoreDao.updateMultiStoreConfig(multiStores);
	}

	public List<MultiStore> getMultiStore(String host) {
		if (host == null) {
			return new ArrayList<MultiStore>();
		} else if (host.equals("all")) {
			host = null;
		}
		return this.multiStoreDao.getMultiStore(host);
	}

	public Map<String, String> getStoreIdMap() {
		return this.multiStoreDao.getStoreIdMap();
	}

	// -------------------------------------------------------------------------
	// private methods
	// -------------------------------------------------------------------------

	// set up prices
	private List<Price> getPrices(Product product, int priceTiers) {
		List<Price> prices = new ArrayList<Price>();
		Integer minimumQty = new Integer(1);
		if (product.getMinimumQty() != null) {
			minimumQty = product.getMinimumQty();
		}
		prices.add(new Price(product.getPrice1(), null, product.getSalesTag(), minimumQty, product.getQtyBreak1(), product.getCost1())); // Price 1
		prices.add(new Price(product.getPrice2(), null, product.getSalesTag(), product.getQtyBreak1(), product.getQtyBreak2(), product.getCost2())); // Price 2
		prices.add(new Price(product.getPrice3(), null, product.getSalesTag(), product.getQtyBreak2(), product.getQtyBreak3(), product.getCost3())); // Price 3
		prices.add(new Price(product.getPrice4(), null, product.getSalesTag(), product.getQtyBreak3(), product.getQtyBreak4(), product.getCost4())); // Price 4
		prices.add(new Price(product.getPrice5(), null, product.getSalesTag(), product.getQtyBreak4(), product.getQtyBreak5(), product.getCost5())); // Price 5
		prices.add(new Price(product.getPrice6(), null, product.getSalesTag(), product.getQtyBreak5(), product.getQtyBreak6(), product.getCost6())); // Price 6
		prices.add(new Price(product.getPrice7(), null, product.getSalesTag(), product.getQtyBreak6(), product.getQtyBreak7(), product.getCost7())); // Price 7
		prices.add(new Price(product.getPrice8(), null, product.getSalesTag(), product.getQtyBreak7(), product.getQtyBreak8(), product.getCost8())); // Price 8
		prices.add(new Price(product.getPrice9(), null, product.getSalesTag(), product.getQtyBreak8(), product.getQtyBreak9(), product.getCost9())); // Price 9
		prices.add(new Price(product.getPrice10(), null, product.getSalesTag(), product.getQtyBreak9(), null, product.getCost10())); // Price 10

		// Fix SalesTag requires only price 1
		if (product.getSalesTag() != null && !product.getSalesTag().isPercent()) {
			priceTiers = 1;
		}

		for (int i = priceTiers; i < prices.size(); i++) {
			prices.get(i).setAmt(null);
		}

		// check price and quantity break
		for (int i = 0; i < 10; i++) {
			Price thisPrice = (Price) prices.get(i);
			if (thisPrice.getAmt() == null) {
				for (int x = 9; x >= i; x--)
					prices.remove(x);
				break;
			} else {
				if (i < 9) {
					Price nextPrice = (Price) prices.get(i + 1);
					if ((nextPrice.getAmt() == null) || (nextPrice.getQtyFrom() == null) || (nextPrice.getQtyFrom().intValue() <= thisPrice.getQtyFrom().intValue())) {
						thisPrice.setQtyTo(null);
						if (i == 0)
							thisPrice.setQtyFrom(null);
						prices.set(i, thisPrice);
						for (int x = 9; x >= i + 1; x--)
							prices.remove(x);
						break;
					}
				}
			}
		}

		return prices;
	}

	// set up price table
	private List<Price> getPrices(Product product, Integer priceTable, Map<String, Configuration> siteConfig) {
		boolean applySalesTag = false;
		if (siteConfig.get("SALES_TAG_ON_PRICE_TABLE").getValue().equals("true")) {
			applySalesTag = true;
		}

		List<Price> prices = new ArrayList<Price>();
		if (priceTable == null)
			return prices;

		Class<Product> c = Product.class;
		Object arglist[] = null;

		try {
			Method m = c.getMethod("getPriceCasePack" + priceTable);
			Double casePrice = (Double) m.invoke(product, arglist);
			Method m2 = c.getMethod("getPriceTable" + priceTable);
			Double price = (Double) m2.invoke(product, arglist);
			if (price != null) {
				if (product.isPriceCasePack()) {
					prices.add(new Price(casePrice, casePrice, (applySalesTag) ? product.getSalesTag() : null, null, null, null));
				} else {
					prices.add(new Price(price, casePrice, (applySalesTag) ? product.getSalesTag() : null, null, null, null));
				}
				product.setSalesTag((applySalesTag) ? product.getSalesTag() : null);
				product.setSubscriptionDiscount(null);
			}
		} catch (Exception e) {
			// do nothing
		}

		return prices;
	}

	// set up special price
	private List<Price> getPrices(Product product, double specialPrice) {
		List<Price> prices = new ArrayList<Price>();
		prices.add(new Price(specialPrice, null, null, null, null, null));
		product.setSalesTag(null);
		product.setSubscriptionDiscount(null);

		return prices;
	}

	private void computeCommission(Order order, List<Integer> parentsId, int numCommTable) {
		order.setAffiliate(new Affiliate());

		int level = 1;
		for (Integer parentId : parentsId) {
			Affiliate affiliate = this.getAffiliatesCommission(parentId);
			if(affiliate.getFirstOrderCommission() != null || affiliate.getOtherOrderCommission() != null){
				if (level == 1) {
					if(order.isFirstOrder()){
						order.getAffiliate().setPercentLevel1(affiliate.getFirstOrderPercent());
						order.getAffiliate().setAffiliateLevel1Id(parentId);
						Double orderCommission = affiliate.getFirstOrderCommission();
						if(orderCommission != null){
							Double	totalOrderCommission;
							if (order.getAffiliate().getPercentLevel1()) {
								totalOrderCommission = order.getSubTotalAfterDiscount() * orderCommission / 100.00;
							} else {
								totalOrderCommission = orderCommission;
							}
							order.getAffiliate().setTotalCommissionLevel1(Utilities.roundFactory(totalOrderCommission, 2, BigDecimal.ROUND_HALF_UP));
							level++;
						}
					}else{
						order.getAffiliate().setPercentLevel1(affiliate.getOtherOrderPercent());
						order.getAffiliate().setAffiliateLevel1Id(parentId);
						Double orderCommission = affiliate.getOtherOrderCommission();
						if(orderCommission != null){
							Double	totalOrderCommission;
							if (order.getAffiliate().getPercentLevel1()) {
								totalOrderCommission = order.getSubTotalAfterDiscount() * orderCommission / 100.00;
							} else {
								totalOrderCommission = orderCommission;
							}
							order.getAffiliate().setTotalCommissionLevel1(Utilities.roundFactory(totalOrderCommission, 2, BigDecimal.ROUND_HALF_UP));
							level++;
						}
					}
				}
			}else{
				if (level == 1) {
					order.getAffiliate().setPercentLevel1(affiliate.getPercentLevel1());
					order.getAffiliate().setAffiliateLevel1Id(parentId);
					Double productCommission = getProductCommission(affiliate, order, level, numCommTable);
					Double invoiceCommission = getInvoiceCommission2(affiliate, order, level);
					if (productCommission == null && invoiceCommission == null)
						order.getAffiliate().setTotalCommissionLevel1(null);
					else if (productCommission == null && invoiceCommission != null)
						order.getAffiliate().setTotalCommissionLevel1(Utilities.roundFactory(invoiceCommission, 2, BigDecimal.ROUND_HALF_UP));
					else if (productCommission != null && invoiceCommission == null)
						order.getAffiliate().setTotalCommissionLevel1(Utilities.roundFactory(productCommission, 2, BigDecimal.ROUND_HALF_UP));
					else if (productCommission != null && invoiceCommission != null)
						order.getAffiliate().setTotalCommissionLevel1(Utilities.roundFactory(productCommission + invoiceCommission, 2, BigDecimal.ROUND_HALF_UP));
				} else if (level == 2) {
					order.getAffiliate().setPercentLevel2(affiliate.getPercentLevel2());
					order.getAffiliate().setAffiliateLevel2Id(parentId);
					Double productCommission = getProductCommission(affiliate, order, level, numCommTable);
					Double invoiceCommission = getInvoiceCommission2(affiliate, order, level);
					if (productCommission == null && invoiceCommission == null)
						order.getAffiliate().setTotalCommissionLevel2(null);
					else if (productCommission == null && invoiceCommission != null)
						order.getAffiliate().setTotalCommissionLevel2(Utilities.roundFactory(invoiceCommission, 2, BigDecimal.ROUND_HALF_UP));
					else if (productCommission != null && invoiceCommission == null)
						order.getAffiliate().setTotalCommissionLevel2(Utilities.roundFactory(productCommission, 2, BigDecimal.ROUND_HALF_UP));
					else if (productCommission != null && invoiceCommission != null)
						order.getAffiliate().setTotalCommissionLevel2(Utilities.roundFactory(productCommission + invoiceCommission, 2, BigDecimal.ROUND_HALF_UP));
				}
				level++;
			}
			}
			
	}

	private Double getInvoiceCommission2(Affiliate affiliate, Order order, int level) {
		Double invoiceCommission = null;
		Class<Affiliate> c = Affiliate.class;
		Object arglist[] = null;
		try {
			Method m = c.getMethod("getInvoiceCommissionLevel" + level);
			Double invoiceCom = (Double) m.invoke(affiliate, arglist);
			Method m1 = c.getMethod("getPercentLevel" + level);
			Boolean percent = (Boolean) m1.invoke(affiliate, arglist);
			if (invoiceCom != null) {
				if (percent) {
					invoiceCommission = order.getSubTotalAfterDiscount() * invoiceCom / 100.00;
				} else {
					invoiceCommission = invoiceCom;
				}
			} else
				return null;

		} catch (Exception e) {
			return null;
		}
		return invoiceCommission;
	}

	private Double getProductCommission(Affiliate affiliate, Order order, int level, int numCommTable) {
		boolean allCommissionsAreNull = true;
		Double productCommission = 0.0;
		Class<Affiliate> c = Affiliate.class;
		Object arglist[] = null;

		try {
			Method m = c.getMethod("getProductCommissionLevel" + level);
			int comTable = (Integer) m.invoke(affiliate, arglist);
			for (LineItem l : order.getLineItems()) {
				switch (comTable) {
				case 1:
					if (l.getProduct().getCommissionTables().getCommissionTable1() != null && numCommTable >= 1) {
						productCommission += (l.getProduct().getCommissionTables().getCommissionTable1() * l.getQuantity());
						allCommissionsAreNull = false;
					}
					break;
				case 2:
					if (l.getProduct().getCommissionTables().getCommissionTable2() != null && numCommTable >= 2) {
						productCommission += (l.getProduct().getCommissionTables().getCommissionTable2() * l.getQuantity());
						allCommissionsAreNull = false;
					}
					break;
				case 3:
					if (l.getProduct().getCommissionTables().getCommissionTable3() != null && numCommTable >= 3) {
						productCommission += (l.getProduct().getCommissionTables().getCommissionTable3() * l.getQuantity());
						allCommissionsAreNull = false;
					}
					break;
				case 4:
					if (l.getProduct().getCommissionTables().getCommissionTable4() != null && numCommTable >= 4) {
						productCommission += (l.getProduct().getCommissionTables().getCommissionTable4() * l.getQuantity());
						allCommissionsAreNull = false;
					}
					break;
				case 5:
					if (l.getProduct().getCommissionTables().getCommissionTable5() != null && numCommTable >= 5) {
						productCommission += (l.getProduct().getCommissionTables().getCommissionTable5() * l.getQuantity());
						allCommissionsAreNull = false;
					}
					break;
				case 6:
					if (l.getProduct().getCommissionTables().getCommissionTable6() != null && numCommTable >= 6) {
						productCommission += (l.getProduct().getCommissionTables().getCommissionTable6() * l.getQuantity());
						allCommissionsAreNull = false;
					}
					break;
				case 7:
					if (l.getProduct().getCommissionTables().getCommissionTable7() != null && numCommTable >= 7) {
						productCommission += (l.getProduct().getCommissionTables().getCommissionTable7() * l.getQuantity());
						allCommissionsAreNull = false;
					}
					break;
				case 8:
					if (l.getProduct().getCommissionTables().getCommissionTable8() != null && numCommTable >= 8) {
						productCommission += (l.getProduct().getCommissionTables().getCommissionTable8() * l.getQuantity());
						allCommissionsAreNull = false;
					}
					break;
				case 9:
					if (l.getProduct().getCommissionTables().getCommissionTable9() != null && numCommTable >= 9) {
						productCommission += (l.getProduct().getCommissionTables().getCommissionTable9() * l.getQuantity());
						allCommissionsAreNull = false;
					}
					break;
				case 10:
					if (l.getProduct().getCommissionTables().getCommissionTable10() != null && numCommTable >= 10) {
						productCommission += (l.getProduct().getCommissionTables().getCommissionTable10() * l.getQuantity());
						allCommissionsAreNull = false;
					}
					break;
				}
			}
		} catch (Exception e) {
			return null;
		}
		return (allCommissionsAreNull) ? null : productCommission;
	}

	public List<ProductField> getProductFieldsHeader(Order order, int numProdFields) throws Exception {
		List<ProductField> productFieldsHeader = new ArrayList<ProductField>();
		// product fields
		List<ProductField> thisProductFields = this.getProductFields(null, numProdFields);

		for (LineItem lineItem : order.getLineItems()) {
			this.setProductFields(thisProductFields, lineItem, productFieldsHeader);
		}

		if (order.getInsertedLineItems() != null) {
			for (LineItem lineItem : order.getInsertedLineItems()) {
				this.setProductFields(thisProductFields, lineItem, productFieldsHeader);
			}
		}
		return productFieldsHeader;
	}

	private void setProductFields(List<ProductField> thisProductFields, LineItem lineItem, List<ProductField> productFieldsHeader) throws Exception {
		List<ProductField> productFields = new ArrayList<ProductField>();
		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;
		Product product = this.getProductById(this.getProductIdBySku(lineItem.getProduct().getSku()), 0, false, null);
		if (product != null) {
			for (ProductField productField : thisProductFields) {
				if (productField.isEnabled() && (productField.isShowOnInvoice() || productField.isShowOnInvoiceBackend() || productField.isPackingField())) {
					m = c.getMethod("getField" + productField.getId());
					ProductField newProductField = new ProductField();
					newProductField.setId(productField.getId());
					newProductField.setValue((String) m.invoke(product, arglist));
					if (newProductField.getValue() != null && newProductField.getValue().length() > 0) {
						if (!productFieldsHeader.toString().contains(productField.toString())) {
							productFieldsHeader.add(productField);
						}
						productFields.add(newProductField);
					}
				}
			}
		}
		lineItem.setProductFields(productFields);
	}

	public List<Product> getProductAjax(ProductSearch productSearch) {
		return this.productDao.getProductAjax(productSearch);
	}

	public Map<String, Long> getProductSkuMap() {
		return this.productDao.getProductSkuMap();
	}

	public List<Map<String, Object>> getCostListByProductId(Integer productId) {
		return this.productDao.getCostListByProductId(productId);
	}

	public String getProductNameBySku(String sku) {
		return this.productDao.getProductNameBySku(sku);
	}

	public String getProductFieldById(int productId, int fieldNumber) {
		return this.productDao.getProductFieldById(productId, fieldNumber);
	}

	public String generateHtmlLineItem(Order order, HttpServletRequest request, ApplicationContext context) {
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		int cols = 0;
		StringBuffer html = new StringBuffer();
		html.append("<br /><br /><table border=\"1\" cellpadding=\"0\" cellspacing=\"0\" style=\"border:solid 2px #111111;width:80%\"> ");
		html.append("<tr style=\"background-color:#cccccc;font:700px;\">");
		html.append("<th width=\"1%\" >" + context.getMessage("line", new Object[0], RequestContextUtils.getLocale(request)) + "#" + "</th>");
		html.append("<th width=\"5%\" >" + context.getMessage("productSku", new Object[0], RequestContextUtils.getLocale(request)) + "</th>");
		html.append("<th width=\"5%\" >" + context.getMessage("productName", new Object[0], RequestContextUtils.getLocale(request)) + "</th>");
		try {
			for (ProductField productField : getProductFieldsHeader(order, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"))) {
				html.append("<th width=\"5%\" >" + productField.getName() + "</th>");
				cols++;
			}
		} catch (Exception e) {
		}
		html.append("<th width=\"5%\" >" + context.getMessage("quantity", new Object[0], RequestContextUtils.getLocale(request)) + "</th>");
		if ((Boolean) gSiteConfig.get("gINVENTORY") && order.ishasLowInventoryMessage()) {
			html.append("<th width=\"5%\" >" + context.getMessage("lowInventory", new Object[0], RequestContextUtils.getLocale(request)) + "</th>");
			cols++;
		}
		if (order.ishasPacking()) {
			html.append("<th width=\"5%\" >" + context.getMessage("packing", new Object[0], RequestContextUtils.getLocale(request)) + "</th>");
			cols++;
		}
		if (order.ishasContent()) {
			html.append("<th width=\"5%\" >" + context.getMessage("content", new Object[0], RequestContextUtils.getLocale(request)) + "</th>");
			cols++;
			if (!siteConfig.get("EXT_QUANTITY_TITLE").getValue().equals("")) {
				html.append("<th width=\"5%\" >" + siteConfig.get("EXT_QUANTITY_TITLE").getValue() + "</th>");
				cols++;
			}
		}
		html.append("</tr>");
		int counter = 1;
		for (LineItem lineItem : order.getLineItems()) {
			html.append("<tr style=\"border: 1px solid black;\">");
			html.append("<td align=\"center\">" + counter++ + "</td>");
			html.append("<td>" + lineItem.getProduct().getSku() + "</td>");
			html.append("<td>" + lineItem.getProduct().getName());
			for (ProductAttribute pa : lineItem.getProductAttributes()) {
				html.append("<div>&nbsp;-&nbsp;" + pa.getOptionName() + ": " + pa.getValueName() + "</div>");
			}
			html.append("</td>");
			try {
				for (ProductField pFHeader : getProductFieldsHeader(order, (Integer) gSiteConfig.get("gPRODUCT_FIELDS"))) {
					boolean check = false;
					if (pFHeader.isPackingField()) {
						for (ProductField productField : lineItem.getProductFields()) {
							if (pFHeader.getId() == productField.getId()) {
								html.append("<td>" + productField.getValue() + "</td>");
								check = true;
								cols++;
							}
						}
						if (!check) {
							html.append("<td>&nbsp;</td>");
							cols++;
						}
					}
				}
			} catch (Exception e) {
			}

			html.append("<td align=\"center\">" + lineItem.getToBeShipQty());
			if (lineItem.getPriceCasePackQty() != null) {
				html.append("<div>(" + lineItem.getPriceCasePackQty() + " per " + siteConfig.get("PRICE_CASE_PACK_UNIT_TITLE").getValue() + ")</div>");
			}
			html.append("</td>");
			if ((Boolean) gSiteConfig.get("gINVENTORY") && order.ishasLowInventoryMessage()) {
				html.append("<td>" + lineItem.getLowInventoryMessage() + "</td>");
				cols++;
			}
			if (order.ishasPacking()) {
				html.append("<td>" + lineItem.getProduct().getPacking() + "</td>");
				cols++;
			}
			if (order.ishasContent()) {
				html.append("<td>" + ((lineItem.getProduct().getCaseContent() == null) ? "" : lineItem.getProduct().getCaseContent()) + "</td>");
				cols++;
				if (!siteConfig.get("EXT_QUANTITY_TITLE").getValue().equals("")) {
					if (lineItem.getProduct().getCaseContent() != null) {
						html.append("<td>" + lineItem.getProduct().getCaseContent() * lineItem.getToBeShipQty() + "</td>");
						cols++;
					} else {
						html.append("<td>" + lineItem.getToBeShipQty() + "</td>");
						cols++;
					}
				}
			}
			html.append("</tr>");
		}
		html.append("</table>");
		return html.toString();
	}

	public void updateTaxAndTotal(Map gSiteConfig, GlobalDao globalDao, Order order) {

		// set tax rate, tax and grand total for Order
		if (!(Boolean) gSiteConfig.get("gTAX_EXEMPTION")) {
			order.setTaxExemption(false);
		}
		// only US and Canada
		if (order.getShipping().getCountry() != null && order.getShipping().getStateProvince() != null
				&& (order.getShipping().getCountry().equalsIgnoreCase("US") || order.getShipping().getCountry().equalsIgnoreCase("CA")) && !order.getTaxExemption()) {
			String FiveDigitZipcode;
			if (order.getShipping().getZip() == null) {
				FiveDigitZipcode = "";
			} else if (order.getShipping().getCountry().equalsIgnoreCase("US") && order.getShipping().getZip().trim().length() > 5) {
				FiveDigitZipcode = order.getShipping().getZip().trim().substring(0, 5).trim();
			} else {
				FiveDigitZipcode = order.getShipping().getZip().trim();
			}
			order.setTaxRate(null); // reset tax

			// apply tax on shipping
			State state = getStateByCode(order.getShipping().getCountry(), order.getShipping().getStateProvince());
			if (state != null) {
				order.setTaxOnShipping(state.isApplyShippingTax());
			}
			// city tax
			if ((Boolean) gSiteConfig.get("gCITY_TAX") && (Boolean) gSiteConfig.get("gCOUNTY_TAX")) {

				City customerCity = globalDao.getCityInfoByZipcode(FiveDigitZipcode);
				if (customerCity != null) {
					City city = getTaxRateByCity(customerCity);
					if (city != null && city.getTaxRate() != null) {
						order.setTaxRate(city.getTaxRate());
					}
				}
			}
			// county tax
			if (order.getTaxRate() == null && (Boolean) gSiteConfig.get("gCOUNTY_TAX")) {
				City cityInfo = globalDao.getCityInfoByZipcode(FiveDigitZipcode);
				if (cityInfo != null) {
					County countyTemp = new County(order.getShipping().getCountry(), order.getShipping().getStateProvince(), cityInfo.getCounty());
					County county = getTaxRateByCounty(countyTemp);
					if (county != null && county.getTaxRate() != null) {
						order.setTaxRate(county.getTaxRate());
					}
				} else {
					order.setTaxRate(null);
				}
			}
			// state tax
			if (order.getTaxRate() == null) {
				if (state != null) {
					order.setTaxRate(state.getTaxRate());
				} else {
					order.setTaxRate((double) 0);
				}
			}
		}
		// other countries
		else if (order.getShipping().getCountry() != null && (!order.getShipping().getCountry().equalsIgnoreCase("US") || !order.getShipping().getCountry().equalsIgnoreCase("CA"))
				&& !order.getTaxExemption()) {
			Country country = getCountryByCode(order.getShipping().getCountry());
			if (country != null) {
				order.setTaxRate(country.getTaxRate());
			} else {
				order.setTaxRate((double) 0);
			}
		} else {
			order.setTaxRate((double) 0);
		}
		// upadte grandTotal
		updateGrandTotal(gSiteConfig, order);
	}

	public void updateGrandTotal(Map gSiteConfig, Order order) {
		
		System.out.println("updateGrandTotal");
		// set Promo Amount
		order.setPromoAmount();
		// set Tax
		if ((Boolean) gSiteConfig.get("gTAX_AFTER_DISCOUNT"))
			order.setTaxAfterDiscount();
		else
			order.setTaxBeforeDiscount();

		// set cc fee
		order.setCcFeeAmount();
		// set Grand Total
		order.setGrandTotal();
	}

	public void updateOrderPrinted(Order order) {
		this.orderDao.updateOrderPrinted(order);
	}

	public void updateOrderApproval(Order order) {
		this.orderDao.updateOrderApproval(order);
	}

	public void updateOrdersPaymentStatus(int orderId, String status, Date date) {
		this.orderDao.updateOrdersPaymentStatus(orderId, status, date);
	}

	public List<CustomerReport> getCustomerQuickViewList(Integer userId) {
		return this.orderDao.getCustomerQuickViewList(userId);
	}

	// mas90, dsi, triplefin
	public void updateOrderExported(String column_name, Date endDate, Set<Integer> orderIds) {
		this.orderDao.updateOrderExported(column_name, endDate, orderIds);
	}

	public void updateDsiOrder(Order order, OrderStatus orderStatus) {
		insertOrderStatus(orderStatus);
		for (LineItem lineItem : order.getLineItems()) {
			this.orderDao.updateLineItemShipping(lineItem);
		}
	}

	// massEmail
	public void insertEmailCampaign(EmailCampaign emailCampaign) {
		this.massEmailDao.insertEmailCampaign(emailCampaign);
	}

	public void updateEmailCampaign(EmailCampaign emailCampaign) {
		this.massEmailDao.updateEmailCampaign(emailCampaign);
	}

	public void finalizeEmailCampaign(EmailCampaign emailCampaign) {
		this.massEmailDao.finalizeEmailCampaign(emailCampaign);
	}

	public EmailCampaign getEmailCampaignById(Integer campaignId) {
		return this.massEmailDao.getEmailCampaignById(campaignId);
	}

	public void addEmailCampaignBounce(Integer campaignId) {
		this.massEmailDao.addEmailCampaignBounce(campaignId);
	}

	public void deductEmailCampaignPoint(Integer point) {
		this.massEmailDao.deductEmailCampaignPoint(point);
	}

	public void addEmailCampaignPoint(Integer point) {
		this.massEmailDao.addEmailCampaignPoint(point);
	}

	public int emailInProcessCount() {
		return this.massEmailDao.emailInProcessCount();
	}

	public void insertEmailCampaignBalance(EmailCampaignBalance balance) {
		this.massEmailDao.insertEmailCampaignBalance(balance);
	}

	public void updateEmailCampaignBalance(EmailCampaignBalance balance) {
		this.massEmailDao.updateEmailCampaignBalance(balance);
	}

	public void deleteEmailCampaignById(Integer id) {
		this.massEmailDao.deleteEmailCampaignById(id);
	}

	public int getEmailBalance() {
		return this.massEmailDao.getEmailBalance();
	}

	public List<EmailCampaignBalance> getBuyCreditList(ReportFilter buyCreditFilter) {
		return this.massEmailDao.getBuyCreditList(buyCreditFilter);
	}

	public void updateClickCount(Click click) {
		this.massEmailDao.updateClickCount(click);
	}

	public Integer insertOrGetClick(Click click) {
		return this.massEmailDao.insertOrGetClick(click);
	}

	public List<Click> getClick(Click click) {
		return this.massEmailDao.getClick(click);
	}
	
	public List<Unsubscribe> getUnsubscribe(Unsubscribe unsubscribe) {
		return this.massEmailDao.getUnsubscribe(unsubscribe);
	}

	public List<ClickDetail> getCountDetail(ClickDetail clickDetail) {
		return this.massEmailDao.getCountDetail(clickDetail);
	}

	public List<ClickDetail> getOpenDetail(ClickDetailSearch clickDetail) {
		return this.massEmailDao.getOpenDetail(clickDetail);
	}

	public void updateOpenCount(Click click) {
		this.massEmailDao.updateOpenCount(click);
	}
	
	public void insertUnsubscribeDetails(String campaignId,Integer userId,String email){
		this.massEmailDao.insertUnsubscribeDetails(campaignId,userId,email);
	}

	// location
	public List<Location> getLocationList(LocationSearch locationSearch) {
		return this.locationDao.getLocationList(locationSearch);
	}

	public List<Location> searchLocationList(LocationSearch search) {
		return this.locationDao.searchLocationList(search);
	}

	public Map<String, Location> getLocationMap(LocationSearch locationSearch) {
		return this.locationDao.getLocationMap(locationSearch);
	}

	public Location getLocationById(Integer userId) {
		return this.locationDao.getLocationById(userId);
	}

	public void updateLocation(Location location) {
		this.locationDao.updateLocation(location);
	}

	public void insertLocation(Location location) throws DataAccessException {
		this.locationDao.insertLocation(location);
	}

	public void deleteLocation(Integer locationId) throws DataAccessException {
		this.locationDao.deleteLocation(locationId);
	}

	public void addToLocationWishList(Integer userId, Integer locationId) {
		this.locationDao.addToLocationWishList(userId, locationId);
	}

	public void removeFromLocationWishList(Integer userId, Set locationIds) {
		this.locationDao.removeFromLocationWishList(userId, locationIds);
	}

	public List<Location> getLocationWishListByUserid(LocationSearch locationSearch) {
		return this.locationDao.getLocationWishListByUserid(locationSearch);
	}

	public void updateKeywordLocationByUserId(Location location) throws DataAccessException {
		this.locationDao.updateKeywordLocationByUserId(location);
	}

	public List<Location> getLocationListByUserId(Integer userId, Integer limit) {
		return this.locationDao.getLocationListByUserId(userId, limit);
	}

	public boolean isValidLocationId(int id) {
		return this.locationDao.isValidLocationId(id);
	}

	// master sku
	public List<Integer> getSlaves(ProductSearch search) {
		return this.productDao.getSlaves(search);
	}

	public Double getProductWeightBySku(String sku) {
		return this.productDao.getProductWeightBySku(sku);
	}

	public void setMasterSkuDetails(Product product, String protectedAccess, boolean checkInventory) {
		Map<String, Object> map = this.productDao.getMasterSkuDetailsBySku(product.getSku(), protectedAccess, checkInventory);
		product.setSlaveCount(((Long) map.get("slave_count")).intValue());
		product.setSlaveLowestPrice((Double) map.get("slave_lowest_price"));
		product.setSlaveHighestPrice((Double) map.get("slave_highest_price"));
	}

	public void setMasterSku(Product product) {
		Map<String, Object> map = this.productDao.getMasterSkuBySku(product.getSku());
		// if masterSKU does not exist.
		try {
			product.setShortDesc(((String) map.get("short_desc")).toString());
		} catch (NullPointerException e) {
			product.setShortDesc(null);
		}
		try {
			product.setWeight((Double) map.get("weight"));
		} catch (NullPointerException e) {
			product.setWeight(null);
		}
	}

	public String getProductFieldValue(String sku, String field) {
		return this.productDao.getProductFieldValue(sku, field);
	}

	// Manufacturer
	public List<Manufacturer> getManufacturerList(ManufacturerSearch search) {
		return this.manufacturerDao.getManufacturerList(search);
	}

	public void insertManufacturer(Manufacturer manufacturer) {
		this.manufacturerDao.insertManufacturer(manufacturer);
	}

	public void deleteManufacturer(Integer manufacturerId) {
		this.manufacturerDao.deleteManufacturer(manufacturerId);
	}

	public void updateManufacturer(Manufacturer manufacturer) {
		this.manufacturerDao.updateManufacturer(manufacturer);
	}

	public void updateProductManufacturerName(String old, String newName) {
		this.manufacturerDao.updateProductManufacturerName(old, newName);
	}

	public void updateShoppingcartManufacturerName(String old, String newName) {
		this.manufacturerDao.updateShoppingcartManufacturerName(old, newName);
	}

	public Map<String, Manufacturer> getManufacturerMap() {
		return this.manufacturerDao.getManufacturerMap();
	}

	public Manufacturer getManufacturerById(Integer manufacturerId) {
		return this.manufacturerDao.getManufacturerById(manufacturerId);
	}

	public Manufacturer getManufacturerByName(String name) {
		return this.manufacturerDao.getManufacturerByName(name);
	}

	public boolean isProductExist(String manufacturerName) {
		return this.manufacturerDao.isProductExist(manufacturerName);
	}

	// Truck Load
	public void insertTruckLoadProcess(TruckLoadProcess truckLoadProcess) {
		this.truckLoadProcessDao.insertTruckLoadProcess(truckLoadProcess);
	}

	public void updateTruckLoadProcess(TruckLoadProcess truckLoadProcess) {
		this.truckLoadProcessDao.updateTruckLoadProcess(truckLoadProcess);
	}
	
	public void updateTruckLoadPT(TruckLoadProcess truckLoadProcess){
		this.truckLoadProcessDao.updateTruckLoadPT(truckLoadProcess);
	}

	public List<TruckLoadProcess> getTruckLoadProcessList(TruckLoadProcessSearch truckLoadProcessSearch) {
		return this.truckLoadProcessDao.getTruckLoadProcessList(truckLoadProcessSearch);
	}

	public List<TruckLoadProcess> getTruckLoadProcessListReport(TruckLoadProcessSearch truckLoadProcessSearch) {
		return this.truckLoadProcessDao.getTruckLoadProcessListReport(truckLoadProcessSearch);
	}
		
	public List<String> getTruckLoadProcessPTList() {
		return this.truckLoadProcessDao.getTruckLoadProcessPTList();
	}

	public TruckLoadProcess getTruckLoadProcessById(Integer id) {
		return this.truckLoadProcessDao.getTruckLoadProcessById(id);
	}
	
	public TruckLoadProcessProducts getTruckLoadDerivedProductById(Integer Id) {
		return this.truckLoadProcessDao.getTruckLoadDerivedProductById(Id);
	}

	// Buy Request
	public List<BuyRequest> getBuyRequestListByUserid(BuyRequestSearch buyRequestSearch) {
		return this.buyRequestDao.getBuyRequestListByUserid(buyRequestSearch);
	}

	public List<BuyRequest> searchBuyRequests(BuyRequestSearch search) {
		return this.buyRequestDao.searchBuyRequests(search);
	}

	public int searchBuyRequestsCount(BuyRequestSearch search) {
		return this.buyRequestDao.searchBuyRequestsCount(search);
	}

	public BuyRequest getBuyRequestById(Integer buyRequestId, Integer userId) {
		return this.buyRequestDao.getBuyRequestById(buyRequestId, userId);
	}

	public void deleteBuyRequestById(Integer buyRequestId) {
		this.buyRequestDao.deleteBuyRequestById(buyRequestId);
	}

	public void updateBuyRequest(BuyRequest buyRequest) {
		this.buyRequestDao.updateBuyRequest(buyRequest);
	}

	public int insertBuyRequest(BuyRequest buyRequest) {
		return this.buyRequestDao.insertBuyRequest(buyRequest);
	}

	public List<BuyRequestVersion> getBuyRequestVesrionList(BuyRequestVersion buyRequestVersion) {
		return this.buyRequestDao.getBuyRequestVesrionList(buyRequestVersion);
	}

	public void insertBuyRequestVesrion(BuyRequestVersion buyRequestVersion) {
		this.buyRequestDao.insertBuyRequestVesrion(buyRequestVersion);
	}

	public List<BuyRequest> getBuyRequestWishListByUserid(Integer userId, HttpServletRequest request) {
		Customer customer = getCustomerByRequest(request);
		BuyRequestSearch search = new BuyRequestSearch();
		Map gSiteConfig = (Map) request.getAttribute("gSiteConfig");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		search.setUserId(userId);
		if (customer != null) {
			search.setProtectedAccess(customer.getProtectedAccess());
		} else {
			search.setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue());
		}

		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		}
		return this.buyRequestDao.getBuyRequestWishListByUserid(search);
	}

	public void addToBuyRequestWishList(Integer userId, String ids[]) {
		Set<Integer> buyRequestIds = new HashSet<Integer>();
		for (String id : ids) {
			buyRequestIds.add(Integer.parseInt(id));
		}
		this.buyRequestDao.addToBuyRequestWishList(userId, buyRequestIds);
	}

	public void removeFromBuyRequestWishList(Integer userId, String ids[]) {
		Set<Integer> buyRequestIds = new HashSet<Integer>();
		for (String id : ids) {
			buyRequestIds.add(Integer.parseInt(id));
		}
		this.buyRequestDao.removeFromBuyRequestWishList(userId, buyRequestIds);
	}

	// product review
	public List<ProductReview> getProductReviewList(ReviewSearch search) {
		return this.reviewDao.getProductReviewList(search);
	}

	public int getProductReviewListCount(ReviewSearch search) {
		return this.reviewDao.getProductReviewListCount(search);
	}

	public void insertProductReview(ProductReview productReview) {
		this.reviewDao.insertProductReview(productReview);
	}

	public void deleteProductReviewById(Integer productReviewId) {
		this.reviewDao.deleteProductReviewById(productReviewId);
	}

	public List<ProductReview> getProductReviewListByProductSku(String productSku, Boolean active, Integer limit) {
		return this.reviewDao.getProductReviewListByProductSku(productSku, active, limit);
	}

	public ProductReview getProductReviewById(Integer productReviewId) {
		return this.reviewDao.getProductReviewById(productReviewId);
	}

	public void updateProductReview(ProductReview productReview) {
		this.reviewDao.updateProductReview(productReview);
	}

	public ProductReview getProductReviewAverage(String productSku) {
		return this.reviewDao.getProductReviewAverage(productSku);
	}

	public boolean checkPreviousReview(ProductReview productReview) {
		return this.reviewDao.checkPreviousReview(productReview);
	}

	// compnay review
	public List<CompanyReview> getcompanyReviewList(ReviewSearch search) {
		return this.reviewDao.getcompanyReviewList(search);
	}

	public int getCompanyReviewListCount(ReviewSearch search) {
		return this.reviewDao.getCompanyReviewListCount(search);
	}

	public void insertCompanyReview(CompanyReview companyReview) {
		this.reviewDao.insertCompanyReview(companyReview);
	}

	public void deleteCompanyReviewById(Integer companyReviewId) {
		this.reviewDao.deleteCompanyReviewById(companyReviewId);
	}

	public List<ProductReview> getCompanyReviewListByCompanyId(Integer companyId, Boolean active) {
		return this.reviewDao.getCompanyReviewListByCompanyId(companyId, active);
	}

	public CompanyReview getCompanyReviewById(Integer companyReviewId) {
		return this.reviewDao.getCompanyReviewById(companyReviewId);
	}

	public void updateCompanyReview(CompanyReview companyReview) {
		this.reviewDao.updateCompanyReview(companyReview);
	}

	public Review getCompanyReviewAverage(Integer companyId) {
		return this.reviewDao.getCompanyReviewAverage(companyId);
	}

	// custom frame
	public LineItem getCustomFrame(int orderId, int lineNum) {
		return this.orderDao.getCustomFrame(orderId, lineNum);
	}

	// techdata
	public void nonTransactionSafeInsertTechdataManufacturers(List<TechdataManufacturer> manufacturers) {
		this.vendorDao.insertTechdataManufacturers(manufacturers);
	}

	public void nonTransactionSafeInsertTechdataCategories(List<TechdataCategory> categories) {
		this.vendorDao.insertTechdataCategories(categories);
	}

	public List<TechdataCategory> getTechdataCategories() {
		return this.vendorDao.getTechdataCategories();
	}

	public void nonTransactionSafeUpdateTechdataCategories(List<TechdataCategory> categories) {
		this.vendorDao.updateTechdataCategories(categories);
	}

	public List<TechdataManufacturer> getTechdataManufacturers() {
		return this.vendorDao.getTechdataManufacturers();
	}

	public List<String> getTechdataRestrictedProductLines() {
		return this.vendorDao.getTechdataRestrictedProductLines();
	}

	public Map<String, TechdataCategory> getTechdataCategoryMap() {
		return this.vendorDao.getTechdataCategoryMap();
	}

	public Map<String, String> getTechdataManufacturerMap() {
		return this.vendorDao.getTechdataManufacturerMap();
	}

	public void nonTransactionSafeInsertCategories(List<Map<String, Object>> data) {
		this.productDao.insertCategories(data);
	}

	// ingram micro
	public void nonTransactionSafeInsertIngramMicroCategories(List<IngramMicroCategory> categories) {
		this.vendorDao.insertIngramMicroCategories(categories);
	}

	public List<IngramMicroCategory> getIngramMicroCategories() {
		return this.vendorDao.getIngramMicroCategories();
	}

	public void nonTransactionSafeUpdateIngramMicroCategories(List<IngramMicroCategory> categories) {
		this.vendorDao.updateIngramMicroCategories(categories);
	}

	public Map<String, IngramMicroCategory> getIngramMicroCategoryMap() {
		return this.vendorDao.getIngramMicroCategoryMap();
	}

	public void deleteOldProducts(String feed, boolean makeInActive) {
		this.productDao.deleteOldProducts(feed, makeInActive);
	}

	public void inventoryZeroInactive(String feed) {
		this.productDao.inventoryZeroInactive(feed);
	}

	public int updateIngramMicroInventoryBySkus(final List<Map<String, Object>> data) {
		return this.vendorDao.updateIngramMicroInventoryBySkus(data);
	}

	// Synnex
	public void nonTransactionSafeInsertSynnexCategories(List<SynnexCategory> categories) {
		this.vendorDao.insertSynnexCategories(categories);
	}

	public List<SynnexCategory> getSynnexCategories() {
		return this.vendorDao.getSynnexCategories();
	}

	public void nonTransactionSafeUpdateSynnexCategories(List<SynnexCategory> categories) {
		this.vendorDao.updateSynnexCategories(categories);
	}

	public Map<String, SynnexCategory> getSynnexCategoryMap() {
		return this.vendorDao.getSynnexCategoryMap();
	}

	// DSI
	public void nonTransactionSafeInsertDsiCategories(List<DsiCategory> categories) {
		this.vendorDao.insertDsiCategories(categories);
	}

	public List<DsiCategory> getDsiCategories() {
		return this.vendorDao.getDsiCategories();
	}

	public void nonTransactionSafeUpdateDsiCategories(List<DsiCategory> categories) {
		this.vendorDao.updateDsiCategories(categories);
	}

	public Map<String, DsiCategory> getDsiCategoryMap() {
		return this.vendorDao.getDsiCategoryMap();
	}

	//
	public List<String> getAsiSupplier() {
		return this.vendorDao.getAsiSupplier();
	}

	public List<Supplier> getSupplierAjax(String company) {
		return this.supplierDao.getSupplierAjax(company);
	}

	/*
	 * TO BE REMOVE public List<AsiCategory> getAsiCategoriesBySupplier(String supplier) { return this.vendorDao.getAsiCategoriesBySupplier(supplier); } public void nonTransactionSafeUpdateAsiCategories(List<AsiCategory> categories) {
	 * this.vendorDao.updateAsiCategories(categories); } public Map<String, AsiCategory> getAsiCategoryMap() { return this.vendorDao.getAsiCategoryMap(); } public void nonTransactionSafeInsertAsiCategories(List<AsiCategory> categories) {
	 * this.vendorDao.insertAsiCategories(categories); } public List<AsiCategory> getAsiCategories(Search search) { return this.vendorDao.getAsiCategories(search); }
	 */
	public boolean isASIUniqueIdExist(int uniqueId) {
		return this.productDao.isASIUniqueIdExist(uniqueId);
	}

	// Kole Imports
	public void nonTransactionSafeInsertKoleImportsCategories(List<KoleImportsCategory> categories) {
		this.vendorDao.insertKoleImportsCategories(categories);
	}

	public List<KoleImportsCategory> getKoleImportsCategories() {
		return this.vendorDao.getKoleImportsCategories();
	}

	public void nonTransactionSafeUpdateKoleImportsCategories(List<KoleImportsCategory> categories) {
		this.vendorDao.updateKoleImportsCategories(categories);
	}

	public Map<String, KoleImportsCategory> getKoleImportsCategoryMap() {
		return this.vendorDao.getKoleImportsCategoryMap();
	}

	// Dsdi Imports
	public void nonTransactionSafeInsertDsdiCategories(List<DsdiCategory> categories) {
		this.vendorDao.insertDsdiCategories(categories);
	}

	public List<DsdiCategory> getDsdiCategories() {
		return this.vendorDao.getDsdiCategories();
	}

	public void nonTransactionSafeUpdateDsdiCategories(List<DsdiCategory> categories) {
		this.vendorDao.updateDsdiCategories(categories);
	}

	public Map<String, DsdiCategory> getDsdiCategoryMap() {
		return this.vendorDao.getDsdiCategoryMap();
	}

	// Etilize
	public int getEtilizeCount() {
		return this.productDao.getEtilizeCount();
	}

	public List<Product> getEtilizeList(int limit, int offset) {
		return this.productDao.getEtilizeList(limit, offset);
	}

	public List<Map<String, Object>> getProductsBySkuList(List<Product> skuList) {
		return this.productDao.getProductsBySkuList(skuList);
	}

	public List<Map<String, Object>> getEtilizeLuceneProduct(int limit, int offset) {
		return this.productDao.getEtilizeLuceneProduct(limit, offset);
	}

	public int getEtilizeLuceneProductCount() {
		return this.productDao.getEtilizeLuceneProductCount();
	}

	// Labels
	public Map<String, String> getLabels(String... prefix) {
		return this.configDao.getLabels(prefix);
	}
	
	public Map<Integer, Integer> getSiteMessageListByGroupId(Integer groupId){
		return this.configDao.getSiteMessageListByGroupId(groupId);
	}

	public void updateLabels(List<Map<String, String>> labels) {
		this.configDao.updateLabels(labels);
	}

	// replace dynamic element
	public void replaceDynamicElement(EmailMessageForm form, Customer customer, SalesRep salesRep) throws Exception {
		Format formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		Format formatter2 = new SimpleDateFormat("MM/dd/yyyy");

		form.setSubject(form.getSubject().replaceAll("(?i)#firstname#", (customer.getAddress().getFirstName() == null) ? "" : customer.getAddress().getFirstName()));
		form.setSubject(form.getSubject().replaceAll("(?i)#lastname#", (customer.getAddress().getLastName() == null) ? "" : customer.getAddress().getLastName()));
		form.setSubject(form.getSubject().replaceAll("(?i)#email#", customer.getUsername()));
		form.setSubject(form.getSubject().replaceAll("(?i)#email#", customer.getUsername()));
		form.setSubject(form.getSubject().replaceAll("(?i)#date#", formatter2.format(new Date()).toString()));

		if (salesRep != null) {
			form.setSubject(form.getSubject().replaceAll("(?i)#salesRepName#", salesRep.getName()));
			form.setSubject(form.getSubject().replaceAll("(?i)#salesRepEmail#", salesRep.getEmail()));
			form.setSubject(form.getSubject().replaceAll("(?i)#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			form.setSubject(form.getSubject().replaceAll("(?i)#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
			
        Random rand = new Random();
        int  n1 = rand.nextInt(500) + 1;
        int  n2 = rand.nextInt(500) + 1;
        	
        String originalString = customer.getId() + "-" + n1 + "-" + n2;   
    	byte[] encoded = Base64.encodeBase64(originalString.getBytes());         
    	String encodedString = new String(encoded);  
		
		form.setMessage(form.getMessage().replaceAll("#DOC_BSUSRC#", "https://www.viatrading.com/dv/sign/form1.php?id=" + encodedString));
		form.setMessage(form.getMessage().replaceAll("#DOC_BSUSRC_SPA#", "https://www.viatrading.com/dv/sign/form1es.php?id=" + encodedString));
		form.setMessage(form.getMessage().replaceAll("#DOC_CRC#", "https://www.viatrading.com/dv/sign/form2.php?id=" + encodedString));
		form.setMessage(form.getMessage().replaceAll("#DOC_CRC_SPA#", "https://www.viatrading.com/dv/sign/form2es.php?id=" + encodedString));
		form.setMessage(form.getMessage().replaceAll("#DOC_MPA#", "https://www.viatrading.com/dv/sign/form3.php?id=" + encodedString));
		form.setMessage(form.getMessage().replaceAll("#DOC_MAP_SPA#", "https://www.viatrading.com/dv/sign/form3es.php?id=" + encodedString));

		form.setMessage(form.getMessage().replaceAll("(?i)#firstname#", (customer.getAddress().getFirstName() == null) ? "" : customer.getAddress().getFirstName()));
		form.setMessage(form.getMessage().replaceAll("(?i)#lastname#", (customer.getAddress().getLastName() == null) ? "" : customer.getAddress().getLastName()));
		form.setMessage(form.getMessage().replaceAll("(?i)#email#", customer.getUsername()));
		form.setMessage(form.getMessage().replaceAll("(?i)#address1#", (customer.getAddress().getAddr1() == null) ? "" : customer.getAddress().getAddr1()));
		form.setMessage(form.getMessage().replaceAll("(?i)#address2#", (customer.getAddress().getAddr2() == null) ? "" : customer.getAddress().getAddr2()));
		form.setMessage(form.getMessage().replaceAll("(?i)#country#", (customer.getAddress().getCountry() == null) ? "" : customer.getAddress().getCountry()));
		form.setMessage(form.getMessage().replaceAll("(?i)#city#", (customer.getAddress().getCity() == null) ? "" : customer.getAddress().getCity()));
		form.setMessage(form.getMessage().replaceAll("(?i)#state#", (customer.getAddress().getStateProvince() == null) ? "" : customer.getAddress().getStateProvince()));
		form.setMessage(form.getMessage().replaceAll("(?i)#phone#", (customer.getAddress().getPhone() == null) ? "" : customer.getAddress().getPhone()));
		form.setMessage(form.getMessage().replaceAll("(?i)#cellphone#", (customer.getAddress().getCellPhone() == null) ? "" : customer.getAddress().getCellPhone()));
		form.setMessage(form.getMessage().replaceAll("(?i)#company#", (customer.getAddress().getCompany() == null) ? "" : customer.getAddress().getCompany()));
		form.setMessage(form.getMessage().replaceAll("(?i)#deliverytype#", (customer.getAddress().isResidential() == true) ? "Yes" : "No"));
		form.setMessage(form.getMessage().replaceAll("(?i)#liftgate#", (customer.getAddress().isLiftGate() == true) ? "Yes" : "No"));
		form.setMessage(form.getMessage().replaceAll("(?i)#date#", formatter2.format(new Date()).toString()));
		form.setMessage(form.getMessage().replaceAll("(?i)#note#", (customer.getNote() == null) ? "" : customer.getNote()));

		form.setMessage(form.getMessage().replaceAll("(?i)#dialingNotes#", (customer.getLastDialingNote() == null) ? "" : customer.getLastDialingNote()));

		
		if (salesRep != null) {
			form.setMessage(form.getMessage().replaceAll("(?i)#salesRepName#", salesRep.getName()));
			form.setMessage(form.getMessage().replaceAll("(?i)#salesRepEmail#", salesRep.getEmail()));
			form.setMessage(form.getMessage().replaceAll("(?i)#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			form.setMessage(form.getMessage().replaceAll("(?i)#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		// customer fields
		Class<Customer> c1 = Customer.class;
		Method m1 = null;
		Object arglist1[] = null;
		for (CustomerField customerField : getCustomerFields()) {
			if (customerField.isEnabled()) {
				m1 = c1.getMethod("getField" + customerField.getId());
				customerField.setValue((String) m1.invoke(customer, arglist1));
				form.setMessage(form.getMessage().replaceAll("(?i)#customerfield" + customerField.getId() + "#", (customerField.getValue() == null) ? "" : customerField.getValue()));

		}
		
		}
	}
	
	public String paymentUrl(Integer orderId){
		String result = null;
		try{
		
			URL url2 = new URL("https://www.viatrading.com/dv/liveapps/generatepayment.php?OrderID=" + orderId);
			HttpURLConnection con = (HttpURLConnection) url2.openConnection();
			con.setRequestMethod("GET");
			con.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			con.addRequestProperty("User-Agent", "Mozilla");
			con.addRequestProperty("Referer", "google.com");
	    	con.setRequestProperty("Accept", "text/html, text/*");
	    	con.addRequestProperty("Content-Type","application/json; charset=UTF-8"); 
		
	    	BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
	    	StringBuilder sb = new StringBuilder();
	    	String inputLine;
		 
			while((inputLine = in.readLine()) != null)
			{
				if(inputLine!=null && !inputLine.isEmpty()){
					sb.append(inputLine);
				}
			}
			result = sb.toString();
		}
		catch(Exception ex){
		}
		
		if(result!=null){
			return result;
		}else{
			return "";
		}
	}

	public void replaceDynamicElement(OrderStatus orderStatus, Customer customer, SalesRep salesRep, Order order, String secureUrl, Contact customContact) throws Exception {
		Format formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		Format formatter2 = new SimpleDateFormat("MM/dd/yyyy");

		orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#firstname#", (order.getBilling().getFirstName() == null) ? "" : order.getBilling().getFirstName()));
		orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#lastname#", (order.getBilling().getLastName() == null) ? "" : order.getBilling().getLastName()));
		orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#email#", (customer.getUsername() == null) ? "" : customer.getUsername()));
		orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#order#", order.getOrderId().toString()));
		orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#status#", getMessageSourceAccessor().getMessage("orderStatus_" + orderStatus.getStatus())));
		orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#tracknum#", (orderStatus.getTrackNum() == null) ? "" : orderStatus.getTrackNum()));
		orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#date#", formatter2.format(new Date()).toString()));
		
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#paymentUrl#", paymentUrl(order.getOrderId())));
		
        Random rand = new Random();
        int  n1 = rand.nextInt(500) + 1;
        int  n2 = rand.nextInt(500) + 1;
        	
        String originalString = customer.getId() + "-" + n1 + "-" + n2;   
    	byte[] encoded = Base64.encodeBase64(originalString.getBytes());         
    	String encodedString = new String(encoded);
		
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#DOC_BSUSRC#", "https://www.viatrading.com/dv/sign/form1.php?id=" + encodedString));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#DOC_BSUSRC_SPA#", "https://www.viatrading.com/dv/sign/form1es.php?id=" + encodedString));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#DOC_CRC#", "https://www.viatrading.com/dv/sign/form2.php?id=" + encodedString));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#DOC_CRC_SPA#", "https://www.viatrading.com/dv/sign/form2es.php?id=" + encodedString));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#DOC_MPA#", "https://www.viatrading.com/dv/sign/form3.php?id=" + encodedString));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#DOC_MAP_SPA#", "https://www.viatrading.com/dv/sign/form3es.php?id=" + encodedString));

		if (salesRep != null) {
			orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#salesRepName#", salesRep.getName()));
			orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#salesRepEmail#", salesRep.getEmail()));
			orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			orderStatus.setSubject(orderStatus.getSubject().replaceAll("(?i)#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}

		// message
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#firstname#", (order.getBilling().getFirstName() == null) ? "" : order.getBilling().getFirstName()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#lastname#", (order.getBilling().getLastName() == null) ? "" : order.getBilling().getLastName()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#email#", (customer.getUsername() == null) ? "" : customer.getUsername()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#address1#", (customer.getAddress().getAddr1() == null) ? "" : customer.getAddress().getAddr1()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#address2#", (customer.getAddress().getAddr2() == null) ? "" : customer.getAddress().getAddr2()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#country#", (customer.getAddress().getCountry() == null) ? "" : customer.getAddress().getCountry()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#city#", (customer.getAddress().getCity() == null) ? "" : customer.getAddress().getCity()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#state#", (customer.getAddress().getStateProvince() == null) ? "" : customer.getAddress().getStateProvince()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#phone#", (customer.getAddress().getPhone() == null) ? "" : customer.getAddress().getPhone()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#cellphone#", (customer.getAddress().getCellPhone() == null) ? "" : customer.getAddress().getCellPhone()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#company#", (customer.getAddress().getCompany() == null) ? "" : customer.getAddress().getCompany()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#deliverytype#", (customer.getAddress().isResidential() == true) ? "Yes" : "No"));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#liftgate#", (customer.getAddress().isLiftGate() == true) ? "Yes" : "No"));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#order#", order.getOrderId().toString()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#status#", getMessageSourceAccessor().getMessage("orderStatus_" + orderStatus.getStatus())));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#tracknum#", (orderStatus.getTrackNum() == null) ? "" : orderStatus.getTrackNum()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#note#", (customer.getNote() == null) ? "" : customer.getNote()));
		
		if (order.getStatus() == "xq") {
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#orderlink#", secureUrl + "invoice.jhtm?quote=" + order.getOrderId()));
		} else {
			// XXX would be replaced by OrderID-UserID Base64-encoded.
			String orig = order.getOrderId() + "-" + order.getUserId();
		    byte[] encode_orig = Base64.encodeBase64(orig.getBytes());         
		    orig = new String(encode_orig);
			if(order.getShipping().getStateProvince().equals("CA")) {
				//If user is in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&walkin=true
				//If user is not in CA: https://www.viatrading.com/dv/inv/invoice.php?order_id=XXX=&other=true
				//orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#orderlink#", secureUrl + "dv/inv/invoice.php?order_id=" + orig + "=&walkin=true"));
				String viewInvoiceHref = secureUrl + "dv/inv/invoice.php?order_id=" + orig + "=&walkin=true";
				StringBuilder sb_aTag = new StringBuilder();
				sb_aTag.append("<a href=").append("\"").append(viewInvoiceHref).append("\"").append(">").append("View Invoice").append("</a>");
				orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#orderlink#", sb_aTag.toString()));
				
				StringBuilder sb_aTag_orderlinkes = new StringBuilder();
				sb_aTag_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHref).append("\"").append(">").append("Ver Factura").append("</a>");
				orderStatus.setMessage(orderStatus.getMessage().replace("#orderlinkes#", sb_aTag_orderlinkes.toString()));
				
			} else {
				//orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#orderlink#", secureUrl + "dv/inv/invoice.php?order_id=" + orig +"=&other=true"));
				String viewInvoiceHref = secureUrl + "dv/inv/invoice.php?order_id=" + orig + "=&other=true";
				StringBuilder sb_aTag = new StringBuilder();
				sb_aTag.append("<a href=").append("\"").append(viewInvoiceHref).append("\"").append(">").append("View Invoice").append("</a>");
				orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#orderlink#", sb_aTag.toString()));
				
				StringBuilder sb_aTag_orderlinkes = new StringBuilder();
				sb_aTag_orderlinkes.append("<a href=").append("\"").append(viewInvoiceHref).append("\"").append(">").append("Ver Factura").append("</a>");
				orderStatus.setMessage(orderStatus.getMessage().replace("#orderlinkes#", sb_aTag_orderlinkes.toString()));
			}
		}
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#orderduedate#", (order.getDueDate() == null) ? "" : formatter.format(order.getDueDate()).toString()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#orderuserduedate#", (order.getUserDueDate() == null) ? "" : formatter.format(order.getUserDueDate()).toString()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#date#", formatter2.format(new Date()).toString()));
		orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#ponumber#", (order.getPurchaseOrder() == null) ? "" : order.getPurchaseOrder().toString()));

		// customer fields
		Class<Customer> c1 = Customer.class;
		Method m1 = null;
		Object arglist1[] = null;
		for (CustomerField customerField : getCustomerFields()) {
			if (customerField.isEnabled()) {
				m1 = c1.getMethod("getField" + customerField.getId());
				customerField.setValue((String) m1.invoke(customer, arglist1));
				orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#customerField" + customerField.getId() + "#", (customerField.getValue() == null) ? "" : customerField.getValue()));
			}
		}
		if (salesRep != null) {
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#salesRepName#", salesRep.getName()));
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#salesRepEmail#", salesRep.getEmail()));
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		


		if (customContact != null) {
			System.out.println("customContact.getId()"+customContact.getId());

			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#contactName#", customContact.getContactName()));
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#contactCompany#", (customContact.getCompany() == null) ? "" : customContact.getCompany()));
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#contactEmail#", (customContact.getEmail() == null) ? "" : customContact.getEmail()));
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#contactPhone1#", (customContact.getTel1() == null) ? "" : customContact.getTel1()));
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#contactPhone2#", (customContact.getTel2() == null) ? "" : customContact.getTel2()));
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#contactWebsite#", (customContact.getWebsite() == null) ? "" : customContact.getWebsite()));
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#contactWebsite#", (customContact.getWebsite() == null) ? "" : customContact.getWebsite()));
			
			orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#contactWebsite#", (customContact.getWebsite() == null) ? "" : customContact.getWebsite()));
			
			System.out.println("orderStatus.getTrackNum()"+orderStatus.getTrackNum());

			// New Dynamic URL
			if(orderStatus.getTrackNum() != null && !orderStatus.getTrackNum().equals("") && customContact.getTrackUrl()!= null && !customContact.getTrackUrl().equals("")){
				String newTrackUrl = customContact.getTrackUrl();
				newTrackUrl = newTrackUrl.replaceAll("(?i)#TrackingNum#", orderStatus.getTrackNum());	
				System.out.println("newTrackUrl"+newTrackUrl);
				orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#newTrackNumUrl#",newTrackUrl));	
			}
		
			if(customContact.getId() == 1){
				orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#trackNumUrl#", (orderStatus.getTrackNum() == null) ? "" : "http://wwwapps.ups.com/WebTracking/track?HTMLVersion=5.0&loc=en_US&Requester=UPSHome&WBPM_lid=homepage%2Fct1.html_pnl_trk&trackNums="+orderStatus.getTrackNum()+"&track.x=Track"));
			}else if(customContact.getId() == 57){
				orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#trackNumUrl#", (orderStatus.getTrackNum() == null) ? "" : "https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers="+orderStatus.getTrackNum()+"&locale=en_US&cntry_code=us"));
			}else if(customContact.getId() == 31){
				orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#trackNumUrl#", (orderStatus.getTrackNum() == null) ? "" : "https://tools.usps.com/go/TrackConfirmAction!input.action?tLabels="+orderStatus.getTrackNum()));
			}else if(customContact.getId() == 2){
				orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#trackNumUrl#", (orderStatus.getTrackNum() == null) ? "" : "https://www.rrts.com/Tools/Tracking/Pages/MultipleResults.aspx?PROS="+orderStatus.getTrackNum()));
			}else if(customContact.getId() == 49){
				orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#trackNumUrl#", (orderStatus.getTrackNum() == null) ? "" : "http://www.godependable.com/TrackingNew.aspx?t="+orderStatus.getTrackNum()));
			}else{
				orderStatus.setMessage(orderStatus.getMessage().replaceAll("(?i)#trackNumUrl#", ""));

			}
			
			
			
		}
	}

	// replace dynamic element
	public String replaceDynamicElement(String original, Customer customer, SalesRep salesRep) throws Exception {
		Format formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		Format formatter2 = new SimpleDateFormat("MM/dd/yyyy");

		original = original.replaceAll("(?i)#firstname#", (customer.getAddress().getFirstName() == null) ? "" : customer.getAddress().getFirstName());
		original = original.replaceAll("(?i)#lastname#", (customer.getAddress().getLastName() == null) ? "" : customer.getAddress().getLastName());
		original = original.replaceAll("(?i)#email#", customer.getUsername());
		original = original.replaceAll("(?i)#address1#", (customer.getAddress().getAddr1() == null) ? "" : customer.getAddress().getAddr1());
		original = original.replaceAll("(?i)#address2#", (customer.getAddress().getAddr2() == null) ? "" : customer.getAddress().getAddr2());
		original = original.replaceAll("(?i)#country#", (customer.getAddress().getCountry() == null) ? "" : customer.getAddress().getCountry());
		original = original.replaceAll("(?i)#city#", (customer.getAddress().getCity() == null) ? "" : customer.getAddress().getCity());
		original = original.replaceAll("(?i)#state#", (customer.getAddress().getStateProvince() == null) ? "" : customer.getAddress().getStateProvince());
		original = original.replaceAll("(?i)#phone#", (customer.getAddress().getPhone() == null) ? "" : customer.getAddress().getPhone());
		original = original.replaceAll("(?i)#cellphone#", (customer.getAddress().getCellPhone() == null) ? "" : customer.getAddress().getCellPhone());
		original = original.replaceAll("(?i)#company#", (customer.getAddress().getCompany() == null) ? "" : customer.getAddress().getCompany());
		original = original.replaceAll("(?i)#deliverytype#", (customer.getAddress().isResidential() == true) ? "Yes" : "No");
		original = original.replaceAll("(?i)#liftgate#", (customer.getAddress().isLiftGate() == true) ? "Yes" : "No");
		original = original.replaceAll("(?i)#date#", formatter2.format(new Date()).toString());
		original = original.replaceAll("(?i)#note#", (customer.getNote() == null) ? "" : customer.getNote());

		// customer fields
		Class<Customer> c1 = Customer.class;
		Method m1 = null;
		Object arglist1[] = null;
		for (CustomerField customerField : getCustomerFields()) {
			if (customerField.isEnabled()) {
				m1 = c1.getMethod("getField" + customerField.getId());
				customerField.setValue((String) m1.invoke(customer, arglist1));
				original = original.replaceAll("(?i)#customerField" + customerField.getId() + "#", (customerField.getValue() == null) ? "" : customerField.getValue());
			}
		}

		if (salesRep != null) {
			original = original.replaceAll("(?i)#salesRepName#", salesRep.getName());
			original = original.replaceAll("(?i)#salesRepEmail#", salesRep.getEmail());
			original = original.replaceAll("(?i)#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone());
			original = original.replaceAll("(?i)#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone());
		}

		return original;
	}

	public void replaceDynamicElement(EmailMessageForm form, CrmContact contact, SalesRep salesRep) throws Exception {
		Format formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		Format formatter2 = new SimpleDateFormat("MM/dd/yyyy");

		form.setSubject(form.getSubject().replaceAll("(?i)#firstname#", (contact.getFirstName() == null) ? "" : contact.getFirstName()));
		form.setSubject(form.getSubject().replaceAll("(?i)#lastname#", (contact.getLastName() == null) ? "" : contact.getLastName()));
		form.setSubject(form.getSubject().replaceAll("(?i)#email#", contact.getEmail1()));
		form.setSubject(form.getSubject().replaceAll("(?i)#date#", formatter2.format(new Date()).toString()));

		if (salesRep != null) {
			form.setSubject(form.getSubject().replaceAll("(?i)#salesRepName#", salesRep.getName()));
			form.setSubject(form.getSubject().replaceAll("(?i)#salesRepEmail#", salesRep.getEmail()));
			form.setSubject(form.getSubject().replaceAll("(?i)#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			form.setSubject(form.getSubject().replaceAll("(?i)#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		
		Customer customer = getCustomerById(getUserIdByCrmId(contact.getId()));
    	if(customer!=null && customer.getId()!=null){
    		
        	Random rand = new Random();
        	int  n1 = rand.nextInt(500) + 1;
        	int  n2 = rand.nextInt(500) + 1;
        	
            String originalString = customer.getId() + "-" + n1 + "-" + n2;   
    	    byte[] encoded = Base64.encodeBase64(originalString.getBytes());         
    	    String encodedString = new String(encoded);  
    		
			
			form.setMessage(form.getMessage().replaceAll("#DOC_BSUSRC#", "https://www.viatrading.com/dv/sign/form1.php?id=" + encodedString));
			form.setMessage(form.getMessage().replaceAll("#DOC_BSUSRC_SPA#", "https://www.viatrading.com/dv/sign/form1es.php?id=" + encodedString));
			form.setMessage(form.getMessage().replaceAll("#DOC_CRC#", "https://www.viatrading.com/dv/sign/form2.php?id=" + encodedString));
			form.setMessage(form.getMessage().replaceAll("#DOC_CRC_SPA#", "https://www.viatrading.com/dv/sign/form2es.php?id=" + encodedString));
			form.setMessage(form.getMessage().replaceAll("#DOC_MPA#", "https://www.viatrading.com/dv/sign/form3.php?id=" + encodedString));
			form.setMessage(form.getMessage().replaceAll("#DOC_MAP_SPA#", "https://www.viatrading.com/dv/sign/form3es.php?id=" + encodedString));
    	}  
    	
    	
    	if(contact!=null && contact.getId()!=null){
    		try{
		    	
		    	form.setMessage(form.getMessage().replaceAll("#crmcontactid#", contact.getId().toString()));
    		}catch(Exception ex){}
    	}

		form.setMessage(form.getMessage().replaceAll("(?i)#firstname#", (contact.getFirstName() == null) ? "" : contact.getFirstName()));
		form.setMessage(form.getMessage().replaceAll("(?i)#lastname#", (contact.getLastName() == null) ? "" : contact.getLastName()));
		form.setMessage(form.getMessage().replaceAll("(?i)#accountname#", (contact.getAccountName() == null) ? "" : contact.getAccountName()));
		form.setMessage(form.getMessage().replaceAll("(?i)#email#", contact.getEmail1()));
		form.setMessage(form.getMessage().replaceAll("(?i)#address1#", (contact.getStreet() == null) ? "" : contact.getStreet()));
		form.setMessage(form.getMessage().replaceAll("(?i)#address2#", (contact.getOtherStreet() == null) ? "" : contact.getOtherStreet()));
		form.setMessage(form.getMessage().replaceAll("(?i)#country#", (contact.getCountry() == null) ? "" : contact.getCountry()));
		form.setMessage(form.getMessage().replaceAll("(?i)#city#", (contact.getCity() == null) ? "" : contact.getCity()));
		form.setMessage(form.getMessage().replaceAll("(?i)#state#", (contact.getStateProvince() == null) ? "" : contact.getStateProvince()));
		form.setMessage(form.getMessage().replaceAll("(?i)#phone#", (contact.getPhone1() == null) ? "" : contact.getPhone1()));
		form.setMessage(form.getMessage().replaceAll("(?i)#cellphone#", (contact.getPhone2() == null) ? "" : contact.getPhone2()));
		form.setMessage(form.getMessage().replaceAll("(?i)#deliverytype#", (contact.getAddressType() == null) ? "" : contact.getAddressType()));
		form.setMessage(form.getMessage().replaceAll("(?i)#date#", formatter2.format(new Date()).toString()));
		form.setMessage(form.getMessage().replaceAll("(?i)#description#", (contact.getDescriptionTrimmed() == null) ? "" : contact.getDescriptionTrimmed()));
		String[] dialingNotes = null;
		if(contact.getDialingNoteHistoryString() != null && !contact.getDialingNoteHistoryString().isEmpty()) {
			dialingNotes = contact.getDialingNoteHistoryString().split("\\|\\|");
		}
		form.setMessage(form.getMessage().replaceAll("(?i)#dialingNotes#", (dialingNotes == null) ? "" : dialingNotes[dialingNotes.length - 1]));


		if (salesRep != null) {
			form.setMessage(form.getMessage().replaceAll("(?i)#salesRepName#", salesRep.getName()));
			form.setMessage(form.getMessage().replaceAll("(?i)#salesRepEmail#", salesRep.getEmail()));
			form.setMessage(form.getMessage().replaceAll("(?i)#salesRepPhone#", (salesRep.getPhone() == null) ? "" : salesRep.getPhone()));
			form.setMessage(form.getMessage().replaceAll("(?i)#salesRepCellPhone#", (salesRep.getCellPhone() == null) ? "" : salesRep.getCellPhone()));
		}
		for (CrmContactField crmContactField : contact.getCrmContactFields()) {
			if (crmContactField.isEnabled()) {
				form.setMessage(form.getMessage().replaceAll("(?i)#contactfield" + crmContactField.getId() + "#", (crmContactField.getFieldValue() == null) ? "" : crmContactField.getFieldValue()));
			}
		}
		
	}
	
	public void replaceDynamicElement(EmailMessageForm form, Customer customer, AccessUser accessUser) throws Exception {
		Format formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		Format formatter2 = new SimpleDateFormat("MM/dd/yyyy");

		form.setSubject(form.getSubject().replaceAll("(?i)#firstname#", (customer.getAddress().getFirstName() == null) ? "" : customer.getAddress().getFirstName()));
		form.setSubject(form.getSubject().replaceAll("(?i)#lastname#", (customer.getAddress().getLastName() == null) ? "" : customer.getAddress().getLastName()));
		form.setSubject(form.getSubject().replaceAll("(?i)#email#", customer.getUsername()));
		form.setSubject(form.getSubject().replaceAll("(?i)#email#", customer.getUsername()));
		form.setSubject(form.getSubject().replaceAll("(?i)#date#", formatter2.format(new Date()).toString()));


		if (accessUser != null) {
			form.setSubject(form.getSubject().replaceAll("(?i)#salesRepName#", accessUser.getName()));
			form.setSubject(form.getSubject().replaceAll("(?i)#salesRepEmail#", accessUser.getEmail()));
			
		}

		form.setMessage(form.getMessage().replaceAll("(?i)#firstname#", (customer.getAddress().getFirstName() == null) ? "" : customer.getAddress().getFirstName()));
		form.setMessage(form.getMessage().replaceAll("(?i)#lastname#", (customer.getAddress().getLastName() == null) ? "" : customer.getAddress().getLastName()));
		form.setMessage(form.getMessage().replaceAll("(?i)#email#", customer.getUsername()));
		form.setMessage(form.getMessage().replaceAll("(?i)#address1#", (customer.getAddress().getAddr1() == null) ? "" : customer.getAddress().getAddr1()));
		form.setMessage(form.getMessage().replaceAll("(?i)#address2#", (customer.getAddress().getAddr2() == null) ? "" : customer.getAddress().getAddr2()));
		form.setMessage(form.getMessage().replaceAll("(?i)#country#", (customer.getAddress().getCountry() == null) ? "" : customer.getAddress().getCountry()));
		form.setMessage(form.getMessage().replaceAll("(?i)#city#", (customer.getAddress().getCity() == null) ? "" : customer.getAddress().getCity()));
		form.setMessage(form.getMessage().replaceAll("(?i)#state#", (customer.getAddress().getStateProvince() == null) ? "" : customer.getAddress().getStateProvince()));
		form.setMessage(form.getMessage().replaceAll("(?i)#phone#", (customer.getAddress().getPhone() == null) ? "" : customer.getAddress().getPhone()));
		form.setMessage(form.getMessage().replaceAll("(?i)#cellphone#", (customer.getAddress().getCellPhone() == null) ? "" : customer.getAddress().getCellPhone()));
		form.setMessage(form.getMessage().replaceAll("(?i)#company#", (customer.getAddress().getCompany() == null) ? "" : customer.getAddress().getCompany()));
        form.setMessage(form.getMessage().replaceAll("(?i)#deliverytype#", (customer.getAddress().isResidential() == true) ? "Yes" : "No"));
		form.setMessage(form.getMessage().replaceAll("(?i)#liftgate#", (customer.getAddress().isLiftGate() == true) ? "Yes" : "No"));
		form.setMessage(form.getMessage().replaceAll("(?i)#date#", formatter2.format(new Date()).toString()));
		form.setMessage(form.getMessage().replaceAll("(?i)#note#", (customer.getNote() == null) ? "" : customer.getNote()));
		form.setMessage(form.getMessage().replaceAll("(?i)#dialingNotes#", (customer.getLastDialingNote() == null) ? "" : customer.getLastDialingNote()));


		if (accessUser != null) {
			form.setMessage(form.getMessage().replaceAll("(?i)#salesRepName#", accessUser.getName()));
			form.setMessage(form.getMessage().replaceAll("(?i)#salesRepEmail#", accessUser.getEmail()));
			
		}
		// customer fields
				Class<Customer> c1 = Customer.class;
				Method m1 = null;
				Object arglist1[] = null;
				for (CustomerField customerField : getCustomerFields()) {
					if (customerField.isEnabled()) {
						m1 = c1.getMethod("getField" + customerField.getId());
						customerField.setValue((String) m1.invoke(customer, arglist1));
						form.setMessage(form.getMessage().replaceAll("(?i)#customerfield" + customerField.getId() + "#", (customerField.getValue() == null) ? "" : customerField.getValue()));
				}
				
				}
		
	}
	
	public void replaceDynamicElement(EmailMessageForm form, CrmContact contact, AccessUser accessUser) throws Exception {
		Format formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		Format formatter2 = new SimpleDateFormat("MM/dd/yyyy");

		form.setSubject(form.getSubject().replaceAll("(?i)#firstname#", (contact.getFirstName() == null) ? "" : contact.getFirstName()));
		form.setSubject(form.getSubject().replaceAll("(?i)#lastname#", (contact.getLastName() == null) ? "" : contact.getLastName()));
		form.setSubject(form.getSubject().replaceAll("(?i)#email#", contact.getEmail1()));
		form.setSubject(form.getSubject().replaceAll("(?i)#date#", formatter2.format(new Date()).toString()));

		if (accessUser != null) {
			form.setSubject(form.getSubject().replaceAll("(?i)#salesRepName#", accessUser.getName()));
			form.setSubject(form.getSubject().replaceAll("(?i)#salesRepEmail#", accessUser.getEmail()));
			
		}

		form.setMessage(form.getMessage().replaceAll("(?i)#firstname#", (contact.getFirstName() == null) ? "" : contact.getFirstName()));
		form.setMessage(form.getMessage().replaceAll("(?i)#lastname#", (contact.getLastName() == null) ? "" : contact.getLastName()));
		form.setMessage(form.getMessage().replaceAll("(?i)#email#", contact.getEmail1()));
		form.setMessage(form.getMessage().replaceAll("(?i)#accountname#", (contact.getAccountName() == null) ? "" : contact.getAccountName()));
		form.setMessage(form.getMessage().replaceAll("(?i)#address1#", (contact.getStreet() == null) ? "" : contact.getStreet()));
		form.setMessage(form.getMessage().replaceAll("(?i)#address2#", (contact.getOtherStreet() == null) ? "" : contact.getOtherStreet()));
		form.setMessage(form.getMessage().replaceAll("(?i)#country#", (contact.getCountry() == null) ? "" : contact.getCountry()));
		form.setMessage(form.getMessage().replaceAll("(?i)#city#", (contact.getCity() == null) ? "" : contact.getCity()));
		form.setMessage(form.getMessage().replaceAll("(?i)#state#", (contact.getStateProvince() == null) ? "" : contact.getStateProvince()));
		form.setMessage(form.getMessage().replaceAll("(?i)#phone#", (contact.getPhone1() == null) ? "" : contact.getPhone1()));
		form.setMessage(form.getMessage().replaceAll("(?i)#cellphone#", (contact.getPhone2() == null) ? "" : contact.getPhone2()));
		form.setMessage(form.getMessage().replaceAll("(?i)#deliverytype#", (contact.getAddressType() == null) ? "" : contact.getAddressType()));
		form.setMessage(form.getMessage().replaceAll("(?i)#date#", formatter2.format(new Date()).toString()));
		form.setMessage(form.getMessage().replaceAll("(?i)#description#", (contact.getDescriptionTrimmed() == null) ? "" : contact.getDescriptionTrimmed()));


		if (accessUser != null) {
			form.setMessage(form.getMessage().replaceAll("(?i)#salesRepName#", accessUser.getName()));
			form.setMessage(form.getMessage().replaceAll("(?i)#salesRepEmail#", accessUser.getEmail()));
			
		}
		for (CrmContactField crmContactField : contact.getCrmContactFields()) {
			if (crmContactField.isEnabled()) {
				form.setMessage(form.getMessage().replaceAll("(?i)#contactfield" + crmContactField.getId() + "#", (crmContactField.getFieldValue() == null) ? "" : crmContactField.getFieldValue()));
			}
		}
		
	}

	// Budget
	public void insertOrderAprrovalDenialHistory(Order order, List<Integer> parentsList, MailSender mailSender, Map<String, Configuration> siteConfig) {
		this.orderDao.insertOrderAprrovalDenialHistory(order, parentsList, mailSender, siteConfig);

		// If order is approved by all AMs convert quote to order.
		if (!parentsList.isEmpty()) {
			convertQuoteToOrder(order, parentsList, mailSender, siteConfig);
		}
	}

	private void convertQuoteToOrder(Order order, List<Integer> parentsList, MailSender mailSender, Map<String, Configuration> siteConfig) {
		boolean check = true;
		// Check if all the parents approved this order.
		for (Integer parentId : parentsList) {
			if (parentId.compareTo(order.getUserId()) != 0) {
				if (check == getApproval(order.getOrderId(), parentId, false)) {
					continue;
				} else {
					check = false;
					break;
				}
			}
		}

		// if all approved order then change the order status and send email to admin.
		if (!parentsList.isEmpty() && check == true) {
			// update order Status
			OrderStatus orderStatus = new OrderStatus();
			orderStatus.setOrderId(order.getOrderId());
			orderStatus.setComments("Quote is approved by all Account Managers on: " + new Date());
			orderStatus.setStatus("p");
			orderStatus.setUsername("");
			insertOrderStatus(orderStatus);
			// notify admin
			notifyAdmin(order, mailSender, siteConfig);

			// if there is credit add payment using Customer Credit
			// We create Payment only if Customer has enough credit.
			Customer customer = getCustomerById(order.getUserId());

			if (order.getRequestForCredit() > 0 && customer.getCredit() >= order.getRequestForCredit()) {
				CreditImp credit = new CreditImp();
				CustomerCreditHistory customerCredit = new CustomerCreditHistory();

				PaymentImp customerPayment = new PaymentImp();
				List<Order> orders = new ArrayList<Order>();
				orders.add(order);
				Payment payment = new Payment();

				customerCredit = credit.updateCustomerCreditByOrderPayment(order, "payment", order.getRequestForCredit(), customer.getUsername());
				payment = customerPayment.createPayemnts(customer, "VBA Credit", order.getRequestForCredit(), "Customer used VBA Credit");

				for (Order orderPayment : orders) {
					orderPayment.setPayment(order.getRequestForCredit());
					// Set credit used based on the amount.
					if (orderPayment.getCreditUsed() != null) {
						orderPayment.setCreditUsed(orderPayment.getCreditUsed() + order.getRequestForCredit());
					} else {
						orderPayment.setCreditUsed(order.getRequestForCredit());
					}
				}

				updateCredit(customerCredit);
				insertCustomerPayment(payment, orders);
			}
		}
	}

	private void notifyAdmin(Order order, MailSender mailSender, Map<String, Configuration> siteConfig) {
		// send email notification
		String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
		String siteURL = siteConfig.get("SITE_URL").getValue();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Order # " + order.getOrderId() + " on " + siteURL + " is approved by all Account Managers.");
		msg.setText("Order # " + order.getOrderId() + " on " + siteURL + " is approved by all Account Managers.");
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}

	public List<Order> getOrderActionHistory(Integer orderId) {
		return this.orderDao.getOrderActionHistory(orderId);
	}

	public boolean getApproval(Integer orderId, Integer actionBy, boolean fullStatus) {
		return this.orderDao.getApproval(orderId, actionBy, fullStatus);
	}

	public Map<Integer, String> getCustomerNameMap(List<Integer> userIdList) {
		return this.customerDao.getCustomerNameMap(userIdList);
	}

	public LocationSearch getLocationSearch(HttpServletRequest request) {
		LocationSearch search = new LocationSearch();

		// status
		if (request.getParameter("zipcode") != null) {
			search.setZipcode(ServletRequestUtils.getStringParameter(request, "zipcode", null));
			try {
				Integer.parseInt(search.getZipcode());
			} catch (NumberFormatException e) {
				search.setZipcode(null);
			}
		}

		// keywords
		if (request.getParameter("keywords") != null) {
			search.setKeywords(ServletRequestUtils.getStringParameter(request, "keywords", ""));
		}

		// country
		if (request.getParameter("country") != null) {
			search.setCountry(ServletRequestUtils.getStringParameter(request, "country", ""));
		}

		// city
		if (request.getParameter("city") != null) {
			search.setCity(ServletRequestUtils.getStringParameter(request, "city", ""));
		}

		// categoryId
		if (request.getParameter("categoryId") != null) {
			search.setCategoryId(ServletRequestUtils.getStringParameter(request, "categoryId", ""));
		}

		// radius
		if (request.getParameter("radius") != null) {
			search.setRadius(ServletRequestUtils.getIntParameter(request, "radius", -1));
			if (search.getRadius() == -1) {
				search.setRadius(null);
			}
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);
			}
		}

		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				search.setPageSize(10);
			} else {
				search.setPageSize(size);
			}
		}
		return search;
	}

	// search
	public OrderSearch getOrderSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		OrderSearch orderSearch = (OrderSearch) request.getSession().getAttribute("orderSearch");
		if (orderSearch == null) {
			orderSearch = new OrderSearch();
			orderSearch.setPageSize(Integer.parseInt(siteConfig.get("DEFAULT_LIST_SIZE").getValue()));
			orderSearch.setDateType("orderDate");
			request.getSession().setAttribute("orderSearch", orderSearch);
			// if orderSearch = null & quote = true set status ='xq'
			if (request.getAttribute("orderName") != null && request.getAttribute("orderName").equals("quotes")) {
				orderSearch.setStatus("xq");
			}
		}

		// sorting
		if (request.getParameter("sort") != null) {
			orderSearch.setSort(ServletRequestUtils.getStringParameter(request, "sort", "order_id DESC"));
		}

		// order status
		if (request.getParameter("status") != null) {
			orderSearch.setStatus(ServletRequestUtils.getStringParameter(request, "status", ""));
		}

		// order sub state
		if (request.getParameter("sub_status") != null) {
			orderSearch.setSubStatus(ServletRequestUtils.getStringParameter(request, "sub_status", ""));
		}

		// order num
		if (request.getParameter("orderNum") != null) {
			try {
				Integer orderNum = Integer.parseInt(request.getParameter("orderNum"));
				if (orderNum > 0) {
					orderSearch.setOrderNum(orderNum.toString());
				} else {
					orderSearch.setOrderNum("");
				}
			} catch (Exception e) {
				orderSearch.setOrderNum("");
			}
		}

		// paid full or not
		if (request.getParameter("paidFull") != null) {
			orderSearch.setPaidFull(ServletRequestUtils.getStringParameter(request, "paidFull", "").trim());
		}
		
		// purchase order
		if (request.getParameter("purchaseOrder") != null) {
			orderSearch.setPurchaseOrder(ServletRequestUtils.getStringParameter(request, "purchaseOrder", "").trim());
		}

		// billTo
		if (request.getParameter("bill_to") != null) {
			orderSearch.setBillToName(ServletRequestUtils.getStringParameter(request, "bill_to", "").trim());
		}

		// shipTo
		if (request.getParameter("ship_to") != null) {
			orderSearch.setShipToName(ServletRequestUtils.getStringParameter(request, "ship_to", "").trim());
		}

		// company Name
		if (request.getParameter("company_name") != null) {
			orderSearch.setCompanyName(ServletRequestUtils.getStringParameter(request, "company_name", "").trim());
		}

		// email
		if (request.getParameter("email") != null) {
			orderSearch.setEmail(ServletRequestUtils.getStringParameter(request, "email", "").trim());
		}
		
		//multiple Search
		if (request.getParameter("multipleSearch") != null) {
			String multiple = (String)ServletRequestUtils.getStringParameter(request, "multipleSearch", "").trim();
			String[] multiples = multiple.split(" ");
			orderSearch.setMultipleSearch(multiples);
			orderSearch.setMultipleSearchString(multiple);
		}

		// rating 1
		if (request.getParameter("rating1") != null) {
			orderSearch.setRating1(ServletRequestUtils.getStringParameter(request, "rating1", ""));
		}

		// rating 2
		if (request.getParameter("rating2") != null) {
			orderSearch.setRating2(ServletRequestUtils.getStringParameter(request, "rating2", ""));
		}

		// phone
		if (request.getParameter("phone") != null) {
			orderSearch.setPhone(ServletRequestUtils.getStringParameter(request, "phone", "").trim());
		}

		// zipcode
		if (request.getParameter("zipcode") != null) {
			orderSearch.setZipcode(ServletRequestUtils.getStringParameter(request, "zipcode", "").trim());
		}

		// trackCode
		if (request.getParameter("trackCode") != null) {
			orderSearch.setTrackCode(ServletRequestUtils.getStringParameter(request, "trackCode", "").trim());
		}
		
		// trackCode
		if (request.getParameter("qualifier") != null) {
			orderSearch.setQualifierId(ServletRequestUtils.getIntParameter(request, "qualifier", -1));
		}

		// promoCode
		if (request.getParameter("promoCode") != null) {
			orderSearch.setPromoCode(ServletRequestUtils.getStringParameter(request, "promoCode", "").trim());
		}

		// paymentMethod
		if (request.getParameter("paymentMethod") != null) {
			orderSearch.setPaymentMethod(ServletRequestUtils.getStringParameter(request, "paymentMethod", "").trim());
		}

		// state
		if (request.getParameter("state") != null) {
			orderSearch.setState(ServletRequestUtils.getStringParameter(request, "state", "").trim());
		}

		// country
		if (request.getParameter("country") != null) {
			orderSearch.setCountry(ServletRequestUtils.getStringParameter(request, "country", "").trim());
		}

		// sales rep
		if (request.getParameter("sales_rep_id") != null) {
			orderSearch.setSalesRepId(ServletRequestUtils.getIntParameter(request, "sales_rep_id", -1));
		}

		// sales rep processedBy
		if (request.getParameter("salesRepProcessedById") != null) {
			orderSearch.setSalesRepProcessedById(ServletRequestUtils.getIntParameter(request, "salesRepProcessedById", -1));
		}

		

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				orderSearch.setPage(1);
			} else {
				orderSearch.setPage(page);
			}
		}

		// page
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				orderSearch.setPageSize(10);
			} else {
				orderSearch.setPageSize(size);
			}
		}

		// host for multi store
		if (request.getParameter("host") != null) {
			orderSearch.setHost(ServletRequestUtils.getStringParameter(request, "host", ""));
		}

		// shipping Method
		if (request.getParameter("shippingMethod") != null) {
			orderSearch.setShippingMethod(ServletRequestUtils.getStringParameter(request, "shippingMethod", ""));
			orderSearch.setShippingOperator(ServletRequestUtils.getIntParameter(request, "shippingOperator", 1));

		}

		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				orderSearch.setStartDate(df.parse(request.getParameter("startDate")));
			} catch (ParseException e) {
				orderSearch.setStartDate(null);
			}
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				orderSearch.setEndDate(df.parse(request.getParameter("endDate")));
			} catch (ParseException e) {
				orderSearch.setEndDate(null);
			}
		}
		// Date Type
		if (request.getParameter("dateType") != null 
				&& (orderSearch.getStartDate() != null || orderSearch.getEndDate() != null)) {
			orderSearch.setDateType(ServletRequestUtils.getStringParameter(request, "dateType", ""));
		}
		
		// data type (=shipDate) without start and end date creates slow query
		if(orderSearch.getStartDate() == null && orderSearch.getEndDate() == null) {
			orderSearch.setDateType("");
		}

		// Sku
		if (request.getParameter("sku") != null) {
			String sku = ServletRequestUtils.getStringParameter(request, "sku", null);
			//If search multiple skus, we use orderSearch.setMultipleSku, 
			if(sku.contains(",")){
				String[] skus = sku.split(",");
				for(int i=0; i<skus.length; i++){
					skus[i] = skus[i].trim();
				}
				orderSearch.setMultipleSku(skus);
				//for show what sku is used to search on the search filter.
				orderSearch.setSku(sku);
			}else{
				orderSearch.setSku(sku);
				orderSearch.setMultipleSku(null);
			}
			orderSearch.setSkuOperator(ServletRequestUtils.getIntParameter(request, "skuOperator", 0));
		}
		
		// Product Parent Sku
		if (request.getParameter("product_parent_sku") != null) {
			orderSearch.setParentSku(ServletRequestUtils.getStringParameter(request, "product_parent_sku", "").trim());
			orderSearch.setParentSkuOperator(ServletRequestUtils.getIntParameter(request, "product_parent_skuOperator", 0));
		}

		// Backend Order
		if (request.getParameter("backendOrder") != null) {
			orderSearch.setBackendOrder(ServletRequestUtils.getStringParameter(request, "backendOrder", ""));
		}

		// Product Field
		if (request.getParameter("productField") != null) {
			orderSearch.setProductField(ServletRequestUtils.getStringParameter(request, "productField", null));
		}

		// Product Field Number
		if (request.getParameter("productFieldNumber") != null) {
			orderSearch.setProductFieldNumber(ServletRequestUtils.getStringParameter(request, "productFieldNumber", null));
		}

		// Customer Field
		if (request.getParameter("customerField") != null) {
			orderSearch.setCustomerField(ServletRequestUtils.getStringParameter(request, "customerField", null));
		}

		// Customer Field Number
		if (request.getParameter("customerFieldNumber") != null) {
			orderSearch.setCustomerFieldNumber(ServletRequestUtils.getStringParameter(request, "customerFieldNumber", null));
		}

		// Order Type
		if (request.getParameter("orderType") != null) {
			orderSearch.setOrderType(ServletRequestUtils.getStringParameter(request, "orderType", null));
		}

		// Flag1
		if (request.getParameter("flag1") != null) {
			orderSearch.setFlag1(ServletRequestUtils.getStringParameter(request, "flag1", null));
		}

		// SubTotal
		if (request.getParameter("subTotal") != null) {
			orderSearch.setSubTotal((ServletRequestUtils.getDoubleParameter(request, "subTotal", -1.0) == -1.0) ? null : ServletRequestUtils.getDoubleParameter(request, "subTotal", -1.0));
		}

		// Card ID
		if (request.getParameter("cardId") != null) {
			orderSearch.setCardId(ServletRequestUtils.getStringParameter(request, "cardId", ""));
		}

		// group ID
		if (request.getParameter("groupId") != null) {
			int groupId = ServletRequestUtils.getIntParameter(request, "groupId", -1);
			if (groupId == -1) {
				orderSearch.setGroupId(null);
			} else {
				orderSearch.setGroupId(groupId);
			}
		}

		// triplefin
		if (request.getParameter("triplefin") != null) {
			if (ServletRequestUtils.getStringParameter(request, "triplefin", "").equals("")) {
				orderSearch.setTriplefin(null);
			} else {
				orderSearch.setTriplefin(ServletRequestUtils.getBooleanParameter(request, "triplefin", false));
			}
		}

		// language
		if (request.getParameter("language") != null) {
			orderSearch.setLanguageCode(ServletRequestUtils.getStringParameter(request, "language", ""));
		}
		
		// first Order
		if (request.getParameter("firstOrder") != null) {
			if (request.getParameter("firstOrder") != "") {
				orderSearch.setFirstOrder(Integer.parseInt(ServletRequestUtils.getStringParameter(request, "firstOrder", "")));
			} else {
				orderSearch.setFirstOrder(null);
			}
		}


		return orderSearch;
	}

	public CustomerSearch getCustomerSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		CustomerSearch customerSearch = (CustomerSearch) request.getSession().getAttribute("customerSearch");
		// 'emptyFilter' is used to reset all the filters to show the sub-accounts of 'parent'
		if (customerSearch == null || request.getParameter("emptyFilter") != null) {
			customerSearch = new CustomerSearch();
			customerSearch.setPageSize(Integer.parseInt(siteConfig.get("DEFAULT_LIST_SIZE").getValue()));
			request.getSession().setAttribute("customerSearch", customerSearch);
		}

		// supplier
		if (request.getParameter("supplier") != null) {
			if (request.getParameter("supplier") != "") {
				customerSearch.setSupplier(Integer.parseInt(ServletRequestUtils.getStringParameter(request, "supplier", "")));
			} else {
				customerSearch.setSupplier(null);
			}
		}
		// text message
		if (request.getParameter("textMessage") != null) {
			if (request.getParameter("textMessage") != "") {
				customerSearch.setTextMessage(Integer.parseInt(ServletRequestUtils.getStringParameter(request, "textMessage", "")));
			} else {
				customerSearch.setTextMessage(null);
			}
		}
		// supplier prefix
		if (request.getParameter("supplierPrefix") != null) {
			customerSearch.setSupplierPrefix(ServletRequestUtils.getStringParameter(request, "supplierPrefix", "").trim());
		}
		// sorting
		if (request.getParameter("sort") != null) {
			customerSearch.setSort(ServletRequestUtils.getStringParameter(request, "sort", ""));
		}

		// parent ID
		if (request.getParameter("parent") != null) {
			int parentId = ServletRequestUtils.getIntParameter(request, "parent", 0);
			if (parentId == -1) {
				customerSearch.setParent(-1);
			} else if (parentId > 0) {
				customerSearch.setParent(parentId);
			} else {
				customerSearch.setParent(null);
			}
		}

		// first name
		if (request.getParameter("firstName") != null) {
			customerSearch.setFirstName(ServletRequestUtils.getStringParameter(request, "firstName", "").trim());
		}

		// last name
		if (request.getParameter("lastName") != null) {
			customerSearch.setLastName(ServletRequestUtils.getStringParameter(request, "lastName", "").trim());
		}

		// rating 1
		if (request.getParameter("rating1") != null) {
			customerSearch.setRating1(ServletRequestUtils.getStringParameter(request, "rating1", ""));
		}

		// rating 2
		if (request.getParameter("rating2") != null) {
			customerSearch.setRating2(ServletRequestUtils.getStringParameter(request, "rating2", ""));
		}
		
		// rating 2
		if (request.getParameter("qualifier") != null) {
			customerSearch.setQualifierId(ServletRequestUtils.getIntParameter(request, "qualifier", -1));
		}

		// sales rep
		if (request.getParameter("sales_rep_id") != null) {
			customerSearch.setSalesRepId(ServletRequestUtils.getIntParameter(request, "sales_rep_id", -1));
		}

		// email
		if (request.getParameter("email") != null) {
			customerSearch.setEmail(ServletRequestUtils.getStringParameter(request, "email", "").trim());
		}

		// account number
		if (request.getParameter("accountNumber") != null) {
			customerSearch.setAccountNumber(ServletRequestUtils.getStringParameter(request, "accountNumber", "").trim());
		}

		// company name
		if (request.getParameter("companyName") != null) {
			customerSearch.setCompanyName(ServletRequestUtils.getStringParameter(request, "companyName", "").trim());
		}
		
		// multiple Search
		if (request.getParameter("multipleSearch") != null) {
			String multiple = (String)ServletRequestUtils.getStringParameter(request, "multipleSearch", "").trim();
			String[] multiples = multiple.split(" ");
			customerSearch.setMultipleSearch(multiples);
			customerSearch.setMultipleSearchString(multiple);
		}
		
		// zipcode
		if (request.getParameter("zipcode") != null) {
			customerSearch.setZipcode(ServletRequestUtils.getStringParameter(request, "zipcode", "").trim());
		}
		
		// radius
		if (request.getParameter("radius") != null) {
			LocationSearch locationSearch = new LocationSearch();
			locationSearch.setRadius(ServletRequestUtils.getIntParameter(request, "radius", -1));			
			locationSearch.setZipcode(ServletRequestUtils.getStringParameter(request, "zipcode", "").trim());
			customerSearch.setLocationSearch(locationSearch);
		}

		// phone
		if (request.getParameter("phone") != null) {
			customerSearch.setPhone(ServletRequestUtils.getStringParameter(request, "phone", "").trim());
		}
		
		// cellPhone
		if (request.getParameter("cellPhone") != null) {
			customerSearch.setCellPhone(ServletRequestUtils.getStringParameter(request, "cellPhone", "").trim());
		}
		
		// medium
		if (request.getParameter("medium") != null) {
			customerSearch.setMedium(ServletRequestUtils.getStringParameter(request, "medium", "").trim());
		}
		
		if (request.getParameter("mediumDropDown") != null) {
			if (request.getParameter("mediumDropDown") != "") {
				customerSearch.setMediumDropDown(Integer.parseInt(ServletRequestUtils.getStringParameter(request, "mediumDropDown", "")));
			} else {
				customerSearch.setMediumDropDown(null);
			}
		}
				
		// mainSource
		if (request.getParameter("mainSource") != null) {
			customerSearch.setMainSource(ServletRequestUtils.getStringParameter(request, "mainSource", "").trim());
		}
		
		if (request.getParameter("mainSourceDropDown") != null) {
			if (request.getParameter("mainSourceDropDown") != "") {
				customerSearch.setMainSourceDropDown(Integer.parseInt(ServletRequestUtils.getStringParameter(request, "mainSourceDropDown", "")));
			} else {
				customerSearch.setMainSourceDropDown(null);
			}
		}
		
		// paid
		if (request.getParameter("paid") != null) {
			customerSearch.setPaid(ServletRequestUtils.getStringParameter(request, "paid", "").trim());
		}
		
		if (request.getParameter("paidDropDown") != null) {
			if (request.getParameter("paidDropDown") != "") {
				customerSearch.setPaidDropDown(Integer.parseInt(ServletRequestUtils.getStringParameter(request, "paidDropDown", "")));
			} else {
				customerSearch.setPaidDropDown(null);
			}
		}
		
		// language
		if (request.getParameter("languageField") != null) {
			customerSearch.setLanguageField(ServletRequestUtils.getStringParameter(request, "languageField", "").trim());
		}
		
		if (request.getParameter("languageFieldDropDown") != null) {
			if (request.getParameter("languageFieldDropDown") != "") {
				customerSearch.setLanguageFieldDropDown(Integer.parseInt(ServletRequestUtils.getStringParameter(request, "languageFieldDropDown", "")));
			} else {
				customerSearch.setLanguageFieldDropDown(null);
			}
		}
		
		// city
		if (request.getParameter("city") != null) {
			customerSearch.setCity(ServletRequestUtils.getStringParameter(request, "city", "").trim());
		}

		// state
		if (request.getParameter("state") != null) {
			customerSearch.setState(ServletRequestUtils.getStringParameter(request, "state", ""));
		}

		// unsubscribe
		if (request.getParameter("unsubscribe") != null) {
			customerSearch.setUnsubscribe(ServletRequestUtils.getStringParameter(request, "unsubscribe", ""));
		}

		// country
		if (request.getParameter("country") != null) {
			customerSearch.setCountry(ServletRequestUtils.getStringParameter(request, "country", ""));
		}

		// region
		if (request.getParameter("region") != null) {
			customerSearch.setRegion(ServletRequestUtils.getStringParameter(request, "region", ""));
		}
		
		// timeZone
		if (request.getParameter("timeZone") != null) {
			customerSearch.setTimeZone(ServletRequestUtils.getStringParameter(request, "timeZone", ""));
		}

		// nationalRegion
		if (request.getParameter("nationalRegion") != null) {
			customerSearch.setNationalRegion(ServletRequestUtils.getStringParameter(request, "nationalRegion", ""));
		}
			
		// language
		if (request.getParameter("language") != null) {
			customerSearch.setLanguageCode(ServletRequestUtils.getStringParameter(request, "language", ""));
		}

		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				customerSearch.setStartDate(df.parse(request.getParameter("startDate")));
			} catch (ParseException e) {
				customerSearch.setStartDate(null);
			}
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				customerSearch.setEndDate(df.parse(request.getParameter("endDate")));
			} catch (ParseException e) {
				customerSearch.setEndDate(null);
			}
		}
		// cartStartDate
		if (request.getParameter("cartStartDate") != null) {
			try {
				customerSearch.setCartStartDate(df.parse(request.getParameter("cartStartDate")));
			} catch (ParseException e) {
				customerSearch.setCartStartDate(null);
			}
		}

		// cartEndDate
		if (request.getParameter("cartEndDate") != null) {
			try {
				customerSearch.setCartEndDate(df.parse(request.getParameter("cartEndDate")));
			} catch (ParseException e) {
				customerSearch.setCartEndDate(null);
			}
		}

		// login start date
		if (request.getParameter("loginStartDate") != null) {
			try {
				customerSearch.setLoginStartDate(df.parse(request.getParameter("loginStartDate")));
			} catch (ParseException e) {
				customerSearch.setLoginStartDate(null);
			}
		}

		// login end date
		if (request.getParameter("loginEndDate") != null) {
			try {
				customerSearch.setLoginEndDate(df.parse(request.getParameter("loginEndDate")));
			} catch (ParseException e) {
				customerSearch.setLoginEndDate(null);
			}
		}

		// Customer Field
		if (request.getParameter("customerField") != null) {
			customerSearch.setCustomerField(ServletRequestUtils.getStringParameter(request, "customerField", null));
		}

		// Customer Field Number
		if (request.getParameter("customerFieldNumber") != null) {
			customerSearch.setCustomerFieldNumber(ServletRequestUtils.getStringParameter(request, "customerFieldNumber", null));
		}

		// Card ID
		if (request.getParameter("cardId") != null) {
			customerSearch.setCardId(ServletRequestUtils.getStringParameter(request, "cardId", "").trim());
		}

		// group ID
		if (request.getParameter("groupId") != null) {
			int groupId = ServletRequestUtils.getIntParameter(request, "groupId", -1);
			if (groupId == -1) {
				customerSearch.setGroupId(null);
			} else {
				customerSearch.setGroupId(groupId);
			}
		}

		if (request.getParameter("suspended") != null) {
			String suspended = ServletRequestUtils.getStringParameter(request, "suspended", "");
			if (suspended.equalsIgnoreCase("true")) {
				customerSearch.setSuspended(true);
			} else if (suspended.equalsIgnoreCase("false")) {
				customerSearch.setSuspended(false);
			} else {
				customerSearch.setSuspended(null);
			}
		}
		// trackcode
		if (request.getParameter("trackcode") != null) {
			customerSearch.setTrackcode(ServletRequestUtils.getStringParameter(request, "trackcode", "").trim());
		}
		
		// trackcode
		if (request.getParameter("trackcodeFieldDropDown") != null) {
			customerSearch.setTrackcodeFieldDropDown(ServletRequestUtils.getStringParameter(request, "trackcodeFieldDropDown", "").trim());
		}

		// registered By
		if (request.getParameter("registeredBy") != null) {
			customerSearch.setRegisteredBy(ServletRequestUtils.getStringParameter(request, "registeredBy", "").trim());
		}

		// search customers with cart not empty
		if (request.getParameter("cartNotEmpty") != null) {
			String cartNotEmpty = ServletRequestUtils.getStringParameter(request, "cartNotEmpty", "");
			if (cartNotEmpty.equals("true")) {
				customerSearch.setCartNotEmpty(true);
				// Shopping Cart Total
				if (request.getParameter("shoppingCartTotal") != null) {
					customerSearch.setShoppingCartTotal(ServletRequestUtils.getDoubleParameter(request, "shoppingCartTotal", 0.0));
				}
			} else if (cartNotEmpty.equals("false")) {
				customerSearch.setCartNotEmpty(false);
			} else {
				customerSearch.setCartNotEmpty(null);
			}
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				customerSearch.setPage(1);
			} else {
				customerSearch.setPage(page);
			}
		}

		// page size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				customerSearch.setPageSize(10);
			} else {
				customerSearch.setPageSize(size);
			}
		}

		// host for multi store
		if (request.getParameter("host") != null) {
			customerSearch.setHost(ServletRequestUtils.getStringParameter(request, "host", ""));
		}

		// taxId
		if (request.getParameter("taxId") != null) {
			customerSearch.setTaxId(ServletRequestUtils.getStringParameter(request, "taxId", ""));
		}

		// Order Total
		if (request.getParameter("totalOrder") != null) {
			customerSearch.setTotalOrder((ServletRequestUtils.getDoubleParameter(request, "totalOrder", 0.0) == 0.0) ? null : ServletRequestUtils.getDoubleParameter(request, "totalOrder", 0.0));
			customerSearch.setTotalOrderOperator(ServletRequestUtils.getIntParameter(request, "totalOrderOperator", 0));
		}

		// numOrder
		if (request.getParameter("numOrder") != null && !request.getParameter("numOrder").isEmpty()) {
			customerSearch.setNumOrder(ServletRequestUtils.getIntParameter(request, "numOrder", 0));
			customerSearch.setNumOrderOperator(ServletRequestUtils.getIntParameter(request, "numOrderOperator", 0));
		} else {
			customerSearch.setNumOrder(null);
		}

		// LoggenIdFlag
		if (request.getParameter("loggedInFlag") != null) {
			customerSearch.setLoggedInFlag(ServletRequestUtils.getStringParameter(request, "loggedInFlag", ""));
		}

		return customerSearch;
		
	}

	public CrmContactSearch getCrmContactSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		CrmContactSearch crmContactSearch = (CrmContactSearch) request.getSession().getAttribute("crmContactSearch");
		if (crmContactSearch == null) {
			crmContactSearch = new CrmContactSearch();
			if (request.getAttribute("accessUser") != null) {
				AccessUser accessUser = (AccessUser) request.getAttribute("accessUser");
				crmContactSearch.setAccessUserId(accessUser.getId());
			}
			crmContactSearch.setPageSize(Integer.parseInt(siteConfig.get("DEFAULT_LIST_SIZE").getValue()));
			request.getSession().setAttribute("crmContactSearch", crmContactSearch);
		}

		// sorting
		if (request.getParameter("sort") != null) {
			crmContactSearch.setSort(ServletRequestUtils.getStringParameter(request, "sort", "id"));
		}

		// account id
		if (request.getParameter("crmAccountId") != null) {
			crmContactSearch.setAccountId(ServletRequestUtils.getIntParameter(request, "crmAccountId", -1));
			crmContactSearch.setAccountName(getCrmAccountNameById(ServletRequestUtils.getIntParameter(request, "crmAccountId", -1)));
		}

		// account name
		if (request.getParameter("crmAccountName") != null) {
			crmContactSearch.setAccountName(ServletRequestUtils.getStringParameter(request, "crmAccountName", ""));
			crmContactSearch.setAccountId(null);
		}

		// converted to customer
		if (request.getParameter("convertedCustomer") != null) {
			crmContactSearch.setConvertedCustomer(ServletRequestUtils.getIntParameter(request, "convertedCustomer", 0));
		}

		// contact name
		if (request.getParameter("crmContactName") != null) {
			crmContactSearch.setContactName(ServletRequestUtils.getStringParameter(request, "crmContactName", "").trim());
		}

		// zipcode
		if (request.getParameter("zipcode") != null) {
			crmContactSearch.setZipcode(ServletRequestUtils.getStringParameter(request, "zipcode", "").trim());
		}
		
		// city
		if (request.getParameter("city") != null) {
			crmContactSearch.setCity(ServletRequestUtils.getStringParameter(request, "city", "").trim());
		}

		// state
		if (request.getParameter("state") != null) {
			crmContactSearch.setState(ServletRequestUtils.getStringParameter(request, "state", "").trim());
		}

		// country
		if (request.getParameter("country") != null) {
			crmContactSearch.setCountry(ServletRequestUtils.getStringParameter(request, "country", "").trim());
		}

		// email
		if (request.getParameter("email") != null) {
			crmContactSearch.setEmail(ServletRequestUtils.getStringParameter(request, "email", "").trim());
		}
		
		// qualifier
		if (request.getParameter("qualifier") != null) {
			crmContactSearch.setQualifierId(ServletRequestUtils.getIntParameter(request, "qualifier", -1));
		}

		// group ID
		if (request.getParameter("groupId") != null) {
			int groupId = ServletRequestUtils.getIntParameter(request, "groupId", -1);
			if (groupId == -1) {
				crmContactSearch.setGroupId(null);
			} else {
				crmContactSearch.setGroupId(groupId);
			}
		}

		// customer group ID
		if (request.getParameter("customerGroupId") != null) {
			int customerGroupId = ServletRequestUtils.getIntParameter(request, "customerGroupId", -1);
			if (customerGroupId == -1) {
				crmContactSearch.setCustomerGroupId(null);
			} else {
				crmContactSearch.setCustomerGroupId(customerGroupId);
			}
		}
		// access user
		if (request.getParameter("accessUserId") != null) {
			int accessUserId = ServletRequestUtils.getIntParameter(request, "accessUserId", -1);
			if (accessUserId == -1) {
				crmContactSearch.setAccessUserId(null);
			} else {
				crmContactSearch.setAccessUserId(accessUserId);
			}
		}

		// trackcode
		if (request.getParameter("trackcode") != null) {
			crmContactSearch.setTrackcode(ServletRequestUtils.getStringParameter(request, "trackcode", "").trim());
		}

		// leadSource
		if (request.getParameter("leadSource") != null) {
			crmContactSearch.setLeadSource(ServletRequestUtils.getStringParameter(request, "leadSource", "").trim());
		}
//		Viatrading don't use it 
//		// rating
//		if (request.getParameter("rating") != null) {
//			crmContactSearch.setRating(ServletRequestUtils.getStringParameter(request, "rating", ""));
//		}
		
		// rating1
		if (request.getParameter("rating1") != null) {
			crmContactSearch.setRating1(ServletRequestUtils.getStringParameter(request, "rating1", ""));
		}
		
		// ratomg2
		if (request.getParameter("rating2") != null) {
			crmContactSearch.setRating2(ServletRequestUtils.getStringParameter(request, "rating2", ""));
		}
		

		// contact field Id
		if (request.getParameter("contactFieldId") != null) {
			crmContactSearch.setContactFieldId(ServletRequestUtils.getIntParameter(request, "contactFieldId", -1));
		}

		// contact field value
		if (request.getParameter("contactFieldValue") != null) {
			crmContactSearch.setContactFieldValue(ServletRequestUtils.getStringParameter(request, "contactFieldValue", ""));
		}

		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				crmContactSearch.setStartDate(df.parse(request.getParameter("startDate")));
			} catch (ParseException e) {
				crmContactSearch.setStartDate(null);
			}
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				crmContactSearch.setEndDate(df.parse(request.getParameter("endDate")));
			} catch (ParseException e) {
				crmContactSearch.setEndDate(null);
			}
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				crmContactSearch.setPage(1);
			} else {
				crmContactSearch.setPage(page);
			}
		}

		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				crmContactSearch.setPageSize(10);
			} else {
				crmContactSearch.setPageSize(size);
			}
		}
		// task Description
		if (request.getParameter("taskDescription") != null) {
			crmContactSearch.setTaskDescription(ServletRequestUtils.getStringParameter(request, "taskDescription", "").trim());
		}
		
		// task Description
		if (request.getParameter("languageField") != null) {
			crmContactSearch.setLanguage(ServletRequestUtils.getStringParameter(request, "languageField", "").trim());
		}

		return crmContactSearch;
	}

	public CrmTaskSearch getCrmTaskSearch(HttpServletRequest request) {
		Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
		CrmTaskSearch crmTaskSearch = (CrmTaskSearch) request.getSession().getAttribute("crmTaskSearch");
		if (crmTaskSearch == null) {
			crmTaskSearch = new CrmTaskSearch();
			if (request.getAttribute("accessUser") != null) {
				AccessUser accessUser = (AccessUser) request.getAttribute("accessUser");
				crmTaskSearch.setAssignedToId(accessUser.getId().toString());
			}
			crmTaskSearch.setPageSize(Integer.parseInt(siteConfig.get("DEFAULT_LIST_SIZE").getValue()));
			request.getSession().setAttribute("crmTaskSearch", crmTaskSearch);
		}

		// sorting
		if (request.getParameter("sort") != null) {
			crmTaskSearch.setSort(ServletRequestUtils.getStringParameter(request, "sort", "id"));
		}

		// account Id
		if (request.getParameter("crmAccountId") != null) {
			int accountId = ServletRequestUtils.getIntParameter(request, "crmAccountId", -1);
			if (accountId == -1) {
				crmTaskSearch.setAccountId(null);
			} else {
				crmTaskSearch.setAccountId(accountId);
			}
		}

		// account name
		if (request.getParameter("crmAccountName") != null) {
			crmTaskSearch.setAccountName(ServletRequestUtils.getStringParameter(request, "crmAccountName", ""));
			List<CrmAccount> crmAccounts = this.getCrmAccountAjax(ServletRequestUtils.getStringParameter(request, "crmAccountName", ""));
			// clear old ids before saving new ids
			crmTaskSearch.getAccountIds().clear();
			for (CrmAccount crmAccount : crmAccounts) {
				crmTaskSearch.getAccountIds().add(crmAccount.getId());
			}
			crmTaskSearch.setAccountId(null);
		}

		// title
		if (request.getParameter("crmTitle") != null) {
			crmTaskSearch.setTitle(ServletRequestUtils.getStringParameter(request, "crmTitle", ""));
		}

		// contact Id
		crmTaskSearch.setContactId(ServletRequestUtils.getIntParameter(request, "contactId", -1));

		// contact name
		if (request.getParameter("crmContactName") != null) {
			crmTaskSearch.setContactName(ServletRequestUtils.getStringParameter(request, "crmContactName", ""));
		}

		// assignedTo Id
		if (request.getParameter("crmAssignedTo") != null) {
			crmTaskSearch.setAssignedToId(ServletRequestUtils.getStringParameter(request, "crmAssignedTo", ""));
		}

		// status
		if (request.getParameter("crmStatus") != null) {
			crmTaskSearch.setStatus(ServletRequestUtils.getStringParameter(request, "crmStatus", ""));
		}

		// description
		if (request.getParameter("crmDescription") != null) {
			crmTaskSearch.setDescription(ServletRequestUtils.getStringParameter(request, "crmDescription", ""));
		}

		// action
		if (request.getParameter("crmAction") != null) {
			crmTaskSearch.setAction(ServletRequestUtils.getStringParameter(request, "crmAction", ""));
		}

		// priority
		if (request.getParameter("crmPriority") != null) {
			crmTaskSearch.setPriority(ServletRequestUtils.getStringParameter(request, "crmPriority", ""));
		}

		// type
		if (request.getParameter("crmType") != null) {
			crmTaskSearch.setType(ServletRequestUtils.getStringParameter(request, "crmType", ""));
		}

		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy", RequestContextUtils.getLocale(request));
		// startDate
		if (request.getParameter("startDate") != null) {
			try {
				crmTaskSearch.setStartDate(df.parse(request.getParameter("startDate")));
			} catch (ParseException e) {
				crmTaskSearch.setStartDate(null);
			}
		}

		// endDate
		if (request.getParameter("endDate") != null) {
			try {
				crmTaskSearch.setEndDate(df.parse(request.getParameter("endDate")));
			} catch (ParseException e) {
				crmTaskSearch.setEndDate(null);
			}
		}
		// startEndDate
		if (request.getParameter("startDueDate") != null) {
			try {
				crmTaskSearch.setStartDueDate(df.parse(request.getParameter("startDueDate")));
			} catch (ParseException e) {
				crmTaskSearch.setStartDueDate(null);
			}
		}

		// endDueDate
		if (request.getParameter("endDueDate") != null) {
			try {
				crmTaskSearch.setEndDueDate(df.parse(request.getParameter("endDueDate")));
			} catch (ParseException e) {
				crmTaskSearch.setEndDueDate(null);
			}
		}
		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				crmTaskSearch.setPage(1);
			} else {
				crmTaskSearch.setPage(page);
			}
		}

		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				crmTaskSearch.setPageSize(10);
			} else {
				crmTaskSearch.setPageSize(size);
			}
		}

		return crmTaskSearch;
	}

	// eBillme
	public void updateEbillmePayment(EBillme eBillme, OrderStatus orderStatus) {
		this.orderDao.updateEbillmePayment(eBillme, orderStatus);
	}

	public void updateEbillmePayment(EBillme eBillme) {
		this.orderDao.updateEbillmePayment(eBillme);
	}

	// CRM Account
	public Integer getCrmAccountListCount(CrmAccountSearch search) {
		return this.crmDao.getCrmAccountListCount(search);
	}

	// CRM Account
	public Integer getCrmQualifierListCount(CrmQualifierSearch search) {
		return this.crmDao.getCrmQualifierListCount(search);
	}
	
	public List<CrmAccount> getCrmAccountList(CrmAccountSearch search) {
		return this.crmDao.getCrmAccountList(search);
	}
	
	public List<CrmQualifier> getCrmQualifierList(CrmQualifierSearch search) {
		return this.crmDao.getCrmQualifierList(search);
	}

	public List<CrmAccount> getCrmAccountNameList() {
		return this.crmDao.getCrmAccountNameList();
	}

	public CrmAccount getCrmAccountById(Integer accountId) {
		return this.crmDao.getCrmAccountById(accountId);
	}

	public CrmAccount getCrmAccountByName(String accountName) {
		return this.crmDao.getCrmAccountByName(accountName);
	}

	public String getCrmAccountNameById(Integer accountId) {
		return this.crmDao.getCrmAccountNameById(accountId);
	}

	public void deleteCrmAccount(Integer accountId) {
		this.crmDao.deleteCrmAccount(accountId);
	}

	public void insertCrmAccount(CrmAccount crmAccount) {
		this.crmDao.insertCrmAccount(crmAccount);
	}
	
	public void insertCrmQualifier(CrmQualifier crmQualifier) {
		this.crmDao.insertCrmQualifier(crmQualifier);
	}
	
	public void deleteCrmQualifier(Integer qualifierId) {
		this.crmDao.deleteCrmQualifier(qualifierId);	
	}
	
	public void updateCrmQualifier(CrmQualifier crmQualifier) {
		this.crmDao.updateCrmQualifier(crmQualifier);	
	}

	public void updateCrmAccount(CrmAccount crmAccount) {
		this.crmDao.updateCrmAccount(crmAccount);
	}

	public boolean isCrmAccountExist(String accountName) {
		return this.crmDao.isCrmAccountExist(accountName);
	}

	public Integer getCrmAccountId(String accountName, String accountNumber) {
		return this.crmDao.getCrmAccountId(accountName, accountNumber);
	}

	public void importCrmAccounts(List<CrmAccount> crmAccounts) {
		this.crmDao.importCrmAccounts(crmAccounts);
	}

	public List<CrmAccount> getCrmAccountAjax(String accountName) {
		return this.crmDao.getCrmAccountAjax(accountName);
	}

	// CRM Contact
	public Integer getCrmContactListCount(CrmContactSearch search) {
		return this.crmDao.getCrmContactListCount(search);
	}

	public List<Integer> getCrmContactIdsList(CrmContactSearch search) {
		return this.crmDao.getCrmContactIdsList(search);
	}

	public List<CrmContact> getCrmContactList(CrmContactSearch search) {
		return this.crmDao.getCrmContactList(search);
	}
	
	public List<CrmContact> getCrmContactList2(CrmContactSearch search) {
		return this.crmDao.getCrmContactList2(search);
	}

	public List<CrmContact> getCrmContactExport(CrmContactSearch search) {
		return this.crmDao.getCrmContactExport(search);
	}

	public CrmContact getCrmContactById(Integer contactId) {
		return this.crmDao.getCrmContactById(contactId);
	}
	
	public CrmQualifier getCrmQualifierById(Integer qualifierId) {
		return this.crmDao.getCrmQualifierById(qualifierId);
	}

	public void deleteCrmContact(Integer contactId) {
		this.crmDao.deleteCrmContact(contactId);
	}

	public void insertCrmContact(CrmContact crmContact) {
		this.crmDao.insertCrmContact(crmContact);
	}

	public void updateCrmContact(CrmContact crmContact) {
		this.crmDao.updateCrmContact(crmContact);
	}

	public List<CrmContact> getCrmContactByEmail1(String email1) {
		return this.crmDao.getCrmContactByEmail1(email1);
	}

	public void importCrmContacts(List<CrmContact> crmContacts) {
		for (CrmContact contact : crmContacts) {
			if (contact.getId() == null) {
				// new contact
				insertCrmContact(contact);
			} else {
				// old contact
				updateCrmContact(contact);
			}

			// synch to customer
			if (contact.getUserId() != null) {
				Customer customer = this.getCustomerById(contact.getUserId());
				if (customer != null) {
					try {
						this.crmContactToCustomerSynch(contact, customer, false);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				this.updateCustomer(customer, false, false);
			}
		}
	}

	public void insertCrmContactField(CrmContactField crmContactField) {
		this.crmDao.insertCrmContactField(crmContactField);
	}

	public List<CrmContactField> getCrmContactFieldList(Search search) {
		return this.crmDao.getCrmContactFieldList(search);
	}

	public CrmContactField getCrmContactFieldById(Integer fieldId) {
		return this.crmDao.getCrmContactFieldById(fieldId);
	}

	public CrmContactField getCrmContactFieldByGlobalName(String fieldName) {
		return this.crmDao.getCrmContactFieldByGlobalName(fieldName);
	}

	public void updateCrmContactField(CrmContactField crmContactField) {
		this.crmDao.updateCrmContactField(crmContactField);
	}

	public void deleteCrmContactField(Integer fieldId) {
		this.crmDao.deleteCrmContactField(fieldId);
	}

	public String createToken(Integer contactId) {
		return this.crmDao.createToken(contactId);
	}

	public void convertCrmContactToCustomer(Integer contactId, Integer userId, String leadSource) {
		this.crmDao.convertCrmContactToCustomer(contactId, userId, leadSource);
	}

	public Map<String, String> getContcatCustomerMap(String key) {
		return this.crmDao.getContcatCustomerMap(key);
	}

	public void updateCustomerMap(List<Configuration> data) {
		this.crmDao.updateCustomerMap(data);
	}

	public void unsubscribeByCrmContactId(Integer crmContactId) {
		this.crmDao.unsubscribeByCrmContactId(crmContactId);
	}

	public Integer getFieldValueCountByFieldIdAndValue(Integer fieldId, String fieldValue) {
		return this.crmDao.getFieldValueCountByFieldIdAndValue(fieldId, fieldValue);
	}

	public boolean validCrmContactToken(String token) {
		return this.crmDao.validCrmContactToken(token);
	}

	public void batchAssignUserToContacts(List<Integer> contactIds, AccessUser accessUser) {
		this.crmDao.batchAssignUserToContacts(contactIds, accessUser);
	}
	
	public void batchAssignUserToCustomers(List<Integer> userIds, AccessUser accessUser) {
		this.customerDao.batchAssignUserToCustomers(userIds, accessUser);
		
	}
	
	public void removeUserFromContacts(List<Integer> contactIds){
		this.crmDao.removeUserFromContacts(contactIds);
	}

	public void batchCrmContactDelete(Set<Integer> contactIds) {
		this.crmDao.batchCrmContactDelete(contactIds);
	}

	public void batchCrmRating(List<Integer> contactIds, String rating) {
		this.crmDao.batchCrmRating(contactIds, rating);
	}
	
	public void batchCrmRating1(List<Integer> contactIds, String rating) {
		this.crmDao.batchCrmRating1(contactIds, rating);
	}
	
	public void batchCrmRating2(List<Integer> contactIds, String rating) {
		this.crmDao.batchCrmRating2(contactIds, rating);
	}
	
	public void batchCrmQualifier(List<Integer> contactIds, String qualifier) {
		this.crmDao.batchCrmQualifier(contactIds, qualifier);
	}
	
	public void batchCrmLanguage(List<Integer> contactIds, String language) {
		this.crmDao.batchCrmLanguage(contactIds, language);
	}
	
	public void batchCustomerRating1(List<Integer> contactIds, String rating) {
		this.customerDao.batchCustomerRating1(contactIds, rating);
	}
	
	public void batchCustomerRating2(List<Integer> contactIds, String rating) {
		this.customerDao.batchCustomerRating2(contactIds, rating);
	}
	
	public void batchCustomerQualifier(List<Integer> contactIds, String qualifier) {
		this.customerDao.batchCustomerQualifier(contactIds, qualifier);
	}
	
	public void batchCustomerLanguage(List<Integer> contactIds, String language) {
		this.customerDao.batchCustomerLanguage(contactIds, language);
	}

	// CRM Group
	public List<CrmContactGroup> getContactGroupList(CrmContactGroupSearch search) {
		return this.crmDao.getContactGroupList(search);
	}

	public List<CrmContactGroup> getContactGroupNameList() {
		return this.crmDao.getContactGroupNameList();
	}

	public CrmContactGroup getContactGroupById(Integer groupId) {
		return this.crmDao.getContactGroupById(groupId);
	}

	public void deleteCrmContactGroupById(Integer groupId) {
		this.crmDao.deleteCrmContactGroupById(groupId);
	}

	public void insertCrmContactGroup(CrmContactGroup group) throws DataAccessException {
		this.crmDao.insertCrmContactGroup(group);
	}

	public void updateCrmContactGroup(CrmContactGroup group) {
		this.crmDao.updateCrmContactGroup(group);
	}

	public List<Integer> getContactGroupIdList(Integer contactId) {
		return this.crmDao.getContactGroupIdList(contactId);
	}

	public void batchCrmContactGroupIds(Integer contactId, Set groupIds, String action) {
		this.crmDao.batchCrmContactGroupIds(contactId, groupIds, action);
	}

	// CRM Task
	public Integer getCrmTaskListCount(CrmTaskSearch search) {
		return this.crmDao.getCrmTaskListCount(search);
	}

	public List<CrmTask> getCrmTaskList(CrmTaskSearch search) {
		return this.crmDao.getCrmTaskList(search);
	}

	public CrmTask getCrmTaskById(Integer taskId) {
		return this.crmDao.getCrmTaskById(taskId);
	}

	public void insertCrmTask(CrmTask crmTask) {
		this.crmDao.insertCrmTask(crmTask);
	}

	public void updateCrmTask(CrmTask crmTask) {
		this.crmDao.updateCrmTask(crmTask);
	}

	public List<CrmTask> getCrmTaskExport(CrmTaskSearch search) {
		return this.crmDao.getCrmTaskExport(search);
	}

	public void deleteCrmTask(Integer taskId) {
		this.crmDao.deleteCrmTask(taskId);
	}

	public void batchCrmTaskDelete(Set<Integer> taskIds) {
		this.crmDao.batchCrmTaskDelete(taskIds);
	}

	// CRM Form
	public List<CrmForm> getCrmFormList(Search search) {
		return this.crmDao.getCrmFormList(search);
	}

	public CrmForm getCrmFormById(Integer formId) {
		return this.crmDao.getCrmFormById(formId);
	}

	public CrmForm getCrmFormByFormNumber(String formNumber) {
		return this.crmDao.getCrmFormByFormNumber(formNumber);
	}

	public void deleteCrmForm(Integer formId) {
		this.crmDao.deleteCrmForm(formId);
	}

	public void insertCrmForm(CrmForm crmForm) {
		this.crmDao.insertCrmForm(crmForm);
	}

	public void updateCrmForm(CrmForm crmForm) {
		this.crmDao.updateCrmForm(crmForm);
	}
	
	public int getCrmContactId(String email) {
		return this.crmDao.getCrmContactId(email);
	}

	public String createForm(CrmFormBuilderForm crmFormBuilderForm, HttpServletRequest request) {

		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		String actionUrl = siteConfig.get("SITE_URL").getValue() + "formProcessor.jhtm";
		if (crmFormBuilderForm.isNewCrmForm()) {
			String formNumber = null;
			CrmForm crmForm = null;
			do {
				formNumber = UUID.randomUUID().toString();
				crmForm = getCrmFormByFormNumber(formNumber);
			} while (crmForm != null);

			crmFormBuilderForm.getCrmForm().setFormNumber(formNumber);
		}
		StringBuffer form = new StringBuffer();

		form.append(crmFormBuilderForm.getCrmForm().getHeader() + "\n");
		form.append("<form method=\"post\" action=" + actionUrl + " enctype=\"multipart/form-data\" accept-charset=\"UTF-8\">\n");
		form.append("<table class=\"fieldTable\">\n");
		int index = 0;
		for (int j = 0; j < crmFormBuilderForm.getCrmForm().getNumberOfFields(); j++) {
			CrmFormField crmField = crmFormBuilderForm.getCrmForm().getCrmFields().get(j);
			form.append("<tr class=\"fieldRow\">\n");
			form.append("<td class=\"fieldName\">" + crmField.getFieldName() + (crmField.isRequired() ? "*" : "") + "</td>\n");

			switch (crmField.getContactFieldId()) {
			case 1010:
				this.getFormFieldHtmlCode("crmContact.firstName", form, index, crmField);
				break;
			case 1020:
				this.getFormFieldHtmlCode("crmContact.lastName", form, index, crmField);
				break;
			case 1030:
				this.getFormFieldHtmlCode("crmContact.email1", form, index, crmField);
				break;
			case 1040:
				this.getFormFieldHtmlCode("crmContact.email2", form, index, crmField);
				break;
			case 1050:
				this.getFormFieldHtmlCode("crmContact.phone1", form, index, crmField);
				break;
			case 1060:
				this.getFormFieldHtmlCode("crmContact.phone2", form, index, crmField);
				break;
			case 1070:
				this.getFormFieldHtmlCode("crmContact.fax", form, index, crmField);
				break;
			case 1080:
				this.getFormFieldHtmlCode("crmContact.street", form, index, crmField);
				break;
			case 1090:
				this.getFormFieldHtmlCode("crmContact.city", form, index, crmField);
				break;
			case 1100:
				this.getFormFieldHtmlCode("crmContact.stateProvince", form, index, crmField);
				break;
			case 1110:
				this.getFormFieldHtmlCode("crmContact.zip", form, index, crmField);
				break;
			case 1120:
				this.getFormFieldHtmlCode("crmContact.country", form, index, crmField);
				break;
			case 1130:
				this.getFormFieldHtmlCode("crmContact.description", form, index, crmField);
				break;
			case 1140:
				this.getFormFieldHtmlCode("crmContact.addressType", form, index, crmField);
				break;
			case 1150:
				this.getFormFieldHtmlCode("attachment", form, index, crmField);
				break;
			case 1160:
				this.getFormFieldHtmlCode("crmContact.language", form, index, crmField);
				break;
			default:
				this.getFormFieldHtmlCode("crmContact.crmContactFields[" + index + "].fieldValue", form, index, crmField);
				form.append("<input type=\"hidden\" name=\"field" + index + "\" value=\"" + crmField.getContactFieldId() + "\">\n");
				index++;
				break;
			}
			form.append("</tr>\n");
		}
		form.append("</table>\n");
		form.append("<input type=\"hidden\" name=\"formNumber\" value=\"" + crmFormBuilderForm.getCrmForm().getFormNumber() + "\">\n");
		form.append("<input type=\"hidden\" name=\"returnUrl\" value=\"" + crmFormBuilderForm.getCrmForm().getReturnUrl() + "\">\n");
		form.append("<input type=\"hidden\" name=\"crmContact.trackcode\" value=\"\">\n");
		form.append("<input type=\"hidden\" name=\"crmContact.leadSource\" value=\"form\">\n");
		form.append("<input type=\"hidden\" name=\"adminEmailSubject\" value=\"\">\n");
		form.append("<input type=\"hidden\" name=\"groupId\" value=\"\">\n");
		form.append("<input type=\"hidden\" name=\"accountId\" value=\"1\">\n");
		form.append("<input type=\"hidden\" name=\"updateIfExist\" value=\"false\">\n");
		form.append("<div class=\"fieldSubmit\"><input type=\"submit\" value=\"" + crmFormBuilderForm.getCrmForm().getSendButton() + "\"></div></br>\n");
		form.append("</form></br>\n");
		form.append(crmFormBuilderForm.getCrmForm().getFooter());

		return form.toString();
	}

	public void getFormFieldHtmlCode(String fieldName, StringBuffer form, int index, CrmFormField crmField) {
		if (crmField.getType() == 1 || crmField.getType() == 7) {

			form.append("<td class=\"fieldValueText\">\n<input type=\"text\" name=\"" + fieldName + "\"></td>\n");

		} else if (crmField.getType() == 2) {

			form.append("<td class=\"fieldValueTextArea\">\n<textarea name=\"" + fieldName + "\"></textarea></td>\n");

		} else if (crmField.getType() == 3) {

			String[] options = crmField.getOptions().split("[,]");
			form.append("<td class=\"fieldValueCheckBox\">\n");
			for (int i = 0; i < options.length; i++) {
				form.append("<input type=\"checkbox\" name=\"" + fieldName + "\" value=\"" + options[i].trim() + "\">" + options[i].trim() + "</br>\n");
			}
			form.append("</td>\n");

		} else if (crmField.getType() == 4) {

			String[] options = crmField.getOptions().split("[,]");
			form.append("<td class=\"fieldValueRadio\">\n");
			for (int i = 0; i < options.length; i++) {
				form.append("<input type=\"radio\" name=\"" + fieldName + "\" value=\"" + options[i].trim() + "\">" + options[i].trim() + "</br>\n");
			}
			form.append("</td>\n");

		} else if (crmField.getType() == 5) {

			String[] options = crmField.getOptions().split("[,]");
			form.append("<td class=\"fieldValueDropDown\">\n<select name=\"" + fieldName + "\">\n");
			form.append("<option value=\"\">" + "Please Select" + "</option></br>\n");
			for (int i = 0; i < options.length; i++) {
				form.append("<option value=\"" + options[i].trim() + "\">" + options[i].trim() + "</option></br>\n");
			}
			form.append("</select>\n</td>\n");
		} else if (crmField.getType() == 6) {
			// attachement
			form.append("<td class=\"fieldValueText\">\n<input type=\"file\" name=\"" + fieldName + "\"></td>\n");
		}
	}

	public void sendAutoResponse(SiteMessage siteMessage, CrmContact crmContact, JavaMailSenderImpl mailSender, Map<String, Configuration> siteConfig) throws MessagingException {

		MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
		try {

			helper.addTo(crmContact.getEmail1());
			helper.setFrom(siteConfig.get("CONTACT_EMAIL").getValue());

			StringBuffer message = new StringBuffer();

			String firstName = crmContact.getFirstName();
			if (firstName == null) {
				firstName = "";
			}
			String lastname = crmContact.getLastName();
			if (lastname == null) {
				lastname = "";
			}

			// subject
			siteMessage.setSubject(siteMessage.getSubject().replace("#firstname#", firstName));
			siteMessage.setSubject(siteMessage.getSubject().replace("#lastname#", lastname));
			siteMessage.setSubject(siteMessage.getSubject().replace("#email#", crmContact.getEmail1()));
			helper.setSubject(siteMessage.getSubject());

			// message
			siteMessage.setMessage(siteMessage.getMessage().replace("#firstname#", firstName));
			siteMessage.setMessage(siteMessage.getMessage().replace("#lastname#", lastname));
			siteMessage.setMessage(siteMessage.getMessage().replace("#email#", crmContact.getEmail1()));
			
			
	    	if(crmContact!=null && crmContact.getId()!=null){
	    		try{
			    	//message = message.replaceAll("#crmcontactid#", crmContact.getId().toString());
					siteMessage.setMessage(siteMessage.getMessage().replace("#crmcontactid#", crmContact.getId().toString()));
	    		}catch(Exception ex){}
	    	}
			
			message.append(siteMessage.getMessage());
			helper.setText(message.toString(), siteMessage.isHtml());
		} catch (Exception e) {
		}

		if (true) {
			mailSender.send(mms);
		}

	}

	public void crmContactToCustomerSynch(CrmContact crmContact, Customer customer, boolean onlyCustomFields) throws IllegalArgumentException, SecurityException, IllegalAccessException,
			InvocationTargetException, NoSuchMethodException {

		Map<String, String> maps = getContcatCustomerMap("customer");
		Map<Integer, String> contactFields = new HashMap<Integer, String>();
		for (CrmContactField crmContactField : crmContact.getCrmContactFields()) {
			if (crmContactField.getId() <= 20) {
				contactFields.put(crmContactField.getId(), crmContactField.getFieldValue());
			}
		}
		Class c = CrmContact.class;
		String methodName = null;

		customer.setCrmContactId(crmContact.getId());
		customer.setCrmAccountId(crmContact.getAccountId());
		// only if custom fileds is empty
		if (!onlyCustomFields) {
			customer.getAddress().setCompany(crmContact.getAccountName());
			customer.setUnsubscribe(crmContact.isUnsubscribe());
			customer.setSalesRepId(crmContact.getAccessUserId() != null ? getAccessUserById(crmContact.getAccessUserId()).getSalesRepId() : null);

			// First Name
			methodName = maps.get("getAddress_getFirstName");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.getAddress().setFirstName((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.getAddress().setFirstName(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}

			// Last Name
			methodName = maps.get("getAddress_getLastName");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.getAddress().setLastName((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.getAddress().setLastName(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// addr1
			methodName = maps.get("getAddress_getAddr1");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.getAddress().setAddr1((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.getAddress().setAddr1(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// addr2
			methodName = maps.get("getAddress_getAddr2");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.getAddress().setAddr2((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.getAddress().setAddr2(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// city
			methodName = maps.get("getAddress_getCity");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.getAddress().setCity((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.getAddress().setCity(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// stateProvince
			methodName = maps.get("getAddress_getStateProvince");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.getAddress().setStateProvince((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.getAddress().setStateProvince(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// zipCode
			methodName = maps.get("getAddress_getZip");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.getAddress().setZip((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.getAddress().setZip(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// country
			methodName = maps.get("getAddress_getCountry");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.getAddress().setCountry((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.getAddress().setCountry(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// address type
			customer.getAddress().setResidential((crmContact.getAddressType() != null && crmContact.getAddressType().equalsIgnoreCase("Residential")) ? true : false);
			// phone
			methodName = maps.get("getAddress_getPhone");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.getAddress().setPhone((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.getAddress().setPhone(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// cellPhone
			methodName = maps.get("getAddress_getCellPhone");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.getAddress().setCellPhone((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.getAddress().setCellPhone(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// fax
			methodName = maps.get("getAddress_getFax");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.getAddress().setFax((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.getAddress().setFax(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// email to username
			methodName = maps.get("getUsername");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.setUsername((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.setUsername(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// taxId
			methodName = maps.get("getTaxId");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.setTaxId((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.setTaxId(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// note
			methodName = maps.get("getDescription");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.setNote((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.setNote(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			
			if(crmContact!=null && crmContact.getCrmContactFields()!=null && crmContact.getCrmContactFields().get(13)!=null){
				customer.getAddress().setCompany(crmContact.getCrmContactFields().get(13).getFieldValue());
			}
			
			// trackcode
			methodName = maps.get("getTrackcode");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.setTrackcode((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.setTrackcode(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			
			// unsubscribe
			methodName = maps.get("isUnsubscribe");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					if((Integer) c.getMethod(methodName).invoke(crmContact)==1){
						customer.setUnsubscribe(true);
					}else{
						customer.setUnsubscribe(false);
					}
				} 
			}
			
			// qualifier
			methodName = maps.get("getQualifier");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.setQualifier((Integer) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.setQualifier(Integer.parseInt(contactFields.get(Integer.parseInt(methodName.split("_")[1]))));
				}
			}
			
			// language
			methodName = maps.get("getLanguageCode");
			if (methodName != null && !methodName.equals("")) {
				String langCode = crmContact.getLanguage();
				if (!methodName.startsWith("getCrmContactFields")) {
					if(langCode!=null){
						if(langCode.equals("en")) {
							customer.setLanguageCode(LanguageCode.en);
						} else if(langCode.equals("es")) {
							customer.setLanguageCode(LanguageCode.es);
						} else if(langCode.equals("fr")) {
							customer.setLanguageCode(LanguageCode.fr);
						} else if(langCode.equals("ar")) {
							customer.setLanguageCode(LanguageCode.ar);
						} else if(langCode.equals("de")) {
							customer.setLanguageCode(LanguageCode.de);
						} else{
							customer.setLanguageCode(LanguageCode.en);
						}
					}
					else{
						customer.setLanguageCode(LanguageCode.en);
					}
				} 
			}
			
			// Registered By
			methodName = maps.get("getRegisteredBy");
			if (methodName != null && !methodName.equals("")) {
				String leadSource = null;
				if (!methodName.startsWith("getCrmContactFields")) {
					leadSource = (String) c.getMethod(methodName).invoke(crmContact);
					if (leadSource != null && !leadSource.isEmpty()) {
						customer.setRegisteredBy(leadSource);
					}
				} else {
					leadSource = contactFields.get(Integer.parseInt(methodName.split("_")[1]));
					if (leadSource != null && !leadSource.isEmpty()) {
						customer.setRegisteredBy(leadSource);
					}
				}
			}
			// getRating1
			methodName = maps.get("getRating1");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.setRating1((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.setRating1(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// getRating2
			methodName = maps.get("getRating2");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.setRating2((String) c.getMethod(methodName).invoke(crmContact));
				} else {
					customer.setRating2(contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
			// isDoNotCall
			methodName = maps.get("isDoNotCall");
			if (methodName != null && !methodName.equals("")) {
				if (!methodName.startsWith("getCrmContactFields")) {
					customer.setDoNotCall((Boolean) c.getMethod(methodName).invoke(crmContact));
				}
			}
		}
		// test
		Class c2 = Customer.class;
		Method m2 = null;
		Class[] arglist = new Class[1];
		arglist[0] = String.class;

		// field1 to field20
		for (int i = 1; i <= 20; i++) {
			methodName = maps.get("getField" + i);
			if (methodName != null && !methodName.equals("")) {
				m2 = c2.getMethod("setField" + i, arglist);
				if (!methodName.startsWith("getCrmContactFields")) {
					m2.invoke(customer, (String) c.getMethod(methodName).invoke(crmContact));
				} else {
					m2.invoke(customer, contactFields.get(Integer.parseInt(methodName.split("_")[1])));
				}
			}
		}
	}

	public void customerToCrmContactSynch(CrmContact crmContact, Customer customer) throws Exception {
		// user id
		crmContact.setUserId(customer.getId());
		// unsubscribe
		crmContact.setUnsubscribe(customer.isUnsubscribe());
		// sales rep
		if (customer.getSalesRepId() != null) {
			AccessUser user = getAccessUserBySalesRepId(customer.getSalesRepId());
			crmContact.setAccessUserId(user != null ? user.getId() : null);
		} else {
			crmContact.setAccessUserId(null);
		}

		Map<String, String> maps = getContcatCustomerMap("contact");
		crmContact.setUserId(customer.getId());
		Class c = Customer.class;
		String methodName = null;
		// First Name
		methodName = maps.get("getFirstName");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setFirstName((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setFirstName((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// Last Name
		methodName = maps.get("getLastName");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setLastName((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setLastName((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// street
		methodName = maps.get("getStreet");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setStreet((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setStreet((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// other street
		methodName = maps.get("getOtherStreet");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setOtherStreet((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setOtherStreet((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// city
		methodName = maps.get("getCity");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setCity((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setCity((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// other city
		methodName = maps.get("getOtherCity");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setOtherCity((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setOtherCity((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// stateProvince
		methodName = maps.get("getStateProvince");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setStateProvince((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setStateProvince((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// other stateProvince
		methodName = maps.get("getOtherStateProvince");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setOtherStateProvince((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setOtherStateProvince((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// zip
		methodName = maps.get("getZip");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setZip((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setZip((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// other zip
		methodName = maps.get("getOtherZip");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setOtherZip((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setOtherZip((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// country
		methodName = maps.get("getCountry");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setCountry((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setCountry((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// other country
		methodName = maps.get("getOtherCountry");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setOtherCountry((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setOtherCountry((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// address type
		crmContact.setAddressType((customer.getAddress().isResidential() ? "Residential" : "Commercial"));
		// phone1
		methodName = maps.get("getPhone1");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setPhone1((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setPhone1((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// phone2
		methodName = maps.get("getPhone2");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setPhone2((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setPhone2((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// fax
		methodName = maps.get("getFax");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setFax((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setFax((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// email1
		methodName = maps.get("getEmail1");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setEmail1((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setEmail1((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// note
		methodName = maps.get("getDescription");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setDescription((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setDescription((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// trackcode
		methodName = maps.get("getTrackcode");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setTrackcode((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setTrackcode((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// LeadSource
		methodName = maps.get("getLeadSource");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setLeadSource((String) (customer.getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setLeadSource((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// Qualifier
		methodName = maps.get("getQualifier");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setQualifier((Integer) (customer.getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setQualifier((Integer) c.getMethod(methodName).invoke(customer));
			}
		}
		// Language
		methodName = maps.get("getLanguage");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setLanguage((String) (customer.getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				try{
					Language.LanguageCode lgcode = (Language.LanguageCode)c.getMethod(methodName).invoke(customer);
					crmContact.setLanguage(lgcode.name());
				}catch(Exception ex){	
				}
			}
		}
		// rating1
		methodName = maps.get("getRating1");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setRating1((String)(customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setRating1((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// rating2
		methodName = maps.get("getRating2");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setRating2((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setRating2((String) c.getMethod(methodName).invoke(customer));
			}
		}
		// isDoNotCall
		methodName = maps.get("isDoNotCall");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setDoNotCall((Boolean) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setDoNotCall((Boolean) c.getMethod(methodName).invoke(customer));
			}
		}
		
		// isUnsubscribe
		methodName = maps.get("isUnsubscribe");
		if (methodName != null && !methodName.equals("")) {
			if (!methodName.startsWith("getAddress")) {
				if((Boolean) c.getMethod(methodName).invoke(customer)){
					crmContact.setUnsubscribe(true);
				}else{
					crmContact.setUnsubscribe(false);
				}
			}
		}
		
		methodName = maps.get("getNote");
		if (methodName != null && !methodName.equals("")) {
			if (methodName.startsWith("getAddress")) {
				crmContact.setDescription((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
			} else {
				crmContact.setDescription((String) c.getMethod(methodName).invoke(customer));
			}
		}
		
		List<CrmContactField> tmp = getCrmContactFieldList(null);
		
		try{
			if(customer!=null && customer.getAddress()!=null && tmp!=null && tmp.get(20)!=null){
				tmp.get(20).setFieldValue(customer.getAddress().getCompany());
			}
		}catch(Exception ex){
		}
		
		// fields
		crmContact.setCrmContactFields(tmp);
		
		for (int i = 1; i <= crmContact.getCrmContactFields().size(); i++) {
			methodName = maps.get("getCrmContactFields_" + i);
			if (methodName != null && !methodName.equals("")) {
				if (methodName.startsWith("getAddress")) {
					crmContact.getCrmContactFields().get(i - 1).setFieldValue((String) (customer.getAddress().getClass().getMethod(methodName.split("_")[1]).invoke(customer.getAddress())));
				} else {
					crmContact.getCrmContactFields().get(i - 1).setFieldValue((String) c.getMethod(methodName).invoke(customer));
				}
			}
			
			methodName = maps.get("getCrmContactFields_" + i +"Set");
			if (methodName != null && !methodName.equals("")) {
				String fieldValues = "";
				Set<String> setValues = (Set) c.getMethod(methodName).invoke(customer);
				for (Object o: setValues) {
					fieldValues += o.toString() + ",";
				}
				//trim the ending comma
				if (fieldValues.length() > 0) {
					fieldValues = fieldValues.substring(0, fieldValues.length() - 1);
				}
				if (methodName != null && !methodName.equals("")) {
					crmContact.getCrmContactFields().get(i - 1).setFieldValue(fieldValues);
				}
			}
			
		}
	}

	public void newLeadEmail(CrmContact crmContact, String recipientEmail, String recipientCcEmail, String recipientBccEmail, HttpServletRequest request, CrmForm crmForm, File attachment,
			JavaMailSenderImpl mailSender) throws Exception {

		MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		helper.setTo(recipientEmail);
		//System.out.println("---------------------------------newLeadEmail crmForm.getFormId()----------------------------------" + crmForm.getFormId());
		// cc emails
		if (ServletRequestUtils.getBooleanParameter(request, "cc", true) && recipientCcEmail != null && !recipientCcEmail.trim().isEmpty()) {
			for (String ccEmail : recipientCcEmail.split(",")) {
				helper.addCc(ccEmail.trim());
			}
		}
		// bcc emails
		if (ServletRequestUtils.getBooleanParameter(request, "bcc", true) && recipientBccEmail != null && !recipientBccEmail.trim().isEmpty()) {
			for (String bccEmail : recipientBccEmail.split(",")) {
				helper.addBcc(bccEmail.trim());
			}
		}
		

		//System.out.println("-------------------------------------newLeadEmail recipientEmail----------------------------------" + recipientEmail);
		if (siteConfig.get("CONTACT_EMAIL") != null && !siteConfig.get("CONTACT_EMAIL").getValue().equals("")) {
			helper.setFrom(siteConfig.get("CONTACT_EMAIL").getValue());
		} else {
			helper.setFrom("noreply@noreply.com");
		}
		if (ServletRequestUtils.getStringParameter(request, "adminEmailSubject") != null && !ServletRequestUtils.getStringParameter(request, "adminEmailSubject").isEmpty()) {
			helper.setSubject(ServletRequestUtils.getStringParameter(request, "adminEmailSubject"));
		} else {
			helper.setSubject("Inquiry " + (crmContact.getTrackcode() != null ? crmContact.getTrackcode() : ""));
		}
		StringBuffer message = new StringBuffer();

		message.append("Hello");
		message.append(",<br/>");
		message.append("You have received a new lead.<br/>");
		message.append("<table>");

		if (crmForm != null) {

			CrmContactField crmContactField = null;
			int i = 0;
			List<CrmContactField> list = new ArrayList<CrmContactField>();

			for (CrmFormField crmFormField : crmForm.getCrmFields()) {
				switch (crmFormField.getContactFieldId()) {
				case 1010://System.out.println("------------------------------------------------1010----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">First Name : </td><td style=\"vertical-align:text-top;\">" + crmContact.getFirstName() + "</td></tr>");
					break;
				case 1020://System.out.println("------------------------------------------------1020----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Last Name : </td><td style=\"vertical-align:text-top;\">" + crmContact.getLastName() + "</td></tr>");
					break;
				case 1030://System.out.println("------------------------------------------------1030----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Email 1 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getEmail1() + "</td></tr>");
					break;
				case 1040://System.out.println("------------------------------------------------1040----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Email 2 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getEmail2() + "</td></tr>");
					break;
				case 1050://System.out.println("------------------------------------------------1050----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\"> Phone 1 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getPhone1() + "</td></tr>");
					break;
				case 1060://System.out.println("------------------------------------------------1060----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Phone 2 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getPhone2() + "</td></tr>");
					break;
				case 1070://System.out.println("------------------------------------------------1070----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Fax : </td><td style=\"vertical-align:text-top;\">" + crmContact.getFax() + "</td></tr>");
					break;
				case 1080://System.out.println("------------------------------------------------1080----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Street : </td><td style=\"vertical-align:text-top;\">" + crmContact.getStreet() + "</td></tr>");
					break;
				case 1090://System.out.println("------------------------------------------------1090----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">City : </td><td style=\"vertical-align:text-top;\">" + crmContact.getCity() + "</td></tr>");
					break;
				case 1100://System.out.println("------------------------------------------------1100----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">State : </td><td style=\"vertical-align:text-top;\">" + crmContact.getStateProvince() + "</td></tr>");
					break;
				case 1110://System.out.println("------------------------------------------------1110----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Zip : </td><td style=\"vertical-align:text-top;\">" + crmContact.getZip() + "</td></tr>");
					break;
				case 1120://System.out.println("------------------------------------------------1120----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Country : </td><td style=\"vertical-align:text-top;\">" + getCountryCodes(crmContact.getCountry()) + "</td></tr>");
					break;
				case 1130://System.out.println("------------------------------------------------1130----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Description : </td><td style=\"vertical-align:text-top;\">"
							+ crmContact.getDescription().replace("\n", "<br/>") + "</td></tr>");
					break;
				case 1140://System.out.println("------------------------------------------------1140----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Address Type : </td><td style=\"vertical-align:text-top;\">"
							+ crmContact.getAddressType().replace("\n", "<br/>") + "</td></tr>");
					break;
				case 1150://System.out.println("------------------------------------------------1150----------------------------------------------------");
					if (attachment != null) {
						helper.addAttachment(attachment.getName(), attachment);
					}
					break;
				case 1160://System.out.println("------------------------------------------------1160----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">City : </td><td style=\"vertical-align:text-top;\">" + crmContact.getLanguage() + "</td></tr>");
					break;
				default:
					crmContactField = new CrmContactField();
					//System.out.println("----------------------------------------------default-11916----------------------------------------------------");
					if (crmFormField.getType() == 3) {
						StringBuffer multiValues = new StringBuffer();
						for (int j = 0; j < ServletRequestUtils.getStringParameters(request, "crmContact.crmContactFields[" + i + "].fieldValue").length; j++) {
							multiValues.append(ServletRequestUtils.getStringParameters(request, "crmContact.crmContactFields[" + i + "].fieldValue")[j]);
							if (j != ServletRequestUtils.getStringParameters(request, "crmContact.crmContactFields[" + i + "].fieldValue").length - 1) {
								multiValues.append(",");
							}
						}
						//System.out.println("-----------------------------------------------------------11925------------------------------------------------------");
						message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">" + crmFormField.getFieldName() + " : </td><td style=\"vertical-align:text-top;\">"
								+ multiValues.toString().replace("\n", "<br/>") + "</td></tr>");
					} else if (crmFormField.getType() == 4) {
						//System.out.println("-------------------------------------------------------------11929-----------------------------------------------------");
						message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">" + crmFormField.getFieldName() + " : </td><td style=\"vertical-align:text-top;\">"
								+ ServletRequestUtils.getStringParameter(request, "crmContact.crmContactFields[" + i + "].fieldValue") + "</td></tr>");
					} else {
						//System.out.println("------------------------------------------------------------11933-----------------------------------------------------------");
						message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">" + crmFormField.getFieldName() + " : </td><td style=\"vertical-align:text-top;\">"
								+ ServletRequestUtils.getStringParameter(request, "crmContact.crmContactFields[" + i + "].fieldValue").replace("\n", "<br/>") + "</td></tr>");
					}
					i++;
					//System.out.println("---------------------------------------------------11938--------------------------------------------------------");
					break;
				}
			}
		} else {
			//System.out.println("--------------------------------------------------11943-----------------------------------------------------");
			if (crmContact.getFirstName() != null) {//System.out.println("--------------------------------------------------getFirstName-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">First Name : </td><td style=\"vertical-align:text-top;\">" + crmContact.getFirstName() + "</td></tr>");
			}
			if (crmContact.getLastName() != null) {//System.out.println("--------------------------------------------------getLastName-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Last Name : </td><td style=\"vertical-align:text-top;\">" + crmContact.getLastName() + "</td></tr>");
			}
			if (crmContact.getEmail1() != null) {//System.out.println("--------------------------------------------------getEmail1-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Email 1 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getEmail1() + "</td></tr>");
			}
			if (crmContact.getEmail2() != null) {//System.out.println("--------------------------------------------------getEmail2-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Email 2 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getEmail2() + "</td></tr>");
			}
			if (crmContact.getPhone1() != null) {//System.out.println("--------------------------------------------------getPhone1-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\"> Phone 1 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getPhone1() + "</td></tr>");
			}
			if (crmContact.getPhone2() != null) {//System.out.println("--------------------------------------------------getPhone2-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Phone 2 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getPhone2() + "</td></tr>");
			}
			if (crmContact.getFax() != null) {//System.out.println("--------------------------------------------------getFax-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Fax : </td><td style=\"vertical-align:text-top;\">" + crmContact.getFax() + "</td></tr>");
			}
			if (crmContact.getDescription() != null) {//System.out.println("--------------------------------------------------getDescription-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Description : </td><td style=\"vertical-align:text-top;\">"
						+ crmContact.getDescription().replace("\n", "<br/>") + "</td></tr>");
			}
			if (crmContact.getDepartment() != null) {//System.out.println("--------------------------------------------------getDepartment-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Department : </td><td style=\"vertical-align:text-top;\">" + crmContact.getDepartment() + "</td></tr>");
			}
			if (crmContact.getStreet() != null) {//System.out.println("--------------------------------------------------getStreet-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Street : </td><td style=\"vertical-align:text-top;\">" + crmContact.getStreet() + "</td></tr>");
			}
			if (crmContact.getCity() != null) {//System.out.println("--------------------------------------------------getCity-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">City : </td><td style=\"vertical-align:text-top;\">" + crmContact.getCity() + "</td></tr>");
			}
			if (crmContact.getStateProvince() != null) {//System.out.println("--------------------------------------------------getStateProvince-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">State : </td><td style=\"vertical-align:text-top;\">" + crmContact.getStateProvince() + "</td></tr>");
			}
			if (crmContact.getZip() != null) {//System.out.println("--------------------------------------------------getZip-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Zip : </td><td style=\"vertical-align:text-top;\">" + crmContact.getZip() + "</td></tr>");
			}
			if (crmContact.getCountry() != null) {//System.out.println("--------------------------------------------------getCountry-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Country : </td><td style=\"vertical-align:text-top;\">" + crmContact.getCountry() + "</td></tr>");
			}
			if (crmContact.getOtherStreet() != null) {//System.out.println("--------------------------------------------------getOtherStreet-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Other Street : </td><td style=\"vertical-align:text-top;\">" + crmContact.getOtherStreet() + "</td></tr>");
			}
			if (crmContact.getOtherCity() != null) {//System.out.println("--------------------------------------------------getOtherCity-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Other City : </td><td style=\"vertical-align:text-top;\">" + crmContact.getOtherCity() + "</td></tr>");
			}
			if (crmContact.getOtherStateProvince() != null) {//System.out.println("--------------------------------------------------getOtherStateProvince-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Other State : </td><td style=\"vertical-align:text-top;\">" + crmContact.getOtherStateProvince()
						+ "</td></tr>");
			}
			if (crmContact.getOtherZip() != null) {//System.out.println("--------------------------------------------------getOtherZip-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Other Zip : </td><td style=\"vertical-align:text-top;\">" + crmContact.getOtherZip() + "</td></tr>");
			}
			if (crmContact.getOtherCountry() != null) {//System.out.println("--------------------------------------------------getOtherCountry-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Other Country : </td><td style=\"vertical-align:text-top;\">" + crmContact.getOtherCountry() + "</td></tr>");
			}
			//System.out.println("-----------------------------------------11003-------------------------------------------");
			if (crmContact.getCrmContactFields() != null) {
				for (CrmContactField crmContactfield : crmContact.getCrmContactFields()) {
					if (crmContactfield.getFieldValue() != null) {//System.out.println("-----------------------------------------getFieldValue-------------------------------------------");
						message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">" + crmContactfield.getFieldName() + " : </td><td style=\"vertical-align:text-top;\">"
								+ crmContactfield.getFieldValue() + "</td></tr>");
					}
				}
			}
		}
		//System.out.println("-------------------------------------------------12013-------------------------------------------------------");
		helper.setText(message.toString(), true);
		try {
			//System.out.println("----------------webjaguarimpl newLeadEmail before-------------------------------");
			mailSender.send(mms);
			//System.out.println("----------------webjaguarimpl newLeadEmail after-------------------------------------------------------");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void newLeadEmail(CrmContact crmContact, String recipientEmail, String recipientCcEmail, String recipientBccEmail, HttpServletRequest request, CrmForm crmForm, File attachment, File attachment1, File attachment2, File attachment3, File attachment4, JavaMailSenderImpl mailSender) throws Exception {

		MimeMessage mms = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		helper.setTo(recipientEmail);
		//System.out.println("---------------------------------newLeadEmail crmForm.getFormId()----------------------------------" + crmForm.getFormId());
		// cc emails
		if (ServletRequestUtils.getBooleanParameter(request, "cc", true) && recipientCcEmail != null && !recipientCcEmail.trim().isEmpty()) {
			for (String ccEmail : recipientCcEmail.split(",")) {
				helper.addCc(ccEmail.trim());
			}
		}
		// bcc emails
		if (ServletRequestUtils.getBooleanParameter(request, "bcc", true) && recipientBccEmail != null && !recipientBccEmail.trim().isEmpty()) {
			for (String bccEmail : recipientBccEmail.split(",")) {
				helper.addBcc(bccEmail.trim());
			}
		}
		

		//System.out.println("-------------------------------------newLeadEmail recipientEmail----------------------------------" + recipientEmail);
		if (siteConfig.get("CONTACT_EMAIL") != null && !siteConfig.get("CONTACT_EMAIL").getValue().equals("")) {
			helper.setFrom(siteConfig.get("CONTACT_EMAIL").getValue());
		} else {
			helper.setFrom("noreply@noreply.com");
		}
		if (ServletRequestUtils.getStringParameter(request, "adminEmailSubject") != null && !ServletRequestUtils.getStringParameter(request, "adminEmailSubject").isEmpty()) {
			helper.setSubject(ServletRequestUtils.getStringParameter(request, "adminEmailSubject"));
		} else {
			helper.setSubject("Inquiry " + (crmContact.getTrackcode() != null ? crmContact.getTrackcode() : ""));
		}
		StringBuffer message = new StringBuffer();

		message.append("Hello");
		message.append(",<br/>");
		message.append("You have received a new lead.<br/>");
		message.append("<table>");

		if (crmForm != null) {

			CrmContactField crmContactField = null;
			int i = 0;
			List<CrmContactField> list = new ArrayList<CrmContactField>();

			for (CrmFormField crmFormField : crmForm.getCrmFields()) {
				switch (crmFormField.getContactFieldId()) {
				case 1010://System.out.println("------------------------------------------------1010----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">First Name : </td><td style=\"vertical-align:text-top;\">" + crmContact.getFirstName() + "</td></tr>");
					break;
				case 1020://System.out.println("------------------------------------------------1020----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Last Name : </td><td style=\"vertical-align:text-top;\">" + crmContact.getLastName() + "</td></tr>");
					break;
				case 1030://System.out.println("------------------------------------------------1030----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Email 1 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getEmail1() + "</td></tr>");
					break;
				case 1040://System.out.println("------------------------------------------------1040----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Email 2 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getEmail2() + "</td></tr>");
					break;
				case 1050://System.out.println("------------------------------------------------1050----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\"> Phone 1 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getPhone1() + "</td></tr>");
					break;
				case 1060://System.out.println("------------------------------------------------1060----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Phone 2 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getPhone2() + "</td></tr>");
					break;
				case 1070://System.out.println("------------------------------------------------1070----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Fax : </td><td style=\"vertical-align:text-top;\">" + crmContact.getFax() + "</td></tr>");
					break;
				case 1080://System.out.println("------------------------------------------------1080----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Street : </td><td style=\"vertical-align:text-top;\">" + crmContact.getStreet() + "</td></tr>");
					break;
				case 1090://System.out.println("------------------------------------------------1090----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">City : </td><td style=\"vertical-align:text-top;\">" + crmContact.getCity() + "</td></tr>");
					break;
				case 1100://System.out.println("------------------------------------------------1100----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">State : </td><td style=\"vertical-align:text-top;\">" + crmContact.getStateProvince() + "</td></tr>");
					break;
				case 1110://System.out.println("------------------------------------------------1110----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Zip : </td><td style=\"vertical-align:text-top;\">" + crmContact.getZip() + "</td></tr>");
					break;
				case 1120://System.out.println("------------------------------------------------1120----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Country : </td><td style=\"vertical-align:text-top;\">" + getCountryCodes(crmContact.getCountry()) + "</td></tr>");
					break;
				case 1130://System.out.println("------------------------------------------------1130----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Description : </td><td style=\"vertical-align:text-top;\">"
							+ crmContact.getDescription().replace("\n", "<br/>") + "</td></tr>");
					break;
				case 1140://System.out.println("------------------------------------------------1140----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Address Type : </td><td style=\"vertical-align:text-top;\">"
							+ crmContact.getAddressType().replace("\n", "<br/>") + "</td></tr>");
					break;
				case 1150://System.out.println("------------------------------------------------1150----------------------------------------------------");
					if (attachment != null) {
						helper.addAttachment(attachment.getName(), attachment);
					}
					if (attachment1 != null) {
						helper.addAttachment(attachment1.getName(), attachment1);
					}
					if (attachment2 != null) {
						helper.addAttachment(attachment2.getName(), attachment2);
					}
					if (attachment3 != null) {
						helper.addAttachment(attachment3.getName(), attachment3);
					}
					if (attachment4 != null) {
						helper.addAttachment(attachment4.getName(), attachment4);
					}
					break;
				case 1160://System.out.println("------------------------------------------------1160----------------------------------------------------");
					message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">City : </td><td style=\"vertical-align:text-top;\">" + crmContact.getLanguage() + "</td></tr>");
					break;
				default:
					crmContactField = new CrmContactField();
					//System.out.println("----------------------------------------------default-11916----------------------------------------------------");
					if (crmFormField.getType() == 3) {
						StringBuffer multiValues = new StringBuffer();
						for (int j = 0; j < ServletRequestUtils.getStringParameters(request, "crmContact.crmContactFields[" + i + "].fieldValue").length; j++) {
							multiValues.append(ServletRequestUtils.getStringParameters(request, "crmContact.crmContactFields[" + i + "].fieldValue")[j]);
							if (j != ServletRequestUtils.getStringParameters(request, "crmContact.crmContactFields[" + i + "].fieldValue").length - 1) {
								multiValues.append(",");
							}
						}
						//System.out.println("-----------------------------------------------------------11925------------------------------------------------------");
						message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">" + crmFormField.getFieldName() + " : </td><td style=\"vertical-align:text-top;\">"
								+ multiValues.toString().replace("\n", "<br/>") + "</td></tr>");
					} else if (crmFormField.getType() == 4) {
						//System.out.println("-------------------------------------------------------------11929-----------------------------------------------------");
						message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">" + crmFormField.getFieldName() + " : </td><td style=\"vertical-align:text-top;\">"
								+ ServletRequestUtils.getStringParameter(request, "crmContact.crmContactFields[" + i + "].fieldValue") + "</td></tr>");
					} else {
						//System.out.println("------------------------------------------------------------11933-----------------------------------------------------------");
						try{
						message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">" + crmFormField.getFieldName() + " : </td><td style=\"vertical-align:text-top;\">"
								+ ServletRequestUtils.getStringParameter(request, "crmContact.crmContactFields[" + i + "].fieldValue").replace("\n", "<br/>") + "</td></tr>");}
						catch(Exception e){
							System.out.println("**WebjaguarImpl:-While sending newlead Email");
							e.printStackTrace();
						}
					}
					i++;
					//System.out.println("---------------------------------------------------11938--------------------------------------------------------");
					break;
				}
			}
		} else {
			//System.out.println("--------------------------------------------------11943-----------------------------------------------------");
			if (crmContact.getFirstName() != null) {//System.out.println("--------------------------------------------------getFirstName-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">First Name : </td><td style=\"vertical-align:text-top;\">" + crmContact.getFirstName() + "</td></tr>");
			}
			if (crmContact.getLastName() != null) {//System.out.println("--------------------------------------------------getLastName-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Last Name : </td><td style=\"vertical-align:text-top;\">" + crmContact.getLastName() + "</td></tr>");
			}
			if (crmContact.getEmail1() != null) {//System.out.println("--------------------------------------------------getEmail1-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Email 1 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getEmail1() + "</td></tr>");
			}
			if (crmContact.getEmail2() != null) {//System.out.println("--------------------------------------------------getEmail2-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Email 2 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getEmail2() + "</td></tr>");
			}
			if (crmContact.getPhone1() != null) {//System.out.println("--------------------------------------------------getPhone1-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\"> Phone 1 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getPhone1() + "</td></tr>");
			}
			if (crmContact.getPhone2() != null) {//System.out.println("--------------------------------------------------getPhone2-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Phone 2 : </td><td style=\"vertical-align:text-top;\">" + crmContact.getPhone2() + "</td></tr>");
			}
			if (crmContact.getFax() != null) {//System.out.println("--------------------------------------------------getFax-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Fax : </td><td style=\"vertical-align:text-top;\">" + crmContact.getFax() + "</td></tr>");
			}
			if (crmContact.getDescription() != null) {//System.out.println("--------------------------------------------------getDescription-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Description : </td><td style=\"vertical-align:text-top;\">"
						+ crmContact.getDescription().replace("\n", "<br/>") + "</td></tr>");
			}
			if (crmContact.getDepartment() != null) {//System.out.println("--------------------------------------------------getDepartment-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Department : </td><td style=\"vertical-align:text-top;\">" + crmContact.getDepartment() + "</td></tr>");
			}
			if (crmContact.getStreet() != null) {//System.out.println("--------------------------------------------------getStreet-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Street : </td><td style=\"vertical-align:text-top;\">" + crmContact.getStreet() + "</td></tr>");
			}
			if (crmContact.getCity() != null) {//System.out.println("--------------------------------------------------getCity-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">City : </td><td style=\"vertical-align:text-top;\">" + crmContact.getCity() + "</td></tr>");
			}
			if (crmContact.getStateProvince() != null) {//System.out.println("--------------------------------------------------getStateProvince-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">State : </td><td style=\"vertical-align:text-top;\">" + crmContact.getStateProvince() + "</td></tr>");
			}
			if (crmContact.getZip() != null) {//System.out.println("--------------------------------------------------getZip-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Zip : </td><td style=\"vertical-align:text-top;\">" + crmContact.getZip() + "</td></tr>");
			}
			if (crmContact.getCountry() != null) {//System.out.println("--------------------------------------------------getCountry-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Country : </td><td style=\"vertical-align:text-top;\">" + crmContact.getCountry() + "</td></tr>");
			}
			if (crmContact.getOtherStreet() != null) {//System.out.println("--------------------------------------------------getOtherStreet-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Other Street : </td><td style=\"vertical-align:text-top;\">" + crmContact.getOtherStreet() + "</td></tr>");
			}
			if (crmContact.getOtherCity() != null) {//System.out.println("--------------------------------------------------getOtherCity-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Other City : </td><td style=\"vertical-align:text-top;\">" + crmContact.getOtherCity() + "</td></tr>");
			}
			if (crmContact.getOtherStateProvince() != null) {//System.out.println("--------------------------------------------------getOtherStateProvince-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Other State : </td><td style=\"vertical-align:text-top;\">" + crmContact.getOtherStateProvince()
						+ "</td></tr>");
			}
			if (crmContact.getOtherZip() != null) {//System.out.println("--------------------------------------------------getOtherZip-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Other Zip : </td><td style=\"vertical-align:text-top;\">" + crmContact.getOtherZip() + "</td></tr>");
			}
			if (crmContact.getOtherCountry() != null) {//System.out.println("--------------------------------------------------getOtherCountry-----------------------------------------------------");
				message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">Other Country : </td><td style=\"vertical-align:text-top;\">" + crmContact.getOtherCountry() + "</td></tr>");
			}
			//System.out.println("-----------------------------------------11003-------------------------------------------");
			if (crmContact.getCrmContactFields() != null) {
				for (CrmContactField crmContactfield : crmContact.getCrmContactFields()) {
					if (crmContactfield.getFieldValue() != null) {//System.out.println("-----------------------------------------getFieldValue-------------------------------------------");
						message.append("<tr height=\"30px;\"><td style=\"vertical-align:text-top;\">" + crmContactfield.getFieldName() + " : </td><td style=\"vertical-align:text-top;\">"
								+ crmContactfield.getFieldValue() + "</td></tr>");
					}
				}
			}
		}
		//System.out.println("-------------------------------------------------12013-------------------------------------------------------");
		helper.setText(message.toString(), true);
		try {
			//System.out.println("----------------webjaguarimpl newLeadEmail before-------------------------------");
			mailSender.send(mms);
			//System.out.println("----------------webjaguarimpl newLeadEmail after-------------------------------------------------------");
		} catch (Exception e) {
			System.out.println("WebJagImpl-:email not sent : line no.12892");
			e.printStackTrace();
		}

	}
	
    private String getCountryCodes(String countryCode){
    	
    	Map<String,String> countryCodeMap = new HashMap<String,String>();
    	Map<String,String> countryMap = new HashMap<String,String>();

    	countryCodeMap.put("Andorra", "AD");
    	countryCodeMap.put("United Arab Emirates", "AE");
    	countryCodeMap.put("Afghanistan", "AF");
    	countryCodeMap.put("Antigua and Barbuda", "AG");
    	countryCodeMap.put("Anguilla", "AI");
    	countryCodeMap.put("Albania", "AL");
    	countryCodeMap.put("Armenia", "AM");
    	countryCodeMap.put("Netherlands Antilles", "AN");
    	countryCodeMap.put("Angola", "AO");
    	countryCodeMap.put("Antarctica", "AQ");
    	countryCodeMap.put("Argentina", "AR");
    	countryCodeMap.put("American Samoa", "AS");
    	countryCodeMap.put("Austria", "AT");
    	countryCodeMap.put("Australia", "AU");
    	countryCodeMap.put("Aruba", "AW");
    	countryCodeMap.put("Azerbaijan", "AZ");
    	countryCodeMap.put("Bosnia and Herzegovina", "BA");
    	countryCodeMap.put("Barbados", "BB");
    	countryCodeMap.put("Bangladesh", "BD");
    	countryCodeMap.put("Belgium", "BE");
    	countryCodeMap.put("Burkina Faso", "BF");
    	countryCodeMap.put("Bulgaria", "BG");
    	countryCodeMap.put("Bahrain", "BH");
    	countryCodeMap.put("Burundi", "BI");
    	countryCodeMap.put("Benin", "BJ");
    	countryCodeMap.put("Bermuda", "BM");
    	countryCodeMap.put("Brunei Darussalam", "BN");
    	countryCodeMap.put("Bolivia", "BO");
    	countryCodeMap.put("Brazil", "BR");
    	countryCodeMap.put("Bahamas", "BS");
    	countryCodeMap.put("Bhutan", "BT");
    	countryCodeMap.put("Bouvet Island", "BV");
    	countryCodeMap.put("Botswana", "BW");
    	countryCodeMap.put("Belarus", "BY");
    	countryCodeMap.put("Belize", "BZ");
    	countryCodeMap.put("Canada", "CA");
    	countryCodeMap.put("Cocos Islands", "CC");
    	countryCodeMap.put("Central African Republic", "CF");
    	countryCodeMap.put("Congo", "CG");
    	countryCodeMap.put("Switzerland", "CH");
    	countryCodeMap.put("Cote D'Ivoire", "CI");
    	countryCodeMap.put("Cook Islands", "CK");
    	countryCodeMap.put("Chile", "CL");
    	countryCodeMap.put("Cameroon", "CM");
    	countryCodeMap.put("China", "CN");
    	countryCodeMap.put("Colombia", "CO");
    	countryCodeMap.put("US Commercial", "COM");
    	countryCodeMap.put("Costa Rica", "CR");
    	countryCodeMap.put("Czechoslovakia", "CS");
    	countryCodeMap.put("Cuba", "CU");
    	countryCodeMap.put("Cape Verde", "CV");
    	countryCodeMap.put("Christmas Island", "CX");
    	countryCodeMap.put("Cyprus", "CY");
    	countryCodeMap.put("Czech Republic", "CZ");
    	countryCodeMap.put("Germany", "DE");
    	countryCodeMap.put("Djibouti", "DJ");
    	countryCodeMap.put("Denmark", "DK");
    	countryCodeMap.put("Dominica", "DM");
    	countryCodeMap.put("Dominican Republic", "DO");
    	countryCodeMap.put("Algeria", "DZ");
    	countryCodeMap.put("Ecuador", "EC");
    	countryCodeMap.put("US Educational", "EDU");
    	countryCodeMap.put("Estonia", "EE");
    	countryCodeMap.put("Egypt", "EG");
    	countryCodeMap.put("Western Sahara", "EH");
    	countryCodeMap.put("Eritrea", "ER");
    	countryCodeMap.put("Spain", "SP");
    	countryCodeMap.put("Ethiopia", "ET");
    	countryCodeMap.put("Finland", "FI");
    	countryCodeMap.put("Fiji", "FJ");
    	countryCodeMap.put("Falkland Islands", "FK");
    	countryCodeMap.put("Micronesia", "FM");
    	countryCodeMap.put("Faroe Islands", "FO");
    	countryCodeMap.put("France", "FR");
    	countryCodeMap.put("France, Metropolitan", "FX");
    	countryCodeMap.put("Gabon", "GA");
    	countryCodeMap.put("Great Britain", "GB");
    	countryCodeMap.put("Grenada", "GD");
    	countryCodeMap.put("Georgia", "GE");
    	countryCodeMap.put("French Guiana", "GF");
    	countryCodeMap.put("Ghana", "GH");
    	countryCodeMap.put("Gibraltar", "GI");
    	countryCodeMap.put("Greenland", "GL");
    	countryCodeMap.put("Gambia", "GM");
    	countryCodeMap.put("Guinea", "GN");
    	countryCodeMap.put("US Government", "GOV");
    	countryCodeMap.put("Guadeloupe", "GP");
    	countryCodeMap.put("Guadeloupe", "GO");
    	countryCodeMap.put("Greece", "GR");
    	countryCodeMap.put("S. Georgia and S. Sandwich Isls.", "GS");
    	countryCodeMap.put("Guatemala", "GT");
    	countryCodeMap.put("Guam", "GU");
    	countryCodeMap.put("Guinea-Bissau", "GW");
    	countryCodeMap.put("Guyana", "GN");
    	countryCodeMap.put("Hong Kong", "HK");
    	countryCodeMap.put("Heard and McDonald Islands", "HM");
    	countryCodeMap.put("Honduras", "HN");
    	countryCodeMap.put("Croatia", "HR");
    	countryCodeMap.put("Haiti", "HT");
    	countryCodeMap.put("Hungary", "HU");
    	countryCodeMap.put("Indonesia", "ID");
    	countryCodeMap.put("Ireland", "IE");
    	countryCodeMap.put("Israel", "IL");
    	countryCodeMap.put("India", "IN");
    	countryCodeMap.put("International", "INT");
    	countryCodeMap.put("British Indian Ocean Territory", "IO");
    	countryCodeMap.put("Iraq", "IQ");
    	countryCodeMap.put("Iran", "IR");
    	countryCodeMap.put("Iceland", "IS");
    	countryCodeMap.put("Italy", "IT");
    	countryCodeMap.put("Jamaica", "JM");
    	countryCodeMap.put("Jordan", "JO");
    	countryCodeMap.put("Japan", "JP");
    	countryCodeMap.put("Kenya", "KE");
    	countryCodeMap.put("Kyrgyzstan", "KG");
    	countryCodeMap.put("Cambodia", "KH");
    	countryCodeMap.put("Kiribati", "KI");
    	countryCodeMap.put("Comoros", "KM");
    	countryCodeMap.put("Saint Kitts and Nevis", "KN");
    	countryCodeMap.put("Korea (North)", "KP");
    	countryCodeMap.put("Korea (South)", "KR");
    	countryCodeMap.put("Kuwait", "KW");
    	countryCodeMap.put("Cayman Islands", "KY");
    	countryCodeMap.put("Kazakhstan", "KZ");
    	countryCodeMap.put("Laos", "LA");
    	countryCodeMap.put("Lebanon", "LB");
    	countryCodeMap.put("Saint Lucia", "LC");
    	countryCodeMap.put("Liechtenstein", "LI");
    	countryCodeMap.put("Sri Lanka", "LK");
    	countryCodeMap.put("Liberia", "LR");
    	countryCodeMap.put("Lesotho", "LS");
    	countryCodeMap.put("Lithuania", "LI");
    	countryCodeMap.put("Luxembourg", "LU");
    	countryCodeMap.put("Latvia", "LV");
    	countryCodeMap.put("Libya", "LY");
    	countryCodeMap.put("Morocco", "MA");
    	countryCodeMap.put("Monaco", "MC");
    	countryCodeMap.put("Moldova", "MD");
    	countryCodeMap.put("Madagascar", "MG");
    	countryCodeMap.put("Marshall Islands", "MH");
    	countryCodeMap.put("US Military", "MIL");
    	countryCodeMap.put("Macedonia", "MK");
    	countryCodeMap.put("Mali", "ML");
    	countryCodeMap.put("Myanmar", "MM");
    	countryCodeMap.put("Mongolia", "MN");
    	countryCodeMap.put("Macau", "MO");
    	countryCodeMap.put("Northern Mariana Islands", "MP");
    	countryCodeMap.put("Martinique", "MQ");
    	countryCodeMap.put("Mauritania", "MR");
    	countryCodeMap.put("Montserrat", "MS");
    	countryCodeMap.put("Malta", "MT");
    	countryCodeMap.put("Mauritius", "MU");
    	countryCodeMap.put("Maldives", "MV");
    	countryCodeMap.put("Malawi", "MW");
    	countryCodeMap.put("Mexico", "MX");
    	countryCodeMap.put("Malaysia", "MY");
    	countryCodeMap.put("Mozambique", "MZ");
    	countryCodeMap.put("Namibia", "MT");
    	countryCodeMap.put("Nato field", "MT");
    	countryCodeMap.put("New Caledonia", "MT");
    	countryCodeMap.put("Niger", "NG");
    	countryCodeMap.put("Norfolk Island", "NL");
    	countryCodeMap.put("Nigeria", "NO");
    	countryCodeMap.put("Nicaragua", "NI");
    	countryCodeMap.put("Netherlands", "NL");
    	countryCodeMap.put("Norway", "NO");
    	countryCodeMap.put("Nepal", "NP");
    	countryCodeMap.put("Nauru", "NR");
    	countryCodeMap.put("Neutral Zone", "NT");
    	countryCodeMap.put("Niue", "NU");
    	countryCodeMap.put("New Zealand", "NZ");
    	countryCodeMap.put("Oman", "OM");
    	countryCodeMap.put("Non-Profit Organization", "ORG");
    	countryCodeMap.put("Panama", "PA");
    	countryCodeMap.put("Peru", "PE");
    	countryCodeMap.put("French Polynesia", "PF");
    	countryCodeMap.put("Papua New Guinea", "PG");
    	countryCodeMap.put("Philippines", "PH");
    	countryCodeMap.put("Pakistan", "PK");
    	countryCodeMap.put("Poland", "PL");
    	countryCodeMap.put("St. Pierre and Miquelon", "PM");
    	countryCodeMap.put("Pitcairn", "PN");
    	countryCodeMap.put("Pitcairn", "PN");
    	countryCodeMap.put("Puerto Rico", "PR");
    	countryCodeMap.put("Portugal", "PT");
    	countryCodeMap.put("Palau", "PW");
    	countryCodeMap.put("Paraguay", "PY");
    	countryCodeMap.put("Qatar", "QA");
    	countryCodeMap.put("Reunion", "RE");
    	countryCodeMap.put("Romania", "RO");
    	countryCodeMap.put("Russian Federation", "RU");
    	countryCodeMap.put("Rwanda", "RW");
    	countryCodeMap.put("Saudi Arabia", "SA");
    	countryCodeMap.put("Seychelles", "SC");
    	countryCodeMap.put("Solomon Islands", "SB");
    	countryCodeMap.put("Sudan", "SD");
    	countryCodeMap.put("Sweden", "SE");
    	countryCodeMap.put("Singapore", "SG");
    	countryCodeMap.put("St. Helena", "SH");
    	countryCodeMap.put("Slovenia", "NI");
    	countryCodeMap.put("Svalbard and Jan Mayen Islands", "SJ");
    	countryCodeMap.put("Slovak Republic", "SK");
    	countryCodeMap.put("Sierra Leone", "SL");
    	countryCodeMap.put("San Marino", "SM");
    	countryCodeMap.put("Senegal", "SN");
    	countryCodeMap.put("Somalia", "SO");
    	countryCodeMap.put("Suriname", "SR");
    	countryCodeMap.put("Sao Tome and Principe", "ST");
    	countryCodeMap.put("USSR", "SU");
    	countryCodeMap.put("El Salvador", "SV");
    	countryCodeMap.put("Syria", "SY");
    	countryCodeMap.put("Swaziland", "SZ");
    	countryCodeMap.put("Suriname", "SR");
    	countryCodeMap.put("Turks and Caicos Islands", "TC");
    	countryCodeMap.put("Chad", "TD");
    	countryCodeMap.put("French Southern Territories", "TF");
    	countryCodeMap.put("Togo", "TG");
    	countryCodeMap.put("Thailand", "TH");
    	countryCodeMap.put("Thailand", "TJ");
    	countryCodeMap.put("Tajikistan", "SR");
    	countryCodeMap.put("Tokelau", "TK");
    	countryCodeMap.put("Turkmenistan", "Turkmenistan");
    	countryCodeMap.put("Tunisia", "TN");
    	countryCodeMap.put("Tonga", "TO");
    	countryCodeMap.put("East Timor", "TP");
    	countryCodeMap.put("Turkey", "TR");
    	countryCodeMap.put("Trinidad and Tobago", "TT");
    	countryCodeMap.put("Tuvalu", "TV");
    	countryCodeMap.put("Taiwan", "TW");
    	countryCodeMap.put("Tanzania", "TZ");
    	countryCodeMap.put("Thailand", "SR");
    	countryCodeMap.put("Ukraine", "UA");
    	countryCodeMap.put("Uganda", "UG");
    	countryCodeMap.put("United Kingdom", "UK");
    	countryCodeMap.put("US Minor Outlying Islands", "UM");
    	countryCodeMap.put("United States", "US");
    	countryCodeMap.put("Uruguay", "UY");
    	countryCodeMap.put("Uzbekistan", "UZ");
    	countryCodeMap.put("Vatican City State", "VA");
    	countryCodeMap.put("Saint Vincent and the Grenadines", "VC");
    	countryCodeMap.put("Venezuela", "VE");
    	countryCodeMap.put("Virgin Islands", "VG");
    	countryCodeMap.put("Virgin Islands (U.S.)", "VI");
    	countryCodeMap.put("Viet Nam", "VN");
    	countryCodeMap.put("Vanuatu", "VU");
    	countryCodeMap.put("Wallis and Futuna Islands", "WF");
    	countryCodeMap.put("Samoa", "WS");
    	countryCodeMap.put("Yemen", "YE");
    	countryCodeMap.put("Mayotte", "YT");
    	countryCodeMap.put("Serbia", "RS");
    	countryCodeMap.put("South Africa", "ZA");
    	countryCodeMap.put("Zambia", "ZM");
    	countryCodeMap.put("Zaire", "ZR");
    	countryCodeMap.put("Zimbabwe", "ZW");
    	

    	for (Map.Entry<String, String> entry : countryCodeMap.entrySet())
    	{
    		//System.out.println(entry.getKey() + "/" + entry.getValue());
    		countryMap.put(entry.getValue(), entry.getKey());
    	}

    	return countryMap.get(countryCode);
    }


	// EchoSign
	public void nonTransactionSafeInsertEchoSign(EchoSign echoSign, boolean insertIgnore) {
		this.echoSignDao.insertEchoSign(echoSign, insertIgnore);
	}

	public List<EchoSign> getEchoSignListByUserid(int userId) {
		return this.echoSignDao.getEchoSignListByUserid(userId);
	}

	public void updateEchoSign(EchoSign echoSign) {
		this.echoSignDao.updateEchoSign(echoSign);
	}

	public EchoSign getEchoSign(String documentKey) {
		return this.echoSignDao.getEchoSign(documentKey);
	}

	public Map<Integer, Long> getEchoSignSentMap(int year) {
		return this.echoSignDao.getEchoSignSentMap(year);
	}

	public List<EchoSign> getEchoSignBySearch(EchoSignSearch search) {
		return this.echoSignDao.getEchoSignBySearch(search);
	}

	public void nonTransactionSafeInsertEchoSignWidget(Widget widget) {
		this.echoSignDao.insertEchoSignWidget(widget);
	}

	public Widget getEchoSignWidgetByPath(String path) {
		return this.echoSignDao.getEchoSignWidgetByPath(path);
	}

	public Widget getEchoSignWidget(String documentKey) {
		return this.echoSignDao.getEchoSignWidget(documentKey);
	}

	public List<Widget> getEchoSignWidgetBySearch(EchoSignWidgetSearch search) {
		return this.echoSignDao.getEchoSignWidgetBySearch(search);
	}

	public Map<Integer, EchoSign> getLatestEchoSignMap() {
		return this.echoSignDao.getLatestEchoSignMap();
	}

	public List<ShippingRate> calculateShippingHandling(Order order, Customer customer, HttpServletRequest request, Map<String, Object> gSiteConfig) {
		// set total weight again as we set to 1 for UPS and USPS
		// this gives error if this function is being called second time when on shopping cart select different Address
		double totalWeight = 0;
		for (LineItem lineItem : order.getLineItems()) {
			if (lineItem.getPackageWeight() != null) {
				totalWeight = totalWeight + (lineItem.getPackageWeight().doubleValue() * lineItem.getQuantity());
			}
		}
		order.setTotalWeight(totalWeight);

		List<ShippingRate> finalList = new ArrayList<ShippingRate>();
		// Customer object is not available on Google checkout and invoice form.
		// Create it over here as it is not going to be used anywhere on Google checkout and invoice form
		if (customer == null) {
			customer = getCustomerById(order.getUserId());
		}
		Map<String, Configuration> siteConfig = getSiteConfig();

		if (siteConfig.get("SITE_URL").getValue().contains("tileshowroom")) {
			finalList = this.calculateTileShowRoomShipping(order, customer, request, gSiteConfig, siteConfig);
			return finalList;
		}

		if (((Boolean) gSiteConfig.get("gROAD_RUNNER")) && (siteConfig.get("SITE_URL").getValue().contains("viatrading") || siteConfig.get("SITE_URL").getValue().contains("test.wjserver430.com"))) {
			finalList = this.calculateViatradingShipping(order, globalDao, request, gSiteConfig, siteConfig);
			return finalList;
		}

		if (siteConfig.get("DSI").getValue().length() > 0 && !order.ishasRegularShipping()) {
			return finalList;
		}

		List<ShippingRate> customList = new ArrayList<ShippingRate>();
		// check if custom shipping is enabled for any of the line items
		for (LineItem lineItem : order.getLineItems()) {
			if (lineItem.getProduct().isCustomShippingEnabled()) {
				customList = getCustomShippingRateList(false, false);
				break;
			}
		}
		if (!customList.isEmpty()) {
			Set<Object> categoryIds = null;
			if ((Boolean) gSiteConfig.get("gCUSTOM_SHIPPING_ADVANCED")) {
				categoryIds = new HashSet<Object>();
				for (LineItem lineItem : order.getLineItems()) {
					categoryIds.addAll(this.getCategoryIdsByProductId(lineItem.getProductId()));
				}

				Set<Object> parentCategoryIds = new HashSet<Object>();
				// get parent categories also
				for (Object obj : categoryIds) {
					parentCategoryIds.addAll(this.getParentCategories(Integer.parseInt(obj.toString())));
				}
				if (!parentCategoryIds.isEmpty()) {
					categoryIds.addAll(parentCategoryIds);
				}
			}
			Iterator<ShippingRate> iter = customList.iterator();
			while (iter.hasNext()) {
				ShippingRate cRate = iter.next();
				if (cRate.getCountry() != null && !cRate.getCountry().equals("") && !order.getShipping().getCountry().equals(cRate.getCountry())) {
					iter.remove();
					continue;
				}
				if (cRate.getMinPrice() != null && order.getCustomShippingSubTotal() < cRate.getMinPrice()) {
					iter.remove();
					continue;
				}
				if (cRate.getMaxPrice() != null && order.getCustomShippingSubTotal() > cRate.getMaxPrice()) {
					iter.remove();
					continue;
				}
				if (cRate.getMinWeight() != null && (order.getTotalWeight() - order.getCustomShippingTotalWeight()) < cRate.getMinWeight()) {
					iter.remove();
					continue;
				}
				if (cRate.getMaxWeight() != null && (order.getTotalWeight() - order.getCustomShippingTotalWeight()) > cRate.getMaxWeight()) {
					iter.remove();
					continue;
				}
				if (!cRate.getHost().equals("") && !cRate.getHost().equals(request.getHeader("host"))) {
					iter.remove();
					continue;
				}
				// customer type
				if (cRate.getCustomerType() != null) {
					// regular customers
					if (cRate.getCustomerType() == 0 && customer.getPriceTable() > 0) {
						iter.remove();
						continue;
					}
					// wholesalers
					if (cRate.getCustomerType() == 1 && customer.getPriceTable() == 0) {
						iter.remove();
						continue;
					}
				}

				if ((Boolean) gSiteConfig.get("gCUSTOM_SHIPPING_ADVANCED")) {
					// customer group
					if ((Boolean) gSiteConfig.get("gGROUP_CUSTOMER")) {
						if (isCustomerInGroup(customer.getId(), cRate.getGroupIdSet(), cRate.getGroupType()) == false) {
							iter.remove();
							continue;
						}
					}
					// states
					if (cRate.getStates() != null && !cRate.getStates().equals("")) {
						String cState = order.getShipping().getStateProvince();
						String states = cRate.getStates();
						if ((!states.contains(cState) && cRate.getStateType().equalsIgnoreCase("include")) || (states.contains(cState) && cRate.getStateType().equalsIgnoreCase("exclude"))) {
							iter.remove();
							continue;
						}
					}
					// category
					if (cRate.getCategoryId() != null && !categoryIds.contains(cRate.getCategoryId())) {
						iter.remove();
						continue;
					}
				}
			}
		}

		ShippingRate flatRate = new ShippingRate();

		ShippingHandling shippingHandling = getShippingHandling();

		if (shippingHandling.isTbdEnable()) {
			flatRate.setTitle(shippingHandling.getTbdTitle());
			finalList.add(flatRate);
			return finalList;

		}
		if (shippingHandling.getPriceLimit() != null && order.getSubTotal() > shippingHandling.getPriceLimit()) {
			flatRate.setTitle(shippingHandling.getNewShippingTitle());
			flatRate.setPrice(shippingHandling.getNewPriceCost().toString());
			finalList.add(flatRate);
		} else if (shippingHandling.getShippingMethod().equals("crr")) {
			if (order.getTotalWeight() - order.getCustomShippingTotalWeight() <= 0) {
				if ((shippingHandling.getZeroWeightNewShippingTitle() != null && shippingHandling.getZeroWeightNewShippingTitle().trim().length() > 0)
						|| shippingHandling.getZeroWeightNewPriceCost() != null) {
					flatRate.setTitle(shippingHandling.getZeroWeightNewShippingTitle());
					if (shippingHandling.getZeroWeightNewPriceCost() != null) {
						flatRate.setPrice(shippingHandling.getZeroWeightNewPriceCost().toString());
					}
					finalList.add(flatRate);
				} else {
					order.setTotalWeight(1.0);
				}
			}
			if (order.getTotalWeight() - order.getCustomShippingTotalWeight() > 0) {
				// check if any custom shipping ignores carriers like UPS, USPS and FedEx
				boolean ignoreCarriers = false;
				for (ShippingRate cRate : customList) {
					if (cRate.isIgnoreCarriers()) {
						ignoreCarriers = true;
						break;
					}
				}

				if (!ignoreCarriers) {

					if (siteConfig.get("SHIPPING_PER_SUPPLIER").getValue().equals("true")) {
						ShippingRateQuotePerSupplier shippingRateQuotePerSupplier = new ShippingRateQuotePerSupplier();
						finalList = shippingRateQuotePerSupplier.getShippingRates(request, order, this, shippingHandling.getHandlingTotal(order.getSubTotal(), order.getTotalWeight()));
					} else {
						ShippingRateQuote shippingRateQuote = new ShippingRateQuote();
						finalList = shippingRateQuote.getShippingRates(request, order, this, shippingHandling.getHandlingTotal(order.getSubTotal(), order.getTotalWeight()));
					}

					if (!finalList.isEmpty()) {
						Iterator<ShippingRate> iter = finalList.iterator();
						while (iter.hasNext()) {
							ShippingRate sRate = iter.next();
							if (sRate.getMinPrice() != null && order.getSubTotal() < sRate.getMinPrice()) {
								iter.remove();
								continue;
							}
							if (sRate.getMaxPrice() != null && order.getSubTotal() > sRate.getMaxPrice()) {
								iter.remove();
								continue;
							}
						}
					}
				}
			}
			// find out custom shipping with max value from all options that has category id and remove those shipping options
			// that have category id but value is less than max value
			Double handlingCostByZipcode = null;
			if ((Boolean) gSiteConfig.get("gCUSTOM_SHIPPING_ADVANCED") && !customList.isEmpty()) {
				this.getCustomShippingWithMaxValue(customList, finalList, order);
				handlingCostByZipcode = this.getHandlingByZipcode(order.getShipping().getCountry(), order.getShipping().getZip());
			}
			if (!customList.isEmpty()) {
				Iterator<ShippingRate> iter = customList.iterator();
				while (iter.hasNext()) {
					ShippingRate cRate = iter.next();
					if (cRate.getCarrierId() != null) {
						for (ShippingRate sRate : finalList) {
							if (sRate.getId() == cRate.getCarrierId()) {
								sRate.setTitle(cRate.getTitle());
								if (cRate.getPercent() == 0 && (cRate.getPrice() != null || cRate.getHandling() != null)) {
									Double price = Double.parseDouble(cRate.getPrice() != null ? cRate.getPrice() : "0.0") + (cRate.getHandling() != null ? cRate.getHandling() : 0.0)
											+ (handlingCostByZipcode != null ? handlingCostByZipcode : 0.0);
									sRate.setPrice(price.toString());
								} else if (cRate.getPercent() == 1 && (cRate.getPrice() != null || cRate.getHandling() != null)) {
									Double discountedPrice = ((100 - Double.parseDouble(cRate.getPrice() != null ? cRate.getPrice() : "0.0")) / 100) * Double.parseDouble(sRate.getPrice())
											+ (cRate.getHandling() != null ? cRate.getHandling() : 0.0) + (handlingCostByZipcode != null ? handlingCostByZipcode : 0.0);
									sRate.setPrice(discountedPrice.toString());
								}
							}
						}
						iter.remove();
					} else if (cRate.getPercent() == 0 && (cRate.getPrice() != null || cRate.getHandling() != null)) {
						Double discountedPrice = Double.parseDouble(cRate.getPrice() != null ? cRate.getPrice() : "0.0") + (cRate.getHandling() != null ? cRate.getHandling() : 0.0)
								+ (handlingCostByZipcode != null ? handlingCostByZipcode : 0.0);
						cRate.setPrice(discountedPrice.toString());
					} else if (cRate.getPercent() == 2 && (cRate.getPrice() != null || cRate.getHandling() != null)) {
						Double discountedPrice = (Double.parseDouble(cRate.getPrice() != null ? cRate.getPrice() : "0.0") / 100) * order.getSubTotal()
								+ (cRate.getHandling() != null ? cRate.getHandling() : 0.0) + (handlingCostByZipcode != null ? handlingCostByZipcode : 0.0);
						cRate.setPrice(discountedPrice.toString());
					} else if (cRate.getPercent() == 3 && (cRate.getPrice() != null || cRate.getHandling() != null)) {
						Double discountedPrice = (Double.parseDouble(cRate.getPrice() != null ? cRate.getPrice() : "0.0")) * (order.getTotalWeight() != null ? order.getTotalWeight() : 0)
								+ (cRate.getHandling() != null ? cRate.getHandling() : 0.0) + (handlingCostByZipcode != null ? handlingCostByZipcode : 0.0);
						cRate.setPrice(discountedPrice.toString());
					}
				}
				finalList.addAll(0, customList);
			}
		} else
		// shipping by flat, weight or price
		{
			flatRate.setTitle(shippingHandling.getShippingTitle());
			if (shippingHandling.getShippingMethod().equals("flt")) {
				flatRate.setPrice(shippingHandling.getShippingBase().toString());
			} else {
				Double total = shippingHandling.getShippingTotal(order.getSubTotal(), order.getTotalWeight()) + shippingHandling.getHandlingTotal(order.getSubTotal(), order.getTotalWeight());
				flatRate.setPrice(total.toString());
			}
			finalList.add(flatRate);
			if (!customList.isEmpty()) {
				finalList.addAll(0, customList);
			}
		}

		if ((Boolean) gSiteConfig.get("gROAD_RUNNER")) {
			QuoteResponse qRes = null;
			try {
				qRes = this.getRoadRunnerQuote(order, siteConfig.get("RR_USERNAME").getValue(), siteConfig.get("RR_PASSWORD").getValue(), siteConfig.get("RR_ORIGIN_ZIP").getValue(),
						siteConfig.get("RR_ORIGIN_TYPE").getValue(), siteConfig.get("RR_PAYMENT_TYPE").getValue(), Integer.parseInt(siteConfig.get("RR_ACTUAL_CLASS").getValue()));
			} catch (Exception e) {
				 e.printStackTrace();
			}

			if (qRes != null) {
				ShippingRate rate = new ShippingRate("Road Runner", "" + qRes.getNetCharge());
				finalList.add(rate);
			}
		}

		// if shipping rate list is empty, than display TBD so that customer can checkout
		if (finalList.isEmpty()) {
			flatRate.setTitle(shippingHandling.getTbdTitle());
			finalList.add(flatRate);
		}

		return finalList;
	}

	public List<ShippingRate> calculateTileShowRoomShipping(Order order, Customer customer, HttpServletRequest request, Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig) {

		List<String> outsideUSStates = new ArrayList<String>();
		outsideUSStates.add("AK");
		outsideUSStates.add("HI");
		outsideUSStates.add("AS");
		outsideUSStates.add("GU");
		outsideUSStates.add("MP");
		outsideUSStates.add("PR");
		outsideUSStates.add("VI");
		outsideUSStates.add("FM");
		outsideUSStates.add("PW");

		// check if order is outside US
		if ((!order.getShipping().getCountry().equalsIgnoreCase("US")) || (order.getShipping().getCountry().equalsIgnoreCase("US") && outsideUSStates.contains(order.getShipping().getStateProvince()))) {
			return calculateTileShowRoomIntlShipping(order, customer, request, gSiteConfig, siteConfig);
		}

		List<ShippingRate> finalList = new ArrayList<ShippingRate>();

		Double orderWeight = order.getTotalWeight();
		Double orderSubtotal = order.getSubTotal();

		Class<Product> c = Product.class;
		Method m = null;
		Object arglist[] = null;

		Integer shippingCategory = 0;
		for (LineItem lineItem : order.getLineItems()) {
			Product product = getProductById(lineItem.getProductId(), 0, false, null);
			if (siteConfig.get("CUSTOM_SHIPPING_FIELD1").getValue().length() > 0 || siteConfig.get("CUSTOM_SHIPPING_FIELD2").getValue().length() > 0) {
				try {
					m = c.getMethod("getField" + siteConfig.get("CUSTOM_SHIPPING_FLAG").getValue());
					if (((String) m.invoke(product, arglist)).equals("0")) {
						for (int i = 1; i < 3; i++) {
							m = c.getMethod("getField" + siteConfig.get("CUSTOM_SHIPPING_FIELD" + i).getValue());
							String value = (String) m.invoke(product, arglist);
							if (value != null && value.length() > 0) {
								orderWeight = orderWeight - lineItem.getProduct().getWeight() * lineItem.getQuantity();
								orderSubtotal = orderSubtotal - lineItem.getUnitPrice() * lineItem.getQuantity();
								break;
							}
						}
					}
				} catch (Exception e) {
					// do nothing
				}
			}
			if (product.getField20() != null && product.getField20().length() > 0) {
				try {
					shippingCategory = shippingCategory >= Integer.parseInt(product.getField20()) ? shippingCategory : Integer.parseInt(product.getField20());
				} catch (Exception e) {
				}
			}
		}

		if (shippingCategory == 0) {
			return getRegularShipping(order, request, true);
		}
		Double shippingCost = null;

		ShippingRate rate = new ShippingRate();
		rate.setTitle("Freight");

		if (shippingCategory == 1 && customer.getPriceTable() == 0) {
			if (orderSubtotal <= 299) {
				if (orderWeight < 150) {
					finalList = getRegularShipping(order, request, true);
				} else if (orderWeight >= 150 && orderWeight < 500) {
					shippingCost = 325.0;
				} else {
					shippingCost = 325.0 + 0.5 * (orderWeight - 500);
				}
			} else {
				rate.setTitle("Free Shipping");
				shippingCost = 0.0;
			}
		} else if (shippingCategory == 2 || (shippingCategory == 1 && customer.getPriceTable() != 0)) {
			if (orderWeight < 150) {
				finalList = getRegularShipping(order, request, true);
			} else if (orderWeight >= 150 && orderWeight < 500) {
				shippingCost = 325.0;
			} else {
				shippingCost = 325.0 + 0.5 * (orderWeight - 500);
			}
		} else if (shippingCategory == 3) {
			if (orderWeight < 500) {
				shippingCost = 325.0;
			} else if (orderWeight >= 500) {
				shippingCost = 325.0 + 0.5 * (orderWeight - 500);
			}
		}

		if (shippingCost != null) {
			rate.setPrice(shippingCost.toString());
			finalList.add(rate);
		}

		return finalList;
	}

	public List<ShippingRate> getRegularShipping(Order order, HttpServletRequest request, boolean tileShowroom) {
		List<ShippingRate> shippingList = new ArrayList<ShippingRate>();
		ShippingHandling shippingHandling = getShippingHandling();

		Map<String, Configuration> siteConfig = getSiteConfig();

		if (siteConfig.get("SHIPPING_PER_SUPPLIER").getValue().equals("true")) {
			ShippingRateQuotePerSupplier shippingRateQuotePerSupplier = new ShippingRateQuotePerSupplier();
			shippingList = shippingRateQuotePerSupplier.getShippingRates(request, order, this, shippingHandling.getHandlingTotal(order.getSubTotal(), order.getTotalWeight()));
		} else {
			ShippingRateQuote shippingRateQuote = new ShippingRateQuote();
			shippingList = shippingRateQuote.getShippingRates(request, order, this, shippingHandling.getHandlingTotal(order.getSubTotal(), order.getTotalWeight()));
		}

		if (tileShowroom) {
			Iterator<ShippingRate> itr = shippingList.iterator();
			while (itr.hasNext()) {
				ShippingRate sRate = itr.next();
				if (!(sRate.getCarrier().equalsIgnoreCase("fedex") || sRate.getCarrier().equalsIgnoreCase("ups"))) {
					itr.remove();
				}
			}
		}
		return shippingList;
	}

	public List<ShippingRate> getViatradingRegularShipping(Order order, HttpServletRequest request, int shippingCategory) {
		List<ShippingRate> shippingList = new ArrayList<ShippingRate>();

		shippingList = this.getRegularShipping(order, request, false);
		if (shippingCategory < 4) {
			Iterator<ShippingRate> itr = shippingList.iterator();
			while (itr.hasNext()) {
				ShippingRate sRate = itr.next();
				if (!(sRate.getCarrier().equalsIgnoreCase("ups"))) {
					itr.remove();
				}
			}
		}
		return shippingList;
	}

	private List<ShippingRate> calculateTileShowRoomIntlShipping(Order order, Customer customer, HttpServletRequest request, Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig) {

		List<ShippingRate> finalList = new ArrayList<ShippingRate>();

		Double orderWeight = order.getTotalWeight();
		Integer shippingCategory = null;

		for (LineItem lineItem : order.getLineItems()) {
			Product product = getProductById(lineItem.getProductId(), 0, false, null);
			if (shippingCategory == null && product.getField20() != null && product.getField20().length() > 0) {
				try {
					shippingCategory = Integer.parseInt(product.getField20());
				} catch (Exception e) {
				}
			}
		}
		if (shippingCategory == null) {
			return getRegularShipping(order, request, true);
		}

		Double shippingCost = null;
		// for canada and mexico
		if (order.getShipping().getCountry().equalsIgnoreCase("CA") || order.getShipping().getCountry().equalsIgnoreCase("MX")) {
			ShippingRate rate = new ShippingRate();
			rate.setTitle("Freight");

			if (shippingCategory == 1 || shippingCategory == 2) {
				if (orderWeight < 150) {
					finalList = getRegularShipping(order, request, true);
				} else if (orderWeight >= 150 && orderWeight <= 500) {
					shippingCost = 500.0;
				} else {
					shippingCost = 500.0 + 0.5 * (orderWeight - 500);
				}

			} else if (shippingCategory == 3) {
				if (orderWeight <= 500) {
					shippingCost = 500.0;
				} else {
					shippingCost = 500.0 + 0.5 * (orderWeight - 500);
				}
			}
			if (shippingCost != null) {
				rate.setPrice(shippingCost.toString());
				finalList.add(rate);
			}
		} else {
			// for any other country including Hawaii, Alaska and other US possessions outside 48 states
			finalList = getRegularShipping(order, request, true);
		}
		return finalList;
	}

	public List<ShippingRate> calculateViatradingShipping(Order order, GlobalDao globalDao, HttpServletRequest request, Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig) {

		List<ShippingRate> finalList = new ArrayList<ShippingRate>();

		// if order source zip code is different than site config, use order source zip code
		String sourceZipCode = siteConfig.get("SOURCE_ZIP").getValue();
		if (order.getSource() != null && order.getSource().getZip() != null && !order.getSource().getZip().isEmpty()) {
			sourceZipCode = order.getSource().getZip();
		}

		// if any lineitem has zero weight call for quote
		for (LineItem item : order.getLineItems()) {
			if (item.getPackageWeight() == 0.0) {
				ShippingRate rate = new ShippingRate("Call for Quote", null);
				order.setShippingMessage("We are sorry but your order contains an item that cannot currently be quoted automatically. Please call or email us for a quote. Thank you!");
				finalList.add(rate);
				return finalList;
			}
		}

		int shippingCategory = 0;
		int packingCategory = 1;
		if (order.getShipping().getCountry().equalsIgnoreCase("US")) {

		}
		Double shippingDistance = null;

		if (order.getShipping().getCountry().equals("US")) {
			List<String> outsideSoCalZone = new ArrayList<String>();
			outsideSoCalZone.add("CA");
			outsideSoCalZone.add("AZ");
			outsideSoCalZone.add("NV");

			List<String> outsideUSStates = new ArrayList<String>();
			outsideUSStates.add("AK");
			outsideUSStates.add("HI");
			outsideUSStates.add("AS");
			outsideUSStates.add("GU");
			outsideUSStates.add("MP");
			outsideUSStates.add("PR");
			outsideUSStates.add("VI");
			outsideUSStates.add("FM");
			outsideUSStates.add("PW");

			try {
				// get first five digit of zip code
				order.getShipping().setZip(order.getShipping().getZip().substring(0, 5));
			} catch (Exception e) {
			}
			shippingDistance = getDistanceBetweenZipCode(sourceZipCode, order.getShipping().getZip(), globalDao);

			if (shippingDistance != null && shippingDistance <= 150) {
				shippingCategory = 1;
			} else if (outsideSoCalZone.contains(order.getShipping().getStateProvince())) {
				shippingCategory = 2;
			} else if (!outsideUSStates.contains(order.getShipping().getStateProvince()) && !outsideSoCalZone.contains(order.getShipping().getStateProvince())) {
				shippingCategory = 3;
			} else if (outsideUSStates.contains(order.getShipping().getStateProvince())) {
				shippingCategory = 4;
			}
		} else if (order.getShipping().getCountry().equals("CA")) {
			shippingCategory = 4;
		} else {
			shippingCategory = 5;
		}

		boolean isCasePresent = false;
		for (LineItem item : order.getLineItems()) {
			if (item.getProduct().getPacking() != null) {
				if (item.getProduct().getPacking().equalsIgnoreCase("Case") && !isCasePresent) {
					isCasePresent = true;
				}
				if ((item.getProduct().getPacking().equalsIgnoreCase("Pallet") || (item.getProduct().getPacking().equalsIgnoreCase("Bin"))) && packingCategory < 2) {
					packingCategory = 2;
				} else if (item.getProduct().getPacking().equalsIgnoreCase("Load") && packingCategory < 3) {
					packingCategory = 3;
				}
			}
		}

		double totalQuantity = 0;
		if (packingCategory > 1) {
			for (LineItem item : order.getLineItems()) {
				item.getProduct().setField15(getProductFieldById(item.getProduct().getId(), 15));
				if (item.getProduct().getPacking() != null) {
					if (item.getProduct().getPacking().equalsIgnoreCase("Pallet") || item.getProduct().getPacking().equalsIgnoreCase("Bin") || item.getProduct().getPacking().equalsIgnoreCase("Load")) {
						if (item.getProduct().getField15() != null && !item.getProduct().getField15().isEmpty()) {
							totalQuantity = totalQuantity + (item.getQuantity() * Double.parseDouble(item.getProduct().getField15().toString()));
						} else {
							totalQuantity = totalQuantity + item.getQuantity();
						}
					}
				}
			}
		}
		ShippingHandling shippingHandling = this.getShippingHandling();
		double handlingCost = 0.0;
		if (shippingHandling != null) {
			handlingCost = shippingHandling.getHandlingTotal(order.getSubTotal(), order.getTotalWeight());
			int decimalPlace = 2;
			BigDecimal bd = new BigDecimal(handlingCost);
			bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
			handlingCost = bd.doubleValue();
		}
		switch (shippingCategory) {
		case 1:
			if (packingCategory == 1) {
				// regular shipping
				finalList.addAll(getViatradingRegularShipping(order, request, shippingCategory));
				order.setShippingMessage("<p>Please note, shipping more than one case at the same time can significantly reduce your shipping cost per case.<br /> If you have over 12 cases in your shipment, please call or email to see if we can reduce your shipping costs by shipping with an LTL carrier.</p>");
			} else if (totalQuantity <= 6) {
				// best overnite
				try {
					ShippingRate rate = this.getBestOvernite(sourceZipCode, order.getShipping().getZip(), "00263405", order, handlingCost);
					if (rate != null) {
						finalList.add(rate);
					}
				} catch (Exception e) {
				}
				// soCal quote
				ShippingRate soCalRate = getSoCalQuote(shippingDistance, handlingCost);
				if (soCalRate != null) {
					order.setShippingMessage("Please note, our SoCal shipping special is a flat-rate shipping cost for up to 12 pallets at a time. This cost includes residential delivery with a liftgate if needed at no extra charge.");
					finalList.add(soCalRate);
				}
			} else if (totalQuantity > 6 && totalQuantity <= 12) {
				// soCal quote

				ShippingRate soCalRate = getSoCalQuote(shippingDistance, handlingCost);
				if (soCalRate != null) {
					order.setShippingMessage("Please note, our SoCal shipping special is a flat-rate shipping cost for up to 12 pallets at a time. This cost includes residential delivery with a liftgate if needed at no extra charge.");
					finalList.add(soCalRate);
				}
			} else if (totalQuantity > 12) {
				// call for quote
				ShippingRate rate = new ShippingRate("Call for Quote", null);
				order.setShippingMessage("<p>We are sorry but our system cannot currently quote full truckloads online and certain LTL carriers limit the number of pallets that can be shipped in a single LTL shipment.</p> <p>If you are ordering 8 pallets, try calculating the quote for 2 orders of 4 pallets each. <br /> Alternatively, try reducing the number of pallets in the order to under 8 (West Coast) or under 7 (all other states). </p> <p>If you are trying to get a quote for a full load or if you need assistance of any kind please call or email us to obtain an accurate quote. Thank you.</p>");
				finalList.add(rate);
			}
			break;
		case 2:
			if (packingCategory == 1) {
				// regular shipping
				finalList.addAll(getViatradingRegularShipping(order, request, shippingCategory));
				order.setShippingMessage("<p>Please note, shipping more than one case at the same time can significantly reduce your shipping cost per case.<br /> If you have over 12 cases in your shipment, please call or email to see if we can reduce your shipping costs by shipping with an LTL carrier.</p>");
			} else if (totalQuantity <= 6) {
				// best overnite
				try {
					ShippingRate rate = this.getBestOvernite(sourceZipCode, order.getShipping().getZip(), "00263405", order, handlingCost);
					if (rate != null) {
						finalList.add(rate);
					}
					if (isCasePresent) {
						order.setShippingMessage("<p>Please note, shipping more than one pallet at a time can significantly reduce your shipping cost per pallet.</p> <p>Adding additional case packs to your order will increase your shipping cost slightly, but decrease the overall amount you pay to ship each item.</p>");
					} else {
						order.setShippingMessage("<p>Please note, since you are ordering a pallet that will ship LTL, adding 1 or more case packs to your current order will increase your shipping cost slightly, but decrease the overall amount you pay to ship each item.</p> <p>Please note, shipping more than one pallet at the same time can significantly reduce your shipping cost per pallet.</p>");
					}
				} catch (Exception e) {
				}

			} else {
				// call for quote
				ShippingRate rate = new ShippingRate("Call for Quote", null);
				order.setShippingMessage("<p>We are sorry but our system cannot currently quote full truckloads online and certain LTL carriers limit the number of pallets that can be shipped in a single LTL shipment.</p> <p>If you are ordering 8 pallets, try calculating the quote for 2 orders of 4 pallets each. <br /> Alternatively, try reducing the number of pallets in the order to under 8 (West Coast) or under 7 (all other states). </p> <p>If you are trying to get a quote for a full load or if you need assistance of any kind please call or email us to obtain an accurate quote. Thank you.</p>");
				finalList.add(rate);
			}
			break;
		case 3:
			if (packingCategory == 1) {
				// regular shipping
				finalList.addAll(getViatradingRegularShipping(order, request, shippingCategory));
				order.setShippingMessage("<p>Please note, shipping more than one case at the same time can significantly reduce your shipping cost per case.<br /> If you have over 12 cases in your shipment, please call or email to see if we can reduce your shipping costs by shipping with an LTL carrier.</p>");
			} else if (totalQuantity <= 7) {
				// RoadRunner
				QuoteResponse qRes = null;

				try {
					qRes = getRoadRunnerQuote(order, siteConfig.get("RR_USERNAME").getValue(), siteConfig.get("RR_PASSWORD").getValue(), siteConfig.get("RR_ORIGIN_ZIP").getValue(),
							siteConfig.get("RR_ORIGIN_TYPE").getValue(), siteConfig.get("RR_PAYMENT_TYPE").getValue(), Integer.parseInt(siteConfig.get("RR_ACTUAL_CLASS").getValue()));
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (qRes != null) {
					ShippingRate rate = new ShippingRate();
					rate.setTitle("Road Runner");
					rate.setPrice("" + (qRes.getNetCharge() + handlingCost));
					finalList.add(rate);

					if (isCasePresent) {
						order.setShippingMessage("<p>Please note, shipping more than one pallet at a time can significantly reduce your shipping cost per pallet.</p> <p>Adding additional case packs to your order will increase your shipping cost slightly, but decrease the overall amount you pay to ship each item.</p>");
					} else {
						order.setShippingMessage("<p>Please note, since you are ordering a pallet that will ship LTL, adding 1 or more case packs to your current order will increase your shipping cost slightly, but decrease the overall amount you pay to ship each item.</p> <p>Please note, shipping more than one pallet at the same time can significantly reduce your shipping cost per pallet.</p>");
					}
				}
			} else {
				// call for quote
				ShippingRate rate = new ShippingRate("Call for Quote", null);
				order.setShippingMessage("<p>We are sorry but our system cannot currently quote full truckloads online and certain LTL carriers limit the number of pallets that can be shipped in a single LTL shipment.</p> <p>If you are ordering 8 pallets, try calculating the quote for 2 orders of 4 pallets each. <br /> Alternatively, try reducing the number of pallets in the order to under 8 (West Coast) or under 7 (all other states). </p> <p>If you are trying to get a quote for a full load or if you need assistance of any kind please call or email us to obtain an accurate quote. Thank you.</p>");
				finalList.add(rate);
			}
			break;

		case 4:
			if (packingCategory == 1) {
				// regular shipping
				finalList.addAll(getViatradingRegularShipping(order, request, shippingCategory));
				order.setShippingMessage("<p>Please note, shipping more than one case at the same time can significantly reduce your shipping cost per case.<br /> If you have over 12 cases in your shipment, please call or email to see if we can reduce your shipping costs by shipping with an LTL carrier.</p>");
			} else {
				// call for quote
				ShippingRate rate = new ShippingRate("Call for Quote", null);
				order.setShippingMessage("Please note our online system can only quote orders for merchandise by the case. Please call or email us to obtain a shipping quote for pallets or loads going to any international or non-continental USA destination. Thank you.");
				finalList.add(rate);
			}
			break;

		default:
			if (packingCategory == 1) {
				// regular shipping
				finalList.addAll(getViatradingRegularShipping(order, request, shippingCategory));
				order.setShippingMessage("<p>Please note, shipping more than one case at the same time can significantly reduce your shipping cost per case.<br /> If you have over 12 cases in your shipment, please call or email to see if we can reduce your shipping costs by shipping with an LTL carrier.</p>");
			} else {
				// call for quote
				ShippingRate rate = new ShippingRate("Call for Quote", null);
				order.setShippingMessage("Please note our online system can only quote orders for merchandise by the case. Please call or email us to obtain a shipping quote for pallets or loads going to any international or non-continental USA destination. Thank you.");
				finalList.add(rate);
			}
			break;
		}

		// if there is no option available, call for quote
		if (finalList.isEmpty()) {
			ShippingRate rate = new ShippingRate("Call for Quote", null);
			order.setShippingMessage("Unfortunately not all orders can currently be quoted online. Please call or email us for an immediate shipping quote. Thank you. ");
			finalList.add(rate);
		}

		// temporory markup ( 12% )
		for (ShippingRate rate : finalList) {
			if (rate.getPrice() != null && rate.getTitle() != null && !rate.getTitle().equalsIgnoreCase("SoCal")) {
				rate.setPrice(new Double(Double.parseDouble(rate.getPrice()) * 1.12).toString());
			}
		}

		return finalList;
	}

	public Double getDistanceBetweenZipCode(String sourceZip, String destZip, GlobalDao globalDao) {
		ZipCode z1 = globalDao.getZipCode(sourceZip);
		ZipCode z2 = globalDao.getZipCode(destZip);
		Double distance = null;

		if (z1 != null && z2 != null) {
			distance = Utilities.getDistance(z1.getLatitude(), z1.getLongitude(), z2.getLatitude(), z2.getLongitude());
		}

		return distance;
	}

	private void rodRunnerHeader(RateQuoteSoap port, String username, String password) {

		WSBindingProvider bp = (WSBindingProvider) port;
		AuthenticationHeader authenticationHeader = new AuthenticationHeader();
		authenticationHeader.setUserName(username);
		authenticationHeader.setPassword(password);

		bp.setOutboundHeaders(authenticationHeader);
	}

	public QuoteResponse getRoadRunnerQuote(Order order, String username, String password, String originZip, String originType, String paymentType, int actualClass) throws MalformedURLException {
		RateQuote_Service service = new RateQuote_Service(new URL("https://webservices.rrts.com/rating/ratequote.asmx"), new QName("https://webservices.rrts.com/ratequote/", "RateQuote"));
		RateQuoteSoap port = service.getRateQuoteSoap();

		rodRunnerHeader(port, username, password);
		QuoteRequest req = new QuoteRequest();

		req.setOriginZip(originZip);
		req.setDestinationZip(order.getShipping().getZip().substring(0, 5));

		ArrayOfShipmentDetail arraySD = new ArrayOfShipmentDetail();
		ShipmentDetail detail = new ShipmentDetail();
		detail.setActualClass(actualClass);
		detail.setWeight(order.getTotalWeight().intValue());
		arraySD.getShipmentDetail().add(detail);
		req.setShipmentDetails(arraySD);

		req.setOriginType(originType);
		req.setPaymentType(paymentType);

		ArrayOfServiceOptions arraySO = new ArrayOfServiceOptions();
		if (order.getShipping().isResidential()) {
			ServiceOptions options = new ServiceOptions();
			options.setServiceCode("RSD");
			arraySO.getServiceOptions().add(options);
		}
		if (order.getShipping().isLiftGate()) {
			ServiceOptions lgOptions = new ServiceOptions();
			lgOptions.setServiceCode("LGD");
			arraySO.getServiceOptions().add(lgOptions);
		}
		if (!arraySO.getServiceOptions().isEmpty()) {
			req.setServiceDeliveryOptions(arraySO);
		}

		QuoteResponse qResp = port.rateQuote(req);

		return qResp;
	}

	public ShippingRate getBestOvernite(String sourceZip, String destZip, String shipperNumber, Order order, double handlingCost) {

		Double quote = null;
		ShippingRate rate = null;
		URL bo = null;
		StringBuffer urlParam = new StringBuffer();
		urlParam.append("http://tgif.bestovernite.com/cgi-bin/wbrate?Action=HTML&wbshnbr=" + shipperNumber + "&wbppd=" + "p" + "&wbsszip=" + sourceZip + "&wbsczip=" + destZip);
		for (int i = 0; i < order.getLineItems().size(); i++) {
			LineItem lineItem = order.getLineItems().get(i);
			double quantity = lineItem.getQuantity();
			if (lineItem.getProduct().getPacking() != null) {
				if (lineItem.getProduct().getPacking().equalsIgnoreCase("Pallet") || lineItem.getProduct().getPacking().equalsIgnoreCase("Bin")
						|| lineItem.getProduct().getPacking().equalsIgnoreCase("Load")) {
					if (lineItem.getProduct().getField15() != null && !lineItem.getProduct().getField15().isEmpty()) {
						double fieldValue = 1;
						try {
							fieldValue = Double.parseDouble(lineItem.getProduct().getField15());
						} catch (Exception e) {
							e.printStackTrace();
						}
						quantity = lineItem.getQuantity() * fieldValue;
					}
				}
			}
			urlParam.append("&wbspc" + (i + 1) + "=" + quantity);
			urlParam.append("&wbsds" + (i + 1) + "=" + order.getLineItems().get(i).getProduct().getSku());
			// bestovernite behaves weirdly if weight is double. Convert it to integer.
			urlParam.append("&wbswt" + (i + 1) + "=" + Math.round(Float.parseFloat(order.getLineItems().get(i).getPackageWeight().toString())) * lineItem.getQuantity());
			urlParam.append("&wbscl" + (i + 1) + "=500");
		}
		// liftgate required for delivery
		if (order.getShipping().isLiftGate()) {
			urlParam.append("&wbckld=checked");
		}
		// delivery is to a residential area
		if (order.getShipping().isResidential()) {
			urlParam.append("&wbckrd=checked");
		}

		try {
			bo = new URL(urlParam.toString());
			URLConnection bestOvernite = bo.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(bestOvernite.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				quote = Double.parseDouble(inputLine.split(",")[1]);
			}
			rate = new ShippingRate();
			rate.setTitle("Best Overnite");
			rate.setPrice("" + (quote + handlingCost));
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return rate;
	}

	public ShippingRate getSoCalQuote(double distance, double handling) {

		ShippingRate rate = null;
		if (distance <= 150) {
			rate = new ShippingRate();
			rate.setTitle("SoCal");
			if (distance <= 50) {
				rate.setPrice("" + (150.00 + handling));
			} else if (distance <= 100) {
				rate.setPrice("" + (200.00 + handling));
			} else if (distance <= 150) {
				rate.setPrice("" + (250.00 + handling));
			}

		}
		return rate;
	}

	// amazon checkout
	public void updateAmazonPayment(Iopn iopn, OrderStatus orderStatus, List<LineItem> lineItems) {
		this.orderDao.updateAmazonPayment(iopn, orderStatus);
		if (lineItems != null && !lineItems.isEmpty()) {
			this.orderDao.updateAmazonLineItem(lineItems);
		}
	}

	// jbd
	public void insertJbdInvoices(Connection connection, List<Order> orderList, Date cutoff) throws Exception {

		Map<Integer, String> customers = new HashMap<Integer, String>();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

		Map<Integer, SalesRep> salesRepMap = new HashMap<Integer, SalesRep>();
		for (SalesRep salesRep : getSalesRepList()) {
			salesRepMap.put(salesRep.getId(), salesRep);
		}

		PreparedStatement insertLine = connection.prepareStatement("INSERT INTO WEBJAGUAR_ORDERS (" + "  CustID" + ", PONumber" + ", SalesOrderDate" + ", OrderNo" + ", ShipToCode" + ", ShipToName"
				+ ", ShipToAdd1" + ", ShipToAdd2" + ", ShipToCity" + ", ShipToState" + ", ShipToZip" + ", ShipToCountry" + ", ShipDate" + ", CancelDate" + ", Comment" + ", ProductNumber"
				+ ", ItemDescription" + ", Qty" + ", Unit_Price" + ", BillToName" + ", BillToAdd1" + ", BillToAdd2" + ", BillToCity" + ", BillToState" + ", BillToZip" + ", SalesPersonCode"
				+ ", PaymentMethod" + ", ShipVia" + ", Total" + ") VALUES (" + "  ?" + // CustID
				", ?" + // PONumber
				", ?" + // SalesOrderDate
				", ?" + // OrderNo
				", ?" + // ShipToCode
				", ?" + // ShipToName
				", ?" + // ShipToAdd1
				", ?" + // ShipToAdd2
				", ?" + // ShipToCity
				", ?" + // ShipToState
				", ?" + // ShipToZip
				", ?" + // ShipToCountry
				", ?" + // ShipDate
				", ?" + // CancelDate
				", ?" + // Comment
				", ?" + // ProductNumber
				", ?" + // ItemDescription
				", ?" + // Qty
				", ?" + // Unit_Price
				", ?" + // BillToName
				", ?" + // BillToAdd1
				", ?" + // BillToAdd2
				", ?" + // BillToCity
				", ?" + // BillToState
				", ?" + // BillToZip
				", ?" + // SalesPersonCode
				", ?" + // PaymentMethod
				", ?" + // ShipVia
				", ?" + // Total
				");");

		for (Order order : orderList) {
			if (!customers.containsKey(order.getUserId())) {
				Customer customer = getCustomerById(order.getUserId());
				if (customer != null) {
					customers.put(order.getUserId(), customer.getAccountNumber());
				} else {
					customers.put(order.getUserId(), null);
				}
			}

			// line items
			for (LineItem lineItem : order.getLineItems()) {
				List<Object> params = new ArrayList<Object>();
				params.add(customers.get(order.getUserId()));
				params.add(order.getPurchaseOrder());
				if (order.getDateOrdered() != null) {
					params.add(dateFormatter.format(order.getDateOrdered()));
				} else {
					params.add(null);
				}
				params.add(order.getOrderId());
				params.add(order.getShipping().getCode());
				params.add(order.getShipping().getCompany());
				params.add(order.getShipping().getAddr1());
				params.add(order.getShipping().getAddr2());
				params.add(order.getShipping().getCity());
				params.add(order.getShipping().getStateProvince());
				params.add(order.getShipping().getZip());
				params.add(order.getShipping().getCountry());
				if (order.getUserDueDate() != null) {
					params.add(dateFormatter.format(order.getUserDueDate()));
				} else {
					params.add(null);
				}
				if (order.getRequestedCancelDate() != null) {
					params.add(dateFormatter.format(order.getRequestedCancelDate()));
				} else {
					params.add(null);
				}
				params.add(order.getInvoiceNote());
				params.add(lineItem.getProduct().getSku());
				params.add(lineItem.getProduct().getName());
				params.add((lineItem.getQuantity() * ((lineItem.getProduct().getCaseContent() == null) ? 1 : lineItem.getProduct().getCaseContent())));
				params.add(lineItem.getUnitPrice());
				params.add(order.getShipping().getCompany());
				params.add(order.getShipping().getAddr1());
				params.add(order.getShipping().getAddr2());
				params.add(order.getShipping().getCity());
				params.add(order.getShipping().getStateProvince());
				params.add(order.getShipping().getZip());
				if (salesRepMap.containsKey(order.getSalesRepId())) {
					params.add(salesRepMap.get(order.getSalesRepId()).getAccountNumber());
				} else {
					params.add(null);
				}
				params.add(order.getPaymentMethod());
				params.add(order.getShippingMethod());
				params.add(order.getGrandTotal());

				int index = 1;
				for (Object param : params) {
					if (param == null) {
						insertLine.setNull(index, Types.NULL);
					} else {
						insertLine.setString(index, param.toString());
					}
					index++;
				}
				insertLine.execute();
			}
		}
		updateOrderExported("mas200", cutoff, null);
		connection.commit();
	}

	// mas200
	public void nonTransactionSafeUpdateMas200order(Order order) {
		this.orderDao.updateMas200order(order);
	}

	// Returns the contents of the file in a byte array.
	public byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		// Get the size of the file
		long length = file.length();

		// You cannot create an array using a long type.
		// It needs to be an int type.
		// Before converting to an int type, check
		// to ensure that file is not larger than Integer.MAX_VALUE.
		if (length > Integer.MAX_VALUE) {
			// File is too large
		}

		// Create the byte array to hold the data
		byte[] bytes = new byte[(int) length];

		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		// Ensure all the bytes have been read in
		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}

		// Close the input stream and return bytes
		is.close();
		return bytes;
	}

	public int[] nonTransactionSafeInsertCustomerGroup(int groupId, Set<String> accountNumber) {
		return this.customerDao.insertCustomerGroup(groupId, accountNumber);
	}

	public int[] nonTransactionSafeDeleteCustomerGroup(int groupId, Set<String> accountNumber) {
		return this.customerDao.nonTransactionSafeDeleteCustomerGroup(groupId, accountNumber);
	}

	// buy safe header
	public void buySafeHeader(BuySafeAPISoap port) {

		Map<String, Configuration> siteConfig = getSiteConfig();
		WSBindingProvider bp = (WSBindingProvider) port;
		BuySafeWSHeader bsHeader = new BuySafeWSHeader();
		bsHeader.setVersion(siteConfig.get("BUY_SAFE_VERSION").getValue());

		MerchantServiceProviderCredentials credentials = new MerchantServiceProviderCredentials();
		credentials.setUserName(siteConfig.get("BUY_SAFE_USERNAME").getValue());
		credentials.setPassword(siteConfig.get("BUY_SAFE_PASSWORD").getValue());

		BuySafeUserCredentials userCrdentials = new BuySafeUserCredentials();
		ArrayOfString stringArray = new ArrayOfString();
		stringArray.getString().add(siteConfig.get("BUY_SAFE_STORE_TOKEN").getValue());
		userCrdentials.setAuthenticationTokens(stringArray);

		bp.setOutboundHeaders(bsHeader, credentials, userCrdentials);
	}

	// buy safe add update request
	public ShoppingCartAddUpdateRS addUpdateShoppingCartRequest(Cart cart, Order order, HttpServletRequest request, Integer userId, JavaMailSenderImpl mailSender) throws MalformedURLException,
			MessagingException {

		Map<String, Configuration> siteConfig = getSiteConfig();
		BuySafeAPI buySafeService = new BuySafeAPI(new URL(siteConfig.get("BUY_SAFE_URL").getValue() + "?VER=" + siteConfig.get("BUY_SAFE_VERSION").getValue() + "&Username="
				+ siteConfig.get("BUY_SAFE_USERNAME").getValue() + "&Password=" + siteConfig.get("BUY_SAFE_PASSWORD").getValue()), new QName("http://ws.buysafe.com", "BuySafeAPI"));
		BuySafeAPISoap port = buySafeService.getBuySafeAPISoap();

		// set header
		buySafeHeader(port);
		Customer customer = null;
		if (userId != null) {
			customer = getCustomerById(userId);
		}
		// Shopping Cart Address
		ShoppingCartAddress cartAddress = new ShoppingCartAddress();
		cartAddress.setCountryCode(customer == null ? CountryCode.US : CountryCode.valueOf(customer.getAddress().getCountry()));

		// Buyer Information
		BuyerInformation buyerInfo = new BuyerInformation();
		buyerInfo.setBillingAddress(cartAddress);
		buyerInfo.setShippingAddress(cartAddress);
		buyerInfo.setFirstName(customer == null ? null : customer.getAddress().getFirstName());
		buyerInfo.setLastName(customer == null ? null : customer.getAddress().getLastName());
		buyerInfo.setEmail(customer == null ? null : customer.getAddress().getEmail());

		// for backend order
		if (cart == null && order != null) {
			Cart tempCart = new Cart();
			CartItem tempItem = null;
			for (LineItem item : order.getInsertedLineItems()) {

				tempItem = new CartItem();
				tempItem.setProduct(item.getProduct());
				tempItem.setUnitPrice(item.getUnitPrice());
				tempItem.setQuantity(item.getQuantity());

				tempCart.getCartItems().add(tempItem);
			}
			cart = tempCart;
			cart.setWantsBond(order.getWantsBond());
		}
		// ShoppingCart Items
		ShoppingCartItem cartItem;

		ArrayOfShoppingCartItem cartItems = new ArrayOfShoppingCartItem();
		PricingDetails priceDetail;
		ItemURL itemUrl;
		Set<String> skuSet = new HashSet<String>();
		for (CartItem item : cart.getCartItems()) {

			String tempSku = item.getProduct().getSku();
			for (int i = 1; i <= cart.getCartItems().size(); i++) {
				if (skuSet.contains(tempSku)) {
					tempSku = item.getProduct().getSku() + "_" + i;
				} else {
					skuSet.add(tempSku);
					break;
				}
			}

			Amount amount = new Amount();
			amount.setCurrencyCode(CurrencyCode.USD);
			amount.setValue((item.getUnitPrice() == null ? 0.0 : item.getUnitPrice()));

			priceDetail = new PricingDetails();
			priceDetail.setFinalPrice(amount);

			itemUrl = new ItemURL();
			itemUrl.setViewItem(siteConfig.get("SITE_URL").getValue() + "product.jhtm?id=" + item.getProduct().getId());

			cartItem = new ShoppingCartItem();
			cartItem.setPriceInfo(priceDetail);
			cartItem.setTitle((item.getProduct().getName() != null && !item.getProduct().getName().equals("")) ? item.getProduct().getName() : item.getProduct().getSku());
			cartItem.setQuantityPurchased(item.getQuantity());
			cartItem.setUserToken(siteConfig.get("BUY_SAFE_STORE_TOKEN").getValue());

			cartItem.setMarketplaceItemCode(tempSku);
			cartItem.setURLInfo(itemUrl);

			cartItems.getShoppingCartItem().add(cartItem);
		}
		// wants bond
		CustomBoolean wantsBond = new CustomBoolean();
		if (cart.getWantsBond() == null) {
			wantsBond.setHasBoolean(false);
			wantsBond.setValue(false);
		} else if (cart.getWantsBond() == true) {
			wantsBond.setHasBoolean(true);
			wantsBond.setValue(true);
		} else if (cart.getWantsBond() == false) {
			wantsBond.setHasBoolean(true);
			wantsBond.setValue(false);
		}

		ShoppingCartAddUpdateRQ shoppingCartRQ = new ShoppingCartAddUpdateRQ();
		shoppingCartRQ.setBuyerInfo(buyerInfo);
		shoppingCartRQ.setClientIP(request.getRemoteAddr());
		shoppingCartRQ.setSessionId(request.getSession().getId());
		shoppingCartRQ.setItems(cartItems);
		shoppingCartRQ.setShoppingCartId(request.getSession().getAttribute("buySafeCartId").toString());
		shoppingCartRQ.setWantsBond(wantsBond);

		// buyer preference
		Cookie buySafeCookie = WebUtils.getCookie(request, "buySAFEUID");
		if (buySafeCookie != null) {
			try {
				shoppingCartRQ.setSessionId(new URLDecoder().decode(buySafeCookie.getValue(), "UTF-8"));
			} catch (Exception e) {
			}
		}
		ShoppingCartAddUpdateRS response;
		BuySafeResponse resp = new BuySafeResponse(port, shoppingCartRQ, null);
		Thread t1 = new Thread(resp);
		t1.start();

		try {
			int time = 0;
			do {
				Thread.sleep(500);
				response = resp.getAddUpdateRS();
				time = time + 500;
			} while (response == null && time < 3001);
		} catch (Exception e) {
			e.printStackTrace();
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			sendMessageToBuySafe(helper, e);
			// mailSender.send(mms);
			response = null;
		}
		return response;
	}

	// buy safe check out request
	public ShoppingCartCheckoutRS setShoppingCartCheckout(Order order, JavaMailSenderImpl mailSender) throws MalformedURLException, MessagingException {

		Map<String, Configuration> siteConfig = getSiteConfig();
		BuySafeAPI buySafeService = new BuySafeAPI(new URL(siteConfig.get("BUY_SAFE_URL").getValue() + "?VER=" + siteConfig.get("BUY_SAFE_VERSION").getValue() + "&Username="
				+ siteConfig.get("BUY_SAFE_USERNAME").getValue() + "&Password=" + siteConfig.get("BUY_SAFE_PASSWORD").getValue()), new QName("http://ws.buysafe.com", "BuySafeAPI"));
		BuySafeAPISoap port = buySafeService.getBuySafeAPISoap();

		// set header
		this.buySafeHeader(port);

		Customer customer = this.getCustomerById(order.getUserId());
		// Shopping Cart Address
		ShoppingCartAddress cartAddress = new ShoppingCartAddress();
		cartAddress.setCountryCode(customer == null ? CountryCode.US : CountryCode.valueOf(customer.getAddress().getCountry()));
		cartAddress.setPostalCode(customer != null ? customer.getAddress().getZip() : null);

		// Buyer Information
		BuyerInformation buyerInfo = new BuyerInformation();
		buyerInfo.setBillingAddress(cartAddress);
		buyerInfo.setShippingAddress(cartAddress);
		buyerInfo.setFirstName(customer == null ? null : customer.getAddress().getFirstName());
		buyerInfo.setLastName(customer == null ? null : customer.getAddress().getLastName());
		buyerInfo.setEmail(customer == null ? null : customer.getUsername());

		// ShoppingCart Items
		ShoppingCartItem cartItem;

		ArrayOfShoppingCartItem cartItems = new ArrayOfShoppingCartItem();
		PricingDetails priceDetail;
		ItemURL itemUrl;
		Set<String> skuSet = new HashSet<String>();
		for (LineItem item : order.getLineItems()) {

			String tempSku = item.getProduct().getSku();
			for (int i = 1; i <= order.getLineItems().size(); i++) {
				if (skuSet.contains(tempSku)) {
					tempSku = item.getProduct().getSku() + "_" + i;
				} else {
					skuSet.add(tempSku);
					break;
				}
			}

			Amount amount = new Amount();
			amount.setCurrencyCode(CurrencyCode.USD);
			amount.setValue(item.getUnitPrice());

			priceDetail = new PricingDetails();
			priceDetail.setFinalPrice(amount);

			itemUrl = new ItemURL();
			itemUrl.setViewItem(siteConfig.get("SITE_URL").getValue() + "product.jhtm?id=" + item.getProduct().getId());

			cartItem = new ShoppingCartItem();
			cartItem.setPriceInfo(priceDetail);
			cartItem.setTitle((item.getProduct().getName() != null && !item.getProduct().getName().equals("")) ? item.getProduct().getName() : item.getProduct().getSku());
			cartItem.setQuantityPurchased(item.getQuantity());
			cartItem.setUserToken(siteConfig.get("BUY_SAFE_STORE_TOKEN").getValue());
			cartItem.setMarketplaceItemCode(tempSku);
			cartItem.setURLInfo(itemUrl);

			cartItems.getShoppingCartItem().add(cartItem);
		}
		// wants bond
		CustomBoolean wantsBond = new CustomBoolean();
		wantsBond.setHasBoolean(true);
		wantsBond.setValue(order.getWantsBond());

		ShoppingCartCheckoutRQ shoppingCartRQ = new ShoppingCartCheckoutRQ();
		shoppingCartRQ.setClientIP(order.getIpAddress());
		shoppingCartRQ.setItems(cartItems);
		shoppingCartRQ.setShoppingCartId(order.getCartId());
		shoppingCartRQ.setWantsBond(wantsBond);
		shoppingCartRQ.setOrderNumber(order.getOrderId().toString());
		shoppingCartRQ.setBuyerInfo(buyerInfo);

		ShoppingCartCheckoutRS response;
		// response should be within 3 sec. Need to implement.
		BuySafeResponse resp = new BuySafeResponse(port, null, shoppingCartRQ);
		Thread t1 = new Thread(resp);
		t1.start();

		try {
			int time = 0;
			do {
				Thread.sleep(500);
				response = resp.getCheckOutRS();
				time = time + 500;
			} while (response == null && time < 3001);
		} catch (Exception e) {
			e.printStackTrace();
			MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			sendMessageToBuySafe(helper, e);
			mailSender.send(mms);
			response = null;
		}
		return response;
	}

	public void sendMessageToBuySafe(MimeMessageHelper helper, Exception e) throws MessagingException {
		helper.setTo("apisupport@buysafe.com");
		// cc emails
		helper.addCc("jwalant@advancedemedia.com");

		helper.setFrom("jwalant@advancedemdia.com");

		helper.setSubject("Error");
		helper.setText(e.toString());

	}

	// GE Money
	public void updateGEMoneyPayment(GEMoney trans, OrderStatus orderStatus) {
		this.orderDao.updateGEMoneyPayment(trans, orderStatus);
	}

	// Plow & Hearth
	public void updatePlowAndHearthLineItems(List<LineItem> lineItems) {
		this.orderDao.updatePlowAndHearthLineItems(lineItems);
	}

	// product variant
	public List<Product> getProductVariant(int productId, String sku, boolean backEnd) {
		return this.productDao.getProductVariant(productId, sku, backEnd);
	}

	// Kit

	public void deismantelKit(Product kit) {
		List<KitParts> kitPartsList = getKitPartsByKitSku(kit.getSku());
		kit.setKitParts(kitPartsList);
		if (!kit.getKitParts().isEmpty() && kit.getInventory() != null) {
			Inventory inventory = new Inventory();
			InventoryActivity inventoryActivity = new InventoryActivity();
			for (KitParts kitParts : kit.getKitParts()) {
				inventory.setSku(kitParts.getKitPartsSku());
				inventoryActivity.setSku(inventory.getSku());
				inventory = getInventory(inventory);
				if (inventory.getInventory() != null) {
					inventoryActivity.setInventory(inventory.getInventory());
					inventory.setInventory(kit.getInventory() * kitParts.getQuantity());
					inventory.setInventoryAFS(kit.getInventory() * kitParts.getQuantity());
					inventory.setSku(kitParts.getKitPartsSku());
					updateInventory(inventory);
					inventoryActivity.setType("adjustment");
					inventoryActivity.setNote("Kit Dismantel");
					inventoryActivity.setQuantity(kit.getInventory() * kitParts.getQuantity());
					addKitPartsHistory(inventoryActivity);
				}
			}
		}

	}

	public List<KitParts> getKitPartsByKitSku(String sku) {
		return this.productDao.getKitPartsByKitSku(sku);
	}

	//Simply adds inventory history
	public void addKitPartsHistory(InventoryActivity kitActivity) {
		this.inventoryDao.addKitPartsHistory(kitActivity);
	}

	// get region
	public Map<String, Object> getRegionMap() {
		return this.configDao.getRegionMap();
	}

	public List<Country> getRegion() {
		return this.configDao.getRegion();
	}

	// pre-hanging
	public List<Map<String, Object>> getPreHanging(String doorConfig, String species, String widthHeight, String optionCode) {
		return this.productDao.getPreHanging(doorConfig, species, widthHeight, optionCode);
	}

	public int[] updatePreHanging(final List<Map<String, Object>> data) {
		return this.productDao.updatePreHanging(data);
	}

	public void deletePreHanging(int id) {
		this.productDao.deletePreHanging(id);
	}

	public void insertPreHanging(List<Map<String, Object>> data) {
		this.productDao.insertPreHanging(data);
	}

	// order-lookup
	public List<UpsOrder> getOrderLookupReport(String customerPO) {
		return this.orderDao.getOrderLookupReport(customerPO);
	}

	public void importUpsOrders(UpsOrder upsWorldShip) {
		this.orderDao.importUpsOrders(upsWorldShip);
	}

	public void updateUpsOrders(UpsOrder upsWorldShip) {
		this.orderDao.updateUpsOrders(upsWorldShip);
	}

	public void updateUpsOrdersTrackAndShipDate(UpsOrder upsWorldShip) {
		this.orderDao.updateUpsOrdersTrackAndShipDate(upsWorldShip);
	}

	// VBA
	public List<VirtualBank> getConsignmentAndAffiliateOrders(VirtualBankOrderSearch orderSearch) {
		return this.orderDao.getConsignmentAndAffiliateOrders(orderSearch);
	}

	public void updateVbaOrders(List<Order> vbaOrders, List<LineItem> vbaOrdersLineItems, String userName) {
		CreditImp credit = new CreditImp();
		CustomerCreditHistory customerCredit = new CustomerCreditHistory();

		// Affiliate
		for (Order order : vbaOrders) {
			if (order.getVbaPaymentMethod() != null && order.getVbaPaymentMethod() != "" && order.getVbaPaymentMethod().equalsIgnoreCase("vba")) {
				if (order.getVbaAmountToPay() > 0) {
					customerCredit = credit.updateCustomerCreditByAffiliatePayment(order, "payment", userName);
				} else {
					customerCredit = credit.updateCustomerCreditByAffiliatePayment(order, "cancel", userName);
				}
				
				this.customerDao.updateCredit(customerCredit);
				/*
				 * To-be-Deleted after testing VBA Module this.customerDao.updateCredit(order.getUserId(), order.getVbaAmountToPay());
				 */
			}
		}
		// Consignment
		for (LineItem lineItem : vbaOrdersLineItems) {
			if (lineItem.getVbaPaymentMethod() != null && lineItem.getVbaPaymentMethod() != "" && lineItem.getVbaPaymentMethod().equalsIgnoreCase("vba")) {
				if (lineItem.getVbaAmountToPay() > 0) {
					customerCredit = credit.updateCustomerCreditByVBAPayment(lineItem, "payment", userName);
				} else {
					customerCredit = credit.updateCustomerCreditByVBAPayment(lineItem, "cancel", userName);
				}
				this.customerDao.updateCredit(customerCredit);
				/*
				 * To-be-Deleted after testing VBA Module this.customerDao.updateCredit(lineItem.getSupplier().getUserId(), lineItem.getVbaAmountToPay());
				 */
			}
		}
		this.orderDao.updateVbaOrders(vbaOrders, vbaOrdersLineItems, userName);
	}

	public List<VbaPaymentReport> getVbaPaymentReport(VbaPaymentReport reportFilter) {
		return this.reportDao.getVbaPaymentReport(reportFilter);
	}

	public Map<Integer, String> getCustomerCompanyMap(List<VirtualBank> useridList) {
		return this.customerDao.getCustomerCompanyMap(useridList);
	}

	public Map<Integer, Double> getPaymentsTotalToCustomerMap(List<VirtualBank> useridList) {
		return this.customerDao.getPaymentsTotalToCustomerMap(useridList);
	}

	public String getUserName(Integer userId, Integer contactId) {
		return this.customerDao.getUserName(userId, contactId);
	}

	public int getDefaultSupplierIdBySku(String sku) {
		return this.productDao.getDefaultSupplierIdBySku(sku);
	}

	public int getProductSupplierMapCount() {
		return this.productDao.getProductSupplierMapCount();
	}

	public List<Map<String, Object>> getProductSupplierMap(int limit, int offset) {
		return this.productDao.getProductSupplierMap(limit, offset);
	}

	/**
	 * Reference to a properties file that stores the configuration for which referrer domains are being captured as well as the Regex expressions to grab the query string. This resource is injected via DI
	 */
	private Properties site;

	public void setSite(Properties site) {
		this.site = site;
	}

	public String addWebReferralProspectInformation(String prospectId, String referrer) {

		// Extract search engine
		String domain = this.getDomain(referrer);

		// If not one of the search engines we are tracking, etc
		if (domain == null || domain.isEmpty()) {
			return null;
		}
		// Based on domain portion, get Friendly name to insert into the database
		String searchEngineName = site.getProperty(domain + ".name");
		if (logger.isTraceEnabled()) {
			// logger.trace("Domain is: " + domain);
			// logger.trace("Search Engine Friendly Name is:" + searchEngineName);
		}
		// Based on Search Engine's domain, get query String
		String queryString = this.getQueryString(referrer, site.getProperty(domain + ".queryString.regex"));
		if (logger.isTraceEnabled()) {
			// logger.trace("Query is: " + queryString);
		}

		// If no prospectID, generate one.
		if (prospectId == null || prospectId.isEmpty()) {
			prospectId = this.generateProspectId();
		}
		// Insert data
		searchEngineProspectDao.addProspectInformation(prospectId, searchEngineName, queryString);
		return prospectId;
	}

	/**
	 * This method returns a list of all stored information for a given prospectId.
	 * 
	 * @param prospectId
	 *        The prospect identifier
	 * @return A {@link List} of {@link SearchEngineProspectEntity}
	 */
	public List<SearchEngineProspectEntity> getProspectInformation(String prospectId) {
		return searchEngineProspectDao.findByProspectId(prospectId);
	}

	/**
	 * This method will fetch the most recent record for a given prospect
	 * 
	 * @param prospectId
	 *        The prospect identifier
	 * @return A {@link SearchEngineProspectEntity}
	 */
	public SearchEngineProspectEntity getNewestProspectInformation(String prospectId) {
		return searchEngineProspectDao.findLastByProspectId(prospectId);
	}

	/**
	 * This method will fetch the first record persisted for a given prospect.
	 * 
	 * @param prospectId
	 *        The prospect identifier
	 * @return A {@link SearchEngineProspectEntity}
	 */
	public SearchEngineProspectEntity getOldestProspectInformation(String prospectId) {
		return searchEngineProspectDao.findFirstByProspectId(prospectId);
	}

	/**
	 * A private method to parse and return the configured domain for a given referrer. It will use the Regex in the configured SearchEngineRules properties file to determine what to find
	 * 
	 * @param referrer
	 *        The referrer string
	 * @return A {@link String}
	 */
	private String getDomain(String referrer) {
		String domain = null;
		Pattern pattern = Pattern.compile(site.getProperty("searchengine.regex"));
		Matcher matcher = pattern.matcher(referrer);
		if (matcher.find()) {
			domain = matcher.group();
		}

		return domain;
	}

	/**
	 * A private method to parse and return the clean query string from a configured referrer
	 * 
	 * @param referrer
	 *        The referrer string
	 * @param regex
	 *        The configured Regex
	 * @return A {@link String} containing the Query String, if available, from the referrer
	 */
	private String getQueryString(String referrer, String regex) {
		String queryString = "";
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(referrer);
		try {
			if (matcher.find()) {
				queryString = URLDecoder.decode(matcher.group(), "UTF-8");
			}
		} catch (Exception e) {
			logger.debug("query string extraction from referrer failed");
		}
		return queryString;
	}

	/**
	 * A method to generate and return a prospectId. This has been put into its own method in case a different method of generating this is desired.
	 * 
	 * @return A {@link String} containing a generated prospect identifier.
	 */
	private String generateProspectId() {
		return UUID.randomUUID().toString();
	}

	public String getSecureUrl(Map<String, Configuration> siteConfig, MultiStore multiStore) {

		String secureUrl = siteConfig.get("SECURE_URL").getValue();
		if (multiStore != null) {
			secureUrl = "https://" + multiStore.getHost() + "/";
			// secureUrl = request == null ? ("http://" + multiStore.getHost() + "/") : ("http://" + multiStore.getHost() + request.getContextPath() + "/");
		}
		return secureUrl;
	}

	public String getLeftBarType(HttpServletRequest request, String categoryLeftBarType) {
		String leftBarType = request.getAttribute("_leftBar").toString();

		if (categoryLeftBarType != null && !categoryLeftBarType.isEmpty()) {
			leftBarType = categoryLeftBarType;
		}
		return leftBarType;
	}
	
	// check if this child sku has a parentSkus 
	private boolean childHasParent(Promo promo, LineItem lineItem) {
		Set<Object> set = promo.getParentSkuSet();
		Iterator<Object> itr = set.iterator();
		while(itr.hasNext()) {
			String next = (String) itr.next();
//			 System.out.println(next + "==" + lineItem.getProduct().getSku() );
			 if(lineItem.getProduct().getSku().equalsIgnoreCase(next)) {
//				 System.out.println("Done!!!!");
	    		   return true;
	    	   }
//			System.out.println(next + "---???");
			ProductSearch search = new ProductSearch();
			
			search.setParentSku(next.toString());
	       List<Product> list =  this.getProductList(search);
	       Iterator<Product> itr2 = list.iterator();
	       while(itr2.hasNext()) {
	    	   Product next2 = itr2.next();
//	    	   System.out.println(next2.getSku() + "==" + lineItem.getProduct().getSku() );
	    	   if(lineItem.getProduct().getSku().equalsIgnoreCase(next2.getSku())) {
	  			

	    		   return true;
	    	   }
	       }
		}
		return false;
	}
	public void validatePromoCode(Order order, HashMap<String, String> err, Customer customer, String PromocodeLink, Map<String, Object> gSiteConfig) {
		System.out.println("Line validatePromoCode");

		String errors = "";
		String tempPromoCode = null;
		
		// promo Code for new customer only
		String promoName = order.getPromo().getTitle();
		Promo promo1 = getPromoByName(promoName);
		
//         System.out.println(promo1.getpromoForNewCustomersOnly()+ "_______");
		if(promo1 != null) {
			if(promo1.getpromoForNewCustomersOnly() ) {
				OrderSearch OS = new OrderSearch();
				OS.setUserId(customer.getId());
				List<Order> list = getOrdersList(OS);
				if(list.size() >= 1 ) {
					Iterator<Order> itr = list.iterator();
					while(itr.hasNext()) {
						Order OD = itr.next();
						if(OD.getStatus().equals("s")) {
							
							err.put("invalidPromo", "invalidPromo");
							break;
							
						}
				     }
		
				}
			}
		}



		if ((order.getPromoCode() != null && !order.getPromoCode().equals("")) || PromocodeLink != null || order.getPromo() != null) {
			if (order.getPromoCode() != null) {
				tempPromoCode = order.getPromoCode();
			} else if (PromocodeLink != null) {
				tempPromoCode = PromocodeLink;
			} else if(order.getPromo() != null){
				tempPromoCode = order.getPromo().getTitle();
			}
			Promo promo = getPromoByName(tempPromoCode);
			System.out.println(" promo"+promo);

			if (promo != null) {
				Map<String, Object> conditionPromo = new HashMap<String, Object>();
				conditionPromo.put("tempPromoCode", tempPromoCode);
				conditionPromo.put("minOrder", order.getSubTotal());
				conditionPromo.put("userId", order.getUserId());

				conditionPromo.put("orderShippingId", order.getCustomShippingId());
				if ((Boolean) gSiteConfig.get("gSALES_PROMOTIONS_ADVANCED")) {
					conditionPromo.put("state", customer.getAddress().getStateProvince());
					conditionPromo.put("groupType", promo.getGroupType());
					conditionPromo.put("stateType", promo.getStateType());
					conditionPromo.put("customShippingId", promo.getShippingIdSet());
				}
				
				// Check if promo is for new customers only
			/*	if(customer.getOrderCount() > 0 && promo.getpromoForNewCustomersOnly() == true ){
					err.put("invoiceError", "invalidPromo");
				}
*/
				System.out.println("condition "+getPromoWithCondition(promo, conditionPromo) +"for"+tempPromoCode);

				if (getPromoWithCondition(promo, conditionPromo) != null) {

					if (promo.getDiscountType().equalsIgnoreCase("product")) {
						
				
						System.out.println("promo type prod true");
						Integer promoMinQty = (promo.getMinQty()!=null && !promo.getMinQty().equals(""))? promo.getMinQty() : 0;
						Double minAmount = (promo.getMinOrder()!=null && !promo.getMinOrder().equals(""))? promo.getMinOrder() : 0.0;
						Boolean validPromo = false;
						Map<String, Double> brandTotalMap = new HashMap<String, Double>();
						
//						Promo code Validation for vaitrading category Ids logic  ===start
						Map<String, Double> categoryIdToalPriceMap = new HashMap<String, Double>();
						Map<String, Integer> categoryIdToalQtyMap = new HashMap<String, Integer>();
						
						
						
						Boolean catIdPromoValid = false;		
						if (promo.getCategoryIdSet() != null && promo.getCategoryIdSet().size() > 0) {
							
							Set<Object> categoryIds = promo.getCategoryIdSet();							
							for ( Object catId : categoryIds) {
	//							catId 738
								String promCatId = catId.toString().trim();
								
								//check for lineItems
								if (order.getLineItems() != null && !order.getLineItems().isEmpty()) {								
									for (LineItem lineItem : order.getLineItems()) {										
										 if (childHasParentCategory(promo, lineItem.getProduct())) {
												Integer productId = lineItem.getProduct().getId();
												Product product = getProductById(productId, 0, true, null);
												if (categoryIdToalPriceMap.containsKey(promCatId)) {
													categoryIdToalPriceMap.replace(promCatId, categoryIdToalPriceMap.get(promCatId) + product.getPrice1() * lineItem.getQuantity());
													categoryIdToalQtyMap.replace(promCatId, categoryIdToalQtyMap.get(promCatId) +  lineItem.getQuantity());
												}else {
													categoryIdToalPriceMap.put(promCatId, product.getPrice1() * lineItem.getQuantity());
													categoryIdToalQtyMap.put(promCatId, lineItem.getQuantity());
												}
										 }
									}
								}
								
								//check for insertedLineItems
								if (order.getInsertedLineItems() != null && !order.getInsertedLineItems().isEmpty()) {
									for (LineItem lineItem : order.getInsertedLineItems()) {
										 if (childHasParentCategory(promo, lineItem.getProduct())) {
												Integer productId = lineItem.getProduct().getId();
												Product product = getProductById(productId, 0, true, null);
												if (categoryIdToalPriceMap.containsKey(promCatId)) {
													categoryIdToalPriceMap.replace(promCatId, categoryIdToalPriceMap.get(promCatId) + product.getPrice1() * lineItem.getQuantity());
													categoryIdToalQtyMap.replace(promCatId, categoryIdToalQtyMap.get(promCatId) +  lineItem.getQuantity());
												}else {
													categoryIdToalPriceMap.put(promCatId, product.getPrice1() * lineItem.getQuantity());
													categoryIdToalQtyMap.put(promCatId, lineItem.getQuantity());
												}
										 }
									}
								}
	
	
								System.out.println(categoryIdToalPriceMap + "====categoryIdToalPriceMap");
								System.out.println(categoryIdToalQtyMap + "====categoryIdToalQtyMap");
								
								if ((minAmount != null && categoryIdToalPriceMap.get(promCatId) != null && categoryIdToalPriceMap.get(promCatId) >= minAmount)
									&& (promoMinQty != null && categoryIdToalQtyMap.get(promCatId) != null && categoryIdToalQtyMap.get(promCatId) >= promoMinQty)) {
									catIdPromoValid = true;
								} 
							}
					  
//						if (!catIdPromoValid) {
////							 remove promo code on lineItem and insertedLineItems
//							if (order.getInsertedLineItems() != null && !order.getInsertedLineItems().isEmpty()) {
//								for (LineItem lineItem : order.getInsertedLineItems()) {
//									lineItem.setPromo(null);
//									lineItem.setPromoAmount();
//								}
//							}
//							if (order.getLineItems() != null && !order.getLineItems().isEmpty()) {
//								for (LineItem lineItem : order.getLineItems()) {
//									lineItem.setPromo(null);
//									lineItem.setPromoAmount();
//								}
//							}
//						}
					}
												
//						Promo code Validation for vaitrading category Ids logic  ===start
												
						//*********** For inserted LineItems ****************//
				     if(order.getInsertedLineItems()!=null && !order.getInsertedLineItems().isEmpty()){
							
							for (LineItem lineItem : order.getLineItems()) {
								if (lineItem.getManufactureName() != null && !lineItem.getManufactureName().equals("")) {
									Double newTotal = lineItem.getTotalPrice();
									if (brandTotalMap.get(lineItem.getManufactureName()) != null) {
										newTotal = newTotal + brandTotalMap.get(lineItem.getManufactureName());										
									}
									brandTotalMap.put(lineItem.getManufactureName(), newTotal);									
								}
							}
							
							
							// check if the sku inside lineItem is a child a set of parentSkus
//							Set<Object> set = promo.getParentSkuSet();
//							Iterator<Object> itr = set.iterator();
//							while(itr.hasNext()) {
//								String next = itr.next().toString();
////								System.out.println(next + "---???");
//								ProductSearch search = new ProductSearch();
//								search.setParentSku(itr.next().toString());
//						       List<Product> list =  this.getProductList(search);
//						       Iterator<Product> itr2 = list.iterator();
//						       while(itr2.hasNext()) {
//						    	   Product next2 = itr2.next();
//						    	   if(lineItem.getProduct.getSku().equals(next2.getSku())) {
//						    		   return true;
//						    	   }
////									System.out.println(next2.getSku() + "-!!!! child");
//						       }
//							}
							
							
							
							for (LineItem lineItem : order.getInsertedLineItems()) {
	
								if (
										 ( lineItem.getProduct().getMasterSku() != null && promo.getSkuSet().contains(lineItem.getProduct().getMasterSku())   ) 
										|| promo.getSkuSet().contains(lineItem.getProduct().getSku()) || childHasParent(promo, lineItem)
										|| (lineItem.getProduct().getManufactureName() != null && promo.getBrandSet().contains(lineItem.getProduct().getManufactureName()))  || promo.getCategoryIds() != null && !promo.getCategoryIds().isEmpty()) {
									
												System.out.println(" INSIDE INCLUDE PRODUCT SPECIFIC SKU ");
												if (lineItem.getManufactureName() != null && brandTotalMap.get(lineItem.getManufactureName()) != null && promo.getMinOrderPerBrand() != null
														&& brandTotalMap.get(lineItem.getManufactureName()) < promo.getMinOrderPerBrand()) {
				
													break;
												}
				
												if (!promo.isProductAvailability() ) {
													if(lineItem.getQuantity() >=promoMinQty && lineItem.getTotalPrice() >minAmount){
														if (promo.getCategoryIds() != null && !promo.getCategoryIds().isEmpty()) {
															Boolean containsCatId = childHasParentCategory(promo, lineItem.getProduct());
															if (containsCatId && catIdPromoValid) {
																validPromo = true;
																lineItem.setPromo(promo);
																lineItem.setPromoAmount();
															}
														} else {
															lineItem.setPromo(promo);
															validPromo = true;
															System.out.println("  set promo for item " );
														}
			
													} else {
														if (promo.getCategoryIds() != null && !promo.getCategoryIds().isEmpty()) {
															Boolean containsCatId = childHasParentCategory(promo, lineItem.getProduct());
															if (containsCatId && catIdPromoValid) {
																validPromo = true;
																lineItem.setPromo(promo);
																lineItem.setPromoAmount();
															}
														} else {
															lineItem.setPromo(null);
															System.out.println("  set promo null " );
														}
													}
													
													
												} else {
													lineItem.getProduct().setField6(getProductAvialabilityById(lineItem.getProduct().getId()));
													if (lineItem.getProduct().getField6() != null && lineItem.getProduct().getField6().equalsIgnoreCase("Available")) {
														lineItem.setPromo(promo);
														validPromo = true;
													} else {
														validPromo = validPromo || false;
													}
												}
											} 
	
								else {
									
									validPromo = validPromo || false;
								}
							}
					}
						
						//For inserted lineitems End //
						
						
						//***** For LineItems *********//
						if(order.getLineItems()!=null && !order.getLineItems().isEmpty()){
							
						
						for (LineItem lineItem : order.getLineItems()) {
							if (lineItem.getManufactureName() != null && !lineItem.getManufactureName().equals("")) {
								Double newTotal = lineItem.getTotalPrice();
								if (brandTotalMap.get(lineItem.getManufactureName()) != null) {
									newTotal = newTotal + brandTotalMap.get(lineItem.getManufactureName());
									System.out.println("Line 28173" + newTotal);
								}
								brandTotalMap.put(lineItem.getManufactureName(), newTotal);
								
							}
						}
						
						
						
						
//						System.out.println(" ORDER SKUS: " + order.getInsertedLineItems().get(0).getProduct().getSku());
						for (LineItem lineItem : order.getLineItems()) {
							
				
							if ( ( lineItem.getProduct().getMasterSku() != null && promo.getSkuSet().contains(lineItem.getProduct().getMasterSku())   ) 
									|| promo.getSkuSet().contains(lineItem.getProduct().getSku()) || childHasParent(promo, lineItem)
									|| (lineItem.getProduct().getManufactureName() != null && promo.getBrandSet().contains(lineItem.getProduct().getManufactureName())) || promo.getCategoryIds() != null && !promo.getCategoryIds().isEmpty()) {
								
								if (lineItem.getManufactureName() != null && brandTotalMap.get(lineItem.getManufactureName()) != null && promo.getMinOrderPerBrand() != null
										&& brandTotalMap.get(lineItem.getManufactureName()) < promo.getMinOrderPerBrand()) {
									break;
								}

								if (!promo.isProductAvailability() ) {									
									if(lineItem.getQuantity() >=promoMinQty && lineItem.getTotalPrice() >minAmount){
										if (promo.getCategoryIds() != null && !promo.getCategoryIds().isEmpty()) {
											Boolean containsCatId = childHasParentCategory(promo, lineItem.getProduct());
											if (containsCatId && catIdPromoValid) {
												validPromo = true;
												lineItem.setPromo(promo);
												lineItem.setPromoAmount();
											}
										} else {
											lineItem.setPromo(promo);
											validPromo = true;
											System.out.println(" set promo for item " );
										}
									} else {
										if (promo.getCategoryIds() != null && !promo.getCategoryIds().isEmpty()) {
											Boolean containsCatId = childHasParentCategory(promo, lineItem.getProduct());
											if (containsCatId && catIdPromoValid) {
												validPromo = true;
												lineItem.setPromo(promo);
												lineItem.setPromoAmount();
											}
										}  else {
											lineItem.setPromo(null);
											System.out.println("  set promo null " );
										}
									}
								} else {
									lineItem.getProduct().setField6(getProductAvialabilityById(lineItem.getProduct().getId()));
									if (lineItem.getProduct().getField6() != null && lineItem.getProduct().getField6().equalsIgnoreCase("Available")) {
										lineItem.setPromo(promo);
										validPromo = true;
									} else {
										validPromo = validPromo || false;
									}							
								}
							} 

							else {		
								validPromo = validPromo || false;
							}
						}
		
					}
						// *****************For LineItems End*************//
						if (!validPromo) {

							errors = "form.invalidPromo";
							if (err.get("orderError") != null) {
								err.put("orderError", errors);
							} else {
								err.put("invoiceError", "invalidPromo");
							}
						}
					} else {
//					promo type is order and valid, 
//					remove promo code on lineItem and insertedLineItems
					if (order.getInsertedLineItems() != null && !order.getInsertedLineItems().isEmpty()) {
						for (LineItem lineItem : order.getInsertedLineItems()) {
							lineItem.setPromo(null);
							lineItem.setPromoAmount();
						}
					}
					if (order.getLineItems() != null && !order.getLineItems().isEmpty()) {
						for (LineItem lineItem : order.getLineItems()) {
							lineItem.setPromo(null);
							lineItem.setPromoAmount();
						}
					}
						order.setPromo(promo);
					}

				} else {
					order.setPromo(null);
					errors = "form.invalidPromo";
					if (err.get("orderError") != null) {
						err.put("orderError", errors);
					} else {
						err.put("invoiceError", "invalidPromo");
					}
				}
			} else {
				order.setPromo(null);
				errors = "form.invalidPromo";
				if (err.get("orderError") != null) {
					err.put("orderError", errors);
				} else {
					err.put("invoiceError", "invalidPromo");
				}
			}
		}else{
			order.setPromo(null);
		}
	}

	/*public void validatePromoCode(Order order, HashMap<String, String> err, Customer customer, String PromocodeLink, Map<String, Object> gSiteConfig) {
		String errors = "";
		String tempPromoCode = null;

		if ((order.getPromoCode() != null && !order.getPromoCode().equals("")) || PromocodeLink != null || order.getPromo() != null) {
			if (order.getPromo() != null) {
				tempPromoCode = order.getPromo().getTitle();
			} else if (order.getPromoCode() == null || order.getPromoCode().equals("")) {
				tempPromoCode = PromocodeLink;
			} else {
				tempPromoCode = order.getPromoCode();
			}
			Promo promo = getPromoByName(tempPromoCode);
			if (promo != null) {
				Map<String, Object> conditionPromo = new HashMap<String, Object>();
				conditionPromo.put("tempPromoCode", tempPromoCode);
				conditionPromo.put("minOrder", order.getSubTotal());
				conditionPromo.put("userId", order.getUserId());
				conditionPromo.put("orderShippingId", order.getCustomShippingId());
				if ((Boolean) gSiteConfig.get("gSALES_PROMOTIONS_ADVANCED")) {
					conditionPromo.put("state", customer.getAddress().getStateProvince());
					conditionPromo.put("groupType", promo.getGroupType());
					conditionPromo.put("stateType", promo.getStateType());
					conditionPromo.put("customShippingId", promo.getShippingIdSet());
				}
				
				System.out.println("--testing");
				if (getPromoWithCondition(promo, conditionPromo) != null) {
					System.out.println("--testing 2");

					if (promo.getDiscountType().equalsIgnoreCase("product")) {
						System.out.println("--testing 3 prod");

						Map<String, Double> brandTotalMap = new HashMap<String, Double>();
						for (LineItem lineItem : order.getLineItems()) {
							if (lineItem.getManufactureName() != null && !lineItem.getManufactureName().equals("")) {
								Double newTotal = lineItem.getTotalPrice();
								if (brandTotalMap.get(lineItem.getManufactureName()) != null) {
									newTotal = newTotal + brandTotalMap.get(lineItem.getManufactureName());
								}
								brandTotalMap.put(lineItem.getManufactureName(), newTotal);
							}
						}
						Boolean validPromo = false;
						for (LineItem lineItem : order.getLineItems()) {
							
							if ((lineItem.getProduct().getMasterSku()!=null && promo.getParentSkuSet().contains(lineItem.getProduct().getMasterSku())) || promo.getSkuSet().contains(lineItem.getProduct().getSku())
									|| (lineItem.getProduct().getManufactureName() != null && promo.getBrandSet().contains(lineItem.getProduct().getManufactureName()))) {
								System.out.println("--testing 4 prod");

								if (lineItem.getManufactureName() != null && brandTotalMap.get(lineItem.getManufactureName()) != null && promo.getMinOrderPerBrand() != null
										&& brandTotalMap.get(lineItem.getManufactureName()) < promo.getMinOrderPerBrand()) {
									break;
								}

								if (!promo.isProductAvailability()) {
									lineItem.setPromo(promo);
									validPromo = true;
								} else {
									lineItem.getProduct().setField6(getProductAvialabilityById(lineItem.getProduct().getId()));
									if (lineItem.getProduct().getField6() != null && lineItem.getProduct().getField6().equalsIgnoreCase("Available")) {
										lineItem.setPromo(promo);
										validPromo = true;
									} else {
										validPromo = validPromo || false;
									}
								}
							} else {
								System.out.println("--testing 5 prod");

								validPromo = validPromo || false;
							}
						}
						if (!validPromo) {

							errors = "form.invalidPromo";
							if (err.get("orderError") != null) {
								err.put("orderError", errors);
							} else {
								err.put("invoiceError", "invalidPromo");
							}
						}
					} else {
						order.setPromo(promo);
					}

				} else {
					order.setPromo(null);
					errors = "form.invalidPromo";
					if (err.get("orderError") != null) {
						err.put("orderError", errors);
					} else {
						err.put("invoiceError", "invalidPromo");
					}
				}
			} else {
				errors = "form.invalidPromo";
				if (err.get("orderError") != null) {
					err.put("orderError", errors);
				} else {
					err.put("invoiceError", "invalidPromo");
				}
			}
		}
	} */
	
	
	
	public void validateAdminPromoCode(HttpServletRequest request, Order order, HashMap<String, String> err, Customer customer, String PromocodeLink, Map<String, Object> gSiteConfig, Double discountAmt) {
		String errors = "";
		String tempPromoCode = null;

		if ((order.getPromoCode() != null && !order.getPromoCode().equals("")) || PromocodeLink != null || order.getPromo() != null) {
			if (order.getPromo() != null) {
				tempPromoCode = order.getPromo().getTitle();
			} else if (order.getPromoCode() == null || order.getPromoCode().equals("")) {
				tempPromoCode = PromocodeLink;
			} else {
				tempPromoCode = order.getPromoCode();
			}
			Promo promo = getPromoByName(tempPromoCode);
			if(promo!=null){
				promo.setDiscount(discountAmt);
			}
			if (promo != null) {
				Map<String, Object> conditionPromo = new HashMap<String, Object>();
				
				
			System.out.println("--- 1");
				
				conditionPromo.put("tempPromoCode", tempPromoCode);
				
				
				conditionPromo.put("minOrder", calculateNonInsertedLinetemsTotal(request,  err, order) + calculateOrderLinetemsTotal(order));

				try {
					if (promo.getDiscountType().equalsIgnoreCase("product")) {
						
							for ( int i = 0 ; i < order.getInsertedLineItems().size() ; i++ ){
								System.out.println("--- line loop");
								System.out.println("promo.getSkuSet()"+promo.getSkuSet().toString());

								//validate for min qty
								if (promo.getSkuSet().contains(order.getInsertedLineItems().get(i).getProduct().getSku())) {
									System.out.println("skuset equals lineItem");

										if(promo.getMinQty()!=null && promo.getMinQty() > order.getInsertedLineItems().get(i).getTotalQty()){
											err.put("invoiceError", "invalidPromo");
											break;
										}
										System.out.println("total price"+order.getInsertedLineItems().get(i).getTotalPrice());

										if(order.getInsertedLineItems().get(i).getTotalPrice() >= promo.getMinOrder() ){
											System.out.println("total price greater than min total");
											order.getInsertedLineItems().get(i).setPromo(promo);
											
											/*err.put("invoiceError", "invalidPromo");
											return;*/
										}
									}
								
								/*if(promo.getSkuSet().contains(order.getInsertedLineItems().get(j).getProduct().getSku())){
									conditionPromo.put("minOrder",order.getInsertedLineItems().get(i).getTotalPrice());
									
								}*/
							
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// put cartSku set only if promoType is Product		
				Set<String> skuSet = new HashSet<String>();
	    		for(LineItem item : order.getInsertedLineItems()) {

	    			if(item.getProduct() != null) {
	    				
		    			skuSet.add(item.getProduct().getSku());
		    			if(item.getProduct().getManufactureName() != null && item.getProduct().getManufactureName().equals("")) {
		    				skuSet.add(item.getProduct().getManufactureName());
		    				
		    			
		    			}
	    			}
	    		}
	    		conditionPromo.put("cartSkuSet",skuSet.toArray());
	    		
				if ((Boolean) gSiteConfig.get("gSALES_PROMOTIONS_ADVANCED")) {
					conditionPromo.put("state", customer.getAddress().getStateProvince());
					conditionPromo.put("groupType", promo.getGroupType());
					conditionPromo.put("stateType", promo.getStateType());
					conditionPromo.put("customShippingId", promo.getShippingIdSet());
				}
				if (order.getOrderId() == null) {
					
					// Check if promo is for new customers only
					if(customer.getOrderCount() > 0 && promo.getpromoForNewCustomersOnly() == true ){
						
						err.put("invoiceError", "invalidPromo");
					}

					if (getPromoWithCondition(promo, conditionPromo) != null) {
	    				System.out.println( "sucess");

						return;
					}else{
						errors = "form.invalidPromo";
						if (err.get("orderError") != null) {
							err.put("orderError", errors);
						} else {
							err.put("invoiceError", "invalidPromo");
						}
					}
				}
				else if (getAdminPromoWithCondition(promo, conditionPromo) != null) {
    				System.out.println( "getAdminPromoWithCondition");

					if (promo.getDiscountType().equalsIgnoreCase("product")) {
//						System.out.println("I am here!!");
						Map<String, Double> brandTotalMap = new HashMap<String, Double>();
						for (LineItem lineItem : order.getLineItems()) {
							if (lineItem.getManufactureName() != null && !lineItem.getManufactureName().equals("")) {
								Double newTotal = lineItem.getTotalPrice();
								if (brandTotalMap.get(lineItem.getManufactureName()) != null) {
									newTotal = newTotal + brandTotalMap.get(lineItem.getManufactureName());
								}
								brandTotalMap.put(lineItem.getManufactureName(), newTotal);
							}
						}
						Boolean validPromo = false;
						for (LineItem lineItem : order.getLineItems()) {
							lineItem.setPromo(promo);
							validPromo = true;
							
							if (promo.getSkuSet().contains(lineItem.getProduct().getSku())
									|| (lineItem.getProduct().getManufactureName() != null && promo.getBrandSet().contains(lineItem.getProduct().getManufactureName()))) {
								if (lineItem.getManufactureName() != null && brandTotalMap.get(lineItem.getManufactureName()) != null && promo.getMinOrderPerBrand() != null
										&& brandTotalMap.get(lineItem.getManufactureName()) < promo.getMinOrderPerBrand()) {
									break;
								}

								if (!promo.isProductAvailability()) {
									lineItem.setPromo(promo);
									validPromo = true;
								} else {
									lineItem.getProduct().setField6(getProductAvialabilityById(lineItem.getProduct().getId()));
									if (lineItem.getProduct().getField6() != null && lineItem.getProduct().getField6().equalsIgnoreCase("Available")) {
										lineItem.setPromo(promo);
										validPromo = true;
									} else {
										validPromo = validPromo || false;
									}
								}
							} else {
								validPromo = validPromo || false;
							}
						}
						if (!validPromo) {

							errors = "form.invalidPromo";
							if (err.get("orderError") != null) {
								err.put("orderError", errors);
							} else {
								err.put("invoiceError", "invalidPromo");
							}
						}
					} else {
						order.setPromo(promo);
					}

				} else {
					order.setPromo(null);
					errors = "form.invalidPromo";
					if (err.get("orderError") != null) {
						err.put("orderError", errors);
					} else {
						err.put("invoiceError", "invalidPromo");
					}
				}
			} 
		}
	}
	
	private Double calculateOrderLinetemsTotal(Order order){
		Double orderTotal = 0.0;
		for (LineItem lineItem : order.getLineItems()) {
			if (lineItem.getTotalPrice() != null) {
				orderTotal = orderTotal + lineItem.getTotalPrice();
			}
		}
		for (LineItem lineItem : order.getInsertedLineItems()) {
			if (lineItem.getTotalPrice() != null) {
				orderTotal = orderTotal + lineItem.getTotalPrice();
			}
		}
		return orderTotal;
	}
	
	private Double calculateNonInsertedLinetemsTotal(HttpServletRequest request,  HashMap<String, String> errors, Order order){
		
	Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
	Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
	List<LineItem> lineItems = new ArrayList<LineItem>();
		
		for (int skuIndex = 1; skuIndex <= 5; skuIndex++) {
			String sku = ServletRequestUtils.getStringParameter( request, "__addProduct" + skuIndex + "_sku", "" );
			if ( !sku.isEmpty() ) {
				Integer existingId = getProductIdBySku(sku);
				//Product product = this.webJaguar.getProductById( existingId, request );
				Product product = getProductById(existingId, 1, false, null);
				if (product == null && !siteConfig.get("ORDER_ADD_VALID_SKU").getValue().equals("true")) {
					product = new Product();
					product.setSku( sku );
				}
				if (product == null && siteConfig.get("ORDER_ADD_VALID_SKU").getValue().equals("true")) {
					errors.put("invalidSku", "Invalid Sku");
				}
				if (product != null) {
					// check if the product is LNOW product and has cost, if doesn't has cost, cannot add to invoice
					if(sku.contains("-")){
						String[] skuContainsDash=Utilities.split(sku, "-");
						Integer supplierID = getCustomerIdBySupplierPrefix(skuContainsDash[0]);
						if(supplierID != null){
							Supplier supplier = getProductSupplierPrimaryBySku(sku);
							if(supplier == null){
								errors.put("invalidSku", "This sku is LNOW sku but doesn't have supplier");
							}else if(supplier.getPrice() == null){
								errors.put("invalidSku", "This sku has supplier but doesn't have cost value");
							}else{
								LineItem lineItem = new LineItem();
								lineItem.setProduct(product);
								lineItem.setProductId( product.getId() );
								lineItem.setQuantity(ServletRequestUtils.getIntParameter( request, "__addProduct" + skuIndex + "_qty", 1 ));
								lineItem.setUnitPrice(getUnitPrice(gSiteConfig, siteConfig, order.getUserId(), product.getSku(), lineItem.getQuantity()));
								lineItem.setOriginalPrice( lineItem.getUnitPrice() );
								lineItems.add(lineItem);
							}
						}else{
							LineItem lineItem = new LineItem();
							lineItem.setProduct(product);
							lineItem.setProductId( product.getId() );
							lineItem.setQuantity(ServletRequestUtils.getIntParameter( request, "__addProduct" + skuIndex + "_qty", 1 ));
							lineItem.setUnitPrice(getUnitPrice(gSiteConfig, siteConfig, order.getUserId(), product.getSku(), lineItem.getQuantity()));
							lineItem.setOriginalPrice( lineItem.getUnitPrice() );
							lineItems.add(lineItem);
						}
						
					}else{
						LineItem lineItem = new LineItem();
						lineItem.setProduct(product);
						lineItem.setProductId( product.getId() );
						lineItem.setQuantity(ServletRequestUtils.getIntParameter( request, "__addProduct" + skuIndex + "_qty", 1 ));
						lineItem.setUnitPrice(getUnitPrice(gSiteConfig, siteConfig, order.getUserId(), product.getSku(), lineItem.getQuantity()));
						lineItem.setOriginalPrice( lineItem.getUnitPrice() );
						lineItems.add(lineItem);
					}
					
				}
			}
		}
		
		Double orderTotal = 0.0;
		for (LineItem lineItem : lineItems) {
			if (lineItem.getTotalPrice() != null) {
				if(orderTotal==null)
					orderTotal = 0.0;
				orderTotal = orderTotal + lineItem.getTotalPrice();
			}
		}
		
		return orderTotal;	
	}

	
	public String getNumber(String phoneNumber){
		if(phoneNumber == null || phoneNumber.length()==0){
			return phoneNumber;
		}
		StringBuffer num = new StringBuffer();
		for(int i =0; i<phoneNumber.length(); i++){
			if(phoneNumber.charAt(i)>='0' && phoneNumber.charAt(i)<='9'){
				num.append(phoneNumber.charAt(i));
			}
		}
		return num.toString();
	}
	
	public void insertAndUpdateCustomerList(List<String> allEmailAddressList, Map<String, CsvMap> availableEmailMap, Map<String, List<CsvData>> customerCsvDataList,Map<String, List<CsvData>> addressCsvDataList, Map<String, List<CsvData>> crmCsvDataList, List<CsvIndex> customerCsvIndex, List<CsvIndex> addressCsvIndex, List<CsvIndex> crmCsvIndex, List<Map<String, Object>> addedItems, List<Map<String, Object>> updatedItems, List<Map<String, Object>> addedCrmItems, List<Map<String, Object>> updatedCrmItems, List<Map<String, Object>> notAddedCrmItems ,  List<Map<String, Object>> notAddedCustomerItems){
		this.customerDao.insertAndUpdateCustomerList(allEmailAddressList, availableEmailMap, customerCsvDataList, addressCsvDataList, crmCsvDataList, customerCsvIndex, addressCsvIndex, crmCsvIndex, addedItems, updatedItems, addedCrmItems, updatedCrmItems, notAddedCrmItems ,  notAddedCustomerItems);

	}

	public void insertCrmTime(String emailAddress, Long crmTime){
		this.crmDao.insertCrmTime(emailAddress, crmTime);
	}
	
	public Long getCrmTime(String emailAddress){
		return this.crmDao.getCrmTime(emailAddress);
	}

	public Long getDuplicateEntry(String emailAddress){
		return this.crmDao.getDuplicateEntry(emailAddress);
	}

	public void updateCrmTime(String emailAddress, Long crmTime){
		this.crmDao.updateCrmTime(emailAddress, crmTime);
	}
	
	public Map<Integer, String> getProductIdCategoryIdsMap() {
		return this.productDao.getProductIdCategoryIdsMap();
	}
	
	public List<ProductFieldUnlimited> getProductFieldsUnlimited() {
		return this.productDao.getProductFieldsUnlimited();
	}
	
	public List<Category> getCategoryTreeStructure(int catId, String direction) {
		return this.categoryDao.getCategoryTreeStructure(catId, direction);
	}
	
	public ProductFieldUnlimited getProductFieldsUnlimitedByName(String fieldName) {
		return this.productDao.getProductFieldsUnlimitedByName(fieldName);
	}
	
	public int getProductFieldUnlimitedNameValueCount() {
		return (Integer) this.productDao.getProductFieldUnlimitedNameValueCount();
	}
	
	public ProductFieldUnlimitedNameValue getProductFieldUnlimitedNameValue(String sku, int fieldId) {
		return  this.productDao.getProductFieldUnlimitedNameValue(sku, fieldId);
	}
	
	public List<Report> getNewCRMContactReportByYear(Integer year) {
		return this.reportDao.getNewCRMContactReportByYear(year);				  
	}
	
	public List<Report> getNewCustomerReportByYear(Integer year) {
		return this.reportDao.getNewCustomerReportByYear(year);			  
	}
	
	public List<ProductCategory> getUpdateProductFields(){
		return this.productImportDao.getUpdateProductFields();
	}
	
	public List<ProductCategory> getAddProductCategory(){
		return this.productImportDao.getAddProductCategory();
	}
	
	public List<ProductCategory> getRemoveProductCategory(){
		return this.productImportDao.getRemoveProductCategory();
	}
	
	public void deleteAllUpdateProductFields(){
		this.productImportDao.deleteAllUpdateProductFields();
	}
	
	public void deleteAllAddProductCategory(){
		this.productImportDao.deleteAllAddProductCategory();
	}
	
	public void deleteAllRemoveProductCategory(){
		this.productImportDao.deleteAllRemoveProductCategory();
	}

	public void updateProductFields(ProductCategory product){
		this.productDao.updateProductFields(product);
	}
	
	public void addProductCategory(ProductCategory product){
		this.productDao.addProductCategory(product);
	}
	
	public void removeProductCategory(ProductCategory product){
		this.productDao.removeProductCategory(product);
	}
	
	public String shippingTitle(String title) {
		StringBuilder sb = new StringBuilder();
		int spc = 0;
		String first = null;
		String second = null;
		//System.out.println("shipping title-----------------------------------------------------------------" + title);
		if(title!=null && title.length()>18){	
			spc = title.lastIndexOf(" ");
			if(spc>0){
				first = title.substring(0, spc);
				second = title.substring(spc+1, title.length());
				sb.append(first);sb.append("<br>");sb.append(second);
			}else{
				first = title.substring(0, 18);
				second = title.substring(18, title.length());
				sb.append(first);sb.append("<br>");sb.append(second);
			}	
		}else{
			sb.append(title);
		}
		return sb.toString();
	}
	
	public void updateCartQuantity(UserSession userSession, HttpServletRequest request, Layout layout) {
		Cart cart = null;
		Integer qnt = new Integer(0);
		if (userSession != null) { 
			cart = getUserCart(userSession.getUserid(), null);
		}else { 
			cart = (Cart) WebUtils.getSessionAttribute(request, "sessionCart");
		}
		if(cart!=null){
			for(CartItem cartItem : cart.getCartItems()) {
				Product p = null;
				if(cartItem!=null && cartItem.getProduct()!=null){
					p = getProductById(cartItem.getProduct().getId(), 0, false, null);
					if(p!=null && p.isActive()){
						qnt = qnt + cartItem.getQuantity();
					}
				}
			}
		}
		if(layout!=null && layout.getHeaderHtml()!=null){
			layout.setHeaderHtml(layout.getHeaderHtml().replaceAll("#cartItemQuantity#", qnt.toString()));
		}
	}
	
	public void updateOrderQuote(Integer orderId, String quote){
		this.orderDao.updateOrderQuote(orderId, quote);
	}
	
	public String getOrderQuote(Integer orderId){
		return this.orderDao.getOrderQuote(orderId);
	}
	
	public Integer checkPreviousPaid(Integer paymentId, Integer orderId) {
		return this.paymentDao.checkPreviousPaid(paymentId, orderId);
	}
	
	public void addNewPurchaseOrderHistoryStatus(Integer poId, String status, String accessUser) {
		this.inventoryDao.addNewPurchaseOrderHistoryStatus(poId, status, accessUser);
	}
	
	public Contact getCompanyContactId(String company) {
		return this.configDao.getCompanyContactId(company);
	}
	
	public List<Customer> getUserNotifications(){
		return this.productImportDao.getUserNotifications();
	}
	
	public void removeUserNotifications(){
		this.productImportDao.removeUserNotifications();
	}
	
	public void updateUserNotifications(Customer customer){
		this.customerDao.updateUserNotifications(customer);
	}
	
	public void updateCrmNotifications(CrmContact crmcontact){
		this.customerDao.updateCrmNotifications(crmcontact);
	}
	
	public String isProductSoftLink(Integer id){
		return this.productDao.isProductSoftLink(id);
	}
	
	public String isProductSoftLinkSku(String sku){
		return this.productDao.isProductSoftLinkSku(sku);
	}
	
	public List<CrmContactField> getCrmContactFieldsByContactId(Integer contactId){
		return this.crmDao.getCrmContactFieldsByContactId(contactId);
	}

	public void insertPromos(List<Promo> promos) {
		 this.promoDao.insertPromos(promos);
		
	}
	
	// check if this child sku is in a parent category
	private boolean childHasParentCategory(Promo promo, Product product) {
		Set<Object> promoCategoryIdset = promo.getCategoryIdSet();
		
		Set<Object> parentCategoryIdSet = product.getCatIds();
		Iterator<Object> itr = parentCategoryIdSet.iterator();
		while (itr.hasNext()) {
			Object next = itr.next();
			if (promoCategoryIdset.contains(next.toString())) {
				return true;
			}
		}
		return false;
	}
	
	public List<CustomerExtContact> getCustomerExtContactListByUserid(Integer userId) {
		return this.customerDao.getCustomerExtContactListByUserid(userId);
	}

	public CustomerExtContact getCustomerExtContactById(Integer customerExtContactId) {
		return this.customerDao.getCustomerExtContactById(customerExtContactId);
	}
	
	public void updateCustomerExtContact(CustomerExtContact customerExtContact) {
		this.customerDao.updateCustomerExtContact(customerExtContact);
	}

	public void deleteCustomerExtContact(CustomerExtContact customerExtContact) {
		this.customerDao.deleteCustomerExtContact(customerExtContact);
	}

	public void insertCustomerExtContact(CustomerExtContact customerExtContact) {
		this.customerDao.insertCustomerExtContact(customerExtContact);
	}
}
