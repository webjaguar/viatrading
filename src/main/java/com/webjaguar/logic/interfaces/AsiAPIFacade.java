package com.webjaguar.logic.interfaces;

import com.webjaguar.model.Supplier;
import com.webjaguar.thirdparty.asi.asiAPI.SearchResult;

public interface AsiAPIFacade {
	
	// Products
	SearchResult getAsiProducts(String query);
	String getAsiProductXml(Integer id);
	boolean getAsiProductImage(Integer id);

	// Suppliers
	int getAsiSuppliersCount(String query);
	SearchResult getAsiSuppliers(String query);
	Supplier getAsiSupplier(Integer id);
}
