package com.webjaguar.logic.interfaces;

import com.webjaguar.model.Customer;
import com.webjaguar.model.CustomerCreditHistory;
import com.webjaguar.model.GiftCard;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;

public interface Credit {
	public CustomerCreditHistory updateCustomerCreditByOrderPayment(Order order, String paymentType, Double amount, String userName);
	public CustomerCreditHistory updateCustomerCreditByVBAPayment(LineItem lineItem, String paymentType, String userName);
	public CustomerCreditHistory updateCustomerCreditByAffiliatePayment(Order order, String paymentType, String userName);
	public CustomerCreditHistory updateCustomerCreditByGiftCard(GiftCard giftCard, Integer usreId);
	public CustomerCreditHistory updateCustomerCreditByLoyalty(Order order, Customer customer, String userName);
}
