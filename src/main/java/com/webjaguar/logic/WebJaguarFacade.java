/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.logic;

import https.webservices_rrts_com.ratequote.QuoteResponse;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.mail.MailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.buySafe.BuySafeAPISoap;
import com.buySafe.ShoppingCartAddUpdateRS;
import com.buySafe.ShoppingCartCheckoutRS;
import com.webjaguar.dao.GlobalDao;
import com.webjaguar.model.*;
import com.webjaguar.model.crm.CrmAccount;
import com.webjaguar.model.crm.CrmAccountSearch;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactField;
import com.webjaguar.model.crm.CrmContactGroup;
import com.webjaguar.model.crm.CrmContactGroupSearch;
import com.webjaguar.model.crm.CrmContactSearch;
import com.webjaguar.model.crm.CrmForm;
import com.webjaguar.model.crm.CrmQualifier;
import com.webjaguar.model.crm.CrmQualifierSearch;
import com.webjaguar.model.crm.CrmTask;
import com.webjaguar.model.crm.CrmTaskSearch;
import com.webjaguar.thirdparty.dsdi.DsdiCategory;
import com.webjaguar.thirdparty.dsi.DsiCategory;
import com.webjaguar.thirdparty.echosign.EchoSign;
import com.webjaguar.thirdparty.echosign.EchoSignSearch;
import com.webjaguar.thirdparty.echosign.EchoSignWidgetSearch;
import com.webjaguar.thirdparty.echosign.Widget;
import com.webjaguar.thirdparty.ingrammicro.IngramMicroCategory;
import com.webjaguar.thirdparty.koleImports.KoleImportsCategory;
import com.webjaguar.thirdparty.payment.amazon.Iopn;
import com.webjaguar.thirdparty.payment.ebillme.EBillme;
import com.webjaguar.thirdparty.payment.geMoney.GEMoney;
import com.webjaguar.thirdparty.synnex.SynnexCategory;
import com.webjaguar.thirdparty.techdata.TechdataCategory;
import com.webjaguar.thirdparty.techdata.TechdataManufacturer;
import com.webjaguar.web.admin.datafeed.WebjaguarCategory;
import com.webjaguar.web.form.AccessPrivilegeForm;
import com.webjaguar.web.form.EmailMessageForm;
import com.webjaguar.web.form.GroupPrivilegeForm;
import com.webjaguar.web.form.OrderForm;
import com.webjaguar.web.form.PurchaseOrderForm;
import com.webjaguar.web.form.TicketForm;
import com.webjaguar.web.form.crm.CrmFormBuilderForm;

public interface WebJaguarFacade {
	// Session
	UserSession getUserSession(HttpServletRequest request);

	void setUserSession(HttpServletRequest request, UserSession userSession);

	Customer getCustomerByRequest(HttpServletRequest request);

	// Product
	void insertProduct(Product product, HttpServletRequest request);

	List<Product> getProductsOnFrontendBySupplier(Integer supplierId);

	List<Product> getProductsBySupplierId(Integer supplierId);

	int getProductsByCategoryIdCount(Category category, List<ProductField> searchProductField, HttpServletRequest request, ProductSearch search);

	List<Product> getProductsByCategoryId(Category category, List<ProductField> searchProductField, HttpServletRequest request, ProductSearch search, boolean fieldFilter);

	List<Product> getProductList(ProductSearch search);

	List<Product> getStoneEdgeProductList(ProductSearch search);

	int getProductListCount(ProductSearch search);

	public List<Integer> getProductIdListBySearch(ProductSearch search);

	List<Product> getProductExportList(int limit, int offset);

	List<Product> getProductExportList(ProductSearch search, int numImages);

	List<Product> getSiteMapProductList(ProductSearch search);

	List<Supplier> getProductSupplierExportList(int limit, int offset);
	
	List<Supplier> getSupplierExportList(int limit, int offset);

	int productCount();

	int productCount(ProductSearch search);

	int productSupplierCount();
	
	int supplierCount();

	Product getProductById(Integer productId, int numImages, boolean showPrice, Customer customer);

	Product getProductById(Integer productId, HttpServletRequest request);

	void getProductFromAsiXml(String productXml, Product product);

	Double getProductCost(String sku, Integer quantity, HttpServletRequest request);

	Set<Object> getCategoryIdsByProductId(Integer productId);

	void updateProduct(Product product, HttpServletRequest request);
	
	void updateApiProduct(Product product, HttpServletRequest request);
	
	void updateParentProduct(String parentSku, Integer parentId);

	void batchCategoryIds(Integer productId, Set categoryIds, String action);

	void batchFieldUpdate(Set productIds, Integer fieldId, String fieldValue);

	void deleteProduct(Product product);

	void deleteProductById(Integer productId);

	void importProducts(List<Product> productList);

	void updateProductFields(List<ProductField> productFields);

	void updateI18nProductFields(List<ProductField> productFields);

	List<ProductField> getProductFields(String protectedAccess, int numProdFields);

	Map<String, Map<Integer, ProductField>> getI18nProductFields(int numProdFields);

	List<ProductField> getProductFieldsRanked(String protectedAccess, int numProdFields, String lang);

	List<ProductField> getProductFields(HttpServletRequest request, List<Product> productList, boolean hideEmptyColum) throws Exception;

	List<ProductField> getProductFields(HttpServletRequest request, List<Product> productList, List<NameValues> dropDownList) throws Exception;

	List<NameValues> getProductFields(Category category, HttpServletRequest request) throws Exception;

	List<Product> searchProducts(ProductSearch search, HttpServletRequest request, Customer customer);

	int searchProductsCount(ProductSearch search, HttpServletRequest request, Customer customer);

	List<Product> getShoppingListByUserid(Integer userId, int groupId, HttpServletRequest request);

	List<ProductOption> getProductOptionsByOptionCode(String optionCode, boolean multiOption, SalesTag salesTag, String protectedAccess);

	void updateProductOptions(Option option);

	List<Option> getOptionsList(OptionSearch search);

	int getOptionsListCount();

	Option getOption(Integer id, String code);

	void deleteOption(Option option);

	Integer getProductIdBySku(String sku);
	
	String getProductSkuById(Integer id);

	boolean isValidProductId(int id);

	void updateCategoryProductRanking(List<Map<String, Integer>> data);

	Product getAlsoConsiderBySku(String sku, HttpServletRequest request);

	boolean duplicateOptionName(String productId, String name);

	List<Supplier> getProductSupplierListBySku(String sku);
	
	Integer getProductSupplierCost(String sku, Integer defaultSupplierId);

	void updateProductSupplier(Supplier supplier);

	void insertProductSupplier(Supplier supplier);

	void insertProductSupplier(final List<Map<String, Object>> data, Integer supplierId);

	int getDefaultSupplierIdBySku(String sku);

	int getProductSupplierMapCount();

	List<Map<String, Object>> getProductSupplierMap(int limit, int offset);

	Supplier getProductSupplierBySIdSku(String sku, Integer supplierId);

	Supplier getProductSupplierPrimaryBySku(String sku);

	void deleteProductSupplier(String sku, Integer supplierId);

	void nonTransactionSafeStats(Product product);

	void updateSalesTagToProductId(int productId, Integer salesTagId);

	void updateManufacturerToProductId(int productId, Integer manufacturerId);

	Double getUnitPrice(Map gSiteConfig, Map<String, Configuration> siteConfig, int customerId, String sku, int qty);

	List<ProductField> getProductFieldsHeader(Order order, int numProdFields) throws Exception;

	List<Product> getProductAjax(ProductSearch productSearch);

	List<Supplier> getSupplierAjax(String company);

	Map<String, Long> getProductSkuMap();
	List<RangeValue> getRangeValues(String type);
	// void updateSyncProduct(final List <CsvFeed> csvFeedList, final List<Map <String, Object>> data, String source);
	void nonTransactionSafeUpdateASIProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data);

	void nonTransactionSafeUpdateASIFileFeedProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data);

	void nonTransactionSafeUpdateWebjaguarProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data);

	void nonTransactionSafeInsertWebjaguarCategories(final List<WebjaguarCategory> categoryList);

	void nonTransactionSafeUpdateCdsDvdsProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data, boolean insertNewProduct);

	void updateDefaultSupplierId(Integer supplierId, String productSku);

	List<String> getProductSkuListByCategoryId(Integer cid);

	public Product getProductByAsiId(Integer asiId, HttpServletRequest request);

	Map<String, Product> getI18nProduct(int id);

	List<ProductImage> getImages(Integer productId, int numImages);

	List<Map<String, Object>> getCostListByProductId(Integer productId);

	String getProductNameBySku(String sku);

	String getProductFieldById(int productId, int fieldNumber);

	String getProductAvialabilityById(Integer productId);

	String getProductRedirectUrl(String sku, Integer productId, String protectedAccess, String field);

	String getProductAsiXMLById(String sku);

	String getProductNoteBySku(String sku);

	boolean isEndQtyPricing(String sku);

	boolean applyPriceDifference(asiProduct.Product product, com.webjaguar.thirdparty.asi.asiAPI.Product iProduct, String globalMarkup, Integer globalMarkupType, int markupFormula);

	void setASIPriceWithSalesTag(asiProduct.Product product, com.webjaguar.thirdparty.asi.asiAPI.Product iProduct, SalesTag salesTag);

	List<Product> getCustomerShoppingCartProductInfo(Integer userId);

	void nonTransactionSafeInactiveASIFileFeedProduct(final List<String> data, String column);

	int[] updatePowerReview(final List<Map<String, Object>> data);

	void updateProduct(String updateStatement, String whereStatement);

	// Product Print lable
	ProductLabelTemplate getProductLabelTemplate(Integer id);

	List<ProductLabelTemplate> getProductLabelTemplates(ProductLabelTemplateSearch search);

	// kit
	List<KitParts> getKitPartsByKitSku(String sku);

	public void addKitPartsHistory(InventoryActivity kitActivity);

	// product variant
	List<Product> getProductVariant(int productId, String sku, boolean backEnd);

	// Lucene
	List<Map<String, Object>> getLuceneProduct(int limit, int offset, boolean onlyParent, String parentSku, String[] searchFieldsList, boolean adminIndex);

	int getLuceneProductCount(boolean onlyParent);

	// Category
	List<Category> getCategoryTree(Integer categoryId, boolean showHidden, HttpServletRequest request);

	Set<Integer> getParentCategories(Integer categoryId);

	Category getCategoryById(Integer categoryId, String protectedAccess);

	void updateCategory(Category category);

	int insertCategory(Category category);

	void deleteCategory(Category category);

	List<Category> getCategoryLinks(Integer parentId, HttpServletRequest request);

	String getCategoryName(Integer categoryId);

	List<Category> getMainCategoryLinks(HttpServletRequest request, String categoryLeftBar);

	List<Category> getMainsAndSubsCategoryLinks(HttpServletRequest request);

	void updateCategoryRanking(List<Map<String, Integer>> data);

	List<Category> getCategoryList(CategorySearch search);

	void nonTransactionSafeStats(Category category);

	Integer getHomePageByHost(String host);

	Map<String, Category> getI18nCategory(int cid, String lang);

	void updateI18nCategory(Collection<Category> categories);

	List<Category> getCategoryTree(Integer catId, String protectedAccess, boolean showHidden, Boolean showOnSearch, Set<Integer> extraIds, String lang);

	Map<String, Long> getCategoryExternalIdMap();

	void insertCategoryTree(Integer parentId, List<Category> categories);

	List<Category> getSiteMapCategoryList(CategorySearch search);

	// Inventory
	void insertPurchaseOrder(PurchaseOrderForm purchaseOrderForm);

	void updatePurchaseOrder(PurchaseOrderForm purchaseOrderForm);

	PurchaseOrder getPurchaseOrder(int poId);

	void insertPurchaseOrderStatus(PurchaseOrderForm purchaseOrderForm, boolean inventoryHistory) throws DataAccessException;

	List<PurchaseOrderStatus> getPurchaseOrderStatusHistory(int poId);

	List<PurchaseOrder> getPurchaseOrderList(PurchaseOrderSearch purchaseOrderSearch);
	
	List<String>  getPoPTList();
	
	void updatePoPtById(int poId, String pt);
	
	void updatePoStatusById(int poId, String status);

	Integer getPurchaseOrderIdByName(String poNumber);

	void updateOnHand(String sku, Integer quantity);

	void updateInventory(List<LineItem> lineItems);

	void deductInventory(List<LineItem> lineItems);

	void addInventory(List<LineItem> lineItems);

	void importInventory(InventoryActivity inventoryActivity);

	int purchaseOrderCount(Integer orderId);

	List<PurchaseOrderLineItem> getPOLineItemsQuantity(Integer orderId, boolean dropShip);

	PurchaseOrderLineItem getLineItemByPOId(PurchaseOrderLineItem lineItem);

	void deletePurchaseOrder(Integer poId);

	void updateInventoryBySkus(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data);

	void adjustInventoryBySku(InventoryActivity inventoryActivity);

	Inventory getInventory(Inventory inventory);

	void updateInventory(Inventory inventory);

	// Report
	List<ImportExportHistory> getImportExportHistoryByType(ImportExportHistorySearch search);

	void insertImportExportHistory(ImportExportHistory imEx);

	List<Report> getOrdersSaleReport(ReportFilter filter);

	List<Report> getSalesRepDetailReport(ReportFilter filter);

	List<Report> getSalesRepDetailReport2(ReportFilter filter);

	List<Report> getOrdersSaleDetailReport(ReportFilter filter);

	List<MetricReport> getMetricReport(ReportFilter filter);

	List<Report> getOrdersSaleReportDaily(ReportFilter filter);

	Integer getOrdersSaleReportDailyCount(ReportFilter filter);

	List<Report> getCustomerQuickViewPurchase(Integer userId);

	List<Report> getCustomerQuickViewPurchaseLastYear(Integer userId);

	Integer getCountPendingProcessingOrder();

	List<Order> getPendingProcessingOrder();

	List<Report> getTrackcodeReport(ReportFilter reportFilter);

	List<Integer> getInactiveCustomerIds(ReportFilter reportFilter);

	List<CustomerReport> getInactiveCustomers(ReportFilter reportFilter);

	List<SalesRepReport> getSalesRepReport(Integer salesRepId, ReportFilter salesRepFilter);

	List<SalesRepReport> getSalesRepReportDaily(Integer salesRepId, ReportFilter salesRepFilter);

	List<CustomerReport> getCustomerReportOverview(ReportFilter filter);

	List<EmailCampaign> getEmailCampaigns(ReportFilter reportFilter);

	List<ProductReport> getProductListReport(ReportFilter reportFilter);

	int getProductListReportCount(ReportFilter reportFilter);

	Integer getNumCustomerActivated(String year, int month);

	Integer getNumCustomerRetained(String year, int month);

	List<Report> getPromoCodeReport(ReportFilter reportFilter);

	List<InventoryActivity> getInventoryActivity(ReportFilter reportFilter);

	Integer getInventoryActivityCount(ReportFilter reportFilter);

	List<InventoryReport> getInventoryReport(ReportFilter reportFilter);

	Integer getInventoryReportCount(ReportFilter reportFilter);

	List<Product> getQuantityPending(List<Product> productList);
	
	List<AbandonedShoppingCart> getAbandonedShoppingCartList(AbandonedShoppingCartSearch AbandonedShoppingCartSearch);
	
	List<TicketReport> getTicketReport(ReportFilter reportFilter);
	
	List<Report> getCustomerReportDaily(ReportFilter reportFilter);
	
	List<ProductReport> getBestSellingProducts(ReportFilter reportFilter);
	
	List<ProductReport> getTopAbandonedProducts(ReportFilter reportFilter);
	
	List<CityReport> getTopShippedCities(ReportFilter reportFilter);
	
	List<StateReport> getTopShippedStates(ReportFilter reportFilter);
	
	List<SalesRepReport> getTopSalesReps(ReportFilter reportFilter);
	
	AbandonedShoppingCartSearch getAbandonedShoppingCartSearch(HttpServletRequest request);

	// Truckload Processing
	void insertTruckLoadProcess(TruckLoadProcess truckLoadProcess);

	List<TruckLoadProcess> getTruckLoadProcessList(TruckLoadProcessSearch truckLoadProcessSearch);

	List<TruckLoadProcess> getTruckLoadProcessListReport(TruckLoadProcessSearch truckLoadProcessSearch);
	
	List<String> getTruckLoadProcessPTList();

	TruckLoadProcess getTruckLoadProcessById(Integer id);
	
	TruckLoadProcessProducts getTruckLoadDerivedProductById (Integer Id);

	void updateTruckLoadProcess(TruckLoadProcess truckLoadProcess);
	
	void updateTruckLoadPT(TruckLoadProcess truckLoadProcess);

	// Policy
	Policy getPolicyById(Integer policyId);

	List<Policy> getPolicyList();

	List<Policy> getPolicies();

	void insertPolicy(Policy policy);

	void updatePolicy(Policy policy);

	void deletePolicy(Integer policyId);

	void updatePolicyRanking(List data);
	
	List<CrmQualifier> getQualifierList(QualifierSearch search);

	// Sales Rep
	List<SalesRep> getSalesRepList();

	List<SalesRep> getSalesRepList(SalesRepSearch search);

	SalesRep getSalesRepByUserId(Integer userId);

	void insertSalesRep(SalesRep salesRep);

	void updateSalesRep(SalesRep salesRep);

	SalesRep getSalesRepById(Integer salesRepId);

	void deleteSalesRep(Integer salesRepId);
	
	boolean salesRepHasCustomer(Integer salesRepId);

	SalesRep getSalesRepByTerritoryZipcode(String zipcode);

	void markSalesRepAssignedToCustomerInCycle(Integer salesRepId, boolean isAssigned);

	SalesRep getNextSalesRepInQueue();

	List<SalesRep> getSalesRepListByTerritoryZipcode(LocationSearch search);

	SalesRep getSalesRep(SalesRep salesRep);

	SalesRep getSalesRep(HttpServletRequest request);

	List<Customer> getCustomerListBySalesRep(int salesRepId, String sort);

	void nonTransactionSafeLoginStats(SalesRep salesRep);

	List<SalesRep> getSalesRepTree(int salesRepId);
	
	List<String> getSalesRepGroupList();

	List<SalesRep> getSalesRepList(int salesRepId);

	SalesRep getSalesRep(Integer userId, Integer accessUserId);

	// Presentation
	List<PresentationProduct> getPresentationProductList(Presentation presentation);

	List<Presentation> getPresentationList(Presentation presentation);

	Presentation getPresentation(Integer presentationId);

	void addProductToPresentation(String[] sku, Integer presentationId, Integer saleRepId);

	List<PresentationTemplate> getPresentationTemplateList(PresentationTemplateSearch templateSearch);

	void insertPresentation(Presentation presentation);

	void updateNoOfViewToPresentation(Integer presentationId);

	void updatePresentationPrice(String sku, double price, Integer presentationId, Integer salesRepId, boolean remove);

	void updatePresentation(Presentation presentation);

	void deletePresentation(Presentation presentation);

	void insertTemplate(PresentationTemplate template);

	void updateTemplate(PresentationTemplate template);

	void deleteTemplate(Integer templateId);

	PresentationTemplate getTemplateById(Integer templateId);

	void removeProductFromPresentationList(String sku[], Integer saleRepId);

	// Special Pricing
	void updateSpecialPricingActive(SpecialPricing specialPricing);

	SpecialPricing getSpecialPricingByCustomerIdSku(SpecialPricing specialPricing);

	List<SpecialPricing> getSpecialPricingByCustomerID(SpecialPricingSearch search);

	void insertSpecialPricing(SpecialPricing specialPricing);

	void deleteSpecialPricingById(Integer id) throws DataAccessException;

	Map<String, SpecialPricing> getSpecialPricingByCustomerIDPerPage(Integer customerId, List productList);

	// Access Privilege
	List<AccessUser> getUserPrivilegeList(AccessPrivilegeSearch accessPrivilegeSearch);

	AccessPrivilege getPrivilegeById(Integer accessUserId);

	AccessUser getUserByUserName(String userName);

	AccessUser getAccessUserById(Integer accessUserId);

	AccessUser getAccessUserBySalesRepId(Integer salesRepId);

	AccessUser getAccessUser(HttpServletRequest request);

	void insertAccessPrivilegeUser(AccessPrivilegeForm privilegeForm);

	void disableAccessPrivilege();

	void updateAccessPrivilegeUser(AccessUser user, boolean changeUsername, boolean changePassword);

	void updateAccessPrivilege(List<Configuration> data);

	void deleteAccessPrivilege(String username);

	void deleteAccessPrivilegeUser(String username);

	boolean isUserExist(String username);

	List getUserAuditList(String username);

	void insertUserAudit(AccessUserAudit accessUserAudit);

	void deleteAccessUserAudit(Integer id);

	List<AccessGroup> getGroupPrivilegeList(AccessPrivilegeSearch accessPrivilegeSearch);

	AccessGroupPrivilege getPrivilegeByGroupId(Integer groupId);

	boolean isGroupUserExist(Integer groupId);

	void insertAccessGroup(GroupPrivilegeForm privilegeForm) throws DataAccessException;

	void updateGroupAccessPrivilege(final List<Configuration> data);

	void deleteAccessPrivilegeGroup(Integer groupId);

	void updateAccessGroup(AccessGroup group);

	void deleteAccessGroup(Integer groupId);

	List<Configuration> getGroupPrivileges(Integer groupId, Integer userId);

	// Promo
	List<Promo> getPromoList(PromoSearch promoSearch);
	List<Promo> getDynamicPromoList(PromoSearch promoSearch);
	List<Promo> getCampaignList(PromoSearch promoSearch);
	List<Promo> getEligiblePromoList(Customer customer, Order order);

	Promo getPromoById(Integer promoId);

	Promo getPromoByName(String promoName);

	Promo getPromoWithCondition(Promo promo, Map<String, Object> conditionPromo);
	
	Promo getAdminPromoWithCondition(Promo promo, Map<String, Object> conditionPromo);

	Integer getPromoIdByName(String promoName);

	void insertPromo(Promo promo);
	void insertPromos(List<Promo> promos);

	void updatePromo(Promo promo);

	void deletePromoById(Integer promoId);
    void deletePromoByIds(int[] promoDynamicIds);

	void updatePromoRanking(List<Map<String, Integer>> data);

	Promo getPromo(String promoName);

	public Integer getPromoCount(PromoSearch promoSearch);

	// Sales Tag
	List<SalesTag> getSalesTagList();

	SalesTag getSalesTagById(Integer salesTagId);

	int insertSalesTag(com.webjaguar.model.SalesTag salesTag);

	void updateSalesTag(com.webjaguar.model.SalesTag salesTag);

	void deleteSalesTagById(Integer salesTagId);

	Map<String, Long> getSalesTagCodeMap();

	void updateSyncSalesTag(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data);

	// Deal
	List<Deal> getDealList(DealSearch search);

	Deal getDealById(Integer dealId);

	Deal getDealBySku(String getSku);

	void insertDeal(Deal deal);

	void updateDeal(Deal deal);

	void deleteDealById(Integer dealId);
	
	// ViaDeal	
	List<ViaDeal> getViaDealsList(DealSearch search);
	
	ViaDeal getViaDealById(Integer dealId);

	ViaDeal getViaDealBySku(String getSku);

	void insertViaDeal(ViaDeal viaDeal);

	void updateViaDeal(ViaDeal viaDeal);

	void deleteViaDealById(Integer dealId);

	// Mail In Rebate
	List<MailInRebate> getMailInRebateList(MailInRebateSearch search);

	MailInRebate getMailInRebateById(Integer mailInRebateId);

	MailInRebate getMailInRebateByName(String name);

	void insertMailInRebate(com.webjaguar.model.MailInRebate mailInRebate);

	void updateMailInRebate(com.webjaguar.model.MailInRebate mailInRebate);

	void deleteMailInRebateById(Integer MailInRebateId);

	// GiftCard
	int insertGiftCardOrder(GiftCardOrder giftCardOrder, GiftCardStatus giftCardStatus);

	void insertGiftCard(GiftCard giftCard);

	void insertGiftCardStatus(GiftCardStatus giftCardStatus);

	List<GiftCard> getGiftCardList(GiftCardSearch search, Integer customerId);

	List<GiftCardOrder> getGiftCardOrderList(GiftCardSearch search, Integer customerId);

	GiftCard getGiftCardByCode(String code, Boolean active);

	GiftCardOrder getGiftCardOrderByGiftCardOrderId(String giftCardOrderId);

	GiftCard getGiftCardById(Integer giftCardOrderId);

	void updateGiftCard(GiftCard giftCard);

	void updateGiftCardOrderCreditCardPayment(GiftCardOrder giftCardOrder);

	void updateGiftCardByPayPal(GiftCard giftCard);

	void updateGiftCardOrderByPayPal(GiftCardOrder giftCardOrder, GiftCardStatus giftCardStatus);

	List<GiftCardStatus> getGiftCardStatusHistory(Integer giftCardOrderId);

	void redeemGiftCardByCode(GiftCard giftCard, UserSession userSession, GiftCardStatus giftCardStatus);

	// FAQ, FaqGroup
	Faq getFaqById(Integer faqId);

	List<Faq> getFaqList(FaqSearch search);

	Faq getFaqByQuestion(FaqSearch search);

	void insertFaq(Faq faq);

	void updateFaq(Faq faq);

	void deleteFaq(Integer faqId);

	void updateFaqRanking(List data);

	FaqGroup getFaqGroupById(Integer groupId);

	List<FaqGroup> getFaqGroupList(String protectedAccess);

	void insertFaqGroup(FaqGroup faqGroup);

	void updateFaqGroup(FaqGroup faqGroup);

	void deleteFaqGroup(Integer groupId);

	void updateFaqGroupRanking(List data);

	// Customer
	List<Customer> getCustomerListWithDate(CustomerSearch search);

	List<Customer> getCustomers(CustomerSearch search);

	int getCustomerListWithDateCount(CustomerSearch search);

	List<Integer> getCustomerIdsList(CustomerSearch search);

	int getCustomersCount();

	List<Customer> getCustomerListByZipCode(LocationSearch search);

	int getCustomerListByZipCodeCount(LocationSearch search);
	
	ZipCode getZipCode(String zipcode);
	
	List<ZipCode> getZipCodeList(LocationSearch locationSearch); 

	Customer getCustomerById(Integer userId);
	
	List<Integer> getUserIdByCrmId(List<Integer> userId);

	
	Integer getUserIdByCrmId(Integer userId);
	
	Integer getCrmIdByUsername(String username);
	
	public Map<String,CsvMap> getAvailableUserList(List<String> username);
	
	public List<String> getUserIdListByUsername(List<String> username);
	
	public Map<String,Long> getUserIdsMapByUsername(List<String> username);
		
	Customer getCustomerByUsername(String username);

	Customer getCustomerByUsernameAndPassword(String username, String password);
	
	Customer getCustomerByCardIdAndPassword(String cardId, String password);

	Customer getCustomerByCardID(String cardID);

	void updateCustomer(Customer customer, boolean changeUsername, boolean changePassword);
	
	int insertDialingNoteHistory(DialingNote note);
	
	void updateCardIdCount(Integer userId);

	boolean getAddressType(Address address);

	void insertCustomer(Customer customer, Address shipping, boolean insertCrmAccountContact);

	void deleteCustomer(Customer customer);

	void nonTransactionSafeLoginStats(Customer customer);

	List<Address> getAddressListByUserid(Integer userId);
	
	List<Address> getAllAddressList(Integer userId);

	Address getAddressById(Integer addressId);

	void updateAddress(Address address);

	void insertAddress(Address address);

	void deleteAddress(Address address);

	Address getDefaultAddressByUserid(Integer userId);

	Address getDefaultShippingAddressByUserid(int userId);

	public String getTaxIdByUserId(Integer userId);
	
	String getTextMessageServerById(Integer textMessageServerId);

	void importCustomers(List<Customer> customerList, boolean insertCrmAccountContact);

	List<Customer> getCustomerExportList(CustomerSearch search);

	int customerCount(CustomerSearch search);

	Customer getCustomerByToken(String token);

	String getTokenByUserId(Integer userId);

	String changeCustomerToken(Integer userId);

	List<CustomerField> getCustomerFields();
	
	List<DialingNote> getDialingNoteHistoryById(int id, boolean isCustomer);
	
	void updateDialingNotesCrmToCustomer(Integer crmId, Integer customerId);

	void updateCustomerFields(List<CustomerField> customerFields);

	Integer getUserIdByPromoCode(String promoCode);

	Integer getCustomerIdByAccountNumber(String accountNumber);

	List<Customer> getCustomerListByParent(CustomerSearch search);

	int getCustomerListByParentCount(CustomerSearch search);

	void updateParent(Customer customer);

	List<Customer> getFamilyTree(int customerId, String direction);

	void updateCustomerNote(Customer customer);

	void updateCredit(CustomerCreditHistory credit);

	void updatePointsToCustomer(int customerId, double loyaltyPoints);

	Integer getCustomerIdBySupplierPrefix(String prefix);

	int productCountByCustomerSupplierId(int supplierId);

	void unsubscribeByCustomerId(Integer customerId, String unsubscibeReason);

	boolean isExistingCustomer(String accountNum);

	List<Customer> getCustomerList(Category category, HttpServletRequest request);

	void updateCategoryIdsByUser(int userId, Set<Object> categoryIds);

	Set<Integer> getCategoryIdsByUser(int userId);

	void updateUserDescription(Customer customer);

	List<Customer> getCustomersAjax(CustomerSearch search);

	List<CustomerGroup> getCustomerGroupList(CustomerGroupSearch search);

	List<CustomerGroup> getCustomerGroupNameList();

	void insertCustomerGroup(CustomerGroup group) throws DataAccessException;

	void updateCustomerGroup(CustomerGroup group);

	List<Integer> getCustomerGroupIdList(Integer customerId);
	
	List<Integer> getInactiveSalesRepIdList();

	public boolean isCustomerInGroup(Integer customerId, Set groupIds, String groupType);

	CustomerGroup getCustomerGroupById(Integer groupId);

	void deleteCustomerGroupById(Integer groupId);

	void batchGroupIds(Integer customerId, Set groupIds, String action);

	boolean isGroupCustomerExist(Integer groupId);

	void updateSuspension(Integer customerId, String action);
	
	void updateQualifier(Integer customerId, String action);
	
	void updateTrackCode(Integer customerId, String trackCode);
	
	void updateCrmTrackCode(Integer customerId, String trackCode);
	
	void updateCrmQualifier(Integer customerId, String qualifier);
	
	void updateMainSource(Integer customerId, String mainSource);
	
	void updatePaid(Integer customerId, String paid);
	
	void updateMedium(Integer customerId, String medium);
	
	void updateLanguageField(Integer customerId, String languageField);

	Address getAddress(int userId, String addressCode);

	CustomerSearch getCustomerSearch(HttpServletRequest request);

	void updateCrmIds(Customer customer);

	int[] nonTransactionSafeInsertCustomerGroup(int groupId, Set<String> accountNumber);

	int[] nonTransactionSafeDeleteCustomerGroup(int groupId, Set<String> accountNumber);

	Customer getCustomerOrderReportByUserID(Integer userId);

	Map<Integer, Customer> getQuoteTotalByUserIDPerPage(List customerList);

	List<Integer> getCustomerIdByGroupId(Integer groupId);

	void insertPartnerCustomer(CustomerBudgetPartner partner);

	void updatePartnerCustomer(CustomerBudgetPartner partner, boolean addHistory);

	List<CustomerBudgetPartner> getPartnersListByCustomerId(Integer userId, Boolean active);

	void insertPartnerCustomerHistory(CustomerBudgetPartner partner);

	Boolean getPartnerByName(String name);
	
	List<MobileCarrier> getMobileCarrierList();
	
	Integer getCustomerIdByUsername(String username);
	
	int getCrmContactId(String email);

	
	// Lucene Customer
	int getLuceneCustomerListCount(CustomerSearch search);
	
	List<Map<String, Object>> getLuceneCustomer(int offset, int limit, Integer userId);
	
	int updateUserRatings(StagingUser user);

	// Affiliate
	List getAffiliates(Integer root, Integer list_type);

	Affiliate getAffiliatesCommission(Integer nodeId);

	void updateEdge(Integer customerId, Integer parentId);

	Integer getParentId(Integer childId);

	List<Integer> getParentsId(Integer childId, int level);

	List getCommissionsByUserId(Integer userId, Integer level, CommissionSearch search);

	CommissionReport getCommissionsByUserIdOrderId(Integer userId, Integer orderId, Integer level);

	CommissionReport getCommissionTotal(Integer userId);

	List<CommissionReport> getCommissionReport();

	void updateCommissionStatus(CommissionReport comReport);

	CommissionReport getCommissionTotalByUserId(Integer userId, Integer level);

	int affiliateCount();

	List getAffiliateChild();

	int getAffiliateListCount(CustomerSearch search);

	List<Customer> getAffiliateList(CustomerSearch search);

	List<Customer> getAffiliateNameList();

	// Event
	Event getEventById(Integer eventId);

	List<Event> getEventList(EventSearch search);

	void insertEvent(Event event);

	void updateEvent(Event event);

	void deleteEvent(Integer eventId);

	int getEventMemberListCount(EventSearch search);

	List<EventMember> getEventMemberList(EventSearch search);
	
	int getAllEventMemberListCount(EventSearch search);
	
	List<EventMember> getAllEventMemberList(EventSearch search);

	void insertEventMember(EventMember event) throws DataAccessException;

	EventMember getEventMemberByEventIdUserId(Integer eventId, Integer userId);

	void updateEventMember(EventMember eventMember) throws DataAccessException;

	void deleteEventMemberById(Integer eventId, Integer userId);

	// Payment
	List<Payment> getCustomerPaymentList(PaymentSearch search);

	List<Payment> getPaymentExportList(PaymentSearch search);

	List<Payment> getCustomerPaymentListByOrder(PaymentSearch search);

	void updatePaymentExported(Set<Integer> paymentIds);

	Payment getCustomerPaymentById(int id);

	void updateCustomerPayment(Payment payment, Collection<Order> invoices);

	void insertCustomerPayment(Payment payment, Collection<Order> invoices);
	
	void insertOrderPayment(Payment payment, Collection<Order> invoices);
	
	void insertPayments(Payment payment);

	void insertCustomerPayment(Payment payment);

	Map<Integer, Order> getInvoices(PaymentSearch search, String searchType);

	Integer getFirstPaymentId(Integer order_id);

	Double getPaymentAmount(Integer paymentId, Integer orderId);

	// Forget Password
	String createPasswordToken(Integer userId);

	boolean validToken(String token);

	void updatePassword(String newPassword, String token);

	Integer getUserIdByToken(String token);

	// Supplier
	List<Supplier> getSuppliers(Integer categoryId);

	List<Supplier> getSuppliers(Search search);

	Integer getProductsCountBySupplierId(Integer supplierId);

	Supplier getSupplierByUserid(Integer userId);

	Integer getSupplierIdByCompany(String company);

	String getSupplierCompanyBySupplierId(Integer suppId);

	Integer getSupplierIdByAccountNumber(String accountNumber);

	Supplier getSupplierByAccountNumber(String accountNumber);

	Supplier getSupplierById(Integer supplierId);

	void insertSupplier(Supplier supplier) throws DataAccessException;

	void updateSupplier(Supplier supplier) throws DataAccessException;

	void updateSupplierById(Supplier supplier) throws DataAccessException;

	void updateSupplierByIdWithMarkup(Supplier supplier) throws DataAccessException;

	public void updateSupplierStatusById(Integer id, boolean active) throws DataAccessException;

	void deleteSupplierById(Integer supplierId);

	List<Supplier> getProductSupplierListByOrderId(int orderId, String filter);

	List getProductSkuBySId(Integer supplierId);

	void importProductSuppliers(List<Supplier> suppliers);

	boolean isValidSupplierId(int id);

	void nonTransactionSafeUpdateProductSupplier(final int supplierId, final List<Map<String, Object>> data);

	List<Address> getAddressListBySupplierId(Integer supplierId);

	Address getDefaultAddressBySupplierId(Integer supplierId);

	void updateSupplierAddress(Address address) throws DataAccessException;

	void insertSupplierAddress(Address address) throws DataAccessException;

	Address getSupplierAddressById(Integer addressId);

	void deleteSupplierAddress(Address address) throws DataAccessException;

	void updateSupplierPrimeAddressBySupplierId(Address address) throws DataAccessException;

	public Map<Integer, String> getSupplierNameMap(List<Integer> suppIdList);

	// Service
	List<ServiceableItem> getServiceableItemList(ServiceableItemSearch search);

	ServiceableItem getServiceableItemById(int id);

	void updateServiceableItem(ServiceableItem item);

	void insertServiceableItem(ServiceableItem item);

	List<ServiceWork> getServiceList(ServiceSearch search);

	ServiceWork getService(int serviceNum);

	void updateService(ServiceWork service);

	Integer getServiceableItemId(String itemId, String serialNum);

	void deleteServiceableItem(ServiceableItem item);

	void insertService(ServiceWork service);

	WorkOrder getWorkOrder(int serviceNum);

	void updateWorkOrder(WorkOrder workOrder);

	ServiceWork getLastService(ServiceableItem item);

	List<WorkOrder> getServiceHistory(ServiceableItem item);

	int getServiceCount(ServiceSearch search, String column);

	int updateServiceableItems(final List<ServiceableItem> items);

	void updateWorkOrderPrinted(int serviceNum);

	void updatePdfService(ServiceWork service, String type);

	Map<Integer, ServiceWork> getServiceMap();

	void getServiceHistory(ServiceableItem item, Map<String, Object> model, WebJaguarFacade webJaguar, Map<String, Object> gSiteConfig);

	List<WorkOrder> getIncompleteWorkOrders(String customerField, int defaultInterval);

	// Data Feed
	List<DataFeed> getDataFeedList(String feed);

	List<Product> getProductDataFeedList(DataFeedSearch search);

	List<Product> getWebjaguarDataFeedProductList(DataFeedSearch search, int numImages, String imagePath, boolean includeCategories);

	void updateDataFeed(List<DataFeed> list, Map<String, Object> dataFeedConfig);

	void updateDataFeedList(List<DataFeed> list, List<WebjaguarDataFeed> configList);

	Map<String, Object> getDataFeedConfig(String feed);

	int productDataFeedListCount(DataFeedSearch search);

	int webjaguarDataFeedProductListCount(DataFeedSearch search);

	void nonTransactionSafeUpdateWebjaguarCategories(List<WebjaguarCategory> categories);

	List<WebjaguarCategory> getWebjguarCategories();

	Map<String, WebjaguarCategory> getWebjaguarCategoryMap();

	void markAndInactiveOldProducts(String feed, Integer dataFeedId, boolean endDocument);

	Integer getWebjaguarDataFeedListCount(DataFeedSearch search);

	List<WebjaguarDataFeed> getWebjaguarDataFeedList(DataFeedSearch search);

	WebjaguarDataFeed getWebjaguarDataFeed(Integer id, String token, String feedType);

	void insertWebjaguarDataFeed(WebjaguarDataFeed dataFeed);

	void updateWebjaguarDataFeed(WebjaguarDataFeed dataFeed);

	void deleteWebjaguarDataFeed(Integer id);

	void nonTransactionSafeUpdatePremierProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data);

	// CSV feed
	Map<String, CsvFeed> getCsvFeedMapping(String feed);

	List<CsvFeed> getCsvFeed(String feed);
	
	Map<String, CsvData> getCsvFeedMap(String feed);

	int[] updateProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data, Set<String> deletedSkus);

	void updateProductImage(Map<Long, List<String>> data);

	void nonTransactionSafeUpdateFreeShipping(String option_code, String field);

	// concord
	void updateProductCategory(Map<Long, List<Long>> data);

	void updateConcordProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data);

	Map<String, Map<String, String>> getCategoryExternalIdNewEggMap();

	// evergreen
	// void updateEvergreenProduct(final List <CsvFeed> csvFeedList, final List<Map <String, Object>> data);
	void updateEvergreenOrderSuc(int orderId, String suc);

	void updateEvergreenCustomerSuc(int customerId, String suc);

	String getEvergreenOrderSuc(int orderId);

	void updateEvergreenSalesRep(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data, Map<String, String> customerSalesRepMap);

	// fragrancenet
	void updateFragrancenetProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data);

	void insertProductCategory(Map<String, List<Long>> data);

	// Jgoodin
	void updateJgoodinProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data);

	// number1Appliance
	void updateNum1ApplainceProduct(final List<CsvFeed> csvFeedList, final List<Map<String, Object>> data);

	// Ticket
	List<Ticket> getTicketsListByUserid(Integer userId, TicketSearch ticketSearch);

	List<Ticket> getTicketsList(TicketSearch ticketSearch);

	List<TicketForm> getTicketVersionListById(Integer ticketId, Integer userId);

	Ticket getTicketById(Integer ticketId);

	int insertTicket(Ticket ticket);

	void insertTicketVersion(TicketVersion ticketVersion);

	void updateTicketLastModified(Integer ticketId, String versionCreatedByType);

	void updateTicketStatus(Ticket ticket);

	// Cart
	void addToCart(CartItem cartItem, Integer userId);

	String removeCartItem(int index, Integer userId);
	
    void removeCartItembyApiIndex(String apiIndex, Integer userId, Integer productId);

	void mergeCart(Cart cart, Integer userId);

	Cart getUserCart(int userId, String manufacturerName);

	String updateCart(Cart cart, HttpServletRequest request);

	String updateCart(Cart cart, Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig, String lang, String host, Map<String, Object> model);

	Double unitPriceByQty(int qty, Product product);

	void updateCartItem(CartItem cartItem);
	
	void updateCartItemByApiIndex(CartItem cartItem,  Integer productId);

	void deleteShoppingCart(int userid, String manufacturerName);

	// List
	void addToList(String ids[], Integer userId);

	void removeFromList(String ids[], Integer userId);

	void addToWishList(Integer userId, Set productIds);

	List getShoppingListGroupByUserid(Integer userId);

	void insertShoppingGroupList(String groupName, Integer userId);

	ShoppingListGroup getShoppingListGroupByName(String groupName);

	void addToShoppingGroupList(String[] ids, Integer userId, Integer groupId);

	void removeMyListGroup(String[] ids, Integer userId);

	// Order
	void insertOrder(OrderForm orderForm, OrderStatus orderStatus, HttpServletRequest request, boolean clearUserCart);

	void insertOrder(Order order, OrderStatus orderStatus, boolean clearUserCart, boolean backendAddedOrder, int gAFFILIATE, Map siteConfig, Map gSiteConfig);

	List<Order> getOrdersListByUser(OrderSearch search, boolean quote);
	 Integer isThereAnOrderUseThisPromo(String promoCode);
	
	Map<Integer, Integer> getFirstOrderAllUser();
	
	List<Order> getManifestsReportByUser(OrderSearch search, boolean quote);

	List<Order> getQuotesListByUser(OrderSearch search);

	List<Order> getOrdersListBySupplier(OrderSearch search);

	List<ConsignmentReport> getConsignmentReport(Integer supplierId, String sort);

	List<Order> getOrdersList(OrderSearch search);
	
	String paymentUrl(Integer orderId);

	List<Order> getQBOrdersList(OrderSearch search, String sort);

	int getOrdersListCount(OrderSearch search);
	
	int getOrdersCountByStatus(String status);

	OrderStatus getLatestStatusHistory(Integer orderId);

	Order getOrder(int orderId, String sort);

	void updateOrder(Order order);

	void updateCreditCardPayment(Order order);

	void insertCreditCardHolderAuthentication(Order order);

	void insertOrderStatus(OrderStatus orderStatus);

	boolean isUserUsedPromoCode(String promoCode, Integer userId);

	List<String> getSKUsByUserId(Integer userId);

	int invoiceCount(OrderSearch search);

	List<Order> getInvoiceExportList(OrderSearch search, String sort);

	List<PackingList> getExportPackingList(PackingListSearch search);

	void updatePdfOrder(Order order);

	List<LineItem> getNotProcessedLineItems(Integer orderId, String packingNumber);

	String generateHtmlLineItem(Order order, HttpServletRequest request, ApplicationContext context);

	void generatePackingList(Order order, boolean backendAddedOrder, Date shipDate, boolean inventoryHistory);

	void updateTaxAndTotal(Map gSiteConfig, GlobalDao globalDao, Order order);

	void updateGrandTotal(Map gSiteConfig, Order order);

	void updateOrderPrinted(Order order);

	void updateOrderApproval(Order order);

	List<CustomerReport> getCustomerQuickViewList(Integer userId);

	boolean isValidOrderId(String orderId);

	List<LineItem> getLineItemByOrderId(Integer OrderId);

	OrderSearch getOrderSearch(HttpServletRequest request);

	List<ShippingRate> calculateShippingHandling(Order order, Customer customer, HttpServletRequest request, Map<String, Object> gSiteConfig);

	List<ShippingRate> calculateTileShowRoomShipping(Order order, Customer customer, HttpServletRequest request, Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig);

	List<ShippingRate> calculateViatradingShipping(Order order, GlobalDao globalDao, HttpServletRequest request, Map<String, Object> gSiteConfig, Map<String, Configuration> siteConfig);

	List<Integer> getCustomersWithOrderCount(OrderSearch search);

	QuoteResponse getRoadRunnerQuote(Order order, String username, String password, String originZip, String originType, String paymentType, int actualClass) throws Exception;

	void cancelBuySafeBond(Order order, OrderStatus orderStatus);

	List<String> getPackisListNumByOrderId(int orderId);

	void updateOrdersPaymentStatus(int orderId, String status, Date date);

	void exportXMLInvoices(List<Order> ordersList, Map<Integer, SalesRep> salesRepMap, List<ProductField> productFields, PrintWriter pw, Set<Integer> orderIds, boolean paymentOption);

	void updateInvoiceExported(Set<Integer> orderIds);

	Order getLastOrderByUserId(Integer userId);
	
	Integer getCustomerOrdersNumber(Integer userId);

	List<ConsignmentReport> getConsignmentSales(ConsignmentReport salesReport);

	List<CustomerCreditHistory> getCustomerCreditHistory(Integer supplierId, Integer userId);

	public Double getCustomerOrderBalance(Integer userId, Date dateOrdered);

	public Double getOrderAmountPaid(Integer orderId);

	public void updateOrderPaymentAlert(boolean paymentAlert, Integer orderId);

	public List<CustomerBudgetPartner> getCustomerPartnerHistory(CustomerBudgetPartner partner);

	public void updateCustomerPartnerHistory(CustomerBudgetPartner partner);

	// eBizCharge
	public void updateGatewayToken(String gatewayToken, Integer id);

	// Budget
	void insertOrderAprrovalDenialHistory(Order order, List<Integer> parentsList, MailSender mailSender, Map<String, Configuration> siteConfig);

	List<Order> getOrderActionHistory(Integer orderId);

	boolean getApproval(Integer orderId, Integer actionBy, boolean fullStatus);

	Map<Integer, String> getCustomerNameMap(List<Integer> userIdList);

	// mas90, dsi, triplefin
	void updateOrderExported(String column_name, Date endDate, Set<Integer> orderIds);

	void updateDsiOrder(Order order, OrderStatus orderStatus);

	// Subscriptions
	List<Subscription> getDueSubscriptions();

	List<Subscription> getSubscriptions(SubscriptionSearch search);

	Subscription getSubscription(String code);

	void updateSubscription(Subscription subscription);

	void deleteSubscription(Subscription subscription);

	// Packing List
	void insertPackingList(PackingList packingList, Integer accessUserId, boolean inventoryHistory);

	List<PackingList> getPackingListByOrderId(PackingListSearch packingListSearch);

	PackingList getPackingList(Integer orderId, String packingNumber);

	void updatePackingList(PackingList packingList);

	void ensurePackingListShipDate(Order order, boolean inventoryHistory, Integer accessUserId);

	void updateLineItemOnHand(PackingList packingList, boolean inventoryHistory, Integer accessUserId);

	void productIventoryHistory(InventoryActivity inventoryActivity);

	List<LineItem> getPackingListLineItemsQuantity(Integer orderId);

	List<LineItem> getPackingListLineItemsByPackingNum(String packingNumber);

	int packingListCount(Integer orderId);

	void deletePackingList(Integer orderId, String packingNumber, Integer accessUserId, boolean inventoryHistory);

	Date getShipDateByOrderId(Integer orderId);

	// Image
	boolean resizeImage(String iPath, String oPath, int maxWidth, int maxHeight, String contentType);

	void watermarkImage(String iPath, String oPath, String watermarkText, int r, int g, int b, int watermarkSize, String contentType);

	// PDF
	boolean createPdfInvoice(File pdfFile, int orderId, HttpServletRequest request);

	boolean createPdfInvoice(File pdfFile, List<Integer> orderIds, HttpServletRequest request);

	boolean createPdfPackingList(File pdfFile, int orderId, HttpServletRequest request, Integer supplierId);

	boolean createPdfPO(File pdfFile, PurchaseOrder purchaseOrder, HttpServletRequest request);

	boolean createPdfPresentation(HttpServletRequest request, HttpServletResponse response, List<Integer> productIds, Integer noOfProduct, Presentation presentation, SalesRep salesrep,
			Map<String, Object> gSiteConfig, Map<Integer, Double> presentationProductPrice, boolean admin);

	// admin config: Country, State, ShippingMethod
	// country
	Country getCountryByCode(String code);
	String getCountryCodeByName(String name);


	void updateCountryList(List data);

	List<Country> getCountryList(boolean stats);

	List<Country> getEnabledCountryList();

	Map<String, String> getCountryMap();

	Map<String, Object> getRegionMap();

	List<Country> getRegion();
	
	// state
	State getStateByCode(String country, String code);

	List<State> getStateList(String country);
	
	List<State> getNationalRegionList();

	void updateStateList(List data);

	Map<String, String> getStateMap(String country);

	// shipping method
	ShippingMethod getShippingMethodById(Integer id);

	ShippingMethod getShippingMethodByCarrierCode(ShippingRate shippingRate);

	void updateShippingMethodList(List<ShippingMethod> data);

	List<ShippingMethod> getShippingMethodList(Boolean active);

	boolean isActiveCarrier(String carrierCode);

	ShippingHandling getShippingHandling();

	void updateShippingHandling(ShippingHandling shippingHandling);

	public void insertBudgetPartners(BudgetPartner partner);

	public void updatePartners(BudgetPartner partner);

	public String getPartnerNameById(Integer id);

	public List<BudgetPartner> getPartnersList(Boolean active);

	public BudgetPartner getPartnerById(BudgetPartner partner);

	// custom shipping
	List<ShippingRate> getCustomShippingRateList(boolean all, boolean internal);

	List<Contact> getCustomShippingContactList(Search search);
	List<Contact> getActiveCustomShippingContactList(Search search);
	Contact getCustomShippingContactById(Integer id);

	void insertCustomShippingContact(Contact contact);

	void updateCustomShippingContact(Contact contact);

	void deleteCustomShippingContact(Integer contactId);

	void insertCustomShipping(ShippingRate shippingRate);

	void updateCustomShipping(ShippingRate shippingRate);

	ShippingRate getCustomShippingById(Integer id);

	Double getHandlingByZipcode(String country, String zipcode);

	void getCustomShippingWithMaxValue(List<ShippingRate> customList, List<ShippingRate> regularList, Order order);

	// counties
	List<County> getCounties(String country, String state);

	void updateCountyList(List<County> counties);

	County getTaxRateByCounty(County county);

	// cities
	List<City> getCities(City city);

	void updateCityList(List<City> cities);

	City getTaxRateByCity(City city);

	// Payment Method
	List<PaymentMethod> getCustomPaymentMethodList(Integer custId);

	void updateCustomPaymentMethods(List<PaymentMethod> customPayments);

	// Site Configuration
	Map<String, Configuration> getSiteConfig();

	void updateSiteConfig(List<Configuration> data);

	void updateSiteConfig(String key, String value);

	public String getSiteId();

	int getNextIndexByName(String accountName);

	// Site Message
	List<SiteMessage> getSiteMessageList();

	void deleteSiteMessage(Integer messageId);

	void insertSiteMessage(SiteMessage siteMessage);
	
	void insertEmailHistory(Integer userId, Integer contactId, Integer messageId, Date created);

	void updateSiteMessage(SiteMessage siteMessage);

	SiteMessage getSiteMessageById(Integer messageId);

	SiteMessage getSiteMessageByName(String messageName);
	
	List<SiteMessage> getSiteMessageList(SiteMessageSearch siteMessageSearch);
	
	SiteMessageSearch getSiteMessageSearch(HttpServletRequest request);
	
	// Site Message Group
	List<SiteMessageGroup> getSiteMessageGroupList();
	
	void insertSiteMessageGroup(SiteMessageGroup group);
	
	void updateSiteMessageGroup(SiteMessageGroup group);
	
	public SiteMessageGroup getSiteMessageGroupById(Integer groupId);
	
	public void deleteSiteMessageGroupById(Integer groupId);
	
	public List<SiteMessageGroup> getSiteMessageGroupList(SiteMessageGroupSearch search);

	// language
	void updateSiteMessageLanguage(Language language);

	Language getLanguageSetting(String languageCode);

	List<Language> getLanguageSettings();

	void insertLanguage(String languageCode);

	void deleteLanguage(String languageCode);

	// Admin Message
	SiteMessage getAdminMessageById(int messageId);

	void updateAdminMessage(SiteMessage adminMessage);

	// Layout
	Layout getLayout(int id, HttpServletRequest request);

	List<Layout> getLayoutList();

	Layout getLayoutByCategoryId(int cid, HttpServletRequest request, HttpServletResponse response);

	Layout getSystemLayout(String type, String host, HttpServletRequest request);

	List<Layout> getSystemLayoutList(String type);

	// Multi Store
	void updateMultiStoreConfig(List<MultiStore> multiStores);

	List<MultiStore> getMultiStore(String host);

	Map<String, String> getStoreIdMap();

	// RMA
	Integer getLastOrderId(String serialNum);

	void insertRma(Rma rma);

	List<Rma> getRmaList(RmaSearch search);

	Rma getRma(Rma rma);

	void updateRma(Rma rma);

	LineItem getLineItem(int orderId, String serialNum);
	
	//get LineItem sku
	String getLineItemSku(String sku);

	// Budget
	List<Brand> getBrands(Integer userId);

	Double getOrderTotalByBrand(Brand brand, int userId, int year);

	void updateBudget(int userId, List<Brand> brands);

	boolean getSubTotalPerBrand(Cart cart, Map<Brand, Double> brandsMap);

	List<Product> getProductExportByBrands(String protectedAccess);

	List<Product> getProductListByBrand(Brand brand);

	Map<String, Object> getOrderReportByBrand(Brand brand);

	Map<String, Brand> getBrandMap();

	List<Map<String, Object>> getOrderReportByBrand(Brand brand, String sku);

	boolean getBudgetByCart(Cart cart, Map<String, BudgetProduct> budget);

	List<BudgetProduct> getBudgetProductList(BudgetProductSearch search);

	BudgetProduct getBudgetProduct(int id);

	void updateBudgetProduct(BudgetProduct budgetProduct);

	List<Integer> getBudgetProductYears(BudgetProductSearch search);

	void insertBudgetProduct(BudgetProduct budgetProduct);

	void deleteBudgetProduct(int id);

	Map<String, BudgetProduct> getBudgetProduct(int userId, List<Product> productList);

	// Mass Email
	void insertEmailCampaign(EmailCampaign emailCampaign);

	void updateEmailCampaign(EmailCampaign emailCampaign);

	void finalizeEmailCampaign(EmailCampaign emailCampaign);

	EmailCampaign getEmailCampaignById(Integer campaignId);

	void addEmailCampaignBounce(Integer campaignId);

	void deductEmailCampaignPoint(Integer point);

	void addEmailCampaignPoint(Integer point);

	int emailInProcessCount();

	void insertEmailCampaignBalance(EmailCampaignBalance balance);

	void updateEmailCampaignBalance(EmailCampaignBalance balance);

	void deleteEmailCampaignById(Integer id);

	int getEmailBalance();

	List<EmailCampaignBalance> getBuyCreditList(ReportFilter buyCreditFilter);

	public void updateClickCount(Click click);

	public Integer insertOrGetClick(Click click);

	public List<Click> getClick(Click click);
	
	public List<Unsubscribe> getUnsubscribe(Unsubscribe unsubscribe);

	public List<ClickDetail> getCountDetail(ClickDetail clickDetail);

	public List<ClickDetail> getOpenDetail(ClickDetailSearch clickDetail);

	public void updateOpenCount(Click click);
	
	public void insertUnsubscribeDetails(String campaignId,Integer userId,String email);

	// Location
	List<Location> getLocationList(LocationSearch locationSearch);

	List<Location> searchLocationList(LocationSearch search);

	Map<String, Location> getLocationMap(LocationSearch locationSearch);

	Location getLocationById(Integer locationId);

	void updateLocation(Location location);

	void insertLocation(Location location) throws DataAccessException;

	void deleteLocation(Integer locationId) throws DataAccessException;

	void addToLocationWishList(Integer userId, Integer locationId);

	void removeFromLocationWishList(Integer userId, Set locationIds);

	List<Location> getLocationWishListByUserid(LocationSearch locationSearch);

	void updateKeywordLocationByUserId(Location location) throws DataAccessException;

	List<Location> getLocationListByUserId(Integer userId, Integer limit);

	LocationSearch getLocationSearch(HttpServletRequest request);

	boolean isValidLocationId(int id);

	// master sku
	List<Integer> getSlaves(ProductSearch search);

	Double getProductWeightBySku(String sku);

	void setMasterSkuDetails(Product product, String protectedAccess, boolean checkInventory);

	void setMasterSku(Product product);

	String getProductFieldValue(String sku, String field);

	// Manufacturer
	List<Manufacturer> getManufacturerList(ManufacturerSearch search);

	void insertManufacturer(Manufacturer manufacturer);

	Manufacturer getManufacturerById(Integer manufacturerId);

	Manufacturer getManufacturerByName(String name);

	void deleteManufacturer(Integer manufacturerId);

	void updateManufacturer(Manufacturer manufacturer);

	void updateProductManufacturerName(String old, String newName);

	void updateShoppingcartManufacturerName(String old, String newName);

	boolean isProductExist(String manufacturerName);

	Map<String, Manufacturer> getManufacturerMap();

	boolean checkManufacturerMinOrder(Cart cart, HttpServletRequest request);

	// BuyRequest
	List<BuyRequest> getBuyRequestListByUserid(BuyRequestSearch buyRequestSearch);

	List<BuyRequest> searchBuyRequests(BuyRequestSearch search);

	int searchBuyRequestsCount(BuyRequestSearch search);

	BuyRequest getBuyRequestById(Integer buyRequestId, Integer userId);

	void deleteBuyRequestById(Integer buyRequestId);

	void updateBuyRequest(BuyRequest buyRequest);

	int insertBuyRequest(BuyRequest buyRequest);

	List<BuyRequestVersion> getBuyRequestVesrionList(BuyRequestVersion buyRequestVersion);

	void insertBuyRequestVesrion(BuyRequestVersion buyRequestVersion);

	List<BuyRequest> getBuyRequestWishListByUserid(Integer userId, HttpServletRequest request);

	void addToBuyRequestWishList(Integer userId, String ids[]);

	void removeFromBuyRequestWishList(Integer userId, String ids[]);

	// product review
	List<ProductReview> getProductReviewList(ReviewSearch search);

	int getProductReviewListCount(ReviewSearch search);

	void insertProductReview(ProductReview productReview);

	void deleteProductReviewById(Integer productReviewId);

	List<ProductReview> getProductReviewListByProductSku(String productSku, Boolean active, Integer limit);

	ProductReview getProductReviewById(Integer productReviewId);

	void updateProductReview(ProductReview productReview);

	ProductReview getProductReviewAverage(String productSku);

	boolean checkPreviousReview(ProductReview productReview);

	// company review
	List<CompanyReview> getcompanyReviewList(ReviewSearch search);

	int getCompanyReviewListCount(ReviewSearch search);

	void insertCompanyReview(CompanyReview companyReview);

	void deleteCompanyReviewById(Integer companyReviewId);

	List<ProductReview> getCompanyReviewListByCompanyId(Integer companyId, Boolean active);

	CompanyReview getCompanyReviewById(Integer companyReviewId);

	void updateCompanyReview(CompanyReview companyReview);

	Review getCompanyReviewAverage(Integer companyId);

	// custom frame
	LineItem getCustomFrame(int orderId, int lineNum);

	// techdata
	void nonTransactionSafeInsertTechdataManufacturers(List<TechdataManufacturer> manufacturers);

	void nonTransactionSafeInsertTechdataCategories(List<TechdataCategory> categories);

	List<TechdataCategory> getTechdataCategories();

	void nonTransactionSafeUpdateTechdataCategories(List<TechdataCategory> categories);

	List<TechdataManufacturer> getTechdataManufacturers();

	List<String> getTechdataRestrictedProductLines();

	Map<String, TechdataCategory> getTechdataCategoryMap();

	Map<String, String> getTechdataManufacturerMap();

	void nonTransactionSafeInsertCategories(List<Map<String, Object>> data);

	// ingram micro
	void nonTransactionSafeInsertIngramMicroCategories(List<IngramMicroCategory> categories);

	List<IngramMicroCategory> getIngramMicroCategories();

	void nonTransactionSafeUpdateIngramMicroCategories(List<IngramMicroCategory> categories);

	Map<String, IngramMicroCategory> getIngramMicroCategoryMap();

	void deleteOldProducts(String feed, boolean makeInActive);

	void inventoryZeroInactive(String feed);

	int updateIngramMicroInventoryBySkus(final List<Map<String, Object>> data);

	// Synnex
	void nonTransactionSafeInsertSynnexCategories(List<SynnexCategory> categories);

	List<SynnexCategory> getSynnexCategories();

	void nonTransactionSafeUpdateSynnexCategories(List<SynnexCategory> categories);

	Map<String, SynnexCategory> getSynnexCategoryMap();

	// dsi
	void nonTransactionSafeInsertDsiCategories(List<DsiCategory> categories);

	List<DsiCategory> getDsiCategories();

	void nonTransactionSafeUpdateDsiCategories(List<DsiCategory> categories);

	Map<String, DsiCategory> getDsiCategoryMap();

	// asi
	List<String> getAsiSupplier();

	/*
	 * TO BE REMOVE List<AsiCategory> getAsiCategoriesBySupplier(String supplier); void nonTransactionSafeUpdateAsiCategories(List<AsiCategory> categories); Map<String, AsiCategory> getAsiCategoryMap(); void nonTransactionSafeInsertAsiCategories(List<AsiCategory> categories);
	 * List<AsiCategory> getAsiCategories(Search search); boolean isASIUniqueIdExist(int uniqueId);
	 */
	// Kole Imports
	void nonTransactionSafeInsertKoleImportsCategories(List<KoleImportsCategory> categories);

	List<KoleImportsCategory> getKoleImportsCategories();

	void nonTransactionSafeUpdateKoleImportsCategories(List<KoleImportsCategory> categories);

	Map<String, KoleImportsCategory> getKoleImportsCategoryMap();

	// Dsdi Imports
	void nonTransactionSafeInsertDsdiCategories(List<DsdiCategory> categories);

	List<DsdiCategory> getDsdiCategories();

	void nonTransactionSafeUpdateDsdiCategories(List<DsdiCategory> categories);

	Map<String, DsdiCategory> getDsdiCategoryMap();

	// Etilize
	int getEtilizeCount();

	List<Product> getEtilizeList(int limit, int offset);

	List<Map<String, Object>> getProductsBySkuList(List<Product> skuList);

	List<Map<String, Object>> getEtilizeLuceneProduct(int limit, int offset);

	int getEtilizeLuceneProductCount();

	// Labels
	Map<String, String> getLabels(String... prefix); // Variable-Length
	
	Map<Integer, Integer> getSiteMessageListByGroupId(Integer groupId);

	void updateLabels(List<Map<String, String>> labels);

	// replace dynamic element
	void replaceDynamicElement(EmailMessageForm form, Customer customer, SalesRep salesRep) throws Exception;
	
	void replaceDynamicElement(EmailMessageForm form, Customer customer, AccessUser accessUser) throws Exception;


	void replaceDynamicElement(EmailMessageForm form, CrmContact contact, SalesRep salesRep) throws Exception;

	void replaceDynamicElement(OrderStatus orderStatus, Customer customer, SalesRep salesRep, Order order, String secureUrl, Contact customContact) throws Exception;

	void replaceDynamicElement(EmailMessageForm form, CrmContact contact, AccessUser accessUser) throws Exception;
	
	String replaceDynamicElement(String original, Customer customer, SalesRep salesRep) throws Exception;

	// eBillme
	void updateEbillmePayment(EBillme eBillme, OrderStatus orderStatus);

	void updateEbillmePayment(EBillme eBillme);

	// CRM Account
	Integer getCrmAccountListCount(CrmAccountSearch search);
	
	// CRM Qualifier
	Integer getCrmQualifierListCount(CrmQualifierSearch search);
	
	List<CrmQualifier> getCrmQualifierList(CrmQualifierSearch search);
	
	void insertCrmQualifier(CrmQualifier crmQualifier);
	
	void deleteCrmQualifier(Integer qualifierId);
	
	void updateCrmQualifier(CrmQualifier crmQualifier);

	List<CrmAccount> getCrmAccountList(CrmAccountSearch search);

	List<CrmAccount> getCrmAccountNameList();

	CrmAccount getCrmAccountById(Integer accountId);

	CrmAccount getCrmAccountByName(String accountName);

	String getCrmAccountNameById(Integer accountId);

	void deleteCrmAccount(Integer accountId);

	void insertCrmAccount(CrmAccount crmAccount);

	void updateCrmAccount(CrmAccount crmAccount);

	boolean isCrmAccountExist(String accountName);

	Integer getCrmAccountId(String accountName, String accountNumber);

	void importCrmAccounts(List<CrmAccount> crmAccounts);

	void insertCrmAccountContact(Customer customer, Address shipping);

	List<CrmAccount> getCrmAccountAjax(String accountName);

	// CRM Contact
	Integer getCrmContactListCount(CrmContactSearch search);

	List<CrmContact> getCrmContactList(CrmContactSearch search);
	
	List<CrmContact> getCrmContactList2(CrmContactSearch search);

	List<Integer> getCrmContactIdsList(CrmContactSearch search);

	List<CrmContact> getCrmContactExport(CrmContactSearch search);

	CrmContact getCrmContactById(Integer contactId);

	CrmQualifier getCrmQualifierById(Integer qualifierId);
	
	void deleteCrmContact(Integer contactId);

	void insertCrmContact(CrmContact crmContact);

	void updateCrmContact(CrmContact crmContact);

	List<CrmContact> getCrmContactByEmail1(String email1);

	void importCrmContacts(List<CrmContact> crmContact);

	CrmContactSearch getCrmContactSearch(HttpServletRequest request);

	void insertCrmContactField(CrmContactField crmContactField);

	List<CrmContactField> getCrmContactFieldList(Search search);

	CrmContactField getCrmContactFieldById(Integer fieldId);

	CrmContactField getCrmContactFieldByGlobalName(String fieldName);

	void updateCrmContactField(CrmContactField crmContactField);

	void deleteCrmContactField(Integer fieldId);

	String createToken(Integer contactId);

	void convertCrmContactToCustomer(Integer contactId, Integer userId, String leadSource);

	Map<String, String> getContcatCustomerMap(String key);

	void updateCustomerMap(List<Configuration> data);

	void crmContactToCustomerSynch(CrmContact crmContact, Customer customer, boolean onlyCustomFields) throws Exception;

	void customerToCrmContactSynch(CrmContact crmContact, Customer customer) throws Exception;

	void unsubscribeByCrmContactId(Integer crmContactId);

	Integer getFieldValueCountByFieldIdAndValue(Integer fieldId, String fieldValue);

	boolean validCrmContactToken(String token);

	void batchAssignUserToContacts(List<Integer> contactIds, AccessUser accessUser);
	
	void batchAssignUserToCustomers(List<Integer> userIds, AccessUser accessUser);

	
	void removeUserFromContacts(List<Integer> contactIds);

	void newLeadEmail(CrmContact crmContact, String recipientEmail, String recipientCcEmail, String recipientBccEmail, HttpServletRequest request, CrmForm crmForm, File attachment, JavaMailSenderImpl mailSender) throws Exception;
	
	void newLeadEmail(CrmContact crmContact, String recipientEmail, String recipientCcEmail, String recipientBccEmail, HttpServletRequest request, CrmForm crmForm, File attachment, File attachment1, File attachment2, File attachment3, File attachment4, JavaMailSenderImpl mailSender) throws Exception;

	void batchCrmContactDelete(Set<Integer> contactIds);

	void batchCrmRating(List<Integer> contactIds, String rating);
	
	void batchCrmRating1(List<Integer> contactIds, String rating);
	
	void batchCustomerRating1(List<Integer> contactIds, String rating);
	
	void batchCustomerRating2(List<Integer> contactIds, String rating);
	
	void batchCustomerQualifier(List<Integer> contactIds, String qualifier);
	
	void batchCustomerLanguage(List<Integer> contactIds, String language);
	
	void batchCrmRating2(List<Integer> contactIds, String rating);
	
	void batchCrmQualifier(List<Integer> contactIds, String qualifier);
	
	void batchCrmLanguage(List<Integer> contactIds, String language);

	// group
	List<CrmContactGroup> getContactGroupList(CrmContactGroupSearch search);

	List<CrmContactGroup> getContactGroupNameList();

	CrmContactGroup getContactGroupById(Integer groupId);

	void deleteCrmContactGroupById(Integer groupId);

	void insertCrmContactGroup(CrmContactGroup group) throws DataAccessException;

	void updateCrmContactGroup(CrmContactGroup group);

	List<Integer> getContactGroupIdList(Integer contactId);

	void batchCrmContactGroupIds(Integer contactId, Set groupIds, String action);

	// Task
	Integer getCrmTaskListCount(CrmTaskSearch search);

	List<CrmTask> getCrmTaskList(CrmTaskSearch search);

	CrmTask getCrmTaskById(Integer contactId);

	void insertCrmTask(CrmTask crmTask);

	void updateCrmTask(CrmTask crmTask);

	CrmTaskSearch getCrmTaskSearch(HttpServletRequest request);

	List<CrmTask> getCrmTaskExport(CrmTaskSearch search);

	void deleteCrmTask(Integer taskId);

	void batchCrmTaskDelete(Set<Integer> taskIds);

	// Form
	List<CrmForm> getCrmFormList(Search search);

	CrmForm getCrmFormById(Integer formId);

	CrmForm getCrmFormByFormNumber(String formNumber);

	void deleteCrmForm(Integer formId);

	void insertCrmForm(CrmForm crmForm);

	void updateCrmForm(CrmForm crmForm);

	String createForm(CrmFormBuilderForm crmFormBuilderForm, HttpServletRequest request);

	void sendAutoResponse(SiteMessage siteMessage, CrmContact crmContact, JavaMailSenderImpl mailSender, Map<String, Configuration> siteConfig) throws MessagingException;

	// EchoSign
	void nonTransactionSafeInsertEchoSign(EchoSign echoSign, boolean insertIgnore);

	List<EchoSign> getEchoSignListByUserid(int userId);

	void updateEchoSign(EchoSign echoSign);

	EchoSign getEchoSign(String documentKey);

	Map<Integer, Long> getEchoSignSentMap(int year);

	List<EchoSign> getEchoSignBySearch(EchoSignSearch search);

	void nonTransactionSafeInsertEchoSignWidget(Widget widget);

	Widget getEchoSignWidgetByPath(String path);

	Widget getEchoSignWidget(String documentKey);

	List<Widget> getEchoSignWidgetBySearch(EchoSignWidgetSearch search);

	Map<Integer, EchoSign> getLatestEchoSignMap();

	// amazon checkout
	void updateAmazonPayment(Iopn iopn, OrderStatus orderStatus, List<LineItem> lineItems);

	// jbd
	void insertJbdInvoices(Connection connection, List<Order> orderList, Date cutoff) throws Exception;

	// mas200
	void nonTransactionSafeUpdateMas200order(Order order);

	byte[] getBytesFromFile(File file) throws IOException;

	// buy safe
	void buySafeHeader(BuySafeAPISoap port);

	ShoppingCartCheckoutRS setShoppingCartCheckout(Order order, JavaMailSenderImpl mailSender) throws MalformedURLException, MessagingException;

	ShoppingCartAddUpdateRS addUpdateShoppingCartRequest(Cart cart, Order order, HttpServletRequest request, Integer userId, JavaMailSenderImpl mailSender) throws MalformedURLException,
			MessagingException;

	void sendMessageToBuySafe(MimeMessageHelper helper, Exception e) throws MessagingException;

	// GE Money
	void updateGEMoneyPayment(GEMoney trans, OrderStatus orderStatus);

	// Plow & Hearth
	void updatePlowAndHearthLineItems(List<LineItem> lineItems);

	// pre-hanging
	List<Map<String, Object>> getPreHanging(String doorConfig, String species, String widthHeight, String optionCode);

	int[] updatePreHanging(final List<Map<String, Object>> data);

	void deletePreHanging(int id);

	void insertPreHanging(List<Map<String, Object>> data);

	// order-lookup
	List<UpsOrder> getOrderLookupReport(String customerPO);

	void importUpsOrders(UpsOrder upsWorldShip);

	void updateUpsOrders(UpsOrder upsWorldShip);

	void updateUpsOrdersTrackAndShipDate(UpsOrder upsWorldShip);

	// VBA
	List<VirtualBank> getConsignmentAndAffiliateOrders(VirtualBankOrderSearch orderSearch);

	void updateVbaOrders(List<Order> vbaOrders, List<LineItem> vbaOrdersLineItems, String userName);

	List<VbaPaymentReport> getVbaPaymentReport(VbaPaymentReport reportFilter);

	Map<Integer, String> getCustomerCompanyMap(List<VirtualBank> useridList);

	Map<Integer, Double> getPaymentsTotalToCustomerMap(List<VirtualBank> useridList);

	public String getUserName(Integer userId, Integer contactId);

	// SearchEngine Prospect
	public String addWebReferralProspectInformation(String prospectId, String referrer);

	/**
	 * This method returns a list of all stored information for a given prospectId.
	 * 
	 * @param prospectId
	 *        The prospect identifier
	 * @return A {@link List} of {@link SearchEngineProspectEntity}
	 */
	public List<SearchEngineProspectEntity> getProspectInformation(String prospectId);

	/**
	 * This method will fetch the most recent record for a given prospect
	 * 
	 * @param prospectId
	 *        The prospect identifier
	 * @return A {@link SearchEngineProspectEntity}
	 */
	public SearchEngineProspectEntity getNewestProspectInformation(String prospectId);

	/**
	 * This method will fetch the first record persisted for a given prospect.
	 * 
	 * @param prospectId
	 *        The prospect identifier
	 * @return A {@link SearchEngineProspectEntity}
	 */
	public SearchEngineProspectEntity getOldestProspectInformation(String prospectId);

	public String getSecureUrl(Map<String, Configuration> siteConfig, MultiStore multiStore);

	public String getLeftBarType(HttpServletRequest request, String categoryLeftBarType);

	public void validatePromoCode(Order order, HashMap<String, String> err, Customer customer, String PromocodeLink, Map<String, Object> gSiteConfig);

	public String getNumber(String phoneNumber);

	public void insertAndUpdateCustomerList(List<String> allEmailAddressList, Map<String, CsvMap> availableEmailMap, Map<String, List<CsvData>> customerCsvDataList,Map<String, List<CsvData>> addressCsvDataList, Map<String, List<CsvData>> crmCsvDataList, List<CsvIndex> customerCsvIndex, List<CsvIndex> addressCsvIndex, List<CsvIndex> crmCsvIndex, List<Map<String, Object>> addedItems, List<Map<String, Object>> updatedItems, List<Map<String, Object>> addedCrmItems, List<Map<String, Object>> updatedCrmItems, List<Map<String, Object>> notAddedCrmItems ,  List<Map<String, Object>> notAddedCustomerItems);
				
	public void validateAdminPromoCode(HttpServletRequest request, Order order, HashMap<String, String> err, Customer customer, String PromocodeLink, Map<String, Object> gSiteConfig, Double discountAmt); 

	public void insertCrmTime(String emailAddress, Long crmTime);
	
	public Long getCrmTime(String emailAddress);
	
	public Long getDuplicateEntry(String emailAddress);

	public void updateCrmTime(String emailAddress, Long crmTime);
	
	public Map<Integer, String> getProductIdCategoryIdsMap();
	
	public List<ProductFieldUnlimited> getProductFieldsUnlimited();
	
	public ProductFieldUnlimited getProductFieldsUnlimitedByName(String fieldName);
	
	public List<Category> getCategoryTreeStructure(int catId, String direction);
	
	public int getProductFieldUnlimitedNameValueCount();

	public ProductFieldUnlimitedNameValue getProductFieldUnlimitedNameValue(String sku, int fieldId);
	
	public boolean createPdfInvoice(File pdfFile, int orderId, HttpServletRequest request, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig);
	
	public List<Report> getNewCRMContactReportByYear(Integer year);

	public List<Report> getNewCustomerReportByYear(Integer year);
	
	public List<ProductCategory> getUpdateProductFields();
	
	public List<ProductCategory> getAddProductCategory();
	
	public List<ProductCategory> getRemoveProductCategory();
	
	public void deleteAllUpdateProductFields();
	
	public void deleteAllAddProductCategory();
	
	public void deleteAllRemoveProductCategory();
	
	void updateProductFields(ProductCategory product);
	
	void addProductCategory(ProductCategory product);
	
	void removeProductCategory(ProductCategory product);
	
	String shippingTitle(String title);
	
	void updateCartQuantity(UserSession userSession, HttpServletRequest request, Layout layout);
	
	public String removeCellPhone(String cellPhone);
	
	public String checkUserByCellPhone(String username2);

	public Customer getCellPhoneByUsernameAndPassword(String username2, String password);
	
	public void insertPackingList(PackingList packingList, Integer accessUserId, boolean inventoryHistory, Order order);
	
	public void updateOrderQuote(Integer orderId, String quote);
	
	public String getOrderQuote(Integer orderId);
	
	public Integer checkPreviousPaid(Integer paymentId, Integer orderId);
	
	public void addNewPurchaseOrderHistoryStatus(Integer poId, String status, String accessUser);
	
	public Contact getCompanyContactId(String company);
	
	public List<Customer> getUserNotifications();
	
	public void removeUserNotifications();
	
	public void updateUserNotifications(Customer customer);
	
	public void updateCrmNotifications(CrmContact crmcontact);
	
	public void generateUsername(Customer customer);
	
	public String isProductSoftLink(Integer id);
	
	public String isProductSoftLinkSku(String sku);
	
	public List<CrmContactField> getCrmContactFieldsByContactId(Integer contactId);
	
	public void insertOrderUserDueDate(Order order);
	
	public void updateOrderSubStatus(Order order);

	Integer isThereAnOrderUseThisPromoByCustomer(String promoCode, Integer customerId);

	void updateNewInformation(Integer integer, Integer batchAction);
	
    void importSuppliers(List<Supplier> suppliers);

	public void updateIsFirstOrder(Order order);
	
	List<CustomerExtContact> getCustomerExtContactListByUserid(Integer userId);

	CustomerExtContact getCustomerExtContactById(Integer customerExtContactId);

	void updateCustomerExtContact(CustomerExtContact customerExtContact);

	void insertCustomerExtContact(CustomerExtContact customerExtContact);

	void deleteCustomerExtContact(CustomerExtContact customerExtContact);
}
